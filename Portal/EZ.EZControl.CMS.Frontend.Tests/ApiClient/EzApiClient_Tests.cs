﻿using System;
using EZ.EZControl.CMS.Frontend.Infra.ApiClient;
using Moq;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.CMS.Frontend.Tests.ApiClient
{
    public class Foo
    {

    }

    public class EzApiClient_Tests : EzApiClientBase
    {
        [Fact]
        public async Task Should_PostAsync()
        {
            // Arrange
            Mock.Setup(x => x.PostAsync<string>(It.IsAny<string>(), It.IsAny<object>(), null, false)).Returns(() => Task.FromResult(string.Empty));

            // Action
            var teste = await Mock.Object.PostAsync<string>("", new object());

            // Assert
            teste.ShouldBe(string.Empty);
        }

        [Fact]
        public void Should_Convert_Object_To_Json()
        {
            // Arrange
            var obj = new Foo();

            // Action
            var teste = EzApiClient.Object2JsonString(obj);

            // Assert
            teste.ShouldBe("{}");

        }

        [Fact]
        public void Should_Convert_Json_To_Object()
        {
            // Arrange
            var jsonStrig = "{}";

            // Action
            var teste = EzApiClient.JsonString2Object<Foo>(jsonStrig);

            // Assert
            teste.ShouldBeOfType<Foo>();
        }

        [Fact]
        public void Should_GetAbsoluteUrl()
        {
            // Arrange
            var ezApiClient = new EzApiClient();
            ezApiClient.BaseUrl = "http://ezcontrol.local/";

            // Action
            var teste = ezApiClient.GetAbsoluteUrl("api/services/teste/getById");

            // Assert
            teste.ShouldBe(string.Concat(ezApiClient.BaseUrl, "api/services/teste/getById"));
        }

        [Fact]
        public void Should_GetAbsoluteUrl_Using_BaseUrl()
        {
            // Arrange
            var ezApiClient = new EzApiClient();

            // Action
            var teste = ezApiClient.GetAbsoluteUrl("http://ezcontrol.local/", "api/services/teste/getById");

            // Assert
            teste.ShouldBe(string.Concat("http://ezcontrol.local/", "api/services/teste/getById"));
        }
    }
}