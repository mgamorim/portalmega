﻿using EZ.EZControl.CMS.Frontend.Infra.ApiClient;
using Moq;

namespace EZ.EZControl.CMS.Frontend.Tests.ApiClient
{
    public class EzApiClientBase
    {
        public Mock<EzApiClient> Mock;

        public EzApiClientBase()
        {
            Mock = new Mock<EzApiClient>();
        }
    }
}