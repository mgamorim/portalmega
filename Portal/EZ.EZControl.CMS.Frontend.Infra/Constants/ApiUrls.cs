﻿namespace EZ.EZControl.CMS.Frontend.Infra.Constants
{
    public static class ApiUrls
    {
        public static class Cms
        {
            public static class Categoria
            {
                public const string Save = "/api/services/app/cms/categoria/Save";
                public const string GetPaged = "/api/services/app/cms/categoria/GetCategoriasPaginado";
            }

            public static class Site
            {
                public const string GetSiteByHost = "/api/services/app/cms/site/GetByHost";
            }

            public static class Link
            {
                public const string GetLink = "/api/services/app/cms/link/GetLink";
            }

            public static class Pagina
            {
                public const string GetBySlug = "/api/services/app/cms/pagina/GetBySlug";
            }

            public static class Conteudo
            {
                public const string Get = "/api/services/app/cms/conteudo/Get";
            }
        }

        public static class CmsFrontend
        {
            public static class Menu
            {
                public const string GetMenu = "/api/services/app/cmsfrontend/menu/GetMenu";
            }

            public static class Template
            {
                public const string GetById = "/api/services/app/cmsfrontend/template/GetById";
                public const string GetByIdAnonymous = "/api/services/app/cms/template/GetByIdAnonymous";
            }

            public static class Widget
            {
                public const string GetById = "/api/services/app/cmsfrontend/widget/GetById";
                public const string Get = "/api/services/app/cmsfrontend/widget/Get";
                public const string GetList = "/api/services/app/cmsfrontend/widget/GetList";
            }

            public static class TemplatePadrao
            {
                public const string SaveContato = "/api/services/app/cmsfrontend/templatePadrao/SaveContato";
            }
        }

        public static class Global
        {
            public static class Profissao
            {
                public const string GetProfissao = "/api/services/app/global/profissao/GetProfissao";
            }

            public static class GrupoPessoa
            {
                public const string GetGrupoPessoa = "/api/services/app/global/grupoPessoa/GrupoPessoa";
            }
        }

        public static class EzLiv
        {
            public static class Beneficiario
            {
                public const string CreateUserPessoaBeneficiario = "/api/services/app/ezliv/beneficiarioBase/CreateUserPessoaBeneficiarioFront";
            }

            public static class Corretora
            {
                public const string GetCorretora = "/api/services/app/ezliv/corretora/GetCorretora";
            }
        }
    }
}