﻿using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Link;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Pagina;
using System.Threading.Tasks;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class PaginaService : ServiceBase
    {
        public async Task<PaginaInput> GetBySlug(GetPaginaBySlugInput input)
        {

            return await PostAsync<PaginaInput>(ApiUrls.Cms.Pagina.GetBySlug, input);
        }
    }
}