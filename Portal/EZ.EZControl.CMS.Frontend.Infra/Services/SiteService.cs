﻿using EZ.EZControl.CMS.Frontend.Infra.ApiClient;
using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site;
using System;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class SiteService : ServiceBase
    {
        public async Task<SiteFrontInput> GetSiteAsync(HttpRequestBase request)
        {
            SetToken(request);

            var host = request.Url.Host;
            var hostInfoCache = EzApiClient.HostInfoCache[host];
            if (hostInfoCache.Site != null)
            {
                return await Task.FromResult<SiteFrontInput>(hostInfoCache.Site);
            }
            else
            {
                var site = await PostAsync<SiteFrontInput>(ApiUrls.Cms.Site.GetSiteByHost, new GetSiteByHostFrontInput { Host = request.Url.Host });
                hostInfoCache.Site = site;
                return site;
            }
        }

        public SiteFrontInput GetSite(HttpRequestBase request)
        {
            SetToken(request);

            var host = request.Url.Host;
            var hostInfoCache = EzApiClient.HostInfoCache[host];
            if (hostInfoCache.Site != null)
            {
                return hostInfoCache.Site;
            }
            else
            {
                var site = Post<SiteFrontInput>(ApiUrls.Cms.Site.GetSiteByHost, new GetSiteByHostFrontInput { Host = host });
                hostInfoCache.Site = site;
                return site;
            }
        }
    }
}