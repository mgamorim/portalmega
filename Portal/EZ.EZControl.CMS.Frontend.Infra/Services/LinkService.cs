﻿using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Link;
using System.Threading.Tasks;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class LinkService : ServiceBase
    {
        public async Task<LinkFrontInput> GetLink(LinkFrontInput input)
        {

            return await PostAsync<LinkFrontInput>(ApiUrls.Cms.Link.GetLink, input);
        }
    }
}