﻿using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class WidgetService : ServiceBase
    {
        public async Task<WidgetFrontInput> GetAsync(GetWidgetFrontInput input)
        {
            return await PostAsync<WidgetFrontInput>(ApiUrls.CmsFrontend.Widget.Get, input);
        }

        public WidgetFrontInput Get(GetWidgetFrontInput input)
        {
            return Post<WidgetFrontInput>(ApiUrls.CmsFrontend.Widget.Get, input);
        }

        public IEnumerable<WidgetFrontInput> GetList(GetWidgetFrontInput input)
        {
            return Post<IEnumerable<WidgetFrontInput>>(ApiUrls.CmsFrontend.Widget.GetList, input);
        }
    }
}