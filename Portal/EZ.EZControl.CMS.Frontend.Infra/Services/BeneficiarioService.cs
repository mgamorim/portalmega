﻿using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMSFrontend.Beneficiario;
using System.Threading.Tasks;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class BeneficiarioService : ServiceBase
    {
        public async Task<BeneficiarioUserPessoaInput> CreateUserPessoaBeneficiario(BeneficiarioUserPessoaInput input)
        {
            return await PostAsync<BeneficiarioUserPessoaInput>(ApiUrls.EzLiv.Beneficiario.CreateUserPessoaBeneficiario, input);
        }
    }
}