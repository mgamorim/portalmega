﻿using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.Abp;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Template;
using System.Threading.Tasks;
using EZ.EZControl.CMS.Frontend.Infra.ApiClient;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMSFrontend.Template;
using System;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class TemplateService : ServiceBase
    {
        public async Task<TemplateFrontInput> GetAsync(GetTemplateFrontInputDto input)
        {
            var hostInfoCache = EzApiClient.HostInfoCache[input.Host];
            if (hostInfoCache.TemplateDefault != null)
            {
                return await Task.FromResult<TemplateFrontInput>(hostInfoCache.TemplateDefault);
            }
            else
            {
                try
                {
                    var template = await PostAsync<TemplateFrontInput>(ApiUrls.CmsFrontend.Template.GetByIdAnonymous, new IdInput { Id = input.Id });
                    hostInfoCache.TemplateDefault = template;
                    return template;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public TemplateFrontInput Get(GetTemplateFrontInputDto input)
        {
            var hostInfoCache = EzApiClient.HostInfoCache[input.Host];
            if (hostInfoCache.TemplateDefault != null)
            {
                return hostInfoCache.TemplateDefault;
            }
            else
            {
                var template = Post<TemplateFrontInput>(ApiUrls.CmsFrontend.Template.GetById, new IdInput() { Id = input.Id });
                hostInfoCache.TemplateDefault = template;
                return template;
            }
        }
    }
}