﻿using EZ.EZControl.CMS.Frontend.Infra.Constants;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Link;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Pagina;
using System.Threading.Tasks;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Conteudo;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public class ConteudoService : ServiceBase
    {
        public async Task<ConteudoInput> Get(GetConteudoByUrlInput input)
        {

            return await PostAsync<ConteudoInput>(ApiUrls.Cms.Conteudo.Get, input);
        }
    }
}