﻿using EZ.EZControl.CMS.Frontend.Infra.ApiClient;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Infra.Services
{
    public abstract class ServiceBase
    {
        private EzApiClient _ezApiClient;

        protected ServiceBase()
        {
            _ezApiClient = new EzApiClient();
        }

        public async Task<TResult> PostAsync<TResult>(string url, object input, int? timeout = null)
            where TResult : class
        {
            return await _ezApiClient.PostAsync<TResult>(url, input, timeout);
        }

        //Todo: Versão para teste
        public TResult Post<TResult>(string url, object input, int? timeout = null)
           where TResult : class
        {
            return _ezApiClient.Post<TResult>(url, input, timeout);
        }

        public void SetToken(HttpRequestBase request)
        {
            _ezApiClient.SetToken(request);
        }

        public string GetAbsoluteUrl(string relativeUrl)
        {
            return _ezApiClient.GetAbsoluteUrl(relativeUrl);
        }

        public string GetAbsoluteUrl(string baseUrl, string relativeUrl)
        {
            return _ezApiClient.GetAbsoluteUrl(baseUrl, relativeUrl);
        }

        public string Object2JsonString(object obj)
        {
            return EzApiClient.Object2JsonString(obj);
        }

        public TObj JsonString2Object<TObj>(string str)
        {
            return EzApiClient.JsonString2Object<TObj>(str);
        }
    }
}