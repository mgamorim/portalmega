﻿using EZ.EZControl.CMS.Frontend.Infra.Models;
using System;
using System.Runtime.Serialization;

namespace EZ.EZControl.CMS.Frontend.Infra.Exceptions
{
    [Serializable]
    public class RemoteCallException : BaseException
    {
        public RemoteCallException()
        {
        }

        public RemoteCallException(ErrorInfo errorInfo) : base(errorInfo.Message)
        {
            this.ErrorInfo = errorInfo;
        }

        public RemoteCallException(SerializationInfo serializationInfo, StreamingContext context) : base(serializationInfo, context)
        {
        }

        public ErrorInfo ErrorInfo { get; set; }
    }
}