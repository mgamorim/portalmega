﻿namespace EZ.EZControl.CMS.Frontend.Infra.Entities.Enums
{
    public enum TipoDeWidgetEnum
    {
        CmsMenu = 1,
        CmsHtml = 2
    }
}
