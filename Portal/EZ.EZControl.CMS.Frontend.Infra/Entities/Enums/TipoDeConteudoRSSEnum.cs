﻿namespace EZ.EZControl.CMS.Frontend.Infra.Entities.Enums
{
    public enum TipoDeConteudoRSSEnum
    {
        Completo = 1,
        Resumo = 2
    }
}