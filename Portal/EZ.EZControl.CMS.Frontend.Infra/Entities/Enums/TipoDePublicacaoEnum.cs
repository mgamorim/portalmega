﻿namespace EZ.EZControl.CMS.Frontend.Infra.Entities.Enums
{
    public enum TipoDePublicacaoEnum
    {
        Post = 1,
        Pagina = 2
    }
}