﻿namespace EZ.EZControl.CMS.Frontend.Infra.Entities.Enums
{
    public enum SistemaEnum
    {
        Global = 1,
        CMS = 2,
        Agenda = 3,
        EzMedical = 4
    }
}
