﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMSFrontend.Template
{
    public class GetTemplateFrontInputDto
    {
        public int Id { get; set; }
        public string Host { get; set; }
    }
}
