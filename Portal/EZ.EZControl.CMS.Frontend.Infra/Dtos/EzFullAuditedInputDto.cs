﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.Abp;
using System;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos
{
    public class EzFullAuditedInputDto : FullAuditedEntityDto
    {
        public int ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
        public bool IsIntegration { get; set; }
    }
}