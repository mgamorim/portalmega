﻿using EZ.EZControl.CMS.Frontend.Infra.Enums;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos
{
    public class FrontEndResultBaseDto<TOutput>
        where TOutput : class
    {
        public TOutput Resultado { get; set; }

        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public CMSFrontEndResultStatusEnum Status { get; set; }
    }
}