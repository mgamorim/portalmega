﻿namespace EZ.EZControl.CMS.Frontend.Infra.Dtos
{
    public class FormDtoBase
    {
        public string Name { get; set; }

        public string UrlBase { get; set; }
    }
}