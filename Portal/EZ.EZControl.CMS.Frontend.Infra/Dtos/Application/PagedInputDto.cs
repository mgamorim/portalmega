﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.Abp;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.Application
{
    public class PagedInputDto : IPagedResultRequest
    {
        [Range(1, AppConsts.MaxPageSize)]
        public int MaxResultCount { get; set; }

        [Range(0, int.MaxValue)]
        public int SkipCount { get; set; }

        public PagedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }
}