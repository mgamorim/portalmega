﻿namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.Application
{
    public class PagedAndSortedInputDto : PagedInputDto
    {
        public string Sorting { get; set; }

        public PagedAndSortedInputDto()
        {
            MaxResultCount = AppConsts.DefaultPageSize;
        }
    }
}