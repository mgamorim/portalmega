﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.Abp;
using System;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos
{
    public class EzInputDto : EntityDto
    {
        public int ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
        public bool IsIntegration { get; set; }
    }
}