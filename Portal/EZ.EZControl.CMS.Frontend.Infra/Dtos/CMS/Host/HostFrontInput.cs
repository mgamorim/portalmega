﻿using EZ.EZControl.CMS.Frontend.Infra.Entities.Abp;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Host
{
    public class HostFrontInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Url { get; set; }
        public bool IsPrincipal { get; set; }
    }
}