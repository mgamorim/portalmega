﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Categoria;
using EZ.EZControl.CMS.Frontend.Infra.Enums;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Link
{
    public class LinkFrontInput : EzInputDto
    {
        public TipoDeLinkEnum TipoDeLink { get; set; }
        public int SiteId { get; set; }
        public int ConteudoId { get; set; }
        public int CategoriaPaiId { get; set; }
        public string Titulo { get; set; }
        public string Slug { get; set; }
        public string Link { get; set; }
        public CategoriaFrontInput PrimeiraCategoria { get; set; }
    }
}
