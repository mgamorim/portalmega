﻿using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Pagina
{
    public class GetPaginaBySlugInput
    {
        [Required]
        public int SiteId { get; set; }

        public string Slug { get; set; }
    }
}
