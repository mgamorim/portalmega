﻿using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Conteudo
{
    public class GetConteudoByUrlInput
    {
        [Required]
        public string Url { get; set; }

        public int ItensPorPagina { get; set; }

        public int NumeroDaPagina { get; set; }
    }
}
