﻿using EZ.EZControl.CMS.Frontend.Infra.Entities.Abp;
using EZ.EZControl.CMS.Frontend.Infra.Entities.Enums;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site
{
    public class SiteFrontListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Host { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTags { get; set; }
        public string Owner { get; set; }
        public int TemplateDefaultId { get; set; }
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public TipoDeConteudoRSSEnum TipoDeConteudoRSS { get; set; }
        public int LimitePostPorPagina { get; set; }
        public int LimitePostPorRss { get; set; }
        public bool EvitarMecanismoDeBusca { get; set; }
        public bool PermitirComentarios { get; set; }
        public bool PermitirLinks { get; set; }
        public bool AutorInformaNomeEmail { get; set; }
        public bool NotificarPorEmailNovoComentario { get; set; }
        public bool AtivarModeracaoDeComentario { get; set; }
        public string TextoCss { get; set; }
        public string TextoJavaScript { get; set; }
        public bool HabilitarPesquisaPost { get; set; }
        public bool HabilitarPesquisaPagina { get; set; }
    }
}