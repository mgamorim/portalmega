﻿namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site
{
    public class GetSiteByHostFrontInput
    {
        public string Host { get; set; }
    }
}