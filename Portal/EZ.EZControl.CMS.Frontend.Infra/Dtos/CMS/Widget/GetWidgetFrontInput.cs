﻿namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Widget
{
    public class GetWidgetFrontInput : EzInputDto
    {
        public int SiteId { get; set; }
        public int TemplateId { get; set; }
        public string Posicao { get; set; }
    }
}
