﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Menu;
using EZ.EZControl.CMS.Frontend.Infra.Entities.Enums;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Widget
{
    public class WidgetFrontInput : EzInputDto
    {
        public string Titulo { get; set; }
        public bool ExibirTitulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public int SiteId { get; set; }
        public int TemplateId { get; set; }
        public TipoDeWidgetEnum Tipo { get; set; }
        public string Posicao { get; set; }
        public bool IsActive { get; set; }
        public MenuFrontInput Menu { get; set; }
        public string Conteudo { get; set; }
    }
}
