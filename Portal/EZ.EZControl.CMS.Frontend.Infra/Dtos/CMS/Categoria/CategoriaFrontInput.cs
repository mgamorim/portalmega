﻿namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Categoria
{
    public class CategoriaFrontInput : EzInputDto
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Slug { get; set; }
        public int? CategoriaPaiId { get; set; }
        public int PostId { get; set; }
        public int SiteId { get; set; }
    }
}