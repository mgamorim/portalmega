﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.Application;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Categoria
{
    public class GetCategoriaFrontInput : PagedAndSortedInputDto
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}