﻿using EZ.EZControl.CMS.Frontend.Infra.Entities.Abp;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Template
{
    public class TemplateFrontInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Autor { get; set; }
        public string AutorUrl { get; set; }
        public string Versao { get; set; }
        public string NomeDoArquivo { get; set; }
        public string Posicoes { get; set; }
        public string TiposDeWidgetSuportados { get; set; }
        public string TiposDePaginasSuportadas { get; set; }
        public string TipoDePaginaParaPaginaDefault { get; set; }
        public string TipoDePaginaParaPostDefault { get; set; }
        public string TipoDePaginaParaCategoriaDefault { get; set; }    
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }
    }
}
