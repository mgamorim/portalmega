﻿using EZ.EZControl.CMS.Frontend.Infra.Enums;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.ItemDeMenu
{
    public class ItemDeMenuFrontInput : EzInputDto
    {
        public string Titulo { get; set; }
        public string DescricaoDoTitulo { get; set; }
        public int MenuId { get; set; }
        public int? ItemMenuId { get; set; }
        public int? CategoriaId { get; set; }
        public int? PaginaId { get; set; }
        public TipoDeItemDeMenuEnum TipoDeItemDeMenu { get; set; }
        public string Url { get; set; }
    }
}
