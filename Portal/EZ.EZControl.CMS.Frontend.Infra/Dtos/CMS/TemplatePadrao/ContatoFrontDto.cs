﻿namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.TemplatePadrao
{
    public class ContatoFrontDto : FormDtoBase
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Mensagem { get; set; }
    }
}