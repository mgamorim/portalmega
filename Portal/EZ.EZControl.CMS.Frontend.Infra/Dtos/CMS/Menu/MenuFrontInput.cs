﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.ItemDeMenu;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Menu
{
    public class MenuFrontInput : EzInputDto
    {
        public Boolean IsActive { get; set; }
        public String Nome { get; set; }
        public virtual SiteFrontInput Site { get; set; }
        public virtual ICollection<ItemDeMenuFrontInput> ItensDeMenu { get; set; }
    }
}
