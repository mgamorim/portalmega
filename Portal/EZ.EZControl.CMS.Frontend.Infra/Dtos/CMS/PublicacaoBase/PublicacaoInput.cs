﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.PublicacaoBase
{
    public class PublicacaoInput
    {
        public bool IsActive { get; set; }
        [Required]
        public string Titulo { get; set; }
        [Required]
        public string Slug { get; set; }
        public string Conteudo { get; set; }
        public int IdImagemDestaque { get; set; }
        public int SiteId { get; set; }
        public DateTime Data { get; set; }
    }
}