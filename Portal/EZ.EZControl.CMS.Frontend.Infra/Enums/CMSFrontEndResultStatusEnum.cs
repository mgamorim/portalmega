﻿namespace EZ.EZControl.CMS.Frontend.Infra.Enums
{
    public enum CMSFrontEndResultStatusEnum
    {
        Sucesso = 1,
        Atencao = 2,
        Erro = 3
    }
}