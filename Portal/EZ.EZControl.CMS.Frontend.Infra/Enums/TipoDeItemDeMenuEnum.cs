﻿namespace EZ.EZControl.CMS.Frontend.Infra.Enums
{
    public enum TipoDeItemDeMenuEnum
    {
        Pagina = 1,
        Link = 2,
        Categoria = 3
    }
}
