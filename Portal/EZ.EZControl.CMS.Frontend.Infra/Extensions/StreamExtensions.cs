﻿using System.IO;

namespace EZ.EZControl.CMS.Frontend.Infra.Extensions
{
    public static class StreamExtensions
    {
        public static byte[] GetAllBytes(this Stream stream)
        {
            using (MemoryStream stream2 = new MemoryStream())
            {
                stream.CopyTo(stream2);
                return stream2.ToArray();
            }
        }
    }
}