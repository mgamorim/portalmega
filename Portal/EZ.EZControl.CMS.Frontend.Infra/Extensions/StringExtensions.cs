﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Infra.Extensions
{
    public static class StringExtensions
    {
        public static string Slugify(this string phrase, int maxLength = 70)
        {
            string str = phrase.RemoveDiacritics().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= maxLength ? str.Length : maxLength).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        static string RemoveDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        public static string EZHtmlEncode(this string text)
        {
            return HttpUtility.HtmlEncode(text);
        }

        public static string EZHtmlDecode(this string text)
        {
            return HttpUtility.HtmlDecode(text);
        }

        public static bool IsValidCharacters(this string str)
        {
            return !string.IsNullOrEmpty(str) && Regex.IsMatch(str, @"^[a-zA-Z0-9_,-. ]+$");
        }

        public static bool IsValidEmail(this string str)
        {
            return !string.IsNullOrEmpty(str) && Regex.IsMatch(str,
                @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                RegexOptions.IgnoreCase);
        }
    }
}
