﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Template;

namespace EZ.EZControl.CMS.Frontend.Infra.ApiClient
{
    public class HostInfoCache
    {
        public SiteFrontInput Site { get; set; }

        public TemplateFrontInput TemplateDefault { get; set; }

        public string TokenGuestUser { get; set; }
    }
}