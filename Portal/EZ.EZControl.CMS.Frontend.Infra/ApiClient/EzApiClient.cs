﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos;
using EZ.EZControl.CMS.Frontend.Infra.Exceptions;
using EZ.EZControl.CMS.Frontend.Infra.Models;
using EZ.EZControl.CMS.Frontend.Infra.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Infra.ApiClient
{
    public class EzApiClient
    {
        #region Properties

        private LoginDto GuestUserLogin
        {
            get
            {
                var tenancyName = ConfigurationManager.AppSettings["tenancyName"];
                var userName = ConfigurationManager.AppSettings["userName"];
                var userPassword = ConfigurationManager.AppSettings["userPassword"];

                return new LoginDto() { TenancyName = tenancyName, UserName = userName, Password = userPassword };
            }
        }

        public static Dictionary<string, HostInfoCache> HostInfoCache { get; set; }

        public string BaseUrl { get; set; }

        public static TimeSpan DefaultTimeout { get; set; }

        public TimeSpan Timeout { get; set; }

        private Dictionary<string, string> RequestHeaders { get; set; }

        private ICollection<NameValue> ResponseHeaders { get; set; }

        public Collection<Cookie> Cookies { get; private set; }

        #endregion Properties

        #region Constructors

        static EzApiClient()
        {
            DefaultTimeout = TimeSpan.FromSeconds(90);
            HostInfoCache = new Dictionary<string, HostInfoCache>();
        }

        public EzApiClient()
        {
            Timeout = DefaultTimeout;
            Cookies = new Collection<Cookie>();
            RequestHeaders = new Dictionary<string, string>();
            ResponseHeaders = new List<NameValue>();

            if (HttpContext.Current != null)
            {
                BaseUrl = HttpContext.Current.Request.IsLocal
                    ? ConfigurationManager.AppSettings["hostDebug"]
                    : BaseUrl = ConfigurationManager.AppSettings["hostRelease"];
            }


        }

        #endregion Constructors

        #region Private Methods

        private void SetResponseHeaders(HttpResponseMessage response)
        {
            ResponseHeaders.Clear();
            foreach (var header in response.Headers)
            {
                foreach (var headerValue in header.Value)
                {
                    ResponseHeaders.Add(new NameValue(header.Key, headerValue));
                }
            }
        }

        //TODO:Configurar aa entidades Empresa, Site e Host do EZControl O login funcionar com o Multi-Empresa
        private string Login(LoginDto loginDto)
        {
            if (string.IsNullOrEmpty(BaseUrl))
            {
                throw new Exception("É necessário definir o endereço do host");
            }

            var token = string.Empty;

            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, "api/Account/Authenticate");

            token = AsyncHelper.RunSync(() =>
                PostAsync<string>(
                    uri.AbsoluteUri,
                    new
                    {
                        TenancyName = loginDto.TenancyName,
                        UsernameOrEmailAddress = loginDto.UserName,
                        Password = loginDto.Password
                    })
            );

            return token;
        }

        private void SetBearerToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new Exception("Token em branco não é válido.");
            }

            KeyValuePair<string, string> bearerToken = new KeyValuePair<string, string>("Authorization", "Bearer " + token);

            if (!RequestHeaders.ContainsValue(bearerToken.Value))
            {
                RequestHeaders.Remove(bearerToken.Key);
                RequestHeaders.Add(bearerToken.Key, bearerToken.Value);
            }
        }

        private string GetTokenFromAutorizationHeader(HttpRequestBase request)
        {
            return request.Headers["Authorization"];
        }

        private string GetTokenForGuestUser()
        {
            return Login(GuestUserLogin);
        }

        #endregion Private Methods

        #region Public Methods

        public static string Object2JsonString(object obj)
        {
            if (obj == null)
            {
                return "";
            }

            return JsonConvert.SerializeObject(obj,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }

        public static TObj JsonString2Object<TObj>(string str)
        {
            return JsonConvert.DeserializeObject<TObj>(str,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }

        //Todo: Versão para teste
        public virtual TResult Post<TResult>(string url, object input, int? timeout = null, bool secondAttempt = false)
            where TResult : class
        {
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = timeout.HasValue ? TimeSpan.FromMilliseconds(timeout.Value) : Timeout;

                    if (!string.IsNullOrEmpty(BaseUrl))
                    {
                        client.BaseAddress = new Uri(BaseUrl);
                    }

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    foreach (var header in RequestHeaders)
                    {
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }

                    using (var requestContent = new StringContent(Object2JsonString(input), Encoding.UTF8, "application/json"))
                    {
                        foreach (var cookie in Cookies)
                        {
                            if (!string.IsNullOrEmpty(BaseUrl))
                            {
                                cookieContainer.Add(new Uri(BaseUrl), cookie);
                            }
                            else
                            {
                                cookieContainer.Add(cookie);
                            }
                        }

                        using (var response = client.PostAsync(url, requestContent).Result)
                        {
                            SetResponseHeaders(response);

                            if (!response.IsSuccessStatusCode)
                            {
                                if (response.StatusCode == HttpStatusCode.Unauthorized && !secondAttempt)
                                {
                                    var token = GetTokenForGuestUser();
                                    SetBearerToken(token);

                                    return Post<TResult>(url, input, timeout, true);

                                }

                                throw new HttpException((int)response.StatusCode,
                                    "Could not made request to " + url + "! StatusCode: " + response.StatusCode +
                                    ", ReasonPhrase: " + response.ReasonPhrase);
                            }

                            var ajaxResponse = JsonString2Object<AjaxResponse<TResult>>(response.Content.ReadAsStringAsync().Result);
                            if (!ajaxResponse.Success)
                            {
                                throw new RemoteCallException(ajaxResponse.Error);
                            }

                            return ajaxResponse.Result;
                        }
                    }
                }
            }
        }

        public virtual async Task<TResult> PostAsync<TResult>(string url, object input, int? timeout = null, bool secondAttempt = false)
           where TResult : class
        {
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = timeout.HasValue ? TimeSpan.FromMilliseconds(timeout.Value) : Timeout;

                    if (!string.IsNullOrEmpty(BaseUrl))
                    {
                        client.BaseAddress = new Uri(BaseUrl);
                    }

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    foreach (var header in RequestHeaders)
                    {
                        client.DefaultRequestHeaders.Add(header.Key, header.Value);
                    }

                    using (var requestContent = new StringContent(Object2JsonString(input), Encoding.UTF8, "application/json"))
                    {
                        foreach (var cookie in Cookies)
                        {
                            if (!string.IsNullOrEmpty(BaseUrl))
                            {
                                cookieContainer.Add(new Uri(BaseUrl), cookie);
                            }
                            else
                            {
                                cookieContainer.Add(cookie);
                            }
                        }

                        using (var response = await client.PostAsync(url, requestContent))
                        {
                            SetResponseHeaders(response);

                            if (!response.IsSuccessStatusCode)
                            {
                                if (response.StatusCode == HttpStatusCode.Unauthorized && !secondAttempt)
                                {
                                    var token = GetTokenForGuestUser();
                                    SetBearerToken(token);

                                    return await PostAsync<TResult>(url, input, timeout, true);

                                }

                                throw new HttpException((int)response.StatusCode,
                                    "Could not made request to " + url + "! StatusCode: " + response.StatusCode +
                                    ", ReasonPhrase: " + response.ReasonPhrase);
                            }

                            var ajaxResponse = JsonString2Object<AjaxResponse<TResult>>(await response.Content.ReadAsStringAsync());
                            if (!ajaxResponse.Success)
                            {
                                throw new RemoteCallException(ajaxResponse.Error);
                            }

                            return ajaxResponse.Result;
                        }
                    }
                }
            }
        }

        public virtual void SetToken(HttpRequestBase request)
        {
            string host = string.Empty;
            string token;

            if (request.Url != null)
            {
                host = request.Url.Host;
            }

            if (string.IsNullOrEmpty(host))
            {
                throw new Exception("Host não identificado.");
            }

            // Procurar pelo host no dicionario de sites
            if (!HostInfoCache.ContainsKey(host))
            {
                HostInfoCache.Add(host, new HostInfoCache());
            }

            var hostInfoCache = HostInfoCache[host];

            if (string.IsNullOrEmpty(hostInfoCache.TokenGuestUser))
            {
                token = GetTokenFromAutorizationHeader(request);

                if (string.IsNullOrEmpty(token))
                {
                    token = GetTokenForGuestUser();
                }
            }
            else
            {
                token = hostInfoCache.TokenGuestUser;
            }

            hostInfoCache.TokenGuestUser = token;
            SetBearerToken(token);
        }

        public string GetAbsoluteUrl(string relativeUrl)
        {
            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, relativeUrl);

            return uri.AbsoluteUri;
        }

        public string GetAbsoluteUrl(string baseUrl, string relativeUrl)
        {
            Uri baseUri = new Uri(baseUrl);
            Uri uri = new Uri(baseUri, relativeUrl);

            return uri.AbsoluteUri;
        }

        #endregion Public Methods
    }
}