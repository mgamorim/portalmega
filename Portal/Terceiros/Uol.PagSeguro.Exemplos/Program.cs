﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Uol.PagSeguro.Resources;

namespace Uol.PagSeguro.Exemplos
{
    /// <summary>
    /// Dicas: 
    /// 1: para criar o SenderHash devemos executar o método javascript PagSeguroDirectPayment.getSenderHash() através da lib:
    /// Sandbox ->  https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js 
    /// Produção -> https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js
    /// 
    /// 2: Email e token no ambiente sandbox do vendedor: pagseguro@ezsoft.com.br 9F2E8A7316474DAAB997A407AC4BCFE6
    /// 
    /// 3: Email no ambiente sandbox do comprador de teste: c14972279702230246032@sandbox.pagseguro.com.br
    /// 
    /// 4: Criar a sessão no método CreateSession.Program.Executar(args)
    /// 
    /// 5: Para cartões de crédito setar a sessão executar no javascript PagSeguroDirectPayment.setSessionId("A_SESSAO_GERADA_DA_DICA_4")
    /// 
    /// 6: Exemplo para criar um token de cartão de crédito nacional: 
    ///        PagSeguroDirectPayment.createCardToken({
    ///            cardNumber: "4111111111111111",
    ///            brand: "visa",
    ///            cvv: "123",
    ///            expirationMonth: "12",
    ///            expirationYear: "2030",
    ///            amount: "4000",              
    ///            maxInstallmentNoInterest: 2,
    ///            success: function(response) {
    ///                //token gerado, esse deve ser usado na chamada da API do Checkout Transparente
    ///                console.log(response);
    ///            },
    ///            error: function(response) {
    ///                //tratamento do erro
    ///                console.log(response);
    ///            },
    ///            complete: function(response) {
    ///                //tratamento comum para todas chamadas
    ///                console.log(response);
    ///            }
    ///        });
    /// 
    /// 7: Exemplo para criar um token de cartão de crédito internacional: 
    ///        PagSeguroDirectPayment.createCardToken({
    ///            cardNumber: "4111111111111111",
    ///            brand: "visa",
    ///            cvv: "123",
    ///            expirationMonth: "12",
    ///            expirationYear: "2030",
    ///            amount: "4000",              
    ///            maxInstallmentNoInterest: 2,
    ///            international: true,
    ///            success: function(response) {
    ///                //token gerado, esse deve ser usado na chamada da API do Checkout Transparente
    ///                console.log(response);
    ///            },
    ///            error: function(response) {
    ///                //tratamento do erro
    ///                console.log(response);
    ///            },
    ///            complete: function(response) {
    ///                //tratamento comum para todas chamadas
    ///                console.log(response);
    ///            }
    ///        });
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            PagSeguroConfiguration.UrlXmlConfiguration = "../../../Uol.PagSeguro/PagSeguroConfig.xml";

            // Pegar sessão aqui e depois comentar
            //CreateSession.Program.Executar(args);

            Direct.CreateTransactionUsingBoleto.Program.Executar(args);
            //Direct.CreateTransactionUsingCreditCard.Program.Executar(args);
            //Direct.CreateTransactionUsingInternationalCreditCard.Program.Executar(args);
            //Direct.CreateTransactionUsingOnlineDebit.Program.Executar(args);
        }
    }
}
