﻿using EZ.EZControl.CMS.Infra.Exceptions;
using EZ.EZControl.CMS.Infra.Extensions;
using EZ.EZControl.CMS.Infra.Models;
using EZ.EZControl.CMS.Infra.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.CMS.Infra.Dtos;

namespace EZ.EZControl.CMS.Infra.ApiClient
{
    public class EZApiClient
    {
        public string TenancyName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public void CookieBasedAuth()
        {
            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, "Account/Login");

            CookieBasedAuth(uri.AbsoluteUri);
        }

        public void TokenBasedAuth()
        {
            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, "api/Account/Authenticate");

            TokenBasedAuth(uri.AbsoluteUri);
        }

        private void CookieBasedAuth(string url)
        {
            var requestBytes = Encoding.UTF8.GetBytes("TenancyName=" + TenancyName + "&UsernameOrEmailAddress=" + UserName + "&Password=" + Password);

            var request = WebRequest.CreateHttp(url);

            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.Accept = "application/json";
            request.CookieContainer = new CookieContainer();
            request.ContentLength = requestBytes.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(requestBytes, 0, requestBytes.Length);
                stream.Flush();

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = Encoding.UTF8.GetString(response.GetResponseStream().GetAllBytes());
                    var ajaxResponse = JsonString2Object<AjaxResponse>(responseString);

                    if (!ajaxResponse.Success)
                    {
                        throw new Exception("Could not login. Reason: " + ajaxResponse.Error.Message + " | " + ajaxResponse.Error.Details);
                    }

                    Cookies.Clear();
                    foreach (Cookie cookie in response.Cookies)
                    {
                        Cookies.Add(cookie);
                    }
                }
            }
        }

        private void TokenBasedAuth(string url)
        {
            var token = AsyncHelper.RunSync(() =>
                PostAsync<string>(
                    url,
                    new
                    {
                        TenancyName = TenancyName,
                        UsernameOrEmailAddress = UserName,
                        Password = Password
                    }));

            RequestHeaders.Add(new NameValue("Authorization", "Bearer " + token));
        }

        public bool Login(LoginDto loginDto, out string token)
        {
            if (string.IsNullOrEmpty(BaseUrl))
            {
                throw new Exception("É necessário definir o endereço do host");
            }

            token = string.Empty;

            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, "api/Account/Authenticate");

            token = AsyncHelper.RunSync(() =>
                PostAsync<string>(
                    uri.AbsoluteUri,
                    new
                    {
                        TenancyName = loginDto.TenancyName,
                        UsernameOrEmailAddress = loginDto.UserName,
                        Password = loginDto.Password
                    })
            );

            return !string.IsNullOrEmpty(token);
        }

        #region AbpWebApiClient

        public static TimeSpan DefaultTimeout { get; set; }

        public string BaseUrl { get; set; }

        public TimeSpan Timeout { get; set; }

        public Collection<Cookie> Cookies { get; private set; }

        public ICollection<NameValue> RequestHeaders { get; set; }

        public ICollection<NameValue> ResponseHeaders { get; set; }

        static EZApiClient()
        {
            DefaultTimeout = TimeSpan.FromSeconds(90);
        }

        public EZApiClient()
        {
            Timeout = DefaultTimeout;
            Cookies = new Collection<Cookie>();
            RequestHeaders = new List<NameValue>();
            ResponseHeaders = new List<NameValue>();
        }

        public virtual async Task PostAsync(string url, int? timeout = null)
        {
            await PostAsync<object>(url, timeout);
        }

        public virtual async Task PostAsync(string url, object input, int? timeout = null)
        {
            await PostAsync<object>(url, input, timeout);
        }

        public virtual async Task<TResult> PostAsync<TResult>(string url, int? timeout = null)
            where TResult : class
        {
            return await PostAsync<TResult>(url, null, timeout);
        }

        public virtual async Task<TResult> PostAsync<TResult>(string url, object input, int? timeout = null)
            where TResult : class
        {
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer })
            {
                using (var client = new HttpClient(handler))
                {
                    client.Timeout = timeout.HasValue ? TimeSpan.FromMilliseconds(timeout.Value) : Timeout;

                    if (!string.IsNullOrEmpty(BaseUrl))
                    {
                        client.BaseAddress = new Uri(BaseUrl);
                    }

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    foreach (var header in RequestHeaders)
                    {
                        client.DefaultRequestHeaders.Add(header.Name, header.Value);
                    }

                    using (var requestContent = new StringContent(Object2JsonString(input), Encoding.UTF8, "application/json"))
                    {
                        foreach (var cookie in Cookies)
                        {
                            if (!string.IsNullOrEmpty(BaseUrl))
                            {
                                cookieContainer.Add(new Uri(BaseUrl), cookie);
                            }
                            else
                            {
                                cookieContainer.Add(cookie);
                            }
                        }

                        using (var response = await client.PostAsync(url, requestContent))
                        {
                            SetResponseHeaders(response);

                            if (!response.IsSuccessStatusCode)
                            {
                                throw new Exception("Could not made request to " + url + "! StatusCode: " + response.StatusCode + ", ReasonPhrase: " + response.ReasonPhrase);
                            }

                            var ajaxResponse = JsonString2Object<AjaxResponse<TResult>>(await response.Content.ReadAsStringAsync());
                            if (!ajaxResponse.Success)
                            {
                                throw new RemoteCallException(ajaxResponse.Error);
                            }

                            return ajaxResponse.Result;
                        }
                    }
                }
            }
        }

        private void SetResponseHeaders(HttpResponseMessage response)
        {
            ResponseHeaders.Clear();
            foreach (var header in response.Headers)
            {
                foreach (var headerValue in header.Value)
                {
                    ResponseHeaders.Add(new NameValue(header.Key, headerValue));
                }
            }
        }

        private static string Object2JsonString(object obj)
        {
            if (obj == null)
            {
                return "";
            }

            return JsonConvert.SerializeObject(obj,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }

        private static TObj JsonString2Object<TObj>(string str)
        {
            return JsonConvert.DeserializeObject<TObj>(str,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }
    }

    #endregion AbpWebApiClient
}