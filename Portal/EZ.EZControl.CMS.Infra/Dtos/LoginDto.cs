﻿namespace EZ.EZControl.CMS.Infra.Dtos
{
    public class LoginDto
    {
        public string TenancyName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}