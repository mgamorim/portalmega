﻿namespace EZ.EZControl.CMS.Infra.Dtos
{
    public class AgenciaListDto
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string DigitoVerificador { get; set; }
        public int BancoId { get; set; }
    }
}