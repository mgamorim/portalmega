﻿namespace EZ.EZControl.CMS.Infra.Dtos
{
    public class ApiSettingsDto
    {
        public string TenancyName { get; set; }
        public string Host { get; set; }
        public string ApiUrl { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public object ResultObject { get; set; }
    }
}