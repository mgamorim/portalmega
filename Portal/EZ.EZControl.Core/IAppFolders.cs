﻿namespace EZ.EZControl
{
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; }

        string SampleProfileImagesFolder { get; }

        string WebLogsFolder { get; set; }

        string TemplateFolder { get; set; }
    }
}