﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto
{
    [AutoMapFrom(typeof(Empresa))]
    public class EmpresaDto : EntityDto
    {
    }
}