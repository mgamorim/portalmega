﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using EZ.EZControl.Authorization.Users;

namespace EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        public IList<EmpresaDto> Empresas { get; set; }
    }
}