﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Roles;
using Abp.AutoMapper;

namespace EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto
{
    [AutoMapFrom(typeof(RolePermissionSetting))]
    public class RolePermissionSettingDto : EntityDto<long>
    {
        public string Name { get; set; }

        public bool IsGranted { get; set; }
    }
}