﻿using Abp.AutoMapper;

namespace EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto
{
    [AutoMapFrom(typeof(Abp.Authorization.Permission))]
    public class PermissionDto
    {
        public string Name { get; set; }
    }
}