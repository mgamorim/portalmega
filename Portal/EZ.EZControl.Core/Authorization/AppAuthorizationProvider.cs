﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace EZ.EZControl.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var somenteLeitura = pages.CreateChildPermission(AppPermissions.Pages_Tenant_AcessoLeitura, L("AcessoLeitura"), multiTenancySides: MultiTenancySides.Tenant);

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);

            pages.CreateChildPermission(AppPermissions.Pages_Administration_ClienteEZ, L("ClienteEZ"), multiTenancySides: MultiTenancySides.Tenant);

            var usuarioPorPessoa = administration.CreateChildPermission(AppPermissions.Pages_Administration_UsuarioPorPessoa, L("UsuariosPorPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            usuarioPorPessoa.CreateChildPermission(AppPermissions.Pages_Administration_UsuarioPorPessoa_Create, L("CreatingNewUsuarioPorPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            usuarioPorPessoa.CreateChildPermission(AppPermissions.Pages_Administration_UsuarioPorPessoa_Edit, L("EditingUsuarioPorPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            usuarioPorPessoa.CreateChildPermission(AppPermissions.Pages_Administration_UsuarioPorPessoa_Delete, L("DeletingUsuarioPorPessoa"), multiTenancySides: MultiTenancySides.Tenant);

            #region Módulo Global

            var global = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Global, L("Global"));

            var perfil = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Perfil, L("Perfil"), multiTenancySides: MultiTenancySides.Tenant);

            var conexaoSmtp = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ConexaoSMTP, L("ConexoesSMTP"), multiTenancySides: MultiTenancySides.Tenant);
            conexaoSmtp.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Create, L("CreatingNewConexaoSMTP"), multiTenancySides: MultiTenancySides.Tenant);
            conexaoSmtp.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Edit, L("EditingConexaoSMTP"), multiTenancySides: MultiTenancySides.Tenant);
            conexaoSmtp.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Delete, L("DeletingConexaoSMTP"), multiTenancySides: MultiTenancySides.Tenant);

            var templateEmail = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TemplateEmail, L("TemplatesDeEmail"), multiTenancySides: MultiTenancySides.Tenant);
            templateEmail.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TemplateEmail_Create, L("CreatingNewTemplateDeEmail"), multiTenancySides: MultiTenancySides.Tenant);
            templateEmail.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TemplateEmail_Edit, L("EditingTemplateDeEmail"), multiTenancySides: MultiTenancySides.Tenant);
            templateEmail.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TemplateEmail_Delete, L("DeletingTemplateDeEmail"), multiTenancySides: MultiTenancySides.Tenant);

            var sendEmail = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_SendEmail, L("SendEmail"), multiTenancySides: MultiTenancySides.Tenant);

            var dadosEmpresa = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_DadosEmpresa, L("DadosEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            dadosEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_DadosEmpresa_Edit, L("EditingDadosEmpresa"), multiTenancySides: MultiTenancySides.Tenant);

            // Empresa
            var empresa = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Empresa, L("Empresas"), multiTenancySides: MultiTenancySides.Tenant);
            empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Empresa_Create, L("CreatingNewEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Empresa_Edit, L("EditingEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Empresa_Delete, L("DeletingEmpresa"), multiTenancySides: MultiTenancySides.Tenant);

            // documento da empresa
            var empresaDocumento = empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaDocumento, L("Documentos"), multiTenancySides: MultiTenancySides.Tenant);
            empresaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaDocumento_Create, L("CreatingNewDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            empresaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaDocumento_Edit, L("EditingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            empresaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaDocumento_Delete, L("DeletingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco eletronico da empresa
            var empresaEnderecoEletronico = empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico, L("EnderecosEletronicos"), multiTenancySides: MultiTenancySides.Tenant);
            empresaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Create, L("CreatingNewEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            empresaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Edit, L("EditingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            empresaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Delete, L("DeletingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco da empresa
            var empresaEndereco = empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEndereco, L("Enderecos"), multiTenancySides: MultiTenancySides.Tenant);
            empresaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEndereco_Create, L("CreatingNewEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            empresaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEndereco_Edit, L("EditingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            empresaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaEndereco_Delete, L("DeletingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            // contato da empresa
            var empresaContato = empresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaContato, L("Contatos"), multiTenancySides: MultiTenancySides.Tenant);
            empresaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaContato_Create, L("CreatingNewContato"), multiTenancySides: MultiTenancySides.Tenant);
            empresaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaContato_Edit, L("EditingContato"), multiTenancySides: MultiTenancySides.Tenant);
            empresaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_EmpresaContato_Delete, L("DeletingContato"), multiTenancySides: MultiTenancySides.Tenant);

            var arquivo = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Arquivo, L("Arquivos"), multiTenancySides: MultiTenancySides.Tenant);
            arquivo.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Arquivo_Create, L("CreatingNewArquivo"), multiTenancySides: MultiTenancySides.Tenant);
            arquivo.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Arquivo_Edit, L("EditingArquivo"), multiTenancySides: MultiTenancySides.Tenant);
            arquivo.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Arquivo_Delete, L("DeletingArquivo"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaFisica = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisica, L("PessoasFisicas"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisica_Create, L("CreatingNewPessoaFisica"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisica_Edit, L("EditingPessoaFisica"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisica_Delete, L("DeletingPessoaFisica"), multiTenancySides: MultiTenancySides.Tenant);

            //Empresa
            var pessoaFisicaEmpresa = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa, L("Empresas"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Create, L("CreatingNewEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Edit, L("EditingEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Delete, L("DeletingEmpresa"), multiTenancySides: MultiTenancySides.Tenant);

            //Telefone
            var pessoaFisicaTelefone = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone, L("Telefones"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTelefone.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Create, L("CreatingNewTelefone"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTelefone.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Edit, L("EditingTelefone"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTelefone.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Delete, L("DeletingTelefone"), multiTenancySides: MultiTenancySides.Tenant);

            //GrupoPessoa
            var pessoaFisicaGrupoPessoa = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa, L("GruposPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaGrupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Create, L("CreatingNewGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaGrupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Edit, L("EditingGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaGrupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Delete, L("DeletingGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);

            // documento
            var pessoaFisicaDocumento = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento, L("Documentos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Create, L("CreatingNewDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Edit, L("EditingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Delete, L("DeletingDocumento"), multiTenancySides: MultiTenancySides.Tenant);

            // endereco eletronico da empresa
            var pessoaFisicaEnderecoEletronico = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico, L("EnderecosEletronicos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Create, L("CreatingNewEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Edit, L("EditingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Delete, L("DeletingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco da empresa
            var pessoaFisicaEndereco = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco, L("Enderecos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Create, L("CreatingNewEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Edit, L("EditingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Delete, L("DeletingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            // contato da empresa
            var pessoaFisicaContato = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato, L("Contatos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Create, L("CreatingNewContato"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Edit, L("EditingContato"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Delete, L("DeletingContato"), multiTenancySides: MultiTenancySides.Tenant);

            //Tipo de Documento
            var pessoaFisicaTipoDeDocumento = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento, L("TiposDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Create, L("CreatingNewTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Edit, L("EditingTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Delete, L("DeletingTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            //Pais
            var pessoaFisicaPais = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais, L("Paises"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaPais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Create, L("CreatingNewPais"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaPais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Edit, L("EditingPais"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaPais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Delete, L("DeletingPais"), multiTenancySides: MultiTenancySides.Tenant);
            //Estado
            var pessoaFisicaEstado = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado, L("Estados"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEstado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Create, L("CreatingNewEstado"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEstado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Edit, L("EditingEstado"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaEstado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Delete, L("DeletingEstado"), multiTenancySides: MultiTenancySides.Tenant);
            //Cidade
            var pessoaFisicaCidade = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade, L("Cidade"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaCidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Create, L("CreatingNewCidade"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaCidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Edit, L("EditingCidade"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaCidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Delete, L("DeletingCidade"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaFisicaTipoDeLogradouro = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro, L("TipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Create, L("CreatingNewTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Edit, L("EditingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Delete, L("DeletingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaFisicaTipoDeContato = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato, L("TipoDeContato"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Create, L("CreatingNewTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Edit, L("EditingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Delete, L("DeletingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaFisicaTratamento = pessoaFisica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTratamento, L("Tratamentos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTratamento_Create, L("CreatingNewTratamento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTratamento_Edit, L("EditingTratamento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaFisicaTratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaFisicaTratamento_Delete, L("DeletingTratamento"), multiTenancySides: MultiTenancySides.Tenant);

            //Pessoa Juridica
            var pessoaJuridica = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridica, L("PessoasJuridicas"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Create, L("CreatingNewPessoaJuridica"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Edit, L("EditingPessoaJuridica"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Delete, L("DeletingPessoaJuridica"), multiTenancySides: MultiTenancySides.Tenant);

            //
            //Empresa
            var pessoaJuridicaEmpresa = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, L("Empresas"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Create, L("CreatingNewEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Edit, L("EditingEmpresa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEmpresa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Delete, L("DeletingEmpresa"), multiTenancySides: MultiTenancySides.Tenant);

            //Telefone
            var pessoaJuridicaTelefone = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone, L("Telefones"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTelefone.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Create, L("CreatingNewTelefone"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTelefone.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Edit, L("EditingTelefone"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTelefone.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Delete, L("DeletingTelefone"), multiTenancySides: MultiTenancySides.Tenant);

            //GrupoPessoa
            var pessoaJuridicaGrupoPessoa = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa, L("GruposPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaGrupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Create, L("CreatingNewGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaGrupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Edit, L("EditingGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaGrupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Delete, L("DeletingGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);

            // documento
            var pessoaJuridicaDocumento = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento, L("Documentos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Create, L("CreatingNewDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Edit, L("EditingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Delete, L("DeletingDocumento"), multiTenancySides: MultiTenancySides.Tenant);

            // endereco eletronico da empresa
            var pessoaJuridicaEnderecoEletronico = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico, L("EnderecosEletronicos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Create, L("CreatingNewEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Edit, L("EditingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Delete, L("DeletingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco da empresa
            var pessoaJuridicaEndereco = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco, L("Enderecos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Create, L("CreatingNewEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Edit, L("EditingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Delete, L("DeletingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            // contato da empresa
            var pessoaJuridicaContato = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato, L("Contatos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Create, L("CreatingNewContato"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Edit, L("EditingContato"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Delete, L("DeletingContato"), multiTenancySides: MultiTenancySides.Tenant);

            //Tipo de Documento
            var pessoaJuridicaTipoDeDocumento = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento, L("TiposDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Create, L("CreatingNewTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Edit, L("EditingTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Delete, L("DeletingTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            //Pais
            var pessoaJuridicaPais = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais, L("Paises"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaPais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Create, L("CreatingNewPais"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaPais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Edit, L("EditingPais"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaPais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Delete, L("DeletingPais"), multiTenancySides: MultiTenancySides.Tenant);
            //Estado
            var pessoaJuridicaEstado = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado, L("Estados"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEstado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Create, L("CreatingNewEstado"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEstado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Edit, L("EditingEstado"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaEstado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Delete, L("DeletingEstado"), multiTenancySides: MultiTenancySides.Tenant);
            //Cidade
            var pessoaJuridicaCidade = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade, L("Cidade"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaCidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Create, L("CreatingNewCidade"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaCidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Edit, L("EditingCidade"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaCidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Delete, L("DeletingCidade"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaJuridicaTipoDeLogradouro = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro, L("TipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Create, L("CreatingNewTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Edit, L("EditingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Delete, L("DeletingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaJuridicaTipoDeContato = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato, L("TipoDeContato"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Create, L("CreatingNewTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Edit, L("EditingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Delete, L("DeletingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);

            var pessoaJuridicaTratamento = pessoaJuridica.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTratamento, L("Tratamentos"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTratamento_Create, L("CreatingNewTratamento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTratamento_Edit, L("EditingTratamento"), multiTenancySides: MultiTenancySides.Tenant);
            pessoaJuridicaTratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTratamento_Delete, L("DeletingTratamento"), multiTenancySides: MultiTenancySides.Tenant);


            //Contato
            var contato = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Contato, L("Contatos"), multiTenancySides: MultiTenancySides.Tenant);
            contato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Contato_Create, L("CreatingNewContato"), multiTenancySides: MultiTenancySides.Tenant);
            contato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Contato_Edit, L("EditingContato"), multiTenancySides: MultiTenancySides.Tenant);
            contato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Contato_Delete, L("DeletingContato"), multiTenancySides: MultiTenancySides.Tenant);

            var endereco = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Endereco, L("Enderecos"), multiTenancySides: MultiTenancySides.Tenant);
            endereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Endereco_Create, L("CreatingNewEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            endereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Endereco_Edit, L("EditingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            endereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Endereco_Delete, L("DeletingEndereco"), multiTenancySides: MultiTenancySides.Tenant);

            var departamento = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Departamento, L("Departamentos"), multiTenancySides: MultiTenancySides.Tenant);
            departamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Departamento_Create, L("CreatingNewDepartamento"), multiTenancySides: MultiTenancySides.Tenant);
            departamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Departamento_Edit, L("EditingDepartamento"), multiTenancySides: MultiTenancySides.Tenant);
            departamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Departamento_Delete, L("DeletingDepartamento"), multiTenancySides: MultiTenancySides.Tenant);

            var itemHierarquia = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ItemHierarquia, L("ItensHierarquia"), multiTenancySides: MultiTenancySides.Tenant);
            itemHierarquia.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Create, L("CreatingNewItemHierarquia"), multiTenancySides: MultiTenancySides.Tenant);
            itemHierarquia.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Edit, L("EditingItemHierarquia"), multiTenancySides: MultiTenancySides.Tenant);
            itemHierarquia.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Delete, L("DeletingItemHierarquia"), multiTenancySides: MultiTenancySides.Tenant);

            var cliente = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cliente, L("Clientes"), multiTenancySides: MultiTenancySides.Tenant);
            cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cliente_Create, L("CreatingNewCliente"), multiTenancySides: MultiTenancySides.Tenant);
            cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cliente_Edit, L("EditingCliente"), multiTenancySides: MultiTenancySides.Tenant);
            cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cliente_Delete, L("DeletingCliente"), multiTenancySides: MultiTenancySides.Tenant);

            // documento do cliente
            var clienteDocumento = cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteDocumento, L("Documentos"), multiTenancySides: MultiTenancySides.Tenant);
            clienteDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteDocumento_Create, L("CreatingNewDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            clienteDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteDocumento_Edit, L("EditingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            clienteDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteDocumento_Delete, L("DeletingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco eletronico do cliente
            var clienteEnderecoEletronico = cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico, L("EnderecosEletronicos"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Create, L("CreatingNewEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Edit, L("EditingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Delete, L("DeletingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco do cliente
            var clienteEndereco = cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEndereco, L("Enderecos"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEndereco_Create, L("CreatingNewEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEndereco_Edit, L("EditingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteEndereco_Delete, L("DeletingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            // contato do cliente
            var clienteContato = cliente.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteContato, L("Contatos"), multiTenancySides: MultiTenancySides.Tenant);
            clienteContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteContato_Create, L("CreatingNewContato"), multiTenancySides: MultiTenancySides.Tenant);
            clienteContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteContato_Edit, L("EditingContato"), multiTenancySides: MultiTenancySides.Tenant);
            clienteContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_ClienteContato_Delete, L("DeletingContato"), multiTenancySides: MultiTenancySides.Tenant);

            var banco = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Banco, L("Bancos"), multiTenancySides: MultiTenancySides.Tenant);
            banco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Banco_Create, L("CreatingNewBanco"), multiTenancySides: MultiTenancySides.Tenant);
            banco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Banco_Edit, L("EditingBanco"), multiTenancySides: MultiTenancySides.Tenant);
            banco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Banco_Delete, L("DeletingBanco"), multiTenancySides: MultiTenancySides.Tenant);

            var agencia = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Agencia, L("Agencias"), multiTenancySides: MultiTenancySides.Tenant);
            agencia.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Agencia_Create, L("CreatingNewAgencia"), multiTenancySides: MultiTenancySides.Tenant);
            agencia.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Agencia_Edit, L("EditingAgencia"), multiTenancySides: MultiTenancySides.Tenant);
            agencia.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Agencia_Delete, L("DeletingAgencia"), multiTenancySides: MultiTenancySides.Tenant);

            var campoPersonalizado = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_CampoPersonalizado, L("CamposPersonalizados"), multiTenancySides: MultiTenancySides.Tenant);
            campoPersonalizado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_CampoPersonalizado_Create, L("CreatingNewCampoPersonalizado"), multiTenancySides: MultiTenancySides.Tenant);
            campoPersonalizado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_CampoPersonalizado_Edit, L("EditingCampoPersonalizado"), multiTenancySides: MultiTenancySides.Tenant);
            campoPersonalizado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_CampoPersonalizado_Delete, L("DeletingCampoPersonalizado"), multiTenancySides: MultiTenancySides.Tenant);

            var fornecedor = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Fornecedor, L("Fornecedores"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Fornecedor_Create, L("CreatingNewFornecedor"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Fornecedor_Edit, L("EditingFornecedor"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Fornecedor_Delete, L("DeletingFornecedor"), multiTenancySides: MultiTenancySides.Tenant);
            // documento do fornecedor
            var fornecedorDocumento = fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorDocumento, L("Documentos"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorDocumento_Create, L("CreatingNewDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorDocumento_Edit, L("EditingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorDocumento_Delete, L("DeletingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco eletronico do fornecedor
            var fornecedorEnderecoEletronico = fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico, L("EnderecosEletronicos"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Create, L("CreatingNewEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Edit, L("EditingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorEnderecoEletronico.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Delete, L("DeletingEnderecoEletronico"), multiTenancySides: MultiTenancySides.Tenant);
            // endereco do fornecedor
            var fornecedorEndereco = fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEndereco, L("Enderecos"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEndereco_Create, L("CreatingNewEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEndereco_Edit, L("EditingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorEndereco.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorEndereco_Delete, L("DeletingEndereco"), multiTenancySides: MultiTenancySides.Tenant);
            // contato do fornecedor
            var fornecedorContato = fornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorContato, L("Contatos"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorContato_Create, L("CreatingNewContato"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorContato_Edit, L("EditingContato"), multiTenancySides: MultiTenancySides.Tenant);
            fornecedorContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_FornecedorContato_Delete, L("DeletingContato"), multiTenancySides: MultiTenancySides.Tenant);

            var ramoDoFornecedor = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor, L("RamosDoFornecedor"), multiTenancySides: MultiTenancySides.Tenant);
            ramoDoFornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Create, L("CreatingNewRamoDoFornecedor"), multiTenancySides: MultiTenancySides.Tenant);
            ramoDoFornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Edit, L("EditingRamoDoFornecedor"), multiTenancySides: MultiTenancySides.Tenant);
            ramoDoFornecedor.CreateChildPermission(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Delete, L("DeletingRamoDoFornecedor"), multiTenancySides: MultiTenancySides.Tenant);

            var feriado = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Feriado, L("Feriados"), multiTenancySides: MultiTenancySides.Tenant);
            feriado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Feriado_Create, L("CreatingNewFeriado"), multiTenancySides: MultiTenancySides.Tenant);
            feriado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Feriado_Edit, L("EditingFeriado"), multiTenancySides: MultiTenancySides.Tenant);
            feriado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Feriado_Delete, L("DeletingFeriado"), multiTenancySides: MultiTenancySides.Tenant);

            var tipoDeDocumento = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeDocumento, L("TiposDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Create, L("CreatingNewTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Edit, L("EditingTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeDocumento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Delete, L("DeletingTipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);

            var tipoDeLogradouro = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro, L("TipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Create, L("CreatingNewTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Edit, L("EditingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeLogradouro.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Delete, L("DeletingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);

            var tipoDeContato = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeContato, L("TipoDeContato"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeContato_Create, L("CreatingNewTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeContato_Edit, L("EditingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeContato.CreateChildPermission(AppPermissions.Pages_Tenant_Global_TipoDeContato_Delete, L("DeletingTipoDeLogradouro"), multiTenancySides: MultiTenancySides.Tenant);

            var profissao = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Profissao, L("Profissoes"), multiTenancySides: MultiTenancySides.Tenant);
            profissao.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Profissao_Create, L("CreatingNewProfissao"), multiTenancySides: MultiTenancySides.Tenant);
            profissao.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Profissao_Edit, L("EditingProfissao"), multiTenancySides: MultiTenancySides.Tenant);
            profissao.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Profissao_Delete, L("DeletingProfissao"), multiTenancySides: MultiTenancySides.Tenant);

            var pais = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Pais, L("Paises"), multiTenancySides: MultiTenancySides.Tenant);
            pais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Pais_Create, L("CreatingNewPais"), multiTenancySides: MultiTenancySides.Tenant);
            pais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Pais_Edit, L("EditingPais"), multiTenancySides: MultiTenancySides.Tenant);
            pais.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Pais_Delete, L("DeletingPais"), multiTenancySides: MultiTenancySides.Tenant);

            var estado = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Estado, L("Estados"), multiTenancySides: MultiTenancySides.Tenant);
            estado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Estado_Create, L("CreatingNewEstado"), multiTenancySides: MultiTenancySides.Tenant);
            estado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Estado_Edit, L("EditingEstado"), multiTenancySides: MultiTenancySides.Tenant);
            estado.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Estado_Delete, L("DeletingEstado"), multiTenancySides: MultiTenancySides.Tenant);

            var cidade = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cidade, L("Cidade"), multiTenancySides: MultiTenancySides.Tenant);
            cidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cidade_Create, L("CreatingNewCidade"), multiTenancySides: MultiTenancySides.Tenant);
            cidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cidade_Edit, L("EditingCidade"), multiTenancySides: MultiTenancySides.Tenant);
            cidade.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Cidade_Delete, L("DeletingCidade"), multiTenancySides: MultiTenancySides.Tenant);

            var tratamento = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Tratamento, L("Tratamentos"), multiTenancySides: MultiTenancySides.Tenant);
            tratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Tratamento_Create, L("CreatingNewTratamento"), multiTenancySides: MultiTenancySides.Tenant);
            tratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Tratamento_Edit, L("EditingTratamento"), multiTenancySides: MultiTenancySides.Tenant);
            tratamento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Tratamento_Delete, L("DeletingTratamento"), multiTenancySides: MultiTenancySides.Tenant);

            var grupoPessoa = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_GrupoPessoa, L("GruposPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            grupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Create, L("CreatingNewGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            grupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Edit, L("EditingGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);
            grupoPessoa.CreateChildPermission(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Delete, L("DeletingGrupoPessoa"), multiTenancySides: MultiTenancySides.Tenant);

            var documento = global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Documento, L("Documentos"), multiTenancySides: MultiTenancySides.Tenant);
            documento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Documento_Create, L("CreatingNewDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            documento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Documento_Edit, L("EditingDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            documento.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Documento_Delete, L("DeletingDocumento"), multiTenancySides: MultiTenancySides.Tenant);



            global.CreateChildPermission(AppPermissions.Pages_Tenant_Global_Parametros, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Módulo CMS

            var cms = pages.CreateChildPermission(AppPermissions.Pages_Tenant_CMS, L("CMS"));
            cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Parametros, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);

            var site = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Site, L("Sites"), multiTenancySides: MultiTenancySides.Tenant);
            site.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Site_Create, L("CreatingNewSite"), multiTenancySides: MultiTenancySides.Tenant);
            site.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Site_Edit, L("EditingSite"), multiTenancySides: MultiTenancySides.Tenant);
            site.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Site_Delete, L("DeletingSite"), multiTenancySides: MultiTenancySides.Tenant);

            var categoria = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Categoria, L("Categorias"), multiTenancySides: MultiTenancySides.Tenant);
            categoria.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Categoria_Create, L("CreatingNewCategoria"), multiTenancySides: MultiTenancySides.Tenant);
            categoria.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Categoria_Edit, L("EditingCategoria"), multiTenancySides: MultiTenancySides.Tenant);
            categoria.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Categoria_Delete, L("DeletingCategoria"), multiTenancySides: MultiTenancySides.Tenant);

            var post = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Post, L("Posts"), multiTenancySides: MultiTenancySides.Tenant);
            post.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Post_Create, L("CreatingNewPost"), multiTenancySides: MultiTenancySides.Tenant);
            post.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Post_Edit, L("EditingPost"), multiTenancySides: MultiTenancySides.Tenant);
            post.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Post_Delete, L("DeletingPost"), multiTenancySides: MultiTenancySides.Tenant);

            var pagina = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Pagina, L("Paginas"), multiTenancySides: MultiTenancySides.Tenant);
            pagina.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Pagina_Create, L("CreatingNewPagina"), multiTenancySides: MultiTenancySides.Tenant);
            pagina.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Pagina_Edit, L("EditingPagina"), multiTenancySides: MultiTenancySides.Tenant);
            pagina.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Pagina_Delete, L("DeletingPagina"), multiTenancySides: MultiTenancySides.Tenant);

            var tag = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Tag, L("Tags"), multiTenancySides: MultiTenancySides.Tenant);
            tag.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Tag_Edit, L("EditingTag"), multiTenancySides: MultiTenancySides.Tenant);
            tag.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Tag_Delete, L("DeletingTag"), multiTenancySides: MultiTenancySides.Tenant);

            var arquivoCMS = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Arquivo, L("Arquivos"), multiTenancySides: MultiTenancySides.Tenant);
            arquivoCMS.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Arquivo_Create, L("CreatingNewArquivo"), multiTenancySides: MultiTenancySides.Tenant);
            arquivoCMS.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Arquivo_Edit, L("EditingArquivo"), multiTenancySides: MultiTenancySides.Tenant);
            arquivoCMS.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Arquivo_Delete, L("DeletingArquivo"), multiTenancySides: MultiTenancySides.Tenant);

            var menu = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Menu, L("Menus"), multiTenancySides: MultiTenancySides.Tenant);
            menu.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Menu_Create, L("CreatingNewMenu"), multiTenancySides: MultiTenancySides.Tenant);
            menu.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Menu_Edit, L("EditingMenu"), multiTenancySides: MultiTenancySides.Tenant);
            menu.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Menu_Delete, L("DeletingMenu"), multiTenancySides: MultiTenancySides.Tenant);

            var itemDeMenu = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_ItemDeMenu, L("ItensDeMenu"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeMenu.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Create, L("CreatingNewItemDeMenu"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeMenu.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Edit, L("EditingItemDeMenu"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeMenu.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Delete, L("DeletingItemDeMenu"), multiTenancySides: MultiTenancySides.Tenant);

            var template = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Template, L("Templates"), multiTenancySides: MultiTenancySides.Tenant);
            template.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Template_Synchronize, L("SynchronizeAll"), multiTenancySides: MultiTenancySides.Tenant);

            var widget = cms.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Widget, L("Widgets"), multiTenancySides: MultiTenancySides.Tenant);
            widget.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Widget_Create, L("CreatingNewWidget"), multiTenancySides: MultiTenancySides.Tenant);
            widget.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Widget_Edit, L("EditingWidget"), multiTenancySides: MultiTenancySides.Tenant);
            widget.CreateChildPermission(AppPermissions.Pages_Tenant_CMS_Widget_Delete, L("DeletingWidget"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Módulo CMSFrontEnd
            var cmsFrontEnd = pages.CreateChildPermission(AppPermissions.Pages_Tenant_CMSFrontEnd, L("CMSFrontEnd"));
            cmsFrontEnd.CreateChildPermission(AppPermissions.Pages_Tenant_CMSFrontEnd_Menu, L("Menu"), multiTenancySides: MultiTenancySides.Tenant);
            cmsFrontEnd.CreateChildPermission(AppPermissions.Pages_Tenant_CMSFrontEnd_Widget, L("Widget"), multiTenancySides: MultiTenancySides.Tenant);
            cmsFrontEnd.CreateChildPermission(AppPermissions.Pages_Tenant_CMSFrontEnd_Template, L("Template"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Módulo Agenda
            var agenda = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda, L("Agenda"));
            var parametroAgenda = agenda.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Parametros, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);
            parametroAgenda.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Parametros_Padroes, L("Agenda.Parametro.Geral"), multiTenancySides: MultiTenancySides.Tenant);

            var regra = parametroAgenda.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras, L("Agenda.Parametro.Regra"), multiTenancySides: MultiTenancySides.Tenant);
            regra.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, L("CreatingNewRegraAgenda"), multiTenancySides: MultiTenancySides.Tenant);
            regra.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit, L("EditingRegraAgenda"), multiTenancySides: MultiTenancySides.Tenant);
            regra.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Delete, L("DeletingRegraAgenda"), multiTenancySides: MultiTenancySides.Tenant);

            var disponibilidade = agenda.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Disponibilidade, L("Disponibilidades"), multiTenancySides: MultiTenancySides.Tenant);
            disponibilidade.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create, L("CreatingNewDisponibilidade"), multiTenancySides: MultiTenancySides.Tenant);
            disponibilidade.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit, L("EditingDisponibilidade"), multiTenancySides: MultiTenancySides.Tenant);
            disponibilidade.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Delete, L("DeletingDisponibilidade"), multiTenancySides: MultiTenancySides.Tenant);

            var bloqueio = agenda.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Bloqueio, L("Bloqueios"), multiTenancySides: MultiTenancySides.Tenant);
            bloqueio.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create, L("CreatingNewBloqueio"), multiTenancySides: MultiTenancySides.Tenant);
            bloqueio.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit, L("EditingBloqueio"), multiTenancySides: MultiTenancySides.Tenant);
            bloqueio.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Delete, L("DeletingBloqueio"), multiTenancySides: MultiTenancySides.Tenant);

            var evento = agenda.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Evento, L("Eventos"), multiTenancySides: MultiTenancySides.Tenant);
            evento.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Evento_Create, L("CreatingNewEvento"), multiTenancySides: MultiTenancySides.Tenant);
            evento.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Evento_Edit, L("EditingEvento"), multiTenancySides: MultiTenancySides.Tenant);
            evento.CreateChildPermission(AppPermissions.Pages_Tenant_Agenda_Evento_Delete, L("DeletingEvento"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Módulo EZMedical
            var ezMedical = pages.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical, L("EZMedical"));

            var parametroEZMedical = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Parametro, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);
            parametroEZMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Parametro_Edit, L("EditingParametroEZMedical"), multiTenancySides: MultiTenancySides.Tenant);

            var eventoConsulta = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Evento, L("Agenda"), multiTenancySides: MultiTenancySides.Tenant);
            eventoConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Evento_Create, L("CreatingEventoEZMedical"), multiTenancySides: MultiTenancySides.Tenant);
            eventoConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Evento_Edit, L("EditingEventoEZMedical"), multiTenancySides: MultiTenancySides.Tenant);
            eventoConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Evento_Delete, L("DeletingEventoEZMedical"), multiTenancySides: MultiTenancySides.Tenant);

            var especialidade = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Especialidade, L("Especialidade"), multiTenancySides: MultiTenancySides.Tenant);
            especialidade.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Create, L("CreatingEspecialidade"), multiTenancySides: MultiTenancySides.Tenant);
            especialidade.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Edit, L("EditingEspecialidade"), multiTenancySides: MultiTenancySides.Tenant);
            especialidade.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Delete, L("DeletingEspecialidade"), multiTenancySides: MultiTenancySides.Tenant);

            var medico = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Medico, L("Medico"), multiTenancySides: MultiTenancySides.Tenant);
            medico.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Medico_Create, L("CreatingMedico"), multiTenancySides: MultiTenancySides.Tenant);
            medico.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Medico_Edit, L("EditingMedico"), multiTenancySides: MultiTenancySides.Tenant);
            medico.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Medico_Delete, L("DeletingMedico"), multiTenancySides: MultiTenancySides.Tenant);

            var disponibilidadeConsulta = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade, L("Disponibilidade"), multiTenancySides: MultiTenancySides.Tenant);
            disponibilidadeConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Create, L("CreatingDisponibilidade"), multiTenancySides: MultiTenancySides.Tenant);
            disponibilidadeConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Edit, L("EditingDisponibilidade"), multiTenancySides: MultiTenancySides.Tenant);
            disponibilidadeConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Delete, L("DeletingDisponibilidade"), multiTenancySides: MultiTenancySides.Tenant);

            var bloqueioConsulta = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Bloqueio, L("Bloqueio"), multiTenancySides: MultiTenancySides.Tenant);
            bloqueioConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Create, L("CreatingBloqueio"), multiTenancySides: MultiTenancySides.Tenant);
            bloqueioConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Edit, L("EditingBloqueio"), multiTenancySides: MultiTenancySides.Tenant);
            bloqueioConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Delete, L("DeletingBloqueio"), multiTenancySides: MultiTenancySides.Tenant);

            var regraConsulta = ezMedical.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta, L("Regra"), multiTenancySides: MultiTenancySides.Tenant);
            regraConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Create, L("CreatingRegra"), multiTenancySides: MultiTenancySides.Tenant);
            regraConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Edit, L("EditingRegra"), multiTenancySides: MultiTenancySides.Tenant);
            regraConsulta.CreateChildPermission(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Delete, L("DeletingRegra"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Módulo Estoque
            var estoque = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque, L("Estoque"));

            var estoqueProduto = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Produto, L("Produto"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueProduto.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Produto_Create, L("CreatingNewProduto"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueProduto.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Produto_Edit, L("EditingProduto"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueProduto.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Produto_Delete, L("DeletingProduto"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueEntrada = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Entrada, L("Estoque.Entrada"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueEntrada.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Entrada_Create, L("CreatingNewEntrada"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueEntrada.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Entrada_Edit, L("EditingEntrada"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueEntrada.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Entrada_Delete, L("DeletingEntrada"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueSaida = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Saida, L("Estoque.Saida"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueSaida.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Saida_Create, L("CreatingNewSaida"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueSaida.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Saida_Edit, L("EditingSaida"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueSaida.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Saida_Delete, L("DeletingSaida"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueParametro = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametro.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Padrao, L("Estoque.Parametro.Padrao"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueParametroNatureza = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza, L("Estoque.Parametro.Natureza"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroNatureza.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Create, L("CreatingNewNatureza"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroNatureza.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Edit, L("EditingNatureza"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroNatureza.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Delete, L("DeletingNatureza"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueParametroFracao = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao, L("Estoque.Parametro.Fracao"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroFracao.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Create, L("CreatingNewFracao"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroFracao.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Edit, L("EditingFracao"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroFracao.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Delete, L("DeletingFracao"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueParametroUnidade = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida, L("Estoque.Parametro.UnidadeMedida"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroUnidade.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Create, L("CreatingNewUnidadeMedida"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroUnidade.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Edit, L("EditingUnidadeMedida"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroUnidade.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Delete, L("DeletingUnidadeMedida"), multiTenancySides: MultiTenancySides.Tenant);

            var estoqueParametroLocalArmazenamento = estoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento, L("Estoque.Parametro.LocalArmazenamento"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroLocalArmazenamento.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Create, L("CreatingNewLocalArmazenamento"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroLocalArmazenamento.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Edit, L("EditingLocalArmazenamento"), multiTenancySides: MultiTenancySides.Tenant);
            estoqueParametroLocalArmazenamento.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Delete, L("DeletingLocalArmazenamento"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Módulo Vendas 
            var vendas = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas, L("Vendas"));
            var pedido = vendas.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas_Pedido, L("Pedidos"));
            pedido.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas_Pedido_Create, L("CreatingNewPedido"));
            pedido.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas_Pedido_Edit, L("EditingPedido"));
            pedido.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas_Pedido_Cancel, L("CancelingPedido"));
            //var parametroVendas = Vendas.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas_Parametro, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);
            //parametroEstoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Edit, L("EditingParametroVendas"), multiTenancySides: MultiTenancySides.Tenant);

            //Vendas.CreateChildPermission(AppPermissions.Pages_Tenant_Vendas_Evento, L("Comissionamento"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Módulo Financeiro 
            var financeiro = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro, L("Financeiro"));

            var tipoDeDocumentoFinanceiro = financeiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento, L("TipoDeDocumento"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeDocumentoFinanceiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento_Create, L("CreatingTipoDeDocumentoFinanceiro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeDocumentoFinanceiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento_Edit, L("EditingTipoDeDocumentoFinanceiro"), multiTenancySides: MultiTenancySides.Tenant);
            tipoDeDocumentoFinanceiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento_Delete, L("DeletingTipoDeDocumentoFinanceiro"), multiTenancySides: MultiTenancySides.Tenant);

            var documentoFinanceiro = financeiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_Documento, L("Documento"), multiTenancySides: MultiTenancySides.Tenant);

            var documentoFinanceiroAPagar = documentoFinanceiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAPagar, L("DocumentoAPagar"), multiTenancySides: MultiTenancySides.Tenant);
            documentoFinanceiroAPagar.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAPagar_Create, L("CreatingDocumentoFinanceiroAPagar"), multiTenancySides: MultiTenancySides.Tenant);
            documentoFinanceiroAPagar.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAPagar_Edit, L("EditingDocumentoFinanceiroAPagar"), multiTenancySides: MultiTenancySides.Tenant);
            documentoFinanceiroAPagar.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAPagar_Estornar, L("EstornarDocumentoFinanceiroAPagar"), multiTenancySides: MultiTenancySides.Tenant);

            var documentoFinanceiroAReceber = documentoFinanceiro.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAReceber, L("DocumentoAReceber"), multiTenancySides: MultiTenancySides.Tenant);
            documentoFinanceiroAReceber.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAReceber_Create, L("CreatingDocumentoFinanceiroAReceber"), multiTenancySides: MultiTenancySides.Tenant);
            documentoFinanceiroAReceber.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAReceber_Edit, L("EditingDocumentoFinanceiroAReceber"), multiTenancySides: MultiTenancySides.Tenant);
            documentoFinanceiroAReceber.CreateChildPermission(AppPermissions.Pages_Tenant_Financeiro_DocumentoAReceber_Estornar, L("EstornarDocumentoFinanceiroAReceber"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Módulo CRM
            var crm = pages.CreateChildPermission(AppPermissions.Pages_Tenant_CRM, L("CRM"));
            #endregion

            #region Módulo EZLiv 
            var ezLiv = pages.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv, L("EZLiv"));

            var beneficiario = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Beneficiario, L("Beneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            beneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Create, L("CreatingNewBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            beneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Edit, L("EditingBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            beneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Delete, L("DeletingBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);

            var proponenteTitular = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular, L("ProponenteTitular"), multiTenancySides: MultiTenancySides.Tenant);
            proponenteTitular.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Create, L("CreatingNewProponenteTitular"), multiTenancySides: MultiTenancySides.Tenant);
            proponenteTitular.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Edit, L("EditingProponenteTitular"), multiTenancySides: MultiTenancySides.Tenant);
            proponenteTitular.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Delete, L("DeletingProponenteTitular"), multiTenancySides: MultiTenancySides.Tenant);

            var responsavel = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Responsavel, L("Responsavel"), multiTenancySides: MultiTenancySides.Tenant);
            responsavel.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Create, L("CreatingNewResponsavel"), multiTenancySides: MultiTenancySides.Tenant);
            responsavel.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Edit, L("EditingResponsavel"), multiTenancySides: MultiTenancySides.Tenant);
            responsavel.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Delete, L("DeletingResponsavel"), multiTenancySides: MultiTenancySides.Tenant);

            var dependente = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Dependente, L("Dependente"), multiTenancySides: MultiTenancySides.Tenant);
            dependente.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Dependente_Create, L("CreatingNewDependente"), multiTenancySides: MultiTenancySides.Tenant);
            dependente.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Dependente_Edit, L("EditingDependente"), multiTenancySides: MultiTenancySides.Tenant);
            dependente.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Dependente_Delete, L("DeletingDependente"), multiTenancySides: MultiTenancySides.Tenant);

            var declaracaoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude, L("DeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            declaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Create, L("CreatingNewDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            declaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Edit, L("EditingDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            declaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Delete, L("DeletingDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var itemDeDeclaracaoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude, L("ItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Create, L("CreatingNewItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Edit, L("EditingItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Delete, L("DeletingItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var respostaDoItemDeDeclaracaoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude, L("RespostaDoItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            respostaDoItemDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Create, L("CreatingNewRespostaDoItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            respostaDoItemDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Edit, L("EditingRespostaDoItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            respostaDoItemDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Delete, L("DeletingRespostaDoItemDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var planoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude, L("PlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            planoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Create, L("CreatingNewPlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            planoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Edit, L("EditingPlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            planoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Delete, L("DeletingPlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var produtoDePlanoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude, L("ProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            produtoDePlanoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Create, L("CreatingNewProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            produtoDePlanoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Edit, L("EditingProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            produtoDePlanoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Delete, L("DeletingProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var operadora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Operadora, L("Operadora"), multiTenancySides: MultiTenancySides.Tenant);
            operadora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Operadora_Create, L("CreatingNewOperadora"), multiTenancySides: MultiTenancySides.Tenant);
            operadora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Operadora_Edit, L("EditingOperadora"), multiTenancySides: MultiTenancySides.Tenant);
            operadora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Operadora_Delete, L("DeletingOperadora"), multiTenancySides: MultiTenancySides.Tenant);

            var corretor = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretor, L("Corretor"), multiTenancySides: MultiTenancySides.Tenant);
            corretor.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretor_Create, L("CreatingNewCorretor"), multiTenancySides: MultiTenancySides.Tenant);
            corretor.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretor_Edit, L("EditingCorretor"), multiTenancySides: MultiTenancySides.Tenant);
            corretor.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretor_Delete, L("DeletingCorretor"), multiTenancySides: MultiTenancySides.Tenant);

            var corretora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretora, L("Corretora"), multiTenancySides: MultiTenancySides.Tenant);
            corretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretora_Create, L("CreatingNewCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            corretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretora_Edit, L("EditingCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            corretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Corretora_Delete, L("DeletingCorretora"), multiTenancySides: MultiTenancySides.Tenant);

            var associacao = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Associacao, L("Entidade"), multiTenancySides: MultiTenancySides.Tenant);
            associacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Associacao_Create, L("CreatingNewEntidade"), multiTenancySides: MultiTenancySides.Tenant);
            associacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Associacao_Edit, L("EditingEntidade"), multiTenancySides: MultiTenancySides.Tenant);
            associacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Associacao_Delete, L("DeletingEntidade"), multiTenancySides: MultiTenancySides.Tenant);

            var administradora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Administradora, L("Administradora"), multiTenancySides: MultiTenancySides.Tenant);
            administradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Administradora_Create, L("CreatingNewAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            administradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Administradora_Edit, L("EditingAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            administradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Administradora_Delete, L("DeletingAdministradora"), multiTenancySides: MultiTenancySides.Tenant);

            var contrato = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Contrato, L("Contrato"), multiTenancySides: MultiTenancySides.Tenant);
            contrato.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Contrato_Create, L("CreatingNewContrato"), multiTenancySides: MultiTenancySides.Tenant);
            contrato.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Contrato_Edit, L("EditingContrato"), multiTenancySides: MultiTenancySides.Tenant);
            contrato.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Contrato_Delete, L("DeletingContrato"), multiTenancySides: MultiTenancySides.Tenant);

            var faixaEtaria = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria, L("FaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            faixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Create, L("CreatingNewFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            faixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Edit, L("EditingFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            faixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Delete, L("DeletingFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);

            var especialidadeSync = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync, L("EspecialidadeSync"), multiTenancySides: MultiTenancySides.Tenant);
            especialidadeSync.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Create, L("CreatingNewEspecialidadeSync"), multiTenancySides: MultiTenancySides.Tenant);
            especialidadeSync.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Edit, L("EditingEspecialidadeSync"), multiTenancySides: MultiTenancySides.Tenant);
            especialidadeSync.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Delete, L("DeletingEspecialidadeSync"), multiTenancySides: MultiTenancySides.Tenant);
            especialidadeSync.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Get, L("GetEspecialidadeSync"), multiTenancySides: MultiTenancySides.Tenant);

            var propostaDeContratacao = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao, L("PropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create, L("CreatingNewPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit, L("EditingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Delete, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_PreCadastro, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_SelecionarPlano, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_PreenchimentoDosDados, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_EnvioDeDocumentos, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_AceiteDoContrato, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_Homologacao, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);
            propostaDeContratacao.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_Pagamento, L("DeletingPropostaDeContratacao"), multiTenancySides: MultiTenancySides.Tenant);

            ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ParametroEZLiv, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);

            var clienteEZLiv = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv, L("ClienteEZLiv"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEZLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Create, L("CreatingNewClienteEZLiv"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEZLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Edit, L("EditingClienteEZLiv"), multiTenancySides: MultiTenancySides.Tenant);
            clienteEZLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Delete, L("DeletingClienteEZLiv"), multiTenancySides: MultiTenancySides.Tenant);

            var valorPorFaixaEtaria = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria, L("ValorPorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            valorPorFaixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Create, L("CreatingNewValorPorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            valorPorFaixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Edit, L("EditingValorPorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            valorPorFaixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Delete, L("DeletingValorPorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);

            var redeCredenciada = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada, L("RedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);
            redeCredenciada.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Create, L("CreatingNewRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);
            redeCredenciada.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Edit, L("EditingRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);
            redeCredenciada.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Delete, L("DeletingRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);

            var ItemDeRedeCredenciada = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada, L("ItemDeRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);
            ItemDeRedeCredenciada.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Create, L("CreatingNewItemDeRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);
            ItemDeRedeCredenciada.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Edit, L("EditingItemDeRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);
            ItemDeRedeCredenciada.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Delete, L("DeletingItemDeRedeCredenciada"), multiTenancySides: MultiTenancySides.Tenant);

            var Laboratorio = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Laboratorio, L("Laboratorio"), multiTenancySides: MultiTenancySides.Tenant);
            Laboratorio.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Create, L("CreatingNewLaboratorio"), multiTenancySides: MultiTenancySides.Tenant);
            Laboratorio.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Edit, L("EditingLaboratorio"), multiTenancySides: MultiTenancySides.Tenant);
            Laboratorio.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Delete, L("DeletingLaboratorio"), multiTenancySides: MultiTenancySides.Tenant);

            var Hospital = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Hospital, L("Hospital"), multiTenancySides: MultiTenancySides.Tenant);
            Hospital.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Hospital_Create, L("CreatingNewHospital"), multiTenancySides: MultiTenancySides.Tenant);
            Hospital.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Hospital_Edit, L("EditingHospital"), multiTenancySides: MultiTenancySides.Tenant);
            Hospital.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Hospital_Delete, L("DeletingHospital"), multiTenancySides: MultiTenancySides.Tenant);

            var EspecialidadePorProdutoDePlanoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude, L("EspecialidadePorProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            EspecialidadePorProdutoDePlanoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Create, L("CreatingNewEspecialidadePorProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            EspecialidadePorProdutoDePlanoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Edit, L("EditingEspecialidadePorProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            EspecialidadePorProdutoDePlanoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Delete, L("DeletingEspecialidadePorProdutoDePlanoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var CorretorCorretora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_CorretorCorretora, L("CorretorCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            CorretorCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_CorretorCorretora_Create, L("CreatingNewCorretorCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            CorretorCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_CorretorCorretora_Edit, L("EditingCorretorCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            CorretorCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_CorretorCorretora_Delete, L("DeletingCorretorCorretora"), multiTenancySides: MultiTenancySides.Tenant);

            var chancela = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Chancela, L("Chancela"), multiTenancySides: MultiTenancySides.Tenant);
            chancela.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Chancela_Create, L("CreatingNewChancela"), multiTenancySides: MultiTenancySides.Tenant);
            chancela.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Chancela_Edit, L("EditingChancela"), multiTenancySides: MultiTenancySides.Tenant);
            chancela.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Chancela_Delete, L("DeletingChancela"), multiTenancySides: MultiTenancySides.Tenant);

            var vigencia = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Vigencia, L("Vigencia"), multiTenancySides: MultiTenancySides.Tenant);
            vigencia.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Create, L("CreatingNewVigencia"), multiTenancySides: MultiTenancySides.Tenant);
            vigencia.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Edit, L("EditingVigencia"), multiTenancySides: MultiTenancySides.Tenant);
            vigencia.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Delete, L("DeletingVigencia"), multiTenancySides: MultiTenancySides.Tenant);

            var aditivo = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Aditivo, L("Aditivo"), multiTenancySides: MultiTenancySides.Tenant);
            aditivo.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Aditivo_Create, L("CreatingNewAditivo"), multiTenancySides: MultiTenancySides.Tenant);
            aditivo.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Aditivo_Edit, L("EditingAditivo"), multiTenancySides: MultiTenancySides.Tenant);
            aditivo.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Aditivo_Delete, L("DeletingAditivo"), multiTenancySides: MultiTenancySides.Tenant);

            var questionarioDeDeclaracaoDeSaude = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude, L("QuestionarioDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            questionarioDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Create, L("CreatingNewQuestionarioDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            questionarioDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit, L("EditingQuestionarioDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);
            questionarioDeDeclaracaoDeSaude.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Delete, L("DeletingQuestionarioDeDeclaracaoDeSaude"), multiTenancySides: MultiTenancySides.Tenant);

            var indiceDeReajustePorFaixaEtaria = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria, L("IndiceDeReajustePorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            indiceDeReajustePorFaixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Create, L("CreatingNewIndiceDeReajustePorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            indiceDeReajustePorFaixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Edit, L("EditingIndiceDeReajustePorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);
            indiceDeReajustePorFaixaEtaria.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Delete, L("DeletingIndiceDeReajustePorFaixaEtaria"), multiTenancySides: MultiTenancySides.Tenant);

            var meusProdutos = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, L("MeusProdutos"), multiTenancySides: MultiTenancySides.Tenant);

            var meusProcessos = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_MeusProcessos, L("MeusProcessos"), multiTenancySides: MultiTenancySides.Tenant);

            var carencia = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Carencia, L("Carencia"), multiTenancySides: MultiTenancySides.Tenant);
            carencia.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Carencia_Create, L("CreatingNewCarencia"), multiTenancySides: MultiTenancySides.Tenant);
            carencia.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Carencia_Edit, L("EditingCarencia"), multiTenancySides: MultiTenancySides.Tenant);
            carencia.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Carencia_Delete, L("DeletingCarencia"), multiTenancySides: MultiTenancySides.Tenant);

            var gerenteCorretora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora, L("GerenteCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            gerenteCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Create, L("CreatingNewGerenteCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            gerenteCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Edit, L("EditingGerenteCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            gerenteCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Delete, L("DeletingGerenteCorretora"), multiTenancySides: MultiTenancySides.Tenant);

            var supervisorCorretora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora, L("SupervisorCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            supervisorCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Create, L("CreatingNewSupervisorCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            supervisorCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Edit, L("EditingSupervisorCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            supervisorCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Delete, L("DeletingSupervisorCorretora"), multiTenancySides: MultiTenancySides.Tenant);

            var gerenteAdministradora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora, L("GerenteAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            gerenteAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Create, L("CreatingNewGerenteAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            gerenteAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Edit, L("EditingGerenteAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            gerenteAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Delete, L("DeletingGerenteAdministradora"), multiTenancySides: MultiTenancySides.Tenant);

            var supervisorAdministradora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora, L("SupervisorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            supervisorAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Create, L("CreatingNewSupervisorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            supervisorAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Edit, L("EditingSupervisorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            supervisorAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Delete, L("DeletingSupervisorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);

            var homologadorAdministradora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_HomologadorAdministradora, L("HomologadorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            homologadorAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_HomologadorAdministradora_Create, L("CreatingNewHomologadorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            homologadorAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_HomologadorAdministradora_Edit, L("EditingHomologadorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);
            homologadorAdministradora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_HomologadorAdministradora_Delete, L("DeletingHomologadorAdministradora"), multiTenancySides: MultiTenancySides.Tenant);

            var declaracaoDoBeneficiario = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario, L("DeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            declaracaoDoBeneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Create, L("CreatingNewDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            declaracaoDoBeneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Edit, L("EditingDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            declaracaoDoBeneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Delete, L("DeletingDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);

            var itemDeDeclaracaoDoBeneficiario = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario, L("ItemDeDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeDeclaracaoDoBeneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Create, L("CreatingNewItemDeDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeDeclaracaoDoBeneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Edit, L("EditingItemDeDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);
            itemDeDeclaracaoDoBeneficiario.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Delete, L("DeletingItemDeDeclaracaoDoBeneficiario"), multiTenancySides: MultiTenancySides.Tenant);

            var permissaoDeVendaDePlanoDeSaudeParaCorretora = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora, L("PermissaoDeVendaDePlanoDeSaudeParaCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            permissaoDeVendaDePlanoDeSaudeParaCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create, L("CreatingNewPermissaoDeVendaDePlanoDeSaudeParaCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            permissaoDeVendaDePlanoDeSaudeParaCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit, L("EditingPermissaoDeVendaDePlanoDeSaudeParaCorretora"), multiTenancySides: MultiTenancySides.Tenant);
            permissaoDeVendaDePlanoDeSaudeParaCorretora.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Delete, L("DeletingPermissaoDeVendaDePlanoDeSaudeParaCorretora"), multiTenancySides: MultiTenancySides.Tenant);

            var permissaoDeVendaDePlanoDeSaudeParaCorretor = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor, L("PermissaoDeVendaDePlanoDeSaudeParaCorretor"), multiTenancySides: MultiTenancySides.Tenant);
            permissaoDeVendaDePlanoDeSaudeParaCorretor.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create, L("CreatingNewPermissaoDeVendaDePlanoDeSaudeParaCorretor"), multiTenancySides: MultiTenancySides.Tenant);
            permissaoDeVendaDePlanoDeSaudeParaCorretor.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit, L("EditingPermissaoDeVendaDePlanoDeSaudeParaCorretor"), multiTenancySides: MultiTenancySides.Tenant);
            permissaoDeVendaDePlanoDeSaudeParaCorretor.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Delete, L("DeletingPermissaoDeVendaDePlanoDeSaudeParaCorretor"), multiTenancySides: MultiTenancySides.Tenant);

            var relatorio = ezLiv.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Relatorio, L("Relatorio"), multiTenancySides: MultiTenancySides.Tenant);
            relatorio.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Create, L("CreatingNewRelatorio"), multiTenancySides: MultiTenancySides.Tenant);
            relatorio.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Edit, L("EditingRelatorio"), multiTenancySides: MultiTenancySides.Tenant);
            relatorio.CreateChildPermission(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Delete, L("DeletingRelatorio"), multiTenancySides: MultiTenancySides.Tenant);

            #endregion

            #region Módulo EZPag
            var ezPag = pages.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag, L("EZPag"));
            var testesEzPag = ezPag.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_Testes, L("Testes"));

            var formaDePagamento = ezPag.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento, L("FormaDePagamento"), multiTenancySides: MultiTenancySides.Tenant);
            formaDePagamento.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Create, L("CreatingNewFormaDePagamento"), multiTenancySides: MultiTenancySides.Tenant);
            formaDePagamento.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Edit, L("EditingFormaDePagamento"), multiTenancySides: MultiTenancySides.Tenant);
            formaDePagamento.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Delete, L("DeletingFormaDePagamento"), multiTenancySides: MultiTenancySides.Tenant);


            ezPag.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_Transacao, L("Transacao"), multiTenancySides: MultiTenancySides.Tenant);

            ezPag.CreateChildPermission(AppPermissions.Pages_Tenant_EZPag_ParametroEzpag, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            #region Módulo Serviços 
            var servicos = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos, L("Servicos"));

            var servicosServico = servicos.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos_Servico, L("Servico"), multiTenancySides: MultiTenancySides.Tenant);
            servicosServico.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos_Servico_Create, L("CreatingNewServico"), multiTenancySides: MultiTenancySides.Tenant);
            servicosServico.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos_Servico_Edit, L("EditingServico"), multiTenancySides: MultiTenancySides.Tenant);
            servicosServico.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos_Servico_Delete, L("DeletingServico"), multiTenancySides: MultiTenancySides.Tenant);

            //var parametroServicos = servicos.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos_Parametro, L("Parametros"), multiTenancySides: MultiTenancySides.Tenant);
            //parametroEstoque.CreateChildPermission(AppPermissions.Pages_Tenant_Estoque_Parametro_Edit, L("EditingParametroServicos"), multiTenancySides: MultiTenancySides.Tenant);

            //servicos.CreateChildPermission(AppPermissions.Pages_Tenant_Servicos_Evento, L("Comissionamento"), multiTenancySides: MultiTenancySides.Tenant);
            #endregion

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, EZControlConsts.LocalizationSourceName);
        }
    }
}
