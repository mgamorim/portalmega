﻿namespace EZ.EZControl.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_ClienteEZ = "Pages.Administration.Tenant.ClienteEZ";

        public const string Pages_Administration_UsuarioPorPessoa = "Pages.Administration.Tenant.UsuarioPorPessoa";
        public const string Pages_Administration_UsuarioPorPessoa_Create = "Pages.Administration.Tenant.UsuarioPorPessoa.Create";
        public const string Pages_Administration_UsuarioPorPessoa_Edit = "Pages.Administration.Tenant.UsuarioPorPessoa.Edit";
        public const string Pages_Administration_UsuarioPorPessoa_Delete = "Pages.Administration.Tenant.UsuarioPorPessoa.Delete";

        public const string Pages_Tenant_AcessoLeitura = "Pages.Tenant.AcessoLeitura";


        #region Módulo Global

        public const string Pages_Tenant_Global = "Pages.Tenant.Global";

        public const string Pages_Tenant_Global_Perfil = "Pages.Tenant.Global.Perfil";

        public const string Pages_Tenant_Global_ConexaoSMTP = "Pages.Tenant.Global.ConexaoSMTP";
        public const string Pages_Tenant_Global_ConexaoSMTP_Create = "Pages.Tenant.Global.ConexaoSMTP.Create";
        public const string Pages_Tenant_Global_ConexaoSMTP_Edit = "Pages.Tenant.Global.ConexaoSMTP.Edit";
        public const string Pages_Tenant_Global_ConexaoSMTP_Delete = "Pages.Tenant.Global.ConexaoSMTP.Delete";

        public const string Pages_Tenant_Global_TemplateEmail = "Pages.Tenant.Global.TemplateEmail";
        public const string Pages_Tenant_Global_TemplateEmail_Create = "Pages.Tenant.Global.TemplateEmail.Create";
        public const string Pages_Tenant_Global_TemplateEmail_Edit = "Pages.Tenant.Global.TemplateEmail.Edit";
        public const string Pages_Tenant_Global_TemplateEmail_Delete = "Pages.Tenant.Global.TemplateEmail.Delete";

        public const string Pages_Tenant_Global_SendEmail = "Pages.Tenant.Global.SendEmail";

        public const string Pages_Tenant_Global_DadosEmpresa = "Pages.Tenant.Global.DadosEmpresa";
        public const string Pages_Tenant_Global_DadosEmpresa_Edit = "Pages.Tenant.Global.DadosEmpresa.Edit";

        public const string Pages_Tenant_Global_Arquivo = "Pages.Tenant.Global.Arquivo";
        public const string Pages_Tenant_Global_Arquivo_Create = "Pages.Tenant.Global.Arquivo.Create";
        public const string Pages_Tenant_Global_Arquivo_Edit = "Pages.Tenant.Global.Arquivo.Edit";
        public const string Pages_Tenant_Global_Arquivo_Delete = "Pages.Tenant.Global.Arquivo.Delete";

        public const string Pages_Tenant_Global_PessoaFisica = "Pages.Tenant.Global.PessoaFisica";
        public const string Pages_Tenant_Global_PessoaFisica_Create = "Pages.Tenant.Global.PessoaFisica.Create";
        public const string Pages_Tenant_Global_PessoaFisica_Edit = "Pages.Tenant.Global.PessoaFisica.Edit";
        public const string Pages_Tenant_Global_PessoaFisica_Delete = "Pages.Tenant.Global.PessoaFisica.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaGrupoPessoa = "Pages.Tenant.Global.PessoaFisicaGrupoPessoa";
        public const string Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Create = "Pages.Tenant.Global.PessoaFisicaGrupoPessoa.Create";
        public const string Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Edit = "Pages.Tenant.Global.PessoaFisicaGrupoPessoa.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Delete = "Pages.Tenant.Global.PessoaFisicaGrupoPessoa.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaEmpresa = "Pages.Tenant.Global.PessoaFisicaEmpresa";
        public const string Pages_Tenant_Global_PessoaFisicaEmpresa_Create = "Pages.Tenant.Global.PessoaFisicaEmpresa.Create";
        public const string Pages_Tenant_Global_PessoaFisicaEmpresa_Edit = "Pages.Tenant.Global.PessoaFisicaEmpresa.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaEmpresa_Delete = "Pages.Tenant.Global.PessoaFisicaEmpresa.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaDocumento = "Pages.Tenant.Global.PessoaFisicaDocumento";
        public const string Pages_Tenant_Global_PessoaFisicaDocumento_Create = "Pages.Tenant.Global.PessoaFisicaDocumento.Create";
        public const string Pages_Tenant_Global_PessoaFisicaDocumento_Edit = "Pages.Tenant.Global.PessoaFisicaDocumento.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaDocumento_Delete = "Pages.Tenant.Global.PessoaFisicaDocumento.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaEnderecoEletronico = "Pages.Tenant.Global.PessoaFisicaEnderecoEletronico";
        public const string Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Create = "Pages.Tenant.Global.PessoaFisicaEnderecoEletronico.Create";
        public const string Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Edit = "Pages.Tenant.Global.PessoaFisicaEnderecoEletronico.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Delete = "Pages.Tenant.Global.PessoaFisicaEnderecoEletronico.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaEndereco = "Pages.Tenant.Global.PessoaFisicaEndereco";
        public const string Pages_Tenant_Global_PessoaFisicaEndereco_Create = "Pages.Tenant.Global.PessoaFisicaEndereco.Create";
        public const string Pages_Tenant_Global_PessoaFisicaEndereco_Edit = "Pages.Tenant.Global.PessoaFisicaEndereco.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaEndereco_Delete = "Pages.Tenant.Global.PessoaFisicaEndereco.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaContato = "Pages.Tenant.Global.PessoaFisicaContato";
        public const string Pages_Tenant_Global_PessoaFisicaContato_Create = "Pages.Tenant.Global.PessoaFisicaContato.Create";
        public const string Pages_Tenant_Global_PessoaFisicaContato_Edit = "Pages.Tenant.Global.PessoaFisicaContato.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaContato_Delete = "Pages.Tenant.Global.PessoaFisicaContato.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaTelefone = "Pages.Tenant.Global.PessoaFisicaTelefone";
        public const string Pages_Tenant_Global_PessoaFisicaTelefone_Create = "Pages.Tenant.Global.PessoaFisicaTelefone.Create";
        public const string Pages_Tenant_Global_PessoaFisicaTelefone_Edit = "Pages.Tenant.Global.PessoaFisicaTelefone.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaTelefone_Delete = "Pages.Tenant.Global.PessoaFisicaTelefone.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaTipoDeDocumento = "Pages.Tenant.Global.PessoaFisicaTipoDeDocumento";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Create = "Pages.Tenant.Global.PessoaFisicaTipoDeDocumento.Create";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Edit = "Pages.Tenant.Global.PessoaFisicaTipoDeDocumento.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Delete = "Pages.Tenant.Global.PessoaFisicaTipoDeDocumento.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaPais = "Pages.Tenant.Global.PessoaFisicaPais";
        public const string Pages_Tenant_Global_PessoaFisicaPais_Create = "Pages.Tenant.Global.PessoaFisicaPais.Create";
        public const string Pages_Tenant_Global_PessoaFisicaPais_Edit = "Pages.Tenant.Global.PessoaFisicaPais.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaPais_Delete = "Pages.Tenant.Global.PessoaFisicaPais.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaEstado = "Pages.Tenant.Global.PessoaFisicaEstado";
        public const string Pages_Tenant_Global_PessoaFisicaEstado_Create = "Pages.Tenant.Global.PessoaFisicaEstado.Create";
        public const string Pages_Tenant_Global_PessoaFisicaEstado_Edit = "Pages.Tenant.Global.PessoaFisicaEstado.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaEstado_Delete = "Pages.Tenant.Global.PessoaFisicaEstado.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaCidade = "Pages.Tenant.Global.PessoaFisicaCidade";
        public const string Pages_Tenant_Global_PessoaFisicaCidade_Create = "Pages.Tenant.Global.PessoaFisicaCidade.Create";
        public const string Pages_Tenant_Global_PessoaFisicaCidade_Edit = "Pages.Tenant.Global.PessoaFisicaCidade.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaCidade_Delete = "Pages.Tenant.Global.PessoaFisicaCidade.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro = "Pages.Tenant.Global.PessoaFisicaTipoDeLogradouro";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Create = "Pages.Tenant.Global.PessoaFisicaTipoDeLogradouro.Create";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Edit = "Pages.Tenant.Global.PessoaFisicaTipoDeLogradouro.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Delete = "Pages.Tenant.Global.PessoaFisicaTipoDeLogradouro.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaTipoDeContato = "Pages.Tenant.Global.PessoaFisicaTipoDeContato";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeContato_Create = "Pages.Tenant.Global.PessoaFisicaTipoDeContato.Create";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeContato_Edit = "Pages.Tenant.Global.PessoaFisicaTipoDeContato.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaTipoDeContato_Delete = "Pages.Tenant.Global.PessoaFisicaTipoDeContato.Delete";

        public const string Pages_Tenant_Global_PessoaFisicaTratamento = "Pages.Tenant.Global.PessoaFisicaTratamento";
        public const string Pages_Tenant_Global_PessoaFisicaTratamento_Create = "Pages.Tenant.Global.PessoaFisicaTratamento.Create";
        public const string Pages_Tenant_Global_PessoaFisicaTratamento_Edit = "Pages.Tenant.Global.PessoaFisicaTratamento.Edit";
        public const string Pages_Tenant_Global_PessoaFisicaTratamento_Delete = "Pages.Tenant.Global.PessoaFisicaTratamento.Delete";


        //Pessoa Juridica
        public const string Pages_Tenant_Global_PessoaJuridica = "Pages.Tenant.Global.PessoaJuridica";
        public const string Pages_Tenant_Global_PessoaJuridica_Create = "Pages.Tenant.Global.PessoaJuridica.Create";
        public const string Pages_Tenant_Global_PessoaJuridica_Edit = "Pages.Tenant.Global.PessoaJuridica.Edit";
        public const string Pages_Tenant_Global_PessoaJuridica_Delete = "Pages.Tenant.Global.PessoaJuridica.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaTelefone = "Pages.Tenant.Global.PessoaJuridicaTelefone";
        public const string Pages_Tenant_Global_PessoaJuridicaTelefone_Create = "Pages.Tenant.Global.PessoaJuridicaTelefone.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaTelefone_Edit = "Pages.Tenant.Global.PessoaJuridicaTelefone.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaTelefone_Delete = "Pages.Tenant.Global.PessoaJuridicaTelefone.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaGrupoPessoa = "Pages.Tenant.Global.PessoaJuridicaGrupoPessoa";
        public const string Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Create = "Pages.Tenant.Global.PessoaJuridicaGrupoPessoa.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Edit = "Pages.Tenant.Global.PessoaJuridicaGrupoPessoa.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Delete = "Pages.Tenant.Global.PessoaJuridicaGrupoPessoa.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaEmpresa = "Pages.Tenant.Global.PessoaJuridicaEmpresa";
        public const string Pages_Tenant_Global_PessoaJuridicaEmpresa_Create = "Pages.Tenant.Global.PessoaJuridicaEmpresa.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaEmpresa_Edit = "Pages.Tenant.Global.PessoaJuridicaEmpresa.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaEmpresa_Delete = "Pages.Tenant.Global.PessoaJuridicaEmpresa.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaDocumento = "Pages.Tenant.Global.PessoaJuridicaDocumento";
        public const string Pages_Tenant_Global_PessoaJuridicaDocumento_Create = "Pages.Tenant.Global.PessoaJuridicaDocumento.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaDocumento_Edit = "Pages.Tenant.Global.PessoaJuridicaDocumento.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaDocumento_Delete = "Pages.Tenant.Global.PessoaJuridicaDocumento.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico = "Pages.Tenant.Global.PessoaJuridicaEnderecoEletronico";
        public const string Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Create = "Pages.Tenant.Global.PessoaJuridicaEnderecoEletronico.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Edit = "Pages.Tenant.Global.PessoaJuridicaEnderecoEletronico.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Delete = "Pages.Tenant.Global.PessoaJuridicaEnderecoEletronico.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaEndereco = "Pages.Tenant.Global.PessoaJuridicaEndereco";
        public const string Pages_Tenant_Global_PessoaJuridicaEndereco_Create = "Pages.Tenant.Global.PessoaJuridicaEndereco.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaEndereco_Edit = "Pages.Tenant.Global.PessoaJuridicaEndereco.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaEndereco_Delete = "Pages.Tenant.Global.PessoaJuridicaEndereco.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaContato = "Pages.Tenant.Global.PessoaJuridicaContato";
        public const string Pages_Tenant_Global_PessoaJuridicaContato_Create = "Pages.Tenant.Global.PessoaJuridicaContato.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaContato_Edit = "Pages.Tenant.Global.PessoaJuridicaContato.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaContato_Delete = "Pages.Tenant.Global.PessoaJuridicaContato.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento = "Pages.Tenant.Global.PessoaJuridicaTipoDeDocumento";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Create = "Pages.Tenant.Global.PessoaJuridicaTipoDeDocumento.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Edit = "Pages.Tenant.Global.PessoaJuridicaTipoDeDocumento.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Delete = "Pages.Tenant.Global.PessoaJuridicaTipoDeDocumento.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaPais = "Pages.Tenant.Global.PessoaJuridicaPais";
        public const string Pages_Tenant_Global_PessoaJuridicaPais_Create = "Pages.Tenant.Global.PessoaJuridicaPais.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaPais_Edit = "Pages.Tenant.Global.PessoaJuridicaPais.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaPais_Delete = "Pages.Tenant.Global.PessoaJuridicaPais.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaEstado = "Pages.Tenant.Global.PessoaJuridicaEstado";
        public const string Pages_Tenant_Global_PessoaJuridicaEstado_Create = "Pages.Tenant.Global.PessoaJuridicaEstado.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaEstado_Edit = "Pages.Tenant.Global.PessoaJuridicaEstado.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaEstado_Delete = "Pages.Tenant.Global.PessoaJuridicaEstado.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaCidade = "Pages.Tenant.Global.PessoaJuridicaCidade";
        public const string Pages_Tenant_Global_PessoaJuridicaCidade_Create = "Pages.Tenant.Global.PessoaJuridicaCidade.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaCidade_Edit = "Pages.Tenant.Global.PessoaJuridicaCidade.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaCidade_Delete = "Pages.Tenant.Global.PessoaJuridicaCidade.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro = "Pages.Tenant.Global.PessoaJuridicaTipoDeLogradouro";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Create = "Pages.Tenant.Global.PessoaJuridicaTipoDeLogradouro.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Edit = "Pages.Tenant.Global.PessoaJuridicaTipoDeLogradouro.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Delete = "Pages.Tenant.Global.PessoaJuridicaTipoDeLogradouro.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeContato = "Pages.Tenant.Global.PessoaJuridicaTipoDeContato";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Create = "Pages.Tenant.Global.PessoaJuridicaTipoDeContato.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Edit = "Pages.Tenant.Global.PessoaJuridicaTipoDeContato.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Delete = "Pages.Tenant.Global.PessoaJuridicaTipoDeContato.Delete";

        public const string Pages_Tenant_Global_PessoaJuridicaTratamento = "Pages.Tenant.Global.PessoaJuridicaTratamento";
        public const string Pages_Tenant_Global_PessoaJuridicaTratamento_Create = "Pages.Tenant.Global.PessoaJuridicaTratamento.Create";
        public const string Pages_Tenant_Global_PessoaJuridicaTratamento_Edit = "Pages.Tenant.Global.PessoaJuridicaTratamento.Edit";
        public const string Pages_Tenant_Global_PessoaJuridicaTratamento_Delete = "Pages.Tenant.Global.PessoaJuridicaTratamento.Delete";

        //Tipo de Contato
        public const string Pages_Tenant_Global_TipoDeContato = "Pages.Tenant.Global.TipoDeContato";
        public const string Pages_Tenant_Global_TipoDeContato_Create = "Pages.Tenant.Global.TipoDeContato.Create";
        public const string Pages_Tenant_Global_TipoDeContato_Edit = "Pages.Tenant.Global.TipoDeContato.Edit";
        public const string Pages_Tenant_Global_TipoDeContato_Delete = "Pages.Tenant.Global.TipoDeContato.Delete";

        public const string Pages_Tenant_Global_Contato = "Pages.Tenant.Global.Contato";
        public const string Pages_Tenant_Global_Contato_Create = "Pages.Tenant.Global.Contato.Create";
        public const string Pages_Tenant_Global_Contato_Edit = "Pages.Tenant.Global.Contato.Edit";
        public const string Pages_Tenant_Global_Contato_Delete = "Pages.Tenant.Global.Contato.Delete";

        public const string Pages_Tenant_Global_Endereco = "Pages.Tenant.Global.Endereco";
        public const string Pages_Tenant_Global_Endereco_Create = "Pages.Tenant.Global.Endereco.Create";
        public const string Pages_Tenant_Global_Endereco_Edit = "Pages.Tenant.Global.Endereco.Edit";
        public const string Pages_Tenant_Global_Endereco_Delete = "Pages.Tenant.Global.Endereco.Delete";

        public const string Pages_Tenant_Global_Departamento = "Pages.Tenant.Global.Departamento";
        public const string Pages_Tenant_Global_Departamento_Create = "Pages.Tenant.Global.Departamento.Create";
        public const string Pages_Tenant_Global_Departamento_Edit = "Pages.Tenant.Global.Departamento.Edit";
        public const string Pages_Tenant_Global_Departamento_Delete = "Pages.Tenant.Global.Departamento.Delete";

        public const string Pages_Tenant_Global_ItemHierarquia = "Pages.Tenant.Global.ItemHierarquia";
        public const string Pages_Tenant_Global_ItemHierarquia_Create = "Pages.Tenant.Global.ItemHierarquia.Create";
        public const string Pages_Tenant_Global_ItemHierarquia_Edit = "Pages.Tenant.Global.ItemHierarquia.Edit";
        public const string Pages_Tenant_Global_ItemHierarquia_Delete = "Pages.Tenant.Global.ItemHierarquia.Delete";

        public const string Pages_Tenant_Global_Banco = "Pages.Tenant.Global.Banco";
        public const string Pages_Tenant_Global_Banco_Create = "Pages.Tenant.Global.Banco.Create";
        public const string Pages_Tenant_Global_Banco_Edit = "Pages.Tenant.Global.Banco.Edit";
        public const string Pages_Tenant_Global_Banco_Delete = "Pages.Tenant.Global.Banco.Delete";

        public const string Pages_Tenant_Global_Agencia = "Pages.Tenant.Global.Agencia";
        public const string Pages_Tenant_Global_Agencia_Create = "Pages.Tenant.Global.Agencia.Create";
        public const string Pages_Tenant_Global_Agencia_Edit = "Pages.Tenant.Global.Agencia.Edit";
        public const string Pages_Tenant_Global_Agencia_Delete = "Pages.Tenant.Global.Agencia.Delete";

        public const string Pages_Tenant_Global_CampoPersonalizado = "Pages.Tenant.Global.CampoPersonalizado";
        public const string Pages_Tenant_Global_CampoPersonalizado_Create = "Pages.Tenant.Global.CampoPersonalizado.Create";
        public const string Pages_Tenant_Global_CampoPersonalizado_Edit = "Pages.Tenant.Global.CampoPersonalizado.Edit";
        public const string Pages_Tenant_Global_CampoPersonalizado_Delete = "Pages.Tenant.Global.CampoPersonalizado.Delete";

        //
        public const string Pages_Tenant_Global_Empresa = "Pages.Tenant.Global.Empresa";
        public const string Pages_Tenant_Global_Empresa_Create = "Pages.Tenant.Global.Empresa.Create";
        public const string Pages_Tenant_Global_Empresa_Edit = "Pages.Tenant.Global.Empresa.Edit";
        public const string Pages_Tenant_Global_Empresa_Delete = "Pages.Tenant.Global.Empresa.Delete";

        public const string Pages_Tenant_Global_EmpresaDocumento = "Pages.Tenant.Global.EmpresaDocumento";
        public const string Pages_Tenant_Global_EmpresaDocumento_Create = "Pages.Tenant.Global.EmpresaDocumento.Create";
        public const string Pages_Tenant_Global_EmpresaDocumento_Edit = "Pages.Tenant.Global.EmpresaDocumento.Edit";
        public const string Pages_Tenant_Global_EmpresaDocumento_Delete = "Pages.Tenant.Global.EmpresaDocumento.Delete";

        public const string Pages_Tenant_Global_EmpresaEnderecoEletronico = "Pages.Tenant.Global.EmpresaEnderecoEletronico";
        public const string Pages_Tenant_Global_EmpresaEnderecoEletronico_Create = "Pages.Tenant.Global.EmpresaEnderecoEletronico.Create";
        public const string Pages_Tenant_Global_EmpresaEnderecoEletronico_Edit = "Pages.Tenant.Global.EmpresaEnderecoEletronico.Edit";
        public const string Pages_Tenant_Global_EmpresaEnderecoEletronico_Delete = "Pages.Tenant.Global.EmpresaEnderecoEletronico.Delete";

        public const string Pages_Tenant_Global_EmpresaEndereco = "Pages.Tenant.Global.EmpresaEndereco";
        public const string Pages_Tenant_Global_EmpresaEndereco_Create = "Pages.Tenant.Global.EmpresaEndereco.Create";
        public const string Pages_Tenant_Global_EmpresaEndereco_Edit = "Pages.Tenant.Global.EmpresaEndereco.Edit";
        public const string Pages_Tenant_Global_EmpresaEndereco_Delete = "Pages.Tenant.Global.EmpresaEndereco.Delete";

        public const string Pages_Tenant_Global_EmpresaContato = "Pages.Tenant.Global.EmpresaContato";
        public const string Pages_Tenant_Global_EmpresaContato_Create = "Pages.Tenant.Global.EmpresaContato.Create";
        public const string Pages_Tenant_Global_EmpresaContato_Edit = "Pages.Tenant.Global.EmpresaContato.Edit";
        public const string Pages_Tenant_Global_EmpresaContato_Delete = "Pages.Tenant.Global.EmpresaContato.Delete";

        //
        public const string Pages_Tenant_Global_Fornecedor = "Pages.Tenant.Global.Fornecedor";
        public const string Pages_Tenant_Global_Fornecedor_Create = "Pages.Tenant.Global.Fornecedor.Create";
        public const string Pages_Tenant_Global_Fornecedor_Edit = "Pages.Tenant.Global.Fornecedor.Edit";
        public const string Pages_Tenant_Global_Fornecedor_Delete = "Pages.Tenant.Global.Fornecedor.Delete";

        public const string Pages_Tenant_Global_FornecedorDocumento = "Pages.Tenant.Global.FornecedorDocumento";
        public const string Pages_Tenant_Global_FornecedorDocumento_Create = "Pages.Tenant.Global.FornecedorDocumento.Create";
        public const string Pages_Tenant_Global_FornecedorDocumento_Edit = "Pages.Tenant.Global.FornecedorDocumento.Edit";
        public const string Pages_Tenant_Global_FornecedorDocumento_Delete = "Pages.Tenant.Global.FornecedorDocumento.Delete";

        public const string Pages_Tenant_Global_FornecedorEnderecoEletronico = "Pages.Tenant.Global.FornecedorEnderecoEletronico";
        public const string Pages_Tenant_Global_FornecedorEnderecoEletronico_Create = "Pages.Tenant.Global.FornecedorEnderecoEletronico.Create";
        public const string Pages_Tenant_Global_FornecedorEnderecoEletronico_Edit = "Pages.Tenant.Global.FornecedorEnderecoEletronico.Edit";
        public const string Pages_Tenant_Global_FornecedorEnderecoEletronico_Delete = "Pages.Tenant.Global.FornecedorEnderecoEletronico.Delete";

        public const string Pages_Tenant_Global_FornecedorEndereco = "Pages.Tenant.Global.FornecedorEndereco";
        public const string Pages_Tenant_Global_FornecedorEndereco_Create = "Pages.Tenant.Global.FornecedorEndereco.Create";
        public const string Pages_Tenant_Global_FornecedorEndereco_Edit = "Pages.Tenant.Global.FornecedorEndereco.Edit";
        public const string Pages_Tenant_Global_FornecedorEndereco_Delete = "Pages.Tenant.Global.FornecedorEndereco.Delete";

        public const string Pages_Tenant_Global_FornecedorContato = "Pages.Tenant.Global.FornecedorContato";
        public const string Pages_Tenant_Global_FornecedorContato_Create = "Pages.Tenant.Global.FornecedorContato.Create";
        public const string Pages_Tenant_Global_FornecedorContato_Edit = "Pages.Tenant.Global.FornecedorContato.Edit";
        public const string Pages_Tenant_Global_FornecedorContato_Delete = "Pages.Tenant.Global.FornecedorContato.Delete";

        //
        public const string Pages_Tenant_Global_Cliente = "Pages.Tenant.Global.Cliente";
        public const string Pages_Tenant_Global_Cliente_Create = "Pages.Tenant.Global.Cliente.Create";
        public const string Pages_Tenant_Global_Cliente_Edit = "Pages.Tenant.Global.Cliente.Edit";
        public const string Pages_Tenant_Global_Cliente_Delete = "Pages.Tenant.Global.Cliente.Delete";

        public const string Pages_Tenant_Global_ClienteDocumento = "Pages.Tenant.Global.ClienteDocumento";
        public const string Pages_Tenant_Global_ClienteDocumento_Create = "Pages.Tenant.Global.ClienteDocumento.Create";
        public const string Pages_Tenant_Global_ClienteDocumento_Edit = "Pages.Tenant.Global.ClienteDocumento.Edit";
        public const string Pages_Tenant_Global_ClienteDocumento_Delete = "Pages.Tenant.Global.ClienteDocumento.Delete";

        public const string Pages_Tenant_Global_ClienteEnderecoEletronico = "Pages.Tenant.Global.ClienteEnderecoEletronico";
        public const string Pages_Tenant_Global_ClienteEnderecoEletronico_Create = "Pages.Tenant.Global.ClienteEnderecoEletronico.Create";
        public const string Pages_Tenant_Global_ClienteEnderecoEletronico_Edit = "Pages.Tenant.Global.ClienteEnderecoEletronico.Edit";
        public const string Pages_Tenant_Global_ClienteEnderecoEletronico_Delete = "Pages.Tenant.Global.ClienteEnderecoEletronico.Delete";

        public const string Pages_Tenant_Global_ClienteEndereco = "Pages.Tenant.Global.ClienteEndereco";
        public const string Pages_Tenant_Global_ClienteEndereco_Create = "Pages.Tenant.Global.ClienteEndereco.Create";
        public const string Pages_Tenant_Global_ClienteEndereco_Edit = "Pages.Tenant.Global.ClienteEndereco.Edit";
        public const string Pages_Tenant_Global_ClienteEndereco_Delete = "Pages.Tenant.Global.ClienteEndereco.Delete";

        public const string Pages_Tenant_Global_ClienteContato = "Pages.Tenant.Global.ClienteContato";
        public const string Pages_Tenant_Global_ClienteContato_Create = "Pages.Tenant.Global.ClienteContato.Create";
        public const string Pages_Tenant_Global_ClienteContato_Edit = "Pages.Tenant.Global.ClienteContato.Edit";
        public const string Pages_Tenant_Global_ClienteContato_Delete = "Pages.Tenant.Global.ClienteContato.Delete";

        public const string Pages_Tenant_Global_RamoDoFornecedor = "Pages.Tenant.Global.RamoDoFornecedor";
        public const string Pages_Tenant_Global_RamoDoFornecedor_Create = "Pages.Tenant.Global.RamoDoFornecedor.Create";
        public const string Pages_Tenant_Global_RamoDoFornecedor_Edit = "Pages.Tenant.Global.RamoDoFornecedor.Edit";
        public const string Pages_Tenant_Global_RamoDoFornecedor_Delete = "Pages.Tenant.Global.RamoDoFornecedor.Delete";

        public const string Pages_Tenant_Global_Feriado = "Pages.Tenant.Global.Feriado";
        public const string Pages_Tenant_Global_Feriado_Create = "Pages.Tenant.Global.Feriado.Create";
        public const string Pages_Tenant_Global_Feriado_Edit = "Pages.Tenant.Global.Feriado.Edit";
        public const string Pages_Tenant_Global_Feriado_Delete = "Pages.Tenant.Global.Feriado.Delete";

        public const string Pages_Tenant_Global_TipoDeDocumento = "Pages.Tenant.Global.TipoDeDocumento";
        public const string Pages_Tenant_Global_TipoDeDocumento_Create = "Pages.Tenant.Global.TipoDeDocumento.Create";
        public const string Pages_Tenant_Global_TipoDeDocumento_Edit = "Pages.Tenant.Global.TipoDeDocumento.Edit";
        public const string Pages_Tenant_Global_TipoDeDocumento_Delete = "Pages.Tenant.Global.TipoDeDocumento.Delete";

        public const string Pages_Tenant_Global_TipoDeLogradouro = "Pages.Tenant.Global.TipoDeLogradouro";
        public const string Pages_Tenant_Global_TipoDeLogradouro_Create = "Pages.Tenant.Global.TipoDeLogradouro.Create";
        public const string Pages_Tenant_Global_TipoDeLogradouro_Edit = "Pages.Tenant.Global.TipoDeLogradouro.Edit";
        public const string Pages_Tenant_Global_TipoDeLogradouro_Delete = "Pages.Tenant.Global.TipoDeLogradouro.Delete";

        public const string Pages_Tenant_Global_Profissao = "Pages.Tenant.Global.Profissao";
        public const string Pages_Tenant_Global_Profissao_Create = "Pages.Tenant.Global.Profissao.Create";
        public const string Pages_Tenant_Global_Profissao_Edit = "Pages.Tenant.Global.Profissao.Edit";
        public const string Pages_Tenant_Global_Profissao_Delete = "Pages.Tenant.Global.Profissao.Delete";

        public const string Pages_Tenant_Global_Pais = "Pages.Tenant.Global.Pais";
        public const string Pages_Tenant_Global_Pais_Create = "Pages.Tenant.Global.Pais.Create";
        public const string Pages_Tenant_Global_Pais_Edit = "Pages.Tenant.Global.Pais.Edit";
        public const string Pages_Tenant_Global_Pais_Delete = "Pages.Tenant.Global.Pais.Delete";

        public const string Pages_Tenant_Global_Estado = "Pages.Tenant.Global.Estado";
        public const string Pages_Tenant_Global_Estado_Create = "Pages.Tenant.Global.Estado.Create";
        public const string Pages_Tenant_Global_Estado_Edit = "Pages.Tenant.Global.Estado.Edit";
        public const string Pages_Tenant_Global_Estado_Delete = "Pages.Tenant.Global.Estado.Delete";

        public const string Pages_Tenant_Global_Cidade = "Pages.Tenant.Global.Cidade";
        public const string Pages_Tenant_Global_Cidade_Create = "Pages.Tenant.Global.Cidade.Create";
        public const string Pages_Tenant_Global_Cidade_Edit = "Pages.Tenant.Global.Cidade.Edit";
        public const string Pages_Tenant_Global_Cidade_Delete = "Pages.Tenant.Global.Cidade.Delete";

        public const string Pages_Tenant_Global_Tratamento = "Pages.Tenant.Global.Tratamento";
        public const string Pages_Tenant_Global_Tratamento_Create = "Pages.Tenant.Global.Tratamento.Create";
        public const string Pages_Tenant_Global_Tratamento_Edit = "Pages.Tenant.Global.Tratamento.Edit";
        public const string Pages_Tenant_Global_Tratamento_Delete = "Pages.Tenant.Global.Tratamento.Delete";

        public const string Pages_Tenant_Global_GrupoPessoa = "Pages.Tenant.Global.GrupoPessoa";
        public const string Pages_Tenant_Global_GrupoPessoa_Create = "Pages.Tenant.Global.GrupoPessoa.Create";
        public const string Pages_Tenant_Global_GrupoPessoa_Edit = "Pages.Tenant.Global.GrupoPessoa.Edit";
        public const string Pages_Tenant_Global_GrupoPessoa_Delete = "Pages.Tenant.Global.GrupoPessoa.Delete";

        public const string Pages_Tenant_Global_Documento = "Pages.Tenant.Global.Documento";
        public const string Pages_Tenant_Global_Documento_Create = "Pages.Tenant.Global.Documento.Create";
        public const string Pages_Tenant_Global_Documento_Edit = "Pages.Tenant.Global.Documento.Edit";
        public const string Pages_Tenant_Global_Documento_Delete = "Pages.Tenant.Global.Documento.Delete";



        public const string Pages_Tenant_Global_Parametros = "Pages.Tenant.Global.Parametros";

        #endregion

        #region Módulo CMS

        public const string Pages_Tenant_CMS = "Pages.Tenant.CMS";
        public const string Pages_Tenant_CMS_Parametros = "Pages.Tenant.CMS.Parametros";

        public const string Pages_Tenant_CMS_Arquivo = "Pages.Tenant.CMS.Arquivo";
        public const string Pages_Tenant_CMS_Arquivo_Create = "Pages.Tenant.CMS.Arquivo.Create";
        public const string Pages_Tenant_CMS_Arquivo_Edit = "Pages.Tenant.CMS.Arquivo.Edit";
        public const string Pages_Tenant_CMS_Arquivo_Delete = "Pages.Tenant.CMS.Arquivo.Delete";

        public const string Pages_Tenant_CMS_Site = "Pages.Tenant.CMS.Site";
        public const string Pages_Tenant_CMS_Site_Create = "Pages.Tenant.CMS.Site.Create";
        public const string Pages_Tenant_CMS_Site_Edit = "Pages.Tenant.CMS.Site.Edit";
        public const string Pages_Tenant_CMS_Site_Delete = "Pages.Tenant.CMS.Site.Delete";

        public const string Pages_Tenant_CMS_Categoria = "Pages.Tenant.CMS.Categoria";
        public const string Pages_Tenant_CMS_Categoria_Create = "Pages.Tenant.CMS.Categoria.Create";
        public const string Pages_Tenant_CMS_Categoria_Edit = "Pages.Tenant.CMS.Categoria.Edit";
        public const string Pages_Tenant_CMS_Categoria_Delete = "Pages.Tenant.CMS.Categoria.Delete";

        public const string Pages_Tenant_CMS_Post = "Pages.Tenant.CMS.Post";
        public const string Pages_Tenant_CMS_Post_Create = "Pages.Tenant.CMS.Post.Create";
        public const string Pages_Tenant_CMS_Post_Edit = "Pages.Tenant.CMS.Post.Edit";
        public const string Pages_Tenant_CMS_Post_Delete = "Pages.Tenant.CMS.Post.Delete";

        public const string Pages_Tenant_CMS_Pagina = "Pages.Tenant.CMS.Pagina";
        public const string Pages_Tenant_CMS_Pagina_Create = "Pages.Tenant.CMS.Pagina.Create";
        public const string Pages_Tenant_CMS_Pagina_Edit = "Pages.Tenant.CMS.Pagina.Edit";
        public const string Pages_Tenant_CMS_Pagina_Delete = "Pages.Tenant.CMS.Pagina.Delete";

        public const string Pages_Tenant_CMS_Menu = "Pages.Tenant.CMS.Menu";
        public const string Pages_Tenant_CMS_Menu_Create = "Pages.Tenant.CMS.Menu.Create";
        public const string Pages_Tenant_CMS_Menu_Edit = "Pages.Tenant.CMS.Menu.Edit";
        public const string Pages_Tenant_CMS_Menu_Delete = "Pages.Tenant.CMS.Menu.Delete";

        public const string Pages_Tenant_CMS_ItemDeMenu = "Pages.Tenant.CMS.ItemDeMenu";
        public const string Pages_Tenant_CMS_ItemDeMenu_Create = "Pages.Tenant.CMS.ItemDeMenu.Create";
        public const string Pages_Tenant_CMS_ItemDeMenu_Edit = "Pages.Tenant.CMS.ItemDeMenu.Edit";
        public const string Pages_Tenant_CMS_ItemDeMenu_Delete = "Pages.Tenant.CMS.ItemDeMenu.Delete";

        public const string Pages_Tenant_CMS_Tag = "Pages.Tenant.CMS.Tag";
        public const string Pages_Tenant_CMS_Tag_Edit = "Pages.Tenant.CMS.Tag.Edit";
        public const string Pages_Tenant_CMS_Tag_Delete = "Pages.Tenant.CMS.Tag.Delete";

        public const string Pages_Tenant_CMS_Template = "Pages.Tenant.CMS.Template";
        public const string Pages_Tenant_CMS_Template_Synchronize = "Pages.Tenant.CMS.Template.Synchronize";

        public const string Pages_Tenant_CMS_Widget = "Pages.Tenant.CMS.Widget";
        public const string Pages_Tenant_CMS_Widget_Create = "Pages.Tenant.CMS.Widget.Create";
        public const string Pages_Tenant_CMS_Widget_Edit = "Pages.Tenant.CMS.Widget.Edit";
        public const string Pages_Tenant_CMS_Widget_Delete = "Pages.Tenant.CMS.Widget.Delete";

        #endregion

        #region Módulo CMSFrontEnd
        public const string Pages_Tenant_CMSFrontEnd = "Pages.Tenant.CMSFrontEnd";

        public const string Pages_Tenant_CMSFrontEnd_Menu = "Pages.Tenant.CMSFrontEnd.Menu";

        public const string Pages_Tenant_CMSFrontEnd_Widget = "Pages.Tenant.CMSFrontEnd.Widget";

        public const string Pages_Tenant_CMSFrontEnd_Template = "Pages.Tenant.CMSFrontEnd.Template";
        #endregion

        #region Módulo Agenda
        public const string Pages_Tenant_Agenda = "Pages.Tenant.Agenda";

        public const string Pages_Tenant_Agenda_Parametros = "Pages.Tenant.Agenda.Parametro";

        public const string Pages_Tenant_Agenda_Parametros_Padroes = "Pages.Tenant.Agenda.Parametros.Padroes";

        public const string Pages_Tenant_Agenda_Disponibilidade = "Pages.Tenant.Agenda.Disponibilidade";
        public const string Pages_Tenant_Agenda_Disponibilidade_Create = "Pages.Tenant.Agenda.Disponibilidade.Create";
        public const string Pages_Tenant_Agenda_Disponibilidade_Edit = "Pages.Tenant.Agenda.Disponibilidade.Edit";
        public const string Pages_Tenant_Agenda_Disponibilidade_Delete = "Pages.Tenant.Agenda.Disponibilidade.Delete";

        public const string Pages_Tenant_Agenda_Parametros_Regras = "Pages.Tenant.Agenda.Parametros.Regras";
        public const string Pages_Tenant_Agenda_Parametros_Regras_Create = "Pages.Tenant.Agenda.Parametros.Regras.Create";
        public const string Pages_Tenant_Agenda_Parametros_Regras_Edit = "Pages.Tenant.Agenda.Parametros.Regras.Edit";
        public const string Pages_Tenant_Agenda_Parametros_Regras_Delete = "Pages.Tenant.Agenda.Parametros.Regras.Delete";

        public const string Pages_Tenant_Agenda_Bloqueio = "Pages.Tenant.Agenda.Bloqueio";
        public const string Pages_Tenant_Agenda_Bloqueio_Create = "Pages.Tenant.Agenda.Bloqueio.Create";
        public const string Pages_Tenant_Agenda_Bloqueio_Edit = "Pages.Tenant.Agenda.Bloqueio.Edit";
        public const string Pages_Tenant_Agenda_Bloqueio_Delete = "Pages.Tenant.Agenda.Bloqueio.Delete";

        public const string Pages_Tenant_Agenda_Evento = "Pages.Tenant.Agenda.Evento";
        public const string Pages_Tenant_Agenda_Evento_Create = "Pages.Tenant.Agenda.Evento.Create";
        public const string Pages_Tenant_Agenda_Evento_Edit = "Pages.Tenant.Agenda.Evento.Edit";
        public const string Pages_Tenant_Agenda_Evento_Delete = "Pages.Tenant.Agenda.Evento.Delete";

        public const string Pages_Tenant_Agenda_Testes = "Pages.Tenant.Agenda.Testes";

        #endregion

        #region Módulo EZMedical
        public const string Pages_Tenant_EZMedical = "Pages.Tenant.EZMedical";

        public const string Pages_Tenant_EZMedical_RegraConsulta = "Pages.Tenant.EZMedical.RegraConsulta";
        public const string Pages_Tenant_EZMedical_RegraConsulta_Create = "Pages.Tenant.EZMedical.RegraConsulta.Create";
        public const string Pages_Tenant_EZMedical_RegraConsulta_Edit = "Pages.Tenant.EZMedical.RegraConsulta.Edit";
        public const string Pages_Tenant_EZMedical_RegraConsulta_Delete = "Pages.Tenant.EZMedical.RegraConsulta.Delete";

        public const string Pages_Tenant_EZMedical_Parametro = "Pages.Tenant.EZMedical.Parametro";
        public const string Pages_Tenant_EZMedical_Parametro_Edit = "Pages.Tenant.EZMedical.Parametro.Edit";

        public const string Pages_Tenant_EZMedical_Evento = "Pages.Tenant.EZMedical.Evento";
        public const string Pages_Tenant_EZMedical_Evento_Create = "Pages.Tenant.EZMedical.Evento.Create";
        public const string Pages_Tenant_EZMedical_Evento_Edit = "Pages.Tenant.EZMedical.Evento.Edit";
        public const string Pages_Tenant_EZMedical_Evento_Delete = "Pages.Tenant.EZMedical.Evento.Delete";

        public const string Pages_Tenant_EZMedical_Especialidade = "Pages.Tenant.EZMedical.Especialidade";
        public const string Pages_Tenant_EZMedical_Especialidade_Create = "Pages.Tenant.EZMedical.Especialidade.Create";
        public const string Pages_Tenant_EZMedical_Especialidade_Edit = "Pages.Tenant.EZMedical.Especialidade.Edit";
        public const string Pages_Tenant_EZMedical_Especialidade_Delete = "Pages.Tenant.EZMedical.Especialidade.Delete";

        public const string Pages_Tenant_EZMedical_Medico = "Pages.Tenant.EZMedical.Medico";
        public const string Pages_Tenant_EZMedical_Medico_Create = "Pages.Tenant.EZMedical.Medico.Create";
        public const string Pages_Tenant_EZMedical_Medico_Edit = "Pages.Tenant.EZMedical.Medico.Edit";
        public const string Pages_Tenant_EZMedical_Medico_Delete = "Pages.Tenant.EZMedical.Medico.Delete";

        public const string Pages_Tenant_EZMedical_Disponibilidade = "Pages.Tenant.EZMedical.Disponibilidade";
        public const string Pages_Tenant_EZMedical_Disponibilidade_Create = "Pages.Tenant.EZMedical.Disponibilidade.Create";
        public const string Pages_Tenant_EZMedical_Disponibilidade_Edit = "Pages.Tenant.EZMedical.Disponibilidade.Edit";
        public const string Pages_Tenant_EZMedical_Disponibilidade_Delete = "Pages.Tenant.EZMedical.Disponibilidade.Delete";

        public const string Pages_Tenant_EZMedical_Bloqueio = "Pages.Tenant.EZMedical.Bloqueio";
        public const string Pages_Tenant_EZMedical_Bloqueio_Create = "Pages.Tenant.EZMedical.Bloqueio.Create";
        public const string Pages_Tenant_EZMedical_Bloqueio_Edit = "Pages.Tenant.EZMedical.Bloqueio.Edit";
        public const string Pages_Tenant_EZMedical_Bloqueio_Delete = "Pages.Tenant.EZMedical.Bloqueio.Delete";
        #endregion

        #region Módulo Estoque
        public const string Pages_Tenant_Estoque = "Pages.Tenant.Estoque";

        public const string Pages_Tenant_Estoque_Produto = "Pages.Tenant.Estoque.Produto";
        public const string Pages_Tenant_Estoque_Produto_Create = "Pages.Tenant.Estoque.Produto.Create";
        public const string Pages_Tenant_Estoque_Produto_Edit = "Pages.Tenant.Estoque.Produto.Edit";
        public const string Pages_Tenant_Estoque_Produto_Delete = "Pages.Tenant.Estoque.Produto.Delete";

        public const string Pages_Tenant_Estoque_Parametro = "Pages.Tenant.Estoque.Parametro";
        public const string Pages_Tenant_Estoque_Parametro_Padrao = "Pages.Tenant.Estoque.Parametro.Padrao";

        public const string Pages_Tenant_Estoque_Parametro_Natureza = "Pages.Tenant.Estoque.Parametro.Natureza";
        public const string Pages_Tenant_Estoque_Parametro_Natureza_Create = "Pages.Tenant.Estoque.Parametro.Natureza.Create";
        public const string Pages_Tenant_Estoque_Parametro_Natureza_Edit = "Pages.Tenant.Estoque.Parametro.Natureza.Edit";
        public const string Pages_Tenant_Estoque_Parametro_Natureza_Delete = "Pages.Tenant.Estoque.Parametro.Natureza.Delete";

        public const string Pages_Tenant_Estoque_Parametro_Fracao = "Pages.Tenant.Estoque.Parametro.Fracao";
        public const string Pages_Tenant_Estoque_Parametro_Fracao_Create = "Pages.Tenant.Estoque.Parametro.Fracao.Create";
        public const string Pages_Tenant_Estoque_Parametro_Fracao_Edit = "Pages.Tenant.Estoque.Parametro.Fracao.Edit";
        public const string Pages_Tenant_Estoque_Parametro_Fracao_Delete = "Pages.Tenant.Estoque.Parametro.Fracao.Delete";

        public const string Pages_Tenant_Estoque_Parametro_UnidadeMedida = "Pages.Tenant.Estoque.Parametro.UnidadeMedida";
        public const string Pages_Tenant_Estoque_Parametro_UnidadeMedida_Create = "Pages.Tenant.Estoque.Parametro.UnidadeMedida.Create";
        public const string Pages_Tenant_Estoque_Parametro_UnidadeMedida_Edit = "Pages.Tenant.Estoque.Parametro.UnidadeMedida.Edit";
        public const string Pages_Tenant_Estoque_Parametro_UnidadeMedida_Delete = "Pages.Tenant.Estoque.Parametro.UnidadeMedida.Delete";

        public const string Pages_Tenant_Estoque_Parametro_LocalArmazenamento = "Pages.Tenant.Estoque.Parametro.LocalArmazenamento";
        public const string Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Create = "Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Create";
        public const string Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Edit = "Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Edit";
        public const string Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Delete = "Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Delete";

        public const string Pages_Tenant_Estoque_Entrada = "Pages.Tenant.Estoque.Entrada";
        public const string Pages_Tenant_Estoque_Entrada_Create = "Pages.Tenant.Estoque.Entrada.Create";
        public const string Pages_Tenant_Estoque_Entrada_Edit = "Pages.Tenant.Estoque.Entrada.Edit";
        public const string Pages_Tenant_Estoque_Entrada_Delete = "Pages.Tenant.Estoque.Entrada.Delete";

        public const string Pages_Tenant_Estoque_Saida = "Pages.Tenant.Estoque.Saida";
        public const string Pages_Tenant_Estoque_Saida_Create = "Pages.Tenant.Estoque.Saida.Create";
        public const string Pages_Tenant_Estoque_Saida_Edit = "Pages.Tenant.Estoque.Saida.Edit";
        public const string Pages_Tenant_Estoque_Saida_Delete = "Pages.Tenant.Estoque.Saida.Delete";

        #endregion  

        #region Módulo Vendas 
        public const string Pages_Tenant_Vendas = "Pages.Tenant.Vendas";
        public const string Pages_Tenant_Vendas_Pedido = "Pages.Tenant.Vendas.Pedido";
        public const string Pages_Tenant_Vendas_Pedido_Create = "Pages.Tenant.Vendas.Pedido.Create";
        public const string Pages_Tenant_Vendas_Pedido_Edit = "Pages.Tenant.Vendas.Pedido.Edit";
        public const string Pages_Tenant_Vendas_Pedido_Cancel = "Pages.Tenant.Vendas.Pedido.Cancel";

        #endregion

        #region Módulo Serviços 
        public const string Pages_Tenant_Servicos = "Pages.Tenant.Servicos";

        public const string Pages_Tenant_Servicos_Servico = "Pages.Tenant.Servicos.Servico";
        public const string Pages_Tenant_Servicos_Servico_Create = "Pages.Tenant.Servicos.Servico.Create";
        public const string Pages_Tenant_Servicos_Servico_Edit = "Pages.Tenant.Servicos.Servico.Edit";
        public const string Pages_Tenant_Servicos_Servico_Delete = "Pages.Tenant.Servicos.Servico.Delete";

        #endregion

        #region Módulo Financeiro 
        public const string Pages_Tenant_Financeiro = "Pages.Tenant.Financeiro";

        public const string Pages_Tenant_Financeiro_Documento = "Pages.Tenant.Financeiro.Documento";
        public const string Pages_Tenant_Financeiro_Documento_Create = "Pages.Tenant.Financeiro.Documento.Create";
        public const string Pages_Tenant_Financeiro_Documento_Edit = "Pages.Tenant.Financeiro.Documento.Edit";
        public const string Pages_Tenant_Financeiro_Documento_Delete = "Pages.Tenant.Financeiro.Documento.Delete";

        public const string Pages_Tenant_Financeiro_DocumentoAPagar = "Pages.Tenant.Financeiro.DocumentoAPagar";
        public const string Pages_Tenant_Financeiro_DocumentoAPagar_Create = "Pages.Tenant.Financeiro.DocumentoAPagar.Create";
        public const string Pages_Tenant_Financeiro_DocumentoAPagar_Edit = "Pages.Tenant.Financeiro.DocumentoAPagar.Edit";
        public const string Pages_Tenant_Financeiro_DocumentoAPagar_Estornar = "Pages.Tenant.Financeiro.DocumentoAPagar.Estornar";

        public const string Pages_Tenant_Financeiro_DocumentoAReceber = "Pages.Tenant.Financeiro.DocumentoAReceber";
        public const string Pages_Tenant_Financeiro_DocumentoAReceber_Create = "Pages.Tenant.Financeiro.DocumentoAReceber.Create";
        public const string Pages_Tenant_Financeiro_DocumentoAReceber_Edit = "Pages.Tenant.Financeiro.DocumentoAReceber.Edit";
        public const string Pages_Tenant_Financeiro_DocumentoAReceber_Estornar = "Pages.Tenant.Financeiro.DocumentoAReceber.Estornar";

        public const string Pages_Tenant_Financeiro_TipoDeDocumento = "Pages.Tenant.Financeiro.TipoDeDocumento";
        public const string Pages_Tenant_Financeiro_TipoDeDocumento_Create = "Pages.Tenant.Financeiro.TipoDeDocumento.Create";
        public const string Pages_Tenant_Financeiro_TipoDeDocumento_Edit = "Pages.Tenant.Financeiro.TipoDeDocumento.Edit";
        public const string Pages_Tenant_Financeiro_TipoDeDocumento_Delete = "Pages.Tenant.Financeiro.TipoDeDocumento.Delete";

        #endregion

        #region CRM
        public const string Pages_Tenant_CRM = "Pages.Tenant.CRM";

        public const string Pages_Tenant_CRM_Lead = "Pages.Tenant.CRM.Lead";
        public const string Pages_Tenant_CRM_Lead_Create = "Pages.Tenant.CRM.Lead.Create";
        public const string Pages_Tenant_CRM_Lead_Edit = "Pages.Tenant.CRM.Lead.Edit";
        public const string Pages_Tenant_CRM_Lead_Delete = "Pages.Tenant.CRM.Lead.Delete";
        #endregion

        #region Módulo EZLiv 
        public const string Pages_Tenant_EZLiv = "Pages.Tenant.EZLiv";

        public const string Pages_Tenant_EZLiv_Beneficiario = "Pages.Tenant.EZLiv.Beneficiario";
        public const string Pages_Tenant_EZLiv_Beneficiario_Create = "Pages.Tenant.EZLiv.Beneficiario.Create";
        public const string Pages_Tenant_EZLiv_Beneficiario_Edit = "Pages.Tenant.EZLiv.Beneficiario.Edit";
        public const string Pages_Tenant_EZLiv_Beneficiario_Delete = "Pages.Tenant.EZLiv.Beneficiario.Delete";

        public const string Pages_Tenant_EZLiv_Responsavel = "Pages.Tenant.EZLiv.Responsavel";
        public const string Pages_Tenant_EZLiv_Responsavel_Create = "Pages.Tenant.EZLiv.Responsavel.Create";
        public const string Pages_Tenant_EZLiv_Responsavel_Edit = "Pages.Tenant.EZLiv.Responsavel.Edit";
        public const string Pages_Tenant_EZLiv_Responsavel_Delete = "Pages.Tenant.EZLiv.Responsavel.Delete";

        public const string Pages_Tenant_EZLiv_ProponenteTitular = "Pages.Tenant.EZLiv.ProponenteTitular";
        public const string Pages_Tenant_EZLiv_ProponenteTitular_Create = "Pages.Tenant.EZLiv.ProponenteTitular.Create";
        public const string Pages_Tenant_EZLiv_ProponenteTitular_Edit = "Pages.Tenant.EZLiv.ProponenteTitular.Edit";
        public const string Pages_Tenant_EZLiv_ProponenteTitular_Delete = "Pages.Tenant.EZLiv.ProponenteTitular.Delete";

        public const string Pages_Tenant_EZLiv_Dependente = "Pages.Tenant.EZLiv.Dependente";
        public const string Pages_Tenant_EZLiv_Dependente_Create = "Pages.Tenant.EZLiv.Dependente.Create";
        public const string Pages_Tenant_EZLiv_Dependente_Edit = "Pages.Tenant.EZLiv.Dependente.Edit";
        public const string Pages_Tenant_EZLiv_Dependente_Delete = "Pages.Tenant.EZLiv.Dependente.Delete";

        public const string Pages_Tenant_EZLiv_DeclaracaoDeSaude = "Pages.Tenant.EZLiv.DeclaracaoDeSaude";
        public const string Pages_Tenant_EZLiv_DeclaracaoDeSaude_Create = "Pages.Tenant.EZLiv.DeclaracaoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_DeclaracaoDeSaude_Edit = "Pages.Tenant.EZLiv.DeclaracaoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_DeclaracaoDeSaude_Delete = "Pages.Tenant.EZLiv.DeclaracaoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_FaixaEtaria = "Pages.Tenant.EZLiv.FaixaEtaria";
        public const string Pages_Tenant_EZLiv_FaixaEtaria_Create = "Pages.Tenant.EZLiv.FaixaEtaria.Create";
        public const string Pages_Tenant_EZLiv_FaixaEtaria_Edit = "Pages.Tenant.EZLiv.FaixaEtaria.Edit";
        public const string Pages_Tenant_EZLiv_FaixaEtaria_Delete = "Pages.Tenant.EZLiv.FaixaEtaria.Delete";

        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDeSaude";
        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Create = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Edit = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Delete = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude = "Pages.Tenant.EZLiv.RespostaDoItemDeDeclaracaoDeSaude";
        public const string Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Create = "Pages.Tenant.EZLiv.RespostaDoItemDeDeclaracaoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Edit = "Pages.Tenant.EZLiv.RespostaDoItemDeDeclaracaoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Delete = "Pages.Tenant.EZLiv.RespostaDoItemDeDeclaracaoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_PlanoDeSaude = "Pages.Tenant.EZLiv.PlanoDeSaude";
        public const string Pages_Tenant_EZLiv_PlanoDeSaude_Create = "Pages.Tenant.EZLiv.PlanoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_PlanoDeSaude_Edit = "Pages.Tenant.EZLiv.PlanoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_PlanoDeSaude_Delete = "Pages.Tenant.EZLiv.PlanoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude = "Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude";
        public const string Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Create = "Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Edit = "Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Delete = "Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_Operadora = "Pages.Tenant.EZLiv.Operadora";
        public const string Pages_Tenant_EZLiv_Operadora_Create = "Pages.Tenant.EZLiv.Operadora.Create";
        public const string Pages_Tenant_EZLiv_Operadora_Edit = "Pages.Tenant.EZLiv.Operadora.Edit";
        public const string Pages_Tenant_EZLiv_Operadora_Delete = "Pages.Tenant.EZLiv.Operadora.Delete";

        public const string Pages_Tenant_EZLiv_Corretor = "Pages.Tenant.EZLiv.Corretor";
        public const string Pages_Tenant_EZLiv_Corretor_Create = "Pages.Tenant.EZLiv.Corretor.Create";
        public const string Pages_Tenant_EZLiv_Corretor_Edit = "Pages.Tenant.EZLiv.Corretor.Edit";
        public const string Pages_Tenant_EZLiv_Corretor_Delete = "Pages.Tenant.EZLiv.Corretor.Delete";

        public const string Pages_Tenant_EZLiv_Corretora = "Pages.Tenant.EZLiv.Corretora";
        public const string Pages_Tenant_EZLiv_Corretora_Create = "Pages.Tenant.EZLiv.Corretora.Create";
        public const string Pages_Tenant_EZLiv_Corretora_Edit = "Pages.Tenant.EZLiv.Corretora.Edit";
        public const string Pages_Tenant_EZLiv_Corretora_Delete = "Pages.Tenant.EZLiv.Corretora.Delete";

        public const string Pages_Tenant_EZLiv_Associacao = "Pages.Tenant.EZLiv.Associacao";
        public const string Pages_Tenant_EZLiv_Associacao_Create = "Pages.Tenant.EZLiv.Associacao.Create";
        public const string Pages_Tenant_EZLiv_Associacao_Edit = "Pages.Tenant.EZLiv.Associacao.Edit";
        public const string Pages_Tenant_EZLiv_Associacao_Delete = "Pages.Tenant.EZLiv.Associacao.Delete";

        public const string Pages_Tenant_EZLiv_Administradora = "Pages.Tenant.EZLiv.Administradora";
        public const string Pages_Tenant_EZLiv_Administradora_Create = "Pages.Tenant.EZLiv.Administradora.Create";
        public const string Pages_Tenant_EZLiv_Administradora_Edit = "Pages.Tenant.EZLiv.Administradora.Edit";
        public const string Pages_Tenant_EZLiv_Administradora_Delete = "Pages.Tenant.EZLiv.Administradora.Delete";

        public const string Pages_Tenant_EZLiv_Contrato = "Pages.Tenant.EZLiv.Contrato";
        public const string Pages_Tenant_EZLiv_Contrato_Create = "Pages.Tenant.EZLiv.Contrato.Create";
        public const string Pages_Tenant_EZLiv_Contrato_Edit = "Pages.Tenant.EZLiv.Contrato.Edit";
        public const string Pages_Tenant_EZLiv_Contrato_Delete = "Pages.Tenant.EZLiv.Contrato.Delete";

        public const string Pages_Tenant_EZLiv_EspecialidadeSync = "Pages.Tenant.EZLiv.EspecialidadeSync";
        public const string Pages_Tenant_EZLiv_EspecialidadeSync_Get = "Pages.Tenant.EZLiv.EspecialidadeSync.Get";
        public const string Pages_Tenant_EZLiv_EspecialidadeSync_Create = "Pages.Tenant.EZLiv.EspecialidadeSync.Create";
        public const string Pages_Tenant_EZLiv_EspecialidadeSync_Edit = "Pages.Tenant.EZLiv.EspecialidadeSync.Edit";
        public const string Pages_Tenant_EZLiv_EspecialidadeSync_Delete = "Pages.Tenant.EZLiv.EspecialidadeSync.Delete";

        public const string Pages_Tenant_EZLiv_ContratoHistorico = "Pages.Tenant.EZLiv.ContratoHistorico";
        public const string Pages_Tenant_EZLiv_ContratoHistorico_Create = "Pages.Tenant.EZLiv.ContratoHistorico.Create";
        public const string Pages_Tenant_EZLiv_ContratoHistorico_Edit = "Pages.Tenant.EZLiv.ContratoHistorico.Edit";
        public const string Pages_Tenant_EZLiv_ContratoHistorico_Delete = "Pages.Tenant.EZLiv.ContratoHistorico.Delete";

        public const string Pages_Tenant_EZLiv_PropostaDeContratacao = "Pages.Tenant.EZLiv.PropostaDeContratacao";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Create = "Pages.Tenant.EZLiv.PropostaDeContratacao.Create";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Edit = "Pages.Tenant.EZLiv.PropostaDeContratacao.Edit";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Delete = "Pages.Tenant.EZLiv.PropostaDeContratacao.Delete";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_PreCadastro = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.PreCadastro";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_SelecionarPlano = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.SelecionarPlano";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_PreenchimentoDosDados = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.PreenchimentoDosDados";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_EnvioDeDocumentos = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.EnvioDeDocumentos";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_AceiteDoContrato = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.AceiteDoContrato";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_Homologacao = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.Homologacao";
        public const string Pages_Tenant_EZLiv_PropostaDeContratacao_Passo_Pagamento = "Pages.Administration.Tenant.UsuarioPorPessoa.Passo.Pagamento";

        public const string Pages_Tenant_EZLiv_ParametroEZLiv = "Pages.Tenant.EZLiv.Parametro";

        public const string Pages_Tenant_EZLiv_ClienteEZLiv = "Pages.Tenant.EZLiv.ClienteEZLiv";
        public const string Pages_Tenant_EZLiv_ClienteEZLiv_Create = "Pages.Tenant.EZLiv.ClienteEZLiv.Create";
        public const string Pages_Tenant_EZLiv_ClienteEZLiv_Edit = "Pages.Tenant.EZLiv.ClienteEZLiv.Edit";
        public const string Pages_Tenant_EZLiv_ClienteEZLiv_Delete = "Pages.Tenant.EZLiv.ClienteEZLiv.Delete";

        public const string Pages_Tenant_EZLiv_ValorPorFaixaEtaria = "Pages.Tenant.EZLiv.ValorPorFaixaEtaria";
        public const string Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Create = "Pages.Tenant.EZLiv.ValorPorFaixaEtaria.Create";
        public const string Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Edit = "Pages.Tenant.EZLiv.ValorPorFaixaEtaria.Edit";
        public const string Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Delete = "Pages.Tenant.EZLiv.ValorPorFaixaEtaria.Delete";

        public const string Pages_Tenant_EZLiv_RedeCredenciada = "Pages.Tenant.EZLiv.RedeCredenciada";
        public const string Pages_Tenant_EZLiv_RedeCredenciada_Create = "Pages.Tenant.EZLiv.RedeCredenciada.Create";
        public const string Pages_Tenant_EZLiv_RedeCredenciada_Edit = "Pages.Tenant.EZLiv.RedeCredenciada.Edit";
        public const string Pages_Tenant_EZLiv_RedeCredenciada_Delete = "Pages.Tenant.EZLiv.RedeCredenciada.Delete";

        public const string Pages_Tenant_EZLiv_ItemDeRedeCredenciada = "Pages.Tenant.EZLiv.ItemDeRedeCredenciada";
        public const string Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Create = "Pages.Tenant.EZLiv.ItemDeRedeCredenciada.Create";
        public const string Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Edit = "Pages.Tenant.EZLiv.ItemDeRedeCredenciada.Edit";
        public const string Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Delete = "Pages.Tenant.EZLiv.ItemDeRedeCredenciada.Delete";

        public const string Pages_Tenant_EZLiv_Laboratorio = "Pages.Tenant.EZLiv.Laboratorio";
        public const string Pages_Tenant_EZLiv_Laboratorio_Create = "Pages.Tenant.EZLiv.Laboratorio.Create";
        public const string Pages_Tenant_EZLiv_Laboratorio_Edit = "Pages.Tenant.EZLiv.Laboratorio.Edit";
        public const string Pages_Tenant_EZLiv_Laboratorio_Delete = "Pages.Tenant.EZLiv.Laboratorio.Delete";

        public const string Pages_Tenant_EZLiv_Hospital = "Pages.Tenant.EZLiv.Hospital";
        public const string Pages_Tenant_EZLiv_Hospital_Create = "Pages.Tenant.EZLiv.Hospital.Create";
        public const string Pages_Tenant_EZLiv_Hospital_Edit = "Pages.Tenant.EZLiv.Hospital.Edit";
        public const string Pages_Tenant_EZLiv_Hospital_Delete = "Pages.Tenant.EZLiv.Hospital.Delete";

        public const string Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude = "Pages.Tenant.EZLiv.EspecialidadePorProdutoDePlanoDeSaude";
        public const string Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Create = "Pages.Tenant.EZLiv.EspecialidadePorProdutoDePlanoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Edit = "Pages.Tenant.EZLiv.EspecialidadePorProdutoDePlanoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Delete = "Pages.Tenant.EZLiv.EspecialidadePorProdutoDePlanoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_CorretorCorretora = "Pages.Tenant.EZLiv.CorretorCorretora";
        public const string Pages_Tenant_EZLiv_CorretorCorretora_Create = "Pages.Tenant.EZLiv.CorretorCorretora.Create";
        public const string Pages_Tenant_EZLiv_CorretorCorretora_Edit = "Pages.Tenant.EZLiv.CorretorCorretora.Edit";
        public const string Pages_Tenant_EZLiv_CorretorCorretora_Delete = "Pages.Tenant.EZLiv.CorretorCorretora.Delete";

        public const string Pages_Tenant_EZLiv_Relatorio = "Pages.Tenant.EZLiv.Relatorio";
        public const string Pages_Tenant_EZLiv_Relatorio_Create = "Pages.Tenant.EZLiv.Relatorio.Create";
        public const string Pages_Tenant_EZLiv_Relatorio_Edit = "Pages.Tenant.EZLiv.Relatorio.Edit";
        public const string Pages_Tenant_EZLiv_Relatorio_Delete = "Pages.Tenant.EZLiv.Relatorio.Delete";

        public const string Pages_Tenant_EZLiv_Chancela = "Pages.Tenant.EZLiv.Chancela";
        public const string Pages_Tenant_EZLiv_Chancela_Create = "Pages.Tenant.EZLiv.Chancela.Create";
        public const string Pages_Tenant_EZLiv_Chancela_Edit = "Pages.Tenant.EZLiv.Chancela.Edit";
        public const string Pages_Tenant_EZLiv_Chancela_Delete = "Pages.Tenant.EZLiv.Chancela.Delete";

        public const string Pages_Tenant_EZLiv_Vigencia = "Pages.Tenant.EZLiv.Vigencia";
        public const string Pages_Tenant_EZLiv_Vigencia_Create = "Pages.Tenant.EZLiv.Vigencia.Create";
        public const string Pages_Tenant_EZLiv_Vigencia_Edit = "Pages.Tenant.EZLiv.Vigencia.Edit";
        public const string Pages_Tenant_EZLiv_Vigencia_Delete = "Pages.Tenant.EZLiv.Vigencia.Delete";

        public const string Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude = "Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude";
        public const string Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Create = "Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Create";
        public const string Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit = "Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Edit";
        public const string Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Delete = "Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Delete";

        public const string Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria = "Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria";
        public const string Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Create = "Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Create";
        public const string Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Edit = "Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Edit";
        public const string Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Delete = "Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Delete";

        public const string Pages_Tenant_EZLiv_Aditivo = "Pages.Tenant.EZLiv.Aditivo";
        public const string Pages_Tenant_EZLiv_Aditivo_Create = "Pages.Tenant.EZLiv.Aditivo.Create";
        public const string Pages_Tenant_EZLiv_Aditivo_Edit = "Pages.Tenant.EZLiv.Aditivo.Edit";
        public const string Pages_Tenant_EZLiv_Aditivo_Delete = "Pages.Tenant.EZLiv.Aditivo.Delete";

        public const string Pages_Tenant_EZLiv_MeusProdutos = "Pages.Tenant.EZLiv.MeusProdutos";

        public const string Pages_Tenant_EZLiv_MeusProcessos = "Pages.Tenant.EZLiv.MeusProcessos";

        public const string Pages_Tenant_EZLiv_Carencia = "Pages.Tenant.EZLiv.Carencia";
        public const string Pages_Tenant_EZLiv_Carencia_Create = "Pages.Tenant.EZLiv.Carencia.Create";
        public const string Pages_Tenant_EZLiv_Carencia_Edit = "Pages.Tenant.EZLiv.Carencia.Edit";
        public const string Pages_Tenant_EZLiv_Carencia_Delete = "Pages.Tenant.EZLiv.Carencia.Delete";

        public const string Pages_Tenant_EZLiv_GerenteCorretora = "Pages.Tenant.EZLiv.GerenteCorretora";
        public const string Pages_Tenant_EZLiv_GerenteCorretora_Create = "Pages.Tenant.EZLiv.GerenteCorretora.Create";
        public const string Pages_Tenant_EZLiv_GerenteCorretora_Edit = "Pages.Tenant.EZLiv.GerenteCorretora.Edit";
        public const string Pages_Tenant_EZLiv_GerenteCorretora_Delete = "Pages.Tenant.EZLiv.GerenteCorretora.Delete";

        public const string Pages_Tenant_EZLiv_SupervisorCorretora = "Pages.Tenant.EZLiv.SupervisorCorretora";
        public const string Pages_Tenant_EZLiv_SupervisorCorretora_Create = "Pages.Tenant.EZLiv.SupervisorCorretora.Create";
        public const string Pages_Tenant_EZLiv_SupervisorCorretora_Edit = "Pages.Tenant.EZLiv.SupervisorCorretora.Edit";
        public const string Pages_Tenant_EZLiv_SupervisorCorretora_Delete = "Pages.Tenant.EZLiv.SupervisorCorretora.Delete";

        public const string Pages_Tenant_EZLiv_GerenteAdministradora = "Pages.Tenant.EZLiv.GerenteAdministradora";
        public const string Pages_Tenant_EZLiv_GerenteAdministradora_Create = "Pages.Tenant.EZLiv.GerenteAdministradora.Create";
        public const string Pages_Tenant_EZLiv_GerenteAdministradora_Edit = "Pages.Tenant.EZLiv.GerenteAdministradora.Edit";
        public const string Pages_Tenant_EZLiv_GerenteAdministradora_Delete = "Pages.Tenant.EZLiv.GerenteAdministradora.Delete";

        public const string Pages_Tenant_EZLiv_SupervisorAdministradora = "Pages.Tenant.EZLiv.SupervisorAdministradora";
        public const string Pages_Tenant_EZLiv_SupervisorAdministradora_Create = "Pages.Tenant.EZLiv.SupervisorAdministradora.Create";
        public const string Pages_Tenant_EZLiv_SupervisorAdministradora_Edit = "Pages.Tenant.EZLiv.SupervisorAdministradora.Edit";
        public const string Pages_Tenant_EZLiv_SupervisorAdministradora_Delete = "Pages.Tenant.EZLiv.SupervisorAdministradora.Delete";

        public const string Pages_Tenant_EZLiv_HomologadorAdministradora = "Pages.Tenant.EZLiv.HomologadorAdministradora";
        public const string Pages_Tenant_EZLiv_HomologadorAdministradora_Create = "Pages.Tenant.EZLiv.HomologadorAdministradora.Create";
        public const string Pages_Tenant_EZLiv_HomologadorAdministradora_Edit = "Pages.Tenant.EZLiv.HomologadorAdministradora.Edit";
        public const string Pages_Tenant_EZLiv_HomologadorAdministradora_Delete = "Pages.Tenant.EZLiv.HomologadorAdministradora.Delete";

        public const string Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario = "Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario";
        public const string Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Create = "Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Create";
        public const string Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Edit = "Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Edit";
        public const string Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Delete = "Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Delete";

        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDoBeneficiario";
        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Create = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDoBeneficiario.Create";
        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Edit = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDoBeneficiario.Edit";
        public const string Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Delete = "Pages.Tenant.EZLiv.ItemDeDeclaracaoDoBeneficiario.Delete";

        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora";
        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Create";
        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Edit";
        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Delete = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Delete";

        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor";
        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Create";
        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Edit";
        public const string Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Delete = "Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Delete";

        #endregion

        #region Módulo EZPag
        public const string Pages_Tenant_EZPag = "Pages.Tenant.EZPag";
        public const string Pages_Tenant_EZPag_Testes = "Pages.Tenant.EZPag.Testes";
        public const string Pages_Tenant_EZPag_ParametroEzpag = "Pages.Tenant.EZPag.Parametro";

        public const string Pages_Tenant_EZPag_Transacao = "Pages.Tenant.EZPag.Transacao";

        public const string Pages_Tenant_EZPag_FormaDePagamento = "Pages.Tenant.EZPag.FormaDePagamento";
        public const string Pages_Tenant_EZPag_FormaDePagamento_Create = "Pages.Tenant.EZPag.FormaDePagamento.Create";
        public const string Pages_Tenant_EZPag_FormaDePagamento_Edit = "Pages.Tenant.EZPag.FormaDePagamento.Edit";
        public const string Pages_Tenant_EZPag_FormaDePagamento_Delete = "Pages.Tenant.EZPag.FormaDePagamento.Delete";

        #endregion

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
    }
}