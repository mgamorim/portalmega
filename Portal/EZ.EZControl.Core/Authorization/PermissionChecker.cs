﻿using Abp.Authorization;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.MultiTenancy;

namespace EZ.EZControl.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}