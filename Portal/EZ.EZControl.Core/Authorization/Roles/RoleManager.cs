﻿using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Caching;
using Abp.Zero.Configuration;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.MultiEmpresa;
using EZ.EZControl.Services.Global.Permissao.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Authorization.Roles
{
    internal class PermissionEqualityComparer : IEqualityComparer<Permission>
    {
        public static PermissionEqualityComparer Instance { get { return _instance; } }
        private static PermissionEqualityComparer _instance = new PermissionEqualityComparer();

        public bool Equals(Permission x, Permission y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null || y == null)
            {
                return false;
            }

            return Equals(x.Name, y.Name);
        }

        public int GetHashCode(Permission permission)
        {
            return permission.Name.GetHashCode();
        }
    }

    /// <summary>
    /// Role manager.
    /// Used to implement domain logic for roles.
    /// </summary>
    public class RoleManager : AbpRoleManager<Role, User>
    {
        private readonly IMultiEmpresa _multiEmpresa;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ICacheManager _cacheManager;
        private readonly IPermissionManager _permissionManager;
        private readonly IPermissaoEmpresaPorRoleService _permissaoEmpresaPorRoleService;
        private readonly IRepository<Role, int> _repository;

        public RoleManager(
            RoleStore store,
            IPermissionManager permissionManager,
            IRoleManagementConfig roleManagementConfig,
            ICacheManager cacheManager,
            IUnitOfWorkManager unitOfWorkManager,
            IMultiEmpresa multiEmpresa,
            IPermissaoEmpresaPorRoleService permissaoEmpresaPorRoleService,
            IRepository<Role, int> repository)
            : base(
                store,
                permissionManager,
                roleManagementConfig,
                cacheManager,
                unitOfWorkManager)
        {
            _permissionManager = permissionManager;
            _multiEmpresa = multiEmpresa;
            _permissaoEmpresaPorRoleService = permissaoEmpresaPorRoleService;
            _unitOfWorkManager = unitOfWorkManager;
            _cacheManager = cacheManager;
            _repository = repository;
        }

        private IRolePermissionStore<Role, User> RolePermissionStore
        {
            get
            {
                if (!(Store is IRolePermissionStore<Role, User>))
                {
                    throw new AbpException("Store is not IRolePermissionStore");
                }

                return Store as IRolePermissionStore<Role, User>;
            }
        }

        public override async Task<bool> IsGrantedAsync(int roleId, Permission permission)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.IsGrantedAsync(roleId, permission);
            }

            var cacheItem = this.GetRolePermissionCacheItem(roleId);

            var isGranted = cacheItem.GrantedPermissions.Contains(permission.Name);

            return isGranted;
        }

        public override async Task<IReadOnlyList<Permission>> GetGrantedPermissionsAsync(Role role)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.GetGrantedPermissionsAsync(role);
            }

            var permissionList = new List<Permission>();

            foreach (var permission in _permissionManager.GetAllPermissions())
            {
                if (await this.IsGrantedAsync(role.Id, permission))
                {
                    permissionList.Add(permission);
                }
            }

            return permissionList;
        }

        public override async Task SetGrantedPermissionsAsync(Role role, IEnumerable<Permission> permissions)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.SetGrantedPermissionsAsync(role, permissions);
            }

            await this.SetGrantedPermissionsCustomAsync(role, permissions);
        }

        public async Task<List<Role>> GetAllList(string[] roles)
        {
            var rolesEntity = await _repository.GetAllListAsync(x => roles.Contains(x.Name));

            return rolesEntity;
        }

        private RolePermissionCacheItem GetRolePermissionCacheItem(int roleId)
        {
            var cacheKey = roleId + "@" + (GetCurrentTenantId() ?? 0) + "@" + Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

            return _cacheManager.GetCache(PermissaoEmpresaPorRole.CacheStoreName).Get(cacheKey, () =>
            {
                var newCacheItem = new RolePermissionCacheItem(roleId);

                var permissiosInfo = this.GetPermissions(roleId);

                foreach (var permissionInfo in permissiosInfo)
                {
                    if (permissionInfo.IsGranted)
                    {
                        newCacheItem.GrantedPermissions.Add(permissionInfo.Name.Trim());
                    }
                }

                return newCacheItem;
            });
        }

        private int? GetCurrentTenantId()
        {
            if (_unitOfWorkManager.Current != null)
            {
                return _unitOfWorkManager.Current.GetTenantId();
            }

            return AbpSession.TenantId;
        }

        private IList<PermissionGrantInfo> GetPermissions(int roleId)
        {
            var tenantId = GetCurrentTenantId();
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var list = _permissaoEmpresaPorRoleService.GetAll()
                .Where(x =>
                    x.RoleId == roleId &&
                    x.TenantId == tenantId &&
                    x.Empresa.Id == empresaId)
                .ToList()
                .Select(x => new PermissionGrantInfo(x.PermissionName, x.IsGranted))
                .ToList();

            return list;
        }

        private async Task SetGrantedPermissionsCustomAsync(Role role, IEnumerable<Permission> permissions)
        {
            var oldPermissions = await this.GetGrantedPermissionsAsync(role);
            var newPermissions = permissions.ToArray();

            foreach (var permission in oldPermissions.Where(p => !newPermissions.Contains(p, PermissionEqualityComparer.Instance)))
            {
                await base.ProhibitPermissionAsync(role, permission);
            }

            foreach (var permission in newPermissions.Where(p => !oldPermissions.Contains(p, PermissionEqualityComparer.Instance)))
            {
                await base.GrantPermissionAsync(role, permission);
            }
        }
    }
}