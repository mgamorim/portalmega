﻿namespace EZ.EZControl.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string Admin = "Admin";
        }

        public static class Tenants
        {
            public const string Admin = "Admin";

            public const string User = "User";

            public const string Beneficiario = "Beneficiario";

            public const string Corretor = "Corretor";

            public const string Corretora = "Corretora";

            public const string Administradora = "Administradora";

            public const string Associacao = "Associacao";

            public const string Operadora = "Operadora";
        }
    }
}