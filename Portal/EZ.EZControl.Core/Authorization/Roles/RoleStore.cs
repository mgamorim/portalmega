﻿using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.MultiEmpresa;
using EZ.EZControl.Services.Global.Permissao.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Authorization.Roles
{
    public class RoleStore : AbpRoleStore<Role, User>
    {
        private readonly IMultiEmpresa _multiEmpresa;
        private readonly IPermissaoEmpresaPorRoleService _permissaoEmpresaPorRoleService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IAbpSession _abpSession;

        public RoleStore(
            IRepository<Role> roleRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<RolePermissionSetting, long> rolePermissionSettingRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IAbpSession abpSession,
            IMultiEmpresa multiEmpresa,
            IPermissaoEmpresaPorRoleService permissaoEmpresaPorRoleService)
            : base(
                roleRepository,
                userRoleRepository,
                rolePermissionSettingRepository)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _abpSession = abpSession;
            _multiEmpresa = multiEmpresa;
            _permissaoEmpresaPorRoleService = permissaoEmpresaPorRoleService;
        }

        private int? GetCurrentTenantId()
        {
            if (_unitOfWorkManager.Current != null)
            {
                return _unitOfWorkManager.Current.GetTenantId();
            }

            return _abpSession.TenantId;
        }

        public override async Task<IList<PermissionGrantInfo>> GetPermissionsAsync(Role role)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.GetPermissionsAsync(role);
            }

            return this.GetPermissions(role.Id);
        }

        private IList<PermissionGrantInfo> GetPermissions(int roleId)
        {
            var tenantId = GetCurrentTenantId();
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var list = _permissaoEmpresaPorRoleService.GetAll()
                .Where(x =>
                    x.RoleId == roleId &&
                    x.TenantId == tenantId &&
                    x.Empresa.Id == empresaId)
                .Select(x => new PermissionGrantInfo(x.PermissionName, x.IsGranted))
                .ToList();

            return list;
        }

        public override async Task<bool> HasPermissionAsync(int roleId, PermissionGrantInfo permissionGrant)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.HasPermissionAsync(roleId, permissionGrant);
            }

            var empresaId = _multiEmpresa.EmpresaLogada().Id;
            bool hasPermission = _permissaoEmpresaPorRoleService.GetAll().FirstOrDefault(
                p => p.RoleId == roleId &&
                     p.PermissionName == permissionGrant.Name &&
                     p.IsGranted == permissionGrant.IsGranted &&
                     p.Empresa.Id == empresaId
                ) != null;

            return hasPermission;
        }

        public override async Task AddPermissionAsync(Role role, PermissionGrantInfo permissionGrant)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.AddPermissionAsync(role, permissionGrant);
            }
            else
            {
                if (await HasPermissionAsync(role.Id, permissionGrant))
                {
                    return;
                }

                await _permissaoEmpresaPorRoleService.CreateEntity(
                    new PermissaoEmpresaPorRole()
                    {
                        TenantId = role.TenantId,
                        RoleId = role.Id,
                        PermissionName = permissionGrant.Name,
                        IsGranted = permissionGrant.IsGranted,
                        Empresa = _multiEmpresa.EmpresaLogada()
                    });
            }
        }

        public override async Task RemovePermissionAsync(Role role, PermissionGrantInfo permissionGrant)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.RemovePermissionAsync(role, permissionGrant);
            }
            else
            {
                var tenantId = GetCurrentTenantId();
                var empresaId = _multiEmpresa.EmpresaLogada().Id;

                var permissaoUsuarioPorEmpresa = _permissaoEmpresaPorRoleService.GetAll()
                    .FirstOrDefault(x =>
                        x.RoleId == role.Id &&
                        x.PermissionName == permissionGrant.Name &&
                        x.IsGranted == permissionGrant.IsGranted &&
                        x.Empresa.Id == empresaId &&
                        x.TenantId == tenantId
                );

                if (permissaoUsuarioPorEmpresa != null)
                    await _permissaoEmpresaPorRoleService.Delete(permissaoUsuarioPorEmpresa.Id);
            }
        }
    }
}