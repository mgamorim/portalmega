﻿using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.MultiEmpresa;
using EZ.EZControl.Services.Global.Permissao.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Authorization.Users
{
    /// <summary>
    /// Used to perform database operations for <see cref="UserManager"/>.
    /// </summary>
    public class UserStore : AbpUserStore<Role, User>
    {
        private readonly IMultiEmpresa _multiEmpresa;
        private readonly IAbpSession _abpSession;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IPermissaoEmpresaPorUsuarioService _permissaoEmpresaPorUsuarioService;

        public UserStore(
            IRepository<User, long> userRepository,
            IRepository<UserLogin, long> userLoginRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<Role> roleRepository,
            IRepository<UserPermissionSetting, long> userPermissionSettingRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<UserClaim, long> userCliamRepository,
            IMultiEmpresa multiEmpresa,
            IAbpSession abpSession,
            IPermissaoEmpresaPorUsuarioService permissaoEmpresaPorUsuarioService)
            : base(
                userRepository,
                userLoginRepository,
                userRoleRepository,
                roleRepository,
                userPermissionSettingRepository,
                unitOfWorkManager,
                userCliamRepository)
        {
            _multiEmpresa = multiEmpresa;
            _abpSession = abpSession;
            _permissaoEmpresaPorUsuarioService = permissaoEmpresaPorUsuarioService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public override async Task<IList<PermissionGrantInfo>> GetPermissionsAsync(long userId)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.GetPermissionsAsync(userId);
            }

            return this.GetPermissions(userId);
        }

        public override async Task<bool> HasPermissionAsync(long userId, PermissionGrantInfo permissionGrant)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.HasPermissionAsync(userId, permissionGrant);
            }

            var empresaId = _multiEmpresa.EmpresaLogada().Id;
            bool hasPermission = _permissaoEmpresaPorUsuarioService.GetAll().FirstOrDefault(
                                     p => p.UserId == userId &&
                                          p.PermissionName == permissionGrant.Name &&
                                          p.IsGranted == permissionGrant.IsGranted &&
                                          p.Empresa.Id == empresaId
                                 ) != null;

            return hasPermission;
        }

        public override async Task AddPermissionAsync(User user, PermissionGrantInfo permissionGrant)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.AddPermissionAsync(user, permissionGrant);
            }

            if (await this.HasPermissionAsync(user.Id, permissionGrant))
            {
                return;
            }

            await _permissaoEmpresaPorUsuarioService.CreateEntity(
                new PermissaoEmpresaPorUsuario()
                {
                    TenantId = user.TenantId,
                    UserId = user.Id,
                    PermissionName = permissionGrant.Name,
                    IsGranted = permissionGrant.IsGranted,
                    Empresa = _multiEmpresa.EmpresaLogada()
                });
        }

        public override async Task RemovePermissionAsync(User user, PermissionGrantInfo permissionGrant)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.RemovePermissionAsync(user, permissionGrant);
            }
            else
            {
                var tenantId = GetCurrentTenantId();
                var empresaId = _multiEmpresa.EmpresaLogada().Id;

                var permissaoUsuarioPorEmpresa = _permissaoEmpresaPorUsuarioService.GetAll()
                    .FirstOrDefault(x =>
                            x.UserId == user.Id &&
                            x.PermissionName == permissionGrant.Name &&
                            x.IsGranted == permissionGrant.IsGranted &&
                            x.TenantId == tenantId &&
                            x.Empresa.Id == empresaId
                    );

                if (permissaoUsuarioPorEmpresa != null)
                {
                    await _permissaoEmpresaPorUsuarioService.Delete(permissaoUsuarioPorEmpresa.Id);
                }
            }
        }

        public override async Task RemoveAllPermissionSettingsAsync(User user)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.RemoveAllPermissionSettingsAsync(user);
            }

            await _permissaoEmpresaPorUsuarioService.Delete(x => x.UserId == user.Id);
        }

        private IList<PermissionGrantInfo> GetPermissions(long userId)
        {
            var tenantId = GetCurrentTenantId();
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var list = _permissaoEmpresaPorUsuarioService.GetAll()
                .Where(x =>
                    x.UserId == userId &&
                    x.TenantId == tenantId &&
                    x.Empresa.Id == empresaId)
                .ToList()
                .Select(x => new PermissionGrantInfo(x.PermissionName, x.IsGranted))
                .ToList();

            return list;
        }

        private int? GetCurrentTenantId()
        {
            if (_unitOfWorkManager.Current != null)
            {
                return _unitOfWorkManager.Current.GetTenantId();
            }

            return _abpSession.TenantId;
        }
    }
}