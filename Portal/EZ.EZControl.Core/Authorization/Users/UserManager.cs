﻿using Abp;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.Organizations;
using Abp.Runtime.Caching;
using Abp.Threading;
using Abp.UI;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Identity;
using EZ.EZControl.MultiEmpresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Authorization.Users
{
    /// <summary>
    /// User manager.
    /// Used to implement domain logic for users.
    /// Extends <see cref="AbpUserManager{TRole,TUser}"/>.
    /// </summary>
    public class UserManager : AbpUserManager<Role, User>
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IMultiEmpresa _multiEmpresa;
        private readonly IPermissionManager _permissionManager;
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<User, long> _repositoryUser;

        public UserManager(
            UserStore userStore,
            RoleManager roleManager,
            IPermissionManager permissionManager,
            IUnitOfWorkManager unitOfWorkManager,
            ICacheManager cacheManager,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IOrganizationUnitSettings organizationUnitSettings,
            IdentityEmailMessageService emailService,
            ILocalizationManager localizationManager,
            ISettingManager settingManager,
            IdentitySmsMessageService smsService,
            IUserTokenProviderAccessor userTokenProviderAccessor,
            IMultiEmpresa multiEmpresa,
            IRepository<User, long> repositoryUser)
            : base(
                  userStore,
                  roleManager,
                  permissionManager,
                  unitOfWorkManager,
                  cacheManager,
                  organizationUnitRepository,
                  userOrganizationUnitRepository,
                  organizationUnitSettings,
                  localizationManager,
                  emailService,
                  settingManager,
                  userTokenProviderAccessor)
        {
            _unitOfWorkManager = unitOfWorkManager;
            _multiEmpresa = multiEmpresa;
            _permissionManager = permissionManager;
            _cacheManager = cacheManager;
            _repositoryUser = repositoryUser;

            SmsService = smsService;
        }

        public async Task<User> GetUserOrNullAsync(UserIdentifier userIdentifier)
        {
            using (_unitOfWorkManager.Begin())
            {
                using (_unitOfWorkManager.Current.SetTenantId(userIdentifier.TenantId))
                {
                    return await FindByIdAsync(userIdentifier.UserId);
                }
            }
        }

        public User GetUserOrNull(UserIdentifier userIdentifier)
        {
            return AsyncHelper.RunSync(() => GetUserOrNullAsync(userIdentifier));
        }

        public async Task<User> GetUserAsync(UserIdentifier userIdentifier)
        {
            var user = await GetUserOrNullAsync(userIdentifier);
            if (user == null)
            {
                throw new ApplicationException("There is no user: " + userIdentifier);
            }

            return user;
        }

        public User GetUser(UserIdentifier userIdentifier)
        {
            return AsyncHelper.RunSync(() => GetUserAsync(userIdentifier));
        }

        public override async Task<bool> IsGrantedAsync(long userId, Permission permission)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.IsGrantedAsync(userId, permission);
            }

            //Check for multi-tenancy side
            if (!permission.MultiTenancySides.HasFlag(AbpSession.MultiTenancySide))
            {
                return false;
            }

            //Check for depended features
            if (permission.FeatureDependency != null && AbpSession.MultiTenancySide == MultiTenancySides.Tenant)
            {
                if (!await permission.FeatureDependency.IsSatisfiedAsync(FeatureDependencyContext))
                {
                    return false;
                }
            }

            //Get cached user permissions
            var cacheItem = await GetUserPermissionCacheItemCustomAsync(userId);

            //Check for user-specific value
            if (cacheItem.GrantedPermissions.Contains(permission.Name))
            {
                return true;
            }

            if (cacheItem.ProhibitedPermissions.Contains(permission.Name))
            {
                return false;
            }

            //Check for roles
            foreach (var roleId in cacheItem.RoleIds)
            {
                if (await RoleManager.IsGrantedAsync(roleId, permission))
                {
                    return true;
                }
            }

            return false;
        }

        public override async Task<IReadOnlyList<Permission>> GetGrantedPermissionsAsync(User user)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                return await base.GetGrantedPermissionsAsync(user);
            }

            var permissionList = new List<Permission>();

            foreach (var permission in _permissionManager.GetAllPermissions())
            {
                if (await this.IsGrantedAsync(user.Id, permission))
                {
                    permissionList.Add(permission);
                }
            }

            return permissionList;
        }

        public override async Task SetGrantedPermissionsAsync(User user, IEnumerable<Permission> permissions)
        {
            if (!_multiEmpresa.IsMultiEmpresa)
            {
                await base.SetGrantedPermissionsAsync(user, permissions);
            }

            await this.SetGrantedPermissionsCustomAsync(user, permissions);
        }

        public async Task<User> CreatePorPessoaFisica(PessoaFisica pessoaFisica, TipoDeUsuarioEnum tipoDeUsuario,
            string[] roleNames = null, string userName = "")
        {
            if (pessoaFisica.EmailPrincipal == null)
            {
                throw new UserFriendlyException("A pessoa selecionada não possui e-mail particular cadastrado.");
            }

            if (string.IsNullOrWhiteSpace(pessoaFisica.Nome)
                || (!string.IsNullOrWhiteSpace(pessoaFisica.Nome) && pessoaFisica.Nome.Split(' ').ToList().Count <= 1))
            {
                throw new UserFriendlyException("o nome e sobrenome não estão preenchidos corretamente");
            }

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                User userCkeck = _repositoryUser.FirstOrDefault(x => x.UserName == userName
                    || x.UserName == pessoaFisica.EmailPrincipal.Endereco);
                if (userCkeck != null)
                {
                    throw new UserFriendlyException(string.Format("Já existe um usuário com o nome {0}-{1}.", userName, pessoaFisica.EmailPrincipal.Endereco));
                }

                userCkeck = _repositoryUser.FirstOrDefault(x => x.EmailAddress == pessoaFisica.EmailPrincipal.Endereco);
                if (userCkeck != null)
                {
                    throw new UserFriendlyException(string.Format("Já existe um usuário com o email {0}.", pessoaFisica.EmailPrincipal.Endereco));
                }
            }

            var user = User.CreateUserPorPessoa(pessoaFisica, tipoDeUsuario, userName);

            await base.CreateAsync(user);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            if (roleNames != null && roleNames.Any())
            {
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var rolesEntity = await (RoleManager as RoleManager).GetAllList(roleNames);

                    user.Roles = new List<UserRole>();

                    foreach (var roleEntity in rolesEntity)
                    {
                        user.Roles.Add(new UserRole(user.TenantId, user.Id, roleEntity.Id));
                    }

                    await _unitOfWorkManager.Current.SaveChangesAsync();
                }
            }

            return user;
        }

        public async Task<User> CreatePorPessoaJuridica(PessoaJuridica pessoaJuridica, TipoDeUsuarioEnum tipoDeUsuario,
          string[] roleNames = null, string userName = "")
        {
            if (pessoaJuridica.EmailPrincipal == null)
            {
                throw new UserFriendlyException("A pessoa selecionada não possui e-mail particular cadastrado.");
            }

            if (string.IsNullOrWhiteSpace(pessoaJuridica.NomeFantasia)
                || (!string.IsNullOrWhiteSpace(pessoaJuridica.NomeFantasia) && pessoaJuridica.NomeFantasia.Split(' ').ToList().Count <= 1))
            {
                throw new UserFriendlyException("O nome e sobrenome não estão preenchidos corretamente");
            }

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                User userCkeck = _repositoryUser.FirstOrDefault(x => x.UserName == userName
                    || x.UserName == pessoaJuridica.EmailPrincipal.Endereco);
                if (userCkeck != null)
                {
                    throw new UserFriendlyException(string.Format("Já existe um usuário com o nome {0}-{1}.", userName, pessoaJuridica.EmailPrincipal.Endereco));
                }

                userCkeck = _repositoryUser.FirstOrDefault(x => x.EmailAddress == pessoaJuridica.EmailPrincipal.Endereco);
                if (userCkeck != null)
                {
                    throw new UserFriendlyException(string.Format("Já existe um usuário com o email {0}.", pessoaJuridica.EmailPrincipal.Endereco));
                }
            }

            var user = User.CreateUserPorPessoaJuridica(pessoaJuridica, tipoDeUsuario, userName);

            await base.CreateAsync(user);
            await _unitOfWorkManager.Current.SaveChangesAsync();

            if (roleNames != null && roleNames.Any())
            {
                using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var rolesEntity = await (RoleManager as RoleManager).GetAllList(roleNames);

                    user.Roles = new List<UserRole>();

                    foreach (var roleEntity in rolesEntity)
                    {
                        user.Roles.Add(new UserRole(user.TenantId, user.Id, roleEntity.Id));
                    }

                    await _unitOfWorkManager.Current.SaveChangesAsync();
                }
            }

            return user;
        }

        public async Task<User> GetByPessoaId(int pessoaId)
        {
            return await _repositoryUser.FirstOrDefaultAsync(x => x.PessoaId == pessoaId);
        }

        public async Task<User> GetByUserName(string UserName)
        {
            return await _repositoryUser.FirstOrDefaultAsync(x => x.UserName == UserName);
        }

        private async Task<UserPermissionCacheItem> GetUserPermissionCacheItemCustomAsync(long userId)
        {
            var cacheKey = userId + "@" + (GetCurrentTenantId() ?? 0) + "@" + Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

            return await _cacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).GetAsync(cacheKey, async () =>
            {
                var newCacheItem = new UserPermissionCacheItem(userId);

                foreach (var roleName in await GetRolesAsync(userId))
                {
                    newCacheItem.RoleIds.Add((await RoleManager.GetRoleByNameAsync(roleName)).Id);
                }

                foreach (var permissionInfo in await UserPermissionStore.GetPermissionsAsync(userId))
                {
                    if (permissionInfo.IsGranted)
                    {
                        newCacheItem.GrantedPermissions.Add(permissionInfo.Name);
                    }
                    else
                    {
                        newCacheItem.ProhibitedPermissions.Add(permissionInfo.Name);
                    }
                }

                return newCacheItem;
            });
        }

        private int? GetCurrentTenantId()
        {
            if (_unitOfWorkManager.Current != null)
            {
                return _unitOfWorkManager.Current.GetTenantId();
            }

            return AbpSession.TenantId;
        }

        private async Task SetGrantedPermissionsCustomAsync(User user, IEnumerable<Permission> permissions)
        {
            var oldPermissions = await this.GetGrantedPermissionsAsync(user);
            var newPermissions = permissions.ToArray();

            foreach (var permission in oldPermissions.Where(p => !newPermissions.Contains(p)))
            {
                await ProhibitPermissionAsync(user, permission);
            }

            foreach (var permission in newPermissions.Where(p => !oldPermissions.Contains(p)))
            {
                await GrantPermissionAsync(user, permission);
            }
        }

    }
}