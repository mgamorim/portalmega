﻿using Abp.Authorization.Users;
using Abp.Extensions;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Authorization.Users
{
    /// <summary>
    /// Represents a user in the system.
    /// </summary>
    public class User : AbpUser<User>
    {
        public const int MinPlainPasswordLength = 6;

        public const int MaxPhoneNumberLength = 24;

        public virtual Guid? ProfilePictureId { get; set; }

        public virtual bool ShouldChangePasswordOnNextLogin { get; set; }

        public virtual ICollection<Empresa> Empresas { get; set; }

        public virtual TipoDeUsuarioEnum TipoDeUsuario { get; set; }

        public virtual Pessoa Pessoa { get; set; }

        public virtual int? PessoaId { get; set; }

        //Can add application specific user properties here

        public User()
        {
            IsLockoutEnabled = true;
            IsTwoFactorEnabled = true;
        }

        /// <summary>
        /// Creates admin <see cref="User"/> for a tenant.
        /// </summary>
        /// <param name="tenantId">Tenant Id</param>
        /// <param name="emailAddress">Email address</param>
        /// <param name="password">Password</param>
        /// <returns>Created <see cref="User"/> object</returns>
        public static User CreateTenantAdminUser(int tenantId, string emailAddress, string password)
        {
            return new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                Surname = AdminUserName,
                EmailAddress = emailAddress,
                Password = new PasswordHasher().HashPassword(password)
            };
        }

        public static User CreateUserPorPessoa(PessoaFisica pessoaFisica, TipoDeUsuarioEnum tipoDeUsuario, string userName)
        {
            var email =
                pessoaFisica.EnderecosEletronicos.FirstOrDefault(
                    x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

            var nomeCompleto = pessoaFisica.Nome.Trim().Split(' ');
            var nomeDoUsuario = !string.IsNullOrWhiteSpace(userName)
                ? userName
                : !string.IsNullOrWhiteSpace(email.Endereco)
                    ? email.Endereco
                    : string.Concat("usuario", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);


            var user = new User
            {
                TenantId = pessoaFisica.TenantId,
                UserName = nomeDoUsuario,
                Name = nomeCompleto.FirstOrDefault(),
                Surname = nomeCompleto.LastOrDefault(),
                EmailAddress = email != null ? email.Endereco : string.Empty,
                Password = new PasswordHasher().HashPassword(CreateRandomPassword()),
                Pessoa = pessoaFisica,
                IsEmailConfirmed = true,
                IsLockoutEnabled = true,
                IsActive = true,
                TipoDeUsuario = tipoDeUsuario
            };

            user.SetNewPasswordResetCode();

            return user;
        }

        public static User CreateUserPorPessoaJuridica(PessoaJuridica pessoaJuridica, TipoDeUsuarioEnum tipoDeUsuario, string userName)
        {
            var email =
                pessoaJuridica.EnderecosEletronicos.FirstOrDefault(
                    x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

            var nomeCompleto = pessoaJuridica.NomeFantasia.Trim().Split(' ');
            var nomeDoUsuario = !string.IsNullOrWhiteSpace(userName)
                ? userName
                : !string.IsNullOrWhiteSpace(email.Endereco)
                    ? email.Endereco
                    : string.Concat("usuario", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);


            var user = new User
            {
                TenantId = pessoaJuridica.TenantId,
                UserName = nomeDoUsuario,
                Name = nomeCompleto.FirstOrDefault(),
                Surname = nomeCompleto.LastOrDefault(),
                EmailAddress = email != null ? email.Endereco : string.Empty,
                Password = new PasswordHasher().HashPassword(CreateRandomPassword()),
                Pessoa = pessoaJuridica,
                IsEmailConfirmed = true,
                IsLockoutEnabled = true,
                IsActive = true,
                TipoDeUsuario = tipoDeUsuario
            };

            user.SetNewPasswordResetCode();

            return user;
        }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public void Unlock()
        {
            AccessFailedCount = 0;
            LockoutEndDateUtc = null;
        }
    }
}