﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.EZ.EzFieldValidation
{
    public class EzFieldValidation
    {
        public EzFieldValidation(bool isValid, string name, string messageError)
        {
            IsValid = isValid;
            Name = name;
            MessageError = messageError;
        }

        public string className { get { return "EzFieldValidation"; } }

        public bool IsValid { get; }

        public string Name { get; }
        public string MessageError { get; }
    }
}
