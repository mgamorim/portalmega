﻿namespace EZ.EZControl.EZ.EzNotification
{
    public class SistemaNotification
    {
        public int? TenantId { get; set; }
        public int UserId { get; set; }
    }
}
