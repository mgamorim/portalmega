﻿using System;
using System.Threading.Tasks;
using Abp.Domain.Services;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.EZ.EzNotification.Interfaces
{
    public interface IEmailNotificationService
    {
        Task Send(string destinatario, TemplateEmail templateEmail);
        Task Send(string destinatario, TemplateEmail templateEmail, ConexaoSMTP conexaoSmtp);
        Task SendComSmtp(String destinatario, Int32 smtpId, Int32 templateDeEmailId);
        Task SendComParametro(String destinatario, Int32 parametroId, Int32 templateDeEmailId);
    }
}
