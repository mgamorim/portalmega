﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Services;

namespace EZ.EZControl.EZ.EzNotification.Interfaces
{
    public interface ISmsNotificationService: IDomainService
    {
        Task Send(SmsNotification sms);
        Task SendAllByItem(List<SmsNotification> smss);
        Task SendAll(SmsNotification sms, List<string> telefones);
    }
}
