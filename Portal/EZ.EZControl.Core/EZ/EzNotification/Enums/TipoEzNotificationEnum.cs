﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.EZ.EzNotification
{
    public enum TipoEzNotificationEnum
    {
        Sistema = 1,
        Email = 2,
        Sms = 3,
        Todos = 999
    }
}
