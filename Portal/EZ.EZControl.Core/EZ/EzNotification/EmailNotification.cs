﻿namespace EZ.EZControl.EZ.EzNotification
{
    public class EmailNotification
    {
        public const int DestinatarioMaxLength = 300;

        public string Descricao { get; set; }
        public string Destinatario { get; set; }
    }
}
