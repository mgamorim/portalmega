﻿using System;
using EZ.EZControl.EZ.EzNotification.Enums;

namespace EZ.EZControl.EZ.EzNotification
{
    public class SmsNotification
    {
        public string Chave { get; set; }
        public TipoSmsEnum TipoSms { get; set; }
        /// <summary>
        /// Ex: 11988887777, não precisa do +55
        /// </summary>
        public string Numero { get; set; }
        public string Mensagem { get; set; }
        public DateTime DataEHora { get; set; }
        public string Referencia { get; set; }
    }
}
