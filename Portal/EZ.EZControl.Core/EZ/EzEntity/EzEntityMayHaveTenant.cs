﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.EZ.EzEntity
{
    public class EzEntityMayHaveTenant<TPrimaryKey> : EzEntity<TPrimaryKey>, IMayHaveTenant
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int? TenantId { get; set; }
    }

    public class EzEntityMayHaveTenantMustHaveEmpresa<TPrimaryKey> : EzEntity<TPrimaryKey>, IMayHaveTenant, IHasEmpresa
    where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int? TenantId { get; set; }
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }

    public class EzEntityMayHaveTenant : EzEntityMayHaveTenant<int>
    {
    }

    public class EzEntityMayHaveTenantMustHaveEmpresa : EzEntityMayHaveTenantMustHaveEmpresa<int>
    {
    }

    public class EzEntityRecursiveMayHaveTenant<TEntity> : EzEntityMayHaveTenant<int>, IEzEntityRecursive<TEntity>
        where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }

    public class EzEntityRecursiveMayHaveTenantMustHaveEmpresa<TEntity> : EzEntityMayHaveTenant<int>, IEzEntityRecursive<TEntity>
    where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }
}