﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.EZ.EzEntity
{
    public class EzFullAuditedEntityMustHaveTenant<TPrimaryKey> : EzFullAuditedEntity<TPrimaryKey>, IMustHaveTenant
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int TenantId { get; set; }
    }

    public class EzFullAuditedEntityMustHaveTenantMustHaveEmpresa<TPrimaryKey> : EzFullAuditedEntity<TPrimaryKey>, IMustHaveTenant, IHasEmpresa
    where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int TenantId { get; set; }
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }

    public class EzFullAuditedEntityMustHaveTenant : EzFullAuditedEntityMustHaveTenant<int>
    {

    }

    public class EzFullAuditedEntityMustHaveTenantMustHaveEmpresa : EzFullAuditedEntityMustHaveTenantMustHaveEmpresa<int>
    {

    }

    public class EzFullAuditedEntityRecursiveMustHaveTenant<TEntity> : EzFullAuditedEntityMustHaveTenant<int>, IEzEntityRecursive<TEntity>
        where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }

    public class EzFullAuditedEntityRecursiveMustHaveTenantMustHaveEmpresa<TEntity> : EzFullAuditedEntityMustHaveTenantMustHaveEmpresa<int>, IEzEntityRecursive<TEntity>
    where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }
}