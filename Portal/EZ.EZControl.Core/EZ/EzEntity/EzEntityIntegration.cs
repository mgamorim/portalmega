﻿using System;
using Abp.Domain.Entities;

namespace EZ.EZControl.EZ.EzIntegration
{
    public abstract class EzEntityIntegration : Entity
    {
        public int ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
        public bool IsIntegration { get; set; }
    }
}