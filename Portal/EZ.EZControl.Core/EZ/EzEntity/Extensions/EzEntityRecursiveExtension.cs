﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.EZ.EzEntity.Extensions
{
    public static class EzEntityRecursiveExtension
    {
        public static TEntity AddFilho<TEntity>(this TEntity pai, TEntity filho)
            where TEntity : IEzEntityRecursive<TEntity>, IEzEntity
        {
            filho.Pai = pai;
            if (pai.Filhos == null)
                pai.Filhos = new HashSet<TEntity>();
            pai.Filhos.Add(filho);
            return pai;
        }

        public static TEntity AddFilhos<TEntity>(this TEntity pai, IEnumerable<TEntity> filhos)
            where TEntity : IEzEntityRecursive<TEntity>, IEzEntity
        {
            filhos.ToList().ForEach(c => pai.AddFilho(c));
            return pai;
        }
    }
}
