﻿using System;
using Abp.Domain.Entities.Auditing;

namespace EZ.EZControl.EZ.EzIntegration
{
    public abstract class EzFullAuditedIntegration : FullAuditedEntity
    {
        public int ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
        public bool IsIntegration { get; set; }
    }
}
