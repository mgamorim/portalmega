﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.EZ.EzEntity
{
    public class EzFullAuditedEntityMayHaveTenant<TPrimaryKey> : EzFullAuditedEntity<TPrimaryKey>, IMayHaveTenant
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int? TenantId { get; set; }
    }

    public class EzFullAuditedEntityMayHaveTenantMustHaveEmpresa<TPrimaryKey> : EzFullAuditedEntity<TPrimaryKey>, IMayHaveTenant, IHasEmpresa
    where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int? TenantId { get; set; }
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }

    public class EzFullAuditedEntityMayHaveTenant : EzFullAuditedEntityMayHaveTenant<int>
    {
    
    }

    public class EzFullAuditedEntityMayHaveTenantMustHaveEmpresa : EzFullAuditedEntityMayHaveTenantMustHaveEmpresa<int>
    {

    }

    public class EzFullAuditedEntityRecursiveMayHaveTenant<TEntity> : EzFullAuditedEntityMayHaveTenant<int>, IEzEntityRecursive<TEntity>
        where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }

    public class EzFullAuditedEntityRecursiveMayHaveTenantMustHaveEmpresa<TEntity> : EzFullAuditedEntityMayHaveTenantMustHaveEmpresa<int>, IEzEntityRecursive<TEntity>
    where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }
}