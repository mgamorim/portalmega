﻿using System;
using Abp.Domain.Entities;

namespace EZ.EZControl.EZ.EzEntity.Interfaces
{
    public interface IEzEntity<TPrimaryKey> : IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        TPrimaryKey ExternalId { get; set; }
        DateTime? DateOfEditionIntegration { get; set; }
    }

    public interface IEzEntity : IEzEntity<int>
    {
    }
}