﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.EZ.EzEntity.Interfaces
{
    public interface IHasEmpresa
    {
        Empresa Empresa { get; set; }
        int EmpresaId { get; set; }
    }
}
