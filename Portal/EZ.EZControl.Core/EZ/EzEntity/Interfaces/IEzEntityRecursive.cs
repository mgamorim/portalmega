﻿using System.Collections.Generic;

namespace EZ.EZControl.EZ.EzEntity.Interfaces
{
    public interface IEzEntityRecursive<TEntity>
        where TEntity : IEzEntity
    {
        string Nome { get; set; }
        TEntity Pai { get; set; }
        int? PaiId { get; set; }
        ICollection<TEntity> Filhos { get; set; }
    }
}
