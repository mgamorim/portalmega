﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.EZ.EzEntity
{
    public class EzEntity<TPrimaryKey> : Entity<TPrimaryKey>, IEzEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public TPrimaryKey ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
    }

    public class EzEntity : EzEntity<int>
    {
    }

    public class EzEntityRecursive<TEntity> : EzEntity<int>, IEzEntityRecursive<TEntity>
        where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }
}
