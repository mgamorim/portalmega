﻿using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.EZ.EzEntity
{
    public class EzEntityMustHaveTenant<TPrimaryKey> : EzEntity<TPrimaryKey>, IMustHaveTenant
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int TenantId { get; set; }
    }

    public class EzEntityMustHaveTenantMustHaveEmpresa<TPrimaryKey> : EzEntity<TPrimaryKey>, IMustHaveTenant, IHasEmpresa
    where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public virtual int TenantId { get; set; }
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }

    public class EzEntityMustHaveTenant : EzEntityMustHaveTenant<int>
    {
    }

    public class EzEntityMustHaveTenantMustHaveEmpresa : EzEntityMustHaveTenantMustHaveEmpresa<int>
    {
    }

    public class EzEntityRecursiveMustHaveTenant<TEntity> : EzEntityMustHaveTenant<int>, IEzEntityRecursive<TEntity>
        where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }

    public class EzEntityRecursiveMustHaveTenantMustHaveTenant<TEntity> : EzEntityMustHaveTenantMustHaveEmpresa<int>, IEzEntityRecursive<TEntity>
    where TEntity : IEzEntity
    {
        public string Nome { get; set; }
        public virtual TEntity Pai { get; set; }
        public int? PaiId { get; set; }
        public virtual ICollection<TEntity> Filhos { get; set; }
    }
}