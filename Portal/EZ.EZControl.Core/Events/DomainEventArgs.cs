﻿using Abp.Domain.Entities;
using System;

namespace EZ.EZControl.Events
{
    public class DomainEventArgs<TEntity, TPrimaryKey> : EventArgs
        where TEntity : class, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public DomainEventArgs(TEntity entity)
        {
            this.Entity = entity;
        }

        public TEntity Entity { get; set; }
    }
}