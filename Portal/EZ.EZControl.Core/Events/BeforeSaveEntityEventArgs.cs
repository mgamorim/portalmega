﻿using Abp.Domain.Entities;
using System;

namespace EZ.EZControl.Events
{
    public class BeforeSaveEntityEventArgs<TEntity, TPrimaryKey> : DomainEventArgs<TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public BeforeSaveEntityEventArgs(TEntity entity)
            : base(entity)
        {
        }
    }

    public class BeforeSaveEntityEventArgs<TEntity> : BeforeSaveEntityEventArgs<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        public BeforeSaveEntityEventArgs(TEntity entity) : base(entity)
        {
        }
    }
}