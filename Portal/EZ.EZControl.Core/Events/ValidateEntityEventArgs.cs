﻿using Abp.Domain.Entities;
using System;

namespace EZ.EZControl.Events
{
    public class ValidateEntityEventArgs<TEntity, TPrimaryKey> : DomainEventArgs<TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public ValidateEntityEventArgs(TEntity entity)
            : base(entity)
        {
        }
    }

    public class ValidateEntityEventArgs<TEntity> : ValidateEntityEventArgs<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        public ValidateEntityEventArgs(TEntity entity)
            : base(entity)
        {
        }
    }
}