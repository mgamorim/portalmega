﻿namespace EZ.EZControl.Emailing
{
    public interface IEmailTemplateProvider
    {
        string GetDefaultTemplate();
    }
}
