﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.Scheduler.TimeZones.TimeZoneInfoModel
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones.Interfaces;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones
{
    internal class TimeZoneInfoModel : ITimeZoneModel
    {
        private readonly TimeZoneInfo TimeZoneInfo;

        /// <summary>The time zone ID.</summary>
        public string TimeZoneId { get; set; }

        /// <summary>Localized version of the standard name</summary>
        public string DisplayName { get; set; }

        /// <summary>The standard name of a time zone</summary>
        public string StandardName { get; set; }

        /// <summary>The base utc offset, without apply daylight saving</summary>
        public TimeSpan BaseUtcOffset { get; set; }

        /// <summary>
        /// Returns bool value indicating whether the time zone
        /// selected uses daylight saving.
        /// </summary>
        public bool SupportsDayLightSaving { get; set; }

        /// <summary>
        /// Returns array of adjustment rules to be used when DLS occurs.
        /// </summary>
        public TimeZoneInfo.AdjustmentRule[] AdjustmentRules { get; set; }

        public TimeZoneInfoModel(TimeZoneInfo info)
        {
            this.TimeZoneInfo = info;
            this.AdjustmentRules = info.GetAdjustmentRules();
            this.TimeZoneId = info.Id;
            this.DisplayName = info.DisplayName;
            this.StandardName = info.StandardName;
            this.BaseUtcOffset = info.BaseUtcOffset;
            this.SupportsDayLightSaving = info.SupportsDaylightSavingTime;
        }

        public TimeSpan GetUtcOffset(DateTime date)
        {
            return this.TimeZoneInfo.GetUtcOffset(date);
        }

        public bool IsUsingDayLightSaving(DateTime date)
        {
            return this.TimeZoneInfo.IsDaylightSavingTime(date);
        }

        public bool IsDefaultZone()
        {
            return this.DisplayName.ToLowerInvariant() == "UTC".ToLowerInvariant();
        }

        /// <summary>
        /// Gets the transition delta of a given time frame., i.e. the time to add or substract in when daylgiht saving starts or end.
        /// </summary>
        public TimeSpan GetTransitionDelta(DateTime rangeStart, DateTime rangeEnd)
        {
            return this.GetUtcOffset(rangeStart) - this.GetUtcOffset(rangeEnd);
        }

        public bool IsTransitionFrame(DateTime start, DateTime end)
        {
            return this.IsUsingDayLightSaving(start) != this.IsUsingDayLightSaving(end);
        }

        public bool IsTransitionTimeSlot(DateTime timeSlot)
        {
            return this.IsTransitionFrame(timeSlot.AddHours(-1.0), timeSlot);
        }

        public TimeZoneInfo.AdjustmentRule GetAdjustmentRuleForDate(DateTime date)
        {
            foreach (TimeZoneInfo.AdjustmentRule adjustmentRule in this.AdjustmentRules)
            {
                if (adjustmentRule.DateStart < date && adjustmentRule.DateEnd > date)
                    return adjustmentRule;
            }
            return this.AdjustmentRules[0];
        }

        public DateTime GetTransitionStart(TimeZoneInfo.AdjustmentRule rule)
        {
            return this.GetTransitionTime(rule.DaylightTransitionStart);
        }

        public DateTime GetTransitionEnd(TimeZoneInfo.AdjustmentRule rule)
        {
            return this.GetTransitionTime(rule.DaylightTransitionEnd);
        }

        private DateTime GetTransitionTime(TimeZoneInfo.TransitionTime transitionTime)
        {
            int month = transitionTime.Month;
            int num1 = DateTime.DaysInMonth(DateTime.Now.Year, month);
            int num2 = 1;
            DateTime dateTime = new DateTime(1601, month, 1);
            if (transitionTime.IsFixedDateRule)
            {
                dateTime = dateTime.AddDays((double)(transitionTime.Day - 1));
            }
            else
            {
                while (dateTime.Day < num1 && (transitionTime.Week != num2 || dateTime.DayOfWeek != transitionTime.DayOfWeek))
                {
                    dateTime = dateTime.AddDays(1.0);
                    if ((int)dateTime.DayOfWeek % 7 == 0)
                        ++num2;
                }
            }
            dateTime = dateTime.AddTicks(transitionTime.TimeOfDay.Ticks);
            return dateTime;
        }
    }
}
