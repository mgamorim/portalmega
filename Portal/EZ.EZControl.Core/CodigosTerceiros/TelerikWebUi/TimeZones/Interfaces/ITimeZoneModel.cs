﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.ITimeZoneModel
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones.Interfaces
{
    /// <summary>This interface defines the ITimeZoneModel.</summary>
    public interface ITimeZoneModel
    {
        string TimeZoneId { get; set; }

        string DisplayName { get; set; }

        string StandardName { get; set; }

        TimeSpan BaseUtcOffset { get; set; }

        bool SupportsDayLightSaving { get; set; }

        TimeZoneInfo.AdjustmentRule[] AdjustmentRules { get; set; }

        TimeSpan GetUtcOffset(DateTime date);

        TimeSpan GetTransitionDelta(DateTime rangeStart, DateTime rangeEnd);

        bool IsTransitionFrame(DateTime start, DateTime end);

        bool IsUsingDayLightSaving(DateTime date);

        TimeZoneInfo.AdjustmentRule GetAdjustmentRuleForDate(DateTime date);
    }
}
