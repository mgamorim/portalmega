﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.TimeZoneInfoProvider
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones.Interfaces;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>
    /// This Class defines the TimeZoneInfoProvider that
    /// implements the TimeZoneProviderBase and IDisposable.
    /// </summary>
    public class TimeZoneInfoProvider : TimeZoneProviderBase, IDisposable
    {
        public TimeZoneInfoProvider()
        {
        }

        public TimeZoneInfoProvider(string timeZoneId)
        {
            this.InitOperationZones(timeZoneId);
        }

        /// <summary>Initializes the provider.</summary>
        /// <param name="name">The friendly name of the provider.</param>
        /// <param name="config">A collection of the name/value pairs representing the provider-specific
        /// attributes specified in the configuration for this provider.</param>
        /// <exception cref="T:System.ArgumentNullException">The name of the provider is
        /// null.</exception>
        /// <exception cref="T:System.ArgumentException">The name of the provider has a length
        /// of zero.</exception>
        /// <exception cref="T:System.InvalidOperationException">An attempt is made to call
        /// <see cref="M:System.Configuration.Provider.ProviderBase.Initialize(System.String,System.Collections.Specialized.NameValueCollection)" />
        /// on a provider after the provider has already been initialized.</exception>
        public override void Initialize(string name, NameValueCollection config)
        {
            base.Initialize(name, config);
            this.InitOperationZones(config["timeZoneId"]);
        }

        private void InitOperationZones(string id)
        {
            if (!string.IsNullOrEmpty(id))
                this.OperationTimeZone = (ITimeZoneModel)new TimeZoneInfoModel(this.GetTimeZoneInfoById(id));
            else
                this.OperationTimeZone = (ITimeZoneModel)new TimeZoneInfoModel(TimeZoneInfo.Utc);
        }

        internal static TimeZoneInfoModel GetTimeZoneModelById(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    id = TimeZoneInfo.Utc.Id;
                return new TimeZoneInfoModel(TimeZoneInfo.GetSystemTimeZones().First<TimeZoneInfo>((Func<TimeZoneInfo, bool>)(timeZoneInfo => timeZoneInfo.Id == id)));
            }
            catch (InvalidOperationException ex)
            {
                throw new TimeZoneNotFoundException("Time Zone with the provided Id was not found", (Exception)ex);
            }
            catch (Exception ex)
            {
                throw new TimeZoneNotFoundException("Time Zone with the provided Id was not found", ex);
            }
        }

        internal static DateTime LocalToUtc(DateTime local, ITimeZoneModel timeZone)
        {
            TimeSpan utcOffset = timeZone.GetUtcOffset(local);
            return new DateTime(local.Add(-utcOffset).Ticks, DateTimeKind.Utc);
        }

        internal static DateTime UtcToLocal(DateTime utc, ITimeZoneModel timeZone)
        {
            TimeSpan utcOffset = timeZone.GetUtcOffset(utc);
            return new DateTime(utc.Add(utcOffset).Ticks, DateTimeKind.Unspecified);
        }

        internal TimeZoneInfo GetTimeZoneInfoById(string id)
        {
            try
            {
                return TimeZoneInfo.GetSystemTimeZones().First<TimeZoneInfo>((Func<TimeZoneInfo, bool>)(timeZoneInfo => timeZoneInfo.Id == id));
            }
            catch (InvalidOperationException ex)
            {
                throw new TimeZoneNotFoundException("Time Zone with the provided Id was not found", (Exception)ex);
            }
        }

        /// <summary>UTCs to local.</summary>
        /// <param name="utc">The UTC.</param>
        /// <returns></returns>
        public override DateTime UtcToLocal(DateTime utc)
        {
            return TimeZoneInfoProvider.UtcToLocal(utc, this.OperationTimeZone);
        }

        /// <summary>Locals to UTC.</summary>
        /// <param name="local">The local.</param>
        /// <returns></returns>
        public override DateTime LocalToUtc(DateTime local)
        {
            return TimeZoneInfoProvider.LocalToUtc(local, this.OperationTimeZone);
        }

        /// <summary>Gets list of all time zones available on the system.</summary>
        /// <returns>Returns list of all time zones available on the system.</returns>
        public override List<TimeZoneNamePair> GetAllTimeZones()
        {
            return TimeZoneInfo.GetSystemTimeZones().Select<TimeZoneInfo, TimeZoneNamePair>((Func<TimeZoneInfo, TimeZoneNamePair>)(timeZoneInfo => new TimeZoneNamePair()
            {
                Id = timeZoneInfo.Id,
                DisplayName = timeZoneInfo.DisplayName
            })).ToList<TimeZoneNamePair>();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing,
        /// or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;
            this.OperationTimeZone = (ITimeZoneModel)null;
        }
    }
}
