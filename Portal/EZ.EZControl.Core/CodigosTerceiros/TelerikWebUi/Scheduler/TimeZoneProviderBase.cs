﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.TimeZoneProviderBase
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones.Interfaces;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>This abstract class defines the TimeZoneProviderBase.</summary>
    public abstract class TimeZoneProviderBase : ProviderBase
    {
        public const string TimeZoneIdKey = "timeZoneId";

        /// <summary>Gets or sets the operation time zone.</summary>
        /// <value>The operation time zone.</value>
        public ITimeZoneModel OperationTimeZone { get; set; }

        /// <summary>Locals to UTC.</summary>
        /// <param name="local">The local.</param>
        /// <returns></returns>
        public abstract DateTime LocalToUtc(DateTime local);

        /// <summary>UTCs to local.</summary>
        /// <param name="utc">The UTC.</param>
        /// <returns></returns>
        public abstract DateTime UtcToLocal(DateTime utc);

        /// <summary>Gets list of all time zones available on the system.</summary>
        /// <returns>Returns list of all time zones available on the system.</returns>
        public abstract List<TimeZoneNamePair> GetAllTimeZones();
    }
}
