﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.TimeZoneNamePair
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>This Class defines the TimeZoneNamePair object.</summary>
    public class TimeZoneNamePair
    {
        /// <summary>Gets or sets the id.</summary>
        /// <value>The id.</value>
        public string Id { get; set; }

        /// <summary>Gets or sets the display name.</summary>
        /// <value>The display name.</value>
        public string DisplayName { get; set; }

        public TimeZoneNamePair()
        {
        }

        public TimeZoneNamePair(string id, string displayName)
        {
            this.Id = id;
            this.DisplayName = displayName;
        }
    }
}
