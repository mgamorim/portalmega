﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.RecurrenceRuleConverter
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using System.ComponentModel;
using System.Globalization;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>
    /// Provides a type converter to convert RecurrenceRule objects to and from string
    /// representation.
    /// </summary>
    public class RecurrenceRuleConverter : TypeConverter
    {
        /// <summary>
        /// Overloaded. Returns whether this converter can convert an object of one type to
        /// the type of this converter.
        /// </summary>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Overloaded. Converts the given value to the type of this converter.
        /// </summary>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
                throw new InvalidOperationException("Cannot convert from null value.");
            string input = value as string;
            if (string.IsNullOrEmpty(input))
                return base.ConvertFrom(context, culture, value);
            RecurrenceRule rrule;
            RecurrenceRule.TryParse(input, out rrule);
            return (object)rrule;
        }

        /// <summary>
        /// Overloaded. Returns whether this converter can convert the object to the
        /// specified type.
        /// </summary>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>Overloaded. Converts the given value object to the specified type.</summary>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                RecurrenceRule recurrenceRule = value as RecurrenceRule;
                if (recurrenceRule != (RecurrenceRule)null)
                    return (object)recurrenceRule.ToString();
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>Overloaded. Returns whether the given value object is valid for this type.</summary>
        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            string input = value as string;
            if (string.IsNullOrEmpty(input))
                return base.IsValid(context, value);
            RecurrenceRule rrule;
            RecurrenceRule.TryParse(input, out rrule);
            return rrule != (RecurrenceRule)null;
        }
    }
}
