﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.RecurrenceRule
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduling;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones.Interfaces;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>Provides the <strong>abstract</strong> base class for recurrence rules.</summary>
    /// <seealso cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.HourlyRecurrenceRule">HourlyRecurrenceRule Class</seealso>
    /// <seealso cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.DailyRecurrenceRule">DailyRecurrenceRule Class</seealso>
    /// <seealso cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.WeeklyRecurrenceRule">WeeklyRecurrenceRule Class</seealso>
    /// <seealso cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.MonthlyRecurrenceRule">MonthlyRecurrenceRule Class</seealso>
    /// <seealso cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.YearlyRecurrenceRule">YearlyRecurrenceRule Class</seealso>
    /// <remarks>
    /// 	<strong>Notes to implementers:</strong> This base class is provided to make it
    /// easier for implementers to create a recurrence rule. Implementers are encouraged to
    /// extend this base class instead of creating their own.
    /// </remarks>
    [TypeConverter(typeof(RecurrenceRuleConverter))]
    [Serializable]
    public abstract class RecurrenceRule : ISerializable, IEquatable<RecurrenceRule>
    {
        /// <summary>Represents an empty recurrence rule</summary>
        public static readonly RecurrenceRule Empty = (RecurrenceRule)new RecurrenceRule.EmptyRecurrenceRule();
        private DateTime _effectiveStart = DateTime.MinValue;
        private DateTime _effectiveEnd = DateTime.MaxValue;
        private IList<DateTime> _exceptions = (IList<DateTime>)new List<DateTime>();
        private int _maximumCandidates = 3000;
        protected RecurrencePattern rulePattern = new RecurrencePattern();
        protected RecurrenceRange ruleRange = new RecurrenceRange();

        /// <summary>Gets the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> associated with this recurrence rule.</summary>
        /// <value>The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> associated with this recurrence rule.</value>
        /// <remarks>
        ///     By calling <see cref="M:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule.SetEffectiveRange(System.DateTime,System.DateTime)" /> the range of the generated
        ///     occurrences can be narrowed.
        /// </remarks>
        public RecurrenceRange Range
        {
            get
            {
                return this.ruleRange;
            }
        }

        /// <summary>Gets the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern" /> associated with this recurrence rule.</summary>
        /// <value>The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern" /> associated with this recurrence rule.</value>
        public RecurrencePattern Pattern
        {
            get
            {
                return this.rulePattern;
            }
        }

        /// <remarks>Occurrence times are in UTC.</remarks>
        /// <summary>Gets the evaluated occurrence times of this recurrence rule.</summary>
        /// <value>The evaluated occurrence times of this recurrence rule.</value>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class HourlyRecurrenceRuleExample
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 10;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             HourlyRecurrenceRule rrule = new HourlyRecurrenceRule(2, range);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 6/1/2007 3:30:00 PM
        ///  2: 6/1/2007 5:30:00 PM
        ///  3: 6/1/2007 7:30:00 PM
        ///  4: 6/1/2007 9:30:00 PM
        ///  5: 6/1/2007 11:30:00 PM
        ///  6: 6/2/2007 1:30:00 AM
        ///  7: 6/2/2007 3:30:00 AM
        ///  8: 6/2/2007 5:30:00 AM
        ///  9: 6/2/2007 7:30:00 AM
        /// 10: 6/2/2007 9:30:00 AM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class HourlyRecurrenceRuleExample
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 10
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             Dim rrule As New HourlyRecurrenceRule(2, range)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 6/1/2007 3:30:00 PM
        /// ' 2: 6/1/2007 5:30:00 PM
        /// ' 3: 6/1/2007 7:30:00 PM
        /// ' 4: 6/1/2007 9:30:00 PM
        /// ' 5: 6/1/2007 11:30:00 PM
        /// ' 6: 6/2/2007 1:30:00 AM
        /// ' 7: 6/2/2007 3:30:00 AM
        /// ' 8: 6/2/2007 5:30:00 AM
        /// ' 9: 6/2/2007 7:30:00 AM
        /// '10: 6/2/2007 9:30:00 AM
        /// '
        ///     </code>
        /// </example>
        public virtual IEnumerable<DateTime> Occurrences
        {
            get
            {
                int candidateIndex = 0;
                int occurrencesCount = 0;
                while (occurrencesCount < this.ruleRange.MaxOccurrences)
                {
                    DateTime nextStart = this.GetOccurrenceStart(candidateIndex++);
                    if (this.ruleRange.RecursUntil <= nextStart || this._effectiveEnd < nextStart || this.MaximumCandidates < candidateIndex)
                        break;
                    if (this.MatchAdvancedPattern(nextStart))
                    {
                        ++occurrencesCount;
                        if (!(nextStart < this._effectiveStart) && !this._exceptions.Contains(nextStart))
                            yield return nextStart;
                    }
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this recurrence rule yields any
        /// occurrences.
        /// </summary>
        /// <value>True this recurrence rule yields any occurrences, false otherwise.</value>
        public bool HasOccurrences
        {
            get
            {
                return this.Occurrences.GetEnumerator().MoveNext();
            }
        }

        /// <summary>Gets or sets a list of the exception dates associated with this recurrence rule.</summary>
        /// <value>A list of the exception dates associated with this recurrence rule.</value>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class RecurrenceExceptionsExample
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 10;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             HourlyRecurrenceRule rrule = new HourlyRecurrenceRule(2, range);
        /// 
        ///             // Creates a recurrence exception for 5:30 PM (local time).
        ///             // Note that exception dates must be in universal time.
        ///             rrule.Exceptions.Add(Convert.ToDateTime("6/1/2007 5:30 PM").ToUniversalTime());
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 6/1/2007 3:30:00 PM
        ///  2: 6/1/2007 7:30:00 PM
        ///  3: 6/1/2007 9:30:00 PM
        ///  4: 6/1/2007 11:30:00 PM
        ///  5: 6/2/2007 1:30:00 AM
        ///  6: 6/2/2007 3:30:00 AM
        ///  7: 6/2/2007 5:30:00 AM
        ///  8: 6/2/2007 7:30:00 AM
        ///  9: 6/2/2007 9:30:00 AM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class RecurrenceExceptionsExample
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 10
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             Dim rrule As New HourlyRecurrenceRule(2, range)
        /// 
        ///             ' Creates a recurrence exception for 5:30 PM (local time).
        ///             ' Note that exception dates must be in universal time.
        ///             rrule.Exceptions.Add(Convert.ToDateTime("6/1/2007 5:30 PM").ToUniversalTime())
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 6/1/2007 3:30:00 PM
        /// ' 2: 6/1/2007 7:30:00 PM
        /// ' 3: 6/1/2007 9:30:00 PM
        /// ' 4: 6/1/2007 11:30:00 PM
        /// ' 5: 6/2/2007 1:30:00 AM
        /// ' 6: 6/2/2007 3:30:00 AM
        /// ' 7: 6/2/2007 5:30:00 AM
        /// ' 8: 6/2/2007 7:30:00 AM
        /// ' 9: 6/2/2007 9:30:00 AM
        /// '
        ///     </code>
        /// </example>
        /// <remarks>
        /// Any date placed in the list will be considered a recurrence exception, i.e. an
        /// occurrence will not be generated for that date. The dates must be in <strong>universal
        /// time</strong>.
        /// </remarks>
        public virtual IList<DateTime> Exceptions
        {
            get
            {
                return this._exceptions;
            }
            set
            {
                this._exceptions = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this recurrence rule has associated
        /// exceptions.
        /// </summary>
        /// <value>True if this recurrence rule has associated exceptions, false otherwise.</value>
        public bool HasExceptions
        {
            get
            {
                return 0 < this._exceptions.Count;
            }
        }

        /// <summary>Gets or sets the maximum candidates limit.</summary>
        /// <remarks>
        /// This limit is used to prevent lockups when evaluating infinite rules without using SetEffectiveRange.
        /// The default value should not be changed under normal conditions.
        /// </remarks>
        /// <value>The maximum candidates limit.</value>
        public int MaximumCandidates
        {
            get
            {
                return this._maximumCandidates;
            }
            set
            {
                this._maximumCandidates = value;
            }
        }

        protected DateTime EffectiveStart
        {
            get
            {
                if (this._effectiveStart < this.ruleRange.Start)
                    return this.ruleRange.Start;
                return this._effectiveStart;
            }
        }

        protected RecurrenceRule(SerializationInfo info, StreamingContext context)
        {
            RecurrenceRule rrule;
            if (!RecurrenceRule.TryParse(info.GetString("RRULE"), out rrule))
                throw new InvalidOperationException("Deserialization failed. Parsing not successfull.");
            this.rulePattern = rrule.Pattern;
            this.ruleRange = rrule.Range;
            this._exceptions = rrule.Exceptions;
        }

        protected RecurrenceRule()
        {
        }

        /// <summary>
        ///     Determines whether two specified <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule" /> objects have the
        ///     same value.
        /// </summary>
        public static bool operator ==(RecurrenceRule o1, RecurrenceRule o2)
        {
            if ((object)o1 != null)
                return o1.Equals(o2);
            return (object)o2 == null;
        }

        /// <summary>
        ///     Determines whether two specified <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule" /> objects have
        ///     different values.
        /// </summary>
        public static bool operator !=(RecurrenceRule o1, RecurrenceRule o2)
        {
            if ((object)o1 != null)
                return !o1.Equals(o2);
            return (object)o2 != null;
        }

        /// <summary>
        /// Creates a recurrence rule with the specified pattern and range.
        /// </summary>
        /// <param name="pattern">The recurrence pattern.</param>
        /// <param name="range">The recurrence range.</param>
        /// <returns>The constructed recurrence rule.</returns>
        public static RecurrenceRule FromPatternAndRange(RecurrencePattern pattern, RecurrenceRange range)
        {
            if (pattern == (RecurrencePattern)null || range == (RecurrenceRange)null)
                return (RecurrenceRule)null;
            switch (pattern.Frequency)
            {
                case RecurrenceFrequency.Hourly:
                    return (RecurrenceRule)new HourlyRecurrenceRule(pattern.Interval, range);
                case RecurrenceFrequency.Daily:
                    if (pattern.DaysOfWeekMask != RecurrenceDay.EveryDay)
                        return (RecurrenceRule)new DailyRecurrenceRule(pattern.DaysOfWeekMask, range);
                    if (0 < pattern.Interval)
                        return (RecurrenceRule)new DailyRecurrenceRule(pattern.Interval, range);
                    break;
                case RecurrenceFrequency.Weekly:
                    if (0 < pattern.Interval && pattern.DaysOfWeekMask != RecurrenceDay.None)
                        return (RecurrenceRule)new WeeklyRecurrenceRule(pattern.Interval, pattern.DaysOfWeekMask, range, pattern.FirstDayOfWeek);
                    break;
                case RecurrenceFrequency.Monthly:
                    if (0 < pattern.DayOfMonth && 0 < pattern.Interval)
                        return (RecurrenceRule)new MonthlyRecurrenceRule(pattern.DayOfMonth, pattern.Interval, range);
                    if (pattern.DayOrdinal != 0 && pattern.DaysOfWeekMask != RecurrenceDay.None && 0 < pattern.Interval)
                        return (RecurrenceRule)new MonthlyRecurrenceRule(pattern.DayOrdinal, pattern.DaysOfWeekMask, pattern.Interval, range);
                    break;
                case RecurrenceFrequency.Yearly:
                    if (pattern.Month != RecurrenceMonth.None && 0 < pattern.DayOfMonth)
                        return (RecurrenceRule)new YearlyRecurrenceRule(pattern.Month, pattern.DayOfMonth, range, pattern.Interval);
                    if (pattern.DayOrdinal != 0 && pattern.Month != RecurrenceMonth.None && pattern.DaysOfWeekMask != RecurrenceDay.None)
                        return (RecurrenceRule)new YearlyRecurrenceRule(pattern.DayOrdinal, pattern.Month, pattern.DaysOfWeekMask, range, pattern.Interval);
                    if (pattern.Month != RecurrenceMonth.None)
                        return (RecurrenceRule)new YearlyRecurrenceRule(pattern.Month, range.Start.Day, range, pattern.Interval);
                    break;
            }
            return (RecurrenceRule)null;
        }

        /// <summary>Creates a recurrence rule instance from it's string representation.</summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class ParsingExample
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 10;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             HourlyRecurrenceRule rrule = new HourlyRecurrenceRule(2, range);
        /// 
        ///             // Prints the string representation of the recurrence rule:
        ///             string rruleAsString = rrule.ToString();
        ///             Console.WriteLine("Recurrence rule:\n\n{0}\n", rruleAsString);
        /// 
        ///             // The string representation can be stored in a database, etc.
        ///             // ...
        /// 
        ///             // Then it can be reconstructed using TryParse method:
        ///             RecurrenceRule parsedRule;
        ///             RecurrenceRule.TryParse(rruleAsString, out parsedRule);
        ///             Console.WriteLine("After parsing (should be the same):\n\n{0}", parsedRule);
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Recurrence rule:
        /// 
        /// DTSTART:20070601T123000Z
        /// DTEND:20070601T130000Z
        /// RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// 
        /// 
        /// After parsing (should be the same):
        /// 
        /// DTSTART:20070601T123000Z
        /// DTEND:20070601T130000Z
        /// RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class ParsingExample
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 10
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             Dim rrule As New HourlyRecurrenceRule(2, range)
        /// 
        ///             ' Prints the string representation of the recurrence rule:
        ///             Dim rruleAsString As String = rrule.ToString()
        ///             Console.WriteLine("Recurrence rule:" &amp; Chr(10) &amp; "" &amp; Chr(10) &amp; "{0}" &amp; Chr(10) &amp; "", rruleAsString)
        /// 
        ///             ' The string representation can be stored in a database, etc.
        ///             ' ...
        /// 
        ///             ' Then it can be reconstructed using TryParse method:
        ///             Dim parsedRule As RecurrenceRule
        ///             RecurrenceRule.TryParse(rruleAsString, parsedRule)
        ///             Console.WriteLine("After parsing (should be the same):" &amp; Chr(10) &amp; "" &amp; Chr(10) &amp; "{0}", parsedRule)
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Recurrence rule:
        /// '
        /// 'DTSTART:20070601T123000Z
        /// 'DTEND:20070601T130000Z
        /// 'RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// '
        /// '
        /// 'After parsing (should be the same):
        /// '
        /// 'DTSTART:20070601T123000Z
        /// 'DTEND:20070601T130000Z
        /// 'RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// '
        ///     </code>
        /// </example>
        /// <returns>True if <em>input</em> was converted successfully, false otherwise.</returns>
        /// <param name="input">The string representation to parse.</param>
        /// <param name="rrule">
        /// When this method returns, contains the recurrence rule instance, if the
        /// conversion succeeded, or null if the conversion failed. The conversion fails if the
        /// <em>value</em> parameter is a null reference (<strong>Nothing</strong> in Visual Basic)
        /// or represents invalid recurrence rule.
        /// </param>
        public static bool TryParse(string input, out RecurrenceRule rrule)
        {
            if (string.IsNullOrEmpty(input))
            {
                rrule = (RecurrenceRule)null;
                return false;
            }
            RecurrenceRange recurrenceRange = new RecurrenceRange();
            RecurrencePattern recurrencePattern = new RecurrencePattern();
            List<DateTime> dateTimeList = new List<DateTime>();
            DateTime? nullable1 = new DateTime?();
            DateTime? nullable2 = new DateTime?();
            bool flag = false;
            try
            {
                input = input.Trim();
                string str1 = input;
                char[] chArray = new char[1] { '\n' };
                foreach (string str2 in str1.Split(chArray))
                {
                    string str3 = str2.Trim();
                    Match match = Regex.Match(str3, "^(DTSTART|DTEND):(.*)$", RegexOptions.IgnoreCase);
                    DateTime date;
                    if (match.Success && RecurrenceRule.TryParseDateTime(match.Groups[2].Value, out date))
                    {
                        if (match.Groups[1].Value == "DTSTART")
                            nullable1 = new DateTime?(date);
                        else
                            nullable2 = new DateTime?(date);
                    }
                    RecurrenceRule.ParseRRule(str3, recurrenceRange, recurrencePattern);
                    RecurrenceRule.ParseExceptions(str3, (ICollection<DateTime>)dateTimeList);
                }
                if (nullable1.HasValue && nullable2.HasValue)
                {
                    recurrenceRange.Start = nullable1.Value;
                    recurrenceRange.EventDuration = nullable2.Value.Subtract(nullable1.Value);
                    rrule = RecurrenceRule.FromPatternAndRange(recurrencePattern, recurrenceRange);
                    rrule.Exceptions = (IList<DateTime>)dateTimeList;
                    flag = rrule != (RecurrenceRule)null;
                }
                else
                    rrule = (RecurrenceRule)null;
            }
            catch (Exception ex)
            {
                flag = false;
                rrule = (RecurrenceRule)null;
            }
            return flag;
        }

        /// <summary>
        /// Creates a recurrence rule instance from it's string representation.
        /// </summary>
        /// <param name="input">The string to parse.</param>
        /// <returns>RecurrenceRule if the parsing succeeded or null (<strong>Nothing</strong> in Visual Basic) if the parsing failed.</returns>
        /// <remarks>
        /// See the <see cref="M:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule.TryParse(System.String,EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule@)">TryParse</see> overload for more information and examples.
        /// </remarks>
        public static RecurrenceRule TryParse(string input)
        {
            RecurrenceRule rrule = (RecurrenceRule)null;
            RecurrenceRule.TryParse(input, out rrule);
            return rrule;
        }

        /// <summary>
        /// Creates a recurrence rule instance from it's string representation, taking into account TimeZone offset.
        /// </summary>
        /// <param name="input">The string to parse.</param>
        /// <param name="timeZoneId">The TimeZoneID.</param>
        /// <returns>RecurrenceRule if the parsing succeeded or null (<strong>Nothing</strong> in Visual Basic) if the parsing failed.</returns>
        /// <remarks>
        /// See the <see cref="M:Telerik.Web.UI.RecurrenceRule.TryParse(System.String,Telerik.Web.UI.RecurrenceRule@)">TryParse</see> overload for more information and examples.
        /// </remarks>
        public static bool TryParse(string input, string timeZoneID, out RecurrenceRule rrule)
        {
            if (RecurrenceRule.TryParse(input, out rrule))
                RecurrenceRule.ConvertRecurrenceRuleToTimeZone(timeZoneID, rrule);
            return rrule != (RecurrenceRule)null;
        }

        private static void ConvertRecurrenceRuleToTimeZone(string timeZoneID, RecurrenceRule rrule)
        {
            rrule.Range.Start = TimeZoneInfoProvider.UtcToLocal(rrule.Range.Start, (ITimeZoneModel)TimeZoneInfoProvider.GetTimeZoneModelById(timeZoneID));
            if (rrule.Range.RecursUntil < DateTime.MaxValue)
                rrule.Range.RecursUntil = TimeZoneInfoProvider.UtcToLocal(rrule.Range.RecursUntil, (ITimeZoneModel)TimeZoneInfoProvider.GetTimeZoneModelById(timeZoneID));
            for (int index = 0; index < rrule.Exceptions.Count; ++index)
                rrule.Exceptions[index] = TimeZoneInfoProvider.UtcToLocal(rrule.Exceptions[index], (ITimeZoneModel)TimeZoneInfoProvider.GetTimeZoneModelById(timeZoneID));
        }

        private static void ParseRRule(string line, RecurrenceRange parsedRange, RecurrencePattern parsedPattern)
        {
            Match match1 = Regex.Match(line, "^(RRULE:)(.*)$", RegexOptions.IgnoreCase);
            if (!match1.Success)
                return;
            string input = match1.Groups[2].Value;
            Match match2 = Regex.Match(input, "FREQ=(HOURLY|DAILY|WEEKLY|MONTHLY|YEARLY)", RegexOptions.IgnoreCase);
            if (match2.Success)
                parsedPattern.Frequency = (RecurrenceFrequency)Enum.Parse(typeof(RecurrenceFrequency), match2.Groups[1].Value, true);
            Match match3 = Regex.Match(input, "COUNT=(\\d{1,4})", RegexOptions.IgnoreCase);
            if (match3.Success)
                parsedRange.MaxOccurrences = int.Parse(match3.Groups[1].Value);
            Match match4 = Regex.Match(input, "UNTIL=([\\w\\d]*)", RegexOptions.IgnoreCase);
            DateTime date;
            if (match4.Success && RecurrenceRule.TryParseDateTime(match4.Groups[1].Value, out date))
                parsedRange.RecursUntil = date;
            Match match5 = Regex.Match(input, "INTERVAL=(\\d{1,})", RegexOptions.IgnoreCase);
            if (match5.Success)
                parsedPattern.Interval = int.Parse(match5.Groups[1].Value);
            Match match6 = Regex.Match(input, "BYSETPOS=(-?\\d{1})", RegexOptions.IgnoreCase);
            if (match6.Success)
                parsedPattern.DayOrdinal = int.Parse(match6.Groups[1].Value);
            Match match7 = Regex.Match(input, "BYMONTHDAY=(\\d{1,2})", RegexOptions.IgnoreCase);
            if (match7.Success)
                parsedPattern.DayOfMonth = int.Parse(match7.Groups[1].Value);
            Match match8 = Regex.Match(input, "BYDAY=(-?\\d{1})?([\\w,]*)", RegexOptions.IgnoreCase);
            if (match8.Success)
            {
                if (!string.IsNullOrEmpty(match8.Groups[1].Value))
                    parsedPattern.DayOrdinal = int.Parse(match8.Groups[1].Value);
                RecurrenceDay mask;
                if (RecurrenceRule.TryParseDayOfWeekMask(match8.Groups[2].Value, out mask))
                    parsedPattern.DaysOfWeekMask = mask;
            }
            Match match9 = Regex.Match(input, "BYMONTH=(\\d{1,2})", RegexOptions.IgnoreCase);
            if (match9.Success)
                parsedPattern.Month = (RecurrenceMonth)Enum.Parse(typeof(RecurrenceMonth), match9.Groups[1].Value, true);
            Match match10 = Regex.Match(input, "WKST=([\\w,]*)", RegexOptions.IgnoreCase);
            if (!match10.Success)
                return;
            parsedPattern.FirstDayOfWeek = RecurrenceRule.ParseDayOfWeek(match10.Groups[1].Value);
        }

        private static void ParseExceptions(string line, ICollection<DateTime> parsedExceptions)
        {
            Match match = Regex.Match(line, "^(EXDATE):(.*)$", RegexOptions.IgnoreCase);
            if (!match.Success)
                return;
            string str = match.Groups[2].Value;
            char[] chArray = new char[1] { ',' };
            foreach (string input in str.Split(chArray))
            {
                DateTime date;
                if (RecurrenceRule.TryParseDateTime(input, out date))
                    parsedExceptions.Add(date);
            }
        }

        protected static bool IsValidValue<T>(T value, T minValue, T maxValue) where T : IComparable
        {
            if (value.CompareTo((object)minValue) < 0)
                return value.CompareTo((object)maxValue) <= 0;
            return true;
        }

        protected static void ValidateValue<T>(string name, T value, T minValue, T maxValue) where T : IComparable
        {
            if (value.CompareTo((object)minValue) <= 0 || value.CompareTo((object)maxValue) >= 0)
                throw new ArgumentOutOfRangeException(string.Format("{0} is out of range. Actual value is {1}, allowed range is [{2} - {3}]", (object)name, (object)value, (object)minValue, (object)maxValue));
        }

        private static bool TryParseDateTime(string input, out DateTime date)
        {
            Match match = Regex.Match(input, "^(\\d{4})(\\d{2})(\\d{2})T(\\d{2})(\\d{2})(\\d{2})(Z)(.*)$", RegexOptions.IgnoreCase);
            if (match.Success)
            {
                int year = int.Parse(match.Groups[1].Value);
                int month = int.Parse(match.Groups[2].Value);
                int day = int.Parse(match.Groups[3].Value);
                int hour = int.Parse(match.Groups[4].Value);
                int minute = int.Parse(match.Groups[5].Value);
                int second = int.Parse(match.Groups[6].Value);
                if (true & RecurrenceRule.IsValidValue<int>(year, 1900, 2900) & RecurrenceRule.IsValidValue<int>(month, 1, 12) & RecurrenceRule.IsValidValue<int>(day, 1, 31) & RecurrenceRule.IsValidValue<int>(hour, 0, 23) & RecurrenceRule.IsValidValue<int>(minute, 0, 59) & RecurrenceRule.IsValidValue<int>(second, 0, 59))
                {
                    date = new DateTime(year, month, day, hour, minute, second, 0, DateTimeKind.Utc);
                    return true;
                }
            }
            date = DateTime.MinValue;
            return false;
        }

        private static bool TryParseDayOfWeekMask(string input, out RecurrenceDay mask)
        {
            Dictionary<string, RecurrenceDay> dictionary = new Dictionary<string, RecurrenceDay>();
            dictionary.Add("MO", RecurrenceDay.Monday);
            dictionary.Add("TU", RecurrenceDay.Tuesday);
            dictionary.Add("WE", RecurrenceDay.Wednesday);
            dictionary.Add("TH", RecurrenceDay.Thursday);
            dictionary.Add("FR", RecurrenceDay.Friday);
            dictionary.Add("SA", RecurrenceDay.Saturday);
            dictionary.Add("SU", RecurrenceDay.Sunday);
            mask = RecurrenceDay.None;
            string str = input;
            char[] chArray = new char[1] { ',' };
            foreach (string key in str.Split(chArray))
            {
                if (!dictionary.ContainsKey(key))
                    return false;
                mask |= dictionary[key];
            }
            return true;
        }

        private static DayOfWeek ParseDayOfWeek(string input)
        {
            Dictionary<string, DayOfWeek> dictionary = new Dictionary<string, DayOfWeek>();
            dictionary.Add("MO", DayOfWeek.Monday);
            dictionary.Add("TU", DayOfWeek.Tuesday);
            dictionary.Add("WE", DayOfWeek.Wednesday);
            dictionary.Add("TH", DayOfWeek.Thursday);
            dictionary.Add("FR", DayOfWeek.Friday);
            dictionary.Add("SA", DayOfWeek.Saturday);
            dictionary.Add("SU", DayOfWeek.Sunday);
            if (dictionary.ContainsKey(input))
                return dictionary[input];
            return DayOfWeek.Sunday;
        }

        /// <summary>Specifies the effective range for evaluating occurrences.</summary>
        /// <exception cref="T:System.ArgumentException" caption="">End date is before Start date.</exception>
        /// <remarks>
        ///     The range is inclusive. To clear the effective range call
        ///     <see cref="M:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule.ClearEffectiveRange" />.
        /// </remarks>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class EffectiveRangeExample
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 10;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             HourlyRecurrenceRule rrule = new HourlyRecurrenceRule(2, range);
        /// 
        ///             // Limits the effective range.
        ///             rrule.SetEffectiveRange(Convert.ToDateTime("6/1/2007 5:00 PM"), Convert.ToDateTime("6/1/2007 8:00 PM"));
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 6/1/2007 5:30:00 PM
        ///  2: 6/1/2007 7:30:00 PM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class EffectiveRangeExample
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 10
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             Dim rrule As New HourlyRecurrenceRule(2, range)
        /// 
        ///             ' Limits the effective range.
        ///             rrule.SetEffectiveRange(Convert.ToDateTime("6/1/2007 5:00 PM"), Convert.ToDateTime("6/1/2007 8:00 PM"))
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurs at the following times:
        /// ' 1: 6/1/2007 5:30:00 PM
        /// ' 2: 6/1/2007 7:30:00 PM
        /// '
        ///     </code>
        /// </example>
        /// <seealso cref="M:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule.ClearEffectiveRange">ClearEffectiveRange Method</seealso>
        /// <param name="start">The starting date of the effective range.</param>
        /// <param name="end">The ending date of the effective range.</param>
        public void SetEffectiveRange(DateTime start, DateTime end)
        {
            if (end < start)
                throw new ArgumentException("The end date is before the start date.");
            this._effectiveStart = DateHelper.AssumeUtc(start);
            this._effectiveEnd = DateHelper.AssumeUtc(end);
        }

        /// <summary>Clears the effective range set by calling <see cref="M:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule.SetEffectiveRange(System.DateTime,System.DateTime)" />.</summary>
        /// <remarks>If no effective range was set, calling this method has no effect.</remarks>
        /// <seealso cref="M:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule.SetEffectiveRange(System.DateTime,System.DateTime)">SetEffectiveRange Method</seealso>
        public void ClearEffectiveRange()
        {
            this._effectiveStart = DateTime.MinValue;
            this._effectiveEnd = DateTime.MaxValue;
        }

        /// <summary>Converts the recurrence rule to it's equivalent string representation.</summary>
        /// <returns>The string representation of this recurrence rule.</returns>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class ParsingExample
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 10;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             HourlyRecurrenceRule rrule = new HourlyRecurrenceRule(2, range);
        /// 
        ///             // Prints the string representation of the recurrence rule:
        ///             string rruleAsString = rrule.ToString();
        ///             Console.WriteLine("Recurrence rule:\n\n{0}\n", rruleAsString);
        /// 
        ///             // The string representation can be stored in a database, etc.
        ///             // ...
        /// 
        ///             // Then it can be reconstructed using TryParse method:
        ///             RecurrenceRule parsedRule;
        ///             RecurrenceRule.TryParse(rruleAsString, out parsedRule);
        ///             Console.WriteLine("After parsing (should be the same):\n\n{0}", parsedRule);
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Recurrence rule:
        /// 
        /// DTSTART:20070601T123000Z
        /// DTEND:20070601T130000Z
        /// RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// 
        /// 
        /// After parsing (should be the same):
        /// 
        /// DTSTART:20070601T123000Z
        /// DTEND:20070601T130000Z
        /// RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class ParsingExample
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 10
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment every 2 hours.
        ///             Dim rrule As New HourlyRecurrenceRule(2, range)
        /// 
        ///             ' Prints the string representation of the recurrence rule:
        ///             Dim rruleAsString As String = rrule.ToString()
        ///             Console.WriteLine("Recurrence rule:" &amp; Chr(10) &amp; "" &amp; Chr(10) &amp; "{0}" &amp; Chr(10) &amp; "", rruleAsString)
        /// 
        ///             ' The string representation can be stored in a database, etc.
        ///             ' ...
        /// 
        ///             ' Then it can be reconstructed using TryParse method:
        ///             Dim parsedRule As RecurrenceRule
        ///             RecurrenceRule.TryParse(rruleAsString, parsedRule)
        ///             Console.WriteLine("After parsing (should be the same):" &amp; Chr(10) &amp; "" &amp; Chr(10) &amp; "{0}", parsedRule)
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Recurrence rule:
        /// '
        /// 'DTSTART:20070601T123000Z
        /// 'DTEND:20070601T130000Z
        /// 'RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// '
        /// '
        /// 'After parsing (should be the same):
        /// '
        /// 'DTSTART:20070601T123000Z
        /// 'DTEND:20070601T130000Z
        /// 'RRULE:FREQ=HOURLY;COUNT=10;INTERVAL=2;
        /// '
        ///     </code>
        /// </example>
        /// <remarks>
        /// The string representation is based on the iCalendar data format (RFC
        /// 2445).
        /// </remarks>
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            DateTime date = this.ruleRange.Start.Add(this.ruleRange.EventDuration);
            stringBuilder.AppendFormat("DTSTART:{0}\r\n", (object)RecurrenceRule.FormatDateTime(this.ruleRange.Start, true, true));
            stringBuilder.AppendFormat("DTEND:{0}\r\n", (object)RecurrenceRule.FormatDateTime(date, true, true));
            stringBuilder.AppendFormat("RRULE:{0}\r\n", (object)this.FormatRRule());
            stringBuilder.Append(this.FormatExceptions());
            return stringBuilder.ToString();
        }

        public string ToString(TimeZoneInfoProvider provider)
        {
            StringBuilder stringBuilder = new StringBuilder();
            DateTime utc = this.ruleRange.Start.Add(this.ruleRange.EventDuration);
            stringBuilder.Append("DTSTART;");
            stringBuilder.AppendFormat("TZID=\"{0}\":{1}", (object)provider.OperationTimeZone.TimeZoneId, (object)RecurrenceRule.FormatDateTime(provider.UtcToLocal(this.ruleRange.Start), true, false));
            stringBuilder.Append("\r\n");
            stringBuilder.Append("DTEND;");
            stringBuilder.AppendFormat("TZID=\"{0}\":{1}", (object)provider.OperationTimeZone.TimeZoneId, (object)RecurrenceRule.FormatDateTime(provider.UtcToLocal(utc), true, false));
            stringBuilder.Append("\r\n");
            stringBuilder.AppendFormat("RRULE:{0}\r\n", (object)this.FormatRRule());
            stringBuilder.Append(this.FormatExceptions());
            return stringBuilder.ToString();
        }

        /// <summary>Overriden. Returns the hash code for this instance.</summary>
        /// <returns>The hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Pattern.GetHashCode() ^ this.Range.GetHashCode();
        }

        /// <summary>
        /// Overloaded. Overridden. Returns a value indicating whether this instance is equal
        /// to a specified object.
        /// </summary>
        /// <returns>
        /// 	<strong>true</strong> if <i>value</i> is an instance of
        ///     <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule" /> and equals the value of this instance;
        ///     otherwise, <b>false</b>.
        /// </returns>
        /// <param name="obj">An object to compare with this instance.</param>
        public override bool Equals(object obj)
        {
            RecurrenceRule other = obj as RecurrenceRule;
            if (obj == null)
                return false;
            return this.Equals(other);
        }

        /// <summary>
        ///     Overloaded. Overridden. Returns a value indicating whether this instance is equal
        ///     to a specified <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule" /> object.
        /// </summary>
        /// <returns>
        /// 	<strong>true</strong> if <i>value</i> equals the value of this instance;
        /// otherwise, <b>false</b>.
        /// </returns>
        /// <param name="other">An <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRule" /> object to compare with this instance.</param>
        public bool Equals(RecurrenceRule other)
        {
            if (other == (RecurrenceRule)null || !(this.Pattern == other.Pattern))
                return false;
            return this.Range == other.Range;
        }

        protected virtual DateTime GetOccurrenceStart(int index)
        {
            throw new InvalidOperationException("Must override GetOccurrenceStart(int index).");
        }

        protected virtual bool MatchAdvancedPattern(DateTime start)
        {
            throw new InvalidOperationException("Must override MatchAdvancedPattern(DateTime start).");
        }

        protected bool MatchDayOfWeekMask(DateTime start)
        {
            RecurrenceDay recurrenceDay = (RecurrenceDay)Enum.Parse(typeof(RecurrenceDay), start.DayOfWeek.ToString());
            return (recurrenceDay & this.rulePattern.DaysOfWeekMask) == recurrenceDay;
        }

        protected bool MatchDayOrdinal(DateTime date)
        {
            if (this.rulePattern.DayOrdinal == 0)
                return true;
            if (0 >= this.rulePattern.DayOrdinal)
                return this.MatchDayOrdinalNegative(date);
            return this.MatchDayOrdinalPositive(date);
        }

        private static string FormatDateTime(DateTime date, bool containsTime, bool utc = true)
        {
            StringBuilder stringBuilder = new StringBuilder();
            DateTime universalTime = date.ToUniversalTime();
            stringBuilder.AppendFormat("{0:00}{1:00}{2:00}", (object)universalTime.Year, (object)universalTime.Month, (object)universalTime.Day);
            if (containsTime)
                stringBuilder.AppendFormat("T{0:00}{1:00}{2:00}", (object)universalTime.Hour, (object)universalTime.Minute, (object)universalTime.Second);
            if (utc)
                stringBuilder.Append("Z");
            return stringBuilder.ToString();
        }

        private string FormatRRule()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("FREQ={0};", (object)this.rulePattern.Frequency.ToString().ToUpperInvariant());
            if (0 < this.ruleRange.MaxOccurrences && this.ruleRange.MaxOccurrences < int.MaxValue)
                stringBuilder.AppendFormat("COUNT={0};", (object)this.ruleRange.MaxOccurrences);
            else if (DateTime.MinValue < this.ruleRange.RecursUntil && this.ruleRange.RecursUntil < DateTime.MaxValue)
                stringBuilder.AppendFormat("UNTIL={0};", (object)RecurrenceRule.FormatDateTime(this.ruleRange.RecursUntil, true, true));
            if (0 < this.rulePattern.Interval)
                stringBuilder.AppendFormat("INTERVAL={0};", (object)this.rulePattern.Interval);
            if (this.rulePattern.DayOrdinal != 0)
                stringBuilder.AppendFormat("BYSETPOS={0};", (object)this.rulePattern.DayOrdinal);
            if (0 < this.rulePattern.DayOfMonth)
                stringBuilder.AppendFormat("BYMONTHDAY={0};", (object)this.rulePattern.DayOfMonth);
            if (this.rulePattern.DaysOfWeekMask != RecurrenceDay.None)
            {
                Dictionary<RecurrenceDay, string> dictionary = new Dictionary<RecurrenceDay, string>();
                dictionary.Add(RecurrenceDay.Monday, "MO");
                dictionary.Add(RecurrenceDay.Tuesday, "TU");
                dictionary.Add(RecurrenceDay.Wednesday, "WE");
                dictionary.Add(RecurrenceDay.Thursday, "TH");
                dictionary.Add(RecurrenceDay.Friday, "FR");
                dictionary.Add(RecurrenceDay.Saturday, "SA");
                dictionary.Add(RecurrenceDay.Sunday, "SU");
                List<string> stringList = new List<string>();
                foreach (RecurrenceDay key in dictionary.Keys)
                {
                    if ((this.rulePattern.DaysOfWeekMask & key) == key)
                        stringList.Add(dictionary[key]);
                }
                stringBuilder.AppendFormat("BYDAY={0};", (object)string.Join(",", stringList.ToArray()));
            }
            if (this.rulePattern.Month != RecurrenceMonth.None)
                stringBuilder.AppendFormat("BYMONTH={0};", (object)this.rulePattern.Month);
            if (this.rulePattern.FirstDayOfWeek != DayOfWeek.Sunday)
            {
                string str = this.rulePattern.FirstDayOfWeek.ToString().ToUpperInvariant().Substring(0, 2);
                stringBuilder.AppendFormat("WKST={0};", (object)str);
            }
            string str1 = stringBuilder.ToString();
            if ((int)str1[str1.Length - 1] == 59)
                str1 = str1.Substring(0, str1.Length - 1);
            return str1;
        }

        private string FormatExceptions()
        {
            if (this._exceptions.Count == 0)
                return string.Empty;
            string[] strArray = new string[this._exceptions.Count];
            for (int index = 0; index < this._exceptions.Count; ++index)
                strArray[index] = RecurrenceRule.FormatDateTime(this._exceptions[index], true, true);
            return string.Format("EXDATE:{0}\r\n", (object)string.Join(",", strArray));
        }

        private bool MatchDayOrdinalPositive(DateTime date)
        {
            DateTime start = DateHelper.GetFirstDayOfMonth(date);
            int num = 0;
            for (; start <= date; start = start.AddDays(1.0))
            {
                if (this.MatchDayOfWeekMask(start))
                    ++num;
            }
            return num == this.rulePattern.DayOrdinal;
        }

        private bool MatchDayOrdinalNegative(DateTime date)
        {
            DateTime start = DateHelper.GetLastDayOfMonth(date).AddHours(23.0).AddMinutes(59.0).AddSeconds(59.0);
            int num = 0;
            for (; date < start; start = start.AddDays(-1.0))
            {
                if (this.MatchDayOfWeekMask(start))
                    --num;
            }
            return num == this.rulePattern.DayOrdinal;
        }

        /// <summary>
        /// Populates a <b>SerializationInfo</b> with the data needed to serialize this
        /// object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("RRULE", (object)this.ToString());
        }

        private class EmptyRecurrenceRule : RecurrenceRule
        {
        }
    }
}
