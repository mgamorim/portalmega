﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.YearlyRecurrenceRule
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using System.Runtime.Serialization;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduling;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>Occurrences of this rule repeat on a yearly basis.</summary>
    /// <example>
    /// 	<code lang="CS">
    /// using System;
    /// using Telerik.Web.UI;
    /// 
    /// namespace RecurrenceExamples
    /// {
    ///     class YearlyRecurrenceRuleExample1
    ///     {
    ///         static void Main()
    ///         {
    ///             // Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
    ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"),
    ///                 Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment");
    /// 
    ///             // Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
    ///             RecurrenceRange range = new RecurrenceRange();
    ///             range.Start = recurringAppointment.Start;
    ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
    ///             range.MaxOccurrences = 5;
    /// 
    ///             // Creates a recurrence rule to repeat the appointment on the 1th of April each year.
    ///             YearlyRecurrenceRule rrule = new YearlyRecurrenceRule(RecurrenceMonth.April, 1, range);
    /// 
    ///             Console.WriteLine("Appointment occurrs at the following times: ");
    ///             int ix = 0;
    ///             foreach (DateTime occurrence in rrule.Occurrences)
    ///             {
    ///                 ix = ix + 1;
    ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
    ///             }
    ///         }
    ///     }
    /// }
    /// 
    /// /*
    /// This example produces the following results:
    /// 
    /// Appointment occurrs at the following times:
    ///  1: 4/1/2007 10:00:00 AM
    ///  2: 4/1/2008 10:00:00 AM
    ///  3: 4/1/2009 10:00:00 AM
    ///  4: 4/1/2010 10:00:00 AM
    ///  5: 4/1/2011 10:00:00 AM
    /// */
    ///     </code>
    /// 	<code lang="VB">
    /// Imports System
    /// Imports Telerik.Web.UI
    /// 
    /// Namespace RecurrenceExamples
    ///     Class YearlyRecurrenceRuleExample1
    ///         Shared Sub Main()
    ///             ' Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
    ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"), Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment")
    /// 
    ///             ' Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
    ///             Dim range As New RecurrenceRange()
    ///             range.Start = recurringAppointment.Start
    ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
    ///             range.MaxOccurrences = 5
    /// 
    ///             ' Creates a recurrence rule to repeat the appointment on the 1th of April each year.
    ///             Dim rrule As New YearlyRecurrenceRule(RecurrenceMonth.April, 1, range)
    /// 
    ///             Console.WriteLine("Appointment occurrs at the following times: ")
    ///             Dim ix As Integer = 0
    ///             For Each occurrence As DateTime In rrule.Occurrences
    ///                 ix = ix + 1
    ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
    ///             Next
    ///         End Sub
    ///     End Class
    /// End Namespace
    /// 
    /// '
    /// 'This example produces the following results:
    /// '
    /// 'Appointment occurrs at the following times:
    /// ' 1: 4/1/2007 10:00:00 AM
    /// ' 2: 4/1/2008 10:00:00 AM
    /// ' 3: 4/1/2009 10:00:00 AM
    /// ' 4: 4/1/2010 10:00:00 AM
    /// ' 5: 4/1/2011 10:00:00 AM
    /// '
    ///     </code>
    /// </example>
    [Serializable]
    public class YearlyRecurrenceRule : RecurrenceRule
    {
        /// <summary>Gets the day of month on which the event recurs.</summary>
        /// <value>The day of month on which the event recurs.</value>
        public int DayOfMonth
        {
            get
            {
                return this.rulePattern.DayOfMonth;
            }
        }

        /// <summary>
        /// Gets the day ordinal modifier. See <see cref="P:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern.DayOrdinal" /> for additional information.
        /// </summary>
        /// <value>The day ordinal modifier.</value>
        public int DayOrdinal
        {
            get
            {
                return this.rulePattern.DayOrdinal;
            }
        }

        /// <summary>Gets the month in which the event recurs.</summary>
        /// <value>The month in which the event recurs.</value>
        public RecurrenceMonth Month
        {
            get
            {
                return this.rulePattern.Month;
            }
        }

        /// <summary>
        /// Gets the bit mask that specifies the week days on which the event
        /// recurs.
        /// </summary>
        /// <seealso cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums.RecurrenceDay">RecurrenceDay Enumeration</seealso>
        /// <remarks>
        ///     For additional information on how to create masks see the
        ///     <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums.RecurrenceDay" /> documentation.
        /// </remarks>
        /// <value>A bit mask that specifies the week days on which the event recurs.</value>
        public RecurrenceDay DaysOfWeekMask
        {
            get
            {
                return this.rulePattern.DaysOfWeekMask;
            }
        }

        /// <summary>Gets the interval (in years) between the occurrences.</summary>
        /// <value>The interval (in years) between the occurrences.</value>
        public int Interval
        {
            get
            {
                return this.rulePattern.Interval;
            }
        }

        private YearlyRecurrenceRule(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.YearlyRecurrenceRule" /> class.
        /// </summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class YearlyRecurrenceRuleExample1
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"),
        ///                 Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 5;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment on the 1th of April each year.
        ///             YearlyRecurrenceRule rrule = new YearlyRecurrenceRule(RecurrenceMonth.April, 1, range);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 4/1/2007 10:00:00 AM
        ///  2: 4/1/2008 10:00:00 AM
        ///  3: 4/1/2009 10:00:00 AM
        ///  4: 4/1/2010 10:00:00 AM
        ///  5: 4/1/2011 10:00:00 AM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class YearlyRecurrenceRuleExample1
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"), Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 5
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment on the 1th of April each year.
        ///             Dim rrule As New YearlyRecurrenceRule(RecurrenceMonth.April, 1, range)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 4/1/2007 10:00:00 AM
        /// ' 2: 4/1/2008 10:00:00 AM
        /// ' 3: 4/1/2009 10:00:00 AM
        /// ' 4: 4/1/2010 10:00:00 AM
        /// ' 5: 4/1/2011 10:00:00 AM
        /// '
        ///     </code>
        /// </example>
        /// <param name="month">The month in which the event recurs.</param>
        /// <param name="dayOfMonth">The day of month on which the event recurs.</param>
        /// <param name="range">The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> instance that specifies the range of this rule.</param>
        public YearlyRecurrenceRule(RecurrenceMonth month, int dayOfMonth, RecurrenceRange range)
          : this(month, dayOfMonth, 0, RecurrenceDay.EveryDay, range, 1)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.YearlyRecurrenceRule" /> class.
        /// </summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class YearlyRecurrenceRuleExample2
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"),
        ///                 Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 5;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment on the second monday of April each year.
        ///             YearlyRecurrenceRule rrule = new YearlyRecurrenceRule(2, RecurrenceMonth.April, RecurrenceDay.Monday, range);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 4/9/2007 10:00:00 AM
        ///  2: 4/14/2008 10:00:00 AM
        ///  3: 4/13/2009 10:00:00 AM
        ///  4: 4/12/2010 10:00:00 AM
        ///  5: 4/11/2011 10:00:00 AM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class YearlyRecurrenceRuleExample2
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"), Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 5
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment on the second monday of April each year.
        ///             Dim rrule As New YearlyRecurrenceRule(2, RecurrenceMonth.April, RecurrenceDay.Monday, range)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 4/9/2007 10:00:00 AM
        /// ' 2: 4/14/2008 10:00:00 AM
        /// ' 3: 4/13/2009 10:00:00 AM
        /// ' 4: 4/12/2010 10:00:00 AM
        /// ' 5: 4/11/2011 10:00:00 AM
        /// '
        ///     </code>
        /// </example>
        /// <param name="dayOrdinal">The day ordinal modifier. See <see cref="P:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern.DayOrdinal" /> for additional information.</param>
        /// <param name="month">The month in which the event recurs.</param>
        /// <param name="daysOfWeekMask">A bit mask that specifies the week days on which the event recurs.</param>
        /// <param name="range">The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> instance that specifies the range of this rule.</param>
        public YearlyRecurrenceRule(int dayOrdinal, RecurrenceMonth month, RecurrenceDay daysOfWeekMask, RecurrenceRange range)
          : this(month, -1, dayOrdinal, daysOfWeekMask, range, 1)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.YearlyRecurrenceRule" /> class.
        /// </summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class YearlyRecurrenceRuleExample3
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"),
        ///                 Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 5;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment on the 1th of April every other year.
        ///             YearlyRecurrenceRule rrule = new YearlyRecurrenceRule(RecurrenceMonth.April, 1, range, 2);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 4/1/2007 10:00:00 AM
        ///  2: 4/1/2009 10:00:00 AM
        ///  3: 4/1/2011 10:00:00 AM
        ///  4: 4/1/2013 10:00:00 AM
        ///  5: 4/1/2015 10:00:00 AM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class YearlyRecurrenceRuleExample3
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"), Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 5
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment on the 1th of April every other year.
        ///             Dim rrule As New YearlyRecurrenceRule(RecurrenceMonth.April, 1, range, 2)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 4/1/2007 10:00:00 AM
        /// ' 2: 4/1/2009 10:00:00 AM
        /// ' 3: 4/1/2011 10:00:00 AM
        /// ' 4: 4/1/2013 10:00:00 AM
        /// ' 5: 4/1/2015 10:00:00 AM
        /// '
        ///     </code>
        /// </example>
        /// <param name="month">The month in which the event recurs.</param>
        /// <param name="dayOfMonth">The day of month on which the event recurs.</param>
        /// <param name="range">The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> instance that specifies the range of this rule.</param>
        /// <param name="interval">The interval (in years) between the occurrences.</param>
        public YearlyRecurrenceRule(RecurrenceMonth month, int dayOfMonth, RecurrenceRange range, int interval)
          : this(month, dayOfMonth, 0, RecurrenceDay.EveryDay, range, interval)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.YearlyRecurrenceRule" /> class.
        /// </summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class YearlyRecurrenceRuleExample4
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"),
        ///                 Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 5;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment on the second monday of April every other year.
        ///             YearlyRecurrenceRule rrule = new YearlyRecurrenceRule(2, RecurrenceMonth.April, RecurrenceDay.Monday, range, 2);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 4/9/2007 10:00:00 AM
        ///  2: 4/13/2009 10:00:00 AM
        ///  3: 4/11/2011 10:00:00 AM
        ///  4: 4/8/2013 10:00:00 AM
        ///  5: 4/13/2015 10:00:00 AM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class YearlyRecurrenceRuleExample4
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 4/1/2007 10:00 AM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("4/1/2007 10:00 AM"), Convert.ToDateTime("4/1/2007 10:30 AM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 5 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 5
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment on the second monday of April every other year.
        ///             Dim rrule As New YearlyRecurrenceRule(2, RecurrenceMonth.April, RecurrenceDay.Monday, range, 2)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 4/9/2007 10:00:00 AM
        /// ' 2: 4/13/2009 10:00:00 AM
        /// ' 3: 4/11/2011 10:00:00 AM
        /// ' 4: 4/8/2013 10:00:00 AM
        /// ' 5: 4/13/2015 10:00:00 AM
        /// '
        ///     </code>
        /// </example>
        /// <param name="dayOrdinal">The day ordinal modifier. See <see cref="P:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern.DayOrdinal" /> for additional information.</param>
        /// <param name="month">The month in which the event recurs.</param>
        /// <param name="daysOfWeekMask">A bit mask that specifies the week days on which the event recurs.</param>
        /// <param name="range">The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> instance that specifies the range of this rule.</param>
        /// <param name="interval">The interval (in years) between the occurrences.</param>
        public YearlyRecurrenceRule(int dayOrdinal, RecurrenceMonth month, RecurrenceDay daysOfWeekMask, RecurrenceRange range, int interval)
          : this(month, -1, dayOrdinal, daysOfWeekMask, range, interval)
        {
        }

        private YearlyRecurrenceRule(RecurrenceMonth month, int dayOfMonth, int dayOrdinal, RecurrenceDay daysOfWeekMask, RecurrenceRange range, int interval)
        {
            this.rulePattern.Frequency = RecurrenceFrequency.Yearly;
            this.rulePattern.Interval = interval;
            this.rulePattern.DaysOfWeekMask = daysOfWeekMask;
            this.rulePattern.DayOfMonth = dayOfMonth;
            this.rulePattern.DayOrdinal = dayOrdinal;
            this.rulePattern.Month = month;
            this.ruleRange = range;
        }

        protected override DateTime GetOccurrenceStart(int index)
        {
            int month = (int)this.rulePattern.Month;
            DateTime dateTime = this.ruleRange.Start;
            if (dateTime.Month != month)
                dateTime = new DateTime(dateTime.Month < month ? dateTime.Year : dateTime.Year + this.rulePattern.Interval, month, 1, dateTime.Hour, dateTime.Minute, dateTime.Second, 0, dateTime.Kind);
            if (0 < this.rulePattern.DayOfMonth)
            {
                int year = dateTime.Year + index * this.rulePattern.Interval;
                int dayOfMonth = this.rulePattern.DayOfMonth;
                if (dateTime.Month == month && dayOfMonth < dateTime.Day)
                    ++year;
                int day = DateHelper.GetLastDayOfMonth(new DateTime(year, month, 1)).Day;
                return new DateTime(year, month, Math.Min(this.rulePattern.DayOfMonth, day), dateTime.Hour, dateTime.Minute, dateTime.Second, 0, dateTime.Kind);
            }
            for (int index1 = 0; index1 < index; ++index1)
            {
                int year = dateTime.Year + this.rulePattern.Interval;
                dateTime = dateTime.AddDays(1.0);
                if (dateTime.Month != month)
                    dateTime = new DateTime(year, month, 1, dateTime.Hour, dateTime.Minute, dateTime.Second, 0, dateTime.Kind);
            }
            return dateTime;
        }

        protected override bool MatchAdvancedPattern(DateTime start)
        {
            return (0 >= this.rulePattern.DayOfMonth || start.Day == this.rulePattern.DayOfMonth || this.rulePattern.DayOfMonth > DateHelper.GetLastDayOfMonth(start).Day) && (this.MatchDayOfWeekMask(start) && this.MatchDayOrdinal(start));
        }
    }
}
