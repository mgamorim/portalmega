﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.MonthlyRecurrenceRule
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using System.Runtime.Serialization;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduling;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler
{
    /// <summary>Occurrences of this rule repeat on a monthly basis.</summary>
    /// <example>
    /// 	<code lang="CS">
    /// using System;
    /// using Telerik.Web.UI;
    /// 
    /// namespace RecurrenceExamples
    /// {
    ///     class MonthlyRecurrenceRuleExample1
    ///     {
    ///         static void Main()
    ///         {
    ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
    ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
    ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
    /// 
    ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
    ///             RecurrenceRange range = new RecurrenceRange();
    ///             range.Start = recurringAppointment.Start;
    ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
    ///             range.MaxOccurrences = 5;
    /// 
    ///             // Creates a recurrence rule to repeat the appointment on the 5th day of every month.
    ///             MonthlyRecurrenceRule rrule = new MonthlyRecurrenceRule(5, 1, range);
    /// 
    ///             Console.WriteLine("Appointment occurrs at the following times: ");
    ///             int ix = 0;
    ///             foreach (DateTime occurrence in rrule.Occurrences)
    ///             {
    ///                 ix = ix + 1;
    ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
    ///             }
    ///         }
    ///     }
    /// }
    /// 
    /// /*
    /// This example produces the following results:
    /// 
    /// Appointment occurrs at the following times:
    ///  1: 6/5/2007 3:30:00 PM
    ///  2: 7/5/2007 3:30:00 PM
    ///  3: 8/5/2007 3:30:00 PM
    ///  4: 9/5/2007 3:30:00 PM
    ///  5: 10/5/2007 3:30:00 PM
    /// */
    ///     </code>
    /// 	<code lang="VB">
    /// Imports System
    /// Imports Telerik.Web.UI
    /// 
    /// Namespace RecurrenceExamples
    ///     Class MonthlyRecurrenceRuleExample1
    ///         Shared Sub Main()
    ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
    ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
    /// 
    ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
    ///             Dim range As New RecurrenceRange()
    ///             range.Start = recurringAppointment.Start
    ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
    ///             range.MaxOccurrences = 5
    /// 
    ///             ' Creates a recurrence rule to repeat the appointment on the 5th day of every month.
    ///             Dim rrule As New MonthlyRecurrenceRule(5, 1, range)
    /// 
    ///             Console.WriteLine("Appointment occurrs at the following times: ")
    ///             Dim ix As Integer = 0
    ///             For Each occurrence As DateTime In rrule.Occurrences
    ///                 ix = ix + 1
    ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
    ///             Next
    ///         End Sub
    ///     End Class
    /// End Namespace
    /// 
    /// '
    /// 'This example produces the following results:
    /// '
    /// 'Appointment occurrs at the following times:
    /// ' 1: 6/5/2007 3:30:00 PM
    /// ' 2: 7/5/2007 3:30:00 PM
    /// ' 3: 8/5/2007 3:30:00 PM
    /// ' 4: 9/5/2007 3:30:00 PM
    /// ' 5: 10/5/2007 3:30:00 PM
    /// '
    ///     </code>
    /// </example>
    [Serializable]
    public class MonthlyRecurrenceRule : RecurrenceRule
    {
        /// <summary>Gets the day of month on which the event recurs.</summary>
        /// <value>The day of month on which the event recurs.</value>
        public int DayOfMonth
        {
            get
            {
                return this.rulePattern.DayOfMonth;
            }
        }

        /// <summary>
        /// Gets the day ordinal modifier. See <see cref="P:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern.DayOrdinal" /> for additional information.
        /// </summary>
        /// <seealso cref="P:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern.DayOrdinal" />
        /// <value>The day ordinal modifier.</value>
        public int DayOrdinal
        {
            get
            {
                return this.rulePattern.DayOrdinal;
            }
        }

        /// <summary>Gets the month in which the event recurs.</summary>
        /// <value>The month in which the event recurs.</value>
        public RecurrenceMonth Month
        {
            get
            {
                return this.rulePattern.Month;
            }
        }

        /// <summary>Gets the interval (in months) between the occurrences.</summary>
        /// <value>The interval (in months) between the occurrences.</value>
        public int Interval
        {
            get
            {
                return this.rulePattern.Interval;
            }
        }

        private MonthlyRecurrenceRule(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.MonthlyRecurrenceRule" /> class.
        /// </summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class MonthlyRecurrenceRuleExample1
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 5;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment on the 5th day of every month.
        ///             MonthlyRecurrenceRule rrule = new MonthlyRecurrenceRule(5, 1, range);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 6/5/2007 3:30:00 PM
        ///  2: 7/5/2007 3:30:00 PM
        ///  3: 8/5/2007 3:30:00 PM
        ///  4: 9/5/2007 3:30:00 PM
        ///  5: 10/5/2007 3:30:00 PM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class MonthlyRecurrenceRuleExample1
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 5
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment on the 5th day of every month.
        ///             Dim rrule As New MonthlyRecurrenceRule(5, 1, range)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 6/5/2007 3:30:00 PM
        /// ' 2: 7/5/2007 3:30:00 PM
        /// ' 3: 8/5/2007 3:30:00 PM
        /// ' 4: 9/5/2007 3:30:00 PM
        /// ' 5: 10/5/2007 3:30:00 PM
        /// '
        ///     </code>
        /// </example>
        /// <param name="dayOfMonth">The day of month on which the event recurs.</param>
        /// <param name="interval">The interval (in months) between the occurrences.</param>
        /// <param name="range">The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> instance that specifies the range of this rule.</param>
        public MonthlyRecurrenceRule(int dayOfMonth, int interval, RecurrenceRange range)
          : this(dayOfMonth, interval, 0, RecurrenceDay.None, range)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.MonthlyRecurrenceRule" /> class.
        /// </summary>
        /// <example>
        /// 	<code lang="CS">
        /// using System;
        /// using Telerik.Web.UI;
        /// 
        /// namespace RecurrenceExamples
        /// {
        ///     class MonthlyRecurrenceRuleExample2
        ///     {
        ///         static void Main()
        ///         {
        ///             // Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Appointment recurringAppointment = new Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"),
        ///                 Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment");
        /// 
        ///             // Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             RecurrenceRange range = new RecurrenceRange();
        ///             range.Start = recurringAppointment.Start;
        ///             range.EventDuration = recurringAppointment.End - recurringAppointment.Start;
        ///             range.MaxOccurrences = 5;
        /// 
        ///             // Creates a recurrence rule to repeat the appointment on the last monday of every two months.
        ///             MonthlyRecurrenceRule rrule = new MonthlyRecurrenceRule(-1, RecurrenceDay.Monday, 2, range);
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ");
        ///             int ix = 0;
        ///             foreach (DateTime occurrence in rrule.Occurrences)
        ///             {
        ///                 ix = ix + 1;
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime());
        ///             }
        ///         }
        ///     }
        /// }
        /// 
        /// /*
        /// This example produces the following results:
        /// 
        /// Appointment occurrs at the following times:
        ///  1: 6/25/2007 3:30:00 PM
        ///  2: 8/27/2007 3:30:00 PM
        ///  3: 10/29/2007 2:30:00 PM
        ///  4: 12/31/2007 2:30:00 PM
        ///  5: 2/25/2008 2:30:00 PM
        /// */
        ///     </code>
        /// 	<code lang="VB">
        /// Imports System
        /// Imports Telerik.Web.UI
        /// 
        /// Namespace RecurrenceExamples
        ///     Class MonthlyRecurrenceRuleExample2
        ///         Shared Sub Main()
        ///             ' Creates a sample appointment that starts at 6/1/2007 3:30 PM (local time) and lasts half an hour.
        ///             Dim recurringAppointment As New Appointment("1", Convert.ToDateTime("6/1/2007 3:30 PM"), Convert.ToDateTime("6/1/2007 4:00 PM"), "Sample appointment")
        /// 
        ///             ' Creates a recurrence range, that specifies a limit of 10 occurrences for the appointment.
        ///             Dim range As New RecurrenceRange()
        ///             range.Start = recurringAppointment.Start
        ///             range.EventDuration = recurringAppointment.[End] - recurringAppointment.Start
        ///             range.MaxOccurrences = 5
        /// 
        ///             ' Creates a recurrence rule to repeat the appointment on the last monday of every two months.
        ///             Dim rrule As New MonthlyRecurrenceRule(-1, RecurrenceDay.Monday, 2, range)
        /// 
        ///             Console.WriteLine("Appointment occurrs at the following times: ")
        ///             Dim ix As Integer = 0
        ///             For Each occurrence As DateTime In rrule.Occurrences
        ///                 ix = ix + 1
        ///                 Console.WriteLine("{0,2}: {1}", ix, occurrence.ToLocalTime())
        ///             Next
        ///         End Sub
        ///     End Class
        /// End Namespace
        /// 
        /// '
        /// 'This example produces the following results:
        /// '
        /// 'Appointment occurrs at the following times:
        /// ' 1: 6/25/2007 3:30:00 PM
        /// ' 2: 8/27/2007 3:30:00 PM
        /// ' 3: 10/29/2007 2:30:00 PM
        /// ' 4: 12/31/2007 2:30:00 PM
        /// ' 5: 2/25/2008 2:30:00 PM
        /// '
        ///     </code>
        /// </example>
        /// <param name="dayOrdinal">The day ordinal modifier. See <see cref="P:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern.DayOrdinal" /> for additional information.</param>
        /// <param name="daysOfWeekMask">A bit mask that specifies the week days on which the event recurs.</param>
        /// <param name="interval">The interval (in months) between the occurrences.</param>
        /// <param name="range">The <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrenceRange" /> instance that specifies the range of this rule.</param>
        public MonthlyRecurrenceRule(int dayOrdinal, RecurrenceDay daysOfWeekMask, int interval, RecurrenceRange range)
          : this(0, interval, dayOrdinal, daysOfWeekMask, range)
        {
        }

        private MonthlyRecurrenceRule(int dayOfMonth, int interval, int dayOrdinal, RecurrenceDay daysOfWeekMask, RecurrenceRange range)
        {
            this.rulePattern.Frequency = RecurrenceFrequency.Monthly;
            this.rulePattern.Interval = interval;
            this.rulePattern.DaysOfWeekMask = daysOfWeekMask;
            this.rulePattern.DayOfMonth = dayOfMonth;
            this.rulePattern.DayOrdinal = dayOrdinal;
            this.rulePattern.Month = RecurrenceMonth.None;
            this.ruleRange = range;
        }

        protected override DateTime GetOccurrenceStart(int index)
        {
            return this.ruleRange.Start.AddDays((double)index);
        }

        protected override bool MatchAdvancedPattern(DateTime start)
        {
            if (this.GetMonthIndex(start) % this.rulePattern.Interval != 0)
                return false;
            if (0 < this.rulePattern.DayOfMonth)
            {
                int day = DateHelper.GetLastDayOfMonth(start).Day;
                bool flag = this.rulePattern.DayOfMonth > day;
                return start.Day == (flag ? day : this.rulePattern.DayOfMonth);
            }
            return this.MatchDayOfWeekMask(start) && this.MatchDayOrdinal(start);
        }

        private int GetMonthIndex(DateTime start)
        {
            if (start < this.ruleRange.Start)
                return 0;
            return start.Month - this.ruleRange.Start.Month + (start.Year - this.ruleRange.Start.Year) * 12;
        }
    }
}
