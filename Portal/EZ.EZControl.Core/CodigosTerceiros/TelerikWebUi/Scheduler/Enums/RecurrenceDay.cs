﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.RecurrenceDay
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums
{
    /// <summary>
    /// 	<para>Specifies the days of the week. Members might be combined using bitwise
    ///     operations to specify multiple days.</para>
    /// </summary>
    /// <remarks>
    ///     The constants in the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums.RecurrenceDay" /> enumeration might be combined
    ///     with bitwise operations to represent any combination of days. It is designed to be
    ///     used in conjunction with the <see cref="T:EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.RecurrencePattern" /> class to filter
    ///     the days of the week for which the recurrence pattern applies.
    /// </remarks>
    /// <example>
    /// 	<para>Consider the following example that demonstrates the basic usage pattern of
    ///     RecurrenceDay. The most common operators used for manipulating bit fields
    ///     are:</para>
    /// 	<list type="bullet">
    /// 		<item>Bitwise OR: Turns a flag on.</item>
    /// 		<item>Bitwise XOR: Toggles a flag.</item>
    /// 		<item>Bitwise AND: Checks if a flag is turned on.</item>
    /// 		<item>Bitwise NOT: Turns a flag off.</item>
    /// 	</list>
    /// 	<code lang="CS">
    /// using System;
    /// using Telerik.Web.UI;
    /// 
    /// namespace RecurrenceExamples
    /// {
    ///     class RecurrenceDayExample
    ///     {
    ///         static void Main()
    ///         {
    ///             // Selects Friday, Saturday and Sunday.
    ///             RecurrenceDay dayMask = RecurrenceDay.Friday | RecurrenceDay.WeekendDays;
    ///             PrintSelectedDays(dayMask);
    /// 
    ///             // Selects all days, except Thursday.
    ///             dayMask = RecurrenceDay.EveryDay ^ RecurrenceDay.Thursday;
    ///             PrintSelectedDays(dayMask);
    ///         }
    /// 
    ///         static void PrintSelectedDays(RecurrenceDay dayMask)
    ///         {
    ///             Console.WriteLine("Value: {0,3} - {1}", (int) dayMask, dayMask);
    ///         }
    ///     }
    /// }
    /// 
    /// /*
    /// This example produces the following results:
    /// 
    /// Value: 112 - Friday, WeekendDays
    /// Value: 119 - Monday, Tuesday, Wednesday, Friday, WeekendDays
    /// */
    ///     </code>
    /// 	<code lang="VB">
    /// Imports System
    /// Imports Telerik.Web.UI
    /// 
    /// Namespace RecurrenceExamples
    ///     Class RecurrenceDayExample
    ///         Shared Sub Main()
    ///             ' Selects Friday, Saturday and Sunday.
    ///             Dim dayMask As RecurrenceDay = RecurrenceDay.Friday Or RecurrenceDay.WeekendDays
    ///             PrintSelectedDays(dayMask)
    /// 
    ///             ' Selects all days, except Thursday.
    ///             dayMask = RecurrenceDay.EveryDay Xor RecurrenceDay.Thursday
    ///             PrintSelectedDays(dayMask)
    ///         End Sub
    /// 
    ///         Shared Sub PrintSelectedDays(ByVal dayMask As RecurrenceDay)
    ///             Console.WriteLine("Value: {0,3} - {1}", DirectCast(dayMask, Integer), dayMask)
    ///         End Sub
    ///     End Class
    /// End Namespace
    /// 
    /// '
    /// 'This example produces the following results:
    /// '
    /// 'Value: 112 - Friday, WeekendDays
    /// 'Value: 119 - Monday, Tuesday, Wednesday, Friday, WeekendDays
    /// '
    ///     </code>
    /// </example>
    [Flags]
    public enum RecurrenceDay
    {
        None = 0,
        Sunday = 1,
        Monday = 2,
        Tuesday = 4,
        Wednesday = 8,
        Thursday = 16,
        Friday = 32,
        Saturday = 64,
        EveryDay = Saturday | Friday | Thursday | Wednesday | Tuesday | Monday | Sunday,
        WeekDays = Friday | Thursday | Wednesday | Tuesday | Monday,
        WeekendDays = Saturday | Sunday,
    }
}
