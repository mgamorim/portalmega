﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.RecurrenceRangeType
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduler.Enums
{
    /// <summary>Specifies the frequency of a recurrence.</summary>
    public enum RecurrenceRangeType
    {
        Indefinitely,
        GivenOccurrences,
        UntilGivenDate,
    }
}
