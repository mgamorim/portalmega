﻿// Decompiled with JetBrains decompiler
// Type: Telerik.Web.UI.Scheduling.DateHelper
// Assembly: Telerik.Web.UI, Version=2016.3.914.45, Culture=neutral, PublicKeyToken=121fae78165ba3d4
// MVID: A519DA8A-D673-48A9-9976-4B08756261D2
// Assembly location: C:\Program Files (x86)\Telerik\UI for ASP.NET AJAX R3 2016\Live Demos\Bin\Telerik.Web.UI.dll

using System;
using EZ.EZControl.CodigosTerceiros.TelerikWebUi.TimeZones.Interfaces;

namespace EZ.EZControl.CodigosTerceiros.TelerikWebUi.Scheduling
{
    internal static class DateHelper
    {
        public static DateTime GetStartOfWeek(DateTime selectedDate, DayOfWeek weekStart)
        {
            int num1 = (int)selectedDate.DayOfWeek;
            int num2 = 0;
            while ((DayOfWeek)num1 != weekStart)
            {
                if (num1 == 0)
                    num1 = 6;
                else
                    --num1;
                ++num2;
            }
            return new DateTime(selectedDate.Subtract(TimeSpan.FromDays((double)num2)).Ticks, selectedDate.Kind);
        }

        public static DateTime GetEndOfWeek(DateTime selectedDate, DayOfWeek weekStart, int numDays)
        {
            return new DateTime(DateHelper.GetStartOfWeek(selectedDate, weekStart).AddDays((double)numDays).Ticks, selectedDate.Kind);
        }

        public static DateTime GetFirstDayOfMonth(DateTime date)
        {
            return new DateTime(new DateTime(date.Year, date.Month, 1).Ticks, date.Kind);
        }

        public static DateTime GetLastDayOfMonth(DateTime date)
        {
            return new DateTime(new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month)).Ticks, date.Kind);
        }

        public static DateTime GetFirstDayOfYear(DateTime date)
        {
            return new DateTime(new DateTime(date.Year, 1, 1).Ticks, date.Kind);
        }

        public static DateTime AssumeUtc(DateTime date)
        {
            return DateTime.SpecifyKind(date, DateTimeKind.Utc);
        }

        public static DateTime AssumeUnspecified(DateTime date)
        {
            return DateTime.SpecifyKind(date, DateTimeKind.Unspecified);
        }

        public static int GetWeekLength(DateTime date, DayOfWeek firstDayOfWeek, DayOfWeek lastDayOfWeek)
        {
            DateTime startOfWeek = DateHelper.GetStartOfWeek(date, firstDayOfWeek);
            DateTime dateTime = startOfWeek;
            while (dateTime.DayOfWeek != lastDayOfWeek)
                dateTime = dateTime.AddDays(1.0);
            return (int)(dateTime - startOfWeek).TotalDays + 1;
        }

        public static DateTime GetStartOfDay(DateTime dateStart, TimeSpan effectiveDayStartTime, ITimeZoneModel model)
        {
            DateTime dateTime = dateStart.Add(effectiveDayStartTime);
            if (model.IsTransitionFrame(dateTime, dateStart))
            {
                TimeSpan ts = model.GetUtcOffset(dateTime) - model.GetUtcOffset(dateStart);
                effectiveDayStartTime = effectiveDayStartTime.Subtract(ts);
            }
            return dateStart.Add(effectiveDayStartTime);
        }
    }
}
