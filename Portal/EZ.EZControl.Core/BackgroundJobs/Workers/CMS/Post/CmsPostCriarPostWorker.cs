﻿using System;
using System.Threading.Tasks;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Threading.BackgroundWorkers;
using Abp.Threading.Timers;
using EZ.EZControl.BackgroundJobs.Arguments.CMS.Post;
using EZ.EZControl.BackgroundJobs.Jobs.CMS.Post;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.BackgroundJobs.Workers.CMS.Post
{
    public class CmsPostCriarPostWorker : PeriodicBackgroundWorkerBase, ISingletonDependency
    {
        private readonly IBackgroundJobManager _backgroundJobManager;
        private readonly IRepository<Site, int> _siteRepository;

        public CmsPostCriarPostWorker(AbpTimer timer,
            IBackgroundJobManager backgroundJobManager,
            IRepository<Site, int> siteRepository)
            : base(timer)
        {
            _backgroundJobManager = backgroundJobManager;
            _siteRepository = siteRepository;
            Timer.Period = 20000;
        }

        [UnitOfWork]
        protected override void DoWork()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                _backgroundJobManager
                    .Enqueue<CmsPostCriarPostJob, CmsPostCriarPostArgs>(new CmsPostCriarPostArgs()
                    {
                        Site = _siteRepository.Get(1),
                        IsActive = true,
                        TipoDePublicacao = TipoDePublicacaoEnum.Post,
                        Slug = string.Format("sem-categoria/bom-dia-{0}-{1}",
                            DateTime.Now.ToShortDateString().Replace('/','-'),
                            DateTime.Now.ToShortTimeString().Replace(':', '-')),
                        Titulo = string.Format("Bom dia - {0} {1}",
                            DateTime.Now.ToShortDateString().Replace('/', '-'),
                            DateTime.Now.ToShortTimeString().Replace(':', '-')),
                        Conteudo = "Este é um Job Teste",
                        Data = DateTime.Now
                    });

                CurrentUnitOfWork.SaveChanges();
            }
        }
    }
}
