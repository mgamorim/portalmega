﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using EZ.EZControl.BackgroundJobs.Arguments.CMS.Post;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.BackgroundJobs.Jobs.CMS.Post
{
    public class CmsPostCriarPostJob : BackgroundJob<CmsPostCriarPostArgs>, ITransientDependency
    {
        private readonly IPostService _postService;

        public CmsPostCriarPostJob(IPostService postService, ISiteService siteService)
        {
            _postService = postService;
        }

        public override async void Execute(CmsPostCriarPostArgs args)
        {
            var entity = new Domain.CMS.Geral.Post()
            {
                IsActive = args.IsActive,
                Site = args.Site,
                Titulo = args.Titulo,
                Slug = args.Slug,
                Conteudo = args.Conteudo,
                TipoDePublicacao = args.TipoDePublicacao
            };
            await _postService.CreateEntity(entity);
        }
    }
}