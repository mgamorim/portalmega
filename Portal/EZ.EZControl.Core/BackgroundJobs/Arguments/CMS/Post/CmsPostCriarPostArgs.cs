﻿using System;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.BackgroundJobs.Arguments.CMS.Post
{
    [Serializable]
    public class CmsPostCriarPostArgs
    {
        public bool IsActive { get; set; }
        public Site Site { get; set; }
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public string Titulo { get; set; }
        public string Conteudo { get; set; }
        public string Slug { get; set; }
        public DateTime Data { get; set; }
    }
}