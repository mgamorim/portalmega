﻿using System.Threading.Tasks;
using Abp.Configuration;

namespace EZ.EZControl.Timing
{
    public interface ITimeZoneService
    {
        Task<string> GetDefaultTimezoneAsync(SettingScopes scope, int? tenantId);
    }
}
