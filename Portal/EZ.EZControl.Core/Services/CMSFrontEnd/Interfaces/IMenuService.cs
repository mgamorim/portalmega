﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMSFrontEnd.Interfaces
{
    public interface IMenuService : IService<Menu>
    {
        string GetHtml(Menu menu);
    }
}
