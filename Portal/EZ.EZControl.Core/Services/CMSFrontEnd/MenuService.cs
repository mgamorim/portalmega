﻿using Abp.Domain.Repositories;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Linq;
using System.Text;

namespace EZ.EZControl.Services.CMSFrontEnd
{
    public class MenuService : EZControlDomainServiceBase<Menu>, Interfaces.IMenuService
    {
        private readonly ILinkService _linkService;
        public MenuService(IRepository<Menu, int> repository,
            ILinkService linkService)
            : base(repository)
        {
            _linkService = linkService;
        }
        public string GetHtml(Menu menu)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(String.Format("<div class='ezmenu ezmenu-id-{0}'>", menu.Id));
            MontaEstruturaRecursivamente(menu, ref sb, null);
            sb.AppendLine("</div>");

            return sb.ToString();
        }

        private void MontaEstruturaRecursivamente(Menu menu, ref StringBuilder sb, ItemDeMenu ultimoItem)
        {
            sb.AppendLine("<ul>");
            var itens = ultimoItem == null ? menu.ItensDeMenu.Where(x => x.ItemMenu == null) : ultimoItem.Itens;
            foreach (var item in itens)
            {
                var estruturaLi = string.Empty;
                var urlLink = "#";
                switch (item.TipoDeItemDeMenu)
                {
                    case TipoDeItemDeMenuEnum.Link:
                        {
                            estruturaLi = String.Format("ezitemdemenu-tipo-{0}", item.TipoDeItemDeMenu.GetDescription().ToLower());
                            urlLink = item.Url;
                            break;
                        }
                    case TipoDeItemDeMenuEnum.Categoria:
                        {
                            estruturaLi = String.Format("ezitemdemenu-tipo-{0} ezcategoria-slug-{1} ezcategoria-id-{2}", item.TipoDeItemDeMenu.GetDescription().ToLower(), item.Categoria.Slug.ToLower(), item.Categoria.Id.ToString());
                            urlLink = _linkService.GetLink(TipoDeLinkEnum.Categoria, item.Categoria.Site, "http://{0}/{1}", item.Categoria, item.Categoria.Nome.Slugify());
                            break;
                        }
                    case TipoDeItemDeMenuEnum.Pagina:
                        {
                            estruturaLi = String.Format("ezitemdemenu-tipo-{0} ezpublicacao-id-{1} ezpublicacao-tipo-{2}", item.TipoDeItemDeMenu.GetDescription().ToLower(), item.Pagina.Id.ToString(), TipoDePublicacaoEnum.Pagina.GetDescription().ToLower());
                            urlLink = _linkService.GetLink(TipoDeLinkEnum.Pagina, item.Pagina.Site, "http://{0}/{1}", null, item.Pagina.Titulo.Slugify());
                            break;
                        }
                }
                sb.AppendLine(string.Format("<li class='ezitemdemenu ezitemdemenu-id-{0} {1}'>", item.Id.ToString(), estruturaLi));
                sb.AppendLine(string.Format("<a href='{0}'>{1}</a>", urlLink, item.Titulo));
                if (item.Itens != null && item.Itens.Count > 0)
                {
                    sb.AppendLine(String.Format("<div class='ezsubmenu'>"));
                    MontaEstruturaRecursivamente(menu, ref sb, item);
                    sb.AppendLine("</div>");
                }
                sb.AppendLine("</li>");
            }
            sb.AppendLine("</ul>");
        }
    }
}