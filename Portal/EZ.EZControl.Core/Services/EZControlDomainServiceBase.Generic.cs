﻿using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Events;
using EZ.EZControl.Extensions;
using EZ.EZControl.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using System.Threading;
using EZ.EZControl.Common.Extensions;
using System.Reflection;
using Castle.Components.DictionaryAdapter.Xml;

namespace EZ.EZControl.Services
{
    public abstract class EZControlDomainServiceBase<TEntity, TPrimaryKey> : EZControlDomainServiceBase, IService<TEntity, TPrimaryKey>
            where TEntity : class, IEzEntity<TPrimaryKey>
            where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        protected EZControlDomainServiceBase(IRepository<TEntity, TPrimaryKey> repository)
        {
            this.Repository = repository;
        }

        public IRepository<TEntity, TPrimaryKey> Repository { get; set; }

        public event EventHandler<ValidateEntityEventArgs<TEntity, TPrimaryKey>> ValidateEntity;

        public event EventHandler<BeforeSaveEntityEventArgs<TEntity, TPrimaryKey>> BeforeSaveEntity;

        public virtual async Task<TPrimaryKey> CreateEntity(TEntity entity)
        {
            Func<TEntity, Task<TPrimaryKey>> createEntityFunc = async (entityFunc) =>
            {
                if (ValidateEntity != null)
                {
                    ValidateEntity(this, new ValidateEntityEventArgs<TEntity, TPrimaryKey>(entityFunc));
                }

                if (BeforeSaveEntity != null)
                {
                    BeforeSaveEntity(this, new BeforeSaveEntityEventArgs<TEntity, TPrimaryKey>(entityFunc));
                }

                var id = await Repository.InsertAndGetIdAsync(entityFunc);

                return id;
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    SetEmpresaInAllEntities(entity);

                    return await createEntityFunc(entity);
                });
            }

            return await createEntityFunc(entity);
        }

        public virtual void Insert(IEnumerable<TEntity> entities)
        {
            Action<IEnumerable<TEntity>> createEntityFunc = (entitiesFunc) =>
            {
                Repository.Insert(entitiesFunc);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                ApplyEmpresaFilter(() =>
                {
                    foreach (var entity in entities)
                    {
                        SetEmpresaInAllEntities(entity);
                    }

                    createEntityFunc(entities);
                });
            }
            else
            {
                createEntityFunc(entities);
            }
        }

        public virtual async Task<TEntity> UpdateEntity(TEntity entity)
        {
            Func<TEntity, Task<TEntity>> createEntityFunc = async (entityFunc) =>
            {
                return await Repository.UpdateAsync(entityFunc);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    SetEmpresaInAllEntities(entity);

                    return await createEntityFunc(entity);
                });
            }

            return await createEntityFunc(entity);
        }

        public virtual async Task<TPrimaryKey> CreateOrUpdateEntity(TEntity entity)
        {
            Func<TEntity, Task<TPrimaryKey>> createOrUpdateEntityFunc = async (entityFunc) =>
            {
                if (ValidateEntity != null)
                {
                    ValidateEntity(this, new ValidateEntityEventArgs<TEntity, TPrimaryKey>(entityFunc));
                }

                if (BeforeSaveEntity != null)
                {
                    BeforeSaveEntity(this, new BeforeSaveEntityEventArgs<TEntity, TPrimaryKey>(entityFunc));
                }

                return await Repository.InsertOrUpdateAndGetIdAsync(entityFunc);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    SetEmpresaInAllEntities(entity);

                    return await createOrUpdateEntityFunc(entity);
                });
            }

            return await createOrUpdateEntityFunc(entity);
        }

        public virtual async Task<TEntity> CreateAndReturnEntity(TEntity entity)
        {
            Func<TEntity, Task<TEntity>> createAndReturnEntityFunc = async (entityFunc) =>
            {
                return await Repository.InsertAsync(entityFunc);
            };


            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    SetEmpresaInAllEntities(entity);

                    return await createAndReturnEntityFunc(entity);
                });
            }

            return await createAndReturnEntityFunc(entity);
        }

        public virtual async Task<TEntity> UpdateAndReturnEntity(TEntity entity)
        {
            Func<TEntity, Task<TEntity>> updateAndReturnEntityFunc = async (entityFunc) =>
            {
                return await Repository.UpdateAsync(entityFunc);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    SetEmpresaInAllEntities(entity);

                    return await updateAndReturnEntityFunc(entity);
                });
            }

            return await updateAndReturnEntityFunc(entity);
        }

        public virtual async Task<TEntity> CreateOrUpdateAndReturnSavedEntity(TEntity entity)
        {
            Func<TEntity, Task<TEntity>> createOrUpdateAndReturnSavedEntityFunc = async (entityFunc) =>
            {
                var id = await CreateOrUpdateEntity(entityFunc);

                return await GetById(id);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    SetEmpresaInAllEntities(entity);

                    return await createOrUpdateAndReturnSavedEntityFunc(entity);
                });
            }

            return await createOrUpdateAndReturnSavedEntityFunc(entity);
        }

        public virtual IQueryable<TEntity> GetAll()
        {
            Func<IQueryable<TEntity>> getAllFunc = () =>
            {
                return Repository.GetAll();
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return ApplyEmpresaFilter(() =>
                {
                    return getAllFunc();
                });
            }

            return getAllFunc();
        }

        public virtual async Task<List<TEntity>> GetAllListAsync()
        {
            Func<Task<List<TEntity>>> getAllFunc = async () => await Repository.GetAllListAsync();

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () => await getAllFunc());
            }

            return await getAllFunc();
        }

        public virtual async Task<List<TEntity>> GetAllListAsync(Expression<Func<TEntity, bool>> predicate)
        {
            Func<Task<List<TEntity>>> getAllFunc = async () => await Repository.GetAllListAsync(predicate);

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () => await getAllFunc());
            }

            return await getAllFunc();
        }

        public virtual async Task<TEntity> FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            Func<Task<TEntity>> getAllFunc = async () => await Repository.FirstOrDefaultAsync(predicate);

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () => await getAllFunc());
            }

            return await getAllFunc();
        }

        public virtual async Task<TEntity> GetById(TPrimaryKey id)
        {
            Func<TPrimaryKey, Task<TEntity>> getByIdFunc = async (idFunc) =>
            {
                try
                {
                    TEntity entity = await Repository.GetAsync(idFunc);

                    return entity;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException("Registro não encontrado.", ex.Message);
                }
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await getByIdFunc(id);
                });
            }

            return await getByIdFunc(id);
        }

        public virtual async Task<TEntity> GetByExternalId(TPrimaryKey id)
        {
            Func<TPrimaryKey, Task<TEntity>> getByExternalIdFunc = async (idFunc) =>
            {
                try
                {
                    // Se caso não existe um registro com o Id especificado pelo usuário, o método abaixo gera uma exceção quando não encontra o registro.
                    // Portanto crio um exceção amigável para o usuário dizendo que não foi localizado o registro que ele informou no request.
                    TEntity entity = await Repository.FirstOrDefaultAsync(x => x.ExternalId.Equals(idFunc));

                    return entity;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException("Registro não encontrado.", ex.Message);
                }
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await getByExternalIdFunc(id);
                });
            }

            return await getByExternalIdFunc(id);
        }

        public virtual async Task Delete(TPrimaryKey id)
        {
            Func<TPrimaryKey, Task> deleteFunc = async (idFunc) =>
            {
                TEntity registro = null;

                try
                {
                    // Usando o método Delete(input.Id), não temos como saber se foi encontrado algum registro para ser excluído do banco,
                    // por esse motivo, estou primeiramente obtendo a instancia do registro para depois excluí-lo.

                    // Se caso não existe um registro com o Id especificado pelo usuário, o método abaixo gera uma exceção quando não encontra o registro.
                    // Portanto crio um exceção amigável para o usuário dizendo que não foi localizado o registro que ele informou no request.
                    registro = await Repository.GetAsync(idFunc);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException("Registro não encontrado.", ex.Message);
                }

                if (registro != null)
                {
                    await Repository.DeleteAsync(registro);
                }
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                await ApplyEmpresaFilter(async () =>
                 {
                     await deleteFunc(id);
                 });
            }
            else
            {
                await deleteFunc(id);
            }
        }

        public virtual async Task Delete(Expression<Func<TEntity, bool>> predicate)
        {
            Func<Expression<Func<TEntity, bool>>, Task> deleteFunc = async (predicateFunc) =>
            {
                await Repository.DeleteAsync(predicateFunc);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                await ApplyEmpresaFilter(async () =>
                {
                    await deleteFunc(predicate);
                });
            }
            else
            {
                await deleteFunc(predicate);
            }
        }

        public virtual bool CheckForFieldDuplicated(TPrimaryKey id, object valor, Func<TEntity, bool> condicao)
        {
            Func<TPrimaryKey, object, Func<TEntity, bool>, bool> checkForFieldDuplicatedFunc = (idFunc, valorFunc, condicaoFunc) =>
           {
               return Repository.GetAll()
                   .WhereIf(valorFunc != null, condicaoFunc)
                   .Any(x => !x.Id.Equals(idFunc));
           };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return ApplyEmpresaFilter(() =>
                {
                    return checkForFieldDuplicatedFunc(id, valor, condicao);
                });
            }

            return checkForFieldDuplicatedFunc(id, valor, condicao);
        }

        public virtual bool CheckForDuplicateInstance(TPrimaryKey id, Expression<Func<TEntity, bool>> condicao)
        {
            Func<TPrimaryKey, Expression<Func<TEntity, bool>>, bool> checkForDuplicateInstanceFunc = (idFunc, condicaoFunc) =>
            {
                return Repository.GetAll()
                    .Where(condicaoFunc)
                    .Any(x => !x.Id.Equals(idFunc));
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return ApplyEmpresaFilter(() =>
                {
                    return checkForDuplicateInstanceFunc(id, condicao);
                });
            }

            return checkForDuplicateInstanceFunc(id, condicao);
        }

        public void SetEmpresaInAllEntities(IEzEntity<TPrimaryKey> entityParent)
        {
            if (entityParent != null)
            {
                SetEmpresaInEntity(entityParent);

                foreach (PropertyInfo prop in entityParent.GetType().GetProperties()
                    .Where(x => x.CanWrite && !x.GetType().IsPrimitive))
                {
                    if (typeof(IEnumerable<object>).IsAssignableFrom(prop.PropertyType))
                    {
                        var entitiesChildren = (IEnumerable<object>)prop.GetValue(entityParent, null);

                        if (entitiesChildren != null)
                        {
                            foreach (var entityChild in entitiesChildren)
                            {
                                var empresa = entityChild as IHasEmpresa;
                                if (empresa != null
                                    && empresa.EmpresaId == 0
                                    && (entityChild is IEzEntity<int> || entityChild is IEzEntity<long>))
                                {
                                    SetEmpresaInAllEntities((IEzEntity<TPrimaryKey>)entityChild);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (typeof(IHasEmpresa).IsAssignableFrom(prop.PropertyType)
                            && typeof(IEzEntity<TPrimaryKey>).IsAssignableFrom(prop.PropertyType))
                        {
                            var entityChild = (IEzEntity<TPrimaryKey>)prop.GetValue(entityParent, null);

                            if (entityChild != null)
                            {
                                SetEmpresaInEntity(entityChild);
                            }
                        }
                    }
                }
            }
        }

        public void SetEmpresaInEntity(IEzEntity<TPrimaryKey> entity)
        {
            var empresa = entity as IHasEmpresa;
            if (empresa != null && empresa.EmpresaId == 0)
            {
                var idEmpresa = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

                if (idEmpresa != null && idEmpresa > 0)
                {
                    empresa.EmpresaId = idEmpresa.Value;
                }
            }
        }
    }

    public abstract class EZControlDomainServiceBase<TEntity> : EZControlDomainServiceBase<TEntity, int>, IService<TEntity>
        where TEntity : class, IEzEntity<int>
    {
        protected EZControlDomainServiceBase(IRepository<TEntity, int> repository)
            : base(repository)
        {
        }
    }
}