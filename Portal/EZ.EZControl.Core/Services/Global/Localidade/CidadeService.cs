﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade
{
    public class CidadeService : EZControlDomainServiceBase<Cidade>, ICidadeService
    {
        public CidadeService(IRepository<Cidade, int> repository)
            : base(repository)
        {
        }
    }
}