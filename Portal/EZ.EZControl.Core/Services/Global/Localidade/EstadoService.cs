﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade
{
    public class EstadoService : EZControlDomainServiceBase<Estado>, IEstadoService
    {
        public EstadoService(IRepository<Estado, int> repository)
            : base(repository)
        {
        }
    }
}