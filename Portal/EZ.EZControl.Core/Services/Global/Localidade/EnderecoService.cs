﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Localidade.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade
{
    public class EnderecoService : EZControlDomainServiceBase<Endereco>, IEnderecoService
    {
        public EnderecoService(IRepository<Endereco, int> repository)
            : base(repository)
        {
        }

        public void CanAssignPessoa(Endereco endereco, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));

            if (pessoa == null)
                throw new UserFriendlyException(L("Endereco.EmptyPessoaError."));


        }

        public void CanAssignContato(Endereco endereco, Contato contato)
        {
            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));

            if (contato == null)
                throw new UserFriendlyException(L("Contato.EmptyContatoError."));

            // TODO: Marcos. Esse campo será obrigatorio?
            //if (contato == null)
            //    throw new UserFriendlyException("É obrigatório informar o contato");


        }

        public void CanAssignTipoDeLogradouro(Endereco endereco, TipoDeLogradouro tipoDeLogradouro)
        {
            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));

            if (tipoDeLogradouro == null)
                throw new UserFriendlyException(L("Endereco.EmptyTipoDeLogradouroError."));

        }

        public void CanAssignCidade(Endereco endereco, Cidade cidade)
        {
            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));

            if (cidade == null)
                throw new UserFriendlyException(L("Cidade.EmptyError"));

        }
    }
}