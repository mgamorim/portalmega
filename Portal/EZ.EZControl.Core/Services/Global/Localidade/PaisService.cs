﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade
{
    public class PaisService : EZControlDomainServiceBase<Pais>, IPaisService
    {
        public PaisService(IRepository<Pais, int> repository)
            : base(repository)
        {
        }
    }
}