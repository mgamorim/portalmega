﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade
{
    public class TipoDeLogradouroService : EZControlDomainServiceBase<TipoDeLogradouro>, ITipoDeLogradouroService
    {
        public TipoDeLogradouroService(IRepository<TipoDeLogradouro, int> repository)
            : base(repository)
        {
        }
    }
}