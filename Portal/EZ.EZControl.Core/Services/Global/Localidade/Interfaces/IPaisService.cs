﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface IPaisService : IService<Pais>
    {
    }
}