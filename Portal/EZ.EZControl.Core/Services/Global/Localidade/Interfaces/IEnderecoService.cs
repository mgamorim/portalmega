﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface IEnderecoService : IService<Endereco>
    {
        void CanAssignPessoa(Endereco endereco, Domain.Global.Pessoa.Pessoa pessoa);

        void CanAssignContato(Endereco endereco, Contato contato);

        void CanAssignTipoDeLogradouro(Endereco endereco, TipoDeLogradouro tipoDeLogradouro);

        void CanAssignCidade(Endereco endereco, Cidade cidade);
    }
}