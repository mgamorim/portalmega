﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class GrupoPessoaService : EZControlDomainServiceBase<GrupoPessoa>, IGrupoPessoaService
    {
        public GrupoPessoaService(IRepository<GrupoPessoa, int> repository)
            : base(repository)
        {
        }

        public void CanAssignGrupoPessoa(GrupoPessoa grupoPessoa, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (grupoPessoa == null)
                throw new UserFriendlyException(L("GrupoPessoa.EmptyGrupoPessoaError"));

            if (pessoa == null)
                throw new UserFriendlyException(L("GrupoPessoa.EmptyPessoaError"));


        }
    }
}