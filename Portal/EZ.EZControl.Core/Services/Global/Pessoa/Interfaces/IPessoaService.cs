﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;
using System;
using System.Linq;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IPessoaService<TPessoa, TPrimaryKey> : IService<TPessoa, TPrimaryKey>
        where TPessoa : Domain.Global.Pessoa.Pessoa, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignGrupoPessoa(TPessoa pessoa, GrupoPessoa grupoPessoa);

        void CanAssignDocumentoPrincipal(TPessoa pessoa, Documento documento);

        void CanAssignEnderecoPrincipal(TPessoa pessoa, Endereco endereco);

        void CanAssignEnderecoCobranca(TPessoa pessoa, Endereco endereco);

        void CanAssignEnderecoEntrega(TPessoa pessoa, Endereco endereco);

        #region Métodos para Manipular Coleções

        TPessoa AddDocumento(TPessoa pessoa, Documento item);

        TPessoa AddDocumento(TPessoa pessoa, TipoDeDocumento tipoDeDocumento, string numero,
            string orgaoExpedidor = null, DateTime? dataExpedicao = null, Estado estado = null,
            DateTime? dataDeValidade = null, string serie = null);

        TPessoa RemoveDocumento(TPessoa pessoa, Documento item);

        void ClearDocumentos(TPessoa pessoa);

        Documento GetDocumentoByDescricao(TPessoa pessoa, string tipoDoc);

        Documento GetDocumentoByTipo(TPessoa pessoa, TipoDeDocumento tipoDoc);

        TPessoa AddEnderecoEletronico(TPessoa pessoa, EnderecoEletronico item);

        TPessoa AddEnderecoEletronico(TPessoa pessoa, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico,
            string endereco);

        TPessoa RemoveEnderecoEletronico(TPessoa pessoa, EnderecoEletronico item);
        void ClearEnderecosEletronicos(TPessoa pessoa);

        TPessoa AddEndereco(TPessoa pessoa, Endereco item);

        TPessoa AddEndereco(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco,
            TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null,
            string referencia = null);

        void ClearEnderecos(TPessoa pessoa);

        TPessoa AddEnderecoPrincipal(TPessoa pessoa, Endereco item);

        TPessoa AddEnderecoPrincipal(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco,
            TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null,
            string referencia = null);

        TPessoa AddEnderecoCobranca(TPessoa pessoa, Endereco item);

        TPessoa AddEnderecoCobranca(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco,
            TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null,
            string referencia = null);

        TPessoa AddEnderecoEntrega(TPessoa pessoa, Endereco item);

        TPessoa AddEnderecoEntrega(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco,
            TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null,
            string referencia = null);

        TPessoa RemoveContato(TPessoa pessoa, Contato item);

        TPessoa AddTelefone(TPessoa pessoa, Telefone item);

        TPessoa AddTelefone(TPessoa pessoa, TipoTelefoneEnum tipo, string ddd, string numero, string ddi = null,
            string ramal = null);

        TPessoa RemoveTelefone(TPessoa pessoa, Telefone item);

        void ClearTelefones(TPessoa pessoa);

        #endregion
    }

    public interface IPessoaService<TPessoa> : IPessoaService<TPessoa, int>
        where TPessoa : Domain.Global.Pessoa.Pessoa
    {
    }

    public interface IPessoaService : IPessoaService<Domain.Global.Pessoa.Pessoa>
    {
    }
}