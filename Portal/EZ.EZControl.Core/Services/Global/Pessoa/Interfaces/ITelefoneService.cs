﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITelefoneService : IService<Telefone>
    {
        void CanAssignPessoa(Telefone telefone, Domain.Global.Pessoa.Pessoa pessoa);
        void PreencherNumeroCelularBrComMascaraByString(Telefone telefone, string numero);
    }
}