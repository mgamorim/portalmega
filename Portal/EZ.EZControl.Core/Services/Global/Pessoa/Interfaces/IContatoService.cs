﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IContatoService : IService<Contato>
    {
        void CanAssignPessoa(Contato contato, Domain.Global.Pessoa.Pessoa pessoa);

        void CanAssignTipoDeContato(Contato contato, TipoDeContato tipoDeContato);

        void CanAssignTratamento(Contato contato, Tratamento tratamento);
    }
}