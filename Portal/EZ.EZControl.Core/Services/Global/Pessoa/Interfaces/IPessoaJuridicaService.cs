﻿using EZ.EZControl.Domain.Global.Pessoa;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IPessoaJuridicaService : IPessoaService<PessoaJuridica>
    {
    }
}