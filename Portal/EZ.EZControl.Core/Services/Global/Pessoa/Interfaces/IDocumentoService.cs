﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IDocumentoService : IService<Documento>
    {
        void CanAssignPessoa(Documento documento, Domain.Global.Pessoa.Pessoa pessoa);

        void CanAssignTipoDocumento(Documento documento, TipoDeDocumento tipoDeDocumento);

        void CanDelete(Documento documento);

    }
}