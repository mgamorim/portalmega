﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IPessoaFisicaService : IPessoaService<PessoaFisica>
    {
        void CanAssignTratamento(PessoaFisica pessoa, Tratamento tratamento);
        Task<PessoaFisica> GetPessoaByCpf(string cpf);
    }
}