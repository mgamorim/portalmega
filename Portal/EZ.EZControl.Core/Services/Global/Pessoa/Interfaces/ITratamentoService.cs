﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITratamentoService : IService<Tratamento>
    {
    }
}