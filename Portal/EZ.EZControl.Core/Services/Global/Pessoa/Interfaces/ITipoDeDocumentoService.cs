﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITipoDeDocumentoService : IService<TipoDeDocumento>
    {
        bool IsCnpj(string cnpj);
        bool IsCpf(string cpf);
        bool IsPis(string pis);
        TipoDeDocumento GetByTipoFixo(TipoDeDocumentoEnum tipoFixo);
    }
}