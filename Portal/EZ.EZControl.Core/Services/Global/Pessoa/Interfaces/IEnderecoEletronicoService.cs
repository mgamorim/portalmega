﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IEnderecoEletronicoService : IService<EnderecoEletronico>
    {
        void CanAssignPessoa(EnderecoEletronico enderecoEletronico, Domain.Global.Pessoa.Pessoa pessoa);

        void CanAssignContato(EnderecoEletronico enderecoEletronico, Contato contato);
    }
}