﻿using System;
using System.Linq;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class PessoaService<TPessoa> : EZControlDomainServiceBase<TPessoa>, IPessoaService<TPessoa>
        where TPessoa : Domain.Global.Pessoa.Pessoa
    {
        public PessoaService(IRepository<TPessoa, int> repository)
            : base(repository)
        {
            ValidateEntity += PessoaService_ValidateEntity;
            BeforeSaveEntity += PessoaService_BeforeSaveEntity;
        }

        private void PessoaService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TPessoa, int> e)
        {
        }

        private void PessoaService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TPessoa, int> e)
        {
        }

        public void CanAssignGrupoPessoa(TPessoa pessoa, GrupoPessoa grupoPessoa)
        {
            if (pessoa == null)
                throw new UserFriendlyException(L("Pessoa.EmptyError"));

            if (grupoPessoa == null)
                throw new UserFriendlyException(L("GrupoPessoa.EmptyGrupoPessoaError"));
        }

        public void CanAssignDocumentoPrincipal(TPessoa pessoa, Documento documento)
        {
            if (pessoa == null)
                throw new UserFriendlyException(L("Pessoa.EmptyError"));

            if (documento == null)
                throw new UserFriendlyException(L("Documento.EmptyError"));


        }

        public void CanAssignEnderecoPrincipal(TPessoa pessoa, Endereco endereco)
        {
            if (pessoa == null)
                throw new UserFriendlyException(L("Pessoa.EmptyError"));

            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));


        }

        public void CanAssignEnderecoCobranca(TPessoa pessoa, Endereco endereco)
        {
            if (pessoa == null)
                throw new UserFriendlyException(L("Pessoa.EmptyError"));

            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));

        }

        public void CanAssignEnderecoEntrega(TPessoa pessoa, Endereco endereco)
        {
            if (pessoa == null)
                throw new UserFriendlyException(L("Pessoa.EmptyError"));

            if (endereco == null)
                throw new UserFriendlyException(L("Endereco.EmptyEnderecoError."));
        }

        #region Métodos para Manipular Coleções

        public TPessoa AddDocumento(TPessoa pessoa, Documento item)
        {
            if (pessoa.Documentos.Count(x => (x.Id != item.Id) && (x.Numero == item.Numero) && (x.TipoDeDocumento == item.TipoDeDocumento)) > 0)
                throw new UserFriendlyException(string.Format("{0} já cadastrado com o Número [{1}].", item.TipoDeDocumento.Descricao, item.Numero));
            else
            {
                if (item.TipoDeDocumento.Principal)
                    pessoa.DocumentoPrincipal = item;
                pessoa.Documentos.Add(item);
                return pessoa;
            }
        }

        public TPessoa AddDocumento(TPessoa pessoa, TipoDeDocumento tipoDeDocumento, string numero,
            string orgaoExpedidor = null, DateTime? dataExpedicao = null, Estado estado = null, DateTime? dataDeValidade = null, string serie = null)
        {
            return AddDocumento(pessoa, new Documento(pessoa, tipoDeDocumento, numero, orgaoExpedidor, dataExpedicao, estado, dataDeValidade, serie));
        }

        public TPessoa RemoveDocumento(TPessoa pessoa, Documento item)
        {
            pessoa.Documentos.Remove(item);

            if (pessoa.DocumentoPrincipal == item)
            {
                pessoa.DocumentoPrincipal = pessoa.Documentos.FirstOrDefault(x => x.TipoDeDocumento.Principal == true);
            }
            return pessoa;
        }

        public void ClearDocumentos(TPessoa pessoa)
        {
            pessoa.Documentos.Clear();
        }

        public Documento GetDocumentoByDescricao(TPessoa pessoa, string tipoDoc)
        {
            return pessoa.Documentos.FirstOrDefault(d => d.TipoDeDocumento.Descricao == tipoDoc);
        }

        public Documento GetDocumentoByTipo(TPessoa pessoa, TipoDeDocumento tipoDoc)
        {
            return pessoa.Documentos.FirstOrDefault(d => d.TipoDeDocumento == tipoDoc);
        }

        public TPessoa AddEnderecoEletronico(TPessoa pessoa, EnderecoEletronico item)
        {
            if (pessoa.EnderecosEletronicos.Count(x => (x.Id != item.Id) && (x.Endereco == item.Endereco) && (x.TipoDeEnderecoEletronico == item.TipoDeEnderecoEletronico)) > 0)
                throw new UserFriendlyException(string.Format("{0} já cadastrado com o Endereço [{1}].", item.TipoDeEnderecoEletronico.GetDescription(), item.Endereco));
            else
            {
                pessoa.EnderecosEletronicos.Add(item);
                return pessoa;
            }
        }

        public TPessoa AddEnderecoEletronico(TPessoa pessoa, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
        {
            return AddEnderecoEletronico(pessoa, new EnderecoEletronico(pessoa, tipoDeEnderecoEletronico, endereco));
        }

        public TPessoa RemoveEnderecoEletronico(TPessoa pessoa, EnderecoEletronico item)
        {
            pessoa.EnderecosEletronicos.Remove(item);
            return pessoa;
        }

        public void ClearEnderecosEletronicos(TPessoa pessoa)
        {
            pessoa.EnderecosEletronicos.Clear();
        }

        public TPessoa AddEndereco(TPessoa pessoa, Endereco item)
        {
            if (pessoa.Enderecos.FirstOrDefault(x => x.Id == item.Id) == null)
            {
                if (pessoa.Enderecos.Count(x => x.Descricao == item.Descricao && x.TipoEndereco == item.TipoEndereco && x.TipoDeLogradouro == item.TipoDeLogradouro && x.Logradouro == item.Logradouro && x.Numero == item.Numero) > 0)
                    throw new UserFriendlyException("Este Endereço já foi cadastrado.");

                item.Pessoa = pessoa;

                pessoa.Enderecos.Add(item);
            }

            return pessoa;
        }

        public TPessoa AddEndereco(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null, string referencia = null)
        {
            return AddEndereco(pessoa, new Endereco(pessoa, descricao, tipoEndereco, tipoLogradouro, logradouro, numero, complemento, bairro, cep, cidade, referencia));
        }

        public void ClearEnderecos(TPessoa pessoa)
        {
            pessoa.Enderecos.Clear();
        }

        public TPessoa AddEnderecoPrincipal(TPessoa pessoa, Endereco item)
        {
            pessoa.EnderecoPrincipal = item;
            return AddEndereco(pessoa, item);
        }

        public TPessoa AddEnderecoPrincipal(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null, string referencia = null)
        {
            return AddEnderecoPrincipal(pessoa, new Endereco(pessoa, descricao, tipoEndereco, tipoLogradouro, logradouro, numero, complemento, bairro, cep, cidade, referencia));
        }

        public TPessoa AddEnderecoCobranca(TPessoa pessoa, Endereco item)
        {
            pessoa.EnderecoCobranca = item;
            return AddEndereco(pessoa, item);
        }

        public TPessoa AddEnderecoCobranca(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null, string referencia = null)
        {
            return AddEnderecoCobranca(pessoa, new Endereco(pessoa, descricao, tipoEndereco, tipoLogradouro, logradouro, numero, complemento, bairro, cep, cidade, referencia));
        }

        public TPessoa AddEnderecoEntrega(TPessoa pessoa, Endereco item)
        {
            pessoa.EnderecoEntrega = item;
            return AddEndereco(pessoa, item);
        }

        public TPessoa AddEnderecoEntrega(TPessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null, string referencia = null)
        {
            return AddEnderecoEntrega(pessoa, new Endereco(pessoa, descricao, tipoEndereco, tipoLogradouro, logradouro, numero, complemento, bairro, cep, cidade, referencia));
        }

        public TPessoa RemoveContato(TPessoa pessoa, Contato item)
        {
            pessoa.Contatos.Remove(item);
            return pessoa;
        }

        public TPessoa AddTelefone(TPessoa pessoa, Telefone item)
        {
            if (pessoa.Telefones.Count(x => (x.Id != item.Id) && (x.DDI == item.DDI) && (x.DDD == item.DDD) && (x.Numero == item.Numero) && (x.Ramal == item.Ramal) && (x.Tipo == item.Tipo)) > 0)
                throw new UserFriendlyException(string.Format("Telefone Tipo [{0}] já cadastrado para esta Pessoa com o DDI [{1}], DDD [{2}], Número [{3}] e Ramal [{4}].", item.Tipo.GetDescription(), item.DDI, item.DDD, item.Numero, item.Ramal));
            else
            {
                pessoa.Telefones.Add(item);
                return pessoa;
            }
        }

        public TPessoa AddTelefone(TPessoa pessoa, TipoTelefoneEnum tipo, string ddd, string numero, string ddi = null, string ramal = null)
        {
            return AddTelefone(pessoa, new Telefone(pessoa, tipo, ddd, numero, ddi, ramal));
        }

        public TPessoa RemoveTelefone(TPessoa pessoa, Telefone item)
        {
            pessoa.Telefones.Remove(item);
            return pessoa;
        }

        public void ClearTelefones(TPessoa pessoa)
        {
            pessoa.Telefones.Clear();
        }

        #endregion Métodos para Manipular Coleções
    }

    public class PessoaService : PessoaService<Domain.Global.Pessoa.Pessoa>, IPessoaService
    {
        public PessoaService(IRepository<Domain.Global.Pessoa.Pessoa, int> repository)
            : base(repository)
        {
        }
    }
}