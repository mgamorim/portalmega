﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class PessoaJuridicaService : PessoaService<PessoaJuridica>, IPessoaJuridicaService
    {
        public PessoaJuridicaService(IRepository<PessoaJuridica, int> repository)
            : base(repository)
        {
        }
    }
}