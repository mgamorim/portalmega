﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Linq;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class TipoDeDocumentoService : EZControlDomainServiceBase<TipoDeDocumento>, ITipoDeDocumentoService
    {
        public TipoDeDocumentoService(IRepository<TipoDeDocumento, int> repository)
            : base(repository)
        {
        }

        public bool IsCnpj(string cnpj)
        {
            if (!string.IsNullOrEmpty(cnpj))
            {
                int[] mt1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
                int[] mt2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

                cnpj = cnpj.Trim();
                cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

                if (cnpj.Length != 14)
                    return false;

                if (cnpj == "00000000000000" || cnpj == "11111111111111" ||
                    cnpj == "22222222222222" || cnpj == "33333333333333" ||
                    cnpj == "44444444444444" || cnpj == "55555555555555" ||
                    cnpj == "66666666666666" || cnpj == "77777777777777" ||
                    cnpj == "88888888888888" || cnpj == "99999999999999")
                    return false;

                var tempCnpj = cnpj.Substring(0, 12);
                var soma = 0;

                for (int i = 0; i < 12; i++)
                    soma += int.Parse(tempCnpj[i].ToString()) * mt1[i];

                var resto = (soma % 11);
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                var digito = resto.ToString();

                tempCnpj = tempCnpj + digito;
                soma = 0;
                for (int i = 0; i < 13; i++)
                    soma += int.Parse(tempCnpj[i].ToString()) * mt2[i];

                resto = (soma % 11);
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;
                digito = digito + resto.ToString();

                return cnpj.EndsWith(digito);
            }

            return false;
        }

        public bool IsCpf(string cpf)
        {
            if (!string.IsNullOrEmpty(cpf))
            {
                int[] mt1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
                int[] mt2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

                cpf = cpf.Trim();
                cpf = cpf.Replace(".", "").Replace("-", "");

                if (cpf.Length != 11)
                    return false;

                var tempCpf = cpf.Substring(0, 9);
                var soma = 0;
                for (int i = 0; i < 9; i++)
                    soma += int.Parse(tempCpf[i].ToString()) * mt1[i];

                var resto = soma % 11;
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                var digito = resto.ToString();
                tempCpf = tempCpf + digito;
                soma = 0;

                for (int i = 0; i < 10; i++)
                    soma += int.Parse(tempCpf[i].ToString()) * mt2[i];

                resto = soma % 11;
                if (resto < 2)
                    resto = 0;
                else
                    resto = 11 - resto;

                digito = digito + resto.ToString();

                return cpf.EndsWith(digito);
            }

            return false;
        }

        public bool IsPis(string pis)
        {
            int[] multiplicador = new int[10] { 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            if (pis.Trim().Length != 11)
                return false;
            pis = pis.Trim();
            pis = pis.Replace("-", "").Replace(".", "").PadLeft(11, '0');

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(pis[i].ToString()) * multiplicador[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            return pis.EndsWith(resto.ToString());
        }

        public TipoDeDocumento GetByTipoFixo(TipoDeDocumentoEnum tipoFixo)
        {
            var tipoDeDocumento = base.GetAll()
                .Where(x => x.TipoDeDocumentoFixo == tipoFixo)
                .FirstOrDefault();

            return tipoDeDocumento;
        }
    }
}