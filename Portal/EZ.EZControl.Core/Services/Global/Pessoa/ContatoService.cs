﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class ContatoService : EZControlDomainServiceBase<Contato>, IContatoService
    {
        public ContatoService(IRepository<Contato, int> repository)
            : base(repository)
        {
        }

        public void CanAssignPessoa(Contato contato, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (contato == null)
                throw new UserFriendlyException(L("Contato.EmptyContatoError"));

            if (pessoa == null)
                throw new UserFriendlyException(L("Contato.EmptyPessoaError"));

             // TODO: Marcos - 2016-07-24. Ocorre um erro para fazer a consulta no banco.
            //if (Repository.Count(x => x.TipoDeContato == contato.TipoDeContato && x.Nome == contato.Nome && x.Tratamento == contato.Tratamento && x.DataNascimento == contato.DataNascimento && x.Sexo == contato.Sexo) > 0)
            //    throw new UserFriendlyException("Este contato já foi cadastrado.");
            
        }

        public void CanAssignTipoDeContato(Contato contato, TipoDeContato tipoDeContato)
        {
            if (contato == null)
                throw new UserFriendlyException(L("Contato.EmptyContatoError"));

            if (tipoDeContato == null)
                throw new UserFriendlyException(L("Contato.EmptyTipoDeContatoError"));

        }

        public void CanAssignTratamento(Contato contato, Tratamento tratamento)
        {
            if (contato == null)
                throw new UserFriendlyException(L("Contato.EmptyContatoError"));

            if (tratamento == null)
                throw new UserFriendlyException(L("Tratamento.EmptyTratamentoError"));

        }
    }
}