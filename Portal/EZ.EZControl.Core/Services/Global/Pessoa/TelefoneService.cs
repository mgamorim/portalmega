﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class TelefoneService : EZControlDomainServiceBase<Telefone>, ITelefoneService
    {
        public TelefoneService(IRepository<Telefone, int> repository)
            : base(repository)
        {
        }

        public void CanAssignPessoa(Telefone telefone, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (telefone == null)
                throw new UserFriendlyException(L("Telefone.RequiredTelefoneError"));

            if (pessoa == null)
                throw new UserFriendlyException(L("Telefone.EmptyPessoaError"));

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="telefone"></param>
        /// <param name="numero">Deve estar no formato "(21) 98774-5454" conform o pattern Telefone.RegexTelefoneCelularBrPattern</param>
        public void PreencherNumeroCelularBrComMascaraByString(Telefone telefone, string numero)
        {
            Regex regex = new Regex(Telefone.RegexTelefoneCelularBrComMascaraPattern);
            Match match = regex.Match(numero);

            if (!match.Success)
            {
                throw new UserFriendlyException(L("Telefone.ErrorTelefoneCelularBrComMascaraNotValid"));
            }

            if (telefone == null)
            {
                throw new NotSupportedException("Para atribuir o numero, o telefone não pode estar nulo!");
            }

            telefone.DDD = match.Groups["DDD"].Value
                .Replace("(", String.Empty)
                .Replace(")", String.Empty);
            telefone.Numero = String.Concat(match.Groups["CAMPO1"], match.Groups["CAMPO2"], match.Groups["CAMPO3"]);
        }
    }
}