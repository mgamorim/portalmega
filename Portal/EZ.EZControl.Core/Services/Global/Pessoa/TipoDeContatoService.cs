﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class TipoDeContatoService : EZControlDomainServiceBase<TipoDeContato>, ITipoDeContatoService
    {
        public TipoDeContatoService(IRepository<TipoDeContato, int> repository)
            : base(repository)
        {
        }
    }
}