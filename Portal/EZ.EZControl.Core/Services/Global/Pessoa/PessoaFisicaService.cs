﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class PessoaFisicaService : PessoaService<PessoaFisica>, IPessoaFisicaService
    {
        public PessoaFisicaService(IRepository<PessoaFisica, int> repository)
            : base(repository)
        {
        }

        public async Task<PessoaFisica> GetPessoaByCpf(string cpf)
        {
            return await base.FirstOrDefault(x => x.Documentos.Any(y => y.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf && y.Numero.StartsWith(cpf)));
        }

        public void CanAssignTratamento(PessoaFisica pessoa, Tratamento tratamento)
        {
            if (pessoa == null)
                throw new UserFriendlyException(L("PessoaFisica.EmptyPessoaFisicaError"));

            if (tratamento == null)
                throw new UserFriendlyException(L("Tratamento.EmptyTratamentoError"));
        }
    }
}