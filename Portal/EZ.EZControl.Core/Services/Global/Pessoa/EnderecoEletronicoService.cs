﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class EnderecoEletronicoService : EZControlDomainServiceBase<EnderecoEletronico>, IEnderecoEletronicoService
    {
        public EnderecoEletronicoService(IRepository<EnderecoEletronico, int> repository)
            : base(repository)
        {
        }

        public void CanAssignPessoa(EnderecoEletronico enderecoEletronico, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (enderecoEletronico == null)
                throw new UserFriendlyException(L("EnderecoEletronico.RequiredEnderecoError"));

            if (pessoa == null)
                throw new UserFriendlyException(L("EnderecoEletronico.EmptyPessoaError"));
            
        }

        public void CanAssignContato(EnderecoEletronico enderecoEletronico, Contato contato)
        {
            if (enderecoEletronico == null)
                throw new UserFriendlyException(L("EnderecoEletronico.RequiredEnderecoError"));

            if (contato == null)
                throw new UserFriendlyException(L("Contato.EmptyContatoError"));

            
        }
    }
}