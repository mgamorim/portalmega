﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class DocumentoService : EZControlDomainServiceBase<Documento>, IDocumentoService
    {
        public DocumentoService(IRepository<Documento, int> repository)
            : base(repository)
        {
        }

        public void CanAssignPessoa(Documento documento, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (documento == null)
                throw new UserFriendlyException(L("Documento.EmptyError"));

            if (pessoa == null)
                throw new UserFriendlyException(L("Documento.EmptyPessoaError"));


        }

        public void CanAssignTipoDocumento(Documento documento, TipoDeDocumento tipoDeDocumento)
        {
            if (documento == null)
                throw new UserFriendlyException(L("Documento.EmptyDocumentoError"));

            if (tipoDeDocumento == null)
                throw new UserFriendlyException(L("TipoDeDocumento.EmptyTipoDeDocumentoError"));


        }

        public void CanDelete(Documento documento)
        {
            if (documento == null)
                throw new UserFriendlyException(L("Documento.EmptyDocumentoError"));

            if (documento.Pessoa.TipoPessoa == TipoPessoaEnum.Fisica || documento.Pessoa.TipoPessoa == TipoPessoaEnum.Juridica)
            {
                if (documento.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cnpj ||
                    documento.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf)
                {
                    throw new UserFriendlyException(L("Documento.InvalidDocumentoDeleteError"));
                }
            }
        }
            
    }
}