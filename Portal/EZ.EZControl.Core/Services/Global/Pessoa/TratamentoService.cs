﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public class TratamentoService : EZControlDomainServiceBase<Tratamento>, ITratamentoService
    {
        public TratamentoService(IRepository<Tratamento, int> repository)
            : base(repository)
        {
        }
    }
}