﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class FeriadoService : EZControlDomainServiceBase<Feriado>, IFeriadoService
    {
        public FeriadoService(IRepository<Feriado, int> repository) : base(repository)
        {
        }
    }
}
