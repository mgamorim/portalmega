﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class ProfissaoService : EZControlDomainServiceBase<Profissao>, IProfissaoService
    {
        public ProfissaoService(IRepository<Profissao, int> repository) : base(repository)
        {
        }
    }
}
