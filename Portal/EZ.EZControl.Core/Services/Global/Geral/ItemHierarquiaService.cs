﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class ItemHierarquiaService : EZControlDomainServiceBase<ItemHierarquia>, IItemHierarquiaService
    {
        public ItemHierarquiaService(IRepository<ItemHierarquia, int> repository) : base(repository)
        {
        }
    }
}
