﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class ConexaoSMTPService : EZControlDomainServiceBase<ConexaoSMTP>, IConexaoSMTPService
    {
        public ConexaoSMTPService(IRepository<ConexaoSMTP, int> repository) : base(repository)
        {
        }
    }
}
