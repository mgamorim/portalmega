﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class TemplateEmailService : EZControlDomainServiceBase<TemplateEmail>, ITemplateEmailService
    {
        public TemplateEmailService(IRepository<TemplateEmail, int> repository) : base(repository)
        {
        }
    }
}
