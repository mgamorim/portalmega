﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IPerfilService : IService<PessoaFisica>
    {

    }
}
