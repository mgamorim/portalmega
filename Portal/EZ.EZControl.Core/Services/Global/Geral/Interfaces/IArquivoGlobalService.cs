﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IArquivoGlobalService : IArquivoBaseService<ArquivoGlobal>
    {
    }
}
