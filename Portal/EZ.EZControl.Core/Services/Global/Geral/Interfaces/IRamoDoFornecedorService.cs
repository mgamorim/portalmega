﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IRamoDoFornecedorService : IService<RamoDoFornecedor>
    {
    }
}
