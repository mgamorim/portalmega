﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class DepartamentoService : EZControlDomainServiceBase<Departamento>, IDepartamentoService
    {
        public DepartamentoService(IRepository<Departamento, int> repository) : base(repository)
        {
        }
    }
}
