﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Services.Global.Geral
{
    public class PerfilService : EZControlDomainServiceBase<PessoaFisica>, IPerfilService
    {
        public PerfilService(IRepository<PessoaFisica, int> repository)
            : base(repository)
        {
        }
    }
}
