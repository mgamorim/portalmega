﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class AgenciaService : EZControlDomainServiceBase<Agencia>, IAgenciaService
    {
        public AgenciaService(IRepository<Agencia, int> repository) : base(repository)
        {
        }
    }
}
