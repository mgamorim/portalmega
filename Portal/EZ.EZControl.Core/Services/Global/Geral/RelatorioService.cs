﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;
using System;
using Abp.UI;
using System.Linq;

namespace EZ.EZControl.Services.Global.Geral
{
    public class RelatorioService : EZControlDomainServiceBase<Relatorio>, IRelatorioService
    {
        public RelatorioService(IRepository<Relatorio, int> repository) : base(repository)
        {
        }

    }
}
