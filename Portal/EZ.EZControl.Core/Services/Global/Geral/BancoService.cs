﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;
using System;
using Abp.UI;
using System.Linq;

namespace EZ.EZControl.Services.Global.Geral
{
    public class BancoService : EZControlDomainServiceBase<Banco>, IBancoService
    {
        public BancoService(IRepository<Banco, int> repository) : base(repository)
        {
        }

        public void CanAssignAgencia(Banco banco, Agencia agencia)
        {
            if (banco == null)
                throw new UserFriendlyException(L("Banco.EmptyBancoError"));
            if (agencia == null)
                throw new UserFriendlyException(L("Agencia.EmptyAgenciaError"));
            if (banco.Agencias.Any(x => x.Id == agencia.Id))
                throw new UserFriendlyException(L("Agencia.AssociateAgenciaError"));

        }
    }
}
