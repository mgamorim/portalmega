﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Core.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Services.Global.Geral
{
    public class ParametroGlobalService : ParametroBaseService<ParametroGlobal>, IParametroGlobalService
    {
        public ParametroGlobalService(IRepository<ParametroGlobal, int> repository)
            : base(repository)
        {
        }
    }
}