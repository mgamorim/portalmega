﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Abp.Domain.Repositories;

namespace EZ.EZControl.Services.Global.Geral
{
    public class RamoDoFornecedorService : EZControlDomainServiceBase<RamoDoFornecedor>, IRamoDoFornecedorService
    {
        public RamoDoFornecedorService(IRepository<RamoDoFornecedor, int> repository) : base(repository)
        {
        }
    }
}
