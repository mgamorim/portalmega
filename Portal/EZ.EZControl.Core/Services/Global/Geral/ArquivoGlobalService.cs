﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;

namespace EZ.EZControl.Services.Global.Geral
{
    public class ArquivoGlobalService : EZControlDomainServiceBase<ArquivoGlobal>, IArquivoGlobalService
    {
        public ArquivoGlobalService(IRepository<ArquivoGlobal, Int32> repository)
            : base(repository)
        {

        }
    }
}
