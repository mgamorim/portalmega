﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;

namespace EZ.EZControl.Services.Global.SubtiposPessoas
{
    public class UsuarioPorPessoaService : EZControlDomainServiceBase<UsuarioPorPessoa>, IUsuarioPorPessoaService
    {
        public UsuarioPorPessoaService(IRepository<UsuarioPorPessoa, int> repository)
            : base(repository)
        {
        }
    }
}