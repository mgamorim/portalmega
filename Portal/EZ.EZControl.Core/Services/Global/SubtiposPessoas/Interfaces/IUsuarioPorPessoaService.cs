﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces
{
    public interface IUsuarioPorPessoaService : IService<UsuarioPorPessoa>
    {
    }
}