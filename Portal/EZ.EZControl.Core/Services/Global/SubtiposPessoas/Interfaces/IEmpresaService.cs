﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces
{
    public interface IEmpresaService : IService<Empresa>
    {
        Task<Empresa> GetEmpresaLogada();
        Task<Empresa> GetEmpresaDefault();
        Task<Empresa> GetEmpresaByPessoaJuridicaId(int id);
        Task AssociaUsuarioEmpresa(User usuario, Empresa empresa);
        Task DesassociaUsuarioEmpresa(User usuario, Empresa empresa);
    }
}