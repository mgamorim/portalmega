﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;

namespace EZ.EZControl.Services.Global.SubtiposPessoas
{
    public class FornecedorService : EZControlDomainServiceBase<Fornecedor>, IFornecedorService
    {
        public FornecedorService(IRepository<Fornecedor, int> repository)
            : base(repository)
        {
        }
    }
}