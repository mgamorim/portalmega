﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoas
{
    public class EmpresaService : EZControlDomainServiceBase<Empresa>, IEmpresaService
    {
        public EmpresaService(IRepository<Empresa, int> repository)
            : base(repository)
        {
        }
        public async Task<Empresa> GetEmpresaLogada()
        {
            ClaimsPrincipal principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            ClaimsIdentity identity = principal.Identity as ClaimsIdentity;
            var empresaClaim = identity.FindFirst("EmpresaId");
            var empresaId = Convert.ToInt32(empresaClaim.Value);
            return await GetById(empresaId);
        }

        public async Task<Empresa> GetEmpresaDefault()
        {
            var empresaDefault = await Repository.FirstOrDefaultAsync(x => x.NomeFantasia.Equals("EZLiv"));

            if (empresaDefault == null)
                throw new UserFriendlyException("Empresa default EZLiv não encontrada. É preciso criar a empresa EZLiv.");

            return empresaDefault;
        }

        public async Task<Empresa> GetEmpresaByPessoaJuridicaId(int id)
        {
            var empresaObj = await Repository.FirstOrDefaultAsync(x => x.PessoaJuridicaId == id);

            if (empresaObj == null)
                throw new UserFriendlyException("Empresa não encontrada. É preciso criar a empresa.");

            return empresaObj;
        }

        public async Task AssociaUsuarioEmpresa(User usuario, Empresa empresa)
        {
            empresa.Usuarios.Add(usuario);
            await Repository.UpdateAsync(empresa);
        }

        public async Task DesassociaUsuarioEmpresa(User usuario, Empresa empresa)
        {
            empresa.Usuarios.Remove(usuario);
            await Repository.UpdateAsync(empresa);
        }
    }
}