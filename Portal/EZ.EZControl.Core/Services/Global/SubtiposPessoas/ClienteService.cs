﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;

namespace EZ.EZControl.Services.Global.SubtiposPessoas
{
    public class ClienteService : EZControlDomainServiceBase<Cliente>, IClienteService
    {
        public ClienteService(IRepository<Cliente, int> repository)
            : base(repository)
        {
        }
    }
}