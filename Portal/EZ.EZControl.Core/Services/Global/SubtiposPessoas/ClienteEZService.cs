﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;

namespace EZ.EZControl.Services.Global.SubtiposPessoas
{
    public class ClienteEZService : EZControlDomainServiceBase<ClienteEZ>, IClienteEZService
    {
        public ClienteEZService(IRepository<ClienteEZ, int> repository)
            : base(repository)
        {
        }
    }
}