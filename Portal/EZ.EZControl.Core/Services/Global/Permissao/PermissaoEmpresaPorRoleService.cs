﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Services.Global.Permissao.Interfaces;

namespace EZ.EZControl.Services.Global.Permissao
{
    public class PermissaoEmpresaPorRoleService : EZControlDomainServiceBase<PermissaoEmpresaPorRole>, IPermissaoEmpresaPorRoleService
    {
        public PermissaoEmpresaPorRoleService(IRepository<PermissaoEmpresaPorRole, int> repository)
            : base(repository)
        {
        }
    }
}