﻿using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Global.Permissao.Interfaces
{
    public interface IPermissaoEmpresaPorUsuarioService : IService<PermissaoEmpresaPorUsuario>
    {
    }
}