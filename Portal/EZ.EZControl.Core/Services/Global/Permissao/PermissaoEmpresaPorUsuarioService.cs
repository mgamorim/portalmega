﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Services.Global.Permissao.Interfaces;

namespace EZ.EZControl.Services.Global.Permissao
{
    public class PermissaoEmpresaPorUsuarioService : EZControlDomainServiceBase<PermissaoEmpresaPorUsuario>, IPermissaoEmpresaPorUsuarioService
    {
        public PermissaoEmpresaPorUsuarioService(IRepository<PermissaoEmpresaPorUsuario, int> repository)
            : base(repository)
        {
        }
    }
}