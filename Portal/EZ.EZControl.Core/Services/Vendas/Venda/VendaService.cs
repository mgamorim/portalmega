﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Vendas.Venda.Interfaces;

namespace EZ.EZControl.Services.Vendas.Venda
{
    public class VendaService : EZControlDomainServiceBase<Pedido>, IVendaService
    {
        public VendaService(IRepository<Pedido, int> repository)
            : base(repository)
        {
        }
    }
}