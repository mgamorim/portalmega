﻿using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Vendas.Venda.Interfaces
{
    public interface IVendaService : IService<Pedido>
    {
    }
}