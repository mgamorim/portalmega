﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System.Linq;

namespace EZ.EZControl.Services.Vendas.Pedidos
{
    public class PedidoService : EZControlDomainServiceBase<Pedido>, IPedidoService
    {
        public PedidoService(IRepository<Pedido, int> repository)
            : base(repository)
        {
        }

        public void ChecarSePodeAtribuirCliente(Cliente cliente, Pedido pedido)
        {
            if (cliente == null)
            {
                throw new UserFriendlyException(L("PedidoPessoaObrigatoria"));
            }
        }

        public void ChecarSePodeAdicionarItemDePedido(ItemDePedido itemDePedido, Pedido pedido)
        {
            if (itemDePedido.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto)
            {
                if (pedido.ItensDePedidoReadOnly.Any(x => x.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto && x.Produto != null && x.Produto.Id == itemDePedido.Produto.Id))
                    throw new UserFriendlyException(L("PedidoProdutoJaAdicionado"));
            }
            else if (itemDePedido.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Servico)
            {
                if (pedido.ItensDePedidoReadOnly.Any(x => x.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Servico && x.Servico != null && x.Servico.Id == itemDePedido.Servico.Id))
                    throw new UserFriendlyException(L("PedidoServicoJaAdicionado"));
            }
        }

        public bool ChecarSePodeRemoverItemDePedido(ItemDePedido itemDePedido, Pedido pedido)
        {
            return pedido.ItensDePedidoReadOnly.Contains(itemDePedido);
        }
    }
}