﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;

namespace EZ.EZControl.Services.Vendas.Pedidos
{
    public class ItemDePedidoService : EZControlDomainServiceBase<ItemDePedido>, IItemDePedidoService
    {
        public ItemDePedidoService(IRepository<ItemDePedido, int> repository)
            : base(repository)
        {
        }

        public void ChecarSePodeAtribuirProdutoParaItemDePedido(ItemDePedido itemDePedido, Produto produto)
        {
            if (!produto.IsActive)
            {
                throw new UserFriendlyException(L("ErroAoChecarSeEstaAtivo"));
            }

            if (itemDePedido.TipoDeItemDePedido != TipoDeItemDePedidoEnum.Produto)
            {
                throw new UserFriendlyException(L("ItemDePedido.ErroAoChecarTipoDeItemDePedidoProduto"));
            }
        }

        public void ChecarSePodeAtribuirServicoParaItemDePedido(ItemDePedido itemDePedido, Servico servico)
        {
            if (!servico.IsActive)
            {
                throw new UserFriendlyException(L("ErroAoChecarSeEstaAtivo"));
            }

            if (itemDePedido.TipoDeItemDePedido != TipoDeItemDePedidoEnum.Servico)
            {
                throw new UserFriendlyException(L("ItemDePedido.ErroAoChecarTipoDeItemDePedidoServico"));
            }
        }

        public void ChecarSePodeAtribuirPedidoParaItemDePedido(ItemDePedido itemDePedido, Pedido pedido)
        {
            if (itemDePedido.Pedido != null && itemDePedido.Pedido.Id != pedido.Id)
            {
                throw new UserFriendlyException(("ItemDePedido.ItemJaEstaAssociadoAOutroPedido"));
            }
        }

        public void ChecarSePodeAlterarQuantidadeDoItem(ItemDePedido itemDePedido, decimal quantidade)
        {
            if (quantidade <= 0)
            {
                throw new UserFriendlyException(("ItemDePedidos.QuantidadeInvalida"));
            }
        }
    }
}