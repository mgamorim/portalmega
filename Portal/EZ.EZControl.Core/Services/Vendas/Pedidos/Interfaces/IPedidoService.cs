﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Vendas.Pedidos.Interfaces
{
    public interface IPedidoService : IService<Pedido>
    {
        void ChecarSePodeAtribuirCliente(Cliente cliente, Pedido pedido);

        void ChecarSePodeAdicionarItemDePedido(ItemDePedido itemDePedido, Pedido pedido);

        bool ChecarSePodeRemoverItemDePedido(ItemDePedido itemDePedido, Pedido pedido);
    }
}