﻿using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Vendas.Pedidos.Interfaces
{
    public interface IItemDePedidoService : IService<ItemDePedido>
    {
        void ChecarSePodeAtribuirProdutoParaItemDePedido(ItemDePedido itemDePedido, Produto produto);

        void ChecarSePodeAtribuirServicoParaItemDePedido(ItemDePedido itemDePedido, Servico servico);

        void ChecarSePodeAtribuirPedidoParaItemDePedido(ItemDePedido itemDePedido, Pedido pedido);

        void ChecarSePodeAlterarQuantidadeDoItem(ItemDePedido itemDePedido, decimal quantidade);
    }
}