﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Financiero.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Services.Agenda
{
    public class DocumentoAReceberService : EZControlDomainServiceBase<DocumentoAReceber>, IDocumentoAReceberService
    {
        public DocumentoAReceberService(IRepository<DocumentoAReceber, int> repository)
            : base(repository)
        {
        }

        public bool CanCreateDocumentoAReceber(int entityId, string numero, string complemento, int clienteId)
        {
            return ApplyEmpresaFilter(() => {
                var documentos = new List<DocumentoAReceber>();
                if (entityId == 0)
                {
                    documentos = Repository.GetAll().Where(x => x.Numero == numero && x.Complemento == complemento && x.Cliente.Id == clienteId).ToList();
                }
                else
                {
                    documentos = Repository.GetAll().Where(x => x.Id != entityId && x.Numero == numero && x.Complemento == complemento && x.Cliente.Id == clienteId).ToList();
                }
                return (documentos.Count <= 0);
            });
        }
    }
}