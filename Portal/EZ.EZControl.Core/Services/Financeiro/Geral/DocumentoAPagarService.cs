﻿using System;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Financiero.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace EZ.EZControl.Services.Agenda
{
    public class DocumentoAPagarService : EZControlDomainServiceBase<DocumentoAPagar>, IDocumentoAPagarService
    {
        public DocumentoAPagarService(IRepository<DocumentoAPagar, int> repository)
            : base(repository)
        {
        }

        public bool CanCreateDocumentoAPagar(int entityId, string numero, string complemento, int fornecedorId)
        {
            return ApplyEmpresaFilter(() =>
            {
                var documentos = new List<DocumentoAPagar>();
                if (entityId == 0)
                {
                    documentos = Repository.GetAll().Where(x => x.Numero == numero && x.Complemento == complemento && x.Fornecedor.Id == fornecedorId).ToList();
                }
                else
                {
                    documentos = Repository.GetAll().Where(x => x.Id != entityId && x.Numero == numero && x.Complemento == complemento && x.Fornecedor.Id == fornecedorId).ToList();
                }
                return (documentos.Count <= 0);
            });
        }
    }
}