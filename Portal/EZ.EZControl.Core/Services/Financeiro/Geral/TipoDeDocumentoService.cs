﻿using System;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Financiero.Interfaces;
using System.Linq;

namespace EZ.EZControl.Services.Agenda
{
    public class TipoDeDocumentoService : EZControlDomainServiceBase<TipoDeDocumentoFinanceiro>, ITipoDeDocumentoService
    {
        public TipoDeDocumentoService(IRepository<TipoDeDocumentoFinanceiro, int> repository)
            : base(repository)
        {
        }

        public bool CanCreate(string nome, TipoDeDocumentoFinanceiroEnum tipo)
        {
            return ApplyEmpresaFilter(() => {
                if (Repository.GetAll().Where(x => x.Nome == nome && x.Tipo == tipo).Count() > 0)
                {
                    return false;
                }
                return true;
            });
        }
    }
}