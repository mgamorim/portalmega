﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Financiero.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class DocumentoBaseService : EZControlDomainServiceBase<DocumentoBase>, IDocumentoBaseService
    {
        public DocumentoBaseService(IRepository<DocumentoBase, int> repository)
            : base(repository)
        {
        }
    }
}