﻿using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Financiero.Interfaces
{
    public interface IDocumentoAPagarService : IService<DocumentoAPagar>
    {
        bool CanCreateDocumentoAPagar(int entityId, string numero, string complemento, int fornecedorId);
    }
}