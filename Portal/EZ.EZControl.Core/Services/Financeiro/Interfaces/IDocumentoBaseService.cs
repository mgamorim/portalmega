﻿using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Financiero.Interfaces
{
    public interface IDocumentoBaseService : IService<DocumentoBase>
    {
    }
}