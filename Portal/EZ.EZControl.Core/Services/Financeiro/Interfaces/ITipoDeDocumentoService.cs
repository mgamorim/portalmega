﻿using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Financiero.Interfaces
{
    public interface ITipoDeDocumentoService : IService<TipoDeDocumentoFinanceiro>
    {
        bool CanCreate(string nome, TipoDeDocumentoFinanceiroEnum tipo);
    }
}