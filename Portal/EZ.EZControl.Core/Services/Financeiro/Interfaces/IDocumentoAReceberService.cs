﻿using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Financiero.Interfaces
{
    public interface IDocumentoAReceberService : IService<DocumentoAReceber>
    {
        bool CanCreateDocumentoAReceber(int entityId, string numero, string complemento, int clienteId);
    }
}