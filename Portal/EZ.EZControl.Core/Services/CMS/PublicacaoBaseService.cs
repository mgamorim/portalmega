﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class PublicacaoBaseService : EZControlDomainServiceBase<PublicacaoBase>, IPublicacaoBaseService
    {
        public PublicacaoBaseService(IRepository<PublicacaoBase, int> repository)
            : base(repository)
        {
        }
    }
}