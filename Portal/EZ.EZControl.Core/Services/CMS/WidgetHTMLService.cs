﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Services.CMS
{
    public class WidgetHTMLService : WidgetBaseService<WidgetHTML>, IWidgetHTMLService
    {
        public WidgetHTMLService(IRepository<WidgetHTML, int> repository)
                : base(repository)
        {
        }

        public string CreateWidgetHtml(WidgetHTML widget)
        {
            return base.CreateWidgetHtml(widget, widget.Conteudo);
        }
    }
}
