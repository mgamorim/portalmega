﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.Core.Geral;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class ParametroCMSService : ParametroBaseService<ParametroCMS>, IParametroCMSService
    {
        public ParametroCMSService(IRepository<ParametroCMS, int> repository)
            : base(repository)
        {
        }
    }
}