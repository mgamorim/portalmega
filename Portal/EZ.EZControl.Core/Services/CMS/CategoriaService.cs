﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Filters;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class CategoriaService : EZControlDomainServiceBase<Categoria>, ICategoriaService
    {
        public CategoriaService(IRepository<Categoria, int> repository)
            : base(repository)
        {
        }
        public string FormataEstruturaCategorias(Categoria categoria, string urlPredicate)
        {
            urlPredicate = string.Format(urlPredicate, categoria.Nome.Slugify());
            if (categoria.CategoriaPai != null) urlPredicate = FormataEstruturaCategorias(categoria.CategoriaPai, "{0}/" + urlPredicate);
            return urlPredicate;
        }

        /// <summary>
        /// Sempre vai retornar a categoria mais específica.
        /// Exemplos: 
        /// [categoria1, categoria2, categoria3] retorna a categoria3
        /// [categoria1, categoria2] retorna a categoria2
        /// [categoria1] retorna a categoria1
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="slugs"></param>
        /// <returns></returns>
        [UnitOfWork]
        public async Task<Categoria> FirstOrDefaultAsync(int siteId, List<string> slugs)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter, AbpDataFilters.MustHaveTenant))
            {
                if (slugs != null && slugs.Count > 0 && siteId > 0)
                {
                    var primeiroSlug = slugs.First();
                    var categoriaPai =
                        await Repository.FirstOrDefaultAsync(x => x.SiteId == siteId && x.Slug == primeiroSlug);
                    if (categoriaPai != null)
                    {
                        // Evita a necessidade de fazer outras pesquisas 
                        if (categoriaPai.Categorias.Count > 0 && slugs.Count > 1)
                        {
                            List<string> slugsFilhos = GetAllSlugsSemPai(slugs);
                            return UltimaCategoriaFilhoOrDefault(categoriaPai, slugsFilhos);
                        }
                        return categoriaPai;
                    }
                    return null;
                }
                return null;
            }
        }

        /// <summary>
        /// Retorna a última categoria filho a partir da hierarquia do parâmetro slugs filhos do primeiro para último.
        /// Ex: [categoria, subcategoria, subsubcategoria] é igual a categoria\subcategoria\subsubcategoria (pertencente a uma URL)  
        /// </summary>
        /// <param name="categoriaPai"></param>
        /// <param name="slugsFilhos"></param>
        /// <returns></returns>
        [UnitOfWork]
        private Categoria UltimaCategoriaFilhoOrDefault(Categoria categoriaPai, List<string> slugsFilhos)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter, AbpDataFilters.MustHaveTenant))
            {
                if (categoriaPai != null)
                {
                    if (categoriaPai.Categorias.Count > 0)
                    {
                        var primeiroSlug = slugsFilhos.First();
                        var categoriaFilho = categoriaPai.Categorias.FirstOrDefault(x => x.Slug == primeiroSlug);
                        if (categoriaFilho != null)
                        {
                            // Evita a necessidade de fazer outras pesquisas 
                            if (slugsFilhos.Count == 1)
                                return categoriaFilho;

                            if (categoriaFilho.Categorias.Count > 0)
                            {
                                List<string> slugsFilhosDosFilho = GetAllSlugsSemPai(slugsFilhos);
                                return UltimaCategoriaFilhoOrDefault(categoriaFilho, slugsFilhosDosFilho);
                            }

                            return null;
                        }
                        return null;
                    }
                    return null;
                }
                return null;
            }
        }

        private List<string> GetAllSlugsSemPai(List<string> slugs)
        {
            List<string> slugsFilhos = new List<string>();

            foreach (var slug in slugs)
            {
                if (slugs.First().Equals(slug))
                    continue;
                slugsFilhos.Add(slug);
            }

            return slugsFilhos;
        }
    }
}