﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class HistoricoPublicacaoService : EZControlDomainServiceBase<HistoricoPublicacao>, IHistoricoPublicacaoService
    {
        public HistoricoPublicacaoService(IRepository<HistoricoPublicacao, int> repository)
            : base(repository)
        {
        }
    }
}