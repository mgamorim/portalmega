﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using System;

namespace EZ.EZControl.Services.CMS
{
    public class TagService : EZControlDomainServiceBase<Tag>, ITagService
    {
        public TagService(IRepository<Tag, Int32> repository)
            : base(repository)
        {

        }
    }
}
