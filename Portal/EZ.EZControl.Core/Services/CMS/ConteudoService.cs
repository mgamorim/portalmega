﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using EZ.EZControl.Filters;

namespace EZ.EZControl.Services.CMS
{
    public class ConteudoService : EZControlDomainServiceBase, IConteudoService
    {
        private readonly ICategoriaService _categoriaService;
        private readonly IPostService _postService;
        private readonly IHostService _hostService;
        private readonly IRepository<Pagina, int> _paginaRepository;
        private readonly IRepository<Post, int> _postRepository;
        private readonly IRepository<Categoria, int> _categoriaRepository;


        public ConteudoService(ICategoriaService categoriaService,
            IRepository<Pagina, int> paginaRepository, 
            IRepository<Categoria, int> categoriaRepository,
            IRepository<Post, int> postRepository, 
            IPostService postService, 
            IHostService hostService)
        {
            _categoriaService = categoriaService;
            _paginaRepository = paginaRepository;
            _categoriaRepository = categoriaRepository;
            _postRepository = postRepository;
            _postService = postService;
            _hostService = hostService;
        }

        public async Task<No> GetNoBySlugAsync(
            int siteId,
            string slug,
            bool buscaPagina = true,
            bool buscaPost = true,
            bool buscaCategoria = true)
        {
            if (siteId > 0)
            {
                if (buscaPagina)
                {
                    var pagina = await _paginaRepository.FirstOrDefaultAsync(x => x.SiteId == siteId && x.Slug == slug);
                    if (pagina != null)
                        return new No()
                        {
                            TipoDeNo = TipoDeNoEnum.Pagina,
                            Pagina = pagina
                        };
                }

                if (buscaPost)
                {
                    var post = await _postRepository.FirstOrDefaultAsync(x => x.SiteId == siteId && x.Slug == slug);
                    if (post != null)
                        return new No()
                        {
                            TipoDeNo = TipoDeNoEnum.Post,
                            Post = post
                        };
                }

                if (buscaCategoria)
                {
                    var categoria =
                        await _categoriaRepository.FirstOrDefaultAsync(x => x.SiteId == siteId && x.Slug == slug);
                    if (categoria != null)
                        return new No()
                        {
                            TipoDeNo = TipoDeNoEnum.Categoria,
                            Categoria = categoria
                        };
                }
            }

            return null;
        }

        /// <summary>
        /// Baseada na estrutura, exemplos:
        /// http://site.com
        /// http://site.com/pagina
        /// http://site.com/categoria
        /// http://site.com/2015/
        /// http://site.com/2015/09/
        /// http://site.com/categoria/subcategoria/... (Considerar Ano, mês e post também)
        /// http://site.com/categoria/2015/
        /// http://site.com/categoria/2015/09/
        /// http://site.com/categoria/2015/09/o-meu-post-pelo-slug (Post sempre vai ter uma categoria)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="numeroDapagina"></param>
        /// <param name="itemPorPagina"></param>
        /// <returns></returns>
        [UnitOfWork]
        public async Task<Conteudo> FirstOrDefaultAsync(string url, int numeroDapagina = 1, int itemPorPagina = 10)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter, AbpDataFilters.MustHaveTenant))
            {
                Uri uri;
                try { uri = new Uri(url); } catch { return null; }

                var host = await _hostService.FisrtOrDefaultAsync(uri.Host);
                if (host != null)
                {
                    int anoMinimo = 2000;
                    int anoMaximo = 2200;

                    var segmentos = GetSegmentos(uri);
                    var segmentosInt = new int[segmentos.Length];

                    for (int i = 0; i < segmentos.Length; i++)
                    {
                        //Seta os inteiro para serem usados como data e para diferenciar do slug 
                        try { segmentosInt.SetValue(Convert.ToInt32(segmentos[i]), i); }
                        catch { segmentosInt.SetValue(default(int), i); }

                        if (string.IsNullOrWhiteSpace(segmentos[i]))
                            return null;
                    }

                    //site.com/algo - Pode ser categoria, página ou posts por ano
                    if (segmentos.Length == 1)
                    {
                        var seg1 = segmentos[0];
                        int seg1Int = segmentosInt[0];

                        bool ePostsPorAno = seg1Int >= anoMinimo && seg1Int <= anoMaximo;
                        if (ePostsPorAno)
                        {
                            return new Conteudo()
                            {
                                AnoDosPosts = seg1Int,
                                Posts = _postService.GetAll(host.SiteId, seg1Int, numeroDapagina: numeroDapagina, itemPorpagina: itemPorPagina),
                                TipoDeConteudo = TipoDeConteudoEnum.PostsPorAno,
                                NumeroDaPaginaPosts = numeroDapagina,
                                ItemPorPaginaPosts = itemPorPagina
                            };
                        }

                        var no = await GetNoBySlugAsync(host.SiteId, seg1, buscaPost: false);

                        if (no != null)
                        {
                            if (no.TipoDeNo == TipoDeNoEnum.Categoria)
                            {
                                return new Conteudo()
                                {
                                    Categoria = no.Categoria,
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComUltimosPosts,
                                    Posts = _postService.GetAllUltimosPublicados(host.SiteId, no.Categoria.Id, numeroDapagina, itemPorPagina),
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }

                            if (no.TipoDeNo == TipoDeNoEnum.Pagina)
                            {
                                return new Conteudo()
                                {
                                    Pagina = no.Pagina,
                                    TipoDeConteudo = TipoDeConteudoEnum.Pagina
                                };
                            }
                        }

                        return null;
                    }

                    //site.com/algo/algo - Pode ser categoria(Subcategoria), posts por ano por categoria ou posts por mës
                    if (segmentos.Length == 2)
                    {
                        string seg1 = segmentos[0],
                               seg2 = segmentos[1];

                        int seg1Int = segmentosInt[0],
                            seg2Int = segmentosInt[1];

                        bool ePostsPorMes = seg1Int >= anoMinimo && seg1Int <= anoMaximo && seg2Int > 0 && seg2Int <= 12;
                        if (ePostsPorMes)
                        {
                            return new Conteudo()
                            {
                                AnoDosPosts = seg1Int,
                                MesDosPosts = seg2Int,
                                Posts = _postService.GetAll(host.SiteId, seg1Int, seg2Int, numeroDapagina, itemPorPagina),
                                TipoDeConteudo = TipoDeConteudoEnum.PostsPorMes,
                                NumeroDaPaginaPosts = numeroDapagina,
                                ItemPorPaginaPosts = itemPorPagina
                            };
                        }

                        bool eCategoriaComPostsPorAno = seg1Int == default(int) && seg2Int >= anoMinimo && seg2Int <= anoMaximo;
                        if (eCategoriaComPostsPorAno)
                        {
                            var no = await GetNoBySlugAsync(host.SiteId, seg1, buscaPost: false, buscaPagina: false);

                            if (no != null)
                            {
                                return new Conteudo()
                                {
                                    AnoDosPosts = seg2Int,
                                    Categoria = no.Categoria,
                                    Posts = _postService.GetAll(host.SiteId, seg2Int, no.Categoria.Id, numeroDapagina, itemPorPagina),
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComPostsPorAno,
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                        }

                        bool eCategoriaComSubCategoria = seg1Int == default(int) && seg2Int == default(int);
                        if (eCategoriaComSubCategoria)
                        {
                            var slugsDeCategoria = new List<string>() { seg1, seg2 };
                            var subCategoria = await _categoriaService.FirstOrDefaultAsync(host.SiteId, slugsDeCategoria);
                            if (subCategoria != null)
                            {
                                return new Conteudo()
                                {
                                    Categoria = subCategoria,
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComUltimosPosts,
                                    Posts = _postService.GetAllUltimosPublicados(host.SiteId, subCategoria.Id, numeroDapagina, itemPorPagina),
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                            return null;
                        }

                        return null;
                    }

                    //site.com/algo/algo/algo - Pode ser categoria(Subcategoria), posts por mês por categoria ou posts por ano por subcategoria
                    if (segmentos.Length == 3)
                    {
                        string seg1 = segmentos[0],
                               seg2 = segmentos[1],
                               seg3 = segmentos[2];

                        int seg1Int = segmentosInt[0],
                            seg2Int = segmentosInt[1],
                            seg3Int = segmentosInt[2];

                        bool eCategoriaComPostsPorMes = seg1Int == default(int) && seg2Int >= anoMinimo && seg2Int <= anoMaximo && seg3Int > 0 && seg3Int <= 12;
                        if (eCategoriaComPostsPorMes)
                        {
                            var no = await GetNoBySlugAsync(host.SiteId, seg1, buscaPost: false, buscaPagina: false);

                            if (no != null)
                            {
                                return new Conteudo()
                                {
                                    AnoDosPosts = seg2Int,
                                    Categoria = no.Categoria,
                                    Posts = _postService.GetAll(host.SiteId, seg2Int, seg3Int, no.Categoria.Id, numeroDapagina, itemPorPagina),
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComPostsPorMes,
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                        }

                        bool eSubCategoriaComPostsPorAno = seg1Int == default(int) && seg2Int == default(int) && seg3Int >= anoMinimo && seg3Int <= anoMaximo;
                        if (eSubCategoriaComPostsPorAno)
                        {
                            var slugsDeCategoria = new List<string>() { seg1, seg2 };
                            var categoria = await _categoriaService.FirstOrDefaultAsync(host.SiteId, slugsDeCategoria);
                            if (categoria != null)
                            {
                                return new Conteudo()
                                {
                                    AnoDosPosts = seg3Int,
                                    Categoria = categoria,
                                    Posts = _postService.GetAll(host.SiteId, seg3Int, categoria.Id, numeroDapagina, itemPorPagina),
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComPostsPorAno,
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                            return null;
                        }

                        bool eCategoriaComSubCategorias = seg1Int == default(int) && seg2Int == default(int) && seg3Int == default(int);
                        if (eCategoriaComSubCategorias)
                        {
                            var slugsDeCategoria = new List<string>() { seg1, seg2, seg3 };
                            var categoria = await _categoriaService.FirstOrDefaultAsync(host.SiteId, slugsDeCategoria);
                            if (categoria != null)
                            {
                                return new Conteudo()
                                {
                                    Categoria = categoria,
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComUltimosPosts,
                                    Posts = _postService.GetAllUltimosPublicados(host.SiteId, categoria.Id, numeroDapagina, itemPorPagina),
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                            return null;
                        }
                        return null;
                    }

                    //site.com/algo/algo/algo/algo - Pode ser categoria(Subcategoria), posts por ano por subcategoria, posts por mês por subcategoria ou post
                    //Acima de 3 slugs sempre acaba em categoria, por isso esse é o ultimo if que serve para todos acima de 3. 
                    if (segmentos.Length > 3)
                    {
                        //Cria lista com os segmentos invertidos. Pois na maioria só trabalhamos com os 4 últimos.
                        //Exs: 
                        //  post/mes/ano/categoria
                        //  subcategoria/subcategoria/subcategoria/categoria
                        //  mes/ano/subcategoria/categoria
                        //  ano/subcategoria/subcategoria/categoria
                        var segsReverse = segmentos.ToList();
                        segsReverse.Reverse();

                        var segsReverseInt = segmentosInt.ToList();
                        segsReverseInt.Reverse();

                        bool ePost = segsReverseInt[0] == default(int)
                                     && segsReverseInt[1] > 0 && segsReverseInt[1] <= 12
                                     && segsReverseInt[2] >= anoMinimo && segsReverseInt[2] <= anoMaximo
                                     && segsReverseInt[3] == default(int);
                        if (ePost)
                        {
                            var no = await GetNoBySlugAsync(host.SiteId, segsReverse[0], buscaCategoria: false, buscaPagina: false);
                            if (no != null)
                            {
                                return new Conteudo()
                                {
                                    Post = no.Post,
                                    TipoDeConteudo = TipoDeConteudoEnum.Post
                                };
                            }
                            return null;
                        }

                        bool eSubcategoria = segsReverseInt.All(x => x == default(int));
                        if (eSubcategoria)
                        {
                            var categoria = await _categoriaService.FirstOrDefaultAsync(host.SiteId, segmentos.ToList());
                            if (categoria != null)
                            {
                                return new Conteudo()
                                {
                                    Categoria = categoria,
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComUltimosPosts,
                                    Posts = _postService.GetAllUltimosPublicados(host.SiteId, categoria.Id, numeroDapagina, itemPorPagina),
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                            return null;
                        }

                        bool eSubCategoriaPorMes = segsReverseInt[0] > 0 && segsReverseInt[0] <= 12
                                                   && segsReverseInt[1] >= anoMinimo && segsReverseInt[1] <= anoMaximo
                                                   && segsReverseInt[2] == default(int)
                                                   && segsReverseInt[3] == default(int);
                        if (eSubCategoriaPorMes)
                        {
                            //Remove o ano e mes para pesquisar a categoria 
                            var slugs = segmentos.ToList();
                            slugs.RemoveAt(slugs.Count - 1);
                            slugs.RemoveAt(slugs.Count - 1);

                            var categoria = await _categoriaService.FirstOrDefaultAsync(host.SiteId, slugs);
                            if (categoria != null)
                            {
                                return new Conteudo()
                                {
                                    AnoDosPosts = segsReverseInt[1],
                                    MesDosPosts = segsReverseInt[0],
                                    Categoria = categoria,
                                    Posts = _postService.GetAll(host.SiteId, segsReverseInt[1], segsReverseInt[0], categoria.Id, numeroDapagina, itemPorPagina),
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComPostsPorMes,
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                        }

                        bool eSubCategoriaPorAno = segsReverseInt[0] >= anoMinimo && segsReverseInt[0] <= anoMaximo
                                                   && segsReverseInt[1] == default(int)
                                                   && segsReverseInt[2] == default(int)
                                                   && segsReverseInt[3] == default(int);
                        if (eSubCategoriaPorAno)
                        {
                            //Remove o ano para pesquisar a categoria 
                            var slugs = segmentos.ToList();
                            slugs.RemoveAt(slugs.Count - 1);

                            var categoria = await _categoriaService.FirstOrDefaultAsync(host.SiteId, slugs);
                            if (categoria != null)
                            {
                                return new Conteudo()
                                {
                                    AnoDosPosts = segsReverseInt[0],
                                    Categoria = categoria,
                                    Posts = _postService.GetAll(host.SiteId, segsReverseInt[0], categoria.Id, numeroDapagina, itemPorPagina),
                                    TipoDeConteudo = TipoDeConteudoEnum.CategoriaComPostsPorAno,
                                    NumeroDaPaginaPosts = numeroDapagina,
                                    ItemPorPaginaPosts = itemPorPagina
                                };
                            }
                        }

                        return null;
                    }

                    return null;
                }
                return null;
            }
        }

        /// <summary>
        /// Corrige e regulariza os segmentos
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public string[] GetSegmentos(Uri uri)
        {
            //Existe uma diferença entre o Teste e o MVC.
            //    Teste: o primeiro segmento no uri.Segments é "/"
            //    MVC: o primeiro e segundo segmento no uri.Segments é "/"
            return uri.Segments.Where(x => x != "/").Select(x => x.Replace("/", "")).ToArray();
        }
    }
}
