﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Services.CMS
{
    public class MenuService : EZControlDomainServiceBase<Menu>, IMenuService
    {
        public MenuService(IRepository<Menu, int> repository)
            : base(repository)
        {

        }
        public List<ItemDeMenu> GetItensDeMenu(Menu menu)
        {
            return menu.ItensDeMenu.ToList();
        }
    }
}