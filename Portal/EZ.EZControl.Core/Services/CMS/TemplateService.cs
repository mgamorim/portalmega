﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS
{
    public class TemplateService : EZControlDomainServiceBase<Template>, ITemplateService
    {
        private readonly IAppFolders appFolders;
        public TemplateService(IRepository<Template, int> repository, IAppFolders _appFolders)
            : base(repository)
        {
            this.appFolders = _appFolders;
        }

        public IEnumerable<string> GetPosicoesByTemplate(Template template)
        {
            return template.Posicoes.Split(',').Select(p => p.Trim()).ToList();
        }

        public IEnumerable<string> GetTiposDePaginasSuportadasByTemplate(Template template)
        {
            return template.TiposDePaginasSuportadas.Split(',').Select(p => p.Trim()).Distinct().ToList();
        }

        public IEnumerable<string> GetTiposDeWidgetSuportadosByTemplate(Template template)
        {
            var tiposDeWidget = template.TiposDeWidgetSuportados.Split(',').Select(p => p.Trim()).ToList();
            var tiposDeWidgetByEnum = new List<string>();

            foreach(string tiposWidget in tiposDeWidget )
            {
                if (IsValidEnumByName(tiposWidget))
                    tiposDeWidgetByEnum.Add(tiposWidget);
            }


            return tiposDeWidgetByEnum;
        }

        public IEnumerable<Template> GetTemplates()
        {
            List<Template> templates = new List<Template>();

            if (appFolders.TemplateFolder != null &&
                Directory.Exists(appFolders.TemplateFolder))
            {
                var paths = Directory.GetFiles(appFolders.TemplateFolder);

                if (!paths.Any())
                    throw new UserFriendlyException(L("Template.FileNotFound"));

                foreach(string path in paths)
                {
                    string arquivo = File.ReadAllText(path);
                    Template template = JsonConvert.DeserializeObject<Template>(arquivo);
                    template.NomeDoArquivo = Path.GetFileNameWithoutExtension(path);
                    templates.Add(template);
                }


                var errors = ValidateEntity(templates);

                if (errors.Any())
                    throw new AbpValidationException(L("ValidationError"), errors);
            }
            else
                throw new UserFriendlyException(L("Template.FolderNotFound"));

            return templates;
        }

        public async Task Sincronizar(List<Template> templates)
        {
            //Templates que devem ser atualizados, caso não existam na pasta e existam no banco.
            var templatesASeremAtualizados = base.GetAll()
                .ToList();

            templatesASeremAtualizados = templatesASeremAtualizados
                .Where(t => !(templates.Select(x => x.Nome).Contains(t.Nome)))
                .ToList();

            if (templatesASeremAtualizados.Any())
            {
                foreach( Template templateDoBanco in templatesASeremAtualizados)
                {
                    templateDoBanco.IsActive = false;
                    await CreateOrUpdateEntity(templateDoBanco);
                }
            };

            foreach(Template template in templates)
            {
                var templatesCriados = GetAll()
                                        .Where(x =>
                                            x.Nome.ToLower() == template.Nome.ToLower() &&
                                            x.Versao.ToLower() == template.Versao.ToLower());

                var templateCriado = templatesCriados.FirstOrDefault();

                if (templateCriado == null)
                    await CreateOrUpdateEntity(template);
                else
                {
                    if (templateCriado.IsActive == false)
                    {
                        templateCriado.IsActive = true;
                        await CreateOrUpdateEntity(templateCriado);
                    }
                }

            };
        }

        private new List<ValidationResult> ValidateEntity(List<Template> templates)
        {
            var validationErrors = new List<ValidationResult>();

            foreach(Template t in templates)
            {
                if (string.IsNullOrEmpty(t.Nome))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Template.RequiredNomeErrorForTemplate"), t.NomeDoArquivo), new[] { "nome".ToCamelCase() }));
                }

                if (string.IsNullOrEmpty(t.Versao))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Template.RequiredVersaoErrorForTemplate"), t.NomeDoArquivo), new[] { "versao".ToCamelCase() }));
                }

                if (string.IsNullOrEmpty(t.TiposDePaginasSuportadas))
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Template.RequiredTipoDePaginaErrorForTemplate"),
                                L("Template.TiposDePaginasSuportadas"), t.NomeDoArquivo),
                            new[] { "tiposDePaginasSuportadas".ToCamelCase() }));
                }
                else
                {
                    if (!t.TiposDePaginasSuportadas.IsValidCharacters())
                    {
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Template.InvalidCharatersTiposDePaginas"), t.NomeDoArquivo),
                                new[] { "tiposDePaginasSuportadas".ToCamelCase() }));
                    }
                }

                if (string.IsNullOrEmpty(t.TipoDePaginaParaPaginaDefault))
                {
                    validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Template.RequiredTipoDePaginaErrorForTemplate"),
                                    L("Template.TipoDePaginaParaPaginaDefault"), t.NomeDoArquivo),
                                new[] { "tipoDePaginaParaPaginaDefault".ToCamelCase() }));
                }
                else
                {
                    if (!IsValidTipoDePagina(t.TiposDePaginasSuportadas, t.TipoDePaginaParaPaginaDefault))
                    {
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Template.InvalidTipoDePagina"),
                                    L("Template.TipoDePaginaParaPaginaDefault"), t.NomeDoArquivo),
                                new[] { "tipoDePaginaParaPaginaDefault".ToCamelCase() }));
                    }
                }

                if (string.IsNullOrEmpty(t.TipoDePaginaParaCategoriaDefault))
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Template.RequiredTipoDePaginaErrorForTemplate"),
                                L("Template.TipoDePaginaParaCategoriaDefault"), t.NomeDoArquivo),
                            new[] { "tipoDePaginaParaCategoriaDefault".ToCamelCase() }));
                }
                else
                {
                    if (!IsValidTipoDePagina(t.TiposDePaginasSuportadas, t.TipoDePaginaParaCategoriaDefault))
                    {
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Template.InvalidTipoDePagina"),
                                    L("Template.TipoDePaginaParaCategoriaDefault"), t.NomeDoArquivo),
                                new[] { "tipoDePaginaParaCategoriaDefault".ToCamelCase() }));
                    }
                }

                if (string.IsNullOrEmpty(t.TipoDePaginaParaPaginaInicialDefault))
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Template.RequiredTipoDePaginaErrorForTemplate"),
                                L("Template.TipoDePaginaParaPaginaInicialDefault"), t.NomeDoArquivo),
                            new[] { "tipoDePaginaParaPaginaInicialDefault".ToCamelCase() }));
                }
                else
                {
                    if (!IsValidTipoDePagina(t.TiposDePaginasSuportadas, t.TipoDePaginaParaPaginaInicialDefault))
                    {
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Template.InvalidTipoDePagina"),
                                    L("Template.TipoDePaginaParaPaginaInicialDefault"), t.NomeDoArquivo),
                                new[] { "tipoDePaginaParaPaginaInicialDefault".ToCamelCase() }));
                    }
                }

                if (string.IsNullOrEmpty(t.TipoDePaginaParaPostDefault))
                {
                    validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Template.RequiredTipoDePaginaErrorForTemplate"),
                                    L("Template.TipoDePaginaParaPostDefault"), t.NomeDoArquivo),
                                new[] { "tipoDePaginaParaPostDefault".ToCamelCase() }));
                }
                else
                {
                    if (!IsValidTipoDePagina(t.TiposDePaginasSuportadas, t.TipoDePaginaParaPostDefault))
                    {
                        validationErrors.Add(
                           new ValidationResult(
                               string.Format(L("Template.InvalidTipoDePagina"),
                                    L("Template.TipoDePaginaParaPostDefault"), t.NomeDoArquivo),
                               new[] { "tipoDePaginaParaPostDefault".ToCamelCase() }));
                    }
                }

                if (!t.Posicoes.IsValidCharacters())
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Template.InvalidCharatersPosicoes"), t.NomeDoArquivo), new[] { "posicoes".ToCamelCase() }));
                }

                if (!t.TiposDeWidgetSuportados.IsValidCharacters())
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Template.InvalidCharatersTiposDeWidget"), t.NomeDoArquivo), new[] { "tiposDeWidgetSuportados".ToCamelCase() }));
                }
            }
            return validationErrors;
        }

        private bool IsValidEnumByName(string name)
        {
            try
            {
                Enum.Parse(typeof(TipoDeWidgetEnum), name);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool IsValidTipoDePagina(string tiposDePaginasSuportadas, string tipoDePagina)
        {
            return tiposDePaginasSuportadas.Split(',').Select(p => p.Trim()).Distinct().ToList().Contains(tipoDePagina);
        }

    }
}
