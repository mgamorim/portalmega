﻿using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Services.CMS
{
    public class WidgetBaseService<TEntity> : EZControlDomainServiceBase<TEntity>, IWidgetBaseService<TEntity>
                                                where TEntity : WidgetBase, IEntity<int>
    {
        public WidgetBaseService(IRepository<TEntity, int> repository)
            : base(repository)
        {

        }

        protected string CreateWidgetHtml(WidgetBase widget, string conteudo)
        {
            var exibirTitulo = widget.ExibirTitulo ? $"<div class=\"ezwidget-titulo\"><h3>{widget.Titulo}</h3></div>" : string.Empty;

            var html = "<div class=\"ezwidget ezwidget-id-{0} ezwidget-tipo-{1}\">" + exibirTitulo + "<div class=\"ezwidget-conteudo\">{2}</div></div>";

            return string.Format(html, widget.Id, widget.Tipo, conteudo);
        }
    }
}
