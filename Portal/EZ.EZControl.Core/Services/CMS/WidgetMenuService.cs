﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Services.CMS
{
    public class WidgetMenuService : WidgetBaseService<WidgetMenu>, IWidgetMenuService
    {
        private readonly CMSFrontEnd.Interfaces.IMenuService _menuService;
        public WidgetMenuService(IRepository<WidgetMenu, int> repository, CMSFrontEnd.Interfaces.IMenuService menuService)
            : base(repository)
        {
            this._menuService = menuService;
        }

        public string CreateWidgetHtml(WidgetMenu widget)
        {
            var conteudo = _menuService.GetHtml(widget.Menu);
            return base.CreateWidgetHtml(widget, conteudo);
        }
    }
}
