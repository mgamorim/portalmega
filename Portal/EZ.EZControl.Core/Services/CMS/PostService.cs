﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class PostService : EZControlDomainServiceBase<Post>, IPostService
    {
        public PostService(IRepository<Post, int> repository)
            : base(repository)
        {
        }

        [UnitOfWork]
        public List<Post> GetAll(int siteId, int ano, int? categoriaId = null, int numeroDapagina = 1, int itemPorpagina = 10)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter, AbpDataFilters.MustHaveTenant))
            {
                if (ano <= 0)
                    throw new UserFriendlyException("Ano inválido.");

                var dataInicial = new DateTime(ano, 1, 1, 0, 0, 0);
                var dataFinal = new DateTime(ano, 12, 31, 23, 59, 59);

                var postQuery = Repository.GetAll();

                if (categoriaId != null && categoriaId > 0)
                {
                    postQuery = postQuery.Where(x => x.Categorias.Any(c => c.SiteId == siteId && c.Id == categoriaId));
                }

                var posts = postQuery
                    .Where(
                        x =>
                            x.SiteId == siteId && (x.DataDePublicacao >= dataInicial || x.DataDePublicacao <= dataFinal))
                    .OrderByDescending(x => x.DataDePublicacao)
                    .Skip((numeroDapagina - 1) * itemPorpagina).Take(itemPorpagina)
                    .ToList();

                return posts;
            }
        }

        [UnitOfWork]
        public List<Post> GetAll(int siteId, int ano, int mes, int? categoriaId = null, int numeroDapagina = 1, int itemPorpagina = 10)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter, AbpDataFilters.MustHaveTenant))
            {
                if (ano <= 0 || mes <= 0 || mes > 12)
                    throw new UserFriendlyException("Ano ou mês inválido.");

                var primeiroDiaDoMes = new DateTime(ano, mes, 1, 0, 0, 0);
                var ultimoDiaDoMes = primeiroDiaDoMes.AddMonths(1).AddDays(-1);

                var posts =
                    Repository.GetAll()
                        .Where(
                            x =>
                                x.SiteId == siteId && (x.DataDePublicacao >= primeiroDiaDoMes ||
                                                       x.DataDePublicacao <= ultimoDiaDoMes))
                        .OrderByDescending(x => x.DataDePublicacao)
                        .Skip((numeroDapagina - 1) * itemPorpagina).Take(itemPorpagina)
                        .ToList();

                return posts;
            }
        }

        [UnitOfWork]
        public List<Post> GetAllUltimosPublicados(int siteId, int? categoriaId = null, int numeroDapagina = 1, int itemPorpagina = 10)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter, AbpDataFilters.MustHaveTenant))
            {
                var postQuery = Repository.GetAll();

                if (categoriaId != null && categoriaId > 0)
                {
                    postQuery = postQuery.Where(x => x.Categorias.Any(c => c.SiteId == siteId && c.Id == categoriaId));
                }

                var posts = postQuery
                    .Where(x => x.SiteId == siteId)
                    .OrderByDescending(x => x.DataDePublicacao)
                    .Skip((numeroDapagina - 1) * itemPorpagina).Take(itemPorpagina)
                    .ToList();

                return posts;
            }
        }
    }
}