﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using EZ.EZControl.Services.Core.Geral;

namespace EZ.EZControl.Services.CMS
{
    public class ArquivoCMSService : ArquivoBaseService<ArquivoCMS>, IArquivoCMSService
    {
        public ArquivoCMSService(IRepository<ArquivoCMS, int> repository)
            : base(repository)
        {
        }
    }
}
