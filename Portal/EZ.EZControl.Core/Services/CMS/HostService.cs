﻿using System.Threading.Tasks;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class HostService : EZControlDomainServiceBase<Host>, IHostService
    {
        public HostService(IRepository<Host, int> repository)
            : base(repository)
        {
        }

        public async Task<Host> FisrtOrDefaultAsync(string host)
        {
            return await Repository.FirstOrDefaultAsync(x => x.Url.ToLower().Equals(host.ToLower()));
        }
    }
}