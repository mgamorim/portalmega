﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class SiteService : EZControlDomainServiceBase<Site>, ISiteService
    {
        public SiteService(IRepository<Site, int> repository)
            : base(repository)
        {
        }
    }
}