﻿using System;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System.Linq;

namespace EZ.EZControl.Services.CMS
{
    public class LinkService : EZControlDomainServiceBase<Site>, ILinkService
    {
        private readonly ICategoriaService _categoriaService;
        public LinkService(IRepository<Site, int> repository, ICategoriaService categoriaService)
            : base(repository)
        {
            _categoriaService = categoriaService;
        }

        public string GetLink(TipoDeLinkEnum tipoDeLink, Site site, string linkStr, Categoria categoria, string slug)
        {
            string retorno = string.Empty;
            switch (tipoDeLink)
            {
                case TipoDeLinkEnum.Pagina:
                    {
                        retorno = string.Format(linkStr, site.Hosts.FirstOrDefault(x => x.IsPrincipal).Url, slug);
                        break;
                    }
                case TipoDeLinkEnum.Post:
                    {
                        //o parâmetro categoria deve vir preenchido com a primeira categoria do POST para que possa formar o link corretamente.
                        var slugCategoria = categoria.Nome.Slugify();
                        var urlPredicate = string.Format("{0}/{1}", slugCategoria, slug);
                        retorno = string.Format(linkStr, site.Hosts.FirstOrDefault(x => x.IsPrincipal).Url, urlPredicate);
                        break;
                    }
                case TipoDeLinkEnum.Categoria:
                    {
                        var urlPredicate = slug;
                        if (categoria.CategoriaPai != null)
                        {
                            urlPredicate = _categoriaService.FormataEstruturaCategorias(categoria.CategoriaPai, "{0}/" + urlPredicate);
                        }
                        retorno = string.Format(linkStr, site.Hosts.FirstOrDefault(x => x.IsPrincipal).Url, urlPredicate);
                        break;
                    }
            }

            return retorno;
        }
    }
}
