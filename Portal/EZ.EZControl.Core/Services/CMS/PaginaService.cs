﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    public class PaginaService : EZControlDomainServiceBase<Pagina>, IPaginaService
    {
        public PaginaService(IRepository<Pagina, int> repository)
            : base(repository)
        {
        }
    }
}