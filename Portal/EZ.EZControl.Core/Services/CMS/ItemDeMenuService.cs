﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Services.CMS
{
    public class ItemDeMenuService : EZControlDomainServiceBase<ItemDeMenu>, IItemDeMenuService
    {
        public ItemDeMenuService(IRepository<ItemDeMenu, int> repository)
            : base(repository)
        {

        }
    }
}
