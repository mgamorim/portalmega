﻿using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IWidgetHTMLService : IWidgetBaseService<WidgetHTML>
    {
        string CreateWidgetHtml(WidgetHTML widget);
    }
}
