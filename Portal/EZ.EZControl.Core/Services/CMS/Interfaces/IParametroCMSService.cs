﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IParametroCMSService : IParametroBaseService<ParametroCMS>
    {
    }
}