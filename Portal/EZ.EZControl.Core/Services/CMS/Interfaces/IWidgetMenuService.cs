﻿using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IWidgetMenuService : IWidgetBaseService<WidgetMenu>
    {
        string CreateWidgetHtml(WidgetMenu widget);
    }
}
