﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IPostService : IService<Post>
    {
        List<Post> GetAll(int siteId, int ano, int? categoriaId = null, int numeroDapagina = 1, int itemPorpagina = 10);
        List<Post> GetAll(int siteId, int ano, int mes, int? categoriaId = null, int numeroDapagina = 1, int itemPorpagina = 10);
        List<Post> GetAllUltimosPublicados(int siteId, int? categoriaId = null, int numeroDapagina = 1, int itemPorpagina = 10);
    }
}