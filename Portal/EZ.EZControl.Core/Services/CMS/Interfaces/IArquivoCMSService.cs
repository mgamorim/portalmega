﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IArquivoCMSService : IArquivoBaseService<ArquivoCMS>
    {
    }
}
