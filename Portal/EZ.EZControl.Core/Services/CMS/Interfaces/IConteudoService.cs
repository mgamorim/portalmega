﻿using System;
using System.Threading.Tasks;
using Abp.Domain.Services;
using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IConteudoService : IDomainService
    {
        Task<No> GetNoBySlugAsync(
            int siteId,
            string slug,
            bool buscaPagina = true,
            bool buscaPost = true,
            bool buscaCategoria = true);
        Task<Conteudo> FirstOrDefaultAsync(string url, int numeroDapagina = 1, int itemPorPagina = 10);

        string[] GetSegmentos(Uri uri);
    }
}
