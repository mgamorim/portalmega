﻿using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface ISiteService : IService<Site>
    {
    }
}