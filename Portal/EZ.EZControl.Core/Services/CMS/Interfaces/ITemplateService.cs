﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface ITemplateService : IService<Template>
    {
        IEnumerable<string> GetPosicoesByTemplate(Template template);
        IEnumerable<string> GetTiposDeWidgetSuportadosByTemplate(Template template);
        IEnumerable<string> GetTiposDePaginasSuportadasByTemplate(Template template);
        IEnumerable<Template> GetTemplates();
        Task Sincronizar(List<Template> templates);
    }
}
