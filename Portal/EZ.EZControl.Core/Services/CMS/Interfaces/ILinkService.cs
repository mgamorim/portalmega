﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface ILinkService : IService<Site>
    {
        string GetLink(TipoDeLinkEnum tipoDeLink, Site site, string link, Categoria categoria, string slug);
    }
}
