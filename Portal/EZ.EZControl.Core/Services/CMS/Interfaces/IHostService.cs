﻿using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IHostService : IService<Host>
    {
        Task<Host> FisrtOrDefaultAsync(string host);
    }
}