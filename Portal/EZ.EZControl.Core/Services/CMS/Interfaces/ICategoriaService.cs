﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface ICategoriaService : IService<Categoria>
    {
        string FormataEstruturaCategorias(Categoria categoria, string urlPredicate);
        Task<Categoria> FirstOrDefaultAsync(int siteId, List<string> slugs);
    }
}