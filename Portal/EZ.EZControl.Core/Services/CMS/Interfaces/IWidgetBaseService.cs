﻿using Abp.Domain.Entities;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IWidgetBaseService<TEntity> : IService<TEntity>
                                                    where TEntity : class, IEntity<int>
    {
    }
}
