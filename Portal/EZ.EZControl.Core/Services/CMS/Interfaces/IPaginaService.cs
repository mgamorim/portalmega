﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IPaginaService : IService<Pagina>
    {
    }
}