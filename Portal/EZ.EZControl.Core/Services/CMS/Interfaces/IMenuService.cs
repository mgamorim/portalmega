﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IMenuService : IService<Menu>
    {
        List<ItemDeMenu> GetItensDeMenu(Menu menu);
    }
}
