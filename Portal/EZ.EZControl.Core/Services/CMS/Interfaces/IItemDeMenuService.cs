﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IItemDeMenuService : IService<ItemDeMenu>
    {
    }
}
