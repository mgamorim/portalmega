﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CRM.Geral;
using EZ.EZControl.Services.CRM.Interfaces;

namespace EZ.EZControl.Services.CRM
{
    public class LeadService : EZControlDomainServiceBase<Lead>, ILeadRepository
    {
        public LeadService(IRepository<Lead, int> repository)
            : base(repository)
        {
        }
    }
}
