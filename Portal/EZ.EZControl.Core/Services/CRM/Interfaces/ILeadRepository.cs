﻿using EZ.EZControl.Domain.CRM.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.CRM.Interfaces
{
    public interface ILeadRepository : IService<Lead>
    {
    }
}
