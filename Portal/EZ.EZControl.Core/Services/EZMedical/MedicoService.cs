﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Services.EZMedical.Interfaces;

namespace EZ.EZControl.Services.EZMedical
{
    public class MedicoService : EZControlDomainServiceBase<Medico>, IMedicoService
    {
        public MedicoService(IRepository<Medico, int> repository) : base(repository)
        {
        }
    }
}
