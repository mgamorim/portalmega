﻿using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZMedical.Interfaces
{
    public interface IMedicoService : IService<Medico>
    {
    }
}
