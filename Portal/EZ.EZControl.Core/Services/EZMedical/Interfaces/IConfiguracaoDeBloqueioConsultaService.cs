﻿using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZMedical.Interfaces
{
    public interface IConfiguracaoDeBloqueioConsultaService : IService<ConfiguracaoDeBloqueioConsulta>
    {

    }
}