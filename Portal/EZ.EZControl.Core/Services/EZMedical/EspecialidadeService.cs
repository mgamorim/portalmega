﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Services.EZMedical.Interfaces;

namespace EZ.EZControl.Services.EZMedical
{
    public class EspecialidadeService : EZControlDomainServiceBase<Especialidade>, IEspecialidadeService
    {
        public EspecialidadeService(IRepository<Especialidade, int> repository)
            : base(repository)
        {
        }
    }
}