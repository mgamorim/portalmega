﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Services.EZMedical.Interfaces;

namespace EZ.EZControl.Services.EZMedical
{
    public class ConfiguracaoDeBloqueioConsultaService : EZControlDomainServiceBase<ConfiguracaoDeBloqueioConsulta>, IConfiguracaoDeBloqueioConsultaService
    {
        public ConfiguracaoDeBloqueioConsultaService(IRepository<ConfiguracaoDeBloqueioConsulta, int> repository)
            : base(repository)
        {
        }
    }
}