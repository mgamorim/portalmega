﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Services.EZMedical.Interfaces;

namespace EZ.EZControl.Services.EZMedical
{
    public class ConfiguracaoDeDisponibilidadeConsultaService : EZControlDomainServiceBase<ConfiguracaoDeDisponibilidadeConsulta>, IConfiguracaoDeDisponibilidadeConsultaService
    {
        public ConfiguracaoDeDisponibilidadeConsultaService(IRepository<ConfiguracaoDeDisponibilidadeConsulta, int> repository)
            : base(repository)
        {
        }
    }
}