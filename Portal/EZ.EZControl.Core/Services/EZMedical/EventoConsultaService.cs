﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Services.EZMedical.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoConsultaService : EZControlDomainServiceBase<EventoConsulta>, IEventoConsultaService
    {
        public EventoConsultaService(IRepository<EventoConsulta, int> repository)
            : base(repository)
        {
        }
    }
}