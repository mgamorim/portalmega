﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraService : EZControlDomainServiceBase<PermissaoDeVendaDePlanoDeSaudeParaCorretora>, IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretoraService(IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretora, int> repository)
            : base(repository)
        {
            
        }
    }
}
