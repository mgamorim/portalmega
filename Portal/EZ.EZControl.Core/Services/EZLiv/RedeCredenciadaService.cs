﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class RedeCredenciadaService : EZControlDomainServiceBase<RedeCredenciada>, IRedeCredenciadaService
    {
        IPlanoDeSaudeService _planoDeSaudeService;
        IItemDeRedeCredenciadaService _itemDeRedeCredenciadaService;

        public RedeCredenciadaService(IRepository<RedeCredenciada, int> repository,
            IPlanoDeSaudeService planoDeSaudeService,
            IItemDeRedeCredenciadaService itemDeRedeCredenciadaService)
            : base(repository)
        {
            _planoDeSaudeService = planoDeSaudeService;
            _itemDeRedeCredenciadaService = itemDeRedeCredenciadaService;
        }

        public void CanDelete(int id)
        {
            //var planoDeSaude = _planoDeSaudeService.GetAll().FirstOrDefault(x => x.RedeCredenciada == id);

            //if(planoDeSaude != null)
            //    throw new UserFriendlyException(L("RedeCredenciada.ExistePlanoDeSaudeAssociado"));
        }

        public async Task<RedeCredenciada> GetByIdWithItems(int id)
        {
            var rede = Repository.Get(id);
            var redeCredenciada = Repository.Get(id);

            //if (redeCredenciada != null)
            //{
            //    redeCredenciada.Items = await _itemDeRedeCredenciadaService.GetAllListAsync(x => x.RedeCredenciada.Id == id);
            //}
            return redeCredenciada;
        }

        public void DeleteWithItems(int id)
        {
            _itemDeRedeCredenciadaService.Delete(x => x.RedeCredenciada.Id == id);
            Repository.Delete(id);
        }
    }
}
