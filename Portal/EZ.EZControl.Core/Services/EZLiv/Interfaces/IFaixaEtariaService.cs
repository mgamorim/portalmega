﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IFaixaEtariaService : IService<FaixaEtaria>
    {
    }
}
