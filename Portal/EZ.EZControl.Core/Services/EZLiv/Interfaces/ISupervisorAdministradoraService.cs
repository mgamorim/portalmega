﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ISupervisorAdministradoraService : IService<SupervisorAdministradora>
    {
        void AddSupervisorInRole(Corretor corretor, Empresa empresa);
    }
}
