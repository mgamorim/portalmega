﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ICorretorDadosBancarioService : IService<CorretorDadosBancario>
    {

        Task<CorretorDadosBancario> GetCorretorDadosBancarioByCorretorId(int corretorId);
        Task<CorretorDadosBancario> GetCorretorDadosBancarioByPessoaId(int pessoaId);
        Task<CorretorDadosBancario> SaveCorretorDadosBancario(CorretorDadosBancario corretorDadosBancario);
        //Task<CorretorDadosBancario> GetDefault();
        //Task<CorretorDadosBancario> GetDefaultById(int id);
    }
}
