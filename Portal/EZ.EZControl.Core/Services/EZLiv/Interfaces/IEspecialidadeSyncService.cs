﻿using EZ.EZControl.Domain.EZLiv.Sync;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IEspecialidadeSyncService : IService<EspecialidadeSync>
    {
        Task<List<EspecialidadeSync>> GetEspecialidadeSyncByEstado(string estado);

        Task<List<EspecialidadeSync>> GetEspecialidadeSyncByEstadoDistinct(string estado);

        Task<List<EspecialidadeSync>> GetEspecialidadeSyncByMunicipio(string estado, string municipio);

        Task<List<EspecialidadeSync>> GetEspecialidadeSyncByMunicipioDistinct(string estado, string municipio);

        Task<List<EspecialidadeSync>> GetEspecialidadeSyncByNomeDistinct(string nome);

        Task Sincronizar();
    }
}
