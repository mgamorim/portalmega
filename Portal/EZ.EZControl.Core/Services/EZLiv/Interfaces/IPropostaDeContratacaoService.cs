﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IPropostaDeContratacaoService : IService<PropostaDeContratacao>
    {
        void SendEmailPreCadastro(PropostaDeContratacao proposta);
        void SendEmailEdicaoDeProposta(PropostaDeContratacao proposta);
        void SendEmailConcluirAceite(PropostaDeContratacao proposta);
        void SendEmailConcluirAceiteOnlyCorretor(PropostaDeContratacao proposta);
        void SendEmailConcluirAceiteOnlyBeneficiario(PropostaDeContratacao proposta);
        Task SendNotificationAceite(PropostaDeContratacao proposta);
        Task SendEmailHomologar(PropostaDeContratacao proposta);
        Task SendNotificationNovaHomologacao(PropostaDeContratacao proposta);
        Task SendNotificationDeclaracaoSaude(PropostaDeContratacao proposta);
        void SendEmailHomologacaoConcluida(PropostaDeContratacao proposta);
        void SendEmailPagamentoConcluidoBeneficiario(PropostaDeContratacao proposta);
        void SendEmailPagamentoConcluidoAdmCorretora(PropostaDeContratacao proposta);
        Task SendNotificationPagamentoConcluido(PropostaDeContratacao proposta);
        Task SendNotificationHomologacaoConcluida(PropostaDeContratacao proposta);
        void SendEmailDocumentoEmExigencia(PropostaDeContratacao proposta, List<ArquivoDocumento> documentosEmExigencia);
        Task SendEmailAceiteDoContrato(PropostaDeContratacao proposta);
        Task SendEmailHomologacaoDoContrato(PropostaDeContratacao proposta);
        void SendEmailBoletoGerado(PropostaDeContratacao proposta);
        void SendEmailFormularios(PropostaDeContratacao proposta, bool enviaCopiaParaAdministradora = false, bool enviaCopiaParaCorretora = false);
        Task PodeSalvarPassoPreCadastro(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoPreCadastro(PropostaDeContratacao proposta, bool sendEmail = true);
        Task<PropostaDeContratacao> SavePassoPreCadastroAnonymous(PropostaDeContratacao proposta, bool sendEmail = true);
        Task PodeSalvarPassoSelecionarPlano(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoSelecionarPlano(PropostaDeContratacao proposta);
        Task PodeSalvarPassoDadosGerais(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoDadosGerais(PropostaDeContratacao proposta);
        Task PodeSalvarPassoEnvioDeDocumentos(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoEnvioDeDocumentos(PropostaDeContratacao proposta);
        Task PodeSalvarPassoAceiteDoContrato(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoAceiteDoContrato(PropostaDeContratacao proposta);
        Task PodeSalvarPassoHomologacao(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoHomologacao(PropostaDeContratacao proposta);
        Task PodeSalvarPassoPagamento(PropostaDeContratacao beneficiario);
        Task<PropostaDeContratacao> SavePassoPagamento(PropostaDeContratacao proposta);
        Task SaveAtualizarPagamento(object objPagseguro);
        Task SendEmailComprovanteDepositoCorretor(PropostaDeContratacao proposta);
    }
}
