﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IDependenteService : IService<Dependente>
    {
        User CreateUserDependente(Dependente dependente, Empresa empresa, TipoDeUsuarioEnum tipoDeUsuario, string staticRoleName);
    }
}
