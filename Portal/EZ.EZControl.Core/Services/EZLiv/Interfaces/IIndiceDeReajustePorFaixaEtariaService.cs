﻿using System.Threading.Tasks;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IIndiceDeReajustePorFaixaEtariaService : IService<IndiceDeReajustePorFaixaEtaria>
    {
        Task<string> GetHTMLFormulario(int operadoraId);
    }
}
