﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IArquivoDocumentoService : IService<ArquivoDocumento>
    {
        Task<List<ArquivoDocumento>> GetDocumentosEmExigenciaByPropostaId(int PropostaId);
    }
}
