﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IContratoHistoricoService<TContratoHistorico, TPrimaryKey> : IService<TContratoHistorico, TPrimaryKey>
        where TContratoHistorico : Domain.EZLiv.Geral.ContratoHistorico, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignContrato(TContratoHistorico contratoHistorico, Contrato contrato);
        void CanAssignProdutoDePlanoDeSaude(TContratoHistorico contrato, ProdutoDePlanoDeSaude produto);
    }

    public interface IContratoHistoricoService<TContratoHistorico> : IContratoHistoricoService<TContratoHistorico, int>
    where TContratoHistorico : Domain.EZLiv.Geral.ContratoHistorico
    {
    }

    public interface IContratoHistoricoService : IContratoHistoricoService<Domain.EZLiv.Geral.ContratoHistorico>
    {
    }
}
