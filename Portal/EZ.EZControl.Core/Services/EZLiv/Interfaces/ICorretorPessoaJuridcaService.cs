﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ICorretorPessoaJuridcaService : IService<CorretorPessoaJuridca>
    {
        Task<CorretorPessoaJuridca> GetCorretorPessoaJuridicaByCorretorId(int corretorId);
        Task<CorretorPessoaJuridca> GetCorretorPessoaJuridicaByPessoaId(int pessoaId);
        Task<CorretorPessoaJuridca> SaveCorretorPessoaJuridca(CorretorPessoaJuridca corretorPessoaJuridca);
        //Task<CorretorDadosBancario> GetDefault();
        //Task<CorretorDadosBancario> GetDefaultById(int id);
    }
}
