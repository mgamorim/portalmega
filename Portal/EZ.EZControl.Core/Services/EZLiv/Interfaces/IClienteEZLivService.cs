﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IClienteEZLivService<TClienteEZLiv, TPrimaryKey> : IService<TClienteEZLiv, TPrimaryKey>
        where TClienteEZLiv : Domain.EZLiv.SubtiposPessoa.ClienteEZLiv, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignCliente(TClienteEZLiv clienteEZLiv, Cliente cliente);
    }

    public interface IClienteEZLivService<TClienteEZLiv> : IClienteEZLivService<TClienteEZLiv, int>
    where TClienteEZLiv : Domain.EZLiv.SubtiposPessoa.ClienteEZLiv
    {
    }

    public interface IClienteEZLivService : IClienteEZLivService<Domain.EZLiv.SubtiposPessoa.ClienteEZLiv>
    {
    }
}
