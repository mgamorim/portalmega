﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IContratoService<TContrato, TPrimaryKey> : IService<TContrato, TPrimaryKey>
        where TContrato : Domain.EZLiv.Geral.Contrato, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignProdutoDePlanoDeSaude(TContrato contrato, ProdutoDePlanoDeSaude produto);
    }

    public interface IContratoService<TContrato> : IContratoService<TContrato, int>
    where TContrato : Domain.EZLiv.Geral.Contrato
    {
    }

    public interface IContratoService : IContratoService<Domain.EZLiv.Geral.Contrato>
    {
        Contrato GetUltimoContratoByProdutoDePlanoDeSaudeId(int ProdutoDePlanoDeSaudeId);
    }
}
