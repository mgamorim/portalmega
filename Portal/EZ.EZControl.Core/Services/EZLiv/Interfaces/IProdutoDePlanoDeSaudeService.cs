﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IProdutoDePlanoDeSaudeService : IService<ProdutoDePlanoDeSaude>
    {
        void CanAssignRedeCredenciada(ProdutoDePlanoDeSaude produto, RedeCredenciada redeCredenciada);
        Task<List<ProdutoDePlanoDeSaude>> GetAllListAtivoAsync();
    }
}
