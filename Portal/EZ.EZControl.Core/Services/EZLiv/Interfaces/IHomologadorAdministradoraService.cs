﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IHomologadorAdministradoraService : IService<HomologadorAdministradora>
    {
        void AddHomologadorInRole(Corretor corretor, Empresa empresa);
    }
}
