﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IItemDeDeclaracaoDeSaudeService : IService<ItemDeDeclaracaoDeSaude>
    {
    }
}
