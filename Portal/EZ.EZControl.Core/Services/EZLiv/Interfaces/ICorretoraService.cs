﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ICorretoraService : IService<Corretora>
    {
        void CanDelete(int id);
        Task<Corretora> GetCorretoraBySlug(string slug);
        Task<CorretoraNotificationInfo> GetCorretoraForNotification(string slug);
        CorretoraNotificationInfo GetCorretoraNotificationInfo(Corretora corretora);
        Corretora GetCorretoraByName(string name);
        List<string> GetCorretorasByName(string name);
        Task<Corretora> GetByEmpresaLogada();

    }
}
