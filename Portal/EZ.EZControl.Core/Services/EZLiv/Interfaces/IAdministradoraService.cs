﻿using System.Threading.Tasks;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IAdministradoraService : IService<Administradora>
    {
        Task<Administradora> GetByEmpresaLogada();
    }
}
