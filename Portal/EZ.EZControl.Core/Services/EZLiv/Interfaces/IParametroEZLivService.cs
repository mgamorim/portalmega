﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IParametroEZLivService : IParametroBaseService<ParametroEZLiv>
    {
    }
}
