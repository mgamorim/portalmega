﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IChancelaService : IService<Chancela>
    {
        Task<IQueryable<Chancela>> GetAllByUsuarioLogado();
    }
}
