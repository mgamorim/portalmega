﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IBeneficiarioBaseService : IService<BeneficiarioBase>
    {
        void IsValidPreCadastro(BeneficiarioBase beneficiario);
        void SendEmail(User user, string slug , Corretora corretora);
        void SendEmailSimulador(User user, CorretoraNotificationInfo corretora, string senha );
        User CreateUserBeneficiario(BeneficiarioBase beneficiario, Empresa empresa, TipoDeUsuarioEnum tipoDeUsuario, string staticRoleName);
    }
}
