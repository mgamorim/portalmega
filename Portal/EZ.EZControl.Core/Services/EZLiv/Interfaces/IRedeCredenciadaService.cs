﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IRedeCredenciadaService : IService<RedeCredenciada>
    {
        void CanDelete(int id);
        Task<RedeCredenciada> GetByIdWithItems(int id);
        void DeleteWithItems(int id);
    }
}
