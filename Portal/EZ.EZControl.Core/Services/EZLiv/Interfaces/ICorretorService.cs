﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ICorretorService : IService<Corretor>
    {
        void SendEmailUsuarioCriado(User user, string nomeCorretor, CorretoraNotificationInfo corretora, string Slug);
        void SendEmailNovoCorretorParaCorretora(string emailCorretora, string nomeCorretor, int corretorId, CorretoraNotificationInfo corretoraInfo);
        Task<Corretor> GetCorretorByPessoaId(int? id);
    }
}
