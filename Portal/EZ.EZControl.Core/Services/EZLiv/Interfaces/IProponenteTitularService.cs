﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IProponenteTitularService : IService<ProponenteTitular>
    {
        ProponenteTitular CreateAndReturnBeneficiario(ProponenteTitular titular);
    }
}
