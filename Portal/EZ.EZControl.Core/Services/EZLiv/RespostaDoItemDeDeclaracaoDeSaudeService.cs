﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class RespostaDoItemDeDeclaracaoDeSaudeService : EZControlDomainServiceBase<RespostaDoItemDeDeclaracaoDeSaude>, IRespostaDoItemDeDeclaracaoDeSaudeService
    {
        public RespostaDoItemDeDeclaracaoDeSaudeService(IRepository<RespostaDoItemDeDeclaracaoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
