﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class FaixaEtariaService : EZControlDomainServiceBase<FaixaEtaria>, IFaixaEtariaService
    {
        public FaixaEtariaService(IRepository<FaixaEtaria, int> repository)
            : base(repository)
        {
        }
    }
}
