﻿using System;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Linq;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Authorization.Roles;
using System.Collections.Generic;
using Abp.Authorization.Users;
using Abp.Domain.Uow;
using Microsoft.AspNet.Identity;

namespace EZ.EZControl.Services.EZLiv
{
    public class GerenteCorretoraService : EZControlDomainServiceBase<GerenteCorretora>, IGerenteCorretoraService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IUnitOfWorkManager _uowManager;
        public GerenteCorretoraService(IRepository<GerenteCorretora, int> repository, UserManager userManager, RoleManager roleManager, IUnitOfWorkManager uowManager)
            : base(repository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _uowManager = uowManager;
        }

        public void AddGerenteInRole(Corretor corretor, Empresa empresa)
        {
            try
            {
                var usuario = _userManager.Users.FirstOrDefault(x => x.PessoaId == corretor.PessoaId);

                if (usuario == null)
                    throw new UserFriendlyException(L("User.NotFoundException"));

                var role = _roleManager.Roles.FirstOrDefault(r => r.TenantId == usuario.TenantId && r.Name == "GerenteCorretora");

                if (!usuario.Empresas.Any(x => x.Id == empresa.Id))
                    usuario.Empresas.Add(empresa);

                _userManager.AddToRole(usuario.Id, "GerenteCorretora");

                _userManager.Update(usuario);
            }
            catch(Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
