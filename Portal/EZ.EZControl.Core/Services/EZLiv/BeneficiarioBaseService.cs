﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.UI;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Web;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Services.EZLiv
{
    public class BeneficiarioBaseService : EZControlDomainServiceBase<BeneficiarioBase>, IBeneficiarioBaseService
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IWebUrlService _webUrlService;
        private readonly IEmpresaService _empresaService;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IEmailSender _emailNotificationService;

        public BeneficiarioBaseService(IRepository<BeneficiarioBase, int> repository,
                                       ITipoDeDocumentoService tipoDeDocumentoService,
                                       RoleManager roleManager,
                                       UserManager userManager,
                                       IUnitOfWorkManager unitOfWorkManager,
                                       IWebUrlService webUrlService,
                                       IEmailSender emailNotificationService,
                                       IEmpresaService empresaService,
                                       IRepository<Tenant> tenantRepository)
            : base(repository)
        {
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _userManager = userManager;
            _unitOfWorkManager = unitOfWorkManager;
            _webUrlService = webUrlService;
            _tenantRepository = tenantRepository;
            _emailNotificationService = emailNotificationService;
            _empresaService = empresaService;
            _roleManager = roleManager;
        }

        public void IsValidPreCadastro(BeneficiarioBase beneficiario)
        {
            if (beneficiario == null)
                throw new UserFriendlyException(L("Beneficiario.EmptyBeneficiarioError"));

            if (beneficiario.PessoaFisica == null)
                throw new UserFriendlyException(L("Beneficiario.EmptyPessoaFisicaError"));

            if (string.IsNullOrEmpty(beneficiario.PessoaFisica.Nome))
                throw new UserFriendlyException(L("PessoaFisica.EmptyNomeError"));

            if (beneficiario.PessoaFisica.Documentos == null)
                throw new UserFriendlyException(L("Beneficiario.EmptyDocumentoError"));

            if (!beneficiario.PessoaFisica.Documentos.Any())
                throw new UserFriendlyException(L("Beneficiario.EmptyDocumentoError"));

            var cpf =
                beneficiario.PessoaFisica.Documentos.FirstOrDefault(
                    x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);

            if (cpf == null)
                throw new UserFriendlyException(L("Beneficiario.EmptyCpfError"));

            if (string.IsNullOrEmpty(cpf.Numero))
                throw new UserFriendlyException(L("Beneficiario.EmptyCpfError"));

            if (!_tipoDeDocumentoService.IsCpf(cpf.Numero))
                throw new UserFriendlyException(L("TipoDeDocumento.MessagemDeErroValidacaoCpf"));


            if (beneficiario.PessoaFisica.EmailPrincipal == null)
                throw new UserFriendlyException(L("EnderecoEletronico.RequiredEnderecoError"));

            if (beneficiario.PessoaFisica.EmailPrincipal.Endereco.IsValidEmail() == false)
                throw new UserFriendlyException(L("EnderecoEletronico.InvalidEnderecoError"));
        }

        public User CreateUserBeneficiario(BeneficiarioBase beneficiario, Empresa empresa, TipoDeUsuarioEnum tipoDeUsuario, string staticRoleName)
        {
            try
            {
                IsValidPreCadastro(beneficiario);

                var email =
                    beneficiario.PessoaFisica.EnderecosEletronicos.FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                var usuario = _userManager.Users.FirstOrDefault(x => x.EmailAddress.ToLower() == email.Endereco.ToLower());

                if (usuario != null)
                    return usuario;

                var user = User.CreateUserPorPessoa(beneficiario.PessoaFisica, tipoDeUsuario, string.Empty);

                if (!user.TenantId.HasValue || user.TenantId.Value == 0)
                    user.TenantId = empresa.TenantId;

                var role = _roleManager.Roles.FirstOrDefault(r => r.TenantId == user.TenantId && r.Name == staticRoleName);
                if (role == null)
                    _roleManager.Create(new Role(user.TenantId, staticRoleName, staticRoleName) { IsStatic = true });
                else
                {
                    user.Roles = new List<UserRole>() { };
                    user.Roles.Add(new UserRole(user.TenantId, user.Id, role.Id));
                }

                user.Empresas = new List<Empresa> { empresa };

                _userManager.Create(user);
                _unitOfWorkManager.Current.SaveChanges();

                if (role == null)
                    _userManager.AddToRole(user.Id, staticRoleName);

                return user;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("Beneficiario.UserNotCreated"), e.Message);
            }
        }



        public void SendEmail(User user, string slug , Corretora corretora)
        {
            try
            {
                if (!string.IsNullOrEmpty(user.PasswordResetCode))
                {
                    var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "Account/ResetPassword" +
                                "?userId=" + Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.Id.ToString())) +
                                "&tenantId=" + (user.TenantId == null ? "" : Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.TenantId.Value.ToString()))) +
                                "&resetCode=" + Uri.EscapeDataString(user.PasswordResetCode) +
                                "&slug=" + slug +
                                "&returnUrl=" + _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "anchor/tenant/ezliv/propostaDeContratacao";

                    var corpo = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoUsuarioCriadoPeloBeneficiario(user.UserName, user.Password, link, corretora);

                    var template = new TemplateEmail
                    {
                        CorpoMensagem = corpo,
                        Remetente = "contato@portalmega.net",
                        Assunto = L("Beneficiario.Create")
                    };

                    _emailNotificationService.Send(user.EmailAddress, L("Beneficiario.Create"), corpo);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("Beneficiario.NotificacaoPorEmailError"), e);
            }
        }




        public void SendEmailSimulador(User user, CorretoraNotificationInfo corretorainnfo, string senha)
        {
            try
            {
                    var corpo = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoUsuarioSimulador(user.FullName, user.UserName , senha, corretorainnfo);

                    var template = new TemplateEmail
                    {
                        CorpoMensagem = corpo,
                        Remetente = "contato@portalmega.net",
                        Assunto = L("Beneficiario.Create")
                    };

                    _emailNotificationService.Send(user.UserName, L("Beneficiario.Create"), corpo);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("Beneficiario.NotificacaoPorEmailError"), e);
            }
        }

    }
}
