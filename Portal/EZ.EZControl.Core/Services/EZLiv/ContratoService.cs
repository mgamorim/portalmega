﻿using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class ContratoService<TContrato> : EZControlDomainServiceBase<TContrato>, IContratoService<TContrato>
    where TContrato : Domain.EZLiv.Geral.Contrato
    {
        public ContratoService(IRepository<TContrato, int> repository)
            : base(repository)
        {
            ValidateEntity += ContratoService_ValidateEntity;
            BeforeSaveEntity += ContratoService_BeforeSaveEntity;
        }

        private void ContratoService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TContrato, int> e)
        {
        }

        private void ContratoService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TContrato, int> e)
        {
            var validationErrors = new List<ValidationResult>();

            // Validar os períodos de vigência
            if (e.Entity.DataCriacao == default(DateTime))
                validationErrors.Add(new ValidationResult(L("Contrato.DataCriacaoInvalida")));
            //if (e.Entity.DataInicioVigencia == default(DateTime) || e.Entity.DataInicioVigencia < e.Entity.DataCriacao)
            //    validationErrors.Add(new ValidationResult(L("Contrato.DataInicioVigenciaInvalida")));
            if (e.Entity.DataFimVigencia == default(DateTime) || e.Entity.DataFimVigencia <= e.Entity.DataInicioVigencia)
                validationErrors.Add(new ValidationResult(L("Contrato.DataFimVigenciaInvalida")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public void CanAssignProdutoDePlanoDeSaude(TContrato contrato, ProdutoDePlanoDeSaude produto)
        {
            if (produto == null)
                throw new UserFriendlyException(L("Contrato.ProdutoDePlanoDeSaudeInvalido"));

            if (!produto.IsActive)
                throw new UserFriendlyException(L("Contrato.ProdutoDePlanoDeSaudeInativo"));
        }
    }

    public class ContratoService : ContratoService<Contrato>, IContratoService
    {
        public ContratoService(IRepository<Contrato, int> repository)
            : base(repository)
        {
        }

        public Contrato GetUltimoContratoByProdutoDePlanoDeSaudeId(int ProdutoDePlanoDeSaudeId)
        {
            var contrato = new Domain.EZLiv.Geral.Contrato();
            contrato = GetAll()
                .Where(x => x.ProdutoDePlanoDeSaudeId == ProdutoDePlanoDeSaudeId &&
                           (DateTime.Now >= x.DataInicioVigencia && DateTime.Now <= x.DataFimVigencia)
                )
                .FirstOrDefault();
            return contrato;
        }
    }
}
