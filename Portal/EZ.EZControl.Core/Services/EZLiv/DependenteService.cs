﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Services.EZLiv
{
    public class DependenteService : EZControlDomainServiceBase<Dependente>, IDependenteService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public DependenteService(IRepository<Dependente, int> repository,
                                 IUnitOfWorkManager unitOfWorkManager,
                                 RoleManager roleManager,
                                 UserManager userManager)
            : base(repository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public User CreateUserDependente(Dependente dependente, Empresa empresa, TipoDeUsuarioEnum tipoDeUsuario, string staticRoleName)
        {
            try
            {
                var email =
                    dependente.PessoaFisica.EnderecosEletronicos.FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                var usuario = _userManager.Users.FirstOrDefault(x => x.EmailAddress.ToLower() == email.Endereco.ToLower());

                if (usuario != null)
                {
                    var nomeCompleto = dependente.PessoaFisica.Nome.Trim().Split(' ');
                    usuario.Name = nomeCompleto.FirstOrDefault();
                    usuario.Surname = nomeCompleto.LastOrDefault();
                    usuario.Pessoa = dependente.PessoaFisica;

                    _userManager.Update(usuario);
                    _unitOfWorkManager.Current.SaveChanges();

                    return usuario;
                }

                var user = User.CreateUserPorPessoa(dependente.PessoaFisica, tipoDeUsuario, string.Empty);

                if (!user.TenantId.HasValue || user.TenantId.Value == 0)
                    user.TenantId = empresa.TenantId;

                var role = _roleManager.Roles.FirstOrDefault(r => r.TenantId == user.TenantId && r.Name == staticRoleName);
                if (role == null)
                    _roleManager.Create(new Role(user.TenantId, staticRoleName, staticRoleName) { IsStatic = true });
                else
                {
                    user.Roles = new List<UserRole>() { };
                    user.Roles.Add(new UserRole(user.TenantId, user.Id, role.Id));
                }

                user.Empresas = new List<Empresa> { empresa };

                _userManager.Create(user);
                _unitOfWorkManager.Current.SaveChanges();

                if (role == null)
                    _userManager.AddToRole(user.Id, staticRoleName);

                return user;
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("Beneficiario.UserNotCreated"), e.Message);
            }
        }
    }
}
