﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ValorPorFaixaEtariaService : EZControlDomainServiceBase<ValorPorFaixaEtaria>, IValorPorFaixaEtariaService
    {
        public ValorPorFaixaEtariaService(IRepository<ValorPorFaixaEtaria, int> repository)
            : base(repository)
        {
        }
    }
}
