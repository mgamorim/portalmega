﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretorService : EZControlDomainServiceBase<PermissaoDeVendaDePlanoDeSaudeParaCorretor>, IPermissaoDeVendaDePlanoDeSaudeParaCorretorService
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretorService(IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretor, int> repository)
            : base(repository)
        {

        }
    }
}
