﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class EspecialidadePorProdutoDePlanoDeSaudeService : EZControlDomainServiceBase<EspecialidadePorProdutoDePlanoDeSaude>, IEspecialidadePorProdutoDePlanoDeSaudeService
    {
        public EspecialidadePorProdutoDePlanoDeSaudeService(IRepository<EspecialidadePorProdutoDePlanoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
