﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ProponenteTitularService : EZControlDomainServiceBase<ProponenteTitular>, IProponenteTitularService
    {
        public ProponenteTitularService(IRepository<ProponenteTitular, int> repository)
            : base(repository)
        {
        }

        public ProponenteTitular CreateAndReturnBeneficiario(ProponenteTitular titular)
        {
            return base.Repository.Insert(titular);
        }
    }
}
