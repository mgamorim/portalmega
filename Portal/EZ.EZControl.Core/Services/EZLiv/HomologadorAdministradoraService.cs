﻿using System;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using Abp.Domain.Uow;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using System.Linq;
using EZ.EZControl.Domain.Global.Enums;
using Microsoft.AspNet.Identity;

namespace EZ.EZControl.Services.EZLiv
{
    public class HomologadorAdministradoraService : EZControlDomainServiceBase<HomologadorAdministradora>, IHomologadorAdministradoraService
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IUnitOfWorkManager _uowManager;
        public HomologadorAdministradoraService(IRepository<HomologadorAdministradora, int> repository, UserManager userManager, RoleManager roleManager, IUnitOfWorkManager uowManager)
            : base(repository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _uowManager = uowManager;
        }

        public void AddHomologadorInRole(Corretor corretor, Empresa empresa)
        {
            try
            {
                var email =
                    corretor.Pessoa.EnderecosEletronicos.FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                var usuario = _userManager.Users.FirstOrDefault(x => x.EmailAddress.ToLower() == email.Endereco.ToLower());

                if (usuario == null)
                    throw new UserFriendlyException(L("User.NotFoundException"));

                var role = _roleManager.Roles.FirstOrDefault(r => r.TenantId == usuario.TenantId && r.Name == "HomologadorAdministradora");

                if (!usuario.Empresas.Any(x => x.Id == empresa.Id))
                    usuario.Empresas.Add(empresa);

                _userManager.AddToRole(usuario.Id, "HomologadorAdministradora");

                _userManager.Update(usuario);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
