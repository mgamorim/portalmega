﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ItemDeDeclaracaoDoBeneficiarioService : EZControlDomainServiceBase<ItemDeDeclaracaoDoBeneficiario>, IItemDeDeclaracaoDoBeneficiarioService
    {
        public ItemDeDeclaracaoDoBeneficiarioService(IRepository<ItemDeDeclaracaoDoBeneficiario, int> repository)
            : base(repository)
        {
        }
    }
}
