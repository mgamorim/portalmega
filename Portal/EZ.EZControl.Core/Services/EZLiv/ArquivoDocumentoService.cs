﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class ArquivoDocumentoService : EZControlDomainServiceBase<ArquivoDocumento>, IArquivoDocumentoService
    {
        public ArquivoDocumentoService(IRepository<ArquivoDocumento, int> repository)
            : base(repository)
        {
        }

        public async Task<List<ArquivoDocumento>> GetDocumentosEmExigenciaByPropostaId(int PropostaId)
        {
            var documentos = await GetAllListAsync(x => x.PropostaDeContratacaoId == PropostaId && x.EmExigencia);
            return documentos;
        }
    }
}
