﻿using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.UI;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Web;
using System;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class CorretorService : EZControlDomainServiceBase<Corretor>, ICorretorService
    {
        private readonly IWebUrlService _webUrlService;
        private readonly IEmailSender _emailNotificationService;

        public CorretorService(IRepository<Corretor, int> repository,
            IWebUrlService webUrlService,
            IEmailSender emailNotificationService)
            : base(repository)
        {
            _webUrlService = webUrlService;
            _emailNotificationService = emailNotificationService;
        }

        public void CanAssignCorretor(Corretor corretor, Domain.Global.Pessoa.Pessoa pessoa)
        {
            if (corretor == null)
                throw new UserFriendlyException(L("Corretor.EmptyError"));

            if (pessoa == null)
                throw new UserFriendlyException(L("Corretor.PessoaEmpty"));

            if (pessoa.PessoaFisica.Cpf == null || string.IsNullOrEmpty(pessoa.PessoaFisica.Cpf))
                throw new UserFriendlyException(L("Corretor.EmptyCPFError"));
        }

        public void SendEmailUsuarioCriado(User user, string nomeCorretor, CorretoraNotificationInfo corretoraInfo, string Slug)
        {
            try
            {
                if (!string.IsNullOrEmpty(user.PasswordResetCode))
                {
                    //var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "Account/ResetPassword" +
                    var link = "http://" + _webUrlService.GetSiteRootAddress("Default") + "Account/ResetPassword" +
                                "?userId=" + Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.Id.ToString())) +
                                "&tenantId=" + (user.TenantId == null ? "" : Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.TenantId.Value.ToString()))) +
                                "&resetCode=" + Uri.EscapeDataString(user.PasswordResetCode) +
                                "&slug=" + Slug +
                                "&returnUrl=" + _webUrlService.GetSiteRootAddress("Default") + "anchor/tenant/dashboard";

                    var corpo = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoUsuarioCorretor(nomeCorretor, user.UserName, link, corretoraInfo);

                    _emailNotificationService.Send(user.EmailAddress, "Criação do Corretor", corpo);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("Beneficiario.NotificacaoPorEmailError"), e.Message);
            }
        }

        public void SendEmailNovoCorretorParaCorretora(string emailCorretora, string nomeCorretor, int corretorId, CorretoraNotificationInfo corretoraInfo)
        {
            try
            {
                var corpo = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoNovoCorretorParaCorretora(nomeCorretor, corretorId, corretoraInfo);

                _emailNotificationService.Send(emailCorretora, "Novo corretor cadastrado - Portal MEGA", corpo);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Não foi possível enviar o e-mail para a corretora", e.Message);
            }
        }

        public async Task<Corretor> GetCorretorByPessoaId(int? PessoaId)
        {
            var corretor = await Repository.FirstOrDefaultAsync(p => p.PessoaId == PessoaId);
            return corretor;
        }
    }
}
