﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class OperadoraService : EZControlDomainServiceBase<Operadora>, IOperadoraService
    {
        public OperadoraService(IRepository<Operadora, int> repository)
            : base(repository)
        {
        }

        //public override async Task Delete(int id)
        //{
        //    var operadora = Repository
        //        .GetAllIncluding(x => x.ProdutosDePlanoDeSaude)
        //        .Where(x => x.Id == id)
        //        .FirstOrDefault();

        //    while (operadora.ProdutosDePlanoDeSaude.Any())
        //    {
        //        var produtoPlanoDeSaude = operadora.ProdutosDePlanoDeSaude.ToList()[0];
        //        if (produtoPlanoDeSaude.Operadoras.Contains(operadora))
        //        {
        //            produtoPlanoDeSaude.Operadoras.Remove(operadora);
        //        }
        //        operadora.ProdutosDePlanoDeSaude.Remove(produtoPlanoDeSaude);
        //    }

        //    await base.Delete(id);
        //}
    }
}