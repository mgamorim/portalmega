﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ItemDeDeclaracaoDeSaudeService : EZControlDomainServiceBase<ItemDeDeclaracaoDeSaude>, IItemDeDeclaracaoDeSaudeService
    {
        public ItemDeDeclaracaoDeSaudeService(IRepository<ItemDeDeclaracaoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
