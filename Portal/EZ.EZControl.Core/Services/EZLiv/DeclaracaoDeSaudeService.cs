﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class DeclaracaoDeSaudeService : EZControlDomainServiceBase<DeclaracaoDeSaude>, IDeclaracaoDeSaudeService
    {
        public DeclaracaoDeSaudeService(IRepository<DeclaracaoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
