﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class CorretorPessoaJuridcaService : EZControlDomainServiceBase<CorretorPessoaJuridca>, ICorretorPessoaJuridcaService
    {
        public CorretorPessoaJuridcaService(IRepository<CorretorPessoaJuridca, int> repository)
           : base(repository)
        {
        }

        public async Task<CorretorPessoaJuridca> GetCorretorPessoaJuridicaByCorretorId(int corretorId)
        {
            var corretorPessoaJuridica = await Repository.FirstOrDefaultAsync(p => p.CorretorId == corretorId && p.IsActive == true);
            return corretorPessoaJuridica;
        }

        public async Task<CorretorPessoaJuridca> GetCorretorPessoaJuridicaByPessoaId(int pessoaId)
        {
            var corretorPessoaJuridica = await Repository.FirstOrDefaultAsync(p => p.PessoaId == pessoaId && p.IsActive == true);
            return corretorPessoaJuridica;
        }

        public async Task<CorretorPessoaJuridca> SaveCorretorPessoaJuridca(CorretorPessoaJuridca corretorPessoaJuridca)
        {
            SetEmpresaInEntity(corretorPessoaJuridca);
            return await CreateOrUpdateAndReturnSavedEntity(corretorPessoaJuridca);
        }
    }
}
