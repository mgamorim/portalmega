﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class QuestionarioDeDeclaracaoDeSaudeService : EZControlDomainServiceBase<QuestionarioDeDeclaracaoDeSaude>, IQuestionarioDeDeclaracaoDeSaudeService
    {
        public QuestionarioDeDeclaracaoDeSaudeService(IRepository<QuestionarioDeDeclaracaoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
