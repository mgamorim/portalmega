﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class VigenciaService : EZControlDomainServiceBase<Vigencia>, IVigenciaService
    {
        public VigenciaService(IRepository<Vigencia, int> repository)
            : base(repository)
        {
        }
    }
}
