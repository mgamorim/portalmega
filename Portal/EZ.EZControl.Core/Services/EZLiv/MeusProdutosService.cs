﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class MeusProdutosService : EZControlDomainServiceBase<ProdutoDePlanoDeSaude>, IMeusProdutosService
    {
        public MeusProdutosService(IRepository<ProdutoDePlanoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
