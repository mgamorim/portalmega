﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class PlanoDeSaudeService : EZControlDomainServiceBase<PlanoDeSaude>, IPlanoDeSaudeService
    {
        public PlanoDeSaudeService(IRepository<PlanoDeSaude, int> repository)
            : base(repository)
        {
        }
    }
}
