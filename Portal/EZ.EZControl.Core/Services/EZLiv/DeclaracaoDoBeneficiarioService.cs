﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class DeclaracaoDoBeneficiarioService : EZControlDomainServiceBase<DeclaracaoDoBeneficiario>, IDeclaracaoDoBeneficiarioService
    {
        public DeclaracaoDoBeneficiarioService(IRepository<DeclaracaoDoBeneficiario, int> repository)
            : base(repository)
        {
        }
    }
}
