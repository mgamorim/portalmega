﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ClienteEZLivService<TClienteEZLiv> : EZControlDomainServiceBase<TClienteEZLiv>, IClienteEZLivService<TClienteEZLiv>
        where TClienteEZLiv : Domain.EZLiv.SubtiposPessoa.ClienteEZLiv
    {
        public ClienteEZLivService(IRepository<TClienteEZLiv, int> repository)
            : base(repository)
        {
            ValidateEntity += ClienteEZLivService_ValidateEntity;
            BeforeSaveEntity += ClienteEZLivService_BeforeSaveEntity;
        }

        private void ClienteEZLivService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TClienteEZLiv, int> e)
        {
        }

        private void ClienteEZLivService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TClienteEZLiv, int> e)
        {
        }

        public void CanAssignCliente(TClienteEZLiv saida, Cliente cliente)
        {
            if (cliente == null)
                throw new UserFriendlyException(L("ClienteEZLiv.ClienteEmpty"));

            if (cliente.IsActive == false)
                throw new UserFriendlyException(L("ClienteEZLiv.ClienteInativo"));
        }
    }

    public class ClienteEZLivService : ClienteEZLivService<ClienteEZLiv>, IClienteEZLivService
    {
        public ClienteEZLivService(IRepository<ClienteEZLiv, int> repository)
            : base(repository)
        {
        }
    }
}
