﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class CorretoraService : EZControlDomainServiceBase<Corretora>, ICorretoraService
    {
        private readonly IBeneficiarioBaseService _beneficiarioBaseService;
        private readonly IEmpresaService _empresaService;
        private readonly IRepository<Empresa, int> _empresaRepository;
        private readonly IArquivoGlobalService _arquivoGlobalService;

        public CorretoraService(
            IRepository<Corretora, int> repository,
            IBeneficiarioBaseService beneficiarioBaseService,
            IEmpresaService empresaService,
            IRepository<Empresa, int> empresaRepository,
            IArquivoGlobalService arquivoGlobalService)
            : base(repository)
        {
            _beneficiarioBaseService = beneficiarioBaseService;
            _empresaService = empresaService;
            _empresaRepository = empresaRepository;
            _arquivoGlobalService = arquivoGlobalService;
        }

        public void CanDelete(int id)
        {
        }

        public async Task<Corretora> GetCorretoraBySlug(string slug)
        {
            using (CurrentUnitOfWork.DisableFilter("EmpresaFilter"))
            {
                var empresa = await _empresaService.FirstOrDefault(x => x.Slug.ToLower() == slug.ToLower());
                if (empresa != null)
                    return base.Repository.FirstOrDefault(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId.Value);

                return null;
            }
        }

        public async Task<CorretoraNotificationInfo> GetCorretoraForNotification(string slug)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var empresa = await _empresaService.FirstOrDefault(x => x.Slug.ToLower() == slug.ToLower());
                if (empresa != null)
                {
                    var corretora = await Repository.FirstOrDefaultAsync(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId.Value);
                    return GetCorretoraNotificationInfo(corretora);
                }

                return null;
            }
        }

        public CorretoraNotificationInfo GetCorretoraNotificationInfo(Corretora corretora)
        {
            if (corretora != null)
            {
                var logo = string.Empty;
                if (corretora.Imagem != null)
                {
                    logo = corretora.Imagem.Path;
                }

                return new CorretoraNotificationInfo
                {
                    NomeFantasia = corretora.PessoaJuridica.NomeFantasia,
                    RazaoSocial = corretora.PessoaJuridica.RazaoSocial,
                    Email = corretora.PessoaJuridica.EmailPrincipal != null
                        ? corretora.PessoaJuridica.EmailPrincipal.Endereco
                        : string.Empty,
                    Telefone = corretora.PessoaJuridica.TelefoneComercial != null
                        ? corretora.PessoaJuridica.TelefoneComercial.Tostring()
                        : string.Empty,
                    Logo = logo
                };
            }

            return CorretoraNotificationInfo.CriarCorretoraDefault();

        }

        public Corretora GetCorretoraByName(string name)
        {
            using (CurrentUnitOfWork.DisableFilter("EmpresaFilter"))
            {
                return base.Repository.FirstOrDefault(x => x.Nome.ToLower() == name.ToLower());
            }
        }

        public List<string> GetCorretorasByName(string name)
        {
            using (CurrentUnitOfWork.DisableFilter("EmpresaFilter"))
            {
                return base.Repository.GetAll().Where(x => x.Nome.Contains(name)).Select(c => c.Nome).ToList();
            }
        }

        public async Task<Corretora> GetByEmpresaLogada()
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

            if (empresaId != null)
            {
                var queryEmpresa = _empresaRepository.GetAll();
                var queryCta = Repository.GetAll();

                var query = from empresa in queryEmpresa
                            join corretora in queryCta on empresa.PessoaJuridicaId equals corretora.PessoaJuridicaId
                            where empresa.Id == empresaId
                            select corretora;

                var cta = await Task.Run(() => query.FirstOrDefault());

                if (cta != null)
                {
                    return cta;
                }
            }

            return null;
        }
    }
}
