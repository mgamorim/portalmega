﻿using System.Threading.Tasks;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class CorretorDadosBancarioService : EZControlDomainServiceBase<CorretorDadosBancario>, ICorretorDadosBancarioService
    {
        public CorretorDadosBancarioService(IRepository<CorretorDadosBancario, int> repository)
           : base(repository)
        {
        }

        public async Task<CorretorDadosBancario> GetCorretorDadosBancarioByCorretorId(int corretorId)
        {
            var corretorDadosBancario = await Repository.FirstOrDefaultAsync(p => p.CorretorId == corretorId);
            return corretorDadosBancario;
        }

        public async Task<CorretorDadosBancario> GetCorretorDadosBancarioByPessoaId(int pessoaId)
        {
            var corretorDadosBancario = await Repository.FirstOrDefaultAsync(p => p.PessoaId == pessoaId && p.IsActive == true);
            return corretorDadosBancario;
        }

        public async Task<CorretorDadosBancario> SaveCorretorDadosBancario(CorretorDadosBancario corretorDadosBancario)
        {
            SetEmpresaInEntity(corretorDadosBancario);
            return await CreateOrUpdateAndReturnSavedEntity(corretorDadosBancario);
        }
    }
}
