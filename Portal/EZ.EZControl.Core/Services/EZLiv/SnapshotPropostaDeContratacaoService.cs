﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class SnapshotPropostaDeContratacaoService : EZControlDomainServiceBase<SnapshotPropostaDeContratacao>, ISnapshotPropostaDeContratacaoService
    {
        public SnapshotPropostaDeContratacaoService(IRepository<SnapshotPropostaDeContratacao, int> repository)
            : base(repository)
        {
        }
    }
}
