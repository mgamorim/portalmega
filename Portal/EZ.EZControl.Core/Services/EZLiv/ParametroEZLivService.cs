﻿
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ParametroEZLivService : EZControlDomainServiceBase<ParametroEZLiv>, IParametroEZLivService
    {
        public ParametroEZLivService(IRepository<ParametroEZLiv, int> repository)
            : base(repository)
        {
        }

    }
}