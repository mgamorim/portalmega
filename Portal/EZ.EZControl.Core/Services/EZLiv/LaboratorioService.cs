﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class LaboratorioService : EZControlDomainServiceBase<Laboratorio>, ILaboratorioService
    {
        public LaboratorioService(IRepository<Laboratorio, int> repository)
            : base(repository)
        {

        }
    }
}
