﻿using System;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class IndiceDeReajustePorFaixaEtariaService : EZControlDomainServiceBase<IndiceDeReajustePorFaixaEtaria>, IIndiceDeReajustePorFaixaEtariaService
    {
        public IndiceDeReajustePorFaixaEtariaService(IRepository<IndiceDeReajustePorFaixaEtaria, int> repository)
            : base(repository)
        {
        }

        public async Task<string> GetHTMLFormulario(int operadoraId)
        {
            var html = new StringBuilder();
            var indices = await Repository.GetAllListAsync(x => x.OperadoraId == operadoraId);

            html.AppendLine("<table class='tabela-indices'>");
            //HEADER
            html.AppendLine("<thead>");
            html.AppendLine("<tr>");
            html.AppendLine("<th>Faixa Etária</th>");
            html.AppendLine("<th>Índice de Reajuste</th>");
            html.AppendLine("</tr>");
            html.AppendLine("</thead>");
            //CORPO
            html.AppendLine("<tbody>");
            foreach (var indice in indices)
            {
                html.AppendLine("<tr>");
                html.AppendLine(string.Format("<td>{0}</td>", FormataFaixaEtaria(indice)));
                html.AppendLine(String.Format("<td>{0}</td>", indice.IndiceDeReajuste.ToString() + "%"));
                html.AppendLine("</tr>");
            }
            html.AppendLine("</tbody>");
            //FINALIZA TABELA
            html.AppendLine("</table>");

            return html.ToString();
        }

        private static string FormataFaixaEtaria(IndiceDeReajustePorFaixaEtaria indice)
        {
            if (indice.FaixaEtaria.IdadeInicial == 0)
                return ("até " + indice.FaixaEtaria.IdadeFinal.ToString() + " anos");

            if (!indice.FaixaEtaria.IdadeFinal.HasValue)
                return (indice.FaixaEtaria.IdadeInicial.ToString() + " anos ou mais");

            return ("de " + indice.FaixaEtaria.IdadeInicial.ToString() + " a " + indice.FaixaEtaria.IdadeFinal.Value.ToString() + " anos");
        }
    }
}
