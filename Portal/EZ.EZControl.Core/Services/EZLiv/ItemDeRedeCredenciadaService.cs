﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class ItemDeRedeCredenciadaService : EZControlDomainServiceBase<ItemDeRedeCredenciada>, IItemDeRedeCredenciadaService
    {
        public ItemDeRedeCredenciadaService(IRepository<ItemDeRedeCredenciada, int> repository)
            : base(repository)
        {
        }

        public void canAssignRedeCredenciada(RedeCredenciada redeCredenciada)
        {
            if (redeCredenciada == null)
                throw new UserFriendlyException(L("ItemDeRedeCredenciada.RedeCredenciadaEmpty"));
            if (!redeCredenciada.IsActive)
                throw new UserFriendlyException(L("ItemDeRedeCredenciada.RedeCredenciadaInativa"));
        }

        public void canAssignUnidadeDeSaude(UnidadeDeSaudeBase unidadeDeSaude)
        {
            if (unidadeDeSaude == null)
                throw new UserFriendlyException(L("ItemDeRedeCredenciada.UnidadeDeSaudeEmpty"));
            if (!unidadeDeSaude.IsActive)
                throw new UserFriendlyException(L("ItemDeRedeCredenciada.UnidadeDeSaudeInativa"));
        }
    }
}
