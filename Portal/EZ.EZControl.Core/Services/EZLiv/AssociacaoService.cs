﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class AssociacaoService : EZControlDomainServiceBase<Associacao>, IAssociacaoService
    {
        public AssociacaoService(IRepository<Associacao, int> repository)
            : base(repository)
        {
        }
    }
}
