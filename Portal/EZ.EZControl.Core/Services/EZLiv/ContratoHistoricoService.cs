﻿using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EZ.EZControl.Services.EZLiv
{

    public class ContratoHistoricoService<TContratoHistorico> : EZControlDomainServiceBase<TContratoHistorico>, IContratoHistoricoService<TContratoHistorico>
    where TContratoHistorico : Domain.EZLiv.Geral.ContratoHistorico
    {
        public ContratoHistoricoService(IRepository<TContratoHistorico, int> repository)
            : base(repository)
        {
            ValidateEntity += ContratoHistoricoService_ValidateEntity;
            BeforeSaveEntity += ContratoHistoricoService_BeforeSaveEntity;
        }
        private void ContratoHistoricoService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TContratoHistorico, int> e)
        {
        }

        private void ContratoHistoricoService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TContratoHistorico, int> e)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public void CanAssignContrato(TContratoHistorico contratoHistorico, Contrato contrato)
        {
            if (contrato == null)
                throw new UserFriendlyException(L("ContratoHistorico.ContratoInvalido"));
        }

        public void CanAssignProdutoDePlanoDeSaude(TContratoHistorico contrato, ProdutoDePlanoDeSaude produto)
        {
            if (produto == null)
                throw new UserFriendlyException(L("Contrato.ProdutoDePlanoDeSaudeInvalido"));
        }
    }

    public class ContratoHistoricoService : ContratoHistoricoService<ContratoHistorico>, IContratoHistoricoService
    {
        public ContratoHistoricoService(IRepository<ContratoHistorico, int> repository)
            : base(repository)
        {
        }
    }
}
