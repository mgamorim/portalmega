﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class HospitalService : EZControlDomainServiceBase<Hospital>, IHospitalService
    {
        public HospitalService(IRepository<Hospital, int> repository)
            : base(repository)
        {

        }
    }
}
