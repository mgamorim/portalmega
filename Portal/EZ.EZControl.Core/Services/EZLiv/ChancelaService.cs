﻿using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Domain.Core.Enums;
using System;

namespace EZ.EZControl.Services.EZLiv
{
    public class ChancelaService : EZControlDomainServiceBase<Chancela>, IChancelaService
    {
        private readonly IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretor> _permissaoDeVendaDePlanoDeSaudeParaCorretorRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Corretor> _corretorRepository;
        private readonly IRepository<Contrato> _contratoRepository;
        public IAbpSession AbpSession { get; set; }

        public ChancelaService(IRepository<Chancela, int> repository,
            IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretor> permissaoDeVendaDePlanoDeSaudeParaCorretorRepository,
            IRepository<User, long> userRepository,
            IRepository<Corretor> corretorRepository,
            IRepository<Contrato> contratoRepository)
            : base(repository)
        {
            _permissaoDeVendaDePlanoDeSaudeParaCorretorRepository = permissaoDeVendaDePlanoDeSaudeParaCorretorRepository;
            _userRepository = userRepository;
            _corretorRepository = corretorRepository;
            _contratoRepository = contratoRepository;
            AbpSession = NullAbpSession.Instance;
        }

        /// <summary>
        /// Pega as chancelas a partir do usuário logado
        /// Usuário do tipo corretor: As chancelas são recuperadas a partir das permissões dos produtos concedida ao corretor, onde fica as chancelas configuradas.
        /// usuário do tipo beneficiário: As chancelas são recuperadas a partir dos produtos, onde fica as chancelas configuradas.
        /// </summary>
        /// <returns></returns>
        public async Task<IQueryable<Chancela>> GetAllByUsuarioLogado()
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var uQuery = _userRepository.GetAll();

                var usuario = uQuery.FirstOrDefault(x => x.Id == AbpSession.UserId.Value);

                if (usuario.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
                {
                    var cQuery = _corretorRepository.GetAll();

                    var corretorQuery = from corretor in cQuery
                                        join user in uQuery on corretor.PessoaId equals user.PessoaId
                                        where user.Id == AbpSession.UserId.Value
                                        select corretor;

                    var resultCorretor = corretorQuery.FirstOrDefault();
                    var corretorId = resultCorretor != null
                        ? resultCorretor.Id
                        : default(int);

                    return await Task.Run(() =>
                    {
                        var queryChancela = _permissaoDeVendaDePlanoDeSaudeParaCorretorRepository
                            .GetAll()
                            .Where(x => x.Corretores.Any(y => y.Id == corretorId))
                            .SelectMany(x => x.Produtos)
                            .Distinct()
                            .SelectMany(x => x.Chancelas)
                            .Distinct();

                        return queryChancela;
                    });
                }
                else if (usuario.TipoDeUsuario == TipoDeUsuarioEnum.Beneficiario)
                {
                    return await Task.Run(() =>
                    {
                        var queryChancela = _contratoRepository.GetAll()
                                .Where(x => x.ProdutoDePlanoDeSaudeId > 0 
                                    && (x.DataInicioVigencia <= DateTime.Now && x.DataFimVigencia >= DateTime.Now))
                                .Select(x => x.ProdutoDePlanoDeSaude)
                                .Distinct()
                                .SelectMany(x => x.Chancelas)
                                .Distinct();

                        return queryChancela;
                    });
                }

                return Repository.GetAll();
            }
        }
    }
}
