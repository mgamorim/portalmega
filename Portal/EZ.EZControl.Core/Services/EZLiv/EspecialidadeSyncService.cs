﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.Sync;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class EspecialidadeSyncService : EZControlDomainServiceBase<EspecialidadeSync>, IEspecialidadeSyncService
    {
        private readonly IEspecialidadeService _especialidadeService;
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IGrupoPessoaService _grupoPessoaService;
        private readonly IEnderecoService _enderecoService;
        private readonly ITipoDeLogradouroService _tipoDeLogradouroService;
        private readonly ICidadeService _cidadeService;
        private readonly ITelefoneService _telefoneService;
        private readonly IHospitalService _hospitalService;

        public EspecialidadeSyncService(IRepository<EspecialidadeSync, int> repository,
            IEspecialidadeService especialidadeService,
            IPessoaService pessoaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IGrupoPessoaService grupoPessoaService,
            IEnderecoService enderecoService,
            ITipoDeLogradouroService tipoDeLogradouroService,
            ICidadeService cidadeService,
            ITelefoneService telefoneService,
            IHospitalService hospitalService)
            : base(repository)
        {
            _especialidadeService = especialidadeService;
            _pessoaService = pessoaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _grupoPessoaService = grupoPessoaService;
            _enderecoService = enderecoService;
            _tipoDeLogradouroService = tipoDeLogradouroService;
            _cidadeService = cidadeService;
            _telefoneService = telefoneService;
            _hospitalService = hospitalService;

        }

        public async Task<List<EspecialidadeSync>> GetEspecialidadeSyncByEstado(string estado)
        {
            return await base.GetAllListAsync(x =>
                                             x.Estado.ToLower().Contains(estado.ToLower()) ||
                                             x.NomeEstado.ToLower().Contains(estado.ToLower()));
        }

        public async Task<List<EspecialidadeSync>> GetEspecialidadeSyncByEstadoDistinct(string estado)
        {
            var especialidadesSync =
                await base.GetAllListAsync(x =>
                                             x.Estado.ToLower().Contains(estado.ToLower()) ||
                                             x.NomeEstado.ToLower().Contains(estado.ToLower()));

            return especialidadesSync.Distinct().ToList();
        }

        public async Task<List<EspecialidadeSync>> GetEspecialidadeSyncByMunicipio(string estado, string municipio)
        {
            return await base.GetAllListAsync(x =>
                                                (x.Estado.ToLower().Contains(estado.ToLower()) ||
                                                x.NomeEstado.ToLower().Contains(estado.ToLower())) &&
                                                x.Municipio.ToLower().Contains(municipio.ToLower()));
        }

        public async Task<List<EspecialidadeSync>> GetEspecialidadeSyncByMunicipioDistinct(string estado, string municipio)
        {
            var especialidadesSync = await base.GetAllListAsync(x =>
                                                                  (x.Estado.ToLower().Contains(estado.ToLower()) ||
                                                                  x.NomeEstado.ToLower().Contains(estado.ToLower())) &&
                                                                  x.Municipio.ToLower().Contains(municipio.ToLower()));

            return especialidadesSync.Distinct().ToList();
        }

        public async Task<List<EspecialidadeSync>> GetEspecialidadeSyncByNomeDistinct(string nome)
        {
            return await base.GetAllListAsync(x => x.Nome.ToLower().Contains(nome.ToLower()));
        }

        public async Task Sincronizar()
        {
            DateTime dataSync = DateTime.Now;
            var listaSync = base.GetAll();

            #region CargaEspecialidades
            try
            {
                var listaEspecialidades = base.GetAll().GroupBy(g => g.Especialidade).Select(x => new
                {
                    //Codigo = x.Key.Trim().Substring(0, Especialidade.CodigoMaxLength).ToUpper(),
                    //Nome = x.Key.Trim().Substring(0, Especialidade.NomeMaxLength).ToUpper(),
                    Codigo = x.Key.Trim().Substring(0, x.Key.Trim().Length > Especialidade.CodigoMaxLength ? Especialidade.CodigoMaxLength : x.Key.Trim().Length).ToUpper(),
                    Nome = x.Key.Trim().Substring(0, x.Key.Trim().Length > Especialidade.NomeMaxLength ? Especialidade.NomeMaxLength : x.Key.Trim().Length).ToUpper(),
                    IsActive = true,
                    ExternalId = x.Max(y => y.Id),
                    DateOfEditionIntegration = dataSync
                }).ToList();

                foreach (var item in listaEspecialidades)
                {
                    try
                    {
                        if (_especialidadeService.GetAll().Where(x => x.Nome.ToUpper() == item.Nome).ToList().Count() == 0)
                        {
                            var espec = new Especialidade()
                            {
                                Codigo = item.Codigo,
                                Nome = item.Nome,
                                IsActive = item.IsActive,
                                ExternalId = item.ExternalId,
                                DateOfEditionIntegration = dataSync
                            };
                            await _especialidadeService.CreateAndReturnEntity(espec);
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO: Criar log de erro
                    }
                }

                listaEspecialidades = null;
            }
            catch (Exception ex)
            {
                //TODO: Criar log de erro
                throw ex;
            }
            #endregion

            #region CargaPessoaJuridica
            try
            {
                // TODO: Corrigir o SEED para criar um grupo correto
                var grupoPessoaPadrao = _grupoPessoaService.GetAll().FirstOrDefault();

                var listaPessoas = listaSync.GroupBy(g => g.Nome).Select(x => new
                {
                    Nome = x.Key.Trim().Substring(0, PessoaJuridica.MaxNomeFantasiaLength).ToUpper(),
                    IsActive = true,
                    ExternalId = x.Max(y => y.Id),
                    DateOfEditionIntegration = dataSync
                }).ToList();

                foreach (var item in listaPessoas)
                {
                    try
                    {
                        var pessoaJuridicaEz = _pessoaJuridicaService.GetAll().Where(x => x.NomeFantasia == item.Nome).FirstOrDefault();
                        if (pessoaJuridicaEz == null)
                        {
                            var pessoa = new PessoaJuridica()
                            {
                                NomeFantasia = item.Nome,
                                RazaoSocial = item.Nome,
                                NomePessoa = item.Nome,
                                ExternalId = item.ExternalId,
                                DateOfEditionIntegration = dataSync
                            };

                            using (var trans = UnitOfWorkManager.Begin())
                            {
                                pessoa.AssociarGrupoPessoa(grupoPessoaPadrao, _pessoaService);
                                var pj = await _pessoaJuridicaService.CreateAndReturnEntity(pessoa);

                                #region Endereço
                                var listEndereco = listaSync.Where(x => x.Nome == item.Nome)
                                    .GroupBy(x => new { x.Nome, x.Logradouro, x.Numero, x.Complemento, x.Bairro, x.Municipio, x.Estado })
                                    .ToList();
                                foreach (var enderecoSync in listEndereco)
                                {
                                    var tipoLogradouroPadrao = _tipoDeLogradouroService.GetAll().FirstOrDefault();
                                    var cidade = _cidadeService.GetAll().Where(x => x.Nome.ToUpper() == enderecoSync.Key.Municipio.ToUpper() && x.Estado.Sigla == enderecoSync.Key.Estado).FirstOrDefault();
                                    var cepPadrao = string.Empty;

                                    var endereco = new Endereco(pj, string.Empty, Domain.Global.Enums.TipoEnderecoEnum.Comercial, tipoLogradouroPadrao, enderecoSync.Key.Logradouro, enderecoSync.Key.Numero, enderecoSync.Key.Complemento, enderecoSync.Key.Bairro, cepPadrao, cidade, string.Empty);

                                    await _enderecoService.CreateEntity(endereco);
                                }
                                #endregion

                                #region Telefones
                                var listTelefone1 = listaSync.Where(x => x.Nome == item.Nome).GroupBy(x => new { x.Nome, x.Telefone1 }).ToList();
                                foreach (var tel1 in listTelefone1)
                                {
                                    var telefone1 = new Telefone(pj, Domain.Global.Enums.TipoTelefoneEnum.Comercial, tel1.Key.Telefone1.Substring(0, 2), tel1.Key.Telefone1.Trim(), "55", string.Empty);

                                    await _telefoneService.CreateEntity(telefone1);
                                }
                                var listTelefone2 = listaSync.Where(x => x.Nome == item.Nome).GroupBy(x => new { x.Nome, x.Telefone1 }).ToList();
                                foreach (var tel2 in listTelefone2)
                                {
                                    var telefone2 = new Telefone(pj, Domain.Global.Enums.TipoTelefoneEnum.Comercial, tel2.Key.Telefone1.Substring(0, 2), tel2.Key.Telefone1.Trim(), "55", string.Empty);

                                    await _telefoneService.CreateEntity(telefone2);
                                }
                                await trans.CompleteAsync();
                                #endregion

                                await trans.CompleteAsync();
                            }
                        }
                        else if (_hospitalService.GetAll().Where(x => x.PessoaJuridicaId == pessoaJuridicaEz.Id).FirstOrDefault() == null) // Já existe a pessoa jurídica, então cria as unidades de saude
                        {
                            var unidadeDeSaude = new Hospital()
                            {
                                IsActive = true,
                                ExternalId = item.ExternalId,
                                DateOfEditionIntegration = dataSync,
                                Nome = pessoaJuridicaEz.NomeFantasia,
                                PessoaJuridicaId = pessoaJuridicaEz.Id,
                                PessoaJuridica = pessoaJuridicaEz,
                                Endereco = pessoaJuridicaEz.Enderecos.FirstOrDefault(),
                                EnderecoId = pessoaJuridicaEz.Enderecos.FirstOrDefault().Id,
                                Telefone1 = pessoaJuridicaEz.Telefones.FirstOrDefault(),
                                Telefone1Id = pessoaJuridicaEz.Telefones.FirstOrDefault().Id
                            };

                            await _hospitalService.CreateEntity(unidadeDeSaude);
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO: Criar log de erro
                    }
                }
                listaPessoas = null;
            }
            catch (Exception ex)
            {
                //TODO: Criar log de erro
                throw ex;
            }
            #endregion
        }
    }
}
