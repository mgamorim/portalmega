﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Text;
using System.IO;

namespace EZ.EZControl.Services.EZLiv
{
    public class ProdutoDePlanoDeSaudeService : EZControlDomainServiceBase<ProdutoDePlanoDeSaude>, IProdutoDePlanoDeSaudeService
    {
        public ProdutoDePlanoDeSaudeService(IRepository<ProdutoDePlanoDeSaude, int> repository)
            : base(repository)
        {
        }


        public void CanAssignRedeCredenciada(ProdutoDePlanoDeSaude produto, RedeCredenciada redeCredenciada)
        {
            if (produto == null)
                throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.EmptyError"));

            if (redeCredenciada == null)
                throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.EmptyRedeCredenciadaError"));
        }

        public async Task<List<ProdutoDePlanoDeSaude>> GetAllListAtivoAsync()
        {
            return await ApplyEmpresaFilter(async () => await Repository.GetAllListAsync(x => x.IsActive));
        }
    }
}
