﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class UnidadeDeSaudeBaseService : EZControlDomainServiceBase<UnidadeDeSaudeBase>, IUnidadeDeSaudeBaseService
    {
        public UnidadeDeSaudeBaseService(IRepository<UnidadeDeSaudeBase, int> repository)
            : base(repository)
        {

        }
    }
}
