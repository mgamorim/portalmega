﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Runtime.Security;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Notifications;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class PropostaDeContratacaoService : EZControlDomainServiceBase<PropostaDeContratacao>, IPropostaDeContratacaoService
    {
        private readonly IWebUrlService _webUrlService;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IEmailSender _emailSender;
        private readonly UserManager _userManager;
        //private readonly IEmailNotificationService _emailNotificationService;
        private readonly IBeneficiarioBaseService _beneficiarioBaseService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IDependenteService _dependenteService;
        private readonly ICorretoraService _corretoraService;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;

        public PropostaDeContratacaoService(IRepository<PropostaDeContratacao, int> repository,
                                            IEmailSender emailSender,
                                            UserManager userManager,
                                            IWebUrlService webUrlService,
                                            IRepository<Tenant> tenantRepository,
                                            IBeneficiarioBaseService beneficiarioBaseService,
                                            IPessoaJuridicaService pessoaJuridicaService,
                                            IDependenteService dependenteService,
                                            ICorretoraService corretoraService,
                                            //,IEmailNotificationService emailNotificationService
                                            INotificationSubscriptionManager notificationSubscriptionManager,
                                            IAppNotifier appNotifier)
            : base(repository)
        {
            _emailSender = emailSender;
            _webUrlService = webUrlService;
            _userManager = userManager;
            _tenantRepository = tenantRepository;
            _beneficiarioBaseService = beneficiarioBaseService;
            //_emailNotificationService = emailNotificationService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _dependenteService = dependenteService;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _corretoraService = corretoraService;
        }

        private string GetNumero()
        {
            var ano = DateTime.Now.Year.ToString();
            var mes = DateTime.Now.Month.ToString();
            var dia = DateTime.Now.Day.ToString();
            var hor = DateTime.Now.Hour.ToString();
            var min = DateTime.Now.Minute.ToString();
            var seg = DateTime.Now.Second.ToString();
            var mil = DateTime.Now.Millisecond.ToString();

            return string.Format("ME{0}{1}{2}{3}{4}{5}{6}", ano, mes, dia, hor, min, seg, mil);
        }

        public void SendEmailPreCadastro(PropostaDeContratacao proposta)
        {
            try
            {
                var email =
                       proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (email == null)
                    throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundEmailError"));

                var user =
                    _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == proposta.Titular.PessoaFisicaId && x.TenantId == proposta.Titular.TenantId);

                if (user == null)
                    throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundUserError"));

                if (string.IsNullOrEmpty(user.PasswordResetCode))
                    throw new UserFriendlyException(L("UsuarioPorPessoa.UsuarioCadastroCompleto"));

                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "Account/ResetPassword" +
                        "?userId=" + Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.Id.ToString())) +
                        "&tenantId=" + (user.TenantId == null ? "" : Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.TenantId.Value.ToString()))) +
                        "&slug=" + user.Empresas.First().Slug +
                        "&resetCode=" + Uri.EscapeDataString(user.PasswordResetCode);

                var corpo = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoUsuarioCriadoPeloBeneficiario(user.UserName, user.Password, link, proposta.Corretora);

                var template = new TemplateEmail
                {
                    CorpoMensagem = corpo,
                    Remetente = "contato@portalmega.net",
                    Assunto = L("Beneficiario.Create")
                };

                _emailSender.Send(user.EmailAddress, L("Beneficiario.Create"), corpo);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
                //throw new AbpValidationException(e.Message);
            }
        }

        public void SendEmailEdicaoDeProposta(PropostaDeContratacao proposta)
        {
            try
            {
                var email =
                       proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (email == null)
                    throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundEmailError"));

                var link =
                    _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) +
                    "#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;

                var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoUsuarioCriadoPelaPropostaDeContratacao(link);

                var template = new TemplateEmail()
                {
                    CorpoMensagem = html,
                    Remetente = "contato@portalmega.net",
                    Assunto = L("PropostaDeContratacao.EditarProposta")
                };
                _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.EditarProposta"), html);
                //_emailNotificationService.Send(email.Endereco, template);
            }
            catch(UserFriendlyException ex)
            {
                throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundEmailError"));
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailConcluirAceite(PropostaDeContratacao proposta)
        {
            try
            {
                var user = proposta.UsuarioTitular;
                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) + "/#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
                var linkRedefinir = string.Empty;
                if (user != null && !string.IsNullOrEmpty(user.PasswordResetCode))
                {
                    linkRedefinir =
                                _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "Account/ResetPassword" +
                                "?userId=" + Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.Id.ToString())) +
                                "&tenantId=" + (user.TenantId == null ? "" : Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.TenantId.Value.ToString()))) +
                                "&resetCode=" + Uri.EscapeDataString(user.PasswordResetCode) +
                                "&slug=" + user.Empresas.First().Slug +
                                "&returnUrl=" + _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "anchor/tenant/ezliv/propostaDeContratacao";
                }

                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);

                var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlAceite(link, linkRedefinir, corretoraInfo);

                if (proposta.Titular?.PessoaFisica?.EnderecosEletronicos?.Count > 0)
                {
                    var email = proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email != null)
                    {
                        _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }

                if (proposta.Responsavel?.PessoaFisica?.EnderecosEletronicos?.Count > 0)
                {
                    var email = proposta.Responsavel.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email != null)
                    {
                        _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }

                if (proposta.Corretor?.Pessoa?.EnderecosEletronicos?.Count > 0)
                {
                    var emailCorretor = proposta.Corretor.Pessoa.EnderecosEletronicos.FirstOrDefault(
                             x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretor != null)
                    {
                        html = TemplateDeEnvioDeEmailDeSistema.GetHtmlAceiteCorretor(link, proposta.Titular.PessoaFisica.Nome, proposta.Numero, corretoraInfo);
                        _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }

                if (proposta.Corretora?.Pessoa?.EnderecosEletronicos?.Count > 0)
                {
                    var emailCorretora = proposta.Corretora.Pessoa.EnderecosEletronicos.FirstOrDefault(
                        x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretora != null)
                    {
                        html = TemplateDeEnvioDeEmailDeSistema.GetHtmlAceiteCorretor(link, proposta.Titular.PessoaFisica.Nome, proposta.Numero, corretoraInfo);

                        _emailSender.Send("contato@portalmega.net", emailCorretora.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailConcluirAceiteOnlyBeneficiario(PropostaDeContratacao proposta)
        {
            try
            {
                var user = proposta.UsuarioTitular;
                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) + "/#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
                var linkRedefinir = string.Empty;
                if (user != null && !string.IsNullOrEmpty(user.PasswordResetCode))
                {
                    linkRedefinir =
                                _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "Account/ResetPassword" +
                                "?userId=" + Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.Id.ToString())) +
                                "&tenantId=" + (user.TenantId == null ? "" : Uri.EscapeDataString(SimpleStringCipher.Instance.Encrypt(user.TenantId.Value.ToString()))) +
                                "&resetCode=" + Uri.EscapeDataString(user.PasswordResetCode) +
                                "&returnUrl=" + _webUrlService.GetSiteRootAddress(_tenantRepository.Get(user.TenantId.Value).TenancyName) + "anchor/tenant/ezliv/propostaDeContratacao";
                }

                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);

                var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlAceite(link, linkRedefinir, corretoraInfo);

                if (proposta.Titular?.PessoaFisica?.EnderecosEletronicos?.Count > 0)
                {
                    var email = proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email != null)
                    {
                        _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }

                if (proposta.Responsavel?.PessoaFisica?.EnderecosEletronicos?.Count > 0)
                {
                    var email = proposta.Responsavel.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email != null)
                    {
                        _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailConcluirAceiteOnlyCorretor(PropostaDeContratacao proposta)
        {
            try
            {
                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) + "/#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;

                CorretoraNotificationInfo corretoraInfo = null;
                if (proposta.Corretora != null)
                {
                    corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);
                }
                else
                {
                    corretoraInfo = CorretoraNotificationInfo.CriarCorretoraDefault();
                }

                if (proposta.Corretor?.Pessoa?.EnderecosEletronicos?.Count > 0)
                {
                    var emailCorretor = proposta.Corretor.Pessoa.EnderecosEletronicos.FirstOrDefault(
                             x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretor != null)
                    {
                        var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlAceiteCorretor(link, proposta.Titular.PessoaFisica.Nome, proposta.Numero, corretoraInfo);
                        _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }

                if (proposta.Corretora?.Pessoa?.EnderecosEletronicos?.Count > 0)
                {
                    var emailCorretora = proposta.Corretora.Pessoa.EnderecosEletronicos.FirstOrDefault(
                        x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretora != null)
                    {
                        var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlAceiteCorretor(link, proposta.Titular.PessoaFisica.Nome, proposta.Numero, corretoraInfo);

                        _emailSender.Send("contato@portalmega.net", emailCorretora.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                    }
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public async Task SendEmailHomologar(PropostaDeContratacao proposta)
        {
            try
            {
                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) + "/#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;

                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);

                if (proposta.Produto?.Administradora?.Homologadores != null)
                {
                    foreach (var homologador in proposta.Produto.Administradora.Homologadores)
                    {
                        var emailHomologador = homologador.Pessoa.EnderecosEletronicos.FirstOrDefault(
                                 x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                        if (emailHomologador != null)
                        {
                            var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlNovaHomologacao(proposta.Titular.NomePessoa, link, corretoraInfo);
                            _emailSender.Send("contato@portalmega.net", emailHomologador.Endereco, L("PropostaDeContratacao.HomologacaoDoContrato"), html);
                        }
                    }
                }

                if (proposta.Corretor?.Pessoa?.EnderecosEletronicos?.Count > 0)
                {
                    var emailCorretor = proposta.Corretor.Pessoa.EnderecosEletronicos.FirstOrDefault(
                        x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretor != null)
                    {
                        var htmlCorretor = TemplateDeEnvioDeEmailDeSistema.GetHtmlNovaHomologacaoParaCorretor(proposta.Titular.NomePessoa, link, corretoraInfo);
                        _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.HomologacaoDoContrato"), htmlCorretor);
                    }
                }

                if (proposta.Corretora?.Pessoa?.EnderecosEletronicos?.Count > 0)
                {
                    var emailCorretora = proposta.Corretora.Pessoa.EnderecosEletronicos.FirstOrDefault(
                        x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretora != null)
                    {
                        var htmlCorretor = TemplateDeEnvioDeEmailDeSistema.GetHtmlNovaHomologacaoParaCorretor(proposta.Titular.NomePessoa, link, corretoraInfo);
                        _emailSender.Send("contato@portalmega.net", emailCorretora.Endereco, L("PropostaDeContratacao.HomologacaoDoContrato"), htmlCorretor);
                    }
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailHomologacaoConcluida(PropostaDeContratacao proposta)
        {
            try
            {
                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) + "/#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);
                var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlHomologacaoConcluida(link, corretoraInfo);

                if (proposta.Titular?.PessoaFisica?.EnderecosEletronicos?.Count > 0)
                {
                    var emailBeneficiario =
                        proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailBeneficiario != null)
                    {
                        _emailSender.Send("contato@portalmega.net", emailBeneficiario.Endereco, L("PropostaDeContratacao.AguardandoPagamento"), html);
                    }
                }

                if (proposta.Responsavel?.PessoaFisica?.EnderecosEletronicos?.Count > 0)
                {
                    var emailResponsavel =
                        proposta.Responsavel.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailResponsavel == null)
                        throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundEmailError"));

                    _emailSender.Send("contato@portalmega.net", emailResponsavel.Endereco, L("PropostaDeContratacao.AguardandoPagamento"), html);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailPagamentoConcluidoBeneficiario(PropostaDeContratacao proposta)
        {
            try
            {
                var emailBeneficiario =
                        proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (emailBeneficiario == null)
                    throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundEmailError"));

                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);

                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName);
                var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlPagamentoBeneficiario(proposta.Titular.NomePessoa, link, corretoraInfo);

                _emailSender.Send("contato@portalmega.net", emailBeneficiario.Endereco, L("PropostaDeContratacao.PagamentoConcluido"), html);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailPagamentoConcluidoAdmCorretora(PropostaDeContratacao proposta)
        {
            try
            {
                var administradora = proposta.Produto != null && proposta.Produto.Administradora != null ? proposta.Produto.Administradora : null;
                var corretora = proposta.Corretora != null ? proposta.Corretora : null;
                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(corretora);

                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName);
                var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlPagamentoAdmCorretora(link, corretoraInfo);

                if (administradora != null)
                {
                    var email = administradora
                                     .Pessoa
                                     .EnderecosEletronicos
                                     .FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);
                    _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.PagamentoConcluido"), html);
                }


                if (corretora != null)
                {
                    var email = corretora
                                     .Pessoa
                                     .EnderecosEletronicos
                                     .FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    _emailSender.Send("contato@portalmega.net", email.Endereco, L("PropostaDeContratacao.PagamentoConcluido"), html);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public async Task SendEmailAceiteDoContrato(PropostaDeContratacao proposta)
        {
            try
            {
                var emailCorretor = proposta.Corretor.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (emailCorretor == null)
                    throw new UserFriendlyException(L("PropostaDeContratacao.EmailCorretorNotFoundError"));

                var corretora = proposta.Corretora != null ? proposta.Corretora : null;
                var administradora = proposta.Produto != null && proposta.Produto.Administradora != null ? proposta.Produto.Administradora : null;
                var operadora = proposta.Produto != null && proposta.Produto.Operadora != null ? proposta.Produto.Operadora : null;

                var html = TemplateDeEnvioDeEmailDeSistema
                                .GetHtmlAceiteDoContrato(proposta.Titular.NomePessoa,
                                                         proposta.Produto.Nome,
                                                         administradora != null ? administradora.PessoaJuridica.RazaoSocial : string.Empty,
                                                         corretora != null ? corretora.PessoaJuridica.RazaoSocial : string.Empty,
                                                         operadora != null ? proposta.Produto.Operadora.PessoaJuridica.RazaoSocial : string.Empty);

                if (emailCorretor != null)
                    _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);

                if (administradora != null)
                {
                    var pjAdministradora = await _pessoaJuridicaService.GetById(proposta.Produto.Administradora.PessoaJuridicaId);

                    var emailAdministradora = pjAdministradora.EnderecosEletronicos.FirstOrDefault(
                             x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailAdministradora != null)
                        _emailSender.Send("contato@portalmega.net", emailAdministradora.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                }

                if (corretora != null)
                {
                    var pjCorretora = await _pessoaJuridicaService.GetById(proposta.Corretora.PessoaJuridicaId);

                    var emailCorretora = pjCorretora.EnderecosEletronicos.FirstOrDefault(
                             x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailCorretora != null)
                        _emailSender.Send("contato@portalmega.net", emailCorretora.Endereco, L("PropostaDeContratacao.AceiteDoContrato"), html);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public async Task SendEmailHomologacaoDoContrato(PropostaDeContratacao proposta)
        {
            try
            {
                var emailCorretor = proposta.Corretor.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (emailCorretor == null)
                    throw new UserFriendlyException(L("PropostaDeContratacao.EmailCorretorNotFoundError"));

                var administradora = proposta.Produto != null && proposta.Produto.Administradora != null ? proposta.Produto.Administradora : null;
                var operadora = proposta.Produto != null && proposta.Produto.Operadora != null ? proposta.Produto.Operadora : null;

                var html = TemplateDeEnvioDeEmailDeSistema
                                .GetHtmlHomologacaoDoContrato(proposta.Titular.NomePessoa,
                                                              proposta.Produto.Nome,
                                                              administradora != null ? administradora.PessoaJuridica.RazaoSocial : string.Empty,
                                                              operadora != null ? proposta.Produto.Operadora.PessoaJuridica.RazaoSocial : string.Empty);

                if (emailCorretor != null)
                    _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.HomologacaoDoContrato"), html);

                if (administradora != null)
                {
                    var pj = await _pessoaJuridicaService.GetById(administradora.PessoaJuridicaId);

                    var emailAdministradora = pj.EnderecosEletronicos.FirstOrDefault(
                             x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (emailAdministradora != null)
                        _emailSender.Send("contato@portalmega.net", emailAdministradora.Endereco, L("PropostaDeContratacao.HomologacaoDoContrato"), html);
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailBoletoGerado(PropostaDeContratacao proposta)
        {
            try
            {
                var emailCorretor = proposta.Corretor.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (emailCorretor == null)
                    throw new UserFriendlyException(L(" UsuarioPorPessoa.NotFoundEmailCorretorError"));

                var administradora = proposta.Produto != null && proposta.Produto.Administradora != null ? proposta.Produto.Administradora : null;
                var operadora = proposta.Produto != null && proposta.Produto.Operadora != null ? proposta.Produto.Operadora : null;

                var html = TemplateDeEnvioDeEmailDeSistema
                                    .GetHtmlBoletoGerado(proposta.Titular.NomePessoa,
                                                         proposta.Produto.Nome,
                                                         administradora != null ? administradora.PessoaJuridica.RazaoSocial : string.Empty,
                                                         operadora != null ? proposta.Produto.Operadora.PessoaJuridica.RazaoSocial : string.Empty);

                _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.BoletoGerado"), html);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public void SendEmailFormularios(PropostaDeContratacao proposta, bool enviaCopiaParaAdministradora = false, bool enviaCopiaParaCorretora = false)
        {
            try
            {
                var emailTitular = proposta.Titular.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                if (emailTitular == null)
                    throw new UserFriendlyException(L("PropostaDeContratacao.NotFoundEmailTitularError"));

                var administradora = proposta.Produto != null && proposta.Produto.Administradora != null ? proposta.Produto.Administradora : null;
                var corretora = proposta.Corretora != null ? proposta.Corretora : null;

                var html = TemplateDeEnvioDeEmailDeSistema
                                    .GetHtmlFormulario(proposta.Titular.NomePessoa,
                                                       administradora != null ? administradora.PessoaJuridica.RazaoSocial :
                                                       corretora != null ? proposta.Corretora.PessoaJuridica.RazaoSocial : string.Empty);

                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("contato@portalmega.net"),
                    To = { new MailAddress(emailTitular.Endereco) },
                    Subject = L("PropostaDeContratacao.Formulario"),
                    Body = html
                };

                if (enviaCopiaParaCorretora && corretora != null)
                {
                    var email = corretora
                                     .PessoaJuridica
                                     .EnderecosEletronicos
                                     .FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email != null)
                    {
                        mail.CC.Add(new MailAddress(email.Endereco));
                        mail.ReplyToList.Add(new MailAddress(email.Endereco));
                    }
                }

                if (enviaCopiaParaAdministradora && administradora != null)
                {
                    var email = administradora
                                     .PessoaJuridica
                                     .EnderecosEletronicos
                                     .FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email != null)
                        mail.CC.Add(new MailAddress(email.Endereco));
                }

                //TODO: Criar lógica para inserir os pdf da proposta

                _emailSender.Send(mail);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        public async Task SendEmailComprovanteDepositoCorretor(PropostaDeContratacao proposta)
        {
            try
            {
                var emailCorretor = proposta.Corretor.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                           x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(proposta.Corretora);

                if (emailCorretor == null)
                    throw new UserFriendlyException(L("PropostaDeContratacao.EmailCorretorNotFoundError"));

                var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName);

                var html = TemplateDeEnvioDeEmailDeSistema
                                .GetHtmlComprovanteDepositoCorretor(proposta.Titular.PessoaFisica.Nome, proposta.Numero, link, corretoraInfo);

                if (emailCorretor != null)
                    _emailSender.Send("contato@portalmega.net", emailCorretor.Endereco, L("PropostaDeContratacao.ComprovanteDeDeposito"), html);
            }
            catch (Exception e)
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.SendEmail"), e.Message);
            }
        }

        private async Task SendNotificationComprovanteDepositoCorretor(User userCorretor, PropostaDeContratacao proposta)
        {
            await _appNotifier.SendMessageAsync(userCorretor.ToUserIdentifier(), String.Format(L("PropostaDeContratacao.ComprovanteDeDepositoEnviado"), proposta.Numero));
        }

        public async Task SendNotificationAceite(PropostaDeContratacao proposta)
        {
            var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) +
                "#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
            var mensagem = string.Format("{0} <a href='{1}'> Link.</a>", L("PropostaDeContratacao.UsuarioAceiteDoContrato"), link);

            if (proposta.UsuarioTitular != null) {
                await _appNotifier.SendMessageAsync(proposta.UsuarioTitular.ToUserIdentifier(), mensagem);
            }

            if (proposta.UsuarioResponsavel != null) {
                await _appNotifier.SendMessageAsync(proposta.UsuarioResponsavel.ToUserIdentifier(), mensagem);
            }
        }


        public async Task SendNotificationDeclaracaoSaude(PropostaDeContratacao proposta)
        {
            var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) +
                "#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
            var mensagem = string.Format("{0} <a href='{1}'> Link.</a>", "Favor Seguir para o preenchimento da Declaração de Saúde e Aceite Eletrônico!", link);

            if (proposta.UsuarioTitular != null)
            {
                await _appNotifier.SendMessageAsync(proposta.UsuarioTitular.ToUserIdentifier(), mensagem);
            }

            if (proposta.UsuarioResponsavel != null)
            {
                await _appNotifier.SendMessageAsync(proposta.UsuarioResponsavel.ToUserIdentifier(), mensagem);
            }
        }


        public async Task SendNotificationNovaHomologacao(PropostaDeContratacao proposta)
        {
            var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) +
                "#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
            var mensagem = string.Format("{0} <a href='{1}'> Link.</a>", L("PropostaDeContratacao.AdministradoraHomologacaoDoContrato"), link);

            if (proposta.Corretor != null)
            {
                var userCorretor = _userManager.Users.FirstOrDefault(
                    x => x.PessoaId == proposta.Corretor.PessoaId);
                if (userCorretor != null)
                {
                    await _appNotifier.SendMessageAsync(userCorretor.ToUserIdentifier(), mensagem);
                }
            }

            if (proposta.Produto?.Administradora?.Homologadores?.Count > 0)
            {
                foreach (var homologador in proposta.Produto.Administradora.Homologadores)
                {
                    var userHomologador = _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == homologador.PessoaId);
                    if (userHomologador != null)
                    {
                        await _appNotifier.SendMessageAsync(userHomologador.ToUserIdentifier(), mensagem);
                    }
                }
            }

            if (proposta.Produto?.Administradora?.Supervisores?.Count > 0)
            {
                foreach (var supervisor in proposta.Produto.Administradora.Supervisores)
                {
                    var userSupervisor = _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == supervisor.PessoaId);
                    if (userSupervisor != null)
                    {
                        await _appNotifier.SendMessageAsync(userSupervisor.ToUserIdentifier(), mensagem);
                    }
                }
            }

            if (proposta.Produto?.Administradora?.Gerentes?.Count > 0)
            {
                foreach (var gerente in proposta.Produto.Administradora.Gerentes)
                {
                    var userGerente = _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == gerente.PessoaId);
                    if (userGerente != null)
                    {
                        await _appNotifier.SendMessageAsync(userGerente.ToUserIdentifier(), mensagem);
                    }
                }
            }
        }

        public async Task SendNotificationHomologacaoConcluida(PropostaDeContratacao proposta)
        {
            var link = _webUrlService.GetSiteRootAddress(_tenantRepository.Get(proposta.TenantId.Value).TenancyName) +
               "#/tenant/ezliv/propostaDeContratacao/alterar/" + proposta.Id;
            var mensagem = string.Format("{0} <a href='{1}'> Link.</a>", L("PropostaDeContratacao.UsuarioAguardandoPagamento"), link);

            if (proposta.Corretor?.PessoaFisica != null)
            {
                var user = _userManager.Users.FirstOrDefault(
                                x => x.PessoaId == proposta.Corretor.PessoaFisica.Id && x.TenantId == proposta.Titular.TenantId);
                if (user != null)
                {
                    await _appNotifier.SendMessageAsync(user.ToUserIdentifier(), mensagem);
                }
            }

            await _appNotifier.SendMessageAsync(proposta.UsuarioTitular.ToUserIdentifier(), mensagem);
        }

        public async Task SendNotificationPagamentoConcluido(PropostaDeContratacao proposta)
        {
            var mensagem = L("PropostaDeContratacao.PropostaPaga");

            if (proposta.Produto?.Administradora?.Homologadores?.Count > 0)
            {
                foreach (var homologador in proposta.Produto.Administradora.Homologadores)
                {
                    var userHomologador = _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == homologador.PessoaId);
                    if (userHomologador != null)
                    {
                        await _appNotifier.SendMessageAsync(userHomologador.ToUserIdentifier(), mensagem);
                    }
                }
            }

            if (proposta.Produto?.Administradora?.Supervisores?.Count > 0)
            {
                foreach (var supervisor in proposta.Produto.Administradora.Supervisores)
                {
                    var userSupervisor = _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == supervisor.PessoaId);
                    if (userSupervisor != null)
                    {
                        await _appNotifier.SendMessageAsync(userSupervisor.ToUserIdentifier(), mensagem);
                    }
                }
            }

            if (proposta.Produto?.Administradora?.Gerentes?.Count > 0)
            {
                foreach (var gerente in proposta.Produto.Administradora.Gerentes)
                {
                    var userGerente = _userManager.Users.FirstOrDefault(
                        x => x.PessoaId == gerente.PessoaId);
                    if (userGerente != null)
                    {
                        await _appNotifier.SendMessageAsync(userGerente.ToUserIdentifier(), mensagem);
                    }
                }
            }

            if (proposta.Corretor?.PessoaFisica != null)
            {
                var user = _userManager.Users.FirstOrDefault(
                                x => x.PessoaId == proposta.Corretor.PessoaFisica.Id && x.TenantId == proposta.Titular.TenantId);
                if (user != null)
                {
                    await _appNotifier.SendMessageAsync(user.ToUserIdentifier(), mensagem);
                }
            }

            await _appNotifier.SendMessageAsync(proposta.UsuarioTitular.ToUserIdentifier(), mensagem);
        }

        public void SendEmailDocumentoEmExigencia(PropostaDeContratacao proposta, List<ArquivoDocumento> documentosEmExigencia)
        {
            var titular = proposta.Titular;
            var corretor = proposta.Corretor;

            var emailTitular = titular.Pessoa.EnderecosEletronicos.Where(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal).FirstOrDefault();

            if (emailTitular == null)
                throw new UserFriendlyException(L("Proposta.EmailTitularNotFound"));

            //DETERMINAR DE ONDE PEGAR O EMAIL DO CORRETOR E DA CORRETORA
            //var emailCorretor = corretor.Pessoa.EnderecosEletronicos.Where(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal).FirstOrDefault();

            //if (emailCorretor == null)
            //    throw new UserFriendlyException(L(" Proposta.EmailCorretorNotFound"));
            ////De onde buscar o email da corretora?
            //var emailCorretora = "";

            //if (emailCorretora == null)
            //    throw new UserFriendlyException(L(" Proposta.EmailCorretoraNotFound"));

            var stringDocumentos = new StringBuilder();
            foreach (var documento in documentosEmExigencia)
            {
                stringDocumentos.AppendLine(String.Format("{0} - {1}", documento.Tipo.ToString(), documento.Motivo));
            }

            var html = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoDocumentosEmExigencia(stringDocumentos.ToString());

            var template = new TemplateEmail()
            {
                CorpoMensagem = html,
                Remetente = "contato@portalmega.net",
                Assunto = L("PropostaDeContratacao.DocumentosEmExigencia")
            };
            _emailSender.Send("contato@portalmega.net", emailTitular.Endereco, L("PropostaDeContratacao.EditarProposta"), html);
            //_emailNotificationService.Send(String.Format("{0}, {1}, {2}", emailTitular, emailCorretor, emailCorretora), template);
        }

        #region Passo pré-cadastro

        public async Task PodeSalvarPassoPreCadastro(PropostaDeContratacao proposta)
        {

        }

        public async Task<PropostaDeContratacao> SavePassoPreCadastro(PropostaDeContratacao proposta, bool sendEmail = true)
        {
            bool ehInclusao = (proposta.Id == 0);

            if (ehInclusao)
                proposta.Numero = GetNumero();

            await PodeSalvarPassoPreCadastro(proposta);
            await CreateOrUpdateEntity(proposta);
            proposta = await Repository.GetAsync(proposta.Id);
            return proposta;
        }

        [AbpAllowAnonymous]
        public async Task<PropostaDeContratacao> SavePassoPreCadastroAnonymous(PropostaDeContratacao proposta, bool sendEmail = true)
        {
            return await base.Repository.InsertOrUpdateAsync(proposta);
        }

        #endregion

        #region Passo selecionar plano

        public async Task PodeSalvarPassoSelecionarPlano(PropostaDeContratacao proposta)
        {

        }

        public async Task<PropostaDeContratacao> SavePassoSelecionarPlano(PropostaDeContratacao proposta)
        {
            await PodeSalvarPassoPreCadastro(proposta);
            await PodeSalvarPassoSelecionarPlano(proposta);
            try
            {
                return await CreateOrUpdateAndReturnSavedEntity(proposta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        #endregion

        #region Passo dados gerais

        public async Task PodeSalvarPassoDadosGerais(PropostaDeContratacao beneficiario)
        {

        }

        public async Task<PropostaDeContratacao> SavePassoDadosGerais(PropostaDeContratacao proposta)
        {
            await PodeSalvarPassoPreCadastro(proposta);
            await PodeSalvarPassoSelecionarPlano(proposta);
            await PodeSalvarPassoDadosGerais(proposta);

            return await CreateOrUpdateAndReturnSavedEntity(proposta);
        }

        #endregion

        #region Passo envio de documentos

        public async Task PodeSalvarPassoEnvioDeDocumentos(PropostaDeContratacao beneficiario)
        {
        }

        public async Task<PropostaDeContratacao> SavePassoEnvioDeDocumentos(PropostaDeContratacao proposta)
        {
            await PodeSalvarPassoPreCadastro(proposta);
            await PodeSalvarPassoSelecionarPlano(proposta);
            await PodeSalvarPassoDadosGerais(proposta);
            await PodeSalvarPassoEnvioDeDocumentos(proposta);
            return await CreateOrUpdateAndReturnSavedEntity(proposta);
        }

        #endregion

        #region Passo aceite do contrato

        public async Task PodeSalvarPassoAceiteDoContrato(PropostaDeContratacao proposta)
        {

        }

        public async Task<PropostaDeContratacao> SavePassoAceiteDoContrato(PropostaDeContratacao proposta)
        {
            await PodeSalvarPassoPreCadastro(proposta);
            await PodeSalvarPassoSelecionarPlano(proposta);
            await PodeSalvarPassoDadosGerais(proposta);
            await PodeSalvarPassoEnvioDeDocumentos(proposta);
            await PodeSalvarPassoAceiteDoContrato(proposta);

            return await CreateOrUpdateAndReturnSavedEntity(proposta);
        }

        #endregion

        #region Passo passo homologação

        public async Task PodeSalvarPassoHomologacao(PropostaDeContratacao proposta)
        {
        }

        public async Task<PropostaDeContratacao> SavePassoHomologacao(PropostaDeContratacao proposta)
        {
            await PodeSalvarPassoPreCadastro(proposta);
            await PodeSalvarPassoSelecionarPlano(proposta);
            await PodeSalvarPassoDadosGerais(proposta);
            await PodeSalvarPassoEnvioDeDocumentos(proposta);
            await PodeSalvarPassoAceiteDoContrato(proposta);
            await PodeSalvarPassoHomologacao(proposta);
            return await CreateOrUpdateAndReturnSavedEntity(proposta);
        }

        #endregion

        #region Passo pagamento

        public async Task PodeSalvarPassoPagamento(PropostaDeContratacao beneficiario)
        {
        }

        public async Task<PropostaDeContratacao> SavePassoPagamento(PropostaDeContratacao proposta)
        {
            await PodeSalvarPassoPreCadastro(proposta);
            await PodeSalvarPassoSelecionarPlano(proposta);
            await PodeSalvarPassoDadosGerais(proposta);
            await PodeSalvarPassoEnvioDeDocumentos(proposta);
            await PodeSalvarPassoAceiteDoContrato(proposta);
            await PodeSalvarPassoHomologacao(proposta);
            await PodeSalvarPassoPagamento(proposta);
            return await CreateOrUpdateAndReturnSavedEntity(proposta);
        }

        #endregion

        #region Atualizar Pagamento

        public async Task SaveAtualizarPagamento(object objPagseguro)
        {
            //TODO: Criar atualização a partir do push do pagseguro
            throw new NotImplementedException();
        }

        public async Task<int> SalvarPreCadastro(PropostaDeContratacao proposta)
        {
            _beneficiarioBaseService.IsValidPreCadastro(proposta.Titular);
            return await Repository.InsertAndGetIdAsync(proposta);
        }

        #endregion
    }
}
