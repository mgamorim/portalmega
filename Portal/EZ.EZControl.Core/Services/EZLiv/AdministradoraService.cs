﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    public class AdministradoraService : EZControlDomainServiceBase<Administradora>, IAdministradoraService
    {
        private readonly IRepository<Empresa, int> _empresaRepository;

        public AdministradoraService(
            IRepository<Administradora, int> repository,
            IRepository<Empresa, int> empresaRepository)
            : base(repository)
        {
            _empresaRepository = empresaRepository;
        }

        public async Task<Administradora> GetByEmpresaLogada()
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

            if (empresaId != null)
            {
                var queryEmpresa = _empresaRepository.GetAll();
                var queryAdm = Repository.GetAll();

                var query = from empresa in queryEmpresa
                    join administradora in queryAdm on empresa.PessoaJuridicaId equals administradora.PessoaJuridicaId
                    where empresa.Id == empresaId
                    select administradora;

                var adm = await Task.Run(() => query.FirstOrDefault());

                if (adm != null)
                {
                    return adm;
                }
            }

            return null;
        }
    }
}
