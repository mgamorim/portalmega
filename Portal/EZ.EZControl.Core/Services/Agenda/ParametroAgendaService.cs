﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Core.Geral;

namespace EZ.EZControl.Services.Agenda
{
    public class ParametroAgendaService : ParametroBaseService<ParametroAgenda>, IParametroAgendaService
    {
        public ParametroAgendaService(IRepository<ParametroAgenda, int> repository)
            : base(repository)
        {
        }
    }
}