﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoReuniaoService : EZControlDomainServiceBase<EventoReuniao>, IEventoReuniaoService
    {
        public EventoReuniaoService(IRepository<EventoReuniao, int> repository)
            : base(repository)
        {
        }
    }
}