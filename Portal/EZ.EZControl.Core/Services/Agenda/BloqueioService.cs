﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class BloqueioService : EZControlDomainServiceBase<Bloqueio>, IBloqueioService
    {
        public BloqueioService(IRepository<Bloqueio, int> repository)
            : base(repository)
        {
        }
    }
}