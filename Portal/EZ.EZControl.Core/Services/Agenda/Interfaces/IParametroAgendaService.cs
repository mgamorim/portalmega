﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IParametroAgendaService : IParametroBaseService<ParametroAgenda>
    {
    }
}