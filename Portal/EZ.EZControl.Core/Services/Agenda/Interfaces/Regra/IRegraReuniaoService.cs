﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Agenda.Interfaces.Regra
{
    public interface IRegraReuniaoService : IService<RegraReuniao>
    {
    }
}
