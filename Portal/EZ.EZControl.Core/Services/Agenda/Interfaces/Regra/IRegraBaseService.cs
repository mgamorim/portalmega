﻿using System.Collections.Generic;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Agenda.Interfaces.Regra
{
    public interface IRegraBaseService : IService<RegraBase>
    {
        List<TipoDeEventoEnum> GetTiposDeEventosBySistema(SistemaEnum sistema);
        List<SistemaEnum> GetSistemasCriamEventos();
    }
}
