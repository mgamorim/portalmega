﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IConfiguracaoDeBloqueioService : IService<ConfiguracaoDeBloqueio>
    {
    }
}