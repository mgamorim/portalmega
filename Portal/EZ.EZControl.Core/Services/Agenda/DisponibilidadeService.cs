﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class DisponibilidadeService : EZControlDomainServiceBase<Disponibilidade>, IDisponibilidadeService
    {
        public DisponibilidadeService(IRepository<Disponibilidade, int> repository)
            : base(repository)
        {
        }
    }
}