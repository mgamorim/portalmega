﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using System.Collections.Generic;

namespace EZ.EZControl.Services.Agenda.Regra
{
    public class RegraBaseService : EZControlDomainServiceBase<RegraBase>, IRegraBaseService
    {
        public RegraBaseService(IRepository<RegraBase, int> repository)
            : base(repository)
        {

        }

        public List<TipoDeEventoEnum> GetTiposDeEventosBySistema(SistemaEnum sistema)
        {
            var result = new List<TipoDeEventoEnum>();

            if (SistemaEnum.Global == sistema)
            {
                result.Add(TipoDeEventoEnum.Tarefa);
                result.Add(TipoDeEventoEnum.Reuniao);
            }
            else if (SistemaEnum.EZMedical == sistema)
            {
                result.Add(TipoDeEventoEnum.Consulta);
            }
            else if (SistemaEnum.Agenda == sistema)
            {
                result.Add(TipoDeEventoEnum.Tarefa);
                result.Add(TipoDeEventoEnum.Reuniao);
            }


            return result;
        }

        public List<SistemaEnum> GetSistemasCriamEventos()
        {
            return new List<SistemaEnum>()
            {
                SistemaEnum.Global,
                SistemaEnum.EZMedical
            };
        }
    }
}