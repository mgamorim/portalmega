﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;

namespace EZ.EZControl.Services.Agenda.Regra
{
    public class RegraTarefaService : EZControlDomainServiceBase<RegraTarefa>, IRegraTarefaService
    {
        public RegraTarefaService(IRepository<RegraTarefa, int> repository)
            : base(repository)
        {

        }

        public bool CanCreateTarefa(RegraTarefa regraTarefa)
        {
            throw new System.NotImplementedException();
        }
    }
}