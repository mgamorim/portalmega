﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;

namespace EZ.EZControl.Services.Agenda.Regra
{
    public class RegraConsultaService : EZControlDomainServiceBase<RegraConsulta>, IRegraConsultaService
    {
        public RegraConsultaService(IRepository<RegraConsulta, int> repository)
            : base(repository)
        {
                
        }
    }
}