﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;

namespace EZ.EZControl.Services.Agenda.Regra
{
    public class RegraReuniaoService : EZControlDomainServiceBase<RegraReuniao>, IRegraReuniaoService
    {
        public RegraReuniaoService(IRepository<RegraReuniao, int> repository)
            : base(repository)
        {
                
        }
    }
}