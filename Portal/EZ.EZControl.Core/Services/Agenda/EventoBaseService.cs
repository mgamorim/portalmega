﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoBaseService : EZControlDomainServiceBase<EventoBase>, IEventoBaseService
    {
        public EventoBaseService(IRepository<EventoBase, int> repository)
            : base(repository)
        {
        }
    }
}