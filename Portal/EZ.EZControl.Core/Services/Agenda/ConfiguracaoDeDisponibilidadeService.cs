﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class ConfiguracaoDeDisponibilidadeService : EZControlDomainServiceBase<ConfiguracaoDeDisponibilidade>, IConfiguracaoDeDisponibilidadeService
    {
        public ConfiguracaoDeDisponibilidadeService(IRepository<ConfiguracaoDeDisponibilidade, int> repository)
            : base(repository)
        {
        }
    }
}