﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoTarefaService : EZControlDomainServiceBase<EventoTarefa>, IEventoTarefaService
    {
        public EventoTarefaService(IRepository<EventoTarefa, int> repository)
            : base(repository)
        {
        }
    }
}