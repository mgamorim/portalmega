﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    public class ConfiguracaoDeBloqueioService : EZControlDomainServiceBase<ConfiguracaoDeBloqueio>, IConfiguracaoDeBloqueioService
    {
        public ConfiguracaoDeBloqueioService(IRepository<ConfiguracaoDeBloqueio, int> repository)
            : base(repository)
        {
        }
    }
}