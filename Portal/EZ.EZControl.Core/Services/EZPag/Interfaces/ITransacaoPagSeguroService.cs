﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Global.SubtiposPessoas;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;
using Uol.PagSeguro.Domain.Direct;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface ITransacaoPagSeguroService : IService<Transacao>
    {
        Task<Checkout> MapFromEzInput(EZPagCheckout EzCheckout, ClienteService ClienteService);
        string TraduzErroPagSeguro(int Codigo);
    }
}
