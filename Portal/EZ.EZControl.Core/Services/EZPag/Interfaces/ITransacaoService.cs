﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Interfaces;
using Uol.PagSeguro.Enums;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface ITransacaoService : IService<Transacao>
    {
        StatusDoPagamentoEnum TraduzirStatusPagSeguro(TransactionStatus statusPagSeguro);
    }
}
