﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IDepositoTransferenciaBancariaService : IService<DepositoTransferenciaBancaria>
    {
        DepositoTransferenciaBancaria GetDepositoBancarioById(int Id);
        DepositoTransferenciaBancaria GetDepositoBancarioByPessoaJuridicaId(int Id);
        DepositoTransferenciaBancaria GetDepositoBancarioByEmpresaId(int Id);
        Task<DepositoTransferenciaBancaria> SaveDepositoBancario(DepositoTransferenciaBancaria depositoTransferenciaBancaria);
    }
}
