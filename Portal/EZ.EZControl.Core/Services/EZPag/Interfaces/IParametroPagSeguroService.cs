﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IParametroPagSeguroService : IService<ParametroPagSeguro>
    {
        ParametroPagSeguro GetParametroByPessoaJuridicaId(int Id);
        Task<ParametroPagSeguro> SaveParametroPagSeguro(ParametroPagSeguro parametroPagSeguro);
    }
}
