﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IParametroEzpagService: IParametroBaseService<ParametroEzpag>
    {
        void EstaEmAmbienteDeTeste(ParametroEzpag param);
    }
}
