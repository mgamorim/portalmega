﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IDepositoService : IService<Deposito>
    {
    }
}
