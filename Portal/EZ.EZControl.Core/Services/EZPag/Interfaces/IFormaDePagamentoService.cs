﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IFormaDePagamentoService: IService<FormaDePagamento>
    {
        IQueryable<FormaDePagamento> GetAllFormasDePagamentos(Int32 id);
    }
}
