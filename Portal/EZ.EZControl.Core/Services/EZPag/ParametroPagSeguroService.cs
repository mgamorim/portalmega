﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag
{
    public class ParametroPagSeguroService : EZControlDomainServiceBase<ParametroPagSeguro>, IParametroPagSeguroService
    {
        public ParametroPagSeguroService(IRepository<ParametroPagSeguro, int> repository) : base(repository)
        {
        }

        public ParametroPagSeguro GetParametroByPessoaJuridicaId(int Id)
        {
            return this.GetAll().FirstOrDefault(x => x.PessoaJuridicaId == Id);
        }

        public async Task<ParametroPagSeguro> SaveParametroPagSeguro(ParametroPagSeguro parametroPagSeguro)
        {
            SetEmpresaInEntity(parametroPagSeguro);
            return await CreateOrUpdateAndReturnSavedEntity(parametroPagSeguro);
        }
    }
}
