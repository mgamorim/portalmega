﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Services.EZPag
{
    public class TransacaoService : EZControlDomainServiceBase<Transacao>, ITransacaoService
    {
        public TransacaoService(IRepository<Transacao, int> repository) : base(repository)
        {
        }
        public StatusDoPagamentoEnum TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus statusPagSeguro)
        {
            StatusDoPagamentoEnum retorno = StatusDoPagamentoEnum.Iniciado;
            if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.Initiated)
                retorno = StatusDoPagamentoEnum.Iniciado;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.WaitingPayment)
                retorno = StatusDoPagamentoEnum.AguardandoPagamento;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.InAnalysis)
                retorno = StatusDoPagamentoEnum.EmAnalise;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.Paid)
                retorno = StatusDoPagamentoEnum.Pago;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.Available)
                retorno = StatusDoPagamentoEnum.Disponivel;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.InDispute)
                retorno = StatusDoPagamentoEnum.EmDisputa;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.Refunded)
                retorno = StatusDoPagamentoEnum.Reembolsado;
            else if (statusPagSeguro == Uol.PagSeguro.Enums.TransactionStatus.Cancelled)
                retorno = StatusDoPagamentoEnum.Cancelado;
            return retorno;
        }
    }
}
