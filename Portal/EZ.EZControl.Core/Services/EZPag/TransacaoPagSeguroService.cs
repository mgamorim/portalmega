﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Uol.PagSeguro.Constants;
using Uol.PagSeguro.Domain;
using Uol.PagSeguro.Domain.Direct;

namespace EZ.EZControl.Services.EZPag
{
    public class TransacaoPagSeguroService : EZControlDomainServiceBase<Transacao>, ITransacaoPagSeguroService
    {
        protected readonly Checkout checkoutCommonInfo = new Checkout();
        protected ClienteService _clienteService;
        protected Cliente _cliente;
        protected IPedidoService _pedidoService;
        public TransacaoPagSeguroService(IRepository<Transacao, int> repository, IPedidoService pedidoService) : base(repository)
        {
            _pedidoService = pedidoService;
        }

        public async Task<Checkout> MapFromEzInput(EZPagCheckout EzCheckout, ClienteService ClienteService)
        {
            _clienteService = ClienteService;
            await GetCliente(EzCheckout);
            CreateCommonCheckoutInfo(EzCheckout);
            switch (EzCheckout.Tipo)
            {
                case TipoCheckoutPagSeguroEnum.BOLETO:
                    {
                        var checkout = new BoletoCheckout();

                        checkout.PaymentMode = checkoutCommonInfo.PaymentMode;
                        checkout.Currency = checkoutCommonInfo.Currency;
                        checkout.ReceiverEmail = checkoutCommonInfo.ReceiverEmail;
                        checkout.Reference = checkoutCommonInfo.Reference;
                        checkout.Shipping = checkoutCommonInfo.Shipping;
                        checkout.Sender = checkoutCommonInfo.Sender;

                        await CreateItemsInfo(EzCheckout);
                        foreach (var item in checkoutCommonInfo.Items)
                        {
                            checkout.Items.Add(item);
                        }

                        return checkout;
                    }
                case TipoCheckoutPagSeguroEnum.ONLINE_DEBIT:
                    {
                        var checkout = new OnlineDebitCheckout();

                        checkout.PaymentMode = checkoutCommonInfo.PaymentMode;
                        checkout.Currency = checkoutCommonInfo.Currency;
                        checkout.ReceiverEmail = checkoutCommonInfo.ReceiverEmail;
                        checkout.Reference = checkoutCommonInfo.Reference;
                        checkout.Shipping = checkoutCommonInfo.Shipping;
                        checkout.Sender = checkoutCommonInfo.Sender;

                        await CreateItemsInfo(EzCheckout);
                        foreach (var item in checkoutCommonInfo.Items)
                        {
                            checkout.Items.Add(item);
                        }

                        checkout.BankName = EzCheckout.NomeDoBanco.ToLower();

                        return checkout;
                    }
                case TipoCheckoutPagSeguroEnum.CREDIT_CARD:
                    {
                        var checkout = new CreditCardCheckout();

                        checkout.PaymentMode = checkoutCommonInfo.PaymentMode;
                        checkout.Currency = checkoutCommonInfo.Currency;
                        checkout.ReceiverEmail = checkoutCommonInfo.ReceiverEmail;
                        checkout.Reference = checkoutCommonInfo.Reference;
                        checkout.Shipping = checkoutCommonInfo.Shipping;
                        checkout.Sender = checkoutCommonInfo.Sender;

                        await CreateItemsInfo(EzCheckout);
                        foreach (var item in checkoutCommonInfo.Items)
                        {
                            checkout.Items.Add(item);
                        }

                        checkout.Billing = CreateBillingInfo(EzCheckout);
                        checkout.Holder = CreateHolderInfo(EzCheckout);
                        checkout.Token = EzCheckout.TokenCartao;
                        checkout.Installment = CreateInstallmentInfo(EzCheckout);

                        return checkout;
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        private Installment CreateInstallmentInfo(EZPagCheckout ezCheckout)
        {
            Installment installment = new Installment();

            installment.Quantity = ezCheckout.OpcaoDeParcelamento.NumeroDeParcelas;
            installment.Value = ezCheckout.OpcaoDeParcelamento.ValorDaParcela;
            installment.NoInterestInstallmentQuantity = ezCheckout.OpcaoDeParcelamento.NumeroDeParcelasSemJuros;

            return installment;
        }

        private Holder CreateHolderInfo(EZPagCheckout ezCheckout)
        {
            Holder holder = new Holder();

            holder.Name = ezCheckout.Holder.Nome;
            holder.Birthdate = ezCheckout.Holder.Nascimento.ToShortDateString();
            holder.Phone = new Phone(ezCheckout.Holder.Telefone.Substring(0, 2), ezCheckout.Holder.Telefone.Substring(2));
            holder.Document = new HolderDocument(Documents.GetDocumentByType("CPF"), ezCheckout.Holder.CPF);

            return holder;
        }

        private Billing CreateBillingInfo(EZPagCheckout ezCheckout)
        {
            Billing billing = new Billing();

            billing.Address = new Address(
                _cliente.Pessoa.EnderecoPrincipal.Cidade.Estado.Pais.Sigla3Caracteres,
                _cliente.Pessoa.EnderecoPrincipal.Cidade.Estado.Sigla,
                _cliente.Pessoa.EnderecoPrincipal.Cidade.Nome,
                _cliente.Pessoa.EnderecoPrincipal.Bairro,
                _cliente.Pessoa.EnderecoPrincipal.CEP.Replace("-", ""),
                _cliente.Pessoa.EnderecoPrincipal.LogradouroComTipo,
                _cliente.Pessoa.EnderecoPrincipal.Numero,
                _cliente.Pessoa.EnderecoPrincipal.Complemento
            );

            return billing;
        }

        private async Task GetCliente(EZPagCheckout ezCheckout)
        {
            _cliente = await _clienteService.GetById(ezCheckout.Pedido.ClienteId);
        }

        private void CreateCommonCheckoutInfo(EZPagCheckout EzCheckout)
        {
            checkoutCommonInfo.PaymentMode = PaymentMode.DEFAULT;
            checkoutCommonInfo.Currency = Currency.Brl;
            checkoutCommonInfo.ReceiverEmail = EzCheckout.EmailVendedor;
            checkoutCommonInfo.Reference = "EZLiv_" + EzCheckout.Pedido.Id;
            CreateShippingInfo(EzCheckout);
            CreateSenderInfo(EzCheckout);
        }

        private async Task CreateItemsInfo(EZPagCheckout ezCheckout)
        {
            var pedido = await _pedidoService.GetById(ezCheckout.Pedido.Id);
            foreach (var item in pedido.ItensDePedidoReadOnly)
            {
                var codigo = item.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto ? item.Produto.Id.ToString() : item.Servico.Id.ToString();
                var descricao = item.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto ? item.Produto.Nome.ToString() : item.Servico.Nome.ToString();

                //checkoutCommonInfo.Items.Add(new Item(codigo, descricao, Decimal.ToInt32(item.Quantidade), (item.Valor + item.TaxaCadastro) ));
                //checkoutCommonInfo.Items.Add(new Item(codigo, descricao, Decimal.ToInt32(item.Quantidade), item.Valor  ));
                //checkoutCommonInfo.Items.Add(new Item("1b", "TAXA DE CADASTRO", Decimal.ToInt32(item.Quantidade), Convert.ToDecimal(25.00) ));
            }
        }

        private void CreateSenderInfo(EZPagCheckout ezCheckout)
        {
            var telefoneContato = _cliente.Pessoa.Telefones.Where(x => x.Tipo == TipoTelefoneEnum.Celular).FirstOrDefault();
            checkoutCommonInfo.Sender = new Sender(
                _cliente.NomePessoa,
                _cliente.Pessoa.EmailPrincipal.Endereco,
                new Phone(telefoneContato.DDD, telefoneContato.Numero)
            );
            checkoutCommonInfo.Sender.Hash = ezCheckout.SenderHash;
            var cpf = _cliente.Pessoa.Documentos.Where(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf).FirstOrDefault();
            SenderDocument senderCPF = new SenderDocument(Documents.GetDocumentByType("CPF"), cpf.Numero.Replace(".", "").Replace("-", ""));
            checkoutCommonInfo.Sender.Documents.Add(senderCPF);
        }

        private void CreateShippingInfo(EZPagCheckout ezCheckout)
        {
            checkoutCommonInfo.Shipping = new Shipping();

            checkoutCommonInfo.Shipping.ShippingType = ShippingType.NotSpecified;
            checkoutCommonInfo.Shipping.Cost = 0.00m;

            if (_cliente.Pessoa.EnderecoPrincipal == null)
            {
                checkoutCommonInfo.Shipping.Address = new Address(
                    _cliente.Pessoa.Enderecos.First().Cidade.Estado.Pais.Sigla3Caracteres,
                    _cliente.Pessoa.Enderecos.First().Cidade.Estado.Sigla,
                    _cliente.Pessoa.Enderecos.First().Cidade.Nome,
                    _cliente.Pessoa.Enderecos.First().Bairro,
                    _cliente.Pessoa.Enderecos.First().CEP.Replace("-", ""),
                    _cliente.Pessoa.Enderecos.First().LogradouroComTipo,
                    _cliente.Pessoa.Enderecos.First().Numero,
                    _cliente.Pessoa.Enderecos.First().Complemento
                );
            }
            else
            {
                checkoutCommonInfo.Shipping.Address = new Address(
                    _cliente.Pessoa.EnderecoPrincipal.Cidade.Estado.Pais.Sigla3Caracteres,
                    _cliente.Pessoa.EnderecoPrincipal.Cidade.Estado.Sigla,
                    _cliente.Pessoa.EnderecoPrincipal.Cidade.Nome,
                    _cliente.Pessoa.EnderecoPrincipal.Bairro,
                    _cliente.Pessoa.EnderecoPrincipal.CEP.Replace("-", ""),
                    _cliente.Pessoa.EnderecoPrincipal.LogradouroComTipo,
                    _cliente.Pessoa.EnderecoPrincipal.Numero,
                    _cliente.Pessoa.EnderecoPrincipal.Complemento
                );
            }


        }

        public string TraduzErroPagSeguro(int Codigo)
        {
            switch (Codigo)
            {
                case 5003: { return "Falha de comunicação com a instituição financeira."; }
                case 10000: { return "Bandeira do cartão inválida."; }
                case 10001: { return "Tamanho do número do cartão inválido."; }
                case 10002: { return "Formato de data inválido."; }
                case 10003: { return "Campo de segurança inválido."; }
                case 10004: { return "CVV é obrigatório."; }
                case 10006: { return "Tamanho do campo de segurança inválido."; }
                case 53004: { return "Quantidade de itens inválida."; }
                case 53005: { return "Moeda é obrigatória."; }
                case 53006: { return "Valor inválido para moeda"; }
                case 53007: { return "Tamanho de referência inválido"; }
                case 53008: { return "Tamanho da URL de notificação inválido."; }
                case 53009: { return "URL de notificação inválida."; }
                case 53010: { return "Email do remetente obrigatório."; }
                case 53011: { return "Tamanho do email do remetente inválido"; }
                case 53012: { return "Email do remetente inválido"; }
                case 53013: { return "Nome do remetente obrigatório"; }
                case 53014: { return "Tamanho do nome do remetente inválido"; }
                case 53015: { return "Nome do remetente inválido"; }
                case 53017: { return "CPF inválido"; }
                case 53018: { return "Código de área do remetente obrigatório"; }
                case 53019: { return "Código de área do remetente inválido"; }
                case 53020: { return "Telefone do remetente obrigatório"; }
                case 53021: { return "Telefone do remetente inválido"; }
                case 53022: { return "Código postal do endereço de entrega é obrigatório"; }
                case 53023: { return "Código postal do endereço de entrega inválido"; }
                case 53024: { return "Logradouro do endereço de entrega é obrigatório."; }
                case 53025: { return "Logradouro do endereço de entrega inválido"; }
                case 53026: { return "Número do endereço de entrega é obrigatório."; }
                case 53027: { return "Número do endereço de entrega inválido"; }
                case 53028: { return "Tamanho inválido para complementoo do endereço de entrega"; }
                case 53029: { return "Bairro do endereço de entrega obrigatório"; }
                case 53030: { return "Tamanho do bairro do endereço de entrega inválido"; }
                case 53031: { return "Cidade do endereço de entrega obrigatório"; }
                case 53032: { return "Tamanho da cidade do endereço de entrega inválido"; }
                case 53033: { return "Estado do endereço de entrega obrigatório"; }
                case 53034: { return "Estado do endereço de entrega inválido"; }
                case 53035: { return "País do endereço de entrega obrigatório"; }
                case 53036: { return "Tamanho do país do endereço de entrega inválido"; }
                case 53037: { return "Token do cartão de crédito obrigatório"; }
                case 53038: { return "Número de parcelas obrigatório"; }
                case 53039: { return "Quantidade de parcelas inválida"; }
                case 53040: { return "Valor da parcela obrigatório"; }
                case 53041: { return "Valor da parcela inválido"; }
                case 53042: { return "Nome do titular do cartão obrigatório"; }
                case 53043: { return "Tamanho do nome do titular do cartão inválido"; }
                case 53044: { return "Valor do nome do titular do cartão de crédito inválido"; }
                case 53045: { return "CPF do titular do cartão de crédito obrigatório"; }
                case 53046: { return "Valor do cpf do titular do cartão de crédito inválido"; }
                case 53047: { return "Nascimento do titular do cartão de crédito obrigatório"; }
                case 53048: { return "Valor do nascimento do titular do cartão de crédito inválido"; }
                case 53049: { return "Código de área do titular do cartão de crédito obrigatório"; }
                case 53050: { return "Valor do código de área do titular do cartão de crédito inválido"; }
                case 53051: { return "Telefone do titular do cartão de crédito obrigatório"; }
                case 53052: { return "Valor do telefone do titular do cartão de crédito inválido"; }
                case 53053: { return "Código postal do endereço de cobrança obrigatório"; }
                case 53054: { return "Valor do código postal do endereço de cobrança inválido"; }
                case 53055: { return "Logradouro do endereço de cobrança obrigatório."; }
                case 53056: { return "Valor do logradouro do endereço de cobrança inválido"; }
                case 53057: { return "Número do endereço de cobrança obrigátório"; }
                case 53058: { return "Número do endereço de cobrança invalid length: {0}"; }
                case 53059: { return "Complemento do endereço de cobrança complemento invalid length: {0}"; }
                case 53060: { return "Bairro do endereço de cobrança obrigatório"; }
                case 53061: { return "Bairro do endereço de cobrança inválido"; }
                case 53062: { return "Cidade do endereço de cobrança obrigatório"; }
                case 53063: { return "Cidade do endereço de cobrança inválido"; }
                case 53064: { return "Estado do endereço de cobrança obrigatório"; }
                case 53065: { return "Estado do endereço de cobrança inválido"; }
                case 53066: { return "País do endereço de cobrança obrigatório"; }
                case 53067: { return "País do endereço de cobrança inválido"; }
                case 53068: { return "Tamanho do email do destinatário inválido"; }
                case 53069: { return "Email do destinatário inválido"; }
                case 53070: { return "Id do item obrigatório"; }
                case 53071: { return "Tamanho do id do item inválido"; }
                case 53072: { return "Descrição do item obrigatória"; }
                case 53073: { return "Tamanho da descrição do item inválido"; }
                case 53074: { return "Quantidade do item obrigatória"; }
                case 53075: { return "Quantidade do item fora dos limites"; }
                case 53076: { return "Quantidade do item inválida"; }
                case 53077: { return "Valor do item obrigatório"; }
                case 53078: { return "Valor do item fora de padrão"; }
                case 53079: { return "Valor do item fora dos limites"; }
                case 53081: { return "Remetente relacionado ao destinatário"; }
                case 53084: { return "Destinatário inválido. Verifique a conta do destinatário e se é uma conta de vendedor"; }
                case 53085: { return "Método de pagamento indisponível"; }
                case 53086: { return "Valor total do carrinho fora dos limites"; }
                case 53087: { return "Dados do cartão de crédito inválidos"; }
                case 53091: { return "Sender Hash inválida"; }
                case 53092: { return "Bandeira do cartão de crédito não é aceita"; }
                case 53095: { return "Padrão inválido para tipo de frete"; }
                case 53096: { return "Padrão inválido para custo de frete"; }
                case 53097: { return "Custo de frete fora dos limites"; }
                case 53098: { return "Valor total do carrinho é negativo"; }
                case 53099: { return "Padrão inválido para valor extra"; }
                case 53101: { return "Valor inválido para modo de pagamento. Valores válidos: padrão e gateway."; }
                case 53102: { return "Valor inválido para forma de pagamento. Valores válidos: Cartão de crédito, boleto e transferência eletrônica de fundos"; }
                case 53104: { return "Custo do frete informado, endereço deve estar completo."; }
                case 53105: { return "Informação do rementente fornecida, email deve ser preenchido."; }
                case 53106: { return "Titular do cartão incompleto."; }
                case 53109: { return "Endereço de entrega foi informado, email do remetente deve ser preenchido."; }
                case 53110: { return "Banco da transferência eletrônica de fundos é obrigatório"; }
                case 53111: { return "Banco da transferência eletrônica de fundos não aceito"; }
                case 53115: { return "Data de nascimento do remetente inválida"; }
                case 53117: { return "CPF do remetente inválido"; }
                case 53122: { return "Domínio inválido para email do remetente. Use um email @sandbox.pagseguro.com.br"; }
                case 53140: { return "Número de parcelas fora dos limites. Valor deve ser maior que zero."; }
                case 53141: { return "Remetente bloqueado"; }
                case 53142: { return "Token do cartão inválido"; }
                default:
                    {
                        return "Erro desconhecido.";
                    }
            }
        }
    }
}
