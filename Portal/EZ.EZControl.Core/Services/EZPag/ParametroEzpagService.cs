﻿
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Services.EZPag
{

    public class ParametroEzpagService: EZControlDomainServiceBase<ParametroEzpag>, IParametroEzpagService
    {
        public ParametroEzpagService(IRepository<ParametroEzpag, int> repository)
            : base(repository)
        {
        }
        public void EstaEmAmbienteDeTeste(ParametroEzpag parametro)
        {
            if (!parametro.IsTest)
                throw new UserFriendlyException(L("Parametro.AmbienteTesteInativo"));
        }
    }
}
