﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Services.EZPag
{
    public class HistoricoTransacaoService : EZControlDomainServiceBase<HistoricoTransacao>, IHistoricoTransacaoService
    {
        public HistoricoTransacaoService(IRepository<HistoricoTransacao, int> repository) : base(repository)
        {
        }
    }
}
