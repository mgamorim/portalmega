﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag
{
    public class FormaDePagamentoService : EZControlDomainServiceBase<FormaDePagamento>, IFormaDePagamentoService
    {
        public FormaDePagamentoService(IRepository<FormaDePagamento, int> repository) : base(repository)
        {
        }

        public IQueryable<FormaDePagamento> GetAllFormasDePagamentos(Int32 id)
        {
            //ClaimsPrincipal principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            //ClaimsIdentity identity = principal.Identity as ClaimsIdentity;
            //var empresaClaim = identity.FindFirst("EmpresaId");
            //var empresaId = Convert.ToInt32(empresaClaim.Value);


           
            var result = GetAll().Where(f => f.EmpresaId == id);
            return result;
        }




    }
}
