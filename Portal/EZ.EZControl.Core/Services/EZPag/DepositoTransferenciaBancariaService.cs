﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag
{
    public class DepositoTransferenciaBancariaService : EZControlDomainServiceBase<DepositoTransferenciaBancaria>, IDepositoTransferenciaBancariaService
    {
        public DepositoTransferenciaBancariaService(IRepository<DepositoTransferenciaBancaria, int> repository) : base(repository)
        {
        }

        public DepositoTransferenciaBancaria GetDepositoBancarioById(int Id)
        {
            return this.GetAll().FirstOrDefault(x => x.Id == Id);
        }

        public async Task<DepositoTransferenciaBancaria> SaveDepositoBancario(DepositoTransferenciaBancaria depositoTransferenciaBancaria)
        {
            SetEmpresaInEntity(depositoTransferenciaBancaria);
            return await CreateOrUpdateAndReturnSavedEntity(depositoTransferenciaBancaria);
        }

        public DepositoTransferenciaBancaria GetDepositoBancarioByPessoaJuridicaId(int Id)
        {            
            return this.GetAll().FirstOrDefault(x => x.PessoaJuridicaId == Id);
        }

        public DepositoTransferenciaBancaria GetDepositoBancarioByEmpresaId(int Id)
        {
            return this.GetAll().FirstOrDefault(x => x.EmpresaId == Id);
        }

        public async Task<DepositoTransferenciaBancaria> GetById(int Id)
        {
            return await this.GetById(Id);
        }
    }
}
