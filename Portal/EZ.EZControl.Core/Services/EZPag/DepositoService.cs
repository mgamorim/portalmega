﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Services.EZPag
{
    public class DepositoService : EZControlDomainServiceBase<Deposito>, IDepositoService
    {
        public DepositoService(IRepository<Deposito, int> repository) : base(repository)
        {
        }
    }
}
