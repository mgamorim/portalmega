﻿using System;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface IFracaoService<TFracao, TPrimaryKey> : IService<TFracao, TPrimaryKey>
        where TFracao : Domain.Estoque.Geral.Fracao, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignUnidadeMedida(TFracao fracao, UnidadeMedida unidadeMedida);
    }

    public interface IFracaoService<TFracao> : IFracaoService<TFracao, int>
    where TFracao : Domain.Estoque.Geral.Fracao
    {
    }

    public interface IFracaoService : IFracaoService<Domain.Estoque.Geral.Fracao>
    {   
    }
}
