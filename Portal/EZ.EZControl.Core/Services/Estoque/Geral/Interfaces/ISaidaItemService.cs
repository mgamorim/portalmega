﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface ISaidaItemService<TSaidaItem, TPrimaryKey> : IService<TSaidaItem, TPrimaryKey>
        where TSaidaItem : Domain.Estoque.Geral.SaidaItem, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignProduto(TSaidaItem saidaItem, Produto produto);
        void CanAssignSaida(TSaidaItem saidaItem, Saida saidaEstoque);
        void CanAssignLocalArmazenamento(TSaidaItem saidaItem, LocalArmazenamento localArmazenamento);
    }

    public interface ISaidaItemService<TSaidaItem> : ISaidaItemService<TSaidaItem, int>
    where TSaidaItem : Domain.Estoque.Geral.SaidaItem
    {
    }
    public interface ISaidaItemService : ISaidaItemService<Domain.Estoque.Geral.SaidaItem>
    {
        
    }
}
