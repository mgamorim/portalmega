﻿using System;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;           

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface IEntradaItemService<TEntradaItem, TPrimaryKey> : IService<TEntradaItem, TPrimaryKey>
        where TEntradaItem : Domain.Estoque.Geral.EntradaItem, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignProduto(TEntradaItem entradaItem, Produto produto);
        void CanAssignEntrada(TEntradaItem entradaItem, Entrada entradaEstoque);
        void CanAssignLocalArmazenamento(TEntradaItem entrada, LocalArmazenamento localArmazenamento);
    }

    public interface IEntradaItemService<TEntradaItem> : IEntradaItemService<TEntradaItem, int>
    where TEntradaItem : Domain.Estoque.Geral.EntradaItem
    {
    }
    public interface IEntradaItemService : IEntradaItemService<Domain.Estoque.Geral.EntradaItem>
    {
        
    }
}
