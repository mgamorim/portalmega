﻿using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface IParametroEstoqueService : IParametroBaseService<ParametroEstoque>
    {
    }
}