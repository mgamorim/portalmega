﻿using System;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;
using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface IEntradaService<TEntrada, TPrimaryKey> : IService<TEntrada, TPrimaryKey>
        where TEntrada : Domain.Estoque.Geral.Entrada, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignEmpresa(TEntrada entrada, Empresa empresa);
        void CanAssignFornecedor(TEntrada entrada, Fornecedor fornecedor);
    }

    public interface IEntradaService<TEntrada> : IEntradaService<TEntrada, int>
    where TEntrada : Domain.Estoque.Geral.Entrada
    {
    }

    public interface IEntradaService : IEntradaService<Domain.Estoque.Geral.Entrada>
    {
    }
}
