﻿using System;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Threading.Tasks;
using EZ.EZControl.Domain.Vendas.Pedidos;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface ISaldoEstoqueService<TSaldoEstoque, TPrimaryKey> : IService<TSaldoEstoque, TPrimaryKey>
        where TSaldoEstoque : Domain.Estoque.Geral.SaldoEstoque, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignEmpresa(TSaldoEstoque saldoEstoque, Empresa empresa);
        void CanAssignLocalArmazenamento(TSaldoEstoque saldoEstoque, LocalArmazenamento localArmazenamento);
        void CanAssignProduto(TSaldoEstoque saldoEstoque, Produto produto);
        void CanAssignEntrada(TSaldoEstoque saldoEstoque, Entrada entrada);
        void CanAssignSaida(TSaldoEstoque saldoEstoque, Saida saida);
        void CanAssignPedido(TSaldoEstoque saldoEstoque, Pedido pedido);
        void CanAtualizarSaida(TSaldoEstoque saldoEstoque);
        Task<int> AtualizarSaida(TSaldoEstoque saldoEstoque);
        Task<int> EstornarSaida(TSaldoEstoque saldoEstoque);
        Task RemoverSaldoPorSaida(int saidaId);
        void CanAtualizarEntrada(TSaldoEstoque saldoEstoque);
        Task<int> AtualizarEntrada(TSaldoEstoque saldoEstoque);
        Task<int> EstornarEntrada(TSaldoEstoque saldoEstoque);
        Task RemoverSaldoPorEntrada(int entradaId);
        void CanAtualizarPedido(TSaldoEstoque saldoEstoque);
        Task<int> AtualizarPedido(TSaldoEstoque saldoEstoque);
        Task<int> EstornarPedido(TSaldoEstoque saldoEstoque);
        Task RemoverSaldoPorPedido(int pedidoId);
        Task<decimal> GetSaldoProduto(TSaldoEstoque saldoEstoque);
        Task<decimal> GetSaldoProdutoPorLocalArmazenamento(TSaldoEstoque saldoEstoque);
        Task ConsolidarSaldo(TSaldoEstoque saldoEstoque);
    }

    public interface ISaldoEstoqueService<TSaldoEstoque> : ISaldoEstoqueService<TSaldoEstoque, int>
    where TSaldoEstoque : Domain.Estoque.Geral.SaldoEstoque
    {
    }

    public interface ISaldoEstoqueService : ISaldoEstoqueService<Domain.Estoque.Geral.SaldoEstoque>
    {
    }
}
