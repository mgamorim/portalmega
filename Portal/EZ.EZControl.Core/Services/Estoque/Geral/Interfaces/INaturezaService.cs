﻿using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface INaturezaService : IService<Natureza>
    {
    }
}
