﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface ILocalArmazenamentoService<TLocalArmazenamento, TPrimaryKey> : IService<TLocalArmazenamento, TPrimaryKey>
        where TLocalArmazenamento : Domain.Estoque.Geral.LocalArmazenamento, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignEmpresa(TLocalArmazenamento localArmazenamento, Empresa empresa);
    }

    public interface ILocalArmazenamentoService<TLocalArmazenamento> : ILocalArmazenamentoService<TLocalArmazenamento, int>
    where TLocalArmazenamento : Domain.Estoque.Geral.LocalArmazenamento
    {
    }

    public interface ILocalArmazenamentoService : ILocalArmazenamentoService<Domain.Estoque.Geral.LocalArmazenamento>
    {
    }
}
