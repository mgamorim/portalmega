﻿using System;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface IProdutoService<TProduto, TPrimaryKey> : IService<TProduto, TPrimaryKey>
        where TProduto : Domain.Estoque.Geral.Produto, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignUnidadeMedida(TProduto produto, UnidadeMedida unidadeMedida);
        void CanAssignNatureza(TProduto produto, Natureza natureza);
    }

    public interface IProdutoService<TProduto> : IProdutoService<TProduto, int>
    where TProduto : Domain.Estoque.Geral.Produto
    {
    }

    public interface IProdutoService : IProdutoService<Domain.Estoque.Geral.Produto>
    {
    }
}
