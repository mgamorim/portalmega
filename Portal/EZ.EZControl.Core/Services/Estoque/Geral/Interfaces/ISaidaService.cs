﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface ISaidaService<TSaida, TPrimaryKey> : IService<TSaida, TPrimaryKey>
        where TSaida : Domain.Estoque.Geral.Saida, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        void CanAssignEmpresa(TSaida saida, Empresa empresa);
        void CanAssignPedido(TSaida saidaItem, Pedido pedido);
    }

    public interface ISaidaService<TSaida> : ISaidaService<TSaida, int>
    where TSaida : Domain.Estoque.Geral.Saida
    {
    }

    public interface ISaidaService : ISaidaService<Domain.Estoque.Geral.Saida>
    {
    }
}
