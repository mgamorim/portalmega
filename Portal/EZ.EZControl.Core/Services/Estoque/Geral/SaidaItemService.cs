﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{        
    public class SaidaItemService<TSaidaItem> : EZControlDomainServiceBase<TSaidaItem>, ISaidaItemService<TSaidaItem>
        where TSaidaItem : Domain.Estoque.Geral.SaidaItem
    {   
        public SaidaItemService(IRepository<TSaidaItem, int> repository) 
            : base(repository)
        {
            ValidateEntity += SaidaItemService_ValidateEntity;
            BeforeSaveEntity += SaidaItemService_BeforeSaveEntity;
        }
        private void SaidaItemService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TSaidaItem, int> e)
        {
        }

        private void SaidaItemService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TSaidaItem, int> e)
        {
        }

        public void CanAssignProduto(TSaidaItem saidaItem, Produto produto)
        {
            if (!produto.IsActive)
                throw new UserFriendlyException(L("Estoque.ProdutoInativo"));                
        }

        public void CanAssignSaida(TSaidaItem saidaItem, Saida saidaEstoque)
        {
            if (!saidaEstoque.IsActive)
                throw new UserFriendlyException(L("Estoque.SaidaInativo"));
        }

        public void CanAssignLocalArmazenamento(TSaidaItem saidaItem, LocalArmazenamento localArmazenamento)
        {
            if (!localArmazenamento.IsActive)
                throw new UserFriendlyException(L("Estoque.LocalArmazenamentoInativo"));
        }
    }

    public class SaidaItemService : SaidaItemService<SaidaItem>, ISaidaItemService
    {
        public SaidaItemService(IRepository<SaidaItem, int> repository)
            : base(repository)
        {
        }
    }
}
