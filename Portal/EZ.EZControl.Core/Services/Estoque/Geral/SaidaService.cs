﻿using System;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Domain.Vendas.Pedidos;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class SaidaService<TSaida> : EZControlDomainServiceBase<TSaida>, ISaidaService<TSaida>
        where TSaida : Domain.Estoque.Geral.Saida
    {
        public SaidaService(IRepository<TSaida, int> repository)
            : base(repository)
        {
            ValidateEntity += SaidaService_ValidateEntity;
            BeforeSaveEntity += SaidaService_BeforeSaveEntity;
        }

        private void SaidaService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TSaida, int> e)
        {
        }

        private void SaidaService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TSaida, int> e)
        {
        }

        public void CanAssignEmpresa(TSaida saida, Empresa empresa)
        {
            if (!empresa.IsActive)
                throw new UserFriendlyException(L("Estoque.EmpresaInativo"));
        }

        public void CanAssignPedido(TSaida saidaItem, Pedido pedido)
        {
            if (pedido != null && !pedido.IsDeleted)
                throw new UserFriendlyException(L("Estoque.PedidoInativo"));
        }
    }

    public class SaidaService : SaidaService<Saida>, ISaidaService
    {   
        public SaidaService(IRepository<Saida, int> repository) 
            : base(repository)
        {
        }
    }
}
