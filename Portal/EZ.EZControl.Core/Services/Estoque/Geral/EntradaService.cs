﻿using System;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class EntradaService<TEntrada> : EZControlDomainServiceBase<TEntrada>, IEntradaService<TEntrada>
        where TEntrada : Domain.Estoque.Geral.Entrada
    {
        public EntradaService(IRepository<TEntrada, int> repository)
            : base(repository)
        {
            ValidateEntity += EntradaService_ValidateEntity;
            BeforeSaveEntity += EntradaService_BeforeSaveEntity;
        }

        private void EntradaService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TEntrada, int> e)
        {
        }

        private void EntradaService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TEntrada, int> e)
        {
        }

        public void CanAssignEmpresa(TEntrada entrada, Empresa empresa)
        {
            if (!empresa.IsActive)
                throw new UserFriendlyException(L("Estoque.EmpresaInativo"));
        }

        public void CanAssignFornecedor(TEntrada entrada, Fornecedor fornecedor)
        {
            if (!fornecedor.IsActive)
                throw new UserFriendlyException(L("Estoque.FornecedorInativo"));
        } 
    }

    public class EntradaService : EntradaService<Entrada>, IEntradaService
    {   
        public EntradaService(IRepository<Entrada, int> repository) 
            : base(repository)
        {
        }
    }
}
