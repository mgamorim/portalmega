﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class FracaoService<TFracao> : EZControlDomainServiceBase<TFracao>, IFracaoService<TFracao>
        where TFracao : Domain.Estoque.Geral.Fracao
    {                                                          
        public FracaoService(IRepository<TFracao, int> repository)
            : base(repository)
        {
            ValidateEntity += FracaoService_ValidateEntity;
            BeforeSaveEntity += FracaoService_BeforeSaveEntity;
        }

        private void FracaoService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TFracao, int> e)
        {
        }

        private void FracaoService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TFracao, int> e)
        {
        }

        public void CanAssignUnidadeMedida(TFracao fracao, UnidadeMedida unidadeMedida)
        {
            if (fracao == null)
                throw new UserFriendlyException(L("Estoque.Fracao.EmptyError"));

            if (fracao.UnidadeMedida != null && unidadeMedida != null && fracao.UnidadeMedida.Id == unidadeMedida.Id)
                throw new UserFriendlyException(L("Estoque.Fracao.EmptyUnidadeMedidaError"));
        }
    }

    public class FracaoService : FracaoService<Domain.Estoque.Geral.Fracao>, IFracaoService
    {
        public FracaoService(IRepository<Domain.Estoque.Geral.Fracao, int> repository)
            : base(repository)
        {
        }
    }
}
