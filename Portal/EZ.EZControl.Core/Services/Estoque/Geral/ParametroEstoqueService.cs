﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Core.Geral;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class ParametroEstoqueService : ParametroBaseService<ParametroEstoque>, IParametroEstoqueService
    {
        public ParametroEstoqueService(IRepository<ParametroEstoque, int> repository)
            : base(repository)
        {
        }
    }
}