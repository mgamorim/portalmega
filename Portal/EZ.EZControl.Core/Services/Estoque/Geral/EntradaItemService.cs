﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{        
    public class EntradaItemService<TEntradaItem> : EZControlDomainServiceBase<TEntradaItem>, IEntradaItemService<TEntradaItem>
        where TEntradaItem : Domain.Estoque.Geral.EntradaItem
    {   
        public EntradaItemService(IRepository<TEntradaItem, int> repository) 
            : base(repository)
        {
            ValidateEntity += EntradaItemService_ValidateEntity;
            BeforeSaveEntity += EntradaItemService_BeforeSaveEntity;
        }
        private void EntradaItemService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TEntradaItem, int> e)
        {
        }

        private void EntradaItemService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TEntradaItem, int> e)
        {
        }

        public void CanAssignProduto(TEntradaItem entradaItem, Produto produto)
        {
            if (!produto.IsActive)
                throw new UserFriendlyException(L("Estoque.ProdutoInativo"));                
        }

        public void CanAssignEntrada(TEntradaItem entradaItem, Entrada entradaEstoque)
        {
            if (!entradaEstoque.IsActive)
                throw new UserFriendlyException(L("Estoque.EntradaInativo"));
        }

        public void CanAssignLocalArmazenamento(TEntradaItem entradaItem, LocalArmazenamento localArmazenamento)
        {
            if (!localArmazenamento.IsActive)
                throw new UserFriendlyException(L("Estoque.LocalArmazenamentoInativo"));
        }
    }

    public class EntradaItemService : EntradaItemService<EntradaItem>, IEntradaItemService
    {
        public EntradaItemService(IRepository<EntradaItem, int> repository)
            : base(repository)
        {
        }
    }
}
