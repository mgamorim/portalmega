﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class ProdutoService<TProduto> : EZControlDomainServiceBase<TProduto>, IProdutoService<TProduto>
        where TProduto : Domain.Estoque.Geral.Produto
    {
        public ProdutoService(IRepository<TProduto, int> repository)
            : base(repository)
        {
            ValidateEntity += ProdutoService_ValidateEntity;
            BeforeSaveEntity += ProdutoService_BeforeSaveEntity;
        }

        private void ProdutoService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TProduto, int> e)
        {
        }

        private void ProdutoService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TProduto, int> e)
        {
        }

        public void CanAssignUnidadeMedida(TProduto produto, UnidadeMedida unidadeMedida)
        {
            if (produto == null)
                throw new UserFriendlyException(L("Produto.EmptyError"));

            if (unidadeMedida == null)
                throw new UserFriendlyException(L("Produto.EmptyUnidadeMedidaError"));
        }

        public void CanAssignNatureza(TProduto produto, Natureza natureza)
        {
            if (produto == null)
                throw new UserFriendlyException(L("Produto.EmptyError"));

            if (natureza == null)
                throw new UserFriendlyException(L("Produto.EmptyNaturezaError"));
        }
    }

    public class ProdutoService : ProdutoService<Produto>, IProdutoService
    {   
        public ProdutoService(IRepository<Produto, int> repository) 
            : base(repository)
        {
        }
    }
}
