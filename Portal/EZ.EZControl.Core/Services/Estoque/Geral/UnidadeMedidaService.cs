﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class UnidadeMedidaService : EZControlDomainServiceBase<UnidadeMedida>, IUnidadeMedidaService
    {
        public UnidadeMedidaService(IRepository<UnidadeMedida, int> repository) 
            : base(repository)
        {
        }
    }
}
