﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class NaturezaService : EZControlDomainServiceBase<Natureza>, INaturezaService
    {
        public NaturezaService(IRepository<Natureza, int> repository) 
            : base(repository)
        {
        }
    }
}
