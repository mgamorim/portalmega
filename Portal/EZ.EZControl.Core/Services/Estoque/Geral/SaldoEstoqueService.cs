﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System.Threading.Tasks;
using System;
using System.Linq;
using Abp.Collections.Extensions;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class SaldoEstoqueService<TSaldoEstoque> : EZControlDomainServiceBase<TSaldoEstoque>, ISaldoEstoqueService<TSaldoEstoque>
        where TSaldoEstoque : Domain.Estoque.Geral.SaldoEstoque
    {
        public SaldoEstoqueService(IRepository<TSaldoEstoque, int> repository)
            : base(repository)
        {
            ValidateEntity += SaldoEstoqueService_ValidateEntity;
            BeforeSaveEntity += SaldoEstoqueService_BeforeSaveEntity;
        }

        private void SaldoEstoqueService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TSaldoEstoque, int> e)
        {
        }

        private void SaldoEstoqueService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TSaldoEstoque, int> e)
        {
        }

        public void CanAssignEmpresa(TSaldoEstoque saldoEstoque, Empresa empresa)
        {
            if (empresa == null)
                throw new UserFriendlyException(L("Estoque.EmpresaEmpty"));
            if (!empresa.IsActive)
                throw new UserFriendlyException(L("Estoque.EmpresaInativo"));
        }

        public void CanAssignLocalArmazenamento(TSaldoEstoque saldoEstoque, LocalArmazenamento localArmazenamento)
        {
            if (localArmazenamento == null)
                throw new UserFriendlyException(L("Estoque.LocalArmazenamentoEmpty"));
            if (!localArmazenamento.IsActive)
                throw new UserFriendlyException(L("Estoque.LocalArmazenamentoInativo"));
        }

        public void CanAssignProduto(TSaldoEstoque saldoEstoque, Produto produto)
        {
            if (produto == null)
                throw new UserFriendlyException(L("Estoque.ProdutoEmpty"));
            if (!produto.IsActive)
                throw new UserFriendlyException(L("Estoque.ProdutoInativo"));
        }

        public void CanAssignEntrada(TSaldoEstoque saldoEstoque, Entrada entrada)
        {
            if (entrada == null)
                throw new UserFriendlyException(L("Estoque.EntradaEmpty"));
            if (!entrada.IsActive)
                throw new UserFriendlyException(L("Estoque.EntradaInativo"));
        }

        public void CanAssignSaida(TSaldoEstoque saldoEstoque, Saida saida)
        {
            if (saida == null)
                throw new UserFriendlyException(L("Estoque.SaidaEmpty"));
            if (!saida.IsActive)
                throw new UserFriendlyException(L("Estoque.SaidaInativo"));
        }

        public void CanAssignPedido(TSaldoEstoque saldoEstoque, Pedido pedido)
        {
            if (pedido == null)
                throw new UserFriendlyException(L("Estoque.PedidoEmpty"));
        }

        #region Saldo Por Saida
        public void CanAtualizarSaida(TSaldoEstoque saldoEstoque)
        {
            if (saldoEstoque.Empresa == null)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaEmpty"));
            }
            if (saldoEstoque.Produto == null)
            {
                throw new UserFriendlyException(L("Estoque.ProdutoEmpty"));
            }
            if (saldoEstoque.Saida == null)
            {
                throw new UserFriendlyException(L("Estoque.SaidaEmpty"));
            }
        }
        public async Task<int> AtualizarSaida(TSaldoEstoque saldoEstoque)
        {
            int id = default(int);
            try
            {
                decimal qtd = await GetSaldoPorSaida(saldoEstoque);
                saldoEstoque.Quantidade -= qtd;
                id = await Repository.InsertAndGetIdAsync(saldoEstoque);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return id;
        }
        public async Task<int> EstornarSaida(TSaldoEstoque saldoEstoque)
        {
            int id = default(int);
            try
            {
                decimal qtd = await GetSaldoPorSaida(saldoEstoque);
                saldoEstoque.Quantidade = qtd * -1;
                id = await Repository.InsertAndGetIdAsync(saldoEstoque);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return id;
        }
        private async Task<decimal> GetSaldoPorSaida(TSaldoEstoque saldoEstoque)
        {
            var resultado = Repository
            .GetAll()
            .Where(x =>
                x.Empresa.Id == saldoEstoque.EmpresaId
                && x.Produto.Id == saldoEstoque.ProdutoId
                && x.SaidaId == saldoEstoque.SaidaId.Value
            )
            .Select(x => x.Quantidade)
            .DefaultIfEmpty(0)
            .Sum();

            return resultado;
        }
        public async Task RemoverSaldoPorSaida(int saidaId)
        {
            Repository.Delete(x => x.SaidaId.Value == saidaId);
        }
        #endregion

        #region Saldo Por Entrada
        public void CanAtualizarEntrada(TSaldoEstoque saldoEstoque)
        {
            if (saldoEstoque.Empresa == null)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaEmpty"));
            }
            if (saldoEstoque.Produto == null)
            {
                throw new UserFriendlyException(L("Estoque.ProdutoEmpty"));
            }
            if (saldoEstoque.Entrada == null)
            {
                throw new UserFriendlyException(L("Estoque.EntradaEmpty"));
            }
        }
        public async Task<int> AtualizarEntrada(TSaldoEstoque saldoEstoque)
        {
            int id = default(int);
            try
            {
                decimal qtd = await GetSaldoPorEntrada(saldoEstoque);
                saldoEstoque.Quantidade += qtd;
                id = await Repository.InsertAndGetIdAsync(saldoEstoque);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return id;
        }
        public async Task<int> EstornarEntrada(TSaldoEstoque saldoEstoque)
        {
            int id = default(int);
            try
            {
                decimal qtd = await GetSaldoPorEntrada(saldoEstoque);
                saldoEstoque.Quantidade = qtd * -1;
                id = await Repository.InsertAndGetIdAsync(saldoEstoque);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return id;
        }
        private async Task<decimal> GetSaldoPorEntrada(TSaldoEstoque saldoEstoque)
        {
            var resultado = Repository
            .GetAll()
            .Where(x =>
                x.Empresa.Id == saldoEstoque.EmpresaId
                && x.Produto.Id == saldoEstoque.ProdutoId
                && x.EntradaId == saldoEstoque.EntradaId.Value
            )
            .Select(x => x.Quantidade)
            .DefaultIfEmpty(0)
            .Sum();

            return resultado;
        }
        public async Task RemoverSaldoPorEntrada(int entradaId)
        {
            Repository.Delete(x => x.EntradaId.Value == entradaId);
        }
        #endregion

        #region Saldo Por Pedido
        public void CanAtualizarPedido(TSaldoEstoque saldoEstoque)
        {
            if (saldoEstoque.Empresa == null)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaEmpty"));
            }
            if (saldoEstoque.Produto == null)
            {
                throw new UserFriendlyException(L("Estoque.ProdutoEmpty"));
            }
            if (saldoEstoque.Pedido == null)
            {
                throw new UserFriendlyException(L("Estoque.PedidoEmpty"));
            }
        }
        public async Task<int> AtualizarPedido(TSaldoEstoque saldoEstoque)
        {
            int id = default(int);
            try
            {
                decimal qtd = await GetSaldoPorPedido(saldoEstoque);
                saldoEstoque.Quantidade -= qtd;
                id = await Repository.InsertAndGetIdAsync(saldoEstoque);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return id;
        }
        public async Task<int> EstornarPedido(TSaldoEstoque saldoEstoque)
        {
            int id = default(int);
            try
            {
                decimal qtd = await GetSaldoPorPedido(saldoEstoque);
                saldoEstoque.Quantidade = qtd * -1;
                id = await Repository.InsertAndGetIdAsync(saldoEstoque);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            return id;
        }
        private async Task<decimal> GetSaldoPorPedido(TSaldoEstoque saldoEstoque)
        {
            var resultado = Repository
            .GetAll()
            .Where(x =>
                x.Empresa.Id == saldoEstoque.EmpresaId
                && x.Produto.Id == saldoEstoque.ProdutoId
                && x.PedidoId == saldoEstoque.PedidoId.Value
            )
            .Select(x => x.Quantidade)
            .DefaultIfEmpty(0)
            .Sum();

            return resultado;
        }
        public async Task RemoverSaldoPorPedido(int pedidoId)
        {
            Repository.Delete(x => x.PedidoId.Value == pedidoId);
        }
        #endregion

        public async Task<decimal> GetSaldoProduto(TSaldoEstoque saldoEstoque)
        {
            var resultado = Repository
            .GetAll()
            .Where(x =>
                x.Empresa.Id == saldoEstoque.EmpresaId
                && x.Produto.Id == saldoEstoque.ProdutoId
            )
            .Select(x => x.Quantidade)
            .DefaultIfEmpty(0)
            .Sum();

            return resultado;
        }

        public async Task<decimal> GetSaldoProdutoPorLocalArmazenamento(TSaldoEstoque saldoEstoque)
        {
            var resultado = Repository
            .GetAll()
            .Where(x =>
                x.Empresa.Id == saldoEstoque.EmpresaId
                && x.Produto.Id == saldoEstoque.ProdutoId
                && x.LocalArmazenamentoId == saldoEstoque.LocalArmazenamentoId.Value
            )
            .Select(x => x.Quantidade)
            .DefaultIfEmpty(0)
            .Sum();

            return resultado;
        }

        public Task ConsolidarSaldo(TSaldoEstoque saldoEstoque)
        {
            throw new NotImplementedException();
        }
    }

    public class SaldoEstoqueService : SaldoEstoqueService<SaldoEstoque>, ISaldoEstoqueService
    {
        public SaldoEstoqueService(IRepository<SaldoEstoque, int> repository)
            : base(repository)
        {
        }
    }
}
