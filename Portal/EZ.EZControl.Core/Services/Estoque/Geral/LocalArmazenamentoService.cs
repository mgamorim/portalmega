﻿using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Services.Estoque.Geral
{
    public class LocalArmazenamentoService<TLocalArmazenamento> : EZControlDomainServiceBase<TLocalArmazenamento>, ILocalArmazenamentoService<TLocalArmazenamento>
        where TLocalArmazenamento : Domain.Estoque.Geral.LocalArmazenamento
    {
        public LocalArmazenamentoService(IRepository<TLocalArmazenamento, int> repository)
            : base(repository)
        {
            ValidateEntity += LocalArmazenamentoService_ValidateEntity;
            BeforeSaveEntity += LocalArmazenamentoService_BeforeSaveEntity;
        }

        private void LocalArmazenamentoService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TLocalArmazenamento, int> e)
        {
        }

        private void LocalArmazenamentoService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TLocalArmazenamento, int> e)
        {
        }

        public void CanAssignEmpresa(TLocalArmazenamento localArmazenamento, Empresa empresa)
        {
            if (localArmazenamento == null)
                throw new UserFriendlyException(L("Estoque.LocalArmazenamentoEmptyError"));

            if (empresa == null)
                throw new UserFriendlyException(L("Empresa.EmptyError"));
           
        }
    }

    public class LocalArmazenamentoService : LocalArmazenamentoService<LocalArmazenamento>, ILocalArmazenamentoService
    {   
        public LocalArmazenamentoService(IRepository<LocalArmazenamento, int> repository) 
            : base(repository)
        {
        }
    }
}
