﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Services.Servicos.Geral.Interfaces;

namespace EZ.EZControl.Services.Servicos.Geral
{
    public class ServicoService : EZControlDomainServiceBase<Servico>, IServicoService
    {
        public ServicoService(IRepository<Servico, int> repository) 
            : base(repository)
        {
        }
    }
}
