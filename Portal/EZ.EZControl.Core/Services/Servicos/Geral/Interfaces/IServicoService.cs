﻿using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Servicos.Geral.Interfaces
{
    public interface IServicoService : IService<Servico>
    {
    }
}
