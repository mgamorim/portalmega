﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.Core.Geral
{
    public class ArquivoBaseService<TArquivo> : EZControlDomainServiceBase<TArquivo>, IArquivoBaseService<TArquivo>
        where TArquivo : ArquivoBase
    {
        public ArquivoBaseService(IRepository<TArquivo, int> repository)
            : base(repository)
        {

        }

        private void ArquivoBaseService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TArquivo, int> e)
        {
        }

        private void ArquivoBaseService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TArquivo, int> e)
        {
        }
    }

    public class ArquivoBaseService : ArquivoBaseService<ArquivoBase>, IArquivoBaseService
    {
        public ArquivoBaseService(IRepository<ArquivoBase, int> repository)
            : base(repository)
        {
        }
    }
}
