﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.Core.Geral
{
    public class ParametroBaseService<TParametro> : EZControlDomainServiceBase<TParametro>, IParametroBaseService<TParametro>
        where TParametro : ParametroBase
    {
        public ParametroBaseService(IRepository<TParametro, int> repository)
            : base(repository)
        {
            ValidateEntity += ParametroBaseService_ValidateEntity;
            BeforeSaveEntity += ParametroBaseService_BeforeSaveEntity;
        }

        private void ParametroBaseService_BeforeSaveEntity(object sender, Events.BeforeSaveEntityEventArgs<TParametro, int> e)
        {
        }

        private void ParametroBaseService_ValidateEntity(object sender, Events.ValidateEntityEventArgs<TParametro, int> e)
        {
        }
    }

    public class ParametroBaseService : ParametroBaseService<ParametroBase>, IParametroBaseService
    {
        public ParametroBaseService(IRepository<ParametroBase, int> repository)
            : base(repository)
        {
        }
    }
}