﻿using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.Interfaces;

namespace EZ.EZControl.Services.Core.Geral.Interfaces
{
    public interface IPictureService : IService<Picture>
    {
    }
}
