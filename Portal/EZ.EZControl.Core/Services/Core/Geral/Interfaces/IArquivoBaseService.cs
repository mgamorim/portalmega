﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.Core.Geral.Interfaces
{
    public interface IArquivoBaseService<TArquivo, TPrimaryKey> : IService<TArquivo, TPrimaryKey>
        where TArquivo : ArquivoBase, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
    }

    public interface IArquivoBaseService<TArquivo> : IArquivoBaseService<TArquivo, int>
        where TArquivo : ArquivoBase
    {
    }

    public interface IArquivoBaseService : IArquivoBaseService<ArquivoBase>
    {
    }
}
