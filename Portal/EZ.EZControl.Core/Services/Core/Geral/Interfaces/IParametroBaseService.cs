﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.Interfaces;
using System;

namespace EZ.EZControl.Services.Core.Geral.Interfaces
{
    public interface IParametroBaseService<TParametro, TPrimaryKey> : IService<TParametro, TPrimaryKey>
        where TParametro : ParametroBase, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
    }

    public interface IParametroBaseService<TParametro> : IParametroBaseService<TParametro, int>
        where TParametro : ParametroBase
    {
    }

    public interface IParametroBaseService : IParametroBaseService<ParametroBase>
    {
    }
}