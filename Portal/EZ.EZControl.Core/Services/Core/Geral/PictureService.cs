﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.Core.Geral.Interfaces;

namespace EZ.EZControl.Services.Core.Geral
{
    public class PictureService : EZControlDomainServiceBase<Picture>, IPictureService
    {
        public PictureService(IRepository<Picture, int> repository) : base(repository)
        {
        }
    }
}
