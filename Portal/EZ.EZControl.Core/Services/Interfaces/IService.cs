﻿using Abp.Domain.Entities;
using Abp.Domain.Services;
using EZ.EZControl.Events;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Interfaces
{
    public interface IService<TEntity, TPrimaryKey> : IDomainService
        where TEntity : class, IEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        bool CheckForFieldDuplicated(TPrimaryKey id, object valor, Func<TEntity, bool> condicao);

        bool CheckForDuplicateInstance(TPrimaryKey id, Expression<Func<TEntity, bool>> condicao);

        event EventHandler<ValidateEntityEventArgs<TEntity, TPrimaryKey>> ValidateEntity;

        event EventHandler<BeforeSaveEntityEventArgs<TEntity, TPrimaryKey>> BeforeSaveEntity;

        Task<TPrimaryKey> CreateEntity(TEntity entity);

        void Insert(IEnumerable<TEntity> entities);

        Task<TPrimaryKey> CreateOrUpdateEntity(TEntity entity);

        Task<TEntity> CreateAndReturnEntity(TEntity entity);

        Task<TEntity> UpdateAndReturnEntity(TEntity entity);

        Task<TEntity> CreateOrUpdateAndReturnSavedEntity(TEntity entity);

        Task<TEntity> UpdateEntity(TEntity entity);

        IQueryable<TEntity> GetAll();

        Task<List<TEntity>> GetAllListAsync();

        Task<List<TEntity>> GetAllListAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetById(TPrimaryKey id);

        Task<TEntity> FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> GetByExternalId(TPrimaryKey id);

        Task Delete(TPrimaryKey id);

        Task Delete(Expression<Func<TEntity, bool>> predicate);

        void SetEmpresaInAllEntities(IEzEntity<TPrimaryKey> entity);

        void SetEmpresaInEntity(IEzEntity<TPrimaryKey> entity);
    }

    public interface IService<TEntity> : IService<TEntity, int>
        where TEntity : class, IEntity<int>
    {
    }
}