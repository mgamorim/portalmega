﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Filters
{
    public static class EzControlFilters
    {
        public const string EmpresaFilter = "EmpresaFilter";

        public static class Parameters
        {
            public const string EmpresaId = "empresaId";
        }
    }
}
