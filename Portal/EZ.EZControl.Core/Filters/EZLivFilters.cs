﻿namespace EZ.EZControl.Filters
{
    public static class EZLivFilters
    {
        public const string CorretoraFilter = "CorretoraFilter";
        public const string AdministradoraFilter = "AdministradoraFilter";
        public const string OperadoraFilter = "OperadoraFilter";
        public const string CorretorFilter = "CorretorFilter";
        public const string AssociacaoFilter = "AssociacaoFilter";
        public const string TitularFilter = "TitularFilter";

        public static class Parameters
        {
            public const string CorretoraId = "corretoraId";
            public const string AdministradoraId = "administradoraId";
            public const string OperadoraId = "operadoraId";
            public const string CorretorId = "corretorId";
            public const string AssociacaoId = "associacaoId";
            public const string TitularId = "titularId";
        }
    }
}
