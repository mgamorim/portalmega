﻿using Abp.Dependency;
using Abp.Domain.Uow;
using System;

namespace EZ.EZControl.Filters.Attribute
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DisableFilterAttribute : System.Attribute
    {
        public DisableFilterAttribute(string ezControlFilter)
        {
            var currentUnitOfWork = IocManager.Instance.Resolve<ICurrentUnitOfWorkProvider>();
            if (currentUnitOfWork != null)
            {
                if (currentUnitOfWork.Current != null)
                    currentUnitOfWork.Current.DisableFilter(ezControlFilter);
            }
        }
    }
}
