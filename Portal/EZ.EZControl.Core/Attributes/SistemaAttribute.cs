﻿using System;

namespace EZ.EZControl.Attributes
{
    [System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    public sealed class SistemaAttribute : Attribute
    {
        readonly string _prefixo;

        public SistemaAttribute(string prefixo)
        {
            _prefixo = prefixo;
        }

        public string Prefixo
        {
            get { return _prefixo; }
        }
    }
}