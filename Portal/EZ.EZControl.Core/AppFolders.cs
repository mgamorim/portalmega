﻿using Abp.Dependency;

namespace EZ.EZControl
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }

        public string SampleProfileImagesFolder { get; set; }

        public string WebLogsFolder { get; set; }

        public string TemplateFolder { get; set; }
    }
}