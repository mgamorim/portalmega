﻿using Abp.Dependency;
using Abp.Extensions;
using Abp.Localization;
using Abp.Reflection;
using System;
using System.Linq;
using System.Reflection;
using System.Text;
using EZ.EZControl.MultiEmpresa;

namespace EZ.EZControl.Web.Constant
{
    /// <summary>   Generates JavaScript for constants and enums 
    ///             marked with the PublishAttribute. </summary >
    public class ContanstScriptManager : IContanstScriptManager, ISingletonDependency
    {
        private readonly ILocalizationManager _localizationManager;
        private readonly IAssemblyFinder _assemblyFinder;
        private readonly IMultiEmpresa _multiEmpresa;

        public ContanstScriptManager(ILocalizationManager localizationManager, IAssemblyFinder assemblyFinder, IMultiEmpresa multiEmpresa)
        {
            _localizationManager = localizationManager;
            _assemblyFinder = assemblyFinder;
            _multiEmpresa = multiEmpresa;
        }

        /// <summary>   Gera um objeto JavaScript que representa o Enum. Cada valor do Enum é 
        /// exportado, bem como um Array que pode ser utilizado numa tag Select. 
        /// O Array inclui automaticamente a tradução, não precisando chamar o app.localization.localize do javascript.
        ///  O nome localizado está no formato Enum_[Parent_][EnumName]_[ValueName].</summary>
        private string GetEnumScript(Type enumType, string parent = null)
        {
            string resourcePrefix = "Enum_";
            var script = new StringBuilder();
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("enumType is not an Enum.", "enumType");
            }
            script.Append("    ez.domain.enum.");
            /**
             * If a parent class is defined, add the extra level.
             * The assumption is that the parent class would have already
             * been declared first in the script.
             */
            if (!string.IsNullOrEmpty(parent))
            {
                parent = parent.Trim('.');
                resourcePrefix += parent + "_";
                script.Append(parent.ToCamelCase());
                script.Append(".");
            }
            resourcePrefix += enumType.Name + "_";
            script.Append(enumType.Name.ToCamelCase());
            script.Append(" = new EZDomainEnum({");

            // Export all enum values
            int added = 0;
            var valuesArray = new StringBuilder();
            valuesArray.Append("        valores: [");
            foreach (var enumValue in enumType.GetEnumValues())
            {
                if (added > 0)
                {
                    script.AppendLine(",");
                    valuesArray.AppendLine(",");
                }
                else
                {
                    script.AppendLine();
                    valuesArray.AppendLine();
                }
                string name = enumType.GetEnumName(enumValue);
                long value = Convert.ToInt64(enumValue);
                string localizedName = L(String.Concat(resourcePrefix, name));
                script.Append(String.Concat("        ", name.ToCamelCase(), ": ", value));
                valuesArray.Append(String.Concat("            { nome: '", name, "', valor: ", value, ", descricao: '", localizedName, "' }"));
                ++added;
            }
            valuesArray.Append("]");
            if (added > 0)
            {
                script.AppendLine(",");
            }
            else
            {
                script.AppendLine();
            }
            script.AppendLine(valuesArray.ToString());
            script.AppendLine("    });");

            script.AppendLine("ez.domain.enum.dayOfWeek = new EZDomainEnum({");
            script.AppendLine("domingo : 0, segunda : 1, terca : 2, quarta : 3, quinta : 4, sexta : 5, sabado : 6, ");
            script.AppendLine("valores: [{nome : 'domingo', valor : 0, descricao : 'Domingo'}, {nome : 'segunda', valor : 1, descricao : 'Segunda'}, {nome : 'terca', valor : 2, descricao : 'Terça'}, {nome : 'quarta', valor : 3, descricao : 'Quarta'}, {nome : 'quinta', valor : 4, descricao : 'Quinta'}, {nome : 'sexta', valor : 5, descricao : 'Sexta'}, {nome : 'sabado', valor : 6, descricao : 'Sábado'}]");
            script.AppendLine("});");

            return script.ToString();
        }

        private string GetConstScript(Type constType)
        {
            var script = new StringBuilder();

            if (constType.IsVisible)
            {
                var potentialConstantFields = constType.GetFields(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Static);

                if (potentialConstantFields.Count() > 0)
                {
                    script.Append("    ez.domain.constant." + constType.Name.ToCamelCase() + " = {");
                    // Export all public constants & readonly values
                    var added = 0;
                    foreach (var potentialConstantField in potentialConstantFields)
                    {
                        if (potentialConstantField.IsLiteral)
                        {
                            object constantValue = potentialConstantField.GetRawConstantValue();
                            if (constantValue != null)
                            {
                                if (added > 0)
                                {
                                    script.AppendLine(",");
                                }
                                else
                                {
                                    script.AppendLine();
                                }
                                string value = constantValue.ToString();
                                if (potentialConstantField.FieldType == typeof(string))
                                {
                                    // If this is a string type, enclose in quotes.
                                    value = string.Format("'{0}'", constantValue);
                                }
                                script.Append("        " + potentialConstantField.Name.ToCamelCase() + ": " + value);
                                ++added;
                            }
                        }
                    }
                    script.AppendLine();
                    script.AppendLine("    };");
                }
            }

            return script.ToString();
        }

        private string GetMultiEmpresaScript()
        {
            var script = new StringBuilder();

            script.AppendLine("(function(){");
            script.AppendLine();

            var empresa = _multiEmpresa.EmpresaLogada();

            script.AppendLine("    abp.session = abp.session || {};");
            script.AppendLine("    abp.session.empresaId = " + (empresa != null ? empresa.Id.ToString() : "null")  + ";");
            script.AppendLine("    abp.session.isMultiEmpresa = " + (_multiEmpresa.IsMultiEmpresa.ToString().ToLower()) + ";");
            script.AppendLine("    abp.session.empresaNome = " + (empresa != null ? "'" + empresa.NomeFantasia + "'" : "null") + ";");

            script.AppendLine();
            script.Append("})();");

            return script.ToString();
        }

        public string GetScript()
        {
            var script = new StringBuilder();

            script.AppendLine("(function(){");
            script.AppendLine("    var EZDomainEnum = function EnumAux(objEnum) {");
            script.AppendLine("         for (var i in objEnum) { this[i] = objEnum[i]; };");
            script.AppendLine("         return this; ");
            script.AppendLine("    }");
            script.AppendLine("    EZDomainEnum.prototype.getItemPorValor = function(valor) { for (var i = 0; i < this.valores.length; i++) { if (this.valores[i].valor == valor) return this.valores[i]; } }");
            script.AppendLine("    window.ez = window.ez || {};");
            script.AppendLine("    ez.domain = ez.domain || {};");
            script.AppendLine("    ez.domain.enum = ez.domain.enum || {}; ");
            script.AppendLine("    ez.domain.constant = ez.domain.constant || {}; ");

            /**
             * Use reflection to find all Enums & Classes marked as publish
             */
            TypeFinder finder = new TypeFinder(_assemblyFinder);
            Type[] DomainTypes = finder.Find(q => ((q.IsClass || q.IsEnum) && q.FullName.Contains("EZ.EZControl.Domain")));

            foreach (Type DomainType in DomainTypes)
            {
                if (DomainType.IsEnum)
                {
                    script.Append(GetEnumScript(DomainType));

                    // Export any nested enums
                    foreach (var nestedType in DomainType.GetNestedTypes())
                    {
                        script.Append(GetEnumScript(nestedType, DomainType.Name));
                    }
                }
                else
                {
                    script.Append(GetConstScript(DomainType));
                }
            }

            script.AppendLine();
            script.Append(GetMultiEmpresaScript());
            script.AppendLine();
            script.Append("})();");

            return script.ToString();
        }

        private string L(string name)
        {
            var ls = new LocalizableString(name, EZControlConsts.LocalizationSourceName);

            return _localizationManager.GetString(ls);
        }
    }
}