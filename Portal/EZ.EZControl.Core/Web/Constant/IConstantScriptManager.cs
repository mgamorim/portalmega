﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Web.Constant
{
    /// <summary>
    /// Define interface to get enums and enumerations injected into a script
    /// </summary>
    public interface IContanstScriptManager
    {
        /// <summary>
        /// Gets Javascript that contains setting values.
        /// </summary>
        string GetScript();
    }
}