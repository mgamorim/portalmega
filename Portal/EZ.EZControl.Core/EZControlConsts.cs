﻿namespace EZ.EZControl
{
    /// <summary>
    /// Some general constants for the application.
    /// </summary>
    public class EZControlConsts
    {
        public const string LocalizationSourceName = "EZControl";
    }
}