﻿using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class EZPagParcelamento : EzEntity
    {
        public int NumeroDeParcelas { get; set; }
        public decimal ValorDaParcela { get; set; }
        public int NumeroDeParcelasSemJuros { get; set; }
    }
}
