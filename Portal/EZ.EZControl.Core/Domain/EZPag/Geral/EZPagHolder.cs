﻿using EZ.EZControl.EZ.EzEntity;
using System;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class EZPagHolder : EzEntity
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public DateTime Nascimento { get; set; }
    }
}
