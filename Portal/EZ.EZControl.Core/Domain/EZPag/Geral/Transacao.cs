﻿using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Domain.EZPag.Enums;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class Transacao : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public Transacao()
        {
            Data = DateTime.Now;
        }
        public int PedidoId { get; set; }
        public virtual Pedido Pedido { get; set; }
        public string TransactionId { get; set; }
        public string Referencia { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPagamentoEnum StatusDoPagamento { get; set; }
        public string LinkParaPagamento { get; set; }
    }
}
