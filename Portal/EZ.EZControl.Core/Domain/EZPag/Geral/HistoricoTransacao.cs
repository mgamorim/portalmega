﻿using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity;
using System;
using EZ.EZControl.Domain.EZPag.Enums;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class HistoricoTransacao : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public HistoricoTransacao()
        {
            Data = DateTime.Now;
        }
        public int TransacaoId { get; set; }
        public virtual Transacao Transacao { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPagamentoEnum Status { get; set; }
    }
}
