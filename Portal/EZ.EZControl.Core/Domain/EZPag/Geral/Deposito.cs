﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity;
using System;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class Deposito : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public Deposito()
        {
            
        }
        public virtual Pedido Pedido { get; set; }
        public int PedidoId { get; set; }
        public string Comprovante { get; set; }
        public virtual User UsuarioEnvio { get; set; }
        public long UsuarioEnvioId { get; set; }
        public virtual User UsuarioDeclarante { get; set; }
        public long? UsuarioDeclaranteId { get; set; }
        public bool Recebido { get; set; }
        public DateTime? DataHoraDeclaracao { get; set; }
        public DateTime DataHoraEnvio { get; set; }
    }
}
