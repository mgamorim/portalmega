﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class DepositoTransferenciaBancaria : EzEntityMustHaveTenantMustHaveEmpresa
    {
        //public int Id { get; set; }
        //[System.ComponentModel.DataAnnotations.Schema.NotMapped]
        //public int EmpresaId { get; set; }
        //public virtual Empresa Empresa { get; set; }
        public int PessoaJuridicaId { get; set; }
        public virtual PessoaJuridica PessoaJuridica { get; set; }
        public string Comprovante { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string Titular { get; set; }
        public string Descricao { get; set; }
        public bool IsActive { get; set; }
    }
}
