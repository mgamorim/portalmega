﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class EZPagCheckout : EzEntity
    {
        public TipoCheckoutPagSeguroEnum Tipo { get; set; }
        public virtual Pedido Pedido { get; set; }
        public string SenderHash { get; set; }
        public EZPagHolder Holder { get; set; }
        public string TokenCartao { get; set; }
        public EZPagParcelamento OpcaoDeParcelamento { get; set; }
        public string NomeDoBanco { get; set; }
        public string EmailVendedor { get; set; }
    }
}
