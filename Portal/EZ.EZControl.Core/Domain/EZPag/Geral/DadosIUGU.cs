﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZPag.Geral
{
   public class DadosIUGU
    {


        public string idUser { get; set; }
        public string idClienteIUGU { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Cpf { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string Telefone { get; set; }
        public string DDD { get; set; }


        public string TipoPagamento { get; set; } // boleto - cartao

        public string TipoCliente { get; set; } // Beneficiário / Corretor / Parceiro
        public string Empresa { get; set; }  // Megavita - Uber - Evida


        public string IdProduto { get; set; }   // Id plano de saude 
        public string IdProposta { get; set; }   // Id da proposta gerada
        public string NomeProduto { get; set; } // Nome Plano de saude
        public string Valor { get; set; }   // Valor do produto total
        public string ValorFicha { get; set; }   // Valor da ficha




    }
}
