﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class FormaDePagamento : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int DescricaoMaxLength = 60;
                
        public bool IsActive { get; set; }
        public string Descricao { get; set; }
        public TipoCheckoutPagSeguroEnum TipoDePagamento { get; set; }

    }
}
