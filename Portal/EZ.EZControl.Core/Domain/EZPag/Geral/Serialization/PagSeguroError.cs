﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace EZ.EZControl.Domain.EZPag.Geral.Serialization
{
    /// <remarks/>
    [SerializableAttribute()]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class errors
    {
        private errorsError errorField;
        public errorsError error
        {
            get
            {
                return errorField;
            }
            set
            {
                errorField = value;
            }
        }
    }
    [SerializableAttribute()]
    [DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public partial class errorsError
    {
        private ushort codeField;
        private string messageField;
        public ushort code
        {
            get
            {
                return codeField;
            }
            set
            {
                codeField = value;
            }
        }
        public string message
        {
            get
            {
                return messageField;
            }
            set
            {
                messageField = value;
            }
        }
    }
}
