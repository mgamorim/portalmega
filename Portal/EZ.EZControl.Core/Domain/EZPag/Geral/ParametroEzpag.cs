﻿using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class ParametroEzpag:ParametroBase
    {
        public bool IsTest { get; set; }

        public void ChecarAmbienteTeste(IParametroEzpagService parametroService)
        {
            parametroService.EstaEmAmbienteDeTeste(this);
        }
    }

   
}
