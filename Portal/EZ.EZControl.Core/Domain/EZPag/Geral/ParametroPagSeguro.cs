﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZPag.Geral
{
    public class ParametroPagSeguro : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public int PessoaJuridicaId { get; set; }
        public virtual PessoaJuridica PessoaJuridica { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string AppId { get; set; }
        public string AppKey { get; set; }
    }
}
