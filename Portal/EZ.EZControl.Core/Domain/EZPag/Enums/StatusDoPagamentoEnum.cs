﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZPag.Enums
{
    public enum StatusDoPagamentoEnum
    {
        Iniciado = 1,
        AguardandoPagamento = 2,
        EmAnalise = 3,
        Pago = 4,
        Disponivel = 5,
        EmDisputa = 6,
        Reembolsado = 7,
        Cancelado = 8
    }
}
