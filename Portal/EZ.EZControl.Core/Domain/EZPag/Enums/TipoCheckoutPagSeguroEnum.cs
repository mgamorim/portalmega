﻿namespace EZ.EZControl.Domain.EZPag.Enums
{
    public enum TipoCheckoutPagSeguroEnum
    {
        BOLETO = 1,
        ONLINE_DEBIT = 2,
        CREDIT_CARD = 3,
        DEPOSIT_ACCOUNT = 4
    }
}
