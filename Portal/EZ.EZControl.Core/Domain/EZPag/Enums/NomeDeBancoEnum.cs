﻿namespace EZ.EZControl.Domain.EZPag.Enums
{
    public enum NomeDeBancoEnum
    {
        Bradesco = 1,
        Itau = 2,
        BancoDoBrasil = 3,
        Banrisul = 4,
        HSBC = 5
    }
}
