﻿namespace EZ.EZControl.Domain.EZPag.Enums
{
    public enum FormaDePagamentoEnum
    {
        Deposito = 1,
        Boleto = 2,
        CartaoDeCredito = 3,
        DepositoEmConta = 4
    }
}
