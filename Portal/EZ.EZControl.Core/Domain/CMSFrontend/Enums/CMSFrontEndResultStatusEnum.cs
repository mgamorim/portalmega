﻿namespace EZ.EZControl.Domain.CMSFrontend.Enums
{
    public enum CMSFrontEndResultStatusEnum
    {
        Sucesso = 1,
        Atencao = 2,
        Erro = 3
    }
}