﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.CMS.Enums
{
    public enum StatusDaPublicacaoEnum
    {
        Agendada = 1,
        Publicada = 2,
        Cancelada = 3,
        Suspensa = 4,
        Rascunho = 5
    }
}
