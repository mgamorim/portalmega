﻿namespace EZ.EZControl.Domain.CMS.Enums
{
    public enum TipoDeConteudoEnum
    {
        Pagina = 1,
        CategoriaComUltimosPosts = 2,
        Post = 3,
        PostsPorAno = 4,
        PostsPorMes = 5,
        CategoriaComPostsPorAno = 6,
        CategoriaComPostsPorMes = 7
    }
}
