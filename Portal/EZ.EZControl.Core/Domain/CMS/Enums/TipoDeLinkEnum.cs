﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.CMS.Enums
{
    public enum TipoDeLinkEnum
    {
        [Description("Página")]
        Pagina = 1,
        [Description("Post")]
        Post = 2,
        [Description("Categoria")]
        Categoria = 3
    }
}
