﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.CMS.Enums
{
    public enum TipoDeConteudoRSSEnum
    {
        Completo = 1,
        Resumo = 2
    }
}
