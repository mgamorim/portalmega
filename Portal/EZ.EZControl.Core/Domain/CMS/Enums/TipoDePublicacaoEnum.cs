﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.CMS.Enums
{
    public enum TipoDePublicacaoEnum
    {
        Post = 1,
        Pagina = 2
    }
}
