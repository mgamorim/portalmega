﻿namespace EZ.EZControl.Domain.CMS.Enums
{
    public enum TipoDeWidgetEnum
    {
        CmsMenu = 1,
        CmsHtml = 2
    }
}
