﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Menu : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 50;
        public Boolean IsActive { get; set; }
        public String Nome { get; set; }
        public virtual Site Site { get; set; }
        public virtual int SiteId { get; set; }
        public virtual ICollection<ItemDeMenu> ItensDeMenu { get; set; }
        
        
    }
}
