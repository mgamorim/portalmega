﻿using System.Collections.Generic;
using EZ.EZControl.Domain.CMS.Enums;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Conteudo
    {
        public TipoDeConteudoEnum TipoDeConteudo { get; set; }
        public Categoria Categoria { get; set; }
        public Pagina Pagina { get; set; }
        public Post Post { get; set; }
        public List<Post> Posts { get; set; }
        public int AnoDosPosts { get; set; }
        public int MesDosPosts { get; set; }
        public int NumeroDaPaginaPosts { get; set; }
        public int ItemPorPaginaPosts { get; set; }
    }
}
