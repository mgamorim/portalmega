﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class WidgetBase : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int TituloMaxLength = 50;
        public string Titulo { get; set; }
        public bool ExibirTitulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public virtual Site Site { get; set; }
        public virtual int SiteId { get; set; }
        public virtual Template Template { get; set; }
        public virtual int TemplateId { get; set; }
        public TipoDeWidgetEnum Tipo { get; set; }
        public string Posicao { get; set; }
        public bool IsActive { get; set; }
        
        
    }
}
