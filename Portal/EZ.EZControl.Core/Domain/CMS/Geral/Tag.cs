﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Tag : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 50;
        public const int SlugMaxLength = 50;
        public const int DescricaoMaxLength = 200;
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Slug { get; set; }
        public string Descricao { get; set; }
        public virtual ICollection<Post> Posts { get; protected set; }
        
        
    }
}
