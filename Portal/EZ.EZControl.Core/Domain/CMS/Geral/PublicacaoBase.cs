﻿using System;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class PublicacaoBase : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int TituloMaxLength = 70;
        public const int SlugMaxLength = 70;
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public virtual Site Site { get; set; }
        public virtual int SiteId { get; set; }
        public string Titulo { get; set; }
        public string Slug { get; set; }
        public string Conteudo { get; set; }
        public int IdImagemDestaque { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<HistoricoPublicacao> Historico { get; set; }

        public DateTime DataDePublicacao { get; set; }
    }
}
