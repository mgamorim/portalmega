﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Site : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 50;
        public const int DescricaoMaxLength = 200;
        public const int TextoCssMaxLength = 500;
        public const int TextoJavaScriptMaxLength = 500;

        public Site()
        {
            IsActive = true;
            LimitePostPorPagina = 10;
            LimitePostPorRss = 20;
            TipoDePublicacao = TipoDePublicacaoEnum.Post;
            TipoDeConteudoRSS = TipoDeConteudoRSSEnum.Resumo;
            LimitePostPorPagina = 10;
            LimitePostPorRss = 20;
            EvitarMecanismoDeBusca = true;
            PermitirComentarios = false;
            PermitirLinks = false;
            AutorInformaNomeEmail = true;
            NotificarPorEmailNovoComentario = true;
            AtivarModeracaoDeComentario = true;
            TextoCss = string.Empty;
            TextoJavaScript = string.Empty;
            HabilitarPesquisaPost = true;
            HabilitarPesquisaPagina = true;
            Hosts = new List<Host>();
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTags { get; set; }
        public string Owner { get; set; }

        // Configurações
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public TipoDeConteudoRSSEnum TipoDeConteudoRSS { get; set; }
        public int LimitePostPorPagina { get; set; }
        public int LimitePostPorRss { get; set; }
        public bool EvitarMecanismoDeBusca { get; set; }
        public virtual Template TemplateDefault { get; set; }
        public virtual int TemplateDefaultId { get; set; }

        //Comentários
        public bool PermitirComentarios { get; set; }
        public bool PermitirLinks { get; set; }
        public bool AutorInformaNomeEmail { get; set; }
        public bool NotificarPorEmailNovoComentario { get; set; }
        public bool AtivarModeracaoDeComentario { get; set; }

        //Inserção de Códigos
        public string TextoCss { get; set; }
        public string TextoJavaScript { get; set; }
        public virtual ICollection<PublicacaoBase> Publicacoes { get; set; }

        //Pesquisa
        public bool HabilitarPesquisaPost { get; set; }
        public bool HabilitarPesquisaPagina { get; set; }

        //Urls Alternativas
        public virtual ICollection<Host> Hosts { get; set; }

        //Tipos de Páginas
        public string TipoDePaginaParaPaginaDefault { get; set; }
        public string TipoDePaginaParaPostDefault { get; set; }
        public string TipoDePaginaParaCategoriaDefault { get; set; }
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }
        
        
    }
}