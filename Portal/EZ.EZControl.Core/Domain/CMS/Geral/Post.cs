﻿using EZ.EZControl.Domain.CMS.Enums;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Post : PublicacaoBase
    {
        public Post()
        {
            TipoDePublicacao = TipoDePublicacaoEnum.Post;
        }
        public virtual ICollection<Categoria> Categorias { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
