﻿using EZ.EZControl.Domain.CMS.Enums;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Pagina : PublicacaoBase
    {
        public Pagina()
        {
            TipoDePublicacao = TipoDePublicacaoEnum.Pagina;
        }

        public string TipoDePagina { get; set; }
    }
}
