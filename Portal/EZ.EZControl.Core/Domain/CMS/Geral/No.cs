﻿using EZ.EZControl.Domain.CMS.Enums;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class No
    {
        public TipoDeNoEnum TipoDeNo { get; set; }
        public Categoria Categoria { get; set; }
        public Pagina Pagina { get; set; }
        public Post Post { get; set; }
    }
}
