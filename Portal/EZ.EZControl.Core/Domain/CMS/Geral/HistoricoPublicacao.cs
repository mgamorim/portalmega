﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class HistoricoPublicacao : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public HistoricoPublicacao()
        {
            Status = StatusDaPublicacaoEnum.Rascunho;
            Data = DateTime.Now;
        }

        public virtual PublicacaoBase Publicacao { get; set; }
        public virtual int PublicacaoId { get; set; }
        public StatusDaPublicacaoEnum Status { get; set; }
        public DateTime Data { get; set; }
        public bool IsActive { get; set; }
        
        
    }
}