﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Template : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public Template()
        {
            this.IsActive = true;
        }

        public const int NomeMaxLength = 50;
        public const int DescricaoMaxLength = 100;
        public bool IsActive { get; set; }
        [Required]
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Autor { get; set; }
        public string AutorUrl { get; set; }
        [Required]
        public string Versao { get; set; }
        public string NomeDoArquivo { get; set; }
        public string Posicoes { get; set; }
        public string TiposDeWidgetSuportados { get; set; }
        public string TiposDePaginasSuportadas { get; set; }
        public string TipoDePaginaParaPaginaDefault { get; set; }
        public string TipoDePaginaParaPostDefault { get; set; }
        public string TipoDePaginaParaCategoriaDefault { get; set; }
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }
        
        
    }
}
