﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using System;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class ItemDeMenu : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int TituloMaxLength = 50;
        public const int DescricaoDoTituloMaxLength = 100;
        public Boolean IsActive { get; set; }
        public virtual Menu Menu { get; set; }
        public int MenuId { get; set; }
        public virtual ItemDeMenu ItemMenu { get; set; }
        public int? ItemMenuId { get; set; }
        public virtual Categoria Categoria { get; set; }
        public int? CategoriaId { get; set; }
        public virtual Pagina Pagina { get; set; }
        public int? PaginaId { get; set; }
        public String Titulo { get; set; }
        public String DescricaoDoTitulo { get; set; }
        public TipoDeItemDeMenuEnum TipoDeItemDeMenu { get; set; }
        public String Url { get; set; }
        public virtual ICollection<ItemDeMenu> Itens { get; set; }
        
        
    }
}
