﻿namespace EZ.EZControl.Domain.CMS.Geral
{
    public class WidgetMenu : WidgetBase
    {
        public virtual Menu Menu { get; set; }
        public virtual int MenuId { get; set; }
    }
}
