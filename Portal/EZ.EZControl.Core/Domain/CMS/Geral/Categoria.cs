﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Categoria : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 50;
        public const int SlugMaxLength = 70;
        public const int DescricaoMaxLength = 200;
        public Categoria()
        {
            this.IsActive = true;
        }
        public string Nome { get; set; }
        public string Slug { get; set; }
        public string Descricao { get; set; }
        public bool IsActive { get; set; }
        public virtual Site Site { get; set; }
        public virtual int SiteId { get; set; }
        public virtual ICollection<Post> Posts { get; set; }
        public virtual Categoria CategoriaPai { get; set; }
        public virtual int? CategoriaPaiId { get; set; }
        public virtual ICollection<Categoria> Categorias { get; set; }
        
        
    }
}
