﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.CMS.Geral
{
    public class Host : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int UrlMaxLength = 300;
        public Host()
        {
            IsActive = true;
            IsPrincipal = false;
        }
        public bool IsActive { get; set; }
        public string Url { get; set; }
        public bool IsPrincipal { get; set; }
        public virtual Site Site { get; set; }
        public virtual int SiteId { get; set; }
        
        
    }
}
