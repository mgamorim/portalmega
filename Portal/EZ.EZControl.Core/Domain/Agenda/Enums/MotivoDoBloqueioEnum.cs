﻿namespace EZ.EZControl.Domain.Agenda.Enums
{
    public enum MotivoDoBloqueioEnum
    {
        Feriado = 1,
        Ferias = 2,
        AtestadoMedico = 3,
        Outro = 4
    }
}
