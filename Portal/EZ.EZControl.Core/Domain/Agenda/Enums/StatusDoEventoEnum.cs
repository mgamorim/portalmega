﻿namespace EZ.EZControl.Domain.Agenda.Enums
{
    public enum StatusDoEventoEnum
    {
        Agendado = 1,
        Iniciado = 2,
        Cancelado = 3,
        Transferido = 4,
        Finalizado = 5,
        Indefinido = 6
    }
}
