﻿namespace EZ.EZControl.Domain.Agenda.Enums
{
    public enum TipoDeEventoEnum
    {
        Tarefa = 1,
        Consulta = 2,
        Reuniao = 3
    }
}
