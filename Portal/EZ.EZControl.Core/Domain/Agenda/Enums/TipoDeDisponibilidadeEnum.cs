﻿namespace EZ.EZControl.Domain.Agenda.Enums
{
    public enum TipoDeDisponibilidadeEnum
    {
        DiasCorridos = 1,
        Semanal = 2,
        DataEspecifica = 3
    }
}
