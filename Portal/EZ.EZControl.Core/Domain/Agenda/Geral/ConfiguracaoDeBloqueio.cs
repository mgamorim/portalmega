﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class ConfiguracaoDeBloqueio : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int TituloMaxLength = 100;
        public ConfiguracaoDeBloqueio()
        {
            IsActive = true;
            Bloqueios = new List<Bloqueio>();
        }

        public bool IsActive { get; set; }
        [Required]
        public string Titulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public TipoDeEventoEnum TipoDeEvento { get; set; }
        public MotivoDoBloqueioEnum MotivoDoBloqueio { get; set; }
        public DateTime InicioBloqueio { get; set; }
        public DateTime FimBloqueio { get; set; }
        public TimeSpan HorarioInicio { get; set; }
        public TimeSpan HorarioFim { get; set; }
        public virtual ICollection<Bloqueio> Bloqueios { get; set; }
    }
}
