﻿using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class RegraConsulta : RegraBase
    {
        public virtual Medico Medico { get; set; }
        public virtual Especialidade Especialidade { get; set; }
    }
}
