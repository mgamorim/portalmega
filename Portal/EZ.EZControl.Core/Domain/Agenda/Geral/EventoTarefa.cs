﻿namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class EventoTarefa : EventoBase
    {
        public EventoTarefa()
        {
            TipoDeEvento = Enums.TipoDeEventoEnum.Tarefa;
        }
    }
}
