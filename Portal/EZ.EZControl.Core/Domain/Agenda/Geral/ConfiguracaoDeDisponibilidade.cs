﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class ConfiguracaoDeDisponibilidade : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int TituloMaxLength = 100;
        public ConfiguracaoDeDisponibilidade()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        [Required]
        public string Titulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public TipoDeEventoEnum TipoDeEvento { get; set; }
        public TipoDeDisponibilidadeEnum TipoDeDisponibilidade { get; set; }
        public string DiasDaSemana { get; set; }
        public int NumeroDeDias { get; set; }
        public DateTime? DataEspecifica { get; set; }
        public TimeSpan HorarioInicio { get; set; }
        public TimeSpan HorarioFim { get; set; }
        public DateTime? DataInicioValidade { get; set; }
        public DateTime? DataFimValidade { get; set; }
        public virtual ICollection<Disponibilidade> Disponibilidades { get; set; }
        
        
        public IEnumerable<DayOfWeek> Semana
        {
            get
            {
                return string.IsNullOrEmpty(DiasDaSemana) ? null : DiasDaSemana.Split(',').Select(dia => (DayOfWeek)Enum.Parse(typeof(DayOfWeek), dia, false)).ToList();
            }
        }

    }
}
