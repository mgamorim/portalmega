﻿using System;
using System.Collections.Generic;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class HistoricoDoEvento : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public virtual EventoBase Evento { get; set; }
        public virtual int EventoId { get; set; }
        public StatusDoEventoEnum Status { get; set; }
        public DateTime Data { get; set; }
        public string Observacao { get; set; }
        public bool IsActive { get; set; }
        
        
    }
}