﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class RegraBase : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        [Required]
        public virtual SistemaEnum Sistema { get; set; }
        [Required]
        public virtual TipoDeEventoEnum TipoDeEvento { get; set; }
        public int DiasExibidosDesdeHojeAteAgendamento { get; set; }
        public int VagasDisponibilizadasPorDia { get; set; }
        public int VagasReservadasPorDia { get; set; }
        [Required]
        public int UnidadeDeTempo { get; set; }
        public int VagasPorUnidadeDeTempo { get; set; }
        public int DiasAntesDoAgendamentoProibidoAlterar { get; set; }
        public int DiasParaNovoAgendamentoPorParticipante { get; set; }
        public bool OverBook { get; set; }
        public bool IsActive { get; set; }
        
        
    }
}
