﻿namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class EventoReuniao : EventoBase
    {
        public EventoReuniao()
        {
            TipoDeEvento = Enums.TipoDeEventoEnum.Reuniao;
        }
    }
}
