﻿using Abp.Domain.Entities;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class Disponibilidade : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public Disponibilidade()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public DateTime Data { get; set; }
        public TimeSpan Horario { get; set; }
        public virtual ConfiguracaoDeDisponibilidade ConfiguracaoDeDisponibilidade { get; set; }
        public virtual int ConfiguracaoDeDisponibilidadeId { get; set; }
        
        
    }
}
