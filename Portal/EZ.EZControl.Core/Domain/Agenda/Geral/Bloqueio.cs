﻿using Abp.Domain.Entities;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class Bloqueio : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public Bloqueio()
        {
            this.IsActive = true;
        }
        public bool IsActive { get; set; }
        public DateTime Data { get; set; }
        public TimeSpan Horario { get; set; }
        public virtual ConfiguracaoDeBloqueio ConfiguracaoDeBloqueio { get; set; }
        public virtual int ConfiguracaoDeBloqueioId { get; set; }
    }
}
