﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.Agenda.Geral
{
    public class EventoBase : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int DescricaoMaxLength = 1024;
        public const int RecurrenceRuleMaxLength = 1024;
        public const int TituloMaxLength = 255;

        public string Descricao { get; set; }

        public DateTime Termino { get; set; }

        public string TerminoEndZone { get; set; }

        public bool DiaInteiro { get; set; }

        public DateTime Inicio { get; set; }

        public string InicioTimeZone { get; set; }

        public virtual EventoBase EventoPai { get; set; }
        public virtual int? EventoPaiId { get; set; }

        public string RecurrenceRule { get; set; }

        public string RecurrenceException { get; set; }

        public string Titulo { get; set; }

        public TipoDeEventoEnum TipoDeEvento { get; set; }

        public SistemaEnum Sistema { get; set; }

        public StatusDoEventoEnum StatusDoEvento { get; set; }

        public virtual ICollection<HistoricoDoEvento> Historico { get; set; }

        public virtual Pessoa Owner { get; set; }
        public virtual int OwnerId { get; set; }

        public virtual ICollection<Pessoa> Participantes { get; set; }

        public virtual Disponibilidade Disponibilidade { get; set; }
        public virtual int DisponibilidadeId { get; set; }

        public virtual ICollection<EventoBase> Itens { get; set; }
    }
}