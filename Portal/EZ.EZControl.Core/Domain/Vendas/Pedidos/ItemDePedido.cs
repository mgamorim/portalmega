﻿using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System;

namespace EZ.EZControl.Domain.Vendas.Pedidos
{
    public class ItemDePedido : EzEntityMustHaveTenant
    {
        public ItemDePedido()
        {

        }
        public string Nome
        {
            get
            {
                if (TipoDeItemDePedido == TipoDeItemDePedidoEnum.Servico
                    && Servico != null)
                {
                    return Servico.Nome;
                }

                if (TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto
                    && Produto != null)
                {
                    return Produto.Nome;
                }

                throw new NotSupportedException(
                    "Nenhum tipo de item de pedido válido foi encontrado neste item de pedido");
            }
        }

        public decimal Valor { get; set; }
        public decimal TaxaCadastro { get; set; }

        public decimal ValorCadastro { get; set; }
        public decimal Quantidade { get; protected set; }
        public TipoDeItemDePedidoEnum TipoDeItemDePedido { get; set; }
        public virtual Pedido Pedido { get; protected set; }
        public virtual int PedidoId { get; protected set; }
        public virtual Produto Produto { get; protected set; }
        public virtual int? ProdutoId { get; protected set; }
        public virtual Servico Servico { get; protected set; }
        public virtual int? ServicoId { get; protected set; }

        #region Negócio

        public void AtribuirProduto(Produto produto, IItemDePedidoService itemDePedidoService)
        {
            itemDePedidoService.ChecarSePodeAtribuirProdutoParaItemDePedido(this, produto);
            Produto = produto;
            ProdutoId = produto.Id;
        }

        public void AtribuirServico(Servico servico, IItemDePedidoService itemDePedidoService)
        {
            itemDePedidoService.ChecarSePodeAtribuirServicoParaItemDePedido(this, servico);
            Servico = servico;
            ServicoId = servico.Id;
        }

        public void AtribuirPedido(Pedido pedido, IItemDePedidoService itemDePedidoService)
        {
            itemDePedidoService.ChecarSePodeAtribuirPedidoParaItemDePedido(this, pedido);
            Pedido = pedido;
            PedidoId = pedido.Id;
        }

        public void AtribuirQuantidade(decimal quantidade, IItemDePedidoService itemDePedidoService)
        {
            itemDePedidoService.ChecarSePodeAlterarQuantidadeDoItem(this, quantidade);
            Quantidade = quantidade;
        }

        #endregion Negócio
    }
}