﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EZ.EZControl.Domain.Vendas.Pedidos
{
    public class Pedido : EzFullAuditedEntityMustHaveTenantMustHaveEmpresa
    {
        public Pedido()
        {
            ItensDePedido = new List<ItemDePedido>();
        }
        public static Expression<Func<Pedido, ICollection<ItemDePedido>>> ItemDePedidoExpression = f => f.ItensDePedido;
        protected virtual ICollection<ItemDePedido> ItensDePedido { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual int ClienteId { get; set; }
        public PedidoStatusEnum Status { get; set; }
        public PedidoTipoEnum Tipo { get; set; }
        public FormaDePagamentoEnum FormaDePagamento { get; set; }
        public void AtribuirCliente(Cliente cliente, IPedidoService pedidoService)
        {
            pedidoService.ChecarSePodeAtribuirCliente(cliente, this);
            Cliente = cliente;
            ClienteId = cliente.Id;
        }
        public void AddItemDePedido(ItemDePedido itemDePedido, IPedidoService pedidoService)
        {
            pedidoService.ChecarSePodeAdicionarItemDePedido(itemDePedido, this);

            ItensDePedido.Add(itemDePedido);
        }
        public void RemoveItemDePedido(ItemDePedido itemDePedido, IPedidoService pedidoService)
        {
            if (pedidoService.ChecarSePodeRemoverItemDePedido(itemDePedido, this))
            {
                ItensDePedido.Remove(itemDePedido);
            }
        }
        public IReadOnlyList<ItemDePedido> ItensDePedidoReadOnly
        {
            get { return ItensDePedido.ToList(); }
        }
    }
}