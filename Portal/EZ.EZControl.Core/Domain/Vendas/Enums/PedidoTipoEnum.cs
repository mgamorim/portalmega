﻿namespace EZ.EZControl.Domain.Vendas.Enums
{
    public enum PedidoTipoEnum
    {
        Orcamento = 1,
        Reserva = 2,
        Pedido = 3
    }
}