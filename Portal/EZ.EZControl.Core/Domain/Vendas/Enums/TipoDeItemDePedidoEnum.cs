﻿namespace EZ.EZControl.Domain.Vendas.Enums
{
    public enum TipoDeItemDePedidoEnum
    {
        Produto = 1,
        Servico = 2
    }
}