﻿namespace EZ.EZControl.Domain.Vendas.Enums
{
    public enum PedidoStatusEnum
    {
        Aberto = 1,
        Cancelado = 2,
        Fechado = 3
    }
}