﻿namespace EZ.EZControl.Domain.Financeiro.Enums
{
    public enum TipoDeDocumentoFinanceiroEnum
    {
        APagar = 1,
        AReceber = 2
    }
}
