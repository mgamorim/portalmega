﻿using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Financeiro.Geral
{
    public class DocumentoBase : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int DescricaoMaxLength = 50;
        public const int ObservacoesMaxLength = 300;
        public DocumentoBase()
        {
            Competencia = DateTime.Now;
        }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public decimal Valor { get; set; }
        public DateTime Data { get; set; }
        public DateTime Competencia { get; set; }
        public string Descricao { get; set; }
        public string Observacoes { get; set; }
        public virtual TipoDeDocumentoFinanceiro TipoDeDocumentoFinanceiro { get; set; }
        public virtual int TipoDeDocumentoFinanceiroId { get; set; }
        public bool Estornado { get; set; }
        public DateTime? DataEstorno { get; set; }
        public string MotivoEstorno { get; set; }
        public DateTime Vencimento { get; set; }
        
        
    }
}
