﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.Domain.Financeiro.Geral
{
    public class DocumentoAReceber : DocumentoBase
    {
        public virtual Cliente Cliente { get; set; }
        public virtual int ClienteId { get; set; }
    }
}
