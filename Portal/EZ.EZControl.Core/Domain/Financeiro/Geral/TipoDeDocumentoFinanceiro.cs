﻿using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Financeiro.Geral
{
    public class TipoDeDocumentoFinanceiro : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int NomeMaxLength = 100;
        public TipoDeDocumentoFinanceiro()
        {
            IsActive = true;    
        }
        public string Nome { get; set; }
        public TipoDeDocumentoFinanceiroEnum Tipo { get; set; }
        public bool IsActive { get; set; }
        
        
    }
}
