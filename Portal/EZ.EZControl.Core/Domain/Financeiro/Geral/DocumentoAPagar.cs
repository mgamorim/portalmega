﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.Domain.Financeiro.Geral
{
    public class DocumentoAPagar : DocumentoBase
    {
        public virtual Fornecedor Fornecedor { get; set; }
        public virtual int FornecedorId { get; set; }
    }
}
