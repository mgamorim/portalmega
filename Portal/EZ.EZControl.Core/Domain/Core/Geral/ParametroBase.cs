﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Core.Geral
{
    public class ParametroBase : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public ParametroBase()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        //Informações de Arquivo
        public int TamanhoMaximoMb { get; set; }

        public int AlturaMaximaPx { get; set; }
        public int LarguraMaximaPx { get; set; }
        public string ExtensoesDocumento { get; set; }
        public string ExtensoesImagem { get; set; }
        
        
    }
}