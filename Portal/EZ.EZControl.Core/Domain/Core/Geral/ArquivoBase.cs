﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Core.Geral
{
    public class ArquivoBase : EzEntityMustHaveTenant, IPassivable
    {
        public const int NomeMaxLength = 100;
        public const int TokenMaxLength = 32;
        public ArquivoBase()
        {
            IsActive = true;
        }
        public string Nome { get; set; }
        public byte[] Conteudo { get; set; }
        public string Path { get; set; }
        public TipoDeArquivoEnum TipoDeArquivoFixo { get; set; }
        public bool IsActive { get; set; }
        public string Token { get; set; }
        public bool IsImagem
        {
            get
            {
                return (TipoDeArquivoFixo == TipoDeArquivoEnum.JPG || TipoDeArquivoFixo == TipoDeArquivoEnum.PNG ||
                        TipoDeArquivoFixo == TipoDeArquivoEnum.GIF);
            }
        }
    }
}
