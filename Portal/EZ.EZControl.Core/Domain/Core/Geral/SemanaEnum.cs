﻿namespace EZ.EZControl.Domain.Core.Geral
{
    public enum SemanaEnum
    {
        Domingo = 1,
        Segunda = 2,
        Terca = 3,
        Quarta = 4,
        Quinta = 5,
        Sexta = 6,
        Sabado = 7
    }
}
