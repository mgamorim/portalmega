﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Core.Geral
{
    public class Picture : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int FileNameMaxLength = 400;

        public Picture()
        {
            this.IsActive = true;
        }

        public Picture(byte[] bytes)
        {
            this.IsActive = true;
            this.Bytes = bytes;
        }

        [Required]
        public byte[] Bytes { get; set; }

        public bool IsActive { get; set; }
        
        
    }
}
