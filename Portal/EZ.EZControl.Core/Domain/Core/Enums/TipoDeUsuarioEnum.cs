﻿namespace EZ.EZControl.Domain.Core.Enums
{
    public enum TipoDeUsuarioEnum
    {
        Administradora = 1,
        Beneficiario = 2,
        Corretor = 3,
        Operadora = 4,
        Associacao = 5,
        Corretora = 6
    }
}
