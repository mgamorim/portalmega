﻿using EZ.EZControl.Attributes;

namespace EZ.EZControl.Domain.Core.Enums
{
    public enum SistemaEnum
    {
        [Sistema("Glb")]
        Global = 1,
        [Sistema("CMS")]
        CMS = 2,
        [Sistema("Agd")]
        Agenda = 3,
        [Sistema("Med")]
        EZMedical = 4,
        [Sistema("Etq")]
        Estoque = 5,
        [Sistema("Vnd")]
        Vendas = 6,
        [Sistema("Srv")]
        Servicos = 7,
        [Sistema("Fin")]
        Financeiro = 8,
        [Sistema("Sau")]
        EZLiv = 9,
        [Sistema("Pag")]
        EZPag = 10,
        [Sistema("CRM")]
        CRM = 11

    }
}