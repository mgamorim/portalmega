﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class Entrada : EzEntityMustHaveTenant, IPassivable
    {
        public Entrada()
        {
            Itens = new Collection<EntradaItem>();
        }

        public const int NotaFiscalMaxLength = 150;
        
        public bool IsActive { get; set; } 

        public virtual Empresa Empresa { get; protected set; }

        public int EmpresaId { get; set; }

        public virtual LocalArmazenamento LocalArmazenamento { get; protected set; }

        public DateTime DataPedido { get; set; }

        public DateTime DataEntrada { get; set; }

        public virtual Fornecedor Fornecedor { get; protected set; }

        public int? FornecedorId { get; set; }

        public string NotaFiscal { get; set; }

        public decimal ValorFrete { get; set; }
        
        protected virtual ICollection<EntradaItem> Itens { get; set; }

        public IReadOnlyList<EntradaItem> ItensReadOnly
        {
            get { return Itens.ToList(); }
        }

        public static Expression<Func<Entrada, ICollection<EntradaItem>>> EntradaItemExpression = f => f.Itens;

        public void AssociarEmpresa(Empresa empresa, IEntradaService entradaService)
        {
            entradaService.CanAssignEmpresa(this, empresa);
            Empresa = empresa;                         
        }
        
        public void AssociarFornecedor(Fornecedor fornecedor, IEntradaService entradaService)
        {
            entradaService.CanAssignFornecedor(this, fornecedor);
            Fornecedor = fornecedor;                         
        }
    }
}
