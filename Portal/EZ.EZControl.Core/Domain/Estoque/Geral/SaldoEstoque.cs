﻿using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System.ComponentModel.DataAnnotations;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Domain.Estoque.Enums;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class SaldoEstoque : EzEntityMustHaveTenant
    {
        public SaldoEstoque()
        {
            Consolidado = false;
            IsActive = true;
            DataAtualizacao = DateTime.Now;
        }

        /// <summary>
        /// Informa se está ativo.
        /// </summary>
        /// <description>Ativo</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool IsActive { get; set; }

        /// <summary>
        /// Informa a empresa responsável pelo lançamento.
        /// </summary>
        /// <description>Empresa</description>
        /// <required>False</required>
        /// <fieldtype>Global.SubtiposPessoa.Empresa</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        [Required]
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }

        /// <summary>
        /// Informa a data de inclusão e/ou atualização.
        /// </summary>
        /// <description>Data Atualização</description>
        /// <required>True</required>
        /// <fieldtype>DateTime</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        [Required]
        public DateTime DataAtualizacao { get; set; }

        /// <summary>
        /// Informa a origem da movimentação no saldo.
        /// </summary>
        /// <description>Origem Saldoo</description>
        /// <required>True</required>
        /// <fieldtype>Enums.OrigemSaldoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        [Required]
        public virtual OrigemMovSaldoEnum OrigemMovSaldo { get; set; }

        /// <summary>
        /// Informa o local de armazenamento do produto.
        /// </summary>
        /// <description>LocalArmazenamento</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.Estoque.Geral.LocalArmazenamento</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual LocalArmazenamento LocalArmazenamento { get; set; }
        public int? LocalArmazenamentoId { get; set; }

        /// <summary>
        /// Informa o produto.
        /// </summary>
        /// <description>Valor</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.Estoque.Geral.Produto</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        [Required]
        public virtual Produto Produto { get; set; }
        public int ProdutoId { get; set; }

        /// <summary>
        /// Informa a quantidade de entrada ou saída.
        /// </summary>
        /// <description>Quantidade</description>
        /// <required>True</required>
        /// <fieldtype>int</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        [Required]
        public decimal Quantidade { get; set; }

        /// <summary>
        /// Informa se é um saldo consolidado.
        /// </summary>
        /// <description>Consolidado</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool Consolidado { get; set; }

        /// <summary>
        /// Informa a entrada no estoque.
        /// </summary>
        /// <description>Entrada</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.Estoque.Geral.Entrada</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Entrada Entrada { get; set; }
        public int? EntradaId { get; set; }

        /// <summary>
        /// Informa a saída do estoque.
        /// </summary>
        /// <description>Saida</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.Estoque.Geral.Saida</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Saida Saida { get; set; }
        public int? SaidaId { get; set; }

        /// <summary>
        /// Informa o pedido que originou a saída do estoque.
        /// </summary>
        /// <description>Pedido</description>
        /// <required>False</required>
        /// <fieldtype>Vendas.Pedidos.Pedido</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Pedido Pedido { get; set; }
        public int? PedidoId { get; set; }

        #region Negocio
        public void AssociarEmpresa(Empresa empresa, ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAssignEmpresa(this, empresa);
            Empresa = empresa;
            EmpresaId = Empresa.Id;             
        }

        public void AssociarLocalArmazenamento(LocalArmazenamento localArmazenamento, ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAssignLocalArmazenamento(this, localArmazenamento);
            LocalArmazenamento = localArmazenamento;
        }

        public void AssociarProduto(Produto produto, ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAssignProduto(this, produto);
            Produto = produto;
        }

        public void AssociarEntrada(Entrada entrada, ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAssignEntrada(this, entrada);
            Entrada = entrada;
        }

        public void AssociarSaida(Saida saida, ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAssignSaida(this, saida);
            Saida = saida;
        }

        public void AssociarPedido(Pedido pedido, ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAssignPedido(this, pedido);
            Pedido = pedido;
        }

        public void PrepararSaida(ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAtualizarSaida(this);
            this.Quantidade = this.Quantidade * -1;
            this.OrigemMovSaldo = OrigemMovSaldoEnum.Saida;            
        }

        public void PrepararEntrada(ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAtualizarEntrada(this);            
            this.OrigemMovSaldo = OrigemMovSaldoEnum.Entrada;
        }

        public void PrepararPedido(ISaldoEstoqueService saldoEstoqueService)
        {
            saldoEstoqueService.CanAtualizarPedido(this);
            this.Quantidade = this.Quantidade * -1;
            this.OrigemMovSaldo = OrigemMovSaldoEnum.Pedido;
        }
        
        #endregion
    }
}
