﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class Saida : EzEntityMustHaveTenant, IPassivable
    {
        public Saida()
        {
            Items = new Collection<SaidaItem>();
        } 
        public bool IsActive { get; set; }
        public virtual Empresa Empresa { get; protected set; }
        public int EmpresaId { get; set; }
        public virtual Pedido Pedido { get; protected set; }
        public int? PedidoId { get; set; }
        [Required]
        public DateTime DataSaida { get; set; }
        [Required]
        public virtual MotivoSaidaEnum MotivoSaida { get; set; }
        protected virtual ICollection<SaidaItem> Items { get; set; }

        public IReadOnlyList<SaidaItem> ItensReadOnly
        {
            get { return Items.ToList(); }
        }

        public static Expression<Func<Saida, ICollection<SaidaItem>>> SaidaItemExpression = f => f.Items;

        public void AssociarEmpresa(Empresa empresa, ISaidaService entradaService)
        {
            entradaService.CanAssignEmpresa(this, empresa);
            Empresa = empresa;                         
        }
        public void AssociarPedido(Pedido pedido, ISaidaService saidaItemService)
        {
            saidaItemService.CanAssignPedido(this, pedido);
            Pedido = pedido;
        }
    }
}
