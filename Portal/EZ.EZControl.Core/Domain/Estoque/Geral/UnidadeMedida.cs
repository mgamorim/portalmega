﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class UnidadeMedida : EzEntityMustHaveTenant, IPassivable
    {
        public const int DescricaoMaxLength = 150;

        public bool IsActive { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public UnidadePadraoEnum UnidadePadrao { get; set; }
    }
}
