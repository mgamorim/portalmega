﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class EntradaItem : EzEntityMustHaveTenant, IPassivable
    {   
        public bool IsActive { get; set; }
        public virtual LocalArmazenamento LocalArmazenamento { get; protected set; }
        public int? LocalArmazenamentoId { get; set; }
        public virtual Produto Produto { get; set; }
        public int ProdutoId { get; protected set; }
        public int Quantidade { get; set; }         
        public DateTime Validade { get; set; }      
        public decimal ValorUnitario { get; set; }  
        public virtual Entrada Entrada { get; protected set; }
        public int EntradaId { get; protected set; }

        public void AssociarProduto(Produto produto, IEntradaItemService entradaItemService)
        {
            entradaItemService.CanAssignProduto(this, produto);
            Produto = produto;
            ProdutoId = produto.Id;
        }

        public void AssociarEntrada(Entrada entrada, IEntradaItemService entradaItemService)
        {
            entradaItemService.CanAssignEntrada(this, entrada);
            Entrada = entrada;
            EntradaId = entrada.Id;
        }

        public void AssociarLocalArmazenamento(LocalArmazenamento localArmazenamento, IEntradaItemService entradaItemService)
        {
            entradaItemService.CanAssignLocalArmazenamento(this, localArmazenamento);
            LocalArmazenamento = localArmazenamento;
        }
    }
}
