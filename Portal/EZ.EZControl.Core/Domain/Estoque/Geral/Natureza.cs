﻿using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class Natureza : EzEntityMustHaveTenant, IPassivable
    {
        public const int DescricaoMaxLength = 150;

        public bool IsActive { get; set; } 
        public string Descricao { get; set; }
        
        
    }
}
