﻿using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class Fracao : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int DescricaoMaxLength = 150;

        /// <summary>
        /// Informa se está ativo.
        /// </summary>
        /// <description>Ativo</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool IsActive { get; set; }
        /// <summary>
        /// Informa a descrição da fração.
        /// </summary>
        /// <description>Descrição</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public string Descricao { get; set; }
        /// <summary>
        /// Informa o valor da fração.
        /// </summary>
        /// <description>Valor</description>
        /// <required>True</required>
        /// <fieldtype>decimal</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public decimal Valor { get; set; }
        /// <summary>
        /// Informa a unidade de medida.
        /// </summary>
        /// <description>Unidade de Medida</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.Estoque.Geral.UnidadeMedida</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual UnidadeMedida UnidadeMedida { get; protected set; }
        public int? UnidadeMedidaId { get; set; }

        public void AssociarUnidadeMedida(UnidadeMedida unidadeMedida, IFracaoService fracaoService)
        {
            fracaoService.CanAssignUnidadeMedida(this, unidadeMedida);
            UnidadeMedida = unidadeMedida;
        }
    }
}
