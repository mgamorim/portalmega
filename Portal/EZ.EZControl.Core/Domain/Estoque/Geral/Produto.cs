﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class Produto : EzEntityMustHaveTenant, IPassivable
    {
        public const int NomeMaxLength = 100;

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public bool IsValorAjustavel { get; set; }
        public TipoDeProdutoEnum TipoDeProduto { get; set; }
        public virtual Natureza Natureza { get; protected set; }
        public int? NaturezaId { get; set; }
        public virtual UnidadeMedida UnidadeMedida { get; protected set; }
        public int? UnidadeMedidaId { get; set; }
        public virtual ArquivoBase Imagem { get; set; }
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }

        public void AssociarUnidadeMedida(UnidadeMedida unidadeMedida, IProdutoService produtoService)
        {
            produtoService.CanAssignUnidadeMedida(this, unidadeMedida);
            UnidadeMedida = unidadeMedida;
        }
        public void AssociarNatureza(Natureza natureza, IProdutoService produtoService)
        {
            produtoService.CanAssignNatureza(this, natureza);
            Natureza = natureza;
        }
    }
}
