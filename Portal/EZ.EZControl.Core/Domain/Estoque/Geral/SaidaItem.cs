﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Domain.Estoque.Geral
{
    public class SaidaItem : EzEntityMustHaveTenant, IPassivable
    {   
        public bool IsActive { get; set; }
        [Required]
        public virtual Saida Saida { get; protected set; }
        public int SaidaId { get; set; }
        public virtual LocalArmazenamento LocalArmazenamento { get; protected set; }        
        public int? LocalArmazenamentoId { get; set; }
        [Required]
        public virtual Produto Produto { get; protected set; }
        public int ProdutoId { get; set; }
        [Required]
        public int Quantidade { get; set; }
        [Required]
        public decimal ValorUnitario { get; set; }  
        
        public void AssociarProduto(Produto produto, ISaidaItemService saidaItemService)
        {
            saidaItemService.CanAssignProduto(this, produto);
            Produto = produto;     
        }
        public void AssociarSaida(Saida saida, ISaidaItemService saidaItemService)
        {
            saidaItemService.CanAssignSaida(this, saida);
            Saida = saida;      
        }
        public void AssociarLocalArmazenamento(LocalArmazenamento localArmazenamento, ISaidaItemService saidaItemService)
        {
            saidaItemService.CanAssignLocalArmazenamento(this, localArmazenamento);
            LocalArmazenamento = localArmazenamento;
        }
    }
}
