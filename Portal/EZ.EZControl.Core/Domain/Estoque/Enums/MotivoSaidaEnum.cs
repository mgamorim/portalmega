﻿namespace EZ.EZControl.Domain.Estoque.Enums
{
    public enum MotivoSaidaEnum
    { 
        Venda = 1,     
        Inventario = 2,
        Defeito = 3,
        Perda = 4,                            
        Outros = 99
    }
}
