﻿namespace EZ.EZControl.Domain.Estoque.Enums
{
    public enum OrigemMovSaldoEnum
    { 
        Entrada = 1,     
        Saida = 2,
        Pedido = 3
    }
}
