﻿namespace EZ.EZControl.Domain.Estoque.Enums
{
    public enum TipoDeProdutoEnum
    {
        Fisico = 1,
        Virtual = 2
    }
}
