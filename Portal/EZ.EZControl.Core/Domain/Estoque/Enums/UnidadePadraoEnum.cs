﻿namespace EZ.EZControl.Domain.Estoque.Enums
{
    public enum UnidadePadraoEnum // Padrões Internacionais exceto o de temperatura
    { 
        Quilograma = 1,     // Massa - Kg
        Litro = 2,          // Volume - L
        Metro = 3,          // Comprimento - M
        Segundo = 4,        // Tempo - s
        Ampere = 5,         // Intensidade de Corrente Elétrica - A
        Celsius = 6,        // Temperatura - ºC
        Mol = 7,            // Quantidade de Matéria - mol
        Candela = 8,        // Intensidade Luminosa - cd
        Bar = 9,            // Pressão - bar
        Outros = 10
    }
}
