﻿namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoDeTemplateDeMensagemEnum
    {
        BoasVindas = 1,
        Despedida = 2
    }
}
