﻿namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoDeArquivoEnum
    {
        PDF = 1,
        TXT = 2,
        XLS = 3,
        DOC = 4,
        JPG = 5,
        PNG = 6,
        GIF = 7,
        PPT = 8,
        JPEG = 9
    }
}