﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum EstadoCivilEnum
    {
        NaoInformado = 1,
        Solteiro = 2,
        Casado = 3,
        Separado = 4,
        Divorciado = 5,
        Viuvo = 6,
        UniaoEstavel = 7,
        Outros = 8
    }
}