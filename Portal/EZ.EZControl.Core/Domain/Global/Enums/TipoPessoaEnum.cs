﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoPessoaEnum
    {
        Fisica = 1,
        Juridica = 2
    }
}