﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoTelefoneEnum
    {
        Comercial = 1,
        Fax = 2,
        Residencial = 3,
        Celular = 4,
        Recados = 5
    }
}