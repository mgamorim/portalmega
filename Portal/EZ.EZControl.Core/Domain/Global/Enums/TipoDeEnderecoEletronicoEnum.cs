﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoDeEnderecoEletronicoEnum
    {
        Email = 1,
        Email_NFe = 2,
        EMail_Faturamento = 3,
        WebSite = 4,
        Blog = 5,
        MSN = 6,
        Twiter = 7,
        Facebook = 8,
        LinkedIn = 9,
        IMessage = 10,
        Outros = 11,
        EmailPrincipal = 12
    }
}