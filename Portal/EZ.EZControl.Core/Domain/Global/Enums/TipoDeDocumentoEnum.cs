﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoDeDocumentoEnum
    {
        Cpf = 1,
        Cnpj = 2,
        Rg = 3,
        Passaporte = 4,
        CarteiraDeTrabalho = 5,
        Pis = 6
    }
}