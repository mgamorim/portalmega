﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum EnderecoDaPessoaUtilizadoEnum
    {
        Principal = 1,
        Entrega = 2,
        Cobranca = 3,
        Nenhum = 4
    }
}