﻿namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoDeContatoEnum
    {
        Email = 1,
        Telefone = 2,
        Fax = 3,
        Outros = 4
    }
}
