﻿namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoEnderecoEnum
    {
        Comercial = 1,
        Residencial = 2,
        Correspondencia = 3,
        KitPrimeiraDocumentacao = 4
    }
}