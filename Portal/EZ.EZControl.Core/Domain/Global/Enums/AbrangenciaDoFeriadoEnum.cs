﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum AbrangenciaDoFeriadoEnum
    {
        Mundial = 1,
        Nacional = 2,
        Estadual = 3,
        Municipal = 4
    }
}