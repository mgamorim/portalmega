﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoDeFeriadoEnum
    {
        Bancario = 1,
        Classista = 2,
        Extraordinario = 3,
        Ordinario = 4,
        PontoFacultativo = 5
    }
}