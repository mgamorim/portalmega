﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum TipoLogradouroEnum
    {
        Rua = 1,
        Avenida = 2,
        Estrada = 3,
        Praca = 4
    }
}