﻿using System.ComponentModel;

namespace EZ.EZControl.Domain.Global.Enums
{
    public enum SexoEnum
    {
        NaoInformado = 1,
        Masculino = 2,
        Feminino = 3
    }
}