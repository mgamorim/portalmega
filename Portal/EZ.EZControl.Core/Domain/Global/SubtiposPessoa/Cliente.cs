﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.SubtiposPessoa
{
    public class Cliente : SubtipoPessoaEntityBase, IPassivable, IHasEmpresa
    {
        public Cliente()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}