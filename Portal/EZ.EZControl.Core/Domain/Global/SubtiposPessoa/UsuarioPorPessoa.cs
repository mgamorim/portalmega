﻿using Abp.Domain.Entities;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Global.SubtiposPessoa
{
    public class UsuarioPorPessoa : EzEntityMustHaveTenant, IPassivable
    {
        public UsuarioPorPessoa()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual User User { get; set; }

        public long UserId { get; set; }
    }
}
