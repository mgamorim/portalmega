﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Global.SubtiposPessoa
{
    public class ClienteEZ : EzEntityMustHaveTenant, IPassivable
    {
        public ClienteEZ()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        /// <summary>
        /// Pode o nome da pessoa física ou o nome fantasia da pessoa jurídica
        /// </summary>
        public string Nome { get; set; }

        public string RazaoSocial { get; set; }

        /// <summary>
        /// Pode ser CPF ou CNPJ
        /// </summary>
        public string DocumentoPrincipal { get; set; }

        // Usado porém sem referências no banco/Map
        public virtual int? PessoaId { get; set; }

        public virtual ICollection<Empresa> Empresas { get; set; }
    }
}