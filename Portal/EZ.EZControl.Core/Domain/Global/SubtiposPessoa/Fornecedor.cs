﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Global.SubtiposPessoa
{
    public class Fornecedor : SubtipoPessoaEntityBase, IPassivable, IHasEmpresa
    {
        public Fornecedor()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }

        public ICollection<RamoDoFornecedor> RamosDoFornecedor { get; set; }
    }
}