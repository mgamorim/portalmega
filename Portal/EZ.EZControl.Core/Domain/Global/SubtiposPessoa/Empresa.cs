﻿using Abp.Domain.Entities;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.Global.SubtiposPessoa
{
    public class Empresa : EzEntityMustHaveTenant, IPassivable
    {
        public const int MaxNomeFantasiaLength = 100;
        public const int MaxRazaoSocialLength = 100;
        public const int MaxSlugLength = 20;

        public Empresa()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        // Usado porém sem referências no banco/Map
        public virtual int? PessoaJuridicaId { get; set; }

        public string RazaoSocial { get; set; }

        public string NomeFantasia { get; set; }

        public string Slug { get; set; }

        public string Cnpj { get; set; }

        public virtual ClienteEZ ClienteEZ { get; set; }
        public virtual int? ClienteEZId { get; set; }

        public virtual ICollection<User> Usuarios { get; set; }
    }
}