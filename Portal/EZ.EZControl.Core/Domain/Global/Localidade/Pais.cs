﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.Global.Localidade
{
    public class Pais : EzEntityMustHaveTenant, IPassivable
    {
        public const int MaxNomeLength = 60;
        public const int MaxNacionlidadeLength = 40;
        public const int MaxCodigoISOLength = 15;
        public const int MaxCodigoInternacionaLength = 15;
        public const int MaxCodigoReceitaFederaLength = 15;
        public const int MaxMascaraDoCodigoPostaLength = 15;
        public const int MaxSigla2Caracteres = 2;
        public const int MaxSigla3Caracteres = 3;

        public Pais()
        {
            IsActive = true;
        }

        /// <summary>
        /// Ativa ou Inativa os dados cadastrados pelo usuário.
        /// Essa propriedade é da interface IPassivable
        /// </summary>
        /// <description>Ativo</description>
        /// <required>False</required>
        /// <fieldtype>Bool</fieldtype>
        public bool IsActive { get; set; }

        /// <summary>
        /// Identifica o nome do País.
        /// </summary>
        /// <description>Nome</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>60</fieldsize>
        public string Nome { get; set; }

        /// <summary>
        /// Identifica a Nacionalidade do País.
        /// </summary>
        /// <description>Nacionalidade</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>40</fieldsize>
        public string Nacionalidade { get; set; }

        /// <summary>
        /// Identifica o código do País na Receita Federal.
        /// </summary>
        /// <description>Código Receita Federal</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>15</fieldsize>
        public string CodigoReceitaFederal { get; set; }

        /// <summary>
        /// Identifica o Código Internacional do País.
        /// </summary>
        /// <description>Código Internacional</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>15</fieldsize>
        public string CodigoInternacional { get; set; }

        /// <summary>
        /// Identifica o Código ISO do País.
        /// </summary>
        /// <description>Código ISO</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>15</fieldsize>
        public string CodigoISO { get; set; }

        /// <summary>
        /// Informa o modelo da Máscara do Código Postal.
        /// </summary>
        /// <description>Máscara do Código Postal</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>15</fieldsize>
        public string MascaraDoCodigoPostal { get; set; }

        public virtual ICollection<Estado> Estados { get; protected set; }

        public virtual ICollection<Feriado> Feriados { get; protected set; }

        public string Sigla2Caracteres { get; set; }

        public string Sigla3Caracteres { get; set; }





        /// <summary>
        /// Máscara de CEP formatada para utilizar no CMMaskedTextBox
        /// </summary>
        public string MascaraCEPControle { get { return this.MascaraDoCodigoPostal != null ? this.MascaraDoCodigoPostal.Replace("#", "a").Replace("9", "#") : ""; } }
    }
}