﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.Global.Localidade
{
    public class Cidade : EzEntityMustHaveTenant, IPassivable
    {
        public const int MaxNomeLength = 60;
        public const int MaxCodigoIBGELength = 20;

        public Cidade()
        {
            IsActive = true;
        }

        /// <summary>
        /// Ativa ou Inativa os dados cadastrados pelo usuário.
        /// Essa propriedade é da interface IPassivable
        /// </summary>
        /// <description>Ativo</description>
        /// <required>False</required>
        /// <fieldtype>Bool</fieldtype>
        public bool IsActive { get; set; }

        /// <summary>
        /// Identifica o nome da cidade.
        /// </summary>
        /// <description>Nome</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>60</fieldsize>
        public string Nome { get; set; }

        /// <summary>
        /// Identifica o Código IBGE referente a cidade.
        /// </summary>
        /// <description>Código IBGE</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>7</fieldsize>
        public string CodigoIBGE { get; set; }

        /// <summary>
        /// Identifica o estado ao qual a cidade pertence.
        /// </summary>
        /// <description>Estado</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.Localidade.Estado</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Estado Estado { get; set; }
        public virtual int EstadoId { get; set; }

        public virtual ICollection<Feriado> Feriados { get; protected set; }


    }
}