﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Global.Localidade
{
    /// <summary>
    /// Classe-modelo de Tipo de Logradouro.
    /// </summary>
    public class TipoDeLogradouro : EzEntityMustHaveTenant, IPassivable
    {
        public const int MaxDescricaoLength = 60;

        public TipoDeLogradouro()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public string Descricao { get; set; }



    }
}