﻿using Abp.UI;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.Global.Localidade
{
    /// <summary>
    /// Endereço de uma Pessoa Física ou Jurídica
    /// </summary>
    public class Endereco : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int MaxBairroLength = 40;
        public const int MaxCEPLength = 8;
        public const int MaxComplementoLength = 40;
        public const int MaxDescricaoLength = 60;
        public const int MaxLogradouroLength = 80;
        public const int MaxNumeroLength = 20;
        public const int MaxReferenciaLength = 50;

        /// <summary>
        /// Construtor Padrao
        /// </summary>
        public Endereco()
        {
        }

        /// <summary>
        /// Construtor recebendo a Pessoa
        /// </summary>
        public Endereco(Pessoa.Pessoa pessoa)
        {
            this.Pessoa = pessoa;
        }

        /// <summary>
        /// Construtor recebendo o Contato
        /// </summary>
        public Endereco(Contato contato)
        {
            this.Contato = contato;
        }

        /// <summary>
        /// Construtor do Endereço de Pessoa recebendo todos os atributos
        /// </summary>
        public Endereco(Pessoa.Pessoa pessoa, string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
                        string complemento, string bairro, string cep, Cidade cidade, string referencia)
        {
            if (pessoa == null)
                throw new UserFriendlyException("Endereco.EmptyPessoaError");

            this.Pessoa = pessoa;
            this.Descricao = descricao;
            this.TipoEndereco = tipoEndereco;
            this.TipoDeLogradouro = tipoLogradouro;
            this.Logradouro = logradouro;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.CEP = cep;
            this.Cidade = cidade;
            this.Referencia = referencia;
        }

        /// <summary>
        /// Construtor do Endereço de Contato recebendo todos os atributos
        /// </summary>
        public Endereco(Pessoa.Pessoa pessoa, Contato contato, string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
                        string complemento, string bairro, string cep, Cidade cidade, string referencia)
        {
            if (pessoa == null)
                throw new UserFriendlyException("Endereco.EmptyPessoaError");

            this.Pessoa = pessoa;
            this.Contato = contato;
            this.Descricao = descricao;
            this.TipoEndereco = tipoEndereco;
            this.TipoDeLogradouro = tipoLogradouro;
            this.Logradouro = logradouro;
            this.Numero = numero;
            this.Complemento = complemento;
            this.Bairro = bairro;
            this.CEP = cep;
            this.Cidade = cidade;
            this.Referencia = referencia;
        }

        /// <summary>
        /// Pessoa Fisica ou Juridica que possui este Endereço
        /// </summary>
        public virtual Pessoa.Pessoa Pessoa { get; set; }
        public virtual int PessoaId { get; set; }

        /// <summary>
        /// Contato da Pessoa Fisica ou Juridica que possui este Endereço
        /// </summary>
        public virtual Contato Contato { get; set; }
        public virtual int? ContatoId { get; set; }

        /// <summary>
        /// Tipo Comercial ou Residencial
        /// </summary>
        /// <description>Tipo</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.TipoEnderecoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public TipoEnderecoEnum TipoEndereco { get; set; }

        /// <summary>
        /// Descrição informada pelo operador para este endereço
        /// </summary>
        /// <description>Descrição</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>40</fieldsize>
        public string Descricao { get; set; }

        /// <summary>
        /// Rua, Avenida, Estrada, etc.
        /// </summary>
        /// <description>Tipo de Logradouro</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.Localidade.TipoDeLogradouro</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual TipoDeLogradouro TipoDeLogradouro { get; set; }
        public virtual int TipoDeLogradouroId { get; set; }

        /// <summary>
        /// Informa nome da Rua, Avenida, Estrada, etc.
        /// </summary>
        /// <description>Logradouro</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>60</fieldsize>
        public string Logradouro { get; set; }

        /// <summary>
        /// Informa o número referente ao logradouro.
        /// </summary>
        /// <description>Número</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>20</fieldsize>
        public string Numero { get; set; }

        /// <summary>
        /// Informações adicionais referentes ao logradouro.
        /// Exemplo: Bloco e andar do apartamento.
        /// </summary>
        /// <description>Complemento</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>40</fieldsize>
        public string Complemento { get; set; }

        /// <summary>
        /// Informa o bairro ao qual o logradouro pertence.
        /// </summary>
        /// <description>Bairro</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>40</fieldsize>
        public string Bairro { get; set; }

        /// <summary>
        /// Informa o número do código postal do logradouro.
        /// </summary>
        /// <description>CEP</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public string CEP { get; set; }

        /// <summary>
        /// Informa a cidade a qual o logradouro pertence.
        /// </summary>
        /// <description>Cidade</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.Localidade.Cidade</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Cidade Cidade { get; set; }
        public virtual int CidadeId { get; set; }

        /// <summary>
        /// Informa um ponto de referência para localização do logradouro.
        /// </summary>
        /// <description>Referência</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>50</fieldsize>
        public string Referencia { get; set; }

        /// <summary>
        /// Indica se este endereço é um dos destinos para Entrega
        /// </summary>
        /// <description>Endereço para Entrega</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool EnderecoParaEntrega { get; set; }

        /// <summary>
        /// Lista de Telefones para este Endereco
        /// </summary>
        ///
        public virtual ICollection<Telefone> Telefones { get; set; }

        public Telefone TelefonePrincipal
        {
            get
            {
                if (this.TipoEndereco == TipoEnderecoEnum.Comercial)
                    return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Comercial);

                if (this.TipoEndereco == TipoEnderecoEnum.Residencial)
                    return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Residencial);

                return null;
            }
        }

        public string EnderecoCompleto
        {
            get
            {
                string endereco = "";

                endereco += (this.TipoDeLogradouro != null ? this.TipoDeLogradouro.Descricao + " " : "");
                endereco += this.Logradouro + " " + this.Numero + " " + this.Complemento + ", " + this.Bairro;
                endereco += (this.Cidade != null ? " - " + this.Cidade.Nome : "");

                return endereco;
            }
        }

        public string EnderecoCompletoComPais
        {
            get
            {
                string endereco = "";

                endereco += (this.TipoDeLogradouro != null ? this.TipoDeLogradouro.Descricao + " " : "");
                endereco += this.Logradouro + " " + this.Numero + " " + this.Complemento + ", " + this.Bairro;
                endereco += (this.Cidade != null ? " - " + this.Cidade.Nome : "");
                endereco += (this.Cidade != null && this.Cidade.Estado != null ? " - " + this.Cidade.Estado.Sigla : "");
                endereco += (this.Cidade != null && this.Cidade.Estado != null && this.Cidade.Estado.Pais != null ? " - " + this.Cidade.Estado.Pais.Nome : "");

                return endereco;
            }
        }

        public string LogradouroComTipo
        {
            get
            {
                if (TipoDeLogradouro != null && !string.IsNullOrEmpty(TipoDeLogradouro.Descricao))
                    return string.Format("{0} {1}", TipoDeLogradouro.Descricao, Logradouro);

                return Logradouro;
            }
        }

        #region Métodos

        public void AssociarPessoa(Pessoa.Pessoa pessoa, IEnderecoService enderecoService)
        {
            enderecoService.CanAssignPessoa(this, pessoa);
            Pessoa = pessoa;
        }

        public void AssociarContato(Contato contato, IEnderecoService enderecoService)
        {
            enderecoService.CanAssignContato(this, contato);
            Contato = contato;

        }

        public void AssociarTipoDeLogradouro(TipoDeLogradouro tipoDeLogradouro, IEnderecoService enderecoService)
        {
            enderecoService.CanAssignTipoDeLogradouro(this, tipoDeLogradouro);
            TipoDeLogradouro = tipoDeLogradouro;

        }

        public void AssociarCidade(Cidade cidade, IEnderecoService enderecoService)
        {
            enderecoService.CanAssignCidade(this, cidade);
            Cidade = cidade;
        }

        #endregion Métodos

        #region Métodos para Adicionar Coleções

        /// <summary>
        /// Método para adicionar Telefones ao Endereco
        /// </summary>
        /// <param name="telefone"></param>
        public Endereco AddTelefoneDoEndereco(Telefone telefone)
        {
            if (Telefones.Any(x =>
                (x.Id != telefone.Id) &&
                (x.DDI == telefone.DDI) &&
                (x.DDD == telefone.DDD) &&
                (x.Numero == telefone.Numero) &&
                (x.Ramal == telefone.Ramal) &&
                (x.Tipo == telefone.Tipo)))
            {
                throw new UserFriendlyException(string.Format("Telefone Tipo [{0}] já  cadastrado para este Endereço com o DDI [{1}], DDD [{2}], Número [{3}] e Ramal [{4}].",
                        telefone.Tipo.GetDescription(), telefone.DDI, telefone.DDD, telefone.Numero, telefone.Ramal));
            }

            this.Telefones.Add(telefone);
            return this;
        }

        /// <summary>
        /// Método para adicionar Telefones ao Endereco, recebendo os valores dos atributos de Telefone
        /// </summary>
        public Endereco AddTelefoneDoEndereco(TipoTelefoneEnum tipo, string ddd, string numero, string ddi = null, string ramal = null)
        {
            return AddTelefoneDoEndereco(new Telefone(this.Pessoa as PessoaFisica, tipo, ddd, numero, ddi, ramal));
        }

        /// <summary>
        /// Método para remover Telefones ao Endereco
        /// </summary>
        /// <param name="item">Um Telefone</param>
        public Endereco RemoveTelefoneDoEndereco(Telefone item)
        {
            Telefones.Remove(item);
            return this;
        }

        public static Endereco GetEndereco(ICollection<Endereco> enderecos, TipoEnderecoEnum tipoEndereco)
        {
            if (enderecos != null)
                return enderecos
                    .OrderBy(x => x.Id)
                    .FirstOrDefault(x => x.TipoEndereco == tipoEndereco);
            else
                return null;
        }

        #endregion Métodos para Adicionar Coleções
    }
}