﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.Global.Localidade
{
    public class Estado : EzEntityMustHaveTenant, IPassivable
    {
        public const int MaxNomeLength = 60;
        public const int MaxSiglaLength = 2;
        public const int MaxCapitalLength = 60;
        public const int MaxCodigoIBGELength = 3;

        public Estado()
        {
            IsActive = true;
        }

        /// <summary>
        /// Ativa ou Inativa os dados cadastrados pelo usuário.
        /// Essa propriedade é da interface IPassivable
        /// </summary>
        /// <description>Ativo</description>
        /// <required>False</required>
        /// <fieldtype>Bool</fieldtype>
        public bool IsActive { get; set; }

        /// <summary>
        /// Informa o nome do Estado.
        /// </summary>
        /// <description>Nome</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>60</fieldsize>
        public string Nome { get; set; }

        /// <summary>
        /// Informa a sigla referente ao Estado.
        /// </summary>
        /// <description>Sigla</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>10</fieldsize>
        public string Sigla { get; set; }

        /// <summary>
        /// Informa a Capital do Estado.
        /// </summary>
        /// <description>Capital</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>60</fieldsize>
        public string Capital { get; set; }

        /// <summary>
        /// Identifica o Código IBGE referente a cidade.
        /// </summary>
        /// <description>Código IBGE</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>3</fieldsize>
        public string CodigoIBGE { get; set; }

        /// <summary>
        /// Informa o País referente ao Estado.
        /// </summary>
        /// <description>País</description>
        /// <required>True</required>
        /// <fieldtype>List<EZ.EZControl.Domain.Localidade.Pais/></fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Pais Pais { get; set; }
        public virtual int PaisId { get; set; }




        public virtual ICollection<Cidade> Municipios { get; protected set; }

        public virtual ICollection<Feriado> Feriados { get; protected set; }
    }
}