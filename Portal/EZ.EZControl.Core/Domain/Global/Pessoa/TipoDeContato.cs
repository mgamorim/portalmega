﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    /// <summary>
    /// Classe-modelo de tipo de contato.
    /// </summary>
    public class TipoDeContato : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int MaxDescricaoLength = 60;
        public const int MaxMascaraLength = 100;


        public TipoDeContato()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        
        

        /// <summary>
        /// Descrição para definição de um cadastro.
        /// </summary>
        /// <description>Descrição</description>
        /// <required>True</required>
        /// <fieldtype>String</fieldtype>
        /// <fieldsize>60</fieldsize>
        public virtual String Descricao { get; set; }

        /// <summary>
        /// Máscara do documento: # para qualquer caracter, 9 para somente números
        /// </summary>
        /// <description>Máscara</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>30</fieldsize>
        public String Mascara { get; set; }

        /// <summary>
        /// Máscara do documento formatada
        /// </summary>
        public string MaskedValue { get { return this.Mascara != null ? this.Mascara.Replace("#", "a").Replace("9", "#") : ""; } }

        /// <summary>
        /// Tipo de contato fixado pelos valores do enumerado
        /// </summary>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.Global.Enums.TipoDeContatoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>///
        public TipoDeContatoEnum TipoDeContatoFixo { get; set; }
    }
}