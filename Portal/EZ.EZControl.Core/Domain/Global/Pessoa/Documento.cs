﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using Abp.UI;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class Documento : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int MaxNumeroLength = 20;
        public const int MaxOrgaoExpedidorLength = 20;
        public const int MaxUFLength = 2;
        public const int MaxSerieLength = 20;

        public Documento()
        {
            //this.ImagemDoDocumento = new ImagemDoDocumento();
        }

        public Documento(Pessoa pessoa, TipoDeDocumento tipoDeDocumento, string numero,
            string orgaoExpedidor = null, DateTime? dataExpedicao = null, Estado estado = null,
            DateTime? dataDeValidade = null, string serie = null)
            : this()
        {
            if (pessoa == null)
                throw new UserFriendlyException("Documento.EmptyPessoaError");

            if (tipoDeDocumento == null)
                throw new UserFriendlyException("Documento.EmptyTipoDeDocumentoError");

            if (string.IsNullOrEmpty(numero))
                throw new UserFriendlyException("Documento.EmptyNumeroError");

            this.Pessoa = pessoa;
            this.TipoDeDocumento = tipoDeDocumento;
            this.Numero = numero;
            this.OrgaoExpedidor = orgaoExpedidor;
            this.DataExpedicao = dataExpedicao;
            this.Estado = estado;
            this.DataDeValidade = dataDeValidade;
            this.Serie = serie;
        }

        public virtual Pessoa Pessoa { get; protected set; }
        public virtual int PessoaId { get; protected set; }

        /// <summary>
        /// Informa o tipo de documento da pessoa.
        /// </summary>
        /// <description>Tipo de Documento</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.TipoDeDocumento</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Domain.Global.Pessoa.TipoDeDocumento TipoDeDocumento { get; protected set; }
        public virtual int TipoDeDocumentoId { get; protected set; }

        /// <summary>
        /// Informa o número do documento.
        /// </summary>
        /// <description>Número</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public string Numero { get; set; }

        /// <summary>
        /// Informa o órgão de expedição do documento.
        /// </summary>
        /// <description>Órgão Expedidor</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>20</fieldsize>
        public string OrgaoExpedidor { get; set; }

        /// <summary>
        /// Informa a data em que o documento foi expedido.
        /// </summary>
        /// <description>Data de Expedição</description>
        /// <required>False</required>
        /// <fieldtype>Date</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public DateTime? DataExpedicao { get; set; }

        /// <summary>
        /// Informa o estado de federação responsável pela expedição do documento.
        /// </summary>
        /// <description>Estado</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>2</fieldsize>
        public virtual Estado Estado { get; set; }
        public virtual int? EstadoId { get; set; }

        /// <summary>
        /// Informa a data de validade do documento.
        /// </summary>
        /// <description>Data de Validade</description>
        /// <required>False</required>
        /// <fieldtype>Date</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public DateTime? DataDeValidade { get; set; }

        /// <summary>
        /// Informa o número de série do documento.
        /// Exemplo: Carteira de Trabalho.
        /// </summary>
        /// <description>Série</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>20</fieldsize>
        public string Serie { get; set; }

        /// <summary>
        /// Arquivo de Imagem do Documento
        /// </summary>
        /// <description>Imagem do Documento</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.ImagemDoDocumento</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        //public  ImagemDoDocumento ImagemDoDocumento { get; set; }

        
        

        #region Métodos

        public void AssociarPessoa(Pessoa pessoa, IDocumentoService documentoService)
        {
            documentoService.CanAssignPessoa(this, pessoa);
           
           Pessoa = pessoa;
           PessoaId = pessoa.Id;
           


        }

        public void AssociarTipoDocumento(TipoDeDocumento tipoDeDocumento, IDocumentoService documentoService)
        {
            documentoService.CanAssignTipoDocumento(this, tipoDeDocumento);
            
            TipoDeDocumento = tipoDeDocumento;
            TipoDeDocumentoId = tipoDeDocumento.Id;
           
        }

        #endregion Métodos
    }
}