﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public abstract class SubtipoPessoaFisicaEntityBase : EzEntityMustHaveTenant
    {
        public virtual PessoaFisica PessoaFisica { get; set; }
        public int PessoaFisicaId { get; set; }
        public Pessoa Pessoa { get { return PessoaFisica as Pessoa; } }
        public string NomePessoa { get { return this.Pessoa.ToString(); } }
    }
}