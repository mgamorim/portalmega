﻿using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class Telefone : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int MaxDDILength = 2;
        public const int MaxDDDLength = 2;
        public const int MaxNumeroLength = 9;
        public const int MaxRamalLength = 10;

        public const string RegexTelefoneCelularBrComMascaraPattern = @"^(?<DDD>\([0-9]{2}\))\s(?<CAMPO1>[0-9]{1})?(?<CAMPO2>[0-9]{4})-(?<CAMPO3>[0-9]{4})$"; //(21) 95151-5158
        public const string RegexTelefoneCelularBrSemMascaraPattern = @"^(?<DDD>[0-9]{2})?(?<CAMPO1>[0-9]{1})?(?<CAMPO2>[0-9]{4})(?<CAMPO3>[0-9]{4})$"; //21951515158

        public Telefone()
        {
        }

        public Telefone(Pessoa pessoa, TipoTelefoneEnum tipo, string ddd, string numero, string ddi, string ramal)
            : this()
        {
            if (pessoa == null)
                throw new UserFriendlyException("Telefone.EmptyPessoaError");

            this.Pessoa = pessoa;
            this.PessoaId = pessoa.Id;
            this.Tipo = tipo;
            this.DDD = ddd;
            this.Numero = numero;
            this.DDI = ddi;
            this.Ramal = ramal;
        }

        public virtual Pessoa Pessoa { get; set; }
        public virtual int PessoaId { get; set; }


        /// <summary>
        /// Informa o tipo de telefone da pessoa.
        /// </summary>
        /// <description>Tipo de Telefone</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.TipoTelefoneEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public TipoTelefoneEnum Tipo { get; set; }

        /// <summary>
        /// Informa o código telefônico para realizar chamadas internacionais.
        /// </summary>
        /// <description>DDI</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>5</fieldsize>
        public string DDI { get; set; }

        /// <summary>
        /// Informa o código telefônico para realizar chamadas para outras cidades ou estados.
        /// </summary>
        /// <description>DDD</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>3</fieldsize>
        public string DDD { get; set; }

        /// <summary>
        /// Informa o número de telefone da pessoa.
        /// </summary>
        /// <description>Número</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>10</fieldsize>
        public string Numero { get; set; }

        /// <summary>
        /// Informa o ramal do telefone da pessoa.
        /// </summary>
        /// <description>Ramal</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>10</fieldsize>
        public string Ramal { get; set; }

        public static Telefone GetTelefone(ICollection<Telefone> telefones, TipoTelefoneEnum tipoTelefone)
        {
            if (telefones != null)
                return telefones
                    .OrderBy(x => x.Id)
                    .FirstOrDefault(x => x.Tipo == tipoTelefone);
            else
                return null;
        }

        public string Tostring()
        {
            return string.Format("{0} {1} {2}", DDI, DDD, Numero);
        }

        #region Métodos auxiliares

        public static bool IsValidTelefoneCelularBrComMascara(string numero)
        {
            if (!string.IsNullOrEmpty(numero))
            {
                Regex regex = new Regex(RegexTelefoneCelularBrComMascaraPattern);
                Match match = regex.Match(numero);
                return match.Success;
            }
            return false;
        }

        public static bool IsValidTelefoneCelularBrSemMascara(string numero)
        {
            if (!string.IsNullOrEmpty(numero))
            {
                Regex regex = new Regex(RegexTelefoneCelularBrSemMascaraPattern);
                Match match = regex.Match(numero);
                return match.Success;
            }
            return false;
        }

        #endregion

        public void AssociarPessoa(Pessoa pessoa, ITelefoneService telefoneService)
        {
            telefoneService.CanAssignPessoa(this, pessoa);
            Pessoa = pessoa;
            PessoaId = pessoa.Id;

        }
    }
}