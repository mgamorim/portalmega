﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    /// <summary>
    /// Classe-modelo de tratamento de pessoa.
    /// </summary>
    public class Tratamento : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int MaxDescricaoLength = 30;

        public Tratamento()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        
        
    }
}