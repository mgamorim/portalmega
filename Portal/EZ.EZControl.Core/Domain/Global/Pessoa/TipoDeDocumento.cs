﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    /// <summary>
    /// Classe-modelo de tipo de d. Possui uma descrição e um nível de prioridade.
    /// Pode possuir uma máscara, ser o documento principal e ter data, órgão e UF de expedição obrigatórios.
    /// </summary>
    public class TipoDeDocumento : EzEntityMustHaveTenant, IPassivable
    {
        public const int MaxDescricaoLength = 40;
        public const int MaxMascaraLength = 30;

        /// <summary>
        /// Construtor com estados iniciais válidos
        /// </summary>
        public TipoDeDocumento()
        {
            Principal = false;
            ObrigaDataExpedicao = false;
            ObrigaOrgaoExpedidor = false;
            ObrigaDataDeValidade = false;
            ObrigaUF = false;
            SerieObrigatoria = false;
            ListaEmOutrosDocumentos = false;
            ExibirNoCadastroSimplificadoDePessoa = false;
            ObrigaAnexo = false;
            PrazoDeAtualizacaoDoAnexo = 0;
            PermiteLancamentoDuplicado = false;
            IsActive = true;
        }

        public TipoDeDocumento(TipoDeDocumentoEnum tipoDeDocumentoEnum)
        {
            TipoDeDocumentoFixo = tipoDeDocumentoEnum;
        }

        public bool IsActive { get; set; }

        /// <summary>
        /// Tipo de pessoa (Física ou Jurídica) do documento
        /// </summary>
        /// <description>Tipo de Pessoa</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.TipoPessoaEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public TipoPessoaEnum TipoPessoa { get; set; }

        /// <summary>
        /// Tipo de documento fixado pelos valores do enumerado
        /// </summary>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.Global.Enums.TipoDeDocumentoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>///
        public TipoDeDocumentoEnum TipoDeDocumentoFixo { get; set; }

        public string Descricao { get; set; }

        /// <summary>
        /// Máscara do documento: # para qualquer caracter, 9 para somente números
        /// </summary>
        /// <description>Máscara</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>30</fieldsize>
        public string Mascara { get; set; }

        /// <summary>
        /// Máscara do documento formatada para utilizar no CMMaskedTextBox
        /// </summary>
        public string MaskedValue { get { return this.Mascara != null ? this.Mascara.Replace("#", "a").Replace("9", "#") : ""; } }

        /// <summary>
        /// Informa se é o documento principal
        /// </summary>
        /// <description>Documento Principal</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool Principal { get; set; }

        /// <summary>
        /// Indica se o Tipo de Documentação Obriga Anexo. O Teste é Feito nos Sistemas que tratam desse Atributo, como por Exemplo, a Gerência de Aquisições.
        /// </summary>
        /// <description>Obriga Indicação do Anexo</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ObrigaAnexo { get; set; }

        /// <summary>
        /// Indica o Prazo em Meses em que o Documento do Pessoa deverá ser atualizado (através do envio de um novo anexo).
        /// VALIDAÇÂO: Só deve ser informado caso o documento Obrigue Anexo.
        /// </summary>
        /// <description>Prazo de Atualização (Meses)</description>
        /// <required>False</required>
        /// <fieldtype>int</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public int PrazoDeAtualizacaoDoAnexo { get; set; }

        /// <summary>
        /// Prioridade do tipo documento caso exista mais de um documento principal cadastrado para o mesmo tipo de pessoa
        /// </summary>
        /// <description>Prioridade</description>
        /// <required>False</required>
        /// <fieldtype>int</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public int Prioridade { get; set; }

        /// <summary>
        /// Informa se o órgão expedidor do documento é obrigatório
        /// </summary>
        /// <description>Habilitar órgão expedidor</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ObrigaOrgaoExpedidor { get; set; }

        /// <summary>
        /// Informa se a data de expedição do documento é obrigatória
        /// </summary>
        /// <description>Habilitar data de expedição</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ObrigaDataExpedicao { get; set; }

        /// <summary>
        /// Informa se a unidade federativa de expedição do documento é obrigatória
        /// </summary>
        /// <description>Habilitar UF de expedição</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ObrigaUF { get; set; }

        /// <summary>
        /// Informa se a data de validade do documento é obrigatória
        /// </summary>
        /// <description>Habilitar data de validade</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ObrigaDataDeValidade { get; set; }

        /// <summary>
        /// Informa se a série do documento é obrigatória.
        /// </summary>
        /// <description>Habilitar série</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool SerieObrigatoria { get; set; }

        /// <summary>
        /// Informa se o documento pode aparecer na lista de outros documentos.
        /// </summary>
        /// <description>Ativo</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ListaEmOutrosDocumentos { get; set; }

        /// <summary>
        /// Informa se o documento será exibido no cadastro simplificado de pessoa.
        /// </summary>
        /// <description>Exibir no cadastro simplificado de pessoa</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool ExibirNoCadastroSimplificadoDePessoa { get; set; }

        /// <summary>
        /// A Inscrição Estadual de Substituto Tributário pode ser informada mais de uma vez para a mesma pessoa.
        /// </summary>
        public bool PermiteLancamentoDuplicado { get; set; }



    }
}