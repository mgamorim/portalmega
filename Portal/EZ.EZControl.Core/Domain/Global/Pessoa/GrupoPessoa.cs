﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class GrupoPessoa : EzEntityRecursiveMustHaveTenant<GrupoPessoa>, IEzEntity, IPassivable
    {
        public const int MaxNomeLength = 60;
        public const int MaxSlugLength = 20;

        #region Construtores

        public GrupoPessoa()
        {
            IsActive = true;
            TipoDePessoa = TipoPessoaEnum.Fisica;
        }

        internal GrupoPessoa(string descricao, bool isActive)
        {
            this.Descricao = descricao;
            this.IsActive = isActive;
        }

        internal GrupoPessoa(string descricao)
            : this(descricao, true)
        { }

        #endregion Construtores

        #region Propriedades

        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        public TipoPessoaEnum TipoDePessoa { get; set; }

        public string Slug { get; set; }
        
        #endregion Propriedades
    }
}