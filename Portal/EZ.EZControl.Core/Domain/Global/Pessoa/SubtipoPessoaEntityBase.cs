﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public abstract class SubtipoPessoaEntityBase : EzEntityMustHaveTenant
    {
        #region Constructors

        protected SubtipoPessoaEntityBase()
        {
        }

        #endregion Constructors

        public virtual Pessoa Pessoa { get; set; }
        public virtual int PessoaId { get; set; }
        
        
        public bool IsPessoaFisica { get { if (this.Pessoa != null) return this.Pessoa.IsPessoaFisica; else return false; } }

        public bool IsPessoaJuridica { get { if (this.Pessoa != null) return this.Pessoa.IsPessoaJuridica; else return false; } }

        public TipoPessoaEnum TipoPessoa { get { if (this.Pessoa != null) return this.Pessoa.TipoPessoa; else return TipoPessoaEnum.Fisica; } }

        public string NomePessoa { get { if (this.Pessoa != null) return this.Pessoa.NomePessoa; else return string.Empty; } }

        public string RazaoSocialPessoa { get { if (this.Pessoa != null) return this.Pessoa.RazaoSocialPessoa; else return string.Empty; } }

        public PessoaFisica PessoaFisica
        {
            get { return this.IsPessoaFisica ? (this.Pessoa as PessoaFisica) : null; }
            set { this.Pessoa = value; }
        }

        public PessoaJuridica PessoaJuridica
        {
            get { return this.IsPessoaJuridica ? (this.Pessoa as PessoaJuridica) : null; }
            set { this.Pessoa = value; }
        }
    }
}