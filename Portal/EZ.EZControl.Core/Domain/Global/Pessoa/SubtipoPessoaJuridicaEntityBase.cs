﻿using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public abstract class SubtipoPessoaJuridicaEntityBase : EzEntityMustHaveTenant
    {
        public virtual PessoaJuridica PessoaJuridica { get; set; }
        public virtual int PessoaJuridicaId { get; set; }
        public Pessoa Pessoa { get { return PessoaJuridica as Pessoa; } }
        public string NomePessoa { get { return this.Pessoa.ToString(); } }
    }
}