﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Linq;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class PessoaFisica : Pessoa
    {
        // O tamanho do nome deve ser igual ao tamanho do nome fantasia da pessoa jurídica
        public const int MaxNomeLength = 100;

        #region Constructor

        public PessoaFisica()
            : base()
        {
            this.Sexo = SexoEnum.NaoInformado;
            this.EstadoCivil = EstadoCivilEnum.NaoInformado;
        }

        public PessoaFisica(string nome, GrupoPessoa grupoPessoa)
            : this()
        {
            this.Nome = nome;
            this.GrupoPessoa = grupoPessoa;
        }

        #endregion Constructor

        #region Properties Geral

        /// <summary>
        /// Informa o nome da pessoa física.
        /// </summary>
        /// <description>Nome</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>50</fieldsize>
        public string Nome { get; set; }

        /// <summary>
        /// Informa o tratamento da pessoa física.
        /// </summary>
        /// <description>Tratamento</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.Tratamento</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Tratamento Tratamento { get; protected set; }
        public virtual int? TratamentoId { get; protected set; }

        /// <summary>
        /// Informa se a pessoa é do sexo feminino, masculino ou não informado.
        /// </summary>
        /// <description>Sexo</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.SexoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public SexoEnum? Sexo { get; set; }

        /// <summary>
        /// Informa se a pessoa física possui filhos.
        /// </summary>
        /// <description>Possui Filhos</description>
        /// <required>True</required>
        /// <fieldtype>bool</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public bool PossuiFilhos { get; set; }

        /// <summary>
        /// Informa o estado civil da pessoa.
        /// </summary>
        /// <description>Estado Civil</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.EstadoCivilEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public EstadoCivilEnum? EstadoCivil { get; set; }

        /// <summary>
        /// Informa a data de nescimento da pessoa.
        /// </summary>
        /// <description>Nascimento</description>
        /// <required>False</required>
        /// <fieldtype>Date</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public DateTime? DataDeNascimento { get; set; }

        /// <summary>
        /// Informa a nacionalidade da pessoa.
        /// </summary>
        /// <description>Nacionalidade</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>N/A</fieldsize>

        public string Nacionalidade { get; set; }

        public int CpfId
        {
            get
            {
                var doc = (this.Documentos != null && this.Documentos.Any()) ? this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf) : null;
                return (doc != null) ? doc.Id : default(int);
            }
        }

        public string Cpf
        {
            get
            {
                var doc = (this.Documentos != null && this.Documentos.Any()) ? this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf) : null;
                return (doc != null) ? doc.Numero : string.Empty;
            }
        }

        public int RgId
        {
            get
            {
                var doc = (this.Documentos != null && this.Documentos.Any()) ? this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Rg) : null;
                return (doc != null) ? doc.Id : default(int);
            }
        }

        public string Rg
        {
            get
            {
                var doc = (this.Documentos != null && this.Documentos.Any()) ? this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Rg) : null;
                return (doc != null) ? doc.Numero : string.Empty;
            }
        }

        public string OrgaoExpedidor
        {
            get
            {
                var doc = (this.Documentos != null && this.Documentos.Any()) ? this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Rg) : null;
                return (doc != null) ? doc.OrgaoExpedidor : string.Empty;
            }
        }


        public string identidade { get; set; }

        public string identidademae { get; set; }

        public string CPFmae { get; set; }


        #endregion Properties Geral

        #region Métodos

        public void AssociarTratamento(Tratamento tratamento, IPessoaFisicaService pessoaFisicaService)
        {
            pessoaFisicaService.CanAssignTratamento(this, tratamento);
            
            Tratamento = tratamento;
            
        }

        #endregion Métodos

        #region Métodos para retornar elementos específicos das Coleções

        public Telefone TelefoneResidencial
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Residencial); }
        }

        #endregion Métodos para retornar elementos específicos das Coleções
    }
}