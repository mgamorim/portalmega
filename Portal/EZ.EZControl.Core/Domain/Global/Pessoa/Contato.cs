﻿using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Abp.UI;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class Contato : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int MaxNomeLength = 50;
        public const int MaxCargoLength = 20;
        public const int MaxSetorLength = 20;
        public const int MaxIdiomaLength = 20;
        public const int MaxObservacaoLength = 1000;

        /// <summary>
        /// Construtor Padrao
        /// </summary>
        public Contato()
        {
        }

        /// <summary>
        /// Construtor recebendo a Pessoa
        /// </summary>
        public Contato(Pessoa pessoa, IContatoService contatoService)
        {
            AssociarPessoa(pessoa, contatoService);
        }

        public Contato(Pessoa pessoa, TipoDeContato tipoDeContato, IContatoService contatoService)
            : this(pessoa, contatoService)
        {
            AssociarTipoDeContato(tipoDeContato, contatoService);
        }

        public Contato(Pessoa pessoa, TipoDeContato tipoDeContato, Tratamento tratamento, IContatoService contatoService)
            : this(pessoa, tipoDeContato, contatoService)
        {
            AssociarTratamento(tratamento, contatoService);
        }

        /// <summary>
        /// Construtor recebendo todos os atributos
        /// </summary>
        public Contato(Pessoa pessoa, string nome, TipoDeContato tipo, Tratamento tratamento, SexoEnum sexo, DateTime? dataNascimento,
                       string cargo, string setor, string idioma, string observacao, IContatoService contatoService)
            : this(pessoa, tipo, tratamento, contatoService)
        {
            if (pessoa == null)
                throw new UserFriendlyException("Contato.EmptyPessoaError");
                

            if (tipo == null)
                throw new UserFriendlyException("Contato.EmptyTipoDeContatoError");

            this.Nome = nome;
            this.DataNascimento = dataNascimento;
            this.Sexo = sexo;
            this.Cargo = cargo;
            this.Setor = setor;
            this.Idioma = idioma;
            this.Observacao = observacao;
        }

        /// <summary>
        /// Pessoa Fisica ou Juridica que possui este Endereço
        /// </summary>
        public virtual Pessoa Pessoa { get; protected set; }
        public virtual int PessoaId { get; protected set; }

        /// <summary>
        /// Informa o tipo de contato.
        /// </summary>
        /// <description>Tipo</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.TipoDeContato</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual TipoDeContato TipoDeContato { get; protected set; }
        public virtual int TipoDeContatoId { get; protected set; }

        /// <summary>
        /// Informa o tipo de tratamento do contato.
        /// </summary>
        /// <description>Tratamento</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.Tratamento</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public virtual Tratamento Tratamento { get; protected set; }
        public virtual int? TratamentoId { get; protected set; }

        /// <summary>
        /// Informa o nome do contato.
        /// </summary>
        /// <description>Nome</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>50</fieldsize>
        public string Nome { get; set; }

        /// <summary>
        /// Informa a data de nascimento do contato.
        /// </summary>
        /// <description>Nome</description>
        /// <required>True</required>
        /// <fieldtype>Date</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public DateTime? DataNascimento { get; set; }

        /// <summary>
        /// Informa se o contato é do sexo feminino, masculino ou não informado.
        /// </summary>
        /// <description>Sexo</description>
        /// <required>False</required>
        /// <fieldtype>EZ.EZControl.Domain.SexoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public SexoEnum Sexo { get; set; }

        /// <summary>
        /// Informa o cargo do contato.
        /// </summary>
        /// <description>Cargo</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>20</fieldsize>
        public string Cargo { get; set; }

        /// <summary>
        /// Informa o setor do cargo do contato.
        /// </summary>
        /// <description>Setor</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>20</fieldsize>
        public string Setor { get; set; }

        /// <summary>
        /// Informa o idioma nativo do contato.
        /// </summary>
        /// <description>Idioma</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>20</fieldsize>
        public string Idioma { get; set; }

        /// <summary>
        /// Campo reservado para informações extras sobre o contato.
        /// </summary>
        /// <description>Observação</description>
        /// <required>False</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>1000</fieldsize>
        public string Observacao { get; set; }

        public virtual ICollection<Telefone> Telefones { get; set; }

        public virtual ICollection<EnderecoEletronico> EnderecosEletronicos { get; set; }

        public virtual ICollection<Endereco> Enderecos { get; set; }

        public virtual Endereco EnderecoPrincipal { get; set; }
        public virtual int? EnderecoPrincipalId { get; set; }



        #region Métodos

        public void AssociarPessoa(Pessoa pessoa, IContatoService contatoService)
        {
            contatoService.CanAssignPessoa(this, pessoa);
            Pessoa = pessoa;
            PessoaId = pessoa.Id;
        }

        public void AssociarTipoDeContato(TipoDeContato tipoDeContato, IContatoService contatoService)
        {
           contatoService.CanAssignTipoDeContato(this, tipoDeContato);
           TipoDeContato = tipoDeContato;
           TipoDeContatoId = tipoDeContato.Id;
           
        }

        public void AssociarTratamento(Tratamento tratamento, IContatoService contatoService)
        {
           contatoService.CanAssignTratamento(this, tratamento);
           Tratamento = tratamento;
           TratamentoId = tratamento.Id;
        }

        #endregion Métodos

        #region Métodos para retornar elementos específicos das Coleções

        public string DescricaoContato
        {
            get
            {
                var descricao = string.Empty;
                descricao += this.Tratamento != null ? string.Format("{0}{1}", this.Tratamento.Descricao, " ") : string.Empty;
                descricao += !string.IsNullOrEmpty(this.Nome) ? string.Format("{0}{1}", this.Nome, " - ") : string.Empty;
                descricao += this.EmailPessoal != null ? string.Format("{0}{1}", this.EmailPessoal.Endereco, " - ") : string.Empty;
                descricao += this.TelefoneResidencial != null ? string.Format("{0}{1}{2}{3}", "(", this.TelefoneResidencial.Tostring(), ")", " ") : string.Empty;
                descricao += this.TelefoneCelular != null ? string.Format("{0}{1}{2}{3}", "(", this.TelefoneCelular.Tostring(), ")", " ") : string.Empty;
                descricao += this.TelefoneComercial != null ? string.Format("{0}{1}{2}", "(", this.TelefoneComercial.Tostring(), ")") : string.Empty;
                return descricao;
            }
        }

        public EnderecoEletronico EmailPessoal
        {
            get { return EnderecoEletronico.GetEnderecoEletronico(EnderecosEletronicos, TipoDeEnderecoEletronicoEnum.EmailPrincipal); }
        }

        public Telefone TelefoneComercial
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Comercial); }
        }

        public Telefone TelefoneCelular
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Celular); }
        }

        public Telefone TelefoneResidencial
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Residencial); }
        }

        public EnderecoDaPessoaUtilizadoEnum EnderecoDaPessoaUtilizado
        {
            get
            {
                if (this.EnderecoPrincipal != null && this.Pessoa != null)
                {
                    if (this.EnderecoPrincipal == this.Pessoa.EnderecoPrincipal)
                        return EnderecoDaPessoaUtilizadoEnum.Principal;
                    else if (this.EnderecoPrincipal == this.Pessoa.EnderecoEntrega)
                        return EnderecoDaPessoaUtilizadoEnum.Entrega;
                    else if (this.EnderecoPrincipal == this.Pessoa.EnderecoCobranca)
                        return EnderecoDaPessoaUtilizadoEnum.Cobranca;
                    else
                        return EnderecoDaPessoaUtilizadoEnum.Nenhum;
                }
                else
                    return EnderecoDaPessoaUtilizadoEnum.Nenhum;
            }
            set
            {
                if (this.Pessoa != null)
                {
                    if (value == EnderecoDaPessoaUtilizadoEnum.Principal)
                        this.EnderecoPrincipal = this.Pessoa.EnderecoPrincipal;
                    else if (value == EnderecoDaPessoaUtilizadoEnum.Entrega)
                        this.EnderecoPrincipal = this.Pessoa.EnderecoEntrega;
                    else if (value == EnderecoDaPessoaUtilizadoEnum.Cobranca)
                        this.EnderecoPrincipal = this.Pessoa.EnderecoCobranca;
                    else
                        this.EnderecoPrincipal = null;
                }
                else
                    this.EnderecoPrincipal = null;
            }
        }

        #endregion Métodos para retornar elementos específicos das Coleções

        #region Métodos para Adicionar Coleções

        public Contato AddTelefoneDoContato(Telefone item)
        {
            if (Telefones.Where(x => (x.Id != item.Id) && (x.DDI == item.DDI) && (x.DDD == item.DDD) && (x.Numero == item.Numero) && (x.Ramal == item.Ramal) && (x.Tipo == item.Tipo)).Count() > 0)
                throw new UserFriendlyException(string.Format("Telefone Tipo [{0}] já  cadastrado para este Contato com o DDI [{1}], DDD [{2}], Número [{3}] e Ramal [{4}].", item.Tipo.GetDescription(), item.DDI, item.DDD, item.Numero, item.Ramal));

            Telefones.Add(item);
            return this;
        }

        public Contato AddTelefoneDoContato(TipoTelefoneEnum tipo, string ddd, string numero, string ddi = null, string ramal = null)
        {
            return AddTelefoneDoContato(new Telefone(this.Pessoa as PessoaFisica, tipo, ddd, numero, ddi, ramal));
        }

        public Contato RemoveTelefoneDoContato(Telefone item)
        {
            Telefones.Remove(item);
            return this;
        }

        public Contato AddEnderecoEletronicoDoContato(EnderecoEletronico item)
        {
            if (EnderecosEletronicos.Where(x => (x.Id != item.Id) && (x.Endereco == item.Endereco) && (x.TipoDeEnderecoEletronico == item.TipoDeEnderecoEletronico)).Count() > 0)
                throw new UserFriendlyException(string.Format("{0} já cadastrado para este Contato com o Endereço [{1}].", item.TipoDeEnderecoEletronico.GetDescription(), item.Endereco));

            EnderecosEletronicos.Add(item);
            return this;
        }

        public Contato AddEnderecoEletronicoDoContato(TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
        {
            return AddEnderecoEletronicoDoContato(new EnderecoEletronico(this.Pessoa, tipoDeEnderecoEletronico, endereco));
        }

        public Contato RemoveEnderecoEletronicoDoContato(EnderecoEletronico item)
        {
            EnderecosEletronicos.Remove(item);
            return this;
        }

        public Contato AddEnderecoDoContato(Endereco item)
        {
            if (Enderecos.FirstOrDefault(x => x.Id == item.Id) == null)
            {
                if (Enderecos.Count(x => x.Descricao == item.Descricao && x.TipoEndereco == item.TipoEndereco && x.TipoDeLogradouro == item.TipoDeLogradouro && x.Logradouro == item.Logradouro && x.Numero == item.Numero) > 0)
                    throw new UserFriendlyException("Este Endereço já foi cadastrado.");

                item.Contato = this;
                Enderecos.Add(item);
            }

            return this;
        }

        public Contato AddEnderecoDoContato(string descricao, TipoEnderecoEnum tipoEndereco, TipoDeLogradouro tipoLogradouro, string logradouro, string numero,
            string complemento = null, string bairro = null, string cep = null, Cidade cidade = null, string referencia = null)
        {
            return AddEnderecoDoContato(new Endereco(this.Pessoa, descricao, tipoEndereco, tipoLogradouro, logradouro, numero, complemento, bairro, cep, cidade, referencia));
        }

        public Contato RemoveEnderecoDoContato(Endereco item)
        {
            Enderecos.Remove(item);
            return this;
        }

        public Contato AddEnderecoPrincipal(Endereco item)
        {
            this.EnderecoPrincipal = item;
            return AddEnderecoDoContato(item);
        }

        #endregion Métodos para Adicionar Coleções
    }
}