﻿using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class PessoaJuridica : Pessoa
    {
        // O tamanho do nome fantasia deve ser igual ao nome da pessoa física
        public const int MaxNomeFantasiaLength = 100;

        public const int MaxRazaoSocialLength = 100;
        /// <summary>
        /// Informa a razão social da pessoa jurídica.
        /// </summary>
        /// <description>Razão Social</description>
        /// <required>True</required>
        /// <fieldtype>String</fieldtype>
        /// <fieldsize>100</fieldsize>
        public string RazaoSocial { get; set; }

        /// <summary>
        /// Informa o nome fantasia da pessoa jurídica.
        /// </summary>
        /// <description>Nome Fantasia</description>
        /// <required>True</required>
        /// <fieldtype>String</fieldtype>
        /// <fieldsize>100</fieldsize>
        public string NomeFantasia { get; set; }
    }
}