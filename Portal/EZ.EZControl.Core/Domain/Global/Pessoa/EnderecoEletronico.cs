﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using Abp.UI;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class EnderecoEletronico : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int MaxEnderecoLength = 40;

        public EnderecoEletronico()
        {
        }

        public EnderecoEletronico(Pessoa pessoa, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
            : this()
        {
            if (pessoa == null)
                throw new UserFriendlyException("Pessoa é obrigatório.");

            this.Pessoa = pessoa;
            this.TipoDeEnderecoEletronico = tipoDeEnderecoEletronico;
            this.Endereco = endereco;
        }

        public virtual Contato Contato { get; protected set; }
        public virtual int? ContatoId { get; protected set; }

        public virtual Pessoa Pessoa { get; protected set; }
        public virtual int PessoaId { get; protected set; }

        /// <summary>
        /// Informa o tipo de endereço eletrônico.
        /// </summary>
        /// <description>Tipo</description>
        /// <required>True</required>
        /// <fieldtype>EZ.EZControl.Domain.TipoEnderecoEletronicoEnum</fieldtype>
        /// <fieldsize>N/A</fieldsize>
        public TipoDeEnderecoEletronicoEnum TipoDeEnderecoEletronico { get; set; }

        /// <summary>
        /// Indorma o endereço eletrônico da pessoa.
        /// </summary>
        /// <description>Endereço</description>
        /// <required>True</required>
        /// <fieldtype>string</fieldtype>
        /// <fieldsize>40</fieldsize>
        public string Endereco { get; set; }

        
        

        #region Métodos

        public void AssociarPessoa(Pessoa pessoa, IEnderecoEletronicoService enderecoEletronicoService)
        {
            enderecoEletronicoService.CanAssignPessoa(this, pessoa);
            
            Pessoa = pessoa;
            PessoaId = pessoa.Id;
            
        }

        public void AssociarContato(Contato contato, IEnderecoEletronicoService enderecoEletronicoService)
        {
            enderecoEletronicoService.CanAssignContato(this, contato);
           
           Contato = contato;
           if (contato != null)
           {
               ContatoId = contato.Id;
           }
           
        }

        public static EnderecoEletronico GetEnderecoEletronico(ICollection<EnderecoEletronico> enderecosEletronicos, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico)
        {
            return enderecosEletronicos?.OrderBy(x => x.Id)
                .FirstOrDefault(x => x.TipoDeEnderecoEletronico == tipoDeEnderecoEletronico);
        }

        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;

            try
            {
                return Regex.IsMatch(email,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        #endregion Métodos
    }
}