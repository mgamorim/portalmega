﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.Global.Pessoa
{
    public class Pessoa : EzEntityMustHaveTenant
    {
        public const int MaxObservacaoLength = 1000;

        #region Propriedades

        /// <summary>
        /// Ativa ou Inativa os dados cadastrados pelo usuário.
        /// </summary>
        /// <description>Ativo</description>
        /// <required>False</required>
        /// <fieldtype>Bool</fieldtype>
        public virtual Endereco EnderecoPrincipal { get; set; }
        public virtual int? EnderecoPrincipalId { get; set; }

        public virtual Endereco EnderecoCobranca { get; set; }
        public virtual int? EnderecoCobrancaId { get; set; }

        private Endereco enderecoEntrega;

        //TODO: mudar o set para uma regra de negócio
        public virtual Endereco EnderecoEntrega
        {
            get { return enderecoEntrega; }
            set
            {
                enderecoEntrega = value;
                if (enderecoEntrega != null)
                    enderecoEntrega.EnderecoParaEntrega = true;
            }
        }

        public virtual int? EnderecoEntregaId { get; protected set; }

        public Telefone TelefoneComercial
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Comercial); }
        }

        public Telefone TelefoneCelular
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Celular); }
        }

        public Telefone TelefoneResidencial
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Residencial); }
        }

        public Telefone TelefoneRecados
        {
            get { return Telefone.GetTelefone(Telefones, TipoTelefoneEnum.Recados); }
        }

        public virtual Documento DocumentoPrincipal { get; set; }
        public virtual int? DocumentoPrincipalId { get; set; }

        public virtual GrupoPessoa GrupoPessoa { get; protected set; }
        public virtual int? GrupoPessoaId { get; protected set; }

        public virtual ICollection<Documento> Documentos { get; set; }

        public virtual ICollection<EnderecoEletronico> EnderecosEletronicos { get; set; }

        public virtual ICollection<Endereco> Enderecos { get; set; }

        public virtual ICollection<Contato> Contatos { get; set; }

        public virtual ICollection<Telefone> Telefones { get; set; }

        public string Email
        {
            get
            {
                if (this.EnderecosEletronicos != null)
                {
                    var email =
                        this.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);
                    return email != null ? email.Endereco : string.Empty;
                }
                return string.Empty;
            }
        }

        public int EmailId
        {
            get
            {
                if (this.EnderecosEletronicos != null)
                {
                    var email =
                        this.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);
                    return email != null ? email.Id : default(int);
                }
                return default(int);
            }
        }

        public string Celular
        {
            get
            {
                if (this.Telefones != null)
                {
                    var celular =
                       this.Telefones.FirstOrDefault(
                           x => x.Tipo == TipoTelefoneEnum.Celular);

                    return celular != null ? celular.DDD + celular.Numero : string.Empty;
                }
                return string.Empty;
            }
        }

        public string DocumentoPrincipalPessoa
        {
            get
            {
                if (this.IsPessoaFisica)
                {
                    var doc = this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);
                    return (doc != null) ? doc.Numero : string.Empty;
                }
                else
                {
                    var doc = this.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cnpj);
                    return (doc != null) ? doc.Numero : string.Empty;
                }
            }
        }

        #endregion

        #region Metodos

        public void AssociarGrupoPessoa(GrupoPessoa grupoPessoa, IPessoaService pessoaService)
        {
            pessoaService.CanAssignGrupoPessoa(this, grupoPessoa);

            GrupoPessoa = grupoPessoa;

        }

        public void AssociarDocumentoPrincipal(Documento documento, IPessoaService pessoaService)
        {
            pessoaService.CanAssignDocumentoPrincipal(this, documento);

            DocumentoPrincipal = documento;
            DocumentoPrincipalId = documento.Id;

        }

        public void AssociarEnderecoPrincipal(Endereco endereco, IPessoaService pessoaService)
        {
            pessoaService.CanAssignEnderecoPrincipal(this, endereco);

            EnderecoPrincipal = endereco;

        }

        public void AssociarEnderecoCobranca(Endereco endereco, IPessoaService pessoaService)
        {
            pessoaService.CanAssignEnderecoCobranca(this, endereco);

            EnderecoCobranca = endereco;

        }

        public void AssociarEnderecoEntrega(Endereco endereco, IPessoaService pessoaService)
        {
            pessoaService.CanAssignEnderecoEntrega(this, endereco);

            EnderecoEntrega = endereco;

        }

        #endregion

        #region Atributos realtivos a Pessoa Física ou Jurídica

        public bool IsPessoaFisica
        {
            get { return this is PessoaFisica; }
        }

        public bool IsPessoaJuridica
        {
            get { return this is PessoaJuridica; }
        }

        public TipoPessoaEnum TipoPessoa
        {
            get { return this.IsPessoaFisica ? TipoPessoaEnum.Fisica : TipoPessoaEnum.Juridica; }
        }

        public PessoaFisica PessoaFisica
        {
            get { return this.IsPessoaFisica ? this as PessoaFisica : null; }
        }

        public PessoaJuridica PessoaJuridica
        {
            get { return this.IsPessoaJuridica ? this as PessoaJuridica : null; }
        }

        public string NomePessoa
        {
            get
            {
                if (this.IsPessoaFisica)
                    return (this as PessoaFisica).Nome;

                if (this.IsPessoaJuridica)
                    return (this as PessoaJuridica).NomeFantasia;

                return string.Empty;
            }
            set
            {
                if (this.IsPessoaFisica)
                    (this as PessoaFisica).Nome = value;
                else if (this.IsPessoaJuridica)
                    (this as PessoaJuridica).NomeFantasia = value;
            }
        }

        public string RazaoSocialPessoa
        {
            get { return this.IsPessoaJuridica ? (this as PessoaJuridica).RazaoSocial : ""; }
            set
            {
                if (this.IsPessoaJuridica)
                    (this as PessoaJuridica).RazaoSocial = value;
            }
        }

        public override string ToString()
        {
            return this.NomePessoa == null ? "" : this.NomePessoa;
        }

        public string Observacao { get; set; }

        #endregion Atributos realtivos a Pessoa Física ou Jurídica

        #region Métodos para retornar elementos específicos das Coleções

        public EnderecoEletronico EmailPrincipal
        {
            get
            {
                if (EnderecosEletronicos == null)
                {
                    EnderecosEletronicos = new List<EnderecoEletronico>();
                }
                return EnderecoEletronico.GetEnderecoEletronico(EnderecosEletronicos, TipoDeEnderecoEletronicoEnum.EmailPrincipal);
            }
        }

        public EnderecoEletronico EmailNFe
        {
            get { return EnderecoEletronico.GetEnderecoEletronico(EnderecosEletronicos, TipoDeEnderecoEletronicoEnum.Email_NFe); }
        }

        public EnderecoEletronico WebSite
        {
            get { return EnderecoEletronico.GetEnderecoEletronico(EnderecosEletronicos, TipoDeEnderecoEletronicoEnum.WebSite); }
        }

        public Contato ContatoPrincipal
        {
            get { return Contatos.OrderBy(x => x.Id).FirstOrDefault(); }
        }

        public bool EnderecoPrincipalIsCobranca { get { return this.EnderecoPrincipal == this.EnderecoCobranca; } }

        public bool EnderecoPrincipalIsEntrega { get { return this.EnderecoPrincipal == this.EnderecoEntrega; } }

        public IEnumerable<Endereco> EnderecosParaEntrega
        {
            get
            {
                foreach (Endereco e in Enderecos)
                {
                    e.Pessoa = this;
                }

                return Enderecos
                    .Where(x => x.EnderecoParaEntrega == true)
                    .ToList();
            }
        }

        public Endereco GetEnderecoById(int idPessoa)
        {
            return Enderecos
                .Where(x => x.Id == idPessoa)
                .FirstOrDefault();
        }

        #endregion Métodos para retornar elementos específicos das Coleções


    }
}