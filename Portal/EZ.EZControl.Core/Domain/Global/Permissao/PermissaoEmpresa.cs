﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.Global.Permissao
{
    public abstract class PermissaoEmpresa : EzEntityMayHaveTenant
    {
        //Esse prop não é assinada pela interface IHasEmpresa para
        //ser possível ler as permissões com o filtro da EmpresaFilter ativado
        public virtual int EmpresaId { get; set; }

        public virtual Empresa Empresa { get; set; }

        public bool IsGranted { get; set; }

        public string PermissionName { get; set; }
    }
}