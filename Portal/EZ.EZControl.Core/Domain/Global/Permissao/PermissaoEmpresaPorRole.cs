﻿namespace EZ.EZControl.Domain.Global.Permissao
{
    public class PermissaoEmpresaPorRole : PermissaoEmpresa
    {
        public const string CacheStoreName = "PermissaoEmpresaCachePorRole";

        public virtual int RoleId { get; set; }
    }
}