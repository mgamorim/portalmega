﻿namespace EZ.EZControl.Domain.Global.Permissao
{
    public class PermissaoEmpresaPorUsuario : PermissaoEmpresa
    {
        public const string CacheStoreName = "PermissaoEmpresaCachePorUsuario";

        public virtual long UserId { get; set; }
    }
}