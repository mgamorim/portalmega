﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class ConexaoSMTP : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 60;
        public const int EnderecoDeEmailPadraoMaxLength = 300;
        public const int ServidorSMTPMaxLength = 200;
        public const int UsuarioAutenticacaoMaxLength = 300;
        public const int SenhaAutenticacaoMaxLength = 50;
        public ConexaoSMTP()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string EnderecoDeEmailPadrao { get; set; }
        public string ServidorSMTP { get; set; }
        public int Porta { get; set; }
        public string UsuarioAutenticacao { get; set; }
        public string SenhaAutenticacao { get; set; }
        public bool RequerAutenticacao { get; set; }
        public bool RequerConexaoCriptografada { get; set; }
        
        
    }
}