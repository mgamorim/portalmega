﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class ItemHierarquia : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 60;
        public const int MascaraMaxLength = 20;

        public ItemHierarquia()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Mascara { get; set; }
        public string Codigo { get; set; }
        public virtual ItemHierarquia ItemHierarquiaPai { get; set; }
        public virtual int? ItemHierarquiaPaiId { get; set; }
        public virtual ICollection<ItemHierarquia> Itens { get; set; }
        
        
    }
}