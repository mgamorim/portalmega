﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class Profissao : EzEntityMustHaveTenant, IPassivable
    {
        public const int CodigoMaxLength = 6;
        public const int TituloMaxLength = 150;

        public Profissao()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        
        
    }
}