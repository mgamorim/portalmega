﻿using EZ.EZControl.Domain.Core.Geral;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class ParametroGlobal : ParametroBase
    {
        public virtual ConexaoSMTP ConexaoSMTP { get; set; }
        public virtual int? ConexaoSMTPId { get; set; }
    }
}