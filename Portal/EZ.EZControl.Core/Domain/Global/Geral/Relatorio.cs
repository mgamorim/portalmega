﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Domain.Core.Enums;

namespace EZ.EZControl.Domain.Global.Geral
{
    /// <summary>
    /// Entidade de relatório que integra com o stimulsoft
    /// </summary>
    public class Relatorio : EzFullAuditedEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 60;

        public Relatorio()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public SistemaEnum Sistema { get; set; }

        /// <summary>
        /// Propriedade usada para identificar qual é o tipo de relatório a partir do enum específico por sistema.
        /// Existe um enum de tipo de relatório para cada sistema. Ex: TipoDeRelatorioEzLivEnum.
        /// Por isso o tipo de relatório é um inteiro, assim pode ter qualquer enum.
        /// </summary>
        public int TipoDeRelatorio { get; set; }

        public string Conteudo { get; set; }
    }
}