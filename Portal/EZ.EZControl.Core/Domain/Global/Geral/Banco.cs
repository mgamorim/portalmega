﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class Banco : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int CodigoMaxLength = 3;
        public const int NomeMaxLength = 60;

        public Banco()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<Agencia> Agencias { get; protected set; }

    }
}