﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using System;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class Feriado : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 200;
        public Feriado()
        {
            IsActive = true;
            Tipo = TipoDeFeriadoEnum.Ordinario;
            Abrangencia = AbrangenciaDoFeriadoEnum.Nacional;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public DateTime Data { get; set; }
        public AbrangenciaDoFeriadoEnum Abrangencia { get; set; }
        public TipoDeFeriadoEnum Tipo { get; set; }
        public virtual Pais Pais { get; set; }
        public virtual int? PaisId { get; set; }
        public virtual Estado Estado { get; set; }
        public virtual int? EstadoId { get; set; }
        public virtual Cidade Cidade { get; set; }
        public virtual int? CidadeId { get; set; }
        
        
    }
}