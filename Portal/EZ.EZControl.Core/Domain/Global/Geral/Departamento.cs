﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class Departamento : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 40;
        public const int DescricaoMaxLength = 100;

        public Departamento()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        
        
    }
}