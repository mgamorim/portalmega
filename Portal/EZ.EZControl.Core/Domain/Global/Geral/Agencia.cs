﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class Agencia : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int CodigoMaxLength = 10;
        public const int DigitoVerificadorMaxLength = 2;
        public const int NomeMaxLength = 60;

        public Agencia()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public virtual Banco Banco { get; set; }
        public virtual int BancoId { get; set; }
        public string Codigo { get; set; }
        public string DigitoVerificador { get; set; }
        public string Nome { get; set; }
        
        
    }
}