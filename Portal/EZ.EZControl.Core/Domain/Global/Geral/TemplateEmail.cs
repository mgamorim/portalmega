﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class TemplateEmail : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 60;
        public const int RemetenteMaxLength = 300;
        public const int AssuntoMaxLength = 100;
        public const int MensagemMaxLength = 4000;

        public TemplateEmail()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Remetente { get; set; }
        public string Assunto { get; set; }
        public string CorpoMensagem { get; set; }
        public TipoDeTemplateDeMensagemEnum TipoDeTemplateDeMensagem { get; set; }
        
        
    }
}