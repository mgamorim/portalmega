﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System;
using System.IO;
using System.Text;

namespace EZ.EZControl.Domain.Global.Geral
{
    public static class TemplateDeEnvioDeEmailDeSistema
    {
        //string de concatenação 'https://'
        private const string strHttp = "https://";
        //email de ativação de cadastro
        private const string _htmlWelcome = "/Resources/Notification/Ez-Welcome.html";
        //email de ativação de cadastro do corretor
        private const string _htmlWelcomeCorretor = "/Resources/Notification/Ez-Welcome-Corretor.html";
        //email de ativação de cadastro do Usuario direto da pagina do simulador
        private const string _htmlWelcomeUserSimulador = "/Resources/Notification/Ez-Welcome-User-Simulador.html";
        //email de aceite do contrato
        private const string _htmlAceite = "/Resources/Notification/Ez-Aceite.html";
        //email de aceite do contrato pelo Corretor
        private const string _htmlAceiteCorretor = "/Resources/Notification/Ez-AceiteCorretor.html";
        //email de nova homologação para administradora
        private const string _htmlNovaHomologacao = "/Resources/Notification/Ez-Nova-Homologacao.html";
        //email de nova homologação para corretor/corretora
        private const string _htmlNovaHomologacaoCorretor = "/Resources/Notification/Ez-Nova-Homologacao-Corretor.html";
        //email de homologação concluida pela administradora
        private const string _htmlHomologacaoConcluida = "/Resources/Notification/Ez-Homologacao-Concluida.html";
        //email de pagamento realizado para o cliente
        private const string _htmlPagamentoCliente = "/Resources/Notification/Ez-Pagamento-Cliente.html";
        //email de pagamento realizado para a adm / corretora
        private const string _htmlPagamentoAdmCorretora = "/Resources/Notification/Ez-Pagamento-Corretora-Administradora.html";
        //email de notificação de cadastro de um corretor para a corretora
        private const string _htmlNotificacaoNovoCorretorParaCorretora = "/Resources/Notification/Ez-Notificacao-Novo-Corretor-Para-Corretora.html";
        //email de reset de senha
        private const string _htmlResetPassword = "/Resources/Usuario/Ez-Reset-Password.html";
        //email de comprovante de deposito para o corretor
        private const string _htmlComprovanteDepositoCorretor = "/Resources/Notification/Ez-Comprovante-Deposito-Corretor.html";




        public static string GetHtmlNotificacaoUsuarioCriadoPelaPropostaDeContratacao(string linkEdicaoDaProposta)
        {
            var mailMessage = new StringBuilder();

            mailMessage.AppendLine("Proposta" + "<br />");
            mailMessage.AppendLine("<b>Link para edição da proposta de contratação</b>: " + "<br />");
            mailMessage.AppendLine("<a href=\"" + linkEdicaoDaProposta + "\">" + linkEdicaoDaProposta + "</a>");

            return mailMessage.ToString();
        }

        //EMAIL DE ATIVAÇÃO DE CADASTRO DO CORRETOR
        public static string GetHtmlNotificacaoUsuarioSimulador(string nomeCompleto, string usuarioOuEmail, string Senha, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlWelcomeUserSimulador;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("{{nomeCompleto}}", nomeCompleto);
            html = html.Replace("{{usuarioOuEmail}}", usuarioOuEmail);
            html = html.Replace("{{Senha}}", Senha);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);

            return html;
        }

        //EMAIL DE ATIVAÇÃO DE CADASTRO DO CORRETOR
        public static string GetHtmlNotificacaoUsuarioCorretor(string nomeCompleto, string usuarioOuEmail, string linkCriarSenha, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlWelcomeCorretor;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("{{nomeCompleto}}", nomeCompleto);
            html = html.Replace("{{usuarioOuEmail}}", usuarioOuEmail);
            html = html.Replace("{{linkCriarSenha}}", linkCriarSenha);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);

            return html;
        }
        //EMAIL DE ATIVAÇÃO DE CADASTRO
        public static string GetHtmlNotificacaoUsuarioCriadoPeloBeneficiario(string login, string senha, string linkRedefinicaoDeSenha, Corretora corretora)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlWelcome;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("link_ativar_cadastro", strHttp + linkRedefinicaoDeSenha);
            html = html.Replace("login_ezsoft", login);

            if (corretora == null)
            {
                var corretoraInfo = CorretoraNotificationInfo.CriarCorretoraDefault();

                html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
                html = html.Replace("{{logoCorretora}}", corretoraInfo.Logo);
                html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
                html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);

            }
            else
            {
                var corretoraInfo = CorretoraNotificationInfo.CriarCorretoraDefault();
                corretoraInfo.NomeFantasia = corretora.PessoaJuridica.NomeFantasia;
                corretoraInfo.Logo = "https://prdmegavita.azurewebsites.net/" + corretora.Imagem.Path;
                corretoraInfo.Email = corretora.PessoaJuridica.EmailPrincipal.Endereco;
                corretoraInfo.Telefone = corretora.PessoaJuridica.TelefoneComercial.DDD + " " + corretora.PessoaJuridica.TelefoneComercial.Numero;

                html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
                html = html.Replace("{{logoCorretora}}", corretoraInfo.Logo);
                html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
                html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);

            }


            return html;
        }
        //EMAIL DE ACEITE
        public static string GetHtmlAceite(string linkAceite, string linkRedefinirSenha, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlAceite;
            var html = File.ReadAllText(urlHtml);

            if (!string.IsNullOrEmpty(linkRedefinirSenha))
            {
                var redefinir = @" <p class=""text - center call - text blue - dark float-center"" align=""center"" style=""Margin: 0; color:#1c4867;font-family:'Lucida Sans Unicode','Lucida Grande',sans-serif!important;font-size:22px;font-weight:400;line-height:35px;margin:0;margin-bottom:15px;padding:0;text-align:center"">
                                        Você ainda não ativou o seu cadastro. Clique no botão abaixo para ativar o seu cadastro no Portal MEGA e comece a usar agora mesmo.
                                   </p>
                                   <a class=""text-center btn-primary float-center"" href=""link_redefinicao"" align=""center"" style=""Margin:0;background-color:#bf1e59;border-radius:30px;color:#fff;display:inline-block;font-family:'Lucida Sans Unicode','Lucida Grande',sans-serif!important;font-size:18px;font-weight:lighter;line-height:1.3;margin:0;padding:20px 15px;text-align:left;text-decoration:none;text-transform:uppercase;transition:all .4s ease"">
                                        Ativar cadastro
                                   </a>
                                   <br />
                                   <br />";

                redefinir = redefinir.Replace("link_redefinicao", strHttp + linkRedefinirSenha);
                html = html.Replace("<label id=linkPassword></label>", redefinir);
            }

            html = html.Replace("link_aceite", strHttp + linkAceite);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        //EMAIL DE ACEITE DO CORRETOR
        public static string GetHtmlAceiteCorretor(string linkAceite, string nomeBeneficiario, string numeroProposta, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlAceiteCorretor;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("link_aceite", strHttp + linkAceite);
            html = html.Replace("{{NOME_BENEFICIARIO}}", nomeBeneficiario);
            html = html.Replace("{{NUMERO_PROPOSTA}}", numeroProposta);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        //EMAIL DE NOVA HOMOLOGAÇÃO
        public static string GetHtmlNovaHomologacao(string nomeTitular, string linkProposta, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlNovaHomologacao;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("nome_titular", nomeTitular);
            html = html.Replace("link_proposta", strHttp + linkProposta);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        public static string GetHtmlNovaHomologacaoParaCorretor(string nomeTitular, string linkProposta, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlNovaHomologacaoCorretor;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("nome_titular", nomeTitular);
            html = html.Replace("link_proposta", strHttp + linkProposta);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        //EMAIL DE HOMOLOGAÇÃO CONCLUIDA
        public static string GetHtmlHomologacaoConcluida(string linkProposta, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlHomologacaoConcluida;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("link_proposta", strHttp + linkProposta);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        //EMAIL DE PAGAMENTO EFETUADO BENEFICIARIO
        public static string GetHtmlPagamentoBeneficiario(string nomeTitular, string linkEZLiv, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlPagamentoCliente;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("nome_titular", nomeTitular);
            html = html.Replace("link_ezliv", strHttp + linkEZLiv);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        //EMAIL DE PAGAMENTO ADM/CORRETORA
        public static string GetHtmlPagamentoAdmCorretora(string linkEZLiv, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlPagamentoAdmCorretora;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("link_ezliv", strHttp + linkEZLiv);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
        //EMAIL DE NOTIFICAÇÃO de cadastro de um corretor para a corretora
        public static string GetHtmlNotificacaoNovoCorretorParaCorretora(string nomeCorretor, int corretorId, CorretoraNotificationInfo corretoraInfo)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlNotificacaoNovoCorretorParaCorretora;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("{{nomeCorretor}}", nomeCorretor);
            html = html.Replace("{{codigoCorretor}}", corretorId.ToString());
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }

        public static string GetHtmlConclusaoDaContratacao(string linkContrato, CorretoraNotificationInfo corretoraInfo)
        {
            string urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlAceite;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("link_contrato_ezsoft", strHttp + linkContrato);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }

        public static string GetHtmlNotificacaoDocumentosEmExigencia(string documentos)
        {
            var mailMessage = new StringBuilder();

            mailMessage.AppendLine("Os documentos abaixo estão em exigência." + "<br />");
            mailMessage.AppendLine(documentos);

            return mailMessage.ToString();
        }

        public static string GetHtmlAceiteDoContrato(string titular, string plano, string administradora, string corretora, string operadora)
        {
            var mailMessage = new StringBuilder();

            mailMessage.AppendLine(titular + " aceitou o contrato." + "<br />");
            mailMessage.AppendLine("Dados da proposta:." + "<br />");
            mailMessage.AppendLine("Plano " + plano + "<br />");
            mailMessage.AppendLine("Administradora " + administradora + "<br />");
            mailMessage.AppendLine("Corretora " + corretora + "<br />");
            mailMessage.AppendLine("Operadora " + operadora + "<br />");

            return mailMessage.ToString();
        }

        public static string GetHtmlHomologacaoDoContrato(string titular, string plano, string administradora, string operadora)
        {
            var mailMessage = new StringBuilder();

            mailMessage.AppendLine(administradora + " homologou o contrato." + "<br />");
            mailMessage.AppendLine("Dados da proposta:" + "<br />");
            mailMessage.AppendLine("Plano " + plano + "<br />");
            mailMessage.AppendLine("Titular " + titular + "<br />");
            mailMessage.AppendLine("Operadora " + operadora + "<br />");

            return mailMessage.ToString();
        }

        public static string GetHtmlBoletoGerado(string titular, string plano, string administradora, string operadora)
        {
            var mailMessage = new StringBuilder();

            mailMessage.AppendLine(titular + " gerou um boleto." + "<br />");
            mailMessage.AppendLine("Dados da proposta:." + "<br />");
            mailMessage.AppendLine("Plano " + plano + "<br />");
            mailMessage.AppendLine("Administradora " + administradora + "<br />");
            mailMessage.AppendLine("Operadora " + operadora + "<br />");

            return mailMessage.ToString();
        }

        public static string GetHtmlFormulario(string titular, string nomeAdminCorretora)
        {
            var mailMessage = new StringBuilder();
            mailMessage.AppendLine("Prezado Cliente " + titular + ".");
            mailMessage.AppendLine();
            mailMessage.AppendLine("Conforme acordado entre as partes, segue em anexo o contrato e os documentos que deverão ser assinados, rubricado nas páginas em que não tem a área de assinatura assinalada, escaneados e respondidos a partir desse e-mail.");
            mailMessage.AppendLine();
            mailMessage.AppendLine("Em seguida, a administradora \"" + nomeAdminCorretora + "\" vai homologar os documentos e você irá receber posteriormente receber a fatura por e-mail.");
            mailMessage.AppendLine();
            mailMessage.AppendLine("Att. " + nomeAdminCorretora);
            mailMessage.AppendLine();
            mailMessage.AppendLine("Via Portal MEGA");

            return mailMessage.ToString();
        }

        public static string GetHtmlResetPassword(string nomeCompleto, string link, string usuarioOuEmail)
        {
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlResetPassword;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("{{nomeCompleto}}", nomeCompleto);
            html = html.Replace("{{link}}", strHttp + link);
            html = html.Replace("{{usuarioOuEmail}}", usuarioOuEmail);


            return html;
        }

        public static string GetHtmlComprovanteDepositoCorretor(string nomeTitular, string numeroProposta, string linkEdicaoProposta, CorretoraNotificationInfo corretoraInfo)
        {       
            var urlHtml = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppDomain.CurrentDomain.RelativeSearchPath ?? "") + _htmlComprovanteDepositoCorretor;

            var html = File.ReadAllText(urlHtml);
            html = html.Replace("{{nomeTitular}}", nomeTitular);
            html = html.Replace("{{numeroProposta}}", numeroProposta);
            html = html.Replace("{{linkEdicaoProposta}}", strHttp + linkEdicaoProposta);
            html = html.Replace("{{nomeCorretora}}", corretoraInfo.NomeFantasia);
            html = html.Replace("{{logoCorretora}}", "https://prdmegavita.azurewebsites.net/" + corretoraInfo.Logo);
            html = html.Replace("{{emailCorretora}}", corretoraInfo.Email);
            html = html.Replace("{{telefoneCorretora}}", corretoraInfo.Telefone);
            return html;
        }
    }
}
