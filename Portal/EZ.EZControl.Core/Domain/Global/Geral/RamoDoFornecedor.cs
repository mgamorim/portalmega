﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Collections.Generic;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Global.Geral
{
    public class RamoDoFornecedor : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 50;

        public RamoDoFornecedor()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public string Nome { get; set; }

        public ICollection<Fornecedor> Fornecedores { get; set; }
        
        
    }
}