﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.EZ.EzEntity.Interfaces;

namespace EZ.EZControl.Domain.Servicos.Geral
{
    public class Servico : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 100;

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public bool IsValorAjustavel { get; set; }
        
        

    }
}
