﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZMedical.SubtiposPessoa
{
    public class Medico : SubtipoPessoaFisicaEntityBase, IPassivable
    {
        public Medico()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual Picture Picture { get; set; }

        public virtual ICollection<Especialidade> Especialidades { get; set; }
    }
}
