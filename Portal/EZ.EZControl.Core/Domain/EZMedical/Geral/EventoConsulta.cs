﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;

namespace EZ.EZControl.Domain.EZMedical.Geral
{
    public class EventoConsulta : EventoBase
    {
        public EventoConsulta()
        {
            TipoDeEvento = TipoDeEventoEnum.Consulta;
            Sistema = SistemaEnum.EZMedical;
        }

        public object Data { get; set; }
    }
}