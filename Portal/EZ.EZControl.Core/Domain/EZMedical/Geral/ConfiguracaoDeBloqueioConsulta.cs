﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZMedical.Geral
{
    public class ConfiguracaoDeBloqueioConsulta : ConfiguracaoDeBloqueio
    {
        public virtual Medico Medico { get; set; }
        public virtual Especialidade Especialidade { get; set; }
    }
}
