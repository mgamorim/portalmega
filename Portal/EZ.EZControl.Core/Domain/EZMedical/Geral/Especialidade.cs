﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZMedical.Geral
{
    public class Especialidade : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const int CodigoMaxLength = 10;
        public const int NomeMaxLength = 250;

        public Especialidade()
        {
            IsActive = true;
        }

        public string Codigo { get; set; }
        public string Nome { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Laboratorio> Laboratorios { get; set; }
        public ICollection<Hospital> Hospitais { get; set; }
    }
}