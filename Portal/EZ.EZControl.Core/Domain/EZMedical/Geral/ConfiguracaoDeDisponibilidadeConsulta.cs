﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;

namespace EZ.EZControl.Domain.EZMedical.Geral
{
    public class ConfiguracaoDeDisponibilidadeConsulta : ConfiguracaoDeDisponibilidade
    {
        public virtual Medico Medico { get; set; }
        public virtual Especialidade Especialidade { get; set; }
    }
}
