﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.CRM.Geral
{
    public class Lead : EzEntityMustHaveTenant, IPassivable
    {
        public bool IsActive { get; set; }
        public virtual PessoaFisica PessoaFisica { get; set; }
        public int PessoaFisicaId { get; set; }
    }
}
