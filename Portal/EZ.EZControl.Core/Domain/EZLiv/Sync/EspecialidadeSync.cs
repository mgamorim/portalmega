﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Sync
{
    public class EspecialidadeSync : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        #region Constantes
        public const int EspecialidadeMaxLength = 100;
        public const int NomeMaxLength = 100;
        public const int LogradouroMaxLength = 100;
        public const int NumeroMaxLength = 100;
        public const int ComplementoMaxLength = 100;
        public const int BairroMaxLength = 100;
        public const int MunicipioMaxLength = 100;
        public const int EstadoMaxLength = 100;
        public const int NomeEstadoMaxLength = 100;
        public const int Telefone1MaxLength = 100;
        public const int Ramal1MaxLength = 100;
        public const int Telefone2MaxLength = 100;
        public const int Ramal2MaxLength = 100;
        public const int MunicipioPaiMaxLength = 100;
        public const int EmailMaxLength = 100;
        public const int HomePageMaxLength = 100;
        #endregion

        public EspecialidadeSync()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public string Especialidade { get; set; }

        public string Nome { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string Municipio { get; set; }

        public string Estado { get; set; }

        public string NomeEstado { get; set; }

        public string Telefone1 { get; set; }

        public string Ramal1 { get; set; }

        public string Telefone2 { get; set; }

        public string Ramal2 { get; set; }

        public string MunicipioPai { get; set; }

        public string Email { get; set; }

        public string HomePage { get; set; }

        public virtual PlanoDeSaudeAnsEnum PlanoDeSaudeAns { get; set; }

        public virtual ICollection<ProdutoDePlanoDeSaude> ProdutosDePlanoDeSaude { get; set; }
    }
}
