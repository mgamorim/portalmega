﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class UnidadeDeSaudeBase : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int MaxEmailLength = 300;
        public const int MaxSiteLength = 100;
        public UnidadeDeSaudeBase()
        {
            IsActive = true;
        }

        public string Nome { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public string Site { get; set; }
        public int PessoaJuridicaId { get; set; }
        public virtual PessoaJuridica PessoaJuridica { get; set; }
        public int EnderecoId { get; set; }
        public virtual Endereco Endereco { get; set; }
        public int? Telefone1Id { get; set; }
        public virtual Telefone Telefone1 { get; set; }
        public int? Telefone2Id { get; set; }
        public virtual Telefone Telefone2 { get; set; }
        public virtual TipoDeUnidadeEnum TipoDeUnidade { get; set; }
    }
}
