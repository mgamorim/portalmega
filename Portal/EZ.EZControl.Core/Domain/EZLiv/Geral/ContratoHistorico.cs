﻿using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ContratoHistorico : EzEntityMustHaveTenantMustHaveEmpresa
    {
        public const string CacheStoreName = "ContratoHistoricoCache";

        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public ProdutoDePlanoDeSaude ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public Contrato Contrato { get; set; }
        public int ContratoId { get; set; }
        public long UserId { get; set; }
        public DateTime DataEvento { get; set; }

        public void AssociarProdutoDePlanoDeSaude(ProdutoDePlanoDeSaude produto, IContratoHistoricoService contratoService)
        {
            contratoService.CanAssignProdutoDePlanoDeSaude(this, produto);
            ProdutoDePlanoDeSaude = produto;
        }

        public void AssociarContrato(Contrato contrato, IContratoHistoricoService contratoService)
        {
            contratoService.CanAssignContrato(this, contrato);
            Contrato = contrato;
        }
    }
}
