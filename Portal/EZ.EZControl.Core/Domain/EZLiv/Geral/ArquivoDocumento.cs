﻿using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ArquivoDocumento : ArquivoBase
    {
        public const int MotivoMaxLength = 300;
        public ArquivoDocumento()
        {
            EmExigencia = false;
        }
        public TipoDeDocumentoEnum Tipo { get; set; }
        public bool EmExigencia { get; set; }
        public string Motivo { get; set; }
        public virtual PropostaDeContratacao PropostaDeContratacao { get; set; }
        public int PropostaDeContratacaoId { get; set; }
    }
}
