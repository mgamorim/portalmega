﻿using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ProdutoDePlanoDeSaude : Produto, IHasEmpresa
    {
        public ProdutoDePlanoDeSaude()
        {
            this.IsActive = true;
            this.TipoDeProduto = TipoDeProdutoEnum.Virtual;
        }

        public virtual PlanoDeSaude PlanoDeSaude { get; set; }
        public int PlanoDeSaudeId { get; set; }

        public virtual Administradora Administradora { get; set; }
        public int? AdministradoraId { get; set; }

        public virtual Operadora Operadora { get; set; }
        public int OperadoraId { get; set; }

        public bool CarenciaEspecial { get; set; }
        public string DescricaoDaCarenciaEspecial { get; set; }

        public bool Acompanhante { get; set; }
        public string DescricaoDoAcompanhante { get; set; }

        public bool CoberturaExtra { get; set; }
        public string DescricaoDaCoberturaExtra { get; set; }

        public string LinkRedeCredenciada { get; set; }

        public virtual RedeCredenciada RedeCredenciada { get; set; }
        public int? RedeCredenciadaId { get; set; }

        protected virtual ICollection<EspecialidadePorProdutoDePlanoDeSaude> Especialidades { get; set; }
        public IReadOnlyList<EspecialidadePorProdutoDePlanoDeSaude> EspecialidadesReadOnly
        {
            get { return Especialidades.ToList(); }
        }
        public static Expression<Func<ProdutoDePlanoDeSaude, ICollection<EspecialidadePorProdutoDePlanoDeSaude>>> EspecialidadeExpression = f => f.Especialidades;

        public void AssociarRedeCredenciada(RedeCredenciada redeCredenciada, IProdutoDePlanoDeSaudeService produtoService)
        {
            produtoService.CanAssignRedeCredenciada(this, redeCredenciada);
            RedeCredenciada = redeCredenciada;
        }

        public virtual ICollection<Estado> Estados { get; set; }
        public virtual ICollection<Relatorio> Aditivos { get; set; }
        public virtual ICollection<Cidade> Cidades { get; set; }
        public FormaDeContratacaoEnum FormaDeContratacao { get; set; }
        public virtual ICollection<Vigencia> Vigencias { get; set; }
        public virtual ICollection<Chancela> Chancelas { get; set; }
        public virtual ICollection<OpcaoAdicionalDeProdutoDePlanoDeSaude> OpcoesAdicionais { get; set; }
        public virtual QuestionarioDeDeclaracaoDeSaude QuestionarioDeDeclaracaoDeSaude { get; set; }
        public int? QuestionarioDeDeclaracaoDeSaudeId { get; set; }
        public int? NumeroDeDiasParaEncerrarAsPropostasRelacionadas { get; set; }
        public string CartaDeOrientacao { get; set; }
        public virtual DeclaracaoDoBeneficiario DeclaracaoDoBeneficiario { get; set; }
        public int? DeclaracaoDoBeneficiarioId { get; set; }
        public virtual Relatorio RelatorioFichaDeEntidade { get; set; }
        public int? RelatorioFichaDeEntidadeId { get; set; }
        public virtual Relatorio RelatorioProspostaDeContratacao { get; set; }
        public int? RelatorioProspostaDeContratacaoId { get; set; }
    }
}
