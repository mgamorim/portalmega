﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    /// <summary>
    /// Entidade usada pela administradora para dar permissão de venda de produtos de plano de saúde para a corretora
    /// </summary>
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretora : EzEntityMustHaveTenant, IPassivable
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretora()
        {
            this.IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }

        /// <summary>
        /// A administradora responsável pela permissão. É preenchido automaticamente pela empresa logado, porque somente perfis de administradora podem acessar.
        /// </summary>
        public virtual Administradora Administradora { get; set; }
        public int AdministradoraId { get; set; }


        public virtual ICollection<Corretora> Corretoras { get; set; }

        /// <summary>
        /// Produtos permitidos para a corretora (Independente empresa) a partir dos produtos relacionados a administradora
        /// 
        /// </summary>
        public virtual ICollection<ProdutoDePlanoDeSaude> Produtos { get; set; }

        /// <summary>
        /// A permissão de venda tem empresa, mas não é utilizado o filtro automático de empresa com o IHasEmpresa, porque é necessário recuperar as corretoras e produtos sem o filtro da (Corretoras/Produtos tem filtro automático de empresa).  
        /// </summary>
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}
