﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class QuestionarioDeDeclaracaoDeSaude : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 250;
        public QuestionarioDeDeclaracaoDeSaude()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual Operadora Operadora { get; set; }
        public int OperadoraId { get; set; }
        public virtual ICollection<ItemDeDeclaracaoDeSaude> ItensDeDeclaracaoDeSaude { get; set; }
    }
}
