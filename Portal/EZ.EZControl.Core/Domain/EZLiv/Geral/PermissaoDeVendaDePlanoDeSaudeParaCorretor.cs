﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    /// <summary>
    /// Entidade usada pela corretora para dar permissão de venda de produtos de plano de saúde para o corretor
    /// </summary>
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretor : EzEntityMustHaveTenant, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Comissao { get; set; }

        /// <summary>
        /// A corretora responsável pela permissão. É preenchido automaticamente pela empresa logado, porque somente perfis de corretora podem acessar.
        /// </summary>
        public virtual Corretora Corretora { get; set; }
        public int CorretoraId { get; set; }

        public virtual List<Corretor> Corretores { get; set; }

        /// <summary>
        /// Produtos permitidos para o corretor (Independente empresa) a partir dos produtos relacionados a corretora a partir da PermissaoDeVendaDePlanoDeSaudeParaCorretora
        /// </summary>
        public virtual List<ProdutoDePlanoDeSaude> Produtos { get; set; }

        /// <summary>
        /// A permissão de venda tem empresa, mas não é utilizado o filtro automático de empresa com o IHasEmpresa, porque é necessário recuperar as corretoras e produtos sem o filtro da (Corretoras/Produtos tem filtro automático de empresa).  
        /// </summary>
        public virtual Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}
