﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class OpcaoAdicionalDeProdutoDePlanoDeSaude : EzFullAuditedEntityMustHaveTenantMustHaveEmpresa
    {
        public const int NomeMaxLength = 100;
        public const int DescricaoMaxLength = 1024;

        public string Nome { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public TipodeOpcaoAdicionalDeProdutoDePlanoDeSaudeEnum Tipo { get; set; }

        public virtual ProdutoDePlanoDeSaude ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
    }
}
