﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class IndiceDeReajustePorFaixaEtaria : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public IndiceDeReajustePorFaixaEtaria()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public decimal IndiceDeReajuste { get; set; }
        public virtual FaixaEtaria FaixaEtaria { get; set; }
        public int FaixaEtariaId { get; set; }
        public virtual Operadora Operadora { get; set; }
        public int OperadoraId { get; set; }
    }
}
