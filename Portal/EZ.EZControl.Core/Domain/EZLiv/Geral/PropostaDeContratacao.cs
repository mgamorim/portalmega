﻿
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Interfaces;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class PropostaDeContratacao : EzFullAuditedEntityMayHaveTenant, IHasCorretor
    {
        public PropostaDeContratacao()
        {
            StatusDaProposta = StatusDaPropostaEnum.Pendente;
            FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            TipoDeHomologacao = TipoDeHomologacaoEnum.PendenteDeHomologacao;
            TipoDeProposta = TipoDePropostaEnum.Nova;
        }
        public string Numero { get; set; }
        public virtual ProponenteTitular Titular { get; set; }
        public int TitularId { get; set; }
        public virtual ProdutoDePlanoDeSaude Produto { get; set; }
        public virtual List<OpcaoAdicionalDeProdutoDePlanoDeSaude> ProdutosAdicionais { get; set; }
        public int? ProdutoId { get; set; }
        public virtual Responsavel Responsavel { get; set; }
        public int? ResponsavelId { get; set; }
        public bool Aceite { get; set; }
        public bool AceiteDaDeclaracaoDeSaude { get; set; }
        public TipoDeHomologacaoEnum TipoDeHomologacao { get; set; }
        public DateTime? DataHoraDaHomologacao { get; set; }
        public string ObservacaoHomologacao { get; set; }
        public DateTime? DataHoraDoAceite { get; set; }
        public virtual Contrato ContratoVigenteNoAceite { get; set; }
        public int? ContratoVigenteNoAceiteId { get; set; }
        public virtual Contrato UltimoContratoAceito { get; set; }
        public int? UltimoContratoAceitoId { get; set; }
        public virtual Corretor Corretor { get; set; }
        public int CorretorId { get; set; }
        public virtual Corretora Corretora { get; set; }
        public int? CorretoraId { get; set; }
        public virtual Pedido Pedido { get; set; }
        public int? PedidoId { get; set; }
        public StatusDaPropostaEnum StatusDaProposta { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public virtual List<ArquivoDocumento> Documentos { get; set; }
        public FormaDeContratacaoEnum FormaDeContratacao { get; set; }
        public virtual Vigencia Vigencia { get; set; }
        public int? VigenciaId { get; set; }
        public virtual Chancela Chancela { get; set; }
        public int? ChancelaId { get; set; }
        public DateTime? InicioDeVigencia { get; set; }
        public TipoDePropostaEnum TipoDeProposta { get; set; }
        public virtual User UsuarioTitular { get; set; }
        public long? UsuarioTitularId { get; set; }
        public virtual User UsuarioResponsavel { get; set; }
        public long? UsuarioResponsavelId { get; set; }
        public virtual Profissao Profissao { get; set; }
        public int? ProfissaoId { get; set; }
        public bool EServidorPublico { get; set; }
        public virtual Estado Estado { get; set; }
        public int? EstadoId { get; set; }
        public bool TitularResponsavelLegal { get; set; }
        public virtual ICollection<Relatorio> Aditivos { get; set; }
        public virtual ICollection<User> UsuariosDependentes { get; set; }
        public virtual ItemDeDeclaracaoDoBeneficiario ItemDeDeclaracaoDoBeneficiario { get; set; }
        public int? ItemDeDeclaracaoDoBeneficiarioId { get; set; }
        public bool AceiteCorretor { get; set; }
        public DateTime? DataAceiteCorretor { get; set; }
        public int? GerenteId { get; set; }
        public virtual GerenteCorretora GerenteCorretora { get; set; }
        public int? SupervisorId { get; set; }
        public virtual SupervisorCorretora SupervisorCorretora { get; set; }
    }
}
