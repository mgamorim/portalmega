﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class PlanoDeSaude : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public PlanoDeSaude()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string NumeroDeRegistro { get; set; }
        public bool Reembolso { get; set; }
        public string DescricaoDoReembolso { get; set; }
        public AbrangenciaDoPlanoEnum Abrangencia { get; set; }
        public SegmentacaoAssistencialDoPlanoEnum SegmentacaoAssistencial { get; set; }
        public PlanoDeSaudeAnsEnum? PlanoDeSaudeAns { get; set; }
        public AcomodacaoEnum? Acomodacao { get; set; }
    }
}
