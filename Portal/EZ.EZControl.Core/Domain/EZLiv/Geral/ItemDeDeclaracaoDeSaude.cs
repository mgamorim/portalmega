﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ItemDeDeclaracaoDeSaude : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int PerguntaMaxLength = 500;

        public ItemDeDeclaracaoDeSaude()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Pergunta { get; set; }
        public int Ordenacao { get; set; }

        public TipoDeItemDeDeclaracaoEnum TipoDeItemDeDeclaracao { get; set; }
    }
}
