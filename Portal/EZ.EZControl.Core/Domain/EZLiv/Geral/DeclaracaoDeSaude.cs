﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class DeclaracaoDeSaude : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 100;

        public DeclaracaoDeSaude()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual ProponenteTitular Titular { get; set; }
        public int TitularId { get; set; }
        public virtual ICollection<Dependente> Dependentes { get; set; }
        public virtual ICollection<ItemDeDeclaracaoDeSaude> ItensDeDeclaracaoDeSaude { get; set; }


    }
}
