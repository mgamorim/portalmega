﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZMedical.Geral;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class Laboratorio : UnidadeDeSaudeBase
    {
        public Laboratorio()
        {
            IsActive = true;
            TipoDeUnidade = TipoDeUnidadeEnum.Laboratorio;
        }

        public virtual ICollection<Especialidade> Especialidades { get; set; }

    }
}
