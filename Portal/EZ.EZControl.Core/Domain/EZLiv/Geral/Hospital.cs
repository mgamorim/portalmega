﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZMedical.Geral;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class Hospital : UnidadeDeSaudeBase
    {
        public Hospital()
        {
            IsActive = true;
            TipoDeUnidade = TipoDeUnidadeEnum.Hospital;
        }

        public virtual ICollection<Especialidade> Especialidades { get; set; }

    }
}
