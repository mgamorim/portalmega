﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class Chancela : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 100;
        public Chancela()
        {
            IsActive = true;
        }
        public string Nome { get; set; }
        public decimal TaxaDeAdesao { get; set; }
        public decimal TaxaMensal { get; set; }
        public virtual ICollection<Profissao> Profissoes { get; set; }
        public bool IsActive { get; set; }
        public virtual Administradora Administradora { get; set; }
        public int AdministradoraId { get; set; }
        public virtual Associacao Associacao { get; set; }
        public int AssociacaoId { get; set; }
    }
}
