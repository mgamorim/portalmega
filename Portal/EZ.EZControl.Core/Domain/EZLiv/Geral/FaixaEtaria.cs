﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class FaixaEtaria : EzEntityMustHaveTenant, IPassivable
    {
        public FaixaEtaria()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        public int IdadeInicial { get; set; }

        public int? IdadeFinal { get; set; }
    }
}
