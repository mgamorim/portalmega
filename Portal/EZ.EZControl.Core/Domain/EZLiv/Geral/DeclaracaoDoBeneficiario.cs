﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class DeclaracaoDoBeneficiario : EzFullAuditedEntityMayHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 100;

        public DeclaracaoDoBeneficiario()
        {
            this.IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<ItemDeDeclaracaoDoBeneficiario> ItensDeDeclaracaoDoBeneficiario { get; set; }
    }
}
