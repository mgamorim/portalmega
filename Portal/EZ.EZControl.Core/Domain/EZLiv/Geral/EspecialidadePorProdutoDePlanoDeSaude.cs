﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class EspecialidadePorProdutoDePlanoDeSaude : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public EspecialidadePorProdutoDePlanoDeSaude()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual Especialidade Especialidade { get; set; }
        public int EspecialidadeId { get; set; }

        public virtual ProdutoDePlanoDeSaude ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
    }
}
