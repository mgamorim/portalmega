﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class RespostaDoItemDeDeclaracaoDeSaude : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int ObservacaoMaxLength = 500;
        public const int MarcadaMaxLength = 1;

        public RespostaDoItemDeDeclaracaoDeSaude()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Marcada { get; set; }
        public string Observacao { get; set; }
        public virtual ItemDeDeclaracaoDeSaude ItemDeDeclaracaoDeSaude { get; set; }
        public int ItemDeDeclaracaoDeSaudeId { get; set; }
        public virtual BeneficiarioBase Beneficiario { get; set; }
        public int BeneficiarioId { get; set; }
        public virtual PropostaDeContratacao PropostaDeContratacao { get; set; }
        public int PropostaDeContratacaoId { get; set; }
        public int AnoDoEvento { get; set; }
    }
}
