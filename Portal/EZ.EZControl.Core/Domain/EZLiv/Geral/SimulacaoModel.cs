﻿using System;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class SimulacaoModel 
    {
        // POST FRONT END - PASSO 1
        public string Empresa { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string DataNascimento { get; set; }


        public string Parceiro { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string Estado { get; set; }
        public string Profissao { get; set; }
        public string TitularResponsavel { get; set; }
        public List<Dependente> Dependentes { get; set; }



        // POST FRONT END - PASSO 2
        public int idPlano { get; set; }
        public string NomePlano { get; set; }
        public string DescricaoPlano { get; set; }
        public string NomeOperadora { get; set; }
        public string NomeAdministradora { get; set; }
        public string Abrangencia { get; set; }
        public string Acomodacao { get; set; }
        public string ValorTotal { get; set; }

        



        // USO BACK END
        public string PossuiDependentes { get; set; }
        public DateTime DataSimulacao { get; set; }
        public int QtdDependentes { get; set; }
        public string idProposta { get; set; }
        public string RedirectUrl { get; set; }
        public string Chave { get; set; }
        public string Id { get; set; }



        public Result resultado { get; set; }


        public class Dependente
        {
            public string Nome { get; set; }
            public string Cpf { get; set; }
            public string DataNascimento { get; set; }
        }


        public class Result
        {
            public string Erro { get; set; }
            public string Sucess { get; set; }
        }

    }
}
