﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public static class CarenciaANS
    {
        public static string GetHTMLCarenciasANS()
        {
            return "<table class='tabela-carencias'>" +
                        "<thead>" +
                            "<tr>" +
                                "<th>Cobertura, serviços médicos e hospitalares</th>" +
                                "<th>Carências</th>" +
                            "</tr>" +
                        "</thead>" +
                        "<tbody>" +
                            "<tr>" +
                                "<td>24 (vinte e quatro) horas após a vigência</td>" +
                                "<td>Acidentes pessoais, urgência e emergência inclusive obstétrica</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>30 (trinta) dias após a vigência</td>" +
                                "<td>Consultas e exames / procedimentos básicos de diagnóstico e terapia</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>180 (cento e oitenta) dias após a vigência</td>" +
                                "<td>Fisioterapia</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>180 (cento e oitenta) dias após a vigência</td>" +
                                "<td>Exames / procedimentos especiais de diagnóstico e terapia, Cirúrgias ambulatoriais e internações clínicas ou cirúrgicas, Exames/procedimentos que exijam internação, Exames/procedimentos que não estejam relacionados anteriormente e não estejam excluídos de cobertura, Procedimentos incluidos no Rol de Procedimentos editado pela ANS a partir da RN 167/2008 e suas atualizações de Radiologia com intervencionista genética, transplantes, cirurgia cardíaca, cirurgia bariátrica, mapeamento cerebral, órtese, prótese, radioterapia / oncologia, oxigenoterapia hiperbárica, nutricionista, fonoaudiologia, psicólogo e terapia ocupacional</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>300 (trezentos) dias após a vigência</td>" +
                                "<td>Parto a termo</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<td>24 (vinte e quatro) meses após a vigência</td>" +
                                "<td>Doenças e Lesões Preexistentes</td>" +
                            "</tr>" +
                        "</tbody>" +
                    "</table>";
        }
    }
}
