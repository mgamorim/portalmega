﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class Vigencia : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public Vigencia()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public int DataInicialDaProposta { get; set; }
        public int DataFinalDaProposta { get; set; }
        public int DataDeFechamento { get; set; }
        public int DataDeVigencia { get; set; }
        public string NumeroDeBoletos { get; set; }
    }
}
