﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class Contrato : EzFullAuditedEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public Contrato()
        {
            this.DataCriacao = DateTime.Now;
        }

        public const string CacheStoreName = "ContratoCache";

        public bool IsActive { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public virtual ProdutoDePlanoDeSaude ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }

        public string ConteudoOnline
        {
            get
            {
                var texto = @"<b>RESOLUÇÃO NORMATIVA - RN Nº 413, DE 11 DE NOVEMBRO DE 2016, SEÇÃO IV - Art. 7º.</b>
                              <p>Esta proposta está devidamente assinada de acordo com os procedimentos adotados pela ANS (Agência Nacional de Saúde) em sua RESOLUÇÃO NORMATIVA - RN Nº 413, DE 11 DE NOVEMBRO DE 2016, SEÇÃO IV - Art. 7º.<p/>
                              <p>A Diretoria Colegiada da Agência Nacional de Saúde Suplementar - ANS, conferem vista do que dispõem os incisos II, XII e XXXII do artigo 4º e o inciso II do artigo 10, todos da Lei nº 9.961, de 28 de janeiro de 2000 e em conformidade com a alínea ""a"" do inciso II do art. 86 da Resolução Normativa - RN nº 197, de 16 de julho de 2009, em reunião realizada em 9 de novembro de 2016, adotou a seguinte Resolução Normativa - RN, e eu, Diretor-Presidente, determino a sua publicação.</p>
                              <br><b> Seção IV - Da Validade das Assinaturas Eletrônicas</b>
                              <br><b> Art. 7º Na contratação eletrônica, os documentos poderão ser assinados das seguintes formas:</b>
                              <br> I – Certificação digital; II – <b> login e senha após cadastro</b>; III – identificação biométrica; ou IV – assinatura eletrônica certificada.
                              <br>§ 1º Serão admitidas outras formas de assinatura, desde que assegurem a sua autenticidade e sejam legalmente permitidas.
                              <br>§ 2º Antes de iniciar a contratação, o interessado deverá ser informado no sentido de que, toda vez em que for solicitada a sua assinatura em qualquer documento, conforme o § 1º do artigo 4º, nas formas previstas no caput deste artigo, significa que estará assinando o documento.
                              <br>§ 3º As reproduções digitalizadas de documentos assinados e enviados pelo interessado fazem a mesma prova que os originais, ressalvada a alegação motivada e fundamentada de adulteração.
                              <br><b> Para maiores informações acesse:</b> <a href = ""http://www.ans.gov.br/component/legislacao/?view=legislacao&task=TextoLei&format=raw&id=MzMyNw"" target = ""_blank"" > http://www.ans.gov.br/component/legislacao/?view=legislacao&task=TextoLei&format=raw&id=MzMyNw </a></p>";
                return this.Conteudo + texto;
            }
        }

        protected virtual ICollection<ContratoHistorico> Historicos { get; set; }
        public IReadOnlyList<ContratoHistorico> HistoricoReadOnly
        {
            get { return Historicos.ToList(); }
        }
        public static Expression<Func<Contrato, ICollection<ContratoHistorico>>> ContratoHistoricoExpression = f => f.Historicos;

        public void AssociarProdutoDePlanoDeSaude(ProdutoDePlanoDeSaude produto, IContratoService contratoService)
        {
            contratoService.CanAssignProdutoDePlanoDeSaude(this, produto);
            ProdutoDePlanoDeSaude = produto;
        }
    }
}
