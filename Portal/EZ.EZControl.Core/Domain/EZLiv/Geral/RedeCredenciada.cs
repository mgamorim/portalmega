﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class RedeCredenciada : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 150;

        public RedeCredenciada()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }

        protected virtual ICollection<ItemDeRedeCredenciada> Items { get; set; }

        public IReadOnlyList<ItemDeRedeCredenciada> ItensReadOnly
        {
            get { return Items.ToList(); }
        }
    }
}
