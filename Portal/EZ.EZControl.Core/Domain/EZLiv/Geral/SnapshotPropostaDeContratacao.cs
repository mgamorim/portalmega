﻿using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class SnapshotPropostaDeContratacao : EzEntity
    {
        public int PropostaDeContratacaoId { get; set; }
        public virtual PropostaDeContratacao PropostaDeContratacao { get; set; }
        public byte[] Json { get; set; }
    }
}
