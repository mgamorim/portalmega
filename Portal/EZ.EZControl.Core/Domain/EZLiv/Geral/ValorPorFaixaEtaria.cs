﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ValorPorFaixaEtaria : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public ValorPorFaixaEtaria()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual FaixaEtaria FaixaEtaria { get; set; }
        public int FaixaEtariaId { get; set; }

        public virtual Contrato Contrato { get; set; }
        public int ContratoId { get; set; }

        public decimal Valor { get; set; }
    }
}
