﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ItemDeDeclaracaoDoBeneficiario : EzFullAuditedEntityMayHaveTenant, IPassivable
    {
        public const int PerguntaMaxLength = 500;

        public ItemDeDeclaracaoDoBeneficiario()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Pergunta { get; set; }
        public int Ordenacao { get; set; }
        public string Ajuda { get; set; }
    }
}
