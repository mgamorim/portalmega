﻿using Abp.Domain.Entities;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Domain.EZLiv.Geral
{
    public class ItemDeRedeCredenciada : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public const int NomeMaxLength = 150;

        public ItemDeRedeCredenciada()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public bool IsRedeDiferenciada { get; set; }
        public int RedeCredenciadaId { get; protected set; }
        public virtual RedeCredenciada RedeCredenciada { get; set; }

        public int UnidadeDeSaudeId { get; protected set; }
        public virtual UnidadeDeSaudeBase UnidadeDeSaude { get; set; }
        public virtual bool IsHospital { get { return UnidadeDeSaude is Hospital; } }
        public virtual bool IsLaboratorio { get { return UnidadeDeSaude is Laboratorio; } }
        public void AssociarRedeCredenciada(IItemDeRedeCredenciadaService intemService, RedeCredenciada redeCredenciada)
        {
            intemService.canAssignRedeCredenciada(redeCredenciada);
            RedeCredenciada = redeCredenciada;
        }

        public void AssociarUnidadeDeSaude(IItemDeRedeCredenciadaService intemService, UnidadeDeSaudeBase unidadeDeSaude)
        {
            intemService.canAssignUnidadeDeSaude(unidadeDeSaude);
            UnidadeDeSaude = unidadeDeSaude;
        }
    }
}
