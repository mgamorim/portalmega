﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class Corretora : SubtipoPessoaJuridicaEntityBase, IPassivable
    {
        public Corretora()
        {
            this.IsActive = true;
        }
        public string Nome { get; set; }
        public bool IsActive { get; set; }
        public virtual ArquivoBase Imagem { get; set; }
        public virtual Corretor Corretor { get; set; }
        public int? CorretorId { get; set; }
        public ICollection<Corretor> Corretores { get; set; }
        public virtual ICollection<SupervisorCorretora> Supervisores { get; set; }
        public virtual ICollection<GerenteCorretora> Gerentes { get; set; }
    }
}
