﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class BeneficiarioBase : SubtipoPessoaFisicaEntityBase, IPassivable
    {
        public BeneficiarioBase()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual TipoDeBeneficiarioEnum TipoDeBeneficiario { get; set; }
        public string NomeDaMae { get; set; }
        public string NumeroDoCartaoNacionalDeSaude { get; set; }
        public string DeclaracaoDeNascidoVivo { get; set; }

        public string matricula { get; set; }

        public bool boleto { get; set; }

        public bool folha { get; set; }

        public bool DebitoConta { get; set; }

        public bool FolhaFicha { get; set; }

        public virtual ICollection<Administradora> Administradoras { get; set; }
        public virtual ICollection<Corretora> Corretoras { get; set; }
    }
}
