﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class Administradora : SubtipoPessoaJuridicaEntityBase, IPassivable
    {
        public Administradora()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public virtual ArquivoBase Imagem { get; set; }
        public virtual ICollection<ProdutoDePlanoDeSaude> ProdutosDePlanoDeSaude { get; set; }
        public virtual ICollection<Corretora> Corretoras { get; set; }
        public virtual ICollection<SupervisorAdministradora> Supervisores { get; set; }
        public virtual ICollection<GerenteAdministradora> Gerentes { get; set; }
        public virtual ICollection<HomologadorAdministradora> Homologadores { get; set; }
        public List<string> IsValidAdministradora()
        {
            var validationErrors = new List<string>();

            if (this.PessoaJuridica != null)
            {
                if (this.PessoaJuridica.EnderecosEletronicos == null)
                    validationErrors.Add("Administradora.EmptyEmailPrincipalError");
                else
                {
                    var email =
                        this.PessoaJuridica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email == null)
                        validationErrors.Add("Administradora.EmptyEmailPrincipalError");
                }
            }
            else
            {
                validationErrors.Add("Administradora.EmptyPessoaJuridicaError");
            }

            return validationErrors;
        }
    }
}
