﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class Responsavel : SubtipoPessoaFisicaEntityBase, IPassivable
    {
        public Responsavel()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public virtual TipoDeResponsavelEnum TipoDeResponsavel { get; set; }



        public List<string> IsValidResponsavel()
        {
            var validationErrors = new List<string>();

            if (this.PessoaFisica != null)
            {
                if (string.IsNullOrEmpty(this.PessoaFisica.Nome))
                    validationErrors.Add("Responsavel.EmptyNomeError");

                if (this.PessoaFisica.DataDeNascimento == null)
                    validationErrors.Add("Responsavel.EmptyDataDeNascimentoError");

                if (this.PessoaFisica.Sexo == null)
                    validationErrors.Add("Responsavel.EmptySexoError");

                if (this.PessoaFisica.EstadoCivil == null)
                    validationErrors.Add("Responsavel.EmptyEstadoCivilError");

                if (string.IsNullOrEmpty(this.PessoaFisica.Nacionalidade))
                    validationErrors.Add("Responsavel.EmptyNacionalidadeError");

                if (this.PessoaFisica.Documentos == null)
                    validationErrors.Add("Responsavel.EmptyDocumentoError");
                else
                {
                    var cpf =
                        this.PessoaFisica.Documentos.FirstOrDefault(
                            x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);

                    if (cpf == null)
                        validationErrors.Add("Responsavel.EmptyCpfError");
                }
            }
            else
            {
                validationErrors.Add("Responsavel.EmptyPessoaFisicaError");
            }


            return validationErrors;
        }
    }
}
