﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class CorretoraNotificationInfo
    {
        public string NomeFantasia { get; set; }
        public string RazaoSocial { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Logo { get; set; }

        public static CorretoraNotificationInfo CriarCorretoraDefault()
        {
            return new CorretoraNotificationInfo
            {
                NomeFantasia = "Evida",
                RazaoSocial = "Evida Saúde e Seguros",
                Email = "contato@evida.online",
                Telefone = "021 3490-0330",
                Logo = "http://www.evida.online/images/brand-default-357x52.png"
            };
        }
    }
}
