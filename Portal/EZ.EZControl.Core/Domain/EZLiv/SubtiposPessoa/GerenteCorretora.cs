﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class GerenteCorretora : SubtipoPessoaEntityBase, IPassivable
    {
        public GerenteCorretora()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public virtual Corretora Corretora { get; set; }
        public int CorretoraId { get; set; }
        public List<string> IsValidGerenteCorretora()
        {
            var validationErrors = new List<string>();
            if (this.PessoaFisica.GrupoPessoa == null || this.PessoaFisica.GrupoPessoaId == 0)
                validationErrors.Add("Corretor.EmptyGrupoPessoaError");
            if (this.PessoaFisica.Documentos == null || this.PessoaFisica.Documentos.Count == 0)
                validationErrors.Add("Corretor.EmptyDocumentoError");
            else
            {
                var cpf =
                    this.PessoaFisica.Documentos.FirstOrDefault(
                        x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);
                if (cpf == null)
                    validationErrors.Add("Corretor.EmptyCPFError");
            }
            return validationErrors;
        }
    }
}
