﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class ClienteEZLiv : EzEntityMustHaveTenantMustHaveEmpresa, IPassivable
    {
        public ClienteEZLiv()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public virtual Cliente Cliente { get; set; }
        public int ClienteId { get; set; }
        public string Observacao { get; set; }


        public void AssociarCliente(IClienteEZLivService clienteEZLiv, Cliente cliente)
        {
            clienteEZLiv.CanAssignCliente(this, cliente);
            Cliente = cliente;
        }
    }
}