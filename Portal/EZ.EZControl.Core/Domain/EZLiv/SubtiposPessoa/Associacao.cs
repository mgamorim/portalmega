﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class Associacao : SubtipoPessoaJuridicaEntityBase, IPassivable
    {
        public Associacao()
        {
            this.IsActive = true;
        }
        public bool IsActive { get; set; }
        public virtual ICollection<Profissao> Profissoes { get; set; }
        public virtual ArquivoBase Imagem { get; set; }
        public string Instrucoes { get; set; }
    }
}
