﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class CorretorDadosBancario : SubtipoPessoaEntityBase, IPassivable
    {
        public CorretorDadosBancario() => this.IsActive = true;

        public bool IsActive { get; set; }

        public virtual Corretor Corretor { get; set; }
        public int CorretorId { get; set; }
        //public virtual ICollection<Banco> Bancos { get; set; }
        public int BancoId { get; set; }
        public string TipoConta { get; set; }
        public string CodigoAgencia { get; set; }        
        public string NomeAgencia { get; set; }
        public string DigitoVerificador { get; set; }
        public string ContaCorrente { get; set; }
        public string NomeFavorecido { get; set; }
    }
}
