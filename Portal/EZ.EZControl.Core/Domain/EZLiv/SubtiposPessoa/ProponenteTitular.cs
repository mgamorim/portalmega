﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.Global.Enums;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class ProponenteTitular : BeneficiarioBase
    {
        public ProponenteTitular()
        {
            base.TipoDeBeneficiario = TipoDeBeneficiarioEnum.Titular;
            this.StatusBeneficiario = StatusTipoDeBeneficiarioTitularEnum.PreCadastradoPeloCorretor;
        }

        public virtual Responsavel Responsavel { get; set; }
        public virtual ICollection<Dependente> Dependentes { get; set; }
        public virtual StatusTipoDeBeneficiarioTitularEnum StatusBeneficiario { get; set; }

        public List<string> IsValidTitular()
        {
            var validationErrors = new List<string>();

            if (this.PessoaFisica != null)
            {
                if (string.IsNullOrEmpty(this.PessoaFisica.Nome))
                    validationErrors.Add("Proponente.EmptyNomeError");

                if (this.PessoaFisica.DataDeNascimento == null)
                    validationErrors.Add("Proponente.EmptyDataDeNascimentoError");

                if (this.PessoaFisica.Sexo == null)
                    validationErrors.Add("Proponente.EmptySexoError");

                if (this.PessoaFisica.EstadoCivil == null)
                    validationErrors.Add("Proponente.EmptyEstadoCivilError");

                if (string.IsNullOrEmpty(this.PessoaFisica.Nacionalidade))
                    validationErrors.Add("Proponente.EmptyNacionalidadeError");

                if (string.IsNullOrEmpty(this.NomeDaMae))
                    validationErrors.Add("Proponente.EmptyNomeDaMaeError");

                if (this.PessoaFisica.Documentos == null)
                    validationErrors.Add("Proponente.EmptyDocumentoError");
                else
                {
                    var cpf =
                        this.PessoaFisica.Documentos.FirstOrDefault(
                            x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);

                    var rg =
                        this.PessoaFisica.Documentos.FirstOrDefault(
                            x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Rg);

                    if (cpf == null)
                        validationErrors.Add("Proponente.EmptyCpfError");

                    if (rg == null)
                        validationErrors.Add("Proponente.EmptyRgError");
                    else
                    {
                        if (string.IsNullOrEmpty(rg.OrgaoExpedidor))
                            validationErrors.Add("Proponente.EmptyOrgaoExpeditorError");
                    }
                }

                if (this.PessoaFisica.Telefones == null)
                    validationErrors.Add("Proponente.EmptyTelefoneError");

                if (this.PessoaFisica.Enderecos == null)
                    validationErrors.Add("Proponente.EmptyEnderecoError");
                else
                {
                    var residencial =
                        this.PessoaFisica.Enderecos.FirstOrDefault(x => x.TipoEndereco == TipoEnderecoEnum.Residencial);

                    if (residencial == null)
                        validationErrors.Add("Proponente.EmptyEnderecoResidencialError");

                    var enderecoEntrega =
                        this.PessoaFisica.Enderecos.FirstOrDefault(x => x.EnderecoParaEntrega);

                    if (enderecoEntrega == null)
                        validationErrors.Add("Proponente.EmptyEnderecoEntregaError");
                }

                if (this.PessoaFisica.EnderecosEletronicos == null)
                    validationErrors.Add("Proponente.EmptyEmailPessoalError");
                else
                {
                    var email =
                        this.PessoaFisica.EnderecosEletronicos.FirstOrDefault(
                            x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);

                    if (email == null)
                        validationErrors.Add("Proponente.EmptyEmailPessoalError");
                }
            }
            else
            {
                validationErrors.Add("Proponente.EmptyPessoaFisicaError");
            }


            return validationErrors;
        }
    }
}
