﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class Operadora : SubtipoPessoaJuridicaEntityBase, IPassivable
    {
        public const int NomeMaxLength = 100;
        public const int CodigoAnsMaxLength = 100;

        public Operadora()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string CodigoAns { get; set; }
        public virtual ArquivoBase Imagem { get; set; }
        public int? ImagemId { get; set; }
        public virtual ICollection<ProdutoDePlanoDeSaude> ProdutosDePlanoDeSaude { get; set; }
    }
}
