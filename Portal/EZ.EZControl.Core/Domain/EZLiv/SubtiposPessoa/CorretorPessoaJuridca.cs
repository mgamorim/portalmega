﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzEntity;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class CorretorPessoaJuridca : SubtipoPessoaEntityBase, IPassivable
    {
        public CorretorPessoaJuridca()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }

        public virtual Corretor Corretor { get; set; }
        public int CorretorId { get; set; }
        public string Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Observacao { get; set; }
    }
}
