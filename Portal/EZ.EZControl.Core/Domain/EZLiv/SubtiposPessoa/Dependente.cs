﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.Global.Enums;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Domain.EZLiv.SubtiposPessoa
{
    public class Dependente : BeneficiarioBase
    {
        public Dependente()
        {
            base.TipoDeBeneficiario = TipoDeBeneficiarioEnum.Dependente;
        }

        //public ProponenteTitular Titular { get; set; }

        //public int TitularId { get; set; }

        public GrauDeParentescoEnum GrauDeParentesco { get; set; }

        public List<string> IsValidDependente()
        {
            var validationErrors = new List<string>();

            if (this.PessoaFisica != null)
            {
                if (string.IsNullOrEmpty(this.PessoaFisica.Nome))
                    validationErrors.Add("Dependente.EmptyNomeError");

                if (this.PessoaFisica.DataDeNascimento == null)
                    validationErrors.Add("Dependente.EmptyDataDeNascimentoError");

                if (this.PessoaFisica.Sexo == null)
                    validationErrors.Add("Dependente.EmptySexoError");

                if (this.PessoaFisica.EstadoCivil == null)
                    validationErrors.Add("Dependente.EmptyEstadoCivilError");

                if (string.IsNullOrEmpty(this.PessoaFisica.Nacionalidade))
                    validationErrors.Add("Dependente.EmptyNacionalidadeError");

                if (string.IsNullOrEmpty(this.NomeDaMae))
                    validationErrors.Add("Dependente.EmptyNomeDaMaeError");

                if (this.PessoaFisica.Documentos == null)
                    validationErrors.Add("Dependente.EmptyDocumentoError");
                else
                {
                    var cpf =
                        this.PessoaFisica.Documentos.FirstOrDefault(
                            x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);

                    if (cpf == null)
                        validationErrors.Add("Proponente.EmptyCpfError");
                }
            }
            else
            {
                validationErrors.Add("Proponente.EmptyPessoaFisicaError");
            }


            return validationErrors;
        }
    }
}
