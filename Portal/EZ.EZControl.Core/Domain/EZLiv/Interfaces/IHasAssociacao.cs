﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;

namespace EZ.EZControl.Domain.EZLiv.Interfaces
{
    public interface IHasAssociacao
    {
        Associacao Associacao { get; set; }
        int AssociacaoId { get; set; }
    }
}
