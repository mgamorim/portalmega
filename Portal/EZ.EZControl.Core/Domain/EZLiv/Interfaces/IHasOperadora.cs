﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;

namespace EZ.EZControl.Domain.EZLiv.Interfaces
{
    public interface IHasOperadora
    {
        Operadora Operadora { get; set; }
        int OperadoraId { get; set; }
    }
}
