﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;

namespace EZ.EZControl.Domain.EZLiv.Interfaces
{
    public interface IHasAdministradora
    {
        Administradora Administradora { get; set; }
        int AdministradoraId { get; set; }
    }
}
