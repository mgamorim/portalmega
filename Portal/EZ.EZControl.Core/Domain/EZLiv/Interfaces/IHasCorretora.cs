﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;

namespace EZ.EZControl.Domain.EZLiv.Interfaces
{
    public interface IHasCorretora
    {
        Corretora Corretora { get; set; }
        int CorretoraId { get; set; }
    }
}
