﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;

namespace EZ.EZControl.Domain.EZLiv.Interfaces
{
    public interface IHasCorretor
    {
        Corretor Corretor { get; set; }
        int CorretorId { get; set; }
    }
}
