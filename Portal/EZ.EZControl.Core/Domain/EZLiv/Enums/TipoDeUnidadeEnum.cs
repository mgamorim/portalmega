﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeUnidadeEnum
    {
        Laboratorio = 1,
        Hospital = 2
    }
}
