﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeRelatorioEzlivEnum
    {
        PropostaDeContratacao = 1,
        FichaDeAssociacao = 2,
        Aditivo = 3
    }
}
