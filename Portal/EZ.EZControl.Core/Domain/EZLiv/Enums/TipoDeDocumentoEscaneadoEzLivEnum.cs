﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeDocumentoEscaneadoEzLivEnum
    {
        Formulario = 10,
        Proposta = 11,
        Aditivo = 12,
        Chancela = 13,
        ComprovanteDeclaracaoDoBeneficiario = 14,
        Cpf = 1,
        Cnpj = 2,
        Rg = 3,
        Passaporte = 4,
        CarteiraDeTrabalho = 5,
        Pis = 6,
        ComprovanteDeResidencia = 7,
        ComprovanteDeProfissao = 8,
        Outros = 9
    }
}
