﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum AbrangenciaDoPlanoEnum
    {
        NaoInformado = 0,
        Nacional = 1,
        GrupoDeEstados = 2,
        Estadual = 3,
        GrupoDeMunicipios = 4,
        Municipal = 5,
        Outra = 6
    }
}
