﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum PassoDaPropostaEnum
    {
        PreCadastro = 10,
        SelecionarPlano = 20,
        PreenchimentoDosDados = 30,
        #region AbasDaProposta
        DadosProponenteTitular = 31,
        DadosResponsavel = 32,
        Endereco = 33,
        DadosDependentes = 34,
        Documentos = 35,
        DadosVigencia = 36,
        DadosDeclaracaoDeSaude = 37,
        DadosGerenciais = 38,
        #endregion
        EnvioDeDocumentos = 40,
        AceiteDoContrato = 50,
        Homologacao = 60,
        Pagamento = 70
    }
}
