﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum SegmentacaoAssistencialDoPlanoEnum
    {
        //NaoInformado = 0,
        //Ambulatorial = 1,
        //HospComObstetricia = 2,
        //HospSemObstetricia = 3,
        //Odontologico = 4,
        //Referencia = 5,
        //HospComObstetriciaAmbulatorial = 6,
        //HospSemObstetriciaAmbulatorial = 7,
        //AmbulatorialMaisOdonto = 8,
        //HospComOuSemObstetricia = 9,
        //HospComObstetriciaMaisOdonto = 10,
        //HospSemObstetriciaOdonto = 11,
        //HospComOuSemObstetriciaMaisAmbulatorial = 12,
        //HospComObstetriciaMaisAmbulatorialMaisOdonto = 13,
        //HospSemObstetriciaMaisAmbulatorialMaisOdonto = 14,
        //HospComOuSemObstetriciaMaisOdonto = 15,
        //testemauricio = 16,
        //InformadoIncorretamente = 99,


        NaoInformado = 0,
        Ambulatorial = 1,
        Dental = 2,
        Hospitalar = 3,
        HospitalarAmbulatorial = 4,
        HospitalarObstetricia = 5,
        HospitalarAmbulatorialObstetricia = 6,
        HospitalarAmbulatorialDental = 7,
        HospitalarObstetriciaDental = 8,
        HospitalarAmbulatorialObstetriciaDental = 9


    }
}
