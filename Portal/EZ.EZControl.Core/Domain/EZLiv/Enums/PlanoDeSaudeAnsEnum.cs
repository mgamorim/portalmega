﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum PlanoDeSaudeAnsEnum
    {
        Safira207 = 1,
        Rubi207 = 2,
        Diamante207 = 3,
        Diamante217 = 4,
        Rubi210 = 5,
        Rubi230 = 6,
        Safira210 = 7,
        Safira230 = 8,
        Diamante206 = 9,
        Diamante216 = 10
    }
}
