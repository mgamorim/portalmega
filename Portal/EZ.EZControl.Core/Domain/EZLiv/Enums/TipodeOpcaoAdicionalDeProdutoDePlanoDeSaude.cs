﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipodeOpcaoAdicionalDeProdutoDePlanoDeSaudeEnum
    {
        AtendimentoDomiciliarDeUrgencia = 1,
        PlanoOdontologico = 2
    }
}
