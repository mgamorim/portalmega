﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeBeneficiarioEnum
    {
        Titular = 1,
        Dependente = 2
    }
}
