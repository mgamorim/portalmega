﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeResponsavelEnum
    {
        PaiMae = 1,
        CuradorTutor = 2,
        TermoDeGuarda = 3,
        PadrastoMadastra = 4,
        Avos = 5
    }
}
