﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum ModalidadeDeOperadoraEnum
    {
        Administradora = 21,
        CooperativaMedica = 22,
        CooperativaOdontologica = 23,
        Autogestao = 24,
        MedicinadeGrupo = 25,
        OdontologiaDeGrupo = 26,
        Filantropia = 27,
        SeguradoraEspecializadaEmSaude = 28,
        Seguradora = 29,
        AdministradoraDeBeneficios = 55
    }
}
