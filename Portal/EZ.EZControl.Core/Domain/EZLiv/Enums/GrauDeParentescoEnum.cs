﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum GrauDeParentescoEnum
    {
        Conjuge = 1,
        Filho = 2,
        Neto = 3
    }
}
