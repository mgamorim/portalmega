﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum FormaDeContratacaoEnum
    {
        Corporativo = 1,
        Fisico = 2,
        Adesao = 3
    }
}
