﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum GrupoDeSegmentacaoAssistencialDoPlanoEnum
    {
        NaoInformado = 0,
        Ambulatorial = 1,
        Hospitalar = 2,
        HospitalarEAmbulatorial = 3,
        Referencia = 4,
        Odontologico = 5,
        InformadoIncorretamente = 6
    }
}
