﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeHomologacaoEnum
    {
        PendenteDeHomologacao = 1,
        Homologado = 2,
        Declinado = 3,
        EmExigencia = 4
    }
}
