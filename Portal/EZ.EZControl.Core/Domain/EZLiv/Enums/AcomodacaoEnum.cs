﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum AcomodacaoEnum
    {
        Enfermaria = 1,
        Apartamento = 2,
        NaoInformado = 3
    }
}
