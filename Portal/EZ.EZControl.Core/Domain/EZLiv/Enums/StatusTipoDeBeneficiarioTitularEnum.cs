﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum StatusTipoDeBeneficiarioTitularEnum
    {
        Ativo = 1,
        PreCadastradoPeloCorretor = 2,
        PreCadastrado = 3,
        Inativo = 4
    }
}
