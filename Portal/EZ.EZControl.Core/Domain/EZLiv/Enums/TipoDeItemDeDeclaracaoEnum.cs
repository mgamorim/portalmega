﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum TipoDeItemDeDeclaracaoEnum
    {
        Generico = 1,
        Altura = 2,
        Peso = 3
    }
}
