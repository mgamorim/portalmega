﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum ContratacaoDePlanoEnum
    {
        NaoInformado = 0,
        IndividualOuFamiliar = 1,
        ColetivoEmpresarial = 2,
        ColetivoPorAdesao = 3,
        ColetivoNaoIdentificado = 4
    }
}
