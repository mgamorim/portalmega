﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum VigenciaDePlanoEnum
    {
        /// <summary>
        /// Anteriores à Lei 9656/1998
        /// </summary>
        Antigos = 1,

        /// <summary>
        /// Posteriores à Lei 9656/1998
        /// </summary>
        Novos = 2
    }
}
