﻿namespace EZ.EZControl.Domain.EZLiv.Enums
{
    public enum StatusDaPropostaEnum
    {
        Concluido = 1,
        Pendente = 2,
        PendenteDeDocumento = 3,
        PendenteDePagamento = 4,
        EmValidacao = 5,
        Encerrado = 6
    }
}
