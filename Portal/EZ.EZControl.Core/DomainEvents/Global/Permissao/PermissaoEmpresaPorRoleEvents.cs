﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Abp.Runtime.Caching;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Permissao;
using System.Threading;

namespace EZ.EZControl.DomainEvents.Global.Permissao
{
    public class PermissaoEmpresaPorRoleEvents :
        IEventHandler<EntityChangedEventData<PermissaoEmpresaPorRole>>,
        IEventHandler<EntityCreatedEventData<PermissaoEmpresaPorRole>>,
        IEventHandler<EntityDeletedEventData<PermissaoEmpresaPorRole>>,
        ITransientDependency
    {
        private readonly ICacheManager _cacheManager;

        public PermissaoEmpresaPorRoleEvents(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public void HandleEvent(EntityChangedEventData<PermissaoEmpresaPorRole> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var roleId = eventData.Entity.RoleId;
            var cacheKey = roleId + "@" + (eventData.Entity.TenantId ?? 0) + "@" + empresaId;
            _cacheManager.GetCache(PermissaoEmpresaPorRole.CacheStoreName).Remove(cacheKey);
        }

        public void HandleEvent(EntityCreatedEventData<PermissaoEmpresaPorRole> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var roleId = eventData.Entity.RoleId;
            var cacheKey = roleId + "@" + (eventData.Entity.TenantId ?? 0) + "@" + empresaId;
            _cacheManager.GetCache(PermissaoEmpresaPorRole.CacheStoreName).Remove(cacheKey);
        }

        public void HandleEvent(EntityDeletedEventData<PermissaoEmpresaPorRole> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var roleId = eventData.Entity.RoleId;
            var cacheKey = roleId + "@" + (eventData.Entity.TenantId ?? 0) + "@" + empresaId;
            _cacheManager.GetCache(PermissaoEmpresaPorRole.CacheStoreName).Remove(cacheKey);
        }
    }
}