﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Abp.Runtime.Caching;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Permissao;
using System.Threading;

namespace EZ.EZControl.DomainEvents.Global.Permissao
{
    internal class PermissaoEmpresaPorUsuarioEvents :
        IEventHandler<EntityChangedEventData<PermissaoEmpresaPorUsuario>>,
        IEventHandler<EntityCreatedEventData<PermissaoEmpresaPorUsuario>>,
        IEventHandler<EntityDeletedEventData<PermissaoEmpresaPorUsuario>>,
        ITransientDependency
    {
        private readonly ICacheManager _cacheManager;

        public PermissaoEmpresaPorUsuarioEvents(ICacheManager cacheManager)
        {
            _cacheManager = cacheManager;
        }

        public void HandleEvent(EntityChangedEventData<PermissaoEmpresaPorUsuario> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var userId = eventData.Entity.UserId;
            var cacheKey = userId + "@" + (eventData.Entity.TenantId ?? 0) + "@" + empresaId;
            _cacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).Remove(cacheKey);
        }

        public void HandleEvent(EntityCreatedEventData<PermissaoEmpresaPorUsuario> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var userId = eventData.Entity.UserId;
            var cacheKey = userId + "@" + (eventData.Entity.TenantId ?? 0) + "@" + empresaId;
            _cacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).Remove(cacheKey);
        }

        public void HandleEvent(EntityDeletedEventData<PermissaoEmpresaPorUsuario> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var userId = eventData.Entity.UserId;
            var cacheKey = userId + "@" + (eventData.Entity.TenantId ?? 0) + "@" + empresaId;
            _cacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).Remove(cacheKey);
        }
    }
}