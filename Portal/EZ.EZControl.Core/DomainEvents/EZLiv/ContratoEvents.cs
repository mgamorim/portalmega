﻿using Abp.Dependency;
using Abp.Events.Bus.Entities;
using Abp.Events.Bus.Handlers;
using Abp.Runtime.Caching;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Threading;

namespace EZ.EZControl.DomainEvents.Global.Permissao
{
    internal class ContratoEvents :
        IEventHandler<EntityChangedEventData<Contrato>>,
        IEventHandler<EntityCreatedEventData<Contrato>>,
        IEventHandler<EntityDeletedEventData<Contrato>>,
        ITransientDependency
    {
        private readonly ICacheManager _cacheManager;
        private readonly IContratoHistoricoService _contratoHistorico;

        public ContratoEvents(ICacheManager cacheManager,
            IContratoHistoricoService contratoHistorico)
        {
            _cacheManager = cacheManager;
            _contratoHistorico = contratoHistorico;
        }

        private void GravarHistorico(EntityEventData<Contrato> eventData, bool create)
        {
            //var contratoHistorico = new ContratoHistorico
            //{
            //    UserId = create ? eventData.Entity.CreatorUserId.Value : eventData.Entity.LastModifierUserId.Value,
            //    DataEvento = eventData.EventTime,
            //    DataCriacao = eventData.Entity.DataCriacao,
            //    DataInicioVigencia = eventData.Entity.DataInicioVigencia,
            //    DataFimVigencia = eventData.Entity.DataFimVigencia,
            //    Conteudo = eventData.Entity.Conteudo,
            //    ProdutoDePlanoDeSaude = eventData.Entity.ProdutoDePlanoDeSaude,
            //    Empresa = eventData.Entity.Empresa,
            //    Contrato = eventData.Entity
            //};

            //_contratoHistorico.CreateOrUpdateAndReturnSavedEntity(contratoHistorico);
        }

        public void HandleEvent(EntityChangedEventData<Contrato> eventData)
        {
            //var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            //var userId = eventData.Entity.LastModifierUserId;
            //var cacheKey = userId + "@" + eventData.Entity.TenantId + "@" + empresaId;
            //_cacheManager.GetCache(Contrato.CacheStoreName).Remove(cacheKey);
            GravarHistorico(eventData, false);
        }

        public void HandleEvent(EntityCreatedEventData<Contrato> eventData)
        {
            //var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            //var userId = eventData.Entity.CreatorUserId;
            GravarHistorico(eventData, true);
        }

        public void HandleEvent(EntityDeletedEventData<Contrato> eventData)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var userId = eventData.Entity.LastModifierUserId;
            var cacheKey = userId + "@" + eventData.Entity.TenantId + "@" + empresaId;
            //_cacheManager.GetCache(Contrato.CacheStoreName).Remove(cacheKey);
        }
    }
}