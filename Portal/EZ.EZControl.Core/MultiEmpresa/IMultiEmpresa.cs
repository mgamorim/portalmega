﻿using Abp.Dependency;
using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.MultiEmpresa
{
    public interface IMultiEmpresa : ITransientDependency
    {
        bool IsMultiEmpresa { get; }
        Empresa EmpresaLogada();
    }
}