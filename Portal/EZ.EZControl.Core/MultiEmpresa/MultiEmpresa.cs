﻿using Abp.Domain.Uow;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System.Linq;
using System.Threading;

namespace EZ.EZControl.MultiEmpresa
{
    public class MultiEmpresa : IMultiEmpresa
    {
        private readonly IEmpresaService _empresaService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public MultiEmpresa(IEmpresaService empresaService, IUnitOfWorkManager unitOfWorkManager)
        {
            _empresaService = empresaService;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public bool IsMultiEmpresa
        {
            get
            {
                bool startedUow = false;
                bool isMultiEmpresa = false;
                IUnitOfWorkCompleteHandle uow = null;

                try
                {
                    if (_unitOfWorkManager.Current == null)
                    {
                        startedUow = true;
                        uow = _unitOfWorkManager.Begin();
                    }

                    isMultiEmpresa = _empresaService.GetAll().Count() > 1;
                }
                finally
                {
                    if (startedUow)
                    {
                        uow.Complete();
                        uow.Dispose();
                    }
                }

                return isMultiEmpresa;
            }
        }

        public Empresa EmpresaLogada()
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
            var empresa = _empresaService.GetAll().FirstOrDefault(x => x.Id == empresaId);
            return empresa;
        }
    }
}