﻿using Abp.Domain.Services;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Filters;
using System;
using System.Threading;

namespace EZ.EZControl
{
    public abstract class EZControlDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected EZControlDomainServiceBase()
        {
            LocalizationSourceName = EZControlConsts.LocalizationSourceName;
        }

        /// <summary>
        /// Somente é aplicado se o filtro estiver ativo. Por padrão é ativo.
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func"></param>
        /// <returns></returns>
        protected TResult ApplyEmpresaFilter<TResult>(Func<TResult> func)
        {
            if (func == null)
            {
                throw new NotSupportedException("Os parâmetros do método ExecActionIfHasEmpresa não podem estar nulos.");
            }

            if (CurrentUnitOfWork != null && CurrentUnitOfWork.IsFilterEnabled(EzControlFilters.EmpresaFilter))
            {
                var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

                if (empresaId <= 0)
                {
                    throw new NotSupportedException("Não foi possível recuperar a empresaId da Claim Principal.");
                }

                using (CurrentUnitOfWork.EnableFilter(EzControlFilters.EmpresaFilter))
                {
                    using (CurrentUnitOfWork.SetFilterParameter(EzControlFilters.EmpresaFilter, EzControlFilters.Parameters.EmpresaId, empresaId))
                    {
                        return func();
                    }
                }
            }

            return func();
        }

        /// <summary>
        /// Somente é aplicado se o filtro estiver ativo. Por padrão é ativo.
        /// </summary>
        /// <param name="action"></param>
        protected void ApplyEmpresaFilter(Action action)
        {
            if (action == null)
            {
                throw new NotSupportedException("Os parâmetros do método ExecActionIfHasEmpresa não podem estar nulos.");
            }

            if (CurrentUnitOfWork != null && CurrentUnitOfWork.IsFilterEnabled(EzControlFilters.EmpresaFilter))
            {
                var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

                if (empresaId <= 0)
                {
                    throw new NotSupportedException("Não foi possível recuperar a empresaId da Claim Principal.");
                }

                using (CurrentUnitOfWork.EnableFilter(EzControlFilters.EmpresaFilter))
                {
                    using (CurrentUnitOfWork.SetFilterParameter(EzControlFilters.EmpresaFilter, EzControlFilters.Parameters.EmpresaId, empresaId))
                    {
                        action();
                    }
                }
            }
        }
    }
}