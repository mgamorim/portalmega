﻿using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace EZ.EZControl.Extensions
{
    public static class MyRepositoryExtensions
    {
        public static void Insert<TEntity, TPrimaryKey>(this IRepository<TEntity, TPrimaryKey> repository, IEnumerable<TEntity> entities)
            where TEntity : class, IEntity<TPrimaryKey>
        {
            var type = Type.GetType("EZ.EZControl.Helpers.RepositoryHelper, EZ.EZControl.EntityFramework");

            if (type == null)
            {
                throw new Exception("Tipo do RepositoryHelper não encontrado.");
            }

            var bulkInsertMethod = type.GetMethod("BulkInsert", BindingFlags.Static | BindingFlags.Public);

            var genericMethod = bulkInsertMethod.MakeGenericMethod(typeof(TEntity), typeof(TPrimaryKey));

            genericMethod.Invoke(null, new object[] { repository, entities });
        }
    }
}