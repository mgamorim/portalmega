﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abp;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Filters;

namespace EZ.EZControl.Extensions
{
    public static class AbpServiceBaseExtensions
    {
        public static void AplicarFiltroDinamicoEmpresaFilter(this AbpServiceBase abpServiceBase, Type tEntity, IActiveUnitOfWork currentUnitOfWork)
        {
            var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (claimsPrincipal != null)
            {
                ClaimsIdentity identity = claimsPrincipal.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    var userIdString = identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier);
                    if (userIdString != null)
                    {
                        var userId = Convert.ToInt32(userIdString.Value);

                        if (userId > 0)
                        {
                            if (tEntity.GetInterface("IHasEmpresa") != null && currentUnitOfWork != null)
                            {
                                currentUnitOfWork.SetFilterParameter(EzControlFilters.EmpresaFilter, EzControlFilters.Parameters.EmpresaId,
                                    claimsPrincipal.GetEmpresaIdFromClaim());
                            }
                        }
                    }
                }
            }
        }
    }
}
