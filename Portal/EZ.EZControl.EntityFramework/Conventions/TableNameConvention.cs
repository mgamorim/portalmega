﻿using EZ.EZControl.Attributes;
using EZ.EZControl.Domain.Core.Enums;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;

namespace EZ.EZControl.Conventions
{
    public class TableNameConvention : Convention
    {
        public TableNameConvention()
        {
            this.Types().Configure(x =>
            {
                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Global"))
                {
                    SistemaAttribute attr = SistemaEnum.Global.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Glb";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.CMS"))
                {
                    SistemaAttribute attr = SistemaEnum.CMS.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Cms";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Core"))
                {
                    SistemaAttribute attr = SistemaEnum.CMS.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Cor";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Agenda"))
                {
                    SistemaAttribute attr = SistemaEnum.Agenda.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Agd";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Estoque"))
                {
                    SistemaAttribute attr = SistemaEnum.Estoque.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Etq";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Vendas"))
                {
                    SistemaAttribute attr = SistemaEnum.Vendas.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Vnd";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Servicos"))
                {
                    SistemaAttribute attr = SistemaEnum.Servicos.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Srv";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.EZMedical"))
                {
                    SistemaAttribute attr = SistemaEnum.EZMedical.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Med";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.Financeiro"))
                {
                    SistemaAttribute attr = SistemaEnum.Financeiro.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Fin";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.EZLiv"))
                {
                    SistemaAttribute attr = SistemaEnum.EZLiv.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Sau";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.EZPag"))
                {
                    SistemaAttribute attr = SistemaEnum.EZPag.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Pag";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }

                if (x.ClrType.Namespace.Contains("EZ.EZControl.Domain.CRM"))
                {
                    SistemaAttribute attr = SistemaEnum.CRM.GetType().GetCustomAttribute<SistemaAttribute>();
                    string prefixo = attr != null ? attr.Prefixo : "Crm";
                    x.ToTable(string.Concat(prefixo, "_", x.ClrType.Name));
                }
            });
        }
    }
}