﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzNotification;
using EZ.EZControl.EZ.EzNotification.Enums;
using EZ.EZControl.EZ.EzNotification.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Infra.EzNotification.Services
{
    public class EmailNotificationService : EZControlServiceBase, IEmailNotificationService
    {
        private readonly IConexaoSMTPService _conexaoSmtpService;
        private readonly ITemplateEmailService _templateEmailService;
        private readonly IParametroGlobalService _parametroGlobalService;

        public EmailNotificationService(
            IConexaoSMTPService conexaoSmtpService,
            ITemplateEmailService templateEmailService,
            IParametroGlobalService parametroGlobalService)
        {
            _conexaoSmtpService = conexaoSmtpService;
            _templateEmailService = templateEmailService;
            _parametroGlobalService = parametroGlobalService;
        }

        public async Task Send(string destinatario, TemplateEmail templateEmail)
        {
            try
            {
                ValidateEmailNotification(destinatario, templateEmail);

                var parametro = await _parametroGlobalService.FirstOrDefault(x => x.ConexaoSMTPId > 0);
                if (parametro == null)
                {
                    throw new UserFriendlyException(L("Parametro.EmptyConexaoSMTPError"));
                }

                var smtpClient = ConfiguracaoSmtp(parametro.ConexaoSMTP.RequerAutenticacao, parametro.ConexaoSMTP.ServidorSMTP, parametro.ConexaoSMTP.Porta,
                                                       parametro.ConexaoSMTP.RequerConexaoCriptografada, parametro.ConexaoSMTP.UsuarioAutenticacao, parametro.ConexaoSMTP.SenhaAutenticacao);

                var mailMessage = ConfiguracaoMailMessage(templateEmail.Remetente, templateEmail.Assunto,
                                                               templateEmail.CorpoMensagem, destinatario);

                await smtpClient.SendMailAsync(mailMessage);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task Send(string destinatario, TemplateEmail templateEmail, ConexaoSMTP conexaoSmtp)
        {
            try
            {
                ValidateEmailNotification(destinatario, templateEmail);

                if (conexaoSmtp == null)
                {
                    var parametro = await _parametroGlobalService.FirstOrDefault(x => x.ConexaoSMTPId > 0);

                    if (parametro == null)
                    {
                        throw new UserFriendlyException(L("Parametro.EmptyConexaoSMTPError"));
                    }

                    conexaoSmtp = parametro.ConexaoSMTP;
                }

                var smtpClient = ConfiguracaoSmtp(conexaoSmtp.RequerAutenticacao, conexaoSmtp.ServidorSMTP, conexaoSmtp.Porta,
                                                       conexaoSmtp.RequerConexaoCriptografada, conexaoSmtp.UsuarioAutenticacao, conexaoSmtp.SenhaAutenticacao);

                var mailMessage = ConfiguracaoMailMessage(templateEmail.Remetente, templateEmail.Assunto,
                                                               templateEmail.CorpoMensagem, destinatario);

                await smtpClient.SendMailAsync(mailMessage);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task SendComSmtp(string destinatario, int smtpId, int templateDeEmailId)
        {
            try
            {
                var smtp = await _conexaoSmtpService.GetById(smtpId);
                var template = await _templateEmailService.GetById(templateDeEmailId);

                var smtpClient = ConfiguracaoSmtp(smtp.RequerAutenticacao, smtp.ServidorSMTP, smtp.Porta,
                                                       smtp.RequerConexaoCriptografada, smtp.UsuarioAutenticacao, smtp.SenhaAutenticacao);

                var mailMessage = ConfiguracaoMailMessage(template.Remetente, template.Assunto,
                                                               template.CorpoMensagem, destinatario);

                await smtpClient.SendMailAsync(mailMessage);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task SendComParametro(string destinatario, int parametroId, int templateDeEmailId)
        {
            try
            {
                var parametro = await _parametroGlobalService.GetById(parametroId);
                var template = await _templateEmailService.GetById(templateDeEmailId);

                var smtpClient = ConfiguracaoSmtp(parametro.ConexaoSMTP.RequerAutenticacao, parametro.ConexaoSMTP.ServidorSMTP, parametro.ConexaoSMTP.Porta,
                                                       parametro.ConexaoSMTP.RequerConexaoCriptografada, parametro.ConexaoSMTP.UsuarioAutenticacao, parametro.ConexaoSMTP.SenhaAutenticacao);

                var mailMessage = ConfiguracaoMailMessage(template.Remetente, template.Assunto,
                                                               template.CorpoMensagem, destinatario);

                await smtpClient.SendMailAsync(mailMessage);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        private SmtpClient ConfiguracaoSmtp(bool requerAutenticacao, string host, int porta, bool enableSsl, string usuario, string senha)
        {
            return new SmtpClient
            {
                UseDefaultCredentials = requerAutenticacao,
                Host = host,
                Port = porta,
                EnableSsl = enableSsl,
                Credentials = new NetworkCredential(usuario, senha),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
        }

        private MailMessage ConfiguracaoMailMessage(string remetente, string assunto, string corpoMensagem, string destinatario)
        {
            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(remetente),
                IsBodyHtml = true,
                Subject = assunto,
                Body = corpoMensagem
            };
            mailMessage.To.Add(destinatario);

            return mailMessage;
        }

        private void ValidateEmailNotification(string destinatario, TemplateEmail template)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(destinatario))
                validationErrors.Add(new ValidationResult("EmailNotification.DestinatarioEmpty"));

            if (!string.IsNullOrEmpty(destinatario) && !EnderecoEletronico.IsValidEmail(destinatario))
                validationErrors.Add(new ValidationResult("EmailNotification.DestinatarioNotValid"));

            if (template == null)
            {
                validationErrors.Add(new ValidationResult(L("EmailNotification.TemplateDeEmailError")));
            }
            else
            {
                if (string.IsNullOrEmpty(template.Remetente))
                    validationErrors.Add(new ValidationResult("TemplateDeEmail.RemetenteRequiredError"));

                if (string.IsNullOrEmpty(template.Assunto))
                    validationErrors.Add(new ValidationResult("TemplateDeEmail.AssuntoRequiredError"));

                if (string.IsNullOrEmpty(template.CorpoMensagem))
                    validationErrors.Add(new ValidationResult("TemplateDeEmail.CorpoDaMensagemRequiredError"));
            }

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }
    }
}
