﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Abp;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Castle.Core.Logging;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EZ.EzNotification;
using EZ.EZControl.EZ.EzNotification.Enums;
using EZ.EZControl.EZ.EzNotification.Interfaces;
using Newtonsoft.Json;

namespace EZ.EZControl.Infra.EzNotification.Services
{
    public class SmsNotificationService : EZControlServiceBase, ISmsNotificationService
    {
        public async Task Send(SmsNotification sms)
        {
            ValidateSmsNotification(sms);
            await SendBySmsEmpresa(sms);
        }

        public async Task SendAllByItem(List<SmsNotification> smss)
        {
            foreach (var sms in smss)
            {
                ValidateSmsNotification(sms);
                await SendBySmsEmpresa(sms);
            }
        }

        public async Task SendAll(SmsNotification sms, List<string> telefones)
        {
            ValidateSmsNotification(sms);
            await SendAllBySmsEmpresa(sms, telefones);
        }

        private async Task SendBySmsEmpresa(SmsNotification sms)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var data = sms.DataEHora.ToString("dd/MM/yyyy");
                    var hora = sms.DataEHora.ToString("HH:mm");

                    client.BaseAddress = new Uri("https://api.smsempresa.com.br");
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("key", sms.Chave),
                        //new KeyValuePair<string, string>("type", ((int) sms.TipoSms).ToString()),
                        new KeyValuePair<string, string>("type", ((int)TipoSmsEnum.Longo).ToString()), // Por padrão longo
                        new KeyValuePair<string, string>("number", sms.Numero),
                        new KeyValuePair<string, string>("msg", sms.Mensagem),
                        new KeyValuePair<string, string>("jobdate", data),
                        new KeyValuePair<string, string>("jobtime", hora),
                        new KeyValuePair<string, string>("refer", sms.Referencia)

                    });
                    var result = await client.PostAsync("/send", content);
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Concat("Erro ao enviar SMS pela Empresa SMSEmpresa: ", ex.Message));
            }
        }

        private async Task SendAllBySmsEmpresa(SmsNotification sms, List<string> telefones)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var telefonesGeral = new StringBuilder();
                    telefones.ForEach(x =>
                    {
                        telefonesGeral.Append(x);
                        telefonesGeral.Append(";");
                    });
                    telefonesGeral.Append(sms.Numero);

                    var data = sms.DataEHora.ToString("dd/MM/yyyy");
                    var hora = sms.DataEHora.ToString("HH:mm");

                    client.BaseAddress = new Uri("https://api.smsempresa.com.br");
                    var content = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("key", sms.Chave),
                        //new KeyValuePair<string, string>("type", ((int) sms.TipoSms).ToString()),
                        new KeyValuePair<string, string>("type", ((int)TipoSmsEnum.Longo).ToString()), // Por padrão longo
                        new KeyValuePair<string, string>("number", telefonesGeral.ToString()),
                        new KeyValuePair<string, string>("msg", sms.Mensagem),
                        new KeyValuePair<string, string>("jobdate", data),
                        new KeyValuePair<string, string>("jobtime", hora),
                        new KeyValuePair<string, string>("refer", sms.Referencia)

                    });
                    var result = await client.PostAsync("/multiple", content);
                    string resultContent = await result.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(string.Concat("Erro ao enviar SMS pela Empresa SMSEmpresa: ", ex.Message));
            }
        }

        private void ValidateSmsNotification(SmsNotification sms)
        {
            var validationErrors = new List<ValidationResult>();

            if (sms == null)
            {
                validationErrors.Add(new ValidationResult(L("SmsNotificationEmpty")));
            }
            else
            {
                if (string.IsNullOrEmpty(sms.Chave))
                    validationErrors.Add(new ValidationResult("SmsNotification.ChaveEmpty"));

                if (string.IsNullOrEmpty(sms.Mensagem))
                    validationErrors.Add(new ValidationResult("SmsNotification.MensagemEmpty"));

                if (string.IsNullOrEmpty(sms.Numero))
                    validationErrors.Add(new ValidationResult("SmsNotification.NumeroEmpty"));

                if (!string.IsNullOrEmpty(sms.Numero) && !Telefone.IsValidTelefoneCelularBrSemMascara(sms.Numero))
                    validationErrors.Add(new ValidationResult("Telefone.ErrorTelefoneCelularBrSemMascaraNotValid"));

                if (!Enum.IsDefined(typeof(TipoSmsEnum), sms.TipoSms))
                    validationErrors.Add(new ValidationResult("SmsNotification.TipoSmsEmpty"));
            }

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }
    }
}
