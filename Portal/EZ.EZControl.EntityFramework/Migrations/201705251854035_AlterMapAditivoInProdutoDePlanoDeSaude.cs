namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterMapAditivoInProdutoDePlanoDeSaude : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "AditivoId", "dbo.Sau_Aditivo");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", new[] { "AditivoId" });
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Aditivo",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        AditivoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.AditivoId });
            
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "AditivoId");
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "ProdutoDePlanoDeSaudeId");
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "AditivoId", "dbo.Sau_Aditivo", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id", cascadeDelete: true);
        }
    }
}
