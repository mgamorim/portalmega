namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedDeclaracaoDoBeneficiario : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.Sau_DeclaracaoDoBeneficiario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_DeclaracaoDoBeneficiario_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Sau_DeclaracaoDoBeneficiario", "EmpresaId", c => c.Int());
            Sql("Update dbo.Sau_DeclaracaoDoBeneficiario set EmpresaId = 1");
            AlterColumn("dbo.Sau_DeclaracaoDoBeneficiario", "EmpresaId", c => c.Int(nullable: false));
            CreateIndex("dbo.Sau_DeclaracaoDoBeneficiario", "EmpresaId");
            AddForeignKey("dbo.Sau_DeclaracaoDoBeneficiario", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_DeclaracaoDoBeneficiario", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_DeclaracaoDoBeneficiario", new[] { "EmpresaId" });
            DropColumn("dbo.Sau_DeclaracaoDoBeneficiario", "EmpresaId");
            AlterTableAnnotations(
                "dbo.Sau_DeclaracaoDoBeneficiario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_DeclaracaoDoBeneficiario_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
