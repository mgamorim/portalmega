namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTaxaMensalInChancela : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_Chancela", "TaxaMensal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_Chancela", "TaxaMensal");
        }
    }
}
