namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ModeloInicial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_Administradora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        PessoaJuridicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ImagemId = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Administradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Arquivo", t => t.ImagemId)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId)
                .Index(t => t.PessoaJuridicaId)
                .Index(t => t.ImagemId);
            
            CreateTable(
                "dbo.Cor_Arquivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Conteudo = c.Binary(),
                        Path = c.String(nullable: false),
                        TipoDeArquivoFixo = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Token = c.String(nullable: false, maxLength: 32),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_Arquivo_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Empresa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        PessoaJuridicaId = c.Int(),
                        RazaoSocial = c.String(nullable: false, maxLength: 100),
                        NomeFantasia = c.String(nullable: false, maxLength: 100),
                        Cnpj = c.String(),
                        ClienteEZId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Empresa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_ClienteEZ", t => t.ClienteEZId)
                .Index(t => t.ClienteEZId);
            
            CreateTable(
                "dbo.Glb_ClienteEZ",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false),
                        RazaoSocial = c.String(),
                        DocumentoPrincipal = c.String(),
                        PessoaId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClienteEZ_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Glb_Pessoa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EnderecoPrincipalId = c.Int(),
                        EnderecoCobrancaId = c.Int(),
                        EnderecoEntregaId = c.Int(),
                        DocumentoPrincipalId = c.Int(),
                        GrupoPessoaId = c.Int(),
                        Observacao = c.String(maxLength: 1000),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pessoa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Documento", t => t.DocumentoPrincipalId)
                .ForeignKey("dbo.Glb_Endereco", t => t.EnderecoCobrancaId)
                .ForeignKey("dbo.Glb_Endereco", t => t.EnderecoEntregaId)
                .ForeignKey("dbo.Glb_Endereco", t => t.EnderecoPrincipalId)
                .ForeignKey("dbo.Glb_GrupoPessoa", t => t.GrupoPessoaId)
                .Index(t => t.EnderecoPrincipalId)
                .Index(t => t.EnderecoCobrancaId)
                .Index(t => t.EnderecoEntregaId)
                .Index(t => t.DocumentoPrincipalId)
                .Index(t => t.GrupoPessoaId);
            
            CreateTable(
                "dbo.Glb_Contato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                        TipoDeContatoId = c.Int(nullable: false),
                        TratamentoId = c.Int(),
                        Nome = c.String(nullable: false, maxLength: 50),
                        DataNascimento = c.DateTime(),
                        Sexo = c.Int(nullable: false),
                        Cargo = c.String(maxLength: 20),
                        Setor = c.String(maxLength: 20),
                        Idioma = c.String(maxLength: 20),
                        Observacao = c.String(maxLength: 1000),
                        EnderecoPrincipalId = c.Int(),
                        EnderecoDaPessoaUtilizado = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Contato_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Contato_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Endereco", t => t.EnderecoPrincipalId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .ForeignKey("dbo.Glb_TipoDeContato", t => t.TipoDeContatoId)
                .ForeignKey("dbo.Glb_Tratamento", t => t.TratamentoId)
                .Index(t => t.PessoaId)
                .Index(t => t.TipoDeContatoId)
                .Index(t => t.TratamentoId)
                .Index(t => t.EnderecoPrincipalId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Endereco",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                        ContatoId = c.Int(),
                        TipoEndereco = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 60),
                        TipoDeLogradouroId = c.Int(nullable: false),
                        Logradouro = c.String(nullable: false, maxLength: 80),
                        Numero = c.String(nullable: false, maxLength: 20),
                        Complemento = c.String(maxLength: 40),
                        Bairro = c.String(maxLength: 40),
                        CEP = c.String(maxLength: 8),
                        CidadeId = c.Int(nullable: false),
                        Referencia = c.String(maxLength: 50),
                        EnderecoParaEntrega = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Endereco_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Endereco_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Cidade", t => t.CidadeId)
                .ForeignKey("dbo.Glb_Contato", t => t.ContatoId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .ForeignKey("dbo.Glb_TipoDeLogradouro", t => t.TipoDeLogradouroId)
                .Index(t => t.PessoaId)
                .Index(t => t.ContatoId)
                .Index(t => t.TipoDeLogradouroId)
                .Index(t => t.CidadeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Cidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        CodigoIBGE = c.String(maxLength: 20),
                        EstadoId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Cidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Cidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Estado", t => t.EstadoId)
                .Index(t => t.EstadoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Estado",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Sigla = c.String(nullable: false, maxLength: 2),
                        Capital = c.String(nullable: false, maxLength: 60),
                        CodigoIBGE = c.String(maxLength: 3),
                        PaisId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Estado_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Estado_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Pais", t => t.PaisId)
                .Index(t => t.Nome, unique: true, name: "IX_Estado_Nome")
                .Index(t => t.Sigla, unique: true, name: "IX_Estado_Sigla")
                .Index(t => t.PaisId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Feriado",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 200),
                        Data = c.DateTime(nullable: false),
                        Abrangencia = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        PaisId = c.Int(),
                        EstadoId = c.Int(),
                        CidadeId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Feriado_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Feriado_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Cidade", t => t.CidadeId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Estado", t => t.EstadoId)
                .ForeignKey("dbo.Glb_Pais", t => t.PaisId)
                .Index(t => t.Nome, unique: true, name: "IX_Feriado_Nome")
                .Index(t => t.PaisId)
                .Index(t => t.EstadoId)
                .Index(t => t.CidadeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Pais",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Nacionalidade = c.String(nullable: false, maxLength: 40),
                        CodigoReceitaFederal = c.String(maxLength: 15),
                        CodigoInternacional = c.String(maxLength: 15),
                        CodigoISO = c.String(nullable: false, maxLength: 15),
                        MascaraDoCodigoPostal = c.String(maxLength: 15),
                        Sigla2Caracteres = c.String(maxLength: 2),
                        Sigla3Caracteres = c.String(maxLength: 3),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pais_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pais_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_Pais_Nome")
                .Index(t => t.Nacionalidade, unique: true, name: "IX_Pais_Nacionalidade")
                .Index(t => t.CodigoISO, unique: true, name: "IX_Pais_CodigoIso")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Telefone",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        DDI = c.String(maxLength: 2),
                        DDD = c.String(nullable: false, maxLength: 2),
                        Numero = c.String(nullable: false, maxLength: 9),
                        Ramal = c.String(maxLength: 10),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        Endereco_Id = c.Int(),
                        Contato_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Telefone_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Telefone_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .ForeignKey("dbo.Glb_Endereco", t => t.Endereco_Id)
                .ForeignKey("dbo.Glb_Contato", t => t.Contato_Id)
                .Index(t => t.PessoaId)
                .Index(t => t.EmpresaId)
                .Index(t => t.Endereco_Id)
                .Index(t => t.Contato_Id);
            
            CreateTable(
                "dbo.Glb_TipoDeLogradouro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeLogradouro_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeLogradouro_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_EnderecoEletronico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContatoId = c.Int(),
                        PessoaId = c.Int(nullable: false),
                        TipoDeEnderecoEletronico = c.Int(nullable: false),
                        Endereco = c.String(nullable: false, maxLength: 40),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnderecoEletronico_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EnderecoEletronico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Contato", t => t.ContatoId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .Index(t => t.ContatoId)
                .Index(t => t.PessoaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_TipoDeContato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 60),
                        Mascara = c.String(maxLength: 100),
                        TipoDeContatoFixo = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeContato_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeContato_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Tratamento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 30),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Tratamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Tratamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Documento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PessoaId = c.Int(nullable: false),
                        TipoDeDocumentoId = c.Int(nullable: false),
                        Numero = c.String(nullable: false, maxLength: 20),
                        OrgaoExpedidor = c.String(maxLength: 20),
                        DataExpedicao = c.DateTime(),
                        EstadoId = c.Int(),
                        DataDeValidade = c.DateTime(),
                        Serie = c.String(maxLength: 20),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Documento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Documento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Estado", t => t.EstadoId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .ForeignKey("dbo.Glb_TipoDeDocumento", t => t.TipoDeDocumentoId)
                .Index(t => t.PessoaId)
                .Index(t => t.TipoDeDocumentoId)
                .Index(t => t.EstadoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_TipoDeDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoPessoa = c.Int(nullable: false),
                        TipoDeDocumentoFixo = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 40),
                        Mascara = c.String(maxLength: 30),
                        Principal = c.Boolean(nullable: false),
                        ObrigaAnexo = c.Boolean(nullable: false),
                        PrazoDeAtualizacaoDoAnexo = c.Int(nullable: false),
                        Prioridade = c.Int(nullable: false),
                        ObrigaOrgaoExpedidor = c.Boolean(nullable: false),
                        ObrigaDataExpedicao = c.Boolean(nullable: false),
                        ObrigaUF = c.Boolean(nullable: false),
                        ObrigaDataDeValidade = c.Boolean(nullable: false),
                        SerieObrigatoria = c.Boolean(nullable: false),
                        ListaEmOutrosDocumentos = c.Boolean(nullable: false),
                        ExibirNoCadastroSimplificadoDePessoa = c.Boolean(nullable: false),
                        PermiteLancamentoDuplicado = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeDocumento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeDocumento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Descricao, unique: true, name: "IX_TipoDeDocumento_Descricao")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_GrupoPessoa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(),
                        TipoDePessoa = c.Int(nullable: false),
                        Slug = c.String(maxLength: 20),
                        Nome = c.String(nullable: false, maxLength: 60),
                        PaiId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GrupoPessoa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_GrupoPessoa", t => t.PaiId)
                .Index(t => t.PaiId);
            
            CreateTable(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        Aceite = c.Boolean(nullable: false),
                        Homologada = c.Boolean(nullable: false),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        CorretorId = c.Int(nullable: false),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        ChancelaId = c.Int(),
                        TenantId = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        TitularId = c.Int(nullable: false),
                        UltimoContratoAceitoId = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PropostaDeContratacao_CorretorFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PropostaDeContratacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PropostaDeContratacao_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Chancela", t => t.ChancelaId)
                .ForeignKey("dbo.Sau_Contrato", t => t.ContratoVigenteNoAceiteId)
                .ForeignKey("dbo.Sau_Corretor", t => t.CorretorId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Vnd_Pedido", t => t.PedidoId)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoId)
                .ForeignKey("dbo.Sau_Responsavel", t => t.ResponsavelId)
                .ForeignKey("dbo.Sau_ProponenteTitular", t => t.TitularId)
                .ForeignKey("dbo.Sau_Contrato", t => t.UltimoContratoAceitoId)
                .ForeignKey("dbo.Sau_Vigencia", t => t.VigenciaId)
                .Index(t => t.CorretorId)
                .Index(t => t.PedidoId)
                .Index(t => t.VigenciaId)
                .Index(t => t.ChancelaId)
                .Index(t => t.EmpresaId)
                .Index(t => t.ContratoVigenteNoAceiteId)
                .Index(t => t.ProdutoId)
                .Index(t => t.ResponsavelId)
                .Index(t => t.TitularId)
                .Index(t => t.UltimoContratoAceitoId);
            
            CreateTable(
                "dbo.Sau_Chancela",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        TaxaDeAdesao = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Chancela_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Chancela_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Profissao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Codigo = c.String(nullable: false, maxLength: 6),
                        Titulo = c.String(nullable: false, maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        Associacao_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Profissao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Associacao", t => t.Associacao_Id)
                .Index(t => t.Associacao_Id);
            
            CreateTable(
                "dbo.Sau_Contrato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false),
                        DataInicioVigencia = c.DateTime(nullable: false),
                        DataFimVigencia = c.DateTime(nullable: false),
                        Conteudo = c.String(unicode: false, storeType: "text"),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Contrato_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Contrato_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Contrato_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Etq_Produto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Descricao = c.String(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsValorAjustavel = c.Boolean(nullable: false),
                        TipoDeProduto = c.Int(nullable: false),
                        NaturezaId = c.Int(),
                        UnidadeMedidaId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ImagemId = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Produto_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Produto_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cor_Arquivo", t => t.ImagemId)
                .ForeignKey("dbo.Etq_Natureza", t => t.NaturezaId)
                .ForeignKey("dbo.Etq_UnidadeMedida", t => t.UnidadeMedidaId)
                .Index(t => t.NaturezaId)
                .Index(t => t.UnidadeMedidaId)
                .Index(t => t.EmpresaId)
                .Index(t => t.ImagemId);
            
            CreateTable(
                "dbo.Sau_Associacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        PessoaJuridicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Associacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId)
                .Index(t => t.PessoaJuridicaId);
            
            CreateTable(
                "dbo.Etq_Natureza",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Natureza_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Natureza_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_Operadora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        CodigoAns = c.String(nullable: false, maxLength: 100),
                        PessoaJuridicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ImagemId = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Operadora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Arquivo", t => t.ImagemId)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId)
                .Index(t => t.PessoaJuridicaId)
                .Index(t => t.ImagemId);
            
            CreateTable(
                "dbo.Sau_PlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        NumeroDeRegistro = c.String(nullable: false),
                        Reembolso = c.Boolean(nullable: false),
                        DescricaoDoReembolso = c.String(),
                        Abrangencia = c.Int(nullable: false),
                        SegmentacaoAssistencial = c.Int(nullable: false),
                        PlanoDeSaudeAns = c.Int(),
                        Acomodacao = c.Int(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_QuestionarioDeDeclaracaoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 250),
                        OperadoraId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_QuestionarioDeDeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_QuestionarioDeDeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_Operadora", t => t.OperadoraId)
                .Index(t => t.OperadoraId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_ItemDeDeclaracaoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Pergunta = c.String(nullable: false, maxLength: 500),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeDeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeDeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_RedeCredenciada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 150),
                        Descricao = c.String(unicode: false, storeType: "text"),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RedeCredenciada_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RedeCredenciada_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Etq_UnidadeMedida",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnidadePadrao = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnidadeMedida_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UnidadeMedida_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_Vigencia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        DataInicialDaProposta = c.Int(nullable: false),
                        DataFinalDaProposta = c.Int(nullable: false),
                        DataDeFechamento = c.Int(nullable: false),
                        DataDeVigencia = c.Int(nullable: false),
                        NumeroDeBoletos = c.String(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Vigencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Vigencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_Corretor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Corretor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.Sau_Corretora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CorretorId = c.Int(),
                        Slug = c.String(maxLength: 20),
                        EmpresaId = c.Int(nullable: false),
                        PessoaJuridicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Corretora_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Corretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Corretor", t => t.CorretorId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId)
                .Index(t => t.CorretorId)
                .Index(t => t.EmpresaId)
                .Index(t => t.PessoaJuridicaId);
            
            CreateTable(
                "dbo.Vnd_Pedido",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pedido_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pedido_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pedido_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Cliente", t => t.ClienteId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.ClienteId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Cliente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Cliente_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Cliente_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .Index(t => t.EmpresaId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.Vnd_ItemDePedido",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValorCadastro = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantidade = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TipoDeItemDePedido = c.Int(nullable: false),
                        PedidoId = c.Int(nullable: false),
                        ProdutoId = c.Int(),
                        ServicoId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDePedido_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vnd_Pedido", t => t.PedidoId)
                .ForeignKey("dbo.Etq_Produto", t => t.ProdutoId)
                .ForeignKey("dbo.Srv_Servico", t => t.ServicoId, cascadeDelete: true)
                .Index(t => t.PedidoId)
                .Index(t => t.ProdutoId)
                .Index(t => t.ServicoId);
            
            CreateTable(
                "dbo.Srv_Servico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsValorAjustavel = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Servico_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Servico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_Responsavel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoDeResponsavel = c.Int(nullable: false),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Responsavel_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisicaId)
                .Index(t => t.PessoaFisicaId);
            
            CreateTable(
                "dbo.Sau_BeneficiarioBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoDeBeneficiario = c.Int(nullable: false),
                        NomeDaMae = c.String(),
                        NumeroDoCartaoNacionalDeSaude = c.String(),
                        DeclaracaoDeNascidoVivo = c.String(),
                        CorretoraId = c.Int(nullable: false),
                        ProfissaoId = c.Int(),
                        EstadoId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BeneficiarioBase_CorretoraFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_BeneficiarioBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_BeneficiarioBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Estado", t => t.EstadoId)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisicaId)
                .ForeignKey("dbo.Glb_Profissao", t => t.ProfissaoId)
                .Index(t => t.CorretoraId)
                .Index(t => t.ProfissaoId)
                .Index(t => t.EstadoId)
                .Index(t => t.EmpresaId)
                .Index(t => t.PessoaFisicaId);
            
            CreateTable(
                "dbo.Glb_Agencia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        BancoId = c.Int(nullable: false),
                        Codigo = c.String(nullable: false, maxLength: 10),
                        DigitoVerificador = c.String(maxLength: 2),
                        Nome = c.String(nullable: false, maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Agencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Agencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Banco", t => t.BancoId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.BancoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Banco",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Codigo = c.String(maxLength: 3),
                        Nome = c.String(nullable: false, maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Banco_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Banco_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Codigo, unique: true, name: "IX_Banco_Codigo")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_Bloqueio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Data = c.DateTime(nullable: false),
                        Horario = c.Time(nullable: false, precision: 7),
                        ConfiguracaoDeBloqueioId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Bloqueio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Bloqueio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_ConfiguracaoDeBloqueio", t => t.ConfiguracaoDeBloqueioId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.ConfiguracaoDeBloqueioId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_ConfiguracaoDeBloqueio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 100),
                        Sistema = c.Int(nullable: false),
                        TipoDeEvento = c.Int(nullable: false),
                        MotivoDoBloqueio = c.Int(nullable: false),
                        InicioBloqueio = c.DateTime(nullable: false),
                        FimBloqueio = c.DateTime(nullable: false),
                        HorarioInicio = c.Time(nullable: false, precision: 7),
                        HorarioFim = c.Time(nullable: false, precision: 7),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeBloqueio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeBloqueio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Med_Especialidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false, maxLength: 10),
                        Nome = c.String(nullable: false, maxLength: 250),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Especialidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Especialidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_UnidadeDeSaudeBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 300),
                        Site = c.String(maxLength: 100),
                        PessoaJuridicaId = c.Int(nullable: false),
                        EnderecoId = c.Int(nullable: false),
                        Telefone1Id = c.Int(),
                        Telefone2Id = c.Int(),
                        TipoDeUnidade = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnidadeDeSaudeBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UnidadeDeSaudeBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Endereco", t => t.EnderecoId)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId)
                .ForeignKey("dbo.Glb_Telefone", t => t.Telefone1Id)
                .ForeignKey("dbo.Glb_Telefone", t => t.Telefone2Id)
                .Index(t => t.PessoaJuridicaId)
                .Index(t => t.EnderecoId)
                .Index(t => t.Telefone1Id)
                .Index(t => t.Telefone2Id)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Med_Medico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        PictureId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Medico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisicaId)
                .ForeignKey("dbo.Cor_Picture", t => t.PictureId)
                .Index(t => t.PessoaFisicaId)
                .Index(t => t.PictureId);
            
            CreateTable(
                "dbo.Cor_Picture",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Bytes = c.Binary(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Picture_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Picture_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_Categoria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 50),
                        Slug = c.String(nullable: false, maxLength: 70),
                        Descricao = c.String(maxLength: 200),
                        IsActive = c.Boolean(nullable: false),
                        SiteId = c.Int(nullable: false),
                        CategoriaPaiId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Categoria_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Categoria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cms_Categoria", t => t.CategoriaPaiId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cms_Site", t => t.SiteId)
                .Index(t => t.Nome, unique: true, name: "IX_Categoria_Nome")
                .Index(t => t.SiteId)
                .Index(t => t.CategoriaPaiId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_PublicacaoBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipoDePublicacao = c.Int(nullable: false),
                        SiteId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 70),
                        Slug = c.String(nullable: false, maxLength: 70),
                        Conteudo = c.String(),
                        IdImagemDestaque = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        DataDePublicacao = c.DateTime(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PublicacaoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PublicacaoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Cms_Site", t => t.SiteId, cascadeDelete: true)
                .Index(t => t.SiteId)
                .Index(t => t.Titulo, unique: true, name: "IX_Publicacao_Titulo")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_HistoricoPublicacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PublicacaoId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoricoPublicacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoricoPublicacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Cms_PublicacaoBase", t => t.PublicacaoId)
                .Index(t => t.PublicacaoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_Site",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                        Descricao = c.String(maxLength: 200),
                        MetaKeywords = c.String(),
                        MetaTags = c.String(),
                        Owner = c.String(),
                        TipoDePublicacao = c.Int(nullable: false),
                        TipoDeConteudoRSS = c.Int(nullable: false),
                        LimitePostPorPagina = c.Int(nullable: false),
                        LimitePostPorRss = c.Int(nullable: false),
                        EvitarMecanismoDeBusca = c.Boolean(nullable: false),
                        TemplateDefaultId = c.Int(nullable: false),
                        PermitirComentarios = c.Boolean(nullable: false),
                        PermitirLinks = c.Boolean(nullable: false),
                        AutorInformaNomeEmail = c.Boolean(nullable: false),
                        NotificarPorEmailNovoComentario = c.Boolean(nullable: false),
                        AtivarModeracaoDeComentario = c.Boolean(nullable: false),
                        TextoCss = c.String(maxLength: 500),
                        TextoJavaScript = c.String(maxLength: 500),
                        HabilitarPesquisaPost = c.Boolean(nullable: false),
                        HabilitarPesquisaPagina = c.Boolean(nullable: false),
                        TipoDePaginaParaPaginaDefault = c.String(nullable: false),
                        TipoDePaginaParaPostDefault = c.String(nullable: false),
                        TipoDePaginaParaCategoriaDefault = c.String(nullable: false),
                        TipoDePaginaParaPaginaInicialDefault = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Site_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Site_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cms_Template", t => t.TemplateDefaultId)
                .Index(t => t.Nome, unique: true, name: "IX_Site_Nome")
                .Index(t => t.TemplateDefaultId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_Host",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Url = c.String(nullable: false, maxLength: 300),
                        IsPrincipal = c.Boolean(nullable: false),
                        SiteId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Host_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Host_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cms_Site", t => t.SiteId)
                .Index(t => t.SiteId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_Template",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                        Descricao = c.String(maxLength: 100),
                        Autor = c.String(),
                        AutorUrl = c.String(),
                        Versao = c.String(nullable: false),
                        NomeDoArquivo = c.String(),
                        Posicoes = c.String(),
                        TiposDeWidgetSuportados = c.String(),
                        TiposDePaginasSuportadas = c.String(nullable: false),
                        TipoDePaginaParaPaginaDefault = c.String(nullable: false),
                        TipoDePaginaParaPostDefault = c.String(nullable: false),
                        TipoDePaginaParaCategoriaDefault = c.String(nullable: false),
                        TipoDePaginaParaPaginaInicialDefault = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Template_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Template_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_Template_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_Tag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                        Slug = c.String(),
                        Descricao = c.String(maxLength: 200),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Tag_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Tag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_Tag_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_ClienteEZLiv",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        ClienteId = c.Int(nullable: false),
                        Observacao = c.String(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClienteEZLiv_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClienteEZLiv_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Cliente", t => t.ClienteId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .Index(t => t.ClienteId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_ConexaoSMTP",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        EnderecoDeEmailPadrao = c.String(),
                        ServidorSMTP = c.String(nullable: false, maxLength: 200),
                        Porta = c.Int(nullable: false),
                        UsuarioAutenticacao = c.String(maxLength: 300),
                        SenhaAutenticacao = c.String(maxLength: 50),
                        RequerAutenticacao = c.Boolean(nullable: false),
                        RequerConexaoCriptografada = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConexaoSMTP_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConexaoSMTP_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_ConexaoSMTP_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_ConfiguracaoDeDisponibilidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Titulo = c.String(nullable: false, maxLength: 100),
                        Sistema = c.Int(nullable: false),
                        TipoDeEvento = c.Int(nullable: false),
                        TipoDeDisponibilidade = c.Int(nullable: false),
                        DiasDaSemana = c.String(),
                        NumeroDeDias = c.Int(nullable: false),
                        DataEspecifica = c.DateTime(),
                        HorarioInicio = c.Time(nullable: false, precision: 7),
                        HorarioFim = c.Time(nullable: false, precision: 7),
                        DataInicioValidade = c.DateTime(),
                        DataFimValidade = c.DateTime(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeDisponibilidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeDisponibilidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_Disponibilidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Data = c.DateTime(nullable: false),
                        Horario = c.Time(nullable: false, precision: 7),
                        ConfiguracaoDeDisponibilidadeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Disponibilidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Disponibilidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_ConfiguracaoDeDisponibilidade", t => t.ConfiguracaoDeDisponibilidadeId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.ConfiguracaoDeDisponibilidadeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_ContratoHistorico",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DataCriacao = c.DateTime(nullable: false),
                        DataInicioVigencia = c.DateTime(nullable: false),
                        DataFimVigencia = c.DateTime(nullable: false),
                        Conteudo = c.String(unicode: false, storeType: "text"),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        ContratoId = c.Int(nullable: false),
                        UserId = c.Long(nullable: false),
                        DataEvento = c.DateTime(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ContratoHistorico_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ContratoHistorico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Contrato", t => t.ContratoId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ContratoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_DeclaracaoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        TitularId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_ProponenteTitular", t => t.TitularId)
                .Index(t => t.TitularId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Departamento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 40),
                        Descricao = c.String(maxLength: 100),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Departamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Departamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_Departamento_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Fin_DocumentoBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(nullable: false),
                        Complemento = c.String(),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Data = c.DateTime(nullable: false),
                        Competencia = c.DateTime(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 50),
                        Observacoes = c.String(maxLength: 300),
                        TipoDeDocumentoFinanceiroId = c.Int(nullable: false),
                        Estornado = c.Boolean(nullable: false),
                        DataEstorno = c.DateTime(),
                        MotivoEstorno = c.String(),
                        Vencimento = c.DateTime(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Fin_TipoDeDocumentoFinanceiro", t => t.TipoDeDocumentoFinanceiroId)
                .Index(t => t.TipoDeDocumentoFinanceiroId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Fornecedor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Fornecedor_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Fornecedor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId)
                .Index(t => t.EmpresaId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.Glb_RamoDoFornecedor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RamoDoFornecedor_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RamoDoFornecedor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_RamoDoFornecedor_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Fin_TipoDeDocumentoFinanceiro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Tipo = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeDocumentoFinanceiro_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeDocumentoFinanceiro_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Etq_EntradaItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        LocalArmazenamentoId = c.Int(),
                        ProdutoId = c.Int(nullable: false),
                        Quantidade = c.Int(nullable: false),
                        Validade = c.DateTime(nullable: false),
                        ValorUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        EntradaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EntradaItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Etq_Entrada", t => t.EntradaId)
                .ForeignKey("dbo.Etq_LocalArmazenamento", t => t.LocalArmazenamentoId)
                .ForeignKey("dbo.Etq_Produto", t => t.ProdutoId)
                .Index(t => t.LocalArmazenamentoId)
                .Index(t => t.ProdutoId)
                .Index(t => t.EntradaId);
            
            CreateTable(
                "dbo.Etq_Entrada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        DataPedido = c.DateTime(),
                        DataEntrada = c.DateTime(nullable: false),
                        FornecedorId = c.Int(),
                        NotaFiscal = c.String(maxLength: 150),
                        ValorFrete = c.Decimal(precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        LocalArmazenamento_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Entrada_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_Fornecedor", t => t.FornecedorId)
                .ForeignKey("dbo.Etq_LocalArmazenamento", t => t.LocalArmazenamento_Id)
                .Index(t => t.EmpresaId)
                .Index(t => t.FornecedorId)
                .Index(t => t.LocalArmazenamento_Id);
            
            CreateTable(
                "dbo.Etq_LocalArmazenamento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocalArmazenamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocalArmazenamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EspecialidadePorProdutoDePlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EspecialidadePorProdutoDePlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EspecialidadeId)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_EspecialidadeSync",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Especialidade = c.String(maxLength: 100),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Logradouro = c.String(maxLength: 100),
                        Numero = c.String(maxLength: 100),
                        Complemento = c.String(maxLength: 100),
                        Bairro = c.String(maxLength: 100),
                        Municipio = c.String(maxLength: 100),
                        Estado = c.String(maxLength: 100),
                        NomeEstado = c.String(maxLength: 100),
                        Telefone1 = c.String(maxLength: 100),
                        Ramal1 = c.String(maxLength: 100),
                        Telefone2 = c.String(maxLength: 100),
                        Ramal2 = c.String(maxLength: 100),
                        MunicipioPai = c.String(maxLength: 100),
                        Email = c.String(maxLength: 100),
                        HomePage = c.String(maxLength: 100),
                        PlanoDeSaudeAns = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EspecialidadeSync_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EspecialidadeSync_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_EventoBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descricao = c.String(maxLength: 1024),
                        Termino = c.DateTime(nullable: false),
                        TerminoEndZone = c.String(),
                        DiaInteiro = c.Boolean(nullable: false),
                        Inicio = c.DateTime(nullable: false),
                        InicioTimeZone = c.String(),
                        EventoPaiId = c.Int(),
                        RecurrenceRule = c.String(maxLength: 1024),
                        RecurrenceException = c.String(),
                        Titulo = c.String(nullable: false, maxLength: 255),
                        TipoDeEvento = c.Int(nullable: false),
                        Sistema = c.Int(nullable: false),
                        StatusDoEvento = c.Int(nullable: false),
                        OwnerId = c.Int(nullable: false),
                        DisponibilidadeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_Disponibilidade", t => t.DisponibilidadeId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Agd_EventoBase", t => t.EventoPaiId)
                .ForeignKey("dbo.Glb_Pessoa", t => t.OwnerId)
                .Index(t => t.EventoPaiId)
                .Index(t => t.OwnerId)
                .Index(t => t.DisponibilidadeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_HistoricoDoEvento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventoId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false),
                        Observacao = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoricoDoEvento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoricoDoEvento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Agd_EventoBase", t => t.EventoId, cascadeDelete: true)
                .Index(t => t.EventoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_FaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false),
                        IdadeInicial = c.Int(nullable: false),
                        IdadeFinal = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FaixaEtaria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pag_FormaDePagamento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 60),
                        TipoDePagamento = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FormaDePagamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FormaDePagamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Etq_Fracao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnidadeMedidaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Fracao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Fracao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Etq_UnidadeMedida", t => t.UnidadeMedidaId)
                .Index(t => t.UnidadeMedidaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Pag_HistoricoTransacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TransacaoId = c.Int(nullable: false),
                        Data = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoricoTransacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoricoTransacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Pag_Transacao", t => t.TransacaoId)
                .Index(t => t.TransacaoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Pag_Transacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PedidoId = c.Int(nullable: false),
                        TransactionId = c.String(),
                        Referencia = c.String(),
                        Data = c.DateTime(nullable: false),
                        StatusDoPagamento = c.Int(nullable: false),
                        LinkParaPagamento = c.String(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Transacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Transacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Vnd_Pedido", t => t.PedidoId)
                .Index(t => t.PedidoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_ItemDeMenu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        MenuId = c.Int(nullable: false),
                        ItemMenuId = c.Int(),
                        CategoriaId = c.Int(),
                        PaginaId = c.Int(),
                        Titulo = c.String(nullable: false, maxLength: 50),
                        DescricaoDoTitulo = c.String(nullable: false, maxLength: 100),
                        TipoDeItemDeMenu = c.Int(nullable: false),
                        Url = c.String(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeMenu_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeMenu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cms_Categoria", t => t.CategoriaId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cms_ItemDeMenu", t => t.ItemMenuId)
                .ForeignKey("dbo.Cms_Menu", t => t.MenuId)
                .ForeignKey("dbo.Cms_Pagina", t => t.PaginaId)
                .Index(t => t.MenuId)
                .Index(t => t.ItemMenuId)
                .Index(t => t.CategoriaId)
                .Index(t => t.PaginaId)
                .Index(t => t.Titulo, unique: true, name: "IX_Menu_Titulo")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_Menu",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 50),
                        SiteId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Menu_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Menu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cms_Site", t => t.SiteId)
                .Index(t => t.Nome, unique: true, name: "IX_Menu_Nome")
                .Index(t => t.SiteId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_ItemDeRedeCredenciada",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        IsRedeDiferenciada = c.Boolean(nullable: false),
                        RedeCredenciadaId = c.Int(nullable: false),
                        UnidadeDeSaudeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeRedeCredenciada_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeRedeCredenciada_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_RedeCredenciada", t => t.RedeCredenciadaId)
                .ForeignKey("dbo.Sau_UnidadeDeSaudeBase", t => t.UnidadeDeSaudeId)
                .Index(t => t.RedeCredenciadaId)
                .Index(t => t.UnidadeDeSaudeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_ItemHierarquia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Mascara = c.String(maxLength: 20),
                        Codigo = c.String(),
                        ItemHierarquiaPaiId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemHierarquia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemHierarquia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Glb_ItemHierarquia", t => t.ItemHierarquiaPaiId)
                .Index(t => t.Nome, unique: true, name: "IX_ItemHierarquia_Nome")
                .Index(t => t.ItemHierarquiaPaiId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cor_Parametro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TamanhoMaximoMb = c.Int(nullable: false),
                        AlturaMaximaPx = c.Int(nullable: false),
                        LarguraMaximaPx = c.Int(nullable: false),
                        ExtensoesDocumento = c.String(),
                        ExtensoesImagem = c.String(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Pag_ParametroPagSeguro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PessoaJuridicaId = c.Int(nullable: false),
                        Email = c.String(),
                        Token = c.String(),
                        AppId = c.String(),
                        AppKey = c.String(),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroPagSeguro_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroPagSeguro_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId)
                .Index(t => t.PessoaJuridicaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_PermissaoEmpresa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmpresaId = c.Int(nullable: false),
                        IsGranted = c.Boolean(nullable: false),
                        PermissionName = c.String(nullable: false),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        RoleId = c.Int(),
                        UserId = c.Long(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PermissaoEmpresa_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PermissaoEmpresaPorRole_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PermissaoEmpresaPorUsuario_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Agd_RegraBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Sistema = c.Int(nullable: false),
                        TipoDeEvento = c.Int(nullable: false),
                        DiasExibidosDesdeHojeAteAgendamento = c.Int(nullable: false),
                        VagasDisponibilizadasPorDia = c.Int(nullable: false),
                        VagasReservadasPorDia = c.Int(nullable: false),
                        UnidadeDeTempo = c.Int(nullable: false),
                        VagasPorUnidadeDeTempo = c.Int(nullable: false),
                        DiasAntesDoAgendamentoProibidoAlterar = c.Int(nullable: false),
                        DiasParaNovoAgendamentoPorParticipante = c.Int(nullable: false),
                        OverBook = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Marcada = c.String(nullable: false, maxLength: 1),
                        Observacao = c.String(maxLength: 500),
                        ItemDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        BeneficiarioId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RespostaDoItemDeDeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RespostaDoItemDeDeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_BeneficiarioBase", t => t.BeneficiarioId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_ItemDeDeclaracaoDeSaude", t => t.ItemDeDeclaracaoDeSaudeId)
                .Index(t => t.ItemDeDeclaracaoDeSaudeId)
                .Index(t => t.BeneficiarioId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Etq_SaidaItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        SaidaId = c.Int(nullable: false),
                        LocalArmazenamentoId = c.Int(),
                        ProdutoId = c.Int(nullable: false),
                        Quantidade = c.Int(nullable: false),
                        ValorUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SaidaItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Etq_LocalArmazenamento", t => t.LocalArmazenamentoId)
                .ForeignKey("dbo.Etq_Produto", t => t.ProdutoId)
                .ForeignKey("dbo.Etq_Saida", t => t.SaidaId)
                .Index(t => t.SaidaId)
                .Index(t => t.LocalArmazenamentoId)
                .Index(t => t.ProdutoId);
            
            CreateTable(
                "dbo.Etq_Saida",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        PedidoId = c.Int(),
                        DataSaida = c.DateTime(nullable: false),
                        MotivoSaida = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Saida_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Vnd_Pedido", t => t.PedidoId)
                .Index(t => t.EmpresaId)
                .Index(t => t.PedidoId);
            
            CreateTable(
                "dbo.Etq_SaldoEstoque",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        DataAtualizacao = c.DateTime(nullable: false),
                        OrigemMovSaldo = c.Int(nullable: false),
                        LocalArmazenamentoId = c.Int(),
                        ProdutoId = c.Int(nullable: false),
                        Quantidade = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Consolidado = c.Boolean(),
                        EntradaId = c.Int(),
                        SaidaId = c.Int(),
                        PedidoId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SaldoEstoque_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Etq_Entrada", t => t.EntradaId)
                .ForeignKey("dbo.Etq_LocalArmazenamento", t => t.LocalArmazenamentoId)
                .ForeignKey("dbo.Vnd_Pedido", t => t.PedidoId)
                .ForeignKey("dbo.Etq_Produto", t => t.ProdutoId)
                .ForeignKey("dbo.Etq_Saida", t => t.SaidaId)
                .Index(t => t.EmpresaId)
                .Index(t => t.LocalArmazenamentoId)
                .Index(t => t.ProdutoId)
                .Index(t => t.EntradaId)
                .Index(t => t.SaidaId)
                .Index(t => t.PedidoId);
            
            CreateTable(
                "dbo.Glb_TemplateEmail",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Remetente = c.String(nullable: false, maxLength: 300),
                        Assunto = c.String(nullable: false, maxLength: 100),
                        CorpoMensagem = c.String(nullable: false, maxLength: 4000),
                        TipoDeTemplateDeMensagem = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TemplateEmail_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TemplateEmail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.Nome, unique: true, name: "IX_TemplateEmail_Nome")
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_UsuarioPorPessoa",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        UserId = c.Long(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UsuarioPorPessoa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AbpUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Sau_ValorPorFaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        FaixaEtariaId = c.Int(nullable: false),
                        ContratoId = c.Int(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ValorPorFaixaEtaria_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ValorPorFaixaEtaria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Contrato", t => t.ContratoId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_FaixaEtaria", t => t.FaixaEtariaId)
                .Index(t => t.FaixaEtariaId)
                .Index(t => t.ContratoId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Cms_WidgetBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false, maxLength: 50),
                        ExibirTitulo = c.Boolean(nullable: false),
                        Sistema = c.Int(nullable: false),
                        SiteId = c.Int(nullable: false),
                        TemplateId = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        Posicao = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WidgetBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WidgetBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Cms_Site", t => t.SiteId, cascadeDelete: true)
                .ForeignKey("dbo.Cms_Template", t => t.TemplateId, cascadeDelete: true)
                .Index(t => t.Titulo, unique: true, name: "IX_Widget_Titulo")
                .Index(t => t.SiteId)
                .Index(t => t.TemplateId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Glb_Empresa_Usuario",
                c => new
                    {
                        EmpresaId = c.Int(nullable: false),
                        UsuarioId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.EmpresaId, t.UsuarioId })
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.AbpUsers", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.EmpresaId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Sau_Chancela_Profissao",
                c => new
                    {
                        ChancelaId = c.Int(nullable: false),
                        ProfissaoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ChancelaId, t.ProfissaoId })
                .ForeignKey("dbo.Sau_Chancela", t => t.ChancelaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Profissao", t => t.ProfissaoId, cascadeDelete: true)
                .Index(t => t.ChancelaId)
                .Index(t => t.ProfissaoId);
            
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Chancela",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        ChancelaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.ChancelaId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Chancela", t => t.ChancelaId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ChancelaId);
            
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Cidade",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        CidadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.CidadeId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Cidade", t => t.CidadeId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.CidadeId);
            
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Estado",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        EstadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.EstadoId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Estado", t => t.EstadoId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EstadoId);
            
            CreateTable(
                "dbo.Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude",
                c => new
                    {
                        QuestionarioDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        ItemDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuestionarioDeDeclaracaoDeSaudeId, t.ItemDeDeclaracaoDeSaudeId })
                .ForeignKey("dbo.Sau_QuestionarioDeDeclaracaoDeSaude", t => t.QuestionarioDeDeclaracaoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ItemDeDeclaracaoDeSaude", t => t.ItemDeDeclaracaoDeSaudeId, cascadeDelete: true)
                .Index(t => t.QuestionarioDeDeclaracaoDeSaudeId)
                .Index(t => t.ItemDeDeclaracaoDeSaudeId);
            
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Vigencia",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        VigenciaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.VigenciaId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Vigencia", t => t.VigenciaId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.VigenciaId);
            
            CreateTable(
                "dbo.Sau_Corretor_Corretora",
                c => new
                    {
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CorretorId, t.CorretoraId })
                .ForeignKey("dbo.Sau_Corretor", t => t.CorretorId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId, cascadeDelete: true)
                .Index(t => t.CorretorId)
                .Index(t => t.CorretoraId);
            
            CreateTable(
                "dbo.Sau_ProponeteTitular_Dependente",
                c => new
                    {
                        ProponeteTitularId = c.Int(nullable: false),
                        DependenteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProponeteTitularId, t.DependenteId })
                .ForeignKey("dbo.Sau_ProponenteTitular", t => t.ProponeteTitularId)
                .ForeignKey("dbo.Sau_Dependente", t => t.DependenteId)
                .Index(t => t.ProponeteTitularId)
                .Index(t => t.DependenteId);
            
            CreateTable(
                "dbo.Sau_Hospital_Especialidade",
                c => new
                    {
                        HospitalId = c.Int(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HospitalId, t.EspecialidadeId })
                .ForeignKey("dbo.Sau_Hospital", t => t.HospitalId, cascadeDelete: true)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId, cascadeDelete: true)
                .Index(t => t.HospitalId)
                .Index(t => t.EspecialidadeId);
            
            CreateTable(
                "dbo.Sau_Laboratorio_Especialidade",
                c => new
                    {
                        LaboratorioId = c.Int(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LaboratorioId, t.EspecialidadeId })
                .ForeignKey("dbo.Sau_Laboratorio", t => t.LaboratorioId, cascadeDelete: true)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId, cascadeDelete: true)
                .Index(t => t.LaboratorioId)
                .Index(t => t.EspecialidadeId);
            
            CreateTable(
                "dbo.Med_Especialidade_Medico",
                c => new
                    {
                        MedicoId = c.Int(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MedicoId, t.EspecialidadeId })
                .ForeignKey("dbo.Med_Medico", t => t.MedicoId, cascadeDelete: true)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId, cascadeDelete: true)
                .Index(t => t.MedicoId)
                .Index(t => t.EspecialidadeId);
            
            CreateTable(
                "dbo.Cms_Post_Categoria",
                c => new
                    {
                        PostId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostId, t.CategoriaId })
                .ForeignKey("dbo.Cms_Post", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.Cms_Categoria", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Cms_Post_Tag",
                c => new
                    {
                        PostId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostId, t.TagId })
                .ForeignKey("dbo.Cms_Post", t => t.PostId, cascadeDelete: true)
                .ForeignKey("dbo.Cms_Tag", t => t.TagId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.TagId);
            
            CreateTable(
                "dbo.Sau_DeclaracaoDeSaude_Dependente",
                c => new
                    {
                        DeclaracaoDeSaudeId = c.Int(nullable: false),
                        DependenteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DeclaracaoDeSaudeId, t.DependenteId })
                .ForeignKey("dbo.Sau_DeclaracaoDeSaude", t => t.DeclaracaoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Dependente", t => t.DependenteId, cascadeDelete: true)
                .Index(t => t.DeclaracaoDeSaudeId)
                .Index(t => t.DependenteId);
            
            CreateTable(
                "dbo.Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude",
                c => new
                    {
                        DeclaracaoDeSaudeId = c.Int(nullable: false),
                        ItemDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DeclaracaoDeSaudeId, t.ItemDeDeclaracaoDeSaudeId })
                .ForeignKey("dbo.Sau_DeclaracaoDeSaude", t => t.DeclaracaoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ItemDeDeclaracaoDeSaude", t => t.ItemDeDeclaracaoDeSaudeId, cascadeDelete: true)
                .Index(t => t.DeclaracaoDeSaudeId)
                .Index(t => t.ItemDeDeclaracaoDeSaudeId);
            
            CreateTable(
                "dbo.Glb_Fornecedor_RamoDoFornecedor",
                c => new
                    {
                        FornecedorId = c.Int(nullable: false),
                        RamoDoFornecedorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FornecedorId, t.RamoDoFornecedorId })
                .ForeignKey("dbo.Glb_Fornecedor", t => t.FornecedorId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_RamoDoFornecedor", t => t.RamoDoFornecedorId, cascadeDelete: true)
                .Index(t => t.FornecedorId)
                .Index(t => t.RamoDoFornecedorId);
            
            CreateTable(
                "dbo.Sau_EspecialidadeSync_ProdutoDePlanoDeSaude",
                c => new
                    {
                        EspecialidadeSyncId = c.Int(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EspecialidadeSyncId, t.ProdutoDePlanoDeSaudeId })
                .ForeignKey("dbo.Sau_EspecialidadeSync", t => t.EspecialidadeSyncId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .Index(t => t.EspecialidadeSyncId)
                .Index(t => t.ProdutoDePlanoDeSaudeId);
            
            CreateTable(
                "dbo.Agd_Evento_Participante",
                c => new
                    {
                        EventoId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EventoId, t.PessoaId })
                .ForeignKey("dbo.Agd_EventoBase", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId, cascadeDelete: true)
                .Index(t => t.EventoId)
                .Index(t => t.PessoaId);
            
            CreateTable(
                "dbo.Cms_ArquivoCMS",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoCMS_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoCMS_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Arquivo", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Sau_ArquivoDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        EmExigencia = c.Boolean(nullable: false),
                        Motivo = c.String(),
                        PropostaDeContratacaoId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoDocumento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoDocumento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Arquivo", t => t.Id)
                .ForeignKey("dbo.Sau_PropostaDeContratacao", t => t.PropostaDeContratacaoId)
                .Index(t => t.Id)
                .Index(t => t.PropostaDeContratacaoId);
            
            CreateTable(
                "dbo.Glb_ArquivoGlobal",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoGlobal_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoGlobal_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Arquivo", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Med_ConfiguracaoDeBloqueioConsulta",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                        MedicoId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeBloqueioConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeBloqueioConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_ConfiguracaoDeBloqueio", t => t.Id)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId)
                .ForeignKey("dbo.Med_Medico", t => t.MedicoId)
                .Index(t => t.Id)
                .Index(t => t.EspecialidadeId)
                .Index(t => t.MedicoId);
            
            CreateTable(
                "dbo.Med_ConfiguracaoDeDisponibilidadeConsulta",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                        MedicoId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeDisponibilidadeConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeDisponibilidadeConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_ConfiguracaoDeDisponibilidade", t => t.Id)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId)
                .ForeignKey("dbo.Med_Medico", t => t.MedicoId)
                .Index(t => t.Id)
                .Index(t => t.EspecialidadeId)
                .Index(t => t.MedicoId);
            
            CreateTable(
                "dbo.Sau_Dependente",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GrauDeParentesco = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Dependente_CorretoraFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Dependente_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Dependente_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_BeneficiarioBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Fin_DocumentoAPagar",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        FornecedorId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentoAPagar_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentoAPagar_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fin_DocumentoBase", t => t.Id)
                .ForeignKey("dbo.Glb_Fornecedor", t => t.FornecedorId)
                .Index(t => t.Id)
                .Index(t => t.FornecedorId);
            
            CreateTable(
                "dbo.Fin_DocumentoAReceber",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentoAReceber_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentoAReceber_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fin_DocumentoBase", t => t.Id)
                .ForeignKey("dbo.Glb_Cliente", t => t.ClienteId)
                .Index(t => t.Id)
                .Index(t => t.ClienteId);
            
            CreateTable(
                "dbo.Med_EventoConsulta",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_EventoBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Agd_EventoReuniao",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoReuniao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoReuniao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_EventoBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Agd_EventoTarefa",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoTarefa_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoTarefa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_EventoBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Sau_Hospital",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Hospital_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Hospital_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_UnidadeDeSaudeBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Sau_Laboratorio",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Laboratorio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Laboratorio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_UnidadeDeSaudeBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Cms_Pagina",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        TipoDePagina = c.String(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pagina_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pagina_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cms_PublicacaoBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Sau_ParametroEZLiv",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroEZLiv_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroEZLiv_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Parametro", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Pag_ParametroEzpag",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        IsTest = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroEzpag_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroEzpag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Parametro", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Agd_ParametroAgenda",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroAgenda_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroAgenda_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Parametro", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Cms_ParametroCMS",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroCMS_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroCMS_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Parametro", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Etq_ParametroEstoque",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroEstoque_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroEstoque_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Parametro", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Glb_ParametroGlobal",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        ConexaoSMTPId = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroGlobal_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroGlobal_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cor_Parametro", t => t.Id)
                .ForeignKey("dbo.Glb_ConexaoSMTP", t => t.ConexaoSMTPId)
                .Index(t => t.Id)
                .Index(t => t.ConexaoSMTPId);
            
            CreateTable(
                "dbo.Glb_PessoaFisica",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Tratamento_Id = c.Int(),
                        Nome = c.String(nullable: false, maxLength: 100),
                        TratamentoId = c.Int(),
                        Sexo = c.Int(),
                        PossuiFilhos = c.Boolean(nullable: false),
                        EstadoCivil = c.Int(),
                        DataDeNascimento = c.DateTime(),
                        Nacionalidade = c.String(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PessoaFisica_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Pessoa", t => t.Id)
                .ForeignKey("dbo.Glb_Tratamento", t => t.Tratamento_Id)
                .Index(t => t.Id)
                .Index(t => t.Tratamento_Id);
            
            CreateTable(
                "dbo.Glb_PessoaJuridica",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        RazaoSocial = c.String(nullable: false, maxLength: 100),
                        NomeFantasia = c.String(nullable: false, maxLength: 100),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PessoaJuridica_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Pessoa", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Cms_Post",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Post_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Post_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cms_PublicacaoBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PlanoDeSaudeId = c.Int(nullable: false),
                        AdministradoraId = c.Int(),
                        OperadoraId = c.Int(nullable: false),
                        CarenciaEspecial = c.Boolean(nullable: false),
                        DescricaoDaCarenciaEspecial = c.String(),
                        Acompanhante = c.Boolean(nullable: false),
                        DescricaoDoAcompanhante = c.String(),
                        CoberturaExtra = c.Boolean(nullable: false),
                        DescricaoDaCoberturaExtra = c.String(),
                        RedeCredenciadaId = c.Int(),
                        FormaDeContratacao = c.Int(nullable: false),
                        AssociacaoId = c.Int(),
                        QuestionarioDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        NumeroDeDiasParaEncerrarAsPropostasRelacionadas = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProdutoDePlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProdutoDePlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Etq_Produto", t => t.Id)
                .ForeignKey("dbo.Sau_PlanoDeSaude", t => t.PlanoDeSaudeId)
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId)
                .ForeignKey("dbo.Sau_Operadora", t => t.OperadoraId)
                .ForeignKey("dbo.Sau_RedeCredenciada", t => t.RedeCredenciadaId)
                .ForeignKey("dbo.Sau_Associacao", t => t.AssociacaoId)
                .ForeignKey("dbo.Sau_QuestionarioDeDeclaracaoDeSaude", t => t.QuestionarioDeDeclaracaoDeSaudeId)
                .Index(t => t.Id)
                .Index(t => t.PlanoDeSaudeId)
                .Index(t => t.AdministradoraId)
                .Index(t => t.OperadoraId)
                .Index(t => t.RedeCredenciadaId)
                .Index(t => t.AssociacaoId)
                .Index(t => t.QuestionarioDeDeclaracaoDeSaudeId);
            
            CreateTable(
                "dbo.Sau_ProponenteTitular",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Responsavel_Id = c.Int(),
                        StatusBeneficiario = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProponenteTitular_CorretoraFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProponenteTitular_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProponenteTitular_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_BeneficiarioBase", t => t.Id)
                .ForeignKey("dbo.Sau_Responsavel", t => t.Responsavel_Id)
                .Index(t => t.Id)
                .Index(t => t.Responsavel_Id);
            
            CreateTable(
                "dbo.Agd_RegraConsulta",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        EspecialidadeId = c.Int(nullable: false),
                        MedicoId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_RegraBase", t => t.Id)
                .ForeignKey("dbo.Med_Especialidade", t => t.EspecialidadeId)
                .ForeignKey("dbo.Med_Medico", t => t.MedicoId)
                .Index(t => t.Id)
                .Index(t => t.EspecialidadeId)
                .Index(t => t.MedicoId);
            
            CreateTable(
                "dbo.Agd_RegraReuniao",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraReuniao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraReuniao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_RegraBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Agd_RegraTarefa",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraTarefa_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraTarefa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agd_RegraBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Cms_WidgetHTML",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Conteudo = c.String(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WidgetHTML_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WidgetHTML_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cms_WidgetBase", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Cms_WidgetMenu",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        MenuId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WidgetMenu_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WidgetMenu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cms_WidgetBase", t => t.Id)
                .ForeignKey("dbo.Cms_Menu", t => t.MenuId, cascadeDelete: true)
                .Index(t => t.Id)
                .Index(t => t.MenuId);
            
            AddColumn("dbo.AbpUsers", "TipoDeUsuario", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "PessoaId", c => c.Int());
            CreateIndex("dbo.AbpUsers", "PessoaId");
            AddForeignKey("dbo.AbpUsers", "PessoaId", "dbo.Glb_Pessoa", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cms_WidgetMenu", "MenuId", "dbo.Cms_Menu");
            DropForeignKey("dbo.Cms_WidgetMenu", "Id", "dbo.Cms_WidgetBase");
            DropForeignKey("dbo.Cms_WidgetHTML", "Id", "dbo.Cms_WidgetBase");
            DropForeignKey("dbo.Agd_RegraTarefa", "Id", "dbo.Agd_RegraBase");
            DropForeignKey("dbo.Agd_RegraReuniao", "Id", "dbo.Agd_RegraBase");
            DropForeignKey("dbo.Agd_RegraConsulta", "MedicoId", "dbo.Med_Medico");
            DropForeignKey("dbo.Agd_RegraConsulta", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Agd_RegraConsulta", "Id", "dbo.Agd_RegraBase");
            DropForeignKey("dbo.Sau_ProponenteTitular", "Responsavel_Id", "dbo.Sau_Responsavel");
            DropForeignKey("dbo.Sau_ProponenteTitular", "Id", "dbo.Sau_BeneficiarioBase");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "QuestionarioDeDeclaracaoDeSaudeId", "dbo.Sau_QuestionarioDeDeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "AssociacaoId", "dbo.Sau_Associacao");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "RedeCredenciadaId", "dbo.Sau_RedeCredenciada");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "OperadoraId", "dbo.Sau_Operadora");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "AdministradoraId", "dbo.Sau_Administradora");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "PlanoDeSaudeId", "dbo.Sau_PlanoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "Id", "dbo.Etq_Produto");
            DropForeignKey("dbo.Cms_Post", "Id", "dbo.Cms_PublicacaoBase");
            DropForeignKey("dbo.Glb_PessoaJuridica", "Id", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_PessoaFisica", "Tratamento_Id", "dbo.Glb_Tratamento");
            DropForeignKey("dbo.Glb_PessoaFisica", "Id", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_ParametroGlobal", "ConexaoSMTPId", "dbo.Glb_ConexaoSMTP");
            DropForeignKey("dbo.Glb_ParametroGlobal", "Id", "dbo.Cor_Parametro");
            DropForeignKey("dbo.Etq_ParametroEstoque", "Id", "dbo.Cor_Parametro");
            DropForeignKey("dbo.Cms_ParametroCMS", "Id", "dbo.Cor_Parametro");
            DropForeignKey("dbo.Agd_ParametroAgenda", "Id", "dbo.Cor_Parametro");
            DropForeignKey("dbo.Pag_ParametroEzpag", "Id", "dbo.Cor_Parametro");
            DropForeignKey("dbo.Sau_ParametroEZLiv", "Id", "dbo.Cor_Parametro");
            DropForeignKey("dbo.Cms_Pagina", "Id", "dbo.Cms_PublicacaoBase");
            DropForeignKey("dbo.Sau_Laboratorio", "Id", "dbo.Sau_UnidadeDeSaudeBase");
            DropForeignKey("dbo.Sau_Hospital", "Id", "dbo.Sau_UnidadeDeSaudeBase");
            DropForeignKey("dbo.Agd_EventoTarefa", "Id", "dbo.Agd_EventoBase");
            DropForeignKey("dbo.Agd_EventoReuniao", "Id", "dbo.Agd_EventoBase");
            DropForeignKey("dbo.Med_EventoConsulta", "Id", "dbo.Agd_EventoBase");
            DropForeignKey("dbo.Fin_DocumentoAReceber", "ClienteId", "dbo.Glb_Cliente");
            DropForeignKey("dbo.Fin_DocumentoAReceber", "Id", "dbo.Fin_DocumentoBase");
            DropForeignKey("dbo.Fin_DocumentoAPagar", "FornecedorId", "dbo.Glb_Fornecedor");
            DropForeignKey("dbo.Fin_DocumentoAPagar", "Id", "dbo.Fin_DocumentoBase");
            DropForeignKey("dbo.Sau_Dependente", "Id", "dbo.Sau_BeneficiarioBase");
            DropForeignKey("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta", "MedicoId", "dbo.Med_Medico");
            DropForeignKey("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta", "Id", "dbo.Agd_ConfiguracaoDeDisponibilidade");
            DropForeignKey("dbo.Med_ConfiguracaoDeBloqueioConsulta", "MedicoId", "dbo.Med_Medico");
            DropForeignKey("dbo.Med_ConfiguracaoDeBloqueioConsulta", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Med_ConfiguracaoDeBloqueioConsulta", "Id", "dbo.Agd_ConfiguracaoDeBloqueio");
            DropForeignKey("dbo.Glb_ArquivoGlobal", "Id", "dbo.Cor_Arquivo");
            DropForeignKey("dbo.Sau_ArquivoDocumento", "PropostaDeContratacaoId", "dbo.Sau_PropostaDeContratacao");
            DropForeignKey("dbo.Sau_ArquivoDocumento", "Id", "dbo.Cor_Arquivo");
            DropForeignKey("dbo.Cms_ArquivoCMS", "Id", "dbo.Cor_Arquivo");
            DropForeignKey("dbo.Glb_PermissaoEmpresa", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_WidgetBase", "TemplateId", "dbo.Cms_Template");
            DropForeignKey("dbo.Cms_WidgetBase", "SiteId", "dbo.Cms_Site");
            DropForeignKey("dbo.Cms_WidgetBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ValorPorFaixaEtaria", "FaixaEtariaId", "dbo.Sau_FaixaEtaria");
            DropForeignKey("dbo.Sau_ValorPorFaixaEtaria", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ValorPorFaixaEtaria", "ContratoId", "dbo.Sau_Contrato");
            DropForeignKey("dbo.Glb_UsuarioPorPessoa", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.Glb_TemplateEmail", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_SaldoEstoque", "SaidaId", "dbo.Etq_Saida");
            DropForeignKey("dbo.Etq_SaldoEstoque", "ProdutoId", "dbo.Etq_Produto");
            DropForeignKey("dbo.Etq_SaldoEstoque", "PedidoId", "dbo.Vnd_Pedido");
            DropForeignKey("dbo.Etq_SaldoEstoque", "LocalArmazenamentoId", "dbo.Etq_LocalArmazenamento");
            DropForeignKey("dbo.Etq_SaldoEstoque", "EntradaId", "dbo.Etq_Entrada");
            DropForeignKey("dbo.Etq_SaldoEstoque", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_SaidaItem", "SaidaId", "dbo.Etq_Saida");
            DropForeignKey("dbo.Etq_Saida", "PedidoId", "dbo.Vnd_Pedido");
            DropForeignKey("dbo.Etq_Saida", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_SaidaItem", "ProdutoId", "dbo.Etq_Produto");
            DropForeignKey("dbo.Etq_SaidaItem", "LocalArmazenamentoId", "dbo.Etq_LocalArmazenamento");
            DropForeignKey("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "ItemDeDeclaracaoDeSaudeId", "dbo.Sau_ItemDeDeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "BeneficiarioId", "dbo.Sau_BeneficiarioBase");
            DropForeignKey("dbo.Agd_RegraBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Pag_ParametroPagSeguro", "PessoaJuridicaId", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Pag_ParametroPagSeguro", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cor_Parametro", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_ItemHierarquia", "ItemHierarquiaPaiId", "dbo.Glb_ItemHierarquia");
            DropForeignKey("dbo.Glb_ItemHierarquia", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ItemDeRedeCredenciada", "UnidadeDeSaudeId", "dbo.Sau_UnidadeDeSaudeBase");
            DropForeignKey("dbo.Sau_UnidadeDeSaudeBase", "Telefone2Id", "dbo.Glb_Telefone");
            DropForeignKey("dbo.Sau_UnidadeDeSaudeBase", "Telefone1Id", "dbo.Glb_Telefone");
            DropForeignKey("dbo.Sau_UnidadeDeSaudeBase", "PessoaJuridicaId", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_UnidadeDeSaudeBase", "EnderecoId", "dbo.Glb_Endereco");
            DropForeignKey("dbo.Sau_UnidadeDeSaudeBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ItemDeRedeCredenciada", "RedeCredenciadaId", "dbo.Sau_RedeCredenciada");
            DropForeignKey("dbo.Sau_ItemDeRedeCredenciada", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_ItemDeMenu", "PaginaId", "dbo.Cms_Pagina");
            DropForeignKey("dbo.Cms_ItemDeMenu", "MenuId", "dbo.Cms_Menu");
            DropForeignKey("dbo.Cms_Menu", "SiteId", "dbo.Cms_Site");
            DropForeignKey("dbo.Cms_Menu", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_ItemDeMenu", "ItemMenuId", "dbo.Cms_ItemDeMenu");
            DropForeignKey("dbo.Cms_ItemDeMenu", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_ItemDeMenu", "CategoriaId", "dbo.Cms_Categoria");
            DropForeignKey("dbo.Pag_HistoricoTransacao", "TransacaoId", "dbo.Pag_Transacao");
            DropForeignKey("dbo.Pag_Transacao", "PedidoId", "dbo.Vnd_Pedido");
            DropForeignKey("dbo.Pag_Transacao", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Pag_HistoricoTransacao", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_Fracao", "UnidadeMedidaId", "dbo.Etq_UnidadeMedida");
            DropForeignKey("dbo.Etq_Fracao", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Pag_FormaDePagamento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_Evento_Participante", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Agd_Evento_Participante", "EventoId", "dbo.Agd_EventoBase");
            DropForeignKey("dbo.Agd_EventoBase", "OwnerId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Agd_HistoricoDoEvento", "EventoId", "dbo.Agd_EventoBase");
            DropForeignKey("dbo.Agd_HistoricoDoEvento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_EventoBase", "EventoPaiId", "dbo.Agd_EventoBase");
            DropForeignKey("dbo.Agd_EventoBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_EventoBase", "DisponibilidadeId", "dbo.Agd_Disponibilidade");
            DropForeignKey("dbo.Sau_EspecialidadeSync_ProdutoDePlanoDeSaude", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_EspecialidadeSync_ProdutoDePlanoDeSaude", "EspecialidadeSyncId", "dbo.Sau_EspecialidadeSync");
            DropForeignKey("dbo.Sau_EspecialidadeSync", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_EntradaItem", "ProdutoId", "dbo.Etq_Produto");
            DropForeignKey("dbo.Etq_EntradaItem", "LocalArmazenamentoId", "dbo.Etq_LocalArmazenamento");
            DropForeignKey("dbo.Etq_EntradaItem", "EntradaId", "dbo.Etq_Entrada");
            DropForeignKey("dbo.Etq_Entrada", "LocalArmazenamento_Id", "dbo.Etq_LocalArmazenamento");
            DropForeignKey("dbo.Etq_LocalArmazenamento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_Entrada", "FornecedorId", "dbo.Glb_Fornecedor");
            DropForeignKey("dbo.Etq_Entrada", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Fin_DocumentoBase", "TipoDeDocumentoFinanceiroId", "dbo.Fin_TipoDeDocumentoFinanceiro");
            DropForeignKey("dbo.Fin_DocumentoBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Fin_TipoDeDocumentoFinanceiro", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Fornecedor_RamoDoFornecedor", "RamoDoFornecedorId", "dbo.Glb_RamoDoFornecedor");
            DropForeignKey("dbo.Glb_Fornecedor_RamoDoFornecedor", "FornecedorId", "dbo.Glb_Fornecedor");
            DropForeignKey("dbo.Glb_RamoDoFornecedor", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Fornecedor", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_Fornecedor", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Departamento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_DeclaracaoDeSaude", "TitularId", "dbo.Sau_ProponenteTitular");
            DropForeignKey("dbo.Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude", "ItemDeDeclaracaoDeSaudeId", "dbo.Sau_ItemDeDeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude", "DeclaracaoDeSaudeId", "dbo.Sau_DeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_DeclaracaoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_DeclaracaoDeSaude_Dependente", "DependenteId", "dbo.Sau_Dependente");
            DropForeignKey("dbo.Sau_DeclaracaoDeSaude_Dependente", "DeclaracaoDeSaudeId", "dbo.Sau_DeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_ContratoHistorico", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_ContratoHistorico", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ContratoHistorico", "ContratoId", "dbo.Sau_Contrato");
            DropForeignKey("dbo.Agd_Disponibilidade", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_ConfiguracaoDeDisponibilidade", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_Disponibilidade", "ConfiguracaoDeDisponibilidadeId", "dbo.Agd_ConfiguracaoDeDisponibilidade");
            DropForeignKey("dbo.Glb_ConexaoSMTP", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ClienteEZLiv", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ClienteEZLiv", "ClienteId", "dbo.Glb_Cliente");
            DropForeignKey("dbo.Cms_Categoria", "SiteId", "dbo.Cms_Site");
            DropForeignKey("dbo.Cms_Post_Tag", "TagId", "dbo.Cms_Tag");
            DropForeignKey("dbo.Cms_Post_Tag", "PostId", "dbo.Cms_Post");
            DropForeignKey("dbo.Cms_Tag", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_HistoricoPublicacao", "PublicacaoId", "dbo.Cms_PublicacaoBase");
            DropForeignKey("dbo.Cms_PublicacaoBase", "SiteId", "dbo.Cms_Site");
            DropForeignKey("dbo.Cms_Site", "TemplateDefaultId", "dbo.Cms_Template");
            DropForeignKey("dbo.Cms_Template", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_Host", "SiteId", "dbo.Cms_Site");
            DropForeignKey("dbo.Cms_Host", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_Site", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_PublicacaoBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_HistoricoPublicacao", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_Post_Categoria", "CategoriaId", "dbo.Cms_Categoria");
            DropForeignKey("dbo.Cms_Post_Categoria", "PostId", "dbo.Cms_Post");
            DropForeignKey("dbo.Cms_Categoria", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cms_Categoria", "CategoriaPaiId", "dbo.Cms_Categoria");
            DropForeignKey("dbo.Agd_Bloqueio", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Med_Medico", "PictureId", "dbo.Cor_Picture");
            DropForeignKey("dbo.Cor_Picture", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Med_Medico", "PessoaFisicaId", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Med_Especialidade_Medico", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Med_Especialidade_Medico", "MedicoId", "dbo.Med_Medico");
            DropForeignKey("dbo.Sau_Laboratorio_Especialidade", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Sau_Laboratorio_Especialidade", "LaboratorioId", "dbo.Sau_Laboratorio");
            DropForeignKey("dbo.Sau_Hospital_Especialidade", "EspecialidadeId", "dbo.Med_Especialidade");
            DropForeignKey("dbo.Sau_Hospital_Especialidade", "HospitalId", "dbo.Sau_Hospital");
            DropForeignKey("dbo.Med_Especialidade", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_ConfiguracaoDeBloqueio", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Agd_Bloqueio", "ConfiguracaoDeBloqueioId", "dbo.Agd_ConfiguracaoDeBloqueio");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "ProfissaoId", "dbo.Glb_Profissao");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "PessoaFisicaId", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "EstadoId", "dbo.Glb_Estado");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Glb_Agencia", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Agencia", "BancoId", "dbo.Glb_Banco");
            DropForeignKey("dbo.Glb_Banco", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_Administradora", "PessoaJuridicaId", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_Administradora", "ImagemId", "dbo.Cor_Arquivo");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "VigenciaId", "dbo.Sau_Vigencia");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "UltimoContratoAceitoId", "dbo.Sau_Contrato");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "TitularId", "dbo.Sau_ProponenteTitular");
            DropForeignKey("dbo.Sau_ProponeteTitular_Dependente", "DependenteId", "dbo.Sau_Dependente");
            DropForeignKey("dbo.Sau_ProponeteTitular_Dependente", "ProponeteTitularId", "dbo.Sau_ProponenteTitular");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ResponsavelId", "dbo.Sau_Responsavel");
            DropForeignKey("dbo.Sau_Responsavel", "PessoaFisicaId", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "PedidoId", "dbo.Vnd_Pedido");
            DropForeignKey("dbo.Vnd_ItemDePedido", "ServicoId", "dbo.Srv_Servico");
            DropForeignKey("dbo.Srv_Servico", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Vnd_ItemDePedido", "ProdutoId", "dbo.Etq_Produto");
            DropForeignKey("dbo.Etq_Produto", "UnidadeMedidaId", "dbo.Etq_UnidadeMedida");
            DropForeignKey("dbo.Etq_Produto", "NaturezaId", "dbo.Etq_Natureza");
            DropForeignKey("dbo.Etq_Produto", "ImagemId", "dbo.Cor_Arquivo");
            DropForeignKey("dbo.Etq_Produto", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Vnd_ItemDePedido", "PedidoId", "dbo.Vnd_Pedido");
            DropForeignKey("dbo.Vnd_Pedido", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Vnd_Pedido", "ClienteId", "dbo.Glb_Cliente");
            DropForeignKey("dbo.Glb_Cliente", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_Cliente", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "CorretorId", "dbo.Sau_Corretor");
            DropForeignKey("dbo.Sau_Corretor", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Sau_Corretor_Corretora", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_Corretor_Corretora", "CorretorId", "dbo.Sau_Corretor");
            DropForeignKey("dbo.Sau_Corretora", "PessoaJuridicaId", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_Corretora", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_Corretora", "CorretorId", "dbo.Sau_Corretor");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ContratoVigenteNoAceiteId", "dbo.Sau_Contrato");
            DropForeignKey("dbo.Sau_Contrato", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Vigencia", "VigenciaId", "dbo.Sau_Vigencia");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Vigencia", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Vigencia", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_UnidadeMedida", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_RedeCredenciada", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_QuestionarioDeDeclaracaoDeSaude", "OperadoraId", "dbo.Sau_Operadora");
            DropForeignKey("dbo.Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude", "ItemDeDeclaracaoDeSaudeId", "dbo.Sau_ItemDeDeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude", "QuestionarioDeDeclaracaoDeSaudeId", "dbo.Sau_QuestionarioDeDeclaracaoDeSaude");
            DropForeignKey("dbo.Sau_ItemDeDeclaracaoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_QuestionarioDeDeclaracaoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_PlanoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_Operadora", "PessoaJuridicaId", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_Operadora", "ImagemId", "dbo.Cor_Arquivo");
            DropForeignKey("dbo.Etq_Natureza", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Estado", "EstadoId", "dbo.Glb_Estado");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Estado", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Cidade", "CidadeId", "dbo.Glb_Cidade");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Cidade", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Chancela", "ChancelaId", "dbo.Sau_Chancela");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Chancela", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Glb_Profissao", "Associacao_Id", "dbo.Sau_Associacao");
            DropForeignKey("dbo.Sau_Associacao", "PessoaJuridicaId", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_Contrato", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ChancelaId", "dbo.Sau_Chancela");
            DropForeignKey("dbo.Sau_Chancela_Profissao", "ProfissaoId", "dbo.Glb_Profissao");
            DropForeignKey("dbo.Sau_Chancela_Profissao", "ChancelaId", "dbo.Sau_Chancela");
            DropForeignKey("dbo.Sau_Chancela", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Cor_Arquivo", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Empresa_Usuario", "UsuarioId", "dbo.AbpUsers");
            DropForeignKey("dbo.Glb_Empresa_Usuario", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.AbpUsers", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_Pessoa", "GrupoPessoaId", "dbo.Glb_GrupoPessoa");
            DropForeignKey("dbo.Glb_GrupoPessoa", "PaiId", "dbo.Glb_GrupoPessoa");
            DropForeignKey("dbo.Glb_Pessoa", "EnderecoPrincipalId", "dbo.Glb_Endereco");
            DropForeignKey("dbo.Glb_Pessoa", "EnderecoEntregaId", "dbo.Glb_Endereco");
            DropForeignKey("dbo.Glb_Pessoa", "EnderecoCobrancaId", "dbo.Glb_Endereco");
            DropForeignKey("dbo.Glb_Pessoa", "DocumentoPrincipalId", "dbo.Glb_Documento");
            DropForeignKey("dbo.Glb_Documento", "TipoDeDocumentoId", "dbo.Glb_TipoDeDocumento");
            DropForeignKey("dbo.Glb_TipoDeDocumento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Documento", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_Documento", "EstadoId", "dbo.Glb_Estado");
            DropForeignKey("dbo.Glb_Documento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Contato", "TratamentoId", "dbo.Glb_Tratamento");
            DropForeignKey("dbo.Glb_Tratamento", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Contato", "TipoDeContatoId", "dbo.Glb_TipoDeContato");
            DropForeignKey("dbo.Glb_TipoDeContato", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Telefone", "Contato_Id", "dbo.Glb_Contato");
            DropForeignKey("dbo.Glb_Contato", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_EnderecoEletronico", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_EnderecoEletronico", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_EnderecoEletronico", "ContatoId", "dbo.Glb_Contato");
            DropForeignKey("dbo.Glb_Contato", "EnderecoPrincipalId", "dbo.Glb_Endereco");
            DropForeignKey("dbo.Glb_Endereco", "TipoDeLogradouroId", "dbo.Glb_TipoDeLogradouro");
            DropForeignKey("dbo.Glb_TipoDeLogradouro", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Telefone", "Endereco_Id", "dbo.Glb_Endereco");
            DropForeignKey("dbo.Glb_Telefone", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_Telefone", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Endereco", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Glb_Endereco", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Endereco", "ContatoId", "dbo.Glb_Contato");
            DropForeignKey("dbo.Glb_Endereco", "CidadeId", "dbo.Glb_Cidade");
            DropForeignKey("dbo.Glb_Cidade", "EstadoId", "dbo.Glb_Estado");
            DropForeignKey("dbo.Glb_Estado", "PaisId", "dbo.Glb_Pais");
            DropForeignKey("dbo.Glb_Feriado", "PaisId", "dbo.Glb_Pais");
            DropForeignKey("dbo.Glb_Pais", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Feriado", "EstadoId", "dbo.Glb_Estado");
            DropForeignKey("dbo.Glb_Feriado", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Feriado", "CidadeId", "dbo.Glb_Cidade");
            DropForeignKey("dbo.Glb_Estado", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Cidade", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Contato", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Empresa", "ClienteEZId", "dbo.Glb_ClienteEZ");
            DropIndex("dbo.Cms_WidgetMenu", new[] { "MenuId" });
            DropIndex("dbo.Cms_WidgetMenu", new[] { "Id" });
            DropIndex("dbo.Cms_WidgetHTML", new[] { "Id" });
            DropIndex("dbo.Agd_RegraTarefa", new[] { "Id" });
            DropIndex("dbo.Agd_RegraReuniao", new[] { "Id" });
            DropIndex("dbo.Agd_RegraConsulta", new[] { "MedicoId" });
            DropIndex("dbo.Agd_RegraConsulta", new[] { "EspecialidadeId" });
            DropIndex("dbo.Agd_RegraConsulta", new[] { "Id" });
            DropIndex("dbo.Sau_ProponenteTitular", new[] { "Responsavel_Id" });
            DropIndex("dbo.Sau_ProponenteTitular", new[] { "Id" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "QuestionarioDeDeclaracaoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "AssociacaoId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "RedeCredenciadaId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "OperadoraId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "AdministradoraId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "PlanoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "Id" });
            DropIndex("dbo.Cms_Post", new[] { "Id" });
            DropIndex("dbo.Glb_PessoaJuridica", new[] { "Id" });
            DropIndex("dbo.Glb_PessoaFisica", new[] { "Tratamento_Id" });
            DropIndex("dbo.Glb_PessoaFisica", new[] { "Id" });
            DropIndex("dbo.Glb_ParametroGlobal", new[] { "ConexaoSMTPId" });
            DropIndex("dbo.Glb_ParametroGlobal", new[] { "Id" });
            DropIndex("dbo.Etq_ParametroEstoque", new[] { "Id" });
            DropIndex("dbo.Cms_ParametroCMS", new[] { "Id" });
            DropIndex("dbo.Agd_ParametroAgenda", new[] { "Id" });
            DropIndex("dbo.Pag_ParametroEzpag", new[] { "Id" });
            DropIndex("dbo.Sau_ParametroEZLiv", new[] { "Id" });
            DropIndex("dbo.Cms_Pagina", new[] { "Id" });
            DropIndex("dbo.Sau_Laboratorio", new[] { "Id" });
            DropIndex("dbo.Sau_Hospital", new[] { "Id" });
            DropIndex("dbo.Agd_EventoTarefa", new[] { "Id" });
            DropIndex("dbo.Agd_EventoReuniao", new[] { "Id" });
            DropIndex("dbo.Med_EventoConsulta", new[] { "Id" });
            DropIndex("dbo.Fin_DocumentoAReceber", new[] { "ClienteId" });
            DropIndex("dbo.Fin_DocumentoAReceber", new[] { "Id" });
            DropIndex("dbo.Fin_DocumentoAPagar", new[] { "FornecedorId" });
            DropIndex("dbo.Fin_DocumentoAPagar", new[] { "Id" });
            DropIndex("dbo.Sau_Dependente", new[] { "Id" });
            DropIndex("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta", new[] { "MedicoId" });
            DropIndex("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta", new[] { "EspecialidadeId" });
            DropIndex("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta", new[] { "Id" });
            DropIndex("dbo.Med_ConfiguracaoDeBloqueioConsulta", new[] { "MedicoId" });
            DropIndex("dbo.Med_ConfiguracaoDeBloqueioConsulta", new[] { "EspecialidadeId" });
            DropIndex("dbo.Med_ConfiguracaoDeBloqueioConsulta", new[] { "Id" });
            DropIndex("dbo.Glb_ArquivoGlobal", new[] { "Id" });
            DropIndex("dbo.Sau_ArquivoDocumento", new[] { "PropostaDeContratacaoId" });
            DropIndex("dbo.Sau_ArquivoDocumento", new[] { "Id" });
            DropIndex("dbo.Cms_ArquivoCMS", new[] { "Id" });
            DropIndex("dbo.Agd_Evento_Participante", new[] { "PessoaId" });
            DropIndex("dbo.Agd_Evento_Participante", new[] { "EventoId" });
            DropIndex("dbo.Sau_EspecialidadeSync_ProdutoDePlanoDeSaude", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_EspecialidadeSync_ProdutoDePlanoDeSaude", new[] { "EspecialidadeSyncId" });
            DropIndex("dbo.Glb_Fornecedor_RamoDoFornecedor", new[] { "RamoDoFornecedorId" });
            DropIndex("dbo.Glb_Fornecedor_RamoDoFornecedor", new[] { "FornecedorId" });
            DropIndex("dbo.Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude", new[] { "ItemDeDeclaracaoDeSaudeId" });
            DropIndex("dbo.Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude", new[] { "DeclaracaoDeSaudeId" });
            DropIndex("dbo.Sau_DeclaracaoDeSaude_Dependente", new[] { "DependenteId" });
            DropIndex("dbo.Sau_DeclaracaoDeSaude_Dependente", new[] { "DeclaracaoDeSaudeId" });
            DropIndex("dbo.Cms_Post_Tag", new[] { "TagId" });
            DropIndex("dbo.Cms_Post_Tag", new[] { "PostId" });
            DropIndex("dbo.Cms_Post_Categoria", new[] { "CategoriaId" });
            DropIndex("dbo.Cms_Post_Categoria", new[] { "PostId" });
            DropIndex("dbo.Med_Especialidade_Medico", new[] { "EspecialidadeId" });
            DropIndex("dbo.Med_Especialidade_Medico", new[] { "MedicoId" });
            DropIndex("dbo.Sau_Laboratorio_Especialidade", new[] { "EspecialidadeId" });
            DropIndex("dbo.Sau_Laboratorio_Especialidade", new[] { "LaboratorioId" });
            DropIndex("dbo.Sau_Hospital_Especialidade", new[] { "EspecialidadeId" });
            DropIndex("dbo.Sau_Hospital_Especialidade", new[] { "HospitalId" });
            DropIndex("dbo.Sau_ProponeteTitular_Dependente", new[] { "DependenteId" });
            DropIndex("dbo.Sau_ProponeteTitular_Dependente", new[] { "ProponeteTitularId" });
            DropIndex("dbo.Sau_Corretor_Corretora", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_Corretor_Corretora", new[] { "CorretorId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Vigencia", new[] { "VigenciaId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Vigencia", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude", new[] { "ItemDeDeclaracaoDeSaudeId" });
            DropIndex("dbo.Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude", new[] { "QuestionarioDeDeclaracaoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Estado", new[] { "EstadoId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Estado", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Cidade", new[] { "CidadeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Cidade", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Chancela", new[] { "ChancelaId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Chancela", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_Chancela_Profissao", new[] { "ProfissaoId" });
            DropIndex("dbo.Sau_Chancela_Profissao", new[] { "ChancelaId" });
            DropIndex("dbo.Glb_Empresa_Usuario", new[] { "UsuarioId" });
            DropIndex("dbo.Glb_Empresa_Usuario", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_WidgetBase", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_WidgetBase", new[] { "TemplateId" });
            DropIndex("dbo.Cms_WidgetBase", new[] { "SiteId" });
            DropIndex("dbo.Cms_WidgetBase", "IX_Widget_Titulo");
            DropIndex("dbo.Sau_ValorPorFaixaEtaria", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_ValorPorFaixaEtaria", new[] { "ContratoId" });
            DropIndex("dbo.Sau_ValorPorFaixaEtaria", new[] { "FaixaEtariaId" });
            DropIndex("dbo.Glb_UsuarioPorPessoa", new[] { "UserId" });
            DropIndex("dbo.Glb_TemplateEmail", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_TemplateEmail", "IX_TemplateEmail_Nome");
            DropIndex("dbo.Etq_SaldoEstoque", new[] { "PedidoId" });
            DropIndex("dbo.Etq_SaldoEstoque", new[] { "SaidaId" });
            DropIndex("dbo.Etq_SaldoEstoque", new[] { "EntradaId" });
            DropIndex("dbo.Etq_SaldoEstoque", new[] { "ProdutoId" });
            DropIndex("dbo.Etq_SaldoEstoque", new[] { "LocalArmazenamentoId" });
            DropIndex("dbo.Etq_SaldoEstoque", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_Saida", new[] { "PedidoId" });
            DropIndex("dbo.Etq_Saida", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_SaidaItem", new[] { "ProdutoId" });
            DropIndex("dbo.Etq_SaidaItem", new[] { "LocalArmazenamentoId" });
            DropIndex("dbo.Etq_SaidaItem", new[] { "SaidaId" });
            DropIndex("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", new[] { "BeneficiarioId" });
            DropIndex("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", new[] { "ItemDeDeclaracaoDeSaudeId" });
            DropIndex("dbo.Agd_RegraBase", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_PermissaoEmpresa", new[] { "EmpresaId" });
            DropIndex("dbo.Pag_ParametroPagSeguro", new[] { "EmpresaId" });
            DropIndex("dbo.Pag_ParametroPagSeguro", new[] { "PessoaJuridicaId" });
            DropIndex("dbo.Cor_Parametro", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_ItemHierarquia", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_ItemHierarquia", new[] { "ItemHierarquiaPaiId" });
            DropIndex("dbo.Glb_ItemHierarquia", "IX_ItemHierarquia_Nome");
            DropIndex("dbo.Sau_ItemDeRedeCredenciada", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_ItemDeRedeCredenciada", new[] { "UnidadeDeSaudeId" });
            DropIndex("dbo.Sau_ItemDeRedeCredenciada", new[] { "RedeCredenciadaId" });
            DropIndex("dbo.Cms_Menu", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_Menu", new[] { "SiteId" });
            DropIndex("dbo.Cms_Menu", "IX_Menu_Nome");
            DropIndex("dbo.Cms_ItemDeMenu", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_ItemDeMenu", "IX_Menu_Titulo");
            DropIndex("dbo.Cms_ItemDeMenu", new[] { "PaginaId" });
            DropIndex("dbo.Cms_ItemDeMenu", new[] { "CategoriaId" });
            DropIndex("dbo.Cms_ItemDeMenu", new[] { "ItemMenuId" });
            DropIndex("dbo.Cms_ItemDeMenu", new[] { "MenuId" });
            DropIndex("dbo.Pag_Transacao", new[] { "EmpresaId" });
            DropIndex("dbo.Pag_Transacao", new[] { "PedidoId" });
            DropIndex("dbo.Pag_HistoricoTransacao", new[] { "EmpresaId" });
            DropIndex("dbo.Pag_HistoricoTransacao", new[] { "TransacaoId" });
            DropIndex("dbo.Etq_Fracao", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_Fracao", new[] { "UnidadeMedidaId" });
            DropIndex("dbo.Pag_FormaDePagamento", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_HistoricoDoEvento", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_HistoricoDoEvento", new[] { "EventoId" });
            DropIndex("dbo.Agd_EventoBase", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_EventoBase", new[] { "DisponibilidadeId" });
            DropIndex("dbo.Agd_EventoBase", new[] { "OwnerId" });
            DropIndex("dbo.Agd_EventoBase", new[] { "EventoPaiId" });
            DropIndex("dbo.Sau_EspecialidadeSync", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude", new[] { "EspecialidadeId" });
            DropIndex("dbo.Etq_LocalArmazenamento", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_Entrada", new[] { "LocalArmazenamento_Id" });
            DropIndex("dbo.Etq_Entrada", new[] { "FornecedorId" });
            DropIndex("dbo.Etq_Entrada", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_EntradaItem", new[] { "EntradaId" });
            DropIndex("dbo.Etq_EntradaItem", new[] { "ProdutoId" });
            DropIndex("dbo.Etq_EntradaItem", new[] { "LocalArmazenamentoId" });
            DropIndex("dbo.Fin_TipoDeDocumentoFinanceiro", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_RamoDoFornecedor", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_RamoDoFornecedor", "IX_RamoDoFornecedor_Nome");
            DropIndex("dbo.Glb_Fornecedor", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Fornecedor", new[] { "EmpresaId" });
            DropIndex("dbo.Fin_DocumentoBase", new[] { "EmpresaId" });
            DropIndex("dbo.Fin_DocumentoBase", new[] { "TipoDeDocumentoFinanceiroId" });
            DropIndex("dbo.Glb_Departamento", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Departamento", "IX_Departamento_Nome");
            DropIndex("dbo.Sau_DeclaracaoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_DeclaracaoDeSaude", new[] { "TitularId" });
            DropIndex("dbo.Sau_ContratoHistorico", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_ContratoHistorico", new[] { "ContratoId" });
            DropIndex("dbo.Sau_ContratoHistorico", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Agd_Disponibilidade", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_Disponibilidade", new[] { "ConfiguracaoDeDisponibilidadeId" });
            DropIndex("dbo.Agd_ConfiguracaoDeDisponibilidade", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_ConexaoSMTP", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_ConexaoSMTP", "IX_ConexaoSMTP_Nome");
            DropIndex("dbo.Sau_ClienteEZLiv", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_ClienteEZLiv", new[] { "ClienteId" });
            DropIndex("dbo.Cms_Tag", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_Tag", "IX_Tag_Nome");
            DropIndex("dbo.Cms_Template", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_Template", "IX_Template_Nome");
            DropIndex("dbo.Cms_Host", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_Host", new[] { "SiteId" });
            DropIndex("dbo.Cms_Site", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_Site", new[] { "TemplateDefaultId" });
            DropIndex("dbo.Cms_Site", "IX_Site_Nome");
            DropIndex("dbo.Cms_HistoricoPublicacao", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_HistoricoPublicacao", new[] { "PublicacaoId" });
            DropIndex("dbo.Cms_PublicacaoBase", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_PublicacaoBase", "IX_Publicacao_Titulo");
            DropIndex("dbo.Cms_PublicacaoBase", new[] { "SiteId" });
            DropIndex("dbo.Cms_Categoria", new[] { "EmpresaId" });
            DropIndex("dbo.Cms_Categoria", new[] { "CategoriaPaiId" });
            DropIndex("dbo.Cms_Categoria", new[] { "SiteId" });
            DropIndex("dbo.Cms_Categoria", "IX_Categoria_Nome");
            DropIndex("dbo.Cor_Picture", new[] { "EmpresaId" });
            DropIndex("dbo.Med_Medico", new[] { "PictureId" });
            DropIndex("dbo.Med_Medico", new[] { "PessoaFisicaId" });
            DropIndex("dbo.Sau_UnidadeDeSaudeBase", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_UnidadeDeSaudeBase", new[] { "Telefone2Id" });
            DropIndex("dbo.Sau_UnidadeDeSaudeBase", new[] { "Telefone1Id" });
            DropIndex("dbo.Sau_UnidadeDeSaudeBase", new[] { "EnderecoId" });
            DropIndex("dbo.Sau_UnidadeDeSaudeBase", new[] { "PessoaJuridicaId" });
            DropIndex("dbo.Med_Especialidade", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_ConfiguracaoDeBloqueio", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_Bloqueio", new[] { "EmpresaId" });
            DropIndex("dbo.Agd_Bloqueio", new[] { "ConfiguracaoDeBloqueioId" });
            DropIndex("dbo.Glb_Banco", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Banco", "IX_Banco_Codigo");
            DropIndex("dbo.Glb_Agencia", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Agencia", new[] { "BancoId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "PessoaFisicaId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "EstadoId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "ProfissaoId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_Responsavel", new[] { "PessoaFisicaId" });
            DropIndex("dbo.Srv_Servico", new[] { "EmpresaId" });
            DropIndex("dbo.Vnd_ItemDePedido", new[] { "ServicoId" });
            DropIndex("dbo.Vnd_ItemDePedido", new[] { "ProdutoId" });
            DropIndex("dbo.Vnd_ItemDePedido", new[] { "PedidoId" });
            DropIndex("dbo.Glb_Cliente", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Cliente", new[] { "EmpresaId" });
            DropIndex("dbo.Vnd_Pedido", new[] { "EmpresaId" });
            DropIndex("dbo.Vnd_Pedido", new[] { "ClienteId" });
            DropIndex("dbo.Sau_Corretora", new[] { "PessoaJuridicaId" });
            DropIndex("dbo.Sau_Corretora", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Corretora", new[] { "CorretorId" });
            DropIndex("dbo.Sau_Corretor", new[] { "PessoaId" });
            DropIndex("dbo.Sau_Vigencia", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_UnidadeMedida", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_RedeCredenciada", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_ItemDeDeclaracaoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_QuestionarioDeDeclaracaoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_QuestionarioDeDeclaracaoDeSaude", new[] { "OperadoraId" });
            DropIndex("dbo.Sau_PlanoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Operadora", new[] { "ImagemId" });
            DropIndex("dbo.Sau_Operadora", new[] { "PessoaJuridicaId" });
            DropIndex("dbo.Etq_Natureza", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Associacao", new[] { "PessoaJuridicaId" });
            DropIndex("dbo.Etq_Produto", new[] { "ImagemId" });
            DropIndex("dbo.Etq_Produto", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_Produto", new[] { "UnidadeMedidaId" });
            DropIndex("dbo.Etq_Produto", new[] { "NaturezaId" });
            DropIndex("dbo.Sau_Contrato", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Contrato", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Glb_Profissao", new[] { "Associacao_Id" });
            DropIndex("dbo.Sau_Chancela", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "UltimoContratoAceitoId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "TitularId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ResponsavelId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ProdutoId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ContratoVigenteNoAceiteId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ChancelaId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "VigenciaId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "PedidoId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "CorretorId" });
            DropIndex("dbo.Glb_GrupoPessoa", new[] { "PaiId" });
            DropIndex("dbo.Glb_TipoDeDocumento", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_TipoDeDocumento", "IX_TipoDeDocumento_Descricao");
            DropIndex("dbo.Glb_Documento", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Documento", new[] { "EstadoId" });
            DropIndex("dbo.Glb_Documento", new[] { "TipoDeDocumentoId" });
            DropIndex("dbo.Glb_Documento", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Tratamento", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_TipoDeContato", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_EnderecoEletronico", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_EnderecoEletronico", new[] { "PessoaId" });
            DropIndex("dbo.Glb_EnderecoEletronico", new[] { "ContatoId" });
            DropIndex("dbo.Glb_TipoDeLogradouro", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Telefone", new[] { "Contato_Id" });
            DropIndex("dbo.Glb_Telefone", new[] { "Endereco_Id" });
            DropIndex("dbo.Glb_Telefone", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Telefone", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Pais", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Pais", "IX_Pais_CodigoIso");
            DropIndex("dbo.Glb_Pais", "IX_Pais_Nacionalidade");
            DropIndex("dbo.Glb_Pais", "IX_Pais_Nome");
            DropIndex("dbo.Glb_Feriado", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Feriado", new[] { "CidadeId" });
            DropIndex("dbo.Glb_Feriado", new[] { "EstadoId" });
            DropIndex("dbo.Glb_Feriado", new[] { "PaisId" });
            DropIndex("dbo.Glb_Feriado", "IX_Feriado_Nome");
            DropIndex("dbo.Glb_Estado", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Estado", new[] { "PaisId" });
            DropIndex("dbo.Glb_Estado", "IX_Estado_Sigla");
            DropIndex("dbo.Glb_Estado", "IX_Estado_Nome");
            DropIndex("dbo.Glb_Cidade", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Cidade", new[] { "EstadoId" });
            DropIndex("dbo.Glb_Endereco", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Endereco", new[] { "CidadeId" });
            DropIndex("dbo.Glb_Endereco", new[] { "TipoDeLogradouroId" });
            DropIndex("dbo.Glb_Endereco", new[] { "ContatoId" });
            DropIndex("dbo.Glb_Endereco", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Contato", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Contato", new[] { "EnderecoPrincipalId" });
            DropIndex("dbo.Glb_Contato", new[] { "TratamentoId" });
            DropIndex("dbo.Glb_Contato", new[] { "TipoDeContatoId" });
            DropIndex("dbo.Glb_Contato", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Pessoa", new[] { "GrupoPessoaId" });
            DropIndex("dbo.Glb_Pessoa", new[] { "DocumentoPrincipalId" });
            DropIndex("dbo.Glb_Pessoa", new[] { "EnderecoEntregaId" });
            DropIndex("dbo.Glb_Pessoa", new[] { "EnderecoCobrancaId" });
            DropIndex("dbo.Glb_Pessoa", new[] { "EnderecoPrincipalId" });
            DropIndex("dbo.AbpUsers", new[] { "PessoaId" });
            DropIndex("dbo.Glb_Empresa", new[] { "ClienteEZId" });
            DropIndex("dbo.Cor_Arquivo", new[] { "EmpresaId" });
            DropIndex("dbo.Cor_Arquivo", "IX_Arquivo_Nome");
            DropIndex("dbo.Sau_Administradora", new[] { "ImagemId" });
            DropIndex("dbo.Sau_Administradora", new[] { "PessoaJuridicaId" });
            DropColumn("dbo.AbpUsers", "PessoaId");
            DropColumn("dbo.AbpUsers", "TipoDeUsuario");
            DropTable("dbo.Cms_WidgetMenu",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WidgetMenu_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WidgetMenu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_WidgetHTML",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WidgetHTML_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WidgetHTML_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_RegraTarefa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraTarefa_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraTarefa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_RegraReuniao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraReuniao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraReuniao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_RegraConsulta",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ProponenteTitular",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProponenteTitular_CorretoraFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProponenteTitular_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProponenteTitular_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ProdutoDePlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ProdutoDePlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Post",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Post_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Post_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_PessoaJuridica",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PessoaJuridica_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_PessoaFisica",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PessoaFisica_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_ParametroGlobal",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroGlobal_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroGlobal_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_ParametroEstoque",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroEstoque_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroEstoque_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_ParametroCMS",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroCMS_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroCMS_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_ParametroAgenda",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroAgenda_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroAgenda_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Pag_ParametroEzpag",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroEzpag_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroEzpag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ParametroEZLiv",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroEZLiv_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroEZLiv_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Pagina",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pagina_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pagina_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Laboratorio",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Laboratorio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Laboratorio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Hospital",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Hospital_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Hospital_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_EventoTarefa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoTarefa_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoTarefa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_EventoReuniao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoReuniao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoReuniao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Med_EventoConsulta",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Fin_DocumentoAReceber",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentoAReceber_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentoAReceber_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Fin_DocumentoAPagar",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentoAPagar_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentoAPagar_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Dependente",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Dependente_CorretoraFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Dependente_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Dependente_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Med_ConfiguracaoDeDisponibilidadeConsulta",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeDisponibilidadeConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeDisponibilidadeConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Med_ConfiguracaoDeBloqueioConsulta",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeBloqueioConsulta_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeBloqueioConsulta_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_ArquivoGlobal",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoGlobal_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoGlobal_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ArquivoDocumento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoDocumento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoDocumento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_ArquivoCMS",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoCMS_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoCMS_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_Evento_Participante");
            DropTable("dbo.Sau_EspecialidadeSync_ProdutoDePlanoDeSaude");
            DropTable("dbo.Glb_Fornecedor_RamoDoFornecedor");
            DropTable("dbo.Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude");
            DropTable("dbo.Sau_DeclaracaoDeSaude_Dependente");
            DropTable("dbo.Cms_Post_Tag");
            DropTable("dbo.Cms_Post_Categoria");
            DropTable("dbo.Med_Especialidade_Medico");
            DropTable("dbo.Sau_Laboratorio_Especialidade");
            DropTable("dbo.Sau_Hospital_Especialidade");
            DropTable("dbo.Sau_ProponeteTitular_Dependente");
            DropTable("dbo.Sau_Corretor_Corretora");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Vigencia");
            DropTable("dbo.Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Estado");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Cidade");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Chancela");
            DropTable("dbo.Sau_Chancela_Profissao");
            DropTable("dbo.Glb_Empresa_Usuario");
            DropTable("dbo.Cms_WidgetBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_WidgetBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_WidgetBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ValorPorFaixaEtaria",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ValorPorFaixaEtaria_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ValorPorFaixaEtaria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_UsuarioPorPessoa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UsuarioPorPessoa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_TemplateEmail",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TemplateEmail_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TemplateEmail_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_SaldoEstoque",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SaldoEstoque_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_Saida",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Saida_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_SaidaItem",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SaidaItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RespostaDoItemDeDeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RespostaDoItemDeDeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_RegraBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RegraBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RegraBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_PermissaoEmpresa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PermissaoEmpresa_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PermissaoEmpresaPorRole_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PermissaoEmpresaPorUsuario_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Pag_ParametroPagSeguro",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroPagSeguro_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroPagSeguro_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cor_Parametro",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ParametroBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ParametroBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_ItemHierarquia",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemHierarquia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemHierarquia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ItemDeRedeCredenciada",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeRedeCredenciada_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeRedeCredenciada_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Menu",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Menu_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Menu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_ItemDeMenu",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeMenu_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeMenu_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Pag_Transacao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Transacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Transacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Pag_HistoricoTransacao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoricoTransacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoricoTransacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_Fracao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Fracao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Fracao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Pag_FormaDePagamento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FormaDePagamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_FormaDePagamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_FaixaEtaria",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_FaixaEtaria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_HistoricoDoEvento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoricoDoEvento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoricoDoEvento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_EventoBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EventoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EventoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_EspecialidadeSync",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EspecialidadeSync_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EspecialidadeSync_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_EspecialidadePorProdutoDePlanoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EspecialidadePorProdutoDePlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EspecialidadePorProdutoDePlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_LocalArmazenamento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_LocalArmazenamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_LocalArmazenamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_Entrada",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Entrada_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_EntradaItem",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EntradaItem_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Fin_TipoDeDocumentoFinanceiro",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeDocumentoFinanceiro_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeDocumentoFinanceiro_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_RamoDoFornecedor",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RamoDoFornecedor_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RamoDoFornecedor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Fornecedor",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Fornecedor_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Fornecedor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Fin_DocumentoBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DocumentoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DocumentoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Departamento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Departamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Departamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_DeclaracaoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ContratoHistorico",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ContratoHistorico_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ContratoHistorico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_Disponibilidade",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Disponibilidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Disponibilidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_ConfiguracaoDeDisponibilidade",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeDisponibilidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeDisponibilidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_ConexaoSMTP",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConexaoSMTP_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConexaoSMTP_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ClienteEZLiv",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClienteEZLiv_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ClienteEZLiv_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Tag",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Tag_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Tag_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Template",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Template_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Template_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Host",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Host_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Host_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Site",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Site_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Site_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_HistoricoPublicacao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HistoricoPublicacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_HistoricoPublicacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_PublicacaoBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PublicacaoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PublicacaoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cms_Categoria",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Categoria_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Categoria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cor_Picture",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Picture_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Picture_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Med_Medico",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Medico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_UnidadeDeSaudeBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnidadeDeSaudeBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UnidadeDeSaudeBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Med_Especialidade",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Especialidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Especialidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_ConfiguracaoDeBloqueio",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ConfiguracaoDeBloqueio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ConfiguracaoDeBloqueio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Agd_Bloqueio",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Bloqueio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Bloqueio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Banco",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Banco_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Banco_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Agencia",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Agencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Agencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_BeneficiarioBase",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_BeneficiarioBase_CorretoraFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_BeneficiarioBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_BeneficiarioBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Responsavel",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Responsavel_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Srv_Servico",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Servico_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Servico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Vnd_ItemDePedido",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDePedido_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Cliente",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Cliente_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Cliente_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Vnd_Pedido",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pedido_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pedido_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pedido_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Corretora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Corretora_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Corretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Corretor",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Corretor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Vigencia",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Vigencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Vigencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_UnidadeMedida",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_UnidadeMedida_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_UnidadeMedida_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_RedeCredenciada",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RedeCredenciada_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_RedeCredenciada_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ItemDeDeclaracaoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeDeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeDeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_QuestionarioDeDeclaracaoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_QuestionarioDeDeclaracaoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_QuestionarioDeDeclaracaoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_PlanoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Operadora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Operadora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_Natureza",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Natureza_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Natureza_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Associacao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Associacao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Etq_Produto",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Produto_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Produto_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Contrato",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Contrato_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Contrato_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Contrato_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Profissao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Profissao_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Chancela",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Chancela_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Chancela_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_PropostaDeContratacao",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PropostaDeContratacao_CorretorFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PropostaDeContratacao_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_PropostaDeContratacao_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_GrupoPessoa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GrupoPessoa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_TipoDeDocumento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeDocumento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeDocumento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Documento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Documento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Documento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Tratamento",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Tratamento_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Tratamento_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_TipoDeContato",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeContato_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeContato_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_EnderecoEletronico",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnderecoEletronico_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_EnderecoEletronico_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_TipoDeLogradouro",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TipoDeLogradouro_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_TipoDeLogradouro_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Telefone",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Telefone_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Telefone_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Pais",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pais_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Pais_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Feriado",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Feriado_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Feriado_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Estado",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Estado_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Estado_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Cidade",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Cidade_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Cidade_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Endereco",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Endereco_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Endereco_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Contato",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Contato_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Contato_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Pessoa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Pessoa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_ClienteEZ",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ClienteEZ_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Glb_Empresa",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Empresa_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Cor_Arquivo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ArquivoBase_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ArquivoBase_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_Administradora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Administradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
