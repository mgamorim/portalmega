namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_Administradora : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_Administradora_Corretora",
                c => new
                    {
                        AdministradoraId = c.Int(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AdministradoraId, t.CorretoraId })
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId, cascadeDelete: true)
                .Index(t => t.AdministradoraId)
                .Index(t => t.CorretoraId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_Administradora_Corretora", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_Administradora_Corretora", "AdministradoraId", "dbo.Sau_Administradora");
            DropIndex("dbo.Sau_Administradora_Corretora", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_Administradora_Corretora", new[] { "AdministradoraId" });
            DropTable("dbo.Sau_Administradora_Corretora");
        }
    }
}
