namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_Administradora_Add_Supervisor_Gerente_Homologador_Administradora : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_GerenteAdministradora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        AdministradoraId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        PessoaFisica_Id = c.Int(),
                        PessoaJuridica_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GerenteAdministradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisica_Id)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridica_Id)
                .Index(t => t.AdministradoraId)
                .Index(t => t.PessoaId)
                .Index(t => t.PessoaFisica_Id)
                .Index(t => t.PessoaJuridica_Id);
            
            CreateTable(
                "dbo.Sau_HomologadorAdministradora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        AdministradoraId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        PessoaFisica_Id = c.Int(),
                        PessoaJuridica_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HomologadorAdministradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisica_Id)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridica_Id)
                .Index(t => t.AdministradoraId)
                .Index(t => t.PessoaId)
                .Index(t => t.PessoaFisica_Id)
                .Index(t => t.PessoaJuridica_Id);
            
            CreateTable(
                "dbo.Sau_SupervisorAdministradora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        AdministradoraId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        PessoaFisica_Id = c.Int(),
                        PessoaJuridica_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupervisorAdministradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisica_Id)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridica_Id)
                .Index(t => t.AdministradoraId)
                .Index(t => t.PessoaId)
                .Index(t => t.PessoaFisica_Id)
                .Index(t => t.PessoaJuridica_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_SupervisorAdministradora", "PessoaJuridica_Id", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_SupervisorAdministradora", "PessoaFisica_Id", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_SupervisorAdministradora", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Sau_SupervisorAdministradora", "AdministradoraId", "dbo.Sau_Administradora");
            DropForeignKey("dbo.Sau_HomologadorAdministradora", "PessoaJuridica_Id", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_HomologadorAdministradora", "PessoaFisica_Id", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_HomologadorAdministradora", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Sau_HomologadorAdministradora", "AdministradoraId", "dbo.Sau_Administradora");
            DropForeignKey("dbo.Sau_GerenteAdministradora", "PessoaJuridica_Id", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_GerenteAdministradora", "PessoaFisica_Id", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_GerenteAdministradora", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Sau_GerenteAdministradora", "AdministradoraId", "dbo.Sau_Administradora");
            DropIndex("dbo.Sau_SupervisorAdministradora", new[] { "PessoaJuridica_Id" });
            DropIndex("dbo.Sau_SupervisorAdministradora", new[] { "PessoaFisica_Id" });
            DropIndex("dbo.Sau_SupervisorAdministradora", new[] { "PessoaId" });
            DropIndex("dbo.Sau_SupervisorAdministradora", new[] { "AdministradoraId" });
            DropIndex("dbo.Sau_HomologadorAdministradora", new[] { "PessoaJuridica_Id" });
            DropIndex("dbo.Sau_HomologadorAdministradora", new[] { "PessoaFisica_Id" });
            DropIndex("dbo.Sau_HomologadorAdministradora", new[] { "PessoaId" });
            DropIndex("dbo.Sau_HomologadorAdministradora", new[] { "AdministradoraId" });
            DropIndex("dbo.Sau_GerenteAdministradora", new[] { "PessoaJuridica_Id" });
            DropIndex("dbo.Sau_GerenteAdministradora", new[] { "PessoaFisica_Id" });
            DropIndex("dbo.Sau_GerenteAdministradora", new[] { "PessoaId" });
            DropIndex("dbo.Sau_GerenteAdministradora", new[] { "AdministradoraId" });
            DropTable("dbo.Sau_SupervisorAdministradora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupervisorAdministradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_HomologadorAdministradora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_HomologadorAdministradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_GerenteAdministradora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GerenteAdministradora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
