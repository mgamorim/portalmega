namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedProposta : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ChancelaId", "dbo.Sau_Chancela");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ChancelaId" });
            AddColumn("dbo.Sau_PropostaDeContratacao", "AssociacaoId", c => c.Int());
            CreateIndex("dbo.Sau_PropostaDeContratacao", "AssociacaoId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "AssociacaoId", "dbo.Sau_Associacao", "Id");
            DropColumn("dbo.Sau_PropostaDeContratacao", "ChancelaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "ChancelaId", c => c.Int());
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "AssociacaoId", "dbo.Sau_Associacao");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "AssociacaoId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "AssociacaoId");
            CreateIndex("dbo.Sau_PropostaDeContratacao", "ChancelaId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "ChancelaId", "dbo.Sau_Chancela", "Id");
        }
    }
}
