namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSnapshot_OrdenacaoDeclaracaoDeSaude : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_SnapshotPropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PropostaDeContratacaoId = c.Int(nullable: false),
                        Json = c.String(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_PropostaDeContratacao", t => t.PropostaDeContratacaoId)
                .Index(t => t.PropostaDeContratacaoId);
            
            AddColumn("dbo.Sau_ItemDeDeclaracaoDoBeneficiario", "Ordenacao", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_ItemDeDeclaracaoDeSaude", "Ordenacao", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_SnapshotPropostaDeContratacao", "PropostaDeContratacaoId", "dbo.Sau_PropostaDeContratacao");
            DropIndex("dbo.Sau_SnapshotPropostaDeContratacao", new[] { "PropostaDeContratacaoId" });
            DropColumn("dbo.Sau_ItemDeDeclaracaoDeSaude", "Ordenacao");
            DropColumn("dbo.Sau_ItemDeDeclaracaoDoBeneficiario", "Ordenacao");
            DropTable("dbo.Sau_SnapshotPropostaDeContratacao");
        }
    }
}
