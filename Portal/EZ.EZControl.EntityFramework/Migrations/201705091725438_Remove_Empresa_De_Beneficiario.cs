namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Empresa_De_Beneficiario : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_BeneficiarioBase", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Sau_ProponenteTitular",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Responsavel_Id = c.Int(),
                        StatusBeneficiario = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProponenteTitular_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_Dependente",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GrauDeParentesco = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Dependente_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_BeneficiarioBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoDeBeneficiario = c.Int(nullable: false),
                        NomeDaMae = c.String(),
                        NumeroDoCartaoNacionalDeSaude = c.String(),
                        DeclaracaoDeNascidoVivo = c.String(),
                        CorretoraId = c.Int(nullable: false),
                        ProfissaoId = c.Int(),
                        EstadoId = c.Int(nullable: false),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_BeneficiarioBase_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Sau_BeneficiarioBase", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_BeneficiarioBase", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Sau_BeneficiarioBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoDeBeneficiario = c.Int(nullable: false),
                        NomeDaMae = c.String(),
                        NumeroDoCartaoNacionalDeSaude = c.String(),
                        DeclaracaoDeNascidoVivo = c.String(),
                        CorretoraId = c.Int(nullable: false),
                        ProfissaoId = c.Int(),
                        EstadoId = c.Int(nullable: false),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_BeneficiarioBase_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_Dependente",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GrauDeParentesco = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Dependente_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_ProponenteTitular",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Responsavel_Id = c.Int(),
                        StatusBeneficiario = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProponenteTitular_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Sau_BeneficiarioBase", "EmpresaId");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
