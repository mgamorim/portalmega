namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Permissao_De_Venda_De_PlanoDeSaude_Para_Corretora : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false),
                        AdministradoraId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PermissaoDeVendaDePlanoDeSaudeParaCorretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.AdministradoraId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora",
                c => new
                    {
                        PermissaoDeVendaDePlanoDeSaudeParaCorretoraId = c.Int(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PermissaoDeVendaDePlanoDeSaudeParaCorretoraId, t.CorretoraId })
                .ForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora", t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretoraId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId, cascadeDelete: true)
                .Index(t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretoraId)
                .Index(t => t.CorretoraId);
            
            CreateTable(
                "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude",
                c => new
                    {
                        PermissaoDeVendaDePlanoDeSaudeParaCorretoraId = c.Int(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PermissaoDeVendaDePlanoDeSaudeParaCorretoraId, t.ProdutoDePlanoDeSaudeId })
                .ForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora", t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretoraId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .Index(t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretoraId)
                .Index(t => t.ProdutoDePlanoDeSaudeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude", "PermissaoDeVendaDePlanoDeSaudeParaCorretoraId", "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora", "PermissaoDeVendaDePlanoDeSaudeParaCorretoraId", "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora", "AdministradoraId", "dbo.Sau_Administradora");
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude", new[] { "PermissaoDeVendaDePlanoDeSaudeParaCorretoraId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora", new[] { "PermissaoDeVendaDePlanoDeSaudeParaCorretoraId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora", new[] { "AdministradoraId" });
            DropTable("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude");
            DropTable("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora");
            DropTable("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PermissaoDeVendaDePlanoDeSaudeParaCorretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
