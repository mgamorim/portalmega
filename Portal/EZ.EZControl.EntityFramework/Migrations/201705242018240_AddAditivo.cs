namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddAditivo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_Aditivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        EZLivReport = c.Int(nullable: false),
                        TipoDeAditivo = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Aditivo_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Aditivo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EmpresaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_Aditivo", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Aditivo", new[] { "ProdutoDePlanoDeSaudeId" });
            DropTable("dbo.Sau_Aditivo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Aditivo_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Aditivo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
