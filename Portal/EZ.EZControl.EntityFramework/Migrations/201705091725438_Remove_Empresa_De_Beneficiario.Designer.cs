// <auto-generated />
namespace EZ.EZControl.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Remove_Empresa_De_Beneficiario : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Remove_Empresa_De_Beneficiario));
        
        string IMigrationMetadata.Id
        {
            get { return "201705091725438_Remove_Empresa_De_Beneficiario"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
