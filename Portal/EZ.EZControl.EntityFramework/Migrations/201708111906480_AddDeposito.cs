namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeposito : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pag_Deposito",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PedidoId = c.Int(nullable: false),
                        Comprovante = c.String(),
                        UsuarioDeclaranteId = c.Long(nullable: false),
                        Recebido = c.Boolean(nullable: false),
                        DataHoraDeclaracao = c.DateTime(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Deposito_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Deposito_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Vnd_Pedido", t => t.PedidoId)
                .ForeignKey("dbo.AbpUsers", t => t.UsuarioDeclaranteId)
                .Index(t => t.PedidoId)
                .Index(t => t.UsuarioDeclaranteId)
                .Index(t => t.EmpresaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pag_Deposito", "UsuarioDeclaranteId", "dbo.AbpUsers");
            DropForeignKey("dbo.Pag_Deposito", "PedidoId", "dbo.Vnd_Pedido");
            DropForeignKey("dbo.Pag_Deposito", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Pag_Deposito", new[] { "EmpresaId" });
            DropIndex("dbo.Pag_Deposito", new[] { "UsuarioDeclaranteId" });
            DropIndex("dbo.Pag_Deposito", new[] { "PedidoId" });
            DropTable("dbo.Pag_Deposito",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Deposito_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Deposito_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
