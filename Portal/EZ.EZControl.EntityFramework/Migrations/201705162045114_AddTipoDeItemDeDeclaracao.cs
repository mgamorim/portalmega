namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTipoDeItemDeDeclaracao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_ItemDeDeclaracaoDeSaude", "TipoDeItemDeDeclaracao", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "AnoDoEvento", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "AnoDoEvento");
            DropColumn("dbo.Sau_ItemDeDeclaracaoDeSaude", "TipoDeItemDeDeclaracao");
        }
    }
}
