namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Table_DepositoBancario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
            "dbo.Pag_DepositoTransferenciaBancaria",
            c => new
            {
                Id = c.Int(nullable: false, identity: true),
                Banco = c.String(nullable: true, maxLength: 100, unicode: false),
                Agencia = c.String(nullable: true, maxLength: 100, unicode: false),
                Conta = c.String(nullable: true, maxLength: 100, unicode: false),
                Titular = c.String(nullable: true, maxLength: 100, unicode: false),
                Comprovante = c.String(nullable: true, maxLength: 100, unicode: false),
                Descricao = c.String(maxLength: 1024, unicode: false),
                PessoaJuridicaId = c.Int(nullable: false),
                IsActive = c.Boolean(nullable: false),
                TenantId = c.Int(nullable: false),
                EmpresaId = c.Int(nullable: false),
                ExternalId = c.Int(nullable: false),
                DateOfEditionIntegration = c.DateTime(),
            })
            .PrimaryKey(t => t.Id)
            .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: false)
            .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridicaId, cascadeDelete: false)
            .Index(t => t.Id)
            .Index(t => t.PessoaJuridicaId)
            .Index(t => t.EmpresaId);
        }

        public override void Down()
        {
            DropTable("dbo.Pag_DepositoTransferenciaBancaria");
        }
    }
}
