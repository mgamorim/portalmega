namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Slug_Corretora_Empresa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Glb_Empresa", "Slug", c => c.String(maxLength: 20));
            DropColumn("dbo.Sau_Corretora", "Slug");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_Corretora", "Slug", c => c.String(maxLength: 20));
            DropColumn("dbo.Glb_Empresa", "Slug");
        }
    }
}
