namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_ProdutoDePlanoDeSaude_Carencias : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "CarenciaId", "dbo.Sau_Carencia");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", new[] { "CarenciaId" });
            AddColumn("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id", c => c.Int());
            CreateIndex("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id");
            AddForeignKey("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Carencia");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Carencia",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        CarenciaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.CarenciaId });
            
            DropForeignKey("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropIndex("dbo.Sau_Carencia", new[] { "ProdutoDePlanoDeSaude_Id" });
            DropColumn("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id");
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "CarenciaId");
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "ProdutoDePlanoDeSaudeId");
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "CarenciaId", "dbo.Sau_Carencia", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id", cascadeDelete: true);
        }
    }
}
