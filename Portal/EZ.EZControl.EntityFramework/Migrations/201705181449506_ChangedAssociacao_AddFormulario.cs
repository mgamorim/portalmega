namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedAssociacao_AddFormulario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_Associacao", "Formulario", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_Associacao", "Formulario");
        }
    }
}
