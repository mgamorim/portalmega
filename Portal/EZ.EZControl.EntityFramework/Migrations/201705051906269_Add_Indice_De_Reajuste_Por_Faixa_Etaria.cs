namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Indice_De_Reajuste_Por_Faixa_Etaria : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_IndiceDeReajustePorFaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        IndiceDeReajuste = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FaixaEtariaId = c.Int(nullable: false),
                        OperadoraId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IndiceDeReajustePorFaixaEtaria_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IndiceDeReajustePorFaixaEtaria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_FaixaEtaria", t => t.FaixaEtariaId)
                .ForeignKey("dbo.Sau_Operadora", t => t.OperadoraId)
                .Index(t => t.FaixaEtariaId)
                .Index(t => t.OperadoraId)
                .Index(t => t.EmpresaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_IndiceDeReajustePorFaixaEtaria", "OperadoraId", "dbo.Sau_Operadora");
            DropForeignKey("dbo.Sau_IndiceDeReajustePorFaixaEtaria", "FaixaEtariaId", "dbo.Sau_FaixaEtaria");
            DropForeignKey("dbo.Sau_IndiceDeReajustePorFaixaEtaria", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_IndiceDeReajustePorFaixaEtaria", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_IndiceDeReajustePorFaixaEtaria", new[] { "OperadoraId" });
            DropIndex("dbo.Sau_IndiceDeReajustePorFaixaEtaria", new[] { "FaixaEtariaId" });
            DropTable("dbo.Sau_IndiceDeReajustePorFaixaEtaria",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_IndiceDeReajustePorFaixaEtaria_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_IndiceDeReajustePorFaixaEtaria_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
