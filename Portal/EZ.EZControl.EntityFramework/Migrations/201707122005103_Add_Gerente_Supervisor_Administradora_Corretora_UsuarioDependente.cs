namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Gerente_Supervisor_Administradora_Corretora_UsuarioDependente : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_GerenteCorretora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        PessoaFisica_Id = c.Int(),
                        PessoaJuridica_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GerenteCorretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisica_Id)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridica_Id)
                .Index(t => t.CorretoraId)
                .Index(t => t.PessoaId)
                .Index(t => t.PessoaFisica_Id)
                .Index(t => t.PessoaJuridica_Id);
            
            CreateTable(
                "dbo.Sau_SupervisorCorretora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                        PessoaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        PessoaFisica_Id = c.Int(),
                        PessoaJuridica_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupervisorCorretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Pessoa", t => t.PessoaId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisica_Id)
                .ForeignKey("dbo.Glb_PessoaJuridica", t => t.PessoaJuridica_Id)
                .Index(t => t.CorretoraId)
                .Index(t => t.PessoaId)
                .Index(t => t.PessoaFisica_Id)
                .Index(t => t.PessoaJuridica_Id);
            
            CreateTable(
                "dbo.Sau_PropostaDeContratacao_UserDependente",
                c => new
                    {
                        PropostaDeContratacaoId = c.Int(nullable: false),
                        UserId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.PropostaDeContratacaoId, t.UserId })
                .ForeignKey("dbo.Sau_PropostaDeContratacao", t => t.PropostaDeContratacaoId, cascadeDelete: true)
                .ForeignKey("dbo.AbpUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.PropostaDeContratacaoId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_PropostaDeContratacao_UserDependente", "UserId", "dbo.AbpUsers");
            DropForeignKey("dbo.Sau_PropostaDeContratacao_UserDependente", "PropostaDeContratacaoId", "dbo.Sau_PropostaDeContratacao");
            DropForeignKey("dbo.Sau_SupervisorCorretora", "PessoaJuridica_Id", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_SupervisorCorretora", "PessoaFisica_Id", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_SupervisorCorretora", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Sau_SupervisorCorretora", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_GerenteCorretora", "PessoaJuridica_Id", "dbo.Glb_PessoaJuridica");
            DropForeignKey("dbo.Sau_GerenteCorretora", "PessoaFisica_Id", "dbo.Glb_PessoaFisica");
            DropForeignKey("dbo.Sau_GerenteCorretora", "PessoaId", "dbo.Glb_Pessoa");
            DropForeignKey("dbo.Sau_GerenteCorretora", "CorretoraId", "dbo.Sau_Corretora");
            DropIndex("dbo.Sau_PropostaDeContratacao_UserDependente", new[] { "UserId" });
            DropIndex("dbo.Sau_PropostaDeContratacao_UserDependente", new[] { "PropostaDeContratacaoId" });
            DropIndex("dbo.Sau_SupervisorCorretora", new[] { "PessoaJuridica_Id" });
            DropIndex("dbo.Sau_SupervisorCorretora", new[] { "PessoaFisica_Id" });
            DropIndex("dbo.Sau_SupervisorCorretora", new[] { "PessoaId" });
            DropIndex("dbo.Sau_SupervisorCorretora", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_GerenteCorretora", new[] { "PessoaJuridica_Id" });
            DropIndex("dbo.Sau_GerenteCorretora", new[] { "PessoaFisica_Id" });
            DropIndex("dbo.Sau_GerenteCorretora", new[] { "PessoaId" });
            DropIndex("dbo.Sau_GerenteCorretora", new[] { "CorretoraId" });
            DropTable("dbo.Sau_PropostaDeContratacao_UserDependente");
            DropTable("dbo.Sau_SupervisorCorretora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_SupervisorCorretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_GerenteCorretora",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_GerenteCorretora_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
