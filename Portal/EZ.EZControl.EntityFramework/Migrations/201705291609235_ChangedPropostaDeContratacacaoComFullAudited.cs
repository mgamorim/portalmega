namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedPropostaDeContratacacaoComFullAudited : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        TitularId = c.Int(nullable: false),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        Aceite = c.Boolean(nullable: false),
                        TipoDeHomologacao = c.Int(nullable: false),
                        DataHoraDaHomologacao = c.DateTime(),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        UltimoContratoAceitoId = c.Int(),
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        AssociacaoId = c.Int(),
                        InicioDeVigencia = c.DateTime(),
                        TipoDeProposta = c.Int(nullable: false),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PropostaDeContratacao_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Sau_PropostaDeContratacao", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sau_PropostaDeContratacao", "DeleterUserId", c => c.Long());
            AddColumn("dbo.Sau_PropostaDeContratacao", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.Sau_PropostaDeContratacao", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.Sau_PropostaDeContratacao", "LastModifierUserId", c => c.Long());
            AddColumn("dbo.Sau_PropostaDeContratacao", "CreationTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.Sau_PropostaDeContratacao", "CreatorUserId", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_PropostaDeContratacao", "CreatorUserId");
            DropColumn("dbo.Sau_PropostaDeContratacao", "CreationTime");
            DropColumn("dbo.Sau_PropostaDeContratacao", "LastModifierUserId");
            DropColumn("dbo.Sau_PropostaDeContratacao", "LastModificationTime");
            DropColumn("dbo.Sau_PropostaDeContratacao", "DeletionTime");
            DropColumn("dbo.Sau_PropostaDeContratacao", "DeleterUserId");
            DropColumn("dbo.Sau_PropostaDeContratacao", "IsDeleted");
            AlterTableAnnotations(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        TitularId = c.Int(nullable: false),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        Aceite = c.Boolean(nullable: false),
                        TipoDeHomologacao = c.Int(nullable: false),
                        DataHoraDaHomologacao = c.DateTime(),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        UltimoContratoAceitoId = c.Int(),
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        AssociacaoId = c.Int(),
                        InicioDeVigencia = c.DateTime(),
                        TipoDeProposta = c.Int(nullable: false),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PropostaDeContratacao_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
