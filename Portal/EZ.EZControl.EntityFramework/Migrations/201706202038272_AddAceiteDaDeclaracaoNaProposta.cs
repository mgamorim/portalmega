namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAceiteDaDeclaracaoNaProposta : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "AceiteDaDeclaracaoDeSaude", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_PropostaDeContratacao", "AceiteDaDeclaracaoDeSaude");
        }
    }
}
