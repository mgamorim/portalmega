namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePropostaEProdutoDePlanoAddAditivos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_PropostaDeContratacao_Aditivo",
                c => new
                    {
                        PropostaDeContratacaoId = c.Int(nullable: false),
                        AditivoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PropostaDeContratacaoId, t.AditivoId })
                .ForeignKey("dbo.Sau_PropostaDeContratacao", t => t.PropostaDeContratacaoId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Relatorio", t => t.AditivoId, cascadeDelete: true)
                .Index(t => t.PropostaDeContratacaoId)
                .Index(t => t.AditivoId);
            
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Aditivo",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        AditivoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.AditivoId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Glb_Relatorio", t => t.AditivoId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.AditivoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "AditivoId", "dbo.Glb_Relatorio");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_PropostaDeContratacao_Aditivo", "AditivoId", "dbo.Glb_Relatorio");
            DropForeignKey("dbo.Sau_PropostaDeContratacao_Aditivo", "PropostaDeContratacaoId", "dbo.Sau_PropostaDeContratacao");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", new[] { "AditivoId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_PropostaDeContratacao_Aditivo", new[] { "AditivoId" });
            DropIndex("dbo.Sau_PropostaDeContratacao_Aditivo", new[] { "PropostaDeContratacaoId" });
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo");
            DropTable("dbo.Sau_PropostaDeContratacao_Aditivo");
        }
    }
}
