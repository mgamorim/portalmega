namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEmpresaLocalidades : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Glb_Cidade", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Estado", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_Pais", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Glb_TipoDeLogradouro", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Glb_Cidade", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Estado", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_Pais", new[] { "EmpresaId" });
            DropIndex("dbo.Glb_TipoDeLogradouro", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Glb_Cidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        CodigoIBGE = c.String(maxLength: 20),
                        EstadoId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Cidade_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_Estado",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Sigla = c.String(nullable: false, maxLength: 2),
                        Capital = c.String(nullable: false, maxLength: 60),
                        CodigoIBGE = c.String(maxLength: 3),
                        PaisId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Estado_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_Pais",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Nacionalidade = c.String(nullable: false, maxLength: 40),
                        CodigoReceitaFederal = c.String(maxLength: 15),
                        CodigoInternacional = c.String(maxLength: 15),
                        CodigoISO = c.String(nullable: false, maxLength: 15),
                        MascaraDoCodigoPostal = c.String(maxLength: 15),
                        Sigla2Caracteres = c.String(maxLength: 2),
                        Sigla3Caracteres = c.String(maxLength: 3),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Pais_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_TipoDeLogradouro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TipoDeLogradouro_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Glb_Cidade", "EmpresaId");
            DropColumn("dbo.Glb_Estado", "EmpresaId");
            DropColumn("dbo.Glb_Pais", "EmpresaId");
            DropColumn("dbo.Glb_TipoDeLogradouro", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Glb_TipoDeLogradouro", "EmpresaId", c => c.Int(nullable: false));
            AddColumn("dbo.Glb_Pais", "EmpresaId", c => c.Int(nullable: false));
            AddColumn("dbo.Glb_Estado", "EmpresaId", c => c.Int(nullable: false));
            AddColumn("dbo.Glb_Cidade", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Glb_TipoDeLogradouro",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 60),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TipoDeLogradouro_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_Pais",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Nacionalidade = c.String(nullable: false, maxLength: 40),
                        CodigoReceitaFederal = c.String(maxLength: 15),
                        CodigoInternacional = c.String(maxLength: 15),
                        CodigoISO = c.String(nullable: false, maxLength: 15),
                        MascaraDoCodigoPostal = c.String(maxLength: 15),
                        Sigla2Caracteres = c.String(maxLength: 2),
                        Sigla3Caracteres = c.String(maxLength: 3),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Pais_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_Estado",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Sigla = c.String(nullable: false, maxLength: 2),
                        Capital = c.String(nullable: false, maxLength: 60),
                        CodigoIBGE = c.String(maxLength: 3),
                        PaisId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Estado_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_Cidade",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        CodigoIBGE = c.String(maxLength: 20),
                        EstadoId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Cidade_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Glb_TipoDeLogradouro", "EmpresaId");
            CreateIndex("dbo.Glb_Pais", "EmpresaId");
            CreateIndex("dbo.Glb_Estado", "EmpresaId");
            CreateIndex("dbo.Glb_Cidade", "EmpresaId");
            AddForeignKey("dbo.Glb_TipoDeLogradouro", "EmpresaId", "dbo.Glb_Empresa", "Id");
            AddForeignKey("dbo.Glb_Pais", "EmpresaId", "dbo.Glb_Empresa", "Id");
            AddForeignKey("dbo.Glb_Estado", "EmpresaId", "dbo.Glb_Empresa", "Id");
            AddForeignKey("dbo.Glb_Cidade", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
