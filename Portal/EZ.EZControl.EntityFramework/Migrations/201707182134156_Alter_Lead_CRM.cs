namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Lead_CRM : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Sau_Lead", newName: "Crm_Lead");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Crm_Lead", newName: "Sau_Lead");
        }
    }
}
