namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelatorio : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Glb_Relatorio",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 60),
                        Sistema = c.Int(nullable: false),
                        TipoDeRelatorio = c.Int(nullable: false),
                        Conteudo = c.String(nullable: false, storeType: "ntext"),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Relatorio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Relatorio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Relatorio_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.EmpresaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Glb_Relatorio", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Glb_Relatorio", new[] { "EmpresaId" });
            DropTable("dbo.Glb_Relatorio",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Relatorio_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Relatorio_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Relatorio_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
