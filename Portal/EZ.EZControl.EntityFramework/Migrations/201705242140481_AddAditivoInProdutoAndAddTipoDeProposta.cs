namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAditivoInProdutoAndAddTipoDeProposta : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Aditivo",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        AditivoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.AditivoId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Aditivo", t => t.AditivoId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.AditivoId);
            
            AddColumn("dbo.Sau_PropostaDeContratacao", "TipoDeProposta", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "AditivoId", "dbo.Sau_Aditivo");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", new[] { "AditivoId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo", new[] { "ProdutoDePlanoDeSaudeId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "TipoDeProposta");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Aditivo");
        }
    }
}
