namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedFaixaEtaria : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_FaixaEtaria", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_FaixaEtaria", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Sau_FaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false),
                        IdadeInicial = c.Int(nullable: false),
                        IdadeFinal = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_FaixaEtaria_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Sau_FaixaEtaria", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_FaixaEtaria", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Sau_FaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false),
                        IdadeInicial = c.Int(nullable: false),
                        IdadeFinal = c.Int(),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_FaixaEtaria_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Sau_FaixaEtaria", "EmpresaId");
            AddForeignKey("dbo.Sau_FaixaEtaria", "EmpresaId", "dbo.Glb_Empresa", "Id", cascadeDelete: true);
        }
    }
}
