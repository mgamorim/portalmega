namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedSnapshot : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_SnapshotPropostaDeContratacao", "JsonTmp", c => c.Binary(nullable: false));
            Sql("Update dbo.Sau_SnapshotPropostaDeContratacao SET JsonTmp = Convert(varbinary, Json)");
            DropColumn("dbo.Sau_SnapshotPropostaDeContratacao", "Json");
            RenameColumn("dbo.Sau_SnapshotPropostaDeContratacao", "JsonTmp", "Json");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_SnapshotPropostaDeContratacao", "JsonTmp", c => c.Binary(nullable: false));
            Sql("Update dbo.Sau_SnapshotPropostaDeContratacao SET JsonTmp = Convert(nvarchar, Json)");
            DropColumn("dbo.Sau_SnapshotPropostaDeContratacao", "Json");
            RenameColumn("dbo.Sau_SnapshotPropostaDeContratacao", "JsonTmp", "Json");
        }
    }
}
