namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedArquivo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cor_Arquivo", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Cor_Arquivo", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Cor_Arquivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Conteudo = c.Binary(),
                        Path = c.String(nullable: false),
                        TipoDeArquivoFixo = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Token = c.String(nullable: false, maxLength: 32),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoBase_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Cms_ArquivoCMS",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoCMS_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_ArquivoDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        EmExigencia = c.Boolean(nullable: false),
                        Motivo = c.String(),
                        PropostaDeContratacaoId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoDocumento_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Glb_ArquivoGlobal",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoGlobal_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Cor_Arquivo", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cor_Arquivo", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Glb_ArquivoGlobal",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoGlobal_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_ArquivoDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        EmExigencia = c.Boolean(nullable: false),
                        Motivo = c.String(),
                        PropostaDeContratacaoId = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoDocumento_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Cms_ArquivoCMS",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoCMS_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Cor_Arquivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Conteudo = c.Binary(),
                        Path = c.String(nullable: false),
                        TipoDeArquivoFixo = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Token = c.String(nullable: false, maxLength: 32),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ArquivoBase_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Cor_Arquivo", "EmpresaId");
            AddForeignKey("dbo.Cor_Arquivo", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
