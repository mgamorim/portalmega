namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedChancela : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_Chancela", "AdministradoraId", c => c.Int(nullable: true));
            AddColumn("dbo.Sau_Chancela", "AssociacaoId", c => c.Int(nullable: true));
            CreateIndex("dbo.Sau_Chancela", "AdministradoraId");
            CreateIndex("dbo.Sau_Chancela", "AssociacaoId");
            //Sql("UPDATE dbo.Sau_Chancela SET AdministradoraId = 1"); // FAZER MANUALMENTE
            //Sql("UPDATE dbo.Sau_Chancela SET AssociacaoId = 1"); // FAZER MANUALMENTE
            AddForeignKey("dbo.Sau_Chancela", "AdministradoraId", "dbo.Sau_Administradora", "Id");
            AddForeignKey("dbo.Sau_Chancela", "AssociacaoId", "dbo.Sau_Associacao", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_Chancela", "AssociacaoId", "dbo.Sau_Associacao");
            DropForeignKey("dbo.Sau_Chancela", "AdministradoraId", "dbo.Sau_Administradora");
            DropIndex("dbo.Sau_Chancela", new[] { "AssociacaoId" });
            DropIndex("dbo.Sau_Chancela", new[] { "AdministradoraId" });
            DropColumn("dbo.Sau_Chancela", "AssociacaoId");
            DropColumn("dbo.Sau_Chancela", "AdministradoraId");
        }
    }
}
