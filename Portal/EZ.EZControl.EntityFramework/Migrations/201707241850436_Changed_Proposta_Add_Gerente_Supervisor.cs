namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_Proposta_Add_Gerente_Supervisor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "GerenteId", c => c.Int());
            AddColumn("dbo.Sau_PropostaDeContratacao", "SupervisorId", c => c.Int());
            CreateIndex("dbo.Sau_PropostaDeContratacao", "GerenteId");
            CreateIndex("dbo.Sau_PropostaDeContratacao", "SupervisorId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "GerenteId", "dbo.Sau_GerenteCorretora", "Id");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "SupervisorId", "dbo.Sau_SupervisorCorretora", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "SupervisorId", "dbo.Sau_SupervisorCorretora");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "GerenteId", "dbo.Sau_GerenteCorretora");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "SupervisorId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "GerenteId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "SupervisorId");
            DropColumn("dbo.Sau_PropostaDeContratacao", "GerenteId");
        }
    }
}
