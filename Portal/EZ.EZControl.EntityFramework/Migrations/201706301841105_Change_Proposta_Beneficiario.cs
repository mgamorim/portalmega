namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Proposta_Beneficiario : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "EstadoId", "dbo.Glb_Estado");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "ProfissaoId", "dbo.Glb_Profissao");
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "ProfissaoId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "EstadoId" });
            AlterTableAnnotations(
                "dbo.Sau_ProponenteTitular",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Responsavel_Id = c.Int(),
                        StatusBeneficiario = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProponenteTitular_CorretoraFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_Dependente",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GrauDeParentesco = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Dependente_CorretoraFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_BeneficiarioBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoDeBeneficiario = c.Int(nullable: false),
                        NomeDaMae = c.String(),
                        NumeroDoCartaoNacionalDeSaude = c.String(),
                        DeclaracaoDeNascidoVivo = c.String(),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_BeneficiarioBase_CorretoraFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AddColumn("dbo.Sau_PropostaDeContratacao", "ProfissaoId", c => c.Int());
            AddColumn("dbo.Sau_PropostaDeContratacao", "EstadoId", c => c.Int());
            CreateIndex("dbo.Sau_PropostaDeContratacao", "ProfissaoId");
            CreateIndex("dbo.Sau_PropostaDeContratacao", "EstadoId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "EstadoId", "dbo.Glb_Estado", "Id");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "ProfissaoId", "dbo.Glb_Profissao", "Id");
            DropColumn("dbo.Sau_BeneficiarioBase", "CorretoraId");
            DropColumn("dbo.Sau_BeneficiarioBase", "ProfissaoId");
            DropColumn("dbo.Sau_BeneficiarioBase", "EstadoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_BeneficiarioBase", "EstadoId", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_BeneficiarioBase", "ProfissaoId", c => c.Int());
            AddColumn("dbo.Sau_BeneficiarioBase", "CorretoraId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ProfissaoId", "dbo.Glb_Profissao");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "EstadoId", "dbo.Glb_Estado");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "EstadoId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ProfissaoId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "EstadoId");
            DropColumn("dbo.Sau_PropostaDeContratacao", "ProfissaoId");
            AlterTableAnnotations(
                "dbo.Sau_BeneficiarioBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoDeBeneficiario = c.Int(nullable: false),
                        NomeDaMae = c.String(),
                        NumeroDoCartaoNacionalDeSaude = c.String(),
                        DeclaracaoDeNascidoVivo = c.String(),
                        PessoaFisicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_BeneficiarioBase_CorretoraFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_Dependente",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        GrauDeParentesco = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Dependente_CorretoraFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_ProponenteTitular",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Responsavel_Id = c.Int(),
                        StatusBeneficiario = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProponenteTitular_CorretoraFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Sau_BeneficiarioBase", "EstadoId");
            CreateIndex("dbo.Sau_BeneficiarioBase", "ProfissaoId");
            CreateIndex("dbo.Sau_BeneficiarioBase", "CorretoraId");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "ProfissaoId", "dbo.Glb_Profissao", "Id");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "EstadoId", "dbo.Glb_Estado", "Id");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora", "Id");
        }
    }
}
