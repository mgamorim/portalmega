namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedCorretoraAddImagem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_Corretora", "ImagemId", c => c.Int());
            CreateIndex("dbo.Sau_Corretora", "ImagemId");
            AddForeignKey("dbo.Sau_Corretora", "ImagemId", "dbo.Cor_Arquivo", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_Corretora", "ImagemId", "dbo.Cor_Arquivo");
            DropIndex("dbo.Sau_Corretora", new[] { "ImagemId" });
            DropColumn("dbo.Sau_Corretora", "ImagemId");
        }
    }
}
