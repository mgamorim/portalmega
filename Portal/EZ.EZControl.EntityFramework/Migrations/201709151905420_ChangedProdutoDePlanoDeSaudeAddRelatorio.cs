namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedProdutoDePlanoDeSaudeAddRelatorio : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioFichaDeEntidadeId", c => c.Int());
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioProspostaDeContratacaoId", c => c.Int());
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioFichaDeEntidadeId");
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioProspostaDeContratacaoId");
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioFichaDeEntidadeId", "dbo.Glb_Relatorio", "Id");
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioProspostaDeContratacaoId", "dbo.Glb_Relatorio", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioProspostaDeContratacaoId", "dbo.Glb_Relatorio");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioFichaDeEntidadeId", "dbo.Glb_Relatorio");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "RelatorioProspostaDeContratacaoId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "RelatorioFichaDeEntidadeId" });
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioProspostaDeContratacaoId");
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "RelatorioFichaDeEntidadeId");
        }
    }
}
