namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_DeclaracaoDoBeneficiario_And_ItemDeDeclaracaoDoBeneficiario : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_ItemDeDeclaracaoDoBeneficiario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Pergunta = c.String(nullable: false, maxLength: 500),
                        Ajuda = c.String(),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeDeclaracaoDoBeneficiario_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeDeclaracaoDoBeneficiario_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sau_DeclaracaoDoBeneficiario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeclaracaoDoBeneficiario_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeclaracaoDoBeneficiario_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario",
                c => new
                    {
                        DeclaracaoDoBeneficiarioId = c.Int(nullable: false),
                        ItemDeDeclaracaoDoBeneficiarioId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DeclaracaoDoBeneficiarioId, t.ItemDeDeclaracaoDoBeneficiarioId })
                .ForeignKey("dbo.Sau_DeclaracaoDoBeneficiario", t => t.DeclaracaoDoBeneficiarioId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ItemDeDeclaracaoDoBeneficiario", t => t.ItemDeDeclaracaoDoBeneficiarioId, cascadeDelete: true)
                .Index(t => t.DeclaracaoDoBeneficiarioId)
                .Index(t => t.ItemDeDeclaracaoDoBeneficiarioId);
            
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "DeclaracaoDoBeneficiarioId", c => c.Int());
            AddColumn("dbo.Sau_PropostaDeContratacao", "ItemDeDeclaracaoDoBeneficiarioId", c => c.Int());
            CreateIndex("dbo.Sau_PropostaDeContratacao", "ItemDeDeclaracaoDoBeneficiarioId");
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude", "DeclaracaoDoBeneficiarioId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "ItemDeDeclaracaoDoBeneficiarioId", "dbo.Sau_ItemDeDeclaracaoDoBeneficiario", "Id");
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "DeclaracaoDoBeneficiarioId", "dbo.Sau_DeclaracaoDoBeneficiario", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "DeclaracaoDoBeneficiarioId", "dbo.Sau_DeclaracaoDoBeneficiario");
            DropForeignKey("dbo.Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario", "ItemDeDeclaracaoDoBeneficiarioId", "dbo.Sau_ItemDeDeclaracaoDoBeneficiario");
            DropForeignKey("dbo.Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario", "DeclaracaoDoBeneficiarioId", "dbo.Sau_DeclaracaoDoBeneficiario");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "ItemDeDeclaracaoDoBeneficiarioId", "dbo.Sau_ItemDeDeclaracaoDoBeneficiario");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "DeclaracaoDoBeneficiarioId" });
            DropIndex("dbo.Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario", new[] { "ItemDeDeclaracaoDoBeneficiarioId" });
            DropIndex("dbo.Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario", new[] { "DeclaracaoDoBeneficiarioId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "ItemDeDeclaracaoDoBeneficiarioId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "ItemDeDeclaracaoDoBeneficiarioId");
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "DeclaracaoDoBeneficiarioId");
            DropTable("dbo.Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario");
            DropTable("dbo.Sau_DeclaracaoDoBeneficiario",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_DeclaracaoDoBeneficiario_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_DeclaracaoDoBeneficiario_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_ItemDeDeclaracaoDoBeneficiario",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ItemDeDeclaracaoDoBeneficiario_MayHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_ItemDeDeclaracaoDoBeneficiario_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
