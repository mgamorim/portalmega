namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_Beneficiario_FaixaEtaria_Add_PermissaoVendaParaCorretor : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PermissaoDeVendaDePlanoDeSaudeParaCorretor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .Index(t => t.CorretoraId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor",
                c => new
                    {
                        PermissaoDeVendaDePlanoDeSaudeParaCorretorId = c.Int(nullable: false),
                        CorretorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PermissaoDeVendaDePlanoDeSaudeParaCorretorId, t.CorretorId })
                .ForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor", t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretorId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Corretor", t => t.CorretorId, cascadeDelete: true)
                .Index(t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretorId)
                .Index(t => t.CorretorId);
            
            CreateTable(
                "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude",
                c => new
                    {
                        PermissaoDeVendaDePlanoDeSaudeParaCorretorId = c.Int(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PermissaoDeVendaDePlanoDeSaudeParaCorretorId, t.ProdutoDePlanoDeSaudeId })
                .ForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor", t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretorId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .Index(t => t.PermissaoDeVendaDePlanoDeSaudeParaCorretorId)
                .Index(t => t.ProdutoDePlanoDeSaudeId);
            
            AlterTableAnnotations(
                "dbo.Sau_ProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PlanoDeSaudeId = c.Int(nullable: false),
                        AdministradoraId = c.Int(),
                        OperadoraId = c.Int(nullable: false),
                        CarenciaEspecial = c.Boolean(nullable: false),
                        DescricaoDaCarenciaEspecial = c.String(),
                        Acompanhante = c.Boolean(nullable: false),
                        DescricaoDoAcompanhante = c.String(),
                        CoberturaExtra = c.Boolean(nullable: false),
                        DescricaoDaCoberturaExtra = c.String(),
                        RedeCredenciadaId = c.Int(),
                        FormaDeContratacao = c.Int(nullable: false),
                        QuestionarioDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        NumeroDeDiasParaEncerrarAsPropostasRelacionadas = c.Int(),
                        Formulario = c.Int(nullable: false),
                        CartaDeOrientacao = c.String(),
                        DeclaracaoDoBeneficiarioId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProdutoDePlanoDeSaude_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_FaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false),
                        IdadeInicial = c.Int(nullable: false),
                        IdadeFinal = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_FaixaEtaria_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Sau_BeneficiarioBase", "AdministradoraId", c => c.Int());
            Sql("Update dbo.Sau_BeneficiarioBase set AdministradoraId = 1");
            AlterColumn("dbo.Sau_BeneficiarioBase", "AdministradoraId", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_BeneficiarioBase", "CorretoraId", c => c.Int());
            Sql("Update dbo.Sau_BeneficiarioBase set CorretoraId = 1");
            AlterColumn("dbo.Sau_BeneficiarioBase", "CorretoraId", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_FaixaEtaria", "EmpresaId", c => c.Int());
            Sql("Update dbo.Sau_FaixaEtaria set EmpresaId = 1");
            AlterColumn("dbo.Sau_FaixaEtaria", "EmpresaId", c => c.Int(nullable: false));
            CreateIndex("dbo.Sau_BeneficiarioBase", "AdministradoraId");
            CreateIndex("dbo.Sau_BeneficiarioBase", "CorretoraId");
            CreateIndex("dbo.Sau_FaixaEtaria", "EmpresaId");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "AdministradoraId", "dbo.Sau_Administradora", "Id");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora", "Id");
            AddForeignKey("dbo.Sau_FaixaEtaria", "EmpresaId", "dbo.Glb_Empresa", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude", "PermissaoDeVendaDePlanoDeSaudeParaCorretorId", "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor", "CorretorId", "dbo.Sau_Corretor");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor", "PermissaoDeVendaDePlanoDeSaudeParaCorretorId", "dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor");
            DropForeignKey("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_FaixaEtaria", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "AdministradoraId", "dbo.Sau_Administradora");
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude", new[] { "PermissaoDeVendaDePlanoDeSaudeParaCorretorId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor", new[] { "CorretorId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor", new[] { "PermissaoDeVendaDePlanoDeSaudeParaCorretorId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_FaixaEtaria", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "AdministradoraId" });
            DropColumn("dbo.Sau_FaixaEtaria", "EmpresaId");
            DropColumn("dbo.Sau_BeneficiarioBase", "CorretoraId");
            DropColumn("dbo.Sau_BeneficiarioBase", "AdministradoraId");
            AlterTableAnnotations(
                "dbo.Sau_FaixaEtaria",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false),
                        IdadeInicial = c.Int(nullable: false),
                        IdadeFinal = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_FaixaEtaria_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_ProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PlanoDeSaudeId = c.Int(nullable: false),
                        AdministradoraId = c.Int(),
                        OperadoraId = c.Int(nullable: false),
                        CarenciaEspecial = c.Boolean(nullable: false),
                        DescricaoDaCarenciaEspecial = c.String(),
                        Acompanhante = c.Boolean(nullable: false),
                        DescricaoDoAcompanhante = c.String(),
                        CoberturaExtra = c.Boolean(nullable: false),
                        DescricaoDaCoberturaExtra = c.String(),
                        RedeCredenciadaId = c.Int(),
                        FormaDeContratacao = c.Int(nullable: false),
                        QuestionarioDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        NumeroDeDiasParaEncerrarAsPropostasRelacionadas = c.Int(),
                        Formulario = c.Int(nullable: false),
                        CartaDeOrientacao = c.String(),
                        DeclaracaoDoBeneficiarioId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProdutoDePlanoDeSaude_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropTable("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude");
            DropTable("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor");
            DropTable("dbo.Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PermissaoDeVendaDePlanoDeSaudeParaCorretor_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
