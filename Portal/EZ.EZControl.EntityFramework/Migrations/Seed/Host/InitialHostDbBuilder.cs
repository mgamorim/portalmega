﻿using EZ.EZControl.EntityFramework;

namespace EZ.EZControl.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly EZControlDbContext _context;

        public InitialHostDbBuilder(EZControlDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
