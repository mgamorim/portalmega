﻿using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EntityFramework;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Migrations.Seed.EZSeeds
{
    public abstract class EzSeedProfileBase
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;

        public EzSeedProfileBase(EZControlDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public abstract void Create();

        protected User GetAdminUser()
        {
            var adminTenatUser = _context
                .Users
                .FirstOrDefault(x => x.TenantId == _tenantId && x.UserName == "admin");

            return adminTenatUser;
        }

        protected Role GetUserRole()
        {
            return _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.User);
        }

        protected Role GetAdminRole()
        {
            return _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
        }

        protected void AssociarUsuarioAEmpresa(User user, ClienteEZ clienteEz, Empresa empresa)
        {
            if (user.Empresas == null)
            {
                user.Empresas = new List<Empresa>();
            }

            if (!user.Empresas.Any(x => x.Id == empresa.Id))
            {
                if (user != null)
                {
                    if (user.Empresas == null)
                        user.Empresas = new List<Empresa>();

                    if (!user.Empresas.Any(x => x.ClienteEZ.Id == clienteEz.Id && x.Id == empresa.Id))
                    {
                        user.Empresas.Add(empresa);
                    }

                    _context.SaveChanges();
                }
            }
        }

        protected void AssociarPermissaoEmpresaPorRole(Empresa empresa, Role role, string permissionName)
        {
            var permissaoUsuarioPorEmpresa = _context
                .PermissaoEmpresaPorRole.FirstOrDefault(x =>
                       x.Empresa != null &&
                       x.Empresa.Id == empresa.Id &&
                       x.RoleId == role.Id &&
                       x.TenantId == _tenantId &&
                       x.PermissionName == permissionName);

            if (permissaoUsuarioPorEmpresa == null)
            {
                permissaoUsuarioPorEmpresa = new PermissaoEmpresaPorRole()
                {
                    Empresa = empresa,
                    IsGranted = true,
                    PermissionName = permissionName,
                    RoleId = role.Id,
                    TenantId = _tenantId
                };

                _context.PermissaoEmpresaPorRole.Add(permissaoUsuarioPorEmpresa);
                _context.SaveChanges();
            }
        }

        protected ClienteEZ GetClienteEz()
        {
            return _context.ClientesEZ.FirstOrDefault();
        }
    }
}