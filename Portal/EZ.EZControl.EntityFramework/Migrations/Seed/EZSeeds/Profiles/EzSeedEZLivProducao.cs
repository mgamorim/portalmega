﻿using Abp.Authorization;
using Abp.Configuration;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.Net.Mail;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Configuration;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EntityFramework;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;

namespace EZ.EZControl.Migrations.Seed.EZSeeds.Profiles
{
    public class EzSeedEZLivProducao : EzSeedProfileBase
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private int _empresaId { get; set; }

        public EzSeedEZLivProducao(EZControlDbContext context, int tenantId)
            : base(context, tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public override void Create()
        {
            ConfigurateDefault();
            CreateCorretor();
            CreateMaximaCare();
            CreateBemBeneficios();
            CreateEzliv();
            CreateRoleBeneficiario();
            CreateRoleCorretor();
            CreateRoleCorretora();
            CreateRoleAdministradora();
            CreateRoleAssociacao();
            CreateRoleOperadora();
            ConfigurateLanguange();
            UpdateAdminUserPermissions();
            CreateProfissoes();
            CreateTipoDocumentos();
            CreatePais();
            CreateEstadoByStorage();
            CreateCidadeByStorage();
            CreatePame();
            CreateEstoqueParametros();
        }


        private void AddOrEditSetting(string name, string value, int? tenantId = null)
        {
            var setting = _context.Settings
                .FirstOrDefault(s => s.Name == name && s.TenantId == null);

            if (setting != null)
            {
                setting.Value = value;
                _context.Settings.AddOrUpdate(setting);
            }
            else
            {
                _context.Settings.Add(new Setting(tenantId, null, name, value));
            }
            _context.SaveChanges();
        }

        private void ConfigurateDefault()
        {
            AddOrEditSetting(EmailSettingNames.Smtp.UserName, "ezliv");
            AddOrEditSetting(EmailSettingNames.Smtp.UseDefaultCredentials, "false");
            AddOrEditSetting(EmailSettingNames.Smtp.Host, "mail.smtp2go.com");
            AddOrEditSetting(EmailSettingNames.Smtp.Password, "enFtbHJ2bHhsMWdq");
            AddOrEditSetting(EmailSettingNames.Smtp.Port, "465");
            AddOrEditSetting(EmailSettingNames.Smtp.EnableSsl, "true");

            AddOrEditSetting(AppSettings.General.WebSiteRootAddress, "https://prdmegavita.azurewebsites.net");

            AddOrEditSetting(EmailSettingNames.DefaultFromAddress, "contato@ezliv.com.br");
            AddOrEditSetting(EmailSettingNames.DefaultFromDisplayName, "Contato - Portal MEGA");
            AddOrEditSetting("Abp.Zero.UserManagement.IsEmailConfirmationRequiredForLogin", "true");
            AddOrEditSetting("Abp.Timing.TimeZone", "E. South America Standard Time");
        }

        private void CreateCorretor()
        {

        }

        public void CreateMaximaCare()
        {
            // Criar empresas
            var query1 = from e in _context.Empresas
                         where e.RazaoSocial == "MARKO ROCHA'S EMPREENDIMENTOS"
                         select e;

            var empresa1 = query1.FirstOrDefault();
            if (empresa1 == null)
            {
                empresa1 = new Empresa
                {
                    IsActive = true,
                    RazaoSocial = "MARKO ROCHA'S EMPREENDIMENTOS",
                    NomeFantasia = "MaximaCare",
                    TenantId = _tenantId,
                };

                _context.Empresas.Add(empresa1);
                _context.SaveChanges();

                // Criar pessoa juridica
                var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.RazaoSocial == "MARKO ROCHA'S EMPREENDIMENTOS");
                if (pessoaJuridica == null)
                {
                    pessoaJuridica = new PessoaJuridica
                    {
                        RazaoSocial = "MARKO ROCHA'S EMPREENDIMENTOS",
                        NomeFantasia = "MaximaCare",
                        TenantId = _tenantId
                    };

                    _context.PessoasJuridicas.Add(pessoaJuridica);
                    _context.SaveChanges();
                }

                // Criar clienteEz
                var clienteEz = _context.ClientesEZ.FirstOrDefault(x => x.Nome.Contains("MARKO ROCHA'S EMPREENDIMENTOS"));
                if (clienteEz == null)
                {
                    clienteEz = new ClienteEZ
                    {
                        IsActive = true,
                        RazaoSocial = "MARKO ROCHA'S EMPREENDIMENTOS",
                        Nome = "MaximaCare",
                        PessoaId = pessoaJuridica.Id,
                        TenantId = _tenantId,
                    };

                    _context.SaveChanges();
                }

                empresa1.ClienteEZ = clienteEz;
                empresa1.PessoaJuridicaId = pessoaJuridica.Id;

                _context.SaveChanges();
            }

            _empresaId = empresa1.Id;
        }

        public void CreateBemBeneficios()
        {
            // Criar empresas
            var query1 = from e in _context.Empresas
                         where e.RazaoSocial == "BEM BENEFICIOS ADMINISTRADORA DE BENEFICIOS LTDA"
                         select e;

            var empresa1 = query1.FirstOrDefault();
            if (empresa1 == null)
            {
                empresa1 = new Empresa
                {
                    IsActive = true,
                    RazaoSocial = "BEM BENEFICIOS ADMINISTRADORA DE BENEFICIOS LTDA",
                    NomeFantasia = "BEM BENEFICIOS",
                    TenantId = _tenantId,
                };

                _context.Empresas.Add(empresa1);
                _context.SaveChanges();

                // Criar pessoa juridica
                var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.RazaoSocial == "BEM BENEFICIOS ADMINISTRADORA DE BENEFICIOS LTDA");
                if (pessoaJuridica == null)
                {
                    pessoaJuridica = new PessoaJuridica
                    {
                        RazaoSocial = "BEM BENEFICIOS ADMINISTRADORA DE BENEFICIOS LTDA",
                        NomeFantasia = "BEM BENEFICIOS",
                        TenantId = _tenantId
                    };

                    _context.PessoasJuridicas.Add(pessoaJuridica);
                    _context.SaveChanges();
                }

                // Criar clienteEz
                var clienteEz = _context.ClientesEZ.FirstOrDefault(x => x.Nome.Contains("BEM BENEFICIOS ADMINISTRADORA DE BENEFICIOS LTDA"));
                if (clienteEz == null)
                {
                    clienteEz = new ClienteEZ
                    {
                        IsActive = true,
                        RazaoSocial = "BEM BENEFICIOS ADMINISTRADORA DE BENEFICIOS LTDA",
                        Nome = "BEM BENEFICIOS",
                        PessoaId = pessoaJuridica.Id,
                        TenantId = _tenantId,
                    };

                    _context.SaveChanges();
                }

                var adm = new Administradora()
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaJuridica = pessoaJuridica,
                };

                _context.Administradoras.Add(adm);

                empresa1.ClienteEZ = clienteEz;
                empresa1.PessoaJuridicaId = pessoaJuridica.Id;

                _context.SaveChanges();
            }

            //_empresaId = empresa1.Id;
        }

        public void CreateEzliv()
        {
            // Criar empresas
            var query1 = from e in _context.Empresas
                         where e.RazaoSocial == "EZLiv Seguros"
                         select e;

            var empresa1 = query1.FirstOrDefault();
            if (empresa1 == null)
            {
                empresa1 = new Empresa
                {
                    IsActive = true,
                    RazaoSocial = "EZLiv Seguros",
                    NomeFantasia = "EZLiv",
                    TenantId = _tenantId,
                };

                _context.Empresas.Add(empresa1);
                _context.SaveChanges();

                // Criar pessoa juridica
                var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.RazaoSocial == "EZLiv Seguros");
                if (pessoaJuridica == null)
                {
                    pessoaJuridica = new PessoaJuridica
                    {
                        RazaoSocial = "EZLiv Seguros",
                        NomeFantasia = "EZLiv",
                        TenantId = _tenantId
                    };

                    _context.PessoasJuridicas.Add(pessoaJuridica);
                    _context.SaveChanges();
                }

                // Criar clienteEz
                var clienteEz = _context.ClientesEZ.FirstOrDefault(x => x.Nome.Contains("EZLiv Seguros"));
                if (clienteEz == null)
                {
                    clienteEz = new ClienteEZ
                    {
                        IsActive = true,
                        RazaoSocial = "EZLiv Seguros",
                        Nome = "EZLiv",
                        PessoaId = pessoaJuridica.Id,
                        TenantId = _tenantId,
                    };

                    _context.SaveChanges();
                }

                var adm = new Administradora()
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaJuridica = pessoaJuridica,
                };

                _context.Administradoras.Add(adm);

                empresa1.ClienteEZ = clienteEz;
                empresa1.PessoaJuridicaId = pessoaJuridica.Id;

                _context.SaveChanges();
            }

            //_empresaId = empresa1.Id;
        }

        private void CreateProfissoes()
        {
            // Profissoes ativos na qualicorp 23/12/2016 - economizecomaqualicorp.com.br/home/ListarProfissaoPorSugestao?sugestao=u&estadoId=19
            var profissoes = new string[]
            {
                "Administrador", "Agrônomo", "Bacharel em direito", "Biomédico", "Bombeiro Militar", "Comerciário",
                "Corretor de Imóveis", "Empregador do Comércio e Serviços", "Enfermeiro", "Engenheiro Químico",
                "Estudante do Ensino Médio ou Fundamental", "Farmacêutico", "Gestor Governamental", "Guarda Municipal",
                "Magistrado", "Marinheiro", "Médico", "Meteorologista", "Militar das Forças Armadas", "Músico",
                "Optometrista", "Perito Criminal", "Professor", "Profissional Liberal", "Químico", "Aeroviário",
                "Arquiteto e Urbanista", "Cirurgião - Dentista", "Contabilista", "Defensor Público",
                "Delegado de Polícia", "Delegado de Polícia Federal", "Dentista", "Designer Gráfico", "Engenheiro ",
                "Estudante do Ensino Infantil ", "Estudante Universitário", "Fisioterapeuta", "Geógrafo", "Geólogo",
                "Juiz Federal", "Não definida", "Não sei", "Procurador da República", "Prof.de Artes e Espetáculos",
                "Prof.de Educação Física", "Servidor da Agricultura", "Servidor das Ag.Reguladoras", "Servidor Público",
                "Veterinário", "Zootecnista", "Analista Tributário", "Atuário", "Funcionário Público", "Óptico",
                "Advogado", "Procurador", "Bibliotecário", "Cinematográfico", "Consultor de Venda Direta",
                "Distribuidor de Venda Direta", "Economista", "Empresário de Venda Direta", "Executivo Público",
                "Fonoaudiólogo", "Investigador", "Jornalista", "Nutricionista", "Policial Civil ", "Policial Militar",
                "Prof.da indústria", "Prof.Indústria do Vestuário", "Profissional de TI", "Profissional de Venda Direta",
                "Psicólogo", "Publicitário", "Relações Públicas", "Representante Comercial",
                "Revendedor de Venda Direta", "Tabeliaes e Oficiais de Registro", "Técnicos Judiciários",
                "Terapeuta Ocupacional", "Transportador Rodoviário", "Vendedor Autônomo"
            };

            var cod = 1;
            foreach (var profissao in profissoes)
            {
                var prof = _context.Profissoes.FirstOrDefault(x => x.Titulo == profissao);
                if (prof == null)
                {
                    prof = new Profissao
                    {
                        IsActive = true,
                        Titulo = profissao,
                        Codigo = cod.ToString(),
                        TenantId = _tenantId
                    };

                    cod = cod + 1;

                    _context.Profissoes.Add(prof);
                    _context.SaveChanges();
                }
            }
        }

        private void CreateRoleBeneficiario()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Beneficiario);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Beneficiario, StaticRoleNames.Tenants.Beneficiario) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleCorretor()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Corretor);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Corretor, StaticRoleNames.Tenants.Corretor) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleCorretora()
        {
            var corretoraRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Corretora);
            if (corretoraRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Corretora, StaticRoleNames.Tenants.Corretora) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleAdministradora()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Administradora);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Administradora, StaticRoleNames.Tenants.Administradora) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleAssociacao()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Associacao);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Associacao, StaticRoleNames.Tenants.Associacao) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleOperadora()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Operadora);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Operadora, StaticRoleNames.Tenants.Operadora) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void ConfigurateLanguange()
        {
            AddOrEditSetting(LocalizationSettingNames.DefaultLanguage, "pt-BR");

            var languanges = _context.Languages.ToList();
            foreach (var languange in languanges)
            {
                if (languange.Name != "en" && languange.Name != "pt-BR")
                {
                    languange.IsDeleted = true;
                }
            }
            _context.SaveChanges();
        }

        private void CreateTipoDocumentos()
        {
            // Cnpj
            var cnpj = _context.TipoDeDocumentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Cnpj".ToLower()));
            if (cnpj == null)
            {
                cnpj = new TipoDeDocumento()
                {
                    IsActive = true,
                    Descricao = "Cnpj",
                    TipoPessoa = TipoPessoaEnum.Juridica,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cnpj,
                    Mascara = "99.999.999/9999-99",
                    TenantId = _tenantId
                };

                _context.TipoDeDocumentos.Add(cnpj);
                _context.SaveChanges();
            }

            // Cpf
            var cpf = _context.TipoDeDocumentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Cpf".ToLower()));
            if (cpf == null)
            {
                cpf = new TipoDeDocumento()
                {
                    IsActive = true,
                    Descricao = "Cpf",
                    TipoPessoa = TipoPessoaEnum.Fisica,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf,
                    Mascara = "999.999.999-99",
                    TenantId = _tenantId
                };

                _context.TipoDeDocumentos.Add(cpf);
                _context.SaveChanges();
            }


            // Cpf
            var rg = _context.TipoDeDocumentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("RG".ToLower()));
            if (rg == null)
            {
                cpf = new TipoDeDocumento()
                {
                    IsActive = true,
                    Descricao = "RG",
                    TipoPessoa = TipoPessoaEnum.Fisica,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Rg,
                    TenantId = _tenantId
                };

                _context.TipoDeDocumentos.Add(cpf);
                _context.SaveChanges();
            }
        }

        private void UpdateAdminUserPermissions()
        {
            var clienteEz = GetClienteEz();

            var empresa1 = _context.Empresas.FirstOrDefault(x => x.NomeFantasia == "MaximaCare");
            var empresa2 = _context.Empresas.FirstOrDefault(x => x.NomeFantasia == "BEM BENEFICIOS");
            var empresa3 = _context.Empresas.FirstOrDefault(x => x.NomeFantasia == "EZLiv");

            var adminUser = GetAdminUser();
            if (adminUser != null)
            {
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa1);
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa2);
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa3);
            }

            Role adminRole = GetAdminRole();

            //bool isMultiTenantEnabled = true;
            //var permissons = PermissionFinder
            //    .GetAllPermissions(new AppAuthorizationProvider(isMultiTenantEnabled));

            var permissions = PermissionFinder
                .GetAllPermissions(new AppAuthorizationProvider(true))
                .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant))
                .ToList();

            foreach (var permission in permissions)
            {
                AssociarPermissaoEmpresaPorRole(empresa1, adminRole, permission.Name);
            }

            foreach (var permissionName in new[] { "Pages", "Pages.Administration", "Pages.Administration.Roles", "Pages.Administration.Roles.Create", "Pages.Administration.Roles.Delete", "Pages.Administration.Roles.Edit", "Pages.Tenant.Global.Empresa", "Pages.Administration.Users", "Pages.Administration.Users.Create", "Pages.Administration.Users.Edit", "Pages.Administration.Users.Delete", "Pages.Administration.Users.ChangePermissions", "Pages.Administration.Users.Impersonation", "Pages.Administration.Host", "Pages.Administration.Host.Settings" })
            {
                AssociarPermissaoEmpresaPorRole(empresa2, adminRole, permissionName);
            }

            foreach (var permissionName in new[] { "Pages", "Pages.Administration", "Pages.Administration.Roles", "Pages.Administration.Roles.Create", "Pages.Administration.Roles.Delete", "Pages.Administration.Roles.Edit", "Pages.Tenant.Global.Empresa", "Pages.Administration.Users", "Pages.Administration.Users.Create", "Pages.Administration.Users.Edit", "Pages.Administration.Users.Delete", "Pages.Administration.Users.ChangePermissions", "Pages.Administration.Users.Impersonation", "Pages.Administration.Host", "Pages.Administration.Host.Settings" })
            {
                AssociarPermissaoEmpresaPorRole(empresa3, adminRole, permissionName);
            }
        }

        private void CreatePais()
        {
            // Brasil
            var brasil = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Brasil".ToLower()));
            if (brasil == null)
            {
                brasil = new Pais()
                {
                    Nome = "Brasil",
                    Nacionalidade = "Brasileira",
                    Sigla2Caracteres = "BR",
                    Sigla3Caracteres = "BRA",
                    MascaraDoCodigoPostal = "99999-999",
                    CodigoInternacional = "55",
                    IsActive = true,
                    CodigoISO = "12345",
                    CodigoReceitaFederal = "12345",

                };

                _context.Paises.Add(brasil);
                _context.SaveChanges();
            }
        }

        private void CreateEstado()
        {
            var appConnection = ConfigurationManager.ConnectionStrings["DbAddress"];
            var brasil = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Brasil".ToLower()));
            if (brasil != null && appConnection != null)
            {
                using (SqlConnection cnn = new SqlConnection(appConnection.ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter("SELECT [ufe_sg],[ufe_no],[ufe_codigo_ibge],[ufe_capital] FROM [AddressStorage].[dbo].[log_faixa_uf]", cnn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "log_faixa_uf");

                    foreach (DataRow row in ds.Tables["log_faixa_uf"].Rows)
                    {
                        var sigla = row["ufe_sg"].ToString();
                        var nome = row["ufe_no"].ToString();
                        var codigoIbge = row["ufe_codigo_ibge"].ToString();
                        var capital = row["ufe_capital"].ToString();

                        var estado = _context.Estados.FirstOrDefault(x => x.Sigla.ToLower() == sigla.ToLower());
                        if (estado == null)
                        {
                            _context.Estados.Add(new Estado
                            {
                                IsActive = true,
                                Nome = nome,
                                Sigla = sigla,
                                CodigoIBGE = codigoIbge,
                                Pais = brasil,
                                Capital = capital,
                                TenantId = _tenantId
                            });
                            _context.SaveChanges();
                        }

                    }
                }
            }
        }

        private void CreateCidade()
        {
            var appConnection = ConfigurationManager.ConnectionStrings["DbAddress"];
            if (appConnection != null)
                using (SqlConnection cnn = new SqlConnection(appConnection.ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter("SELECT [loc_no],[ufe_sg] FROM [AddressStorage].[dbo].[log_localidade] where loc_in_tipo_localidade = 'M'", cnn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "log_localidade");

                    foreach (DataRow row in ds.Tables["log_localidade"].Rows)
                    {
                        var siglaEstado = row["ufe_sg"].ToString();
                        var nome = row["loc_no"].ToString();

                        var est = _context.Estados.FirstOrDefault(x => x.Sigla.ToLower().Contains(siglaEstado));
                        if (est != null)
                        {
                            var cid = _context.Cidades.FirstOrDefault(x => x.Nome.ToLower() == nome.ToLower() && x.Estado.Sigla == est.Sigla);
                            if (cid == null)
                            {
                                _context.Cidades.Add(new Cidade
                                {
                                    Nome = nome,
                                    IsActive = true,
                                    Estado = est,
                                    TenantId = _tenantId
                                });
                                _context.SaveChanges();
                            }
                        }
                    }
                }
        }

        private void CreateEstadoByStorage()
        {
            var appConnection = ConfigurationManager.ConnectionStrings["DbAddress"];
            var brasil = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Brasil".ToLower()));
            if (brasil != null && appConnection != null)
            {
                using (SqlConnection cnn = new SqlConnection(appConnection.ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter("SELECT [ufe_sg],[ufe_no],[ufe_codigo_ibge],[ufe_capital] FROM [AddressStorage].[dbo].[log_faixa_uf]", cnn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "log_faixa_uf");

                    foreach (DataRow row in ds.Tables["log_faixa_uf"].Rows)
                    {
                        var sigla = row["ufe_sg"].ToString();
                        var nome = row["ufe_no"].ToString();
                        var codigoIbge = row["ufe_codigo_ibge"].ToString();
                        var capital = row["ufe_capital"].ToString();

                        var estado = _context.Estados.FirstOrDefault(x => x.Sigla.ToLower() == sigla.ToLower());
                        if (estado == null)
                        {
                            _context.Estados.Add(new Estado
                            {
                                IsActive = true,
                                Nome = nome,
                                Sigla = sigla,
                                CodigoIBGE = codigoIbge,
                                Pais = brasil,
                                Capital = capital,
                                TenantId = _tenantId
                            });
                            _context.SaveChanges();
                        }

                    }
                }
            }
        }

        private void CreateCidadeByStorage()
        {
            var appConnection = ConfigurationManager.ConnectionStrings["DbAddress"];
            if (appConnection != null)
                using (SqlConnection cnn = new SqlConnection(appConnection.ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter("SELECT [loc_no],[ufe_sg] FROM [AddressStorage].[dbo].[log_localidade] where loc_in_tipo_localidade = 'M'", cnn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "log_localidade");

                    foreach (DataRow row in ds.Tables["log_localidade"].Rows)
                    {
                        var siglaEstado = row["ufe_sg"].ToString();
                        var nome = row["loc_no"].ToString();

                        var est = _context.Estados.FirstOrDefault(x => x.Sigla.ToLower().Contains(siglaEstado));
                        if (est != null)
                        {
                            var cid = _context.Cidades.FirstOrDefault(x => x.Nome.ToLower() == nome.ToLower() && x.Estado.Sigla == est.Sigla);
                            if (cid == null)
                            {
                                _context.Cidades.Add(new Cidade
                                {
                                    Nome = nome,
                                    IsActive = true,
                                    Estado = est,
                                    TenantId = _tenantId
                                });
                                _context.SaveChanges();
                            }
                        }
                    }
                }
        }

        private List<ItemDeDeclaracaoDeSaude> CreateItensDeDeclaracao(ProponenteTitular titular)
        {
            var perguntas = new string[]
               {
                "Tem ou teve bronquite?",
                "Tem ou teve miopia, hipermetropia ou astigmatismo?",
                "Tem ou teve estrabismo, catarata, ceratocone, glaucoma, lesões de retina ou córnea?",
                "Tem ou teve sinusite, desvio de septo, hipertrofia de amigdalas, adenóides ou doença do ronco? Tem indicação de cirurgia?",
                "Tem ou teve eplepsia, convulsão ou enxaqueca?",
                "Tem ou teve varizes?",
                "Tem ou teve pressão alta, infarto, angina, arritmia, aneurisma, trombose, embolia? Possui marcapasso ou stent?",
                "Tem ou teve doença do sangue (anemia, hemofilia, púrpura ou outra)?",
                "Tem diabetes? Qual tipo?",
                "Tem ou teve doença do rim (Insuficiência renal, cálculo, infecção urinária de repetição, transplante ou outra)? Faz ou fez diálise ou hemodiálise?",
                "Tem fimose? hidrocele, varicocele, testículo não descido (criptorquidia)?",
                "Tem ou teve úlcera, refluxo gastroesofágico, hérnia de hiato, varizes de esôfago, esofagite, doença de Crohn, pólipos ou outros?",
                "Tem ou teve cálculo na vesícula, doença do pâncreas?",
                "Tem hepatite (B e C)? É portador do virus HIV (AIDS) e suas complicações?",
                "Tem ou teve doença maligna (leucemia, lifomas, ou outros tipos de câncer)? Faz ou fez quimoterapia ou radioterapia?",
                "Tem ou teve doenças das articulações, dos músculos ou dos ossos (articulação da mandíbula, hérnia de disco, artrite, escoliose, bico de papagaio, lesão de joelho, ombro ou outras)? Alguma doença reumatológica(lúpus, artrite, reumatóide ou outra) ? ",
                "Tem ou teve doença endocrinológica (hipertiroidismo, hipotiroidismo, bócio ou nódulo da freóide, da hipófise, doença da supra-renal ou outra?)",
                "Tem algum tipo de hérnia (inguinal, umbilical, bolsa escrotal ou outra)?",
                "Tem ou teve doença da próstata ou de infertilidade?",
                "Tem ou teve doença de mama (aumento do volume, displasia, nódulo ou outra)?",
                "Tem ou teve doença doença ginecológica (cisto de ovário, ovário policístico, endometriose, mioma, infertilidade ou outra?)",
                "Encontra-se internado ou em tratamento?",
                "Tem ou teve doença congênita (desde que nasceu)? Alguma síndrome (Down, Marfan ou outra)?",
                "Já foi submetido algum tipo de cirurgia? Especifique.",
                "Tem indicação de submeter-se a algum tipo de cirurgia? Especifique."
               };

            var cod = 1;
            var itensDeDeclaracao = new List<ItemDeDeclaracaoDeSaude>();
            foreach (var pergunta in perguntas)
            {
                var itemDeDeclaracao = _context.ItensDeDeclaracaoDeSaude.FirstOrDefault(x => x.Pergunta == pergunta);
                if (itemDeDeclaracao == null)
                {
                    itemDeDeclaracao = new ItemDeDeclaracaoDeSaude
                    {
                        IsActive = true,
                        Pergunta = pergunta,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };

                    cod = cod + 1;

                    _context.ItensDeDeclaracaoDeSaude.Add(itemDeDeclaracao);
                    _context.SaveChanges();
                }

                itensDeDeclaracao.Add(itemDeDeclaracao);
            }

            return itensDeDeclaracao;
        }

        private void CreateDeclaracaoDeSaudeAndItens()
        {
            var titular = _context.Proponentes.FirstOrDefault(x => x.PessoaFisica.Nome == "Maria das Dores");
            var itensDeDeclaracao = this.CreateItensDeDeclaracao(titular);

            var declaracaoDeSaude = _context.DeclaracoesDeSaude.FirstOrDefault(x => x.Nome == "Declaração EZSoft");
            if (declaracaoDeSaude == null && titular != null)
            {
                declaracaoDeSaude = new DeclaracaoDeSaude
                {
                    IsActive = true,
                    Nome = "Declaração EZSoft",
                    Titular = titular,
                    Dependentes = titular.Dependentes,
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaude>(),
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                declaracaoDeSaude.ItensDeDeclaracaoDeSaude = itensDeDeclaracao;
                _context.DeclaracoesDeSaude.Add(declaracaoDeSaude);
                _context.SaveChanges();
            }
        }

        private void CreatePame()
        {
            var pame = _context.Operadoras.FirstOrDefault(x => x.Nome == "PAME");

            if (pame != null) return;

            var pessoaJuridica = new PessoaJuridica
            {
                TenantId = _tenantId,
                NomeFantasia = "Pame - Planos de Assistencia Medica",
                RazaoSocial = "Pame - Associacao de Assistencia Plena Em Saude"
            };

            _context.PessoasJuridicas.Add(pessoaJuridica);
            _context.SaveChanges();

            pame = new Operadora
            {
                IsActive = true,
                TenantId = _tenantId,
                CodigoAns = "34240-8",
                Nome = "PAME",
                ProdutosDePlanoDeSaude = new List<ProdutoDePlanoDeSaude>(),
                PessoaJuridica = pessoaJuridica,
                PessoaJuridicaId = pessoaJuridica.Id
            };

            _context.Operadoras.Add(pame);
            _context.SaveChanges();
        }

        private void CreateEstoqueParametros()
        {
            var unidadeMedida = _context.UnidadeMedidas.FirstOrDefault(x => x.Descricao.ToLower() == "unidade".ToLower());

            if (unidadeMedida == null)
            {
                unidadeMedida = new UnidadeMedida
                {
                    Descricao = "Unidade",
                    TenantId = _tenantId,
                    UnidadePadrao = UnidadePadraoEnum.Outros,
                    Valor = 1
                };

                _context.UnidadeMedidas.Add(unidadeMedida);
                _context.SaveChanges();
            }


            var natureza = _context.Naturezas.FirstOrDefault(x => x.Descricao.ToLower() == "planos de saúde".ToLower());
            if (natureza == null)
            {
                natureza = new Natureza
                {
                    Descricao = "Planos de Saúde",
                    TenantId = _tenantId
                };

                _context.Naturezas.Add(natureza);
                _context.SaveChanges();
            }
        }
    }
}