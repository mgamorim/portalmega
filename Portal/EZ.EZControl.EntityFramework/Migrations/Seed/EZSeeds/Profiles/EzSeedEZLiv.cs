﻿using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.Net.Mail;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Configuration;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.EZLiv.Sync;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Migrations.Seed.Tenants;
using Microsoft.AspNet.Identity;
using ML.EZControl.DataBuilder.Cms.Categoria;
using ML.MarcosLima.DataBuilder.Cms.Pagina;
using ML.MarcosLima.DataBuilder.Cms.Post;
using ML.MarcosLima.DataBuilder.Cms.Site;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;

namespace EZ.EZControl.Migrations.Seed.EZSeeds
{
    public class EzSeedEZLiv : EzSeedProfileBase
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private int _empresaId { get; set; }

        public EzSeedEZLiv(EZControlDbContext context, int tenantId)
            : base(context, tenantId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = new TenantEmpresaBuilder(_context, _tenantId).CreateEZLiv();

            new TemplateDataBuilder(context, tenantId, _empresaId).Create();
            new SiteDataBuilder(context, tenantId, _empresaId).Create();
            new PaginaDataBuilder(context, tenantId, _empresaId).Create();
            new CategoriaDataBuilder(context, tenantId, _empresaId).Create();
            new PostDataBuilder(context, tenantId, _empresaId).Create();
        }

        public override void Create()
        {
            ConfigurateDefault();
            CreateOutrasEmpresas();
            CreateRoleBeneficiario();
            CreateRoleCorretor();
            CreateRoleCorretora();
            CreateRoleAdministradora();
            CreateRoleAssociacao();
            CreateRoleOperadora();
            ConfigurateLanguange();
            UpdateAdminUserPermissions();
            CreatePais();
            //CreateEstadoByStorage();
            //CreateCidadeByStorage();
            CreateEstado();
            CreateCidade();
            CreateProfissoes();
            CreateCorretor();
            CreateCorretora();
            CreateOperadora();
            CreateAssociacao();
            CreateAdministradora();
            CreateBeneficiario();
            CreateDeclaracaoDeSaudeAndItens();
            CreateQuestionario();
            CreateParametrosPagSeguro();
            CreatePlanoDeSaude();
            CreateEstoqueParametros();
            CreateProdutoDePlanoDeSaude();
            CreateTipoDocumentos();
            CreateContrato();
            CreateEspecialidadeSync();
            AssociarAdminParaPessoaFisicaEComoCorretor();
            CreateFaixaEtaria();
            CreateValorPorFaixaEtaria();

            CreateUserCmsGuest();
            CreateGrupoPessoas();
            CreateBancos();
            CreateTratamentos();
            CreateTipoDeContatos();
            CreateTipoDeLogradouros();
            CreateConexoesSmtp();
            CreateTemplate();
            CreateSite();
            CreatePagina();
            CreateMenu();
            CreateItemDeMenu();
            CreateWidgetMenu();
            CreateEspecialidade();
            CreateMedico();
            CreateRegrasAgenda();
            CreateServico();
            CreateProduto();
        }

        public void CreateOutrasEmpresas()
        {
            var tenantEmpresaBuilder = new TenantEmpresaBuilder(_context, _tenantId);
            var empresaIdEzSoft = tenantEmpresaBuilder.CreateEzSoft();
            var empresaIdEmpresaExemplo = tenantEmpresaBuilder.CreateEmpresaExemplo();
        }

        private void CreateParametrosPagSeguro()
        {
            var parametroEZLiv = new ParametroPagSeguro();
            var administradora = new PessoaJuridica()
            {
                NomeFantasia = "EZLiv",
                RazaoSocial = "EZLiv",
                TenantId = _tenantId
            };
            parametroEZLiv.PessoaJuridica = administradora;
            parametroEZLiv.Email = "pagseguro@ezsoft.com.br";
            parametroEZLiv.Token = "9F2E8A7316474DAAB997A407AC4BCFE6";
            parametroEZLiv.TenantId = _tenantId;
            parametroEZLiv.EmpresaId = _empresaId;

            _context.ParametrosPagSeguro.Add(parametroEZLiv);
            _context.SaveChanges();

            var parametroCACS = new ParametroPagSeguro();
            administradora = new PessoaJuridica()
            {
                NomeFantasia = "CACS",
                RazaoSocial = "CACS",
                TenantId = _tenantId
            };
            parametroCACS.PessoaJuridica = administradora;
            parametroCACS.Email = "";
            parametroCACS.Token = "";
            parametroCACS.TenantId = _tenantId;
            parametroCACS.EmpresaId = _empresaId;

            _context.ParametrosPagSeguro.Add(parametroCACS);
            _context.SaveChanges();

            var parametroBEMBeneficios = new ParametroPagSeguro();
            administradora = new PessoaJuridica()
            {
                NomeFantasia = "BEM Benefícios",
                RazaoSocial = "BEM Benefícios",
                TenantId = _tenantId
            };
            parametroBEMBeneficios.PessoaJuridica = administradora;
            parametroBEMBeneficios.Email = "";
            parametroBEMBeneficios.Token = "";
            parametroBEMBeneficios.TenantId = _tenantId;
            parametroBEMBeneficios.EmpresaId = _empresaId;

            _context.ParametrosPagSeguro.Add(parametroBEMBeneficios);
            _context.SaveChanges();
        }

        private void CreateProfissoes()
        {
            // Profissoes ativos na qualicorp 23/12/2016 - economizecomaqualicorp.com.br/home/ListarProfissaoPorSugestao?sugestao=u&estadoId=19
            var profissoes = new string[]
            {
                "Administrador", "Agrônomo", "Bacharel em direito", "Biomédico", "Bombeiro Militar", "Comerciário",
                "Corretor de Imóveis", "Empregador do Comércio e Serviços", "Enfermeiro", "Engenheiro Químico",
                "Estudante do Ensino Médio ou Fundamental", "Farmacêutico", "Gestor Governamental", "Guarda Municipal",
                "Magistrado", "Marinheiro", "Médico", "Meteorologista", "Militar das Forças Armadas", "Músico",
                "Optometrista", "Perito Criminal", "Professor", "Profissional Liberal", "Químico", "Aeroviário",
                "Arquiteto e Urbanista", "Cirurgião - Dentista", "Contabilista", "Defensor Público",
                "Delegado de Polícia", "Delegado de Polícia Federal", "Dentista", "Designer Gráfico", "Engenheiro ",
                "Estudante do Ensino Infantil ", "Estudante Universitário", "Fisioterapeuta", "Geógrafo", "Geólogo",
                "Juiz Federal", "Não definida", "Não sei", "Procurador da República", "Prof.de Artes e Espetáculos",
                "Prof.de Educação Física", "Servidor da Agricultura", "Servidor das Ag.Reguladoras", "Servidor Público",
                "Veterinário", "Zootecnista", "Analista Tributário", "Atuário", "Funcionário Público", "Óptico",
                "Advogado", "Procurador", "Bibliotecário", "Cinematográfico", "Consultor de Venda Direta",
                "Distribuidor de Venda Direta", "Economista", "Empresário de Venda Direta", "Executivo Público",
                "Fonoaudiólogo", "Investigador", "Jornalista", "Nutricionista", "Policial Civil ", "Policial Militar",
                "Prof.da indústria", "Prof.Indústria do Vestuário", "Profissional de TI", "Profissional de Venda Direta",
                "Psicólogo", "Publicitário", "Relações Públicas", "Representante Comercial",
                "Revendedor de Venda Direta", "Tabeliaes e Oficiais de Registro", "Técnicos Judiciários",
                "Terapeuta Ocupacional", "Transportador Rodoviário", "Vendedor Autônomo"
            };

            var cod = 1;
            foreach (var profissao in profissoes)
            {
                var prof = _context.Profissoes.FirstOrDefault(x => x.Titulo == profissao);
                if (prof == null)
                {
                    prof = new Profissao
                    {
                        IsActive = true,
                        Titulo = profissao,
                        Codigo = cod.ToString(),
                        TenantId = _tenantId
                    };

                    cod = cod + 1;

                    _context.Profissoes.Add(prof);
                    _context.SaveChanges();
                }
            }
        }

        private void AddOrEditSetting(string name, string value, int? tenantId = null)
        {
            var setting = _context.Settings
                .FirstOrDefault(s => s.Name == name && s.TenantId == null);

            if (setting != null)
            {
                setting.Value = value;
                _context.Settings.AddOrUpdate(setting);
            }
            else
            {
                _context.Settings.Add(new Setting(tenantId, null, name, value));
            }
            _context.SaveChanges();
        }

        private void ConfigurateDefault()
        {
            AddOrEditSetting(EmailSettingNames.Smtp.UserName, "smtp_admin_ezsoft");
            AddOrEditSetting(EmailSettingNames.Smtp.UseDefaultCredentials, "false");
            AddOrEditSetting(EmailSettingNames.Smtp.Host, "mail.smtp2go.com");
            AddOrEditSetting(EmailSettingNames.Smtp.Password, "N2M4enNlZXozNTRk");
            AddOrEditSetting(EmailSettingNames.Smtp.Port, "587");
            AddOrEditSetting(EmailSettingNames.Smtp.EnableSsl, "false");

            AddOrEditSetting(AppSettings.General.WebSiteRootAddress, "http://www.ezliv.com.br");

            AddOrEditSetting(EmailSettingNames.DefaultFromAddress, "contato@ezliv.com.br");
            AddOrEditSetting(EmailSettingNames.DefaultFromDisplayName, "Contato - EZLiv");
            AddOrEditSetting("Abp.Zero.UserManagement.IsEmailConfirmationRequiredForLogin", "true");
            AddOrEditSetting("Abp.Timing.TimeZone", "E. South America Standard Time");
        }

        private void CreateUserCmsGuest()
        {
            // Usuário Cms Guest
            var ucmsg = _context.Users.FirstOrDefault(x => x.UserName.ToLower().Contains("CmsGuest".ToLower()));
            if (ucmsg == null)
            {
                ucmsg = new User()
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    Name = "CmsGuest",
                    Surname = "Ez",
                    UserName = "CmsGuest",
                    Password = new PasswordHasher().HashPassword("123qwe"),
                    IsEmailConfirmed = true,
                    EmailAddress = "cmsguest@ezsoft.com.br"
                };

                var role = _context.Roles.FirstOrDefault(x => x.Name.ToLower().Contains("User".ToLower()));

                if (role != null)
                {
                    ucmsg.Roles = new List<UserRole>() { };
                    ucmsg.Roles.Add(new UserRole(_tenantId, ucmsg.Id, role.Id));
                }

                _context.Users.Add(ucmsg);
                _context.SaveChanges();
            }
        }

        private void CreateRoleBeneficiario()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Beneficiario);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Beneficiario, StaticRoleNames.Tenants.Beneficiario) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleCorretor()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Corretor);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Corretor, StaticRoleNames.Tenants.Corretor) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleCorretora()
        {
            var corretoraRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Corretora);
            if (corretoraRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Corretora, StaticRoleNames.Tenants.Corretora) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleAdministradora()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Administradora);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Administradora, StaticRoleNames.Tenants.Administradora) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleAssociacao()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Associacao);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Associacao, StaticRoleNames.Tenants.Associacao) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void CreateRoleOperadora()
        {
            var recrutadorRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Operadora);
            if (recrutadorRole == null)
            {
                //TODO: Adicionar permissões específicas

                _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Operadora, StaticRoleNames.Tenants.Operadora) { IsStatic = true });
                _context.SaveChanges();
            }
        }

        private void ConfigurateLanguange()
        {
            AddOrEditSetting(LocalizationSettingNames.DefaultLanguage, "pt-BR");

            var languanges = _context.Languages.ToList();
            foreach (var languange in languanges)
            {
                if (languange.Name != "en" && languange.Name != "pt-BR")
                {
                    languange.IsDeleted = true;
                }
            }
            _context.SaveChanges();
        }

        private void CreateTipoDocumentos()
        {
            // Cnpj
            var cnpj = _context.TipoDeDocumentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Cnpj".ToLower()));
            if (cnpj == null)
            {
                cnpj = new TipoDeDocumento()
                {
                    IsActive = true,
                    Descricao = "Cnpj",
                    TipoPessoa = TipoPessoaEnum.Juridica,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cnpj,
                    Mascara = "99.999.999/9999-99",
                    TenantId = _tenantId
                };

                _context.TipoDeDocumentos.Add(cnpj);
                _context.SaveChanges();
            }

            // Cpf
            var cpf = _context.TipoDeDocumentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Cpf".ToLower()));
            if (cpf == null)
            {
                cpf = new TipoDeDocumento()
                {
                    IsActive = true,
                    Descricao = "Cpf",
                    TipoPessoa = TipoPessoaEnum.Fisica,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf,
                    Mascara = "999.999.999-99",
                    TenantId = _tenantId
                };

                _context.TipoDeDocumentos.Add(cpf);
                _context.SaveChanges();
            }

            // RG
            var rg = _context.TipoDeDocumentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("RG".ToLower()));
            if (rg == null)
            {
                rg = new TipoDeDocumento()
                {
                    IsActive = true,
                    Descricao = "RG",
                    TipoPessoa = TipoPessoaEnum.Fisica,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Rg,
                    Mascara = "99.999.999-9",
                    TenantId = _tenantId
                };

                _context.TipoDeDocumentos.Add(rg);
                _context.SaveChanges();
            }
        }

        public void UpdateAdminUserPermissions()
        {
            #region EZLiv

            var clienteEz = GetClienteEz();

            var empresa1 = _context.Empresas.FirstOrDefault(x => x.RazaoSocial == "EZLiv Ltda");

            var adminUser = GetAdminUser();
            if (adminUser != null)
            {
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa1);
            }

            Role adminRole = GetAdminRole();

            bool isMultiTenantEnabled = true;
            var permissons = PermissionFinder.GetAllPermissions(new AppAuthorizationProvider(isMultiTenantEnabled));

            foreach (var permission in permissons)
            {
                AssociarPermissaoEmpresaPorRole(empresa1, adminRole, permission.Name);
            }

            #endregion

            #region Outros

            //var clienteEz = GetClienteEz();

            var empresa2 = _context.Empresas.FirstOrDefault(x => x.RazaoSocial == "EZSoft Ltda");
            var empresa3 = _context.Empresas.FirstOrDefault(x => x.RazaoSocial == "Empresa Exemplo Ltda");

            //var adminUser = GetAdminUser();
            if (adminUser != null)
            {
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa2);
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa3);
            }

            //Role adminRole = GetAdminRole();

            var permissions = PermissionFinder
                .GetAllPermissions(new AppAuthorizationProvider(true))
                .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant))
                .ToList();

            foreach (var permission in permissions)
            {
                AssociarPermissaoEmpresaPorRole(empresa2, adminRole, permission.Name);
            }

            foreach (var permissionName in new[] { "Pages", "Pages.Administration", "Pages.Administration.Roles", "Pages.Administration.Roles.Create", "Pages.Administration.Roles.Delete", "Pages.Administration.Roles.Edit", "Pages.Tenant.Global.Empresa", "Pages.Administration.Users", "Pages.Administration.Users.Create", "Pages.Administration.Users.Edit", "Pages.Administration.Users.Delete", "Pages.Administration.Users.ChangePermissions", "Pages.Administration.Users.Impersonation", "Pages.Administration.Host", "Pages.Administration.Host.Settings" })
            {
                AssociarPermissaoEmpresaPorRole(empresa3, adminRole, permissionName);
            }

            #endregion
        }

        private void AssociarAdminParaPessoaFisicaEComoCorretor()
        {
            // Criar responsavel pessoa fisica
            var pessoaAdmin = _context.PessoasFisicas.FirstOrDefault(x => x.Nome == "Admin Silva");
            if (pessoaAdmin == null)
            {
                pessoaAdmin = new PessoaFisica
                {
                    Nome = "Admin Silva",
                    Sexo = SexoEnum.Feminino,
                    EstadoCivil = EstadoCivilEnum.Casado,
                    DataDeNascimento = DateTime.Now.AddYears(-40),
                    Nacionalidade = "Brasileira",
                    TenantId = _tenantId
                };
                _context.PessoasFisicas.Add(pessoaAdmin);
                _context.SaveChanges();

                var corretor = new Corretor
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaFisica = pessoaAdmin,
                };
                _context.Corretores.Add(corretor);
                _context.SaveChanges();

                var adminUser = GetAdminUser();
                adminUser.Pessoa = pessoaAdmin;
                adminUser.PessoaId = pessoaAdmin.Id;
                adminUser.TipoDeUsuario = TipoDeUsuarioEnum.Corretor;
            }
        }

        private void CreatePais()
        {
            // Brasil
            var brasil = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Brasil".ToLower()));
            if (brasil == null)
            {
                brasil = new Pais()
                {
                    Nome = "Brasil",
                    Nacionalidade = "Brasileira",
                    Sigla2Caracteres = "BR",
                    Sigla3Caracteres = "BRA",
                    MascaraDoCodigoPostal = "99999-999",
                    CodigoInternacional = "55",
                    IsActive = true,
                    CodigoISO = "12345",
                    CodigoReceitaFederal = "12345",
                    TenantId = _tenantId
                };

                _context.Paises.Add(brasil);
                _context.SaveChanges();
            }

            // EUA
            var eua = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Estados Unidos".ToLower()));
            if (eua == null)
            {
                eua = new Pais()
                {
                    Nome = "Estados Unidos",
                    Nacionalidade = "Americana",
                    Sigla2Caracteres = "US",
                    Sigla3Caracteres = "USA",
                    MascaraDoCodigoPostal = "99999",
                    CodigoInternacional = "1",
                    IsActive = true,
                    CodigoISO = "54321",
                    CodigoReceitaFederal = "54321",
                    TenantId = _tenantId
                };

                _context.Paises.Add(eua);
                _context.SaveChanges();
            }
        }

        private void CreateEstado()
        {
            // RJ
            var rj = _context.Estados.FirstOrDefault(x => x.Nome.ToLower().Contains("Rio de Janeiro".ToLower()));
            if (rj == null)
            {
                rj = new Estado()
                {
                    Nome = "Rio de Janeiro",
                    IsActive = true,
                    Pais = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Brasil".ToLower())),
                    Capital = "Rio de Janeiro",
                    CodigoIBGE = "123",
                    Sigla = "RJ",
                    TenantId = _tenantId
                };

                _context.Estados.Add(rj);
                _context.SaveChanges();
            }
        }

        private void CreateCidade()
        {
            // RJ
            var rj = _context.Cidades.FirstOrDefault(x => x.Nome.ToLower().Contains("Rio de Janeiro".ToLower()));
            if (rj == null)
            {
                rj = new Cidade()
                {
                    Nome = "Rio de Janeiro",
                    IsActive = true,
                    Estado = _context.Estados.FirstOrDefault(x => x.Nome.ToLower().Contains("Rio de Janeiro".ToLower())),
                    CodigoIBGE = "223",
                    TenantId = _tenantId
                };

                _context.Cidades.Add(rj);
                _context.SaveChanges();
            }

            // Duque de Caxias
            var dc = _context.Cidades.FirstOrDefault(x => x.Nome.ToLower().Contains("Duque de Caxias".ToLower()));
            if (dc == null)
            {
                dc = new Cidade()
                {
                    Nome = "Duque de Caxias",
                    IsActive = true,
                    Estado = _context.Estados.FirstOrDefault(x => x.Nome.ToLower().Contains("Rio de Janeiro".ToLower())),
                    CodigoIBGE = "243",
                    TenantId = _tenantId
                };

                _context.Cidades.Add(dc);
                _context.SaveChanges();
            }
        }

        private void CreateEstadoByStorage()
        {
            var appConnection = ConfigurationManager.ConnectionStrings["DbAddress"];
            var brasil = _context.Paises.FirstOrDefault(x => x.Nome.ToLower().Contains("Brasil".ToLower()));
            if (brasil != null && appConnection != null)
            {
                using (SqlConnection cnn = new SqlConnection(appConnection.ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter("SELECT [ufe_sg],[ufe_no],[ufe_codigo_ibge],[ufe_capital] FROM [AddressStorage].[dbo].[log_faixa_uf]", cnn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "log_faixa_uf");

                    foreach (DataRow row in ds.Tables["log_faixa_uf"].Rows)
                    {
                        var sigla = row["ufe_sg"].ToString();
                        var nome = row["ufe_no"].ToString();
                        var codigoIbge = row["ufe_codigo_ibge"].ToString();
                        var capital = row["ufe_capital"].ToString();

                        var estado = _context.Estados.FirstOrDefault(x => x.Sigla.ToLower() == sigla.ToLower());
                        if (estado == null)
                        {
                            _context.Estados.Add(new Estado
                            {
                                IsActive = true,
                                Nome = nome,
                                Sigla = sigla,
                                CodigoIBGE = codigoIbge,
                                Pais = brasil,
                                Capital = capital,
                                TenantId = _tenantId
                            });
                            _context.SaveChanges();
                        }

                    }
                }
            }
        }

        private void CreateCidadeByStorage()
        {
            var appConnection = ConfigurationManager.ConnectionStrings["DbAddress"];
            if (appConnection != null)
                using (SqlConnection cnn = new SqlConnection(appConnection.ConnectionString))
                {
                    SqlDataAdapter da = new SqlDataAdapter("SELECT [loc_no],[ufe_sg] FROM [AddressStorage].[dbo].[log_localidade] where loc_in_tipo_localidade = 'M'", cnn);
                    DataSet ds = new DataSet();
                    da.Fill(ds, "log_localidade");

                    foreach (DataRow row in ds.Tables["log_localidade"].Rows)
                    {
                        var siglaEstado = row["ufe_sg"].ToString();
                        var nome = row["loc_no"].ToString();

                        var est = _context.Estados.FirstOrDefault(x => x.Sigla.ToLower().Contains(siglaEstado));
                        if (est != null)
                        {
                            var cid = _context.Cidades.FirstOrDefault(x => x.Nome.ToLower() == nome.ToLower() && x.Estado.Sigla == est.Sigla);
                            if (cid == null)
                            {
                                _context.Cidades.Add(new Cidade
                                {
                                    Nome = nome,
                                    IsActive = true,
                                    Estado = est,
                                    TenantId = _tenantId
                                });
                                _context.SaveChanges();
                            }
                        }
                    }
                }
        }

        private void CreateBeneficiario()
        {
            // Criar responsavel pessoa fisica
            var pfResponsavel = _context.PessoasFisicas.FirstOrDefault(x => x.Nome == "Rosa das Dores");
            if (pfResponsavel == null)
            {
                pfResponsavel = new PessoaFisica
                {
                    Nome = "Rosa das Dores",
                    Sexo = SexoEnum.Feminino,
                    EstadoCivil = EstadoCivilEnum.Casado,
                    DataDeNascimento = DateTime.Now.AddYears(-40),
                    Nacionalidade = "Brasileira",
                    TenantId = _tenantId
                };

                _context.PessoasFisicas.Add(pfResponsavel);
                _context.SaveChanges();
            }

            // Criar pessoa fisica
            var pessoaFisica = _context.PessoasFisicas.FirstOrDefault(x => x.Nome == "Maria das Dores");
            if (pessoaFisica == null)
            {
                pessoaFisica = new PessoaFisica
                {
                    Nome = "Maria das Dores",
                    Sexo = SexoEnum.Feminino,
                    Nacionalidade = "Brasileira",
                    EstadoCivil = EstadoCivilEnum.Casado,
                    DataDeNascimento = DateTime.Now.AddYears(-10),
                    TenantId = _tenantId
                };

                _context.PessoasFisicas.Add(pessoaFisica);
                _context.SaveChanges();
            }

            var responsavel = _context.Responsaveis.FirstOrDefault(x => x.PessoaFisica.Nome == "Rosa das Dores");

            if (responsavel == null)
            {
                responsavel = new Responsavel
                {
                    PessoaFisica = pfResponsavel,
                    TipoDeResponsavel = TipoDeResponsavelEnum.PaiMae,
                    TenantId = _tenantId
                };

                _context.Responsaveis.Add(responsavel);
                _context.SaveChanges();
            }

            var titular = _context.Proponentes.FirstOrDefault(x => x.PessoaFisica.Nome == "Maria das Dores");
            if (titular == null)
            {
                // Criar dependente pessoa fisica
                var pfDependente = _context.PessoasFisicas.FirstOrDefault(x => x.Nome == "Magalli Alves");
                if (pfDependente == null)
                {
                    pfDependente = new PessoaFisica
                    {
                        Nome = "Magalli Alves",
                        Sexo = SexoEnum.Feminino,
                        EstadoCivil = EstadoCivilEnum.Solteiro,
                        DataDeNascimento = DateTime.Now.AddYears(-10),
                        Nacionalidade = "Brasileira",
                        TenantId = _tenantId
                    };

                    _context.PessoasFisicas.Add(pfDependente);
                    _context.SaveChanges();
                }

                var dependente = new Dependente
                {
                    PessoaFisica = pfDependente,
                    GrauDeParentesco = GrauDeParentescoEnum.Filho,
                    DeclaracaoDeNascidoVivo = "0001",
                    TipoDeBeneficiario = TipoDeBeneficiarioEnum.Dependente,
                    NomeDaMae = "Maria das Dores",
                    NumeroDoCartaoNacionalDeSaude = "0026665111",
                    TenantId = _tenantId,
                };

                _context.Dependentes.Add(dependente);
                _context.SaveChanges();


                titular = new ProponenteTitular
                {
                    PessoaFisica = pessoaFisica,
                    NomeDaMae = "Jurema Alves",
                    NumeroDoCartaoNacionalDeSaude = "11111.222.333",
                    TipoDeBeneficiario = TipoDeBeneficiarioEnum.Titular,
                    StatusBeneficiario = StatusTipoDeBeneficiarioTitularEnum.PreCadastradoPeloCorretor,
                    DeclaracaoDeNascidoVivo = "00007",
                    Responsavel = responsavel,
                    TenantId = _tenantId,
                    Dependentes = new List<Dependente>(),
                };


                var dependentes = new List<Dependente>();
                var dependenteCriado = _context.Dependentes.FirstOrDefault(x => x.PessoaFisica.Nome == "Magalli Alves");
                dependentes.Add(dependenteCriado);

                titular.Dependentes = dependentes;

                _context.Proponentes.Add(titular);
                _context.SaveChanges();
            }


        }

        private List<ItemDeDeclaracaoDeSaude> CreateItensDeDeclaracao(ProponenteTitular titular)
        {

            var perguntas = new Dictionary<string, TipoDeItemDeDeclaracaoEnum>();

            perguntas.Add("Tem ou teve bronquite?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve miopia, hipermetropia ou astigmatismo?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve estrabismo, catarata, ceratocone, glaucoma, lesões de retina ou córnea?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve sinusite, desvio de septo, hipertrofia de amigdalas, adenóides ou doença do ronco? Tem indicação de cirurgia?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve eplepsia, convulsão ou enxaqueca?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve varizes?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve pressão alta, infarto, angina, arritmia, aneurisma, trombose, embolia? Possui marcapasso ou stent?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença do sangue (anemia, hemofilia, púrpura ou outra)?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem diabetes? Qual tipo?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença do rim (Insuficiência renal, cálculo, infecção urinária de repetição, transplante ou outra)? Faz ou fez diálise ou hemodiálise?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem fimose? hidrocele, varicocele, testículo não descido (criptorquidia)?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve úlcera, refluxo gastroesofágico, hérnia de hiato, varizes de esôfago, esofagite, doença de Crohn, pólipos ou outros?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve cálculo na vesícula, doença do pâncreas?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem hepatite (B e C)? É portador do virus HIV (AIDS) e suas complicações?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença maligna (leucemia, lifomas, ou outros tipos de câncer)? Faz ou fez quimoterapia ou radioterapia?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doenças das articulações, dos músculos ou dos ossos (articulação da mandíbula, hérnia de disco, artrite, escoliose, bico de papagaio, lesão de joelho, ombro ou outras)? Alguma doença reumatológica(lúpus, artrite, reumatóide ou outra) ? ", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença endocrinológica (hipertiroidismo, hipotiroidismo, bócio ou nódulo da freóide, da hipófise, doença da supra-renal ou outra?)", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem algum tipo de hérnia (inguinal, umbilical, bolsa escrotal ou outra)?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença da próstata ou de infertilidade?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença de mama (aumento do volume, displasia, nódulo ou outra)?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença doença ginecológica (cisto de ovário, ovário policístico, endometriose, mioma, infertilidade ou outra?)", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Encontra-se internado ou em tratamento?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem ou teve doença congênita (desde que nasceu)? Alguma síndrome (Down, Marfan ou outra)?", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Já foi submetido algum tipo de cirurgia? Especifique.", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Tem indicação de submeter-se a algum tipo de cirurgia? Especifique.", TipoDeItemDeDeclaracaoEnum.Generico);
            perguntas.Add("Qual seu peso?", TipoDeItemDeDeclaracaoEnum.Peso);
            perguntas.Add("Qual sua altura?", TipoDeItemDeDeclaracaoEnum.Altura);

            var cod = 1;
            var itensDeDeclaracao = new List<ItemDeDeclaracaoDeSaude>();
            foreach (var pergunta in perguntas)
            {
                var itemDeDeclaracao = _context.ItensDeDeclaracaoDeSaude.FirstOrDefault(x => x.Pergunta == pergunta.Key);
                if (itemDeDeclaracao == null)
                {
                    itemDeDeclaracao = new ItemDeDeclaracaoDeSaude
                    {
                        IsActive = true,
                        Pergunta = pergunta.Key,
                        TipoDeItemDeDeclaracao = pergunta.Value,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };

                    cod = cod + 1;

                    _context.ItensDeDeclaracaoDeSaude.Add(itemDeDeclaracao);
                    _context.SaveChanges();
                }

                itensDeDeclaracao.Add(itemDeDeclaracao);
            }

            return itensDeDeclaracao;
        }

        private void CreateDeclaracaoDeSaudeAndItens()
        {
            var titular = _context.Proponentes.FirstOrDefault(x => x.PessoaFisica.Nome == "Maria das Dores");
            var itensDeDeclaracao = this.CreateItensDeDeclaracao(titular);

            var declaracaoDeSaude = _context.DeclaracoesDeSaude.FirstOrDefault(x => x.Nome == "Declaração EZSoft");
            if (declaracaoDeSaude == null && titular != null)
            {
                declaracaoDeSaude = new DeclaracaoDeSaude
                {
                    IsActive = true,
                    Nome = "Declaração EZSoft",
                    Titular = titular,
                    Dependentes = titular.Dependentes,
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaude>(),
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                declaracaoDeSaude.ItensDeDeclaracaoDeSaude = itensDeDeclaracao;
                _context.DeclaracoesDeSaude.Add(declaracaoDeSaude);
                _context.SaveChanges();
            }
        }

        private void CreateQuestionario()
        {
            var declaracaoDeSaude = _context.DeclaracoesDeSaude.FirstOrDefault(x => x.Nome == "Declaração EZSoft");
            var questionario = _context.QuestionariosDeDeclaracaoDeSaude.FirstOrDefault(x => x.Nome == "Questionário Amil");
            var amil = _context.Operadoras.FirstOrDefault(x => x.Nome == "Amil");

            if (questionario == null &&
                declaracaoDeSaude != null &&
                amil != null)
            {
                questionario = new QuestionarioDeDeclaracaoDeSaude
                {
                    IsActive = true,
                    Nome = "Questionário Amil",
                    EmpresaId = _empresaId,
                    TenantId = _tenantId,
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaude>(),
                    OperadoraId = amil.Id
                };

                questionario.ItensDeDeclaracaoDeSaude = declaracaoDeSaude.ItensDeDeclaracaoDeSaude;
                _context.QuestionariosDeDeclaracaoDeSaude.Add(questionario);
                _context.SaveChanges();
            }
        }

        private void CreatePlanoDeSaude()
        {
            var planoSafira207 = _context.PlanosDeSaude.FirstOrDefault(x => x.PlanoDeSaudeAns == PlanoDeSaudeAnsEnum.Safira207);
            if (planoSafira207 == null)
            {
                planoSafira207 = new PlanoDeSaude
                {
                    TenantId = _tenantId,
                    IsActive = true,
                    PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Safira207,
                    Abrangencia = AbrangenciaDoPlanoEnum.Estadual,
                    Acomodacao = AcomodacaoEnum.Enfermaria,
                    NumeroDeRegistro = "ANS 475.261/16-9",
                    Reembolso = true,
                    DescricaoDoReembolso = "Não há",
                    SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia,
                    EmpresaId = _empresaId
                };

                _context.PlanosDeSaude.Add(planoSafira207);
                _context.SaveChanges();
            }

            var rubi207 = _context.PlanosDeSaude.FirstOrDefault(x => x.PlanoDeSaudeAns == PlanoDeSaudeAnsEnum.Rubi207);
            if (rubi207 == null)
            {
                rubi207 = new PlanoDeSaude
                {
                    TenantId = _tenantId,
                    IsActive = true,
                    PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Rubi207,
                    Abrangencia = AbrangenciaDoPlanoEnum.Estadual,
                    Acomodacao = AcomodacaoEnum.Apartamento,
                    NumeroDeRegistro = "ANS 475.260/16-1",
                    Reembolso = true,
                    DescricaoDoReembolso = "Não há",
                    SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia,
                    EmpresaId = _empresaId
                };

                _context.PlanosDeSaude.Add(rubi207);
                _context.SaveChanges();
            }

            var planoDiamante207 = _context.PlanosDeSaude.FirstOrDefault(x => x.PlanoDeSaudeAns == PlanoDeSaudeAnsEnum.Diamante207);
            if (planoDiamante207 == null)
            {
                planoDiamante207 = new PlanoDeSaude
                {
                    TenantId = _tenantId,
                    IsActive = true,
                    PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Diamante207,
                    Abrangencia = AbrangenciaDoPlanoEnum.Nacional,
                    Acomodacao = AcomodacaoEnum.Apartamento,
                    NumeroDeRegistro = "ANS 475.388/16-7",
                    Reembolso = true,
                    DescricaoDoReembolso = "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação",
                    SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia,
                    EmpresaId = _empresaId
                };

                _context.PlanosDeSaude.Add(planoDiamante207);
                _context.SaveChanges();
            }

            var planoDiamante217 = _context.PlanosDeSaude.FirstOrDefault(x => x.PlanoDeSaudeAns == PlanoDeSaudeAnsEnum.Diamante217);
            if (planoDiamante217 == null)
            {
                planoDiamante217 = new PlanoDeSaude
                {
                    TenantId = _tenantId,
                    IsActive = true,
                    PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Diamante217,
                    Abrangencia = AbrangenciaDoPlanoEnum.Nacional,
                    Acomodacao = AcomodacaoEnum.Apartamento,
                    NumeroDeRegistro = "ANS 475.387/16-9",
                    Reembolso = true,
                    DescricaoDoReembolso = "3x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e internação",
                    SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia,
                    EmpresaId = _empresaId
                };

                _context.PlanosDeSaude.Add(planoDiamante217);
                _context.SaveChanges();
            }
        }

        private void CreateOperadora()
        {
            var amil = _context.Operadoras.FirstOrDefault(x => x.Nome == "Amil");
            var sulAmerica = _context.Operadoras.FirstOrDefault(x => x.Nome == "Sul América");

            if (amil == null)
            {
                var pessoaJuridica = new PessoaJuridica
                {
                    TenantId = _tenantId,
                    NomeFantasia = "Amil RJ",
                    RazaoSocial = "Amil Operadora de Saúde RJ Ltda"
                };

                _context.PessoasJuridicas.Add(pessoaJuridica);
                _context.SaveChanges();

                amil = new Operadora
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    CodigoAns = "326305",
                    Nome = "Amil",
                    PessoaJuridica = pessoaJuridica,
                    PessoaJuridicaId = pessoaJuridica.Id
                };

                _context.Operadoras.Add(amil);
                _context.SaveChanges();
            }

            if (sulAmerica == null)
            {

                var pessoaJuridica = new PessoaJuridica
                {
                    TenantId = _tenantId,
                    NomeFantasia = "Sul América RJ",
                    RazaoSocial = "Sul América Operadora de Saúde RJ Ltda"
                };

                _context.PessoasJuridicas.Add(pessoaJuridica);
                _context.SaveChanges();

                sulAmerica = new Operadora
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    CodigoAns = "006246",
                    Nome = "Sul América",
                    PessoaJuridica = pessoaJuridica,
                    PessoaJuridicaId = pessoaJuridica.Id
                };

                _context.Operadoras.Add(sulAmerica);
                _context.SaveChanges();
            }
        }

        private void CreateAssociacao()
        {
            var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.NomeFantasia.ToLower() == "AUSSESP".ToLower());

            if (pessoaJuridica == null)
            {
                pessoaJuridica = new PessoaJuridica
                {
                    TenantId = _tenantId,
                    NomeFantasia = "AUSSESP",
                    RazaoSocial = "Associação dos Usuários de Planos de Saúde do Estado de São Paulo"
                };

                _context.PessoasJuridicas.Add(pessoaJuridica);
                _context.SaveChanges();
            }

            var associacao = _context.Associacoes.FirstOrDefault(x => x.PessoaJuridicaId == pessoaJuridica.Id);

            if (associacao == null)
            {
                associacao = new Associacao
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaJuridica = pessoaJuridica,
                    PessoaJuridicaId = pessoaJuridica.Id
                };

                _context.Associacoes.Add(associacao);
                _context.SaveChanges();
            }
        }

        private void CreateCorretor()
        {
            var pessoaFisica = _context.PessoasFisicas.FirstOrDefault(x => x.Nome.ToLower() == "Jéssica Antunes".ToLower());

            if (pessoaFisica == null)
            {
                pessoaFisica = new PessoaFisica
                {
                    Nome = "Jéssica Antunes",
                    TenantId = _tenantId,
                    DataDeNascimento = DateTime.Now.AddYears(-30),
                    Sexo = SexoEnum.Feminino,
                    Nacionalidade = "Argentina",
                    EstadoCivil = EstadoCivilEnum.Divorciado
                };

                _context.PessoasFisicas.Add(pessoaFisica);
                _context.SaveChanges();

                var corretor = new Corretor
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaFisica = pessoaFisica,
                };

                _context.Corretores.Add(corretor);
                _context.SaveChanges();
            }
        }

        private void CreateCorretora()
        {
            var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.NomeFantasia.ToLower() == "EZLiv".ToLower());
            var corretoraExiste = _context.Corretoras.Any(x => x.PessoaJuridicaId == pessoaJuridica.Id);
            var corretor = _context.Corretores.FirstOrDefault();

            if (pessoaJuridica != null && !corretoraExiste)
            {
                var corretora = new Corretora
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaJuridica = pessoaJuridica,
                    Corretor = corretor,
                    Nome = pessoaJuridica.NomeFantasia
                };

                _context.Corretoras.Add(corretora);
                _context.SaveChanges();
            }
        }

        private void CreateAdministradora()
        {
            var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.NomeFantasia.ToLower() == "Safe Care".ToLower());

            if (pessoaJuridica == null)
            {
                pessoaJuridica = new PessoaJuridica
                {
                    TenantId = _tenantId,
                    NomeFantasia = "Safe Care",
                    RazaoSocial = "Safe Care"
                };

                _context.PessoasJuridicas.Add(pessoaJuridica);
                _context.SaveChanges();

                var administradora = new Administradora
                {
                    IsActive = true,
                    TenantId = _tenantId,
                    PessoaJuridica = pessoaJuridica,
                };

                _context.Administradoras.Add(administradora);
                _context.SaveChanges();
            }
        }

        private void CreateEstoqueParametros()
        {
            var unidadeMedida = _context.UnidadeMedidas.FirstOrDefault(x => x.Descricao.ToLower() == "unidade".ToLower());

            if (unidadeMedida == null)
            {
                unidadeMedida = new UnidadeMedida
                {
                    Descricao = "Unidade",
                    TenantId = _tenantId,
                    UnidadePadrao = UnidadePadraoEnum.Outros,
                    Valor = 1
                };

                _context.UnidadeMedidas.Add(unidadeMedida);
                _context.SaveChanges();
            }


            var natureza = _context.Naturezas.FirstOrDefault(x => x.Descricao.ToLower() == "planos de saúde".ToLower());
            if (natureza == null)
            {
                natureza = new Natureza
                {
                    Descricao = "Planos de Saúde",
                    TenantId = _tenantId
                };

                _context.Naturezas.Add(natureza);
                _context.SaveChanges();
            }
        }

        private void CreateProdutoDePlanoDeSaude()
        {
            var produto = _context.ProdutosDePlanoDeSaude.FirstOrDefault(x => x.Nome == "Especial 100");
            var planoDiamante207 = _context.PlanosDeSaude.FirstOrDefault(x => x.PlanoDeSaudeAns == PlanoDeSaudeAnsEnum.Diamante207);
            var operadora = _context.Operadoras.FirstOrDefault(x => x.Nome.ToLower() == "Amil".ToLower());
            var administradora = _context.Administradoras.FirstOrDefault(x => x.PessoaJuridica.NomeFantasia == "Safe Care".ToLower());
            var unidadeMedida = _context.UnidadeMedidas.FirstOrDefault(x => x.Descricao.ToLower() == "unidade".ToLower());
            var natureza = _context.Naturezas.FirstOrDefault(x => x.Descricao.ToLower() == "planos de saúde".ToLower());
            var questionario = _context.QuestionariosDeDeclaracaoDeSaude.FirstOrDefault(x => x.Nome.ToLower() == "Questionário Amil".ToLower());

            if (produto == null && planoDiamante207 != null)
            {
                if (natureza != null && unidadeMedida != null)
                {
                    produto = new ProdutoDePlanoDeSaude
                    {
                        IsActive = true,
                        TenantId = _tenantId,
                        PlanoDeSaudeId = planoDiamante207.Id,
                        OperadoraId = operadora.Id,
                        QuestionarioDeDeclaracaoDeSaudeId = questionario.Id,
                        Nome = "Especial 100",
                        IsValorAjustavel = false,
                        Valor = 100,
                        FormaDeContratacao = FormaDeContratacaoEnum.Adesao,
                        TipoDeProduto = TipoDeProdutoEnum.Virtual,
                        Descricao = "Melhor custo benefício do mercado nacional",
                        EmpresaId = _empresaId
                    };

                    if (natureza != null)
                        produto.NaturezaId = natureza.Id;

                    if (unidadeMedida != null)
                        produto.UnidadeMedidaId = unidadeMedida.Id;

                    if (administradora != null)
                        produto.AdministradoraId = administradora.Id;

                    _context.ProdutosDePlanoDeSaude.Add(produto);
                    _context.SaveChanges();
                }

            }
        }

        private void CreateEspecialidadeSync()
        {
            var especialidade = _context.EspecialidadesSync.FirstOrDefault(x => x.Nome.ToLower() == "Cardiologista".ToLower());

            if (especialidade == null)
            {
                especialidade = new EspecialidadeSync
                {
                    Nome = "Cardiologista",
                    EmpresaId = _empresaId,
                    TenantId = _tenantId,
                    IsActive = true,
                    Estado = "Rio de Janeiro",
                    Bairro = "Barra da Tijuca",
                    Especialidade = "Cardiologista",
                    NomeEstado = "Rio de Janeiro",
                    Email = "cardio@ezliv.com.br",
                    Complemento = "Sala 523",
                    Logradouro = "Avenida Embaixador Abelardo Bueno",
                    Municipio = "Rio de Janeiro",
                    HomePage = "ezliv.cardio.com.br",
                    Numero = "908",
                    Telefone1 = "(21) 9643-7800"
                };

                _context.EspecialidadesSync.Add(especialidade);
                _context.SaveChanges();
            }


            var natureza = _context.Naturezas.FirstOrDefault(x => x.Descricao.ToLower() == "planos de saúde".ToLower());
            if (natureza == null)
            {
                natureza = new Natureza
                {
                    Descricao = "Planos de Saúde",
                    TenantId = _tenantId
                };

                _context.Naturezas.Add(natureza);
                _context.SaveChanges();
            }
        }

        private void CreateContrato()
        {
            var contrato = _context.Contratos.FirstOrDefault(x => x.ProdutoDePlanoDeSaudeId == 1);

            if (contrato == null)
            {
                var produto = _context.ProdutosDePlanoDeSaude.FirstOrDefault(x => x.Nome == "Especial 100");

                contrato = new Contrato
                {
                    IsActive = true,
                    DataCriacao = Convert.ToDateTime("01/01/2017"),
                    DataInicioVigencia = Convert.ToDateTime("01/01/2017"),
                    DataFimVigencia = Convert.ToDateTime("01/01/2017").AddYears(1),
                    ProdutoDePlanoDeSaude = produto,
                    Conteudo = "Conteúdo do Contrato",
                    EmpresaId = _empresaId,
                    TenantId = _tenantId
                };

                _context.Contratos.Add(contrato);
                _context.SaveChanges();
            }
        }

        private void CreateFaixaEtaria()
        {
            this.AddFaixaEtaria("0 A 18", 0, 18);
            this.AddFaixaEtaria("19 A 23", 19, 23);
            this.AddFaixaEtaria("24 A 28", 24, 28);
            this.AddFaixaEtaria("29 A 33", 29, 33);
            this.AddFaixaEtaria("34 A 38", 34, 38);
            this.AddFaixaEtaria("39 A 43", 39, 43);
            this.AddFaixaEtaria("44 A 48", 44, 48);
            this.AddFaixaEtaria("49 A 53", 49, 53);
            this.AddFaixaEtaria("54 A 58", 54, 58);
            this.AddFaixaEtaria("Acima de 59", 59);
        }

        private void AddFaixaEtaria(string descricao, int idadeInicial, int? idadeFinal = null)
        {
            var faixaEtaria = _context.FaixasEtarias.FirstOrDefault(x => x.Descricao.ToLower() == descricao.ToLower());
            if (faixaEtaria == null)
            {
                faixaEtaria = new FaixaEtaria
                {
                    IsActive = true,
                    Descricao = descricao,
                    IdadeInicial = idadeInicial,
                    IdadeFinal = idadeFinal,
                    TenantId = _tenantId
                };

                _context.FaixasEtarias.Add(faixaEtaria);
                _context.SaveChanges();
            }
        }

        private void CreateValorPorFaixaEtaria()
        {
            var contrato = _context.Contratos.FirstOrDefault(x => x.ProdutoDePlanoDeSaudeId == 1);
            var faixaEtaria = _context.FaixasEtarias.FirstOrDefault(x => x.Descricao.ToLower() == "0 A 18".ToLower());

            if (contrato != null && faixaEtaria != null)
            {
                var valorPorFaixaEtaria =
                        _context.ValoresPorFaixasEtarias.FirstOrDefault(
                            x => x.Valor == 150 && x.ContratoId == contrato.Id && x.FaixaEtariaId == faixaEtaria.Id);

                if (valorPorFaixaEtaria == null)
                {
                    valorPorFaixaEtaria = new ValorPorFaixaEtaria
                    {
                        IsActive = true,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId,
                        Contrato = contrato,
                        FaixaEtaria = faixaEtaria,
                        Valor = 150
                    };

                    _context.ValoresPorFaixasEtarias.Add(valorPorFaixaEtaria);
                    _context.SaveChanges();
                }
            }
        }

        private void CreateGrupoPessoas()
        {
            var grupoClientes = _context.GrupoPessoas.FirstOrDefault(x => x.Nome.ToLower() == "Clientes".ToLower());
            if (grupoClientes == null)
            {
                grupoClientes = new GrupoPessoa()
                {
                    IsActive = true,
                    Nome = "Clientes",
                    TipoDePessoa = TipoPessoaEnum.Fisica,
                    TenantId = _tenantId
                };

                _context.GrupoPessoas.Add(grupoClientes);
                _context.SaveChanges();
            }

            var grupoFornecedores = _context.GrupoPessoas.FirstOrDefault(x => x.Nome.ToLower() == "Fornecedores".ToLower());
            if (grupoFornecedores == null)
            {
                grupoFornecedores = new GrupoPessoa()
                {
                    IsActive = true,
                    Nome = "Fornecedores",
                    TipoDePessoa = TipoPessoaEnum.Juridica,
                    TenantId = _tenantId
                };

                _context.GrupoPessoas.Add(grupoFornecedores);
                _context.SaveChanges();
            }

            var grupoMedicos = _context.GrupoPessoas.FirstOrDefault(x => x.Nome.ToLower() == "Médicos".ToLower());
            if (grupoMedicos == null)
            {
                grupoMedicos = new GrupoPessoa
                {
                    IsActive = true,
                    Nome = "Médicos",
                    TipoDePessoa = TipoPessoaEnum.Fisica,
                    TenantId = _tenantId
                };

                _context.GrupoPessoas.Add(grupoMedicos);
                _context.SaveChanges();
            }
        }

        private void CreateBancos()
        {
            // Banco Santander
            var santander = _context.Bancos.FirstOrDefault(x => x.Nome.ToLower().Contains("Santander".ToLower()));
            if (santander == null)
            {
                santander = new Banco()
                {
                    IsActive = true,
                    Nome = "Santander",
                    Codigo = "033",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Bancos.Add(santander);
                _context.SaveChanges();
            }

            // Banco Caixa Economica
            var caixa = _context.Bancos.FirstOrDefault(x => x.Nome.ToLower().Contains("Caixa".ToLower()));
            if (caixa == null)
            {
                caixa = new Banco()
                {
                    IsActive = true,
                    Nome = "Caixa Econômica Federal",
                    Codigo = "104",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Bancos.Add(caixa);
                _context.SaveChanges();
            }

            // Agencias do Santander
            var agencia1Santander = _context.Agencias.FirstOrDefault(x => x.Banco.Id == santander.Id && x.Codigo == "1792");
            if (agencia1Santander == null)
            {
                agencia1Santander = new Agencia()
                {
                    IsActive = true,
                    Banco = santander,
                    Codigo = "1792",
                    Nome = "Metropolitano",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Agencias.Add(agencia1Santander);
                _context.SaveChanges();
            }

            var agencia2Santander = _context.Agencias.FirstOrDefault(x => x.Banco.Id == santander.Id && x.Codigo == "1800");
            if (agencia2Santander == null)
            {
                agencia2Santander = new Agencia()
                {
                    IsActive = true,
                    Banco = santander,
                    Codigo = "1800",
                    Nome = "Barra Shopping",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Agencias.Add(agencia2Santander);
                _context.SaveChanges();
            }

            // Agencias da Caixa
            var agencia1Caixa = _context.Agencias.FirstOrDefault(x => x.Banco.Id == caixa.Id && x.Codigo == "0102");
            if (agencia1Caixa == null)
            {
                agencia1Caixa = new Agencia()
                {
                    IsActive = true,
                    Banco = caixa,
                    Codigo = "0102",
                    Nome = "Aberlado Bueno",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Agencias.Add(agencia1Caixa);
                _context.SaveChanges();
            }

            var agencia2Caixa = _context.Agencias.FirstOrDefault(x => x.Banco.Id == caixa.Id && x.Codigo == "0506");
            if (agencia2Caixa == null)
            {
                agencia2Caixa = new Agencia()
                {
                    IsActive = true,
                    Banco = caixa,
                    Codigo = "0506",
                    Nome = "Centro",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Agencias.Add(agencia2Caixa);
                _context.SaveChanges();
            }
        }

        private void CreateTratamentos()
        {
            // Sr
            var sr = _context.Tratamentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Sr".ToLower()));
            if (sr == null)
            {
                sr = new Tratamento()
                {
                    IsActive = true,
                    Descricao = "Sr",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Tratamentos.Add(sr);
                _context.SaveChanges();
            }

            // Sra
            var sra = _context.Tratamentos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Sra".ToLower()));
            if (sra == null)
            {
                sra = new Tratamento()
                {
                    IsActive = true,
                    Descricao = "Sra",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Tratamentos.Add(sra);
                _context.SaveChanges();
            }
        }

        private void CreateTipoDeContatos()
        {
            // Telefone
            var sr = _context.TipoDeContatos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Telefone".ToLower()));
            if (sr == null)
            {
                sr = new TipoDeContato()
                {
                    IsActive = true,
                    Descricao = "Telefone",
                    TipoDeContatoFixo = TipoDeContatoEnum.Telefone,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.TipoDeContatos.Add(sr);
                _context.SaveChanges();
            }

            // Fax
            var sra = _context.TipoDeContatos.FirstOrDefault(x => x.Descricao.ToLower().Contains("Fax".ToLower()));
            if (sra == null)
            {
                sra = new TipoDeContato()
                {
                    IsActive = true,
                    Descricao = "Fax",
                    TipoDeContatoFixo = TipoDeContatoEnum.Fax,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.TipoDeContatos.Add(sra);
                _context.SaveChanges();
            }

            // Email
            var email = _context.TipoDeContatos.FirstOrDefault(x => x.Descricao.ToLower().Contains("E-mail".ToLower()));
            if (email == null)
            {
                email = new TipoDeContato()
                {
                    IsActive = true,
                    Descricao = "E-mail",
                    TipoDeContatoFixo = TipoDeContatoEnum.Email,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.TipoDeContatos.Add(email);
                _context.SaveChanges();
            }
        }

        private void CreateTipoDeLogradouros()
        {
            // Avenida
            var sr = _context.TiposDeLogradouros.FirstOrDefault(x => x.Descricao.ToLower().Contains("Avenida".ToLower()));
            if (sr == null)
            {
                sr = new TipoDeLogradouro()
                {
                    IsActive = true,
                    Descricao = "Avenida",
                    TenantId = _tenantId
                };

                _context.TiposDeLogradouros.Add(sr);
                _context.SaveChanges();
            }

            // Rua
            var rua = _context.TiposDeLogradouros.FirstOrDefault(x => x.Descricao.ToLower().Contains("Rua".ToLower()));
            if (rua == null)
            {
                rua = new TipoDeLogradouro()
                {
                    IsActive = true,
                    Descricao = "Rua",
                    TenantId = _tenantId
                };

                _context.TiposDeLogradouros.Add(rua);
                _context.SaveChanges();
            }

            // Praça
            var praca = _context.TiposDeLogradouros.FirstOrDefault(x => x.Descricao.ToLower().Contains("Praça".ToLower()));
            if (praca == null)
            {
                praca = new TipoDeLogradouro()
                {
                    IsActive = true,
                    Descricao = "Praça",
                    TenantId = _tenantId
                };

                _context.TiposDeLogradouros.Add(praca);
                _context.SaveChanges();
            }
        }

        private void CreateConexoesSmtp()
        {
            // EZSoft
            var ezSoft = _context.ConexoesSMTP.FirstOrDefault(x => x.Nome.ToLower().Contains("EZSoft".ToLower()));
            if (ezSoft == null)
            {
                ezSoft = new ConexaoSMTP()
                {
                    Nome = "EZSoft",
                    RequerAutenticacao = true,
                    RequerConexaoCriptografada = true,
                    IsActive = true,
                    UsuarioAutenticacao = "smtppadrao@ezsoft.com.br",
                    SenhaAutenticacao = "Senha123",
                    Porta = 587,
                    ServidorSMTP = "smtp.uni5.net",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.ConexoesSMTP.Add(ezSoft);
                _context.SaveChanges();
            }

            // Gmail
            var gmail = _context.ConexoesSMTP.FirstOrDefault(x => x.Nome.ToLower().Contains("Gmail".ToLower()));
            if (gmail == null)
            {
                gmail = new ConexaoSMTP()
                {
                    Nome = "Gmail",
                    RequerAutenticacao = true,
                    RequerConexaoCriptografada = true,
                    IsActive = true,
                    UsuarioAutenticacao = "ezsoft0@gmail.com",
                    SenhaAutenticacao = "ezsoft2016",
                    Porta = 587,
                    ServidorSMTP = "smtp.gmail.com",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.ConexoesSMTP.Add(gmail);
                _context.SaveChanges();
            }
        }

        private void CreateTemplate()
        {
            var templatePadrao = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "Template Padrão".ToLower());
            if (templatePadrao == null)
            {
                templatePadrao = new Template
                {
                    IsActive = true,
                    Autor = "EZSoft",
                    Nome = "Template Padrão",
                    AutorUrl = "http://www.ezsoft.com.br",
                    Descricao = "Template padrão do módulo CMS.",
                    NomeDoArquivo = "templatePadrao",
                    Versao = "1.0",
                    Posicoes = "menu-top",
                    TiposDeWidgetSuportados = "CmsMenu",
                    TiposDePaginasSuportadas = "Default",
                    TipoDePaginaParaPaginaDefault = "Default",
                    TipoDePaginaParaPostDefault = "Default",
                    TipoDePaginaParaCategoriaDefault = "Default",
                    TipoDePaginaParaPaginaInicialDefault = "Default",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Templates.Add(templatePadrao);
                _context.SaveChanges();
            }

            var templatePrincipal = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "Principal".ToLower());
            if (templatePrincipal == null)
            {
                templatePrincipal = new Template
                {
                    IsActive = true,
                    Autor = "EzSoft",
                    Nome = "Principal",
                    AutorUrl = "http://www.ezsoft.com",
                    Descricao = "Template principal.",
                    NomeDoArquivo = "templatePrincipal",
                    Versao = "1.0",
                    Posicoes = "menu-top, sidebar, footer",
                    TiposDeWidgetSuportados = "CmsMenu",
                    TiposDePaginasSuportadas = "Home, Contato, QuemSomos, Login",
                    TipoDePaginaParaCategoriaDefault = "Login",
                    TipoDePaginaParaPaginaDefault = "Home",
                    TipoDePaginaParaPaginaInicialDefault = "Contato",
                    TipoDePaginaParaPostDefault = "QuemSomos",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Templates.Add(templatePrincipal);
                _context.SaveChanges();
            }

            var templateDoutorEz = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "DoutorEZ".ToLower());

            if (templateDoutorEz == null)
            {
                templateDoutorEz = new Template
                {
                    IsActive = true,
                    Autor = "Marcos Lima",
                    Nome = "DoutorEZ",
                    AutorUrl = "http://www.marcoslima.com",
                    Descricao = "Template aplicado no produto DoutorEZ.",
                    NomeDoArquivo = "DoutorEz",
                    Versao = "1.0",
                    Posicoes = "main-menu",
                    TiposDeWidgetSuportados = "CmsMenu",
                    TiposDePaginasSuportadas = "Home,Search,Info",
                    TipoDePaginaParaPaginaDefault = "Home",
                    TipoDePaginaParaPostDefault = "Home",
                    TipoDePaginaParaCategoriaDefault = "Home",
                    TipoDePaginaParaPaginaInicialDefault = "Home",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Templates.Add(templateDoutorEz);
                _context.SaveChanges();
            }
        }

        private void CreateSite()
        {
            // EZControl Frontend
            var ezfront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl FrontEnd".ToLower()));
            if (ezfront == null)
            {
                ezfront = new Site
                {
                    Nome = "EZControl FrontEnd",
                    IsActive = true,
                    TipoDePaginaParaCategoriaDefault = "Login",
                    TipoDePaginaParaPaginaDefault = "Home",
                    TipoDePaginaParaPaginaInicialDefault = "Contato",
                    TipoDePaginaParaPostDefault = "QuemSomos",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };
                ezfront.Hosts.Add(new Domain.CMS.Geral.Host
                {
                    IsPrincipal = true,
                    Url = "ezcontrolfrontendlocal.com",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                });

                var templatePadrao = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "Template Padrão".ToLower());

                if (templatePadrao != null)
                {
                    ezfront.TemplateDefault = templatePadrao;
                }

                _context.Sites.Add(ezfront);
                _context.SaveChanges();
            }

            // EZControl Frontend
            var ezremotofront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl Remoto FrontEnd".ToLower()));
            if (ezremotofront == null)
            {
                ezremotofront = new Site
                {
                    Nome = "EZControl Remoto FrontEnd",
                    IsActive = true,
                    TipoDePaginaParaCategoriaDefault = "Login",
                    TipoDePaginaParaPaginaDefault = "Home",
                    TipoDePaginaParaPaginaInicialDefault = "Contato",
                    TipoDePaginaParaPostDefault = "QuemSomos",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };
                ezremotofront.Hosts.Add(new Domain.CMS.Geral.Host
                {
                    IsPrincipal = true,
                    Url = "ezcontrolfrontend.ezsoft.com.br",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                });

                var templatePadrao = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "Template Padrão".ToLower());

                if (templatePadrao != null)
                {
                    ezremotofront.TemplateDefault = templatePadrao;
                }

                _context.Sites.Add(ezremotofront);
                _context.SaveChanges();
            }

            // DoutorEZ
            var drEz = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("DoutorEZ".ToLower()));
            if (drEz == null)
            {
                drEz = new Site
                {
                    Nome = "DoutorEZ",
                    IsActive = true,
                    TipoDePaginaParaCategoriaDefault = "Home",
                    TipoDePaginaParaPaginaDefault = "Home",
                    TipoDePaginaParaPaginaInicialDefault = "Home",
                    TipoDePaginaParaPostDefault = "Home",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };
                drEz.Hosts.Add(new Domain.CMS.Geral.Host
                {
                    IsPrincipal = true,
                    Url = "doutorezlocal.com",
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                });

                var templatePadrao = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "DoutorEZ".ToLower());

                if (templatePadrao != null)
                {
                    drEz.TemplateDefault = templatePadrao;
                }

                _context.Sites.Add(drEz);
                _context.SaveChanges();
            }
        }

        private void CreatePagina()
        {
            // EZControl Frontend
            var ezfront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl FrontEnd".ToLower()));

            if (ezfront != null)
            {
                // Menu Principal
                var pagina = _context.Paginas.FirstOrDefault(x => x.Titulo.ToLower().Contains("Quem Somos".ToLower()));
                if (pagina == null)
                {
                    pagina = new Pagina()
                    {
                        Titulo = "Quem Somos",
                        Slug = "quem-somos",
                        IsActive = true,
                        TipoDePublicacao = TipoDePublicacaoEnum.Pagina,
                        Conteudo = "<p>Conteúdo da página quem somos</p>",
                        Site = ezfront,
                        Historico = new List<HistoricoPublicacao>(),
                        DataDePublicacao = DateTime.Now,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };

                    pagina.Historico.Add(new HistoricoPublicacao()
                    {
                        Data = DateTime.Now,
                        IsActive = true,
                        Status = StatusDaPublicacaoEnum.Rascunho,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    });

                    _context.Paginas.Add(pagina);
                    _context.SaveChanges();
                }
            }
        }

        private void CreateMenu()
        {
            // EZControl Frontend
            var ezfront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl FrontEnd".ToLower()));

            if (ezfront != null)
            {
                // Menu Principal
                var mp = _context.Menus.FirstOrDefault(x => x.Nome.ToLower().Contains("Menu Principal Local".ToLower()));
                if (mp == null)
                {
                    mp = new Menu()
                    {
                        Nome = "Menu Principal Local",
                        IsActive = true,
                        Site = ezfront,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };

                    _context.Menus.Add(mp);
                    _context.SaveChanges();
                }
            }

            // EZControl Frontend Remoto
            var ezremotofront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl Remoto FrontEnd".ToLower()));

            if (ezremotofront != null)
            {
                // Menu Principal
                var mpr = _context.Menus.FirstOrDefault(x => x.Nome.ToLower().Contains("Menu Principal Remoto".ToLower()));
                if (mpr == null)
                {
                    mpr = new Menu()
                    {
                        Nome = "Menu Principal Remoto",
                        IsActive = true,
                        Site = ezremotofront,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };

                    _context.Menus.Add(mpr);
                    _context.SaveChanges();
                }
            }
        }

        private void CreateItemDeMenu()
        {
            // Menu Principal
            var mp = _context.Menus.FirstOrDefault(x => x.Nome.ToLower().Contains("Menu Principal".ToLower()));

            if (mp != null)
            {
                // Item de Menu
                var itemDeMenu = _context.ItensDeMenu.FirstOrDefault(x => x.Titulo.ToLower().Contains("Home".ToLower()));
                if (itemDeMenu == null)
                {
                    itemDeMenu = new ItemDeMenu()
                    {
                        Titulo = "Home",
                        IsActive = true,
                        DescricaoDoTitulo = "Página Inicial",
                        TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                        Menu = mp,
                        Url = "/",
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };

                    _context.ItensDeMenu.Add(itemDeMenu);
                    _context.SaveChanges();
                }
            }
        }

        private void CreateWidgetMenu()
        {
            // Menu Principal FrontEnd Local
            var wmp = _context.WidgetsMenu.FirstOrDefault(x => x.Titulo.ToLower().Contains("Menu Principal Local".ToLower()));

            if (wmp == null)
            {
                var mp = _context.Menus.FirstOrDefault(x => x.Nome.ToLower().Contains("Menu Principal Local".ToLower()));
                var ezfront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl FrontEnd".ToLower()));
                var templatePadrao = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "Template Padrão".ToLower());

                if (mp != null && ezfront != null && templatePadrao != null)
                {
                    wmp = new WidgetMenu()
                    {
                        Titulo = "Menu Principal Local",
                        ExibirTitulo = false,
                        Menu = mp,
                        IsActive = true,
                        Posicao = "menu-top",
                        Sistema = SistemaEnum.CMS,
                        Site = ezfront,
                        Template = templatePadrao,
                        Tipo = TipoDeWidgetEnum.CmsMenu,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };
                }

                _context.WidgetsMenu.Add(wmp);
                _context.SaveChanges();
            }

            // Menu Principal FrontEnd Remoto
            var wmpr = _context.WidgetsMenu.FirstOrDefault(x => x.Titulo.ToLower().Contains("Menu Principal Remoto".ToLower()));

            if (wmpr == null)
            {
                var mp2 = _context.Menus.FirstOrDefault(x => x.Nome.ToLower().Contains("Menu Principal Remoto".ToLower()));
                var ezremotofront = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Contains("EZControl Remoto FrontEnd".ToLower()));
                var templatePadrao2 = _context.Templates.FirstOrDefault(x => x.Nome.ToLower() == "Template Padrão".ToLower());

                if (mp2 != null && ezremotofront != null && templatePadrao2 != null)
                {
                    wmpr = new WidgetMenu()
                    {
                        Titulo = "Menu Principal Remoto",
                        ExibirTitulo = false,
                        Menu = mp2,
                        IsActive = true,
                        Posicao = "menu-top",
                        Sistema = SistemaEnum.CMS,
                        Site = ezremotofront,
                        Template = templatePadrao2,
                        Tipo = TipoDeWidgetEnum.CmsMenu,
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    };
                }

                _context.WidgetsMenu.Add(wmpr);
                _context.SaveChanges();
            }
        }

        private void CreateRegrasAgenda()
        {
            // Regra Tarefa
            var ag = _context.RegraTarefas.FirstOrDefault(x =>
                x.Sistema == SistemaEnum.Global
                && x.TipoDeEvento == TipoDeEventoEnum.Reuniao);
            if (ag == null)
            {
                ag = new RegraTarefa()
                {
                    IsActive = true,
                    Sistema = SistemaEnum.Global,
                    DiasAntesDoAgendamentoProibidoAlterar = 10,
                    DiasExibidosDesdeHojeAteAgendamento = 90,
                    DiasParaNovoAgendamentoPorParticipante = 15,
                    OverBook = false,
                    UnidadeDeTempo = 30,
                    TipoDeEvento = TipoDeEventoEnum.Reuniao,
                    VagasDisponibilizadasPorDia = 14,
                    VagasPorUnidadeDeTempo = 3,
                    VagasReservadasPorDia = 12,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.RegraTarefas.Add(ag);
                _context.SaveChanges();
            }

            // Regra Consulta
            var ac = _context.RegraConsultas.FirstOrDefault(
                x => x.Sistema == SistemaEnum.Global
                && x.TipoDeEvento == TipoDeEventoEnum.Tarefa);
            if (ac == null)
            {
                ac = new RegraConsulta()
                {
                    IsActive = true,
                    Sistema = SistemaEnum.Global,
                    DiasAntesDoAgendamentoProibidoAlterar = 10,
                    DiasExibidosDesdeHojeAteAgendamento = 90,
                    DiasParaNovoAgendamentoPorParticipante = 15,
                    OverBook = false,
                    UnidadeDeTempo = 30,
                    TipoDeEvento = TipoDeEventoEnum.Tarefa,
                    VagasDisponibilizadasPorDia = 14,
                    VagasPorUnidadeDeTempo = 3,
                    VagasReservadasPorDia = 12,
                    TenantId = _tenantId,
                    Medico = _context.Medicos.FirstOrDefault(),
                    Especialidade = _context.Especialidades.FirstOrDefault(),
                    EmpresaId = _empresaId
                };

                _context.RegraConsultas.Add(ac);
                _context.SaveChanges();
            }

            // Regra Reuniao
            var ar = _context.RegraReunioes.FirstOrDefault(
                x => x.Sistema == SistemaEnum.EZMedical
                && x.TipoDeEvento == TipoDeEventoEnum.Consulta);
            if (ar == null)
            {
                ar = new RegraReuniao()
                {
                    IsActive = true,
                    Sistema = SistemaEnum.EZMedical,
                    DiasAntesDoAgendamentoProibidoAlterar = 10,
                    DiasExibidosDesdeHojeAteAgendamento = 90,
                    DiasParaNovoAgendamentoPorParticipante = 15,
                    OverBook = false,
                    UnidadeDeTempo = 30,
                    TipoDeEvento = TipoDeEventoEnum.Consulta,
                    VagasDisponibilizadasPorDia = 14,
                    VagasPorUnidadeDeTempo = 3,
                    VagasReservadasPorDia = 12,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.RegraReunioes.Add(ar);
                _context.SaveChanges();
            }
        }

        private void CreateServico()
        {
            // Serviços
            var srv = _context.Servicos.FirstOrDefault(x => x.Nome.ToLower().Contains("Construção de site".ToLower()));
            if (srv == null)
            {
                srv = new Servico()
                {
                    Nome = "Construção de site",
                    Valor = 15454.98M,
                    IsActive = true,
                    IsValorAjustavel = true,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Servicos.Add(srv);
                _context.SaveChanges();
            }

            // Serviços
            var srv2 = _context.Servicos.FirstOrDefault(x => x.Nome.ToLower().Contains("Arquitetura de informação".ToLower()));
            if (srv2 == null)
            {
                srv2 = new Servico()
                {
                    Nome = "Arquitetura de informação",
                    Valor = 1554.28M,
                    IsActive = true,
                    IsValorAjustavel = true,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Servicos.Add(srv2);
                _context.SaveChanges();
            }
        }

        private void CreateProduto()
        {
            // Serviços
            var prod = _context.Produtos.FirstOrDefault(x => x.Nome.ToLower().Contains("EZCRM".ToLower()));
            if (prod == null)
            {
                prod = new Produto()
                {
                    Nome = "EZCRM",
                    Valor = 1545.98M,
                    IsActive = true,
                    Descricao = "Produto EZCRM",
                    TipoDeProduto = TipoDeProdutoEnum.Virtual,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Produtos.Add(prod);
                _context.SaveChanges();
            }

            // Serviços
            var prod2 = _context.Produtos.FirstOrDefault(x => x.Nome.ToLower().Contains("EZCMS".ToLower()));
            if (prod2 == null)
            {
                prod2 = new Produto()
                {
                    Nome = "EZCMS",
                    Valor = 1545.98M,
                    IsActive = true,
                    Descricao = "Produto EZCMS",
                    TipoDeProduto = TipoDeProdutoEnum.Virtual,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Produtos.Add(prod2);
                _context.SaveChanges();
            }
        }

        private void CreateEspecialidade()
        {
            var especialidade2251 = _context.Especialidades.FirstOrDefault(x => x.Codigo.ToLower().Contains("2251".ToLower()));
            if (especialidade2251 == null)
            {
                especialidade2251 = new Especialidade
                {
                    Codigo = "2251",
                    Nome = "Médicos Clínicos",
                    IsActive = true,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Especialidades.Add(especialidade2251);
                _context.SaveChanges();
            }

            var especialidade225133 = _context.Especialidades.FirstOrDefault(x => x.Codigo.ToLower().Contains("225133".ToLower()));
            if (especialidade225133 == null)
            {
                especialidade225133 = new Especialidade
                {
                    Codigo = "225133",
                    Nome = "Médico Psicanalista",
                    IsActive = true,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Especialidades.Add(especialidade225133);
                _context.SaveChanges();
            }
        }

        private void CreateMedico()
        {
            // Criar pessoa fisica
            var pessoaFisica = _context.PessoasFisicas.FirstOrDefault(x => x.Nome == "Rogério Assunção");
            if (pessoaFisica == null)
            {
                pessoaFisica = new PessoaFisica
                {
                    Nome = "Rogério Assunção",
                    TenantId = _tenantId
                };

                _context.PessoasFisicas.Add(pessoaFisica);
                _context.SaveChanges();
            }

            var especialidade225120 = _context.Especialidades.FirstOrDefault(x => x.Codigo.ToLower().Contains("225120".ToLower()));
            if (especialidade225120 == null)
            {
                especialidade225120 = new Especialidade
                {
                    Codigo = "225120",
                    Nome = "Cardiologista",
                    IsActive = true,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                };

                _context.Especialidades.Add(especialidade225120);
                _context.SaveChanges();
            }

            var especialidades = new List<Especialidade>();
            var especialidade = _context.Especialidades.FirstOrDefault(x => x.Codigo.ToLower().Contains("225120".ToLower()));
            especialidades.Add(especialidade);

            var medico = _context.Medicos.FirstOrDefault(x => x.PessoaFisica.Nome.ToLower().Contains("Rogério Assunção".ToLower()));
            if (medico == null)
            {
                medico = new Medico
                {
                    IsActive = true,
                    PessoaFisica = pessoaFisica,
                    Picture = new Picture
                    {
                        IsActive = true,
                        Bytes = new byte[10],
                        TenantId = _tenantId,
                        EmpresaId = _empresaId
                    },
                    Especialidades = new List<Especialidade>(),
                    TenantId = _tenantId,
                };

                medico.Especialidades = especialidades;

                _context.Medicos.Add(medico);
                _context.SaveChanges();
            }
        }
    }
}