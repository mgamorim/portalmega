﻿using Abp.Authorization;
using Abp.MultiTenancy;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EntityFramework;
using System.Linq;
using Abp.Dependency;
using Castle.Windsor;
using EZ.EZControl.Authorization;
using EZ.EZControl.Migrations.Seed.Tenants;

namespace EZ.EZControl.Migrations.Seed.EZSeeds
{
    public class EzSeedAdminEzsoftComBr : EzSeedProfileBase
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private int _empresaId { get; set; }

        public EzSeedAdminEzsoftComBr(EZControlDbContext context, int tenantId)
            : base(context, tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public override void Create()
        {
            CreateEmpresas();
            UpdateAdminUserPermissions();
        }

        private void CreateEmpresas()
        {
            new TenantEmpresaBuilder(_context, _tenantId).CreateEzSoft();
        }

        private void UpdateAdminUserPermissions()
        {
            var clienteEz = GetClienteEz();

            var empresa1 = _context.Empresas.FirstOrDefault(x => x.RazaoSocial == "EZSoft Ltda");

            var adminUser = GetAdminUser();
            if (adminUser != null)
            {
                AssociarUsuarioAEmpresa(adminUser, clienteEz, empresa1);
            }

            Role adminRole = GetAdminRole();

            bool isMultiTenantEnabled = true;
            var permissons = PermissionFinder.GetAllPermissions(new AppAuthorizationProvider(isMultiTenantEnabled));

            foreach (var permission in permissons)
            {
                AssociarPermissaoEmpresaPorRole(empresa1, adminRole, permission.Name);
            }
        }
    }
}