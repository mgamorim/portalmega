﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.EntityFramework;
using System.Linq;

namespace EZ.EZControl.Migrations.Seed.Tenants
{
    public class TenantEmpresaBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;

        public TenantEmpresaBuilder(EZControlDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public int CreateEzSoft()
        {
            // Criar empresas
            var query1 = from e in _context.Empresas
                         join pj in _context.PessoasJuridicas on e.Id equals pj.Id
                         where pj.RazaoSocial == "EZSoft Ltda"
                         select e;

            var empresa1 = query1.FirstOrDefault();
            if (empresa1 == null)
            {
                empresa1 = new Empresa
                {
                    IsActive = true,
                    RazaoSocial = "EZSoft Ltda",
                    NomeFantasia = "EZSoft",
                    TenantId = _tenantId
                };

                _context.Empresas.Add(empresa1);
                _context.SaveChanges();

                // Criar pessoa juridica
                var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.NomeFantasia == "EZSoft");
                if (pessoaJuridica == null)
                {
                    pessoaJuridica = new PessoaJuridica
                    {
                        RazaoSocial = "EZSoft Ltda",
                        NomeFantasia = "EZSoft",
                        TenantId = _tenantId
                    };

                    _context.PessoasJuridicas.Add(pessoaJuridica);
                    _context.SaveChanges();
                }


                // Criar clienteEz
                var clienteEz = _context.ClientesEZ.FirstOrDefault(x => x.Nome.Contains("EZSoft"));
                if (clienteEz == null)
                {
                    clienteEz = new ClienteEZ
                    {
                        IsActive = true,
                        RazaoSocial = "EZSoft Ltda",
                        Nome = "EZSoft",
                        PessoaId = pessoaJuridica.Id,
                        TenantId = _tenantId,
                    };

                    _context.SaveChanges();
                }

                empresa1.PessoaJuridicaId = pessoaJuridica.Id;
                empresa1.ClienteEZ = clienteEz;
                _context.SaveChanges();
            }

            return empresa1.Id;
        }

        public int CreateEmpresaExemplo()
        {
            // Criar empresas
            var query1 = from e in _context.Empresas
                         where e.RazaoSocial == "Empresa Exemplo Ltda"
                         select e;

            var empresa1 = query1.FirstOrDefault();
            if (empresa1 == null)
            {
                empresa1 = new Empresa
                {
                    IsActive = true,
                    RazaoSocial = "Empresa Exemplo Ltda",
                    NomeFantasia = "Empresa Exemplo",
                    TenantId = _tenantId
                };

                _context.Empresas.Add(empresa1);
                _context.SaveChanges();

                // Criar pessoa juridica
                var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.RazaoSocial == "Empresa Exemplo Ltda");
                if (pessoaJuridica == null)
                {
                    pessoaJuridica = new PessoaJuridica
                    {
                        RazaoSocial = "Empresa Exemplo Ltda",
                        NomeFantasia = "Empresa Exemplo",
                        TenantId = _tenantId
                    };

                    _context.PessoasJuridicas.Add(pessoaJuridica);
                    _context.SaveChanges();
                }


                // Criar clienteEz
                var clienteEz = _context.ClientesEZ.FirstOrDefault(x => x.Nome.Contains("Empresa Exemplo"));
                if (clienteEz == null)
                {
                    clienteEz = new ClienteEZ
                    {
                        IsActive = true,
                        RazaoSocial = "Empresa Exemplo Ltda",
                        Nome = "Empresa Exemplo",
                        PessoaId = pessoaJuridica.Id,
                        TenantId = _tenantId,
                    };

                    _context.SaveChanges();
                }

                empresa1.PessoaJuridicaId = pessoaJuridica.Id;
                empresa1.ClienteEZ = clienteEz;
                _context.SaveChanges();
            }

            return empresa1.Id;
        }

        public int CreateEZLiv()
        {
            // Criar empresas
            var query1 = from e in _context.Empresas
                         where e.RazaoSocial == "EZLiv Ltda"
                         select e;

            var empresa1 = query1.FirstOrDefault();
            if (empresa1 == null)
            {
                empresa1 = new Empresa
                {
                    IsActive = true,
                    RazaoSocial = "EZLiv Ltda",
                    NomeFantasia = "EZLiv",
                    TenantId = _tenantId,
                };

                _context.Empresas.Add(empresa1);
                _context.SaveChanges();

                // Criar pessoa juridica
                var pessoaJuridica = _context.PessoasJuridicas.FirstOrDefault(x => x.RazaoSocial == "EZLiv Ltda");
                if (pessoaJuridica == null)
                {
                    pessoaJuridica = new PessoaJuridica
                    {
                        RazaoSocial = "EZLiv Ltda",
                        NomeFantasia = "EZLiv",
                        TenantId = _tenantId
                    };

                    _context.PessoasJuridicas.Add(pessoaJuridica);
                    _context.SaveChanges();
                }

                // Criar clienteEz
                var clienteEz = _context.ClientesEZ.FirstOrDefault(x => x.Nome.Contains("EZLiv"));
                if (clienteEz == null)
                {
                    clienteEz = new ClienteEZ
                    {
                        IsActive = true,
                        RazaoSocial = "EZLiv Ltda",
                        Nome = "EZLiv",
                        PessoaId = pessoaJuridica.Id,
                        TenantId = _tenantId,
                    };

                    _context.SaveChanges();
                }

                empresa1.ClienteEZ = clienteEz;
                empresa1.PessoaJuridicaId = pessoaJuridica.Id;

                _context.SaveChanges();
            }

            return empresa1.Id;
        }
    }
}
