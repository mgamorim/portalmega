namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddFilterTitular : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        TitularId = c.Int(nullable: false),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        Aceite = c.Boolean(nullable: false),
                        TipoDeHomologacao = c.Int(nullable: false),
                        DataHoraDaHomologacao = c.DateTime(),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        UltimoContratoAceitoId = c.Int(),
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        AssociacaoId = c.Int(),
                        InicioDeVigencia = c.DateTime(),
                        TipoDeProposta = c.Int(nullable: false),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PropostaDeContratacao_TitularFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
        }
        
        public override void Down()
        {
            AlterTableAnnotations(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        TitularId = c.Int(nullable: false),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        Aceite = c.Boolean(nullable: false),
                        TipoDeHomologacao = c.Int(nullable: false),
                        DataHoraDaHomologacao = c.DateTime(),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        UltimoContratoAceitoId = c.Int(),
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        AssociacaoId = c.Int(),
                        InicioDeVigencia = c.DateTime(),
                        TipoDeProposta = c.Int(nullable: false),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PropostaDeContratacao_TitularFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
