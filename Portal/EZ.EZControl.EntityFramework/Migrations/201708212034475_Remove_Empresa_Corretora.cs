namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Empresa_Corretora : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_Corretora", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_Corretora", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Sau_ProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PlanoDeSaudeId = c.Int(nullable: false),
                        AdministradoraId = c.Int(),
                        OperadoraId = c.Int(nullable: false),
                        CarenciaEspecial = c.Boolean(nullable: false),
                        DescricaoDaCarenciaEspecial = c.String(),
                        Acompanhante = c.Boolean(nullable: false),
                        DescricaoDoAcompanhante = c.String(),
                        CoberturaExtra = c.Boolean(nullable: false),
                        DescricaoDaCoberturaExtra = c.String(),
                        RedeCredenciadaId = c.Int(),
                        FormaDeContratacao = c.Int(nullable: false),
                        QuestionarioDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        NumeroDeDiasParaEncerrarAsPropostasRelacionadas = c.Int(),
                        Formulario = c.Int(nullable: false),
                        CartaDeOrientacao = c.String(),
                        DeclaracaoDoBeneficiarioId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProdutoDePlanoDeSaude_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Etq_Produto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Descricao = c.String(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsValorAjustavel = c.Boolean(nullable: false),
                        TipoDeProduto = c.Int(nullable: false),
                        NaturezaId = c.Int(),
                        UnidadeMedidaId = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ImagemId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Produto_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_Corretora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CorretorId = c.Int(),
                        PessoaJuridicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Corretora_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Sau_Corretora", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_Corretora", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Sau_Corretora",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        CorretorId = c.Int(),
                        PessoaJuridicaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Corretora_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Etq_Produto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Nome = c.String(nullable: false, maxLength: 100),
                        Descricao = c.String(nullable: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsValorAjustavel = c.Boolean(nullable: false),
                        TipoDeProduto = c.Int(nullable: false),
                        NaturezaId = c.Int(),
                        UnidadeMedidaId = c.Int(),
                        EmpresaId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ImagemId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Produto_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Sau_ProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PlanoDeSaudeId = c.Int(nullable: false),
                        AdministradoraId = c.Int(),
                        OperadoraId = c.Int(nullable: false),
                        CarenciaEspecial = c.Boolean(nullable: false),
                        DescricaoDaCarenciaEspecial = c.String(),
                        Acompanhante = c.Boolean(nullable: false),
                        DescricaoDoAcompanhante = c.String(),
                        CoberturaExtra = c.Boolean(nullable: false),
                        DescricaoDaCoberturaExtra = c.String(),
                        RedeCredenciadaId = c.Int(),
                        FormaDeContratacao = c.Int(nullable: false),
                        QuestionarioDeDeclaracaoDeSaudeId = c.Int(nullable: false),
                        NumeroDeDiasParaEncerrarAsPropostasRelacionadas = c.Int(),
                        Formulario = c.Int(nullable: false),
                        CartaDeOrientacao = c.String(),
                        DeclaracaoDoBeneficiarioId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ProdutoDePlanoDeSaude_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Sau_Corretora", "EmpresaId");
            AddForeignKey("dbo.Sau_Corretora", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
