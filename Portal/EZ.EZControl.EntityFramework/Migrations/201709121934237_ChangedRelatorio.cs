namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedRelatorio : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Glb_Relatorio", "Conteudo", c => c.String(storeType: "ntext"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Glb_Relatorio", "Conteudo", c => c.String(nullable: false, storeType: "ntext"));
        }
    }
}
