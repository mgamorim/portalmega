// <auto-generated />
namespace EZ.EZControl.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Add_Carencia : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_Carencia));
        
        string IMigrationMetadata.Id
        {
            get { return "201706261752458_Add_Carencia"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
