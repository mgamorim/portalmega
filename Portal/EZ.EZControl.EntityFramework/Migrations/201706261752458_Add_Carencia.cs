namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Carencia : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_Carencia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OperadoraId = c.Int(nullable: false),
                        Cobertura = c.String(nullable: false),
                        Descricao = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Carencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Carencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Operadora", t => t.OperadoraId)
                .Index(t => t.OperadoraId)
                .Index(t => t.EmpresaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_Carencia", "OperadoraId", "dbo.Sau_Operadora");
            DropForeignKey("dbo.Sau_Carencia", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_Carencia", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Carencia", new[] { "OperadoraId" });
            DropTable("dbo.Sau_Carencia",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Carencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Carencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
