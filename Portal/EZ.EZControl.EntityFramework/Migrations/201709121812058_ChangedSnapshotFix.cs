namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedSnapshotFix : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sau_SnapshotPropostaDeContratacao", "Json", c => c.Binary(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sau_SnapshotPropostaDeContratacao", "Json", c => c.String(nullable: false));
        }
    }
}
