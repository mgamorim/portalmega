namespace EZ.EZControl.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ChangedPodutoDePlanoDeSaude_AddFormulario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "Formulario", c => c.Int(nullable: false));
            Sql("UPDATE dbo.Sau_ProdutoDePlanoDeSaude SET Formulario = 3");
        }

        public override void Down()
        {
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "Formulario");
        }
    }
}
