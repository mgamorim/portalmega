namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Logo_Entidade : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_Associacao", "Instrucoes", c => c.String());
            AddColumn("dbo.Sau_Associacao", "ImagemId", c => c.Int());
            CreateIndex("dbo.Sau_Associacao", "ImagemId");
            AddForeignKey("dbo.Sau_Associacao", "ImagemId", "dbo.Cor_Arquivo", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_Associacao", "ImagemId", "dbo.Cor_Arquivo");
            DropIndex("dbo.Sau_Associacao", new[] { "ImagemId" });
            DropColumn("dbo.Sau_Associacao", "ImagemId");
            DropColumn("dbo.Sau_Associacao", "Instrucoes");
        }
    }
}
