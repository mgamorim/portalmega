namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Empresa_De_Proposta_De_Contratacao : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        Aceite = c.Boolean(nullable: false),
                        TipoDeHomologacao = c.Int(nullable: false),
                        DataHoraDaHomologacao = c.DateTime(),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        ChancelaId = c.Int(),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        TitularId = c.Int(nullable: false),
                        UltimoContratoAceitoId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PropostaDeContratacao_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AddColumn("dbo.Sau_PropostaDeContratacao", "CorretoraId", c => c.Int());
            CreateIndex("dbo.Sau_PropostaDeContratacao", "CorretoraId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "CorretoraId", "dbo.Sau_Corretora", "Id");
            DropColumn("dbo.Sau_PropostaDeContratacao", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "EmpresaId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "CorretoraId", "dbo.Sau_Corretora");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "CorretoraId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "CorretoraId");
            AlterTableAnnotations(
                "dbo.Sau_PropostaDeContratacao",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        Aceite = c.Boolean(nullable: false),
                        TipoDeHomologacao = c.Int(nullable: false),
                        DataHoraDaHomologacao = c.DateTime(),
                        ObservacaoHomologacao = c.String(),
                        DataHoraDoAceite = c.DateTime(),
                        CorretorId = c.Int(nullable: false),
                        CorretoraId = c.Int(),
                        PedidoId = c.Int(),
                        StatusDaProposta = c.Int(nullable: false),
                        PassoDaProposta = c.Int(nullable: false),
                        FormaDeContratacao = c.Int(nullable: false),
                        VigenciaId = c.Int(),
                        ChancelaId = c.Int(),
                        TenantId = c.Int(),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ContratoVigenteNoAceiteId = c.Int(),
                        ProdutoId = c.Int(),
                        ResponsavelId = c.Int(),
                        TitularId = c.Int(nullable: false),
                        UltimoContratoAceitoId = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PropostaDeContratacao_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Sau_PropostaDeContratacao", "EmpresaId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
