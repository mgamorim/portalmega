namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemovidoAditivo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_AditivoReport", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_AditivoReport", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaude_Id", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Aditivo", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropIndex("dbo.Sau_Aditivo", new[] { "ProdutoId" });
            DropIndex("dbo.Sau_Aditivo", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Aditivo", new[] { "ProdutoDePlanoDeSaude_Id" });
            DropIndex("dbo.Sau_AditivoReport", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_AditivoReport", new[] { "EmpresaId" });
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "Formulario");
            DropColumn("dbo.Sau_Associacao", "Formulario");
            DropTable("dbo.Sau_Aditivo",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Aditivo_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Aditivo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.Sau_AditivoReport",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AditivoReport_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AditivoReport_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Sau_AditivoReport",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        EZLivReport = c.Int(nullable: false),
                        TipoDeAditivo = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AditivoReport_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AditivoReport_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sau_Aditivo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProdutoId = c.Int(nullable: false),
                        Titulo = c.String(nullable: false),
                        Descricao = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        IsEspecial = c.Boolean(nullable: false),
                        IsCarencia = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ProdutoDePlanoDeSaude_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Aditivo_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Aditivo_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Sau_Associacao", "Formulario", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "Formulario", c => c.Int(nullable: false));
            CreateIndex("dbo.Sau_AditivoReport", "EmpresaId");
            CreateIndex("dbo.Sau_AditivoReport", "ProdutoDePlanoDeSaudeId");
            CreateIndex("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaude_Id");
            CreateIndex("dbo.Sau_Aditivo", "EmpresaId");
            CreateIndex("dbo.Sau_Aditivo", "ProdutoId");
            AddForeignKey("dbo.Sau_Aditivo", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            AddForeignKey("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaude_Id", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            AddForeignKey("dbo.Sau_AditivoReport", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            AddForeignKey("dbo.Sau_AditivoReport", "EmpresaId", "dbo.Glb_Empresa", "Id");
            AddForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa", "Id", cascadeDelete: true);
        }
    }
}
