namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_PropostaDeContratacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "AceiteCorretor", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sau_PropostaDeContratacao", "DataAceiteCorretor", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_PropostaDeContratacao", "DataAceiteCorretor");
            DropColumn("dbo.Sau_PropostaDeContratacao", "AceiteCorretor");
        }
    }
}
