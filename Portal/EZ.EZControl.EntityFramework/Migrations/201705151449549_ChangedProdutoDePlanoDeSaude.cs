namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedProdutoDePlanoDeSaude : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "AssociacaoId", "dbo.Sau_Associacao");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude", new[] { "AssociacaoId" });
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "AssociacaoId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "AssociacaoId", c => c.Int());
            CreateIndex("dbo.Sau_ProdutoDePlanoDeSaude", "AssociacaoId");
            AddForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", "AssociacaoId", "dbo.Sau_Associacao", "Id");
        }
    }
}
