namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class EditUnidadeMedidaENaturezaRemovidaEmpresa : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Etq_Natureza", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Etq_UnidadeMedida", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Etq_Natureza", new[] { "EmpresaId" });
            DropIndex("dbo.Etq_UnidadeMedida", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Etq_Natureza",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Natureza_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Etq_UnidadeMedida",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnidadePadrao = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_UnidadeMedida_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Etq_Natureza", "EmpresaId");
            DropColumn("dbo.Etq_UnidadeMedida", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Etq_UnidadeMedida", "EmpresaId", c => c.Int(nullable: false));
            AddColumn("dbo.Etq_Natureza", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Etq_UnidadeMedida",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnidadePadrao = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_UnidadeMedida_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Etq_Natureza",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        Descricao = c.String(nullable: false, maxLength: 150),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Natureza_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Etq_UnidadeMedida", "EmpresaId");
            CreateIndex("dbo.Etq_Natureza", "EmpresaId");
            AddForeignKey("dbo.Etq_UnidadeMedida", "EmpresaId", "dbo.Glb_Empresa", "Id");
            AddForeignKey("dbo.Etq_Natureza", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
