namespace EZ.EZControl.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddPropostaDeContratacaoInRespostaDoItemDeDeclaracao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "PropostaDeContratacaoId", c => c.Int(nullable: true));
            CreateIndex("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "PropostaDeContratacaoId");
            AddForeignKey("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "PropostaDeContratacaoId", "dbo.Sau_PropostaDeContratacao", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "PropostaDeContratacaoId", "dbo.Sau_PropostaDeContratacao");
            DropIndex("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", new[] { "PropostaDeContratacaoId" });
            DropColumn("dbo.Sau_RespostaDoItemDeDeclaracaoDeSaude", "PropostaDeContratacaoId");
        }
    }
}
