namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedBeneficiario : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_BeneficiarioBase", "AdministradoraId", "dbo.Sau_Administradora");
            DropForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora");
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "AdministradoraId" });
            DropIndex("dbo.Sau_BeneficiarioBase", new[] { "CorretoraId" });
            CreateTable(
                "dbo.Sau_Administradora_Beneficiario",
                c => new
                    {
                        BeneficiarioId = c.Int(nullable: false),
                        AdministradoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BeneficiarioId, t.AdministradoraId })
                .ForeignKey("dbo.Sau_BeneficiarioBase", t => t.BeneficiarioId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Administradora", t => t.AdministradoraId, cascadeDelete: true)
                .Index(t => t.BeneficiarioId)
                .Index(t => t.AdministradoraId);
            
            CreateTable(
                "dbo.Sau_Corretora_Beneficiario",
                c => new
                    {
                        BeneficiarioId = c.Int(nullable: false),
                        CorretoraId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BeneficiarioId, t.CorretoraId })
                .ForeignKey("dbo.Sau_BeneficiarioBase", t => t.BeneficiarioId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Corretora", t => t.CorretoraId, cascadeDelete: true)
                .Index(t => t.BeneficiarioId)
                .Index(t => t.CorretoraId);
            
            DropColumn("dbo.Sau_BeneficiarioBase", "AdministradoraId");
            DropColumn("dbo.Sau_BeneficiarioBase", "CorretoraId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_BeneficiarioBase", "CorretoraId", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_BeneficiarioBase", "AdministradoraId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sau_Corretora_Beneficiario", "CorretoraId", "dbo.Sau_Corretora");
            DropForeignKey("dbo.Sau_Corretora_Beneficiario", "BeneficiarioId", "dbo.Sau_BeneficiarioBase");
            DropForeignKey("dbo.Sau_Administradora_Beneficiario", "AdministradoraId", "dbo.Sau_Administradora");
            DropForeignKey("dbo.Sau_Administradora_Beneficiario", "BeneficiarioId", "dbo.Sau_BeneficiarioBase");
            DropIndex("dbo.Sau_Corretora_Beneficiario", new[] { "CorretoraId" });
            DropIndex("dbo.Sau_Corretora_Beneficiario", new[] { "BeneficiarioId" });
            DropIndex("dbo.Sau_Administradora_Beneficiario", new[] { "AdministradoraId" });
            DropIndex("dbo.Sau_Administradora_Beneficiario", new[] { "BeneficiarioId" });
            DropTable("dbo.Sau_Corretora_Beneficiario");
            DropTable("dbo.Sau_Administradora_Beneficiario");
            CreateIndex("dbo.Sau_BeneficiarioBase", "CorretoraId");
            CreateIndex("dbo.Sau_BeneficiarioBase", "AdministradoraId");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "CorretoraId", "dbo.Sau_Corretora", "Id");
            AddForeignKey("dbo.Sau_BeneficiarioBase", "AdministradoraId", "dbo.Sau_Administradora", "Id");
        }
    }
}
