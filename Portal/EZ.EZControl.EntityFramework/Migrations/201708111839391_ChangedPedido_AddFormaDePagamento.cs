namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedPedido_AddFormaDePagamento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vnd_Pedido", "FormaDePagamento", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Vnd_Pedido", "FormaDePagamento");
        }
    }
}
