namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class AddOpcoesAdicionaisParaProdutoDePlanoDeSaude : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 100, unicode: false),
                        Descricao = c.String(maxLength: 1024, unicode: false),
                        Valor = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tipo = c.Int(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OpcaoAdicionalDeProdutoDePlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OpcaoAdicionalDeProdutoDePlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OpcaoAdicionalDeProdutoDePlanoDeSaude_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EmpresaId);
            
            CreateTable(
                "dbo.PropostaDeContratacaoOpcaoAdicionalDeProdutoDePlanoDeSaude",
                c => new
                    {
                        PropostaDeContratacao_Id = c.Int(nullable: false),
                        OpcaoAdicionalDeProdutoDePlanoDeSaude_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PropostaDeContratacao_Id, t.OpcaoAdicionalDeProdutoDePlanoDeSaude_Id })
                .ForeignKey("dbo.Sau_PropostaDeContratacao", t => t.PropostaDeContratacao_Id, cascadeDelete: true)
                .ForeignKey("dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude", t => t.OpcaoAdicionalDeProdutoDePlanoDeSaude_Id, cascadeDelete: true)
                .Index(t => t.PropostaDeContratacao_Id)
                .Index(t => t.OpcaoAdicionalDeProdutoDePlanoDeSaude_Id);
            
            AddColumn("dbo.Sau_PropostaDeContratacao", "EServidorPublico", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PropostaDeContratacaoOpcaoAdicionalDeProdutoDePlanoDeSaude", "OpcaoAdicionalDeProdutoDePlanoDeSaude_Id", "dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude");
            DropForeignKey("dbo.PropostaDeContratacaoOpcaoAdicionalDeProdutoDePlanoDeSaude", "PropostaDeContratacao_Id", "dbo.Sau_PropostaDeContratacao");
            DropForeignKey("dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.PropostaDeContratacaoOpcaoAdicionalDeProdutoDePlanoDeSaude", new[] { "OpcaoAdicionalDeProdutoDePlanoDeSaude_Id" });
            DropIndex("dbo.PropostaDeContratacaoOpcaoAdicionalDeProdutoDePlanoDeSaude", new[] { "PropostaDeContratacao_Id" });
            DropIndex("dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude", new[] { "ProdutoDePlanoDeSaudeId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "EServidorPublico");
            DropTable("dbo.PropostaDeContratacaoOpcaoAdicionalDeProdutoDePlanoDeSaude");
            DropTable("dbo.Sau_OpcaoAdicionalDeProdutoDePlanoDeSaude",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_OpcaoAdicionalDeProdutoDePlanoDeSaude_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OpcaoAdicionalDeProdutoDePlanoDeSaude_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_OpcaoAdicionalDeProdutoDePlanoDeSaude_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
