namespace EZ.EZControl.Migrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    public partial class Add_Lead : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sau_Lead",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    IsActive = c.Boolean(nullable: false),
                    PessoaFisicaId = c.Int(nullable: false),
                    TenantId = c.Int(nullable: false),
                    ExternalId = c.Int(nullable: false),
                    DateOfEditionIntegration = c.DateTime(),
                },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Lead_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_PessoaFisica", t => t.PessoaFisicaId)
                .Index(t => t.PessoaFisicaId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Sau_Lead", "PessoaFisicaId", "dbo.Glb_PessoaFisica");
            DropIndex("dbo.Sau_Lead", new[] { "PessoaFisicaId" });
            DropTable("dbo.Sau_Lead",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Lead_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
