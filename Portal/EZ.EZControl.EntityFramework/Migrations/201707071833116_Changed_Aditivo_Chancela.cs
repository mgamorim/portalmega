namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_Aditivo_Chancela : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_Carencia", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_Carencia", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_Aditivo", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_Carencia", new[] { "ProdutoId" });
            DropIndex("dbo.Sau_Carencia", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_Carencia", new[] { "ProdutoDePlanoDeSaude_Id" });
            RenameColumn(table: "dbo.Sau_Aditivo", name: "ProdutoDePlanoDeSaudeId", newName: "ProdutoDePlanoDeSaude_Id");
            CreateTable(
                "dbo.Sau_AditivoReport",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        EZLivReport = c.Int(nullable: false),
                        TipoDeAditivo = c.Int(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AditivoReport_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AditivoReport_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Glb_Empresa", t => t.EmpresaId)
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.EmpresaId);
            
            AddColumn("dbo.Sau_Aditivo", "ProdutoId", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_Aditivo", "Titulo", c => c.String(nullable: false));
            AddColumn("dbo.Sau_Aditivo", "Descricao", c => c.String(nullable: false));
            AddColumn("dbo.Sau_Aditivo", "IsEspecial", c => c.Boolean(nullable: false));
            AddColumn("dbo.Sau_Aditivo", "IsCarencia", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaude_Id", c => c.Int());
            CreateIndex("dbo.Sau_Aditivo", "ProdutoId");
            CreateIndex("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaude_Id");
            AddForeignKey("dbo.Sau_Aditivo", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            AddForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa", "Id", cascadeDelete: true);
            DropColumn("dbo.Sau_Aditivo", "EZLivReport");
            DropColumn("dbo.Sau_Aditivo", "TipoDeAditivo");
            DropTable("dbo.Sau_Carencia",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Carencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Carencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Sau_Carencia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProdutoId = c.Int(nullable: false),
                        Cobertura = c.String(nullable: false),
                        Descricao = c.String(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        EmpresaId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                        ProdutoDePlanoDeSaude_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Carencia_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Carencia_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Sau_Aditivo", "TipoDeAditivo", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_Aditivo", "EZLivReport", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa");
            DropForeignKey("dbo.Sau_Aditivo", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_AditivoReport", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_AditivoReport", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Sau_AditivoReport", new[] { "EmpresaId" });
            DropIndex("dbo.Sau_AditivoReport", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_Aditivo", new[] { "ProdutoDePlanoDeSaude_Id" });
            DropIndex("dbo.Sau_Aditivo", new[] { "ProdutoId" });
            AlterColumn("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaude_Id", c => c.Int(nullable: false));
            DropColumn("dbo.Sau_Aditivo", "IsCarencia");
            DropColumn("dbo.Sau_Aditivo", "IsEspecial");
            DropColumn("dbo.Sau_Aditivo", "Descricao");
            DropColumn("dbo.Sau_Aditivo", "Titulo");
            DropColumn("dbo.Sau_Aditivo", "ProdutoId");
            DropTable("dbo.Sau_AditivoReport",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_AditivoReport_EmpresaFilter", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_AditivoReport_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            RenameColumn(table: "dbo.Sau_Aditivo", name: "ProdutoDePlanoDeSaude_Id", newName: "ProdutoDePlanoDeSaudeId");
            CreateIndex("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id");
            CreateIndex("dbo.Sau_Carencia", "EmpresaId");
            CreateIndex("dbo.Sau_Carencia", "ProdutoId");
            CreateIndex("dbo.Sau_Aditivo", "ProdutoDePlanoDeSaudeId");
            AddForeignKey("dbo.Sau_Aditivo", "EmpresaId", "dbo.Glb_Empresa", "Id");
            AddForeignKey("dbo.Sau_Carencia", "ProdutoDePlanoDeSaude_Id", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            AddForeignKey("dbo.Sau_Carencia", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            AddForeignKey("dbo.Sau_Carencia", "EmpresaId", "dbo.Glb_Empresa", "Id", cascadeDelete: true);
        }
    }
}
