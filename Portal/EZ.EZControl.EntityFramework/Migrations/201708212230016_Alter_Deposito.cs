namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Deposito : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Pag_Deposito", new[] { "UsuarioDeclaranteId" });
            AddColumn("dbo.Pag_Deposito", "UsuarioEnvioId", c => c.Long(nullable: false));
            AddColumn("dbo.Pag_Deposito", "DataHoraEnvio", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Pag_Deposito", "UsuarioDeclaranteId", c => c.Long());
            AlterColumn("dbo.Pag_Deposito", "DataHoraDeclaracao", c => c.DateTime());
            CreateIndex("dbo.Pag_Deposito", "UsuarioEnvioId");
            CreateIndex("dbo.Pag_Deposito", "UsuarioDeclaranteId");
            AddForeignKey("dbo.Pag_Deposito", "UsuarioEnvioId", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pag_Deposito", "UsuarioEnvioId", "dbo.AbpUsers");
            DropIndex("dbo.Pag_Deposito", new[] { "UsuarioDeclaranteId" });
            DropIndex("dbo.Pag_Deposito", new[] { "UsuarioEnvioId" });
            AlterColumn("dbo.Pag_Deposito", "DataHoraDeclaracao", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Pag_Deposito", "UsuarioDeclaranteId", c => c.Long(nullable: false));
            DropColumn("dbo.Pag_Deposito", "DataHoraEnvio");
            DropColumn("dbo.Pag_Deposito", "UsuarioEnvioId");
            CreateIndex("dbo.Pag_Deposito", "UsuarioDeclaranteId");
        }
    }
}
