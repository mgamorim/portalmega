namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Carencia : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sau_Carencia", "OperadoraId", "dbo.Sau_Operadora");
            DropIndex("dbo.Sau_Carencia", new[] { "OperadoraId" });
            CreateTable(
                "dbo.Sau_ProdutoDePlanoDeSaude_Carencia",
                c => new
                    {
                        ProdutoDePlanoDeSaudeId = c.Int(nullable: false),
                        CarenciaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProdutoDePlanoDeSaudeId, t.CarenciaId })
                .ForeignKey("dbo.Sau_ProdutoDePlanoDeSaude", t => t.ProdutoDePlanoDeSaudeId, cascadeDelete: true)
                .ForeignKey("dbo.Sau_Carencia", t => t.CarenciaId, cascadeDelete: true)
                .Index(t => t.ProdutoDePlanoDeSaudeId)
                .Index(t => t.CarenciaId);
            
            AddColumn("dbo.Sau_Carencia", "ProdutoId", c => c.Int(nullable: false));
            CreateIndex("dbo.Sau_Carencia", "ProdutoId");
            AddForeignKey("dbo.Sau_Carencia", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude", "Id");
            DropColumn("dbo.Sau_Carencia", "OperadoraId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_Carencia", "OperadoraId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "CarenciaId", "dbo.Sau_Carencia");
            DropForeignKey("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", "ProdutoDePlanoDeSaudeId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropForeignKey("dbo.Sau_Carencia", "ProdutoId", "dbo.Sau_ProdutoDePlanoDeSaude");
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", new[] { "CarenciaId" });
            DropIndex("dbo.Sau_ProdutoDePlanoDeSaude_Carencia", new[] { "ProdutoDePlanoDeSaudeId" });
            DropIndex("dbo.Sau_Carencia", new[] { "ProdutoId" });
            DropColumn("dbo.Sau_Carencia", "ProdutoId");
            DropTable("dbo.Sau_ProdutoDePlanoDeSaude_Carencia");
            CreateIndex("dbo.Sau_Carencia", "OperadoraId");
            AddForeignKey("dbo.Sau_Carencia", "OperadoraId", "dbo.Sau_Operadora", "Id");
        }
    }
}
