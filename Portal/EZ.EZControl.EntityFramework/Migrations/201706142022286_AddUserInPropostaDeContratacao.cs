namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserInPropostaDeContratacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "UsuarioTitularId", c => c.Long());
            AddColumn("dbo.Sau_PropostaDeContratacao", "UsuarioResponsavelId", c => c.Long());
            CreateIndex("dbo.Sau_PropostaDeContratacao", "UsuarioTitularId");
            CreateIndex("dbo.Sau_PropostaDeContratacao", "UsuarioResponsavelId");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "UsuarioResponsavelId", "dbo.AbpUsers", "Id");
            AddForeignKey("dbo.Sau_PropostaDeContratacao", "UsuarioTitularId", "dbo.AbpUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "UsuarioTitularId", "dbo.AbpUsers");
            DropForeignKey("dbo.Sau_PropostaDeContratacao", "UsuarioResponsavelId", "dbo.AbpUsers");
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "UsuarioResponsavelId" });
            DropIndex("dbo.Sau_PropostaDeContratacao", new[] { "UsuarioTitularId" });
            DropColumn("dbo.Sau_PropostaDeContratacao", "UsuarioResponsavelId");
            DropColumn("dbo.Sau_PropostaDeContratacao", "UsuarioTitularId");
        }
    }
}
