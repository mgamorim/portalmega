namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Homologacao_Proposta_De_Contratacao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "TipoDeHomologacao", c => c.Int(nullable: false));
            AddColumn("dbo.Sau_PropostaDeContratacao", "DataHoraDaHomologacao", c => c.DateTime());
            DropColumn("dbo.Sau_PropostaDeContratacao", "Homologada");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Sau_PropostaDeContratacao", "Homologada", c => c.Boolean(nullable: false));
            DropColumn("dbo.Sau_PropostaDeContratacao", "DataHoraDaHomologacao");
            DropColumn("dbo.Sau_PropostaDeContratacao", "TipoDeHomologacao");
        }
    }
}
