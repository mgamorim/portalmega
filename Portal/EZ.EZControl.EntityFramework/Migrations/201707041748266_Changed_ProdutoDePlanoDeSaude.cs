namespace EZ.EZControl.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Changed_ProdutoDePlanoDeSaude : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Sau_ProdutoDePlanoDeSaude", "CartaDeOrientacao", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Sau_ProdutoDePlanoDeSaude", "CartaDeOrientacao");
        }
    }
}
