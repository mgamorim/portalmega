namespace EZ.EZControl.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveEmpresaDeTipoDeDocumento : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Glb_TipoDeDocumento", "EmpresaId", "dbo.Glb_Empresa");
            DropIndex("dbo.Glb_TipoDeDocumento", new[] { "EmpresaId" });
            AlterTableAnnotations(
                "dbo.Glb_TipoDeDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoPessoa = c.Int(nullable: false),
                        TipoDeDocumentoFixo = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 40),
                        Mascara = c.String(maxLength: 30),
                        Principal = c.Boolean(nullable: false),
                        ObrigaAnexo = c.Boolean(nullable: false),
                        PrazoDeAtualizacaoDoAnexo = c.Int(nullable: false),
                        Prioridade = c.Int(nullable: false),
                        ObrigaOrgaoExpedidor = c.Boolean(nullable: false),
                        ObrigaDataExpedicao = c.Boolean(nullable: false),
                        ObrigaUF = c.Boolean(nullable: false),
                        ObrigaDataDeValidade = c.Boolean(nullable: false),
                        SerieObrigatoria = c.Boolean(nullable: false),
                        ListaEmOutrosDocumentos = c.Boolean(nullable: false),
                        ExibirNoCadastroSimplificadoDePessoa = c.Boolean(nullable: false),
                        PermiteLancamentoDuplicado = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TipoDeDocumento_EmpresaFilter",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            DropColumn("dbo.Glb_TipoDeDocumento", "EmpresaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Glb_TipoDeDocumento", "EmpresaId", c => c.Int(nullable: false));
            AlterTableAnnotations(
                "dbo.Glb_TipoDeDocumento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsActive = c.Boolean(nullable: false),
                        TipoPessoa = c.Int(nullable: false),
                        TipoDeDocumentoFixo = c.Int(nullable: false),
                        Descricao = c.String(maxLength: 40),
                        Mascara = c.String(maxLength: 30),
                        Principal = c.Boolean(nullable: false),
                        ObrigaAnexo = c.Boolean(nullable: false),
                        PrazoDeAtualizacaoDoAnexo = c.Int(nullable: false),
                        Prioridade = c.Int(nullable: false),
                        ObrigaOrgaoExpedidor = c.Boolean(nullable: false),
                        ObrigaDataExpedicao = c.Boolean(nullable: false),
                        ObrigaUF = c.Boolean(nullable: false),
                        ObrigaDataDeValidade = c.Boolean(nullable: false),
                        SerieObrigatoria = c.Boolean(nullable: false),
                        ListaEmOutrosDocumentos = c.Boolean(nullable: false),
                        ExibirNoCadastroSimplificadoDePessoa = c.Boolean(nullable: false),
                        PermiteLancamentoDuplicado = c.Boolean(nullable: false),
                        TenantId = c.Int(nullable: false),
                        ExternalId = c.Int(nullable: false),
                        DateOfEditionIntegration = c.DateTime(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_TipoDeDocumento_EmpresaFilter",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            CreateIndex("dbo.Glb_TipoDeDocumento", "EmpresaId");
            AddForeignKey("dbo.Glb_TipoDeDocumento", "EmpresaId", "dbo.Glb_Empresa", "Id");
        }
    }
}
