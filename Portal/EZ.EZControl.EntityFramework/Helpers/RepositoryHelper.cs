﻿using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.EntityFramework.Repositories;
using System.Collections.Generic;

namespace EZ.EZControl.Helpers
{
    public static class RepositoryHelper
    {
        public static void BulkInsert<TEntity, TPrimaryKey>(IRepository<TEntity, TPrimaryKey> repository, IEnumerable<TEntity> entities)
            where TEntity : class, IEntity<TPrimaryKey>, new()
        {
            var dbContext = repository.GetDbContext();
            dbContext.Set<TEntity>().AddRange(entities);
        }
    }
}