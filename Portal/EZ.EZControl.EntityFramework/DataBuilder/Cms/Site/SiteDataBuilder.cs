﻿using System.Collections.Generic;
using System.Linq;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.EntityFramework;

namespace ML.MarcosLima.DataBuilder.Cms.Site
{
    public class SiteDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public SiteDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            CreateSite("Site Teste", "Site para teste", "sitedeteste.com", 
                "Template EZLiv", "Cadastro", "Cadastro", "Cadastro", "Default");
        }
        private void CreateSite(string nome, string descricao, string hostCompleto, string nomeDoTemplate,
            string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault, string tipoDePaginaParaCategoriaDefault,
            string tipoDePaginaParaPaginaInicialDefault)
        {
            var site = _context.Sites.FirstOrDefault(x => x.Nome.ToLower().Equals(nome.ToLower()));
            if (site == null)
            {
                var template = _context.Templates.FirstOrDefault(x => x.Nome.ToLower().Equals(nomeDoTemplate.ToLower()));
                if (template != null)
                {
                    site = _context.Sites.Add(new EZ.EZControl.Domain.CMS.Geral.Site()
                    {
                        IsActive = true,
                        Nome = nome,
                        Descricao = descricao,
                        TemplateDefaultId = template.Id,
                        EmpresaId = _empresaId,
                        TenantId = _tenantId,
                        TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                        TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                        TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                        TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault
                    });
                    _context.SaveChanges();

                    var host = _context.Hosts.FirstOrDefault(x => x.Url.ToLower().Equals(hostCompleto.ToLower()));
                    if (host == null)
                    {
                        _context.Hosts.Add(new Host()
                        {
                            Url = hostCompleto,
                            SiteId = site.Id,
                            IsPrincipal = true,
                            IsActive = true,
                            EmpresaId = _empresaId,
                            TenantId = _tenantId
                        });
                        _context.SaveChanges();
                    }

                    site.Hosts.Add(host);
                    _context.SaveChanges();
                }
            }
        }
    }
}
