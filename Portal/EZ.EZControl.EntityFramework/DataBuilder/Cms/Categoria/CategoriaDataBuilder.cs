﻿using System.Linq;
using EZ.EZControl.EntityFramework;

namespace ML.EZControl.DataBuilder.Cms.Categoria
{
    /* Estrutura criada
     * 
     * - categoria1
     *   - categoria12
     *     - categoria123
     *       - categoria1234
     */
    public class CategoriaDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public CategoriaDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            CreateCategorias("Site Teste");
        }

        private void CreateCategorias(string nomeDoSite)
        {
            var site =
                _context.Sites.FirstOrDefault(
                    x => x.Nome.ToLower().Equals(nomeDoSite.ToLower()) && x.EmpresaId == _empresaId);
            if (site != null)
            {
                var categoria1 = CreateCategoria("Categoria 1", "categoria1", site.Id);
                var categoria12 = CreateCategoria("Categoria 12", "categoria12", site.Id, categoria1.Id);
                var categoria123 = CreateCategoria("Categoria 123", "categoria123", site.Id, categoria12.Id);
                var categoria1234 = CreateCategoria("Categoria 1234", "categoria1234", site.Id, categoria123.Id);
            }
        }

        private EZ.EZControl.Domain.CMS.Geral.Categoria CreateCategoria(string nome, string slug, int siteId, int? paiId = null)
        {
            var categoria =
                _context.Categorias.FirstOrDefault(
                    x => x.Nome.ToLower().Equals(nome.ToLower()) && x.EmpresaId == _empresaId);
            if (categoria == null)
            {
                categoria = _context.Categorias.Add(new EZ.EZControl.Domain.CMS.Geral.Categoria()
                {
                    SiteId = siteId,
                    Slug = slug,
                    Nome = nome,
                    CategoriaPaiId = paiId,
                    IsActive = true,
                    EmpresaId = _empresaId,
                    TenantId = _tenantId
                });
                _context.SaveChanges();
            }
            return categoria;
        }
    }
}
