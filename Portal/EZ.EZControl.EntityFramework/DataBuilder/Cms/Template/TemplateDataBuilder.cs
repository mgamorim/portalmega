﻿using System.Collections.Generic;
using System.Linq;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.EntityFramework;

namespace ML.MarcosLima.DataBuilder.Cms.Site
{
    public class TemplateDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TemplateDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            CreateTemplate("Template EZLiv", "EZLiv", "http://www.ezliv.com.br", "Template do EZLiv.", "TemplateEZLiv",
                "2.0", "menu-top", "CmsMenu", "Default,Cadastro", "Cadastro", "Cadastro", "Cadastro", "Default");
        }

        private void CreateTemplate(string nome, string autor, string autorUrl, string descricao, string nomeDoArquivo,
            string versao, string posicoes, string tiposDeWidgetSuportados, string tiposDePaginasSuportadas,
            string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault, string tipoDePaginaParaCategoriaDefault,
            string tipoDePaginaParaPaginaInicialDefault)
        {
            var template = _context.Templates.FirstOrDefault(x => x.Nome.ToLower().Equals(nome.ToLower()));
            if (template == null)
            {
                _context.Templates.Add(new Template
                {
                    IsActive = true,
                    Autor = autor,
                    Nome = nome,
                    AutorUrl = autorUrl,
                    Descricao = descricao,
                    NomeDoArquivo = nomeDoArquivo,
                    Versao = versao,
                    Posicoes = posicoes,
                    TiposDeWidgetSuportados = tiposDeWidgetSuportados,
                    TiposDePaginasSuportadas = tiposDePaginasSuportadas,
                    TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                    TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                    TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                    TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                });
                _context.SaveChanges();
            }
        }
    }
}
