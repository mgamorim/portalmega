﻿using System;
using System.Linq;
using EZ.EZControl.EntityFramework;

namespace ML.MarcosLima.DataBuilder.Cms.Pagina
{
    public class PaginaDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public PaginaDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            CreatePagina("Site Teste", "Página 1", "pagina1", "Conteudo da Página");
        }
        private void CreatePagina(string nomeDoSite, string titulo, string slug, string conteudo)
        {
            var site =
                _context.Sites.FirstOrDefault(
                    x => x.Nome.ToLower().Equals(nomeDoSite.ToLower()) && x.EmpresaId == _empresaId);
            if (site != null)
            {
                var pagina =
                    _context.Paginas.FirstOrDefault(
                        x => x.Titulo.ToLower().Equals(titulo.ToLower()) && x.EmpresaId == _empresaId);
                if (pagina == null)
                {
                    _context.Paginas.Add(new EZ.EZControl.Domain.CMS.Geral.Pagina()
                    {
                        Conteudo = conteudo,
                        IsActive = true,
                        DataDePublicacao = DateTime.Now,
                        Slug = slug,
                        Titulo = titulo,
                        SiteId = site.Id,
                        EmpresaId = _empresaId,
                        TenantId = _tenantId
                    });
                    _context.SaveChanges();
                }
            }
        }
    }
}
