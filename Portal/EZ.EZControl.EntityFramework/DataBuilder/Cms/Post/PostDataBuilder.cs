﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.EntityFramework;

namespace ML.MarcosLima.DataBuilder.Cms.Post
{
    /* Estrutura criada
     * 
     * - categoria1
     *     post-1-1
     *   - categoria12
     *       post-1-12
     *       post-1-12
     *     - categoria123
     *         post-1-123
     *         post-2-123
     *         post-3-123
     *       - categoria1234
     *           post-1-1234
     *           post-2-1234
     *           post-3-1234
     *           post-4-1234
     *           post-5-1234
     *           post-6-1234
     *           post-7-1234
     *           post-8-1234
     *           post-9-1234
     *           post-10-1234
     *           post-11-1234
     *           post-12-1234
     */
    public class PostDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public PostDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            var site =
                _context.Sites.FirstOrDefault(
                    x => x.Nome.ToLower().Equals("Site Teste".ToLower()) && x.EmpresaId == _empresaId);
            if (site != null)
            {
                var categoria1 =
                    _context.Categorias.FirstOrDefault(
                        x => x.Nome.ToLower().Equals("Categoria 1".ToLower()) && x.EmpresaId == _empresaId);
                if (categoria1 != null)
                {
                    CreatePost(site.Id, categoria1, "Post 1 1", "post-1-1", "Conteudo do post 1 da categoria 1", 2017, 1, 10);
                }

                var categoria12 =
                    _context.Categorias.FirstOrDefault(
                        x => x.Nome.ToLower().Equals("Categoria 12".ToLower()) && x.EmpresaId == _empresaId);
                if (categoria12 != null)
                {
                    CreatePost(site.Id, categoria12, "Post 1 12", "post-1-12", "Conteudo do post 1 da categoria 12", 2017, 1, 10);
                    CreatePost(site.Id, categoria12, "Post 2 12", "post-2-12", "Conteudo do post 2 da categoria 12", 2017, 2, 10);

                }

                var categoria123 =
                    _context.Categorias.FirstOrDefault(
                        x => x.Nome.ToLower().Equals("Categoria 123".ToLower()) && x.EmpresaId == _empresaId);
                if (categoria123 != null)
                {
                    CreatePost(site.Id, categoria123, "Post 1 123", "post-1-123", "Conteudo do post 1 da categoria 123", 2017, 1, 10);
                    CreatePost(site.Id, categoria123, "Post 2 123", "post-2-123", "Conteudo do post 2 da categoria 123", 2017, 2, 10);
                    CreatePost(site.Id, categoria123, "Post 3 123", "post-3-123", "Conteudo do post 3 da categoria 123", 2017, 3, 10);

                }

                var categoria1234 =
                    _context.Categorias.FirstOrDefault(
                        x => x.Nome.ToLower().Equals("Categoria 1234".ToLower()) && x.EmpresaId == _empresaId);
                if (categoria1234 != null)
                {
                    CreatePost(site.Id, categoria1234, "Post 1 1234", "post-1-1234", "Conteudo do post 1 da categoria 1234", 2017, 1, 10);
                    CreatePost(site.Id, categoria1234, "Post 2 1234", "post-2-1234", "Conteudo do post 2 da categoria 1234", 2017, 2, 10);
                    CreatePost(site.Id, categoria1234, "Post 3 1234", "post-3-1234", "Conteudo do post 3 da categoria 1234", 2017, 3, 10);
                    CreatePost(site.Id, categoria1234, "Post 4 1234", "post-4-1234", "Conteudo do post 4 da categoria 1234", 2017, 4, 10);
                    CreatePost(site.Id, categoria1234, "Post 5 1234", "post-5-1234", "Conteudo do post 5 da categoria 1234", 2017, 5, 10);
                    CreatePost(site.Id, categoria1234, "Post 6 1234", "post-6-1234", "Conteudo do post 6 da categoria 1234", 2017, 6, 10);
                    CreatePost(site.Id, categoria1234, "Post 7 1234", "post-7-1234", "Conteudo do post 7 da categoria 1234", 2017, 7, 10);
                    CreatePost(site.Id, categoria1234, "Post 8 1234", "post-8-1234", "Conteudo do post 8 da categoria 1234", 2017, 8, 10);
                    CreatePost(site.Id, categoria1234, "Post 9 1234", "post-9-1234", "Conteudo do post 9 da categoria 1234", 2017, 9, 10);
                    CreatePost(site.Id, categoria1234, "Post 10 1234", "post-10-1234", "Conteudo do post 10 da categoria 1234", 2017, 10, 10);
                    CreatePost(site.Id, categoria1234, "Post 11 1234", "post-11-1234", "Conteudo do post 11 da categoria 1234", 2017, 11, 10);
                    CreatePost(site.Id, categoria1234, "Post 12 1234", "post-12-1234", "Conteudo do post 12 da categoria 1234", 2017, 12, 10);
                }
            }
        }

        private void CreatePost(int siteId, Categoria categoria, string titulo, string slug, string conteudo, int ano,
            int mes, int dia)
        {
            var post =
                _context.Posts.FirstOrDefault(
                    x => x.Titulo.ToLower().Equals(titulo.ToLower()) && x.EmpresaId == _empresaId);
            if (post == null)
            {
                _context.Posts.Add(new EZ.EZControl.Domain.CMS.Geral.Post()
                {
                    SiteId = siteId,
                    Slug = slug,
                    Titulo = titulo,
                    Conteudo = conteudo,
                    DataDePublicacao = new DateTime(ano, mes, dia),
                    IsActive = true,
                    Categorias = new List<Categoria>()
                    {
                        categoria
                    },
                    TenantId = _tenantId,
                    EmpresaId = _empresaId
                });
                _context.SaveChanges();
            }
        }
    }
}
