﻿using Abp.Modules;
using Abp.Zero.EntityFramework;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Filters;
using System.Data.Entity;
using System.Reflection;

namespace EZ.EZControl
{
    /// <summary>
    /// Entity framework module of the application.
    /// </summary>
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(EZControlCoreModule))]
    public class EZControlDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<EZControlDbContext>());

            //web.config (or app.config for non-web projects) file should contain a connection string named "Default".
            Configuration.DefaultNameOrConnectionString = "Default";

            //Ativa por default o filtro de empresa através do EZEntity
            Configuration.UnitOfWork.RegisterFilter(EzControlFilters.EmpresaFilter, true);

            // Específico para o EZLiv
            Configuration.UnitOfWork.RegisterFilter(EZLivFilters.CorretoraFilter, false);
            Configuration.UnitOfWork.RegisterFilter(EZLivFilters.AdministradoraFilter, false);
            Configuration.UnitOfWork.RegisterFilter(EZLivFilters.OperadoraFilter, false);
            Configuration.UnitOfWork.RegisterFilter(EZLivFilters.CorretorFilter, false);
            Configuration.UnitOfWork.RegisterFilter(EZLivFilters.AssociacaoFilter, false);
            Configuration.UnitOfWork.RegisterFilter(EZLivFilters.TitularFilter, false);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
