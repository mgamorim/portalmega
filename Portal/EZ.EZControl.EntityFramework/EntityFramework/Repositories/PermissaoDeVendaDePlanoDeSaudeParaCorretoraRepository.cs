﻿using Abp.Domain.Repositories;
using Abp.EntityFramework;
using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity;
using System.Threading.Tasks;

namespace EZ.EZControl.EntityFramework.Repositories
{
    public interface IPermissaoDeVendaDePlanoDeSaudeParaCorretoraRepository : IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretora>
    {
        Task<int> UpdateEspeficicoAsync(PermissaoDeVendaDePlanoDeSaudeParaCorretora entity);
    }

    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraRepository : EZControlRepositoryBase<PermissaoDeVendaDePlanoDeSaudeParaCorretora>, IPermissaoDeVendaDePlanoDeSaudeParaCorretoraRepository
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretoraRepository(IDbContextProvider<EZControlDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public Task<int> UpdateEspeficicoAsync(PermissaoDeVendaDePlanoDeSaudeParaCorretora entity)
        {
            base.GetDbContext().Entry(entity).State = EntityState.Modified;
            return base.GetDbContext().SaveChangesAsync();
        }
    }
}
