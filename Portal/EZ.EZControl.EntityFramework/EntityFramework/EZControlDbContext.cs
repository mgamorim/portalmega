﻿using Abp.UI;
using Abp.Zero.EntityFramework;
using EntityFramework.DynamicFilters;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Chat;
using EZ.EZControl.Conventions;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.Interfaces;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.EZLiv.Sync;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Filters;
using EZ.EZControl.Friendships;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Storage;
using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.EntityFramework
{
    public class EZControlDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        /* Define an IDbSet for each entity of the application */

        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }
        public virtual IDbSet<ArquivoBase> ArquivoBase { get; set; }
        public virtual IDbSet<ArquivoCMS> ArquivosCMS { get; set; }
        public virtual IDbSet<ArquivoGlobal> ArquivosGlobal { get; set; }
        public virtual IDbSet<Cidade> Cidades { get; set; }
        public virtual IDbSet<Endereco> Enderecos { get; set; }
        public virtual IDbSet<Estado> Estados { get; set; }
        public virtual IDbSet<Pais> Paises { get; set; }
        public virtual IDbSet<TipoDeLogradouro> TiposDeLogradouros { get; set; }
        public virtual IDbSet<Contato> Contatos { get; set; }
        public virtual IDbSet<Documento> Documentos { get; set; }
        public virtual IDbSet<EnderecoEletronico> EnderecosEletronicos { get; set; }
        public virtual IDbSet<GrupoPessoa> GrupoPessoas { get; set; }
        public virtual IDbSet<Pessoa> Pessoas { get; set; }
        public virtual IDbSet<PessoaFisica> PessoasFisicas { get; set; }
        public virtual IDbSet<PessoaJuridica> PessoasJuridicas { get; set; }
        public virtual IDbSet<Telefone> Telefones { get; set; }
        public virtual IDbSet<TipoDeContato> TipoDeContatos { get; set; }
        public virtual IDbSet<TipoDeDocumento> TipoDeDocumentos { get; set; }
        public virtual IDbSet<Tratamento> Tratamentos { get; set; }
        public virtual IDbSet<Banco> Bancos { get; set; }
        public virtual IDbSet<Agencia> Agencias { get; set; }
        public virtual IDbSet<ConexaoSMTP> ConexoesSMTP { get; set; }
        public virtual IDbSet<Feriado> Feriados { get; set; }
        public virtual IDbSet<RamoDoFornecedor> RamosDoFornecedor { get; set; }
        public virtual IDbSet<Profissao> Profissoes { get; set; }
        public virtual IDbSet<Departamento> Departamentos { get; set; }
        public virtual IDbSet<ItemHierarquia> ItensHierarquia { get; set; }
        public virtual IDbSet<TemplateEmail> TemplatesEmail { get; set; }
        public virtual IDbSet<Fornecedor> Fornecedores { get; set; }
        public virtual IDbSet<Cliente> Clientes { get; set; }
        public virtual IDbSet<ClienteEZ> ClientesEZ { get; set; }
        public virtual IDbSet<ParametroBase> ParametroBase { get; set; }
        public virtual IDbSet<ParametroGlobal> ParametrosGlobal { get; set; }
        public virtual IDbSet<ParametroCMS> ParametrosCMS { get; set; }
        public virtual IDbSet<ParametroAgenda> ParametrosAgenda { get; set; }
        public virtual IDbSet<Categoria> Categorias { get; set; }
        public virtual IDbSet<Empresa> Empresas { get; set; }
        public virtual IDbSet<Site> Sites { get; set; }
        public virtual IDbSet<Tag> Tags { get; set; }
        public virtual IDbSet<PermissaoEmpresaPorUsuario> PermissaoEmpresaPorUsuario { get; set; }
        public virtual IDbSet<PermissaoEmpresaPorRole> PermissaoEmpresaPorRole { get; set; }
        public virtual IDbSet<Pagina> Paginas { get; set; }
        public virtual IDbSet<Post> Posts { get; set; }
        public virtual IDbSet<Menu> Menus { get; set; }
        public virtual IDbSet<ItemDeMenu> ItensDeMenu { get; set; }
        public virtual IDbSet<Template> Templates { get; set; }
        public virtual IDbSet<PublicacaoBase> Publicacoes { get; set; }
        public virtual IDbSet<Friendship> Friendships { get; set; }
        public virtual IDbSet<ChatMessage> ChatMessages { get; set; }
        public virtual IDbSet<WidgetBase> Widgets { get; set; }
        public virtual IDbSet<WidgetHTML> WidgetsHTML { get; set; }
        public virtual IDbSet<WidgetMenu> WidgetsMenu { get; set; }
        public virtual IDbSet<Host> Hosts { get; set; }
        public virtual IDbSet<EventoBase> EventosBase { get; set; }
        public virtual IDbSet<EventoReuniao> EventosReuniao { get; set; }
        public virtual IDbSet<EventoTarefa> EventosTarefa { get; set; }
        public virtual IDbSet<Disponibilidade> Disponibilidades { get; set; }
        public virtual IDbSet<ConfiguracaoDeDisponibilidade> ConfiguracoesDeDisponibilidades { get; set; }
        public virtual IDbSet<RegraBase> RegraBases { get; set; }
        public virtual IDbSet<RegraTarefa> RegraTarefas { get; set; }
        public virtual IDbSet<RegraConsulta> RegraConsultas { get; set; }
        public virtual IDbSet<RegraReuniao> RegraReunioes { get; set; }
        public virtual IDbSet<ConfiguracaoDeBloqueio> ConfiguracoesDeBloqueio { get; set; }
        public virtual IDbSet<Bloqueio> Bloqueios { get; set; }
        public virtual IDbSet<HistoricoDoEvento> HistoricosDosEventos { get; set; }
        public virtual IDbSet<EventoConsulta> EventosConsulta { get; set; }
        public virtual IDbSet<Produto> Produtos { get; set; }
        public virtual IDbSet<Servico> Servicos { get; set; }
        public virtual IDbSet<Pedido> Pedidos { get; set; }
        public virtual IDbSet<ItemDePedido> ItensDePedidos { get; set; }
        public virtual IDbSet<TipoDeDocumentoFinanceiro> TiposDeDocumentoFinanceiro { get; set; }
        public virtual IDbSet<DocumentoBase> DocumentosBase { get; set; }
        public virtual IDbSet<DocumentoAPagar> DocumentosAPagar { get; set; }
        public virtual IDbSet<DocumentoAReceber> DocumentosAReceber { get; set; }
        public virtual IDbSet<Especialidade> Especialidades { get; set; }
        public virtual IDbSet<Medico> Medicos { get; set; }
        public virtual IDbSet<ParametroEstoque> ParametrosEstoque { get; set; }
        public virtual IDbSet<Natureza> Naturezas { get; set; }
        public virtual IDbSet<UnidadeMedida> UnidadeMedidas { get; set; }
        public virtual IDbSet<Fracao> Fracoes { get; set; }
        public virtual IDbSet<ConfiguracaoDeBloqueioConsulta> ConfiguracoesDeBloqueioConsulta { get; set; }
        public virtual IDbSet<ConfiguracaoDeDisponibilidadeConsulta> ConfiguracoesDeDisponibilidadeConsulta { get; set; }
        public virtual IDbSet<Picture> Pictures { get; set; }
        public virtual IDbSet<BeneficiarioBase> Beneficiarios { get; set; }
        public virtual IDbSet<ProponenteTitular> Proponentes { get; set; }
        public virtual IDbSet<Responsavel> Responsaveis { get; set; }
        public virtual IDbSet<Dependente> Dependentes { get; set; }
        public virtual IDbSet<DeclaracaoDeSaude> DeclaracoesDeSaude { get; set; }
        public virtual IDbSet<ItemDeDeclaracaoDeSaude> ItensDeDeclaracaoDeSaude { get; set; }
        public virtual IDbSet<RespostaDoItemDeDeclaracaoDeSaude> RespostasDoItemDeDeclaracaoDeSaude { get; set; }
        public virtual IDbSet<PlanoDeSaude> PlanosDeSaude { get; set; }
        public virtual IDbSet<ProdutoDePlanoDeSaude> ProdutosDePlanoDeSaude { get; set; }
        public virtual IDbSet<OpcaoAdicionalDeProdutoDePlanoDeSaude> OpcoesAdicionaisDeProdutosDePlanoDeSaude { get; set; }
        public virtual IDbSet<Operadora> Operadoras { get; set; }
        public virtual IDbSet<Corretor> Corretores { get; set; }
        public virtual IDbSet<CorretorPessoaJuridca> CorretoresPessoaJuridca { get; set; }
        public virtual IDbSet<CorretorDadosBancario> CorretoresDadosBancario { get; set; }
        public virtual IDbSet<Corretora> Corretoras { get; set; }
        public virtual IDbSet<Administradora> Administradoras { get; set; }
        public virtual IDbSet<UsuarioPorPessoa> UsuariosPorPessoa { get; set; }
        public virtual IDbSet<LocalArmazenamento> LocaisArmazenamento { get; set; }
        public virtual IDbSet<Entrada> Entradas { get; set; }
        public virtual IDbSet<EntradaItem> EntradaItens { get; set; }
        public virtual IDbSet<Saida> Saidas { get; set; }
        public virtual IDbSet<SaidaItem> SaidaItens { get; set; }
        public virtual IDbSet<SaldoEstoque> SaldosEstoque { get; set; }
        public virtual IDbSet<Transacao> Transacoes { get; set; }
        public virtual IDbSet<ParametroPagSeguro> ParametrosPagSeguro { get; set; }
        public virtual IDbSet<PropostaDeContratacao> PropostasDeContratacao { get; set; }
        public virtual IDbSet<Contrato> Contratos { get; set; }
        public virtual IDbSet<FaixaEtaria> FaixasEtarias { get; set; }
        public virtual IDbSet<ParametroEZLiv> ParametroEZLiv { get; set; }
        public virtual IDbSet<ParametroEzpag> ParametroEzpag { get; set; }
        public virtual IDbSet<FormaDePagamento> FormasDePagamentos { get; set; }
        public virtual IDbSet<ContratoHistorico> ContratoHistoricos { get; set; }
        public virtual IDbSet<ArquivoDocumento> ArquivosDocumento { get; set; }
        public virtual IDbSet<EspecialidadeSync> EspecialidadesSync { get; set; }
        public virtual IDbSet<ClienteEZLiv> ClientesEZLiv { get; set; }
        public virtual IDbSet<ValorPorFaixaEtaria> ValoresPorFaixasEtarias { get; set; }
        public virtual IDbSet<Associacao> Associacoes { get; set; }
        public virtual IDbSet<RedeCredenciada> RedesCredenciadas { get; set; }
        public virtual IDbSet<ItemDeRedeCredenciada> ItensDeRedeCredenciada { get; set; }
        public virtual IDbSet<UnidadeDeSaudeBase> UnidadeDeSaudeBase { get; set; }
        public virtual IDbSet<Laboratorio> Laboratorio { get; set; }
        public virtual IDbSet<Hospital> Hospital { get; set; }
        public virtual IDbSet<HistoricoTransacao> HistoricosTransacoes { get; set; }
        public virtual IDbSet<EspecialidadePorProdutoDePlanoDeSaude> EspecialidadePorProdutoDePlanoDeSaude { get; set; }
        public virtual IDbSet<Chancela> Chancelas { get; set; }
        public virtual IDbSet<Vigencia> Vigencias { get; set; }
        public virtual IDbSet<QuestionarioDeDeclaracaoDeSaude> QuestionariosDeDeclaracaoDeSaude { get; set; }
        public virtual IDbSet<IndiceDeReajustePorFaixaEtaria> IndicesDeReajustePorFaixaEtaria { get; set; }
        public virtual IDbSet<GerenteCorretora> GerentesCorretora { get; set; }
        public virtual IDbSet<SupervisorCorretora> SupervisoresCorretora { get; set; }
        public virtual IDbSet<GerenteAdministradora> GerentesAdministradora { get; set; }
        public virtual IDbSet<SupervisorAdministradora> SupervisoresAdministradora { get; set; }
        public virtual IDbSet<HomologadorAdministradora> HomologadoresAdministradora { get; set; }
        public virtual IDbSet<ItemDeDeclaracaoDoBeneficiario> ItensDeDeclaracaoDoBeneficiario { get; set; }
        public virtual IDbSet<DeclaracaoDoBeneficiario> DeclaracoesDoBeneficiario { get; set; }
        public virtual IDbSet<Deposito> Depositos { get; set; }
        public virtual IDbSet<PermissaoDeVendaDePlanoDeSaudeParaCorretora> PermissoesDeVendaDePlanoDeSaudeParaCorretoras { get; set; }
        public virtual IDbSet<PermissaoDeVendaDePlanoDeSaudeParaCorretor> PermissoesDeVendaDePlanoDeSaudeParaCorretores { get; set; }
        public virtual IDbSet<SnapshotPropostaDeContratacao> SnapshotsPropostaDeContratacao { get; set; }
        public virtual IDbSet<Relatorio> Relatorios { get; set; }
        public virtual IDbSet<DepositoTransferenciaBancaria> DepositoTransferenciaBancarias { get; set; }


        /* Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         * But it may cause problems when working Migrate.exe of EF. ABP works either way.         *
         */

        public EZControlDbContext()
            : base("Default")
        {

        }

        /* This constructor is used by ABP to pass connection string defined in EZControlDataModule.PreInitialize.
         * Notice that, actually you will not directly create an instance of EZControlDbContext since ABP automatically handles it.
         */

        public EZControlDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        /* This constructor is used in tests to pass a fake/mock connection.
         */

        public EZControlDbContext(DbConnection dbConnection)
            : base(dbConnection, true)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());

            modelBuilder.Conventions.Add<TableNameConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Filter(EzControlFilters.EmpresaFilter, (IHasEmpresa entity, int empresaId) => entity.EmpresaId == empresaId, 0);

            // Específico para o EZLiv
            modelBuilder.Filter(EZLivFilters.CorretoraFilter, (IHasCorretora entity, int corretoraId) => entity.CorretoraId == corretoraId, 0);
            modelBuilder.Filter(EZLivFilters.AdministradoraFilter, (IHasAdministradora entity, int administradoraId) => entity.AdministradoraId == administradoraId, 0);
            modelBuilder.Filter(EZLivFilters.OperadoraFilter, (IHasOperadora entity, int operadoraId) => entity.OperadoraId == operadoraId, 0);
            modelBuilder.Filter(EZLivFilters.CorretorFilter, (IHasCorretor entity, int corretorId) => entity.CorretorId == corretorId, 0);
            modelBuilder.Filter(EZLivFilters.AssociacaoFilter, (IHasAssociacao entity, int associacaoId) => entity.AssociacaoId == associacaoId, 0);
            modelBuilder.Filter(EZLivFilters.TitularFilter, (PropostaDeContratacao entity, int titularId) => entity.TitularId == titularId, 0);

        }


#if DEBUG
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            try
            {
                return await base.SaveChangesAsync(cancellationToken);
            }
            catch (DbEntityValidationException dbEx)
            {
                var message = new StringBuilder();
                foreach (var error in dbEx.EntityValidationErrors)
                {
                    foreach (var vError in error.ValidationErrors)
                    {
                        message.AppendLine(vError.ErrorMessage);
                    }
                }
                throw new UserFriendlyException(message.ToString());
            }
            catch (Exception ex)
            {
                var message = new StringBuilder();
                if (ex.InnerException != null)
                {
                    message.AppendLine(ex.Message);
                    message.AppendLine(ex.InnerException.Message);
                    if (ex.InnerException.InnerException != null)
                    {
                        message.AppendLine(ex.InnerException.InnerException.Message);
                    }
                }
                throw new UserFriendlyException(message.ToString());
            }
        }
#endif
    }
}