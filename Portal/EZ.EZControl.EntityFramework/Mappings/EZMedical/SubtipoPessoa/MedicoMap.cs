﻿using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZMedical.SubtipoPessoa
{
    public class MedicoMap : EntityTypeConfiguration<Medico>
    {
        public MedicoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.PessoaFisica)
                .WithMany()
                .HasForeignKey(x => x.PessoaFisicaId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.Pessoa);

            Ignore(x => x.NomePessoa);

            HasRequired(x => x.Picture)
                .WithOptional()
                .Map(x => x.MapKey("PictureId"))
                .WillCascadeOnDelete(false);

            HasMany(x => x.Especialidades)
               .WithMany()
               .Map(x =>
               {
                   x.ToTable("Med_Especialidade_Medico");
                   x.MapLeftKey("MedicoId");
                   x.MapRightKey("EspecialidadeId");
               });

           
        }
    }
}
