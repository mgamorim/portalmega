﻿using EZ.EZControl.Domain.EZMedical.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZMedical.Geral
{
    public class ConfiguracaoDeDisponibilidadeConsultaMap : EntityTypeConfiguration<ConfiguracaoDeDisponibilidadeConsulta>
    {
        public ConfiguracaoDeDisponibilidadeConsultaMap()
        {
            HasRequired(x => x.Medico)
                .WithOptional()
                .Map(m => {
                    m.MapKey("MedicoId");
                })
                .WillCascadeOnDelete(false);
            HasRequired(x => x.Especialidade)
                .WithOptional()
                .Map(m => {
                    m.MapKey("EspecialidadeId");
                })
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}