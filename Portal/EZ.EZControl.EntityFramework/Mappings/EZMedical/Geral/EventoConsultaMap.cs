﻿using EZ.EZControl.Domain.EZMedical.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZMedical.Geral
{
    public class EventoConsultaMap : EntityTypeConfiguration<EventoConsulta>
    {
        public EventoConsultaMap()
        {
            HasKey(x => x.Id);
            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}