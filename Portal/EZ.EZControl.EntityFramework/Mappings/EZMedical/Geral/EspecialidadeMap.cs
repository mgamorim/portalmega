﻿using EZ.EZControl.Domain.EZMedical.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZMedical.Geral
{
    public class EspecialidadeMap : EntityTypeConfiguration<Especialidade>
    {
        public EspecialidadeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Codigo)
                .HasMaxLength(Especialidade.CodigoMaxLength)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Especialidade.NomeMaxLength)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}