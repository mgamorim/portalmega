﻿using EZ.EZControl.Domain.EZPag.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZPag.Geral
{
    public class ParametroEzpagMap: EntityTypeConfiguration<ParametroEzpag>
    {
        public ParametroEzpagMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TamanhoMaximoMb);

            Property(x => x.AlturaMaximaPx);

            Property(x => x.LarguraMaximaPx);

            Property(x => x.ExtensoesDocumento);

            Property(x => x.ExtensoesImagem);

            Property(x => x.IsTest);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
