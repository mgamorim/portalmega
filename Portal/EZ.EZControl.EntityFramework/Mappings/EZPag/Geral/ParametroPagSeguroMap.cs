﻿using EZ.EZControl.Domain.EZPag.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZPag.Geral
{
    public class ParametroPagSeguroMap : EntityTypeConfiguration<ParametroPagSeguro>
    {
        public ParametroPagSeguroMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.PessoaJuridica)
               .WithMany()
               .HasForeignKey(x => x.PessoaJuridicaId)
               .WillCascadeOnDelete(false);

            Property(x => x.Email);
            Property(x => x.Token);
        }
    }
}
