﻿using EZ.EZControl.Domain.EZPag.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZPag.Geral
{
    public class TransacaoMap : EntityTypeConfiguration<Transacao>
    {
        public TransacaoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Pedido)
               .WithMany()
               .HasForeignKey(x => x.PedidoId)
               .WillCascadeOnDelete(false);

            Property(x => x.TransactionId);
            Property(x => x.Referencia);
            Property(x => x.Data);

            Property(x => x.StatusDoPagamento)
               .IsRequired();

            Property(x => x.LinkParaPagamento);
        }
    }
}
