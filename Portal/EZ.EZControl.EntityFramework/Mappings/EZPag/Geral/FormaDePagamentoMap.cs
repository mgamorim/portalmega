﻿using EZ.EZControl.Domain.EZPag.Geral;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Mappings.EZPag.Geral
{
    public class FormaDePagamentoMap: EntityTypeConfiguration<FormaDePagamento>
    {
        public FormaDePagamentoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.Descricao)
                .HasMaxLength(FormaDePagamento.DescricaoMaxLength)
                .IsRequired();
            Property(x => x.TipoDePagamento)
                .IsRequired();
        }
    }
}
