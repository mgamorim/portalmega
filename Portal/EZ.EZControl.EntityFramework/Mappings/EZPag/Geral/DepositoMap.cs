﻿using EZ.EZControl.Domain.EZPag.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZPag.Geral
{
    public class DepositoMap : EntityTypeConfiguration<Deposito>
    {
        public DepositoMap()
        {
            HasKey(x => x.Id);
            HasRequired(x => x.Pedido)
               .WithMany()
               .HasForeignKey(x => x.PedidoId)
               .WillCascadeOnDelete(false);
            Property(x => x.Comprovante);
            HasRequired(x => x.UsuarioEnvio)
               .WithMany()
               .HasForeignKey(x => x.UsuarioEnvioId)
               .WillCascadeOnDelete(false);
            HasOptional(x => x.UsuarioDeclarante)
               .WithMany()
               .HasForeignKey(x => x.UsuarioDeclaranteId)
               .WillCascadeOnDelete(false);
            Property(x => x.Recebido);
            Property(x => x.DataHoraDeclaracao);
            Property(x => x.DataHoraEnvio);
        }
    }
}
