﻿using EZ.EZControl.Domain.EZPag.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZPag.Geral
{
    public class HistoricoTransacaoMap : EntityTypeConfiguration<HistoricoTransacao>
    {
        public HistoricoTransacaoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Transacao)
               .WithMany()
               .HasForeignKey(x => x.TransacaoId)
               .WillCascadeOnDelete(false);

            Property(x => x.Data);

            Property(x => x.Status)
               .IsRequired();
        }
    }
}
