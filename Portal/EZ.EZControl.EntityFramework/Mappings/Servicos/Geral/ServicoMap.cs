﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.Domain.Servicos.Geral;

namespace EZ.EZControl.Mappings.Servicos.Geral
{
    public class ServicoMap : EntityTypeConfiguration<Servico>
    {
        public ServicoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.Nome)
                .HasMaxLength(Servico.NomeMaxLength)
                .IsRequired();
            Property(x => x.Valor)
                .IsRequired();
            Property(x => x.IsValorAjustavel)
                .IsRequired();
            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
