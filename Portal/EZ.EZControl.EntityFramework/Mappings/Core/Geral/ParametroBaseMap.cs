﻿using EZ.EZControl.Domain.Core.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Core.Geral
{
    public class ParametroBaseMap : EntityTypeConfiguration<ParametroBase>
    {
        public ParametroBaseMap()
        {
            HasKey(x => x.Id);

            ToTable("Cor_Parametro");

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TamanhoMaximoMb);

            Property(x => x.AlturaMaximaPx);

            Property(x => x.LarguraMaximaPx);

            Property(x => x.ExtensoesDocumento);

            Property(x => x.ExtensoesImagem);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}