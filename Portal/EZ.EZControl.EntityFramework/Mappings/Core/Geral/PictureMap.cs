﻿using EZ.EZControl.Domain.Core.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Core.Geral
{
    public class PictureMap : EntityTypeConfiguration<Picture>
    {
        public PictureMap()
        {
            HasKey(x => x.Id);

            ToTable("Cor_Picture");

            Property(x => x.Bytes)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
