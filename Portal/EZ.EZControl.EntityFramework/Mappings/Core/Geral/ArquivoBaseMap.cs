﻿using EZ.EZControl.Domain.Core.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Core.Geral
{
    public class ArquivoBaseMap : EntityTypeConfiguration<ArquivoBase>
    {
        public ArquivoBaseMap()
        {
            HasKey(x => x.Id);

            ToTable("Cor_Arquivo");

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TipoDeArquivoFixo)
                .IsRequired();

            Property(x => x.Conteudo);

            Property(x => x.Path)
                .IsRequired();

            Property(x => x.Token)
                .IsRequired()
                .HasMaxLength(ArquivoBase.TokenMaxLength);

            Property(x => x.Nome)
                .HasMaxLength(ArquivoBase.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Arquivo_Nome")
                    {
                        IsUnique = true
                    }));

            //HasRequired(x => x.Empresa)
            //    .WithMany()
            //    .HasForeignKey(x => x.EmpresaId)
            //    .WillCascadeOnDelete(false);
        }
    }
}
