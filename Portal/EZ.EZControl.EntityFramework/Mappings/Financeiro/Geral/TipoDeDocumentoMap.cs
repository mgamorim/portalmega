﻿using EZ.EZControl.Domain.Financeiro.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Financeiro.Geral
{
    public class TipoDeDocumentoMap : EntityTypeConfiguration<TipoDeDocumentoFinanceiro>
    {
        public TipoDeDocumentoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .HasMaxLength(TipoDeDocumentoFinanceiro.NomeMaxLength)
                .IsRequired();

            Property(x => x.Tipo);

            Property(x => x.IsActive);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}