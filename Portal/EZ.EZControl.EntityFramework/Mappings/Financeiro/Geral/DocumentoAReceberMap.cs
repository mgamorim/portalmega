﻿using EZ.EZControl.Domain.Financeiro.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Financeiro.Geral
{
    public class DocumentoAReceberMap : EntityTypeConfiguration<DocumentoAReceber>
    {
        public DocumentoAReceberMap()
        {
            HasRequired(x => x.Cliente)
                .WithMany()
                .HasForeignKey(x => x.ClienteId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}