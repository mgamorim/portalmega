﻿using EZ.EZControl.Domain.Financeiro.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Financeiro.Geral
{
    public class DocumentoBaseMap : EntityTypeConfiguration<DocumentoBase>
    {
        public DocumentoBaseMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Competencia)
                .IsRequired();

            Property(x => x.Complemento);

            Property(x => x.Data)
                .IsRequired();

            Property(x => x.Descricao)
                .HasMaxLength(DocumentoBase.DescricaoMaxLength)
                .IsRequired();

            Property(x => x.Numero)
                .IsRequired();

            Property(x => x.Observacoes)
                .HasMaxLength(DocumentoAPagar.ObservacoesMaxLength);

            Property(x => x.Valor)
                .IsRequired();

            HasRequired(x => x.TipoDeDocumentoFinanceiro)
                .WithMany()
                .HasForeignKey(x => x.TipoDeDocumentoFinanceiroId)
                .WillCascadeOnDelete(false);

            Property(x => x.Estornado);

            Property(x => x.DataEstorno);

            Property(x => x.MotivoEstorno);

            Property(x => x.Vencimento)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}