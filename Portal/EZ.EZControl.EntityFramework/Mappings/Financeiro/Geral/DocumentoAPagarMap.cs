﻿using EZ.EZControl.Domain.Financeiro.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Financeiro.Geral
{
    public class DocumentoAPagarMap : EntityTypeConfiguration<DocumentoAPagar>
    {
        public DocumentoAPagarMap()
        {
            HasRequired(x => x.Fornecedor)
                .WithMany()
                .HasForeignKey(x => x.FornecedorId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}