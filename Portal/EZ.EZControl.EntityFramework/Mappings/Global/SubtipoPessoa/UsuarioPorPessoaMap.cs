﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.SubtipoPessoa
{
    public class UsuarioPorPessoaMap : EntityTypeConfiguration<UsuarioPorPessoa>
    {
        public UsuarioPorPessoaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.User)
               .WithMany()
               .HasForeignKey(x => x.UserId)
               .WillCascadeOnDelete(false);
        }
    }
}
