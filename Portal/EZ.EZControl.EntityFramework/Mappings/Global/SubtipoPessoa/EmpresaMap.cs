﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.SubtipoPessoa
{
    public class EmpresaMap : EntityTypeConfiguration<Empresa>
    {
        public EmpresaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.NomeFantasia)
                .HasMaxLength(Empresa.MaxNomeFantasiaLength)
                .IsRequired();

            Property(x => x.RazaoSocial)
                .HasMaxLength(Empresa.MaxRazaoSocialLength)
                .IsRequired();

            Property(x => x.Cnpj)
                .IsOptional();

            Property(x => x.PessoaJuridicaId)
                .IsOptional();

            //Opcional porquê a empresa precisa existir devido ao conceito MultiEmpresa 
            HasOptional(x => x.ClienteEZ)
                .WithMany(x => x.Empresas)
                .HasForeignKey(x => x.ClienteEZId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Usuarios)
                .WithMany(x => x.Empresas)
                .Map(x =>
                {
                    x.ToTable("Glb_Empresa_Usuario");
                    x.MapLeftKey("EmpresaId");
                    x.MapRightKey("UsuarioId");
                });

            Property(x => x.Slug)
               .HasMaxLength(Empresa.MaxSlugLength)
               .IsOptional();
        }
    }
}