﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.SubtipoPessoa
{
    public class ClienteEZMap : EntityTypeConfiguration<ClienteEZ>
    {
        public ClienteEZMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .IsRequired();

            Property(x => x.RazaoSocial)
                .IsOptional();

            Property(x => x.DocumentoPrincipal)
                .IsOptional();

            Property(x => x.PessoaId)
                .IsOptional();

            HasMany(x => x.Empresas);
        }
    }
}