﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.SubtipoPessoa
{
    public class FornecedorMap : EntityTypeConfiguration<Fornecedor>
    {
        public FornecedorMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.Pessoa)
                .WithMany()
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.RamosDoFornecedor)
                .WithMany(x => x.Fornecedores)
                .Map(x =>
                {
                    x.ToTable("Glb_Fornecedor_RamoDoFornecedor");
                    x.MapLeftKey("FornecedorId");
                    x.MapRightKey("RamoDoFornecedorId");
                });

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.PessoaFisica);

            Ignore(x => x.PessoaJuridica);
        }
    }
}