﻿using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.SubtipoPessoa
{
    public class ClienteMap : EntityTypeConfiguration<Cliente>
    {
        public ClienteMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.Pessoa)
                .WithMany()
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.PessoaFisica);

            Ignore(x => x.PessoaJuridica);
        }
    }
}