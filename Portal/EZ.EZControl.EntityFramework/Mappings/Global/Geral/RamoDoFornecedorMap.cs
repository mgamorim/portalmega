﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class RamoDoFornecedorMap : EntityTypeConfiguration<RamoDoFornecedor>
    {
        public RamoDoFornecedorMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(RamoDoFornecedor.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_RamoDoFornecedor_Nome")
                    {
                        IsUnique = true
                    }));

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}