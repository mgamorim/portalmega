﻿using EZ.EZControl.Domain.Global.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class AgenciaMap : EntityTypeConfiguration<Agencia>
    {
        public AgenciaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.Banco)
                .WithMany(x => x.Agencias)
                .HasForeignKey(x => x.BancoId)
                .WillCascadeOnDelete(false);

            Property(x => x.Codigo)
                .HasMaxLength(Agencia.CodigoMaxLength)
                .IsRequired();

            Property(x => x.DigitoVerificador)
                .HasMaxLength(Agencia.DigitoVerificadorMaxLength);

            Property(x => x.Nome)
                .HasMaxLength(Agencia.NomeMaxLength)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}