﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class TemplateEmailMap : EntityTypeConfiguration<TemplateEmail>
    {
        public TemplateEmailMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                    .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(TemplateEmail.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_TemplateEmail_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.TipoDeTemplateDeMensagem)
                .IsRequired();

            Property(x => x.Remetente)
                .HasMaxLength(TemplateEmail.RemetenteMaxLength)
                .IsRequired();

            Property(x => x.Assunto)
                .HasMaxLength(TemplateEmail.AssuntoMaxLength)
                .IsRequired();

            Property(x => x.CorpoMensagem)
                .HasMaxLength(TemplateEmail.MensagemMaxLength)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}