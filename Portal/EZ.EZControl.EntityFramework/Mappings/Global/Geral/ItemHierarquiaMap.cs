﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class ItemHierarquiaMap : EntityTypeConfiguration<ItemHierarquia>
    {
        public ItemHierarquiaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(ItemHierarquia.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_ItemHierarquia_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Mascara)
                .HasMaxLength(ItemHierarquia.MascaraMaxLength);

            HasOptional(x => x.ItemHierarquiaPai)
                .WithMany(x => x.Itens)
                .HasForeignKey(x => x.ItemHierarquiaPaiId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}