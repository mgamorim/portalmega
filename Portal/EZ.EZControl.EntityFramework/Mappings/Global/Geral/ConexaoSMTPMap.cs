﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class ConexaoSMTPMap : EntityTypeConfiguration<ConexaoSMTP>
    {
        public ConexaoSMTPMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(ConexaoSMTP.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_ConexaoSMTP_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.ServidorSMTP)
                .HasMaxLength(ConexaoSMTP.ServidorSMTPMaxLength)
                .IsRequired();

            Property(x => x.Porta).IsRequired();

            Property(x => x.UsuarioAutenticacao)
                .HasMaxLength(ConexaoSMTP.UsuarioAutenticacaoMaxLength);

            Property(x => x.SenhaAutenticacao)
                .HasMaxLength(ConexaoSMTP.SenhaAutenticacaoMaxLength);

            Property(x => x.RequerAutenticacao);

            Property(x => x.RequerConexaoCriptografada);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}