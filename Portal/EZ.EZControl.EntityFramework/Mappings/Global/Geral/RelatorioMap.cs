﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class RelatorioMap : EntityTypeConfiguration<Relatorio>
    {
        public RelatorioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Relatorio.NomeMaxLength)
                .IsRequired();

            Property(x => x.Sistema)
                .IsRequired();

            Property(x => x.TipoDeRelatorio)
                .IsRequired();

            Property(x => x.Conteudo)
                .HasColumnType("ntext");

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}