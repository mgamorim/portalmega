﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class BancoMap : EntityTypeConfiguration<Banco>
    {
        public BancoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Banco.NomeMaxLength)
                .IsRequired();

            Property(x => x.Codigo)
                .HasMaxLength(Banco.CodigoMaxLength)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Banco_Codigo")
                    {
                        IsUnique = true
                    }));

            HasMany(x => x.Agencias);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}