﻿using EZ.EZControl.Domain.Global.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class ProfissaoMap : EntityTypeConfiguration<Profissao>
    {
        public ProfissaoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Codigo)
                .IsRequired()
                .HasMaxLength(Profissao.CodigoMaxLength);

            Property(x => x.Titulo)
                .IsRequired()
                .HasMaxLength(Profissao.TituloMaxLength);
        }
    }
}