﻿using EZ.EZControl.Domain.Global.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class FeriadoMap : EntityTypeConfiguration<Feriado>
    {
        public FeriadoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Feriado.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Feriado_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Data)
                .IsRequired();

            Property(x => x.Abrangencia)
                .IsRequired();

            Property(x => x.Tipo)
                .IsRequired();

            HasOptional(x => x.Pais)
                .WithMany(x => x.Feriados)
                .HasForeignKey(x => x.PaisId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Estado)
                .WithMany(x => x.Feriados)
                .HasForeignKey(x => x.EstadoId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Cidade)
                .WithMany(x => x.Feriados)
                .HasForeignKey(x => x.CidadeId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}