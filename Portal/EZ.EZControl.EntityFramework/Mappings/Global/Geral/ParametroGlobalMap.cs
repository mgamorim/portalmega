﻿using EZ.EZControl.Domain.Global.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Geral
{
    public class ParametroGlobalMap : EntityTypeConfiguration<ParametroGlobal>
    {
        public ParametroGlobalMap()
        {
            HasKey(x => x.Id);

            HasOptional(x => x.ConexaoSMTP)
                .WithMany()
                .HasForeignKey(x => x.ConexaoSMTPId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}