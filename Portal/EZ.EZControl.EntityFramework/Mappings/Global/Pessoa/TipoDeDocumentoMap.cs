﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class TipoDeDocumentoMap : EntityTypeConfiguration<TipoDeDocumento>
    {
        public TipoDeDocumentoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TipoPessoa)
                .IsRequired();

            Property(x => x.Descricao)
                .HasMaxLength(TipoDeDocumento.MaxDescricaoLength)
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_TipoDeDocumento_Descricao")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Mascara)
                .HasMaxLength(TipoDeDocumento.MaxMascaraLength);

            Property(x => x.Principal)
                .IsRequired();

            Property(x => x.ObrigaAnexo)
                .IsRequired();

            Property(x => x.PrazoDeAtualizacaoDoAnexo);

            Property(x => x.Prioridade);

            Property(x => x.ObrigaOrgaoExpedidor)
                .IsRequired();

            Property(x => x.ObrigaDataExpedicao)
                .IsRequired();

            Property(x => x.ObrigaUF)
                .IsRequired();

            Property(x => x.ObrigaDataDeValidade)
                .IsRequired();

            Property(x => x.SerieObrigatoria)
                .IsRequired();

            Property(x => x.ListaEmOutrosDocumentos)
                .IsRequired();

            Property(x => x.ExibirNoCadastroSimplificadoDePessoa)
                .IsRequired();

            Property(x => x.PermiteLancamentoDuplicado)
                .IsRequired();

            Property(x => x.TipoDeDocumentoFixo)
                .IsRequired();
        }
    }
}