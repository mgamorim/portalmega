﻿using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class PessoaMap : EntityTypeConfiguration<Domain.Global.Pessoa.Pessoa>
    {
        public PessoaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Observacao)
                .HasMaxLength(Domain.Global.Pessoa.Pessoa.MaxObservacaoLength);

            HasOptional(x => x.EnderecoPrincipal)
                .WithMany()
                .HasForeignKey(x => x.EnderecoPrincipalId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.EnderecoCobranca)
                .WithMany()
                .HasForeignKey(x => x.EnderecoCobrancaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.EnderecoEntrega)
                .WithMany()
                .HasForeignKey(x => x.EnderecoEntregaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.DocumentoPrincipal)
                .WithMany()
                .HasForeignKey(x => x.DocumentoPrincipalId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.GrupoPessoa)
                .WithMany()
                .HasForeignKey(x => x.GrupoPessoaId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Documentos);

            HasMany(x => x.EnderecosEletronicos);

            HasMany(x => x.Enderecos);

            HasMany(x => x.Contatos);

            HasMany(x => x.Telefones);

            Ignore(x => x.NomePessoa);

            Ignore(x => x.RazaoSocialPessoa);

            Ignore(x => x.PessoaJuridica);

            Ignore(x => x.PessoaFisica);
        }
    }
}