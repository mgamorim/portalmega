﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class TelefoneMap : EntityTypeConfiguration<Telefone>
    {
        public TelefoneMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Tipo)
                .IsRequired();

            Property(x => x.DDI)
                .HasMaxLength(Telefone.MaxDDILength);

            Property(x => x.DDD)
                .HasMaxLength(Telefone.MaxDDDLength)
                .IsRequired();

            Property(x => x.Numero)
                .HasMaxLength(Telefone.MaxNumeroLength)
                .IsRequired();

            Property(x => x.Ramal)
                .HasMaxLength(Telefone.MaxRamalLength);

            HasRequired(x => x.Pessoa)
                .WithMany(x => x.Telefones)
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}