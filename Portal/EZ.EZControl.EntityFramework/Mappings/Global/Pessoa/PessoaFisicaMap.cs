﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class PessoaFisicaMap : EntityTypeConfiguration<PessoaFisica>
    {
        public PessoaFisicaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .HasMaxLength(PessoaFisica.MaxNomeLength)
                .IsRequired();

            Property(x => x.Sexo);

            Property(x => x.PossuiFilhos).IsRequired();

            Property(x => x.EstadoCivil);

            Property(x => x.DataDeNascimento);

            HasOptional(x => x.Tratamento)
                .WithOptionalDependent();

            Property(x => x.Nacionalidade)
                .IsOptional();
        }
    }
}