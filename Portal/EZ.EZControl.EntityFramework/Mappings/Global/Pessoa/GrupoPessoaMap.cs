﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;
using System.Security.Cryptography.X509Certificates;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class GrupoPessoaMap : EntityTypeConfiguration<GrupoPessoa>
    {
        public GrupoPessoaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(GrupoPessoa.MaxNomeLength)
                .IsRequired();

            HasOptional(x => x.Pai)
                .WithMany(x => x.Filhos)
                .HasForeignKey(x => x.PaiId)
                .WillCascadeOnDelete(false);

            Property(x => x.Descricao);

            Property(x => x.TipoDePessoa)
                .IsRequired();

            Property(x => x.Slug)
                .HasMaxLength(GrupoPessoa.MaxSlugLength)
                .IsOptional();
        }
    }
}