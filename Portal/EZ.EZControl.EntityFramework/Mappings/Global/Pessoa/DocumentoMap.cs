﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class DocumentoMap : EntityTypeConfiguration<Documento>
    {
        public DocumentoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Numero)
                .HasMaxLength(Documento.MaxNumeroLength)
                .IsRequired();

            Property(x => x.OrgaoExpedidor)
                .HasMaxLength(Documento.MaxOrgaoExpedidorLength);

            Property(x => x.DataExpedicao);

            Property(x => x.DataDeValidade);

            Property(x => x.Serie)
                .HasMaxLength(Documento.MaxSerieLength);

            HasRequired(x => x.TipoDeDocumento)
                .WithMany()
                .HasForeignKey(x => x.TipoDeDocumentoId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Estado)
                .WithMany()
                .HasForeignKey(x => x.EstadoId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Pessoa)
                .WithMany(x => x.Documentos)
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}