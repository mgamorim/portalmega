﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class TratamentoMap : EntityTypeConfiguration<Tratamento>
    {
        public TratamentoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Descricao)
                .HasMaxLength(Tratamento.MaxDescricaoLength)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}