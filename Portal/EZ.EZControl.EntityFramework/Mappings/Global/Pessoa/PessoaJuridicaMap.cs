﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class PessoaJuridicaMap : EntityTypeConfiguration<PessoaJuridica>
    {
        public PessoaJuridicaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.RazaoSocial)
                .HasMaxLength(PessoaJuridica.MaxRazaoSocialLength)
                .IsRequired();

            Property(x => x.NomeFantasia)
                .HasMaxLength(PessoaJuridica.MaxNomeFantasiaLength)
                .IsRequired();
        }
    }
}