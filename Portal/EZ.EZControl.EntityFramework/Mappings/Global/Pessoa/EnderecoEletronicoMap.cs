﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class EnderecoEletronicoMap : EntityTypeConfiguration<EnderecoEletronico>
    {
        public EnderecoEletronicoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.TipoDeEnderecoEletronico).IsRequired();

            Property(x => x.Endereco)
                .HasMaxLength(EnderecoEletronico.MaxEnderecoLength)
                .IsRequired();

            HasRequired(x => x.Pessoa)
                .WithMany(x => x.EnderecosEletronicos)
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Contato)
                .WithMany(x => x.EnderecosEletronicos)
                .HasForeignKey(x => x.ContatoId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}