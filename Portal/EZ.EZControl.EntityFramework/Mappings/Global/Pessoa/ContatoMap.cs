﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class ContatoMap : EntityTypeConfiguration<Contato>
    {
        public ContatoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .HasMaxLength(Contato.MaxNomeLength)
                .IsRequired();

            Property(x => x.DataNascimento);

            Property(x => x.Sexo);

            Property(x => x.Cargo)
                .HasMaxLength(Contato.MaxCargoLength);

            Property(x => x.Setor)
                .HasMaxLength(Contato.MaxSetorLength);

            Property(x => x.Idioma)
                .HasMaxLength(Contato.MaxIdiomaLength);

            Property(x => x.Observacao)
                .HasMaxLength(Contato.MaxObservacaoLength);

            HasRequired(x => x.Pessoa)
                .WithMany(x => x.Contatos)
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.TipoDeContato)
                .WithMany()
                .HasForeignKey(x => x.TipoDeContatoId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Tratamento)
                .WithMany()
                .HasForeignKey(x => x.TratamentoId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.EnderecoPrincipal)
                .WithMany()
                .HasForeignKey(x => x.EnderecoPrincipalId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Telefones);

            HasMany(x => x.Enderecos);

            HasMany(x => x.EnderecosEletronicos);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}