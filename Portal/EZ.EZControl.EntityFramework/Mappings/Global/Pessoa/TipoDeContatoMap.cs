﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Pessoa
{
    public class TipoDeContatoMap : EntityTypeConfiguration<TipoDeContato>
    {
        public TipoDeContatoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Descricao)
                .HasMaxLength(TipoDeContato.MaxDescricaoLength)
                .IsRequired();

            Property(x => x.Mascara)
                .HasMaxLength(TipoDeContato.MaxMascaraLength);

            Property(x => x.TipoDeContatoFixo)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}