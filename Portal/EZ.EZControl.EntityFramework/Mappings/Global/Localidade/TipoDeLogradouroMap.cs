﻿using EZ.EZControl.Domain.Global.Localidade;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Localidade
{
    public class TipoDeLogradouroMap : EntityTypeConfiguration<TipoDeLogradouro>
    {
        public TipoDeLogradouroMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Descricao)
                .HasMaxLength(TipoDeLogradouro.MaxDescricaoLength)
                .IsRequired();
        }
    }
}