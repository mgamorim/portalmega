﻿using EZ.EZControl.Domain.Global.Localidade;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Localidade
{
    public class EnderecoMap : EntityTypeConfiguration<Endereco>
    {
        public EnderecoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Bairro)
                .HasMaxLength(Endereco.MaxBairroLength);

            Property(x => x.CEP)
                .HasMaxLength(Endereco.MaxCEPLength);

            Property(x => x.Complemento)
                .HasMaxLength(Endereco.MaxComplementoLength);

            Property(x => x.Descricao)
                .HasMaxLength(Endereco.MaxDescricaoLength);

            Property(x => x.EnderecoParaEntrega);
                // TODO: Marcos. Esse campo é obrigatorio?
                //.IsRequired();

            Property(x => x.Logradouro)
                .HasMaxLength(Endereco.MaxLogradouroLength)
                .IsRequired();

            Property(x => x.Numero)
                .HasMaxLength(Endereco.MaxNumeroLength)
                .IsRequired();

            Property(x => x.TipoEndereco);

            Property(x => x.Referencia)
                .HasMaxLength(Endereco.MaxReferenciaLength);

            HasRequired(x => x.Pessoa)
                .WithMany(x => x.Enderecos)
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Contato)
                .WithMany(x => x.Enderecos)
                .HasForeignKey(x => x.ContatoId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.TipoDeLogradouro)
                .WithMany()
                .HasForeignKey(x => x.TipoDeLogradouroId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Cidade)
                .WithMany()
                .HasForeignKey(x => x.CidadeId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Telefones);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}