﻿using EZ.EZControl.Domain.Global.Localidade;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Localidade
{
    public class EstadoMap : EntityTypeConfiguration<Estado>
    {
        public EstadoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Estado.MaxNomeLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Estado_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Sigla)
                .HasMaxLength(Estado.MaxSiglaLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Estado_Sigla")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Capital)
                .HasMaxLength(Estado.MaxCapitalLength)
                .IsRequired();

            Property(x => x.CodigoIBGE)
                .HasMaxLength(Estado.MaxCodigoIBGELength);

            HasRequired(x => x.Pais)
                .WithMany(x => x.Estados)
                .HasForeignKey(x => x.PaisId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Municipios);

            HasMany(x => x.Feriados);
        }
    }
}