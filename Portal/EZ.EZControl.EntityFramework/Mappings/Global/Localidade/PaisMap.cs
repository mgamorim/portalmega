﻿using EZ.EZControl.Domain.Global.Localidade;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Localidade
{
    public class PaisMap : EntityTypeConfiguration<Pais>
    {
        public PaisMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Pais.MaxNomeLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Pais_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Nacionalidade)
                .HasMaxLength(Pais.MaxNacionlidadeLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Pais_Nacionalidade")
                    {
                        IsUnique = true
                    }));

            Property(x => x.CodigoISO)
                .HasMaxLength(Pais.MaxCodigoISOLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Pais_CodigoIso")
                    {
                        IsUnique = true
                    }));

            Property(x => x.CodigoInternacional)
                .HasMaxLength(Pais.MaxCodigoInternacionaLength);

            Property(x => x.CodigoReceitaFederal)
                .HasMaxLength(Pais.MaxCodigoReceitaFederaLength);

            Property(x => x.MascaraDoCodigoPostal)
                .HasMaxLength(Pais.MaxMascaraDoCodigoPostaLength);

            Property(x => x.Sigla2Caracteres)
                .HasMaxLength(Pais.MaxSigla2Caracteres);

            Property(x => x.Sigla3Caracteres)
                .HasMaxLength(Pais.MaxSigla3Caracteres);

            HasMany(x => x.Estados);

            HasMany(x => x.Feriados);
        }
    }
}