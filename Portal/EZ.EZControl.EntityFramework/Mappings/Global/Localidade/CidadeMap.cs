﻿using EZ.EZControl.Domain.Global.Localidade;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Localidade
{
    public class CidadeMap : EntityTypeConfiguration<Cidade>
    {
        public CidadeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Cidade.MaxNomeLength)
                .IsRequired();

            Property(x => x.CodigoIBGE)
                .HasMaxLength(Cidade.MaxCodigoIBGELength)
                .IsOptional();

            HasRequired(x => x.Estado)
                .WithMany(x => x.Municipios)
                .HasForeignKey(x => x.EstadoId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Feriados);
        }
    }
}