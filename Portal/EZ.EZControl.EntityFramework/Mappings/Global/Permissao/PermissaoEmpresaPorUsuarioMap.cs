﻿using EZ.EZControl.Domain.Global.Permissao;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Permissao
{
    public class PermissaoEmpresaPorUsuarioMap : EntityTypeConfiguration<PermissaoEmpresaPorUsuario>
    {
        public PermissaoEmpresaPorUsuarioMap()
        {
            ToTable("Glb_PermissaoEmpresa");

            Property(x => x.UserId).IsRequired();
        }
    }
}