﻿using EZ.EZControl.Domain.Global.Permissao;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Permissao
{
    public class PermissaoEmpresaPorRoleMap : EntityTypeConfiguration<PermissaoEmpresaPorRole>
    {
        public PermissaoEmpresaPorRoleMap()
        {
            ToTable("Glb_PermissaoEmpresa");

            Property(x => x.RoleId).IsRequired();
        }
    }
}