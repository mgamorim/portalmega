﻿using EZ.EZControl.Domain.Global.Permissao;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Global.Permissao
{
    public class PermissaoEmpresaMap : EntityTypeConfiguration<PermissaoEmpresa>
    {
        public PermissaoEmpresaMap()
        {
            ToTable("Glb_PermissaoEmpresa");

            HasKey(x => x.Id);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            Property(x => x.IsGranted);

            Property(x => x.PermissionName).IsRequired();
        }
    }
}