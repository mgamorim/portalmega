﻿using EZ.EZControl.Domain.CRM.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CRM.Geral
{
    public class LeadMap : EntityTypeConfiguration<Lead>
    {
        public LeadMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            HasRequired(x => x.PessoaFisica)
                .WithMany()
                .HasForeignKey(x => x.PessoaFisicaId)
                .WillCascadeOnDelete(false);
        }
    }
}
