﻿using System.Data.Entity.ModelConfiguration;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Mappings.Agenda.Regra
{
    public class RegraReuniaoMap : EntityTypeConfiguration<RegraReuniao>
    {
        public RegraReuniaoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
