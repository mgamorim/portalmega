﻿using System.Data.Entity.ModelConfiguration;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Mappings.Agenda.Regra
{
    public class RegraConsultaMap : EntityTypeConfiguration<RegraConsulta>
    {
        public RegraConsultaMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Especialidade)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("EspecialidadeId");
                })
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Medico)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("MedicoId");
                })
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
