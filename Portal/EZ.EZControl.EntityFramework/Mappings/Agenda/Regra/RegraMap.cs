﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Mappings.Agenda.Regra
{
    public class RegraMap : EntityTypeConfiguration<RegraBase>
    {
        public RegraMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive)
                .IsRequired();
            Property(x => x.Sistema)
                .IsRequired();
            Property(x => x.TipoDeEvento)
                .IsRequired();
            Property(x => x.DiasExibidosDesdeHojeAteAgendamento)
                .IsRequired();
            Property(x => x.VagasDisponibilizadasPorDia)
                .IsRequired();
            Property(x => x.VagasReservadasPorDia);
            Property(x => x.UnidadeDeTempo)
                .IsRequired();
            Property(x => x.VagasPorUnidadeDeTempo);
            Property(x => x.DiasAntesDoAgendamentoProibidoAlterar);
            Property(x => x.OverBook);
            Property(x => x.DiasParaNovoAgendamentoPorParticipante);
            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
