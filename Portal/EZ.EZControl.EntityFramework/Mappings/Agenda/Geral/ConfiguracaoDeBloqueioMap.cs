﻿using EZ.EZControl.Domain.Agenda.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class ConfiguracaoDeBloqueioMap : EntityTypeConfiguration<ConfiguracaoDeBloqueio>
    {
        public ConfiguracaoDeBloqueioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Titulo)
                .HasMaxLength(ConfiguracaoDeBloqueio.TituloMaxLength)
                .IsRequired();

            Property(x => x.MotivoDoBloqueio)
                 .IsRequired();

            Property(x => x.TipoDeEvento)
                .IsRequired();

            Property(x => x.Sistema)
                .IsRequired();

            Property(x => x.HorarioInicio)
                .IsRequired();

            Property(x => x.HorarioFim)
                .IsRequired();

            Property(x => x.InicioBloqueio);

            Property(x => x.FimBloqueio);

            HasMany(x => x.Bloqueios)
                .WithRequired(x => x.ConfiguracaoDeBloqueio)
                .HasForeignKey(x => x.ConfiguracaoDeBloqueioId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
