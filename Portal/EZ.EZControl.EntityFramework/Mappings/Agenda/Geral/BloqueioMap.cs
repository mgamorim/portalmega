﻿using EZ.EZControl.Domain.Agenda.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class BloqueioMap : EntityTypeConfiguration<Bloqueio>
    {
        public BloqueioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Data)
                 .IsRequired();

            Property(x => x.Horario)
                 .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
