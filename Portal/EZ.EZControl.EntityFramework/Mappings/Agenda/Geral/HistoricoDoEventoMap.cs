﻿using EZ.EZControl.Domain.Agenda.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class HistoricoDoEventoMap : EntityTypeConfiguration<HistoricoDoEvento>
    {
        public HistoricoDoEventoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            Property(x => x.Data)
                 .IsRequired();

            Property(x => x.Status)
                 .IsRequired();

            Property(x => x.Observacao);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
