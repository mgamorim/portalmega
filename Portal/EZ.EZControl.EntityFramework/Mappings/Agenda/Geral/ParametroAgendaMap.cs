﻿using System.Data.Entity.ModelConfiguration;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class ParametroAgendaMap : EntityTypeConfiguration<ParametroAgenda>
    {
        public ParametroAgendaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TamanhoMaximoMb);

            Property(x => x.AlturaMaximaPx);

            Property(x => x.LarguraMaximaPx);

            Property(x => x.ExtensoesDocumento);

            Property(x => x.ExtensoesImagem);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}