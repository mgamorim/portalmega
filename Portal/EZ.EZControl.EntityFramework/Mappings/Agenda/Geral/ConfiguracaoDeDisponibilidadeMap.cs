﻿using EZ.EZControl.Domain.Agenda.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class ConfiguracaoDeDisponibilidadeMap : EntityTypeConfiguration<ConfiguracaoDeDisponibilidade>
    {
        public ConfiguracaoDeDisponibilidadeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Titulo)
                .HasMaxLength(ConfiguracaoDeDisponibilidade.TituloMaxLength)
                .IsRequired();

            Property(x => x.TipoDeDisponibilidade)
                 .IsRequired();

            Property(x => x.TipoDeEvento)
                .IsRequired();

            Property(x => x.Sistema)
                .IsRequired();

            Property(x => x.DiasDaSemana);

            Property(x => x.NumeroDeDias);

            Property(x => x.DataEspecifica);

            Property(x => x.HorarioInicio)
                .IsRequired();

            Property(x => x.HorarioFim)
                .IsRequired();

            Property(x => x.DataInicioValidade);

            Property(x => x.DataFimValidade);

            HasMany(x => x.Disponibilidades)
                .WithRequired(x => x.ConfiguracaoDeDisponibilidade)
                .HasForeignKey(x => x.ConfiguracaoDeDisponibilidadeId)
                .WillCascadeOnDelete(true);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
