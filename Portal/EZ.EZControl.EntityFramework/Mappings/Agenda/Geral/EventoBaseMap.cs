﻿using EZ.EZControl.Domain.Agenda.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class EventoBaseMap : EntityTypeConfiguration<EventoBase>
    {
        public EventoBaseMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Descricao)
                .HasMaxLength(EventoBase.DescricaoMaxLength);

            Property(x => x.Termino).IsRequired();

            Property(x => x.TerminoEndZone);

            Property(x => x.DiaInteiro);

            Property(x => x.Inicio)
                .IsRequired();

            Property(x => x.InicioTimeZone);

            HasOptional(x => x.EventoPai)
                .WithMany(x => x.Itens)
                .HasForeignKey(x => x.EventoPaiId)
                .WillCascadeOnDelete(false);

            Property(x => x.RecurrenceRule)
                .HasMaxLength(EventoBase.RecurrenceRuleMaxLength);

            Property(x => x.RecurrenceException);

            Property(x => x.Titulo)
                .HasMaxLength(EventoBase.TituloMaxLength)
                .IsRequired();

            Property(x => x.TipoDeEvento).IsRequired();

            Property(x => x.Sistema).IsRequired();

            HasMany(x => x.Historico)
                .WithRequired(x => x.Evento)
                .HasForeignKey(x => x.EventoId)
                .WillCascadeOnDelete(true);

            HasRequired(x => x.Owner)
                .WithMany()
                .HasForeignKey(x => x.OwnerId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Disponibilidade)
                .WithMany()
                .HasForeignKey(x => x.DisponibilidadeId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Participantes)
               .WithMany()
               .Map(x =>
               {
                   x.ToTable("Agd_Evento_Participante");
                   x.MapLeftKey("EventoId");
                   x.MapRightKey("PessoaId");
               });

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}