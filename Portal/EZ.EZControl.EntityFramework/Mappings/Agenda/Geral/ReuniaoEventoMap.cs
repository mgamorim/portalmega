﻿using EZ.EZControl.Domain.Agenda.Geral;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Mappings.Agenda.Geral
{
    public class ReuniaoEventoMap : EntityTypeConfiguration<EventoReuniao>
    {
        public ReuniaoEventoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
