﻿using EZ.EZControl.Domain.Vendas.Pedidos;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Vendas.Pedidos
{
    public class PedidoMap : EntityTypeConfiguration<Pedido>
    {
        public PedidoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Cliente)
                .WithMany()
                .HasForeignKey(x => x.ClienteId);

            HasMany(Pedido.ItemDePedidoExpression);

            Property(x => x.Status).IsRequired();

            Property(x => x.Tipo).IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
            Property(x => x.FormaDePagamento);
        }
    }
}