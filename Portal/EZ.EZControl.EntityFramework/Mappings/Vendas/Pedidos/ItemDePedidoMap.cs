﻿using EZ.EZControl.Domain.Vendas.Pedidos;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Vendas.Pedidos
{
    public class ItemDePedidoMap : EntityTypeConfiguration<ItemDePedido>
    {
        public ItemDePedidoMap()
        {
            HasKey(x => x.Id);

            Ignore(x => x.Nome);

            Property(x => x.Valor).IsRequired();
            Property(x => x.ValorCadastro).IsRequired();
            Property(x => x.TipoDeItemDePedido);
            Property(x => x.Quantidade);

            HasOptional(x => x.Servico)
                .WithMany()
                .HasForeignKey(x => x.ServicoId)
                .WillCascadeOnDelete(true);

            HasOptional(x => x.Produto)
                .WithMany()
                .HasForeignKey(x => x.ProdutoId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Pedido)
                .WithMany(Pedido.ItemDePedidoExpression)
                .HasForeignKey(x => x.PedidoId)
                .WillCascadeOnDelete(false);            
        }
    }
}