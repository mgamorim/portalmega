﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class CorretorMap : EntityTypeConfiguration<Corretor>
    {
        public CorretorMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.Pessoa)
                .WithMany()
                .HasForeignKey(x => x.PessoaId)
                .WillCascadeOnDelete(false);


            Ignore(x => x.PessoaFisica);

            Ignore(x => x.PessoaJuridica);

            HasMany(s => s.Corretoras)
                .WithMany(c => c.Corretores)
                .Map(cs =>
                {
                    cs.ToTable("Sau_Corretor_Corretora");
                    cs.MapLeftKey("CorretorId");
                    cs.MapRightKey("CorretoraId");
                });
        }
    }
}
