﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class BeneficiarioMap : EntityTypeConfiguration<BeneficiarioBase>
    {
        public BeneficiarioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.PessoaFisica)
                .WithMany()
                .HasForeignKey(x => x.PessoaFisicaId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.Pessoa);

            Ignore(x => x.NomePessoa);

            Property(x => x.TipoDeBeneficiario).IsRequired();
            Property(x => x.NomeDaMae).IsOptional();

            Property(x => x.NumeroDoCartaoNacionalDeSaude).IsOptional();
            Property(x => x.DeclaracaoDeNascidoVivo).IsOptional();

            HasMany(x => x.Administradoras)
                .WithMany()
                .Map(
                    m => {
                        m.MapLeftKey("BeneficiarioId");
                        m.MapRightKey("AdministradoraId");
                        m.ToTable("Sau_Administradora_Beneficiario");
                    }
                );

            HasMany(x => x.Corretoras)
                .WithMany()
                .Map(
                    m => {
                        m.MapLeftKey("BeneficiarioId");
                        m.MapRightKey("CorretoraId");
                        m.ToTable("Sau_Corretora_Beneficiario");
                    }
                );
        }
    }
}
