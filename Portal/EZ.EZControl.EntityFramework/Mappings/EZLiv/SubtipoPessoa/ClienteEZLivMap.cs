﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class ClienteEZLivMap : EntityTypeConfiguration<ClienteEZLiv>
    {
        public ClienteEZLivMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.Cliente)
                .WithMany()
                .HasForeignKey(x => x.ClienteId)
                .WillCascadeOnDelete(false);
        }
    }
}
