﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class ResponsavelMap : EntityTypeConfiguration<Responsavel>
    {
        public ResponsavelMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasRequired(x => x.PessoaFisica)
                .WithMany()
                .HasForeignKey(x => x.PessoaFisicaId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.Pessoa);

            Ignore(x => x.NomePessoa);

            Property(x => x.TipoDeResponsavel).IsRequired();

           
        }
    }
}
