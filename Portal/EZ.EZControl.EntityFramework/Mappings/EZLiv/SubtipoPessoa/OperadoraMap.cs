﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class OperadoraMap : EntityTypeConfiguration<Operadora>
    {
        public OperadoraMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
               .HasMaxLength(Operadora.NomeMaxLength)
               .IsRequired();

            Property(x => x.CodigoAns)
               .HasMaxLength(Operadora.CodigoAnsMaxLength)
               .IsRequired();

            HasMany(x => x.ProdutosDePlanoDeSaude);

            Ignore(x => x.Pessoa);

            Ignore(x => x.NomePessoa);

            HasRequired(x => x.PessoaJuridica)
                .WithMany()
                .HasForeignKey(x => x.PessoaJuridicaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Imagem)
             .WithMany()
             .HasForeignKey(x => x.ImagemId)
             .WillCascadeOnDelete(false);
        }
    }
}
