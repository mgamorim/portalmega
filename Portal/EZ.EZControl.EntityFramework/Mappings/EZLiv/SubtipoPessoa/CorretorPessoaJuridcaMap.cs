﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class CorretorPessoaJuridcaMap : EntityTypeConfiguration<CorretorPessoaJuridca>
    {
        public CorretorPessoaJuridcaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Cnpj);
            Property(x => x.NomeFantasia);

            Property(x => x.RazaoSocial);

            Property(x => x.Observacao);         
     
            HasRequired(x => x.Corretor)
                .WithMany()
                .HasForeignKey(x => x.CorretorId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.PessoaFisica);

            Ignore(x => x.PessoaJuridica);

        }
    }
}
