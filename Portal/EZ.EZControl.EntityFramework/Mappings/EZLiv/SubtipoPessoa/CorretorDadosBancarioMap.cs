﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class CorretorDadosBancarioMap : EntityTypeConfiguration<CorretorDadosBancario>
    {
        public CorretorDadosBancarioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.CodigoAgencia);

            Property(x => x.TipoConta);

            Property(x => x.NomeAgencia);

            Property(x => x.ContaCorrente);

            Property(x => x.DigitoVerificador);

            Property(x => x.NomeFavorecido);

            Property(x => x.BancoId);

            HasRequired(x => x.Corretor)
                .WithMany()
                .HasForeignKey(x => x.CorretorId)
                .WillCascadeOnDelete(false);

            Ignore(x => x.PessoaFisica);

            Ignore(x => x.PessoaJuridica);
        }
    }
}
