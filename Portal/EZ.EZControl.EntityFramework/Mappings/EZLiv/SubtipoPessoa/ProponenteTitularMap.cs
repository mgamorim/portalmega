﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class ProponenteTitularMap : EntityTypeConfiguration<ProponenteTitular>
    {
        public ProponenteTitularMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            HasOptional(x => x.Responsavel);

            Property(x => x.StatusBeneficiario).IsRequired();

            HasMany(x => x.Dependentes)
              .WithMany()
              .Map(x =>
              {
                  x.ToTable("Sau_ProponeteTitular_Dependente");
                  x.MapLeftKey("ProponeteTitularId");
                  x.MapRightKey("DependenteId");
              });

           
        }
    }
}
