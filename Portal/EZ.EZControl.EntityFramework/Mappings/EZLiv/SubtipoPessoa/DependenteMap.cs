﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class DependenteMap : EntityTypeConfiguration<Dependente>
    {
        public DependenteMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.GrauDeParentesco)
                .IsRequired();

            

            //HasRequired(x => x.Titular)
            //    .WithMany(x => x.Dependentes)
            //    .HasForeignKey(x => x.TitularId);
        }
    }
}
