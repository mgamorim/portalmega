﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class AdministradoraMap : EntityTypeConfiguration<Administradora>
    {
        public AdministradoraMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Ignore(x => x.Pessoa);

            Ignore(x => x.NomePessoa);

            HasMany(x => x.ProdutosDePlanoDeSaude);

            HasRequired(x => x.PessoaJuridica)
                .WithMany()
                .HasForeignKey(x => x.PessoaJuridicaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Imagem)
               .WithMany()
               .Map(x => x.MapKey("ImagemId"))
               .WillCascadeOnDelete(false);

            HasMany(x => x.Corretoras)
                .WithMany()
                .Map(x =>
                {
                    x.ToTable("Sau_Administradora_Corretora");
                    x.MapLeftKey("AdministradoraId");
                    x.MapRightKey("CorretoraId");
                });
        }
    }
}
