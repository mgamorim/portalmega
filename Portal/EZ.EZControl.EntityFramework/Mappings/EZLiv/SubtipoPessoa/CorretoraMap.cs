﻿using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.SubtipoPessoa
{
    public class CorretoraMap : EntityTypeConfiguration<Corretora>
    {
        public CorretoraMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired();

            Property(x => x.IsActive)
                .IsRequired();

            Ignore(x => x.Pessoa);

            Ignore(x => x.NomePessoa);

            HasRequired(x => x.PessoaJuridica)
                .WithMany()
                .HasForeignKey(x => x.PessoaJuridicaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Imagem)
               .WithMany()
               .Map(x => x.MapKey("ImagemId"))
               .WillCascadeOnDelete(false);

            HasOptional(x => x.Corretor)
                .WithMany()
                .HasForeignKey(x => x.CorretorId)
                .WillCascadeOnDelete(false);


        }
    }
}
