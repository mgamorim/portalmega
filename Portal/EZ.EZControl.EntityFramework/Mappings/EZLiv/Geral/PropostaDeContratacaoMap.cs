﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class PropostaDeContratacaoMap : EntityTypeConfiguration<PropostaDeContratacao>
    {
        public PropostaDeContratacaoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Titular)
               .WithMany()
               .HasForeignKey(x => x.TitularId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.Produto)
               .WithMany()
               .HasForeignKey(x => x.ProdutoId)
               .WillCascadeOnDelete(false);

            HasMany(x => x.ProdutosAdicionais)
               .WithMany();

            HasOptional(x => x.Responsavel)
               .WithMany()
               .HasForeignKey(x => x.ResponsavelId)
               .WillCascadeOnDelete(false);

            Property(x => x.Aceite);

            Property(x => x.EServidorPublico);

            Property(x => x.AceiteDaDeclaracaoDeSaude);

            Property(x => x.TitularResponsavelLegal);

            Property(x => x.TipoDeHomologacao)
                .IsRequired();

            Property(x => x.DataHoraDaHomologacao);

            Property(x => x.ObservacaoHomologacao);

            Property(x => x.DataHoraDoAceite);

            HasOptional(x => x.ContratoVigenteNoAceite)
               .WithMany()
               .HasForeignKey(x => x.ContratoVigenteNoAceiteId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.UltimoContratoAceito)
               .WithMany()
               .HasForeignKey(x => x.UltimoContratoAceitoId)
               .WillCascadeOnDelete(false);

            HasRequired(x => x.Corretor)
                .WithMany()
                .HasForeignKey(x => x.CorretorId)
                .WillCascadeOnDelete(false);

            Property(x => x.StatusDaProposta)
                .IsRequired();

            Property(x => x.PassoDaProposta)
                .IsRequired();

            Property(x => x.FormaDeContratacao)
               .IsRequired();

            Property(x => x.InicioDeVigencia);

            HasOptional(x => x.Pedido)
               .WithMany()
               .HasForeignKey(x => x.PedidoId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.Corretora)
               .WithMany()
               .HasForeignKey(x => x.CorretoraId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.Vigencia)
               .WithMany()
               .HasForeignKey(x => x.VigenciaId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.Chancela)
               .WithMany()
               .HasForeignKey(x => x.ChancelaId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.UsuarioTitular)
               .WithMany()
               .HasForeignKey(x => x.UsuarioTitularId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.UsuarioResponsavel)
               .WithMany()
               .HasForeignKey(x => x.UsuarioResponsavelId)
               .WillCascadeOnDelete(false);

            HasMany(x => x.Documentos)
               .WithRequired(x => x.PropostaDeContratacao)
               .HasForeignKey(x => x.PropostaDeContratacaoId)
               .WillCascadeOnDelete(false);

            HasMany(s => s.Aditivos)
               .WithMany()
               .Map(cs =>
               {
                   cs.ToTable("Sau_PropostaDeContratacao_Aditivo");
                   cs.MapLeftKey("PropostaDeContratacaoId");
                   cs.MapRightKey("AditivoId");
               });

            HasMany(s => s.UsuariosDependentes)
               .WithMany()
               .Map(cs =>
               {
                   cs.ToTable("Sau_PropostaDeContratacao_UserDependente");
                   cs.MapLeftKey("PropostaDeContratacaoId");
                   cs.MapRightKey("UserId");
               });

            HasOptional(x => x.Profissao)
             .WithMany()
             .HasForeignKey(x => x.ProfissaoId)
             .WillCascadeOnDelete(false);

            HasOptional(x => x.Estado)
                .WithMany()
                .HasForeignKey(x => x.EstadoId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.ItemDeDeclaracaoDoBeneficiario)
                .WithMany()
                .HasForeignKey(x => x.ItemDeDeclaracaoDoBeneficiarioId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.GerenteCorretora)
                .WithMany()
                .HasForeignKey(x => x.GerenteId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.SupervisorCorretora)
                .WithMany()
                .HasForeignKey(x => x.SupervisorId)
                .WillCascadeOnDelete(false);
        }
    }
}
