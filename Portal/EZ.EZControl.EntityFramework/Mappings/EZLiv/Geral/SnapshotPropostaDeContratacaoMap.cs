﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class SnapshotPropostaDeContratacaoMap : EntityTypeConfiguration<SnapshotPropostaDeContratacao>
    {
        public SnapshotPropostaDeContratacaoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.PropostaDeContratacao)
               .WithMany()
               .HasForeignKey(x => x.PropostaDeContratacaoId)
               .WillCascadeOnDelete(false);

            Property(x => x.Json)
                .IsRequired();
        }
    }
}
