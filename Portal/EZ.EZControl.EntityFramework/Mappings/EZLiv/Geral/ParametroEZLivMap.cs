﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ParametroEZLivMap : EntityTypeConfiguration<ParametroEZLiv>
    {
        public ParametroEZLivMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TamanhoMaximoMb);

            Property(x => x.AlturaMaximaPx);

            Property(x => x.LarguraMaximaPx);

            Property(x => x.ExtensoesDocumento);

            Property(x => x.ExtensoesImagem);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
