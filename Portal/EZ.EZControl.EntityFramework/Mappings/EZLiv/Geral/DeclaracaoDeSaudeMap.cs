﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class DeclaracaoDeSaudeMap : EntityTypeConfiguration<DeclaracaoDeSaude>
    {
        public DeclaracaoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
               .HasMaxLength(DeclaracaoDeSaude.NomeMaxLength)
               .IsRequired();

            HasRequired(x => x.Titular)
               .WithMany()
               .HasForeignKey(x => x.TitularId)
               .WillCascadeOnDelete(false);

            HasMany(x => x.Dependentes)
              .WithMany()
              .Map(x =>
              {
                  x.ToTable("Sau_DeclaracaoDeSaude_Dependente");
                  x.MapLeftKey("DeclaracaoDeSaudeId");
                  x.MapRightKey("DependenteId");
              });

            HasMany(x => x.ItensDeDeclaracaoDeSaude)
              .WithMany()
              .Map(x =>
              {
                  x.ToTable("Sau_DeclaracaoDeSaude_ItemDeDeclaracaoDeSaude");
                  x.MapLeftKey("DeclaracaoDeSaudeId");
                  x.MapRightKey("ItemDeDeclaracaoDeSaudeId");
              });

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
