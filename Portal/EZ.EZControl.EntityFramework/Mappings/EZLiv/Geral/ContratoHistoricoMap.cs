﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ContratoHistoricoMap : EntityTypeConfiguration<ContratoHistorico>
    {
        public ContratoHistoricoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.DataCriacao).IsRequired();
            Property(x => x.DataInicioVigencia).IsRequired();
            Property(x => x.DataFimVigencia).IsRequired();
            Property(x => x.Conteudo).HasColumnType("text");
            HasRequired(x => x.ProdutoDePlanoDeSaude)
                .WithMany()
                .HasForeignKey(x => x.ProdutoDePlanoDeSaudeId)
                .WillCascadeOnDelete(false);
            HasRequired(x => x.Contrato)
                .WithMany()
                .HasForeignKey(x => x.ContratoId)
                .WillCascadeOnDelete(false);
            Property(x => x.DataEvento).IsRequired();
            Property(x => x.UserId).IsRequired();
        }
    }
}
