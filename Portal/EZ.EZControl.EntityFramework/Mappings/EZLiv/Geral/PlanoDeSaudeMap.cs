﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class PlanoDeSaudeMap : EntityTypeConfiguration<PlanoDeSaude>
    {
        public PlanoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.NumeroDeRegistro)
                .IsRequired();

            Property(x => x.Reembolso);

            Property(x => x.DescricaoDoReembolso)
                .IsOptional();

            Property(x => x.Abrangencia)
                .IsRequired();

            Property(x => x.Acomodacao)
                .IsOptional();

            Property(x => x.PlanoDeSaudeAns)
                .IsOptional();

            Property(x => x.SegmentacaoAssistencial)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
