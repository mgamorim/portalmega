﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class IndiceDeReajustePorFaixaEtariaMap : EntityTypeConfiguration<IndiceDeReajustePorFaixaEtaria>
    {
        public IndiceDeReajustePorFaixaEtariaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            HasRequired(x => x.FaixaEtaria)
                .WithMany()
                .HasForeignKey(x => x.FaixaEtariaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Operadora)
                .WithMany()
                .HasForeignKey(x => x.OperadoraId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            Property(x => x.IndiceDeReajuste)
                .IsRequired();
        }
    }
}
