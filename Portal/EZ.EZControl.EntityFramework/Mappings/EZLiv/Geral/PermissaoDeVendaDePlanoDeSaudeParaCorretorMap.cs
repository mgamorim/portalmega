﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretorMap : EntityTypeConfiguration<PermissaoDeVendaDePlanoDeSaudeParaCorretor>
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretorMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired();

            HasRequired(x => x.Corretora)
                .WithMany()
                .HasForeignKey(x => x.CorretoraId)
                .WillCascadeOnDelete(false);

            HasMany(s => s.Corretores)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Corretor");
                    cs.MapLeftKey("PermissaoDeVendaDePlanoDeSaudeParaCorretorId");
                    cs.MapRightKey("CorretorId");
                });

            HasMany(s => s.Produtos)
               .WithMany()
               .Map(cs =>
               {
                   cs.ToTable("Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretor_ProdutoDePlanoDeSaude");
                   cs.MapLeftKey("PermissaoDeVendaDePlanoDeSaudeParaCorretorId");
                   cs.MapRightKey("ProdutoDePlanoDeSaudeId");
               });

            HasRequired(x => x.Empresa)
               .WithMany()
               .HasForeignKey(x => x.EmpresaId)
               .WillCascadeOnDelete(false);
        }
    }
}
