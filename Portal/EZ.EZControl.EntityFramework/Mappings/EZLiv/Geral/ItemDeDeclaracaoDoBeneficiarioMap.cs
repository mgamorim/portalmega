﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ItemDeDeclaracaoDoBeneficiarioMap : EntityTypeConfiguration<ItemDeDeclaracaoDoBeneficiario>
    {
        public ItemDeDeclaracaoDoBeneficiarioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Pergunta)
               .HasMaxLength(ItemDeDeclaracaoDoBeneficiario.PerguntaMaxLength)
               .IsRequired();

            Property(x => x.Ordenacao)
               .IsRequired();

            Property(x => x.Ajuda).IsOptional();
        }
    }
}
