﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ItemDeRedeCredenciadaMap : EntityTypeConfiguration<ItemDeRedeCredenciada>
    {
        public ItemDeRedeCredenciadaMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.IsRedeDiferenciada);

            HasRequired(x => x.RedeCredenciada)
                .WithMany()
                .HasForeignKey(x => x.RedeCredenciadaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.UnidadeDeSaude)
                .WithMany()
                .HasForeignKey(x => x.UnidadeDeSaudeId)
                .WillCascadeOnDelete(false);
        }
    }
}
