﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class FaixaEtariaMap : EntityTypeConfiguration<FaixaEtaria>
    {
        public FaixaEtariaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            Property(x => x.Descricao).IsRequired();

            Property(x => x.IdadeInicial).IsRequired();

            Property(x => x.IdadeFinal).IsOptional();
        }
    }
}
