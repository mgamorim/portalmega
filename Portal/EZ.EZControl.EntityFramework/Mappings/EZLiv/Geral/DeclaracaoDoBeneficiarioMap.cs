﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class DeclaracaoDoBeneficiarioMap : EntityTypeConfiguration<DeclaracaoDoBeneficiario>
    {
        public DeclaracaoDoBeneficiarioMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
               .HasMaxLength(DeclaracaoDoBeneficiario.NomeMaxLength)
               .IsRequired();

            HasMany(x => x.ItensDeDeclaracaoDoBeneficiario)
              .WithMany()
              .Map(x =>
              {
                  x.ToTable("Sau_DeclaracaoDoBeneficiario_ItemDeDeclaracaoDoBeneficiario");
                  x.MapLeftKey("DeclaracaoDoBeneficiarioId");
                  x.MapRightKey("ItemDeDeclaracaoDoBeneficiarioId");
              });

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
