﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraMap : EntityTypeConfiguration<PermissaoDeVendaDePlanoDeSaudeParaCorretora>
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretoraMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired();

            HasRequired(x => x.Administradora)
                .WithMany()
                .HasForeignKey(x => x.AdministradoraId)
                .WillCascadeOnDelete(false);

            HasMany(s => s.Corretoras)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Corretora");
                    cs.MapLeftKey("PermissaoDeVendaDePlanoDeSaudeParaCorretoraId");
                    cs.MapRightKey("CorretoraId");
                });

            HasMany(s => s.Produtos)
               .WithMany()
               .Map(cs =>
               {
                   cs.ToTable("Sau_PermissaoDeVendaDePlanoDeSaudeParaCorretora_ProdutoDePlanoDeSaude");
                   cs.MapLeftKey("PermissaoDeVendaDePlanoDeSaudeParaCorretoraId");
                   cs.MapRightKey("ProdutoDePlanoDeSaudeId");
               });

            HasRequired(x => x.Empresa)
               .WithMany()
               .HasForeignKey(x => x.EmpresaId)
               .WillCascadeOnDelete(false);
        }
    }
}
