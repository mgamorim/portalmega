﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ContratoMap : EntityTypeConfiguration<Contrato>
    {
        public ContratoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.DataCriacao).IsRequired();
            Property(x => x.DataInicioVigencia).IsRequired();
            Property(x => x.DataFimVigencia).IsRequired();
            Property(x => x.Conteudo).HasColumnType("text");
            HasRequired(x => x.ProdutoDePlanoDeSaude)
                .WithMany()
                .HasForeignKey(x => x.ProdutoDePlanoDeSaudeId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
