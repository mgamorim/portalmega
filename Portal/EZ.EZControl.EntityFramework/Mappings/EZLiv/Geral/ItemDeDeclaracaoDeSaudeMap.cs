﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ItemDeDeclaracaoDeSaudeMap : EntityTypeConfiguration<ItemDeDeclaracaoDeSaude>
    {
        public ItemDeDeclaracaoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Pergunta)
               .HasMaxLength(ItemDeDeclaracaoDeSaude.PerguntaMaxLength)
               .IsRequired();

            Property(x => x.Ordenacao)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            Property(x => x.TipoDeItemDeDeclaracao)
                .IsRequired();
        }
    }
}
