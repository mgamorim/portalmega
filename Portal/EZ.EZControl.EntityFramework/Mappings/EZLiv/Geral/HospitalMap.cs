﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class HospitalMap : EntityTypeConfiguration<Hospital>
    {
        public HospitalMap()
        {
            HasMany(s => s.Especialidades)
               .WithMany(c => c.Hospitais)
               .Map(cs =>
               {
                   cs.ToTable("Sau_Hospital_Especialidade");
                   cs.MapLeftKey("HospitalId");
                   cs.MapRightKey("EspecialidadeId");
               });
        }
    }
}
