﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class RespostaDoItemDeDeclaracaoDeSaudeMap : EntityTypeConfiguration<RespostaDoItemDeDeclaracaoDeSaude>
    {
        public RespostaDoItemDeDeclaracaoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Observacao)
               .HasMaxLength(RespostaDoItemDeDeclaracaoDeSaude.ObservacaoMaxLength)
               .IsOptional();

            Property(x => x.Marcada)
               .HasMaxLength(RespostaDoItemDeDeclaracaoDeSaude.MarcadaMaxLength)
               .IsRequired();

            HasRequired(x => x.ItemDeDeclaracaoDeSaude)
               .WithMany()
               .HasForeignKey(x => x.ItemDeDeclaracaoDeSaudeId)
               .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Beneficiario)
               .WithMany()
               .HasForeignKey(x => x.BeneficiarioId)
               .WillCascadeOnDelete(false);

            HasRequired(x => x.PropostaDeContratacao)
              .WithMany()
              .HasForeignKey(x => x.PropostaDeContratacaoId)
              .WillCascadeOnDelete(false);
        }
    }
}
