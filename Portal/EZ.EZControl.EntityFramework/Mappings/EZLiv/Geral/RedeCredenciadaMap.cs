﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class RedeCredenciadaMap : EntityTypeConfiguration<RedeCredenciada>
    {
        public RedeCredenciadaMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.Nome).HasMaxLength(RedeCredenciada.NomeMaxLength).IsRequired();
            Property(x => x.Descricao).HasColumnType("text");
        }
    }
}
