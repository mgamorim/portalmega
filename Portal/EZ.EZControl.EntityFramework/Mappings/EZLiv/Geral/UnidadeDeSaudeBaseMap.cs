﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class UnidadeDeSaudeBaseMap : EntityTypeConfiguration<UnidadeDeSaudeBase>
    {
        public UnidadeDeSaudeBaseMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired();

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Site)
                .HasMaxLength(UnidadeDeSaudeBase.MaxSiteLength);

            Property(x => x.Email)
                .HasMaxLength(UnidadeDeSaudeBase.MaxEmailLength);

            Property(x => x.TipoDeUnidade)
                .IsRequired();

            HasRequired(x => x.PessoaJuridica)
                .WithMany()
                .HasForeignKey(x => x.PessoaJuridicaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Endereco)
                .WithMany()
                .HasForeignKey(x => x.EnderecoId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Telefone1)
                .WithMany()
                .HasForeignKey(x => x.Telefone1Id)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Telefone2)
                .WithMany()
                .HasForeignKey(x => x.Telefone2Id)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);


        }
    }
}
