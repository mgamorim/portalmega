﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ChancelaMap : EntityTypeConfiguration<Chancela>
    {
        public ChancelaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .IsRequired()
                .HasMaxLength(Chancela.NomeMaxLength);

            Property(x => x.TaxaDeAdesao)
                .IsRequired();

            Property(x => x.TaxaMensal)
               .IsRequired();

            Property(x => x.IsActive);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            HasMany(s => s.Profissoes)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_Chancela_Profissao");
                    cs.MapLeftKey("ChancelaId");
                    cs.MapRightKey("ProfissaoId");
                });

            HasRequired(x => x.Associacao)
                .WithMany()
                .HasForeignKey(x => x.AssociacaoId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Administradora)
                .WithMany()
                .HasForeignKey(x => x.AdministradoraId)
                .WillCascadeOnDelete(false);
        }
    }
}
