﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class VigenciaMap : EntityTypeConfiguration<Vigencia>
    {
        public VigenciaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            Property(x => x.DataInicialDaProposta).IsRequired();

            Property(x => x.DataFinalDaProposta).IsRequired();

            Property(x => x.DataDeFechamento).IsRequired();

            Property(x => x.DataDeVigencia).IsRequired();

            Property(x => x.NumeroDeBoletos).IsOptional();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
