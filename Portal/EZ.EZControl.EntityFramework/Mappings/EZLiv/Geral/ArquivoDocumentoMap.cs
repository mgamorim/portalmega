﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ArquivoDocumentoMap : EntityTypeConfiguration<ArquivoDocumento>
    {
        public ArquivoDocumentoMap()
        {
            HasKey(x => x.Id);

            //HasRequired(x => x.Empresa)
            //    .WithMany()
            //    .HasForeignKey(x => x.EmpresaId)
            //    .WillCascadeOnDelete(false);

            Property(x => x.Tipo);
            Property(x => x.EmExigencia);
            Property(x => x.Motivo);
            HasRequired(x => x.PropostaDeContratacao)
                .WithMany(x => x.Documentos)
                .HasForeignKey(x => x.PropostaDeContratacaoId)
                .WillCascadeOnDelete(false);
        }
    }
}
