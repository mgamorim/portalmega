﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class LaboratorioMap : EntityTypeConfiguration<Laboratorio>
    {
        public LaboratorioMap()
        {
            HasMany(s => s.Especialidades)
              .WithMany(c => c.Laboratorios)
              .Map(cs =>
              {
                  cs.ToTable("Sau_Laboratorio_Especialidade");
                  cs.MapLeftKey("LaboratorioId");
                  cs.MapRightKey("EspecialidadeId");
              });
        }
    }
}
