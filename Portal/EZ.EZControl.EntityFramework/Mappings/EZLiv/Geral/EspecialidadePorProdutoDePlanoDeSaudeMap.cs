﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class EspecialidadePorProdutoDePlanoDeSaudeMap : EntityTypeConfiguration<EspecialidadePorProdutoDePlanoDeSaude>
    {
        public EspecialidadePorProdutoDePlanoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            HasRequired(x => x.Especialidade)
                .WithMany()
                .HasForeignKey(x => x.EspecialidadeId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.ProdutoDePlanoDeSaude)
                .WithMany()
                .HasForeignKey(x => x.ProdutoDePlanoDeSaudeId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
              .WithMany()
              .HasForeignKey(x => x.EmpresaId)
              .WillCascadeOnDelete(false);
        }
    }
}
