﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class ProdutoDePlanoDeSaudeMap : EntityTypeConfiguration<ProdutoDePlanoDeSaude>
    {
        public ProdutoDePlanoDeSaudeMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.PlanoDeSaude)
                .WithMany()
                .HasForeignKey(x => x.PlanoDeSaudeId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Administradora)
               .WithMany(x => x.ProdutosDePlanoDeSaude)
               .HasForeignKey(x => x.AdministradoraId)
               .WillCascadeOnDelete(false);

            HasRequired(x => x.Operadora)
               .WithMany(x => x.ProdutosDePlanoDeSaude)
               .HasForeignKey(x => x.OperadoraId)
               .WillCascadeOnDelete(false);

            Property(x => x.CarenciaEspecial);

            Property(x => x.DescricaoDaCarenciaEspecial)
                .IsOptional();

            Property(x => x.Acompanhante);

            Property(x => x.LinkRedeCredenciada)
                .IsOptional();

            Property(x => x.DescricaoDoAcompanhante)
               .IsOptional();

            Property(x => x.CoberturaExtra);

            Property(x => x.DescricaoDaCoberturaExtra)
                .IsOptional();

            HasOptional(x => x.RedeCredenciada)
                .WithMany()
                .HasForeignKey(x => x.RedeCredenciadaId)
                .WillCascadeOnDelete(false);

            HasMany(s => s.Estados)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_ProdutoDePlanoDeSaude_Estado");
                    cs.MapLeftKey("ProdutoDePlanoDeSaudeId");
                    cs.MapRightKey("EstadoId");
                });

            HasMany(s => s.Aditivos)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_ProdutoDePlanoDeSaude_Aditivo");
                    cs.MapLeftKey("ProdutoDePlanoDeSaudeId");
                    cs.MapRightKey("AditivoId");
                });

            HasMany(s => s.Cidades)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_ProdutoDePlanoDeSaude_Cidade");
                    cs.MapLeftKey("ProdutoDePlanoDeSaudeId");
                    cs.MapRightKey("CidadeId");
                });

            HasMany(s => s.Vigencias)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_ProdutoDePlanoDeSaude_Vigencia");
                    cs.MapLeftKey("ProdutoDePlanoDeSaudeId");
                    cs.MapRightKey("VigenciaId");
                });

            Property(x => x.FormaDeContratacao)
                .IsRequired();

            Property(x => x.NumeroDeDiasParaEncerrarAsPropostasRelacionadas)
                .IsOptional();

            HasRequired(x => x.QuestionarioDeDeclaracaoDeSaude)
               .WithMany()
               .HasForeignKey(x => x.QuestionarioDeDeclaracaoDeSaudeId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.DeclaracaoDoBeneficiario)
               .WithMany()
               .HasForeignKey(x => x.DeclaracaoDoBeneficiarioId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.RelatorioFichaDeEntidade)
               .WithMany()
               .HasForeignKey(x => x.RelatorioFichaDeEntidadeId)
               .WillCascadeOnDelete(false);

            HasOptional(x => x.RelatorioProspostaDeContratacao)
               .WithMany()
               .HasForeignKey(x => x.RelatorioProspostaDeContratacaoId)
               .WillCascadeOnDelete(false);

            HasMany(s => s.Chancelas)
                .WithMany()
                .Map(cs =>
                {
                    cs.ToTable("Sau_ProdutoDePlanoDeSaude_Chancela");
                    cs.MapLeftKey("ProdutoDePlanoDeSaudeId");
                    cs.MapRightKey("ChancelaId");
                });

            HasMany(s => s.OpcoesAdicionais);

            Property(x => x.CartaDeOrientacao)
                .IsOptional();
        }
    }
}
