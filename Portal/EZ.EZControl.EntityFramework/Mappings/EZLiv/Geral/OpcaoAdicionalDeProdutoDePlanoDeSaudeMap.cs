﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class OpcaoAdicionalDeProdutoDePlanoDeSaudeMap : EntityTypeConfiguration<OpcaoAdicionalDeProdutoDePlanoDeSaude>
    {
        public OpcaoAdicionalDeProdutoDePlanoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Nome)
                .HasMaxLength(OpcaoAdicionalDeProdutoDePlanoDeSaude.NomeMaxLength)
                .HasColumnType("varchar")
                .IsRequired();

            Property(x => x.Tipo)
                .IsRequired();

            Property(x => x.Descricao)
                .HasMaxLength(OpcaoAdicionalDeProdutoDePlanoDeSaude.DescricaoMaxLength)
                .HasColumnType("varchar");

            Property(x => x.Valor)
                .IsRequired();

            HasRequired(x => x.ProdutoDePlanoDeSaude)
                .WithMany(x => x.OpcoesAdicionais)
                .HasForeignKey(x => x.ProdutoDePlanoDeSaudeId)
                .WillCascadeOnDelete(false);
        }
    }
}
