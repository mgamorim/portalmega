﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Geral
{
    public class QuestionarioDeDeclaracaoDeSaudeMap : EntityTypeConfiguration<QuestionarioDeDeclaracaoDeSaude>
    {
        public QuestionarioDeDeclaracaoDeSaudeMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            Property(x => x.Nome)
                 .IsRequired()
                 .HasMaxLength(QuestionarioDeDeclaracaoDeSaude.NomeMaxLength);

            HasRequired(x => x.Operadora)
               .WithMany()
               .HasForeignKey(x => x.OperadoraId)
               .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.ItensDeDeclaracaoDeSaude)
               .WithMany()
               .Map(x =>
               {
                   x.ToTable("Sau_ItemDeDeclaracaoDeSaude_QuestionarioDeDeclaracaoDeSaude");
                   x.MapLeftKey("QuestionarioDeDeclaracaoDeSaudeId");
                   x.MapRightKey("ItemDeDeclaracaoDeSaudeId");
               });
        }
    }
}
