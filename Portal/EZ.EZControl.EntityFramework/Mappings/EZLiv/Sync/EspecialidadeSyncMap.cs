﻿using EZ.EZControl.Domain.EZLiv.Sync;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.EZLiv.Sync
{
    public class EspecialidadeSyncMap : EntityTypeConfiguration<EspecialidadeSync>
    {
        public EspecialidadeSyncMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Especialidade)
                .HasMaxLength(EspecialidadeSync.EspecialidadeMaxLength);

            Property(x => x.Nome)
               .HasMaxLength(EspecialidadeSync.NomeMaxLength)
               .IsRequired();

            Property(x => x.Logradouro)
               .HasMaxLength(EspecialidadeSync.LogradouroMaxLength);

            Property(x => x.Numero)
                .HasMaxLength(EspecialidadeSync.NumeroMaxLength);

            Property(x => x.Complemento)
                .HasMaxLength(EspecialidadeSync.ComplementoMaxLength);

            Property(x => x.Bairro)
                .HasMaxLength(EspecialidadeSync.BairroMaxLength);

            Property(x => x.Municipio)
                .HasMaxLength(EspecialidadeSync.MunicipioMaxLength);

            Property(x => x.Estado)
                .HasMaxLength(EspecialidadeSync.EstadoMaxLength);

            Property(x => x.NomeEstado)
                .HasMaxLength(EspecialidadeSync.NomeEstadoMaxLength);

            Property(x => x.Telefone1)
                .HasMaxLength(EspecialidadeSync.Telefone1MaxLength);

            Property(x => x.Ramal1)
                .HasMaxLength(EspecialidadeSync.Ramal1MaxLength);

            Property(x => x.Telefone2)
                .HasMaxLength(EspecialidadeSync.Telefone2MaxLength);

            Property(x => x.Ramal2)
                .HasMaxLength(EspecialidadeSync.Ramal2MaxLength);

            Property(x => x.MunicipioPai)
                .HasMaxLength(EspecialidadeSync.MunicipioPaiMaxLength);

            Property(x => x.Email)
                .HasMaxLength(EspecialidadeSync.EmailMaxLength);

            Property(x => x.HomePage)
                .HasMaxLength(EspecialidadeSync.HomePageMaxLength);

            Property(x => x.PlanoDeSaudeAns);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.ProdutosDePlanoDeSaude)
             .WithMany()
             .Map(x =>
             {
                 x.ToTable("Sau_EspecialidadeSync_ProdutoDePlanoDeSaude");
                 x.MapLeftKey("EspecialidadeSyncId");
                 x.MapRightKey("ProdutoDePlanoDeSaudeId");
             });

        }
    }
}
