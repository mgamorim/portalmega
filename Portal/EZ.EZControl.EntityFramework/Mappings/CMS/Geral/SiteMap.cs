﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Security.Cryptography.X509Certificates;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class SiteMap : EntityTypeConfiguration<Site>
    {
        public SiteMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive)
                .IsRequired();
            Property(x => x.Nome)
                .HasMaxLength(Site.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Site_Nome")
                    {
                        IsUnique = true
                    }));
            Property(x => x.Descricao)
                .HasMaxLength(Site.DescricaoMaxLength);
            Property(x => x.MetaKeywords);
            Property(x => x.MetaTags);
            Property(x => x.Owner);

            HasRequired(x => x.TemplateDefault)
                .WithMany()
                .HasForeignKey(x => x.TemplateDefaultId)
                .WillCascadeOnDelete(false);

            Property(x => x.TipoDePublicacao)
                .IsRequired();

            Property(x => x.TipoDeConteudoRSS)
                .IsRequired();

            Property(x => x.LimitePostPorPagina)
                .IsRequired();

            Property(x => x.LimitePostPorRss)
                .IsRequired();

            Property(x => x.EvitarMecanismoDeBusca);

            Property(x => x.PermitirComentarios);

            Property(x => x.PermitirLinks);

            Property(x => x.AutorInformaNomeEmail);

            Property(x => x.NotificarPorEmailNovoComentario);

            Property(x => x.AtivarModeracaoDeComentario);

            Property(x => x.TextoCss)
                .HasMaxLength(Site.TextoCssMaxLength);

            Property(x => x.TextoJavaScript)
                .HasMaxLength(Site.TextoJavaScriptMaxLength);

            Property(x => x.TipoDePaginaParaPaginaDefault)
                .IsRequired();

            Property(x => x.TipoDePaginaParaPostDefault)
                .IsRequired();

            Property(x => x.TipoDePaginaParaCategoriaDefault)
                .IsRequired();
           
            Property(x => x.TipoDePaginaParaPaginaInicialDefault)
                .IsRequired();

            HasMany(x => x.Hosts)
                .WithRequired(x => x.Site)
                .HasForeignKey(x => x.SiteId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}