﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class WidgetHTMLMap : EntityTypeConfiguration<WidgetHTML>
    {
        public WidgetHTMLMap()
        {
            HasKey(x => x.Id);

            Property(x => x.Conteudo);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
