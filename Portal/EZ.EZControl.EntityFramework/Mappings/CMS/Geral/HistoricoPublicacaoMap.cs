﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class HistoricoPublicacaoMap : EntityTypeConfiguration<HistoricoPublicacao>
    {
        public HistoricoPublicacaoMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Publicacao)
                .WithMany(x => x.Historico)
                .HasForeignKey(x => x.PublicacaoId)
                .WillCascadeOnDelete(false);

            Property(x => x.Data);

            Property(x => x.Status);

            Property(x => x.IsActive);
        }
    }
}