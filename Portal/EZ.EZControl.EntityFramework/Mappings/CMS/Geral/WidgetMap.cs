﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class WidgetMap : EntityTypeConfiguration<WidgetBase>
    {
        public WidgetMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Tipo);

            Property(x => x.Titulo)
                .HasMaxLength(WidgetBase.TituloMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Widget_Titulo")
                    {
                        IsUnique = true
                    }));

            Property(x => x.ExibirTitulo);

            Property(x => x.Posicao);

            Property(x => x.Sistema);

            HasRequired(x => x.Site)
                .WithMany()
                .HasForeignKey(x => x.SiteId);

            HasRequired(x => x.Template)
                .WithMany()
                .HasForeignKey(x => x.TemplateId);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}