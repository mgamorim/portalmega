﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class TemplateMap : EntityTypeConfiguration<Template>
    {
        public TemplateMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Template.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Template_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Descricao)
                .HasMaxLength(Template.DescricaoMaxLength);

            Property(x => x.Autor);

            Property(x => x.AutorUrl);

            Property(x => x.Versao)
                .IsRequired();

            Property(x => x.NomeDoArquivo);

            Property(x => x.Posicoes);

            Property(x => x.TiposDeWidgetSuportados);

            Property(x => x.TiposDePaginasSuportadas)
                .IsRequired();

            Property(x => x.TipoDePaginaParaPaginaDefault)
                .IsRequired();

            Property(x => x.TipoDePaginaParaPostDefault)
                .IsRequired();

            Property(x => x.TipoDePaginaParaCategoriaDefault)
                .IsRequired();

            Property(x => x.TipoDePaginaParaPaginaInicialDefault)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
