﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class HostMap : EntityTypeConfiguration<Host>
    {
        public HostMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive)
                .IsRequired();
            Property(x => x.Url)
                .IsRequired()
                .HasMaxLength(Host.UrlMaxLength);
            Property(x => x.IsPrincipal);
            HasRequired(x => x.Site)
                .WithMany()
                .HasForeignKey(x => x.SiteId)
                .WillCascadeOnDelete(false);
            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}