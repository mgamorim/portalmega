﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class CategoriaMap : EntityTypeConfiguration<Categoria>
    {
        public CategoriaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Categoria.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Categoria_Nome")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Descricao)
                .HasMaxLength(Categoria.DescricaoMaxLength);

            Property(x => x.Slug)
                .HasMaxLength(Categoria.SlugMaxLength)
                .IsRequired();

            HasOptional(x => x.CategoriaPai)
                .WithMany(x => x.Categorias)
                .HasForeignKey(x=>x.CategoriaPaiId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Site)
                .WithMany()
                .HasForeignKey(x => x.SiteId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.Posts);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}