﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class PublicacaoMap : EntityTypeConfiguration<PublicacaoBase>
    {
        public PublicacaoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.TipoDePublicacao);

            Property(x => x.Titulo)
                .HasMaxLength(PublicacaoBase.TituloMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Publicacao_Titulo")
                    {
                        IsUnique = true
                    }));

            Property(x => x.Slug)
                .HasMaxLength(PublicacaoBase.SlugMaxLength)
                .IsRequired();

            Property(x => x.Conteudo);

            Property(x => x.IdImagemDestaque);

            HasRequired(x => x.Site)
                .WithMany(x => x.Publicacoes)
                .HasForeignKey(x => x.SiteId);

            HasMany(x => x.Historico);
        }
    }
}