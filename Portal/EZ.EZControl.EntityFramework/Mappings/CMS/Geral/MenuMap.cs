﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class MenuMap : EntityTypeConfiguration<Menu>
    {
        public MenuMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Nome)
                .HasMaxLength(Menu.NomeMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Menu_Nome")
                    {
                        IsUnique = true
                    }));

            HasRequired(x => x.Site)
                .WithMany()
                .HasForeignKey(x => x.SiteId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.ItensDeMenu);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}