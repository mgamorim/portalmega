﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class PaginaMap : EntityTypeConfiguration<Pagina>
    {
        public PaginaMap()
        {
            HasKey(x => x.Id);

            Property(x => x.TipoDePagina)
                .IsOptional();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
