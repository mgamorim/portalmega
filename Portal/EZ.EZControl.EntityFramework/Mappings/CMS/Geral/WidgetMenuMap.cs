﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class WidgetMenuMap : EntityTypeConfiguration<WidgetMenu>
    {
        public WidgetMenuMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Site)
                .WithMany()
                .HasForeignKey(x => x.MenuId);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
