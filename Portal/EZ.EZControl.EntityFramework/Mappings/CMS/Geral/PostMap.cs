﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class PostMap : EntityTypeConfiguration<Post>
    {
        public PostMap()
        {
            HasKey(x => x.Id);

            HasMany(x => x.Categorias)
                .WithMany(x => x.Posts)
                .Map(x =>
                {
                    x.ToTable("Cms_Post_Categoria");
                    x.MapLeftKey("PostId");
                    x.MapRightKey("CategoriaId");
                });

            HasMany(x => x.Tags)
                .WithMany(x => x.Posts)
                .Map(x =>
                {
                    x.ToTable("Cms_Post_Tag");
                    x.MapLeftKey("PostId");
                    x.MapRightKey("TagId");
                });

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
