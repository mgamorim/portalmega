﻿using EZ.EZControl.Domain.CMS.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Security.Cryptography.X509Certificates;

namespace EZ.EZControl.Mappings.Cms.Geral
{
    public class ItemDeMenuMap : EntityTypeConfiguration<ItemDeMenu>
    {
        public ItemDeMenuMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive)
                .IsRequired();

            Property(x => x.Titulo)
                .HasMaxLength(ItemDeMenu.TituloMaxLength)
                .IsRequired()
                .HasColumnAnnotation("Index",
                    new IndexAnnotation(new IndexAttribute("IX_Menu_Titulo")
                    {
                        IsUnique = true
                    }));

            Property(x => x.DescricaoDoTitulo)
                .HasMaxLength(ItemDeMenu.DescricaoDoTituloMaxLength)
                .IsRequired();

            HasRequired(x => x.Menu)
                .WithMany(x => x.ItensDeMenu)
                .HasForeignKey(x => x.MenuId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.ItemMenu)
                .WithMany(x => x.Itens)
                .HasForeignKey(x => x.ItemMenuId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Categoria)
                .WithMany()
                .HasForeignKey(x => x.CategoriaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Pagina)
                .WithMany()
                .HasForeignKey(x => x.PaginaId)
                .WillCascadeOnDelete(false);

            Property(x => x.TipoDeItemDeMenu);

            Property(x => x.Url)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
