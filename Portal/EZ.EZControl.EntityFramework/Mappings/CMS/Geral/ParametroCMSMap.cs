﻿using EZ.EZControl.Domain.CMS.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.CMS.Geral
{
    public class ParametroCMSMap : EntityTypeConfiguration<ParametroCMS>
    {
        public ParametroCMSMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}