﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class EntradaMap : EntityTypeConfiguration<Entrada>
    {
        public EntradaMap()
        {                          
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.DataEntrada);
            Property(x => x.DataPedido)
                .IsOptional();
            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.Fornecedor)
                .WithMany()
                .HasForeignKey(x => x.FornecedorId)
                .WillCascadeOnDelete(false);
            Property(x => x.NotaFiscal)
                .HasMaxLength(Entrada.NotaFiscalMaxLength)
                .IsOptional();
            Property(x => x.ValorFrete)
                .IsOptional();
            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
