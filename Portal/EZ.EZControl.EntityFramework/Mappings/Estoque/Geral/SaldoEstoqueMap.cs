﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class SaldoEstoqueMap : EntityTypeConfiguration<SaldoEstoque>
    {
        public SaldoEstoqueMap()
        {                          
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.OrigemMovSaldo).IsRequired();
            //Property(x => x.TenantId)
            //    .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_saldoestoque_pesquisa", 1)))
            //    .IsRequired();
            Property(x => x.DataAtualizacao)
                //.HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("ix_saldoestoque_pesquisa", 4)))
                .IsRequired();
            HasRequired(x => x.Empresa) 
                .WithMany()
                //.Map(x => x.MapKey("EmpresaId")
                //    .HasColumnAnnotation("EmpresaId", IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute(), new IndexAttribute("ix_saldoestoque_pesquisa", 2) }))
                //)
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
            HasRequired(x => x.Produto)
                .WithMany()
                //.Map(x => x.MapKey("ProdutoId")
                //    //.HasColumnAnnotation("ProdutoId", IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute(), new IndexAttribute("ix_saldoestoque_pesquisa", 3) }))
                //)                               
                .HasForeignKey(x => x.ProdutoId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.LocalArmazenamento)
                .WithMany()
                //.Map(x => x.MapKey("LocalArmazenamentoId")
                //    //.HasColumnAnnotation("LocalArmazenamentoId", IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute(), new IndexAttribute("ix_saldoestoque_pesquisa", 5) }))
                //)
                .HasForeignKey(x => x.LocalArmazenamentoId)
                .WillCascadeOnDelete(false);
            Property(x => x.Quantidade).IsRequired();                
            Property(x => x.Consolidado).IsOptional();
            HasOptional(x => x.Entrada)
                .WithMany()
                .HasForeignKey(x => x.EntradaId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.Saida)
                .WithMany()
                .HasForeignKey(x => x.SaidaId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.Pedido)
                .WithMany()
                .HasForeignKey(x => x.PedidoId)
                .WillCascadeOnDelete(false);
        }
    }
}
