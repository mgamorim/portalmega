﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class ParametroEstoqueMap : EntityTypeConfiguration<ParametroEstoque>
    {
        public ParametroEstoqueMap()
        {
            HasKey(x => x.Id);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}