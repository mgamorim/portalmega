﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class NaturezaMap : EntityTypeConfiguration<Natureza>
    {
        public NaturezaMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.Descricao)
                .HasMaxLength(Natureza.DescricaoMaxLength)
                .IsRequired();
        }
    }
}
