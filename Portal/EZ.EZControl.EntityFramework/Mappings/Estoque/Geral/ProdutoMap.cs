﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class ProdutoMap : EntityTypeConfiguration<Produto>
    {
        public ProdutoMap()
        {
            HasKey(x => x.Id);

            Property(x => x.IsActive);

            Property(x => x.Nome)
                .HasMaxLength(Produto.NomeMaxLength)
                .IsRequired();

            Property(x => x.Descricao)
                .IsRequired();

            Property(x => x.Valor)
                .IsRequired();

            Property(x => x.IsValorAjustavel);

            Property(x => x.TipoDeProduto)
                .IsRequired();

            HasOptional(x => x.UnidadeMedida)
                .WithMany()
                .HasForeignKey(x => x.UnidadeMedidaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Natureza)
                .WithMany()
                .HasForeignKey(x => x.NaturezaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Imagem)
                .WithMany()
                .Map(x => x.MapKey("ImagemId"))
                .WillCascadeOnDelete(false);
        }
    }
}
