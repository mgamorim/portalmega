﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class EntradaItemMap : EntityTypeConfiguration<EntradaItem>
    {
        public EntradaItemMap()
        {      
            HasKey(x => x.Id);
            Property(x => x.IsActive);            
            HasRequired(x => x.Entrada)
                .WithMany(Entrada.EntradaItemExpression)
                .HasForeignKey(x => x.EntradaId)
                .WillCascadeOnDelete(false);
            HasRequired(x => x.Produto)
                .WithMany()
                .HasForeignKey(x => x.ProdutoId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.LocalArmazenamento)
                .WithMany()
                .HasForeignKey(x => x.LocalArmazenamentoId)
                .WillCascadeOnDelete(false);
            Property(x => x.Quantidade);
            Property(x => x.Validade);
            Property(x => x.ValorUnitario);
        }
    }
}
