﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class UnidadeMedidaMap : EntityTypeConfiguration<UnidadeMedida>
    {
        public UnidadeMedidaMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.Descricao)
                .HasMaxLength(UnidadeMedida.DescricaoMaxLength)
                .IsRequired();
            Property(x => x.UnidadePadrao);
        }
    }
}
