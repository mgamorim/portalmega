﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class SaidaItemMap : EntityTypeConfiguration<SaidaItem>
    {
        public SaidaItemMap()
        {      
            HasKey(x => x.Id);
            Property(x => x.IsActive);            
            HasRequired(x => x.Saida)
                .WithMany(Saida.SaidaItemExpression)
                .HasForeignKey(x => x.SaidaId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.LocalArmazenamento)
                .WithMany()                                
                .HasForeignKey(x => x.LocalArmazenamentoId)
                .WillCascadeOnDelete(false);
            HasRequired(x => x.Produto)
                .WithMany()
                .HasForeignKey(x => x.ProdutoId)
                .WillCascadeOnDelete(false);
            Property(x => x.Quantidade);
            Property(x => x.ValorUnitario);                           
        }
    }
}
