﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class LocalArmazenamentoMap : EntityTypeConfiguration<LocalArmazenamento>
    {
        public LocalArmazenamentoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            HasRequired(x => x.Empresa)
                .WithMany()                
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
            Property(x => x.Descricao)
                .HasMaxLength(LocalArmazenamento.DescricaoMaxLength)
                .IsRequired();

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
