﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class SaidaMap : EntityTypeConfiguration<Saida>
    {
        public SaidaMap()
        {                          
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.DataSaida);            
            HasRequired(x => x.Empresa)
                .WithMany()                     
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
            HasOptional(x => x.Pedido)
                .WithMany()                     
                .HasForeignKey(x => x.PedidoId)
                .WillCascadeOnDelete(false);
            Property(x => x.MotivoSaida).IsRequired();
        }
    }
}
