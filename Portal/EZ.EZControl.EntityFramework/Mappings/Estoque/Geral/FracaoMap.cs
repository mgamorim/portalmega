﻿using EZ.EZControl.Domain.Estoque.Geral;
using System.Data.Entity.ModelConfiguration;

namespace EZ.EZControl.Mappings.Estoque.Geral
{
    public class FracaoMap : EntityTypeConfiguration<Fracao>
    {
        public FracaoMap()
        {
            HasKey(x => x.Id);
            Property(x => x.IsActive);
            Property(x => x.Descricao)
                .HasMaxLength(Fracao.DescricaoMaxLength)
                .IsRequired();
            
            HasRequired(x => x.UnidadeMedida)
                .WithMany()
                .HasForeignKey(x => x.UnidadeMedidaId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Empresa)
                .WithMany()
                .HasForeignKey(x => x.EmpresaId)
                .WillCascadeOnDelete(false);
        }
    }
}
