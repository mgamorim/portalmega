namespace EZ.EZControl.Web.Bundling
{
    public static class StylePaths
    {
        public const string JQuery_UI = "~/libs/jquery-ui/jquery-ui.min.css";
        public const string Simple_Line_Icons = "~/libs/simple-line-icons/simple-line-icons.min.css";
        public const string Bootstrap = "~/libs/bootstrap/css/bootstrap.min.css";
        public const string BootstrapRTL = "~/libs/bootstrap/css/bootstrap-rtl.min.css";
        public const string EzLibCssAll = "~/libs/ez-lib/css/all.css";
        public const string SweetAlert = "~/libs/sweetalert/sweet-alert.css";
        public const string Toastr = "~/libs/toastr/toastr.min.css";
        public const string FontAwesome = "~/Content/font-awesome.min.css";
        public const string FamFamFamFlags = "~/Content/flags/famfamfam-flags.css";
        public const string JQuery_Uniform = "~/libs/jquery-uniform/css/uniform.default.css";
        public const string Morris = "~/libs/morris/morris.css";
        public const string JsTree = "~/libs/jstree/themes/default/style.css";
        public const string Angular_Ui_Grid = "~/libs/angular-ui-grid/ui-grid.min.css";//SPA!
        public const string Bootstrap_DateRangePicker = "~/libs/bootstrap-daterangepicker/daterangepicker.css";
        public const string Bootstrap_Select = "~/libs/bootstrap-select/bootstrap-select.min.css";
        public const string Bootstrap_Switch = "~/libs/bootstrap-switch/css/bootstrap-switch.min.css";
        public const string Bootstrap_TimePicker = "~/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css";
        public const string JQuery_jTable_Theme = "~/libs/jquery-jtable/themes/metro/blue/jtable.min.css";
        public const string JQuery_Jcrop = "~/libs/jcrop/css/jquery.Jcrop.min.css";
        public const string Tokenize = "~/libs/tokenize/jquery.tokenize.css";
        public const string Kendo_Common = "~/Content/kendo/2016.3.914/kendo.common.min.css";
        public const string Kendo_Default = "~/Content/kendo/2016.3.914/kendo.default.min.css";
        public const string Kendo_BootStrap = "~/Content/kendo/2016.3.914/kendo.bootstrap.min.css";
        public const string Angular_Material = "~/libs/angular-material/angular-material.css";
        public const string EzGridPlano = "~/libs/ez-grid-plano/main.css";
        public const string Slick = "~/libs/slick/slick.css";
    }
}