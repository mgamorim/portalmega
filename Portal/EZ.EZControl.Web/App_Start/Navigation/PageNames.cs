namespace EZ.EZControl.Web.Navigation
{
    public static class PageNames
    {
        public static class App
        {
            public static class Common
            {
                public const string Administration = "Administration";
                public const string Roles = "Administration.Roles";
                public const string Users = "Administration.Users";
                public const string AuditLogs = "Administration.AuditLogs";
                public const string OrganizationUnits = "Administration.OrganizationUnits";
                public const string Languages = "Administration.Languages";
            }

            public static class Host
            {
                public const string Tenants = "Tenants";
                public const string Editions = "Editions";
                public const string Maintenance = "Administration.Maintenance";
                public const string Settings = "Administration.Settings.Host";
            }

            public static class Tenant
            {
                public const string Dashboard = "Dashboard.Tenant";
                public const string Settings = "Administration.Settings.Tenant";
                public const string ClienteEZ = "Administration.ClienteEZ";
                public const string UsuarioPorPessoa = "Administration.UsuarioPorPessoa";

                public const string AcessoLeitura = "AcessoLeitura.Tenant";

                #region In�cio
                public const string Inicio = "Inicio.Tenant";
                #endregion

                #region M�dulo Global
                public const string Global = "Global.Tenant";
                public const string GlobalConexoesSMTP = "Global.ConexaoSMTP.Tenant";
                public const string GlobalTemplatesEmail = "Global.TemplateEmail.Tenant";
                public const string GlobalDadosEmpresa = "Global.DadosEmpresa.Tenant";
                public const string GlobalArquivos = "Global.Arquivos.Tenant";
                public const string GlobalPessoasFisicas = "Global.PessoaFisica.Tenant";
                public const string GlobalPessoasJuridicas = "Global.PessoaJuridica.Tenant";
                public const string GlobalTipoDeContato = "Global.TipoDeContato.Tenant";
                public const string GlobalContatos = "Global.Contato.Tenant";
                public const string GlobalEnderecos = "Global.Endereco.Tenant";
                public const string GlobalDepartamentos = "Global.Departamentos.Tenant";
                public const string GlobalTipoDeLogradouro = "Global.TipoDeLogradouro.Tenant";
                public const string GlobalItensHierarquia = "Global.ItemHierarquia.Tenant";
                public const string GlobalClientes = "Global.Clientes.Tenant";
                public const string GlobalBancos = "Global.Bancos.Tenant";
                public const string GlobalAgencias = "Global.Agencias.Tenant";
                public const string GlobalCamposPersonalizados = "Global.CamposPersonalizados.Tenant";
                public const string GlobalFornecedores = "Global.Fornecedores.Tenant";
                public const string GlobalRamosDoFornecedor = "Global.RamoDoFornecedor.Tenant";
                public const string GlobalFeriado = "Global.Feriados.Tenant";
                public const string GlobalTipoDeDocumento = "Global.TipoDeDocumento.Tenant";
                public const string GlobalProfissao = "Global.Profissao.Tenant";
                public const string GlobalPais = "Global.Pais.Tenant";
                public const string GlobalEstado = "Global.Estado.Tenant";
                public const string GlobalCidade = "Global.Cidade.Tenant";
                public const string GlobalTratamento = "Global.Tratamento.Tenant";
                public const string GlobalGrupoPessoa = "Global.GrupoPessoa.Tenant";
                public const string GlobalDocumento = "Global.Documento.Tenant";
                public const string GlobalParametros = "Global.Parametros.Tenant";
                public const string GlobalEmpresas = "Global.Empresas.Tenant";
                #endregion

                #region M�dulo CMS
                public const string CMS = "CMS.Tenant";
                public const string CMSParametros = "CMS.Parametros.Tenant";
                public const string CMSSite = "CMS.Site.Tenant";
                public const string CMSCategoria = "CMS.Categoria.Tenant";
                public const string CMSArquivos = "CMS.Arquivos.Tenant";
                public const string CMSPost = "CMS.Post.Tenant";
                public const string CMSPagina = "CMS.Pagina.Tenant";
                public const string CMSTags = "CMS.Tags.Tenant";
                public const string CMSMenu = "CMS.Menu.Tenant";
                public const string CMSItemDeMenu = "CMS.ItemDeMenu.Tenant";
                public const string CMSTemplate = "CMS.Template.Tenant";
                public const string CMSWidget = "CMS.Widget.Tenant";
                #endregion

                #region M�dulo Agenda
                public const string Agenda = "Agenda.Tenant";
                public const string AgendaParametro = "Agenda.Parametro.Tenant";
                public const string AgendaParametrosPadroes = "Agenda.Parametros.Padroes.Tenant";
                public const string AgendaParametrosRegras = "Agenda.Parametros.Regras.Tenant";
                public const string AgendaDisponibilidade = "Agenda.Disponibilidade.Tenant";
                public const string AgendaBloqueio = "Agenda.Bloqueio.Tenant";
                public const string AgendaEvento = "Agenda.Evento.Tenant";
                public const string AgendaTestes = "Agenda.Testes.Tenant";
                #endregion

                #region M�dulo CMSFrontEnd
                public const string CMSFrontEnd = "CMSFrontEnd.Tenant";
                public const string CMSFrontEndMenu = "CMSFrontEnd.Menu.Tenant";
                public const string CMSFrontEndWidget = "CMSFrontEnd.Widget.Tenant";
                #endregion

                #region M�dulo EZMedical
                public const string EZMedical = "EZMedical.Tenant";
                public const string EZMedicalDisponibilidade = "EZMedical.Disponibilidade.Tenant";
                public const string EZMedicalRegra = "EZMedical.Regra.Tenant";
                public const string EZMedicalParametro = "EZMedical.Parametro.Tenant";
                public const string EZMedicalBloqueio = "EZMedical.Bloqueio.Tenant";
                public const string EZMedicalEvento = "EZMedical.Evento.Tenant";
                public const string EZMedicalEspecialidade = "EZMedical.Especialidade.Tenant";
                public const string EZMedicalMedico = "EZMedical.Medico.Tenant";
                #endregion

                #region M�dulo Estoque
                public const string Estoque = "Estoque.Tenant";
                public const string EstoqueProduto = "Estoque.Produto.Tenant";
                public const string EstoqueEntrada = "Estoque.Entrada.Tenant";
                public const string EstoqueSaida = "Estoque.Saida.Tenant";
                public const string EstoqueParametro = "Estoque.Parametro.Tenant";
                public const string EstoqueParametroPadrao = "Estoque.Parametro.Padrao.Tenant";
                public const string EstoqueParametroNatureza = "Estoque.Parametro.Natureza.Tenant";
                public const string EstoqueParametroFracao = "Estoque.Parametro.Fracao.Tenant";
                public const string EstoqueParametroUnidadeMedida = "Estoque.Parametro.UnidadeMedida.Tenant";
                public const string EstoqueParametroLocalArmazenamento = "Estoque.Parametro.LocalArmazenamento.Tenant";
                #endregion

                #region M�dulo Vendas
                public const string Vendas = "Vendas.Tenant";
                public const string VendasPedido = "Vendas.Pedidos.Tenant";
                #endregion

                #region M�dulo Financeiro
                public const string Financeiro = "Financeiro.Tenant";
                public const string FinanceiroDocumento = "Financeiro.Documento.Tenant";
                public const string FinanceiroDocumentoAPagar = "Financeiro.DocumentoAPagar.Tenant";
                public const string FinanceiroDocumentoAReceber = "Financeiro.DocumentoAReceber.Tenant";
                public const string FinanceiroTipoDeDocumento = "Financeiro.TipoDeDocumento.Tenant";
                #endregion

                #region M�dulo Servi�os
                public const string Servicos = "Servicos.Tenant";
                public const string ServicosServico = "Servicos.Servico.Tenant";
                #endregion

                #region M�dulo EZLiv
                public const string EZLiv = "EZLiv.Tenant";

                public const string EZLiv_Contrato = "EZLiv.Tenant.Contrato";
                public const string EZLiv_Contrato_Create = "EZLiv.Tenant.Contrato.Create";
                public const string EZLiv_Contrato_Edit = "EZLiv.Tenant.Contrato.Edit";
                public const string EZLiv_Contrato_Delete = "EZLiv.Tenant.Contrato.Delete";

                public const string EZLiv_PropostaDeContratacao = "EZLiv.Tenant.PropostaDeContratacao";
                public const string EZLiv_PropostaDeContratacao_Create = "EZLiv.Tenant.PropostaDeContratacao.Create";
                public const string EZLiv_PropostaDeContratacao_Edit = "EZLiv.Tenant.PropostaDeContratacao.Edit";
                public const string EZLiv_PropostaDeContratacao_Delete = "EZLiv.Tenant.PropostaDeContratacao.Delete";

                public const string EZLiv_ProdutoDePlanoDeSaude = "EZLiv.Tenant.ProdutoDePlanoDeSaude";
                public const string EZLiv_ProdutoDePlanoDeSaude_Create = "EZLiv.Tenant.ProdutoDePlanoDeSaude.Create";
                public const string EZLiv_ProdutoDePlanoDeSaude_Edit = "EZLiv.Tenant.ProdutoDePlanoDeSaude.Edit";
                public const string EZLiv_ProdutoDePlanoDeSaude_Delete = "EZLiv.Tenant.ProdutoDePlanoDeSaude.Delete";

                public const string EZLiv_Administradora = "EZLiv.Tenant.Administradora";
                public const string EZLiv_Administradora_Create = "EZLiv.Tenant.Administradora.Create";
                public const string EZLiv_Administradora_Edit = "EZLiv.Tenant.Administradora.Edit";
                public const string EZLiv_Administradora_Delete = "EZLiv.Tenant.Administradora.Delete";

                public const string EZLiv_Operadora = "EZLiv.Tenant.Operadora";
                public const string EZLiv_Operadora_Create = "EZLiv.Tenant.Operadora.Create";
                public const string EZLiv_Operadora_Edit = "EZLiv.Tenant.Operadora.Edit";
                public const string EZLiv_Operadora_Delete = "EZLiv.Tenant.Operadora.Delete";

                public const string EZLivParametro = "EZLiv.Parametro.Tenant";

                public const string EZLiv_Corretora = "EZLiv.Tenant.Corretora";
                public const string EZLiv_Corretora_Create = "EZLiv.Tenant.Corretora.Create";
                public const string EZLiv_Corretora_Edit = "EZLiv.Tenant.Corretora.Edit";
                public const string EZLiv_Corretora_Delete = "EZLiv.Tenant.Corretora.Delete";

                public const string EZLiv_Corretor = "EZLiv.Tenant.Corretor";
                public const string EZLiv_Corretor_Create = "EZLiv.Tenant.Corretor.Create";
                public const string EZLiv_Corretor_Edit = "EZLiv.Tenant.Corretor.Edit";
                public const string EZLiv_Corretor_Delete = "EZLiv.Tenant.Corretor.Delete";

                public const string EZLiv_Associacao = "EZLiv.Tenant.Associacao";
                public const string EZLiv_Associacao_Create = "EZLiv.Tenant.Associacao.Create";
                public const string EZLiv_Associacao_Edit = "EZLiv.Tenant.Associacao.Edit";
                public const string EZLiv_Associacao_Delete = "EZLiv.Tenant.Associacao.Delete";

                public const string EZLiv_Relatorio = "EZLiv.Tenant.Relatorio";
                public const string EZLiv_Relatorio_Create = "EZLiv.Tenant.Relatorio.Create";
                public const string EZLiv_Relatorio_Edit = "EZLiv.Tenant.Relatorio.Edit";
                public const string EZLiv_Relatorio_Delete = "EZLiv.Tenant.Relatorio.Delete";

                public const string EZLiv_ClienteEZLiv = "EZLiv.Tenant.ClienteEZLiv";
                public const string EZLiv_ClienteEZLiv_Create = "EZLiv.Tenant.ClienteEZLiv.Create";
                public const string EZLiv_ClienteEZLiv_Edit = "EZLiv.Tenant.ClienteEZLiv.Edit";
                public const string EZLiv_ClienteEZLiv_Delete = "EZLiv.Tenant.ClienteEZLiv.Delete";

                public const string EZLiv_FaixaEtaria = "EZLiv.Tenant.FaixaEtaria";
                public const string EZLiv_FaixaEtaria_Create = "EZLiv.Tenant.FaixaEtaria.Create";
                public const string EZLiv_FaixaEtaria_Edit = "EZLiv.Tenant.FaixaEtaria.Edit";
                public const string EZLiv_FaixaEtaria_Delete = "EZLiv.Tenant.FaixaEtaria.Delete";

                public const string EZLiv_RedeCredenciada = "EZLiv.Tenant.RedeCredenciada";
                public const string EZLiv_RedeCredenciada_Create = "EZLiv.Tenant.RedeCredenciada.Create";
                public const string EZLiv_RedeCredenciada_Edit = "EZLiv.Tenant.RedeCredenciada.Edit";
                public const string EZLiv_RedeCredenciada_Delete = "EZLiv.Tenant.RedeCredenciada.Delete";

                public const string EZLiv_Beneficiario = "EZLiv.Tenant.Beneficiario";
                public const string EZLiv_Beneficiario_Create = "EZLiv.Tenant.Beneficiario.Create";
                public const string EZLiv_Beneficiario_Edit = "EZLiv.Tenant.Beneficiario.Edit";
                public const string EZLiv_Beneficiario_Delete = "EZLiv.Tenant.Beneficiario.Delete";

                public const string EZLiv_Laboratorio = "EZLiv.Tenant.Laboratorio";
                public const string EZLiv_Laboratorio_Create = "EZLiv.Tenant.Laboratorio.Create";
                public const string EZLiv_Laboratorio_Edit = "EZLiv.Tenant.Laboratorio.Edit";
                public const string EZLiv_Laboratorio_Delete = "EZLiv.Tenant.Laboratorio.Delete";

                public const string EZLiv_Hospital = "EZLiv.Tenant.Hospital";
                public const string EZLiv_Hospital_Create = "EZLiv.Tenant.Hospital.Create";
                public const string EZLiv_Hospital_Edit = "EZLiv.Tenant.Hospital.Edit";
                public const string EZLiv_Hospital_Delete = "EZLiv.Tenant.Hospital.Delete";

                public const string EZLiv_Chancela = "EZLiv.Tenant.Chancela";
                public const string EZLiv_Chancela_Create = "EZLiv.Tenant.Chancela.Create";
                public const string EZLiv_Chancela_Edit = "EZLiv.Tenant.Chancela.Edit";
                public const string EZLiv_Chancela_Delete = "EZLiv.Tenant.Chancela.Delete";

                public const string EZLiv_QuestionarioDeDeclaracaoDeSaude = "EZLiv.Tenant.QuestionarioDeDeclaracaoDeSaude";
                public const string EZLiv_QuestionarioDeDeclaracaoDeSaude_Create = "EZLiv.Tenant.QuestionarioDeDeclaracaoDeSaude.Create";
                public const string EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit = "EZLiv.Tenant.QuestionarioDeDeclaracaoDeSaude.Edit";
                public const string EZLiv_QuestionarioDeDeclaracaoDeSaude_Delete = "EZLiv.Tenant.QuestionarioDeDeclaracaoDeSaude.Delete";

                public const string EZLiv_IndiceDeReajustePorFaixaEtaria = "EZLiv.Tenant.IndiceDeReajustePorFaixaEtaria";
                public const string EZLiv_IndiceDeReajustePorFaixaEtaria_Create = "EZLiv.Tenant.IndiceDeReajustePorFaixaEtaria.Create";
                public const string EZLiv_IndiceDeReajustePorFaixaEtaria_Edit = "EZLiv.Tenant.IndiceDeReajustePorFaixaEtaria.Edit";
                public const string EZLiv_IndiceDeReajustePorFaixaEtaria_Delete = "EZLiv.Tenant.IndiceDeReajustePorFaixaEtaria.Delete";

                public const string EZLiv_MeusProdutos = "EZLiv.Tenant.MeusProdutos";

                public const string EZLiv_MeusProcessos = "EZLiv.Tenant.MeusProcessos";

                public const string EZLiv_Carencia = "EZLiv.Tenant.Carencia";
                public const string EZLiv_Carencia_Create = "EZLiv.Tenant.Carencia.Create";
                public const string EZLiv_Carencia_Edit = "EZLiv.Tenant.Carencia.Edit";
                public const string EZLiv_Carencia_Delete = "EZLiv.Tenant.Carencia.Delete";

                public const string EZLiv_GerenteCorretora = "EZLiv.Tenant.GerenteCorretora";
                public const string EZLiv_GerenteCorretora_Create = "EZLiv.Tenant.GerenteCorretora.Create";
                public const string EZLiv_GerenteCorretora_Edit = "EZLiv.Tenant.GerenteCorretora.Edit";
                public const string EZLiv_GerenteCorretora_Delete = "EZLiv.Tenant.GerenteCorretora.Delete";

                public const string EZLiv_SupervisorCorretora = "EZLiv.Tenant.SupervisorCorretora";
                public const string EZLiv_SupervisorCorretora_Create = "EZLiv.Tenant.SupervisorCorretora.Create";
                public const string EZLiv_SupervisorCorretora_Edit = "EZLiv.Tenant.SupervisorCorretora.Edit";
                public const string EZLiv_SupervisorCorretora_Delete = "EZLiv.Tenant.SupervisorCorretora.Delete";

                public const string EZLiv_GerenteAdministradora = "EZLiv.Tenant.GerenteAdministradora";
                public const string EZLiv_GerenteAdministradora_Create = "EZLiv.Tenant.GerenteAdministradora.Create";
                public const string EZLiv_GerenteAdministradora_Edit = "EZLiv.Tenant.GerenteAdministradora.Edit";
                public const string EZLiv_GerenteAdministradora_Delete = "EZLiv.Tenant.GerenteAdministradora.Delete";

                public const string EZLiv_SupervisorAdministradora = "EZLiv.Tenant.SupervisorAdministradora";
                public const string EZLiv_SupervisorAdministradora_Create = "EZLiv.Tenant.SupervisorAdministradora.Create";
                public const string EZLiv_SupervisorAdministradora_Edit = "EZLiv.Tenant.SupervisorAdministradora.Edit";
                public const string EZLiv_SupervisorAdministradora_Delete = "EZLiv.Tenant.SupervisorAdministradora.Delete";

                public const string EZLiv_HomologadorAdministradora = "EZLiv.Tenant.HomologadorAdministradora";
                public const string EZLiv_HomologadorAdministradora_Create = "EZLiv.Tenant.HomologadorAdministradora.Create";
                public const string EZLiv_HomologadorAdministradora_Edit = "EZLiv.Tenant.HomologadorAdministradora.Edit";
                public const string EZLiv_HomologadorAdministradora_Delete = "EZLiv.Tenant.HomologadorAdministradora.Delete";

                public const string EZLiv_DeclaracaoDoBeneficiario = "EZLiv.Tenant.DeclaracaoDoBeneficiario";
                public const string EZLiv_DeclaracaoDoBeneficiario_Create = "EZLiv.Tenant.DeclaracaoDoBeneficiario.Create";
                public const string EZLiv_DeclaracaoDoBeneficiario_Edit = "EZLiv.Tenant.DeclaracaoDoBeneficiario.Edit";
                public const string EZLiv_DeclaracaoDoBeneficiario_Delete = "EZLiv.Tenant.DeclaracaoDoBeneficiario.Delete";

                public const string EZLiv_ItemDeDeclaracaoDoBeneficiario = "EZLiv.Tenant.ItemDeDeclaracaoDoBeneficiario";
                public const string EZLiv_ItemDeDeclaracaoDoBeneficiario_Create = "EZLiv.Tenant.ItemDeDeclaracaoDoBeneficiario.Create";
                public const string EZLiv_ItemDeDeclaracaoDoBeneficiario_Edit = "EZLiv.Tenant.ItemDeDeclaracaoDoBeneficiario.Edit";
                public const string EZLiv_ItemDeDeclaracaoDoBeneficiario_Delete = "EZLiv.Tenant.ItemDeDeclaracaoDoBeneficiario.Delete";

                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretora";
                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Create";
                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Edit";
                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Delete = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Delete";

                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretor";
                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Create";
                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Edit";
                public const string EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Delete = "EZLiv.Tenant.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Delete";

                public const string EZLiv_PerfilTitular = "EZLiv.Tenant.PerfilTitular";
                public const string EZLiv_PerfilTitular_Create = "EZLiv.Tenant.PerfilTitular.Create";
                public const string EZLiv_PerfilTitular_Edit = "EZLiv.Tenant.PerfilTitular.Edit";

                public const string EZLiv_PerfilCorretor = "EZLiv.Tenant.PerfilCorretor";
                public const string EZLiv_PerfilCorretor_Create = "EZLiv.Tenant.PerfilCorretor.Create";
                public const string EZLiv_PerfilCorretor_Edit = "EZLiv.Tenant.PerfilCorretor.Edit";

                public const string EZLiv_PerfilCorretora = "EZLiv.Tenant.PerfilCorretora";
                public const string EZLiv_PerfilCorretora_Create = "EZLiv.Tenant.PerfilCorretora.Create";
                public const string EZLiv_PerfilCorretora_Edit = "EZLiv.Tenant.PerfilCorretora.Edit";

                public const string EZLiv_PerfilAdministradora = "EZLiv.Tenant.PerfilAdministradora";
                public const string EZLiv_PerfilAdministradora_Create = "EZLiv.Tenant.PerfilAdministradora.Create";
                public const string EZLiv_PerfilAdministradora_Edit = "EZLiv.Tenant.PerfilAdministradora.Edit";
                #endregion

                #region M�dulo EZPag
                public const string EZPag = "EZPag.Tenant";
                public const string EZPagTestes = "EZPag.Testes.Tenant";

                public const string EZPag_Transacao = "EZPag.Transacao.Tenant";


                public const string EZPagParametro = "EZPag.Parametro.Tenant";

                public const string EZPag_FormaDePagamento = "EZPag.Tenant.FormaDePagamento";
                public const string EZPag_FormaDePagamento_Create = "EZPag.Tenant.FormaDePagamento.Create";
                public const string EZPag_FormaDePagamento_Edit = "EZPag.Tenant.FormaDePagamento.Edit";
                public const string EZPag_FormaDePagamento_Delete = "EZPag.Tenant.FormaDePagamento.Delete";
                #endregion
            }
        }

        public static class Frontend
        {
            public const string Home = "Frontend.Home";
            public const string About = "Frontend.About";
        }
    }
}