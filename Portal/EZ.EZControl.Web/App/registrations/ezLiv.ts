﻿//propostaDeContratacao
import * as propostaDeContratacaoService from "services/domain/ezliv/propostaDeContratacao/propostaDeContratacaoService";
import * as propostaDeContratacaoIndex from "tenant/views/ezliv/propostaDeContratacao/propostaDeContratacaoIndex";
import * as propostaDeContratacaoForm from "tenant/views/ezliv/propostaDeContratacao/propostaDeContratacaoForm";

//Produto de Plano de Saúde
import * as produtoDePlanoDeSaudeService from "services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as produtoDePlanoDeSaudeIndex from "tenant/views/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeIndex";
import * as produtoDePlanoDeSaudeForm from "tenant/views/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeForm";

// Plano de Saúde
import * as planoDeSaudeService from "services/domain/ezliv/planoDeSaude/planoDeSaudeService";

// Operadora
import * as operadoraService from "services/domain/ezliv/operadora/operadoraService";
import * as operadoraIndex from "tenant/views/ezliv/operadora/operadoraIndex";
import * as operadoraForm from "tenant/views/ezliv/operadora/operadoraForm";

// Parâmetro Padrão
import * as parametroEZLivForm from "tenant/views/ezliv/parametro/parametroForm";
import * as parametroEZLivService from "services/domain/ezliv/parametro/parametroEZLivService";

//Corretora
import * as corretoraService from "services/domain/ezliv/corretora/corretoraService";
import * as corretoraIndex from "tenant/views/ezliv/corretora/corretoraIndex";
import * as corretoraForm from "tenant/views/ezliv/corretora/corretoraForm";

//Corretor
import * as corretorService from "services/domain/ezliv/corretor/corretorService";
import * as corretorIndex from "tenant/views/ezliv/corretor/corretorIndex";
import * as corretorForm from "tenant/views/ezliv/corretor/corretorForm";


//Associação
import * as associacaoService from "services/domain/ezliv/associacao/associacaoService";
import * as associacaoIndex from "tenant/views/ezliv/associacao/associacaoIndex";
import * as associacaoForm from "tenant/views/ezliv/associacao/associacaoForm";

//Item de Declaração de Saúde
import * as itemDeDeclaracaoDeSaudeService from "services/domain/ezliv/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeService";

//administradora
import * as administradoraService from "services/domain/ezliv/administradora/administradoraService";
import * as administradoraIndex from "tenant/views/ezliv/administradora/administradoraIndex";
import * as administradoraForm from "tenant/views/ezliv/administradora/administradoraForm";

//clienteEZLiv
import * as clienteEZLivService from "services/domain/ezliv/clienteEZLiv/clienteEZLivService";
import * as clienteEZLivIndex from "tenant/views/ezliv/clienteEZLiv/clienteEZLivIndex";
import * as clienteEZLivForm from "tenant/views/ezliv/clienteEZLiv/clienteEZLivForm";

//faixaEtaria
import * as faixaEtariaService from "services/domain/ezliv/faixaEtaria/faixaEtariaService";
import * as faixaEtariaIndex from "tenant/views/ezliv/faixaEtaria/faixaEtariaIndex";
import * as faixaEtariaForm from "tenant/views/ezliv/faixaEtaria/faixaEtariaForm";

//contrato
import * as contratoService from "services/domain/ezliv/contrato/contratoService";
import * as contratoIndex from "tenant/views/ezliv/contrato/contratoIndex";
import * as contratoForm from "tenant/views/ezliv/contrato/contratoForm";

//Valor por Faixa Etária
import * as valorPorFaixaEtariaService from "services/domain/ezliv/valorPorFaixaEtaria/valorPorFaixaEtariaService";

//Arquivo Documento
import * as arquivoDocumentoService from "services/domain/ezliv/arquivoDocumento/arquivoDocumentoService";

//Especialidade Sync
import * as especialidadeSyncService from "services/domain/ezliv/especialidadeSync/especialidadeSyncService";

//Rede Credenciada
import * as redeCredenciadaService from "services/domain/ezliv/redeCredenciada/redeCredenciadaService";
import * as redeCredenciadaIndex from "tenant/views/ezliv/redeCredenciada/redeCredenciadaIndex";
import * as redeCredenciadaForm from "tenant/views/ezliv/redeCredenciada/redeCredenciadaForm";

//Item de Rede Credenciada
import * as itemDeRedeCredenciadaService from "services/domain/ezliv/itemDeRedeCredenciada/itemDeRedeCredenciadaService";

//Unidade de Saude
import * as unidadeDeSaudeService from "services/domain/ezliv/unidadeDeSaude/unidadeDeSaudeService";

//Beneficiario
import * as beneficiarioService from "services/domain/ezliv/beneficiario/beneficiarioService";
import * as beneficiarioIndex from "tenant/views/ezliv/beneficiario/beneficiarioIndex";
import * as beneficiarioForm from "tenant/views/ezliv/beneficiario/beneficiarioForm";

// Proponente Titular
import * as proponenteTitularService from "services/domain/ezliv/proponenteTitular/proponenteTitularService";

//Laboratorio
import * as laboratorioService from "services/domain/ezliv/laboratorio/laboratorioService";
import * as laboratorioIndex from "tenant/views/ezliv/laboratorio/laboratorioIndex";
import * as laboratorioForm from "tenant/views/ezliv/laboratorio/laboratorioForm";

//Laboratorio
import * as hospitalService from "services/domain/ezliv/hospital/hospitalService";
import * as hospitalIndex from "tenant/views/ezliv/hospital/hospitalIndex";
import * as hospitalForm from "tenant/views/ezliv/hospital/hospitalForm";

//Valor por Faixa Etária
import * as especialidadePorProdutoDePlanoDeSaudeService from "services/domain/ezliv/especialidadePorProdutoDePlanoDeSaude/especialidadePorProdutoDePlanoDeSaudeService";

//chancela
import * as chancelaService from "services/domain/ezliv/chancela/chancelaService";
import * as chancelaIndex from "tenant/views/ezliv/chancela/chancelaIndex";
import * as chancelaForm from "tenant/views/ezliv/chancela/chancelaForm";

//índice de reajuste
import * as indiceDeReajustePorFaixaEtariaService from "services/domain/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaService";
import * as indiceDeReajustePorFaixaEtariaIndex from "tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaIndex";
import * as indiceDeReajustePorFaixaEtariaForm from "tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaForm";

//questionário
import * as questionarioDeDeclaracaoDeSaudeService from "services/domain/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeService";
import * as questionarioDeDeclaracaoDeSaudeIndex from "tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeIndex";
import * as questionarioDeDeclaracaoDeSaudeForm from "tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeForm";

//declaracaoDoBeneficiario
import * as declaracaoDoBeneficiarioService from "services/domain/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioService";
import * as declaracaoDoBeneficiarioIndex from "tenant/views/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioIndex";
import * as declaracaoDoBeneficiarioForm from "tenant/views/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioForm";

//relatorio
import * as relatorioEzlivService from "services/domain/ezliv/relatorio/relatorioEzlivService";
import * as relatorioEzlivIndex from "tenant/views/ezliv/relatorio/relatorioEzlivIndex";
import * as relatorioEzlivForm from "tenant/views/ezliv/relatorio/relatorioEzlivForm";

//permissaoDeVendaDePlanoDeSaudeParaCorretora
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraService from "services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraService";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraIndex from "tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraIndex";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraForm from "tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraForm";

//permissaoDeVendaDePlanoDeSaudeParaCorretora
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorService from "services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorService";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorIndex from "tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorIndex";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorForm from "tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorForm";

//itemDeDeclaracaoDoBeneficiario
import * as itemDeDeclaracaoDoBeneficiarioService from "services/domain/ezliv/itemDeDeclaracaoDoBeneficiario/itemDeDeclaracaoDoBeneficiarioService";

//vigência
import * as vigenciaService from "services/domain/ezliv/vigencia/vigenciaService";

//meus produtos
import * as meusProdutosIndex from "tenant/views/ezliv/meusProdutos/meusProdutosIndex";
import * as meusProdutosService from "services/domain/ezliv/meusProdutos/meusProdutosService";

//meus processos
import * as meusProcessosIndex from "tenant/views/ezliv/meusProcessos/meusProcessosIndex";
import * as meusProcessosService from "services/domain/ezliv/meusProcessos/meusProcessosService";

//gerenteCorretora
import * as gerenteCorretoraService from "services/domain/ezliv/gerenteCorretora/gerenteCorretoraService";

//supervisorCorretora
import * as supervisorCorretoraService from "services/domain/ezliv/supervisorCorretora/supervisorCorretoraService";

//gerenteAdministradora
import * as gerenteAdministradoraService from "services/domain/ezliv/gerenteAdministradora/gerenteAdministradoraService";

//supervisorAdministradora
import * as supervisorAdministradoraService from "services/domain/ezliv/supervisorAdministradora/supervisorAdministradoraService";

//homologadorAdministradora
import * as homologadorAdministradoraService from "services/domain/ezliv/homologadorAdministradora/homologadorAdministradoraService";

export module Registrations {
    export class EZLivComponents {
        static registerComponents() {
            appModule.service('parametroEZLivService', parametroEZLivService.Services.ParametroEZLivService);
            appModule.component('parametroEZLivForm', new parametroEZLivForm.Forms.ParametroFormComponent());

            // Proposta De Contratação
            appModule.service('propostaDeContratacaoService', propostaDeContratacaoService.Services.PropostaDeContratacaoService);
            appModule.component('propostaDeContratacaoIndex', new propostaDeContratacaoIndex.EZLiv.PropostaDeContratacao.PropostaDeContratacaoIndexComponent());
            appModule.component('propostaDeContratacaoForm', new propostaDeContratacaoForm.Forms.PropostaDeContratacaoFormComponent());

            //Item de Declaração de Saúde
            appModule.service('itemDeDeclaracaoDeSaudeService', itemDeDeclaracaoDeSaudeService.Services.ItemDeDeclaracaoDeSaudeService);

            // Produto de Plano de Saúde
            appModule.service('produtoDePlanoDeSaudeService', produtoDePlanoDeSaudeService.Services.ProdutoDePlanoDeSaudeService);
            appModule.component('produtoDePlanoDeSaudeIndex', new produtoDePlanoDeSaudeIndex.EZLiv.ProdutoDePlanoDeSaude.ProdutoDePlanoDeSaudeIndexComponent());
            appModule.component('produtoDePlanoDeSaudeForm', new produtoDePlanoDeSaudeForm.Forms.ProdutoDePlanoDeSaudeFormComponent());

            // Plano de Saúde
            appModule.service('planoDeSaudeService', planoDeSaudeService.Services.PlanoDeSaudeService);

            // Operadora
            appModule.service('operadoraService', operadoraService.Services.OperadoraService);
            appModule.component('operadoraIndex', new operadoraIndex.EZLiv.Operadora.OperadoraIndexComponent());
            appModule.component('operadoraForm', new operadoraForm.Forms.OperadoraFormComponent());

            // Corretora
            appModule.service('corretoraService', corretoraService.Services.CorretoraService);
            appModule.component('corretoraIndex', new corretoraIndex.Corretora.CorretoraIndexComponent());
            appModule.component('corretoraForm', new corretoraForm.Forms.CorretoraFormComponent());

            // Corretor
            appModule.service('corretorService', corretorService.Services.CorretorService);
            appModule.component('corretorIndex', new corretorIndex.Corretor.CorretorIndexComponent());
            appModule.component('corretorForm', new corretorForm.Forms.CorretorFormComponent());

            // Associação
            appModule.service('associacaoService', associacaoService.Services.AssociacaoService);
            appModule.component('associacaoIndex', new associacaoIndex.Associacao.AssociacaoIndexComponent());
            appModule.component('associacaoForm', new associacaoForm.Forms.AssociacaoFormComponent());

            // Administradora
            appModule.service('administradoraService', administradoraService.Services.AdministradoraService);
            appModule.component('administradoraIndex', new administradoraIndex.EZLiv.Administradora.AdministradoraIndexComponent());
            appModule.component('administradoraForm', new administradoraForm.Forms.AdministradoraFormComponent());
                             
            // Arquivo Documento
            appModule.service('arquivoDocumentoService', arquivoDocumentoService.Services.ArquivoDocumentoService);

            // EspecialidadeSync
            appModule.service('especialidadeSyncService', especialidadeSyncService.Services.EspecialidadeSyncService);

            // ClienteEZLiv
            appModule.service('clienteEZLivService', clienteEZLivService.Services.ClienteEZLivService);
            appModule.component('clienteEZLivIndex', new clienteEZLivIndex.EZLiv.ClienteEZLiv.ClienteEZLivIndexComponent());
            appModule.component('clienteEZLivForm', new clienteEZLivForm.Forms.ClienteEZLivFormComponent());

            // FaixaEtaria
            appModule.service('faixaEtariaService', faixaEtariaService.Services.FaixaEtariaService);
            appModule.component('faixaEtariaIndex', new faixaEtariaIndex.EZLiv.FaixaEtaria.FaixaEtariaIndexComponent());
            appModule.component('faixaEtariaForm', new faixaEtariaForm.Forms.FaixaEtariaFormComponent());

            // Contrato
            appModule.service('contratoService', contratoService.Services.ContratoService);
            appModule.component('contratoIndex', new contratoIndex.EZLiv.Contrato.ContratoIndexComponent());
            appModule.component('contratoForm', new contratoForm.Forms.ContratoFormComponent());

            //ValorPorFaixaEtaria
            appModule.service('valorPorFaixaEtariaService', valorPorFaixaEtariaService.Services.ValorPorFaixaEtariaService);

            // RedeCredenciada
            appModule.service('redeCredenciadaService', redeCredenciadaService.Services.RedeCredenciadaService);
            appModule.component('redeCredenciadaIndex', new redeCredenciadaIndex.EZLiv.RedeCredenciada.RedeCredenciadaIndexComponent());
            appModule.component('redeCredenciadaForm', new redeCredenciadaForm.Forms.RedeCredenciadaFormComponent());

            // ItemDeRedeCredenciada
            appModule.service('itemDeRedeCredenciadaService', itemDeRedeCredenciadaService.Services.ItemDeRedeCredenciadaService);

            // Beneficiario
            appModule.service('beneficiarioService', beneficiarioService.Services.BeneficiarioService);
            appModule.component('beneficiarioIndex', new beneficiarioIndex.Beneficiario.BeneficiarioIndexComponent());
            appModule.component('beneficiarioForm', new beneficiarioForm.Forms.BeneficiarioFormComponent());

            //Proponente Titular
            appModule.service('proponenteTitularService', proponenteTitularService.Services.ProponenteTitularService);

            // Unidade de Saude
            appModule.service('unidadeDeSaudeService', unidadeDeSaudeService.Services.UnidadeDeSaudeService);

            // Laboratorio
            appModule.service('laboratorioService', laboratorioService.Services.LaboratorioService);
            appModule.component('laboratorioIndex', new laboratorioIndex.Laboratorio.LaboratorioIndexComponent());
            appModule.component('laboratorioForm', new laboratorioForm.Forms.LaboratorioFormComponent());

            // Hospital
            appModule.service('hospitalService', hospitalService.Services.HospitalService);
            appModule.component('hospitalIndex', new hospitalIndex.Hospital.HospitalIndexComponent());
            appModule.component('hospitalForm', new hospitalForm.Forms.HospitalFormComponent());

            //EspecialidadePorProdutoDePlanoDeSaude
            appModule.service('especialidadePorProdutoDePlanoDeSaudeService', especialidadePorProdutoDePlanoDeSaudeService.Services.EspecialidadePorProdutoDePlanoDeSaudeService);

            // Chancela
            appModule.service('chancelaService', chancelaService.Services.ChancelaService);
            appModule.component('chancelaIndex', new chancelaIndex.EZLiv.Chancela.ChancelaIndexComponent());
            appModule.component('chancelaForm', new chancelaForm.Forms.ChancelaFormComponent());

            // Vigência
            appModule.service('vigenciaService', vigenciaService.Services.VigenciaService);

            // Questionário
            appModule.service('questionarioDeDeclaracaoDeSaudeService', questionarioDeDeclaracaoDeSaudeService.Services.QuestionarioDeDeclaracaoDeSaudeService);
            appModule.component('questionarioDeDeclaracaoDeSaudeIndex', new questionarioDeDeclaracaoDeSaudeIndex.EZLiv.QuestionarioDeDeclaracaoDeSaude.QuestionarioDeDeclaracaoDeSaudeIndexComponent());
            appModule.component('questionarioDeDeclaracaoDeSaudeForm', new questionarioDeDeclaracaoDeSaudeForm.Forms.QuestionarioDeDeclaracaoDeSaudeFormComponent());

            // Declaração do Beneficiário
            appModule.service('declaracaoDoBeneficiarioService', declaracaoDoBeneficiarioService.Services.DeclaracaoDoBeneficiarioService);
            appModule.component('declaracaoDoBeneficiarioIndex', new declaracaoDoBeneficiarioIndex.EZLiv.DeclaracaoDoBeneficiario.DeclaracaoDoBeneficiarioIndexComponent());
            appModule.component('declaracaoDoBeneficiarioForm', new declaracaoDoBeneficiarioForm.Forms.DeclaracaoDoBeneficiarioFormComponent());

            // Relatório
            appModule.service('relatorioEzlivService', relatorioEzlivService.Services.RelatorioEzlivService);
            appModule.component('relatorioEzlivIndex', new relatorioEzlivIndex.Relatorio.RelatorioEzlivIndexComponent());
            appModule.component('relatorioEzlivForm', new relatorioEzlivForm.Forms.RelatorioEzlivFormComponent());

            // Item de Declaração do Beneficiário
            appModule.service('itemDeDeclaracaoDoBeneficiarioService', itemDeDeclaracaoDoBeneficiarioService.Services.ItemDeDeclaracaoDoBeneficiarioService);

            // Índice de Reajuste
            appModule.service('indiceDeReajustePorFaixaEtariaService', indiceDeReajustePorFaixaEtariaService.Services.IndiceDeReajustePorFaixaEtariaService);
            appModule.component('indiceDeReajustePorFaixaEtariaIndex', new indiceDeReajustePorFaixaEtariaIndex.EZLiv.IndiceDeReajustePorFaixaEtaria.IndiceDeReajustePorFaixaEtariaIndexComponent());
            appModule.component('indiceDeReajustePorFaixaEtariaForm', new indiceDeReajustePorFaixaEtariaForm.Forms.IndiceDeReajustePorFaixaEtariaFormComponent());

            // Permissão Corretora
            appModule.service('permissaoDeVendaDePlanoDeSaudeParaCorretoraService', permissaoDeVendaDePlanoDeSaudeParaCorretoraService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretoraService);
            appModule.component('permissaoDeVendaDePlanoDeSaudeParaCorretoraIndex', new permissaoDeVendaDePlanoDeSaudeParaCorretoraIndex.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.PermissaoDeVendaDePlanoDeSaudeParaCorretoraIndexComponent());
            appModule.component('permissaoDeVendaDePlanoDeSaudeParaCorretoraForm', new permissaoDeVendaDePlanoDeSaudeParaCorretoraForm.Forms.PermissaoDeVendaDePlanoDeSaudeParaCorretoraFormComponent());

            // Permissão Corretor
            appModule.service('permissaoDeVendaDePlanoDeSaudeParaCorretorService', permissaoDeVendaDePlanoDeSaudeParaCorretorService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretorService);
            appModule.component('permissaoDeVendaDePlanoDeSaudeParaCorretorIndex', new permissaoDeVendaDePlanoDeSaudeParaCorretorIndex.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.PermissaoDeVendaDePlanoDeSaudeParaCorretorIndexComponent());
            appModule.component('permissaoDeVendaDePlanoDeSaudeParaCorretorForm', new permissaoDeVendaDePlanoDeSaudeParaCorretorForm.Forms.PermissaoDeVendaDePlanoDeSaudeParaCorretorFormComponent());

            //Meus Produtos
            appModule.component('meusProdutosIndex', new meusProdutosIndex.EZLiv.MeusProdutos.MeusProdutosIndexComponent());
            appModule.service('meusProdutosService', meusProdutosService.Services.MeusProdutosService);

            //Meus Processos
            appModule.component('meusProcessosIndex', new meusProcessosIndex.EZLiv.MeusProcessos.MeusProcessosIndexComponent());
            appModule.service('meusProcessosService', meusProcessosService.Services.MeusProcessosService);

            //Gerente Corretora
            appModule.service('gerenteCorretoraService', gerenteCorretoraService.Services.GerenteCorretoraService);

            //Supervisor Corretora
            appModule.service('supervisorCorretoraService', supervisorCorretoraService.Services.SupervisorCorretoraService);

            //Gerente Administradora
            appModule.service('gerenteAdministradoraService', gerenteAdministradoraService.Services.GerenteAdministradoraService);

            //Supervisor Administradora
            appModule.service('supervisorAdministradoraService', supervisorAdministradoraService.Services.SupervisorAdministradoraService);

            //Homologador Administradora
            appModule.service('homologadorAdministradoraService', homologadorAdministradoraService.Services.HomologadorAdministradoraService);
        }
    }
}