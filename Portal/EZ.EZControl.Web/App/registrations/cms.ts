﻿//arquivo cms
import * as arquivoCmsIndex from "tenant/views/cms/arquivo/arquivoIndex";
import * as arquivoCmsForm from "tenant/views/cms/arquivo/arquivoForm";
import * as arquivoCmsService from "services/domain/cms/arquivo/arquivoCmsService";

//categoria cms
import * as categoriaCmsIndex from "tenant/views/cms/categoria/categoriaIndex";
import * as categoriaCmsForm from "tenant/views/cms/categoria/categoriaForm";
import * as categoriaCmsService from "services/domain/cms/categoria/categoriaService";

//site cms
import * as siteIndex from "tenant/views/cms/site/siteIndex";
import * as siteForm from "tenant/views/cms/site/siteForm";
import * as siteService from "services/domain/cms/site/siteService";

//menu cms
import * as menuCmsIndex from "tenant/views/cms/menu/menuIndex";
import * as menuCmsForm from "tenant/views/cms/menu/menuForm";
import * as menuCmsService from "services/domain/cms/menu/menuService";

//itemDeMenu cms
import * as itemDeMenuCmsIndex from "tenant/views/cms/itemDeMenu/itemDeMenuIndex";
import * as itemDeMenuCmsForm from "tenant/views/cms/itemDeMenu/itemDeMenuForm";
import * as itemDeMenuCmsService from "services/domain/cms/itemDeMenu/itemDeMenuService";

//parametro cms
import * as parametroCmsForm from "tenant/views/cms/parametros/parametroCmsForm";
import * as parametroCmsService from "services/domain/cms/parametros/parametroCmsService";

//tag cms
import * as tagCmsIndex from "tenant/views/cms/tag/tagIndex";
import * as tagCmsForm from "tenant/views/cms/tag/tagForm";
import * as tagCmsService from "services/domain/cms/tag/tagService";

//template cms
import * as templateCmsIndex from "tenant/views/cms/template/templateIndex";
import * as templateCmsService from "services/domain/cms/template/templateService";

//widget cms
import * as widgetCmsIndex from "tenant/views/cms/widget/widgetIndex";
import * as widgetCmsForm from "tenant/views/cms/widget/widgetForm";
import * as widgetCmsService from "services/domain/cms/widget/widgetService";

//post cms
import * as postCmsIndex from "tenant/views/cms/post/postIndex";
import * as postCmsForm from "tenant/views/cms/post/postForm";
import * as postCmsService from "services/domain/cms/post/postService";

//pagina cms
import * as paginaCmsIndex from "tenant/views/cms/pagina/paginaIndex";
import * as paginaCmsForm from "tenant/views/cms/pagina/paginaForm";
import * as paginaCmsService from "services/domain/cms/pagina/paginaService";

//link cms
import * as linkCmsService from "services/domain/cms/link/linkService";

//host cms
import * as hostCmsService from "services/domain/cms/host/hostService";

export module Registrations {
    export class CmsComponents {
        static registerComponents() {
            appModule.component('arquivoCmsIndex', new arquivoCmsIndex.Cms.Arquivo.ArquivoIndexComponent());
            appModule.service('arquivoCmsService', arquivoCmsService.Services.ArquivoCmsService);
            appModule.component('arquivoCmsForm', new arquivoCmsForm.Forms.ArquivoFormComponent());

            appModule.component('categoriaIndex', new categoriaCmsIndex.CMS.Categoria.CategoriaIndexComponent());
            appModule.service('categoriaService', categoriaCmsService.Services.CategoriaService);
            appModule.component('categoriaForm', new categoriaCmsForm.Forms.CategoriaFormComponent());

            appModule.component('siteIndex', new siteIndex.CMS.Site.SiteIndexComponent());
            appModule.service('siteService', siteService.Services.SiteService);
            appModule.component('siteForm', new siteForm.Forms.SiteFormComponent());

            appModule.component('menuIndex', new menuCmsIndex.CMS.Menu.MenuIndexComponent());
            appModule.service('menuService', menuCmsService.Services.MenuService);
            appModule.component('menuForm', new menuCmsForm.Forms.MenuFormComponent());

            appModule.component('itemDeMenuIndex', new itemDeMenuCmsIndex.CMS.ItemDeMenu.ItemDeMenuIndexComponent());
            appModule.service('itemDeMenuService', itemDeMenuCmsService.Services.ItemDeMenuService);
            appModule.component('itemDeMenuForm', new itemDeMenuCmsForm.Forms.ItemDeMenuFormComponent());

            appModule.component('tagIndex', new tagCmsIndex.CMS.Tag.TagIndexComponent());
            appModule.service('tagService', tagCmsService.Services.TagService);
            appModule.component('tagForm', new tagCmsForm.Forms.TagFormComponent());

            appModule.component('cmsTemplateIndex', new templateCmsIndex.CMS.Template.CMSTemplateIndexComponent());
            appModule.service('templateService', templateCmsService.Services.TemplateService);

            appModule.service('paginaService', paginaCmsService.Services.PaginaService);

            appModule.service('parametroCmsService', parametroCmsService.Services.ParametroCmsService);
            appModule.component('parametroCmsForm', new parametroCmsForm.Forms.ParametroCMSFormComponent());

            appModule.component('widgetIndex', new widgetCmsIndex.CMS.Widget.WidgetIndexComponent());
            appModule.service('widgetService', widgetCmsService.Services.WidgetService);
            appModule.component('widgetForm', new widgetCmsForm.Forms.WidgetFormComponent());

            appModule.component('postIndex', new postCmsIndex.CMS.Post.PostIndexComponent());
            appModule.service('postService', postCmsService.Services.PostService);
            appModule.component('postForm', new postCmsForm.Forms.PostFormComponent());

            appModule.component('paginaIndex', new paginaCmsIndex.CMS.Pagina.PaginaIndexComponent());
            appModule.service('paginaService', paginaCmsService.Services.PaginaService);
            appModule.component('paginaForm', new paginaCmsForm.Forms.PaginaFormComponent());

            appModule.service('linkService', linkCmsService.Services.LinkService);

            appModule.service('hostService', hostCmsService.Services.HostService);
        }
    }
}
