﻿// agencia
import * as agenciaIndex from "tenant/views/global/agencia/agenciaIndex";
import * as agenciaForm from "tenant/views/global/agencia/agenciaForm";
import * as agenciaService from "services/domain/global/agencia/agenciaService";

// banco
import * as bancoIndex from "tenant/views/global/banco/bancoIndex";
import * as bancoForm from "tenant/views/global/banco/bancoForm";
import * as bancoService from "services/domain/global/banco/bancoService";

// cidade
import * as cidade from "tenant/views/global/cidade/cidadeIndex";
import * as cidadeService from "services/domain/global/cidade/cidadeService";
import * as cidadeForm from "tenant/views/global/cidade/cidadeForm";

// estado
import * as estadoIndex from "tenant/views/global/estado/estadoIndex";
import * as estadoService from "services/domain/global/estado/estadoService";
import * as estadoForm from "tenant/views/global/estado/estadoForm";

// pais
import * as paisIndex from "tenant/views/global/pais/paisIndex";
import * as paisService from "services/domain/global/pais/paisService";
import * as paisForm from "tenant/views/global/pais/paisForm";

// conexaoSmtp
import * as conexaoSmtpIndex from "tenant/views/global/conexaoSmtp/conexaoSmtpIndex";
import * as conexaoSmtpForm from "tenant/views/global/conexaoSmtp/conexaoSmtpForm";
import * as conexaoSmtpService from "services/domain/global/conexaoSmtp/conexaoSmtpService";

//departamento
import * as departamentoIndex from "tenant/views/global/departamento/departamentoIndex";
import * as departamentoForm from "tenant/views/global/departamento/departamentoForm";
import * as departamentoService from "services/domain/global/departamento/departamentoService";

//feriado
import * as feriadoIndex from "tenant/views/global/feriado/feriadoIndex";
import * as feriadoForm from "tenant/views/global/feriado/feriadoForm";
import * as feriadoService from "services/domain/global/feriado/feriadoService";

//grupoPessoa
import * as grupoPessoaIndex from "tenant/views/global/grupoPessoa/grupoPessoaIndex";
import * as grupoPessoaForm from "tenant/views/global/grupoPessoa/grupoPessoaForm";
import * as grupoPessoaService from "services/domain/global/grupoPessoa/grupoPessoaService";

//parametro global
import * as parametroGlobalForm from "tenant/views/global/parametros/parametroGlobalForm";
import * as parametroGlobalService from "services/domain/global/parametro/parametroGlobalService";

//profissao
import * as profissaoIndex from "tenant/views/global/profissao/profissaoIndex";
import * as profissaoForm from "tenant/views/global/profissao/profissaoForm";
import * as profissaoService from "services/domain/global/profissao/profissaoService";

//perfil titular
import * as perfilService from "services/domain/global/perfil/perfilService";
import * as perfilForm from "tenant/views/global/perfil/perfilForm";

//ramoDoFornecedor
import * as ramoDoFornecedorIndex from "tenant/views/global/ramoDoFornecedor/ramoDoFornecedorIndex";
import * as ramoDoFornecedorForm from "tenant/views/global/ramoDoFornecedor/ramoDoFornecedorForm";
import * as ramoDoFornecedorService from "services/domain/global/ramoDoFornecedor/ramoDoFornecedorService";

//templateEmail
import * as templateEmailIndex from "tenant/views/global/templateEmail/templateEmailIndex";
import * as templateEmailForm from "tenant/views/global/templateEmail/templateEmailForm";
import * as templateEmailService from "services/domain/global/templateEmail/templateEmailService";

//tipoDeContato
import * as tipoDeContatoIndex from "tenant/views/global/tipoDeContato/tipoDeContatoIndex";
import * as tipoDeContatoForm from "tenant/views/global/tipoDeContato/tipoDeContatoForm";
import * as tipoDeContatoService from "services/domain/global/tipoDeContato/tipoDeContatoService";

//tipoDeDocumento
import * as tipoDeDocumentoIndex from "tenant/views/global/tipoDeDocumento/tipoDeDocumentoIndex";
import * as tipoDeDocumentoForm from "tenant/views/global/tipoDeDocumento/tipoDeDocumentoForm";
import * as tipoDeDocumentoService from "services/domain/global/tipoDeDocumento/tipoDeDocumentoService";

//tipoDeLogradouro
import * as tipoDeLogradouroIndex from "tenant/views/global/tipoDeLogradouro/tipoDeLogradouroIndex";
import * as tipoDeLogradouroForm from "tenant/views/global/tipoDeLogradouro/tipoDeLogradouroForm";
import * as tipoDeLogradouroService from "services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";

//tratamento
import * as tratamentoIndex from "tenant/views/global/tratamento/tratamentoIndex";
import * as tratamentoForm from "tenant/views/global/tratamento/tratamentoForm";
import * as tratamentoService from "services/domain/global/tratamento/tratamentoService";

//documento
import * as documentoIndex from "tenant/views/global/documento/documentoIndex";
import * as documentoForm from "tenant/views/global/documento/documentoForm";
import * as documentoService from "services/domain/global/documento/documentoService";

//contato
import * as contatoIndex from "tenant/views/global/contato/contatoIndex";
import * as contatoForm from "tenant/views/global/contato/contatoForm";
import * as contatoService from "services/domain/global/contato/contatoService";

//endereco
import * as enderecoIndex from "tenant/views/global/endereco/enderecoIndex";
import * as enderecoForm from "tenant/views/global/endereco/enderecoForm";
import * as enderecoService from "services/domain/global/endereco/enderecoService";

//enderecoEletronico
//import * as enderecoEletronicoIndex from "tenant/views/global/enderecoEletronico/enderecoEletronicoIndex";
import * as enderecoEletronicoForm from "tenant/views/global/enderecoEletronico/enderecoEletronicoForm";
import * as enderecoEletronicoService from "services/domain/global/enderecoEletronico/enderecoEletronicoService";

//itemHierarquia
import * as itemHierarquiaIndex from "tenant/views/global/itemHierarquia/itemHierarquiaIndex";
import * as itemHierarquiaForm from "tenant/views/global/itemHierarquia/itemHierarquiaForm";
import * as itemHierarquiaService from "services/domain/global/itemHierarquia/itemHierarquiaService";

//pessoa
import * as pessoaService from "services/domain/global/pessoa/pessoaService";

//pessoa fisica
import * as pessoaFisicaIndex from "tenant/views/global/pessoaFisica/pessoaFisicaIndex";
import * as pessoaFisicaForm from "tenant/views/global/pessoaFisica/pessoaFisicaForm";
import * as pessoaFisicaService from "services/domain/global/pessoaFisica/pessoaFisicaService";

//pessoa fisica
import * as pessoaJuridicaIndex from "tenant/views/global/pessoaJuridica/pessoaJuridicaIndex";
import * as pessoaJuridicaForm from "tenant/views/global/pessoaJuridica/pessoaJuridicaForm";
import * as pessoaJuridicaService from "services/domain/global/pessoaJuridica/pessoaJuridicaService";

//cliente
import * as clienteIndex from "tenant/views/global/clientes/clienteIndex";
import * as clienteForm from "tenant/views/global/clientes/clienteForm";
import * as clienteService from "services/domain/global/cliente/clienteService";

//fornecedor
import * as fornecedorIndex from "tenant/views/global/fornecedores/fornecedorIndex";
import * as fornecedorForm from "tenant/views/global/fornecedores/fornecedorForm";
import * as fornecedorService from "services/domain/global/fornecedor/fornecedorService";

//empresa
import * as empresaIndex from "tenant/views/global/empresa/empresaIndex";
import * as empresaForm from "tenant/views/global/empresa/empresaForm";
import * as empresaService from "services/domain/global/empresa/empresaService";

//arquivo global
import * as arquivoGlobalIndex from "tenant/views/global/arquivo/arquivoIndex";
import * as arquivoGlobalForm from "tenant/views/global/arquivo/arquivoForm";
import * as arquivoGlobalService from "services/domain/global/arquivo/arquivoGlobalService";

//telefone
//import * as telefoneIndex from "tenant/views/global/telefone/telefoneIndex";
import * as telefoneForm from "tenant/views/global/telefone/telefoneForm";
import * as telefoneService from "services/domain/global/telefone/telefoneService";

export module Registrations {
    export class GlobalComponents {
        static registerComponents() {
            appModule.component('agenciaIndex', new agenciaIndex.Global.Agencia.AgenciaIndexComponent());
            appModule.service('agenciaService', agenciaService.Services.AgenciaService);
            appModule.component('agenciaForm', new agenciaForm.Forms.AgenciaFormComponent());

            appModule.component('bancoIndex', new bancoIndex.Global.Banco.BancoIndexComponent());
            appModule.service('bancoService', bancoService.Services.BancoService);
            appModule.component('bancoForm', new bancoForm.Forms.BancoFormComponent());

            appModule.component('cidadeIndex', new cidade.Global.Cidade.CidadeIndexComponent());
            appModule.component('cidadeForm', new cidadeForm.Forms.CidadeFormComponent());
            appModule.service('cidadeService', cidadeService.Services.CidadeService);

            appModule.component('estadoIndex', new estadoIndex.Global.Estado.EstadoIndexComponent());
            appModule.service('estadoService', estadoService.Services.EstadoService);
            appModule.component('estadoForm', new estadoForm.Forms.EstadoFormComponent());

            appModule.component('paisIndex', new paisIndex.Global.Pais.PaisIndexComponent());
            appModule.service('paisService', paisService.Services.PaisService);
            appModule.component('paisForm', new paisForm.Forms.PaisFormComponent());

            appModule.component('conexaoSmtpIndex', new conexaoSmtpIndex.Global.ConexaoSmtp.ConexaoSmtpIndexComponent());
            appModule.service('conexaoSMTPService', conexaoSmtpService.Services.ConexaoSmtpService);
            appModule.component('conexaoSmtpForm', new conexaoSmtpForm.Forms.ConexaoSmtpFormComponent());

            appModule.component('departamentoIndex', new departamentoIndex.Global.Departamento.DepartamentoIndexComponent());
            appModule.service('departamentoService', departamentoService.Services.DepartamentoService);
            appModule.component('departamentoForm', new departamentoForm.Forms.DepartamentoFormComponent());

            appModule.component('feriadoIndex', new feriadoIndex.Global.Feriado.FeriadoIndexComponent());
            appModule.service('feriadoService', feriadoService.Services.FeriadoService);
            appModule.component('feriadoForm', new feriadoForm.Forms.FeriadoFormComponent());

            appModule.component('grupoPessoaIndex', new grupoPessoaIndex.Global.GrupoPessoa.GrupoPessoaIndexComponent());
            appModule.service('grupoPessoaService', grupoPessoaService.Services.GrupoPessoaService);
            appModule.component('grupoPessoaForm', new grupoPessoaForm.Forms.GrupoPessoaFormComponent());

            appModule.component('profissaoIndex', new profissaoIndex.Global.Profissao.ProfissaoIndexComponent());
            appModule.service('profissaoService', profissaoService.Services.ProfissaoService);
            appModule.component('profissaoForm', new profissaoForm.Forms.ProfissaoFormComponent());

            appModule.service('perfilService', perfilService.Services.PerfilService);
            appModule.component('perfilForm', new perfilForm.Forms.PerfilFormComponent());

            appModule.service('parametroGlobalService', parametroGlobalService.Services.ParametroGlobalService);
            appModule.component('parametrosGlobalForm', new parametroGlobalForm.Forms.ParametroGlobalFormComponent());

            appModule.component('ramoDoFornecedorIndex', new ramoDoFornecedorIndex.Global.RamoDoFornecedor.RamoDoFornecedorIndexComponent());
            appModule.service('ramoDoFornecedorService', ramoDoFornecedorService.Services.RamoDoFornecedorService);
            appModule.component('ramoDoFornecedorForm', new ramoDoFornecedorForm.Forms.RamoDoFornecedorFormComponent());

            appModule.component('templateEmailIndex', new templateEmailIndex.Global.TemplateEmail.TemplateEmailIndexComponent());
            appModule.service('templateEmailService', templateEmailService.Services.TemplateEmailService);
            appModule.component('templateEmailForm', new templateEmailForm.Forms.TemplateEmailFormComponent());

            appModule.component('tipoDeContatoIndex', new tipoDeContatoIndex.Global.TipoDeContato.TipoDeContatoIndexComponent());
            appModule.service('tipoDeContatoService', tipoDeContatoService.Services.TipoDeContatoService);
            appModule.component('tipoDeContatoForm', new tipoDeContatoForm.Forms.TipoDeContatoFormComponent());

            appModule.component('tipoDeDocumentoIndex', new tipoDeDocumentoIndex.Global.TipoDeDocumento.TipoDeDocumentoIndexComponent());
            appModule.service('tipoDeDocumentoService', tipoDeDocumentoService.Services.TipoDeDocumentoService);
            appModule.component('tipoDeDocumentoForm', new tipoDeDocumentoForm.Forms.TipoDeDocumentoFormComponent());

            appModule.component('tipoDeLogradouroIndex', new tipoDeLogradouroIndex.Global.TipoDeLogradouro.TipoDeLogradouroIndexComponent());
            appModule.service('tipoDeLogradouroService', tipoDeLogradouroService.Services.TipoDeLogradouroService);
            appModule.component('tipoDeLogradouroForm', new tipoDeLogradouroForm.Forms.TipoDeLogradouroFormComponent());

            appModule.component('tratamentoIndex', new tratamentoIndex.Global.Tratamento.TratamentoIndexComponent());
            appModule.service('tratamentoService', tratamentoService.Services.TratamentoService);
            appModule.component('tratamentoForm', new tratamentoForm.Forms.TratamentoFormComponent());

            appModule.component('documentoIndex', new documentoIndex.Global.Documento.DocumentoIndexComponent());
            appModule.service('documentoService', documentoService.Services.DocumentoService);
            appModule.component('documentoForm', new documentoForm.Forms.DocumentoFormComponent());

            appModule.component('contatoIndex', new contatoIndex.Global.Contato.ContatoIndexComponent());
            appModule.service('contatoService', contatoService.Services.ContatoService);
            appModule.component('contatoForm', new contatoForm.Forms.ContatoFormComponent());

            appModule.component('enderecoIndex', new enderecoIndex.Global.Endereco.EnderecoIndexComponent());
            appModule.service('enderecoService', enderecoService.Services.EnderecoService);
            appModule.component('enderecoForm', new enderecoForm.Forms.EnderecoFormComponent());

            //appModule.component('enderecoEletronicoIndex', new enderecoEletronicoIndex.Global.EnderecoEletronico.EnderecoEletronicoIndexComponent());
            appModule.service('enderecoEletronicoService', enderecoEletronicoService.Services.EnderecoEletronicoService);
            appModule.component('enderecoEletronicoForm', new enderecoEletronicoForm.Forms.EnderecoEletronicoFormComponent());

            appModule.component('itemHierarquiaIndex', new itemHierarquiaIndex.Global.ItemHierarquia.ItemHierarquiaIndexComponent());
            appModule.service('itemHierarquiaService', itemHierarquiaService.Services.ItemHierarquiaService);
            appModule.component('itemHierarquiaForm', new itemHierarquiaForm.Forms.ItemHierarquiaFormComponent());

            appModule.service('pessoaService', pessoaService.Services.PessoaService);

            appModule.component('pessoaFisicaIndex', new pessoaFisicaIndex.Global.PessoaFisica.PessoaFisicaIndexComponent());
            appModule.service('pessoaFisicaService', pessoaFisicaService.Services.PessoaFisicaService);
            appModule.component('pessoaFisicaForm', new pessoaFisicaForm.Forms.PessoaFisicaFormComponent());

            appModule.component('pessoaJuridicaIndex', new pessoaJuridicaIndex.Global.PessoaJuridica.PessoaJuridicaIndexComponent());
            appModule.service('pessoaJuridicaService', pessoaJuridicaService.Services.PessoaJuridicaService);
            appModule.component('pessoaJuridicaForm', new pessoaJuridicaForm.Forms.PessoaJuridicaFormComponent());

            appModule.component('clienteIndex', new clienteIndex.Global.Cliente.ClienteIndexComponent());
            appModule.service('clienteService', clienteService.Services.ClienteService);
            appModule.component('clienteForm', new clienteForm.Forms.ClienteFormComponent());

            appModule.component('fornecedorIndex', new fornecedorIndex.Global.Fornecedor.FornecedorIndexComponent());
            appModule.service('fornecedorService', fornecedorService.Services.FornecedorService);
            appModule.component('fornecedorForm', new fornecedorForm.Forms.FornecedorFormComponent());

            appModule.component('empresaIndex', new empresaIndex.Global.Empresa.EmpresaIndexComponent());
            appModule.service('empresaService', empresaService.Services.EmpresaService);
            appModule.component('empresaForm', new empresaForm.Forms.EmpresaFormComponent());

            appModule.component('arquivoGlobalIndex', new arquivoGlobalIndex.Global.Arquivo.ArquivoIndexComponent());
            appModule.service('arquivoGlobalService', arquivoGlobalService.Services.ArquivoGlobalService);
            appModule.component('arquivoGlobalForm', new arquivoGlobalForm.Forms.ArquivoFormComponent());

            appModule.service('telefoneService', telefoneService.Services.TelefoneService);
            appModule.component('telefoneForm', new telefoneForm.Forms.TelefoneFormComponent());

           
        }
    }
}