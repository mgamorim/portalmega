﻿//parametro Agenda
import * as parametrosPadroesAgendaForm from "tenant/views/Agenda/parametros/padroes/parametrosPadroesAgendaForm";
import * as parametrosPadroesAgendaService from "services/domain/Agenda/parametros/parametrosPadroesAgendaService";

//parametro Agenda Regra
import * as parametrosRegrasAgendaIndex from "tenant/views/agenda/parametros/regras/parametrosRegrasAgendaIndex";
import * as parametrosRegrasAgendaForm from "tenant/views/agenda/parametros/regras/parametrosRegrasAgendaForm";
import * as parametrosRegrasAgendaService from "services/domain/agenda/parametros/parametrosRegraBaseAgendaService";

//Disponibilidade Agenda
import * as disponibilidadeAgendaIndex from "tenant/views/agenda/disponibilidade/disponibilidadeIndex";
import * as disponibilidadeAgendaForm from "tenant/views/agenda/disponibilidade/disponibilidadeForm";
import * as disponibilidadeAgendaService from "services/domain/agenda/disponibilidade/disponibilidadeService";

//Testes
import * as testesIndex from "tenant/views/agenda/testes/testesIndex";

//Disponibilidade Configuração de Agenda
import * as configuracaoDeDisponibilidadeAgendaService from "services/domain/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeService";

//Bloqueio Agenda
import * as bloqueioAgendaIndex from "tenant/views/agenda/bloqueio/bloqueioIndex";
import * as bloqueioAgendaForm from "tenant/views/agenda/bloqueio/bloqueioForm";
import * as bloqueioAgendaService from "services/domain/agenda/bloqueio/bloqueioService";

//Evento Agenda
import * as eventoAgendaIndex from "tenant/views/agenda/evento/eventoIndex";
import * as eventoAgendaForm from "tenant/views/agenda/evento/eventoForm";
import * as eventoAgendaService from "services/domain/agenda/evento/eventoService";

//Disponibilidade Configuração de Agenda
import * as configuracaoDeBloqueioAgendaService from "services/domain/agenda/configuracaoDeBloqueio/configuracaoDeBloqueioService";

export module Registrations {
    export class AgendaComponents {
        static registerComponents() {
            appModule.service('parametrosPadroesAgendaService', parametrosPadroesAgendaService.Services.ParametroPadroesAgendaService);
            appModule.component('parametrosPadroesAgendaForm', new parametrosPadroesAgendaForm.Forms.ParametrosPadroesAgendaFormComponent());

            appModule.component('parametrosRegrasAgendaIndex', new parametrosRegrasAgendaIndex.Agenda.Parametros.Regras.ParametrosRegrasAgendaIndexComponent());
            appModule.service('parametrosRegrasAgendaService', parametrosRegrasAgendaService.Services.RegraBaseService);
            appModule.component('parametrosRegrasAgendaForm', new parametrosRegrasAgendaForm.Forms.ParametrosRegrasAgendaFormComponent());

            appModule.component('disponibilidadeIndex', new disponibilidadeAgendaIndex.Agenda.Disponibilidade.DisponibilidadeIndexComponent());
            appModule.service('disponibilidadeService', disponibilidadeAgendaService.Services.DisponibilidadeService);
            appModule.component('disponibilidadeForm', new disponibilidadeAgendaForm.Forms.DisponibilidadeFormComponent());

            appModule.component('testesAgenda', new testesIndex.Agenda.Testes.AgendaTestesIndexComponent());

            appModule.service('configuracaoDeDisponibilidadeService', configuracaoDeDisponibilidadeAgendaService.Services.ConfiguracaoDeDisponibilidadeService);

            appModule.component('bloqueioIndex', new bloqueioAgendaIndex.Agenda.Bloqueio.BloqueioIndexComponent());
            appModule.service('bloqueioService', bloqueioAgendaService.Services.BloqueioService);
            appModule.component('bloqueioForm', new bloqueioAgendaForm.Forms.BloqueioFormComponent());

            appModule.component('eventoIndex', new eventoAgendaIndex.Agenda.Evento.EventoIndexComponent());
            appModule.service('eventoService', eventoAgendaService.Services.EventoService);
            appModule.component('eventoForm', new eventoAgendaForm.Forms.EventoFormComponent());

            appModule.service('configuracaoDeBloqueioService', configuracaoDeBloqueioAgendaService.Services.ConfiguracaoDeBloqueioService);
        }
    }
}