﻿import * as uniform from "directives/bnUniform/bnUniform";
import * as ezGrid from "components/ezGrid/ezGridComponent";
import * as ezSelect from "components/ezSelect/ezSelectComponent";
import * as ezScheduler from "components/ezScheduler/ezSchedulerComponent";
import * as ezFile from "components/ezFile/ezFileComponent"
import * as ezAutoComplete from "components/ezAutoComplete/ezAutoCompleteComponent"

export module Registrations {
    export class AppComponents {
        static registerComponents() {
            appModule.directive('bnUniform', uniform.Directives.BnUniform.instance);            
            appModule.directive('ezValidationLocal', EzValidationLocal.factory());
            appModule.component('ezGridTyped', new ezGrid.Components.EzGrid.EzGridComponent());
            appModule.component('ezSelectTyped', new ezSelect.Components.EzSelect.EzSelectComponent());
            appModule.component('ezScheduler', new ezScheduler.Componentes.EzScheduler.EzSchedulerComponent());
            appModule.component('ezFileTyped', new ezFile.Components.EZFile.EZFileComponent());
            appModule.component('ezAutoComplete', new ezAutoComplete.Components.EZAutoComplete.EZAutoCompleteComponent());
        }
    }
}