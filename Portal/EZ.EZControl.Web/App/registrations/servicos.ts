﻿////parametro Servicos
//import * as parametrosPadroesServicosForm from "tenant/views/Servicos/parametros/padroes/parametrosPadroesServicosForm";
//import * as parametrosPadroesServicosService from "services/domain/Servicos/parametros/parametrosPadroesServicosService";

// SERVICO
import * as servicoIndex from "tenant/views/servicos/servico/servicoIndex";
import * as servicoService from "services/domain/servicos/servico/servicoService";
import * as servicoForm from "tenant/views/servicos/servico/servicoForm";

export module Registrations {
    export class ServicosComponents {
        static registerComponents() {
            //appModule.service('parametrosPadroesServicosService', parametrosPadroesServicosService.Services.ParametroPadroesServicosService);
            //appModule.component('parametrosPadroesServicosForm', new parametrosPadroesServicosForm.Forms.ParametrosPadroesServicosFormComponent());

            appModule.component('servicosServicoIndex', new servicoIndex.Servicos.Servico.ServicoIndexComponent());
            appModule.service('servicosServicoService', servicoService.Services.ServicoService);
            appModule.component('servicosServicoForm', new servicoForm.Forms.ServicoFormComponent());
        }
    }
}