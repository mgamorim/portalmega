﻿////parametro Vendas
//import * as parametrosPadroesVendasForm from "tenant/views/Vendas/parametros/padroes/parametrosPadroesVendasForm";
//import * as parametrosPadroesVendasService from "services/domain/Vendas/parametros/parametrosPadroesVendasService";

import * as pedidoService from "../services/domain/vendas/pedido/pedidoService";
import * as itemDePedidoService from "../services/domain/vendas/itemDePedido/itemDePedidoService";
import * as pedidoIndex from "../tenant/views/vendas/pedido/pedidoIndex";
import * as pedidoForm from "../tenant/views/vendas/pedido/pedidoForm";

//Venda
import * as vendaService from "../services/domain/vendas/venda/vendaService";

export module Registrations {
    export class VendasComponents {
        static registerComponents() {
            appModule.service('pedidoService', pedidoService.Services.PedidoService);
            appModule.service('itemDePedidoService', itemDePedidoService.Services.ItemDePedidoService);
            appModule.component('pedidoIndex', new pedidoIndex.Global.Pedido.PedidoIndexComponent());
            appModule.component('pedidoForm', new pedidoForm.Forms.PedidoFormComponent());
            appModule.service('vendaService', vendaService.Services.VendaService);
        }
    }
}