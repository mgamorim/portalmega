﻿//tipo de documento
import * as tipoDeDocumentoForm from "tenant/views/financeiro/tipoDeDocumento/tipoDeDocumentoForm";
import * as tipoDeDocumentoIndex from "tenant/views/financeiro/tipoDeDocumento/tipoDeDocumentoIndex";
import * as tipoDeDocumentoService from "services/domain/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroService";

//documento a receber
import * as documentoAReceberForm from "tenant/views/financeiro/documentoAReceber/documentoAReceberForm";
import * as documentoAReceberIndex from "tenant/views/financeiro/documentoAReceber/documentoAReceberIndex";
import * as documentoAReceberService from "services/domain/financeiro/documento/documentoAReceberService";

//documento a pagar
import * as documentoAPagarForm from "tenant/views/financeiro/documentoAPagar/documentoAPagarForm";
import * as documentoAPagarIndex from "tenant/views/financeiro/documentoAPagar/documentoAPagarIndex";
import * as documentoAPagarService from "services/domain/financeiro/documento/documentoAPagarService";

export module Registrations {
    export class FinanceiroComponents {
        static registerComponents() {
            appModule.service('tipoDeDocumentoFinanceiroService', tipoDeDocumentoService.Services.TipoDeDocumentoFinanceiroService);
            appModule.component('tipoDeDocumentoFinanceiroForm', new tipoDeDocumentoForm.Forms.TipoDeDocumentoFinanceiroFormComponent());
            appModule.component('tipoDeDocumentoFinanceiroIndex', new tipoDeDocumentoIndex.Financeiro.TipoDeDocumento.TipoDeDocumentoIndexComponent());

            appModule.service('documentoAReceberService', documentoAReceberService.Services.DocumentoAReceberService);
            appModule.component('documentoAReceberForm', new documentoAReceberForm.Forms.DocumentoAReceberFormComponent());
            appModule.component('documentoAReceberIndex', new documentoAReceberIndex.Financeiro.DocumentoAReceber.DocumentoAReceberIndexComponent());

            appModule.service('documentoAPagarService', documentoAPagarService.Services.DocumentoAPagarService);
            appModule.component('documentoAPagarForm', new documentoAPagarForm.Forms.DocumentoAPagarFormComponent());
            appModule.component('documentoAPagarIndex', new documentoAPagarIndex.Financeiro.DocumentoAPagar.DocumentoAPagarIndexComponent());
        }
    }
}