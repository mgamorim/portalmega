﻿//evento
import * as eventoConsultaIndex from "tenant/views/ezmedical/evento/eventoConsultaIndex";
import * as eventoConsultaForm from "tenant/views/ezmedical/evento/eventoConsultaForm";
import * as eventoConsultaService from "services/domain/ezmedical/evento/eventoConsultaService";

//especialidade
import * as especialidadeIndex from "tenant/views/ezmedical/especialidade/especialidadeIndex";
import * as especialidadeForm from "tenant/views/ezmedical/especialidade/especialidadeForm";
import * as especialidadeService from "services/domain/ezmedical/especialidade/especialidadeService";

//médico
import * as medicoIndex from "tenant/views/ezmedical/medico/medicoIndex";
import * as medicoForm from "tenant/views/ezmedical/medico/medicoForm";
import * as medicoService from "services/domain/ezmedical/medico/medicoService";

//regra
import * as regraConsultaService from "services/domain/ezmedical/regraConsulta/regraConsultaService";
import * as regraConsultaIndex from "tenant/views/ezmedical/regraConsulta/regraConsultaIndex";
import * as regraConsultaForm from "tenant/views/ezmedical/regraConsulta/regraConsultaForm";

//configuacaoDeBloqueio
import * as configuracaoDeBloqueioConsultaService from "services/domain/ezmedical/configuracaoDeBloqueioConsulta/configuracaoDeBloqueioConsultaService";
import * as bloqueioConsultaIndex from "tenant/views/ezmedical/bloqueioConsulta/bloqueioConsultaIndex";
import * as bloqueioConsultaForm from "tenant/views/ezmedical/bloqueioConsulta/bloqueioConsultaForm";

//configuracaoDeDisponibilidade
import * as configuracaoDeDisponibilidadeConsultaService from "services/domain/ezmedical/configuracaoDeDisponibilidadeConsulta/configuracaoDeDisponibilidadeConsultaService";
import * as disponibilidadeConsultaIndex from "tenant/views/ezmedical/disponibilidadeConsulta/disponibilidadeConsultaIndex";
import * as disponibilidadeConsultaForm from "tenant/views/ezmedical/disponibilidadeConsulta/disponibilidadeConsultaForm";

export module Registrations {
    export class EZMedicalComponents {
        static registerComponents() {
            appModule.component('eventoConsultaIndex', new eventoConsultaIndex.EZMedical.EventoConsulta.EventoConsultaIndexComponent());
            appModule.service('eventoConsultaService', eventoConsultaService.Services.EventoConsultaService);
            appModule.component('eventoConsultaForm', new eventoConsultaForm.Forms.EventoConsultaFormComponent());

            appModule.component('especialidadeIndex', new especialidadeIndex.EZMedical.Especialidade.EspecialidadeIndexComponent());
            appModule.service('especialidadeService', especialidadeService.Services.EspecialidadeService);
            appModule.component('especialidadeForm', new especialidadeForm.Forms.EspecialidadeFormComponent());

            appModule.component('medicoIndex', new medicoIndex.EZMedical.Medico.MedicoIndexComponent());
            appModule.service('medicoService', medicoService.Services.MedicoService);
            appModule.component('medicoForm', new medicoForm.Forms.MedicoFormComponent());

            appModule.service('regraConsultaService', regraConsultaService.Services.RegraConsultaService);
            appModule.component('regraConsultaIndex', new regraConsultaIndex.EZMedical.RegraConsulta.RegraConsultaIndexComponent());
            appModule.component('regraConsultaForm', new regraConsultaForm.Forms.RegraConsultaFormComponent());

            appModule.service('configuracaoDeBloqueioConsultaService', configuracaoDeBloqueioConsultaService.Services.ConfiguracaoDeBloqueioConsultaService);
            appModule.component('bloqueioConsultaIndex', new bloqueioConsultaIndex.EZMedical.Bloqueio.BloqueioConsultaIndexComponent());
            appModule.component('bloqueioConsultaForm', new bloqueioConsultaForm.Forms.BloqueioConsultaFormComponent());

            appModule.service('configuracaoDeDisponibilidadeConsultaService', configuracaoDeDisponibilidadeConsultaService.Services.ConfiguracaoDeDisponibilidadeConsultaService);
            appModule.component('disponibilidadeConsultaIndex', new disponibilidadeConsultaIndex.EZMedical.Disponibilidade.DisponibilidadeConsultaIndexComponent());
            appModule.component('disponibilidadeConsultaForm', new disponibilidadeConsultaForm.Forms.DisponibilidadeFormComponent());
        }
    }
}