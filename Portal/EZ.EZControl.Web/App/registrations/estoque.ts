﻿// Parâmetro Padrão
import * as parametrosEstoqueForm from "tenant/views/Estoque/parametros/padrao/parametrosEstoqueForm";
import * as parametrosEstoqueService from "services/domain/Estoque/parametros/parametrosEstoqueService";

// Parâmetro - Natureza
import * as estoqueNaturezaIndex from "tenant/views/Estoque/parametros/natureza/naturezaIndex";
import * as estoqueNaturezaService from "services/domain/estoque/parametros/naturezaService";
import * as estoqueNaturezaForm from "tenant/views/Estoque/parametros/natureza/naturezaForm";

// Parâmetro - Unidade de Medida
import * as estoqueUnidadeMedidaIndex from "tenant/views/Estoque/parametros/unidadeMedida/unidadeMedidaIndex";
import * as estoqueUnidadeMedidaService from "services/domain/estoque/parametros/unidadeMedidaService";
import * as estoqueUnidadeMedidaForm from "tenant/views/Estoque/parametros/unidadeMedida/unidadeMedidaForm";

// Parâmetro - Fração
import * as estoqueFracaoIndex from "tenant/views/Estoque/parametros/fracao/fracaoIndex";
import * as estoqueFracaoService from "services/domain/estoque/parametros/fracaoService";
import * as estoqueFracaoForm from "tenant/views/Estoque/parametros/fracao/fracaoForm";

// Parâmetro - Local Armazenamento
import * as estoqueLocalArmazenamentoIndex from "tenant/views/Estoque/parametros/localArmazenamento/localArmazenamentoIndex";
import * as estoqueLocalArmazenamentoService from "services/domain/estoque/parametros/localArmazenamentoService";
import * as estoqueLocalArmazenamentoForm from "tenant/views/Estoque/parametros/localArmazenamento/localArmazenamentoForm";

// Produto
import * as produtoIndex from "tenant/views/estoque/produto/produtoIndex";
import * as produtoService from "services/domain/estoque/produto/produtoService";
import * as produtoForm from "tenant/views/estoque/produto/produtoForm";

// Entrada
import * as estoqueEntradaIndex from "tenant/views/estoque/entrada/entradaIndex";
import * as estoqueEntradaService from "services/domain/estoque/entrada/entradaService";
import * as estoqueEntradaForm from "tenant/views/estoque/entrada/entradaForm";
import * as estoqueEntradaItemService from "services/domain/estoque/entradaItem/entradaItemService";

// Saida
import * as estoqueSaidaIndex from "tenant/views/estoque/saida/saidaIndex";
import * as estoqueSaidaService from "services/domain/estoque/saida/saidaService";
import * as estoqueSaidaForm from "tenant/views/estoque/saida/saidaForm";
import * as estoqueSaidaItemService from "services/domain/estoque/saidaItem/saidaItemService";

export module Registrations {
    export class EstoqueComponents {
        static registerComponents() {
            appModule.service('parametrosEstoqueService', parametrosEstoqueService.Services.ParametrosEstoqueService);
            appModule.component('parametrosEstoqueForm', new parametrosEstoqueForm.Forms.ParametrosEstoqueFormComponent());

            appModule.component('estoqueNaturezaIndex', new estoqueNaturezaIndex.Estoque.Natureza.NaturezaIndexComponent());
            appModule.service('estoqueNaturezaService', estoqueNaturezaService.Services.NaturezaService);
            appModule.component('estoqueNaturezaForm', new estoqueNaturezaForm.Forms.NaturezaFormComponent());

            appModule.component('estoqueUnidadeMedidaIndex', new estoqueUnidadeMedidaIndex.Estoque.UnidadeMedida.UnidadeMedidaIndexComponent());
            appModule.service('estoqueUnidadeMedidaService', estoqueUnidadeMedidaService.Services.UnidadeMedidaService);
            appModule.component('estoqueUnidadeMedidaForm', new estoqueUnidadeMedidaForm.Forms.UnidadeMedidaFormComponent());

            appModule.component('estoqueFracaoIndex', new estoqueFracaoIndex.Estoque.Fracao.FracaoIndexComponent());
            appModule.service('estoqueFracaoService', estoqueFracaoService.Services.FracaoService);
            appModule.component('estoqueFracaoForm', new estoqueFracaoForm.Forms.FracaoFormComponent());

            appModule.component('estoqueLocalArmazenamentoIndex', new estoqueLocalArmazenamentoIndex.Estoque.LocalArmazenamento.LocalArmazenamentoIndexComponent());
            appModule.service('estoqueLocalArmazenamentoService', estoqueLocalArmazenamentoService.Services.LocalArmazenamentoService);
            appModule.component('estoqueLocalArmazenamentoForm', new estoqueLocalArmazenamentoForm.Forms.LocalArmazenamentoFormComponent());

            appModule.component('estoqueProdutoIndex', new produtoIndex.Estoque.Produto.ProdutoIndexComponent());
            appModule.service('estoqueProdutoService', produtoService.Services.ProdutoService);
            appModule.component('estoqueProdutoForm', new produtoForm.Forms.ProdutoFormComponent());

            appModule.component('estoqueEntradaIndex', new estoqueEntradaIndex.Estoque.Entrada.EntradaIndexComponent());
            appModule.service('estoqueEntradaService', estoqueEntradaService.Services.EntradaService);
            appModule.component('estoqueEntradaForm', new estoqueEntradaForm.Forms.EntradaFormComponent());
            appModule.service('estoqueEntradaItemService', estoqueEntradaItemService.Services.EntradaItemService);

            appModule.component('estoqueSaidaIndex', new estoqueSaidaIndex.Estoque.Saida.SaidaIndexComponent());
            appModule.service('estoqueSaidaService', estoqueSaidaService.Services.SaidaService);
            appModule.component('estoqueSaidaForm', new estoqueSaidaForm.Forms.SaidaFormComponent());
            appModule.service('estoqueSaidaItemService', estoqueSaidaItemService.Services.SaidaItemService);
        }
    }
}