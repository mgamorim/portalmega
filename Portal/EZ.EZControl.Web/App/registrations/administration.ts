﻿//cliente ez
import * as clienteEzForm from "tenant/views/global/clienteEz/clienteEzForm";
import * as clienteEzService from "services/domain/global/clienteEz/clienteEzService";

//usuario por pessoa
import * as usuarioPorPessoaIndex from "tenant/views/global/usuarioPorPessoa/usuarioPorPessoaIndex";
import * as usuarioPorPessoaForm from "tenant/views/global/usuarioPorPessoa/usuarioPorPessoaForm";
import * as usuarioPorPessoaService from "services/domain/global/usuarioPorPessoa/usuarioPorPessoaService";

//user
import * as userService from "services/domain/core/user/userService";

export module Registrations {
    export class AdministrationComponents {
        static registerComponents() {
            appModule.service('clienteEzService', clienteEzService.Services.ClienteEzService);
            appModule.component('clienteEzForm', new clienteEzForm.Forms.ClienteEzFormComponent());

            appModule.component('usuarioPorPessoaIndex', new usuarioPorPessoaIndex.Global.UsuarioPorPessoa.UsuarioPorPessoaIndexComponent());
            appModule.service('usuarioPorPessoaService', usuarioPorPessoaService.Services.UsuarioPorPessoaService);
            appModule.component('usuarioPorPessoaForm', new usuarioPorPessoaForm.Forms.UsuarioPorPessoaFormComponent());

            appModule.service('userService', userService.Services.UserService);
        }
    }
}