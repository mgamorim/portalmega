﻿//Transação PagSeguro
import * as transacaoPagSeguroService from "services/domain/ezpag/transacaoPagSeguro/transacaoPagSeguroService";

// Transação 
import * as transacaoIndex from "tenant/views/ezpag/transacao/transacaoIndex";
import * as transacaoService from "services/domain/ezpag/transacao/transacaoService";
// Parâmetro Padrão
import * as parametroEzpagForm from "tenant/views/ezpag/parametro/parametroForm";
import * as parametroEzpagService from "services/domain/ezpag/parametro/parametroEZpagService";
// formaDePagamento
import * as formaDePagamentoIndex from "tenant/views/ezpag/formaDePagamento/formaDePagamentoIndex";
import * as formaDePagamentoService from "services/domain/ezpag/formaDePagamento/formaDePagamentoService";
import * as formaDePagamentoForm from "tenant/views/ezpag/formaDePagamento/formaDePagamentoForm";

// parametroPagSeguro
import * as parametroPagSeguroService from "services/domain/ezpag/parametroPagSeguro/parametroPagSeguroService";

export module Registrations {
    export class EZPagComponents {
        static registerComponents() {
            //Parâmetro
            appModule.service('parametroEzpagService', parametroEzpagService.Services.ParametroEzpagService);
            appModule.component('parametroEzpagForm', new parametroEzpagForm.Forms.ParametroFormComponent());

            appModule.service('transacaoPagSeguroService', transacaoPagSeguroService.Services.TransacaoPagSeguroService);

            appModule.component('formaDePagamentoIndex', new formaDePagamentoIndex.EzPag.FormaDePagamento.FormaDePagamentoIndexComponent());
            appModule.service('formaDePagamentoService', formaDePagamentoService.Services.FormaDePagamentoService);
            appModule.component('formaDePagamentoForm', new formaDePagamentoForm.Forms.FormaDePagamentoFormComponent());

            appModule.component('transacaoIndex', new transacaoIndex.EZPag.Transacao.TransacaoIndexComponent());
            appModule.service('transacaoService', transacaoService.Services.TransacaoService);

            appModule.service('parametroPagSeguroService', parametroPagSeguroService.Services.ParametroPagSeguroService);
        }
    }
}

