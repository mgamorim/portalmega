﻿interface JQueryStatic {
    remap: () => void;
}

interface JQueryStatic {
    tokenAdd: (value: string, text: string) => void;
}

interface JQueryStatic {
    clear: () => void;
}

interface JQuery {
    tokenize: (options?: any) => JQueryStatic;
}