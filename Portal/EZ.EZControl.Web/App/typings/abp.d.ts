﻿interface IAbpLocalization {
    getSource(sourceName: string): string;
    isCurrentCulture(name: string): boolean;
}

interface IAbpNotify {
    success(message: string, title: string): void;
    info(message: string, title: string): void;
    warn(message: string, title: string): void;
    error(message: string, title: string): void;
}

interface IAbpMessage {
    success(message: string, title: string): void;
    info(message: string, title: string): void;
    warn(message: string, title: string): void;
    error(message: string, title: string): void;
    confirm(message: string, title: string, callback: (isConfirmed: boolean) => void): void;
}

interface IAbpAuth {
    hasPermission(permissionName: string): boolean;
}

interface IAbpGridConstants {
    defaultPageSize: number;
    defaultPageSizes: number[];
}

interface IAbpUserManagement {
    defaultAdminUserName: string;
}

interface IAbpContentTypes {
    formUrlencoded: string;
}

interface IAbpConsts {
    grid: IAbpGridConstants;
}

interface ISession {
    empresaId: number;
    empresaNome: string;
    isMultiEmpresa: boolean;
    multiTenancySide: number;
    tenantId: number;
    userId: number;
}

declare module abp {
    var appPath: string;
    var pageLoadTime: Date;
    function toAbsAppPath(path: string);
    var localization: IAbpLocalization;
    var notify: IAbpNotify;
    var message: IAbpMessage;
    var auth: IAbpAuth;
    var services: any;
    function ajax(obj: any);
    var session: ISession;
}

declare module app {
    function localize(word: string): string;
    var consts: IAbpConsts;
    var userManagement: IAbpUserManagement;
    var contentTypes: IAbpContentTypes;
}