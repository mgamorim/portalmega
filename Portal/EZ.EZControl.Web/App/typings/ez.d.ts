﻿interface IEzDomain {
    enum: any;
}

declare module ez {
    var domain: IEzDomain;
}

declare module PagSeguroDirectPayment {
    function createCardToken(input: any): any;
    function getBrand(input: any): any;
    function getInstallments(input: any): any;
    function getPaymentMethods(input: any): any;
    function setSessionId(id: string);
    function getSenderHash(): string;
}