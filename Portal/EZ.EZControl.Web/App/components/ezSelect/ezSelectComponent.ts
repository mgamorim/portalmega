﻿import * as ezSelect from "../../common/ezSelect/ezSelect";

export module Components.EzSelect {

    export class EzSelectController {

        static $inject = ['uiGridConstants', '$uibModal'];

        public ezSelect: ezSelect.EzSelect.EzSelect<any, any>;
        public inputText: string;
        public inputDisabled: boolean;
        public onInitialize: any;
        public name: string;

        constructor(private uiGridConstants: uiGrid.IUiGridConstants, private $uibModal: angular.ui.bootstrap.IModalService) {

        }

        $onInit() {
            this.ezSelect.setEzGridController(this);
            this.ezSelect.name = this.name ? this.name : '';
            if (this.onInitialize) {
                this.onInitialize();
            }
        }

        $onDestroy() {
            this.ezSelect.onComponentDestroy();
        }

        setInputText(value: string) {
            this.inputText = value;
        }

        getInputText(): string {
            return this.inputText;
        }

        clearInputText() {
            this.inputText = '';
        }
    }

    export class EzSelectComponent implements ng.IComponentOptions {
        public controller: any;
        public templateUrl: string;
        public bindings: any;
        public require: any;

        constructor() {
            this.controller = EzSelectController;
            this.templateUrl = '/app/components/ezSelect/ezSelectComponent.html';
            this.bindings = {
                ezSelect: '<',
                inputDisabled: '<',
                isRequired: '<',
                onInitialize: '&',
                name: '@'
            }
        }
    }
}