﻿import * as ezGrid from "../../common/ezGrid/ezGrid";

export module Components.EzGrid {

    export interface IEzGridBindings {
        ezGrid: any;
    }

    export class EzGridController implements IEzGridBindings {
        constructor() {
        }

        public ezGrid: ezGrid.EzGrid.EzGrid<any, any>;
        public name: string;

        $onInit() {
            this.ezGrid.name = this.name ? this.name : '';
        }

        $onDestroy() {
            this.ezGrid.onComponentDestroy();
        }
    }

    export class EzGridComponent implements ng.IComponentOptions {
        public controller: any;
        public templateUrl: string;
        public bindings: any;

        constructor() {
            this.controller = EzGridController;
            this.templateUrl = '/app/components/ezGrid/ezGridComponent.html';
            this.bindings = {
                ezGrid: '<',
                name: '@'
            }
        }
    }
}