﻿import Scheduler = require("../../common/ezScheduler/ezScheduler");
import ParametrosRegraBaseAgendaService = require("../../services/domain/agenda/parametros/parametrosRegraBaseAgendaService");

export module Componentes.EzScheduler {
    class FiltroPorTipoDeEvento {
        tipoDeEvento: any;
        selecionado: boolean;
    }

    export class EzSchedulerController {
        public scheduler: kendo.ui.Scheduler;
        public schedulerOptions: kendo.ui.SchedulerOptions;
        public dataSource: kendo.data.SchedulerDataSource;

        // atributos
        public ezScheduler: Scheduler.EzScheduler.IEzScheduler;
        public views: any;
        public height: number;
        public showWorkHours: boolean;
        public timezone: string;
        public messages: any;
        public filtrosPorTipoDeEvento: Array<FiltroPorTipoDeEvento>;        

        static $inject = ['$scope', 'parametrosRegrasAgendaService'];

        constructor(private $scope: ng.IScope, private regraBaseService: ParametrosRegraBaseAgendaService.Services.RegraBaseService) {
            this.filtrosPorTipoDeEvento = new Array<FiltroPorTipoDeEvento>();
            this.dataSource = new kendo.data.SchedulerDataSource();
            this.initValues();            

            $scope.$on('kendoRendered', () => {
                this.ezScheduler.configureDataSource();
                // É necessário criar uma nova instancia do datasource na classe EzScheduler
                // e depois associa-la ao scheduler
                this.dataSource = this.ezScheduler.configureDataSource();
                this.scheduler.setDataSource(this.dataSource);
                if (this.ezScheduler.onSchedulerRendered) {
                    this.ezScheduler.setScheduler(this.scheduler);
                    this.ezScheduler.onSchedulerRendered(this.scheduler);
                }
                this.scheduler.refresh();
                
            });

            kendo.culture('pt-BR');
        }

        initValues() {
            this.regraBaseService.getTiposDeEventosBySistema(this.ezScheduler.sistema)
              .then((data) => {
                    for (let i in data.tiposDeEventos) {
                      var tipoDeEvento = ez.domain.enum.tipoDeEventoEnum.getItemPorValor(data.tiposDeEventos[i]);
                      var filtroPorTipoDeEvento = new FiltroPorTipoDeEvento();
                      filtroPorTipoDeEvento.tipoDeEvento = tipoDeEvento;
                      filtroPorTipoDeEvento.selecionado = true;
                      this.filtrosPorTipoDeEvento.push(filtroPorTipoDeEvento);
                      this.ezScheduler.tiposDeEventosPermitidos.push(tipoDeEvento);
                    }
                    this.applyFilter();
                });

            if (angular.isDefined(this.ezScheduler)) {

                this.schedulerOptions = this.ezScheduler.options;

                if (angular.isDefined(this.views)) {
                    this.schedulerOptions.views = this.views;
                }

                if (angular.isDefined(this.height)) {
                    this.schedulerOptions.height = this.height;
                }

                if (angular.isDefined(this.showWorkHours)) {
                    this.schedulerOptions.showWorkHours = this.showWorkHours;
                }

                if (angular.isDefined(this.timezone)) {
                    this.schedulerOptions.timezone = this.timezone;
                }

                if (angular.isDefined(this.messages)) {
                    this.schedulerOptions.messages = this.messages;
                }
            }
        }

        $onInit() {
        }

        applyFilter() {
            var checked = this.filtrosPorTipoDeEvento.map((value, index, lista) => {
                if (value.selecionado)
                    return value.tipoDeEvento.valor;
            });

            this.scheduler.dataSource.filter({
                operator(task) {
                    return $.inArray(task.tipoDeEvento, checked) >= 0;
                }
            });
        }
    }

    export class EzSchedulerComponent implements ng.IComponentOptions {
        public controller: any;
        public templateUrl: string;
        public bindings: any;

        constructor() {
            this.controller = EzSchedulerController;
            this.templateUrl = '/app/components/ezScheduler/ezSchedulerComponent.html';
            this.bindings = {
                ezScheduler: '<',
                views: '<',
                height: '<',
                showWorkHours: '<',
                timezone: '@',
                messages: '<'
            }
        }
    }
}