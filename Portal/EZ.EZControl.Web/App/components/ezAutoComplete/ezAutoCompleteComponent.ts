﻿import * as ezAutoComplete from "../../common/ezAutoComplete/ezAutoComplete"
export module Components.EZAutoComplete {
    export class EZAutoCompleteController {
        public ezAutoComplete: ezAutoComplete.EZAutoComplete.EZAutoComplete;
        public placeholder: string = "Pesquisar...";
        public name: string;
        public displayKey: string;
        public noCache: boolean;
        public minLength: number;
        public maxLength: number;
        public searchText: string = "";
        public selectedItem: any;
        public querySearch: (query: string) => any;
        public onSelectRecord: (item: any) => void;
        constructor() 
        {
        }
        $onInit() {
            this.ezAutoComplete.setEZAutoCompleteController(this);
        }
    }
    export class EZAutoCompleteComponent implements ng.IComponentOptions {
        public controller: any;
        public templateUrl: string;
        public bindings: any;
        public require: any;
        constructor() {
            this.controller = EZAutoCompleteController;
            this.templateUrl = "/app/components/ezAutoComplete/ezAutoCompleteComponent.html";
            this.bindings = {
                ezAutoComplete: '<',
                placeholder: '@',
                name: '@',
                displayKey: '@',
                noCache: '@',
                minLength: '@',
                maxLength: '@'
            };
        }
    }
}