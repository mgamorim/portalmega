﻿import * as ezFile from "../../common/ezFile/ezFile";
import * as loadingData from "../../common/interfaces/ILoadingData";
export module Components.EZFile {
    export class EZFileController {
        static $inject = ['Upload', 'apiUrl'];
        public ezFile: ezFile.EZFile.EZFile;
        public arquivoId: number = 0;
        public imagem: string;
        public tipoArquivo: TipoDeArquivoEnum;
        public file: any;
        public extensions: string;
        public autoUpload: boolean;
        public sistema: string;
        public uploadConfig: angular.angularFileUpload.IFileUploadConfigFile;
        public onBeforeUpload: () => void;
        public onUploadComplete: (fileName: string) => void;
        public loading: boolean;
        public tipoDocumento: TipoDeDocumentoEnum;
        public doSaveFile: () => void;
        public files: any;
        public refId: number;
        constructor(
            public Upload: angular.angularFileUpload.IUploadService,
            public apiUrl: string) {
            this.file = {
                percentual: 0
            };
            this.autoUpload = false;
            this.doSaveFile = () => {
                this.saveInternal(this.files);
            };
        }

        $onInit() {
            this.ezFile.setEzFileController(this);
        }

        fechar() {
            this.arquivoId = 0;
        }

        selectedFile(files: any) {
            if (files) {
                this.files = files;
                this.file.nome = this.formataNomeComHash(files.name);
                this.file.percentual = 0;
                if (this.autoUpload) {
                    this.saveInternal(files);
                }
            } else {
                this.file = {
                    percentual: 0
                };
            }
        }

        saveInternal(files: any) {
            if (this.files) {
                this.Upload.rename(this.files, this.file.nome);
                if (this.onBeforeUpload) {
                    this.onBeforeUpload();
                }

                var promise = this.Upload.upload({
                    url: this.apiUrl,
                    method: "POST",
                    data: {
                        file: this.files,
                        arquivoId: this.arquivoId,
                        nome: this.file.nome,
                        ativo: true,
                        sistema: this.sistema,
                        tipo: this.tipoDocumento,
                        refId: this.refId
                    }
                });
                promise.then((result) => {
                    if (this.onUploadComplete) {
                        this.onUploadComplete(this.file.nome);
                    }
                }).catch((err) => {
                    abp.message.error(err.data.message, app.localize("Error"));
                }).finally(() => {
                    this.file = {
                        percentual: 0
                    };
                });
            }
        }

        isImage() {
            if (this.tipoArquivo == TipoDeArquivoEnum.GIF ||
                this.tipoArquivo == TipoDeArquivoEnum.JPEG ||
                this.tipoArquivo == TipoDeArquivoEnum.JPG ||
                this.tipoArquivo == TipoDeArquivoEnum.PNG
            ) return true;

            return false;
        }
        private formataNomeComHash(nome: string) {
            var nomeComHash = "";
            var indexExtensao = nome.lastIndexOf('.');
            for (var i = 0; i < nome.length; i++) {
                if (i == indexExtensao) {
                    var dateNow = new Date();
                    var dia = dateNow.getDate().toString();
                    var mes = dateNow.getMonth().toString();
                    var ano = dateNow.getFullYear().toString();
                    var hora = dateNow.getHours().toString();
                    var minuto = dateNow.getMinutes().toString();
                    var segundo = dateNow.getSeconds().toString();
                    var milisegundo = dateNow.getMilliseconds().toString();
                    nomeComHash += (dia + mes + ano + hora + minuto + segundo + milisegundo + nome[i]);
                }
                else {
                    nomeComHash += nome[i];
                }
            }
            return nomeComHash;
        };
    }
    export class EZFileComponent implements ng.IComponentOptions {
        public controller: any;
        public templateUrl: string;
        public bindings: any;
        public require: any;
        constructor() {
            this.controller = EZFileController;
            this.templateUrl = "/app/components/ezFile/ezFileComponent.html";
            this.bindings = {
                ezFile: '<'
            };
        }
    }
}