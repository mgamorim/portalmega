﻿using Abp.Application.Navigation;
using Abp.Localization;
using EZ.EZControl.Authorization;
using EZ.EZControl.Web.Navigation;
using System.Collections.Generic;

namespace EZ.EZControl.Web.App.Startup
{
    public class AddMenuItemHelper
    {
        public List<MenuItemDefinition> Lista { get; set; }
        public void SetList(List<MenuItemDefinition> lista)
        {
            Lista = lista;
        }

        public AddMenuItemHelper AdicionarItem(MenuItemDefinition menuItemDefinition)
        {
            Lista.Add(menuItemDefinition);
            return this;
        }

        public void Ordenar()
        {
            Lista.Sort(Comparison);
        }

        private int Comparison(MenuItemDefinition menuItemDefinition, MenuItemDefinition itemDefinition)
        {

            return string.Compare(LocalizationHelper.Manager.GetString((LocalizableString)menuItemDefinition.DisplayName), LocalizationHelper.Manager.GetString((LocalizableString)itemDefinition.DisplayName));
        }

        public void AdicionarListaSubMenus(MenuItemDefinition menuItemDefinition)
        {
            foreach (var item in Lista)
            {
                menuItemDefinition.AddItem(item);
            }
        }
    }

    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class AppNavigationProvider : NavigationProvider
    {
        private List<MenuItemDefinition> inicioMenuItems { get; set; }

        private List<MenuItemDefinition> globalMenuItems { get; set; }

        private List<MenuItemDefinition> cmsMenuItems { get; set; }

        private List<MenuItemDefinition> agendaMenuItems { get; set; }

        private List<MenuItemDefinition> ezMedicalMenuItems { get; set; }

        private List<MenuItemDefinition> estoqueMenuItems { get; set; }

        private List<MenuItemDefinition> vendasMenuItems { get; set; }

        private List<MenuItemDefinition> servicosMenuItems { get; set; }

        private List<MenuItemDefinition> financeiroMenuItems { get; set; }

        private List<MenuItemDefinition> ezLivMenuItems { get; set; }

        private List<MenuItemDefinition> ezPagMenuItems { get; set; }


        private AddMenuItemHelper addMenuItemHelper { get; set; }

        public override void SetNavigation(INavigationProviderContext context)
        {
            addMenuItemHelper = new AddMenuItemHelper();
            inicioMenuItems = new List<MenuItemDefinition>();
            globalMenuItems = new List<MenuItemDefinition>();
            cmsMenuItems = new List<MenuItemDefinition>();
            agendaMenuItems = new List<MenuItemDefinition>();
            ezMedicalMenuItems = new List<MenuItemDefinition>();
            estoqueMenuItems = new List<MenuItemDefinition>();
            vendasMenuItems = new List<MenuItemDefinition>();
            financeiroMenuItems = new List<MenuItemDefinition>();
            servicosMenuItems = new List<MenuItemDefinition>();
            ezLivMenuItems = new List<MenuItemDefinition>();
            ezPagMenuItems = new List<MenuItemDefinition>();

            MenuItemDefinition inicioMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Inicio,
                L("Inicio"),
                icon: "icon-target");

            MenuItemDefinition globalMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Global,
                L("Global"),
                icon: "icon-globe");

            MenuItemDefinition cmsMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.CMS,
                L("CMS"),
                icon: "icon-grid");

            MenuItemDefinition agendaMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Agenda,
                L("Agenda"),
                icon: "icon-calendar");

            MenuItemDefinition estoqueMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Estoque,
                L("Estoque"),
                icon: "icon-basket-loaded");

            MenuItemDefinition ezMedicalMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.EZMedical,
                L("EZMedical"),
                icon: "icon-chemistry");

            MenuItemDefinition vendasMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Vendas,
                L("Vendas"),
                icon: "icon-wallet");

            MenuItemDefinition servicosMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Servicos,
                L("Servicos"),
                icon: "icon-screen-desktop");

            MenuItemDefinition financeiroMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.Financeiro,
                L("Financeiro"),
                icon: "icon-credit-card");

            MenuItemDefinition ezLivMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.EZLiv,
                L("Mega"),
                icon: "icon-heart");

            MenuItemDefinition ezPagMenuItem = new MenuItemDefinition(
                PageNames.App.Tenant.EZPag,
                L("EZPag"),
                icon: "icon-wallet");

            #region Início

            addMenuItemHelper.SetList(inicioMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                PageNames.App.Tenant.Dashboard,
                L("Dashboard"),
                url: "tenant.dashboard",
                icon: "icon-graph",
                requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
            ));

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(inicioMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion

            #region Menus do Global

            addMenuItemHelper.SetList(globalMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalAgencias,
                    L("Agencias"),
                    url: "tenant.global.agencia",
                    icon: "icon-wallet",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Agencia
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalArquivos,
                    L("Arquivos"),
                    url: "tenant.global.arquivo",
                    icon: "icon-docs",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Arquivo
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalBancos,
                    L("Bancos"),
                    url: "tenant.global.banco",
                    icon: "icon-wallet",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Banco
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalCamposPersonalizados,
                    L("CamposPersonalizados"),
                    url: "tenant.global.campospersonalizados",
                    icon: "icon-puzzle",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_CampoPersonalizado
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalCidade,
                    L("Cidades"),
                    url: "tenant.global.cidade",
                    icon: "icon-map",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Cidade
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalClientes,
                    L("Clientes"),
                    url: "tenant.global.clientes",
                    icon: "icon-user-follow",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Cliente
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalConexoesSMTP,
                    L("ConexoesSMTP"),
                    url: "tenant.global.conexaoSMTP",
                    icon: "icon-paper-plane",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_ConexaoSMTP
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalContatos,
                    L("Contatos"),
                    url: "tenant.global.contato",
                    icon: "icon-envelope-open",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Contato
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalEnderecos,
                    L("Enderecos"),
                    url: "tenant.global.endereco",
                    icon: "icon-map",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Endereco
                    )
                )
                .AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalDadosEmpresa,
                    L("DadosEmpresa"),
                    url: "tenant.global.dadosempresa",
                    icon: "icon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_DadosEmpresa
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalDepartamentos,
                    L("Departamentos"),
                    url: "tenant.global.departamento",
                    icon: "icon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Departamento
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalDocumento,
                    L("Documentos"),
                    url: "tenant.global.documento",
                    icon: "icon-docs",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Documento
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalEstado,
                    L("Estados"),
                    url: "tenant.global.estado",
                    icon: "icon-map",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Estado
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalFeriado,
                    L("Feriados"),
                    url: "tenant.global.feriado",
                    icon: "icon-calendar",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Feriado
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalFornecedores,
                    L("Fornecedores"),
                    url: "tenant.global.fornecedores",
                    icon: "icon-user-follow",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Fornecedor
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalGrupoPessoa,
                    L("GruposPessoa"),
                    url: "tenant.global.grupoPessoa",
                    icon: "icon-user-following",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_GrupoPessoa
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalItensHierarquia,
                    L("ItensHierarquia"),
                    url: "tenant.global.itemHierarquia",
                    icon: "icon-list",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_ItemHierarquia
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalPais,
                    L("Paises"),
                    url: "tenant.global.pais",
                    icon: "icon-globe-alt",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Pais
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalPessoasFisicas,
                    L("PessoasFisicas"),
                    url: "tenant.global.pessoaFisica",
                    icon: "icon-user",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_PessoaFisica
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalPessoasJuridicas,
                    L("PessoasJuridicas"),
                    url: "tenant.global.pessoaJuridica",
                    icon: "icon-user",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_PessoaJuridica
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalProfissao,
                    L("Profissoes"),
                    url: "tenant.global.profissao",
                    icon: "icon-graduation",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Profissao
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalRamosDoFornecedor,
                    L("RamosDoFornecedor"),
                    url: "tenant.global.ramoDoFornecedor",
                    icon: "icon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_RamoDoFornecedor
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalTemplatesEmail,
                    L("TemplatesDeEmail"),
                    url: "tenant.global.templateEmail",
                    icon: "icon-envelope",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_TemplateEmail
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalTipoDeLogradouro,
                    L("TipoDeLogradouro"),
                    url: "tenant.global.tipoDeLogradouro",
                    icon: "icon-home",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_TipoDeLogradouro
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalTipoDeContato,
                    L("TiposDeContatos"),
                    url: "tenant.global.tipoDeContato",
                    icon: "icon-envelope-letter",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_TipoDeContato
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalTipoDeDocumento,
                    L("TiposDeDocumento"),
                    url: "tenant.global.tipoDeDocumento",
                    icon: "icon-doc",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_TipoDeDocumento
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalTratamento,
                    L("Tratamentos"),
                    url: "tenant.global.tratamento",
                    icon: "icon-graduation",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Tratamento
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalParametros,
                    L("Parametros"),
                    url: "tenant.global.parametros",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Parametros
                    )
                ).AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.GlobalEmpresas,
                    L("Empresas"),
                    url: "tenant.global.empresa",
                    icon: "icon-user-follow",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Global_Empresa
                    )
                );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(globalMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus do Global

            #region Menus do CMS

            addMenuItemHelper.SetList(cmsMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.CMSParametros,
                    L("Parametros"),
                    url: "tenant.cms.parametros",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Parametros
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.CMSSite,
                    L("Sites"),
                    url: "tenant.cms.site",
                    icon: "icon-cloud-upload",
                    requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Site
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.CMSCategoria,
                    L("Categorias"),
                    url: "tenant.cms.categoria",
                    icon: "icon-note",
                    requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Categoria
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.CMSArquivos,
                    L("Arquivos"),
                    url: "tenant.cms.arquivo",
                    icon: "icon-docs",
                    requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Arquivo
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.CMSPost,
                    L("Posts"),
                    url: "tenant.cms.post",
                    icon: "icon-docs",
                    requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Post
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.CMSPagina,
                    L("Paginas"),
                    url: "tenant.cms.pagina",
                    icon: "icon-docs",
                    requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Pagina
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.CMSTags,
                   L("Tags"),
                   url: "tenant.cms.tag",
                   icon: "icon-tag",
                   requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Tag
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.CMSMenu,
                  L("Menu"),
                  url: "tenant.cms.menu",
                  icon: "icon-list",
                  requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Menu
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.CMSItemDeMenu,
                  L("ItensDeMenu"),
                  url: "tenant.cms.itemDeMenu",
                  icon: "icon-directions",
                  requiredPermissionName: AppPermissions.Pages_Tenant_CMS_ItemDeMenu
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.CMSTemplate,
                  L("Templates"),
                  url: "tenant.cms.template",
                  icon: "icon-directions",
                  requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Template
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.CMSWidget,
                  L("Widgets"),
                  url: "tenant.cms.widget",
                  icon: "icon-directions",
                  requiredPermissionName: AppPermissions.Pages_Tenant_CMS_Widget
                  )
              );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(cmsMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus do CMS

            #region Menus da Agenda

            addMenuItemHelper.SetList(agendaMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.AgendaParametro,
                    L("Parametros"),
                    url: "tenant.agenda.parametros",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Parametros
                    ).AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.AgendaParametrosPadroes,
                            L("Agenda.Parametros.Padroes"),
                            url: "tenant.agenda.parametros",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Parametros_Padroes
                    )).AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.AgendaParametrosRegras,
                            L("Agenda.Parametros.Regras"),
                            url: "tenant.agenda.regras",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Parametros_Regras
                    ))
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.AgendaDisponibilidade,
                    L("Disponibilidades"),
                    url: "tenant.agenda.disponibilidade",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Disponibilidade
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.AgendaTestes,
                    L("Testes"),
                    url: "tenant.agenda.testes",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Disponibilidade
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.AgendaDisponibilidade,
                    L("Bloqueios"),
                    url: "tenant.agenda.bloqueio",
                    icon: "icon-lock",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Disponibilidade
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.AgendaEvento,
                   L("Eventos"),
                   url: "tenant.agenda.evento",
                   icon: "icon-layers",
                   requiredPermissionName: AppPermissions.Pages_Tenant_Agenda_Evento
                   )
               );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(agendaMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus da Agenda

            #region Menus do Módulo Estoque

            addMenuItemHelper.SetList(estoqueMenuItems);

            //addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
            //        PageNames.App.Tenant.EstoqueAlgumaCoisa,
            //        L("Estoque"),
            //        url: "tenant.estoque.algumacoisa",
            //        icon: "icon-settings",
            //        requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_ALGUMACOISA
            //        )
            //    );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EstoqueProduto,
                    L("Produto"),
                    url: "tenant.estoque.produto",
                    icon: "icon-calendar",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Produto)
                );
            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EstoqueEntrada,
                    L("Estoque.Entrada"),
                    url: "tenant.estoque.entrada",
                    icon: "icon-calendar",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Entrada)
                );
            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EstoqueSaida,
                    L("Estoque.Saida"),
                    url: "tenant.estoque.saida",
                    icon: "icon-calendar",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Saida)
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EstoqueParametro,
                    L("Parametros"),
                    url: "tenant.estoque.parametro",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Parametro)
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.EstoqueParametroPadrao,
                            L("Estoque.Parametro.Padrao"),
                            url: "tenant.estoque.parametro",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Parametro_Padrao
                    ))
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.EstoqueParametroNatureza,
                            L("Estoque.Parametro.Natureza"),
                            url: "tenant.estoque.natureza",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza
                    ))
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.EstoqueParametroFracao,
                            L("Estoque.Parametro.Fracao"),
                            url: "tenant.estoque.fracao",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao
                    ))
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.EstoqueParametroUnidadeMedida,
                            L("Estoque.Parametro.UnidadeMedida"),
                            url: "tenant.estoque.unidadeMedida",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida
                    ))
                    .AddItem(new MenuItemDefinition(
                            PageNames.App.Tenant.EstoqueParametroLocalArmazenamento,
                            L("Estoque.Parametro.LocalArmazenamento"),
                            url: "tenant.estoque.localArmazenamento",
                            icon: "icon-settings",
                            requiredPermissionName: AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento
                    ))
                );


            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(estoqueMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus do Módulo Estoque

            #region Menus do Módulo Vendas

            addMenuItemHelper.SetList(vendasMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.VendasPedido,
                    L("Pedidos"),
                    url: "tenant.vendas.pedido",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Vendas_Pedido
                    )
                );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(vendasMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus do Módulo Vendas

            #region Menus do Módulo Financeiro

            addMenuItemHelper.SetList(financeiroMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.FinanceiroTipoDeDocumento,
                    L("Financeiro.TipoDeDocumentoFinanceiro"),
                    url: "tenant.financeiro.tipoDeDocumento",
                    icon: "icon-doc",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                        PageNames.App.Tenant.FinanceiroDocumento,
                        L("Financeiro.Documento"),
                        url: "tenant.financeiro.documento",
                        icon: "icon-drawer",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Financeiro_Documento
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.FinanceiroDocumentoAPagar,
                        L("Financeiro.Documento.APagar"),
                        icon: "icon-action-redo",
                        url: "tenant.financeiro.documentoAPagar",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Financeiro_DocumentoAPagar
                        ))
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.FinanceiroDocumentoAReceber,
                        L("Financeiro.Documento.AReceber"),
                        icon: "icon-action-undo",
                        url: "tenant.financeiro.documentoAReceber",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Financeiro_DocumentoAReceber
                    ))
                );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(financeiroMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus do Módulo Financeiro

            #region Menus do Módulo Servicos

            addMenuItemHelper.SetList(servicosMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.ServicosServico,
                    L("Servicos"),
                    url: "tenant.servicos.servico",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Servicos_Servico
                    )
                );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(servicosMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus do Módulo Servicos

            #region Menus de EZMedical

            addMenuItemHelper.SetList(ezMedicalMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EZMedicalParametro,
                    L("Parametros"),
                    url: "tenant.ezmedical.parametro",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_Parametro
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EZMedicalEvento,
                    L("Agenda"),
                    url: "tenant.ezmedical.evento",
                    icon: "icon-calendar",
                    requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_Evento
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EZMedicalEspecialidade,
                    L("Especialidade"),
                    url: "tenant.ezmedical.especialidade",
                    icon: "icon-graduation",
                    requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_Especialidade
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZMedicalMedico,
                   L("Medico"),
                   url: "tenant.ezmedical.medico",
                   icon: "icon-chemistry",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_Medico
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZMedicalRegra,
                   L("Regras"),
                   url: "tenant.ezmedical.regras",
                   icon: "icon-settings",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_RegraConsulta
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZMedicalDisponibilidade,
                   L("Disponibilidade"),
                   url: "tenant.ezmedical.disponibilidade",
                   icon: "icon-settings",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_Disponibilidade
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZMedicalBloqueio,
                   L("Bloqueio"),
                   url: "tenant.ezmedical.bloqueio",
                   icon: "icon-lock",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZMedical_Bloqueio
                   )
               );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(ezMedicalMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus de EZMedical

            #region Menus de EZLiv

            addMenuItemHelper.SetList(ezLivMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLivParametro,
                   L("Parametro"),
                   url: "tenant.ezliv.parametro",
                   icon: "icon-settings",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_ParametroEZLiv
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_ProdutoDePlanoDeSaude,
                   L("ProdutoDePlanoDeSaude"),
                   url: "tenant.ezliv.produtoDePlanoDeSaude",
                   icon: "icon-basket-loaded",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZLiv_Contrato,
                  L("Contrato"),
                  url: "tenant.ezliv.contrato",
                  icon: "icon-doc",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Contrato
                  )
              );


            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_PropostaDeContratacao,
                   L("PropostaDeContratacao"),
                   url: "tenant.ezliv.propostaDeContratacao",
                   icon: "icon-settings",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_Administradora,
                   L("Administradora"),
                   url: "tenant.ezliv.administradora",
                   icon: "icon-home",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Administradora
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZLiv_Corretor,
                  L("Corretor"),
                  url: "tenant.ezliv.corretor",
                  icon: "icon-user",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Corretora
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZLiv_Corretora,
                  L("Corretora"),
                  url: "tenant.ezliv.corretora",
                  icon: "icon-home",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Corretora
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                 PageNames.App.Tenant.EZLiv_Associacao,
                 L("Entidade"),
                 url: "tenant.ezliv.associacao",
                 icon: "icon-home",
                 requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Associacao
                 )
             );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_Operadora,
                   L("Operadora"),
                   url: "tenant.ezliv.operadora",
                   icon: "icon-home",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Operadora
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_ClienteEZLiv,
                   L("ClienteEZLiv"),
                   url: "tenant.ezliv.clienteEZLiv",
                   icon: "icon-user-follow",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_FaixaEtaria,
                   L("FaixaEtaria"),
                   url: "tenant.ezliv.faixaEtaria",
                   icon: "icon-calendar",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_RedeCredenciada,
                   L("RedeCredenciada"),
                   url: "tenant.ezliv.redeCredenciada",
                   icon: "icon-home",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada
                   )
               );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZLiv_Beneficiario,
                  L("Beneficiario"),
                  url: "tenant.ezliv.beneficiario",
                  icon: "icon-calendar",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Beneficiario
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                 PageNames.App.Tenant.EZLiv_Laboratorio,
                 L("Laboratorio"),
                 url: "tenant.ezliv.laboratorio",
                 icon: "icon-calendar",
                 requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Laboratorio
                 )
             );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
              PageNames.App.Tenant.EZLiv_Hospital,
              L("Hospital"),
              url: "tenant.ezliv.hospital",
              icon: "icon-calendar",
              requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Hospital
              )
            );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                 PageNames.App.Tenant.EZLiv_Chancela,
                 L("Chancela"),
                 url: "tenant.ezliv.chancela",
                 icon: "icon-home",
                 requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Chancela
                 )
             );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                PageNames.App.Tenant.EZLiv_QuestionarioDeDeclaracaoDeSaude,
                L("QuestionarioDeDeclaracaoDeSaude"),
                url: "tenant.ezliv.questionarioDeDeclaracaoDeSaude",
                icon: "icon-doc",
                requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude
                )
            );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                PageNames.App.Tenant.EZLiv_Relatorio,
                L("Relatorio"),
                url: "tenant.ezliv.relatorio",
                icon: "icon-doc",
                requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Relatorio
                )
            );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
               PageNames.App.Tenant.EZLiv_DeclaracaoDoBeneficiario,
               L("DeclaracaoDoBeneficiario"),
               url: "tenant.ezliv.declaracaoDoBeneficiario",
               icon: "icon-doc",
               requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario
               )
           );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                   PageNames.App.Tenant.EZLiv_IndiceDeReajustePorFaixaEtaria,
                   L("IndiceDeReajustePorFaixaEtaria"),
                   url: "tenant.ezliv.indiceDeReajustePorFaixaEtaria",
                   icon: "icon-calendar",
                   requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria
                   )
               );

            //addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
            //       PageNames.App.Tenant.EZLiv_MeusProdutos,
            //       L("MeusProdutos"),
            //       url: "tenant.ezliv.meusProdutos",
            //       icon: "fa fa-shopping-cart",
            //       requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_MeusProdutos
            //       )
            //   );

            //addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
            //       PageNames.App.Tenant.EZLiv_MeusProcessos,
            //       L("MeusProcessos"),
            //       url: "tenant.ezliv.meusProcessos",
            //       icon: "fa fa-shopping-cart",
            //       requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_MeusProcessos
            //       )
            //   );

            //addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
            //     PageNames.App.Tenant.EZLiv_Carencia,
            //     L("Carencia"),
            //     url: "tenant.ezliv.carencia",
            //     icon: "icon-home",
            //     requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_Carencia
            //     )
            // );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora,
                  L("PermissaoDeVendaDePlanoDeSaudeParaCorretoraMenu"),
                  url: "tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora",
                  icon: "icon-settings",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                 PageNames.App.Tenant.EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor,
                 L("PermissaoDeVendaDePlanoDeSaudeParaCorretorMenu"),
                 url: "tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor",
                 icon: "icon-settings",
                 requiredPermissionName: AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor
                 )
             );
            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(ezLivMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus de EZLiv

            #region Menus de EZPag

            addMenuItemHelper.SetList(ezPagMenuItems);

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZPagParametro,
                  L("Parametro"),
                  url: "tenant.ezpag.parametro",
                  icon: "icon-settings",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZPag_ParametroEzpag
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                  PageNames.App.Tenant.EZPag_Transacao,
                  L("Transacao"),
                  url: "tenant.ezpag.transacao",
                  icon: "icon-settings",
                  requiredPermissionName: AppPermissions.Pages_Tenant_EZPag_Transacao
                  )
              );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EZPagTestes,
                    L("Testes"),
                    url: "tenant.ezpag.testes",
                    icon: "icon-settings",
                    requiredPermissionName: AppPermissions.Pages_Tenant_EZPag_Testes
                    )
                );

            addMenuItemHelper.AdicionarItem(new MenuItemDefinition(
                    PageNames.App.Tenant.EZPag_FormaDePagamento,
                    L("FormaDePagamento"),
                    url: "tenant.ezpag.formaDePagamento",
                    icon: "icon-calendar",
                    requiredPermissionName: AppPermissions.Pages_Tenant_EZPag_FormaDePagamento)
                );

            addMenuItemHelper.Ordenar();
            addMenuItemHelper.AdicionarListaSubMenus(ezPagMenuItem);
            addMenuItemHelper.Lista.Clear();

            #endregion Menus de EZPag

            context.Manager.MainMenu

            #region Common

                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Tenants,
                    L("Tenants"),
                    url: "host.tenants",
                    icon: "icon-globe",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Editions,
                    L("Editions"),
                    url: "host.editions",
                    icon: "icon-grid",
                    requiredPermissionName: AppPermissions.Pages_Editions
                    )
                )

            #endregion Common

            #region Inicio

                .AddItem(inicioMenuItem)

            #endregion Módulo Global

            #region Módulo Global

                .AddItem(globalMenuItem)

            #endregion Módulo Global

            #region Módulo CMS

                .AddItem(cmsMenuItem)

            #endregion Módulo CMS

            #region Módulo Agenda

                .AddItem(agendaMenuItem)

            #endregion Módulo Agenda

            #region Módulo Vendas

                .AddItem(vendasMenuItem)

            #endregion Módulo Vendas

            #region Módulo Estoque

                .AddItem(estoqueMenuItem)

            #endregion Módulo Estoque

            #region Módulo Serviços

                .AddItem(servicosMenuItem)

            #endregion Módulo Serviços

            #region EZMedical

                .AddItem(ezMedicalMenuItem)

            #endregion EZMedical

            #region EZLiv

                .AddItem(ezLivMenuItem)

            #endregion EZLiv

            #region EZPag

                .AddItem(ezPagMenuItem)

            #endregion EZPag

            #region Módulo Financeiro

                .AddItem(financeiroMenuItem)

            #endregion Módulo Financeiro

            #region Administration

                .AddItem(new MenuItemDefinition(
                    PageNames.App.Common.Administration,
                    L("Administration"),
                    icon: "icon-wrench"
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.OrganizationUnits,
                        L("OrganizationUnits"),
                        url: "organizationUnits",
                        icon: "icon-layers",
                        requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Roles,
                        L("Roles"),
                        url: "roles",
                        icon: "icon-briefcase",
                        requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Users,
                        L("Users"),
                        url: "users",
                        icon: "icon-users",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.UsuarioPorPessoa,
                        L("UsuariosPorPessoa"),
                        url: "tenant.usuarioPorPessoa",
                        icon: "icon-users",
                        requiredPermissionName: AppPermissions.Pages_Administration_UsuarioPorPessoa
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Languages,
                        L("Languages"),
                        url: "languages",
                        icon: "icon-flag",
                        requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.AuditLogs,
                        L("AuditLogs"),
                        url: "auditLogs",
                        icon: "icon-lock",
                        requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Maintenance,
                        L("Maintenance"),
                        url: "host.maintenance",
                        icon: "icon-wrench",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Maintenance
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Settings,
                        L("Settings"),
                        url: "host.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Settings,
                        L("Settings"),
                        url: "tenant.settings",
                        icon: "icon-settings",
                        requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.ClienteEZ,
                        L("ClienteEZ"),
                        url: "tenant.clienteez",
                        icon: "icon-user-follow",
                        requiredPermissionName: AppPermissions.Pages_Administration_ClienteEZ
                    ))
                );

            #endregion Administration
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, EZControlConsts.LocalizationSourceName);
        }
    }
}