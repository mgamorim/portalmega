﻿export module Routes {
    export class VendasRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.vendas', {
                        'abstract': true,
                        url: '/vendas',
                        template: '<div ui-view class="fade-in-up"></div>'
                    });

                    //// Parametros
                    //if (abp.auth.hasPermission('Pages.Tenant.Vendas.Parametros.Padroes')) {
                    //    $stateProvider.state('tenant.vendas.parametros', {
                    //        url: '/parametros',
                    //        templateUrl: '~/App/tenant/views/vendas/parametros/padroes/edit.cshtml',
                    //        menu: 'Vendas.Parametros.Tenant'
                    //    });
                    //}

                    // Pedido
                    if (abp.auth.hasPermission('Pages.Tenant.Vendas.Pedido')) {
                        $stateProvider.state('tenant.vendas.pedido', {
                            url: '/pedidos',
                            templateUrl: '~/App/tenant/views/vendas/pedido/index.cshtml',
                            menu: 'Vendas.Pedidos.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Vendas.Pedido.Edit')) {
                        $stateProvider.state('tenant.vendas.pedido.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/vendas/pedido/edit.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Vendas.Pedido.Create')) {
                        $stateProvider.state('tenant.vendas.pedido.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/vendas/pedido/create.cshtml'
                                    }
                                }
                            });
                    }
               }
            ]);
        }
    }
}