﻿export module Routes {
    export class GlobalRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {

                    $stateProvider.state('tenant.global', {
                        'abstract': true,
                        url: '/global',
                        template: '<div ui-view class="fade-in-up">AQUiii</div>'
                    });

                    // agencia
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Agencia')) {
                        $stateProvider.state('tenant.global.agencia',
                            {
                                url: "/agencia",
                                templateUrl: '/app/tenant/views/global/agencia/index.cshtml',
                                menu: 'Global.Agencia.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Agencia.Create')) {
                        $stateProvider.state('tenant.global.agencia.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/global/agencia/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Agencia.Edit')) {
                        $stateProvider.state('tenant.global.agencia.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/global/agencia/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // banco
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Banco')) {
                        $stateProvider.state('tenant.global.banco',
                            {
                                url: '/banco',
                                templateUrl: '~/App/tenant/views/global/banco/index.cshtml',
                                menu: 'Global.Banco.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Banco.Create')) {
                        $stateProvider.state('tenant.global.banco.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/banco/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Banco.Edit')) {
                        $stateProvider.state('tenant.global.banco.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/banco/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Perfil
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Perfil')) {
                      $stateProvider.state('tenant.perfil',
                        {
                          url: '/perfil',
                          templateUrl: '~/App/tenant/views/global/perfil/edit.cshtml',
                          menu: 'Global.Perfil.Tenant',
                          ncyBreadcrumb: {
                            label: 'Perfil'
                          }
                        });
                    }

                    // Cidade
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Cidade')) {
                        $stateProvider.state('tenant.global.cidade',
                            {
                                url: '/cidade',
                                templateUrl: '/app/tenant/views/global/cidade/index.cshtml',
                                menu: 'Global.Cidade.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Cidade.Create')) {
                        $stateProvider.state('tenant.global.cidade.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/global/cidade/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Cidade.Edit')) {
                        $stateProvider.state('tenant.global.cidade.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/global/cidade/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Estado
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Estado')) {
                        $stateProvider.state('tenant.global.estado',
                            {
                                url: '/estado',
                                templateUrl: '~/App/tenant/views/global/estado/index.cshtml',
                                menu: 'Global.Estado.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Estado.Create')) {
                        $stateProvider.state('tenant.global.estado.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/estado/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Estado.Edit')) {
                        $stateProvider.state('tenant.global.estado.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/estado/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // País
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Pais')) {
                        $stateProvider.state('tenant.global.pais',
                            {
                                url: '/pais',
                                templateUrl: '~/App/tenant/views/global/pais/index.cshtml',
                                menu: 'Global.Pais.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Pais.Create')) {
                        $stateProvider.state('tenant.global.pais.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/pais/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Pais.Edit')) {
                        $stateProvider.state('tenant.global.pais.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/pais/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Conexao SMTP
                    if (abp.auth.hasPermission('Pages.Tenant.Global.ConexaoSMTP')) {
                        $stateProvider.state('tenant.global.conexaoSMTP',
                            {
                                url: '/conexaoSMTP',
                                templateUrl: '~/App/tenant/views/global/conexaoSmtp/index.cshtml',
                                menu: 'Global.ConexaoSMTP.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.ConexaoSMTP.Create')) {
                        $stateProvider.state('tenant.global.conexaoSMTP.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/conexaoSMTP/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.ConexaoSMTP.Edit')) {
                        $stateProvider.state('tenant.global.conexaoSMTP.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/conexaoSMTP/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Departamento
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Departamento')) {
                        $stateProvider.state('tenant.global.departamento',
                            {
                                url: '/departamento',
                                templateUrl: '~/App/tenant/views/global/departamento/index.cshtml',
                                menu: 'Global.Departamento.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Departamento.Create')) {
                        $stateProvider.state('tenant.global.departamento.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/departamento/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Departamento.Edit')) {
                        $stateProvider.state('tenant.global.departamento.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/departamento/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Feriado
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Feriado')) {
                        $stateProvider.state('tenant.global.feriado',
                            {
                                url: '/feriado',
                                templateUrl: '~/App/tenant/views/global/feriado/index.cshtml',
                                menu: 'Global.Feriado.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Feriado.Create')) {
                        $stateProvider.state('tenant.global.feriado.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/feriado/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Feriado.Edit')) {
                        $stateProvider.state('tenant.global.feriado.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/feriado/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // GrupoPessoa
                    if (abp.auth.hasPermission('Pages.Tenant.Global.GrupoPessoa')) {
                        $stateProvider.state('tenant.global.grupoPessoa',
                            {
                                url: '/grupoPessoa',
                                templateUrl: '~/App/tenant/views/global/grupoPessoa/index.cshtml',
                                menu: 'Global.GrupoPessoa.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.GrupoPessoa.Create')) {
                        $stateProvider.state('tenant.global.grupoPessoa.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/grupoPessoa/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.GrupoPessoa.Edit')) {
                        $stateProvider.state('tenant.global.grupoPessoa.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/grupoPessoa/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Parametro
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Parametros')) {
                        $stateProvider.state('tenant.global.parametros',
                            {
                                url: '/parametros',
                                templateUrl: '~/App/tenant/views/global/parametros/edit.cshtml',
                                menu: 'Global.Parametros.Tenant'
                            });
                    }

                    // Profissão
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Profissao')) {
                        $stateProvider.state('tenant.global.profissao',
                            {
                                url: '/profissao',
                                templateUrl: '~/App/tenant/views/global/profissao/index.cshtml',
                                menu: 'Global.Profissao.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Profissao.Create')) {
                        $stateProvider.state('tenant.global.profissao.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/profissao/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Profissao.Edit')) {
                        $stateProvider.state('tenant.global.profissao.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/profissao/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Ramo Do Fornecedor
                    if (abp.auth.hasPermission('Pages.Tenant.Global.RamoDoFornecedor')) {
                        $stateProvider.state('tenant.global.ramoDoFornecedor',
                            {
                                url: '/ramoDoFornecedor',
                                templateUrl: '~/App/tenant/views/global/ramoDoFornecedor/index.cshtml',
                                menu: 'Global.RamoDoFornecedor.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.RamoDoFornecedor.Create')) {
                        $stateProvider.state('tenant.global.ramoDoFornecedor.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/ramoDoFornecedor/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.RamoDoFornecedor.Edit')) {
                        $stateProvider.state('tenant.global.ramoDoFornecedor.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/ramoDoFornecedor/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // TemplateEmail
                    if (abp.auth.hasPermission('Pages.Tenant.Global.TemplateEmail')) {
                        $stateProvider.state('tenant.global.templateEmail',
                            {
                                url: '/templateEmail',
                                templateUrl: '~/App/tenant/views/global/templateEmail/index.cshtml',
                                menu: 'Global.TemplateEmail.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TemplateEmail.Create')) {
                        $stateProvider.state('tenant.global.templateEmail.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/templateEmail/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TemplateEmail.Edit')) {
                        $stateProvider.state('tenant.global.templateEmail.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/templateEmail/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Tipo de Contato
                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeContato')) {
                        $stateProvider.state('tenant.global.tipoDeContato',
                            {
                                url: '/tipodecontato',
                                templateUrl: '~/App/tenant/views/global/tipoDeContato/index.cshtml',
                                menu: 'Global.TipoDeContato.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeContato.Create')) {
                        $stateProvider.state('tenant.global.tipoDeContato.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tipoDeContato/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeContato.Edit')) {
                        $stateProvider.state('tenant.global.tipoDeContato.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tipoDeContato/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Tipo de Documento
                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeDocumento')) {
                        $stateProvider.state('tenant.global.tipoDeDocumento',
                            {
                                url: '/tipodedocumento',
                                templateUrl: '~/App/tenant/views/global/tipoDeDocumento/index.cshtml',
                                menu: 'Global.TipoDeDocumento.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeDocumento.Create')) {
                        $stateProvider.state('tenant.global.tipoDeDocumento.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tipoDeDocumento/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeDocumento.Edit')) {
                        $stateProvider.state('tenant.global.tipoDeDocumento.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tipoDeDocumento/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Tipo de Logradouro
                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeLogradouro')) {
                        $stateProvider.state('tenant.global.tipoDeLogradouro',
                            {
                                url: '/tipodelogradouro',
                                templateUrl: '~/App/tenant/views/global/tipoDeLogradouro/index.cshtml',
                                menu: 'Global.TipoDeLogradouro.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeLogradouro.Create')) {
                        $stateProvider.state('tenant.global.tipoDeLogradouro.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tipoDeLogradouro/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.TipoDeLogradouro.Edit')) {
                        $stateProvider.state('tenant.global.tipoDeLogradouro.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tipoDeLogradouro/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Tratamento
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Tratamento')) {
                        $stateProvider.state('tenant.global.tratamento',
                            {
                                url: '/tratamento',
                                templateUrl: '~/App/tenant/views/global/tratamento/index.cshtml',
                                menu: 'Global.Tratamento.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Tratamento.Create')) {
                        $stateProvider.state('tenant.global.tratamento.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tratamento/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Tratamento.Edit')) {
                        $stateProvider.state('tenant.global.tratamento.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/tratamento/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Documento
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Documento')) {
                        $stateProvider.state('tenant.global.documento',
                            {
                                url: '/documento',
                                templateUrl: '~/App/tenant/views/global/documento/index.cshtml',
                                menu: 'Global.Documento.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Documento.Create')) {
                        $stateProvider.state('tenant.global.documento.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/documento/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Documento.Edit')) {
                        $stateProvider.state('tenant.global.documento.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/documento/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Contato
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Contato')) {
                        $stateProvider.state('tenant.global.contato',
                            {
                                url: '/contato',
                                templateUrl: '~/App/tenant/views/global/contato/index.cshtml',
                                menu: 'Global.Contato.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Contato.Create')) {
                        $stateProvider.state('tenant.global.contato.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/contato/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Contato.Edit')) {
                        $stateProvider.state('tenant.global.contato.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/contato/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Endereco
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Endereco')) {
                        $stateProvider.state('tenant.global.endereco',
                            {
                                url: '/endereco',
                                templateUrl: '~/App/tenant/views/global/endereco/index.cshtml',
                                menu: 'Global.Endereco.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Endereco.Create')) {
                        $stateProvider.state('tenant.global.endereco.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/endereco/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Endereco.Edit')) {
                        $stateProvider.state('tenant.global.endereco.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/endereco/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // ItemHierarquia
                    if (abp.auth.hasPermission('Pages.Tenant.Global.ItemHierarquia')) {
                        $stateProvider.state('tenant.global.itemHierarquia',
                            {
                                url: '/itemHierarquia',
                                templateUrl: '~/App/tenant/views/global/itemHierarquia/index.cshtml',
                                menu: 'Global.ItemHierarquia.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.ItemHierarquia.Create')) {
                        $stateProvider.state('tenant.global.itemHierarquia.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/itemHierarquia/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Global.ItemHierarquia.Edit')) {
                        $stateProvider.state('tenant.global.itemHierarquia.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/itemHierarquia/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Clientes
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Cliente')) {
                        $stateProvider.state('tenant.global.clientes',
                            {
                                url: '/clientes',
                                templateUrl: '~/App/tenant/views/global/clientes/index.cshtml',
                                menu: 'Global.Fornecedores.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Cliente.Create')) {
                        $stateProvider.state('tenant.global.clientes.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/clientes/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Cliente.Edit')) {
                        $stateProvider.state('tenant.global.clientes.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/clientes/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Fornecedores
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Fornecedor')) {
                        $stateProvider.state('tenant.global.fornecedores',
                            {
                                url: '/fornecedores',
                                templateUrl: '~/App/tenant/views/global/fornecedores/index.cshtml',
                                menu: 'Global.Fornecedores.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Fornecedor.Create')) {
                        $stateProvider.state('tenant.global.fornecedores.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/fornecedores/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Fornecedor.Edit')) {
                        $stateProvider.state('tenant.global.fornecedores.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/fornecedores/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Empresa
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Empresa')) {
                        $stateProvider.state('tenant.global.empresa',
                            {
                                url: '/empresa',
                                templateUrl: '~/App/tenant/views/global/empresa/index.cshtml',
                                menu: 'Global.Empresa.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Empresa.Create')) {
                        $stateProvider.state('tenant.global.empresa.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/empresa/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Empresa.Edit')) {
                        $stateProvider.state('tenant.global.empresa.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/empresa/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Pessoa Física
                    if (abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisica')) {
                        $stateProvider.state('tenant.global.pessoaFisica',
                            {
                                url: '/pessoaFisica',
                                templateUrl: '~/App/tenant/views/global/pessoaFisica/index.cshtml',
                                menu: 'Global.PessoaFisica.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisica.Create')) {
                        $stateProvider.state('tenant.global.pessoaFisica.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/pessoaFisica/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisica.Edit')) {
                        $stateProvider.state('tenant.global.pessoaFisica.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/pessoaFisica/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Pessoa Jurídica
                    if (abp.auth.hasPermission('Pages.Tenant.Global.PessoaJuridica')) {
                        $stateProvider.state('tenant.global.pessoaJuridica',
                            {
                                url: '/pessoaJuridica',
                                templateUrl: '~/App/tenant/views/global/pessoaJuridica/index.cshtml',
                                menu: 'Global.PessoaJuridica.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.PessoaJuridica.Create')) {
                        $stateProvider.state('tenant.global.pessoaJuridica.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/pessoaJuridica/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.PessoaJuridica.Edit')) {
                        $stateProvider.state('tenant.global.pessoaJuridica.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/pessoaJuridica/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Arquivo
                    if (abp.auth.hasPermission('Pages.Tenant.Global.Arquivo')) {
                        $stateProvider.state('tenant.global.arquivo',
                            {
                                url: '/arquivo',
                                templateUrl: '~/App/tenant/views/global/arquivo/index.cshtml',
                                menu: 'Global.Arquivo.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Arquivo.Create')) {
                        $stateProvider.state('tenant.global.arquivo.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/arquivo/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Global.Arquivo.Edit')) {
                        $stateProvider.state('tenant.global.arquivo.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/arquivo/edit.cshtml'
                                    }
                                }
                            });
                    }

                    
                }
            ]);
        }
    }
}