﻿export module Routes {
    export class EZLivRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.ezliv', {
                        'abstract': true,
                        url: '/ezliv',
                        template: '<div ui-view class="fade-in-up"></div>'
                    });

                    // Proponente Titular
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ProponenteTitular')) {
                        $stateProvider.state('tenant.ezliv.proponenteTitular', {
                            url: '/proponente',
                            templateUrl: '~/app/tenant/views/ezliv/proponenteTitular/index.cshtml',
                            menu: 'EZLiv.ProponenteTitular.Tenant',
                            ncyBreadcrumb: {
                                label: 'Proponente Titular'
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ProponenteTitular.Create')) {
                        $stateProvider.state('tenant.ezliv.proponenteTitular.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezliv/proponenteTitular/create.cshtml'
                                }
                            },
                            ncyBreadcrumb: {
                                label: 'Proponente Titular'
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.ProponenteTitular.Edit')) {
                        $stateProvider.state('tenant.ezliv.proponenteTitular.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezliv/proponenteTitular/edit.cshtml'
                                }
                            },
                            ncyBreadcrumb: {
                                label: 'Proponente Titular'
                            }
                        });
                    }

                    // Parametro
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Parametro')) {
                        $stateProvider.state('tenant.ezliv.parametro',
                            {
                                url: '/parametro',
                                templateUrl: '~/App/tenant/views/ezliv/parametro/edit.cshtml',
                                menu: 'EZLiv.Parametro.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Parâmetros'
                                }
                            });
                    }

                    // Proposta de Contratação
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PropostaDeContratacao')) {
                        $stateProvider.state('tenant.ezliv.propostaDeContratacao',
                            {
                                url: '/propostaDeContratacao',
                                templateUrl: '~/App/tenant/views/ezliv/propostaDeContratacao/index.cshtml',
                                menu: 'EZLiv.PropostaDeContratacao.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Proposta de Contratação'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PropostaDeContratacao.Create')) {
                        $stateProvider.state('tenant.ezliv.propostaDeContratacao.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/propostaDeContratacao/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Proposta de Contratação'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PropostaDeContratacao.Edit')) {
                        $stateProvider.state('tenant.ezliv.propostaDeContratacao.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/propostaDeContratacao/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Proposta de Contratação'
                                }
                            });
                    }

                    // Produto de Plano de Saúde
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude')) {
                        $stateProvider.state('tenant.ezliv.produtoDePlanoDeSaude',
                            {
                                url: '/produtoDePlanoDeSaude',
                                templateUrl: '~/App/tenant/views/ezliv/produtoDePlanoDeSaude/index.cshtml',
                                menu: 'EZLiv.ProdutoDePlanoDeSaude.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Produtos de Plano de Saúde'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Create')) {
                        $stateProvider.state('tenant.ezliv.produtoDePlanoDeSaude.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/produtoDePlanoDeSaude/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Produto de Plano de Saúde'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Edit')) {
                        $stateProvider.state('tenant.ezliv.produtoDePlanoDeSaude.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/produtoDePlanoDeSaude/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Produto de Plano de Saúde'
                                }
                            });
                    }

                    // Contrato
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Contrato')) {
                        $stateProvider.state('tenant.ezliv.contrato',
                            {
                                url: '/contrato',
                                templateUrl: '~/App/tenant/views/ezliv/contrato/index.cshtml',
                                menu: 'EZLiv.Contrato.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Contratos'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Contrato.Create')) {
                        $stateProvider.state('tenant.ezliv.contrato.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/contrato/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Contrato'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Contrato.Edit')) {
                        $stateProvider.state('tenant.ezliv.contrato.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/contrato/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Contrato'
                                }
                            });
                    }


                    // Operadora
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Operadora')) {
                        $stateProvider.state('tenant.ezliv.operadora',
                            {
                                url: '/operadora',
                                templateUrl: '~/App/tenant/views/ezliv/operadora/index.cshtml',
                                menu: 'EZLiv.Operadora.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Operadoras'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Operadora.Create')) {
                        $stateProvider.state('tenant.ezliv.operadora.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/operadora/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Operadora'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Operadora.Edit')) {
                        $stateProvider.state('tenant.ezliv.operadora.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/operadora/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Operadora'
                                }
                            });
                    }


                    // Corretora
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretora')) {
                        $stateProvider.state('tenant.ezliv.corretora',
                            {
                                url: '/corretora',
                                templateUrl: '~/App/tenant/views/ezliv/corretora/index.cshtml',
                                menu: 'EZLiv.Corretora.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Corretoras'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretora.Create')) {
                        $stateProvider.state('tenant.ezliv.corretora.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/corretora/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Corretora'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretora.Edit')) {
                        $stateProvider.state('tenant.ezliv.corretora.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/corretora/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Corretora'
                                }
                            });
                    }

                    // Corretor
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretor')) {
                        $stateProvider.state('tenant.ezliv.corretor',
                            {
                                url: '/corretor',
                                templateUrl: '~/App/tenant/views/ezliv/corretor/index.cshtml',
                                menu: 'EZLiv.Corretor.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Corretores'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretor.Create')) {
                        $stateProvider.state('tenant.ezliv.corretor.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/corretor/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Corretor'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretor.Edit')) {
                        $stateProvider.state('tenant.ezliv.corretor.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/corretor/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Corretor'
                                }
                            });
                    }

                    // Associacao
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Associacao')) {
                        $stateProvider.state('tenant.ezliv.associacao',
                            {
                                url: '/associacao',
                                templateUrl: '~/App/tenant/views/ezliv/associacao/index.cshtml',
                                menu: 'EZLiv.Associacao.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Entidades'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Associacao.Create')) {
                        $stateProvider.state('tenant.ezliv.associacao.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/associacao/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Entidade'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Associacao.Edit')) {
                        $stateProvider.state('tenant.ezliv.associacao.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/associacao/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Entidade'
                                }
                            });
                    }


                    // Administradora
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Administradora')) {
                        $stateProvider.state('tenant.ezliv.administradora',
                            {
                                url: '/administradora',
                                templateUrl: '~/App/tenant/views/ezliv/administradora/index.cshtml',
                                menu: 'EZLiv.Administradora.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Administradoras'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Administradora.Create')) {
                        $stateProvider.state('tenant.ezliv.administradora.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/administradora/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Administradora'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Administradora.Edit')) {
                        $stateProvider.state('tenant.ezliv.administradora.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/administradora/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Administradora'
                                }
                            });
                    }

                    // ClienteEZLiv
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ClienteEZLiv')) {
                        $stateProvider.state('tenant.ezliv.clienteEZLiv',
                            {
                                url: '/clienteEZLiv',
                                templateUrl: '~/App/tenant/views/ezliv/clienteEZLiv/index.cshtml',
                                menu: 'EZLiv.ClienteEZLiv.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Clientes EZLiv'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ClienteEZLiv.Create')) {
                        $stateProvider.state('tenant.ezliv.clienteEZLiv.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/clienteEZLiv/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Cliente EZLiv'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.ClienteEZLiv.Edit')) {
                        $stateProvider.state('tenant.ezliv.clienteEZLiv.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/clienteEZLiv/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Cliente EZLiv'
                                }
                            });
                    }

                    // Faixa Etária
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.FaixaEtaria')) {
                        $stateProvider.state('tenant.ezliv.faixaEtaria',
                            {
                                url: '/faixaEtaria',
                                templateUrl: '~/App/tenant/views/ezliv/faixaEtaria/index.cshtml',
                                menu: 'EZLiv.FaixaEtaria.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Faixas Etárias'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.FaixaEtaria.Create')) {
                        $stateProvider.state('tenant.ezliv.faixaEtaria.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/faixaEtaria/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Faixa Etária'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.FaixaEtaria.Edit')) {
                        $stateProvider.state('tenant.ezliv.faixaEtaria.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/faixaEtaria/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Faixa Etária'
                                }
                            });
                    }

                    // Rede Credenciada
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.RedeCredenciada')) {
                        $stateProvider.state('tenant.ezliv.redeCredenciada',
                            {
                                url: '/redeCredenciada',
                                templateUrl: '~/App/tenant/views/ezliv/redeCredenciada/index.cshtml',
                                menu: 'EZLiv.RedeCredenciada.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Redes Credenciadas'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.RedeCredenciada.Create')) {
                        $stateProvider.state('tenant.ezliv.redeCredenciada.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/redeCredenciada/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Rede Credenciada'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.RedeCredenciada.Edit')) {
                        $stateProvider.state('tenant.ezliv.redeCredenciada.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/redeCredenciada/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Rede Credenciada'
                                }
                            });
                    }

                    // Beneficiário
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Beneficiario')) {
                        $stateProvider.state('tenant.ezliv.beneficiario',
                            {
                                url: '/beneficiario',
                                templateUrl: '~/App/tenant/views/ezliv/beneficiario/index.cshtml',
                                menu: 'EZLiv.Beneficiario.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Beneficiários'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Beneficiario.Create')) {
                        $stateProvider.state('tenant.ezliv.beneficiario.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/beneficiario/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Beneficiário'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Beneficiario.Edit')) {
                        $stateProvider.state('tenant.ezliv.beneficiario.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/beneficiario/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Beneficiário'
                                }
                            });
                    }

                    // Laboratório
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Laboratorio')) {
                        $stateProvider.state('tenant.ezliv.laboratorio',
                            {
                                url: '/laboratorio',
                                templateUrl: '~/App/tenant/views/ezliv/laboratorio/index.cshtml',
                                menu: 'EZLiv.Laboratorio.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Laboratórios'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Laboratorio.Create')) {
                        $stateProvider.state('tenant.ezliv.laboratorio.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/laboratorio/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Laboratório'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Laboratorio.Edit')) {
                        $stateProvider.state('tenant.ezliv.laboratorio.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/laboratorio/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Laboratório'
                                }
                            });
                    }


                    // Hospital
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Hospital')) {
                        $stateProvider.state('tenant.ezliv.hospital',
                            {
                                url: '/hospital',
                                templateUrl: '~/App/tenant/views/ezliv/hospital/index.cshtml',
                                menu: 'EZLiv.Hospital.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Hospitais'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Hospital.Create')) {
                        $stateProvider.state('tenant.ezliv.hospital.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/hospital/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Hospital'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Hospital.Edit')) {
                        $stateProvider.state('tenant.ezliv.hospital.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/hospital/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Hospital'
                                }
                            });
                    }

                    // Relatorio
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio')) {
                      $stateProvider.state('tenant.ezliv.relatorio',
                        {
                          url: '/relatorio',
                          templateUrl: '~/App/tenant/views/ezliv/relatorio/index.cshtml',
                          menu: 'EZLiv.Relatorio.Tenant',
                          ncyBreadcrumb: {
                            label: 'Relatórios'
                          }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Create')) {
                      $stateProvider.state('tenant.ezliv.relatorio.novo',
                        {
                          url: '/novo',
                          views: {
                            '@tenant': {
                              templateUrl: '~/App/tenant/views/ezliv/relatorio/create.cshtml'
                            }
                          },
                          ncyBreadcrumb: {
                            label: 'Relatório'
                          }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Edit')) {
                      $stateProvider.state('tenant.ezliv.relatorio.alterar',
                        {
                          url: '/alterar/:id',
                          views: {
                            '@tenant': {
                              templateUrl: '~/App/tenant/views/ezliv/relatorio/edit.cshtml'
                            }
                          },
                          ncyBreadcrumb: {
                            label: 'Relatório'
                          }
                        });
                    }

                    // Chancela
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Chancela')) {
                        $stateProvider.state('tenant.ezliv.chancela',
                            {
                                url: '/chancela',
                                templateUrl: '~/App/tenant/views/ezliv/chancela/index.cshtml',
                                menu: 'EZLiv.Chancela.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Chancelas'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Chancela.Create')) {
                        $stateProvider.state('tenant.ezliv.chancela.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/chancela/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Chancela'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Chancela.Edit')) {
                        $stateProvider.state('tenant.ezliv.chancela.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/chancela/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Chancela'
                                }
                            });
                    }

                    // Questionário
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude')) {
                        $stateProvider.state('tenant.ezliv.questionarioDeDeclaracaoDeSaude',
                            {
                                url: '/questionarioDeDeclaracaoDeSaude',
                                templateUrl: '~/App/tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/index.cshtml',
                                menu: 'EZLiv.QuestionarioDeDeclaracaoDeSaude.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Questionários'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Create')) {
                        $stateProvider.state('tenant.ezliv.questionarioDeDeclaracaoDeSaude.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Questionário'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Edit')) {
                        $stateProvider.state('tenant.ezliv.questionarioDeDeclaracaoDeSaude.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Questionário'
                                }
                            });
                    }

                    // Declaração do Beneficiário
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario')) {
                        $stateProvider.state('tenant.ezliv.declaracaoDoBeneficiario',
                            {
                                url: '/declaracaoDoBeneficiario',
                                templateUrl: '~/App/tenant/views/ezliv/declaracaoDoBeneficiario/index.cshtml',
                                menu: 'EZLiv.DeclaracaoDoBeneficiario.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Declarações do Beneficiário'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Create')) {
                        $stateProvider.state('tenant.ezliv.declaracaoDoBeneficiario.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/declaracaoDoBeneficiario/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Declaração do Beneficiário'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Edit')) {
                        $stateProvider.state('tenant.ezliv.declaracaoDoBeneficiario.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/declaracaoDoBeneficiario/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Declaração do Beneficiário'
                                }
                            });
                    }

                    // Índice de Reajuste
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria')) {
                        $stateProvider.state('tenant.ezliv.indiceDeReajustePorFaixaEtaria',
                            {
                                url: '/indiceDeReajustePorFaixaEtaria',
                                templateUrl: '~/App/tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/index.cshtml',
                                menu: 'EZLiv.IndiceDeReajustePorFaixaEtaria.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Índices de Reajuste'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Create')) {
                        $stateProvider.state('tenant.ezliv.indiceDeReajustePorFaixaEtaria.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Índice de Reajuste'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Edit')) {
                        $stateProvider.state('tenant.ezliv.indiceDeReajustePorFaixaEtaria.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Índice de Reajuste'
                                }
                            });
                    }

                    // Meus Produtos
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.MeusProdutos')) {
                        $stateProvider.state('tenant.ezliv.meusProdutos',
                            {
                                url: '/meusProdutos',
                                templateUrl: '~/App/tenant/views/ezliv/meusProdutos/index.cshtml',
                                menu: 'EZLiv.MeusProdutos.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Meus Produtos'
                                }
                            });
                    }

                    // Meus Processos
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.MeusProcessos')) {
                        $stateProvider.state('tenant.ezliv.meusProcessos',
                            {
                                url: '/meusProcessos',
                                templateUrl: '~/App/tenant/views/ezliv/meusProcessos/index.cshtml',
                                menu: 'EZLiv.MeusProcessos.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Meus Processos'
                                }
                            });
                    }

                    // Carencia
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Carencia')) {
                        $stateProvider.state('tenant.ezliv.carencia',
                            {
                                url: '/carencia',
                                templateUrl: '~/App/tenant/views/ezliv/carencia/index.cshtml',
                                menu: 'EZLiv.Carencia.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Carências'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Carencia.Create')) {
                        $stateProvider.state('tenant.ezliv.carencia.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/carencia/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Carência'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Carencia.Edit')) {
                        $stateProvider.state('tenant.ezliv.carencia.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/carencia/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Carência'
                                }
                            });
                    }

                    // PermissaoDeVendaDePlanoDeSaudeParaCorretora
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora')) {
                        $stateProvider.state('tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora',
                            {
                                url: '/permissaoDeVendaDePlanoDeSaudeParaCorretora',
                                templateUrl: '~/App/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/index.cshtml',
                                menu: 'EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Permissões de Venda de Plano de Saúde para Corretora'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Create')) {
                        $stateProvider.state('tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Permissões de Venda de Plano de Saúde para Corretora'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Edit')) {
                        $stateProvider.state('tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Permissões de Venda de Plano de Saúde para Corretora'
                                }
                            });
                    }

                    // PermissaoDeVendaDePlanoDeSaudeParaCorretor
                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor')) {
                        $stateProvider.state('tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor',
                            {
                                url: '/permissaoDeVendaDePlanoDeSaudeParaCorretor',
                                templateUrl: '~/App/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/index.cshtml',
                                menu: 'EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Tenant',
                                ncyBreadcrumb: {
                                    label: 'Permissões de Venda de Plano de Saúde para Corretor'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Create')) {
                        $stateProvider.state('tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/create.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Permissões de Venda de Plano de Saúde para Corretor'
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Edit')) {
                        $stateProvider.state('tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/edit.cshtml'
                                    }
                                },
                                ncyBreadcrumb: {
                                    label: 'Permissões de Venda de Plano de Saúde para Corretor'
                                }
                            });
                    }
                }
            ]);
        }
    }
}