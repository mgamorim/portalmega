﻿export module Routes {
    export class EstoqueRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.estoque', {
                        'abstract': true,
                        url: '/estoque',
                        template: '<div ui-view class="fade-in-up"></div>'
                    });

                    // Parametros
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro')) {
                        $stateProvider.state('tenant.estoque.parametro', {
                            url: '/parametros',
                            templateUrl: '/app/tenant/views/estoque/parametros/padrao/edit.cshtml',
                            menu: 'Estoque.Parametro.Padrao.Tenant'
                        });
                    }

                    // Fracao
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Fracao')) {
                        $stateProvider.state('tenant.estoque.fracao',
                            {
                                url: '/fracao',
                                templateUrl: '/app/tenant/views/estoque/parametros/fracao/index.cshtml',
                                menu: 'Estoque.Parametro.Fracao.Tenant'
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Fracao.Create')) {
                        $stateProvider.state('tenant.estoque.fracao.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/fracao/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Fracao.Edit')) {
                        $stateProvider.state('tenant.estoque.fracao.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/fracao/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Natureza
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Natureza')) {
                        $stateProvider.state('tenant.estoque.natureza',
                            {
                                url: '/natureza',
                                templateUrl: '/app/tenant/views/estoque/parametros/natureza/index.cshtml',
                                menu: 'Estoque.Parametro.Natureza.Tenant'
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Natureza.Create')) {
                        $stateProvider.state('tenant.estoque.natureza.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/natureza/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Natureza.Edit')) {
                        $stateProvider.state('tenant.estoque.natureza.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/natureza/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Unidade de Medida
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida')) {
                        $stateProvider.state('tenant.estoque.unidadeMedida',
                            {
                                url: '/unidadeMedida',
                                templateUrl: '/app/tenant/views/estoque/parametros/unidadeMedida/index.cshtml',
                                menu: 'Estoque.Parametro.UnidadeMedia.Tenant'
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Create')) {
                        $stateProvider.state('tenant.estoque.unidadeMedida.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/unidadeMedida/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Edit')) {
                        $stateProvider.state('tenant.estoque.unidadeMedida.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/unidadeMedida/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Local de Armazenamento
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.LocalArmazenamento')) {
                        $stateProvider.state('tenant.estoque.localArmazenamento',
                            {
                                url: '/localArmazenamento',
                                templateUrl: '/app/tenant/views/estoque/parametros/localArmazenamento/index.cshtml',
                                menu: 'Estoque.Parametro.LocalArmazenamento.Tenant'
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Create')) {
                        $stateProvider.state('tenant.estoque.localArmazenamento.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/localArmazenamento/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Edit')) {
                        $stateProvider.state('tenant.estoque.localArmazenamento.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/parametros/localArmazenamento/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // produto
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Produto')) {
                    $stateProvider.state('tenant.estoque.produto',
                        {
                            url: "/produtos",
                            templateUrl: '/app/tenant/views/estoque/produto/index.cshtml',
                            menu: 'Estoque.Produto.Tenant'
                        });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Produto.Create')) {
                        $stateProvider.state('tenant.estoque.produto.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/produto/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Produto.Edit')) {
                        $stateProvider.state('tenant.estoque.produto.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/produto/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Entrada no Estoque
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Entrada')) {
                        $stateProvider.state('tenant.estoque.entrada',
                            {
                                url: "/entrada",
                                templateUrl: '/app/tenant/views/estoque/entrada/index.cshtml',
                                menu: 'Estoque.Entrada.Tenant'
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Entrada.Create')) {
                        $stateProvider.state('tenant.estoque.entrada.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/entrada/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Entrada.Edit')) {
                        $stateProvider.state('tenant.estoque.entrada.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/entrada/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Saida no Estoque
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Saida')) {
                        $stateProvider.state('tenant.estoque.saida',
                            {
                                url: "/saida",
                                templateUrl: '/app/tenant/views/estoque/saida/index.cshtml',
                                menu: 'Estoque.Saida.Tenant'
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Saida.Create')) {
                        $stateProvider.state('tenant.estoque.saida.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/saida/create.cshtml'
                                    }
                                }
                            });
                    }
                    if (abp.auth.hasPermission('Pages.Tenant.Estoque.Saida.Edit')) {
                        $stateProvider.state('tenant.estoque.saida.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/estoque/saida/edit.cshtml'
                                    }
                                }
                            });
                    }
                }
            ]);
        }
    }
}