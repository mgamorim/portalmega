﻿export module Routes {
    export class EZMedicalRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.ezmedical', {
                        'abstract': true,
                        url: '/ezmedical',
                        template: '<div ui-view class="fade-in-up">AQUiii</div>'
                    });


                    // Evento
                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento')) {
                        $stateProvider.state('tenant.ezmedical.evento', {
                            url: '/eventos',
                            templateUrl: '~/app/tenant/views/ezmedical/evento/index.cshtml',
                            menu: 'EZMedical.Evento.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Create')) {
                        $stateProvider.state('tenant.ezmedical.evento.novo', {
                            url: '/novo/:start/:end',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/evento/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Edit')) {
                        $stateProvider.state('tenant.ezmedical.evento.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/evento/edit.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Delete')) {
                        $stateProvider.state('tenant.ezmedical.evento.excluir', {
                            url: '/excluir/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/evento/delete.cshtml'
                                }
                            }
                        });
                    }

                    // Especialidade
                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Especialidade')) {
                        $stateProvider.state('tenant.ezmedical.especialidade', {
                            url: '/especialidade',
                            templateUrl: '~/app/tenant/views/ezmedical/especialidade/index.cshtml',
                            menu: 'EZMedical.Especialidade.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Especialidade.Create')) {
                        $stateProvider.state('tenant.ezmedical.especialidade.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/especialidade/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Especialidade.Edit')) {
                        $stateProvider.state('tenant.ezmedical.especialidade.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/especialidade/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Médico
                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Medico')) {
                        $stateProvider.state('tenant.ezmedical.medico', {
                            url: '/medico',
                            templateUrl: '~/app/tenant/views/ezmedical/medico/index.cshtml',
                            menu: 'EZMedical.Medico.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Medico.Create')) {
                        $stateProvider.state('tenant.ezmedical.medico.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/medico/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Medico.Edit')) {
                        $stateProvider.state('tenant.ezmedical.medico.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/medico/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Regras
                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.RegraConsulta')) {
                        $stateProvider.state('tenant.ezmedical.regras', {
                            url: '/regras',
                            templateUrl: '~/app/tenant/views/ezmedical/regraConsulta/index.cshtml',
                            menu: 'EZMedical.Parametros.Regras.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.RegraConsulta.Create')) {
                        $stateProvider.state('tenant.ezmedical.regras.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/regraConsulta/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.RegraConsulta.Edit')) {
                        $stateProvider.state('tenant.ezmedical.regras.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/regraConsulta/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Disponibilidade
                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Disponibilidade')) {
                        $stateProvider.state('tenant.ezmedical.disponibilidade', {
                            url: '/disponibilidade',
                            templateUrl: '~/app/tenant/views/ezmedical/disponibilidadeConsulta/index.cshtml',
                            menu: 'EZMedical.Disponibilidade.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Disponibilidade.Create')) {
                        $stateProvider.state('tenant.ezmedical.disponibilidade.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/disponibilidadeConsulta/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Disponibilidade.Edit')) {
                        $stateProvider.state('tenant.ezmedical.disponibilidade.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/disponibilidadeConsulta/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Bloqueio
                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Bloqueio')) {
                        $stateProvider.state('tenant.ezmedical.bloqueio', {
                            url: '/bloqueio',
                            templateUrl: '~/app/tenant/views/ezmedical/bloqueioConsulta/index.cshtml',
                            menu: 'EZMedical.Bloqueio.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Bloqueio.Create')) {
                        $stateProvider.state('tenant.ezmedical.bloqueio.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/bloqueioConsulta/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZMedical.Bloqueio.Edit')) {
                        $stateProvider.state('tenant.ezmedical.bloqueio.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/ezmedical/bloqueioConsulta/edit.cshtml'
                                }
                            }
                        });
                    }
               }
            ]);
        }
    }
}