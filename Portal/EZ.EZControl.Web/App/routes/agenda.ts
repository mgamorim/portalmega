﻿export module Routes {
    export class AgendaRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.agenda', {
                        'abstract': true,
                        url: '/agenda',
                        template: '<div ui-view class="fade-in-up">AQUiii</div>'
                    });

                    // Parametros
                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Padroes')) {
                        $stateProvider.state('tenant.agenda.parametros', {
                            url: '/parametros',
                            templateUrl: '~/App/tenant/views/agenda/parametros/padroes/edit.cshtml',
                            menu: 'Agenda.Parametros.Tenant'
                        });
                    }


                    // Parametros Regras
                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Regras')) {
                        $stateProvider.state('tenant.agenda.regras', {
                            url: '/regras',
                            templateUrl: '~/app/tenant/views/agenda/parametros/regras/index.cshtml',
                            menu: 'Agenda.Parametros.Regras.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Regras.Create')) {
                        $stateProvider.state('tenant.agenda.regras.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/parametros/regras/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Regras.Edit')) {
                        $stateProvider.state('tenant.agenda.regras.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/parametros/regras/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Disponibilidade
                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Disponibilidade')) {
                        $stateProvider.state('tenant.agenda.disponibilidade', {
                            url: '/disponibilidade',
                            templateUrl: '~/app/tenant/views/agenda/disponibilidade/index.cshtml',
                            menu: 'Agenda.Disponibilidade.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Disponibilidade.Create')) {
                        $stateProvider.state('tenant.agenda.disponibilidade.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/disponibilidade/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Disponibilidade.Edit')) {
                        $stateProvider.state('tenant.agenda.disponibilidade.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/disponibilidade/edit.cshtml'
                                }
                            }
                        });
                    }

                    $stateProvider.state('tenant.agenda.testes', {
                        url: '/testes',
                        templateUrl: '~/app/tenant/views/agenda/testes/index.cshtml',
                        menu: 'Agenda.Testes.Tenant'
                    });

                    // Bloqueio
                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Bloqueio')) {
                        $stateProvider.state('tenant.agenda.bloqueio', {
                            url: '/bloqueio',
                            templateUrl: '~/app/tenant/views/agenda/bloqueio/index.cshtml',
                            menu: 'Agenda.Bloqueio.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Bloqueio.Create')) {
                        $stateProvider.state('tenant.agenda.bloqueio.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/bloqueio/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Bloqueio.Edit')) {
                        $stateProvider.state('tenant.agenda.bloqueio.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/bloqueio/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Evento
                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Evento')) {
                        $stateProvider.state('tenant.agenda.evento', {
                            url: '/evento',
                            templateUrl: '~/app/tenant/views/agenda/evento/index.cshtml',
                            menu: 'Agenda.Evento.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Evento.Create')) {
                        $stateProvider.state('tenant.agenda.evento.novo', {
                            url: '/novo/:start/:end',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/evento/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Agenda.Evento.Edit')) {
                        $stateProvider.state('tenant.agenda.evento.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/agenda/evento/edit.cshtml'
                                }
                            }
                        });
                    }
               }
            ]);
        }
    }
}