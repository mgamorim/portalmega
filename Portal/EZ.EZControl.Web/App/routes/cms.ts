﻿export module Routes {
    export class CmsRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {

                    // Parametro
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Parametros')) {
                        $stateProvider.state('tenant.cms.parametros', {
                            url: '/parametros',
                            templateUrl: '~/App/tenant/views/cms/parametros/edit.cshtml',
                            menu: 'CMS.Parametros.Tenant'
                        });
                    }

                    // Configurar Site
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.ConfigurarSite')) {
                        $stateProvider.state('tenant.cms.configurarsite', {
                            url: '/configurarSite',
                            templateUrl: '~/App/tenant/views/cms/configurarSite/edit.cshtml',
                            menu: 'CMS.ConfigurarSite.Tenant'
                        });
                    }

                    // Categoria
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Categoria')) {
                        $stateProvider.state('tenant.cms.categoria', {
                            url: '/categoria',
                            templateUrl: '~/App/tenant/views/cms/categoria/index.cshtml',
                            menu: 'CMS.Categoria.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Categoria.Create')) {
                        $stateProvider.state('tenant.cms.categoria.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/categoria/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Categoria.Edit')) {
                        $stateProvider.state('tenant.cms.categoria.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/categoria/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Site
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Site')) {
                        $stateProvider.state('tenant.cms.site', {
                            url: '/site',
                            templateUrl: '~/App/tenant/views/cms/site/index.cshtml',
                            menu: 'CMS.Site.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Site.Create')) {
                        $stateProvider.state('tenant.cms.site.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/site/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Site.Edit')) {
                        $stateProvider.state('tenant.cms.site.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/site/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Arquivo
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Arquivo')) {
                        $stateProvider.state('tenant.cms.arquivo', {
                            url: '/arquivo',
                            templateUrl: '~/App/tenant/views/cms/arquivo/index.cshtml',
                            menu: 'CMS.Arquivo.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Arquivo.Create')) {
                        $stateProvider.state('tenant.cms.arquivo.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/arquivo/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Arquivo.Edit')) {
                        $stateProvider.state('tenant.cms.arquivo.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/arquivo/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Template
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Template')) {
                        $stateProvider.state('tenant.cms.template', {
                            url: '/template',
                            templateUrl: '~/App/tenant/views/cms/template/index.cshtml',
                            menu: 'CMS.Template.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Template.Synchronize')) {
                        $stateProvider.state('tenant.cms.template.sincronizar', {
                            url: '/template',
                            templateUrl: '~/App/tenant/views/cms/template/index.cshtml',
                            menu: 'CMS.Template.Tenant'
                        });
                    }

                    // Widget
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Widget')) {
                        $stateProvider.state('tenant.cms.widget', {
                            url: '/widget',
                            templateUrl: '~/App/tenant/views/cms/widget/index.cshtml',
                            menu: 'CMS.Widget.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Widget.Create')) {
                        $stateProvider.state('tenant.cms.widget.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/widget/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Widget.Edit')) {
                        $stateProvider.state('tenant.cms.widget.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/widget/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Post
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Post')) {
                        $stateProvider.state('tenant.cms.post', {
                            url: '/post',
                            templateUrl: '~/App/tenant/views/cms/post/index.cshtml',
                            menu: 'CMS.Post.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Post.Create')) {
                        $stateProvider.state('tenant.cms.post.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/post/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Post.Edit')) {
                        $stateProvider.state('tenant.cms.post.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/post/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Pagina
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Pagina')) {
                        $stateProvider.state('tenant.cms.pagina', {
                            url: '/pagina',
                            templateUrl: '~/App/tenant/views/cms/pagina/index.cshtml',
                            menu: 'CMS.Pagina.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Pagina.Create')) {
                        $stateProvider.state('tenant.cms.pagina.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/pagina/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.Pagina.Edit')) {
                        $stateProvider.state('tenant.cms.pagina.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/pagina/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Item de Menu
                    if (abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu')) {
                        $stateProvider.state('tenant.cms.itemDeMenu', {
                            url: '/itemDeMenu',
                            templateUrl: '~/App/tenant/views/cms/itemDeMenu/index.cshtml',
                            menu: 'CMS.ItemDeMenu.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Create')) {
                        $stateProvider.state('tenant.cms.itemDeMenu.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/itemDeMenu/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Edit')) {
                        $stateProvider.state('tenant.cms.itemDeMenu.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/cms/itemDeMenu/edit.cshtml'
                                }
                            }
                        });
                    }
                }
            ]);
        }
    }
}