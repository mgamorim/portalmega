﻿export module Routes {
    export class AdministrationRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {

                    // Cliente EZ
                    if (abp.auth.hasPermission('Pages.Administration.Tenant.ClienteEZ')) {
                        $stateProvider.state('tenant.clienteez', {
                            url: '/clienteez',
                            templateUrl: '~/App/tenant/views/global/clienteEz/index.cshtml'
                        });
                    }

                    // Usuário por Pessoa
                    if (abp.auth.hasPermission('Pages.Administration.Tenant.UsuarioPorPessoa')) {
                        $stateProvider.state('tenant.usuarioPorPessoa',
                            {
                                url: '/usuarioPorPessoa',
                                templateUrl: '~/App/tenant/views/global/usuarioPorPessoa/index.cshtml',
                                menu: 'Global.UsuarioPorPessoa.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Administration.Tenant.UsuarioPorPessoa.Create')) {
                        $stateProvider.state('tenant.usuarioPorPessoa.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/usuarioPorPessoa/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Administration.Tenant.UsuarioPorPessoa.Edit')) {
                        $stateProvider.state('tenant.usuarioPorPessoa.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/App/tenant/views/global/usuarioPorPessoa/edit.cshtml'
                                    }
                                }
                            });
                    }
                }
            ]);
        }
    }
}