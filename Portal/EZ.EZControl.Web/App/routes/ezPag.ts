﻿export module Routes {
    export class EZPagRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.ezpag', {
                        'abstract': true,
                        url: '/ezpag',
                        template: '<div ui-view class="fade-in-up">AQUiii</div>'
                    });

                    // Testes
                    $stateProvider.state('tenant.ezpag.testes', {
                        url: '/testes',
                        templateUrl: '~/app/tenant/views/ezpag/testes/index.cshtml',
                        menu: 'EZPag.Testes.Tenant'
                    });

                    // Forma de Pagamento
                    if (abp.auth.hasPermission('Pages.Tenant.EZPag.FormaDePagamento')) {
                        $stateProvider.state('tenant.ezpag.formaDePagamento',
                            {
                                url: '/formaDePagamento',
                                templateUrl: '~/app/tenant/views/ezpag/formaDePagamento/index.cshtml',
                                menu: 'EzPag.FormaDePagamento.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZPag.FormaDePagamento.Create')) {
                        $stateProvider.state('tenant.ezpag.formaDePagamento.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/app/tenant/views/ezpag/formaDePagamento/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.EZPag.FormaDePagamento.Edit')) {
                        $stateProvider.state('tenant.ezpag.formaDePagamento.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '~/app/tenant/views/ezpag/formaDePagamento/edit.cshtml'
                                    }
                                }
                            });
                    }

                    // Parametro
                    if (abp.auth.hasPermission('Pages.Tenant.EZPag.Parametro')) {
                        $stateProvider.state('tenant.ezpag.parametro',
                            {
                                url: '/parametro',
                                templateUrl: '~/app/tenant/views/ezpag/parametro/edit.cshtml',
                                menu: 'EZPag.Parametro.Tenant'
                            });
                    }

                    //Transacao
                    if (abp.auth.hasPermission('Pages.Tenant.EZPag.Transacao')) {
                        $stateProvider.state('tenant.ezpag.transacao', {
                            url: '/transacao',
                            templateUrl: '~/app/tenant/views/ezpag/transacao/index.cshtml',
                            menu: 'EZPag.Transacao.Tenant'
                        });
                    }
               }
            ]);
        }
    }
}