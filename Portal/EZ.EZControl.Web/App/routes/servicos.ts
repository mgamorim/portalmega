﻿export module Routes {
    export class ServicosRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.servicos', {
                        'abstract': true,
                        url: '/servicos',
                        template: '<div ui-view class="fade-in-up"></div>'
                    });

                    //// Parametros
                    //if (abp.auth.hasPermission('Pages.Tenant.Servicos.Parametros.Padroes')) {
                    //    $stateProvider.state('tenant.servicos.parametros', {
                    //        url: '/parametros',
                    //        templateUrl: '~/App/tenant/views/servicos/parametros/padroes/edit.cshtml',
                    //        menu: 'Servicos.Parametros.Tenant'
                    //    });
                    //}

                    // servico
                    if (abp.auth.hasPermission('Pages.Tenant.Servicos.Servico')) {
                        $stateProvider.state('tenant.servicos.servico',
                            {
                                url: "/servico",
                                templateUrl: '/app/tenant/views/servicos/servico/index.cshtml',
                                menu: 'Servicos.Servico.Tenant'
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Servicos.Servico.Create')) {
                        $stateProvider.state('tenant.servicos.servico.novo',
                            {
                                url: '/novo',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/servicos/servico/create.cshtml'
                                    }
                                }
                            });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Servicos.Servico.Edit')) {
                        $stateProvider.state('tenant.servicos.servico.alterar',
                            {
                                url: '/alterar/:id',
                                views: {
                                    '@tenant': {
                                        templateUrl: '/app/tenant/views/servicos/servico/edit.cshtml'
                                    }
                                }
                            });
                    }
               }
            ]);
        }
    }
}