﻿export module Routes {
    export class FinanceiroRoutes {
        static defineRoutes() {
            appModule.config([
                "$stateProvider", "$urlRouterProvider", ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $stateProvider.state('tenant.financeiro', {
                        'abstract': true,
                        url: '/financeiro',
                        template: '<div ui-view class="fade-in-up"></div>'
                    });

                    // Tipo de Documento
                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.TipoDeDocumento')) {
                        $stateProvider.state('tenant.financeiro.tipoDeDocumento', {
                            url: '/tipoDeDocumento',
                            templateUrl: '~/app/tenant/views/financeiro/tipoDeDocumento/index.cshtml',
                            menu: 'Financeiro.TipoDeDocumento.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.TipoDeDocumento.Create')) {
                        $stateProvider.state('tenant.financeiro.tipoDeDocumento.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/tipoDeDocumento/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.TipoDeDocumento.Edit')) {
                        $stateProvider.state('tenant.financeiro.tipoDeDocumento.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/tipoDeDocumento/edit.cshtml'
                                }
                            }
                        });
                    }

                    // Documento A Pagar
                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar')) {
                        $stateProvider.state('tenant.financeiro.documentoAPagar', {
                            url: '/documentoAPagar',
                            templateUrl: '~/app/tenant/views/financeiro/documentoAPagar/index.cshtml',
                            menu: 'Financeiro.DocumentoAPagar.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Create')) {
                        $stateProvider.state('tenant.financeiro.documentoAPagar.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/documentoAPagar/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Edit')) {
                        $stateProvider.state('tenant.financeiro.documentoAPagar.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/documentoAPagar/edit.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Estornar')) {
                        $stateProvider.state('tenant.financeiro.documentoAPagar.estornar', {
                            url: '/estornar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/documentoAPagar/estornar.cshtml'
                                }
                            }
                        });
                    }

                    // Documento A Receber
                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber')) {
                        $stateProvider.state('tenant.financeiro.documentoAReceber', {
                            url: '/documentoAReceber',
                            templateUrl: '~/app/tenant/views/financeiro/documentoAReceber/index.cshtml',
                            menu: 'Financeiro.DocumentoAReceber.Tenant'
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Create')) {
                        $stateProvider.state('tenant.financeiro.documentoAReceber.novo', {
                            url: '/novo',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/documentoAReceber/create.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Edit')) {
                        $stateProvider.state('tenant.financeiro.documentoAReceber.alterar', {
                            url: '/alterar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/documentoAReceber/edit.cshtml'
                                }
                            }
                        });
                    }

                    if (abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Estornar')) {
                        $stateProvider.state('tenant.financeiro.documentoAReceber.estornar', {
                            url: '/estornar/:id',
                            views: {
                                '@tenant': {
                                    templateUrl: '~/App/tenant/views/financeiro/documentoAReceber/estornar.cshtml'
                                }
                            }
                        });
                    }
               }
            ]);
        }
    }
}