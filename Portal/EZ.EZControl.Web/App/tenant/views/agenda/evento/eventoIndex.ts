﻿import Scheduler = require("../../../../common/ezScheduler/ezScheduler");
import * as eventoDtos from "../../../../dtos/agenda/evento/eventoDtos";

export module Agenda.Evento{

    class EventoIndexController {
        public ezScheduler: Scheduler.EzScheduler.EzScheduler;
        static $inject = ['$state'];

        constructor(public $state: ng.ui.IStateService) {
            this.ezScheduler = new Scheduler.EzScheduler.EzScheduler();
            this.ezScheduler.sistema = SistemaEnum.Agenda;

            this.ezScheduler.onSchedulerRendered = (scheduler: kendo.ui.Scheduler) => {
                this.ezScheduler.dataSource.options.transport.read.url = '/api/services/app/agenda/eventoBase/read';
                this.ezScheduler.dataSource.options.transport.read.data = () => {
                    var startDate = this.ezScheduler.scheduler.view().startDate();
                    var endDate = this.ezScheduler.scheduler.view().endDate();
                    var input = new eventoDtos.Dtos.Agenda.Evento.GetEventoInput();
                    input.inicio = startDate;
                    input.termino = endDate;
                    input.tipo = TipoDeEventoEnum.Tarefa;
                    input.sistema = SistemaEnum.Agenda;
                    return input;
                }

                this.ezScheduler.options.edit = (e) => {
                    e.preventDefault();
                    if (e.event.id == 0) {
                        this.$state.go('tenant.agenda.evento.novo', { 'start': e.event.start, 'end': e.event.end });
                    } else {
                        this.$state.go('tenant.agenda.evento.alterar', { "id": e.event.id });
                    }
                };

                this.ezScheduler.dataSource.options.transport.create.url = '/api/services/app/agenda/eventoBase/create';
                this.ezScheduler.dataSource.options.transport.update.url = '/api/services/app/agenda/eventoBase/update';
                this.ezScheduler.dataSource.options.transport.destroy.url = '/api/services/app/agenda/eventoBase/destroy';
                this.ezScheduler.scheduler.setOptions(this.ezScheduler.options);
                this.ezScheduler.scheduler.setDataSource(this.ezScheduler.dataSource);
            };
        }

        $onInit() {
        }
    }

    export class EventoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/evento/eventoIndex.cshtml';
            this.controller = EventoIndexController;
        }
    }
}