﻿import * as eventoService from "../../../../services/domain/agenda/evento/eventoService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as disponibilidadeService from "../../../../services/domain/agenda/disponibilidade/disponibilidadeService";
import * as eventoDtos from "../../../../dtos/agenda/evento/eventoDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as disponibilidadeDtos from "../../../../dtos/agenda/disponibilidade/disponibilidadeDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class EventoFormController extends controllers.Controllers.ControllerFormComponentBase<eventoDtos.Dtos.Agenda.Evento.EventoListDto, eventoDtos.Dtos.Agenda.Evento.EventoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoEvento: (termoDigitadoPesquisa: string) => any;
        private eventoSelecionado: (registro: eventoDtos.Dtos.Agenda.Evento.EventoListDto) => string;
        private eventoDeselecionado: () => void;
        private requestParamsEvento: requestParam.RequestParam.RequestParams;

        public ezSelectEventoPai: ezSelect.EzSelect.EzSelect<eventoDtos.Dtos.Agenda.Evento.EventoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaEventoPai: (termoDigitadoPesquisa: string) => any;
        private eventoPaiSelecionado: (registro: eventoDtos.Dtos.Agenda.Evento.EventoListDto) => string;
        private eventoPaiDeselecionado: () => void;
        private requestParamsEventoPai: requestParam.RequestParam.RequestParams;

        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;

        private sistemaEnum = ez.domain.enum.sistemaEnum.valores;
        private statusDoEventoEnum = ez.domain.enum.statusDoEventoEnum.valores;
        private tipoDeEventoEnum :any;

        public participantesOld = [];
        public participantes = [];
        public requestParamsParticipante: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'eventoService', 'pessoaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public eventoService: eventoService.Services.EventoService,
            public pessoaService: pessoaService.Services.PessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                eventoService,
                'tenant.agenda.evento');
        }

        $onInit() {
            super.init();
            this.paramTokenize();

            this.tipoDeEventoEnum =  angular.copy(ez.domain.enum.tipoDeEventoEnum.valores, this.tipoDeEventoEnum);
            this.tipoDeEventoEnum.splice(1, 1);

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new eventoDtos.Dtos.Agenda.Evento.EventoInput();
                instance.id = 0;
                instance.participantes = [];
                if (this.$stateParams['start']) {
                    instance.inicio = new Date(this.$stateParams['start']);
                }
                if (this.$stateParams['end']) {
                    instance.termino = new Date(this.$stateParams['end']);
                }

                this.getParticipantes();
                return instance;
            };

            this.events.onGetEntity = (entity) => {

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.inicio = this.entity.inicio != null ? new Date(this.entity.inicio.toString()) : null;
                this.entity.termino = this.entity.termino != null ? new Date(this.entity.termino.toString()) : null;

                if (entity.eventoPaiId) {
                    this.ezSelectEventoPai.loading = true;
                    var promiseEvento = this.eventoService.getById(entity.eventoPaiId);
                    promiseEvento.then(result => {
                        this.ezSelectEventoPai.setInputText(result.titulo);
                    }).finally(() => {
                        this.ezSelectEventoPai.loading = false;
                    });
                }

                this.ezSelectPessoa.loading = true;
                var promisePessoa = this.pessoaService.getById(entity.ownerId);
                promisePessoa.then(result => {
                    this.ezSelectPessoa.setInputText(result.nomePessoa);
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                    });

                this.formataParticipantes();
            };

            this.events.onBeforeSaveEntity = () => {
                this.ajustaParticipantes();
            }

            this.ezSelectEventoPaiConfig();
            this.ezSelectPessoaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectEventoPaiConfig() {
            this.requestParamsEventoPai = new requestParam.RequestParam.RequestParams();

            this.eventoPaiSelecionado = (registro) => {
                this.entity.eventoPaiId = registro.id;
                return registro.titulo;
            }

            this.eventoPaiDeselecionado = () => {
                this.entity.eventoPaiId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaEventoPai = (termoDigitadoPesquisa) => {
                var filtro = new eventoDtos.Dtos.Agenda.Evento.GetEventoExceptForInput();
                filtro.id = this.entity.id;
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEventoPai = new ezSelect.EzSelect.EzSelect<eventoDtos.Dtos.Agenda.Evento.EventoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.eventoService,
                this.getFiltroParaEventoPai,
                this.requestParamsEventoPai,
                this.$uibModal,
                this.eventoPaiSelecionado,
                this.eventoPaiDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEventoPai.onEzGridCreated = () => {
                this.ezSelectEventoPai.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.eventoService.getEventoPaiExceptForId(filtro, requestParams);
                };

                this.ezSelectEventoPai.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Evento.Titulo'),
                    field: 'titulo'
                });

                this.ezSelectEventoPai.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Evento.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectEventoPai.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectEventoPai.on.beforeOpenModalDialog = () => {
                this.ezSelectEventoPai.ezGrid.getRegistros();
            };
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();

            this.pessoaSelecionado = (registro) => {
                this.entity.ownerId = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.entity.ownerId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPessoa = (termoDigitadoPesquisa) => {
                var filtro = new eventoDtos.Dtos.Agenda.Evento.GetEventoInput();
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pessoa.Nome'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        paramTokenize = () => {
            $('#participantesList').tokenize({
                searchMinLength: 3,
                valueField: "id",
                textField: "nomePessoa"
            });
        }

        formataParticipantes = () => {
            if (this.entity.participantes) {
                this.participantesOld = this.entity.participantes;
                var ids = new Array();
                this.participantesOld.forEach((item) => {
                    var id = item.id.toString();
                    $('#participantesList').tokenize().tokenAdd(id, item.nomePessoa);
                    ids[id] = id;
                });

                $('#participantesList').tokenize().remap();

                this.requestParamsParticipante = new requestParam.RequestParam.RequestParams();
                var promise = this.pessoaService.getPaged(new pessoaDtos.Dtos.Pessoa.GetPessoaInput(), this.requestParamsParticipante);

                promise.then((result) => {
                    if (result.items.length > 0) {
                        for (var i = 0; i < result.items.length; i++) {
                            var out = result.items[i];

                            if (!ids[out.id]) {
                                this.participantes.push({
                                    id: out.id,
                                    nome: out.nomePessoa
                                });
                            }
                        }
                    }
                });
            }
        }

        getParticipantes = () => {
            this.requestParamsParticipante = new requestParam.RequestParam.RequestParams();
            var promise = this.pessoaService.getPaged(new pessoaDtos.Dtos.Pessoa.GetPessoaInput(), this.requestParamsParticipante);
            promise.then((result) => {
                if (result.items.length > 0) {
                    for (var i = 0; i < result.items.length; i++) {
                        this.participantes.push({
                            id: result.items[i].id,
                            nomePessoa: result.items[i].nomePessoa
                        });
                    }
                }
            });
        };

        ajustaParticipantes = () => {
            this.entity.idsParticipantes = [];
            var itens = $('#participantesList').next('div').find('.TokensContainer li.Token').toArray();
            itens.forEach((item) => {
                var id = isNaN($(item).data('value')) ? ($(item).data('value').indexOf(':') > -1 ? parseInt($(item).data('value').toString().split(':')[1]) : 0) : parseInt($(item).data('value'), 0);
                this.entity.idsParticipantes.push(id);
            });
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EventoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/evento/eventoForm.cshtml';
            this.controller = EventoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}