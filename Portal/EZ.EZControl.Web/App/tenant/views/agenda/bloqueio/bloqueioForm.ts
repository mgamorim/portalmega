﻿import * as bloqueioService from "../../../../services/domain/agenda/bloqueio/bloqueioService";
import * as configuracaoDeBloqueioService from "../../../../services/domain/agenda/configuracaoDeBloqueio/configuracaoDeBloqueioService";
import * as configuracaoDeBloqueioDtos from "../../../../dtos/agenda/configuracaoDeBloqueio/configuracaoDeBloqueioDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class BloqueioFormController extends controllers.Controllers.ControllerFormComponentBase<configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioListDto, configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBloqueio: (termoDigitadoPesquisa: string) => any;
        private bloqueioSelecionado: (registro: configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioListDto) => string;
        private bloqueioDeselecionado: () => void;
        private requestParamsBloqueio: requestParam.RequestParam.RequestParams;
        private sistemaEnum: any;
        private motivoDoBloqueioEnum: any;
        private tipoDeEventoEnum: any;
        private horarioInicioModel: Date;
        private horarioFimModel: Date;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'configuracaoDeBloqueioService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeBloqueioService: configuracaoDeBloqueioService.Services.ConfiguracaoDeBloqueioService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                configuracaoDeBloqueioService,
                'tenant.agenda.bloqueio');
        }

        $onInit() {
            super.init();

            this.sistemaEnum = ez.domain.enum.sistemaEnum.valores;
            this.motivoDoBloqueioEnum = ez.domain.enum.motivoDoBloqueioEnum.valores;
            this.tipoDeEventoEnum = ez.domain.enum.tipoDeEventoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            this.events.onBeforeSaveEntity = () => {
                var horarioInicio = this.horarioInicioModel;
                var horaInicio = (horarioInicio.getHours() < 10) ? "0" + horarioInicio.getHours().toString() : horarioInicio.getHours().toString();
                var minutoInicio = (horarioInicio.getMinutes() < 10) ? "0" + horarioInicio.getMinutes().toString() : horarioInicio.getMinutes().toString();
                this.entity.horarioInicio = horaInicio + ":" + minutoInicio;
                
                var horarioFim = this.horarioFimModel;
                var horaFim = (horarioFim.getHours() < 10) ? "0" + horarioFim.getHours().toString() : horarioFim.getHours().toString();
                var minutoFim = (horarioFim.getMinutes() < 10) ? "0" + horarioFim.getMinutes().toString() : horarioFim.getMinutes().toString();
                this.entity.horarioFim = horaFim + ":" + minutoFim;
            }

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.inicioBloqueio = this.entity.inicioBloqueio != null ? new Date(this.entity.inicioBloqueio.toString()) : null;
                this.entity.fimBloqueio = this.entity.fimBloqueio != null ? new Date(this.entity.fimBloqueio.toString()) : null;

                var horaInicio = 0;
                var minutoInicio = 0;
                var horaFim = 0;
                var minutoFim = 0;

                var partesHorarioInicio = this.entity.horarioInicio.split(':');
                var partesHorarioFim = this.entity.horarioFim.split(':');

                horaInicio = parseInt(partesHorarioInicio[0], 0);
                minutoInicio = parseInt(partesHorarioInicio[1], 0);

                horaFim = parseInt(partesHorarioFim[0], 0);
                minutoFim = parseInt(partesHorarioFim[1], 0);

                this.horarioInicioModel = new Date(2016, 1, 1, horaInicio, minutoInicio);
                this.horarioFimModel = new Date(2016, 1, 1, horaFim, minutoFim);
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class BloqueioFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/bloqueio/bloqueioForm.cshtml';
            this.controller = BloqueioFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}