﻿import * as ezGrid from "../../../../../common/ezGrid/ezGrid";
import * as parametrosRegraBaseService from "../../../../../services/domain/agenda/parametros/parametrosRegraBaseAgendaService";
import * as parametrosRegrasDtos from "../../../../../dtos/agenda/parametros/parametrosRegrasAgendaDtos";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../../common/requestParams/requestParams";

export module Agenda.Parametros.Regras {

    class ParametrosRegrasIndexController extends controllers.Controllers.ControllerIndexBase<parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseListDto, parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseInput>{

        static $inject = ['$state', 'uiGridConstants', 'parametrosRegrasAgendaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public parametrosRegraBaseService: parametrosRegraBaseService.Services.RegraBaseService) {

            super(
                $state,
                uiGridConstants,
                parametrosRegraBaseService, {
                    rotaAlterarRegistro: 'tenant.agenda.regras.alterar',
                    rotaNovoRegistro: 'tenant.agenda.regras.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.GetRegraBaseInput();
                filtro.sistema = termoDigitadoPesquisa;
                filtro.tipoDeEvento = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.parametrosRegraBaseService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('Agenda.Parametros.CreatingNewRegra', this.novo, abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Regras.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Regras.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Agenda.Parametros.Regras.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agenda.Parametros.Regras.Sistema'),
                field: 'sistema',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agenda.Parametros.Regras.TipoDeEvento'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeEventoEnum", "tipoDeEvento")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agenda.Parametros.Regras.IsActive'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class ParametrosRegrasAgendaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/parametros/regras/parametrosRegrasAgendaIndex.cshtml';
            this.controller = ParametrosRegrasIndexController;
        }
    }
}