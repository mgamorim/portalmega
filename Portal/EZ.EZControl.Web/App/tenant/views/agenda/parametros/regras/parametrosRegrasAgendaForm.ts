﻿import * as parametrosRegrasService from "../../../../../services/domain/agenda/parametros/parametrosRegraBaseAgendaService";
import * as parametrosRegrasDtos from "../../../../../dtos/agenda/parametros/parametrosRegrasAgendaDtos";
import * as requestParam from "../../../../../common/requestParams/requestParams";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ParametrosRegrasFormController extends controllers.Controllers.ControllerFormComponentBase<parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseListDto, parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoParametrosRegras: (termoDigitadoPesquisa: string) => any;
        private parametrosRegrasSelecionado: (registro: parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseListDto) => string;
        private parametrosRegrasDeselecionado: () => void;
        private requestParamsParametrosRegras: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'parametrosRegrasAgendaService', '$uibModal'];

        public sistema: any; 
        public tipoDeEvento: any; 
        public sistemaOnChange: (sistemaEnum: number) => void;
        private setTipoDeEvento: (sistemaEnum: number) => void;
        private setSistema: () => void;

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public parametrosRegrasService: parametrosRegrasService.Services.RegraBaseService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                parametrosRegrasService,
                'tenant.agenda.regras');

            this.setTipoDeEvento = (sistemaEnum) => {

                this.loadingEntity = true;

                var promise = parametrosRegrasService.getTiposDeEventosBySistema(sistemaEnum);

                promise.then(result => {

                    if (result) {
                        this.tipoDeEvento = ez.domain.enum.tipoDeEventoEnum.valores
                            .filter((item) => {
                                return result.tiposDeEventos.indexOf(item.valor) >= 0;
                            });
                    }

                }).finally(() => {
                    this.loadingEntity = false;
                });
            }

            this.setSistema = () => {

                this.loadingEntity = true;

                var promise = parametrosRegrasService.getSistemasCriamEventos();

                promise.then(result => {

                    if (result) {
                        this.sistema = ez.domain.enum.sistemaEnum.valores
                            .filter((item) => {
                                return result.sistemas.indexOf(item.valor) >= 0;
                            });
                    }

                }).finally(() => {
                    this.loadingEntity = false;
                });
            }

            this.sistemaOnChange = (sistemaEnum) => {

                this.setTipoDeEvento(sistemaEnum);

            };
        }

        $onInit() {
            super.init();

            this.setSistema();

            this.events.onGetEntity = (regraBase) => {
                this.setTipoDeEvento(regraBase.sistema);  
            };

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new parametrosRegrasDtos.Dtos.Agenda.Parametros.Regras.RegraBaseInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ParametrosRegrasAgendaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/parametros/regras/parametrosRegrasAgendaForm.cshtml';
            this.controller = ParametrosRegrasFormController;
            this.bindings = {
                operation: '@'
            };
        }
    }
}