﻿import * as parametrosPadroesAgendaService from "../../../../../services/domain/agenda/parametros/parametrosPadroesAgendaService";
import * as parametrosAgendaPadroesDtos from "../../../../../dtos/agenda/parametros/parametrosPadroesAgendaDtos";
import * as requestParam from "../../../../../common/requestParams/requestParams";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ParametrosPadroesAgendaFormController extends controllers.Controllers.ControllerFormComponentBase<parametrosAgendaPadroesDtos.Dtos.Agenda.Parametros.Padroes.ParametroAgendaListDto, parametrosAgendaPadroesDtos.Dtos.Agenda.Parametros.Padroes.ParametroAgendaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'parametrosPadroesAgendaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public parametroAgendaService: parametrosPadroesAgendaService.Services.ParametroPadroesAgendaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                parametrosPadroesAgendaService,
                'tenant.agenda.parametros');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new parametrosAgendaPadroesDtos.Dtos.Agenda.Parametros.Padroes.ParametroAgendaInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ParametrosPadroesAgendaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/parametros/padroes/parametrosPadroesAgendaForm.cshtml';
            this.controller = ParametrosPadroesAgendaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}