﻿import * as disponibilidadeService from "../../../../services/domain/agenda/disponibilidade/disponibilidadeService";
import * as configuracaoDeDisponibilidadeService from "../../../../services/domain/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeService";
import * as configuracaoDeDisponibilidadeDtos from "../../../../dtos/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class DisponibilidadeFormController extends controllers.Controllers.ControllerFormComponentBase<configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto, configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoDisponibilidade: (termoDigitadoPesquisa: string) => any;
        private disponibilidadeSelecionado: (registro: configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto) => string;
        private disponibilidadeDeselecionado: () => void;
        private editWithEvento: () => void;
        private requestParamsDisponibilidade: requestParam.RequestParam.RequestParams;
        private sistemaEnum: any;
        private tipoDeDisponibilidadeEnum: any;
        private tipoDeEventoEnum: any;
        private dayOfWeek: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'configuracaoDeDisponibilidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeDisponibilidadeService: configuracaoDeDisponibilidadeService.Services.ConfiguracaoDeDisponibilidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                configuracaoDeDisponibilidadeService,
                'tenant.agenda.disponibilidade');
        }

        $onInit() {
            super.init();

            this.sistemaEnum = ez.domain.enum.sistemaEnum.valores;
            this.tipoDeDisponibilidadeEnum = ez.domain.enum.tipoDeDisponibilidadeEnum.valores;
            this.tipoDeEventoEnum = ez.domain.enum.tipoDeEventoEnum.valores;
            this.dayOfWeek = ez.domain.enum.dayOfWeek.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };



            this.events.onGetEntity = () => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.dataEspecifica = this.entity.dataEspecifica != null ? new Date(this.entity.dataEspecifica.toString()) : null;
                this.entity.dataInicioValidade = this.entity.dataInicioValidade != null ? new Date(this.entity.dataInicioValidade.toString()) : null;
                this.entity.dataFimValidade = this.entity.dataFimValidade != null ? new Date(this.entity.dataFimValidade.toString()) : null;
            };

            this.editWithEvento = () => {
                var promise = this.configuracaoDeDisponibilidadeService.existeEvento(this.entity.id);
                promise.then(result => {
                    if (result) {
                        abp.message.confirm(
                            'Existem Eventos Gerados para Disponibilidades',
                            'Deseja realmente alterar?',
                            isConfirmed => {
                                if (isConfirmed) {
                                    this.save();
                                }
                            }
                        );
                    } else {
                        this.save();
                    }
                });
            };

            this.events.onBeforeSaveEntity = () => {
                this.entity.diasDaSemana = (this.entity.tipoDeDisponibilidade === 2) ? this.entity.semana.toString() : null;

                if (this.entity.tipoDeDisponibilidade === 1) {
                    this.entity.dataFimValidade = new Date();
                    this.entity.dataFimValidade.setDate(this.entity.dataInicioValidade.getDate() + this.entity.numeroDeDias);
                }
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        tipoDeDisponibilidadeSelected = () => {
            if (this.entity.tipoDeDisponibilidade !== 3) {
                this.entity.dataInicioValidade = new Date();
                this.entity.dataInicioValidade.setDate(this.entity.dataInicioValidade.getDate() + 1);
            }
        };

        calcValidade = () => {
            if (this.entity.dataInicioValidade != null) {
                this.entity.dataFimValidade = new Date(this.entity.dataInicioValidade);
                this.entity.dataFimValidade.setDate(this.entity.dataFimValidade.getDate() + this.entity.numeroDeDias);
            }
        };


    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DisponibilidadeFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/disponibilidade/disponibilidadeForm.cshtml';
            this.controller = DisponibilidadeFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}