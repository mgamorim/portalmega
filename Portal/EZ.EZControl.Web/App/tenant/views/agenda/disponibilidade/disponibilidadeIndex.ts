﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as disponibilidadeService from "../../../../services/domain/agenda/disponibilidade/disponibilidadeService";
import * as disponibilidadeDtos from "../../../../dtos/agenda/disponibilidade/disponibilidadeDtos";
import * as configuracaoDeDisponibilidadeService from "../../../../services/domain/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeService";
import * as configuracaoDeDisponibilidadeDtos from "../../../../dtos/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Agenda.Disponibilidade {
    class DisponibilidadeIndexController extends controllers.Controllers.ControllerIndexBase<configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto, configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeInput>{
        static $inject = ['$state', 'uiGridConstants', 'configuracaoDeDisponibilidadeService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeDisponibilidadeService: configuracaoDeDisponibilidadeService.Services.ConfiguracaoDeDisponibilidadeService) {
            super(
                $state,
                uiGridConstants,
                configuracaoDeDisponibilidadeService, {
                    rotaAlterarRegistro: 'tenant.agenda.disponibilidade.alterar',
                    rotaNovoRegistro: 'tenant.agenda.disponibilidade.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.GetConfiguracaoDeDisponibilidadeInput();
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.configuracaoDeDisponibilidadeService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDisponibilidade', this.novo, abp.auth.hasPermission('Pages.Tenant.Agenda.Disponibilidade.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Agenda.Disponibilidade.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Agenda.Disponibilidade.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Titulo'),
                field: 'titulo',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.TipoDeDisponibilidade'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeDisponibilidadeEnum", "tipoDeDisponibilidade")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Sistema'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "sistemaEnum", "sistema")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.TipoDeEvento'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeEventoEnum", "tipoDeEvento")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.DiaDaSemana'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValues(row.entity, "dayOfWeek", "diasDaSemana")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.NumeroDeDias'),
                field: 'numeroDeDias'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Data'),
                field: 'dataEspecifica',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.HorarioInicio'),
                field: 'horarioInicio',
                cellFilter: 'date:"HH:mm\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.HorarioFim'),
                field: 'horarioFim',
                cellFilter: 'date:"HH:mm\"'
            });
        }
    }

    export class DisponibilidadeIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/disponibilidade/disponibilidadeIndex.cshtml';
            this.controller = DisponibilidadeIndexController;
        }
    }
}