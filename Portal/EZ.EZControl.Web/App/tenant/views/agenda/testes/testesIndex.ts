﻿import Scheduler = require("../../../../common/ezScheduler/ezScheduler");

export module Agenda.Testes {    

    class TestesController {
        constructor() {
            this.ezScheduler = new Scheduler.EzScheduler.EzScheduler();
            this.ezScheduler.sistema = ez.domain.enum.sistemaEnum.global;
        }

        ezScheduler: Scheduler.EzScheduler.EzScheduler;

        $onInit() {
        }
    }

    export class AgendaTestesIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/agenda/testes/testesIndex.cshtml';
            this.controller = TestesController;
        }
    }
}