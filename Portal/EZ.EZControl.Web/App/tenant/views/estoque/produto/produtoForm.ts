﻿import * as produtoService from "../../../../services/domain/estoque/produto/produtoService";
import * as produtoDtos from "../../../../dtos/estoque/produto/produtoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as ezFile from "../../../../common/ezFile/ezFile";

import * as naturezaService from "../../../../services/domain/estoque/parametros/naturezaService";
import * as naturezaDtos from "../../../../dtos/estoque/parametro/naturezaDtos";
import * as unidadeMedidaService from "../../../../services/domain/estoque/parametros/unidadeMedidaService";
import * as unidadeMedidaDtos from "../../../../dtos/estoque/parametro/unidadeMedidaDtos";

import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";

import * as arquivoDtos from "../../../../dtos/core/arquivo/arquivoDtos";

export module Forms {
    export class ProdutoFormController extends controllers.Controllers.ControllerFormComponentBase<produtoDtos.Dtos.Produto.ProdutoListDto, produtoDtos.Dtos.Produto.ProdutoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;

        public ezSelectNatureza: ezSelect.EzSelect.EzSelect<naturezaDtos.Dtos.Natureza.NaturezaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaNatureza: (termoDigitadoPesquisa: string) => any;
        private naturezaSelecionado: (registro: naturezaDtos.Dtos.Natureza.NaturezaListDto) => string;
        private naturezaDeselecionado: () => void;
        private requestParamsNatureza: requestParam.RequestParam.RequestParams;

        public ezSelectUnidadeMedida: ezSelect.EzSelect.EzSelect<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaUnidadeMedida: (termoDigitadoPesquisa: string) => any;
        private unidadeMedidaSelecionado: (registro: unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto) => string;
        private unidadeMedidaDeselecionado: () => void;
        private requestParamsUnidadeMedida: requestParam.RequestParam.RequestParams;

        private imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;

        public tipoDeProduto = ez.domain.enum.tipoDeProdutoEnum.valores;

        public upload: () => void;

        public ezFile: ezFile.EZFile.EZFile;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'estoqueProdutoService', 'estoqueNaturezaService', 'estoqueUnidadeMedidaService', 'arquivoGlobalService', 'parametroGlobalService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public produtoService: produtoService.Services.ProdutoService,
            public naturezaService: naturezaService.Services.NaturezaService,
            public unidadeMedidaService: unidadeMedidaService.Services.UnidadeMedidaService,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                produtoService,
                'tenant.estoque.produto');
        }

        $onInit() {
            super.init();

            this.ezFile = new ezFile.EZFile.EZFile();

            this.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            this.imagem.id = 0;
            this.imagem.isActive = true;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new produtoDtos.Dtos.Produto.ProdutoInput();
                instance.id = 0;
                instance.isActive = true;
                instance.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
                instance.imagem.id = 0;
                instance.imagemId = 0;
                instance.imagem.isActive = true;
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                if (entity.naturezaId) {
                    this.ezSelectNatureza.loading = true;
                    var promiseNatureza = this.naturezaService.getById(entity.naturezaId);
                    promiseNatureza.then(result => {
                        this.ezSelectNatureza.setInputText(result.descricao);
                    }).finally(() => {
                        this.ezSelectNatureza.loading = false;
                    });
                }
                if (entity.unidadeMedidaId) {
                    this.ezSelectUnidadeMedida.loading = true;
                    var promiseUnidadeMedida = this.unidadeMedidaService.getById(entity.unidadeMedidaId);
                    promiseUnidadeMedida.then(result => {
                        this.ezSelectUnidadeMedida.setInputText(result.descricao);
                    }).finally(() => {
                        this.ezSelectUnidadeMedida.loading = false;
                    });
                }

                this.ezFile.controller.arquivoId = entity.imagemId;
                this.ezFile.initialize();
            };

            this.ezSelectNaturezaConfig();
            this.ezSelectUnidadeMedidaConfig();
            this.ezFileConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectNaturezaConfig() {
            this.requestParamsNatureza = new requestParam.RequestParam.RequestParams();

            this.naturezaSelecionado = (registro) => {
                this.entity.naturezaId = registro.id;
                return registro.descricao;
            }

            this.naturezaDeselecionado = () => {
                this.entity.naturezaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaNatureza = (termoDigitadoPesquisa) => {
                var filtro = new naturezaDtos.Dtos.Natureza.GetNaturezaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectNatureza = new ezSelect.EzSelect.EzSelect<naturezaDtos.Dtos.Natureza.NaturezaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.naturezaService,
                this.getFiltroParaNatureza,
                this.requestParamsNatureza,
                this.$uibModal,
                this.naturezaSelecionado,
                this.naturezaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectNatureza.onEzGridCreated = () => {
                this.ezSelectNatureza.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Natureza'),
                    field: 'descricao'
                });

                this.ezSelectNatureza.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectNatureza.on.beforeOpenModalDialog = () => {
                this.ezSelectNatureza.ezGrid.getRegistros();
            };
        }

        private ezSelectUnidadeMedidaConfig() {
            this.requestParamsUnidadeMedida = new requestParam.RequestParam.RequestParams();

            this.unidadeMedidaSelecionado = (registro) => {
                this.entity.unidadeMedidaId = registro.id;
                return registro.descricao;
            }

            this.unidadeMedidaDeselecionado = () => {
                this.entity.unidadeMedidaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaUnidadeMedida = (termoDigitadoPesquisa) => {
                var filtro = new unidadeMedidaDtos.Dtos.UnidadeMedida.GetUnidadeMedidaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectUnidadeMedida = new ezSelect.EzSelect.EzSelect<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.unidadeMedidaService,
                this.getFiltroParaUnidadeMedida,
                this.requestParamsUnidadeMedida,
                this.$uibModal,
                this.unidadeMedidaSelecionado,
                this.unidadeMedidaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectUnidadeMedida.onEzGridCreated = () => {
                this.ezSelectUnidadeMedida.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.UnidadeMedida'),
                    field: 'descricao'
                });

                this.ezSelectUnidadeMedida.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectUnidadeMedida.on.beforeOpenModalDialog = () => {
                this.ezSelectUnidadeMedida.ezGrid.getRegistros();
            };
        }

        private ezFileConfig() {
            this.ezFile.loadImage = () => {
                var promise = this.arquivoService.getById(this.entity.imagemId);
                promise.then((result) => {
                    this.ezFile.setImageInfo(result.path, result.tipoDeArquivoFixo);
                });
            }
            this.ezFile.getParametros = () => {
                var promise = this.parametroGlobalService.get();
                promise.then((result) => {
                    this.ezFile.setParametos(result.extensoesImagem);
                });
            }
            this.ezFile.onUploadComplete = (fileName: string) => {
                var input = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
                input.nome = fileName;
                var promise = this.arquivoService.getIdByName(input);
                promise.then((result) => {
                    this.entity.imagemId = result.id;
                });
            }
            this.ezFile.initialize = () => {
                this.ezFile.inicializado = true;
                this.ezFile.controller.sistema = "Estoque";
                this.ezFile.controller.autoUpload = true;
                if (this.ezFile.controller.arquivoId > 0) {
                    if (this.ezFile.loadImage) {
                        this.ezFile.loadImage();
                    }
                }
                if (this.ezFile.onUploadComplete) {
                    this.ezFile.controller.onUploadComplete = this.ezFile.onUploadComplete;
                }
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ProdutoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/produto/produtoForm.cshtml';
            this.controller = ProdutoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}