﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as produtoService from "../../../../services/domain/estoque/produto/produtoService";
import * as produtoDtos from "../../../../dtos/estoque/produto/produtoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Estoque.Produto {

    class ProdutoIndexController extends controllers.Controllers.ControllerIndexBase<produtoDtos.Dtos.Produto.ProdutoListDto, produtoDtos.Dtos.Produto.ProdutoInput>{

        static $inject = ['$state', 'uiGridConstants', 'estoqueProdutoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<produtoDtos.Dtos.Produto.ProdutoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public produtoService: produtoService.Services.ProdutoService) {

            super(
                $state,
                uiGridConstants,
                produtoService, {
                    rotaAlterarRegistro: 'tenant.estoque.produto.alterar',
                    rotaNovoRegistro: 'tenant.estoque.produto.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new produtoDtos.Dtos.Produto.GetProdutoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<produtoDtos.Dtos.Produto.ProdutoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewProduto', this.novo, abp.auth.hasPermission('Pages.Tenant.Estoque.Produto.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Produto.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Produto.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Produto.TipoDeProduto'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeProdutoEnum", "tipoDeProduto")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Valor'),
                field: 'valor'
            });
        }
    }

    export class ProdutoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/produto/produtoIndex.cshtml';
            this.controller = ProdutoIndexController;
        }
    }
}