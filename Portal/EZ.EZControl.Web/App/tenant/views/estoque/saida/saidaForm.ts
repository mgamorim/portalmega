﻿import * as saidaService from "../../../../services/domain/estoque/saida/saidaService";
import * as saidaDtos from "../../../../dtos/estoque/saida/saidaDtos";
import * as saidaItemService from "../../../../services/domain/estoque/saidaItem/saidaItemService";
import * as saidaItemDtos from "../../../../dtos/estoque/saidaItem/saidaItemDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as empresaService from "../../../../services/domain/global/empresa/empresaService";
import * as empresaDtos from "../../../../dtos/global/empresa/empresaDtos";
import * as localArmazenamentoService from "../../../../services/domain/estoque/parametros/localArmazenamentoService";
import * as localArmazenamentoDtos from "../../../../dtos/estoque/parametro/localArmazenamentoDtos";
import * as pedidoService from "../../../../services/domain/vendas/pedido/pedidoService";
import * as pedidoDtos from "../../../../dtos/vendas/pedido/pedidoDtos";
import * as produtoService from "../../../../services/domain/estoque/produto/produtoService";
import * as produtoDtos from "../../../../dtos/estoque/produto/produtoDtos";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class SaidaFormController extends controllers.Controllers.ControllerFormComponentBase<saidaDtos.Dtos.Saida.SaidaListDto, saidaDtos.Dtos.Saida.SaidaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        public motivoSaidaEnum: any;
        public saidaForm: ng.IFormController;
        public saidaItemForm: ng.IFormController;

        // Empresa
        public ezSelectEmpresa: ezSelect.EzSelect.EzSelect<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaEmpresa: (termoDigitadoPesquisa: string) => any;
        private empresaSelecionado: (registro: empresaDtos.Dtos.Empresa.EmpresaListDto) => string;
        private empresaDeselecionado: () => void;
        private requestParamsEmpresa: requestParam.RequestParam.RequestParams;
        // Local Armazenamento
        public ezSelectLocalArmazenamento: ezSelect.EzSelect.EzSelect<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaLocalArmazenamento: (termoDigitadoPesquisa: string) => any;
        private localArmazenamentoSelecionado: (registro: localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto) => string;
        private localArmazenamentoDeselecionado: () => void;
        private requestParamsLocalArmazenamento: requestParam.RequestParam.RequestParams;
        // Pedido
        public ezSelectPedido: ezSelect.EzSelect.EzSelect<pedidoDtos.Dtos.Pedido.PedidoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPedido: (termoDigitadoPesquisa: string) => any;
        private pedidoSelecionado: (registro: pedidoDtos.Dtos.Pedido.PedidoListDto) => string;
        private pedidoDeselecionado: () => void;
        private requestParamsPedido: requestParam.RequestParam.RequestParams;
        //Produto
        public ezSelectProduto: ezSelect.EzSelect.EzSelect<produtoDtos.Dtos.Produto.ProdutoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoProduto: (termoDigitadoPesquisa: string) => any;
        private produtoSelecionado: (registro: produtoDtos.Dtos.Produto.ProdutoListDto) => string;
        private produtoDeselecionado: () => void;
        private requestParamsProduto: requestParam.RequestParam.RequestParams;
        

        //Saida Item
        private gridDataEnumerable: saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto[];
        private requestParamsSaidaItem: requestParam.RequestParam.RequestParams;
        private saidaItemAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>;        
        public loadingSaidaItem: boolean;
        public saidaItemGridOptions: uiGrid.IGridOptionsOf<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>;
        public editarItem: (saidaItem: saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto) => void;
        public excluirItem: (saidaItem: saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto) => void;
        public limparSaidaItemCurrent: () => void;
        public getItemGrid: () => void;        
        public saidaItemCurrent: saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto;
        public editandoItem: boolean;
        public totalItems: () => number;

        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'estoqueSaidaService', 'estoqueSaidaItemService', 'empresaService', 'estoqueLocalArmazenamentoService', 'pedidoService', 'estoqueProdutoService', '$uibModal'];

        constructor(
            private $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public saidaService: saidaService.Services.SaidaService,
            public saidaItemService: saidaItemService.Services.SaidaItemService,
            public empresaService: empresaService.Services.EmpresaService,
            public localArmazenamentoService: localArmazenamentoService.Services.LocalArmazenamentoService,
            public pedidoService: pedidoService.Services.PedidoService,
            public produtoService: produtoService.Services.ProdutoService,            
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                saidaService,
                'tenant.estoque.saida');

            this.editandoItem = false;

            this.editarItem = (saidaItem: saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto) => {
                this.editandoItem = true;
                this.saidaItemCurrent = angular.copy(saidaItem);
                
                this.ezSelectProduto.setInputText(saidaItem.produto.nome);
                if (saidaItem.localArmazenamento != null)
                    this.ezSelectLocalArmazenamento.setInputText(saidaItem.localArmazenamento.descricao);
            }

            this.excluirItem = (saidaItem: saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto) => {
                abp.message.confirm(
                    '', 'Deseja realmente excluir este item?',
                    isConfirmed => {
                        if (isConfirmed) {     
                            this.saving = true;
                            var promise = (this.saidaItemService as any as saidaItemService.Services.SaidaItemService).delete(saidaItem.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarSaidaItemCurrent();
                            }).finally(() => {
                                this.saving = false;
                                });

                            var index = this.entity.items.indexOf(saidaItem);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.items.splice(index, 1);
                            }
                            this.editandoItem = false;
                        }
                    }
                );
            }

            this.limparSaidaItemCurrent = () => {
                this.inicializarSaidaItemCurrent();
                this.ezSelectProduto.gridSelectClear();
                this.ezSelectLocalArmazenamento.gridSelectClear();
                this.ezSelectProduto.setInputText("");
                this.ezSelectLocalArmazenamento.setInputText("");
                this.editandoItem = false;
            }

            this.getItemGrid = () => {
                this.saidaItemGridOptions.data = this.entity.items;
                this.saidaItemGridOptions.totalItems = this.entity.items.length;
            }

            this.totalItems = (): number => {
              if (this.gridDataEnumerable) {
                  var total = 0;
                  for (let i in this.gridDataEnumerable) {
                    let x = this.gridDataEnumerable[i];
                    total = total + (x.valorUnitario * x.quantidade);
                  }
                  return total;
                }

                return 0;
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new saidaDtos.Dtos.Saida.SaidaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.dataSaida = new Date();                
                this.$scope.$watchCollection('entity.items', (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                }); 
                // Seleciona a Empresa logada             
                this.ezSelectEmpresa.loading = true;
                var promiseEmpresa = this.empresaService.getById(abp.session.empresaId);
                promiseEmpresa.then(result => {
                    this.ezSelectEmpresa.setInputText(result.nomeFantasia);
                    instance.empresaId = abp.session.empresaId;
                }).finally(() => {
                    this.ezSelectEmpresa.loading = false;
                });
                this.inicializarSaidaItemCurrent();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.dataSaida = this.entity.dataSaida != null ? new Date(this.entity.dataSaida.toString()) : new Date();
                
                if (entity.empresaId) {
                    this.ezSelectEmpresa.loading = true;
                    var promiseEmpresa = this.empresaService.getById(entity.empresaId);
                    promiseEmpresa.then(result => {
                        this.ezSelectEmpresa.setInputText(result.nomeFantasia);
                    }).finally(() => {
                        this.ezSelectEmpresa.loading = false;
                    });
                }

                if (entity.pedidoId) {
                    this.ezSelectPedido.loading = true;
                    var promisePedido = this.pedidoService.getById(entity.pedidoId);
                    promisePedido.then(result => {
                        this.ezSelectPedido.setInputText(result.id.toString());
                    }).finally(() => {
                        this.ezSelectPedido.loading = false;
                    });
                }               
                 
                // Saida Itens
                this.$scope.$watchCollection(() => { return this.entity.items }, (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });
                this.inicializarSaidaItemCurrent();
                this.getItemGrid();
            }


            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.ezSelectEmpresaConfig();
            this.ezSelectLocalArmazenamentoConfig();
            this.ezSelectPedidoConfig();
            this.ezSelectProdutoConfig();

            this.motivoSaidaEnum = ez.domain.enum.motivoSaidaEnum.valores;

            this.saidaItemGridConfig();
            
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        setSaidaForm(form: ng.IFormController) {
            this.saidaForm = form;
        }

        setSaidaItemForm(form: ng.IFormController) {
            this.saidaItemForm = form;
        }

        private ezSelectEmpresaConfig() {
            this.requestParamsEmpresa = new requestParam.RequestParam.RequestParams();

            this.empresaSelecionado = (registro) => {
                this.entity.empresaId = registro.id;
                return registro.nomeFantasia;
            }

            this.empresaDeselecionado = () => {
                this.entity.empresaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaEmpresa = (termoDigitadoPesquisa) => {
                var filtro = new empresaDtos.Dtos.Empresa.GetEmpresaInput();
                filtro.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEmpresa = new ezSelect.EzSelect.EzSelect<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.empresaService,
                this.getFiltroParaEmpresa,
                this.requestParamsEmpresa,
                this.$uibModal,
                this.empresaSelecionado,
                this.empresaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEmpresa.onEzGridCreated = () => {
                this.ezSelectEmpresa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Empresa.NomePessoa'),
                    field: 'nomeFantasia'
                });

                this.ezSelectEmpresa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectEmpresa.on.beforeOpenModalDialog = () => {
                this.ezSelectEmpresa.ezGrid.getRegistros();
            };
        }

        private ezSelectLocalArmazenamentoConfig() {
            this.requestParamsLocalArmazenamento = new requestParam.RequestParam.RequestParams();

            this.localArmazenamentoSelecionado = (registro) => {                
                this.saidaItemCurrent.localArmazenamentoId = registro.id;
                this.saidaItemCurrent.localArmazenamento = registro;
                return registro.descricao;
            }

            this.localArmazenamentoDeselecionado = () => {
                this.saidaItemCurrent.localArmazenamentoId = null;
                this.saidaItemCurrent.localArmazenamento = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaLocalArmazenamento = (termoDigitadoPesquisa) => {
                var filtro = new localArmazenamentoDtos.Dtos.LocalArmazenamento.GetLocalArmazenamentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectLocalArmazenamento = new ezSelect.EzSelect.EzSelect<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.localArmazenamentoService,
                this.getFiltroParaLocalArmazenamento,
                this.requestParamsLocalArmazenamento,
                this.$uibModal,
                this.localArmazenamentoSelecionado,
                this.localArmazenamentoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectLocalArmazenamento.onEzGridCreated = () => {
                this.ezSelectLocalArmazenamento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estoque.Saida.LocalArmazenamento'),
                    field: 'descricao'
                });

                this.ezSelectLocalArmazenamento.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectLocalArmazenamento.on.beforeOpenModalDialog = () => {
                this.ezSelectLocalArmazenamento.ezGrid.getRegistros();
            };
        }

        private ezSelectPedidoConfig() {
            this.requestParamsEmpresa = new requestParam.RequestParam.RequestParams();

            this.pedidoSelecionado = (registro) => {
                this.entity.pedidoId = registro.id;
                return registro.id.toString();
            }

            this.pedidoDeselecionado = () => {
                this.entity.pedidoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPedido = (termoDigitadoPesquisa) => {
                var filtro = new pedidoDtos.Dtos.Pedido.GetPedidoInput();
                filtro.nomeCliente = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPedido = new ezSelect.EzSelect.EzSelect<pedidoDtos.Dtos.Pedido.PedidoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pedidoService,
                this.getFiltroParaEmpresa,
                this.requestParamsEmpresa,
                this.$uibModal,
                this.pedidoSelecionado,
                this.pedidoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPedido.onEzGridCreated = () => {
                this.ezSelectPedido.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pedido.Cliente'),
                    field: 'nomeCliente'
                });

                this.ezSelectPedido.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectPedido.on.beforeOpenModalDialog = () => {
                this.ezSelectPedido.ezGrid.getRegistros();
            };
        }

        private ezSelectProdutoConfig() {
            this.requestParamsProduto = new requestParam.RequestParam.RequestParams();

            this.produtoSelecionado = (registro) => {                
                this.saidaItemCurrent.produtoId = registro.id;
                this.saidaItemCurrent.produto = registro;
                return registro.nome;
            }

            this.produtoDeselecionado = () => {
                this.saidaItemCurrent.produto = null;
                this.saidaItemCurrent.produtoId = 0;
                this.saidaItemCurrent.valorUnitario = 0;
                this.saidaItemCurrent.quantidade = 1;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoProduto = (termoDigitadoPesquisa) => {
                var filtro = new produtoDtos.Dtos.Produto.GetProdutoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProduto = new ezSelect.EzSelect.EzSelect<produtoDtos.Dtos.Produto.ProdutoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoService,
                this.getFiltroParaPaginacaoProduto,
                this.requestParamsProduto,
                this.$uibModal,
                this.produtoSelecionado,
                this.produtoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProduto.onEzGridCreated = () => {
                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Nome'),
                    field: 'nome'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Valor'),
                    field: 'valor'
                });

                this.ezSelectProduto.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectProduto.on.beforeOpenModalDialog = () => {
                this.ezSelectProduto.ezGrid.getRegistros();
            };
        }

        /* Saida Item */
        private inicializarSaidaItemCurrent() {
            this.saidaItemCurrent = new saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto();
            this.saidaItemCurrent.valorUnitario = 0;
            this.saidaItemCurrent.quantidade = 1;
            this.saidaItemCurrent.localArmazenamento = new localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto();
            this.saidaItemCurrent.produto = new produtoDtos.Dtos.Produto.ProdutoListDto();
            this.saidaItemCurrent.isActive = true;
            if (this.entity)
                this.saidaItemCurrent.saidaId = this.entity.id;
        }

        private isQuantidadeValida(value: any): boolean {
            return value > 0;
        }

        private getSaidaItemByTempId(tempId: number): saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto {
            for (let i = 0; i < this.entity.items.length; i++) {
                if (this.entity.items[i].id === tempId) {
                    return this.entity.items[i];
                }
            }
        }

        private cancelarItem() {
            this.limparSaidaItemCurrent();
            this.editandoItem = false;
        }

        private gravarItem(item: any) {
            this.saving = true;
            this.saidaItemCurrent.saidaId = this.entity.id;            
            var promise = (this.saidaItemService as any as saidaItemService.Services.SaidaItemService).save(item);
            promise.then(result => {
                this.saidaItemCurrent.id = result.id;
                if (!this.editandoItem) {
                    this.entity.items.push(this.saidaItemCurrent);
                }
                this.inicializarSaidaItemCurrent();
                this.editandoItem = false;
                this.gridDataEnumerable = this.entity.items;
                this.getItemGrid();
                this.limparSaidaItemCurrent();                
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarItem() {
            this.saidaItemCurrent.total = this.saidaItemCurrent.valorUnitario * this.saidaItemCurrent.quantidade;
            if (!this.editandoItem) {
                this.gravarItem(this.saidaItemCurrent);                                
            }
            else {
                var item = this.getSaidaItemByTempId(this.saidaItemCurrent.id);
                angular.merge(item, this.saidaItemCurrent);                
                this.gravarItem(item);
            }
        }

        private saidaItemGridConfig() {
            this.requestParamsSaidaItem = new requestParam.RequestParam.RequestParams();
            this.requestParamsSaidaItem.sorting = 'Id';

            this.saidaItemAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>();
            this.saidaItemAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarItem, 'Edit', true));
            this.saidaItemAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirItem, 'Delete', true));

            //callbacks
            var callBack: ezGrid.EzGrid.IEzGridAppScopeCallBack = {
                key: 'totalItems',
                fn: this.totalItems
            };
            this.saidaItemAppScopeProvider.addCallBack(callBack);

            this.saidaItemGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.saidaItemAppScopeProvider
            }

            var lista = this.entity ? this.entity.items : new Array<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>();
            this.saidaItemGridOptions.data = lista;
            this.saidaItemGridOptions.totalItems = lista.length;

            this.saidaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.SaidaItem.Produto'),
                field: 'produto.nome',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.saidaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.SaidaItem.ValorUnitario'),
                field: 'valorUnitario',
                cellFilter: 'currency:"R$ "'
            });

            this.saidaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.SaidaItem.Quantidade'),
                field: 'quantidade'
            });

            this.saidaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.SaidaItem.Total'),
                field: 'total',
                cellFilter: 'currency:"R$ "',
                footerCellTemplate: '<div class="ui-grid-cell-contents">Total: {{grid.appScope.executeCallBack("totalItems")}}</div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class SaidaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/saida/saidaForm.cshtml';
            this.controller = SaidaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}