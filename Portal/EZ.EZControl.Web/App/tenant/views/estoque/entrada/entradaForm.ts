﻿import * as entradaService from "../../../../services/domain/estoque/entrada/entradaService";
import * as entradaDtos from "../../../../dtos/estoque/entrada/entradaDtos";
import * as entradaItemService from "../../../../services/domain/estoque/entradaItem/entradaItemService";
import * as entradaItemDtos from "../../../../dtos/estoque/entradaItem/entradaItemDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as empresaService from "../../../../services/domain/global/empresa/empresaService";
import * as empresaDtos from "../../../../dtos/global/empresa/empresaDtos";
import * as localArmazenamentoService from "../../../../services/domain/estoque/parametros/localArmazenamentoService";
import * as localArmazenamentoDtos from "../../../../dtos/estoque/parametro/localArmazenamentoDtos";
import * as fornecedorService from "../../../../services/domain/global/fornecedor/fornecedorService";
import * as fornecedorDtos from "../../../../dtos/global/fornecedor/fornecedorDtos";
import * as produtoService from "../../../../services/domain/estoque/produto/produtoService";
import * as produtoDtos from "../../../../dtos/estoque/produto/produtoDtos";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class EntradaFormController extends controllers.Controllers.ControllerFormComponentBase<entradaDtos.Dtos.Entrada.EntradaListDto, entradaDtos.Dtos.Entrada.EntradaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;

        // Empresa
        public ezSelectEmpresa: ezSelect.EzSelect.EzSelect<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaEmpresa: (termoDigitadoPesquisa: string) => any;
        private empresaSelecionado: (registro: empresaDtos.Dtos.Empresa.EmpresaListDto) => string;
        private empresaDeselecionado: () => void;
        private requestParamsEmpresa: requestParam.RequestParam.RequestParams;
        // Local Armazenamento
        public ezSelectLocalArmazenamento: ezSelect.EzSelect.EzSelect<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaLocalArmazenamento: (termoDigitadoPesquisa: string) => any;
        private localArmazenamentoSelecionado: (registro: localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto) => string;
        private localArmazenamentoDeselecionado: () => void;
        private requestParamsLocalArmazenamento: requestParam.RequestParam.RequestParams;
        // Fornecedor
        public ezSelectFornecedor: ezSelect.EzSelect.EzSelect<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaFornecedor: (termoDigitadoPesquisa: string) => any;
        private fornecedorSelecionado: (registro: fornecedorDtos.Dtos.Fornecedor.FornecedorListDto) => string;
        private fornecedorDeselecionado: () => void;
        private requestParamsFornecedor: requestParam.RequestParam.RequestParams;
        //Produto
        public ezSelectProduto: ezSelect.EzSelect.EzSelect<produtoDtos.Dtos.Produto.ProdutoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoProduto: (termoDigitadoPesquisa: string) => any;
        private produtoSelecionado: (registro: produtoDtos.Dtos.Produto.ProdutoListDto) => string;
        private produtoDeselecionado: () => void;
        private requestParamsProduto: requestParam.RequestParam.RequestParams;

        //Entrada Item
        private gridDataEnumerable: entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto[];
        private requestParamsEntradaItem: requestParam.RequestParam.RequestParams;
        private entradaItemAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>;        
        public loadingEntradaItem: boolean;
        public entradaItemGridOptions: uiGrid.IGridOptionsOf<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>;
        public editarItem: (entradaItem: entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto) => void;
        public excluirItem: (entradaItem: entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto) => void;
        public limparEntradaItemCurrent: () => void;        
        public getItemGrid: () => void;
        public entradaItemForm: ng.IFormController;
        public entradaItemCurrent: entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto;
        public editandoItem: boolean;   
        public totalItems: () => number;

        public entradaForm: ng.IFormController;

        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'estoqueEntradaService', 'estoqueEntradaItemService', 'empresaService', 'estoqueLocalArmazenamentoService', 'fornecedorService', 'estoqueProdutoService', '$uibModal'];

        constructor(
            private $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public entradaService: entradaService.Services.EntradaService,
            public entradaItemService: entradaItemService.Services.EntradaItemService,
            public empresaService: empresaService.Services.EmpresaService,
            public localArmazenamentoService: localArmazenamentoService.Services.LocalArmazenamentoService,
            public fornecedorService: fornecedorService.Services.FornecedorService,
            public produtoService: produtoService.Services.ProdutoService,            
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                entradaService,
                'tenant.estoque.entrada');

            this.editandoItem = false;

            this.editarItem = (entradaItem: entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto) => {
                this.editandoItem = true;
                this.entradaItemCurrent = angular.copy(entradaItem);
                this.ezSelectProduto.setInputText(entradaItem.produto.nome);
            }

            this.excluirItem = (entradaItem: entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto) => {
                abp.message.confirm(
                    '', 'Deseja realmente excluir este item?',
                    isConfirmed => {
                        if (isConfirmed) {     
                            this.saving = true;                            
                            var promise = (this.entradaItemService as any as entradaItemService.Services.EntradaItemService).delete(entradaItem.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarEntradaItemCurrent();
                            }).finally(() => {
                                this.saving = false;
                                });

                            var index = this.entity.items.indexOf(entradaItem);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.items.splice(index, 1);
                            }
                            this.editandoItem = false;
                        }
                    }
                );
            }

            this.limparEntradaItemCurrent = () => {
                this.inicializarEntradaItemCurrent();
                this.ezSelectProduto.gridSelectClear();   
                this.editandoItem = false;
            }

            this.getItemGrid = () => {
                this.entradaItemGridOptions.data = this.entity.items;
                this.entradaItemGridOptions.totalItems = this.entity.items.length;
            }

            this.totalItems = (): number => {
                if (this.gridDataEnumerable) {
                    var total = 0;
                    for (let i in this.gridDataEnumerable) {
                      let x = this.gridDataEnumerable[i];
                      total = total + (x.valorUnitario * x.quantidade);
                    }
                    return total;
                }

                return 0;
            } 
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new entradaDtos.Dtos.Entrada.EntradaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.dataEntrada = new Date();  
                this.$scope.$watchCollection('entity.items', (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });              
                // Seleciona a Empresa logada
                this.ezSelectEmpresa.loading = true;
                var promiseEmpresa = this.empresaService.getById(abp.session.empresaId);
                promiseEmpresa.then(result => {
                    this.ezSelectEmpresa.setInputText(result.nomeFantasia);
                    instance.empresaId = abp.session.empresaId;
                }).finally(() => {
                    this.ezSelectEmpresa.loading = false;
                });
                this.inicializarEntradaItemCurrent();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.dataEntrada = this.entity.dataEntrada != null ? new Date(this.entity.dataEntrada.toString()) : new Date();
                this.entity.dataPedido = this.entity.dataPedido != null ? new Date(this.entity.dataPedido.toString()) : null;
                
                if (entity.empresaId) {
                    this.ezSelectEmpresa.loading = true;
                    var promiseEmpresa = this.empresaService.getById(entity.empresaId);
                    promiseEmpresa.then(result => {
                        this.ezSelectEmpresa.setInputText(result.nomeFantasia);
                    }).finally(() => {
                        this.ezSelectEmpresa.loading = false;
                    });
                }

                if (entity.fornecedorId) {
                    this.ezSelectFornecedor.loading = true;
                    var promiseFornecedor = this.fornecedorService.getById(entity.fornecedorId);
                    promiseFornecedor.then(result => {
                        this.ezSelectFornecedor.setInputText(result.pessoa.nomePessoa);
                    }).finally(() => {
                        this.ezSelectFornecedor.loading = false;
                    });
                }               
                 
                // Entrada Itens
                this.$scope.$watchCollection(() => { return this.entity.items }, (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });
                this.inicializarEntradaItemCurrent();
                this.getItemGrid();
            } 

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.ezSelectEmpresaConfig();
            this.ezSelectLocalArmazenamentoConfig();
            this.ezSelectFornecedorConfig();
            this.ezSelectProdutoConfig();

            this.entradaItemGridConfig();
            
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        setEntradaForm(form: ng.IFormController) {
            this.entradaForm = form;
        }

        setEntradaItemForm(form: ng.IFormController) {
            this.entradaItemForm = form;
        }

        private ezSelectEmpresaConfig() {
            this.requestParamsEmpresa = new requestParam.RequestParam.RequestParams();

            this.empresaSelecionado = (registro) => {
                this.entity.empresaId = registro.id;
                return registro.nomeFantasia;
            }

            this.empresaDeselecionado = () => {
                this.entity.empresaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaEmpresa = (termoDigitadoPesquisa) => {
                var filtro = new empresaDtos.Dtos.Empresa.GetEmpresaInput();
                filtro.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEmpresa = new ezSelect.EzSelect.EzSelect<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.empresaService,
                this.getFiltroParaEmpresa,
                this.requestParamsEmpresa,
                this.$uibModal,
                this.empresaSelecionado,
                this.empresaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEmpresa.onEzGridCreated = () => {
                this.ezSelectEmpresa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Empresa.NomePessoa'),
                    field: 'nomeFantasia'
                });

                this.ezSelectEmpresa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectEmpresa.on.beforeOpenModalDialog = () => {
                this.ezSelectEmpresa.ezGrid.getRegistros();
            };
        }

        private ezSelectLocalArmazenamentoConfig() {
            this.requestParamsLocalArmazenamento = new requestParam.RequestParam.RequestParams();

            this.localArmazenamentoSelecionado = (registro) => {
                this.entradaItemCurrent.localArmazenamentoId = registro.id;
                return registro.descricao;
            }

            this.localArmazenamentoDeselecionado = () => {
                this.entradaItemCurrent.localArmazenamentoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaLocalArmazenamento = (termoDigitadoPesquisa) => {
                var filtro = new localArmazenamentoDtos.Dtos.LocalArmazenamento.GetLocalArmazenamentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectLocalArmazenamento = new ezSelect.EzSelect.EzSelect<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.localArmazenamentoService,
                this.getFiltroParaLocalArmazenamento,
                this.requestParamsLocalArmazenamento,
                this.$uibModal,
                this.localArmazenamentoSelecionado,
                this.localArmazenamentoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectLocalArmazenamento.onEzGridCreated = () => {
                this.ezSelectLocalArmazenamento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estoque.EntradaItem.LocalArmazenamento'),
                    field: 'descricao'
                });

                this.ezSelectLocalArmazenamento.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectLocalArmazenamento.on.beforeOpenModalDialog = () => {
                this.ezSelectLocalArmazenamento.ezGrid.getRegistros();
            };
        }

        private ezSelectFornecedorConfig() {
            this.requestParamsEmpresa = new requestParam.RequestParam.RequestParams();

            this.fornecedorSelecionado = (registro) => {
                this.entity.fornecedorId = registro.id;
                return registro.pessoa.nomePessoa;
            }

            this.fornecedorDeselecionado = () => {
                this.entity.fornecedorId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaFornecedor = (termoDigitadoPesquisa) => {
                var filtro = new fornecedorDtos.Dtos.Fornecedor.GetFornecedorInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectFornecedor = new ezSelect.EzSelect.EzSelect<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.fornecedorService,
                this.getFiltroParaEmpresa,
                this.requestParamsEmpresa,
                this.$uibModal,
                this.fornecedorSelecionado,
                this.fornecedorDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectFornecedor.onEzGridCreated = () => {
                this.ezSelectFornecedor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.NomePessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectFornecedor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectFornecedor.on.beforeOpenModalDialog = () => {
                this.ezSelectFornecedor.ezGrid.getRegistros();
            };
        }

        private ezSelectProdutoConfig() {
            this.requestParamsProduto = new requestParam.RequestParam.RequestParams();

            this.produtoSelecionado = (registro) => {
                this.entradaItemCurrent.produtoId = registro.id;
                this.entradaItemCurrent.produto = registro;
                return registro.nome;
            }

            this.produtoDeselecionado = () => {
                this.entradaItemCurrent.produto = null;
                this.entradaItemCurrent.produtoId = 0;
                this.entradaItemCurrent.valorUnitario = 0;
                this.entradaItemCurrent.quantidade = 0;
                this.entradaItemCurrent.validade = new Date();
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoProduto = (termoDigitadoPesquisa) => {
                var filtro = new produtoDtos.Dtos.Produto.GetProdutoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProduto = new ezSelect.EzSelect.EzSelect<produtoDtos.Dtos.Produto.ProdutoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoService,
                this.getFiltroParaPaginacaoProduto,
                this.requestParamsProduto,
                this.$uibModal,
                this.produtoSelecionado,
                this.produtoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProduto.onEzGridCreated = () => {
                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Nome'),
                    field: 'nome'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estoque.EntradaItem.LocalArmazenamento'),
                    field: 'localArmazenamento.descricao'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Valor'),
                    field: 'valor'
                });

                this.ezSelectProduto.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectProduto.on.beforeOpenModalDialog = () => {
                this.ezSelectProduto.ezGrid.getRegistros();
            };
        }

        /* Entrada Item */
        private inicializarEntradaItemCurrent() {
            this.entradaItemCurrent = new entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto();
            this.entradaItemCurrent.valorUnitario = 0;
            this.entradaItemCurrent.quantidade = 1;
            this.entradaItemCurrent.validade = new Date();
            this.entradaItemCurrent.isActive = true;
            if (this.entity)
                this.entradaItemCurrent.entradaId = this.entity.id;
        }

        private isQuantidadeValida(value: any): boolean {
            return value > 0;
        }

        private getEntradaItemByTempId(tempId: number): entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto {
            for (let i = 0; i < this.entity.items.length; i++) {
                if (this.entity.items[i].id === tempId) {
                    return this.entity.items[i];
                }
            }
        }

        private cancelarItem() {
            this.limparEntradaItemCurrent();
            this.editandoItem = false;
        }

        private gravarItem(item: any) {
            this.saving = true;            
            var promise = (this.entradaItemService as any as entradaItemService.Services.EntradaItemService).save(item);
            promise.then(result => {             
                this.entradaItemCurrent.id = result.id;
                if (!this.editandoItem) {
                    this.entity.items.push(this.entradaItemCurrent);
                }                             
                this.inicializarEntradaItemCurrent();
                this.editandoItem = false;
                this.gridDataEnumerable = this.entity.items;
                this.getItemGrid();
                this.limparEntradaItemCurrent();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarItem() {
            this.entradaItemCurrent.total = this.entradaItemCurrent.valorUnitario * this.entradaItemCurrent.quantidade;
            this.entradaItemCurrent.entradaId = this.entity.id;
            if (!this.editandoItem) {
                this.gravarItem(this.entradaItemCurrent);
            }
            else {
                var item = this.getEntradaItemByTempId(this.entradaItemCurrent.id);
                angular.merge(item, this.entradaItemCurrent);                
                this.gravarItem(item);
            }
        }

        private entradaItemGridConfig() {
            this.requestParamsEntradaItem = new requestParam.RequestParam.RequestParams();
            this.requestParamsEntradaItem.sorting = 'Id';

            this.entradaItemAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>();
            this.entradaItemAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarItem, 'Edit', true));
            this.entradaItemAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirItem, 'Delete', true));

            //callbacks
            var callBack: ezGrid.EzGrid.IEzGridAppScopeCallBack = {
                key: 'totalItems',
                fn: this.totalItems
            };
            this.entradaItemAppScopeProvider.addCallBack(callBack);

            this.entradaItemGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.entradaItemAppScopeProvider
            }

            var lista = this.entity ? this.entity.items : new Array<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>();
            this.entradaItemGridOptions.data = lista;
            this.entradaItemGridOptions.totalItems = lista.length;

            this.entradaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.EntradaItem.Produto'),
                field: 'produto.nome',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.entradaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.EntradaItem.ValorUnitario'),
                field: 'valorUnitario',
                cellFilter: 'currency:"R$ "'
            });

            this.entradaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.EntradaItem.Quantidade'),
                field: 'quantidade'
            });

            this.entradaItemGridOptions.columnDefs.push({
                name: app.localize('Estoque.EntradaItem.Total'),
                field: 'total',
                cellFilter: 'currency:"R$ "',
                footerCellTemplate: '<div class="ui-grid-cell-contents">Total: {{grid.appScope.executeCallBack("totalItems")}}</div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EntradaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/entrada/entradaForm.cshtml';
            this.controller = EntradaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}