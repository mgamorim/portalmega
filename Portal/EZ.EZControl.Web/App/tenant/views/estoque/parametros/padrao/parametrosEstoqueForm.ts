﻿import * as parametroGlobalService from "../../../../../services/domain/global/parametro/parametroGlobalService";
import * as conexaoSMTPService from "../../../../../services/domain/global/conexaoSmtp/conexaoSmtpService";
import * as parametroGlobalDtos from "../../../../../dtos/global/parametro/parametroGlobalDtos";
import * as conexaoSmtpGlobalDtos from "../../../../../dtos/global/conexaoSmtp/conexaoSmtpDtos";
import * as requestParam from "../../../../../common/requestParams/requestParams";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ParametrosEstoqueFormController extends
        controllers.Controllers.ControllerFormComponentBase<parametroGlobalDtos.Dtos.Parametro.Global.ParametroGlobalListDto,
        parametroGlobalDtos.Dtos.Parametro.Global.ParametroGlobalInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>

    {
        public ezSelectConexaoSmtp: ezSelect.EzSelect.EzSelect<conexaoSmtpGlobalDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoConexaoSmtp: (termoDigitadoPesquisa: string) => any;
        private conexaoSmtpSelecionado: (registro: conexaoSmtpGlobalDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto) => string;
        private conexaoSmtpDeselecionado: () => void;
        private requestParamsConexaoSmtp: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'parametroGlobalService', 'conexaoSMTPService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public conexaoSmtpService: conexaoSMTPService.Services.ConexaoSmtpService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                parametroGlobalService,
                'tenant.global.parametros');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new parametroGlobalDtos.Dtos.Parametro.Global.ParametroGlobalInput();
                instance.id = 0;
                instance.isActive = true;
                return instance; 
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (entity) {
                    this.ezSelectConexaoSmtp.loading = true;
                    var promise = this.conexaoSmtpService.getById(entity.conexaoSMTPId);
                    promise.then(result => {
                        this.ezSelectConexaoSmtp.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectConexaoSmtp.loading = false;
                    });
                }
            };
            this.ezSelectConexaoSmtpConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectConexaoSmtpConfig() {
            this.requestParamsConexaoSmtp = new requestParam.RequestParam.RequestParams();

            this.conexaoSmtpSelecionado = (registro) => {
                this.entity.conexaoSMTPId = registro.id;
                return registro.nome;
            }

            this.conexaoSmtpDeselecionado = () => {
                this.entity.conexaoSMTPId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoConexaoSmtp = (termoDigitadoPesquisa) => {
                var filtro = new conexaoSmtpGlobalDtos.Dtos.ConexaoSmtp.GetConexaoSmtpInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectConexaoSmtp = new ezSelect.EzSelect.EzSelect<conexaoSmtpGlobalDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.conexaoSmtpService,
                this.getFiltroParaPaginacaoConexaoSmtp,
                this.requestParamsConexaoSmtp,
                this.$uibModal,
                this.conexaoSmtpSelecionado,
                this.conexaoSmtpDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectConexaoSmtp.onEzGridCreated = () => {
                this.ezSelectConexaoSmtp.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ConexaoSMTP.Nome'),
                    field: 'nome'
                });

                this.ezSelectConexaoSmtp.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ConexaoSMTP.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectConexaoSmtp.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ConexaoSMTP.ServidorSMTP'),
                    field: 'servidorSMTP'
                });

                this.ezSelectConexaoSmtp.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ConexaoSMTP.RequerConexaoCriptografada'),
                    field: 'requerConexaoCriptografada',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectConexaoSmtp.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ConexaoSMTP.RequerAutenticacao'),
                    field: 'requerAutenticacao',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectConexaoSmtp.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectConexaoSmtp.on.beforeOpenModalDialog = () => {
                this.ezSelectConexaoSmtp.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ParametrosEstoqueFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/padrao/ParametrosEstoqueForm.cshtml';
            this.controller = ParametrosEstoqueFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}