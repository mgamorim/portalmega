﻿import * as ezGrid from "../../../../../common/ezGrid/ezGrid";
import * as unidadeMedidaService from "../../../../../services/domain/estoque/parametros/unidadeMedidaService";
import * as unidadeMedidaDtos from "../../../../../dtos/estoque/parametro/unidadeMedidaDtos";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../../common/requestParams/requestParams";

export module Estoque.UnidadeMedida {

    class UnidadeMedidaIndexController extends controllers.Controllers.ControllerIndexBase<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaInput>{

        static $inject = ['$state', 'uiGridConstants', 'estoqueUnidadeMedidaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public unidadeMedidaService: unidadeMedidaService.Services.UnidadeMedidaService) {

            super(
                $state,
                uiGridConstants,
                unidadeMedidaService, {
                    rotaAlterarRegistro: 'tenant.estoque.unidadeMedida.alterar',
                    rotaNovoRegistro: 'tenant.estoque.unidadeMedida.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new unidadeMedidaDtos.Dtos.UnidadeMedida.GetUnidadeMedidaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.unidadeMedidaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewUnidadeMedida', this.novo, abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.UnidadeMedida.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.UnidadeMedida.Valor'),
                field: 'valor'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.UnidadeMedida.UnidadePadrao'),
                field: 'unidadePadraoValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.UnidadeMedida.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class UnidadeMedidaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/unidadeMedida/unidadeMedidaIndex.cshtml';
            this.controller = UnidadeMedidaIndexController;
        }
    }
}