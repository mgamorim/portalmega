﻿import * as unidadeMedidaService from "../../../../../services/domain/estoque/parametros/unidadeMedidaService";
import * as unidadeMedidaDtos from "../../../../../dtos/estoque/parametro/unidadeMedidaDtos";
import * as requestParam from "../../../../../common/requestParams/requestParams";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../../common/ezSelect/ezSelect";

export module Forms {
    export class UnidadeMedidaFormController extends controllers.Controllers.ControllerFormComponentBase<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        public unidadePadraoEnum: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'estoqueUnidadeMedidaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public unidadeMedidaService: unidadeMedidaService.Services.UnidadeMedidaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                unidadeMedidaService,
                'tenant.estoque.unidadeMedida');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
            }

            this.unidadePadraoEnum = ez.domain.enum.unidadePadraoEnum.valores;

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class UnidadeMedidaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/unidadeMedida/unidadeMedidaForm.cshtml';
            this.controller = UnidadeMedidaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}