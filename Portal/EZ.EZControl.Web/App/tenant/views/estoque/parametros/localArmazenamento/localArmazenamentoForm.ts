﻿import * as localArmazenamentoService from "../../../../../services/domain/estoque/parametros/localArmazenamentoService";
import * as localArmazenamentoDtos from "../../../../../dtos/estoque/parametro/localArmazenamentoDtos";
import * as empresaService from "../../../../../services/domain/global/empresa/empresaService";
import * as empresaDtos from "../../../../../dtos/global/empresa/empresaDtos";
import * as requestParam from "../../../../../common/requestParams/requestParams";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../../common/ezSelect/ezSelect";

export module Forms {
    export class LocalArmazenamentoFormController extends controllers.Controllers.ControllerFormComponentBase<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        public localArmazenamentoForm: ng.IFormController;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;

        public ezSelectEmpresa: ezSelect.EzSelect.EzSelect<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaEmpresa: (termoDigitadoPesquisa: string) => any;
        private empresaSelecionado: (registro: empresaDtos.Dtos.Empresa.EmpresaListDto) => string;
        private empresaDeselecionado: () => void;
        private requestParamsEmpresa: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'estoqueLocalArmazenamentoService', 'empresaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public localArmazenamentoService: localArmazenamentoService.Services.LocalArmazenamentoService,
            public empresaService: empresaService.Services.EmpresaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                localArmazenamentoService,
                'tenant.estoque.localArmazenamento');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                if (entity.empresaId) {
                    this.ezSelectEmpresa.loading = true;
                    var promiseEmpresa = this.empresaService.getById(entity.empresaId);
                    promiseEmpresa.then(result => {                        
                        this.ezSelectEmpresa.setInputText(result.nomeFantasia);
                    }).finally(() => {
                        this.ezSelectEmpresa.loading = false;
                    });
                }
            }

            this.ezSelectEmpresaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectEmpresaConfig() {
            this.requestParamsEmpresa = new requestParam.RequestParam.RequestParams();
            //this.requestParamsEmpresa.sorting = "NomePessoa";

            this.empresaSelecionado = (registro) => {
                this.entity.empresaId = registro.id;
                return registro.nomeFantasia;
            }

            this.empresaDeselecionado = () => {
                this.entity.empresaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaEmpresa = (termoDigitadoPesquisa) => {
                var filtro = new empresaDtos.Dtos.Empresa.GetEmpresaInput();
                filtro.nomeFantasia = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectEmpresa = new ezSelect.EzSelect.EzSelect<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.empresaService,
                this.getFiltroParaEmpresa,
                this.requestParamsEmpresa,
                this.$uibModal,
                this.empresaSelecionado,
                this.empresaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEmpresa.onEzGridCreated = () => {
                this.ezSelectEmpresa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'nomeFantasia'
                });

                this.ezSelectEmpresa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectEmpresa.on.beforeOpenModalDialog = () => {
                this.ezSelectEmpresa.ezGrid.getRegistros();
            };
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.localArmazenamentoForm = form;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class LocalArmazenamentoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/localArmazenamento/localArmazenamentoForm.cshtml';
            this.controller = LocalArmazenamentoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}