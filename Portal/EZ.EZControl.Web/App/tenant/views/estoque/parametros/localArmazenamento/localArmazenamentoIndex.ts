﻿import * as ezGrid from "../../../../../common/ezGrid/ezGrid";
import * as localArmazenamentoService from "../../../../../services/domain/estoque/parametros/localArmazenamentoService";
import * as localArmazenamentoDtos from "../../../../../dtos/estoque/parametro/localArmazenamentoDtos";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../../common/requestParams/requestParams";

export module Estoque.LocalArmazenamento {

    class LocalArmazenamentoIndexController extends controllers.Controllers.ControllerIndexBase<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoInput>{

        static $inject = ['$state', 'uiGridConstants', 'estoqueLocalArmazenamentoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public localArmazenamentoService: localArmazenamentoService.Services.LocalArmazenamentoService) {

            super(
                $state,
                uiGridConstants,
                localArmazenamentoService, {
                    rotaAlterarRegistro: 'tenant.estoque.localArmazenamento.alterar',
                    rotaNovoRegistro: 'tenant.estoque.localArmazenamento.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new localArmazenamentoDtos.Dtos.LocalArmazenamento.GetLocalArmazenamentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.localArmazenamentoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewLocalArmazenamento', this.novo, abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.LocalArmazenamento.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.LocalArmazenamento.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.LocalArmazenamento.Empresa'),
                field: 'empresa.pessoaJuridica.nomeFantasia'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.LocalArmazenamento.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class LocalArmazenamentoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/localArmazenamento/localArmazenamentoIndex.cshtml';
            this.controller = LocalArmazenamentoIndexController;
        }
    }
}