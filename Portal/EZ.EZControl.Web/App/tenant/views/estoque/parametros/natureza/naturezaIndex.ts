﻿import * as ezGrid from "../../../../../common/ezGrid/ezGrid";
import * as naturezaService from "../../../../../services/domain/estoque/parametros/naturezaService";
import * as naturezaDtos from "../../../../../dtos/estoque/parametro/naturezaDtos";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../../common/requestParams/requestParams";

export module Estoque.Natureza {

    class NaturezaIndexController extends controllers.Controllers.ControllerIndexBase<naturezaDtos.Dtos.Natureza.NaturezaListDto, naturezaDtos.Dtos.Natureza.NaturezaInput>{

        static $inject = ['$state', 'uiGridConstants', 'estoqueNaturezaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<naturezaDtos.Dtos.Natureza.NaturezaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public naturezaService: naturezaService.Services.NaturezaService) {

            super(
                $state,
                uiGridConstants,
                naturezaService, {
                    rotaAlterarRegistro: 'tenant.estoque.natureza.alterar',
                    rotaNovoRegistro: 'tenant.estoque.natureza.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new naturezaDtos.Dtos.Natureza.GetNaturezaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<naturezaDtos.Dtos.Natureza.NaturezaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.naturezaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewNatureza', this.novo, abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Natureza.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Natureza.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Natureza.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.Natureza.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.Natureza.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class NaturezaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/natureza/naturezaIndex.cshtml';
            this.controller = NaturezaIndexController;
        }
    }
}