﻿import * as ezGrid from "../../../../../common/ezGrid/ezGrid";
import * as fracaoService from "../../../../../services/domain/estoque/parametros/fracaoService";
import * as fracaoDtos from "../../../../../dtos/estoque/parametro/fracaoDtos";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../../common/requestParams/requestParams";

export module Estoque.Fracao {

    class FracaoIndexController extends controllers.Controllers.ControllerIndexBase<fracaoDtos.Dtos.Fracao.FracaoListDto, fracaoDtos.Dtos.Fracao.FracaoInput>{

        static $inject = ['$state', 'uiGridConstants', 'estoqueFracaoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<fracaoDtos.Dtos.Fracao.FracaoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public fracaoService: fracaoService.Services.FracaoService) {

            super(
                $state,
                uiGridConstants,
                fracaoService, {
                    rotaAlterarRegistro: 'tenant.estoque.fracao.alterar',
                    rotaNovoRegistro: 'tenant.estoque.fracao.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new fracaoDtos.Dtos.Fracao.GetFracaoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<fracaoDtos.Dtos.Fracao.FracaoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.fracaoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewFracao', this.novo, abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Fracao.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Fracao.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.Fracao.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.Fracao.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.Fracao.UnidadeMedida'),
                field: 'unidadeMedida.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.Fracao.Valor'),
                field: 'valor'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estoque.Fracao.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class FracaoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/fracao/fracaoIndex.cshtml';
            this.controller = FracaoIndexController;
        }
    }
}