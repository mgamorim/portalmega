﻿import * as fracaoService from "../../../../../services/domain/estoque/parametros/fracaoService";
import * as fracaoDtos from "../../../../../dtos/estoque/parametro/fracaoDtos";
import * as unidadeMedidaService from "../../../../../services/domain/estoque/parametros/unidadeMedidaService";
import * as unidadeMedidaDtos from "../../../../../dtos/estoque/parametro/unidadeMedidaDtos";
import * as requestParam from "../../../../../common/requestParams/requestParams";
import * as controllers from "../../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../../common/ezSelect/ezSelect";

export module Forms {
    export class FracaoFormController extends controllers.Controllers.ControllerFormComponentBase<fracaoDtos.Dtos.Fracao.FracaoListDto, fracaoDtos.Dtos.Fracao.FracaoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;

        public ezSelectUnidadeMedida: ezSelect.EzSelect.EzSelect<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaUnidadeMedida: (termoDigitadoPesquisa: string) => any;
        private unidadeMedidaSelecionado: (registro: unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto) => string;
        private unidadeMedidaDeselecionado: () => void;
        private requestParamsUnidadeMedida: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'estoqueFracaoService', 'estoqueUnidadeMedidaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public fracaoService: fracaoService.Services.FracaoService,
            public unidadeMedidaService: unidadeMedidaService.Services.UnidadeMedidaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                fracaoService,
                'tenant.estoque.fracao');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new fracaoDtos.Dtos.Fracao.FracaoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                if (entity.unidadeMedidaId) {
                    this.ezSelectUnidadeMedida.loading = true;
                    var promiseUnidadeMedida = this.unidadeMedidaService.getById(entity.unidadeMedidaId);
                    promiseUnidadeMedida.then(result => {
                        this.ezSelectUnidadeMedida.setInputText(result.descricao);
                    }).finally(() => {
                        this.ezSelectUnidadeMedida.loading = false;
                    });
                }
            }

            this.ezSelectUnidadeMedidaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectUnidadeMedidaConfig() {
            this.requestParamsUnidadeMedida = new requestParam.RequestParam.RequestParams();

            this.unidadeMedidaSelecionado = (registro) => {
                this.entity.unidadeMedidaId = registro.id;
                return registro.descricao;
            }

            this.unidadeMedidaDeselecionado = () => {
                this.entity.unidadeMedidaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaUnidadeMedida = (termoDigitadoPesquisa) => {
                var filtro = new unidadeMedidaDtos.Dtos.UnidadeMedida.GetUnidadeMedidaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectUnidadeMedida = new ezSelect.EzSelect.EzSelect<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.unidadeMedidaService,
                this.getFiltroParaUnidadeMedida,
                this.requestParamsUnidadeMedida,
                this.$uibModal,
                this.unidadeMedidaSelecionado,
                this.unidadeMedidaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectUnidadeMedida.onEzGridCreated = () => {
                this.ezSelectUnidadeMedida.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estoque.UnidadeMedida.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectUnidadeMedida.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectUnidadeMedida.on.beforeOpenModalDialog = () => {
                this.ezSelectUnidadeMedida.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class FracaoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/estoque/parametros/fracao/fracaoForm.cshtml';
            this.controller = FracaoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}