﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraService from "../../../../services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraService";
import * as corretoraService from "../../../../services/domain/ezliv/corretora/corretoraService";
import * as produtoDePlanoDeSaudeService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as produtoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";

import * as entidadeService from "../../../../services/domain/ezliv/associacao/associacaoService";
import * as entidadeDtos from "../../../../dtos/ezliv/subtipoPessoa/associacao/associacaoDtos";

export module Forms {
    export class PermissaoDeVendaDePlanoDeSaudeParaCorretoraFormController extends controllers.Controllers.ControllerFormComponentBase<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        private createDescription: () => any;

        public ezSelectCorretora: ezSelect.EzSelect.EzSelect<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaCorretora: (termoDigitadoPesquisa: string) => any;
        private corretoraSelecionado: (registro: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto) => string;
        private corretoraDeselecionado: () => void;
        private requestParamsCorretora: requestParam.RequestParam.RequestParams;

        private idsCorretoras: number[];

        public saving: boolean;
        public editing: boolean;
        public excluir: (valor: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto) => void;
        public excluirProduto: (valor: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => void;
        public limparEntidade: () => void;
        public addCorretora: () => void;
        public addProduto: () => void;

        private setDataGridInput: (result: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto[]) => void;
        private gridApiInput: uiGrid.IGridApiOf<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>;
        private requestParamsCorretorasInput: requestParam.RequestParam.RequestParams;
        private corretorasAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>;
        public getCorretorasInput: () => void;
        public loadingValoresInput: boolean;
        public corretorasGridOptionsInput: uiGrid.IGridOptionsOf<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>;
        private gridDataInput: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto[];

        private setDataGridProdutoInput: (result: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[]) => void;
        private gridApiProdutoInput: uiGrid.IGridApiOf<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>;
        private requestParamsProdutoDePlanoDeSaudesInput: requestParam.RequestParam.RequestParams;
        private produtoDePlanoDeSaudesAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>;
        public getProdutoDePlanoDeSaudesInput: () => void;
        public loadingValoresProdutoInput: boolean;
        public produtoDePlanoDeSaudesGridOptionsInput: uiGrid.IGridOptionsOf<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>;
        private gridProdutoDataInput: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];

        public corretoraCurrent: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto;
        public produtoDePlanoDeSaudeCurrent: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto;

        public ezSelectProdutoDePlanoDeSaude: ezSelect.EzSelect.EzSelect<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaProdutoDePlanoDeSaude: (termoDigitadoPesquisa: string) => any;
        private produtoDePlanoDeSaudeSelecionado: (registro: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => string;
        private produtoDePlanoDeSaudeDeselecionado: () => void;
        private requestParamsProdutoDePlanoDeSaude: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'permissaoDeVendaDePlanoDeSaudeParaCorretoraService',  'produtoDePlanoDeSaudeService', 'corretoraService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public permissaoDeVendaDePlanoDeSaudeParaCorretoraService: permissaoDeVendaDePlanoDeSaudeParaCorretoraService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretoraService,
            public produtoDePlanoDeSaudeService: produtoDePlanoDeSaudeService.Services.ProdutoDePlanoDeSaudeService,
            public corretoraService: corretoraService.Services.CorretoraService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                permissaoDeVendaDePlanoDeSaudeParaCorretoraService,
                'tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora');

            this.getCorretorasInput = () => {
                this.loadingValoresInput = true;
                if (this.entity) {
                    this.setDataGridInput(this.entity.corretoras);
                }

                this.loadingValoresInput = false;
            }

            this.getProdutoDePlanoDeSaudesInput = () => {
                this.loadingValoresProdutoInput = true;
                if (this.entity) {
                    this.setDataGridProdutoInput(this.entity.produtos);
                }

                this.loadingValoresProdutoInput = false;
            }

            this.setDataGridInput = (result: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto[]) => {
                this.gridDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInput.push(result[i]);
                }

                this.corretorasGridOptionsInput.totalItems = result.length;
            };

            this.setDataGridProdutoInput = (result: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[]) => {
                this.gridProdutoDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridProdutoDataInput.push(result[i]);
                }

                this.produtoDePlanoDeSaudesGridOptionsInput.totalItems = result.length;
            };

            this.excluir = (valor: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto) => {
                this.editing = false;
                abp.message.confirm(
                    'Deseja remover essa corretora?', 'Confirmação',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.corretoras.length; i++) {
                                if (this.entity.corretoras[i].id === valor.id) {
                                    this.entity.corretoras.splice(i, 1);
                                    this.getCorretorasInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.excluirProduto = (valor: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => {
                this.editing = false;
                abp.message.confirm(
                    'Deseja remover esse produto?', 'Confirmação',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.produtos.length; i++) {
                                if (this.entity.produtos[i].id === valor.id) {
                                    this.entity.produtos.splice(i, 1);
                                    this.getProdutoDePlanoDeSaudesInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.addCorretora = () => {
                this.entity.corretoras.push(this.corretoraCurrent);
                this.getCorretorasInput();
                this.corretoraCurrent = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto();
                this.ezSelectCorretora.setInputText('');
            }

            this.addProduto = () => {
                this.entity.produtos.push(this.produtoDePlanoDeSaudeCurrent);
                this.getProdutoDePlanoDeSaudesInput();
                this.produtoDePlanoDeSaudeCurrent = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto();
                this.ezSelectProdutoDePlanoDeSaude.setInputText('');
            }
        }

        $onInit() {
            super.init();
            this.corretoraCurrent = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto();
            this.produtoDePlanoDeSaudeCurrent = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput();
                instance.id = 0;
                instance.isActive = true;
                instance.corretoras = new Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>();
                instance.produtos = new Array<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>();
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                this.getCorretorasInput();
                this.getProdutoDePlanoDeSaudesInput();
            }

            this.ezSelectCorretoraConfig();
            this.ezSelectProdutoDePlanoDeSaudeConfig();
            this.corretoraGridConfigInput();
            this.produtoDePlanoDeSaudeGridConfigInput();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectCorretoraConfig() {
            this.requestParamsCorretora = new requestParam.RequestParam.RequestParams();

            this.corretoraSelecionado = (registro) => {
                this.corretoraCurrent = registro;
                return registro.nome;
            }

            this.corretoraDeselecionado = () => {
                this.corretoraCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaCorretora = (termoDigitadoPesquisa) => {
                var filtro = new corretoraDtos.Dtos.EZLiv.Geral.GetCorretoraInput();
                let exceptIds = new Array();
                this.entity.corretoras.forEach(x => exceptIds.push(x.id));

                filtro.ids = exceptIds;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCorretora = new ezSelect.EzSelect.EzSelect<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretoraService,
                this.getFiltroParaCorretora,
                this.requestParamsCorretora,
                this.$uibModal,
                this.corretoraSelecionado,
                this.corretoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCorretora.onEzGridCreated = () => {

                this.ezSelectCorretora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretora.NomeFantasia'),
                    field: 'nome'
                });

                this.ezSelectCorretora.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCorretora.on.beforeOpenModalDialog = () => {
                this.ezSelectCorretora.ezGrid.getRegistros();
            };
        }

        private ezSelectProdutoDePlanoDeSaudeConfig() {
            this.requestParamsProdutoDePlanoDeSaude = new requestParam.RequestParam.RequestParams();

            this.produtoDePlanoDeSaudeSelecionado = (registro) => {
                this.produtoDePlanoDeSaudeCurrent = registro;
                return registro.nome;
            }

            this.produtoDePlanoDeSaudeDeselecionado = () => {
                this.produtoDePlanoDeSaudeCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaProdutoDePlanoDeSaude = (termoDigitadoPesquisa) => {
                var filtro = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput();
                let exceptIds = new Array();
                this.entity.produtos.forEach(x => exceptIds.push(x.id));

                filtro.exceptIds = exceptIds;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProdutoDePlanoDeSaude = new ezSelect.EzSelect.EzSelect<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoDePlanoDeSaudeService,
                this.getFiltroParaProdutoDePlanoDeSaude,
                this.requestParamsProdutoDePlanoDeSaude,
                this.$uibModal,
                this.produtoDePlanoDeSaudeSelecionado,
                this.produtoDePlanoDeSaudeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProdutoDePlanoDeSaude.onEzGridCreated = () => {

                this.ezSelectProdutoDePlanoDeSaude.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.Nome'),
                    field: 'nome'
                });

                this.ezSelectProdutoDePlanoDeSaude.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectProdutoDePlanoDeSaude.on.beforeOpenModalDialog = () => {
                this.ezSelectProdutoDePlanoDeSaude.ezGrid.getRegistros();
            };
        }

        private corretoraGridConfigInput() {
            this.requestParamsCorretorasInput = new requestParam.RequestParam.RequestParams();

            this.corretorasAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>();

            this.corretorasAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridDataInput = new Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>();

            this.corretorasGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.corretorasAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInput = api;
                    this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsCorretorasInput.sorting = null;
                        } else {
                            this.requestParamsCorretorasInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getCorretorasInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsCorretorasInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsCorretorasInput.maxResultCount = pageSize;

                        this.getCorretorasInput();
                    });
                }
            }

            this.corretorasGridOptionsInput.data = this.gridDataInput;

            this.corretorasGridOptionsInput.columnDefs.push({
                name: app.localize('Corretora.Nome'),
                field: 'nome',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.getCorretorasInput();
        }

        private produtoDePlanoDeSaudeGridConfigInput() {
            this.requestParamsProdutoDePlanoDeSaude = new requestParam.RequestParam.RequestParams();

            this.produtoDePlanoDeSaudesAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>();

            this.produtoDePlanoDeSaudesAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirProduto, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridProdutoDataInput = new Array<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>();

            this.produtoDePlanoDeSaudesGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.produtoDePlanoDeSaudesAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiProdutoInput = api;
                    this.gridApiProdutoInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsProdutoDePlanoDeSaudesInput.sorting = null;
                        } else {
                            this.requestParamsProdutoDePlanoDeSaudesInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getProdutoDePlanoDeSaudesInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsProdutoDePlanoDeSaudesInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsProdutoDePlanoDeSaudesInput.maxResultCount = pageSize;

                        this.getProdutoDePlanoDeSaudesInput();
                    });
                }
            }

            this.produtoDePlanoDeSaudesGridOptionsInput.data = this.gridProdutoDataInput;

            this.produtoDePlanoDeSaudesGridOptionsInput.columnDefs.push({
                name: app.localize('PessoaFisica.Nome'),
                field: 'nome',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.getProdutoDePlanoDeSaudesInput();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PermissaoDeVendaDePlanoDeSaudeParaCorretoraFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraForm.cshtml';
            this.controller = PermissaoDeVendaDePlanoDeSaudeParaCorretoraFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}