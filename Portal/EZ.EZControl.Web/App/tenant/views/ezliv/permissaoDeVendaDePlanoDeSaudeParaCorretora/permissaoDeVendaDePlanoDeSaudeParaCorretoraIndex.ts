﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraService from "../../../../services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraService";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora {

    class PermissaoDeVendaDePlanoDeSaudeParaCorretoraIndexController extends controllers.Controllers.ControllerIndexBase <permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>{

        static $inject = ['$state', 'uiGridConstants', 'permissaoDeVendaDePlanoDeSaudeParaCorretoraService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public permissaoDeVendaDePlanoDeSaudeParaCorretoraService: permissaoDeVendaDePlanoDeSaudeParaCorretoraService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretoraService) {

            super(
                $state,
                uiGridConstants,
                permissaoDeVendaDePlanoDeSaudeParaCorretoraService, {
                    rotaAlterarRegistro: 'tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.permissaoDeVendaDePlanoDeSaudeParaCorretoraService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPermissaoDeVendaDePlanoDeSaudeParaCorretora', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PermissaoDeVendaDePlanoDeSaudeParaCorretora.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class PermissaoDeVendaDePlanoDeSaudeParaCorretoraIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraIndex.cshtml';
            this.controller = PermissaoDeVendaDePlanoDeSaudeParaCorretoraIndexController;
        }
    }
}