﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as contratoService from "../../../../services/domain/ezliv/contrato/contratoService";
import * as contratoDtos from "../../../../dtos/ezliv/geral/contrato/contratoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.Contrato {

    class ContratoIndexController extends controllers.Controllers.ControllerIndexBase <contratoDtos.Dtos.EZLiv.Geral.ContratoListDto, contratoDtos.Dtos.EZLiv.Geral.ContratoInput>{

        static $inject = ['$state', 'uiGridConstants', 'contratoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<contratoDtos.Dtos.EZLiv.Geral.ContratoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public contratoService: contratoService.Services.ContratoService) {

            super(
                $state,
                uiGridConstants,
                contratoService, {
                    rotaAlterarRegistro: 'tenant.ezliv.contrato.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.contrato.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new contratoDtos.Dtos.EZLiv.Geral.GetContratoInput();
                filtro.conteudo = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<contratoDtos.Dtos.EZLiv.Geral.ContratoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.contratoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewContrato', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Contrato.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Contrato.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Contrato.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoDePlanoDeSaude'),
                field: 'produtoDePlanoDeSaude.nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contrato.Operadora'),
                field: 'produtoDePlanoDeSaude.operadora.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contrato.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contrato.DataCriacao'),
                cellFilter: 'date:\'dd/MM/yyyy\'',
                field: 'dataCriacao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contrato.DataInicioVigencia'),
                cellFilter: 'date:\'dd/MM/yyyy\'',
                field: 'dataInicioVigencia'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contrato.DataFimVigencia'),
                cellFilter: 'date:\'dd/MM/yyyy\'',
                field: 'dataFimVigencia'
            });
        }
    }

    export class ContratoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/contrato/contratoIndex.cshtml';
            this.controller = ContratoIndexController;
        }
    }
}