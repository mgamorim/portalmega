﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as contratoService from "../../../../services/domain/ezliv/contrato/contratoService";
import * as produtoDePlanoDeSaudeService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as faixaEtariaService from "../../../../services/domain/ezliv/faixaEtaria/faixaEtariaService";
import * as valorPorFaixaEtariaService from "../../../../services/domain/ezliv/valorPorFaixaEtaria/valorPorFaixaEtariaService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as contratoDtos from "../../../../dtos/ezliv/geral/contrato/contratoDtos";
import * as produtoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as faixaEtariaDtos from "../../../../dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";
import * as valorPorFaixaEtariaDtos from "../../../../dtos/ezliv/geral/valorPorFaixaEtaria/valorPorFaixaEtariaDtos";

export module Forms {
    export class ContratoFormController extends controllers.Controllers.ControllerFormComponentBase<contratoDtos.Dtos.EZLiv.Geral.ContratoListDto, contratoDtos.Dtos.EZLiv.Geral.ContratoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        private createDescription: () => any;

        public ezSelectProdutoDePlanoDeSaude: ezSelect.EzSelect.EzSelect<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaProdutoDePlanoDeSaude: (termoDigitadoPesquisa: string) => any;
        private produtoDePlanoDeSaudeSelecionado: (registro: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => string;
        private produtoDePlanoDeSaudeDeselecionado: () => void;
        private requestParamsProdutoDePlanoDeSaude: requestParam.RequestParam.RequestParams;

        public ezSelectFaixaEtaria: ezSelect.EzSelect.EzSelect<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaFaixaEtaria: (termoDigitadoPesquisa: string) => any;
        private faixaEtariaSelecionado: (registro: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto) => string;
        private faixaEtariaDeselecionado: () => void;
        private requestParamsFaixaEtaria: requestParam.RequestParam.RequestParams;

        private idsFaixas: number[];

        public saving: boolean;
        public editing: boolean;
        public editar: (valor: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto) => void;
        public excluir: (valor: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto) => void;
        public limparEntidade: () => void;
        public addValorPorFaixa: () => void;
        public cancelEditValor: () => void;

        private setDataGridInput: (result: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput[]) => void;
        private gridApiInput: uiGrid.IGridApiOf<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput>;
        private requestParamsValoresInput: requestParam.RequestParam.RequestParams;
        private valoresAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput>;
        public getValoresInput: () => void;
        public loadingValoresInput: boolean;
        public valoresGridOptionsInput: uiGrid.IGridOptionsOf<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput>;
        private gridDataInput: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput[];

        public valorPorFaixaEtariaCurrent: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'contratoService', 'produtoDePlanoDeSaudeService', 'faixaEtariaService', 'valorPorFaixaEtariaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public contratoService: contratoService.Services.ContratoService,
            public produtoDePlanoDeSaudeService: produtoDePlanoDeSaudeService.Services.ProdutoDePlanoDeSaudeService,
            public faixaEtariaService: faixaEtariaService.Services.FaixaEtariaService,
            public valorPorFaixaEtariaService: valorPorFaixaEtariaService.Services.ValorPorFaixaEtariaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                contratoService,
                'tenant.ezliv.contrato');

            this.getValoresInput = () => {
                this.loadingValoresInput = true;
                if (this.entity) {
                    this.setDataGridInput(this.entity.valores);
                }

                this.loadingValoresInput = false;
            }

            this.setDataGridInput = (result: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput[]) => {
                this.gridDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInput.push(result[i]);
                }

                this.valoresGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditValor = () => {
                this.valorPorFaixaEtariaCurrent = new valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput;
                this.ezSelectFaixaEtaria.gridSelectClear();
            }

            this.editar = (valor: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto) => {
                this.editing = true;
                this.valorPorFaixaEtariaCurrent = valor;
                this.ezSelectFaixaEtaria.setInputText(valor.faixaEtaria.descricao);
                this.loadingValoresInput = false;
            }

            this.excluir = (valor: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto) => {
                this.editing = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.valores.length; i++) {
                                if (this.entity.valores[i].faixaEtariaId === valor.faixaEtariaId &&
                                    this.entity.valores[i].valor === valor.valor) {
                                    this.entity.valores.splice(i, 1);
                                    this.getValoresInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.limparEntidade = () => {
                this.valorPorFaixaEtariaCurrent = new valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput();
                this.ezSelectFaixaEtaria.gridSelectClear();
            }

            this.addValorPorFaixa = () => {
                if (this.editing) {
                    this.cancelEditValor();
                } else {
                    this.valorPorFaixaEtariaCurrent.isActive = true;
                    this.valorPorFaixaEtariaCurrent.contratoId = this.entity.id;
                    this.entity.valores.push(this.valorPorFaixaEtariaCurrent);
                    this.getValoresInput();
                    this.limparEntidade();
                }
            }
        }

        $onInit() {
            super.init();
            this.valorPorFaixaEtariaCurrent = new valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput();
            this.valorPorFaixaEtariaCurrent.isActive = true;
            this.valorPorFaixaEtariaCurrent.faixaEtaria = new faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new contratoDtos.Dtos.EZLiv.Geral.ContratoInput();
                instance.id = 0;
                instance.isActive = true;
                instance.valores = new Array<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput>();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                this.acertarDatas();

                if (entity.produtoDePlanoDeSaudeId) {
                    this.ezSelectProdutoDePlanoDeSaude.loading = true;
                    var promiseProduDePlanoDeSaude = this.produtoDePlanoDeSaudeService.getById(entity.produtoDePlanoDeSaudeId);
                    promiseProduDePlanoDeSaude.then(result => {
                        this.ezSelectProdutoDePlanoDeSaude.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectProdutoDePlanoDeSaude.loading = false;
                    });
                }

                this.getValoresInput();
            }


            this.ezSelectProdutoDePlanoDeSaudeConfig();
            this.ezSelectFaixaEtariaConfig();
            this.valorGridConfigInput();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectProdutoDePlanoDeSaudeConfig() {
            this.requestParamsProdutoDePlanoDeSaude = new requestParam.RequestParam.RequestParams();

            this.produtoDePlanoDeSaudeSelecionado = (registro) => {
                this.entity.produtoDePlanoDeSaudeId = registro.id;
                return registro.nome;
            }

            this.produtoDePlanoDeSaudeDeselecionado = () => {
                this.entity.produtoDePlanoDeSaudeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaProdutoDePlanoDeSaude = (termoDigitadoPesquisa) => {
                var filtro = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProdutoDePlanoDeSaude = new ezSelect.EzSelect.EzSelect<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoDePlanoDeSaudeService,
                this.getFiltroParaProdutoDePlanoDeSaude,
                this.requestParamsProdutoDePlanoDeSaude,
                this.$uibModal,
                this.produtoDePlanoDeSaudeSelecionado,
                this.produtoDePlanoDeSaudeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProdutoDePlanoDeSaude.onEzGridCreated = () => {

                this.ezSelectProdutoDePlanoDeSaude.ezGrid.serviceGetCallback = () => {
                    return this.produtoDePlanoDeSaudeService.getProdutoDePlanoDeSaudeExceptForContratoActive();
                };

                this.ezSelectProdutoDePlanoDeSaude.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Nome'),
                    field: 'nome'
                });

                this.ezSelectProdutoDePlanoDeSaude.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectProdutoDePlanoDeSaude.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectProdutoDePlanoDeSaude.on.beforeOpenModalDialog = () => {
                this.ezSelectProdutoDePlanoDeSaude.ezGrid.getRegistros();
            };
        }

        private ezSelectFaixaEtariaConfig() {
            this.requestParamsFaixaEtaria = new requestParam.RequestParam.RequestParams();

            this.faixaEtariaSelecionado = (registro) => {
                this.valorPorFaixaEtariaCurrent.faixaEtariaId = registro.id;
                this.valorPorFaixaEtariaCurrent.faixaEtaria = registro;
                return registro.descricao;
            }

            this.faixaEtariaDeselecionado = () => {
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaFaixaEtaria = (termoDigitadoPesquisa) => {
                var filtro = new faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectFaixaEtaria = new ezSelect.EzSelect.EzSelect<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.faixaEtariaService,
                this.getFiltroParaFaixaEtaria,
                this.requestParamsFaixaEtaria,
                this.$uibModal,
                this.faixaEtariaSelecionado,
                this.faixaEtariaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectFaixaEtaria.onEzGridCreated = () => {

                this.ezSelectFaixaEtaria.ezGrid.serviceGetCallback = () => {
                    this.idsFaixas = new Array();
                    this.entity.valores.forEach(x => this.idsFaixas.push(x.faixaEtariaId));
                    var getids = new faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaExceptForIdsInput();
                    getids.ids = this.idsFaixas;

                    return this.faixaEtariaService.getPaginadoExceptForIds(getids);
                };

                this.ezSelectFaixaEtaria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('FaixaEtaria.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectFaixaEtaria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('FaixaEtaria.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectFaixaEtaria.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectFaixaEtaria.on.beforeOpenModalDialog = () => {
                this.ezSelectFaixaEtaria.ezGrid.getRegistros();
            };
        }

        private acertarDatas() {
            if (this.entity) {
                this.entity.dataInicioVigencia = new Date(this.entity.dataInicioVigencia.toString());
                this.entity.dataFimVigencia = new Date(this.entity.dataFimVigencia.toString());
            }
        }

        private valorGridConfigInput() {
            this.requestParamsValoresInput = new requestParam.RequestParam.RequestParams();

            this.valoresAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput>();

            this.valoresAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ValorPorFaixaEtaria.Edit');
                }
            ));

            this.valoresAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ValorPorFaixaEtaria.Delete');
                }
            ));

            this.gridDataInput = new Array<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput>();

            this.valoresGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.valoresAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInput = api;
                    this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInput.sorting = null;
                        } else {
                            this.requestParamsValoresInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getValoresInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInput.maxResultCount = pageSize;

                        this.getValoresInput();
                    });
                }
            }

            this.valoresGridOptionsInput.data = this.gridDataInput;


            this.valoresGridOptionsInput.columnDefs.push({
                name: app.localize('FaixaEtaria.Descricao'),
                field: 'faixaEtaria.descricao',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.valoresGridOptionsInput.columnDefs.push({
                name: app.localize('FaixaEtaria.Valor'),
                field: 'valor',
                cellFilter: 'currency:"R$ "'
            });


            this.getValoresInput();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ContratoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/contrato/contratoForm.cshtml';
            this.controller = ContratoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}