﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as faixaEtariaService from "../../../../services/domain/ezliv/faixaEtaria/faixaEtariaService";
import * as faixaEtariaDtos from "../../../../dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.FaixaEtaria {

    class FaixaEtariaIndexController extends controllers.Controllers.ControllerIndexBase <faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput>{

        static $inject = ['$state', 'uiGridConstants', 'faixaEtariaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public faixaEtariaService: faixaEtariaService.Services.FaixaEtariaService) {

            super(
                $state,
                uiGridConstants,
                faixaEtariaService, {
                    rotaAlterarRegistro: 'tenant.ezliv.faixaEtaria.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.faixaEtaria.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaInput();
                filtro.descricao = termoDigitadoPesquisa; 
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.faixaEtariaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewFaixaEtaria', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.FaixaEtaria.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.FaixaEtaria.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.FaixaEtaria.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FaixaEtaria.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FaixaEtaria.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FaixaEtaria.IdadeInicial'),
                field: 'idadeInicial'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FaixaEtaria.IdadeFinal'),
                field: 'idadeFinal'
            });
        }
    }

    export class FaixaEtariaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/faixaEtaria/faixaEtariaIndex.cshtml';
            this.controller = FaixaEtariaIndexController;
        }
    }
}