﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as faixaEtariaService from "../../../../services/domain/ezliv/faixaEtaria/faixaEtariaService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as faixaEtariaDtos from "../../../../dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";

export module Forms {
    export class FaixaEtariaFormController extends controllers.Controllers.ControllerFormComponentBase<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        private createDescription: () => any;
        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'faixaEtariaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public faixaEtariaService: faixaEtariaService.Services.FaixaEtariaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                faixaEtariaService,
                'tenant.ezliv.faixaEtaria');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
            }

            this.createDescription = () => {
                var idadeInicial = this.entity.idadeInicial === undefined || this.entity.idadeInicial === null ? "" : this.entity.idadeInicial;
                var idadeFinal = this.entity.idadeFinal === undefined || this.entity.idadeFinal === null ? "" : this.entity.idadeFinal;
                if (idadeInicial === "" && idadeFinal === "") {
                    this.entity.descricao = "";
                } else {
                    if (idadeFinal === "") {
                        this.entity.descricao = "Acima de " + idadeInicial;
                    } else {
                        this.entity.descricao = idadeInicial + " A " + idadeFinal;
                    }
                }
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class FaixaEtariaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/faixaEtaria/faixaEtariaForm.cshtml';
            this.controller = FaixaEtariaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}