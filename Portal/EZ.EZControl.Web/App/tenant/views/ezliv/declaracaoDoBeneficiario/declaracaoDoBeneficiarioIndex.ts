﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as declaracaoDoBeneficiarioService from "../../../../services/domain/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioService";
import * as declaracaoDoBeneficiarioDtos from "../../../../dtos/ezliv/geral/declaracaoDoBeneficiario/declaracaoDoBeneficiarioDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.DeclaracaoDoBeneficiario {

    class DeclaracaoDoBeneficiarioIndexController extends controllers.Controllers.ControllerIndexBase <declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto, declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioInput>{

        static $inject = ['$state', 'uiGridConstants', 'declaracaoDoBeneficiarioService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public declaracaoDoBeneficiarioService: declaracaoDoBeneficiarioService.Services.DeclaracaoDoBeneficiarioService) {

            super(
                $state,
                uiGridConstants,
                declaracaoDoBeneficiarioService, {
                    rotaAlterarRegistro: 'tenant.ezliv.declaracaoDoBeneficiario.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.declaracaoDoBeneficiario.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.GetDeclaracaoDoBeneficiarioInput();
                filtro.nome = termoDigitadoPesquisa; 
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.declaracaoDoBeneficiarioService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDeclaracaoDoBeneficiario', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.DeclaracaoDoBeneficiario.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DeclaracaoDoBeneficiario.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DeclaracaoDoBeneficiario.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class DeclaracaoDoBeneficiarioIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioIndex.cshtml';
            this.controller = DeclaracaoDoBeneficiarioIndexController;
        }
    }
}