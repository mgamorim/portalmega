﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as declaracaoDoBeneficiarioService from "../../../../services/domain/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioService";
import * as itemDeDeclaracaoDoBeneficiarioService from "../../../../services/domain/ezliv/itemDeDeclaracaoDoBeneficiario/itemDeDeclaracaoDoBeneficiarioService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as declaracaoDoBeneficiarioDtos from "../../../../dtos/ezliv/geral/declaracaoDoBeneficiario/declaracaoDoBeneficiarioDtos";
import * as perguntaDtos from "../../../../dtos/ezliv/geral/itemDeDeclaracaoDoBeneficiario/itemDeDeclaracaoDoBeneficiarioDtos";

export module Forms {
    export class DeclaracaoDoBeneficiarioFormController extends controllers.Controllers.ControllerFormComponentBase<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto, declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        private createDescription: () => any;

        //Perguntas
        public addPergunta: () => void;
        public limparPergunta: () => void;
        public getPerguntaInput: () => void;
        public perguntaCurrent: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput;
        public savingPergunta: boolean;
        public editingPergunta: boolean;
        public editarPergunta: (valor: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput) => void;
        public excluirPergunta: (pergunta: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput) => void;
        public cancelEditPergunta: () => void;

        // Grid Perguntas
        private setDataGridInputPergunta: (result: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput[]) => void;
        private gridApiInputPergunta: uiGrid.IGridApiOf<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput>;
        private requestParamsValoresInputPergunta: requestParam.RequestParam.RequestParams;
        private perguntaAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput>;
        public loadingPerguntaInput: boolean;
        public perguntaGridOptionsInput: uiGrid.IGridOptionsOf<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput>;
        private gridDataInputPergunta: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput[];
        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'declaracaoDoBeneficiarioService', 'itemDeDeclaracaoDoBeneficiarioService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public declaracaoDoBeneficiarioService: declaracaoDoBeneficiarioService.Services.DeclaracaoDoBeneficiarioService,
            public itemDeDeclaracaoDoBeneficiarioService: itemDeDeclaracaoDoBeneficiarioService.Services.ItemDeDeclaracaoDoBeneficiarioService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                declaracaoDoBeneficiarioService,
                'tenant.ezliv.declaracaoDoBeneficiario');


            //Pergunta
            this.addPergunta = () => {
                this.perguntaCurrent.isActive = true;
                if (this.perguntaCurrent.id > 0) {
                    this.itemDeDeclaracaoDoBeneficiarioService.save(this.perguntaCurrent);
                } else if (!containsArray(this.entity.itensDeDeclaracaoDoBeneficiario, this.perguntaCurrent)) {
                    this.entity.itensDeDeclaracaoDoBeneficiario.push(this.perguntaCurrent);
                }

                this.getPerguntaInput();
                this.limparPergunta();
            }

            this.editarPergunta = (valor: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput) => {
                this.editingPergunta = true;
                this.perguntaCurrent = valor;
                this.loadingPerguntaInput = false;
            }

            this.limparPergunta = () => {
                this.perguntaCurrent = new perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput();
            }

            this.getPerguntaInput = () => {
                this.loadingEntity = true;
                if (this.entity) {
                    this.setDataGridInputPergunta(this.entity.itensDeDeclaracaoDoBeneficiario);
                }

                this.loadingEntity = false;
            }

            this.excluirPergunta = (pergunta: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput) => {
                this.editingPergunta = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.itensDeDeclaracaoDoBeneficiario.length; i++) {
                                if (this.entity.itensDeDeclaracaoDoBeneficiario[i].id === pergunta.id) {
                                    this.entity.itensDeDeclaracaoDoBeneficiario.splice(i, 1);
                                    this.getPerguntaInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInputPergunta = (result: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput[]) => {
                this.gridDataInputPergunta.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputPergunta.push(result[i]);
                }

                this.perguntaGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditPergunta = () => {
                this.perguntaCurrent = new perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput;
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioInput();
                instance.id = 0;
                instance.isActive = true;
                instance.itensDeDeclaracaoDoBeneficiario = new Array<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput>();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                this.getPerguntaInput();
            }

            this.perguntaGridConfigInput();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private perguntaGridConfigInput() {
            this.requestParamsValoresInputPergunta = new requestParam.RequestParam.RequestParams();

            this.perguntaAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput>();

            this.perguntaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarPergunta, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ItemDeDeclaracaoDoBeneficiario.Editar');
                }
            ));

            this.perguntaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirPergunta, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ItemDeDeclaracaoDoBeneficiario.Delete');
                }
            ));

            this.gridDataInputPergunta = new Array<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput>();

            this.perguntaGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.perguntaAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputPergunta = api;
                    this.gridApiInputPergunta.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputPergunta.sorting = null;
                        } else {
                            this.requestParamsValoresInputPergunta.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getPerguntaInput();
                    });
                    this.gridApiInputPergunta.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputPergunta.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputPergunta.maxResultCount = pageSize;

                        this.getPerguntaInput();
                    });
                }
            }

            this.perguntaGridOptionsInput.data = this.gridDataInputPergunta;

            this.perguntaGridOptionsInput.columnDefs.push({
                name: app.localize('ItemDeDeclaracaoDoBeneficiario.Pergunta'),
                field: 'pergunta',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.perguntaGridOptionsInput.columnDefs.push({
                name: app.localize('ItemDeDeclaracaoDeSaude.Ordenacao'),
                field: 'ordenacao',
                enableSorting: true,
                sort: { direction: this.uiGridConstants.ASC },
                type: 'number'
            });

            this.perguntaGridOptionsInput.columnDefs.push({
                name: app.localize('ItemDeDeclaracaoDoBeneficiario.Ajuda'),
                field: 'ajuda'
            });

            this.getPerguntaInput();
        }
    }

    function containsArray(arr, findValue) {
        var i = arr.length;

        while (i--) {
            if (arr[i] === findValue) return true;
        }
        return false;
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DeclaracaoDoBeneficiarioFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioForm.cshtml';
            this.controller = DeclaracaoDoBeneficiarioFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}