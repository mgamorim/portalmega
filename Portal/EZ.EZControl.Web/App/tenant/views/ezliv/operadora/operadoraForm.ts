﻿import * as operadoraService from "../../../../services/domain/ezliv/operadora/operadoraService";
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as ezFile from "../../../../common/ezFile/ezFile";

import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";
import * as arquivoDtos from "../../../../dtos/core/arquivo/arquivoDtos";

export module Forms {
    export class OperadoraFormController extends
        controllers.Controllers.ControllerFormComponentBase<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, 
        operadoraDtos.Dtos.EZLiv.Geral.OperadoraInput, operadoraDtos.Dtos.EZLiv.Geral.OperadoraPessoaIdDto>{
        public operadoraForm: ng.IFormController;

        // pessoa
        public pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        public ezSelectPessoaJuridica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaJuridica: (termoDigitadoPesquisa: string) => any;
        private pessoaJuridicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto) => string;
        private pessoaJuridicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaJuridica: (id: number) => void;

        public produtosDePlanoDeSaude = [];

        public ezFile: ezFile.EZFile.EZFile;
        private imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;

        public upload: () => void;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'operadoraService', 'pessoaJuridicaService', 'arquivoGlobalService', 'parametroGlobalService',  '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public operadoraService: operadoraService.Services.OperadoraService,
            public pessoaJuridicaService: pessoaJuridicaService.Services.PessoaJuridicaService,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                operadoraService,
                'tenant.ezliv.operadora');

            this.carregarPessoaJuridica = (id: number) => {
                this.ezSelectPessoaJuridica.loading = true;
                var promise = this.pessoaJuridicaService.getById(id);
                promise.then(result => {
                    this.pessoaJuridica = result;
                    this.entity.pessoaJuridica.id = this.pessoaJuridica.id;
                    this.ezSelectPessoaJuridica.setInputText(result.nomeFantasia);
                }).finally(() => {
                    this.ezSelectPessoaJuridica.loading = false;
                });

            }
        }

        $onInit() {
            super.init();

            this.ezFile = new ezFile.EZFile.EZFile();

            this.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            this.imagem.id = 0;
            this.imagem.isActive = true;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new operadoraDtos.Dtos.EZLiv.Geral.OperadoraInput();
                instance.id = 0;
                instance.pessoa = new pessoaDtos.Dtos.Pessoa.PessoaInput();
                instance.isActive = true;
                instance.pessoa.tipoPessoa = 2;
                instance.pessoa.grupoPessoaId = null;
                instance.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
                instance.pessoaJuridica.tipoPessoa = 2;
                instance.imagemId = 0;
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (entity.pessoa) {
                    this.carregarPessoaJuridica(entity.pessoaJuridica.id);
                }
                this.ezFile.controller.arquivoId = entity.imagemId;
                this.ezFile.initialize();
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.operadoraId;
                this.entity.pessoa.id = result.pessoaId;
                this.entity.pessoaJuridica.id = result.pessoaId;
            };

            this.events.onBeforeSaveEntity = () => {
            }

            this.ezSelectPessoaConfig();

            this.ezFileConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaJuridica) {
                this.carregarPessoaJuridica(this.pessoaJuridica.id);
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaJuridicaSelecionado = (registro) => {
                this.pessoaJuridica = registro;
                this.entity.pessoaJuridica.id = registro.id;
                this.entity.pessoaJuridica.tipoPessoa = 2;
                this.entity.pessoaJuridica = registro;
                return registro.nomePessoa;
            }

            this.pessoaJuridicaDeselecionado = () => {
                this.pessoaJuridica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaJuridica = (termoDigitadoPesquisa) => {
                var filtro = new operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput();
                filtro.nome = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaJuridica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaJuridicaService,
                this.getFiltroParaPaginacaoPessoaJuridica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaJuridicaSelecionado,
                this.pessoaJuridicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaJuridica.onEzGridCreated = () => {

                this.ezSelectPessoaJuridica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.operadoraService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaJuridica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaJuridica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaJuridica.ezGrid.getRegistros();
            };
        }

        private ezFileConfig() {
            this.ezFile.loadImage = () => {
                var promise = this.arquivoService.getById(this.entity.imagemId);
                promise.then((result) => {
                    this.ezFile.setImageInfo(result.path, result.tipoDeArquivoFixo);
                });
            }
            this.ezFile.getParametros = () => {
                var promise = this.parametroGlobalService.get();
                promise.then((result) => {
                    this.ezFile.setParametos(result.extensoesImagem);
                });
            }
            this.ezFile.onUploadComplete = (fileName: string) => {
                var input = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
                input.nome = fileName;
                var promise = this.arquivoService.getIdByName(input);
                promise.then((result) => {
                    this.entity.imagemId = result.id;
                });
            }
            this.ezFile.initialize = () => {
                this.ezFile.inicializado = true;
                this.ezFile.controller.sistema = "Estoque";
                this.ezFile.controller.autoUpload = true;
                if (this.ezFile.controller.arquivoId > 0) {
                    if (this.ezFile.loadImage) {
                        this.ezFile.loadImage();
                    }
                }
                if (this.ezFile.onUploadComplete) {
                    this.ezFile.controller.onUploadComplete = this.ezFile.onUploadComplete;
                }
            }
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.operadoraForm = form;
        }
    }



    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class OperadoraFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/operadora/operadoraForm.cshtml';
            this.controller = OperadoraFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}