﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as operadoraService from "../../../../services/domain/ezliv/operadora/operadoraService";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.Operadora {

    class OperadoraIndexController extends
        controllers.Controllers.ControllerIndexBase<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto,
        operadoraDtos.Dtos.EZLiv.Geral.OperadoraInput> {

        static $inject = ['$state', 'uiGridConstants', 'operadoraService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public operadoraService: operadoraService.Services.OperadoraService) {

            super(
                $state,
                uiGridConstants,
                operadoraService, {
                    rotaAlterarRegistro: 'tenant.ezliv.operadora.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.operadora.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigoAns = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.operadoraService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewOperadora', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Operadora.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Operadora.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Operadora.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Operadora.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Operadora.CodigoAns'),
                field: 'codigoAns'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica'),
                field: 'nomePessoa'
            });
        }
    }

    export class OperadoraIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/operadora/operadoraIndex.cshtml';
            this.controller = OperadoraIndexController;
        }
    }
}