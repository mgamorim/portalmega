﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as ezFile from "../../../../common/ezFile/ezFile";

// Servicos/DTOS
import * as associacaoService from "../../../../services/domain/ezliv/associacao/associacaoService";
import * as associacaoDtos from "../../../../dtos/ezliv/subtipoPessoa/associacao/associacaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as parametroPagSeguroDtos from "../../../../dtos/ezpag/parametroPagSeguro/parametroPagSeguroDtos"
import * as parametroPagSeguroService from "../../../../services/domain/ezpag/parametroPagSeguro/parametroPagSeguroService"
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as arquivoDtos from "../../../../dtos/core/arquivo/arquivoDtos";
import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";

export module Forms {
    export class AssociacaoFormController extends
        controllers.Controllers.ControllerFormComponentBase<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoInput,
        associacaoDtos.Dtos.EZLiv.Geral.AssociacaoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        public tipoPessoaEnum: any;
        public associacaoForm: ng.IFormController;
        public pagSeguroForm: ng.IFormController;

        // pessoa
        public pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        public ezSelectPessoaJuridica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaJuridica: (termoDigitadoPesquisa: string) => any;
        private pessoaJuridicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto) => string;
        private pessoaJuridicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaJuridica: (id: number) => void;

        //ParametroPagSeguro
        public parametroPagSeguroCurrent: parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput;

        public ezFile: ezFile.EZFile.EZFile;
        private imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
        public upload: () => void;

        static $inject = ['$scope', '$rootScope', '$state', '$stateParams', 'uiGridConstants', 'associacaoService', 'pessoaJuridicaService', 'parametroPagSeguroService', 'arquivoGlobalService', 'parametroGlobalService', '$uibModal'];

        constructor(
            public $scope: ng.IScope,
            public $rootScope: any,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public associacaoService: associacaoService.Services.AssociacaoService,
            public pessoaJuridicaService: pessoaJuridicaService.Services.PessoaJuridicaService,
            public parametroPagSeguroService: parametroPagSeguroService.Services.ParametroPagSeguroService,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                associacaoService,
                'tenant.ezliv.associacao');

            this.carregarPessoaJuridica = (id: number) => {
                this.ezSelectPessoaJuridica.loading = true;
                var promise = this.pessoaJuridicaService.getById(id);
                promise.then(result => {
                    this.pessoaJuridica = result;
                    this.entity.pessoaJuridicaId = this.pessoaJuridica.id;
                    this.ezSelectPessoaJuridica.setInputText(result.nomeFantasia);
                }).finally(() => {
                    this.ezSelectPessoaJuridica.loading = false;
                });
            }
        }

        $onInit() {
            super.init();
            this.ezFile = new ezFile.EZFile.EZFile();

            this.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            this.imagem.id = 0;
            this.imagem.isActive = true;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new associacaoDtos.Dtos.EZLiv.Geral.AssociacaoInput;
                instance.id = 0;
                instance.isActive = true;
                instance.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
                instance.pessoaJuridica = TipoPessoaEnum.Juridica;
                instance.imagemId = 0;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaJuridica) {
                    this.carregarPessoaJuridica(entity.pessoaJuridicaId);
                }

                if (entity.parametroPagSeguro != null) {
                    this.parametroPagSeguroCurrent = new parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput();
                    this.parametroPagSeguroCurrent.email = entity.parametroPagSeguro.email;
                    this.parametroPagSeguroCurrent.token = entity.parametroPagSeguro.token;
                    this.parametroPagSeguroCurrent.pessoaJuridicaId = entity.parametroPagSeguro.pessoaJuridicaId;
                }

                this.ezFile.controller.arquivoId = entity.imagemId;
                this.ezFile.initialize();
            };
            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
                this.entity.parametroPagSeguro = this.parametroPagSeguroCurrent;
            };

            this.events.onBeforeSaveEntity = () => {
                if (this.parametroPagSeguroCurrent != null) {
                    this.parametroPagSeguroCurrent.pessoaJuridicaId = this.entity.pessoaJuridicaId;
                    this.entity.parametroPagSeguro = this.parametroPagSeguroCurrent;
                }
            };

            this.ezSelectPessoaConfig();
            this.ezFileConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        setAssociacaoForm(form: ng.IFormController) {
            this.associacaoForm = form;
        }

        setPagSeguroForm(form: ng.IFormController) {
            this.pagSeguroForm = form;
        }



        private inicializarPagSeguroItemCurrent() {
            this.parametroPagSeguroCurrent = new parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput();
            this.parametroPagSeguroCurrent.email = "";
            this.parametroPagSeguroCurrent.token = "";
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.pessoaJuridicaSelecionado = (registro) => {
                this.pessoaJuridica = registro;
                this.entity.pessoaJuridicaId = registro.id;
                this.entity.pessoaJuridica = registro;
                return registro.nomePessoa;
            }

            this.pessoaJuridicaDeselecionado = () => {
                this.pessoaJuridica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaJuridica = (termoDigitadoPesquisa) => {
                var filtro = new associacaoDtos.Dtos.EZLiv.Geral.GetPessoaExceptForAssociacao();
                filtro.nomePessoa = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaJuridica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaJuridicaService,
                this.getFiltroParaPaginacaoPessoaJuridica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaJuridicaSelecionado,
                this.pessoaJuridicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaJuridica.onEzGridCreated = () => {

                this.ezSelectPessoaJuridica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.associacaoService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.RazaoSocial'),
                    field: 'razaoSocial'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaJuridica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaJuridica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaJuridica.ezGrid.getRegistros();
            };
        }

        private ezFileConfig() {
            this.ezFile.loadImage = () => {
                var promise = this.arquivoService.getById(this.entity.imagemId);
                promise.then((result) => {
                    this.ezFile.setImageInfo(result.path, result.tipoDeArquivoFixo);
                });
            }
            this.ezFile.getParametros = () => {
                var promise = this.parametroGlobalService.get();
                promise.then((result) => {
                    this.ezFile.setParametos(result.extensoesImagem);
                });
            }
            this.ezFile.onUploadComplete = (fileName: string) => {
                var input = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
                input.nome = fileName;
                var promise = this.arquivoService.getIdByName(input);
                promise.then((result) => {
                    this.entity.imagemId = result.id;
                });
            }
            this.ezFile.initialize = () => {
                this.ezFile.inicializado = true;
                this.ezFile.controller.sistema = "Estoque";
                this.ezFile.controller.autoUpload = true;
                if (this.ezFile.controller.arquivoId > 0) {
                    if (this.ezFile.loadImage) {
                        this.ezFile.loadImage();
                    }
                }
                if (this.ezFile.onUploadComplete) {
                    this.ezFile.controller.onUploadComplete = this.ezFile.onUploadComplete;
                }
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class AssociacaoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/associacao/associacaoForm.cshtml';
            this.controller = AssociacaoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}