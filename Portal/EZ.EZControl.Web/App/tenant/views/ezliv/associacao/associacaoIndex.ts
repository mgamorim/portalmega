﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as associacaoService from "../../../../services/domain/ezliv/associacao/associacaoService";
import * as associacaoDtos from "../../../../dtos/ezliv/subtipoPessoa/associacao/associacaoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Associacao {

    class AssociacaoIndexController extends
        controllers.Controllers.ControllerIndexBase<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto,
        associacaoDtos.Dtos.EZLiv.Geral.AssociacaoInput> {

        static $inject = ['$state', 'uiGridConstants', 'associacaoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public associacaoService: associacaoService.Services.AssociacaoService) {

            super(
                $state,
                uiGridConstants,
                associacaoService, {
                    rotaAlterarRegistro: 'tenant.ezliv.associacao.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.associacao.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'PessoaJuridica.NomeFantasia';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new associacaoDtos.Dtos.EZLiv.Geral.GetAssociacaoInput();
                filtro.razaoSocial = termoDigitadoPesquisa;
                filtro.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.associacaoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewEntidade', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Associacao.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Associacao.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Associacao.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Entidade.NomeFantasia'),
                field: 'pessoaJuridica.nomeFantasia',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Entidade.RazaoSocial'),
                field: 'pessoaJuridica.razaoSocial'
            });
          
        }
    }

    export class AssociacaoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/associacao/associacaoIndex.cshtml';
            this.controller = AssociacaoIndexController;
        }
    }
}