﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as produtoDePlanoDeSaudeService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as produtoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.ProdutoDePlanoDeSaude {

    class ProdutoDePlanoDeSaudeIndexController extends
        controllers.Controllers.ControllerIndexBase<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto,
        produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput> {

        static $inject = ['$state', 'uiGridConstants', 'produtoDePlanoDeSaudeService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public produtoDePlanoDeSaudeService: produtoDePlanoDeSaudeService.Services.ProdutoDePlanoDeSaudeService) {

            super(
                $state,
                uiGridConstants,
                produtoDePlanoDeSaudeService, {
                    rotaAlterarRegistro: 'tenant.ezliv.produtoDePlanoDeSaude.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.produtoDePlanoDeSaude.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoDePlanoDeSaudeService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewProdutoDePlanoDeSaude', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            //this.ezGrid.optionsGrid.columnDefs.push({
            //    name: app.localize('Produto.Valor'),
            //    field: 'valor'
            //});

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoDePlanoDeSaude.PlanoDeSaude'),
                field: 'planoDeSaude.numeroDeRegistro'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoDePlanoDeSaude.DescricaoDaNatureza'),
                field: 'natureza.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoDePlanoDeSaude.DescricaoDaUnidade'),
                field: 'unidadeMedida.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
              name: app.localize('ProdutoDePlanoDeSaude.Ativo'),
              field: 'isActive',
              cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class ProdutoDePlanoDeSaudeIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeIndex.cshtml';
            this.controller = ProdutoDePlanoDeSaudeIndexController;
        }
    }
}