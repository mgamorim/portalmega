﻿import * as produtoDePlanoDeSaudeService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as produtoService from "../../../../services/domain/estoque/produto/produtoService";
import * as faixaEtariaService from "../../../../services/domain/ezliv/faixaEtaria/faixaEtariaService";
import * as valorPorFaixaEtariaService from "../../../../services/domain/ezliv/valorPorFaixaEtaria/valorPorFaixaEtariaService";
import * as planoDeSaudeService from "../../../../services/domain/ezliv/planoDeSaude/planoDeSaudeService";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as produtoDtos from "../../../../dtos/estoque/produto/produtoDtos";
import * as operadoraService from "../../../../services/domain/ezliv/operadora/operadoraService";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as planoDeSaudeDtos from "../../../../dtos/ezliv/geral/planoDeSaude/planoDeSaudeDtos";
import * as contratoDtos from "../../../../dtos/ezliv/geral/contrato/contratoDtos";
import * as valorPorFaixaEtariaDtos from "../../../../dtos/ezliv/geral/valorPorFaixaEtaria/valorPorFaixaEtariaDtos";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as produtoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as faixaEtariaDtos from "../../../../dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";
import * as vigenciaDtos from "../../../../dtos/ezliv/geral/vigencia/vigenciaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as naturezaService from "../../../../services/domain/estoque/parametros/naturezaService";
import * as naturezaDtos from "../../../../dtos/estoque/parametro/naturezaDtos";
import * as unidadeMedidaService from "../../../../services/domain/estoque/parametros/unidadeMedidaService";
import * as unidadeMedidaDtos from "../../../../dtos/estoque/parametro/unidadeMedidaDtos";
import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";
import * as arquivoDtos from "../../../../dtos/core/arquivo/arquivoDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as especialidadePorProdutoDePlanoDeSaudeService from "../../../../services/domain/ezliv/especialidadePorProdutoDePlanoDeSaude/especialidadePorProdutoDePlanoDeSaudeService";
import * as especialidadePorProdutoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/especialidadePorProdutoDePlanoDeSaude/especialidadePorProdutoDePlanoDeSaudeDtos";
import * as redeCredenciadaService from "../../../../services/domain/ezliv/redeCredenciada/redeCredenciadaService";
import * as redeCredenciadaDtos from "../../../../dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as associacaoDtos from "../../../../dtos/ezliv/subtipopessoa/associacao/associacaoDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as vigenciaService from "../../../../services/domain/ezliv/vigencia/vigenciaService";
import * as associacaoService from "../../../../services/domain/ezliv/associacao/associacaoService";
import * as chancelaService from "../../../../services/domain/ezliv/chancela/chancelaService";
import * as chancelaDtos from "../../../../dtos/ezliv/geral/chancela/chancelaDtos";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";
import * as relatorioEzlivService from "../../../../services/domain/ezliv/relatorio/relatorioEzlivService";
import * as questionarioService from "../../../../services/domain/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeService";
import * as questionarioDtos from "../../../../dtos/ezliv/geral/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeDtos";
import * as declaracaoService from "../../../../services/domain/ezliv/declaracaoDoBeneficiario/declaracaoDoBeneficiarioService";
import * as declaracaoDoBeneficiarioDtos from "../../../../dtos/ezliv/geral/declaracaoDoBeneficiario/declaracaoDoBeneficiarioDtos";

export module Forms {
    export class ProdutoDePlanoDeSaudeFormController extends controllers.Controllers.ControllerFormComponentBase<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoProduto: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoOperadora: (termoDigitadoPesquisa: string) => any;
        private produtoSelecionado: (registro: produtoDtos.Dtos.Produto.ProdutoListDto) => string;
        private produtoDeselecionado: () => void;
        private requestParamsProduto: requestParam.RequestParam.RequestParams;
        private requestParamsEspecialidades: requestParam.RequestParam.RequestParams;

        public ezSelectOperadora: ezSelect.EzSelect.EzSelect<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaOperadora: (termoDigitadoPesquisa: string) => any;
        private operadoraSelecionado: (registro: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto) => string;
        private operadoraDeselecionado: () => void;
        private requestParamsOperadora: requestParam.RequestParam.RequestParams;

        public ezSelectNatureza: ezSelect.EzSelect.EzSelect<naturezaDtos.Dtos.Natureza.NaturezaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaNatureza: (termoDigitadoPesquisa: string) => any;
        private naturezaSelecionado: (registro: naturezaDtos.Dtos.Natureza.NaturezaListDto) => string;
        private naturezaDeselecionado: () => void;
        private requestParamsNatureza: requestParam.RequestParam.RequestParams;

        public ezSelectUnidadeMedida: ezSelect.EzSelect.EzSelect<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaUnidadeMedida: (termoDigitadoPesquisa: string) => any;
        private unidadeMedidaSelecionado: (registro: unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto) => string;
        private unidadeMedidaDeselecionado: () => void;
        private requestParamsUnidadeMedida: requestParam.RequestParam.RequestParams;

        public ezSelectFaixaEtaria: ezSelect.EzSelect.EzSelect<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaFaixaEtaria: (termoDigitadoPesquisa: string) => any;
        private faixaEtariaSelecionado: (registro: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto) => string;
        private faixaEtariaDeselecionado: () => void;
        private requestParamsFaixaEtaria: requestParam.RequestParam.RequestParams;

        public ezSelectRedeCredenciada: ezSelect.EzSelect.EzSelect<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaRedeCredenciada: (termoDigitadoPesquisa: string) => any;
        private redeCredenciadaSelecionado: (registro: redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto) => string;
        private redeCredenciadaDeselecionado: () => void;
        private requestParamsRedeCredenciada: requestParam.RequestParam.RequestParams;

        public ezSelectQuestionario: ezSelect.EzSelect.EzSelect<questionarioDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaQuestionario: (termoDigitadoPesquisa: string) => any;
        private questionarioSelecionado: (registro: questionarioDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto) => string;
        private questionarioDeselecionado: () => void;
        private requestParamsQuestionario: requestParam.RequestParam.RequestParams;

        public ezSelectDeclaracaoDoBeneficiario: ezSelect.EzSelect.EzSelect<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaDeclaracaoDoBeneficiario: (termoDigitadoPesquisa: string) => any;
        private declaracaoDoBeneficiarioSelecionado: (registro: declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto) => string;
        private declaracaoDoBeneficiarioDeselecionado: () => void;
        private requestParamsDeclaracaoDoBeneficiario: requestParam.RequestParam.RequestParams;

        // EzSelec Especialidade
        public ezSelectEspecialidade: ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaEspecialidade: (termoDigitadoPesquisa: string) => any;
        private especialidadeSelecionado: (registro: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto) => string;
        private especialidadeDeselecionado: () => void;
        private requestParamsEspecialidade: requestParam.RequestParam.RequestParams;

        // Especialidade por Produto
        private requestParamsGridEspecialidade: requestParam.RequestParam.RequestParams;
        public especialidadeCurrent: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput;
        public loadingEspecialidadeInput: boolean;
        public editandoEspecialidade: boolean;
        public limparEspecialidade: () => void;
        public cancelEditEspecialidade: () => void;
        private idsEspecialidades: number[];

        //Grid Especialidades
        private gridDataEnumerable: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto[];
        private especialidadeAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>;
        public especialidadeGridOptions: uiGrid.IGridOptionsOf<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>;
        public editarEspecialidade: (valor: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto) => void;
        public excluirEspecialidade: (valor: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto) => void;
        public getEspecialidadeGrid: () => void;

        // EzSelec Estado
        public estado: estadoDtos.Dtos.Estado.EstadoInput;
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoInput) => string;
        private estadoDeselecionado: () => void;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;

        public addEstado: () => void;
        public limparEstado: () => void;
        public getEstadoInput: () => void;
        public estadoCurrent: estadoDtos.Dtos.Estado.EstadoInput;
        public savingEstado: boolean;
        public editingEstado: boolean;
        public excluirEstado: (estado: estadoDtos.Dtos.Estado.EstadoInput) => void;
        public cancelEditEstado: () => void;

        // Grid Estado
        private setDataGridInputEstado: (result: estadoDtos.Dtos.Estado.EstadoInput[]) => void;
        private gridApiInputEstado: uiGrid.IGridApiOf<estadoDtos.Dtos.Estado.EstadoInput>;
        private requestParamsValoresInputEstado: requestParam.RequestParam.RequestParams;
        private estadoAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<estadoDtos.Dtos.Estado.EstadoInput>;
        public loadingEstadoInput: boolean;
        public estadoGridOptionsInput: uiGrid.IGridOptionsOf<estadoDtos.Dtos.Estado.EstadoInput>;
        private gridDataInputEstado: estadoDtos.Dtos.Estado.EstadoInput[];

        // EzSelec Aditivo
        public aditivo: relatorioDtos.Dtos.Global.Geral.RelatorioInput;
        public ezSelectAditivo: ezSelect.EzSelect.EzSelect<relatorioDtos.Dtos.Global.Geral.RelatorioInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoAditivo: (termoDigitadoPesquisa: string) => any;
        private aditivoSelecionado: (registro: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => string;
        private aditivoDeselecionado: () => void;
        private requestParamsAditivo: requestParam.RequestParam.RequestParams;

        public addAditivo: () => void;
        public limparAditivo: () => void;
        public getAditivoInput: () => void;
        public aditivoCurrent: relatorioDtos.Dtos.Global.Geral.RelatorioInput;
        public savingAditivo: boolean;
        public editingAditivo: boolean;
        public excluirAditivo: (aditivo: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => void;
        public cancelEditAditivo: () => void;

        // Grid Aditivo
        private setDataGridInputAditivo: (result: relatorioDtos.Dtos.Global.Geral.RelatorioInput[]) => void;
        private gridApiInputAditivo: uiGrid.IGridApiOf<relatorioDtos.Dtos.Global.Geral.RelatorioInput>;
        private requestParamsValoresInputAditivo: requestParam.RequestParam.RequestParams;
        private aditivoAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<relatorioDtos.Dtos.Global.Geral.RelatorioInput>;
        public loadingAditivoInput: boolean;
        public aditivoGridOptionsInput: uiGrid.IGridOptionsOf<relatorioDtos.Dtos.Global.Geral.RelatorioInput>;
        private gridDataInputAditivo: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];

        // EzSelec Cidade
        public cidade: cidadeDtos.Dtos.Cidade.CidadeInput;
        public ezSelectCidade: ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCidade: (termoDigitadoPesquisa: string) => any;
        private cidadeSelecionado: (registro: cidadeDtos.Dtos.Cidade.CidadeInput) => string;
        private cidadeDeselecionado: () => void;
        private requestParamsCidade: requestParam.RequestParam.RequestParams;

        public addCidade: () => void;
        public limparCidade: () => void;
        public getCidadeInput: () => void;
        public cidadeCurrent: cidadeDtos.Dtos.Cidade.CidadeInput;
        public savingCidade: boolean;
        public editingCidade: boolean;
        public excluirCidade: (cidade: cidadeDtos.Dtos.Cidade.CidadeInput) => void;
        public cancelEditCidade: () => void;

        // Grid Cidade
        private setDataGridInputCidade: (result: cidadeDtos.Dtos.Cidade.CidadeInput[]) => void;
        private gridApiInputCidade: uiGrid.IGridApiOf<cidadeDtos.Dtos.Cidade.CidadeInput>;
        private requestParamsValoresInputCidade: requestParam.RequestParam.RequestParams;
        private cidadeAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<cidadeDtos.Dtos.Cidade.CidadeInput>;
        public loadingCidadeInput: boolean;
        public cidadeGridOptionsInput: uiGrid.IGridOptionsOf<cidadeDtos.Dtos.Cidade.CidadeInput>;
        private gridDataInputCidade: cidadeDtos.Dtos.Cidade.CidadeInput[];

        //EZSelect Associação
        public ezSelectAssociacao: ezSelect.EzSelect.EzSelect<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroAssociacao: (termoDigitadoPesquisa: string) => any;
        private associacaoSelecionado: (registro: associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto) => string;
        private associacaoDeselecionado: () => void;
        private requestParamsAssociacao: requestParam.RequestParam.RequestParams;

        // EzSelec Chancela
        public chancela: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput;
        public ezSelectChancela: ezSelect.EzSelect.EzSelect<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoChancela: (termoDigitadoPesquisa: string) => any;
        private chancelaSelecionado: (registro: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput) => string;
        private chancelaDeselecionado: () => void;
        private requestParamsChancela: requestParam.RequestParam.RequestParams;

        // EzSelec RelatorioPropostaDeContratacao
        public relatorioPropostaDeContratacao: relatorioDtos.Dtos.Global.Geral.RelatorioInput;
        public ezSelectRelatorioPropostaDeContratacao: ezSelect.EzSelect.EzSelect<relatorioDtos.Dtos.Global.Geral.RelatorioInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoRelatorioPropostaDeContratacao: (termoDigitadoPesquisa: string) => any;
        private relatorioPropostaDeContratacaoSelecionado: (registro: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => string;
        private relatorioPropostaDeContratacaoDeselecionado: () => void;
        private requestParamsRelatorioPropostaDeContratacao: requestParam.RequestParam.RequestParams;

        // EzSelec RelatorioFichaDaEntidade
        public relatorioFichaDaEntidade: relatorioDtos.Dtos.Global.Geral.RelatorioInput;
        public ezSelectRelatorioFichaDaEntidade: ezSelect.EzSelect.EzSelect<relatorioDtos.Dtos.Global.Geral.RelatorioInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoRelatorioFichaDaEntidade: (termoDigitadoPesquisa: string) => any;
        private relatorioFichaDaEntidadeSelecionado: (registro: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => string;
        private relatorioFichaDaEntidadeDeselecionado: () => void;
        private requestParamsRelatorioFichaDaEntidade: requestParam.RequestParam.RequestParams;

        public addChancela: () => void;
        public limparChancela: () => void;
        public getChancelaInput: () => void;
        public chancelaCurrent: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput;
        public savingChancela: boolean;
        public editingChancela: boolean;
        public excluirChancela: (chancela: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput) => void;
        public cancelEditChancela: () => void;

        // Grid Chancela
        private setDataGridInputChancela: (result: chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto[]) => void;
        private gridApiInputChancela: uiGrid.IGridApiOf<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput>;
        private requestParamsValoresInputChancela: requestParam.RequestParam.RequestParams;
        private chancelaAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput>;
        public loadingChancelaInput: boolean;
        public chancelaGridOptionsInput: uiGrid.IGridOptionsOf<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput>;
        private gridDataInputChancela: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput[];


        //Vigências
        public addVigencia: () => void;
        public limparVigencia: () => void;
        public getVigenciaInput: () => void;
        public vigenciaCurrent: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput;
        public savingVigencia: boolean;
        public editingVigencia: boolean;
        public editarVigencia: (valor: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput) => void;
        public excluirVigencia: (vigencia: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput) => void;
        public cancelEditVigencia: () => void;

        // Grid Vigência
        private setDataGridInputVigencia: (result: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput[]) => void;
        private gridApiInputVigencia: uiGrid.IGridApiOf<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput>;
        private requestParamsValoresInputVigencia: requestParam.RequestParam.RequestParams;
        private vigenciaAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput>;
        public loadingVigenciaInput: boolean;
        public vigenciaGridOptionsInput: uiGrid.IGridOptionsOf<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput>;
        private gridDataInputVigencia: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput[];

        private planoDeSaudeAnsEnum: any;
        private abrangenciaDoPlanoEnum: any;
        private segmentacaoAssistencialDoPlanoEnum: any;
        private acomodacaoEnum: any;
        private formaDeContratacaoEnum: any;

        private imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
        public upload: () => void;
        public produtoDePlanoDeSaudeForm: ng.IFormController;
        public especialidadeForm: ng.IFormController;

        static $inject = ['$scope', '$rootScope', '$state', '$stateParams', 'uiGridConstants', 'produtoDePlanoDeSaudeService',
          'planoDeSaudeService', 'estoqueNaturezaService', 'estoqueUnidadeMedidaService', 'arquivoGlobalService', 'parametroGlobalService',
          'operadoraService', 'especialidadeService', 'faixaEtariaService', 'valorPorFaixaEtariaService', 'especialidadePorProdutoDePlanoDeSaudeService',
          'redeCredenciadaService', 'estadoService', 'cidadeService', '$uibModal', 'associacaoService', 'chancelaService', 'vigenciaService',
          'questionarioDeDeclaracaoDeSaudeService', 'declaracaoDoBeneficiarioService', 'relatorioEzlivService'];

        constructor(
            private $scope: ng.IScope,
            public $rootScope: any,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public produtoDePlanoDeSaudeService: produtoDePlanoDeSaudeService.Services.ProdutoDePlanoDeSaudeService,
            public planoDeSaudeService: planoDeSaudeService.Services.PlanoDeSaudeService,
            public naturezaService: naturezaService.Services.NaturezaService,
            public unidadeDeMedidaService: unidadeMedidaService.Services.UnidadeMedidaService,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public operadoraService: operadoraService.Services.OperadoraService,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public faixaEtariaService: faixaEtariaService.Services.FaixaEtariaService,
            public valorPorFaixaEtariaService: valorPorFaixaEtariaService.Services.ValorPorFaixaEtariaService,
            public especialidadePorProdutoDePlanoDeSaudeService: especialidadePorProdutoDePlanoDeSaudeService.Services.EspecialidadePorProdutoDePlanoDeSaudeService,
            public redeCredenciadaService: redeCredenciadaService.Services.RedeCredenciadaService,
            public estadoService: estadoService.Services.EstadoService,
            public cidadeService: cidadeService.Services.CidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public associacaoService: associacaoService.Services.AssociacaoService,
            public chancelaService: chancelaService.Services.ChancelaService,
            public vivenciaService: vigenciaService.Services.VigenciaService,
            public questionarioDeDeclaracaoDeSaudeService: questionarioService.Services.QuestionarioDeDeclaracaoDeSaudeService,
            public declaracaoDoBeneficiarioService: declaracaoService.Services.DeclaracaoDoBeneficiarioService,
            public relatorioEzlivService: relatorioEzlivService.Services.RelatorioEzlivService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                produtoDePlanoDeSaudeService,
                'tenant.ezliv.produtoDePlanoDeSaude');

            this.editingVigencia = false;

            //Especialidade
            this.getEspecialidadeGrid = () => {
                this.especialidadeGridOptions.data = this.entity.especialidades;
                this.especialidadeGridOptions.totalItems = this.entity.especialidades.length;
            }

            this.editandoEspecialidade = false;
            this.editingVigencia = false;

            this.cancelEditEspecialidade = () => {
                this.especialidadeCurrent = new especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput();
                this.ezSelectEspecialidade.gridSelectClear();
            }

            this.limparEspecialidade = () => {
                this.inicializarEspecialidadeCurrent();
                this.ezSelectEspecialidade.gridSelectClear();
                this.ezSelectEspecialidade.setInputText("");
                this.editandoEspecialidade = false;
            }

            this.editarEspecialidade = (valor: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput) => {
                this.editandoEspecialidade = true;
                this.especialidadeCurrent = valor;
                this.ezSelectEspecialidade.setInputText(valor.especialidade.nome);
                this.loadingEspecialidadeInput = false;
            }

            this.excluirEspecialidade = (valor: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput) => {
                this.editandoEspecialidade = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            var promise = (this.especialidadePorProdutoDePlanoDeSaudeService as any as especialidadePorProdutoDePlanoDeSaudeService.Services.EspecialidadePorProdutoDePlanoDeSaudeService).delete(valor.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarEspecialidadeCurrent();
                            }).finally(() => {
                                
                            });
                            var index = this.entity.especialidades.indexOf(valor);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.especialidades.splice(index, 1);
                            }
                            this.editandoEspecialidade = false;
                        }
                    }
                );
            }

            this.getEspecialidadeGrid = () => {
                this.especialidadeGridOptions.data = this.entity.especialidades;
                this.especialidadeGridOptions.totalItems = this.entity.especialidades.length;
            }

            //Aditivo
            this.addAditivo = () => {
              this.aditivoCurrent.isActive = true;
              this.entity.aditivos.push(this.aditivoCurrent);
              this.getAditivoInput();
              this.limparAditivo();
            }

            this.limparAditivo = () => {
              this.aditivoCurrent = new relatorioDtos.Dtos.Global.Geral.RelatorioInput;
              this.ezSelectAditivo.gridSelectClear();
            }

            this.getAditivoInput = () => {

              if (this.entity) {
                this.setDataGridInputAditivo(this.entity.aditivos);
              }


            }

            this.excluirAditivo = (aditivo: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => {
              this.editingAditivo = false;
              abp.message.confirm(
                '', '',
                isConfirmed => {
                  if (isConfirmed) {
                    for (var i = 0; i < this.entity.aditivos.length; i++) {
                      if (this.entity.aditivos[i].id === aditivo.id) {
                        this.entity.aditivos.splice(i, 1);
                        this.getAditivoInput();
                        abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                        break;
                      }
                    }
                  }
                }
              );
            }

            this.setDataGridInputAditivo = (result: relatorioDtos.Dtos.Global.Geral.RelatorioInput[]) => {
              this.gridDataInputAditivo.length = 0;

              for (var i = 0; i < result.length; i++) {
                this.gridDataInputAditivo.push(result[i]);
              }

              this.aditivoGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditAditivo = () => {
              this.aditivoCurrent = new relatorioDtos.Dtos.Global.Geral.RelatorioInput;
              this.ezSelectAditivo.gridSelectClear();
            }

            //Estado
            this.addEstado = () => {
                this.estadoCurrent.isActive = true;
                this.entity.estados.push(this.estadoCurrent);
                this.getEstadoInput();
                this.limparEstado();
            }

            this.limparEstado = () => {
                this.estadoCurrent = new estadoDtos.Dtos.Estado.EstadoInput();
                this.ezSelectEstado.gridSelectClear();
            }

            this.getEstadoInput = () => {
                
                if (this.entity) {
                    this.setDataGridInputEstado(this.entity.estados);
                }

                
            }

            this.excluirEstado = (estado: estadoDtos.Dtos.Estado.EstadoInput) => {
                this.editingEstado = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.estados.length; i++) {
                                if (this.entity.estados[i].id === estado.id) {
                                    this.entity.estados.splice(i, 1);
                                    this.getEstadoInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInputEstado = (result: estadoDtos.Dtos.Estado.EstadoInput[]) => {
                this.gridDataInputEstado.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputEstado.push(result[i]);
                }

                this.estadoGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditEstado = () => {
                this.estadoCurrent = new estadoDtos.Dtos.Estado.EstadoInput;
                this.ezSelectEstado.gridSelectClear();
            }

            //Cidade
            this.addCidade = () => {
                this.cidadeCurrent.isActive = true;
                this.entity.cidades.push(this.cidadeCurrent);
                this.getCidadeInput();
                this.limparCidade();
            }

            this.limparCidade = () => {
                this.cidadeCurrent = new cidadeDtos.Dtos.Cidade.CidadeInput();
                this.ezSelectCidade.gridSelectClear();
            }

            this.getCidadeInput = () => {
                if (this.entity) {
                    this.setDataGridInputCidade(this.entity.cidades);
                }
            }

            this.excluirCidade = (cidade: cidadeDtos.Dtos.Cidade.CidadeInput) => {
                this.editingCidade = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.cidades.length; i++) {
                                if (this.entity.cidades[i].id === cidade.id) {
                                    this.entity.cidades.splice(i, 1);
                                    this.getCidadeInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInputCidade = (result: cidadeDtos.Dtos.Cidade.CidadeInput[]) => {
                this.gridDataInputCidade.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputCidade.push(result[i]);
                }

                this.cidadeGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditCidade = () => {
                this.cidadeCurrent = new cidadeDtos.Dtos.Cidade.CidadeInput;
                this.ezSelectCidade.gridSelectClear();
            }

            //Chancela
            this.addChancela = () => {
                this.chancelaCurrent.isActive = true;
                this.entity.chancelas.push(this.chancelaCurrent);
                this.getChancelaInput();
                this.limparChancela();
            }

            this.limparChancela = () => {
                this.chancelaCurrent = new chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput();
                this.ezSelectChancela.gridSelectClear();
            }

            this.getChancelaInput = () => {
                
                if (this.entity) {
                    this.setDataGridInputChancela(this.entity.chancelas);
                }

                
            }

            this.excluirChancela = (chancela: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput) => {
                this.editingChancela = false;
                abp.message.confirm(
                    'Deseja remover a chancela?', 'Confirmação',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.chancelas.length; i++) {
                                if (this.entity.chancelas[i].id === chancela.id) {
                                    this.entity.chancelas.splice(i, 1);
                                    this.getChancelaInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInputChancela = (result: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput[]) => {
                this.gridDataInputChancela.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputChancela.push(result[i]);
                }

                this.chancelaGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditChancela = () => {
                this.chancelaCurrent = new chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput;
                this.ezSelectChancela.gridSelectClear();
            }

            //Vigência
            this.addVigencia = () => {
                this.vigenciaCurrent.isActive = true;
                if (this.vigenciaCurrent.id > 0) {
                    this.vivenciaService.save(this.vigenciaCurrent);
                } else if (!containsArray(this.entity.vigencias, this.vigenciaCurrent)) {
                    this.entity.vigencias.push(this.vigenciaCurrent);
                }

                this.getVigenciaInput();
                this.limparVigencia();
            }

            this.limparVigencia = () => {
                this.vigenciaCurrent = new vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput();
                this.editingVigencia = false;
            }

            this.getVigenciaInput = () => {
                if (this.entity) {
                    this.setDataGridInputVigencia(this.entity.vigencias);
                }
            }

            this.editarVigencia = (valor: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput) => {
                this.editingVigencia = true;
                this.vigenciaCurrent = valor;
                this.loadingVigenciaInput = false;
            }

            this.excluirVigencia = (vigencia: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput) => {
                this.editingVigencia = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.vigencias.length; i++) {
                                if (this.entity.vigencias[i].id === vigencia.id) {
                                    this.entity.vigencias.splice(i, 1);
                                    this.getVigenciaInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                        this.editingVigencia = false;
                    }
                );
            }

            this.setDataGridInputVigencia = (result: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput[]) => {
                this.gridDataInputVigencia.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputVigencia.push(result[i]);
                }

                this.vigenciaGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditVigencia = () => {
                this.vigenciaCurrent = new vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput;
            }
        }

        $onInit() {
            super.init();

            this.abrangenciaDoPlanoEnum = ez.domain.enum.abrangenciaDoPlanoEnum.valores;
            this.segmentacaoAssistencialDoPlanoEnum = ez.domain.enum.segmentacaoAssistencialDoPlanoEnum.valores;
            this.acomodacaoEnum = ez.domain.enum.acomodacaoEnum.valores;
            this.formaDeContratacaoEnum = ez.domain.enum.formaDeContratacaoEnum.valores;
            this.planoDeSaudeAnsEnum = ez.domain.enum.planoDeSaudeAnsEnum.valores;

            this.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            this.imagem.id = 0;
            this.imagem.isActive = true;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput();
                instance.id = 0;
                instance.isActive = true;
                instance.planoDeSaude = new planoDeSaudeDtos.Dtos.EZLiv.Geral.PlanoDeSaudeInput();
                instance.planoDeSaude.isActive = true;
                instance.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
                instance.imagem.id = 0;
                instance.imagemId = 0;
                instance.imagem.isActive = true;
                instance.contrato = new contratoDtos.Dtos.EZLiv.Geral.ContratoInput();
                instance.contrato.id = 0;
                instance.contrato.isActive = true;
                instance.naturezaId = 1;
                instance.unidadeMedidaId = 1;
                instance.tipoDeProduto = 2;
                //instance.redeCredenciada = new redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto();
                //instance.redeCredenciadaId = 0;
                instance.especialidades = new Array<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>();
                if (instance.naturezaId) {
                    
                    var promiseNatureza = this.naturezaService.getById(instance.naturezaId);
                    promiseNatureza.then(result => {
                        this.ezSelectNatureza.setInputText(result.descricao);
                    }).finally(() => {
                        
                    });
                }

                if (instance.unidadeMedidaId) {
                    
                    var promiseUnidadeMedida = this.unidadeDeMedidaService.getById(instance.unidadeMedidaId);
                    promiseUnidadeMedida.then(result => {
                        this.ezSelectUnidadeMedida.setInputText(result.descricao);
                    }).finally(() => {
                        
                    });
                }

                if (instance.operadoraId) {
                    
                    var promiseOperadora = this.operadoraService.getById(instance.operadoraId);
                    promiseOperadora.then(result => {
                        this.ezSelectOperadora.setInputText(result.nome);
                    }).finally(() => {
                        
                    });
                }

                if (instance.relatorioFichaDeEntidadeId) {

                  var promise = this.relatorioEzlivService.getById(instance.relatorioFichaDeEntidadeId);
                  promise.then(result => {
                    this.ezSelectRelatorioFichaDaEntidade.setInputText(result.nome);
                  }).finally(() => {

                  });
                }

                if (instance.relatorioProspostaDeContratacaoId) {

                  var promise = this.relatorioEzlivService.getById(instance.relatorioProspostaDeContratacaoId);
                  promise.then(result => {
                    this.ezSelectRelatorioPropostaDeContratacao.setInputText(result.nome);
                  }).finally(() => {

                  });
                }

                //if (instance.associacaoId) {
                //    this.ezSelectAssociacao.loading = true;
                //    var promiseAssociacao = this.associacaoService.getById(instance.associacaoId);
                //    promiseAssociacao.then(result => {
                //        this.ezSelectAssociacao.setInputText(result.nomeFantasia);
                //    }).finally(() => {
                //        this.ezSelectAssociacao.loading = false;
                //    });
                //}

                this.$scope.$watchCollection('entity.especialidades', (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.especialidades;
                });
                this.inicializarEspecialidadeCurrent();

                instance.aditivos = new Array<relatorioDtos.Dtos.Global.Geral.RelatorioInput>();
                instance.estados = new Array<estadoDtos.Dtos.Estado.EstadoInput>();
                instance.cidades = new Array<cidadeDtos.Dtos.Cidade.CidadeInput>();
                instance.chancelas = new Array<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>();

                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                this.acertarDatas();

                if (entity.naturezaId) {
                    
                    var promiseNatureza = this.naturezaService.getById(entity.naturezaId);
                    promiseNatureza.then(result => {
                        this.ezSelectNatureza.setInputText(result.descricao);
                    }).finally(() => {
                        
                    });
                }
                if (entity.unidadeMedidaId) {
                    
                    var promiseUnidadeMedida = this.unidadeDeMedidaService.getById(entity.unidadeMedidaId);
                    promiseUnidadeMedida.then(result => {
                        this.ezSelectUnidadeMedida.setInputText(result.descricao);
                    }).finally(() => {
                        
                    });
                }

                if (entity.operadoraId) {
                    
                    var promiseOperadora = this.operadoraService.getById(entity.operadoraId);
                    promiseOperadora.then(result => {
                        this.ezSelectOperadora.setInputText(result.nome);
                    }).finally(() => {
                        
                    });
                }

                if (entity.relatorioFichaDeEntidadeId) {

                  var promise = this.relatorioEzlivService.getById(entity.relatorioFichaDeEntidadeId);
                  promise.then(result => {
                    this.ezSelectRelatorioFichaDaEntidade.setInputText(result.nome);
                  }).finally(() => {

                  });
                }

                if (entity.relatorioProspostaDeContratacaoId) {

                  var promise = this.relatorioEzlivService.getById(entity.relatorioProspostaDeContratacaoId);
                  promise.then(result => {
                    this.ezSelectRelatorioPropostaDeContratacao.setInputText(result.nome);
                  }).finally(() => {

                  });
                }

                if (entity.redeCredenciadaId) {
                    
                    var promiseRedeCredenciada = this.redeCredenciadaService.getById(entity.redeCredenciadaId);
                    promiseRedeCredenciada.then(result => {
                        this.ezSelectRedeCredenciada.setInputText(result.nome);
                    }).finally(() => {
                        
                    });
                }

                if (entity.questionarioDeDeclaracaoDeSaudeId) {
                    
                    var promiseQuestionario = this.questionarioDeDeclaracaoDeSaudeService.getById(entity.questionarioDeDeclaracaoDeSaudeId);
                    promiseQuestionario.then(result => {
                        this.ezSelectQuestionario.setInputText(result.nome);
                    }).finally(() => {
                        
                    });
                }

                if (entity.declaracaoDoBeneficiarioId) {

                    var promiseDeclaracao = this.declaracaoDoBeneficiarioService.getById(entity.declaracaoDoBeneficiarioId);
                    promiseDeclaracao.then(result => {
                        this.ezSelectDeclaracaoDoBeneficiario.setInputText(result.nome);
                    }).finally(() => {

                    });
                }

                this.$scope.$watchCollection(() => { return this.entity.especialidades }, (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.especialidades;
                });

                this.getEstadoInput();
                this.getAditivoInput();
                this.getCidadeInput();
                this.getChancelaInput();
                this.getVigenciaInput();

                this.inicializarEspecialidadeCurrent();
                this.getEspecialidadeGrid();
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.events.onBeforeSaveEntity = () => {
                if (!this.entity.planoDeSaude.reembolso) {
                    this.entity.planoDeSaude.descricaoDoReembolso = null;
                }

                if (!this.entity.acompanhante) {
                    this.entity.descricaoDoAcompanhante = null;
                }

                if (!this.entity.coberturaExtra) {
                    this.entity.descricaoDaCoberturaExtra = null;
                }
            }

            this.ezSelectNaturezaConfig();
            this.ezSelectUnidadeMedidaConfig();
            this.ezSelectOperadoraConfig();
            this.ezSelectFaixaEtariaConfig();
            this.ezSelectEspecialidadeConfig();
            this.especialidadeGridConfig();
            this.ezSelectRedeCredenciadaConfig();
            this.ezSelectQuestionarioConfig();
            this.ezSelectDeclaracaoDoBeneficiarioConfig();
            this.ezSelectEstadoConfig();
            this.estadoGridConfigInput();
            this.aditivoGridConfigInput();
            this.ezSelectAditivoConfig();
            this.ezSelectCidadeConfig();
            this.cidadeGridConfigInput();
            this.vigenciaGridConfigInput();
            this.ezSelectChancelaConfig();
            this.chancelaGridConfigInput();
            this.ezSelectRelatorioFichaDaEntidadeConfig();
            this.ezSelectRelatorioPropostaDeContratacaoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectChancelaConfig() {
            this.requestParamsChancela = new requestParam.RequestParam.RequestParams();
            this.requestParamsChancela.sorting = "nome";

            this.chancelaSelecionado = (registro) => {
                this.chancelaCurrent = registro;
                return registro.nome;
            }

            this.chancelaDeselecionado = () => {
                this.chancelaCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoChancela = (termoDigitadoPesquisa) => {
                var filtro = new chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectChancela = new ezSelect.EzSelect.EzSelect<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoChancela,
                this.requestParamsChancela,
                this.$uibModal,
                this.chancelaSelecionado,
                this.chancelaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectChancela.onEzGridCreated = () => {

                this.ezSelectChancela.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.chancelaService.getPaged(filtro, requestParams);
                };

                this.ezSelectChancela.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Chancela.Nome'),
                    field: 'nome'
                });

                this.ezSelectChancela.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectChancela.on.beforeOpenModalDialog = () => {
                this.ezSelectChancela.ezGrid.getRegistros();
            };
        }

        private ezSelectRelatorioFichaDaEntidadeConfig() {
          this.requestParamsRelatorioFichaDaEntidade = new requestParam.RequestParam.RequestParams();
          this.requestParamsRelatorioFichaDaEntidade.sorting = "nome";

          this.relatorioFichaDaEntidadeSelecionado = (registro) => {
            this.entity.relatorioFichaDeEntidadeId = registro.id;
            return registro.nome;
          }

          this.relatorioFichaDaEntidadeDeselecionado = () => {
            this.entity.relatorioFichaDeEntidadeId = null;
          }

          // callback para criar e definir o filtro para buscar os registros
          this.getFiltroParaPaginacaoRelatorioFichaDaEntidade = (termoDigitadoPesquisa) => {
            var filtro = new relatorioDtos.Dtos.Global.Geral.RelatorioInput();
            filtro.nome = termoDigitadoPesquisa;
            filtro.sistema = SistemaEnum.EZLiv;
            filtro.tipoDeRelatorio = TipoDeRelatorioEzlivEnum.FichaDeAssociacao;
            return filtro;
          }

          this.ezSelectRelatorioFichaDaEntidade = new ezSelect.EzSelect.EzSelect < relatorioDtos.Dtos.Global.Geral.RelatorioInput, requestParam.RequestParam.RequestParams>(
            this.uiGridConstants,
            this.relatorioEzlivService,
            this.getFiltroParaPaginacaoRelatorioFichaDaEntidade,
            this.requestParamsRelatorioFichaDaEntidade,
            this.$uibModal,
            this.relatorioFichaDaEntidadeSelecionado,
            this.relatorioFichaDaEntidadeDeselecionado);

          // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
          // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
          // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
          this.ezSelectRelatorioFichaDaEntidade.onEzGridCreated = () => {

            this.ezSelectRelatorioFichaDaEntidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
              filtro.sistema = SistemaEnum.EZLiv;
              filtro.tipoDeRelatorio = TipoDeRelatorioEzlivEnum.FichaDeAssociacao;
              return this.relatorioEzlivService.getPaged(filtro, requestParams);
            };

            this.ezSelectRelatorioFichaDaEntidade.ezGrid.optionsGrid.columnDefs.push({
              name: app.localize('RelatorioFichaDaEntidade.Nome'),
              field: 'nome'
            });

            this.ezSelectRelatorioFichaDaEntidade.setLinkSelecionarPrimeiraColunaGrid();
          }

          // Eventos do ezSelect.
          // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
          this.ezSelectRelatorioFichaDaEntidade.on.beforeOpenModalDialog = () => {
            this.ezSelectRelatorioFichaDaEntidade.ezGrid.getRegistros();
          };
        }

        private ezSelectRelatorioPropostaDeContratacaoConfig() {
          this.requestParamsRelatorioPropostaDeContratacao = new requestParam.RequestParam.RequestParams();
          this.requestParamsRelatorioPropostaDeContratacao.sorting = "nome";

          this.relatorioPropostaDeContratacaoSelecionado = (registro) => {
            this.entity.relatorioProspostaDeContratacaoId = registro.id;
            return registro.nome;
          }

          this.relatorioPropostaDeContratacaoDeselecionado = () => {
            this.entity.relatorioProspostaDeContratacaoId = null;
          }

          // callback para criar e definir o filtro para buscar os registros
          this.getFiltroParaPaginacaoRelatorioPropostaDeContratacao = (termoDigitadoPesquisa) => {
            var filtro = new relatorioDtos.Dtos.Global.Geral.RelatorioInput();
            filtro.nome = termoDigitadoPesquisa;
            filtro.sistema = SistemaEnum.EZLiv;
            filtro.tipoDeRelatorio = TipoDeRelatorioEzlivEnum.PropostaDeContratacao;
            return filtro;
          }

          this.ezSelectRelatorioPropostaDeContratacao = new ezSelect.EzSelect.EzSelect < relatorioDtos.Dtos.Global.Geral.RelatorioInput, requestParam.RequestParam.RequestParams>(
            this.uiGridConstants,
            this.relatorioEzlivService,
            this.getFiltroParaPaginacaoRelatorioPropostaDeContratacao,
            this.requestParamsRelatorioPropostaDeContratacao,
            this.$uibModal,
            this.relatorioPropostaDeContratacaoSelecionado,
            this.relatorioPropostaDeContratacaoDeselecionado);

          // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
          // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
          // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
          this.ezSelectRelatorioPropostaDeContratacao.onEzGridCreated = () => {

            this.ezSelectRelatorioPropostaDeContratacao.ezGrid.serviceGetCallback = (filtro, requestParams) => {
              filtro.sistema = SistemaEnum.EZLiv;
              filtro.tipoDeRelatorio = TipoDeRelatorioEzlivEnum.PropostaDeContratacao;
              return this.relatorioEzlivService.getPaged(filtro, requestParams);
            };

            this.ezSelectRelatorioPropostaDeContratacao.ezGrid.optionsGrid.columnDefs.push({
              name: app.localize('RelatorioPropostaDeContratacao.Nome'),
              field: 'nome'
            });

            this.ezSelectRelatorioPropostaDeContratacao.setLinkSelecionarPrimeiraColunaGrid();
          }

          // Eventos do ezSelect.
          // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
          this.ezSelectRelatorioPropostaDeContratacao.on.beforeOpenModalDialog = () => {
            this.ezSelectRelatorioPropostaDeContratacao.ezGrid.getRegistros();
          };
        }

        private chancelaGridConfigInput() {
            this.requestParamsValoresInputChancela = new requestParam.RequestParam.RequestParams();

            this.chancelaAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput>();

            this.chancelaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirChancela, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridDataInputChancela = new Array<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput>();

            this.chancelaGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.chancelaAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputChancela = api;
                    this.gridApiInputChancela.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputChancela.sorting = null;
                        } else {
                            this.requestParamsValoresInputChancela.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getChancelaInput();
                    });
                    this.gridApiInputChancela.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputChancela.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputChancela.maxResultCount = pageSize;

                        this.getChancelaInput();
                    });
                }
            }

            this.chancelaGridOptionsInput.data = this.gridDataInputChancela;

            this.chancelaGridOptionsInput.columnDefs.push({
                name: app.localize('Chancela.Nome'),
                field: 'nome',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });
            this.getChancelaInput();
        }

        private ezSelectOperadoraConfig() {
            this.requestParamsOperadora = new requestParam.RequestParam.RequestParams();

            this.operadoraSelecionado = (registro) => {
                this.entity.operadoraId = registro.id;
                this.entity.operadora = registro;
                return registro.nome;
            }

            this.operadoraDeselecionado = () => {
                this.entity.operadoraId = null;
                this.entity.operadora = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoOperadora = (termoDigitadoPesquisa) => {
                var filtro = new operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectOperadora = new ezSelect.EzSelect.EzSelect<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.operadoraService,
                this.getFiltroParaPaginacaoOperadora,
                this.requestParamsOperadora,
                this.$uibModal,
                this.operadoraSelecionado,
                this.operadoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectOperadora.onEzGridCreated = () => {
                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Operadora.Nome'),
                    field: 'nome'
                });

                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica'),
                    field: 'nomePessoa'
                });

                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Operadora.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectOperadora.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectOperadora.on.beforeOpenModalDialog = () => {
                this.ezSelectOperadora.ezGrid.getRegistros();
            };
        }

        private ezSelectFaixaEtariaConfig() {
            this.requestParamsFaixaEtaria = new requestParam.RequestParam.RequestParams();

            this.faixaEtariaSelecionado = (registro) => {
                return registro.descricao;
            }

            this.faixaEtariaDeselecionado = () => {
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaFaixaEtaria = (termoDigitadoPesquisa) => {
                var filtro = new faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectFaixaEtaria = new ezSelect.EzSelect.EzSelect<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.faixaEtariaService,
                this.getFiltroParaFaixaEtaria,
                this.requestParamsFaixaEtaria,
                this.$uibModal,
                this.faixaEtariaSelecionado,
                this.faixaEtariaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectFaixaEtaria.onEzGridCreated = () => {
                this.ezSelectFaixaEtaria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('FaixaEtaria.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectFaixaEtaria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('FaixaEtaria.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectFaixaEtaria.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectFaixaEtaria.on.beforeOpenModalDialog = () => {
                this.ezSelectFaixaEtaria.ezGrid.getRegistros();
            };
        }

        private ezSelectNaturezaConfig() {
            this.requestParamsNatureza = new requestParam.RequestParam.RequestParams();

            this.naturezaSelecionado = (registro) => {
                this.entity.naturezaId = registro.id;
                return registro.descricao;
            }

            this.naturezaDeselecionado = () => {
                this.entity.naturezaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaNatureza = (termoDigitadoPesquisa) => {
                var filtro = new naturezaDtos.Dtos.Natureza.GetNaturezaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectNatureza = new ezSelect.EzSelect.EzSelect<naturezaDtos.Dtos.Natureza.NaturezaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.naturezaService,
                this.getFiltroParaNatureza,
                this.requestParamsNatureza,
                this.$uibModal,
                this.naturezaSelecionado,
                this.naturezaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectNatureza.onEzGridCreated = () => {
                this.ezSelectNatureza.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Natureza'),
                    field: 'descricao'
                });

                this.ezSelectNatureza.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectNatureza.on.beforeOpenModalDialog = () => {
                this.ezSelectNatureza.ezGrid.getRegistros();
            };
        }

        private ezSelectUnidadeMedidaConfig() {
            this.requestParamsUnidadeMedida = new requestParam.RequestParam.RequestParams();

            this.unidadeMedidaSelecionado = (registro) => {
                this.entity.unidadeMedidaId = registro.id;
                return registro.descricao;
            }

            this.unidadeMedidaDeselecionado = () => {
                this.entity.unidadeMedidaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaUnidadeMedida = (termoDigitadoPesquisa) => {
                var filtro = new unidadeMedidaDtos.Dtos.UnidadeMedida.GetUnidadeMedidaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectUnidadeMedida = new ezSelect.EzSelect.EzSelect<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.unidadeDeMedidaService,
                this.getFiltroParaUnidadeMedida,
                this.requestParamsUnidadeMedida,
                this.$uibModal,
                this.unidadeMedidaSelecionado,
                this.unidadeMedidaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectUnidadeMedida.onEzGridCreated = () => {
                this.ezSelectUnidadeMedida.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.UnidadeMedida'),
                    field: 'descricao'
                });

                this.ezSelectUnidadeMedida.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectUnidadeMedida.on.beforeOpenModalDialog = () => {
                this.ezSelectUnidadeMedida.ezGrid.getRegistros();
            };
        }

        private ezSelectRedeCredenciadaConfig() {
            this.requestParamsRedeCredenciada = new requestParam.RequestParam.RequestParams();

            this.redeCredenciadaSelecionado = (registro) => {
                this.entity.redeCredenciadaId = registro.id;
                return registro.nome;
            }

            this.redeCredenciadaDeselecionado = () => {
                this.entity.redeCredenciadaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaRedeCredenciada = (termoDigitadoPesquisa) => {
                var filtro = new redeCredenciadaDtos.Dtos.EZLiv.Geral.GetRedeCredenciadaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectRedeCredenciada = new ezSelect.EzSelect.EzSelect<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.redeCredenciadaService,
                this.getFiltroParaRedeCredenciada,
                this.requestParamsRedeCredenciada,
                this.$uibModal,
                this.redeCredenciadaSelecionado,
                this.redeCredenciadaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectRedeCredenciada.onEzGridCreated = () => {
                this.ezSelectRedeCredenciada.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('RedeCredenciada.Nome'),
                    field: 'nome'
                });

                this.ezSelectRedeCredenciada.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('RedeCredenciada.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectRedeCredenciada.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectRedeCredenciada.on.beforeOpenModalDialog = () => {
                this.ezSelectRedeCredenciada.ezGrid.getRegistros();
            };
        }

        private ezSelectQuestionarioConfig() {
            this.requestParamsQuestionario = new requestParam.RequestParam.RequestParams();

            this.questionarioSelecionado = (registro) => {
                this.entity.questionarioDeDeclaracaoDeSaudeId = registro.id;
                return registro.nome;
            }

            this.questionarioDeselecionado = () => {
                this.entity.questionarioDeDeclaracaoDeSaudeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaQuestionario = (termoDigitadoPesquisa) => {
                var filtro = new questionarioDtos.Dtos.EZLiv.Geral.GetQuestionarioDeDeclaracaoDeSaudeInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectQuestionario = new ezSelect.EzSelect.EzSelect<questionarioDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.questionarioDeDeclaracaoDeSaudeService,
                this.getFiltroParaQuestionario,
                this.requestParamsQuestionario,
                this.$uibModal,
                this.questionarioSelecionado,
                this.questionarioDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectQuestionario.onEzGridCreated = () => {
                this.ezSelectQuestionario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('QuestionarioDeDeclaracaoDeSaude.Nome'),
                    field: 'nome'
                });

                this.ezSelectQuestionario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('QuestionarioDeDeclaracaoDeSaude.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectQuestionario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('QuestionarioDeDeclaracaoDeSaude.Operadora'),
                    field: 'operadora.nome'
                });

                this.ezSelectQuestionario.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectQuestionario.on.beforeOpenModalDialog = () => {
                this.ezSelectQuestionario.ezGrid.getRegistros();
            };
        }

        private ezSelectDeclaracaoDoBeneficiarioConfig() {
            this.requestParamsDeclaracaoDoBeneficiario = new requestParam.RequestParam.RequestParams();

            this.declaracaoDoBeneficiarioSelecionado = (registro) => {
                this.entity.declaracaoDoBeneficiarioId = registro.id;
                return registro.nome;
            }

            this.declaracaoDoBeneficiarioDeselecionado = () => {
                this.entity.declaracaoDoBeneficiarioId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaDeclaracaoDoBeneficiario = (termoDigitadoPesquisa) => {
                var filtro = new declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.GetDeclaracaoDoBeneficiarioInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectDeclaracaoDoBeneficiario = new ezSelect.EzSelect.EzSelect<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.declaracaoDoBeneficiarioService,
                this.getFiltroParaDeclaracaoDoBeneficiario,
                this.requestParamsDeclaracaoDoBeneficiario,
                this.$uibModal,
                this.declaracaoDoBeneficiarioSelecionado,
                this.declaracaoDoBeneficiarioDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectDeclaracaoDoBeneficiario.onEzGridCreated = () => {
                this.ezSelectDeclaracaoDoBeneficiario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('DeclaracaoDoBeneficiario.Nome'),
                    field: 'nome'
                });

                this.ezSelectDeclaracaoDoBeneficiario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('DeclaracaoDoBeneficiario.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectDeclaracaoDoBeneficiario.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectDeclaracaoDoBeneficiario.on.beforeOpenModalDialog = () => {
                this.ezSelectDeclaracaoDoBeneficiario.ezGrid.getRegistros();
            };
        }

        private acertarDatas() {
            if (this.entity.contrato) {
                this.entity.contrato.dataInicioVigencia = new Date(this.entity.contrato.dataInicioVigencia.toString());
                this.entity.contrato.dataFimVigencia = new Date(this.entity.contrato.dataFimVigencia.toString());
            }
        }

        setProdutoDePlanoDeSaudeForm(form: ng.IFormController) {
            this.produtoDePlanoDeSaudeForm = form;
        }

        setEspecialidadeForm(form: ng.IFormController) {
            this.especialidadeForm = form;
        }

        /* Especialidade Current */
        private inicializarEspecialidadeCurrent() {
            this.especialidadeCurrent = new especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput();
            this.especialidadeCurrent.especialidade = new especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto();
            this.especialidadeCurrent.isActive = true;
            if (this.entity)
                this.especialidadeCurrent.produtoDePlanoDeSaudeId = this.entity.id;
        }

        private getEspecialidadeByTempId(tempId: number): especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto {
            for (let i = 0; i < this.entity.especialidades.length; i++) {
                if (this.entity.especialidades[i].id === tempId) {
                    return this.entity.especialidades[i];
                }
            }
        }

        private gravarEspecialidade(item: any) {
            this.saving = true;
            this.especialidadeCurrent.produtoDePlanoDeSaudeId = this.entity.id;
            var promise = (this.especialidadePorProdutoDePlanoDeSaudeService as any as especialidadePorProdutoDePlanoDeSaudeService.Services.EspecialidadePorProdutoDePlanoDeSaudeService).save(item);
            promise.then(result => {
                this.especialidadeCurrent.id = result.id;
                if (!this.editandoEspecialidade) {
                    this.entity.especialidades.push(this.especialidadeCurrent);
                }
                this.editandoEspecialidade = false;
                this.gridDataEnumerable = this.entity.especialidades;
                this.getEspecialidadeGrid();
                this.limparEspecialidade();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarEspecialidade() {
            if (!this.editandoEspecialidade) {
                this.gravarEspecialidade(this.especialidadeCurrent);
            }
            else {
                var item = this.getEspecialidadeByTempId(this.especialidadeCurrent.id);
                angular.merge(item, this.especialidadeCurrent);
                this.gravarEspecialidade(item);
            }
        }

        private ezSelectEspecialidadeConfig() {
            this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();

            this.especialidadeSelecionado = (registro) => {
                this.especialidadeCurrent.especialidadeId = registro.id;
                this.especialidadeCurrent.especialidade = registro;
                return registro.nome;
            }

            this.especialidadeDeselecionado = () => {
                this.especialidadeCurrent.especialidadeId = null;
                this.especialidadeCurrent.especialidade = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaEspecialidade = (termoDigitadoPesquisa) => {
                var filtro = new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEspecialidade = new ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaEspecialidade,
                this.requestParamsEspecialidade,
                this.$uibModal,
                this.especialidadeSelecionado,
                this.especialidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEspecialidade.onEzGridCreated = () => {
                this.ezSelectEspecialidade.ezGrid.serviceGetCallback = () => {
                    this.idsEspecialidades = new Array();
                    if (this.entity.especialidades != null)
                        this.entity.especialidades.forEach(x => this.idsEspecialidades.push(x.especialidade.id));
                    var getids = new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput();
                    getids.ids = this.idsEspecialidades;

                    return this.especialidadeService.getPaginadoExceptForIds(getids);
                };
                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectEspecialidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectEspecialidade.on.beforeOpenModalDialog = () => {
                this.ezSelectEspecialidade.ezGrid.getRegistros();
            };
        }

        private especialidadeGridConfig() {
            this.requestParamsGridEspecialidade = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridEspecialidade.sorting = 'Id';

            this.especialidadeAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>();
            this.especialidadeAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarEspecialidade, 'Edit', true));
            this.especialidadeAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirEspecialidade, 'Delete', true));

            this.especialidadeGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.especialidadeAppScopeProvider
            }

            var lista = this.entity ? this.entity.especialidades : new Array<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>();
            this.especialidadeGridOptions.data = lista;
            this.especialidadeGridOptions.totalItems = lista.length;

            this.especialidadeGridOptions.columnDefs.push({
                name: app.localize('Especialidade.Nome'),
                field: 'especialidade.nome',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();
        }

        private ezSelectEstadoConfig() {
            this.requestParamsEstado = new requestParam.RequestParam.RequestParams();
            this.requestParamsEstado.sorting = "nome";

            this.estadoSelecionado = (registro) => {
                this.estadoCurrent = registro;
                return registro.nome;
            }

            this.estadoDeselecionado = () => {
                this.estadoCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstado = (termoDigitadoPesquisa) => {
                var filtro = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEstadoExceptForProduto();
                filtro.nome = termoDigitadoPesquisa;
                filtro.produtoDePlanoDeSaudeId = this.entity.id;
                filtro.estados = this.entity.estados;
                return filtro;
            }

            this.ezSelectEstado = new ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoEstado,
                this.requestParamsEstado,
                this.$uibModal,
                this.estadoSelecionado,
                this.estadoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstado.onEzGridCreated = () => {

                this.ezSelectEstado.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.produtoDePlanoDeSaudeService.getEstadoExceptForProduto(filtro, requestParams);
                };

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstado.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstado.on.beforeOpenModalDialog = () => {
                this.ezSelectEstado.ezGrid.getRegistros();
            };
        }

        private estadoGridConfigInput() {
            this.requestParamsValoresInputEstado = new requestParam.RequestParam.RequestParams();

            this.estadoAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<estadoDtos.Dtos.Estado.EstadoInput>();

            this.estadoAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirEstado, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Edit');
                }
            ));

            this.gridDataInputEstado = new Array<estadoDtos.Dtos.Estado.EstadoInput>();

            this.estadoGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.estadoAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputEstado = api;
                    this.gridApiInputEstado.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputEstado.sorting = null;
                        } else {
                            this.requestParamsValoresInputEstado.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getEstadoInput();
                    });
                    this.gridApiInputEstado.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputEstado.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputEstado.maxResultCount = pageSize;

                        this.getEstadoInput();
                    });
                }
            }

            this.estadoGridOptionsInput.data = this.gridDataInputEstado;

            this.estadoGridOptionsInput.columnDefs.push({
                name: app.localize('Estado.Nome'),
                field: 'nome',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });
            this.getEstadoInput();
        }

        private ezSelectAditivoConfig() {
          this.requestParamsAditivo = new requestParam.RequestParam.RequestParams();
          this.requestParamsAditivo.sorting = "nome";

          this.aditivoSelecionado = (registro) => {
            this.aditivoCurrent = registro;
            return registro.nome;
          }

          this.aditivoDeselecionado = () => {
            this.aditivoCurrent = null;
          }

          // callback para criar e definir o filtro para buscar os registros
          this.getFiltroParaPaginacaoAditivo = (termoDigitadoPesquisa) => {
            var filtro = new relatorioDtos.Dtos.Global.Geral.GetRelatorioExceptForIdInput();
            filtro.nome = termoDigitadoPesquisa;
            filtro.relatorioId = this.entity.id;
            filtro.tipoDeRelatorio = TipoDeRelatorioEzlivEnum.Aditivo;
            filtro.relatorios = this.entity.aditivos;
            return filtro;
          }

          this.ezSelectAditivo = new ezSelect.EzSelect.EzSelect<relatorioDtos.Dtos.Global.Geral.RelatorioInput, requestParam.RequestParam.RequestParams>(
            this.uiGridConstants,
            this.especialidadeService,
            this.getFiltroParaPaginacaoAditivo,
            this.requestParamsAditivo,
            this.$uibModal,
            this.aditivoSelecionado,
            this.aditivoDeselecionado);

          // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
          // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
          // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
          this.ezSelectAditivo.onEzGridCreated = () => {

            this.ezSelectAditivo.ezGrid.serviceGetCallback = (filtro, requestParams) => {
              return this.relatorioEzlivService.getRelatorioExceptForIdInput(filtro, requestParams);
            };

            this.ezSelectAditivo.ezGrid.optionsGrid.columnDefs.push({
              name: app.localize('Aditivo.Nome'),
              field: 'nome'
            });

            this.ezSelectAditivo.setLinkSelecionarPrimeiraColunaGrid();
          }

          // Eventos do ezSelect.
          // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
          this.ezSelectAditivo.on.beforeOpenModalDialog = () => {
            this.ezSelectAditivo.ezGrid.getRegistros();
          };
        }

        private aditivoGridConfigInput() {
          this.requestParamsValoresInputAditivo = new requestParam.RequestParam.RequestParams();

          this.aditivoAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<relatorioDtos.Dtos.Global.Geral.RelatorioInput>();

          this.aditivoAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirAditivo, 'Delete',
            (row) => {
              return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Edit');
            }
          ));

          this.gridDataInputAditivo = new Array<relatorioDtos.Dtos.Global.Geral.RelatorioInput>();

          this.aditivoGridOptionsInput = {
            enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
            enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
            paginationPageSizes: app.consts.grid.defaultPageSizes,
            paginationPageSize: app.consts.grid.defaultPageSize,
            useExternalPagination: true,
            showColumnFooter: true,
            rowHeight: 50,
            enableGridMenu: true,
            exporterMenuCsv: false,
            exporterMenuPdf: false,
            rowTemplate: 'App/common/views/common/grid/linha.html',
            columnDefs: [],
            appScopeProvider: this.aditivoAppScopeProviderInput,
            onRegisterApi: (api) => {
              this.gridApiInputAditivo = api;
              this.gridApiInputAditivo.core.on.sortChanged(null, (grid, sortColumns) => {
                if (!sortColumns.length || !sortColumns[0].field) {
                  this.requestParamsValoresInputAditivo.sorting = null;
                } else {
                  this.requestParamsValoresInputAditivo.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                }

                this.getAditivoInput();
              });
              this.gridApiInputAditivo.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                this.requestParamsValoresInputAditivo.skipCount = (pageNumber - 1) * pageSize;
                this.requestParamsValoresInputAditivo.maxResultCount = pageSize;

                this.getAditivoInput();
              });
            }
          }

          this.aditivoGridOptionsInput.data = this.gridDataInputAditivo;

          this.aditivoGridOptionsInput.columnDefs.push({
            name: app.localize('Aditivo.Nome'),
            field: 'nome',
            cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
          });
          this.getAditivoInput();
        }

        private ezSelectCidadeConfig() {
            this.requestParamsCidade = new requestParam.RequestParam.RequestParams();
            this.requestParamsCidade.sorting = "nome";

            this.cidadeSelecionado = (registro) => {
                this.cidadeCurrent = registro;
                return registro.nome;
            }

            this.cidadeDeselecionado = () => {
                this.cidadeCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCidade = (termoDigitadoPesquisa) => {
                var filtro = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetCidadeExceptForProduto();
                filtro.nome = termoDigitadoPesquisa;
                filtro.produtoDePlanoDeSaudeId = this.entity.id;
                filtro.cidades = this.entity.cidades;
                return filtro;
            }

            this.ezSelectCidade = new ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoCidade,
                this.requestParamsCidade,
                this.$uibModal,
                this.cidadeSelecionado,
                this.cidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCidade.onEzGridCreated = () => {

                this.ezSelectCidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.produtoDePlanoDeSaudeService.getCidadeExceptForProduto(filtro, requestParams);
                };

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectCidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCidade.on.beforeOpenModalDialog = () => {
                this.ezSelectCidade.ezGrid.getRegistros();
            };
        }

        private cidadeGridConfigInput() {
            this.requestParamsValoresInputCidade = new requestParam.RequestParam.RequestParams();

            this.cidadeAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<cidadeDtos.Dtos.Cidade.CidadeInput>();

            this.cidadeAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirCidade, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Edit');
                }
            ));

            this.gridDataInputCidade = new Array<cidadeDtos.Dtos.Cidade.CidadeInput>();

            this.cidadeGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.cidadeAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputCidade = api;
                    this.gridApiInputCidade.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputCidade.sorting = null;
                        } else {
                            this.requestParamsValoresInputCidade.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getCidadeInput();
                    });
                    this.gridApiInputCidade.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputCidade.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputCidade.maxResultCount = pageSize;

                        this.getCidadeInput();
                    });
                }
            }

            this.cidadeGridOptionsInput.data = this.gridDataInputCidade;

            this.cidadeGridOptionsInput.columnDefs.push({
                name: app.localize('Cidade.Nome'),
                field: 'nome',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });
            this.getCidadeInput();
        }

        private vigenciaGridConfigInput() {
            this.requestParamsValoresInputVigencia = new requestParam.RequestParam.RequestParams();

            this.vigenciaAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput>();

            this.vigenciaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarVigencia, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Editar');
                }
            ));

            this.vigenciaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirVigencia, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ProdutoDePlanoDeSaude.Delete');
                }
            ));

            this.gridDataInputVigencia = new Array<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput>();

            this.vigenciaGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.vigenciaAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputVigencia = api;
                    this.gridApiInputVigencia.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputVigencia.sorting = null;
                        } else {
                            this.requestParamsValoresInputVigencia.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getVigenciaInput();
                    });
                    this.gridApiInputVigencia.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputVigencia.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputVigencia.maxResultCount = pageSize;

                        this.getVigenciaInput();
                    });
                }
            }

            this.vigenciaGridOptionsInput.data = this.gridDataInputVigencia;

            this.vigenciaGridOptionsInput.columnDefs.push({
                name: app.localize('Vigencia.NumeroDeBoletos'),
                field: 'numeroDeBoletos',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.vigenciaGridOptionsInput.columnDefs.push({
                name: app.localize('Vigencia.DataDePropostaInicial'),
                field: 'dataInicialDaProposta'
            });

            this.vigenciaGridOptionsInput.columnDefs.push({
                name: app.localize('Vigencia.DataDePropostaFinal'),
                field: 'dataFinalDaProposta'
            });

            this.vigenciaGridOptionsInput.columnDefs.push({
                name: app.localize('Vigencia.DataDeFechamento'),
                field: 'dataDeFechamento'
            });

            this.vigenciaGridOptionsInput.columnDefs.push({
                name: app.localize('Vigencia.DataDeVigencia'),
                field: 'dataDeVigencia'
            });

            this.getVigenciaInput();
        }
    }

    function containsArray(arr, findValue) {
        var i = arr.length;

        while (i--) {
            if (arr[i] === findValue) return true;
        }
        return false;
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ProdutoDePlanoDeSaudeFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeForm.cshtml';
            this.controller = ProdutoDePlanoDeSaudeFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}