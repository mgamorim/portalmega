﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as laboratorioService from "../../../../services/domain/ezliv/laboratorio/laboratorioService";
import * as laboratorioDtos from "../../../../dtos/ezliv/geral/laboratorio/laboratorioDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Laboratorio {

    class LaboratorioIndexController extends
        controllers.Controllers.ControllerIndexBase<laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioListDto,
        laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput> {

        static $inject = ['$state', 'uiGridConstants', 'laboratorioService'];

        public ezGrid: ezGrid.EzGrid.EzGrid<laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public laboratorioService: laboratorioService.Services.LaboratorioService) {

            super(
                $state,
                uiGridConstants,
                laboratorioService, {
                    rotaAlterarRegistro: 'tenant.ezliv.laboratorio.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.laboratorio.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'site';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new laboratorioDtos.Dtos.EZLiv.Geral.GetLaboratorioInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.email = termoDigitadoPesquisa;
                filtro.site = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.laboratorioService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            //// objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewLaboratorio', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Laboratorio.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Laboratorio.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Laboratorio.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Laboratorio.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Laboratorio.NomeFantasia'),
                field: 'pessoaJuridica.nomeFantasia',
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Laboratorio.RazaoSocial'),
                field: 'pessoaJuridica.razaoSocial',
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Laboratorio.Site'),
                field: 'site'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Laboratorio.Email'),
                field: 'email'
            });
        }
    }

    export class LaboratorioIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/laboratorio/laboratorioIndex.cshtml';
            this.controller = LaboratorioIndexController;
        }
    }
}