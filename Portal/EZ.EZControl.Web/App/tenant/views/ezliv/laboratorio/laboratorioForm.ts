﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

import * as laboratorioService from "../../../../services/domain/ezliv/laboratorio/laboratorioService";
import * as laboratorioDtos from "../../../../dtos/ezliv/geral/laboratorio/laboratorioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";

import * as enderecoPessoaDtos from "../../../../dtos/global/endereco/enderecoDtos";
import * as enderecoService from "../../../../services/domain/global/endereco/enderecoService";

import * as telefoneDtos from "../../../../dtos/global/telefone/telefoneDtos";
import * as telefoneService from "../../../../services/domain/global/telefone/telefoneService";

import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";


export module Forms {
    export class LaboratorioFormController extends
        controllers.Controllers.ControllerFormComponentBase<laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput,
        laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        // pessoa
        public pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        public ezSelectPessoaJuridica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaJuridica: (termoDigitadoPesquisa: string) => any;
        private pessoaJuridicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto) => string;
        private pessoaJuridicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaJuridica: (id: number) => void;


        // Endereço
        public enderecoPessoaJuridica: enderecoPessoaDtos.Dtos.Endereco.EnderecoInput;
        public ezSelectEnderecoPessoaJuridica: ezSelect.EzSelect.EzSelect<enderecoPessoaDtos.Dtos.Endereco.EnderecoInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEnderecoPessoaJuridica: (termoDigitadoPesquisa: string) => any;
        private enderecoPessoaJuridicaSelecionado: (registro: enderecoPessoaDtos.Dtos.Endereco.EnderecoInput) => string;
        private enderecoPessoaJuridicaDeselecionado: () => void;
        private requestParamsEnderecoPessoaJuridica: requestParam.RequestParam.RequestParams;
        private carregarEnderecoPessoaJuridica: (id: number) => void;

        // Telefone
        public telefone1: telefoneDtos.Dtos.Telefone.GetTelefoneInput;
        public ezSelectTelefone1: ezSelect.EzSelect.EzSelect<telefoneDtos.Dtos.Telefone.TelefoneInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTelefone: (termoDigitadoPesquisa: string) => any;
        private telefoneSelecionado: (registro: telefoneDtos.Dtos.Telefone.TelefoneInput) => string;
        private telefoneDeselecionado: () => void;
        private requestParamsTelefone: requestParam.RequestParam.RequestParams;
        private carregarTelefone: (id: number) => void;

        // Telefone
        public telefone2: telefoneDtos.Dtos.Telefone.GetTelefoneInput;
        public ezSelectTelefone2: ezSelect.EzSelect.EzSelect<telefoneDtos.Dtos.Telefone.TelefoneInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTelefone2: (termoDigitadoPesquisa: string) => any;
        private telefoneSelecionado2: (registro: telefoneDtos.Dtos.Telefone.TelefoneInput) => string;
        private telefoneDeselecionado2: () => void;
        private requestParamsTelefone2: requestParam.RequestParam.RequestParams;
        private carregarTelefone2: (id: number) => void;

        // Especialidade
        public especialidade: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput;
        public ezSelectEspecialidade: ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEspecialidade: (termoDigitadoPesquisa: string) => any;
        private especialidadeSelecionado: (registro: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput) => string;
        private especialidadeDeselecionado: () => void;
        private requestParamsEspecialidade: requestParam.RequestParam.RequestParams;

        public addEspecialidade: () => void;
        public limparEntidade: () => void;
        public getEspecialidadeInput: () => void;
        public especialidadeCurrent: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput;
        public saving: boolean;
        public editing: boolean;
        public excluir: (especialidade: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput) => void;
        public cancelEditEspecialidade: () => void;


        private setDataGridInput: (result: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[]) => void;
        private gridApiInput: uiGrid.IGridApiOf<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>;
        private requestParamsValoresInput: requestParam.RequestParam.RequestParams;
        private especialidadeAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>;
        public loadingEspecialidadeInput: boolean;
        public especialidadeGridOptionsInput: uiGrid.IGridOptionsOf<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>;
        private gridDataInput: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];

        private carregaTelefoneComDDD: (ddd: string, numero: string) => string;

        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'laboratorioService', 'pessoaJuridicaService', 'enderecoService', 'telefoneService', 'especialidadeService', '$uibModal'];

        constructor(
            public $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public laboratorioService: laboratorioService.Services.LaboratorioService,
            public pessoaJuridicaService: pessoaJuridicaService.Services.PessoaJuridicaService,
            public enderecoService: enderecoService.Services.EnderecoService,
            public telefoneService: telefoneService.Services.TelefoneService,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                laboratorioService,
                'tenant.ezliv.laboratorio');

            this.carregarPessoaJuridica = (id: number) => {
                this.ezSelectPessoaJuridica.loading = true;
                var promise = this.pessoaJuridicaService.getById(id);
                promise.then(result => {
                    this.pessoaJuridica = result;
                    this.entity.pessoaJuridicaId = this.pessoaJuridica.id;
                    this.ezSelectPessoaJuridica.setInputText(result.nomeFantasia);
                }).finally(() => {
                    this.ezSelectPessoaJuridica.loading = false;
                });
            }

            this.carregarEnderecoPessoaJuridica = (id: number) => {
                this.ezSelectEnderecoPessoaJuridica.loading = true;
                var promise = this.enderecoService.getById(id);
                promise.then(result => {
                    this.enderecoPessoaJuridica = result;
                    this.ezSelectEnderecoPessoaJuridica.setInputText(result.descricao);
                }).finally(() => {
                    this.ezSelectEnderecoPessoaJuridica.loading = false;
                });
            }

            this.carregarTelefone = (id: number) => {
                this.ezSelectTelefone1.loading = true;
                var promise = this.telefoneService.getById(id);
                promise.then(result => {
                    this.telefone1 = result;
                    this.ezSelectTelefone1.setInputText(this.carregaTelefoneComDDD(result.ddd, result.numero));
                }).finally(() => {
                    this.ezSelectTelefone1.loading = false;
                });
            }

            this.carregarTelefone2 = (id: number) => {
                this.ezSelectTelefone2.loading = true;
                var promise = this.telefoneService.getById(id);
                promise.then(result => {
                    this.telefone2 = result;
                    this.ezSelectTelefone2.setInputText(this.carregaTelefoneComDDD(result.ddd, result.numero));
                }).finally(() => {
                    this.ezSelectTelefone2.loading = false;
                });
            }

            this.carregaTelefoneComDDD = (ddd: string, numero: string) =>
            {
                return "(" + ddd + ") " + numero;
            }

            this.addEspecialidade = () => {
                this.especialidadeCurrent.isActive = true;
                this.entity.especialidades.push(this.especialidadeCurrent);
                this.getEspecialidadeInput();
                this.limparEntidade();
            }

            this.limparEntidade = () => {
                this.especialidadeCurrent = new especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput();
                this.ezSelectEspecialidade.gridSelectClear();
            }

            this.getEspecialidadeInput = () => {
                this.loadingEntity = true;
                if (this.entity) {
                    this.setDataGridInput(this.entity.especialidades);
                }

                this.loadingEntity = false;
            }

            this.excluir = (especialidade: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput) => {
                this.editing = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.especialidades.length; i++) {
                                if (this.entity.especialidades[i].id === especialidade.id) {
                                    this.entity.especialidades.splice(i, 1);
                                    this.getEspecialidadeInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInput = (result: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[]) => {
                this.gridDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInput.push(result[i]);
                }

                this.especialidadeGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditEspecialidade = () => {
                this.especialidadeCurrent = new especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput;
                this.ezSelectEspecialidade.gridSelectClear();
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput;
                instance.isActive = true;
                instance.pessoaJuridicaId = 0;
                instance.especialidades = new Array<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.carregarPessoaJuridica(entity.pessoaJuridicaId);
                this.carregarEnderecoPessoaJuridica(entity.enderecoId);
                this.carregarTelefone(entity.telefone1Id);

                if (entity.telefone2Id > 0)
                {
                    this.carregarTelefone2(entity.telefone1Id);
                }
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.events.onBeforeSaveEntity = () => {
            };


            this.ezSelectPessoaConfig();
            this.ezSelectEnderecoPessoaJuridicaConfig();
            this.ezSelectTelefone1Config();
            this.ezSelectTelefone2Config();
            this.ezSelectEspecialidadeConfig();
            this.especialidadeGridConfigInput();


            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomeFantasia";

            this.pessoaJuridicaSelecionado = (registro) => {
                this.pessoaJuridica = registro;
                this.entity.pessoaJuridicaId = registro.id;
                this.entity.pessoaJuridica = registro;
                return registro.nomePessoa;
            }

            this.pessoaJuridicaDeselecionado = () => {
                this.pessoaJuridica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaJuridica = (termoDigitadoPesquisa) => {
                var filtro = new laboratorioDtos.Dtos.EZLiv.Geral.GetPessoaExceptForLaboratorio();
                filtro.nomePessoa = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaJuridica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaJuridicaService,
                this.getFiltroParaPaginacaoPessoaJuridica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaJuridicaSelecionado,
                this.pessoaJuridicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaJuridica.onEzGridCreated = () => {

                //this.ezSelectPessoaJuridica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                //    return this.laboratorioService.getPagedExceptForId(filtro, requestParams);
                //};

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.RazaoSocial'),
                    field: 'razaoSocial'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaJuridica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaJuridica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaJuridica.ezGrid.getRegistros();
            };
        }

        private ezSelectEnderecoPessoaJuridicaConfig() {
            this.requestParamsEnderecoPessoaJuridica = new requestParam.RequestParam.RequestParams();
            this.requestParamsEnderecoPessoaJuridica.sorting = "Descricao";

            this.enderecoPessoaJuridicaSelecionado = (registro) => {
                this.enderecoPessoaJuridica = registro;
                this.entity.endereco = registro;
                this.entity.enderecoId = registro.id;
                return registro.descricao;
            }

            this.enderecoPessoaJuridicaDeselecionado = () => {
                this.enderecoPessoaJuridica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEnderecoPessoaJuridica = (termoDigitadoPesquisa) => {
                var filtro = new enderecoPessoaDtos.Dtos.Endereco.GetEnderecoInput();
                filtro.descricao = termoDigitadoPesquisa;
                filtro.pessoaId = this.entity.pessoaJuridicaId;
                return filtro;
            }

            this.ezSelectEnderecoPessoaJuridica = new ezSelect.EzSelect.EzSelect<enderecoPessoaDtos.Dtos.Endereco.EnderecoInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.enderecoService,
                this.getFiltroParaPaginacaoEnderecoPessoaJuridica,
                this.requestParamsEnderecoPessoaJuridica,
                this.$uibModal,
                this.enderecoPessoaJuridicaSelecionado,
                this.enderecoPessoaJuridicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEnderecoPessoaJuridica.onEzGridCreated = () => {


                this.ezSelectEnderecoPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Endereco.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectEnderecoPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Endereco.CEP'),
                    field: 'cep'
                });

                this.ezSelectEnderecoPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Endereco.Logradouro'),
                    field: 'logradouro'
                });

                this.ezSelectEnderecoPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Endereco.Numero'),
                    field: 'numero'
                });

                this.ezSelectEnderecoPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Endereco.Complemento'),
                    field: 'complemento'
                });


                this.ezSelectEnderecoPessoaJuridica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEnderecoPessoaJuridica.on.beforeOpenModalDialog = () => {
                this.ezSelectEnderecoPessoaJuridica.ezGrid.getRegistros();
            };
        }

        private ezSelectTelefone1Config() {
            this.requestParamsTelefone = new requestParam.RequestParam.RequestParams();
            this.requestParamsTelefone.sorting = "numero";

            this.telefoneSelecionado = (registro) => {
                this.telefone1 = registro;
                this.entity.telefone1Id = registro.id;
                this.entity.telefone1 = registro;
                return this.carregaTelefoneComDDD(registro.ddd, registro.numero);
            }

            this.telefoneDeselecionado = () => {
                this.telefone1 = null;
                this.entity.telefone1 = null;
                this.entity.telefone1Id = 0;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTelefone = (termoDigitadoPesquisa) => {
                var filtro = new telefoneDtos.Dtos.Telefone.GetTelefoneInput();
                filtro.numero = termoDigitadoPesquisa;
                filtro.pessoaId = this.entity.pessoaJuridicaId;
                return filtro;
            }

            this.ezSelectTelefone1 = new ezSelect.EzSelect.EzSelect<telefoneDtos.Dtos.Telefone.TelefoneInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.telefoneService,
                this.getFiltroParaPaginacaoTelefone,
                this.requestParamsTelefone,
                this.$uibModal,
                this.telefoneSelecionado,
                this.telefoneDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTelefone1.onEzGridCreated = () => {

                this.ezSelectTelefone1.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.DDD'),
                    field: 'ddd'
                });

                this.ezSelectTelefone1.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Numero'),
                    field: 'numero'
                });

                this.ezSelectTelefone1.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Tipo'),
                    field: 'tipo'
                });


                this.ezSelectTelefone1.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTelefone1.on.beforeOpenModalDialog = () => {
                this.ezSelectTelefone1.ezGrid.getRegistros();
            };
        }

        private ezSelectTelefone2Config() {
            this.requestParamsTelefone2 = new requestParam.RequestParam.RequestParams();
            this.requestParamsTelefone2.sorting = "numero";

            this.telefoneSelecionado2 = (registro) => {
                this.telefone2 = registro;
                this.entity.telefone2Id = registro.id;
                this.entity.telefone2 = registro;
                return this.carregaTelefoneComDDD(registro.ddd, registro.numero);
            }

            this.telefoneDeselecionado2 = () => {
                this.telefone2 = null;
                this.entity.telefone2 = null;
                this.entity.telefone2Id = 0;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTelefone2 = (termoDigitadoPesquisa) => {
                var filtro = new telefoneDtos.Dtos.Telefone.GetTelefoneInput();
                filtro.numero = termoDigitadoPesquisa;
                filtro.pessoaId = this.entity.pessoaJuridicaId;
                return filtro;
            }

            this.ezSelectTelefone2 = new ezSelect.EzSelect.EzSelect<telefoneDtos.Dtos.Telefone.TelefoneInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.telefoneService,
                this.getFiltroParaPaginacaoTelefone2,
                this.requestParamsTelefone2,
                this.$uibModal,
                this.telefoneSelecionado2,
                this.telefoneDeselecionado2);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTelefone2.onEzGridCreated = () => {

                this.ezSelectTelefone2.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.DDD'),
                    field: 'ddd'
                });

                this.ezSelectTelefone2.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Numero'),
                    field: 'numero'
                });

                this.ezSelectTelefone2.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Tipo'),
                    field: 'tipo'
                });


                this.ezSelectTelefone2.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTelefone2.on.beforeOpenModalDialog = () => {
                this.ezSelectTelefone2.ezGrid.getRegistros();
            };
        }

        private ezSelectEspecialidadeConfig() {
            this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();
            this.requestParamsEspecialidade.sorting = "nome";

            this.especialidadeSelecionado = (registro) => {
                this.especialidadeCurrent = registro;
                return registro.nome;
            }

            this.especialidadeDeselecionado = () => {
                this.especialidadeCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEspecialidade = (termoDigitadoPesquisa) => {
                var filtro = new laboratorioDtos.Dtos.EZLiv.Geral.GetEspecialidadeExceptForLaboratorio();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                filtro.laboratorioId = this.entity.id;
                filtro.especialidades = this.entity.especialidades;
                return filtro;
            }

            this.ezSelectEspecialidade = new ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoEspecialidade,
                this.requestParamsEspecialidade,
                this.$uibModal,
                this.especialidadeSelecionado,
                this.especialidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEspecialidade.onEzGridCreated = () => {

                this.ezSelectEspecialidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.laboratorioService.getEspecialidadeExceptForLaboratorio(filtro, requestParams);
                };

                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Codigo'),
                    field: 'codigo'
                });

                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectEspecialidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEspecialidade.on.beforeOpenModalDialog = () => {
                this.ezSelectEspecialidade.ezGrid.getRegistros();
            };
        }

        private especialidadeGridConfigInput() {
            this.requestParamsValoresInput = new requestParam.RequestParam.RequestParams();

            this.especialidadeAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>();

           
            this.especialidadeAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ValorPorFaixaEtaria.Delete');
                }
            ));

            this.gridDataInput = new Array<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>();

            this.especialidadeGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.especialidadeAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInput = api;
                    this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInput.sorting = null;
                        } else {
                            this.requestParamsValoresInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getEspecialidadeInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInput.maxResultCount = pageSize;

                        this.getEspecialidadeInput();
                    });
                }
            }

            this.especialidadeGridOptionsInput.data = this.gridDataInput;


            this.especialidadeGridOptionsInput.columnDefs.push({
                name: app.localize('Especialidade.Codigo'),
                field: 'codigo',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.especialidadeGridOptionsInput.columnDefs.push({
                name: app.localize('Especialidade.Nome'),
                field: 'nome',
            });

            this.getEspecialidadeInput();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class LaboratorioFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/laboratorio/laboratorioForm.cshtml';
            this.controller = LaboratorioFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}