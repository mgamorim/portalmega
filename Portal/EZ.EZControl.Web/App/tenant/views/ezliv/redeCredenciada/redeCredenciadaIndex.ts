﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as redeCredenciadaService from "../../../../services/domain/ezliv/redeCredenciada/redeCredenciadaService";
import * as redeCredenciadaDtos from "../../../../dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.RedeCredenciada {

    class RedeCredenciadaIndexController extends controllers.Controllers.ControllerIndexBase <redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto, redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaInput>{
        public ezGrid: ezGrid.EzGrid.EzGrid<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        static $inject = ['$state', 'uiGridConstants', 'redeCredenciadaService'];
        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public redeCredenciadaService: redeCredenciadaService.Services.RedeCredenciadaService) {

            super(
                $state,
                uiGridConstants,
                redeCredenciadaService, {
                    rotaAlterarRegistro: 'tenant.ezliv.redeCredenciada.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.redeCredenciada.novo'
                }
            );
        }

        $onInit() {
            super.init();
            
            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new redeCredenciadaDtos.Dtos.EZLiv.Geral.GetRedeCredenciadaInput();
                filtro.nome = termoDigitadoPesquisa; 
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.redeCredenciadaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewRedeCredenciada', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.RedeCredenciada.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.RedeCredenciada.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.RedeCredenciada.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('RedeCredenciada.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('RedeCredenciada.Descricao'),
                field: 'descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('RedeCredenciada.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class RedeCredenciadaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/redeCredenciada/redeCredenciadaIndex.cshtml';
            this.controller = RedeCredenciadaIndexController;
        }
    }
}