﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as redeCredenciadaService from "../../../../services/domain/ezliv/redeCredenciada/redeCredenciadaService";
import * as itemDeRedeCredenciadaService from "../../../../services/domain/ezliv/itemDeRedeCredenciada/itemDeRedeCredenciadaService";
import * as unidadeDeSaudeService from "../../../../services/domain/ezliv/unidadeDeSaude/unidadeDeSaudeService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as redeCredenciadaDtos from "../../../../dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as itemDeRedeCredenciadaDtos from "../../../../dtos/ezliv/geral/itemDeRedeCredenciada/itemDeRedeCredenciadaDtos";
import * as unidadeSaudeBaseDtos from "../../../../dtos/ezliv/geral/unidadeSaudeBase/unidadeSaudeBaseDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Forms {
    export class RedeCredenciadaFormController extends controllers.Controllers.ControllerFormComponentBase<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaInput, redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public redeCredenciadaForm: ng.IFormController;
        public itemDeRedeCredenciadaForm: ng.IFormController; 
        public tipoDeUnidadeEnum: any;

        //EzSelect UnidadeSaudeBase
        public ezSelectUnidadeSaudeBase: ezSelect.EzSelect.EzSelect<unidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaUnidadeSaudeBase: (termoDigitadoPesquisa: string) => any;
        private unidadeSaudeBaseSelecionado: (registro: unidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto) => string;
        private unidadeSaudeBaseDeselecionado: () => void;
        private requestParamsUnidadeSaudeBase: requestParam.RequestParam.RequestParams;     

        // Item Rede Credenciada
        private gridDataEnumerable: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput[];
        private requestParamsItemDeRedeCredenciada: requestParam.RequestParam.RequestParams;
        private itemDeRedeCredenciadaAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput>;
        public loadingItemDeRedeCredenciada: boolean;
        public itemDeRedeCredenciadaGridOptions: uiGrid.IGridOptionsOf<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput>;
        public editarItem: (itemDeRedeCredenciada: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput) => void;
        public excluirItem: (itemDeRedeCredenciada: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput) => void;
        public limparItemDeRedeCredenciadaCurrent: () => void;
        public getItemGrid: () => void;
        public itemCurrent: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput;
        public editandoItem: boolean;     

        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        private createDescription: () => any;
        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'redeCredenciadaService', 'itemDeRedeCredenciadaService', 'unidadeDeSaudeService', '$uibModal'];        

        constructor(
            private $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public redeCredenciadaService: redeCredenciadaService.Services.RedeCredenciadaService,
            public itemDeRedeCredenciadaService: itemDeRedeCredenciadaService.Services.ItemDeRedeCredenciadaService,
            public unidadeDeSaudeService: unidadeDeSaudeService.Services.UnidadeDeSaudeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                redeCredenciadaService,
                'tenant.ezliv.redeCredenciada');

            this.editandoItem = false;

            this.editarItem = (itemDeRedeCredenciada: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput) => {
                this.editandoItem = true;
                this.itemCurrent = angular.copy(itemDeRedeCredenciada);
 
                if (itemDeRedeCredenciada.unidadeDeSaude != null)
                    this.ezSelectUnidadeSaudeBase.setInputText(itemDeRedeCredenciada.unidadeDeSaude.pessoaJuridica.nomeFantasia);
            }

            this.excluirItem = (itemDeRedeCredenciada: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput) => {
                abp.message.confirm(
                    '', 'Deseja realmente excluir este item?',
                    isConfirmed => {
                        if (isConfirmed) {
                            this.saving = true;
                            var promise = (this.itemDeRedeCredenciadaService as any as itemDeRedeCredenciadaService.Services.ItemDeRedeCredenciadaService).delete(itemDeRedeCredenciada.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarItemDeRedeCredenciadaCurrent();
                            }).finally(() => {
                                this.saving = false;
                            });

                            var index = this.entity.items.indexOf(itemDeRedeCredenciada);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.items.splice(index, 1);
                            }
                            this.editandoItem = false;
                        }
                    }
                );
            }

            this.limparItemDeRedeCredenciadaCurrent = () => {
                this.inicializarItemDeRedeCredenciadaCurrent();
                this.editandoItem = false;
                this.ezSelectUnidadeSaudeBase.setInputText("");
            }

            this.getItemGrid = () => {
                this.itemDeRedeCredenciadaGridOptions.data = this.entity.items;
                this.itemDeRedeCredenciadaGridOptions.totalItems = this.entity.items.length;
            }
        }

        $onInit() {
            super.init();
            this.tipoDeUnidadeEnum = ez.domain.enum.tipoDeUnidadeEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaInput();
                instance.id = 0;
                instance.isActive = true;

                this.$scope.$watchCollection('entity.items', (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });
                this.inicializarItemDeRedeCredenciadaCurrent();
                
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // Itens
                this.$scope.$watchCollection(() => { return this.entity.items }, (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });
                this.inicializarItemDeRedeCredenciadaCurrent();
                this.getItemGrid();
            }

            //this.events.onBeforeSaveEntity = () => {
                
            //}

            //this.createDescription = () => {
                
            //};

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };


            this.itemDeRedeCredenciadaGridConfig();
            this.ezSelectUnidadeSaudeBaseConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectUnidadeSaudeBaseConfig() {
            this.requestParamsUnidadeSaudeBase = new requestParam.RequestParam.RequestParams();

            this.unidadeSaudeBaseSelecionado = (registro) => {
                this.itemCurrent.unidadeDeSaudeId = registro.id;
                this.itemCurrent.unidadeDeSaude = registro;
                return registro.pessoaJuridica.nomeFantasia;
            }

            this.unidadeSaudeBaseDeselecionado = () => {
                this.itemCurrent.unidadeDeSaudeId = null;
                this.itemCurrent.unidadeDeSaude
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaUnidadeSaudeBase = (termoDigitadoPesquisa) => {
                var filtro = new unidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseInput();
                filtro.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput();
                filtro.pessoaJuridica.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectUnidadeSaudeBase = new ezSelect.EzSelect.EzSelect<unidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.unidadeDeSaudeService,
                this.getFiltroParaUnidadeSaudeBase,
                this.requestParamsUnidadeSaudeBase,
                this.$uibModal,
                this.unidadeSaudeBaseSelecionado,
                this.unidadeSaudeBaseDeselecionado);

            // Esse documento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse documento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectUnidadeSaudeBase.onEzGridCreated = () => {
                this.ezSelectUnidadeSaudeBase.ezGrid.serviceGetCallback = () => {
                    var idsUnidades = new Array();
                    if (this.entity.items != null)
                        this.entity.items.forEach(x => idsUnidades.push(x.unidadeDeSaude.id));
                    var getids = new unidadeSaudeBaseDtos.Dtos.EZLiv.Geral.GetUnidadeSaudeBaseInput();
                    getids.ids = idsUnidades;
                    return this.unidadeDeSaudeService.getPaginadoExceptForIds(getids);                    
                };
                this.ezSelectUnidadeSaudeBase.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemDeRedeCredenciada.UnidadeDeSaude'),
                    field: 'pessoaJuridica.nomeFantasia'
                });

                this.ezSelectUnidadeSaudeBase.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este documento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectUnidadeSaudeBase.on.beforeOpenModalDialog = () => {
                this.ezSelectUnidadeSaudeBase.ezGrid.getRegistros();
            };
        }

        /* Itens */
        private inicializarItemDeRedeCredenciadaCurrent() {
            this.itemCurrent = new itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput();
            this.itemCurrent.unidadeDeSaude = new unidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto();
            this.itemCurrent.isRedeDiferenciada = false;
            this.itemCurrent.isActive = true;
            if (this.entity)
                this.itemCurrent.redeCredenciadaId = this.entity.id;
        }

        private cancelarItem() {
            this.limparItemDeRedeCredenciadaCurrent();
            this.ezSelectUnidadeSaudeBase.setInputText("");
            this.editandoItem = false;
        }

        private getItemDeRedeCredenciadaByTempId(tempId: number): itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput {
            for (let i = 0; i < this.entity.items.length; i++) {
                if (this.entity.items[i].id === tempId) {
                    return this.entity.items[i];
                }
            }
        }

        private gravarItem(item: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput) {
            this.saving = true;
            this.itemCurrent.redeCredenciadaId = this.entity.id;
            var promise = this.itemDeRedeCredenciadaService.save(item);
            promise.then(result => {
                this.itemCurrent.id = result.id;
                if (!this.editandoItem) {
                    this.entity.items.push(this.itemCurrent);
                }
                this.gridDataEnumerable = this.entity.items;
                this.limparItemDeRedeCredenciadaCurrent();
                this.getItemGrid();
                this.editandoItem = false;
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }, function (reason) {
                abp.notify.error('Failed: ' + reason, "Erro");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarItem() {            
            if (!this.editandoItem) {
                this.gravarItem(this.itemCurrent);
            }
            else {
                var item = this.getItemDeRedeCredenciadaByTempId(this.itemCurrent.id);
                angular.merge(item, this.itemCurrent);
                this.gravarItem(item);
            }
        }

        private itemDeRedeCredenciadaGridConfig() {
            this.requestParamsItemDeRedeCredenciada = new requestParam.RequestParam.RequestParams();
            this.requestParamsItemDeRedeCredenciada.sorting = 'Id';

            this.itemDeRedeCredenciadaAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput>();
            this.itemDeRedeCredenciadaAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarItem, 'Edit', true));
            this.itemDeRedeCredenciadaAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirItem, 'Delete', true));

            this.itemDeRedeCredenciadaGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.itemDeRedeCredenciadaAppScopeProvider
            }

            var lista = this.entity ? this.entity.items : new Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput>();
            this.itemDeRedeCredenciadaGridOptions.data = lista;
            this.itemDeRedeCredenciadaGridOptions.totalItems = lista.length;

            this.itemDeRedeCredenciadaGridOptions.columnDefs.push({
                name: app.localize('ItemDeRedeCredenciada.UnidadeDeSaude'),
                field: 'unidadeDeSaude.pessoaJuridica.nomeFantasia',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            //this.itemDeRedeCredenciadaGridOptions.columnDefs.push({
            //    name: app.localize('UnidadeDeSaude.TipoDeUnidade'),
            //    cellTemplate: '\
            //    <div class="ui-grid-cell-contents">\
            //        {{grid.appScope.getEnumValue(row.entity, "tipoDeUnidadeEnum", "unidadeDeSaude.tipoDeUnidade")}}\
            //    </div>'
            //});

            this.itemDeRedeCredenciadaGridOptions.columnDefs.push({
                name: app.localize('ItemDeRedeCredenciada.RedeDiferenciada'),
                field: 'isRedeDiferenciada',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
            this.itemDeRedeCredenciadaGridOptions.columnDefs.push({
                name: app.localize('ItemDeRedeCredenciada.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
            

            ezFixes.uiGrid.uiTab.registerFix();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class RedeCredenciadaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/redeCredenciada/redeCredenciadaForm.cshtml';
            this.controller = RedeCredenciadaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}