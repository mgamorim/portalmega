﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as administradoraService from "../../../../services/domain/ezliv/administradora/administradoraService";
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.Administradora {

    class AdministradoraIndexController extends
        controllers.Controllers.ControllerIndexBase<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto,
        administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput> {

        static $inject = ['$state', 'uiGridConstants', 'administradoraService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public administradoraService: administradoraService.Services.AdministradoraService) {

            super(
                $state,
                uiGridConstants,
                administradoraService, {
                    rotaAlterarRegistro: 'tenant.ezliv.administradora.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.administradora.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'PessoaJuridica.NomeFantasia';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new administradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraInput();
                filtro.nomeFantasia = termoDigitadoPesquisa;
                filtro.razaoSocial = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.administradoraService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewAdministradora', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Administradora.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Administradora.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Administradora.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.RazaoSocial'),
                field: 'pessoaJuridica.razaoSocial',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.NomeFantasia'),
                field: 'pessoaJuridica.nomeFantasia'
            });

             this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Administradora.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
         
        }
    }

    export class AdministradoraIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/administradora/administradoraIndex.cshtml';
            this.controller = AdministradoraIndexController;
        }
    }
}