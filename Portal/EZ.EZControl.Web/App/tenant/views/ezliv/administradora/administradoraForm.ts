﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as ezFile from "../../../../common/ezFile/ezFile";

// Servicos
import * as administradoraService from "../../../../services/domain/ezliv/administradora/administradoraService";
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";
import * as parametroPagSeguroService from "../../../../services/domain/ezpag/parametroPagSeguro/parametroPagSeguroService";
import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";
import * as corretoraService from "../../../../services/domain/ezliv/corretora/corretoraService";

//DTOS
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as parametroPagSeguroDtos from "../../../../dtos/ezpag/parametroPagSeguro/parametroPagSeguroDtos";
import * as arquivoDtos from "../../../../dtos/core/arquivo/arquivoDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";

//Corretor
import * as corretorService from "../../../../services/domain/ezliv/corretor/corretorService";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
//GerenteAdministradora
import * as gerenteService from "../../../../services/domain/ezliv/gerenteAdministradora/gerenteAdministradoraService";
import * as gerenteDtos from "../../../../dtos/ezliv/subtipoPessoa/gerenteAdministradora/gerenteAdministradoraDtos";
//SupervisorAdministradora
import * as supervisorService from "../../../../services/domain/ezliv/supervisorAdministradora/supervisorAdministradoraService";
import * as supervisorDtos from "../../../../dtos/ezliv/subtipoPessoa/supervisorAdministradora/supervisorAdministradoraDtos";
//HomologadorAdministradora
import * as homologadorService from "../../../../services/domain/ezliv/homologadorAdministradora/homologadorAdministradoraService";
import * as homologadorDtos from "../../../../dtos/ezliv/subtipoPessoa/homologadorAdministradora/homologadorAdministradoraDtos";

export module Forms {
    export class AdministradoraFormController extends
        controllers.Controllers.ControllerFormComponentBase<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput,
        administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        public tipoPessoaEnum: any;
        public administradoraForm: ng.IFormController;
        public pagSeguroForm: ng.IFormController;

        // pessoa
        public pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        public ezSelectPessoaJuridica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaJuridica: (termoDigitadoPesquisa: string) => any;
        private pessoaJuridicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto) => string;
        private pessoaJuridicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaJuridica: (id: number) => void;

        //ParametroPagSeguro
        public parametroPagSeguroCurrent: parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput;

        public ezFile: ezFile.EZFile.EZFile;
        private imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;

        public upload: () => void;

        // corretora
        public corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        public ezSelectCorretora: ezSelect.EzSelect.EzSelect<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCorretora: (termoDigitadoPesquisa: string) => any;
        private corretoraSelecionado: (registro: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto) => string;
        private corretoraDeselecionado: () => void;
        private requestParamsCorretora: requestParam.RequestParam.RequestParams;
        private carregarCorretora: (id: number) => void;

        // Corretoras
        private requestParamsGridCorretora: requestParam.RequestParam.RequestParams;

        //Grid Corretoras
        private setDataGridInputCorretora: (result: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[]) => void;
        private gridApiInputCorretora: uiGrid.IGridApiOf<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        private requestParamsValoresInputCorretora: requestParam.RequestParam.RequestParams;
        private corretoraAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        public loadingCorretoraInput: boolean;
        public corretoraGridOptionsInput: uiGrid.IGridOptionsOf<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        private gridDataInputCorretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[];

        public addCorretora: () => void;
        public getCorretoraInput: () => void;
        public limparCorretora: () => void;
        public excluirCorretora: (corretra: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput) => void;

        //Corretor
        public pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        public corretor: corretorDtos.Dtos.EZLiv.Geral.CorretorInput;
        public ezSelectCorretor: ezSelect.EzSelect.EzSelect<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCorretor: (termoDigitadoPesquisa: string) => any;
        private corretorSelecionado: (registro: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto) => string;
        private corretorDeselecionado: () => void;
        private requestParamsCorretor: requestParam.RequestParam.RequestParams;
        private carregarCorretor: (id: number) => void;

        // EzSelec GerenteAdministradora
        public ezSelectGerente: ezSelect.EzSelect.EzSelect<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaGerenteAdministradora: (termoDigitadoPesquisa: string) => any;
        private gerenteAdministradoraSelecionado: (registro: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto) => string;
        private gerenteAdministradoraDeselecionado: () => void;
        private requestParamsGerenteAdministradora: requestParam.RequestParam.RequestParams;

        // EzSelec SupervisorAdministradora
        public ezSelectSupervisor: ezSelect.EzSelect.EzSelect<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaSupervisorAdministradora: (termoDigitadoPesquisa: string) => any;
        private supervisorAdministradoraSelecionado: (registro: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto) => string;
        private supervisorAdministradoraDeselecionado: () => void;
        private requestParamsSupervisorAdministradora: requestParam.RequestParam.RequestParams;

        // EzSelec HomologadorAdministradora
        public ezSelectHomologador: ezSelect.EzSelect.EzSelect<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaHomologadorAdministradora: (termoDigitadoPesquisa: string) => any;
        private homologadorAdministradoraSelecionado: (registro: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto) => string;
        private homologadorAdministradoraDeselecionado: () => void;
        private requestParamsHomologadorAdministradora: requestParam.RequestParam.RequestParams;

        // Grid Gerentes
        private gridDataGerentesEnumerable: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto[];
        private gerenteAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto>;
        public gerenteGridOptions: uiGrid.IGridOptionsOf<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput>;
        public editarGerente: (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto) => void;
        public excluirGerente: (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto) => void;
        public getGerenteGrid: () => void;

        private requestParamsGridGerente: requestParam.RequestParam.RequestParams;
        public gerenteCurrent: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput;
        public loadingGerenteInput: boolean;
        public editandoGerente: boolean;
        public limparGerente: () => void;
        public cancelEditGerente: () => void;
        private idsGerentes: number[];

        // Grid Supervisores
        private gridDataSupervisoresEnumerable: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto[];
        private supervisorAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto>;
        public supervisorGridOptions: uiGrid.IGridOptionsOf<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput>;
        public editarSupervisor: (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto) => void;
        public excluirSupervisor: (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto) => void;
        public getSupervisorGrid: () => void;

        private requestParamsGridSupervisor: requestParam.RequestParam.RequestParams;
        public supervisorCurrent: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput;
        public loadingSupervisorInput: boolean;
        public editandoSupervisor: boolean;
        public limparSupervisor: () => void;
        public cancelEditSupervisor: () => void;
        private idsSupervisores: number[];

        // Grid Homologadores
        private gridDataHomologadoresEnumerable: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto[];
        private homologadorAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto>;
        public homologadorGridOptions: uiGrid.IGridOptionsOf<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput>;
        public editarHomologador: (valor: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto) => void;
        public excluirHomologador: (valor: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto) => void;
        public getHomologadorGrid: () => void;

        private requestParamsGridHomologador: requestParam.RequestParam.RequestParams;
        public homologadorCurrent: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput;
        public loadingHomologadorInput: boolean;
        public editandoHomologador: boolean;
        public limparHomologador: () => void;
        public cancelEditHomologador: () => void;
        private idsHomologadores: number[];

        static $inject = ['$scope', '$rootScope', '$state', '$stateParams', 'uiGridConstants', 'administradoraService', 'pessoaJuridicaService', 'parametroPagSeguroService', 'arquivoGlobalService', 'parametroGlobalService', '$uibModal', 'corretoraService', 'gerenteAdministradoraService', 'supervisorAdministradoraService', 'homologadorAdministradoraService', 'corretorService'];

        constructor(
            public $scope: ng.IScope,
            public $rootScope: any,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public administradoraService: administradoraService.Services.AdministradoraService,
            public pessoaJuridicaService: pessoaJuridicaService.Services.PessoaJuridicaService,
            public parametroPagSeguroService: parametroPagSeguroService.Services.ParametroPagSeguroService,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public corretoraService: corretoraService.Services.CorretoraService,
            public gerenteService: gerenteService.Services.GerenteAdministradoraService,
            public supervisorService: supervisorService.Services.SupervisorAdministradoraService,
            public homologadorService: homologadorService.Services.HomologadorAdministradoraService,
            public corretorService: corretorService.Services.CorretorService
            ) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                administradoraService,
                'tenant.ezliv.administradora');

            this.editandoGerente = false;
            this.editandoSupervisor = false;
            this.editandoHomologador = false;

            this.addCorretora = () => {
                this.corretora.isActive = true;
                this.entity.corretoras.push(this.corretora);
                this.getCorretoraInput();
                this.limparCorretora();
            }

            this.limparCorretora = () => {
                this.corretora = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput();
                this.ezSelectCorretora.gridSelectClear();
            }

            this.getCorretoraInput = () => {
                
                if (this.entity) {
                    this.setDataGridInputCorretora(this.entity.corretoras);
                }

                
            }

            this.excluirCorretora = (corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput) => {
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.corretoras.length; i++) {
                                if (this.entity.corretoras[i].id === corretora.id) {
                                    this.entity.corretoras.splice(i, 1);
                                    this.getCorretoraInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInputCorretora = (result: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[]) => {
                this.gridDataInputCorretora.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputCorretora.push(result[i]);
                }

                this.corretoraGridOptionsInput.totalItems = result.length;
            };

            this.carregarPessoaJuridica = (id: number) => {
                this.ezSelectPessoaJuridica.loading = true;
                var promise = this.pessoaJuridicaService.getById(id);
                promise.then(result => {
                    this.pessoaJuridica = result;
                    this.entity.pessoaJuridicaId = this.pessoaJuridica.id;
                    this.ezSelectPessoaJuridica.setInputText(result.nomeFantasia);
                }).finally(() => {
                    this.ezSelectPessoaJuridica.loading = false;
                });
            }

            //Gerente
            this.getGerenteGrid = () => {
                if (this.gerenteGridOptions) {
                    this.gerenteGridOptions.data = this.entity ? this.entity.gerentes : null;
                    this.gerenteGridOptions.totalItems = this.entity && this.entity.gerentes ? this.entity.gerentes.length : 0;
                }
            }

            this.cancelEditGerente = () => {
                this.gerenteCurrent = new gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput();
                this.ezSelectGerente.gridSelectClear();
                this.editandoGerente = false;
            }

            this.editarGerente = (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput) => {
                this.editandoGerente = true;
                this.gerenteCurrent = valor;
                this.ezSelectGerente.setInputText(valor.pessoa.nomePessoa);
                this.loadingGerenteInput = false;
            }

            this.excluirGerente = (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto) => {
                this.editandoGerente = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            
                            var promise = this.gerenteService.delete(valor.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarGerenteCurrent();
                            }).finally(() => {
                                
                            });

                            var index = this.entity.gerentes.indexOf(valor);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.gerentes.splice(index, 1);
                            }
                            this.editandoGerente = false;
                        }
                    }
                );
            }

            //Supervisor
            this.getSupervisorGrid = () => {
                if (this.supervisorGridOptions) {
                    this.supervisorGridOptions.data = this.entity ? this.entity.supervisores : null;
                    this.supervisorGridOptions.totalItems = this.entity && this.entity.supervisores ? this.entity.supervisores.length : 0;
                }
            }

            this.cancelEditSupervisor = () => {
                this.supervisorCurrent = new supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput();
                this.ezSelectSupervisor.gridSelectClear();
                this.editandoSupervisor = false;
            }

            this.editarSupervisor = (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput) => {
                this.editandoSupervisor = true;
                this.supervisorCurrent = valor;
                this.ezSelectSupervisor.setInputText(valor.pessoa.nomePessoa);
                this.loadingSupervisorInput = false;
            }

            this.excluirSupervisor = (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto) => {
                this.editandoSupervisor = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            
                            var promise = this.supervisorService.delete(valor.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarSupervisorCurrent();
                            }).finally(() => {
                                
                            });

                            var index = this.entity.supervisores.indexOf(valor);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.supervisores.splice(index, 1);
                            }
                            this.editandoSupervisor = false;
                        }
                    }
                );
            }

            //Homologador
            this.getHomologadorGrid = () => {
                if (this.homologadorGridOptions) {
                    this.homologadorGridOptions.data = this.entity ? this.entity.homologadores : null;
                    this.homologadorGridOptions.totalItems = this.entity && this.entity.homologadores ? this.entity.homologadores.length : 0;
                }
            }

            this.cancelEditHomologador = () => {
                this.homologadorCurrent = new homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput();
                this.ezSelectHomologador.gridSelectClear();
                this.editandoHomologador = false;
            }

            this.editarHomologador = (valor: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput) => {
                this.editandoHomologador = true;
                this.homologadorCurrent = valor;
                this.ezSelectHomologador.setInputText(valor.pessoa.nomePessoa);
                this.loadingHomologadorInput = false;
            }

            this.excluirHomologador = (valor: homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto) => {
                this.editandoHomologador = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            
                            var promise = this.homologadorService.delete(valor.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarHomologadorCurrent();
                            }).finally(() => {
                                
                            });

                            var index = this.entity.homologadores.indexOf(valor);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.homologadores.splice(index, 1);
                            }
                            this.editandoHomologador = false;
                        }
                    }
                );
            }
        }

        $onInit() {
            super.init();

            this.ezFile = new ezFile.EZFile.EZFile();

            this.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            this.imagem.id = 0;
            this.imagem.isActive = true;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput;
                instance.id = 0;
                instance.isActive = true;
                instance.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
                instance.pessoaJuridica = TipoPessoaEnum.Juridica;
                instance.imagemId = 0;
                instance.corretoras = new Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>();
                instance.gerentes = new Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput>();
                instance.supervisores = new Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput>();
                instance.homologadores = new Array<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput>();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaJuridica) {
                    this.carregarPessoaJuridica(entity.pessoaJuridicaId);
                }

                if (entity.parametroPagSeguro != null) {
                    this.parametroPagSeguroCurrent = new parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput();
                    this.parametroPagSeguroCurrent.email = entity.parametroPagSeguro.email;
                    this.parametroPagSeguroCurrent.token = entity.parametroPagSeguro.token;
                    this.parametroPagSeguroCurrent.appId = entity.parametroPagSeguro.appId;
                    this.parametroPagSeguroCurrent.appKey = entity.parametroPagSeguro.appKey;
                    this.parametroPagSeguroCurrent.pessoaJuridicaId = entity.parametroPagSeguro.pessoaJuridicaId;
                }

                this.ezFile.controller.arquivoId = entity.imagemId;
                this.ezFile.initialize();

                this.getCorretoraInput();

                this.getGerenteGrid();
                this.getSupervisorGrid();
                this.getHomologadorGrid();
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
                this.entity.parametroPagSeguro = this.parametroPagSeguroCurrent;
            };

            this.events.onBeforeSaveEntity = () => {
                if (this.parametroPagSeguroCurrent != null)
                {
                    this.parametroPagSeguroCurrent.pessoaJuridicaId = this.entity.pessoaJuridicaId;
                    this.entity.parametroPagSeguro = this.parametroPagSeguroCurrent;
                }
            };

            this.ezSelectPessoaConfig();
            this.ezSelectCorretoraConfig();
            this.ezSelectGerenteConfig();
            this.ezSelectSupervisorConfig();
            this.ezSelectHomologadorConfig();

            this.gerenteGridConfig();
            this.supervisorGridConfig();
            this.homologadorGridConfig();

            this.corretoraGridConfigInput();
            this.getCorretoraInput();
            this.ezFileConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private gerenteGridConfig() {
            this.requestParamsGridGerente = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridGerente.sorting = 'Id';

            this.gerenteAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto>();
            this.gerenteAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarGerente, 'Edit', true));
            this.gerenteAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirGerente, 'Delete', true));

            this.gerenteGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.gerenteAppScopeProvider
            }

            var lista = this.entity ? this.entity.gerentes : new Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto>();
            this.gerenteGridOptions.data = lista;
            this.gerenteGridOptions.totalItems = lista.length;

            this.gerenteGridOptions.columnDefs.push({
                name: app.localize('Gerente.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();

            this.getGerenteGrid();
        }

        private supervisorGridConfig() {
            this.requestParamsGridSupervisor = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridSupervisor.sorting = 'Id';

            this.supervisorAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto>();
            this.supervisorAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarSupervisor, 'Edit', true));
            this.supervisorAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirSupervisor, 'Delete', true));

            this.supervisorGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.supervisorAppScopeProvider
            }

            var lista = this.entity ? this.entity.supervisores : new Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto>();
            this.supervisorGridOptions.data = lista;
            this.supervisorGridOptions.totalItems = lista.length;

            this.supervisorGridOptions.columnDefs.push({
                name: app.localize('Supervisor.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();

            this.getSupervisorGrid();
        }

        private homologadorGridConfig() {
            this.requestParamsGridHomologador = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridHomologador.sorting = 'Id';

            this.homologadorAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto>();
            this.homologadorAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarHomologador, 'Edit', true));
            this.homologadorAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirHomologador, 'Delete', true));

            this.homologadorGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.homologadorAppScopeProvider
            }

            var lista = this.entity ? this.entity.homologadores : new Array<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto>();
            this.homologadorGridOptions.data = lista;
            this.homologadorGridOptions.totalItems = lista.length;

            this.homologadorGridOptions.columnDefs.push({
                name: app.localize('Homologador.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();

            this.getHomologadorGrid();
        }

        private inicializarGerenteCurrent() {
            this.gerenteCurrent = new gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput();
            this.gerenteCurrent.isActive = true;
            if (this.entity)
                this.gerenteCurrent.administradoraId = this.entity.id;
        }

        private gravarGerente(item: any) {
            this.saving = true;
            this.gerenteCurrent.administradoraId = this.entity.id;
            var promise = this.gerenteService.save(item);
            promise.then((result) => {
                this.gerenteCurrent.id = result.id;
                if (!this.editandoGerente) {
                    if (!this.entity.gerentes)
                        this.entity.gerentes = new Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto>();

                    this.entity.gerentes.push(this.gerenteCurrent as gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto);
                }
                this.editandoGerente = false;
                this.cancelEditGerente();
                this.inicializarGerenteCurrent();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarGerente() {
            if (!this.editandoGerente) {
                this.gravarGerente(this.gerenteCurrent);
            }
            else {
                var item = this.getGerenteByTempId(this.gerenteCurrent.id);
                angular.merge(item, this.gerenteCurrent);
                this.gravarGerente(item);
            }
        }

        private getGerenteByTempId(tempId: number): gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto {
            for (let i = 0; i < this.entity.gerentes.length; i++) {
                if (this.entity.gerentes[i].id === tempId) {
                    return this.entity.gerentes[i] as gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto;
                }
            }
        }

        private inicializarSupervisorCurrent() {
            this.supervisorCurrent = new supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput();
            this.supervisorCurrent.isActive = true;
            if (this.entity)
                this.supervisorCurrent.administradoraId = this.entity.id;
        }

        private gravarSupervisor(item: any) {
            this.saving = true;
            this.supervisorCurrent.administradoraId = this.entity.id;
            var promise = this.supervisorService.save(item);
            promise.then((result) => {
                this.supervisorCurrent.id = result.id;
                if (!this.editandoSupervisor) {

                    if (!this.entity.supervisores)
                        this.entity.supervisores = new Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto>();

                    this.entity.supervisores.push(this.supervisorCurrent as supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto);
                }
                this.editandoSupervisor = false;
                this.getSupervisorGrid();
                this.cancelEditSupervisor();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarSupervisor() {
            if (!this.editandoSupervisor) {
                this.gravarSupervisor(this.supervisorCurrent);
            }
            else {
                var item = this.getSupervisorByTempId(this.supervisorCurrent.id);
                angular.merge(item, this.supervisorCurrent);
                this.gravarSupervisor(item);
            }
        }

        private getSupervisorByTempId(tempId: number): supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto {
            for (let i = 0; i < this.entity.supervisores.length; i++) {
                if (this.entity.supervisores[i].id === tempId) {
                    return this.entity.supervisores[i] as supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto;
                }
            }
        }

        private inicializarHomologadorCurrent() {
            this.homologadorCurrent = new homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput();
            this.homologadorCurrent.isActive = true;
            if (this.entity)
                this.homologadorCurrent.administradoraId = this.entity.id;
        }

        private gravarHomologador(item: any) {
            this.saving = true;
            this.homologadorCurrent.administradoraId = this.entity.id;
            var promise = this.homologadorService.save(item);
            promise.then((result) => {
                this.homologadorCurrent.id = result.id;
                if (!this.editandoHomologador) {

                    if (!this.entity.homologadores)
                        this.entity.homologadores = new Array<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto>();

                    this.entity.homologadores.push(this.homologadorCurrent as homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto);
                }
                this.editandoHomologador = false;
                this.getHomologadorGrid();
                this.cancelEditHomologador();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarHomologador() {
            if (!this.editandoHomologador) {
                this.gravarHomologador(this.homologadorCurrent);
            }
            else {
                var item = this.getHomologadorByTempId(this.homologadorCurrent.id);
                angular.merge(item, this.homologadorCurrent);
                this.gravarHomologador(item);
            }
        }

        private getHomologadorByTempId(tempId: number): homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto {
            for (let i = 0; i < this.entity.homologadores.length; i++) {
                if (this.entity.homologadores[i].id === tempId) {
                    return this.entity.homologadores[i] as homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto;
                }
            }
        }

        setAdministradoraForm(form: ng.IFormController) {
            this.administradoraForm = form;
        }

        setPagSeguroForm(form: ng.IFormController) {
            this.pagSeguroForm = form;
        }

        private inicializarPagSeguroItemCurrent() {
            this.parametroPagSeguroCurrent = new parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput();
            this.parametroPagSeguroCurrent.email = "";
            this.parametroPagSeguroCurrent.token = "";
            this.parametroPagSeguroCurrent.appId = "";
            this.parametroPagSeguroCurrent.appKey = "";
        }

        private corretoraGridConfigInput() {
            this.requestParamsValoresInputCorretora = new requestParam.RequestParam.RequestParams();

            this.corretoraAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>();

            this.corretoraAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirCorretora, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridDataInputCorretora = new Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>();

            this.corretoraGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.corretoraAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputCorretora = api;
                    this.gridApiInputCorretora.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputCorretora.sorting = null;
                        } else {
                            this.requestParamsValoresInputCorretora.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getCorretoraInput();
                    });
                    this.gridApiInputCorretora.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputCorretora.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputCorretora.maxResultCount = pageSize;

                        this.getCorretoraInput();
                    });
                }
            }

            this.corretoraGridOptionsInput.data = this.gridDataInputCorretora;

            this.corretoraGridOptionsInput.columnDefs.push({
                name: app.localize('Corretora.Nome'),
                field: 'nome',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });
            this.getCorretoraInput();
        }

        private ezSelectCorretoraConfig() {
            this.requestParamsCorretora = new requestParam.RequestParam.RequestParams();

            this.corretoraSelecionado = (registro) => {
                this.corretora = registro as corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
                return registro.nome;
            }

            this.corretoraDeselecionado = () => {
                this.corretora = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCorretora = (termoDigitadoPesquisa) => {
                var filtro = new corretoraDtos.Dtos.EZLiv.Geral.GetCorretoraInput();
                filtro.nome = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectCorretora = new ezSelect.EzSelect.EzSelect<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretoraService,
                this.getFiltroParaPaginacaoCorretora,
                this.requestParamsCorretora,
                this.$uibModal,
                this.corretoraSelecionado,
                this.corretoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCorretora.onEzGridCreated = () => {

                this.ezSelectCorretora.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretoraService.getPaged(filtro, requestParams);
                };

                this.ezSelectCorretora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretora.Nome'),
                    field: 'nome'
                });

                this.ezSelectCorretora.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCorretora.on.beforeOpenModalDialog = () => {
                this.ezSelectCorretora.ezGrid.getRegistros();
            };
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.pessoaJuridicaSelecionado = (registro) => {
                this.pessoaJuridica = registro;
                this.entity.pessoaJuridicaId = registro.id;
                this.entity.pessoaJuridica = registro;
                return registro.nomePessoa;
            }

            this.pessoaJuridicaDeselecionado = () => {
                this.pessoaJuridica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaJuridica = (termoDigitadoPesquisa) => {
                var filtro = new administradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForAdministradora();
                filtro.nomePessoa = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaJuridica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaJuridicaService,
                this.getFiltroParaPaginacaoPessoaJuridica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaJuridicaSelecionado,
                this.pessoaJuridicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaJuridica.onEzGridCreated = () => {

                this.ezSelectPessoaJuridica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.administradoraService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.RazaoSocial'),
                    field: 'razaoSocial'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaJuridica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaJuridica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaJuridica.ezGrid.getRegistros();
            };
        }

        private ezFileConfig() {
            this.ezFile.loadImage = () => {
                var promise = this.arquivoService.getById(this.entity.imagemId);
                promise.then((result) => {
                    this.ezFile.setImageInfo(result.path, result.tipoDeArquivoFixo);
                });
            }
            this.ezFile.getParametros = () => {
                var promise = this.parametroGlobalService.get();
                promise.then((result) => {
                    this.ezFile.setParametos(result.extensoesImagem);
                });
            }
            this.ezFile.onUploadComplete = (fileName: string) => {
                var input = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
                input.nome = fileName;
                var promise = this.arquivoService.getIdByName(input);
                promise.then((result) => {
                    this.entity.imagemId = result.id;
                });
            }
            this.ezFile.initialize = () => {
                this.ezFile.inicializado = true;
                this.ezFile.controller.sistema = "Estoque";
                this.ezFile.controller.autoUpload = true;
                if (this.ezFile.controller.arquivoId > 0) {
                    if (this.ezFile.loadImage) {
                        this.ezFile.loadImage();
                    }
                }
                if (this.ezFile.onUploadComplete) {
                    this.ezFile.controller.onUploadComplete = this.ezFile.onUploadComplete;
                }
            }
        }

        private ezSelectGerenteConfig() {
            this.requestParamsGerenteAdministradora = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.gerenteAdministradoraSelecionado = (registro) => {
                this.gerenteCurrent = registro as gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput;
                return registro.pessoa.nomePessoa;
            }

            this.gerenteAdministradoraDeselecionado = () => {
                this.gerenteCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaGerenteAdministradora = (termoDigitadoPesquisa) => {
                var filtro = new gerenteDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteAdministradora();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGerente = new ezSelect.EzSelect.EzSelect<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.gerenteService,
                this.getFiltroParaGerenteAdministradora,
                this.requestParamsGerenteAdministradora,
                this.$uibModal,
                this.gerenteAdministradoraSelecionado,
                this.gerenteAdministradoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGerente.onEzGridCreated = () => {

                this.ezSelectGerente.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPaged(filtro, requestParams);
                };

                this.ezSelectGerente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectGerente.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGerente.on.beforeOpenModalDialog = () => {
                this.ezSelectGerente.ezGrid.getRegistros();
            };
        }

        private ezSelectSupervisorConfig() {
            this.requestParamsSupervisorAdministradora = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.supervisorAdministradoraSelecionado = (registro) => {
                this.supervisorCurrent = registro as supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput;
                return registro.pessoa.nomePessoa;
            }

            this.supervisorAdministradoraDeselecionado = () => {
                this.supervisorCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaSupervisorAdministradora = (termoDigitadoPesquisa) => {
                var filtro = new supervisorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorAdministradora();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSupervisor = new ezSelect.EzSelect.EzSelect<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.supervisorService,
                this.getFiltroParaSupervisorAdministradora,
                this.requestParamsSupervisorAdministradora,
                this.$uibModal,
                this.supervisorAdministradoraSelecionado,
                this.supervisorAdministradoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSupervisor.onEzGridCreated = () => {

                this.ezSelectSupervisor.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPaged(filtro, requestParams);
                };

                this.ezSelectSupervisor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectSupervisor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSupervisor.on.beforeOpenModalDialog = () => {
                this.ezSelectSupervisor.ezGrid.getRegistros();
            };
        }

        private ezSelectHomologadorConfig() {
            this.requestParamsHomologadorAdministradora = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.homologadorAdministradoraSelecionado = (registro) => {
                this.homologadorCurrent = registro as homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput;
                return registro.pessoa.nomePessoa;
            }

            this.homologadorAdministradoraDeselecionado = () => {
                this.homologadorCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaHomologadorAdministradora = (termoDigitadoPesquisa) => {
                var filtro = new homologadorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForHomologadorAdministradora();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectHomologador = new ezSelect.EzSelect.EzSelect<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.homologadorService,
                this.getFiltroParaHomologadorAdministradora,
                this.requestParamsHomologadorAdministradora,
                this.$uibModal,
                this.homologadorAdministradoraSelecionado,
                this.homologadorAdministradoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectHomologador.onEzGridCreated = () => {

                this.ezSelectHomologador.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPaged(filtro, requestParams);
                };

                this.ezSelectHomologador.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectHomologador.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectHomologador.on.beforeOpenModalDialog = () => {
                this.ezSelectHomologador.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class AdministradoraFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/administradora/administradoraForm.cshtml';
            this.controller = AdministradoraFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}