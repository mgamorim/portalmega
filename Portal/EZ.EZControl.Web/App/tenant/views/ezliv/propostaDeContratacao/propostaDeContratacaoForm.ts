﻿import * as propostaDeContratacaoService from "../../../../services/domain/ezliv/propostaDeContratacao/propostaDeContratacaoService";
import * as profissaoService from "../../../../services/domain/global/profissao/profissaoService";
import * as propostaDeContratacaoDtos from "../../../../dtos/ezliv/geral/propostaDeContratacao/propostaDeContratacaoDtos";
import * as proponenteTitularDtos from "../../../../dtos/ezliv/subtipoPessoa/proponenteTitular/proponenteTitularDtos";
import * as profissaoDtos from "../../../../dtos/global/profissao/profissaoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as tipoDeDocumentoDtos from "../../../../dtos/global/tipoDeDocumento/tipoDeDocumentoDtos";
import * as documentoDtos from "../../../../dtos/global/documento/documentoDtos";
import * as tipoDeDocumentoService from "../../../../services/domain/global/tipoDeDocumento/tipoDeDocumentoService";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as ezAutoComplete from "../../../../common/ezAutoComplete/ezAutoComplete";
import * as transacaoPagSeguroDtos from "../../../../dtos/ezpag/transacaoPagSeguro/transacaoPagSeguroDtos"
import * as transacaoPagSeguroService from "../../../../services/domain/ezpag/transacaoPagSeguro/transacaoPagSeguroService"
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as clienteService from "../../../../services/domain/global/cliente/clienteService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
import * as pedidoDtos from "../../../../dtos/vendas/pedido/pedidoDtos";
import * as pedidoService from "../../../../services/domain/vendas/pedido/pedidoService";
import * as ezpagDtos from "../../../../dtos/ezpag/geral/ezpagDtos";
import * as itemDePedidoDtos from "../../../../dtos/vendas/itemDePedido/itemDePedidoDtos";
import * as produtoService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as enderecoService from "../../../../services/domain/global/endereco/enderecoService";
import * as produtoDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as ezFile from "../../../../common/ezFile/ezFile";
import * as itemDeDeclaracaoDeSaudeService from "../../../../services/domain/ezliv/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeService";
import * as questionarioDeDeclaracaoDeSaudeService from "../../../../services/domain/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeService";
import * as itemDeDeclaracaoDeSaudeDtos from "../../../../dtos/ezliv/geral/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeDtos";
import * as contratoDtos from "../../../../dtos/ezliv/geral/contrato/contratoDtos"
import * as arquivoDocumentoService from "../../../../services/domain/ezliv/arquivoDocumento/arquivoDocumentoService";
import * as arquivoDocumentoDto from "../../../../dtos/ezliv/geral/arquivoDocumento/arquivoDocumentoDtos";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";
import * as dependenteDto from "../../../../dtos/ezliv/geral/dependente/dependenteDtos";
import * as respostasDto from "../../../../dtos/ezliv/geral/respostaDeDeclaracaoDeSaude/respostaDeDeclaracaoDeSaudeDtos";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as grupoPessoaService from "../../../../services/domain/global/grupoPessoa/grupoPessoaService";
import * as enderecoEletronicoService from "../../../../services/domain/global/enderecoEletronico/enderecoEletronicoService";
import * as enderecoEletronicoDtos from "../../../../dtos/global/enderecoEletronico/enderecoEletronicoDtos";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as tipoDeLogradouroService from "../../../../services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";
import * as telefoneService from "../../../../services/domain/global/telefone/telefoneService";
import * as telefoneDtos from "../../../../dtos/global/telefone/telefoneDtos";
import * as userDtos from "../../../../dtos/core/user/userDtos";
import * as userService from "../../../../services/domain/core/user/userService";
import * as vendaService from "../../../../services/domain/vendas/venda/vendaService";
import * as enderecoDtos from "App/dtos/global/endereco/enderecoDtos";
import * as paisDtos from "App/dtos/global/pais/paisDtos";
import * as cidadeDtos from "App/dtos/global/cidade/cidadeDtos";
import * as vigenciaDtos from "../../../../dtos/ezliv/geral/vigencia/vigenciaDtos";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as vigenciaService from "../../../../services/domain/ezliv/vigencia/vigenciaService";
import * as redeCredenciadaDtos from "../../../../dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as redeCredenciadaService from "../../../../services/domain/ezliv/redeCredenciada/redeCredenciadaService";
import * as transacaoDtos from "../../../../dtos/ezpag/transacao/transacaoDtos"
import * as transacaoService from "../../../../services/domain/ezpag/transacao/transacaoService"
import * as chancelaDtos from "../../../../dtos/ezliv/geral/chancela/chancelaDtos";
import * as chancelaService from "../../../../services/domain/ezliv/chancela/chancelaService";
import * as operadoraService from "../../../../services/domain/ezliv/operadora/operadoraService";
import * as corretoraService from "../../../../services/domain/ezliv/corretora/corretoraService";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipopessoa/operadora/operadoraDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipopessoa/corretora/corretoraDtos";
import * as gerenteDtos from "../../../../dtos/ezliv/subtipopessoa/gerenteCorretora/gerenteCorretoraDtos";
import * as supervisorDtos from "../../../../dtos/ezliv/subtipopessoa/supervisorCorretora/supervisorCorretoraDtos";
import * as gerenteService from "../../../../services/domain/ezliv/gerenteCorretora/gerenteCorretoraService";
import * as supervisorService from "../../../../services/domain/ezliv/supervisorCorretora/supervisorCorretoraService";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraService from "../../../../services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraService";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos";
import * as formaDePagamentoService from "../../../../services/domain/ezpag/formaDePagamento/formaDePagamentoService";
import * as formaDePagamentoDtos from "../../../../dtos/ezpag/formaDePagamento/formaDePagamentoDtos";
import * as depositoTransferenciaBancariaDtos from "../../../../dtos/ezpag/depositoTransferenciaBancaria/depositoTransferenciaBancariaDtos";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";

export module Forms {
    export class PropostaDeContratacaoFormController extends
        controllers.Controllers
            .ControllerFormComponentBase<propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput,
        propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput,
        applicationServiceDtos.Dtos.ApplicationService.IdInput> {

        private estadosCivis: any;
        private sexos: any;
        private grausDeParentesco: any;
        private tiposDeResponsavel: any;
        private idadeDoTitular: number;

        public loading = false;
        public disableTabSelecionarPlano = true;
        public disableTabProposta = true;
        public disableTabAceite = true;
        public disableTabHomologacao = true;
        public disableTabPagamento = true;

        public setStepByPassoDaProposta: (numPassoProposta: number) => void;
        public passoAtual = PassoDaPropostaEnum.PreCadastro;
        public selectTab: (numPassoProposta: number, uiClick: boolean) => void;
        public passoDaPropostaForTabsEnum: any;
        public setDadosPagamento: () => void;
        public saveGeral: () => void;
        public savePassoPreCadastro: () => void;
        //public scrollFixed: () => void;
        //public scrollFixedFn: () => void;

        //Produto
        public ezGridProduto: ezGrid.EzGrid.EzGrid<produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto,
        requestParam.RequestParam.RequestParams>;
        public requestParamsProduto: requestParam.RequestParam.RequestParams;
        public getFiltroParaPaginacaoProduto: (termoDigitadoPesquisa: string) => any;

        //Corretora
        public ezGridCorretora: ezGrid.EzGrid.EzGrid<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto,
        requestParam.RequestParam.RequestParams>;
        public requestParamsCorretora: requestParam.RequestParam.RequestParams;
        public getFiltroParaPaginacaoCorretora: (termoDigitadoPesquisa: string) => any;

        //FormasDePagamentos
        private filtroFormaDePagamento = new formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.GetFormaDePagamentoInput;
        private requestParamsFormaDePagamento = new requestParam.RequestParam.RequestParams;

        //Vigência
        public ezGridVigencia: ezGrid.EzGrid.EzGrid<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto,
        requestParam.RequestParam.RequestParams>;
        public requestParamsVigencia: requestParam.RequestParam.RequestParams;
        public getFiltroParaPaginacaoVigencia: (termoDigitadoPesquisa: string) => any;

        //Transfer
        public documento: any;
        public estadoId: number;
        public profissaoId: number;
        public chancelaId: number;

        // CPF 
        public validaNumeroDocumento: () => any;
        public mascaraDocumento: string;
        public numeroCpf: string;
        public disabledCPFAndEmailPreCadastro: boolean;
        public disabledCPFAndEmailResponsavel: boolean;

        // Estado
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private estadoDeselecionado: () => void;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;
        private carregarEstado: (id: number, text: string) => void;

        // Profissão
        public ezSelectProfissao: ezSelect.EzSelect.
        EzSelect<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoProfissao: (termoDigitadoPesquisa: string) => any;
        private profissaoSelecionado: (registro: profissaoDtos.Dtos.Profissao.ProfissaoListDto) => string;
        private profissaoDeselecionado: () => void;
        private requestParamsProfissao: requestParam.RequestParam.RequestParams;
        private carregarProfissao: (id: number, text: string) => void;

        // grupo pessoa
        public ezSelectGrupoPessoa: ezSelect.EzSelect.
        EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoGrupoPessoa: (termoDigitadoPesquisa: string) => any;
        private grupoPessoaSelecionado: (registro: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto) => string;
        private grupoPessoaDeselecionado: () => void;
        private requestParamsGrupoPessoa: requestParam.RequestParam.RequestParams;
        private carregarGrupoPessoa: (id: number, text: string) => void;

        // tipo de logradouro
        public ezSelectTipoDeLogradouro: ezSelect.EzSelect.
        EzSelect<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDeLogradouro: (termoDigitadoPesquisa: string) => any;
        private tipoDeLogradouroSelecionado: (registro: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.
            TipoDeLogradouroListDto) => string;
        private tipoDeLogradouroDeselecionado: () => void;
        private requestParamsTipoDeLogradouro: requestParam.RequestParam.RequestParams;
        private carregarTipoDeLogradouro: (id: number, text: string) => void;

        // pais
        public ezSelectPais: ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private paisDeselecionado: () => void;
        private requestParamsPais: requestParam.RequestParam.RequestParams;
        private carregarPais: (id: number, text: string) => void;

        // estado
        public ezSelectEstadoEndereco: ezSelect.EzSelect.
        EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEstadoEndereco: (termoDigitadoPesquisa: string) => any;
        private estadoEnderecoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private estadoEnderecoDeselecionado: () => void;
        private requestParamsEstadoEndereco: requestParam.RequestParam.RequestParams;
        private carregarEstadoEndereco: (id: number, text: string) => void;

        // cidade
        public ezSelectCidade: ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCidade: (termoDigitadoPesquisa: string) => any;
        private cidadeSelecionado: (registro: cidadeDtos.Dtos.Cidade.CidadeListDto) => string;
        private cidadeDeselecionado: () => void;
        private requestParamsCidade: requestParam.RequestParam.RequestParams;
        private carregarCidade: (id: number, text: string) => void;

        //Pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaByCPFListDto,
        requestParam.RequestParam.RequestParams>;
        //public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaFisicaByCPFListDto) => string;
        //private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        //Pagamento
        private dadosPagamento: transacaoPagSeguroDtos.Dtos.EZPag.TransacaoPagSeguro.CheckoutInput;
        private nomesDeBancoEnum = ez.domain.enum.nomeDeBancoEnum.valores;
        private brandImagePattern =
        "https://stc.pagseguro.uol.com.br/public/img/payment-methods-flags/42x20/[brandName].png";
        private brandImage = "";
        private brandName: string;
        private formasDePagamentoEnum = ez.domain.enum.tipoCheckoutPagSeguroEnum.valores;
        private createCardToken: () => void;
        private getBrand: () => void;
        private getInstallments: () => void;
        private whenReady: (obj: any, callback: () => void) => void;
        private setSessionId: () => void;
        private isValidForCardToken: () => void;
        private realizarCheckout: (isAdm: boolean) => void;
        public cardToken: string;
        public cardBin: number;
        public numeroCartao: string;
        public cvv: number;
        public cvvSize: number;
        public expirable: boolean;
        public mesVencimento: number;
        public anoVencimento: number;
        public validationAlgorithm: string;
        public opcoesDeParcelamento: any;
        public valorPedido: number;
        public opcaoDeParcelamento: string;
        public linkParaPagamento: string;
        public linkParaPagamentoCartao: string;
        public protocolNumber: string;

        //Dados do Proponente Titular
        public cpfTitular: string;
        public celularTitular: string;
        public emailTitular: string;
        public matricula: string;

        //Dados do Responsável
        public cpfResponsavel: string;
        public celularResponsavel: string;
        public emailResponsavel: string;

        //Pré-Cadastro
        public preCadastroInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral
            .PropostaDeContratacaoPassoPreCadastroInput();
        public preencherPreCadastro: (entity: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput) =>
            void;

        //Seleção de Produto
        public selecaoDeProdutoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral
            .PropostaDeContratacaoPassoSelecionarPlanoInput();

        //Dados Gerais
        public dadosGeraisInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral
            .PropostaDeContratacaoPassoDadosGeraisInput();
        public preencherDadosGerais: (entity: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput) =>
            void;
        private onChangeAditivo: (index: number, aditivo: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => void;
        private aditivosCheckbox: boolean[];

        //Contrato
        public contratoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral
            .PropostaDeContratacaoPassoAceiteDoContratoInput();
        public conteudoDoContrato: string = "";

        //Envio de Documento
        public envioDeDocumentoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral
            .PropostaDeContratacaoPassoEnvioDeDocumentosInput();
        public tiposDeDocumentoEnum = TipoDeDocumentoEnum.valores();
        public tipoDeDocumentoEscaneadoEzLivEnum = TipoDeDocumentoEscaneadoEzLivEnum.getEnumValues([
            TipoDeDocumentoEscaneadoEzLivEnum.Cpf,
            TipoDeDocumentoEscaneadoEzLivEnum.Cnpj,
            TipoDeDocumentoEscaneadoEzLivEnum.Rg,
            TipoDeDocumentoEscaneadoEzLivEnum.Passaporte,
            TipoDeDocumentoEscaneadoEzLivEnum.CarteiraDeTrabalho,
            TipoDeDocumentoEscaneadoEzLivEnum.Pis,
            TipoDeDocumentoEscaneadoEzLivEnum.ComprovanteDeResidencia,
            TipoDeDocumentoEscaneadoEzLivEnum.ComprovanteDeProfissao,
            TipoDeDocumentoEscaneadoEzLivEnum.Outros]);
        public getTiposDeFormulariosEnum = TipoDeDocumentoEscaneadoEzLivEnum.getEnumValues([TipoDeDocumentoEscaneadoEzLivEnum.Proposta, TipoDeDocumentoEscaneadoEzLivEnum.Aditivo, TipoDeDocumentoEscaneadoEzLivEnum.Chancela]);

        //Homologação
        public homologacaoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoHomologacaoInput();
        public preencherHomologacao: (entity: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoHomologacaoInput) => void;
        private realizarHomologacao: () => void;
        private declinarHomologacao: () => void;
        private emExigencia: () => void;
        private homologar: () => void;
        private eParaSerReadOnly: boolean;
        private tipoDeUsuarioCorretor: boolean;
        private tipoDeUsuarioBeneficiario: boolean;
        private tipoDeUsuarioAdministradora: boolean;
        private tipoDeUsuarioCorretora: boolean;
        private salvarContinuarVisible: boolean;
        private salvarContinuarDisabled: boolean;
        private botaoSalvarVisaoAdministradora: () => void;


        //Arquivo Documento
        public arquivoDocumentoInput = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
        public ezFileArquivoDocumento: ezFile.EZFile.EZFile;
        public ezFileArquivoDeclaracao: ezFile.EZFile.EZFile;

        public detalhes: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto;

        //Aceite do contrato
        public aceiteDoContratoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral
            .PropostaDeContratacaoPassoAceiteDoContratoInput();
        public preencherAceiteDoContrato: (entity: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.
            PropostaDeContratacaoInput) => void;

        //Declaração de Saúde
        public respostas: respostasDto.Dtos.EZLiv.Geral.RespostaDeDeclaracaoDeSaudeInput[];

        //Endereco Eletronico
        //public getEnderecoEletronicos: () => void;
        //public loadingEnderecoEletronicos: boolean;


        //Transação
        private pago: boolean;

        public atualizaStatusDaProposta: (status: StatusDaPropostaEnum) => void;

        public activeStep: number;
        public activeSubStep: number;
        public activeStepPagamento: number;

        public paisId: number;
        public estadoEnderecoId: number;

        // Chancela
        public ezSelectChancela: ezSelect.EzSelect.
        EzSelect<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoChancela: (termoDigitadoPesquisa: string) => any;
        private chancelaSelecionado: (registro: chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto) => string;
        private chancelaDeselecionado: () => void;
        private requestParamsChancela: requestParam.RequestParam.RequestParams;
        private carregarChancela: (id: number, text: string) => void;

        public unicaEntidade = false;

        public chancela: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput;

        public modalStatus: boolean = false;
        public modalInstance: any;

        public planoModal: produtoDtos.Dtos.EZLiv.Geral.DetalhesProdutoDePlanoDeSaudeInput;
        public closeModal: () => void;

        public modalCorretoraStatus: boolean = false;
        public modalCorretoraInstance: any;

        public ezGridVigenciaTeste: ezGrid.EzGrid.EzGrid<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto,
        requestParam.RequestParam.RequestParams>;

        public planoCorretoraModal: produtoDtos.Dtos.EZLiv.Geral.DetalhesProdutoDePlanoDeSaudeInput;
        public closeCorretoraModal: () => void;
        //public destroyGridEvent: () => void;

        public operadoras: Array<any>;

        // Gerente
        public ezSelectGerente: ezSelect.EzSelect.EzSelect<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoGerente: (termoDigitadoPesquisa: string) => any;
        private gerenteSelecionado: (registro: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto) => string;
        private gerenteDeselecionado: () => void;
        private requestParamsGerente: requestParam.RequestParam.RequestParams;
        private carregarGerente: (id: number, text: string) => void;

        // Supervisor
        public ezSelectSupervisor: ezSelect.EzSelect.EzSelect<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoSupervisor: (termoDigitadoPesquisa: string) => any;
        private supervisorSelecionado: (registro: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto) => string;
        private supervisorDeselecionado: () => void;
        private requestParamsSupervisor: requestParam.RequestParam.RequestParams;
        private carregarSupervisor: (id: number, text: string) => void;
        //private fnPagamentos: (PagamentoEnum: FormaDePagamentoEnum) => void;
        private fnViewContaBancaria: () => void;

        public enviarComprovante: () => void;
        public enviarComprovanteDepositoBancario: () => void;
        public enviarComprovanteCard: () => void;
        public confirmarComprovante: () => void;
        public removeComprovante: () => void;
        public pcDepositoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput();
        public pgFormas: any = { boleto: true, disableBoleto: false, transferencia: true, disableTransferencia: false, depositoBancario: true, disableDepositoBancario: false, cartaoCredito: true, disableCartaoCredito: false };
        public depositoBancario: any;
        public transformaConteudoComprovante: (files: any) => void;
        public transformaConteudoComprovanteDepositoBancario: (files: any) => void;
        public fileComprovante: any;

        static $inject = [
            '$scope', '$sce', '$rootScope', '$window', '$state', '$stateParams', 'uiGridConstants', 'propostaDeContratacaoService',
            'tipoDeDocumentoService', 'estadoService', 'profissaoService', 'transacaoPagSeguroService',
            'clienteService', 'pedidoService', 'enderecoService', 'produtoDePlanoDeSaudeService',
            'itemDeDeclaracaoDeSaudeService', 'arquivoDocumentoService', 'parametroGlobalService', 'grupoPessoaService',
            'pessoaFisicaService', 'enderecoEletronicoService', 'telefoneService', 'userService', 'vendaService',
            'tipoDeLogradouroService', 'paisService', 'cidadeService', 'redeCredenciadaService', 'transacaoService',
            'vigenciaService', 'questionarioDeDeclaracaoDeSaudeService', 'chancelaService', '$uibModal', 'operadoraService',
            'gerenteCorretoraService', 'supervisorCorretoraService', 'corretoraService', 'permissaoDeVendaDePlanoDeSaudeParaCorretoraService',
            'formaDePagamentoService'
        ];

        constructor(
            public $scope: ng.IScope,
            public $sce : ng.ISCEService,
            public $rootScope: any,
            public $window: any,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public propostaDeContratacaoService: propostaDeContratacaoService.Services.PropostaDeContratacaoService,
            public tipoDeDocumentoService: tipoDeDocumentoService.Services.TipoDeDocumentoService,
            public estadoService: estadoService.Services.EstadoService,
            public profissaoService: profissaoService.Services.ProfissaoService,
            public transacaoPagSeguroService: transacaoPagSeguroService.Services.TransacaoPagSeguroService,
            public clienteService: clienteService.Services.ClienteService,
            public pedidoService: pedidoService.Services.PedidoService,
            public enderecoService: enderecoService.Services.EnderecoService,
            public produtoService: produtoService.Services.ProdutoDePlanoDeSaudeService,
            public itemDeDeclaracaoDeSaudeService: itemDeDeclaracaoDeSaudeService.Services.ItemDeDeclaracaoDeSaudeService,
            public arquivoDocumentoService: arquivoDocumentoService.Services.ArquivoDocumentoService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public grupoPessoaService: grupoPessoaService.Services.GrupoPessoaService,
            public pessoaFisicaService: pessoaFisicaService.Services.PessoaFisicaService,
            public enderecoEletronicoService: enderecoEletronicoService.Services.EnderecoEletronicoService,
            public telefoneService: telefoneService.Services.TelefoneService,
            public userService: userService.Services.UserService,
            public vendaService: vendaService.Services.VendaService,
            public tipoDeLogradouroService: tipoDeLogradouroService.Services.TipoDeLogradouroService,
            public paisService: paisService.Services.PaisService,
            public cidadeService: cidadeService.Services.CidadeService,
            public redeCredenciadaService: redeCredenciadaService.Services.RedeCredenciadaService,
            public transacaoService: transacaoService.Services.TransacaoService,
            public vigenciaService: vigenciaService.Services.VigenciaService,
            public questionarioDeDeclaracaoDeSaudeService: questionarioDeDeclaracaoDeSaudeService.Services.QuestionarioDeDeclaracaoDeSaudeService,
            public chancelaService: chancelaService.Services.ChancelaService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public operadoraService: operadoraService.Services.OperadoraService,
            public gerenteService: gerenteService.Services.GerenteCorretoraService,
            public supervisorService: supervisorService.Services.SupervisorCorretoraService,
            public corretoraService: corretoraService.Services.CorretoraService,
            public permissaoDeVendaDePlanoDeSaudeParaCorretoraService: permissaoDeVendaDePlanoDeSaudeParaCorretoraService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretoraService,
            public formaDePagamentoService: formaDePagamentoService.Services.FormaDePagamentoService
        ) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                propostaDeContratacaoService,
                'tenant.ezliv.propostaDeContratacao');

            this.dadosGeraisInput.endereco = new enderecoDtos.Dtos.Endereco.EnderecoInput();

            this.salvarContinuarVisible = true;

            this.validaNumeroDocumento = () => {
                var listaServicosValidacao = [];
                listaServicosValidacao.push(abp.services.app.global.tipoDeDocumento.isCpf);
                return listaServicosValidacao;
            }

            this.carregarEstado = (id: number, text: string) => {
                if (!text) {
                    var promise = this.estadoService.getById(id);
                    promise.then(result => {
                        var estado = result;
                        this.ezSelectEstado.setInputText(estado.nome);
                    }).finally(() => {

                    });
                } else {
                    this.ezSelectEstado.setInputText(text);
                }
            }

            this.carregarProfissao = (id: number, text: string) => {
                if (!text) {
                    var promise = this.profissaoService.getById(id);
                    promise.then(result => {
                        var profissao = result;
                        this.ezSelectProfissao.setInputText(profissao.titulo);
                    }).finally(() => {
                        
                    });
                } else {
                    this.ezSelectProfissao.setInputText(text);
                }
            }

            this.carregarChancela = (id: number, text: string) => {
                if (!text) {
                    var promise = this.chancelaService.getByIdParaPropostaDeContratacao(id);
                    promise.then(result => {
                        var chancela = result;
                        this.ezSelectChancela.setInputText(chancela.nome);
                    }).finally(() => {

                    });
                } else {
                    this.ezSelectChancela.setInputText(text);
                }
            }

            this.carregarSupervisor = (id: number, text: string) => {
                if (!text) {
                    var promise = this.supervisorService.getById(id);
                    promise.then(result => {
                        var supervisor = result;
                        this.ezSelectSupervisor.setInputText(supervisor.nome);
                    }).finally(() => {

                    });
                } else {
                    this.ezSelectSupervisor.setInputText(text);
                }
            }

            this.fnViewContaBancaria = () => {
                var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                input.id = this.entity.corretoraId;
                var promiseVenda = this.vendaService.getDadosDepositoBancarios(input);
                promiseVenda.then((_result) => {
                    this.depositoBancario = this.pcDepositoInput;
                    this.depositoBancario.depositoTransferenciaBancariaInput = new depositoTransferenciaBancariaDtos.Dtos.EZPag.DepositoTransferenciaBancariaDtos.DepositoTransferenciaBancariaInput();
                    this.depositoBancario.depositoTransferenciaBancariaInput.banco = _result.banco;
                    this.depositoBancario.depositoTransferenciaBancariaInput.agencia = _result.agencia;
                    this.depositoBancario.depositoTransferenciaBancariaInput.conta = _result.conta;
                    this.depositoBancario.depositoTransferenciaBancariaInput.titular = _result.titular;
                });
            }

            //this.fnPagamentos = (PagamentoEnum: FormaDePagamentoEnum) => {
            //    $(document).ready(function () {

            //        var intervalId = null;
            //        let varCounter = 0;

            //        let fnRemoveClass = function (node, cls) {
            //            if (node && node.className && node.className.indexOf(cls) >= 0) {
            //                var pattern = new RegExp('\\s*' + cls + '\\s*');
            //                node.className = node.className.replace(pattern, ' ');
            //            }
            //        }

            //        let varName = function () {
            //            if (varCounter <= 10) {
            //                varCounter++;
            //                /* your code goes here */
            //                let elementLi = angular.element(document.querySelectorAll('#pagamentos div > ul > li'));
            //                let elementDiv = angular.element(document.querySelectorAll('#pagamentos div.tab-pane'));
            //                let elementActiveLi = angular.element(document.querySelector('#pagamentos div > ul > li.active'));
            //                let elementActiveDiv = angular.element(document.querySelector('#pagamentos div.tab-pane.active'));
            //                if (elementActiveLi.length > 0 && elementActiveDiv.length > 0) {
            //                    fnRemoveClass(elementActiveLi[0], 'active')
            //                    fnRemoveClass(elementActiveDiv[0], 'active')
            //                } else if ((elementActiveLi != null && elementActiveDiv != null) && (elementActiveLi.length == 0 && elementActiveDiv.length == 0)) {
            //                    if (PagamentoEnum == FormaDePagamentoEnum.Boleto) {
            //                        if (elementLi[0].className.indexOf('active') == -1) {
            //                            elementLi[0].className = 'active';
            //                            clearInterval(intervalId);
            //                        } 
            //                    } else if (PagamentoEnum == FormaDePagamentoEnum.Deposito) {
            //                        if (elementLi[1].className.indexOf('active') == -1) {
            //                            elementLi[1].className = 'active';
            //                            clearInterval(intervalId);
            //                        }
            //                    } else if (PagamentoEnum == FormaDePagamentoEnum.DepositoEmConta) {
            //                        if (elementLi[2].className.indexOf('active') == -1) {
            //                            elementLi[2].className = 'active';
            //                            elementDiv[2].className = 'active';
            //                            clearInterval(intervalId);
            //                        }
            //                    } else if (PagamentoEnum == FormaDePagamentoEnum.CartaoDeCredito) {
            //                        if (elementLi[3].className.indexOf('active') == -1) {
            //                            elementLi[3].className = 'active';
            //                            clearInterval(intervalId);
            //                        } 
            //                    }
            //                }
            //            } else {
            //                clearInterval(intervalId);
            //            }
            //        };
            //        intervalId = setInterval(varName, 3000);
            //    });
            //} 

            this.carregarGerente = (id: number, text: string) => {
                if (!text) {
                    var promise = this.gerenteService.getById(id);
                    promise.then(result => {
                        var gerente = result;
                        this.ezSelectGerente.setInputText(gerente.nome);
                    }).finally(() => {

                    });
                } else {
                    this.ezSelectGerente.setInputText(text);
                }
            }

            this.carregarGrupoPessoa = (id: number, text: string) => {
                if (!text) {

                    var promise = this.grupoPessoaService.getById(id);
                    promise.then(result => {
                        var grupoPessoa = result;
                        this.ezSelectGrupoPessoa.setInputText(grupoPessoa.nome);
                    }).finally(() => {

                    });
                } else {
                    this.ezSelectGrupoPessoa.setInputText(text);
                }
            }

            this.carregarPessoa = (id: number) => {

                var promise = this.pessoaFisicaService.getById(id);
                promise.then(result => {
                    var pessoa = result;
                    this.ezSelectPessoa.setInputText(pessoa.nome);
                }).finally(() => {

                });
            }

            this.carregarTipoDeLogradouro = (id: number, text: string) => {
                if (!text) {
                    if (id > 0) {
                        var promise = this.tipoDeLogradouroService.getById(id);
                        promise.then(result => {
                            var tipoDeLogradouro = result;
                            this.ezSelectTipoDeLogradouro.setInputText(tipoDeLogradouro.descricao);
                        }).finally(() => {

                        });
                    }
                } else {
                    this.ezSelectTipoDeLogradouro.setInputText(text);
                }
            }

            this.carregarPais = (id: number, text: string) => {
                if (!text) {
                    if (id > 0) {

                        var promise = this.paisService.getById(id);
                        promise.then(result => {
                            var pais = result;
                            this.ezSelectPais.setInputText(pais.nome);
                        }).finally(() => {

                        });
                    }
                } else {
                    this.ezSelectPais.setInputText(text);
                }
            }

            this.carregarEstadoEndereco = (id: number, text: string) => {
                if (!text) {
                    if (id > 0) {

                        var promise = this.estadoService.getById(id);
                        promise.then(result => {
                            var estado = result;
                            this.ezSelectEstadoEndereco.setInputText(estado.nome);
                            this.carregarPais(estado.paisId, '');
                        }).finally(() => {

                        });
                    }
                } else {
                    this.ezSelectEstadoEndereco.setInputText(text);
                }
            }

            this.carregarCidade = (id: number, text: string) => {
                if (!text) {
                    if (id > 0) {

                        var promise = this.cidadeService.getById(id);
                        promise.then(result => {
                            var cidade = result;
                            this.ezSelectCidade.setInputText(cidade.nome);
                            this.carregarEstadoEndereco(cidade.estadoId, '');
                        }).finally(() => {

                        });
                    }
                } else {
                    this.ezSelectCidade.setInputText(text);
                }
            }
        }

        $onInit() {

            super.init();

            this.activeStep = 0;
            this.activeSubStep = 0;
            this.sexos = ez.domain.enum.sexoEnum.valores;
            this.estadosCivis = ez.domain.enum.estadoCivilEnum.valores;
            this.grausDeParentesco = ez.domain.enum.grauDeParentescoEnum.valores;
            this.tiposDeResponsavel = ez.domain.enum.tipoDeResponsavelEnum.valores;
            this.arquivoDocumentoInput.id = 0;
            this.arquivoDocumentoInput.isActive = true;
            this.arquivoDocumentoInput.emExigencia = false;
            this.arquivoDocumentoInput.motivo = "";
            this.ezFileArquivoDocumento = new ezFile.EZFile.EZFile();
            this.ezFileArquivoDeclaracao = new ezFile.EZFile.EZFile();
            this.preCadastroInput.passoDaProposta = PassoDaPropostaEnum.PreCadastro;
            setTimeout(() => {
                $(".ezs-container input:text:visible:not(:disabled)").first().focus();
            }, 1000);
            this.selecaoDeProdutoInput.passoDaProposta = PassoDaPropostaEnum.SelecionarPlano;
            this.dadosGeraisInput.passoDaProposta = PassoDaPropostaEnum.PreenchimentoDosDados;
            this.envioDeDocumentoInput.passoDaProposta = PassoDaPropostaEnum.EnvioDeDocumentos;
            this.aceiteDoContratoInput.passoDaProposta = PassoDaPropostaEnum.AceiteDoContrato;
            this.homologacaoInput.passoDaProposta = PassoDaPropostaEnum.Homologacao;
            this.homologacaoInput.tipoDeHomologacao = TipoDeHomologacaoEnum.PendenteDeHomologacao;
            this.requestParamsProduto = new requestParam.RequestParam.RequestParams();
            this.requestParamsVigencia = new requestParam.RequestParam.RequestParams();
            this.requestParamsCorretora = new requestParam.RequestParam.RequestParams();

            this.aditivosCheckbox = [];

            // callback para criar e definir o filtro para buscar os registros de produto
            this.getFiltroParaPaginacaoProduto = (termoDigitadoPesquisa) => {
                var filtro = new produtoDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // callback para criar e definir o filtro para buscar os registros de vigencia
            this.getFiltroParaPaginacaoVigencia = (termoDigitadoPesquisa) => {
                var filtro = new vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaInput();
                filtro.numeroDeBoletos = termoDigitadoPesquisa;
                return filtro;
            }

            // callback para criar e definir o filtro para buscar os registros de vigencia
            this.getFiltroParaPaginacaoCorretora = (termoDigitadoPesquisa) => {
                var filtro = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }


            // instanciar grid de produto
            this.ezGridProduto = new ezGrid.EzGrid.EzGrid<produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto,
                requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoService,
                this.getFiltroParaPaginacaoProduto,
                this.requestParamsProduto);

            // instanciar grid de produto
            this.ezGridVigencia = new ezGrid.EzGrid.EzGrid<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto,
                requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.vigenciaService,
                this.getFiltroParaPaginacaoVigencia,
                this.requestParamsVigencia);

            // instanciar grid de corretora
            this.ezGridCorretora = new ezGrid.EzGrid.EzGrid<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto,
                requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretoraService,
                this.getFiltroParaPaginacaoCorretora,
                this.requestParamsCorretora);


            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput();
                instance.id = 0;
                instance.documentos = [];
                instance.passoDaProposta = PassoDaPropostaEnum.PreCadastro;
                instance.dependentes = [];
                instance.formaDeContratacao = FormaDeContratacaoEnum.Adesao;
                instance.tipoDeProposta = TipoDePropostaEnum.Nova;

                this.setDadosPagamento();
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.propostaDeContratacaoService.enqueue = true;

                if (entity.numero) {
                    this.protocolNumber = entity.numero;
                }

                if (entity.produto) {
                    this.getItensDeDeclaracaoDeSaude();
                }

                this.getOnDemand(entity);

                this.idadeDoTitular = entity.titular.idade;

                this.setStepByPassoDaProposta(entity.passoDaProposta);

                this.propostaDeContratacaoService.runQueue(() => {
                    this.passoAtual = entity.passoDaProposta;
                    this.selectTab(entity.passoDaProposta, false);
                    this.propostaDeContratacaoService.enqueue = false;
                });
            };

            this.preencherPreCadastro = (entity) => {
                if (this.entity != null) {
                    this.preCadastroInput.passoDaProposta = this.entity.passoDaProposta;
                    this.preCadastroInput.titularResponsavelLegal = this.entity.titularResponsavelLegal;
                    this.disabledCPFAndEmailPreCadastro = true;
                    //PreCadastro/
                    if (this.entity.titular != null) {
                        if (this.entity.passoDaProposta == 0)
                            this.entity.passoDaProposta = PassoDaPropostaEnum.PreCadastro;

                        this.preCadastroInput.id = this.entity.id;
                        this.preCadastroInput.titularId = this.entity.titular.id;

                        if (this.entity.titular.pessoaFisica != null) {
                            this.preCadastroInput.nomeCompleto = this.entity.titular.pessoaFisica.nome;
                            this.preCadastroInput.email = this.entity.titular.pessoaFisica.emailPrincipal != null
                                ? this.entity.titular.pessoaFisica.emailPrincipal.endereco
                                : "";
                            this.preCadastroInput
                                .emailId = this.entity.titular.pessoaFisica.emailPrincipal != null
                                    ? this.entity.titular.pessoaFisica.emailPrincipal.id
                                    : 0;
                            this.preCadastroInput.cpfId = this.entity.titular.pessoaFisica.cpfId;
                            this.preCadastroInput.cpf = this.entity.titular.pessoaFisica.cpf;
                            this.preCadastroInput.pessoaId = this.entity.titular.pessoaFisica.id;

                            this.preCadastroInput
                                .dataDeNascimento = this.entity.titular.pessoaFisica.dataDeNascimento != null
                                    ? new Date(this.entity.titular.pessoaFisica.dataDeNascimento.toString())
                                    : new Date();
                            this.entity.titular.pessoaFisica
                                .dataDeNascimento = this.entity.titular.pessoaFisica.dataDeNascimento != null
                                    ? new Date(this.entity.titular.pessoaFisica.dataDeNascimento.toString())
                                    : new Date();
                            this.preCadastroInput
                                .celular = this.entity.titular.pessoaFisica.telefoneCelular != null
                                    ? this.entity.titular.pessoaFisica.telefoneCelular.ddd +
                                    this.entity.titular.pessoaFisica.telefoneCelular.numero
                                    : "";
                            this.preCadastroInput
                                .celulaId = this.entity.titular.pessoaFisica.telefoneCelular != null
                                    ? this.entity.titular.pessoaFisica.telefoneCelular.id
                                    : 0;
                            this.preCadastroInput.grupoPessoaId = this.entity.titular.pessoaFisica.grupoPessoaId;

                            if (this.preCadastroInput.grupoPessoaId > 0)
                                this.carregarGrupoPessoa(this.preCadastroInput.grupoPessoaId, '');
                        }
                    }

                    if (this.entity.estadoId > 0) {
                        this.preCadastroInput.estadoId = this.entity.estadoId;
                        if (this.entity.passoDaProposta < PassoDaPropostaEnum.Homologacao) {
                            this.carregarEstado(this.preCadastroInput.estadoId, '');
                        } else {
                            this.carregarEstado(this.preCadastroInput.estadoId, this.entity.estado.sigla);
                        }
                    }

                    if (this.entity.profissaoId > 0) {
                        this.preCadastroInput.profissaoId = this.entity.profissaoId;
                        if (this.entity.passoDaProposta < PassoDaPropostaEnum.Homologacao) {
                            this.carregarProfissao(this.preCadastroInput.profissaoId, '');
                        } else {
                            this.carregarProfissao(this.preCadastroInput.profissaoId, this.entity.profissao.titulo);
                        }
                    }

                    if (this.entity.titular.dependentes != null) {
                        this.preCadastroInput.dependentes = this.entity.dependentes = this.entity.titular.dependentes;

                        if (this.entity.titular != null)
                            this.entity.dependentes = this.entity.titular.dependentes;

                        for (let dependente of this.entity.dependentes) {
                            var dep = (dependente as dependenteDto.Dtos.EZLiv.Geral.DependenteInput);

                            if (dep.pessoaFisica.dataDeNascimento != null) {
                                dep.pessoaFisica
                                    .dataDeNascimento = dependente.pessoaFisica.dataDeNascimento != null
                                        ? new Date(dependente.pessoaFisica.dataDeNascimento.toString())
                                        : new Date();
                            }

                            dependente.pessoaFisica.documentoPrincipal = documentoDtos.Dtos.Documento.DocumentoInput;
                            dependente.pessoaFisica.documentoPrincipal.numero = dependente.pessoaFisica.cpf;
                        }
                    }
                }
            }

            this.preencherDadosGerais = (entity) => {
                //Dados Gerais/

                if (this.entity != null && this.entity.inicioDeVigencia) {
                    this.dadosGeraisInput.inicioDeVigencia = new Date(this.entity.inicioDeVigencia.toString());

                    if (this.entity.vigenciaId) {
                        this.dadosGeraisInput.vigenciaId = this.entity.vigenciaId;
                    }
                }

                if (this.entity.produtoId > 0) {

                    this.ezGridVigencia.serviceGetCallback = () => {
                        var inputVigencia = new vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaByProdutoInput;
                        inputVigencia.produtoId = this.entity.produtoId;
                        return this.vigenciaService.getPagedByProduto(inputVigencia, this.requestParamsVigencia);
                    };



                }

               



                if (this.entity.itemDeDeclaracaoDoBeneficiarioId) {
                    this.dadosGeraisInput.itemDeDeclaracaoDoBeneficiarioId = this.entity.itemDeDeclaracaoDoBeneficiarioId;
                }

                this.dadosGeraisInput.aceiteDaDeclaracaoDeSaude = this.entity.aceiteDaDeclaracaoDeSaude;

                if (this.entity != null && this.entity.produtoId > 0 && this.entity.vigenciaId) {
                    for (var i = 0; i < this.ezGridVigencia.optionsGrid.data.length; i++) {
                        var rowDataVigencia = this.ezGridVigencia.optionsGrid
                            .data[i] as vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto;
                        if (rowDataVigencia.id == this.entity.vigenciaId) {
                            var rowVigencia = this.ezGridVigencia.gridApi.grid.getRow(rowDataVigencia);
                            if (rowVigencia) {
                                this.ezGridVigencia.gridApi.grid.modifyRows(Array<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto>());
                                this.ezGridVigencia.optionsGrid.enableRowSelection = true;
                                rowVigencia.isSelected = true;
                            }
                        }
                        else
                        {
                            

                        }
                    }
                }

                this.ezGridVigencia.getRegistros();

               



                if (this.entity.chancelaId > 0) {

                    // Store
                    this.$window.localStorage.setItem('keyChancela', angular.toJson({ id: this.entity.produto.chancelas[0].id, nome: this.entity.produto.chancelas[0].nome }));

                    if (this.entity.passoDaProposta < PassoDaPropostaEnum.Homologacao) {
                        var promiseChancela = this.chancelaService.getByIdParaPropostaDeContratacao(this.entity.chancelaId);
                        promiseChancela.then((result) => {
                            this.chancela = result;
                            this.dadosGeraisInput.chancelaId = result.id;
                            this.ezSelectChancela.setInputText(result.nome);
                        });
                    } else {
                        this.ezSelectChancela.setInputText(this.entity.chancela.nome);
                    }
                }

                if (this.entity.gerenteId > 0) {
                    // TODO COlocar o gerente como input trava o carregamento da proposta. remover
                    //if (this.entity.passoDaProposta < PassoDaPropostaEnum.Homologacao) {
                    var promiseGerente = this.gerenteService.getById(this.entity.gerenteId);
                    promiseGerente.then((result) => {
                        this.dadosGeraisInput.gerenteId = result.id;
                        this.ezSelectGerente.setInputText(result.pessoa.nomePessoa);
                    });
                    //} else {
                    //    this.ezSelectGerente.setInputText(this.entity.gerente.nome);
                    //}
                }

                if (this.entity.supervisorId > 0) {
                    // TODO COlocar o supervisor como input trava o carregamento da proposta. remover
                    //if (this.entity.passoDaProposta < PassoDaPropostaEnum.Homologacao) {
                    var promiseSupervisor = this.supervisorService.getById(this.entity.supervisorId);
                    promiseSupervisor.then((result) => {
                        this.dadosGeraisInput.supervisorId = result.id;
                        this.ezSelectSupervisor.setInputText(result.pessoa.nomePessoa);
                    });
                    //} else {
                    //    this.ezSelectSupervisor.setInputText(this.entity.supervisor.nome);
                    //}
                }

                if (this.entity != null && this.entity.titular) {
                    this.entity.dependentes = this.entity.titular.dependentes;

                    if (this.respostas && (!this.entity.titular.respostasDeclaracaoDeSaude ||
                        this.entity.titular.respostasDeclaracaoDeSaude.length != this.respostas.length)) {
                        this.entity.titular.respostasDeclaracaoDeSaude = this.respostas;
                    }

                    this.dadosGeraisInput.dependentes = this.entity.dependentes;

                    for (let dependente of this.entity.dependentes) {
                        if ((dependente as dependenteDto.Dtos.EZLiv.Geral.DependenteInput).pessoaFisica
                            .dataDeNascimento !=
                            null)
                            (dependente as dependenteDto.Dtos.EZLiv.Geral.DependenteInput).pessoaFisica
                                .dataDeNascimento = new Date(dependente.pessoaFisica.dataDeNascimento);
                    }


                    if (this.entity.titular != null) {
                        this.dadosGeraisInput.id = this.entity.id;

                        // Proponente Titular
                        if (this.dadosGeraisInput.proponenteTitular == null ||
                            this.dadosGeraisInput.proponenteTitular == undefined)
                            this.dadosGeraisInput.proponenteTitular = new proponenteTitularDtos.Dtos.EZLiv.Geral
                                .ProponenteTitularInput();

                        this.dadosGeraisInput.proponenteTitular.id = this.entity.titular.id;

                        if (this.entity.titular.pessoaFisica != null) {
                            this.dadosGeraisInput.nomeProponenteTitular = this.entity.titular.pessoaFisica.nome;
                            this.dadosGeraisInput
                                .emailProponenteTitular = this.entity.titular.pessoaFisica.emailPrincipal
                                    ? this.entity.titular.pessoaFisica.emailPrincipal.endereco
                                    : "";
                            this.dadosGeraisInput
                                .emailIdProponenteTitular =
                                this.entity.titular.pessoaFisica.emailPrincipal
                                    ? this.entity.titular.pessoaFisica.emailPrincipal.id
                                    : 0;
                            this.dadosGeraisInput.cpfIdProponenteTitular = this.entity.titular.pessoaFisica.cpfId;
                            this.dadosGeraisInput.cpfProponenteTitular = this.entity.titular.pessoaFisica.cpf;

                            if (this.entity.titular.pessoaFisica.dataDeNascimento != null) {
                                this.dadosGeraisInput
                                    .dataDeNascimentoProponenteTitular = new
                                        Date(this.entity.titular.pessoaFisica.dataDeNascimento.toString());
                                this.entity.titular.pessoaFisica
                                    .dataDeNascimento = new
                                        Date(this.entity.titular.pessoaFisica.dataDeNascimento.toString());
                            } else {
                                this.dadosGeraisInput.dataDeNascimentoProponenteTitular = this.preCadastroInput
                                    .dataDeNascimento;
                            }


                            this.dadosGeraisInput.rgProponenteTitular = this.entity.titular.pessoaFisica.rg;
                            this.dadosGeraisInput.rgIdProponenteTitular = this.entity.titular.pessoaFisica.rgId;
                            this.dadosGeraisInput.orgaoExpedidorProponenteTitular = this.entity.titular.pessoaFisica
                                .orgaoExpedidor;
                            this.dadosGeraisInput.nacionalidadeProponenteTitular = this.entity.titular.pessoaFisica
                                .nacionalidade;
                            this.dadosGeraisInput.sexoProponenteTitular = this.entity.titular.pessoaFisica.sexo;
                            this.dadosGeraisInput.estadoCivilProponenteTitular = this.entity.titular.pessoaFisica
                                .estadoCivil;
                            this.dadosGeraisInput.nomeDaMaeProponenteTitular = this.entity.titular.nomeDaMae;
                            this.dadosGeraisInput.numeroDoCartaoNacionalDeSaudeProponenteTitular = this.entity.titular
                                .numeroDoCartaoNacionalDeSaude;
                            this.dadosGeraisInput.matricula = this.entity.titular
                                .matricula;
                            this.dadosGeraisInput.boleto = this.entity.titular.boleto == true ? "Boleto" : "";
                            this.dadosGeraisInput.folha = this.entity.titular.folha == true ? "Folha" : "";


                            if (this.entity.titular.debitoConta == true)
                                this.dadosGeraisInput.DebitoConta = "sim";
                            else
                                this.dadosGeraisInput.DebitoConta = "nao";


                            if (this.entity.titular.folhaFicha == true)
                                this.dadosGeraisInput.FolhaFicha = "sim";
                            else
                                this.dadosGeraisInput.FolhaFicha = "nao";



                            this.dadosGeraisInput.declaracaoDeNascidoVivoProponenteTitular = this.entity.titular
                                .declaracaoDeNascidoVivo;
                        }

                        //Dados do Responsável Legal
                        if (this.entity.responsavel != null) {
                            this.dadosGeraisInput.tipoResponsavel = this.entity.responsavel.tipoDeResponsavel;
                            this.dadosGeraisInput.nomeResponsavel = this.entity.responsavel.pessoaFisica.nome;
                            this.dadosGeraisInput
                                .dataDeNascimentoReponsavel = new
                                    Date(this.entity.responsavel.pessoaFisica.dataDeNascimento);
                            this.dadosGeraisInput.nacionalidadeResponsavel = this.entity.responsavel.pessoaFisica
                                .nacionalidade;
                            this.dadosGeraisInput.sexoResponsavel = this.entity.responsavel.pessoaFisica.sexo;
                            this.dadosGeraisInput.estadoCivilResponsavel = this.entity.responsavel.pessoaFisica
                                .estadoCivil;
                            this.dadosGeraisInput.cpfIdReponsavel = this.entity.responsavel.pessoaFisica.cpfId;
                            this.dadosGeraisInput.cpfReponsavel = this.entity.responsavel.pessoaFisica.cpf;

                            this.dadosGeraisInput.celulaId = this.entity.responsavel.pessoa.telefoneCelularId > 0 ?
                                this.entity.responsavel.pessoa.telefoneCelularId : 0;
                            this.dadosGeraisInput.celular = this.entity.responsavel.pessoaFisica.telefoneCelular != null
                                ? this.entity.responsavel.pessoaFisica.telefoneCelular.ddd +
                                this.entity.responsavel.pessoaFisica.telefoneCelular.numero
                                : "";
                            this.dadosGeraisInput.email = this.entity.responsavel.pessoaFisica.emailPrincipal != null
                                ? this.entity.responsavel.pessoaFisica.emailPrincipal.endereco
                                : "";
                            this.dadosGeraisInput.emailId = this.entity.responsavel.pessoaFisica.emailPrincipal != null
                                ? this.entity.responsavel.pessoaFisica.emailPrincipal.id
                                : 0;
                        }

                        this.atualizarIdade(this.dadosGeraisInput);
                    }

                    /*
                    var endereco = (this.idadeDoTitular >= 18)
                        ? entity.titular.pessoa.enderecoPrincipal
                        : (entity.responsavel != null ? entity.responsavel.pessoa.enderecoPrincipal : null);
                    */
                    var endereco = entity.titular.pessoa.enderecoPrincipal;

                    if (endereco != null) {
                        this.dadosGeraisInput.endereco = endereco;

                        this.carregarCidade(endereco.cidadeId, '');
                        this.carregarTipoDeLogradouro(endereco.tipoDeLogradouroId, '');
                    }

                    //TODO: Carregar respostas de declaração de saúde aqui
                    if (this.entity.passoDaProposta >= 37) {
                        var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                        input.id = this.entity.id;
                        var promiseRespostas = this.propostaDeContratacaoService.getRespostasDeclaracaoDeSaude(input);
                        promiseRespostas.then((result) => {
                            //Associa respostas dadas pelo titular
                            if (result.proponenteTitular.respostasDeclaracaoDeSaude != null) {
                                this.entity.titular.respostasDeclaracaoDeSaude = result.proponenteTitular
                                    .respostasDeclaracaoDeSaude;
                                //Associa respostas dadas para cada dependente
                                for (var d = 0; d < this.entity.dependentes.length; d++) {
                                    var dependenteLocal = this.entity.dependentes[d];
                                    for (var r = 0; r < result.dependentes.length; r++) {
                                        var dependenteResposta = result.dependentes[r];
                                        if (dependenteResposta.id == dependenteLocal.id) {
                                            this.entity.dependentes[d].respostasDeclaracaoDeSaude = dependenteResposta
                                                .respostasDeclaracaoDeSaude;
                                        }
                                    }
                                }
                            }
                        });
                    }
                }

                // Seta aditivos
                if (this.entity && this.entity.aditivos && this.entity.produto && this.entity.produto.aditivos) {
                    var lengthAditivoOpcao = this.entity.produto.aditivos.length;

                    for (i = 0; i < lengthAditivoOpcao; i++) {
                        var aditivoJaMarcada = this.entity.aditivos.filter(adt => adt.id === this.entity.produto.aditivos[i].id);
                        if (aditivoJaMarcada.length > 0) {

                            // Seta checkbox
                            this.aditivosCheckbox[i] = true;

                            // Seta input dados gerais
                            if (!this.dadosGeraisInput.aditivos) {
                                this.dadosGeraisInput.aditivos = [];
                            }
                            this.dadosGeraisInput.aditivos[i] = aditivoJaMarcada[0];
                        }
                    }
                }


                if (this.entity.inicioDeVigencia == null) {

                    //this.ezGridVigencia.getRegistros();
                    var rowDataVigencia = this.ezGridVigencia.optionsGrid.data[0] as vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto;

                    if (rowDataVigencia != undefined) {

                        this.dadosGeraisInput.vigenciaId = rowDataVigencia.id;

                        var registro = new vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto;
                        registro.dataDeFechamento = rowDataVigencia.dataDeFechamento;
                        registro.dataDeVigencia = rowDataVigencia.dataDeVigencia;
                        registro.dataFinalDaProposta = rowDataVigencia.dataFinalDaProposta;
                        registro.dataInicialDaProposta = rowDataVigencia.dataInicialDaProposta;
                        registro.id = rowDataVigencia.id;
                        registro.isActive = rowDataVigencia.isActive;
                        registro.numeroDeBoletos = rowDataVigencia.numeroDeBoletos;
                        //seleciona o item clicado
                        this.setInicioVigencia(registro);
                        
                    }

                }





            }

            this.preencherAceiteDoContrato = (entity) => {
                //Aceite de Contrato
                this.aceiteDoContratoInput.id = entity.id;
                this.aceiteDoContratoInput.aceite = entity.aceite;
                this.aceiteDoContratoInput.aceiteCorretor = entity.aceiteCorretor;
            }

            this.preencherHomologacao = (entity) => {
                //Homologação
                this.homologacaoInput.id = entity.id;
                this.homologacaoInput.tipoDeHomologacao = entity.tipoDeHomologacao;
                this.homologacaoInput.observacaoHomologacao = entity.observacaoHomologacao;
            }

            this.passoDaPropostaForTabsEnum = PassoDaPropostaEnum;

            this.botaoSalvarVisaoAdministradora = () => {
                if (this.eParaSerReadOnly && this.activeStep != 5)
                    this.salvarContinuarVisible = false;
                else
                    this.salvarContinuarVisible = true;
            }

            this.setStepByPassoDaProposta = (numPassoProposta) => {
                switch (numPassoProposta) {
                    case PassoDaPropostaEnum.PreCadastro:
                        {
                            this.activeStep = 0;
                            break;
                        }
                    case PassoDaPropostaEnum.SelecionarPlano:
                        {                            
                            this.activeStep = 1;
                            break;
                        }
                    case PassoDaPropostaEnum.DadosProponenteTitular:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 0;
                            break;
                        }
                    case PassoDaPropostaEnum.DadosResponsavel:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 1;
                            break;
                        }
                    case PassoDaPropostaEnum.Endereco:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 2;
                            break;
                        }
                    case PassoDaPropostaEnum.DadosDependentes:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 3;
                            break;
                        }
                    case PassoDaPropostaEnum.Documentos:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 4;
                            break;
                        }
                    case PassoDaPropostaEnum.DadosVigencia:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 5;
                            break;
                        }
                    case PassoDaPropostaEnum.DadosGerenciais:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 6;
                            break;
                        }
                    case PassoDaPropostaEnum.DadosDeclaracaoDeSaude:
                        {
                            this.activeStep = 2;
                            this.activeSubStep = 7;
                            break;
                        }
                    case PassoDaPropostaEnum.AceiteDoContrato:
                        {
                            this.activeStep = 3;
                            break;
                        }
                    case PassoDaPropostaEnum.Homologacao:
                        {
                            this.activeStep = 4;
                            break;
                        }
                    case PassoDaPropostaEnum.Pagamento:
                        {
                            this.activeStep = 5;
                            break;
                        }
                }
            }

            //this.scrollFixedFn = () => {
            //    var visibleOffset = $('#carousel-operadoras').offset();
            //    if (visibleOffset) {
            //        var carouselVisibleOffset = $('#carousel-operadoras').offset().top - $(window).scrollTop();
            //        if ($(window).scrollTop() >= 460) {
            //            $('.ezGridPlanoWrapper').addClass('top-fixed');
            //            $('body').addClass('top-fixed');
            //        }
            //        if ($(window).scrollTop() <= 150) {
            //            $('.ezGridPlanoWrapper').removeClass('top-fixed');
            //            $('body').removeClass('top-fixed');
            //        }
            //    }
            //}

            //this.scrollFixed = () => {
            //    $(window).on('scroll', this.scrollFixedFn);
            //}

            this.selectTab = (numPassoProposta, uiClick = true) => {

                setTimeout(() => {

                    if ($("div.portlet-body.form > proposta-de-contratacao-form > div > form > div > div > ul > li.active > a").text() == "Pré-cadastro") {

                        if ($(".ezs-container input:text:visible").first().prop('disabled')) {
                            $(".ezs-container input[type=date]:visible:not(:disabled)").first().focus();
                        } else {
                            $(".ezs-container input:text:visible:not(:disabled)").first().focus();
                        }
                    }

                    if ($("div.portlet-body.form > proposta-de-contratacao-form > div > form > div > div > ul > li.active > a").text() == "Proposta") {
                        if ($(".tab-pane.active div > ul > li.uib-tab.nav-item.active a").text() == "Documentos") {
                            $(".ezs-container select:visible:not(:disabled)").first().focus();
                        } else if ($(".tab-pane.active div > ul > li.uib-tab.nav-item.active a").text() == "Declaração de Saúde") {
                            $(".ezs-container input[type=radio]:visible:not(:disabled)").first().focus();
                        } else if ($(".ez-accordion-wrapper > div.ez-accordion.active:visible").first().length > 0) {
                            $(".ezs-container input:text:visible:not(:disabled)").first().focus();
                        } else {
                            $(".ezs-container input:text:visible:not(:disabled)").first().focus();
                        }
                    }

                }, 1000);

                if (this.entity && this.entity.id == 0 && numPassoProposta > 10)
                    return;

                if (this.entity.passoDaProposta < numPassoProposta) {
                    return;
                }

                //if (uiClick)
                //    return;                       

                this.passoAtual = numPassoProposta;

                switch (numPassoProposta) {
                    case PassoDaPropostaEnum.PreCadastro:
                        {
                            this.activeStep = 0;
                            this.botaoSalvarVisaoAdministradora();
                            this.PreencherDependentePorAba();
                            this.preencherPreCadastro(this.entity);
                            break;
                        }
                    case PassoDaPropostaEnum.SelecionarPlano:
                        {
                            if (!uiClick) {
                                setTimeout(() => {
                                    this.selectTab(PassoDaPropostaEnum.SelecionarPlano, true);
                                }, 3000);
                            }

                            $(window).off("scroll");
                            $(window).scrollTop(0);
                            this.activeStep = 1;
                            this.disableTabSelecionarPlano = false;
                            this.botaoSalvarVisaoAdministradora();

                            if (this.entity.titularId > 0) {
                                this.ezGridProduto.serviceGetCallback = () => {

                                    //var inputProduto = new produtoDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput;
                                    //inputProduto.proponenteId = this.entity.titularId;
                                    //inputProduto.profissaoId = this.entity.profissaoId;
                                    //inputProduto.estadoId = this.entity.estadoId;
                                    //return this.produtoService.getPagedProdutoAtivoComValor(inputProduto);

                                    var input = new produtoDtos.Dtos.EZLiv.Geral.GetValoresAndProdutoByCorretorInput();
                                    input.proponenteId = this.entity.titularId;
                                    input.profissaoId = this.entity.profissaoId;
                                    input.estadoId = this.entity.estadoId;
                                    input.corretorId = this.entity.corretorId;
                                    return this.produtoService.getProdutoDePlanoDeSaudeWithValorForCorretor(input);



                                };
                            }

                            if (this.entity.produtoId > 0) {
                                this.ezGridProduto.appScopeProvider.executeAfterGetRegistros = () => {
                                    setTimeout(() => {
                                        $('.grid').find('[data-plano-id=' + this.entity.produtoId + ']').find('row-fake-select').addClass('active');
                                    }, 500);
                                };

                                this.ezGridVigencia.serviceGetCallback = () => {
                                    var inputVigencia = new vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaByProdutoInput;
                                    inputVigencia.produtoId = this.entity.produtoId;
                                    return this.vigenciaService.getPagedByProduto(inputVigencia, this.requestParamsVigencia);
                                };
                            }

                            if (this.entity != null && this.entity.produtoId > 0 && this.entity.vigenciaId) {
                                for (var i = 0; i < this.ezGridVigencia.optionsGrid.data.length; i++) {
                                    var rowDataVigencia = this.ezGridVigencia.optionsGrid
                                        .data[i] as vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto;
                                    if (rowDataVigencia.id == this.entity.vigenciaId) {
                                        var rowVigencia = this.ezGridVigencia.gridApi.grid.getRow(rowDataVigencia);
                                        if (rowVigencia) {
                                            this.ezGridVigencia.gridApi.grid
                                                .modifyRows(Array<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto>());
                                            this.ezGridVigencia.optionsGrid.enableRowSelection = true;
                                            //rowVigencia.setSelected(true);
                                        }
                                    }
                                }
                            } else {
                                if (this.tipoDeUsuarioBeneficiario) {

                                    if (this.entity.produtoId &&
                                        this.entity.produtoId > 0 &&
                                        this.entity.corretorId &&
                                        this.entity.corretorId > 0) {
                                        this.ezGridProduto.serviceGetCallback = () => {
                                            var input = new produtoDtos.Dtos.EZLiv.Geral.GetValoresAndProdutoByCorretorInput();
                                            input.proponenteId = this.entity.titularId;
                                            input.contratoId = this.entity.corretorId;
                                            input.profissaoId = this.entity.profissaoId;
                                            input.estadoId = this.entity.estadoId;
                                            return this.produtoService.getProdutoDePlanoDeSaudeWithValorForCorretor(input);
                                        };
                                    } else {
                                        this.ezGridProduto.serviceGetCallback = () => {
                                            var input = new produtoDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput();
                                            input.proponenteId = this.entity.titularId;
                                            input.contratoId = this.entity.corretorId;
                                            return this.produtoService.getProdutoDePlanoDeSaudeWithValorForCorretora(input);
                                        };
                                    }
                                }
                                if (this.tipoDeUsuarioCorretor) {
                                    this.ezGridProduto.serviceGetCallback = () => {
                                        var input = new produtoDtos.Dtos.EZLiv.Geral.GetValoresAndProdutoByCorretorInput();
                                        input.proponenteId = this.entity.titularId;
                                        input.profissaoId = this.entity.profissaoId;
                                        input.estadoId = this.entity.estadoId;
                                        return this.produtoService.getProdutoDePlanoDeSaudeWithValorForCorretor(input);
                                    };
                                }
                            }


                            this.ezGridProduto.getRegistros();
                            //this.scrollFixed();

                            break;
                        }
                    case PassoDaPropostaEnum.PreenchimentoDosDados:
                        {
                            this.setStepByPassoDaProposta(PassoDaPropostaEnum.DadosProponenteTitular);
                            this.setStepPreenchimentoDeDados(PassoDaPropostaEnum.DadosProponenteTitular);
                            break;
                        }
                    case PassoDaPropostaEnum.DadosProponenteTitular:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.DadosResponsavel:
                        {
                            if (this.preCadastroInput.titularResponsavelLegal) {
                                this.setStepByPassoDaProposta(PassoDaPropostaEnum.Endereco);
                                break;
                            }
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.Endereco:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.DadosDependentes:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.Documentos:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.DadosVigencia:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.DadosGerenciais:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.DadosDeclaracaoDeSaude:
                        {
                            this.setStepByPassoDaProposta(numPassoProposta);
                            this.setStepPreenchimentoDeDados(numPassoProposta);
                            break;
                        }
                    case PassoDaPropostaEnum.AceiteDoContrato:
                        {
                            this.activeStep = 3;
                            this.disableTabSelecionarPlano = false;
                            this.disableTabProposta = false;
                            this.disableTabAceite = false;

                            var inputDetail = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                            inputDetail.produtoId = this.entity.produtoId;
                            inputDetail.propostaId = this.entity.id;

                            var promiseDetail = this.produtoService.getDetalhesById(inputDetail);
                            promiseDetail.then((result) => {
                                this.planoModal = result;
                            });

                            window.setInterval(() => {
                                window.console.log(this.tipoDeUsuarioCorretor, this.entity.aceiteCorretor);
                            }, 1000);

                            if (this.entity.produtoId > 0) {
                                var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput;
                                input.id = this.entity.produtoId;
                                var promise = this.produtoService.getContrato(input);
                                promise.then((result) => {
                                    this.contratoInput.contratoId = result.id;
                                });
                                this.loadConteudoContrato();
                            }

                            this.botaoSalvarVisaoAdministradora();

                            break;
                        }
                    case PassoDaPropostaEnum.Homologacao:
                        {
                            this.activeStep = 4;
                            this.disableTabSelecionarPlano = false;
                            this.disableTabProposta = false;
                            this.disableTabAceite = false;
                            this.disableTabHomologacao = false;
                            var inputDetail = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                            inputDetail.produtoId = this.entity.produtoId;
                            inputDetail.propostaId = this.entity.id;
                            var promiseDetail = this.produtoService.getDetalhesById(inputDetail);
                            promiseDetail.then((result) => {
                                this.planoModal = result;
                            });
                            this.botaoSalvarVisaoAdministradora();
                            break;
                        }
                    case PassoDaPropostaEnum.Pagamento:
                        {
                            var promiseFormasDePagamento = this.formaDePagamentoService.getIsFormaPg(this.entity.corretoraId);
                            promiseFormasDePagamento.then((_returnFormasDePagamento) => {

                                if (_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.BOLETO).length > 0)
                                    this.pgFormas.boleto = !_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.BOLETO)[0].isActive;
                                if (_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.ONLINE_DEBIT).length > 0)
                                    this.pgFormas.transferencia = !_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.ONLINE_DEBIT)[0].isActive;

                                if (_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.DEPOSIT_ACCOUNT).length > 0)
                                    this.pgFormas.depositoBancario = !_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.DEPOSIT_ACCOUNT)[0].isActive;

                                if (_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.CREDIT_CARD).length > 0)
                                    this.pgFormas.cartaoCredito = !_returnFormasDePagamento.items.filter(x => x['tipoDePagamento'] == TipoCheckoutPagSeguroEnum.CREDIT_CARD)[0].isActive;



                                if (this.entity.pedidoId && this.entity.pedido) {

                                    if (this.entity.pedido.formaDePagamento == FormaDePagamentoEnum.Boleto) {

                                        var promiseTransacao = this.transacaoService.getTransacaoPorPedidoId(this.entity.pedidoId);
                                        promiseTransacao.then((result) => {
                                            if (result) {
                                                this.pago = result.statusDoPagamento == StatusDoPagamentoEnum.Pago;
                                                this.linkParaPagamento = result.linkParaPagamento;
                                                this.linkParaPagamentoCartao = result.linkParaPagamento.replace('bs=true','bs=false');

                                                // Seta aba do pagamento boleto
                                                if (this.linkParaPagamento) {
                                                    if (this.tipoDeUsuarioBeneficiario) {
                                                        this.pgFormas.disableTransferencia = false;
                                                        this.pgFormas.disableDepositoBancario = false;
                                                        this.pgFormas.disableCartaoCredito = false;
                                                    } else {
                                                        this.pgFormas.disableTransferencia = false;
                                                        this.pgFormas.disableDepositoBancario = false;
                                                        this.pgFormas.disableCartaoCredito = false;
                                                    }
                                                    this.activeStepPagamento = 0;
                                                }

                                                

                                            }
                                            this.fnViewContaBancaria();

                                        });
                                    }

                                    if (this.entity.pedido.formaDePagamento == FormaDePagamentoEnum.Deposito) {

                                        var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                                        input.id = this.entity.id;
                                        var promiseVenda = this.vendaService.getDadosDeposito(input);
                                        promiseVenda.then((result) => {
                                            this.pcDepositoInput = result;
                                            this.depositoBancario = result;

                                            // Seta aba do pagamento deposito
                                            if (result.comprovante) {
                                                this.pgFormas.disableTransferencia = false;
                                                if (this.tipoDeUsuarioBeneficiario) {
                                                    this.pgFormas.disableBoleto = true;
                                                    this.pgFormas.disableCartaoCredito = true;
                                                    this.pgFormas.disableDepositoBancario = result.comprovante.indexOf('data:image') > -1;
                                                } else {
                                                    this.pgFormas.disableBoleto = true;
                                                    this.pgFormas.disableDepositoBancario = true;
                                                    this.pgFormas.disableCartaoCredito = true;
                                                }

                                                this.activeStepPagamento = 1;
                                            }
                                        });
                                    }

                                    if (this.entity.pedido.formaDePagamento == FormaDePagamentoEnum.DepositoEmConta) {

                                        var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                                        input.id = this.entity.id;
                                        var promiseVenda = this.vendaService.getDadosDeposito(input);
                                        promiseVenda.then((result) => {
                                            this.pcDepositoInput = result;
                                            this.depositoBancario = result;

                                            // Seta aba do pagamento deposito bancário
                                            if (result.comprovante) {
                                                this.pgFormas.disableDepositoBancario = false;
                                                if (this.tipoDeUsuarioBeneficiario) {
                                                    this.pgFormas.disableBoleto = true;
                                                    this.pgFormas.disableCartaoCredito = true;
                                                    this.pgFormas.disableTransferencia = result.comprovante.indexOf('data:image') > -1;
                                                } else {
                                                    this.pgFormas.disableBoleto = true;
                                                    this.pgFormas.disableTransferencia = true;
                                                    this.pgFormas.disableCartaoCredito = true;
                                                }
                                                this.activeStepPagamento = 2;
                                            }
                                        });
                                    }

                                    if (this.entity.pedido.formaDePagamento == FormaDePagamentoEnum.CartaoDeCredito) {
                                        ///TODO CARTAO DE CREDITO
                                        this.fnViewContaBancaria();
                                    }

                                } else {
                                   // this.pgFormas.boleto = true;
                                    //this.pgFormas.transferencia = true;
                                   // this.pgFormas.cartaoCredito = true;
                                    //this.pgFormas.depositoBancario = true;

                                    this.fnViewContaBancaria();
                                }



                            });

                            

                            var inputDetail = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                            inputDetail.produtoId = this.entity.produtoId;
                            inputDetail.propostaId = this.entity.id;
                            this.homologacaoInput.observacaoHomologacao = this.entity.observacaoHomologacao;
                            var promiseDetail = this.produtoService.getDetalhesById(inputDetail);
                            promiseDetail.then((result) => {
                                this.planoModal = result;
                                this.valorPedido = this.planoModal.valor;
                            });

                            this.botaoSalvarVisaoAdministradora();
                            this.activeStep = 5;
                            this.disableTabSelecionarPlano = false;
                            this.disableTabProposta = false;
                            this.disableTabAceite = false;
                            this.disableTabHomologacao = false;
                            this.disableTabPagamento = false;
                            this.setSessionId();
                            break;
                        }
                }

                this.salvarContinuarDisabled = (
                    (this.activeStep == 1 && !this.selecaoDeProdutoInput.produtoDePlanoDeSaudeId) ||
                    (this.activeStep == 3 && !this.aceiteDoContratoInput.aceite) ||
                    (this.activeStep == 2 && this.activeSubStep == 7 && !this.dadosGeraisInput.aceiteDaDeclaracaoDeSaude) ||
                    (this.activeStep == 2 && this.activeSubStep == 6 && (this.dadosGeraisInput.gerenteId == 0 || this.dadosGeraisInput.supervisorId == 0)) ||
                    (this.activeStep == 3 && !this.aceiteDoContratoInput.aceiteCorretor && this.tipoDeUsuarioCorretor)
                );

                //Usado para remover o scroll do passo selecao de plano.
                //if (numPassoProposta !== PassoDaPropostaEnum.SelecionarPlano) {
                //    $(window).off('scroll', this.scrollFixedFn);
                //}

            }

            this.onChangeAditivo = (index: number, aditivo: relatorioDtos.Dtos.Global.Geral.RelatorioInput) => {
                if (!this.dadosGeraisInput.aditivos) {
                    this.dadosGeraisInput.aditivos = [];
                }
                if (this.aditivosCheckbox[index]) {
                    this.dadosGeraisInput.aditivos.push(aditivo);
                } else {
                    this.dadosGeraisInput.aditivos = this.dadosGeraisInput.aditivos.filter(adf => adf.id !== aditivo.id);
                }
            }

            this.setDadosPagamento = () => {
                this.dadosPagamento = new transacaoPagSeguroDtos.Dtos.EZPag.TransacaoPagSeguro.CheckoutInput();
                this.dadosPagamento.id = 0;
                this.dadosPagamento.pedido = new pedidoDtos.Dtos.Pedido.PedidoInput();
                this.dadosPagamento.holder = new ezpagDtos.Dtos.EZPag.Geral.HolderInput();
                this.dadosPagamento.opcaoDeParcelamento = new ezpagDtos.Dtos.EZPag.Geral.ParcelamentoInput();
                //TEMPORARIAMENTE ESTÁ FIXO COMO BOLETO
                //DEPOIS SERÁ ALTERADO PARA ACEITAR CARTÃO DE CRÉDITO TAMBÉM
                this.dadosPagamento.tipo = TipoCheckoutPagSeguroEnum.BOLETO;
            };

            this.setSessionId = () => {

                this.setDadosPagamento();
                var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                input.id = this.entity.id;
                var promise = this.transacaoPagSeguroService.criarSessao(input);
                promise.then((result) => {
                    console.log('sessionId: ' + result.id);
                    PagSeguroDirectPayment.setSessionId(result.id);
                    this.whenReady(PagSeguroDirectPayment,
                        () => {
                            this.dadosPagamento.senderHash = PagSeguroDirectPayment.getSenderHash();
                            console.log('senderHash: ' + this.dadosPagamento.senderHash);
                        });
                }).finally(() => {

                });
            };

            this.whenReady = (obj, callback) => {
                var iid = setInterval(() => {
                    if (obj.ready) {
                        callback();
                        clearInterval(iid);
                    }
                },
                    20);
            };

            this.createCardToken = () => {
                if (this.isValidForCardToken()) {
                    PagSeguroDirectPayment.createCardToken({
                        cardNumber: this.numeroCartao,
                        brand: this.brandName,
                        cvv: this.cvv,
                        expirationMonth: this.mesVencimento,
                        expirationYear: this.anoVencimento,
                        success: (result) => {
                            var card = result.card;
                            this.dadosPagamento.tokenCartao = card.token;
                        },
                        error: (result) => {
                            abp.message.error('Erro ao gerar token do cartão!', 'Erro');
                        },
                        complete: (result) => {
                            this.getInstallments();
                        }
                    });
                }
            };

            this.getBrand = () => {
                if (this.numeroCartao && this.numeroCartao.length == 16) {
                    PagSeguroDirectPayment.getBrand({
                        cardBin: this.numeroCartao,
                        success: (result) => {
                            var brand = result["brand"];
                            this.brandName = brand.name;
                            this.cardBin = brand.bin;
                            this.cvvSize = brand.cvvSize;
                            this.expirable = brand.expirable;
                            this.validationAlgorithm = brand.validationAlgorithm;
                            this.brandImage = this.brandImagePattern.replace('[brandName]', this.brandName);
                        },
                        error: (result) => {
                            abp.message.error('Erro ao buscar dados do cartão!', 'Erro');
                        }
                    });
                }
            };

            this.getInstallments = () => {
                PagSeguroDirectPayment.getInstallments({
                    amount: this.valorPedido,
                    brand: this.brandName,
                    maxInstallmentNoInterest: 2,
                    success: (result) => {
                        var installments = result.installments;
                        this.opcoesDeParcelamento = installments[this.brandName];
                    },
                    error: (result) => {
                        abp.message.error('Erro ao buscar opções de parcelamento!', 'Erro');
                    }
                });
            };

            this.isValidForCardToken = () => {
                return (this.cvv.toString().length == this.cvvSize &&
                    this.numeroCartao &&
                    this.brandName &&
                    this.cvv > 0 &&
                    this.mesVencimento > 0 &&
                    this.anoVencimento > 0);
            }

            this.realizarCheckout = (isAdm) => {
                this.saving = true;
                var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                input.id = this.entity.id;
                this.dadosPagamento.id = this.entity.id;

                



                var promiseVenda = this.vendaService.save(input);

                promiseVenda.then((result) => {
                    if (!this.dadosPagamento) {
                        this.setDadosPagamento();
                        this.setSessionId();
                    }
                    this.entity.pedidoId = result.id;
                    this.dadosPagamento.pedido.id = result.id;
                    this.dadosPagamento.pedido.clienteId = result.clienteId;

                    //if (this.dadosPagamento.tipo == TipoCheckoutPagSeguroEnum.CREDIT_CARD) {
                    //    var dadosParcelamento = this.opcaoDeParcelamento.split('_');
                    //    this.dadosPagamento.opcaoDeParcelamento.numeroDeParcelas = parseInt(dadosParcelamento[0]);
                    //    this.dadosPagamento.opcaoDeParcelamento.valorDaParcela = parseFloat(dadosParcelamento[1]);
                    //    //deve ser o mesmo numero passado no metodo getInstallments
                    //    this.dadosPagamento.opcaoDeParcelamento.numeroDeParcelasSemJuros = 2;
                    //}

                    //if (this.dadosPagamento.senderHash) {
                    //    this.finalizaCheckout(isAdm);
                    //} else {
                    //    this.whenReady(PagSeguroDirectPayment,
                    //        () => {
                    //            this.dadosPagamento.senderHash = PagSeguroDirectPayment.getSenderHash();
                    //            this.finalizaCheckout(isAdm);
                    //        });
                    //}

                    this.finalizaCheckout(isAdm);


                    //}).finally(() => {
                    //if (!isAdm)
                    //    this.realizarCheckout(true);
                }).catch((err) => {
                    this.saving = false;
                    abp.message.error(err.message, app.localize("Error"));
                    });



            }

            this.enviarComprovante = () => {
                if (this.tipoDeUsuarioBeneficiario) {

                    if (this.pcDepositoInput && this.pcDepositoInput.comprovante) {
                        this.pcDepositoInput.propostaId = this.entity.id;
                        this.pcDepositoInput.formaDePagamento = 1;
                        var promise = this.vendaService.realizaVendaProdutoPlanoDeSaudeDeposito(this.pcDepositoInput);
                        promise.then((result) => {
                            this.pcDepositoInput = result;

                            var promisePc = this.propostaDeContratacaoService.getById(this.entity.id);
                            promisePc.then((result) => {
                                this.entity = result;
                            });

                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            var promiseEmail = this.propostaDeContratacaoService.sendEmailDepositoCorretor(input);
                        });
                    } else {
                        abp.message.info('Selecione um comprovante antes de enviar um comprovante', 'Comprovante ausente');
                    }
                }
            }

            this.enviarComprovanteDepositoBancario = () => {
                if (this.tipoDeUsuarioBeneficiario) {

                    if (this.pcDepositoInput && this.pcDepositoInput.comprovante) {
                        this.pcDepositoInput.propostaId = this.entity.id;
                        this.pcDepositoInput.formaDePagamento = 4;
                        var promise = this.vendaService.realizaVendaProdutoPlanoDeSaudeDeposito(this.pcDepositoInput);
                        promise.then((result) => {
                            this.pcDepositoInput = result;

                            var promisePc = this.propostaDeContratacaoService.getById(this.entity.id);
                            promisePc.then((result) => {
                                this.entity = result;
                            });

                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            var promiseEmail = this.propostaDeContratacaoService.sendEmailDepositoCorretor(input);
                        });
                    } else {
                        abp.message.info('Selecione um comprovante antes de enviar um comprovante', 'Comprovante ausente');
                    }
                }
            }

            this.enviarComprovanteCard = () => {

                //TODO CARTÃO DE CREDITO
                alert('No momento as operações por cartão de crédito estão temporariamente indisponíveis!');
                return false;

                //if (this.tipoDeUsuarioBeneficiario) {

                //        if (this.pcDepositoInput && this.pcDepositoInput.comprovante) {
                //            this.pcDepositoInput.propostaId = this.entity.id;
                //            this.pcDepositoInput.formaDePagamento = 3;
                //            var promise = this.vendaService.realizaVendaProdutoPlanoDeSaudeDeposito(this.pcDepositoInput);
                //            promise.then((result) => {
                //                this.pcDepositoInput = result;

                //                var promisePc = this.propostaDeContratacaoService.getById(this.entity.id);
                //                promisePc.then((result) => {
                //                    this.entity = result;
                //                });

                //                var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                //                input.id = this.entity.id;
                //                var promiseEmail = this.propostaDeContratacaoService.sendEmailDepositoCorretor(input);
                //            });
                //        } else {
                //            abp.message.info('Selecione um comprovante antes de enviar um comprovante', 'Comprovante ausente');
                //        }
                //    }                
            }

            this.confirmarComprovante = () => {
                if (this.tipoDeUsuarioCorretor) {
                    var promise = this.vendaService.confirmaRecebimentoVendaProdutoPlanoDeSaudeDeposito(this.pcDepositoInput);
                    promise.then((result) => {
                        this.passoAtual = PassoDaPropostaEnum.Pagamento;
                        this.saveStep();
                    });
                }
            }

            this.removeComprovante = () => {
                if (this.pcDepositoInput) {
                    this.pcDepositoInput.comprovante = '';
                }
                if (this.depositoBancario) {
                    this.depositoBancario.comprovante = '';
                }
                this.pgFormas.disableTransferencia = false;
                this.pgFormas.disableDepositoBancario = false;
                this.pgFormas.disableCartaoCredito = false;
                this.pgFormas.disableBoleto = false;
                this.fileComprovante = null;
            }

            this.transformaConteudoComprovante = (file: any) => {
                var reader = new FileReader();

                reader.addEventListener("load", () => {
                    if (!this.pcDepositoInput) this.pcDepositoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput();
                    this.pcDepositoInput.comprovante = reader.result;
                    this.$scope.$apply();
                }, false);

                if (file) {
                    reader.readAsDataURL(file);
                    this.pgFormas.disableTransferencia = false;
                    this.pgFormas.disableBoleto = true;
                    this.pgFormas.disableDepositoBancario = true;
                    this.pgFormas.disableCartaoCredito = true;
                }
            }

            this.transformaConteudoComprovanteDepositoBancario = (file: any) => {
                var reader = new FileReader();

                reader.addEventListener("load", () => {
                    if (!this.pcDepositoInput) this.pcDepositoInput = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput();
                    this.pcDepositoInput.comprovante = reader.result;
                    this.$scope.$apply();
                }, false);

                if (file) {
                    reader.readAsDataURL(file);
                    this.pgFormas.disableTransferencia = true;
                    this.pgFormas.disableBoleto = true;
                    this.pgFormas.disableCartaoCredito = true;
                    this.pgFormas.disableDepositoBancario = false;
                }
            }

            this.atualizaStatusDaProposta = (status: StatusDaPropostaEnum) => {
                this.entity.statusDaProposta = status;
                var input = this.entity;
                var promise = this.propostaDeContratacaoService.atualizaStatusDaProposta(input);
                promise.then((result) => {

                }).catch((err) => {
                    abp.message.error(err.message, app.localize("Error"));
                });
            }

            this.realizarHomologacao = () => {
                this.homologacaoInput.tipoDeHomologacao = TipoDeHomologacaoEnum.Homologado;
                this.homologacaoInput.dataHoraDaHomologacao = new Date();
                this.disableTabSelecionarPlano = false;
                this.disableTabProposta = false;
                this.disableTabAceite = false;
                this.disableTabHomologacao = false;
                this.disableTabPagamento = false;
                this.homologar();
            }

            this.declinarHomologacao = () => {
                this.homologacaoInput.tipoDeHomologacao = TipoDeHomologacaoEnum.Declinado;
                this.homologar();
            }

            this.emExigencia = () => {
                this.homologacaoInput.tipoDeHomologacao = TipoDeHomologacaoEnum.EmExigencia;
                this.homologar();
            }

            this.homologar = () => {

                this.homologacaoInput.id = this.entity.id;

                if (this.homologacaoInput.tipoDeHomologacao == TipoDeHomologacaoEnum.Declinado) {
                    abp.message.confirm("Ao declinar a homologação, a proposta será cancelada. Esta ação não pode ser desfeita. Deseja continuar?", "Atenção", (isConfirmed) => {
                        if (isConfirmed) {
                            this.homologarInternal();
                        }
                    });
                } else {
                    this.homologarInternal();
                }
            };

            this.events.onBeforeSaveEntity = () => {
                this.entity.formaDeContratacao = FormaDeContratacaoEnum.Adesao;

                switch (this.entity.passoDaProposta) {
                    case PassoDaPropostaEnum.PreCadastro:
                        {
                            //Pessoa
                            this.entity.titular.pessoa.tipoPessoa = TipoPessoaEnum.Fisica;

                            // Documento
                            var documento = new documentoDtos.Dtos.Documento.DocumentoInput();
                            var tipoDeDocumentoInput = new tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput();
                            tipoDeDocumentoInput.tipoDeDocumento = TipoDeDocumentoEnum.Cpf;

                            var promiseDocumento = this.tipoDeDocumentoService.getByTipoFixo(tipoDeDocumentoInput);
                            promiseDocumento.then(result => {
                                documento.tipoDeDocumentoId = result.id;
                                documento.numero = this.cpfTitular;
                                this.entity.titular.pessoa.documentoPrincipal = documento;
                            })
                                .catch(result => {
                                    abp.message.error(result, "Erro");
                                });
                            //Email

                            //Celular

                            break;
                        }
                }
            }

            //Carrega os tipos de usuário e ezGridProdutoConfig
            this.tipoUsuario();

            this.ezGridVigenciaConfig();

            this.ezGridCorretoraConfig();

            this.ezSelectEstadoConfig();

            this.ezSelectGerenteConfig();

            this.ezSelectSupervisorConfig();

            this.ezSelectProfissaoConfig();

            this.ezSelectChancelaConfig();

            //this.ezAutoCompleteProfissaoConfig();

            this.ezSelectGrupoPessoaConfig();

            this.ezSelectPessoaFisicaConfig();

            //configurar o ezFile de Arquivo Documento
            this.ezFileArquivoDocumentoConfig();

            this.ezFileArquivoDeclaracaoConfig();

            this.ezSelectTipoDeLogradouroConfig();

            this.ezSelectPaisConfig();

            this.ezSelectEstadoEnderecoConfig();

            this.ezSelectCidadeConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

        }

        private homologarInternal = () => {
            var promiseHomologacao = this.propostaDeContratacaoService.savePassoHomologacao(this.homologacaoInput);
            promiseHomologacao.then((result) => {
                this.entity = result;
                this.homologacaoInput.tipoDeHomologacao = result.tipoDeHomologacao;

                if (this.homologacaoInput.tipoDeHomologacao == TipoDeHomologacaoEnum.EmExigencia) {
                    abp.message.success(app.localize("PropostaDeContratacao.EmExigenciaComSucesso"),
                        app.localize("Sucesso"));
                } else {
                    if (this.homologacaoInput.tipoDeHomologacao == TipoDeHomologacaoEnum.Homologado) {
                        var inputDetail = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                        inputDetail.produtoId = this.entity.produtoId;
                        inputDetail.propostaId = this.entity.id;

                        var promiseDetail = this.produtoService.getDetalhesById(inputDetail);
                        promiseDetail.then((result) => {
                            this.planoModal = result;
                        });
                        abp.message.success(app.localize("PropostaDeContratacao.HomologadaComSucesso"),
                            app.localize("Sucesso"));

                        var promiseEmailHomologacao = this.propostaDeContratacaoService.sendEmailHomologacaoConcluida(this.entity.id);
                        promiseEmailHomologacao.then(() => {
                        });
                    } else {
                        abp.notify.success(app.localize("PropostaDeContratacao.DeclinadaComSucesso"), "");
                        this.goToHome();
                    }
                }

            }).finally(() => {

            });
        }

        private getOnDemand(entity: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput) {
            switch (entity.passoDaProposta) {
                case PassoDaPropostaEnum.PreCadastro:
                case PassoDaPropostaEnum.SelecionarPlano:
                    this.preencherPreCadastro(entity);
                    break;
                case PassoDaPropostaEnum.DadosGerenciais:
                case PassoDaPropostaEnum.DadosProponenteTitular:
                case PassoDaPropostaEnum.DadosResponsavel:
                case PassoDaPropostaEnum.Endereco:
                case PassoDaPropostaEnum.DadosDependentes:
                case PassoDaPropostaEnum.Documentos:
                case PassoDaPropostaEnum.DadosVigencia:
                case PassoDaPropostaEnum.DadosDeclaracaoDeSaude:
                    this.preencherPreCadastro(entity);
                    this.preencherDadosGerais(entity);
                    break;
                case PassoDaPropostaEnum.AceiteDoContrato:
                    this.preencherPreCadastro(entity);
                    this.preencherDadosGerais(entity);
                    this.preencherAceiteDoContrato(entity);
                    break;
                case PassoDaPropostaEnum.Homologacao:
                    this.preencherPreCadastro(entity);
                    this.preencherDadosGerais(entity);
                    this.preencherAceiteDoContrato(entity);
                    this.preencherHomologacao(entity);
                    break;
            }
        }

        private setStepPreenchimentoDeDados(numPassoProposta: number) {
            this.botaoSalvarVisaoAdministradora();
            this.activeStep = 2;
            this.disableTabSelecionarPlano = false;
            this.disableTabProposta = false;

            if (this.entity) {
                if (this.entity.titular != null)
                    this.entity.dependentes = this.entity.titular.dependentes;

                if (this.entity.produto) {
                    this.respostas = [];
                    this.getItensDeDeclaracaoDeSaude();
                }

                if (this.entity.titular) {
                    if (!this.entity.titular.respostasDeclaracaoDeSaude && this.respostas) {
                        for (let resposta of this.respostas) {
                            var itemResposta = new respostasDto.Dtos.EZLiv.Geral.RespostaDeDeclaracaoDeSaudeInput();
                            itemResposta.beneficiarioId = this.entity.titular.id;
                            itemResposta.perguntaId = resposta.perguntaId;
                            itemResposta.pergunta = resposta.pergunta;
                            itemResposta.marcada = resposta.marcada;
                            itemResposta.anoDoEvento = resposta.anoDoEvento;
                            itemResposta.tipoDeItemDeDeclaracao = resposta.tipoDeItemDeDeclaracao;
                            itemResposta.observacao = '';
                            if (!this.entity.titular.respostasDeclaracaoDeSaude) {
                                this.entity.titular.respostasDeclaracaoDeSaude = [itemResposta];
                            } else {
                                this.entity.titular.respostasDeclaracaoDeSaude.push(itemResposta);
                            }
                        }
                    }
                }


                for (let dependente of this.entity.dependentes) {
                    if ((dependente as dependenteDto.Dtos.EZLiv.Geral.DependenteInput).pessoaFisica
                        .dataDeNascimento !=
                        null)
                        (dependente as dependenteDto.Dtos.EZLiv.Geral.DependenteInput).pessoaFisica
                            .dataDeNascimento = new Date(dependente.pessoaFisica.dataDeNascimento);

                    if (!dependente.respostasDeclaracaoDeSaude || dependente.respostasDeclaracaoDeSaude.length == 0 && this.respostas) {
                        for (let resposta of this.respostas) {
                            var itemResposta = new respostasDto.Dtos.EZLiv.Geral.RespostaDeDeclaracaoDeSaudeInput();
                            itemResposta.beneficiarioId = this.entity.titular.id;
                            itemResposta.perguntaId = resposta.perguntaId;
                            itemResposta.pergunta = resposta.pergunta;
                            itemResposta.marcada = resposta.marcada;
                            itemResposta.anoDoEvento = resposta.anoDoEvento;
                            itemResposta.tipoDeItemDeDeclaracao = resposta.tipoDeItemDeDeclaracao;
                            itemResposta.observacao = '';
                            if (!dependente.respostasDeclaracaoDeSaude) {
                                dependente.respostasDeclaracaoDeSaude = [itemResposta];
                            } else {
                                dependente.respostasDeclaracaoDeSaude.push(itemResposta);
                            }
                        }
                    }
                }

                this.botaoSalvarVisaoAdministradora();
                this.preencherDadosGerais(this.entity);
                this.PreencherDependentePorAba();
                this.setStepByPassoDaProposta(numPassoProposta);
            }
        }

        private ezSelectChancelaConfig() {
            this.requestParamsChancela = new requestParam.RequestParam.RequestParams();

            this.chancelaSelecionado = (registro) => {
                this.dadosGeraisInput.chancelaId = registro.id;
                this.entity.chancelaId = registro.id;
                return registro.nome;
            }

            this.chancelaDeselecionado = () => {
                this.dadosGeraisInput.chancelaId = null
                this.entity.chancelaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoChancela = (termoDigitadoPesquisa) => {
                var filtro = new chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectChancela = new ezSelect.EzSelect
                .EzSelect<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.chancelaService,
                this.getFiltroParaPaginacaoChancela,
                this.requestParamsChancela,
                this.$uibModal,
                this.chancelaSelecionado,
                this.chancelaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectChancela.onEzGridCreated = () => {
                this.ezSelectChancela.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    let objSession = this.$window.localStorage.getItem('keyChancela');
                    let objChancela = angular.fromJson(objSession);
                    filtro = new chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput();
                    filtro.nome = objChancela.nome;
                    return this.chancelaService.getPaginadoParaPropostaDeContratacao(filtro, requestParams);
                };

                this.ezSelectChancela.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Chancela.Nome'),
                    field: 'nome'
                });

                this.ezSelectChancela.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectChancela.on.beforeOpenModalDialog = () => {
                this.ezSelectChancela.ezGrid.getRegistros();
            };
        }

        finalizaCheckout = (isAdm) => {

            //var promisePagamento = this.transacaoPagSeguroService.realizarCheckout(this.dadosPagamento);
            var promisePagamento = this.transacaoPagSeguroService.realizarCheckout2(this.entity);

            promisePagamento.then((result) => {

                this.saving = false;
                this.linkParaPagamento = result.linkParaPagamento;
                this.linkParaPagamentoCartao = result.linkParaPagamentoCartao;
                abp.message.success(app.localize("PropostaDeContratacao.BoletoGerado"), app.localize("Sucesso"));

                var promiseSendEmailBoletoGerado = this.propostaDeContratacaoService.sendEmailBoletoGerado(this.entity.id);

                promiseSendEmailBoletoGerado.then((resultSendEmailBoletoGerado) => {
                }).finally(() => {

                });

                this.atualizaStatusDaProposta(StatusDaPropostaEnum.PendenteDePagamento);

            }).catch((err) => {
                this.saving = false;
                abp.message.error(err.message, app.localize("Error"));
            });
        }

        private ezSelectTipoDeLogradouroConfig() {
            this.requestParamsTipoDeLogradouro = new requestParam.RequestParam.RequestParams();

            this.tipoDeLogradouroSelecionado = (registro) => {
                this.dadosGeraisInput.endereco.tipoDeLogradouroId = registro.id;
                return registro.descricao;
            }

            this.tipoDeLogradouroDeselecionado = () => {
                this.dadosGeraisInput.endereco.tipoDeLogradouroId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDeLogradouro = (termoDigitadoPesquisa) => {
                var filtro = null;

                filtro = new tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.GetTipoDeLogradouroInput();
                filtro.descricao = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectTipoDeLogradouro = new ezSelect.EzSelect
                .EzSelect<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto,
                requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoTipoDeLogradouro,
                this.requestParamsTipoDeLogradouro,
                this.$uibModal,
                this.tipoDeLogradouroSelecionado,
                this.tipoDeLogradouroDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeLogradouro.onEzGridCreated = () => {
                this.ezSelectTipoDeLogradouro.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.tipoDeLogradouroService.getPaged(filtro, requestParams);
                };

                this.ezSelectTipoDeLogradouro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeLogradouro.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTipoDeLogradouro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeLogradouro.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTipoDeLogradouro.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDeLogradouro.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectTipoDeLogradouro.ezGrid.getRegistros();
                //}
            };
        }

        private ezSelectPaisConfig() {
            this.requestParamsPais = new requestParam.RequestParam.RequestParams();

            this.paisSelecionado = (registro) => {
                this.paisId = registro.id;
                return registro.nome;
            }

            this.paisDeselecionado = () => {
                this.paisId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPais = (termoDigitadoPesquisa) => {
                var filtro = null;

                filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                filtro.nacionalidade = termoDigitadoPesquisa;
                filtro.codigoISO = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPais = new ezSelect.EzSelect
                .EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoPais,
                this.requestParamsPais,
                this.$uibModal,
                this.paisSelecionado,
                this.paisDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPais.onEzGridCreated = () => {
                this.ezSelectPais.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.paisService.getPaged(filtro, requestParams);
                };

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nome'),
                    field: 'nome'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPais.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPais.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectPais.ezGrid.getRegistros();
                //}
            };
        }

        private ezSelectEstadoEnderecoConfig() {
            this.requestParamsEstadoEndereco = new requestParam.RequestParam.RequestParams();

            this.estadoEnderecoSelecionado = (registro) => {
                this.estadoEnderecoId = registro.id;
                return registro.nome;
            }

            this.estadoEnderecoDeselecionado = () => {
                this.estadoEnderecoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstadoEndereco = (termoDigitadoPesquisa) => {
                var filtro = null;
                var paisId = this.paisId;

                if (!paisId) {
                    abp.message.info(app.localize('Pais.SelecaoDoPaisNecessaria'), '');
                } else {
                    filtro = new estadoDtos.Dtos.Estado.GetEstadoByPaisInput();
                    filtro.paisId = paisId;
                    filtro.nomeEstado = termoDigitadoPesquisa;
                    filtro.siglaEstado = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectEstadoEndereco = new ezSelect.EzSelect
                .EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoEstadoEndereco,
                this.requestParamsEstadoEndereco,
                this.$uibModal,
                this.estadoEnderecoSelecionado,
                this.estadoEnderecoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstadoEndereco.onEzGridCreated = () => {
                this.ezSelectEstadoEndereco.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.estadoService.getEstadosByPaisId(filtro, requestParams);
                };

                this.ezSelectEstadoEndereco.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstadoEndereco.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEstadoEndereco.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Sigla'),
                    field: 'sigla'
                });

                this.ezSelectEstadoEndereco.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstadoEndereco.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectEstadoEndereco.ezGrid.getRegistros();
                //}
            };
        }

        private ezSelectCidadeConfig() {
            this.requestParamsCidade = new requestParam.RequestParam.RequestParams();

            this.cidadeSelecionado = (registro) => {
                this.dadosGeraisInput.endereco.cidadeId = registro.id;
                return registro.nome;
            }

            this.cidadeDeselecionado = () => {
                this.dadosGeraisInput.endereco.cidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCidade = (termoDigitadoPesquisa) => {
                var filtro = null;
                var estadoId = this.estadoEnderecoId;

                if (!estadoId) {
                    abp.message.info(app.localize('Cidade.SelecaoDoEstadoNecessaria'), '');
                } else {
                    filtro = new cidadeDtos.Dtos.Cidade.GetCidadeByEstadoInput();
                    filtro.estadoId = estadoId;
                    filtro.nomeCidade = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectCidade = new ezSelect.EzSelect
                .EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoCidade,
                this.requestParamsCidade,
                this.$uibModal,
                this.cidadeSelecionado,
                this.cidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCidade.onEzGridCreated = () => {
                this.ezSelectCidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.cidadeService.getCidadesByEstadoId(filtro, requestParams);
                };

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectCidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCidade.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectCidade.ezGrid.getRegistros();
                //}
            };
        }

        getEndereco = () => {
            if (this.dadosGeraisInput &&
                this.dadosGeraisInput.endereco &&
                this.dadosGeraisInput.endereco.cep &&
                this.dadosGeraisInput.endereco.cep.length === 8) {
                var filtro = new enderecoDtos.Dtos.Endereco.GetEnderecoByCepInput();
                filtro.cep = this.dadosGeraisInput.endereco.cep;
                var pessoaId = this.dadosGeraisInput.endereco.pessoaId;
                var promise = this.enderecoService.getEnderecoByCep(filtro);

                if (promise) {
                    promise.then(result => {
                        this.dadosGeraisInput.endereco = result;
                        this.dadosGeraisInput.endereco.pessoaId = pessoaId;
                        this.carregarEzSelects(this.dadosGeraisInput.endereco);
                    }).finally(() => {

                    });
                }
            } else {
                this.clearComponents();
            }
        };

        irPopupReceitaParaValidacao = (cpf: string, dataNascimento: string) => {
            if (!cpf || !dataNascimento) {
                abp.message.info("Para validar o CPF os campos CPF e data de nascimento devem estar preenchidos.",
                    "CPF ou Data de nascimento em branco");
            } else {

                var cpfFormatado = cpf.length === 11
                    ? cpf.replace(/(\d{3})(\d)/, "$1.$2")
                        .replace(/(\d{3})(\d)/, "$1.$2")
                        .replace(/(\d{3})(\d{1,2})$/, "$1-$2")
                    : cpf;

                window.open('https://www.receita.fazenda.gov.br/Aplicacoes/SSL/ATCTA/CPF/ConsultaSituacao/ConsultaPublica.asp?CPF=' + cpfFormatado + '&NASCIMENTO=' + dataNascimento, '_blank');
            }
        }

        formataParaDataBrasilera = (data: Date) => {
            if (data) {
                var dd = data.getDate();
                var mm = data.getMonth() + 1;
                var yyyy = data.getFullYear();

                var ddS = dd < 10 ? "0" + dd : dd;
                var mmS = mm < 10 ? "0" + mm : mm;

                return ddS + '/' + mmS + '/' + yyyy;
            }
        }

        getPessoaByCpf = () => {
            if (this.preCadastroInput &&
                this.preCadastroInput.cpf &&
                this.preCadastroInput.cpf.replace(/\D/g, "").length === 11) {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput();
                filtro.cpfPessoaFisica = this.preCadastroInput.cpf;
                var promise = this.pessoaFisicaService.getPessoaByCpf(filtro);


                if (promise) {
                    promise.then(result => {
                        if (result) {
                            this.preCadastroInput.nomeCompleto = result.nomePessoa;
                            this.preCadastroInput.dataDeNascimento = result.dataDeNascimento != null
                                ? new Date(result.dataDeNascimento.toString())
                                : new Date();
                            this.preCadastroInput.email = result.email;
                            this.preCadastroInput.celular = result.celular;
                            this.preCadastroInput.grupoPessoaId = result.grupoPessoaId;
                            if (result.grupoPessoaId &&
                                result.grupoPessoaId > 0) {
                                this.carregarGrupoPessoa(result.grupoPessoaId, '');
                            }
                        }
                    }).finally(() => {

                    });
                }
            } else {
                this.clearComponentsPreCadastro();
            }
        };

        onlyNumberKeyup = (element) => {
            element.target.value = element.target.value.replace(/\D/g, "");
        };

        getPessoaDependenteByCpf = (dependente: dependenteDto.Dtos.EZLiv.Geral.DependenteInput) => {
            if (dependente &&
                dependente.pessoaFisica &&
                dependente.pessoaFisica.cpf &&
                dependente.pessoaFisica.cpf.length === 11) {

                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput();
                filtro.cpfPessoaFisica = dependente.pessoaFisica.cpf;
                var promise = this.pessoaFisicaService.getPessoaByCpf(filtro);

                if (promise) {
                    promise.then(result => {
                        if (result) {
                            dependente.pessoaFisica.nome = result.nomePessoa;
                            dependente.pessoaFisica.dataDeNascimento = result.dataDeNascimento != null
                                ? new Date(result.dataDeNascimento.toString())
                                : new Date();
                            dependente.email = result.email;
                        }
                    }).finally(() => {

                    });
                }
            }
        };

        getPessoaByCpfResponsavel = () => {
            if (this.dadosGeraisInput &&
                this.dadosGeraisInput.cpfReponsavel &&
                this.dadosGeraisInput.cpfReponsavel.length == 11) {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput();
                filtro.cpfPessoaFisica = this.dadosGeraisInput.cpfReponsavel;
                var promise = this.pessoaFisicaService.getPessoaByCpf(filtro);


                if (promise) {
                    promise.then(result => {
                        this.dadosGeraisInput.nomeResponsavel = result.nomePessoa;
                        this.dadosGeraisInput.dataDeNascimentoReponsavel = result.dataDeNascimento != null
                            ? new Date(result.dataDeNascimento.toString())
                            : new Date();
                        this.dadosGeraisInput.email = result.email;
                        this.dadosGeraisInput.celular = result.celular;
                    }).finally(() => {

                    });
                }
            } else {
                //this.clearComponentsDadosGerais();
            }
        };

        private carregarEzSelects(endereco) {
            this.carregarPais(endereco.paisId, '');
            this.carregarCidade(endereco.cidadeId, '');
            this.carregarTipoDeLogradouro(endereco.tipoDeLogradouroId, '');
        }

        private clearComponents() {
            this.dadosGeraisInput.endereco.logradouro = '';
            this.dadosGeraisInput.endereco.bairro = '';
            this.dadosGeraisInput.endereco.complemento = '';
            this.ezSelectTipoDeLogradouro.gridSelectClear();
            this.ezSelectPais.gridSelectClear();
            this.ezSelectEstadoEndereco.gridSelectClear();
            this.ezSelectCidade.gridSelectClear();
        }

        private clearComponentsPreCadastro() {
            this.preCadastroInput.nomeCompleto = '';
            this.preCadastroInput.dataDeNascimento = null;
            this.preCadastroInput.celular = '';
            this.preCadastroInput.email = '';
            this.ezSelectGrupoPessoa.gridSelectClear();
        }

        private clearComponentsDadosGerais() {
            this.dadosGeraisInput.nomeResponsavel = '';
            this.dadosGeraisInput.dataDeNascimentoReponsavel = null;
            this.dadosGeraisInput.celular = '';
            this.dadosGeraisInput.email = '';
        }

        atualizarIdade = (entity: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput) => {
            var nascimento = new Date(entity.dataDeNascimentoProponenteTitular);
            var dataCorrete = new Date();

            var ano_atual = dataCorrete.getFullYear();
            var mes_atual = dataCorrete.getMonth() + 1;
            var dia_atual = dataCorrete.getDate();

            var ano_aniversario = nascimento.getFullYear();
            var mes_aniversario = nascimento.getMonth();
            var dia_aniversario = nascimento.getDay();

            this.idadeDoTitular = ano_atual - ano_aniversario;

            if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
                this.idadeDoTitular--;
            }
        };

        public addArquivoDocumento() {
            if (this.arquivoDocumentoInput.tipo) {
                this.ezFileArquivoDocumento.doSave();
            } else {


                abp.message.info(app.localize('PropostaDeContratacao.InformacaoPreenchimentoDoTipoEDoArquivoAntesDeEnviar'), '');
            }
        }

        public addArquivoDeclaracao() {
            this.arquivoDocumentoInput.tipo = 14;
            this.ezFileArquivoDeclaracao.doSave();
        }

        public removeArquivoDocumento = (arquivoDocumento) => {

            var promise = this.arquivoDocumentoService.delete(arquivoDocumento.id);
            promise.then((result) => {
                var index = this.entity.documentos.indexOf(arquivoDocumento);
                if (index > -1) {
                    this.entity.documentos.splice(index, 1);
                }
            }).finally(() => {

            });
        }

        private ezFileArquivoDocumentoConfig() {
            this.ezFileArquivoDocumento.loadImage = () => {

            }
            this.ezFileArquivoDocumento.getParametros = () => {
                var promise = this.parametroGlobalService.get();
                promise.then((result) => {
                    this.ezFileArquivoDocumento.setParametos(result.extensoesImagem);
                });
            }
            this.ezFileArquivoDocumento.onUploadComplete = (fileName: string) => {

                var input = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
                input.nome = fileName;
                var promise = this.arquivoDocumentoService.getArquivoByName(input);
                promise.then((result) => {
                    var add = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
                    add.tipo = result.tipo;
                    add.nome = result.nome;
                    add.id = result.id;
                    add.propostaDeContratacaoId = result.propostaDeContratacaoId;

                    angular.forEach(angular.element("input[type='file']"), function (inputElem) { angular.element(inputElem).val(null) });

                    if (!this.entity.documentos) {
                        this.entity.documentos = [];
                        this.entity.documentos.splice(0, 1, add);
                    } else {
                        this.entity.documentos.push(add);
                    }
                    this.arquivoDocumentoInput = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
                    this.ezFileArquivoDocumento.controller.arquivoId = 0;
                }).catch((err) => {

                    abp.message.error(err.data.message, app.localize("Error"))
                }).finally(() => {

                })
            }

            this.ezFileArquivoDocumento.initialize = () => {
                this.ezFileArquivoDocumento.inicializado = true;
                this.ezFileArquivoDocumento.controller.sistema = "EZLiv";
                this.ezFileArquivoDocumento.controller.autoUpload = false;
            }
            this.ezFileArquivoDocumento.onBeforeUpload = () => {

                this.ezFileArquivoDocumento.controller.tipoDocumento = this.arquivoDocumentoInput.tipo;
                this.ezFileArquivoDocumento.controller.refId = this.entity.id;
                this.arquivoDocumentoInput.nome = this.ezFileArquivoDocumento.controller.file.nome;

            }
        }

        private ezFileArquivoDeclaracaoConfig() {
            this.ezFileArquivoDeclaracao.loadImage = () => {

            }
            this.ezFileArquivoDeclaracao.getParametros = () => {
                var promise = this.parametroGlobalService.get();
                promise.then((result) => {
                    this.ezFileArquivoDeclaracao.setParametos(result.extensoesImagem);
                });
            }
            this.ezFileArquivoDeclaracao.onUploadComplete = (fileName: string) => {

                var input = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
                input.nome = fileName;
                var promise = this.arquivoDocumentoService.getArquivoByName(input);
                promise.then((result) => {
                    var add = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
                    add.tipo = result.tipo;
                    add.nome = result.nome;
                    add.id = result.id;
                    add.propostaDeContratacaoId = result.propostaDeContratacaoId;

                    angular.forEach(angular.element("input[type='file']"), function (inputElem) { angular.element(inputElem).val(null) });

                    if (!this.entity.documentos) {
                        this.entity.documentos = [];
                        this.entity.documentos.splice(0, 1, add);
                    } else {
                        this.entity.documentos.push(add);
                    }
                    this.arquivoDocumentoInput = new arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput();
                    this.ezFileArquivoDeclaracao.controller.arquivoId = 0;
                }).catch((err) => {

                    abp.message.error(err.data.message, app.localize("Error"))
                }).finally(() => {

                })
            }

            this.ezFileArquivoDeclaracao.initialize = () => {
                this.ezFileArquivoDeclaracao.inicializado = true;
                this.ezFileArquivoDeclaracao.controller.sistema = "EZLiv";
                this.ezFileArquivoDeclaracao.controller.autoUpload = false;
            }
            this.ezFileArquivoDeclaracao.onBeforeUpload = () => {

                this.ezFileArquivoDeclaracao.controller.tipoDocumento = this.arquivoDocumentoInput.tipo;
                this.ezFileArquivoDeclaracao.controller.refId = this.entity.id;
                this.arquivoDocumentoInput.nome = this.ezFileArquivoDeclaracao.controller.file.nome;

            }
        }

        private ezGridProdutoConfig() {
            this.ezGridProduto.entityService = this.produtoService;

            this.ezGridProduto.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            if (this.tipoDeUsuarioBeneficiario || this.tipoDeUsuarioCorretor) {

                this.ezGridProduto.optionsGrid.enableRowSelection = true;

                this.ezGridProduto.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                    (registro: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, row) => {
                        //seleciona o item clicado
                        this.selecaoDeProdutoInput.produtoDePlanoDeSaudeId = registro.id;
                        this.salvarContinuarDisabled = false;
                        this.entity.produtoId = registro.id;
                        row.isSelected = true;
                        //varre todos os itens do grid desmarcando o produto
                        for (var i = 0; i < this.ezGridProduto.optionsGrid.data.length; i++) {
                            var rowData = this.ezGridProduto.optionsGrid
                                .data[i] as produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto;
                            if (rowData.id != registro.id && row.isSelected) {
                                var gridRow = this.ezGridProduto.gridApi.grid.getRow(rowData);
                                gridRow.isSelected = false;
                            }
                        }
                    },
                    'Select',
                    (row) => {
                        return true;
                    })
                );
            } else
                this.ezGridProduto.optionsGrid.enableRowSelection = true;

            this.ezGridProduto.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                (registro: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, row) => {

                    registro.detailProduto = new produtoDtos.Dtos.EZLiv.Geral.GetDetailProdutoDePlanoDeSaude;
                    registro.detailProduto.descricaoAbrangencia = ez.domain.enum.abrangenciaDoPlanoEnum
                        .getItemPorValor(registro.planoDeSaude.abrangencia).descricao;
                    registro.detailProduto.descricaoTipoDePlanoAns = ez.domain.enum.planoDeSaudeAnsEnum
                        .getItemPorValor(registro.planoDeSaude.planoDeSaudeAns).descricao;
                    registro.detailProduto.descricaoAcomodacao = ez.domain.enum.acomodacaoEnum
                        .getItemPorValor(registro.planoDeSaude.acomodacao).descricao;
                    registro.detailProduto.descricaoSegmentacao = ez.domain.enum.segmentacaoAssistencialDoPlanoEnum
                        .getItemPorValor(registro.planoDeSaude.segmentacaoAssistencial).descricao;

                    registro.detailProduto.descricaoRedes = "";

                    if (this.entity) {
                        var inputDetail = new produtoDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput();
                        inputDetail.proponenteId = this.entity.titularId;
                        inputDetail.produtoDePlanoDeSaudeId = registro.id;

                        var promiseValores = this.produtoService.getValoresByContratoDetail(inputDetail);
                        promiseValores.then(result => {
                            registro.detailProduto.nomeTitular = result.titular.nome;
                            registro.detailProduto.valorTitular = result.titular.valor;
                            registro.detailProduto.dependentes = result.dependentes;
                        });
                    }

                    if (registro.especialidades != null) {
                        for (var i = 0; i < registro.especialidades.length; i++) {
                            registro.detailProduto.descricaoRedes =
                                registro.detailProduto.descricaoRedes === ""
                                    ? registro.especialidades[i].especialidade.nome
                                    : registro.detailProduto.descricaoRedes +
                                    " " +
                                    registro.especialidades[i].especialidade.nome;
                        }
                    }

                    if (registro.redeCredenciadaId > 0) {
                        var promise = this.redeCredenciadaService.getById(registro.redeCredenciadaId);
                        promise.then(result => {
                            registro.redeCredenciada.hospitaisDiferenciados = result.hospitaisDiferenciados;
                            registro.redeCredenciada.laboratoriosDiferenciados = result.laboratoriosDiferenciados;
                        });
                    }

                    this.$uibModal.open({
                        templateUrl: '/app/tenant/views/ezliv/produtoDePlanoDeSaude/detail.cshtml',
                        controller: ['$uibModalInstance', 'registro', ModalDetailController],
                        controllerAs: '$ctrl',
                        resolve: {
                            registro: () => {
                                return registro;
                            }
                        }

                    });
                },
                'Detalhar',
                (row) => {
                    return true;
                }));

            this.ezGridProduto.optionsGrid.showGridFooter = false;
            this.ezGridProduto.optionsGrid.showColumnFooter = false;
            this.ezGridProduto.optionsGrid.enablePagination = false;
            this.ezGridProduto.optionsGrid.enablePaginationControls = false;
            this.ezGridProduto.optionsGrid.enableGridMenu = false;
            this.ezGridProduto.optionsGrid.enableFiltering = false;
            this.ezGridProduto.optionsGrid.enableSorting = true;
            this.ezGridProduto.optionsGrid.enableInfiniteScroll = false;
            this.ezGridProduto.optionsGrid.flatEntityAccess = true;
            this.ezGridProduto.optionsGrid.enableHorizontalScrollbar = this.uiGridConstants.scrollbars.NEVER;
            this.ezGridProduto.optionsGrid.enableVerticalScrollbar = this.uiGridConstants.scrollbars.NEVER;
            this.ezGridProduto.optionsGrid.rowHeight = 80;

            //this.ezGridProduto.gridApi.core.on.rowsRendered = function () {
            //    $('.ui-grid-render-container').trigger($.Event('$destroy'));
            //}

            this.ezGridProduto.optionsGrid.rowTemplate =
                '<div ng-click="grid.appScope.openRowMenu($event)" ' +
                'class="ui-grid-row-ez {{grid.appScope.replaceNomeOperadora(row.entity.operadoraNome)}} plano-row inativa" data-plano-id="{{row.entity.id}}" > ' +
                '   <div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name"  class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader }"  ui-grid-cell></div> ' +
                '   <grid-details-menu>' +
                '       <div class="plano-selecionar" ng-click="grid.appScope.selecionarRegistro(row.entity)">Selecionar e Avançar</div>' +
                //'       <div class="plano-semelhantes" >Buscar Semelhantes</div>' +
                //'       <div class="plano-comparar" >Comparar</div>' +
                '       <div class="plano-informacoes" ng-click="grid.appScope.detalhesPlano(row.entity)">Mais Informações</div>' +
                '       <div class="cancelar" ng-click="grid.appScope.closeRowMenu($event); $event.stopPropagation();">Cancelar</div>' +
                '   </grid-details-menu> ' +
                '</div>';



            this.ezGridProduto.appScopeProvider.smoothScroll = (anchor) => {

            }

            //this.ezGridProduto.appScopeProvider.smoothScroll = (anchor, duration, modifier) => {
            //    // Calculate how far and how fast to scroll
            //    var modifier = modifier || 0;
            //    var startLocation = window.pageYOffset;
            //    var endLocation = anchor.offsetTop + 203;
            //    var distance = endLocation - (startLocation + modifier);
            //    var increments = distance / (duration / 16);
            //    var stopAnimation;

            //    //// Scroll the page by an increment, and check if it's time to stop
            //    //var animateScroll = function () {
            //    //    window.scrollBy(0, increments);
            //    //    stopAnimation();
            //    //};
            //    //// If Scrolling Down
            //    //if (increments >= 0) {
            //    //    // Stop animation when you reach the anchor OR the bottom of the page
            //    //    stopAnimation = function () {
            //    //        var travelled = window.pageYOffset;
            //    //        if ((travelled >= (endLocation - increments)) || ((window.innerHeight + travelled) >= document.body.offsetHeight)) {
            //    //            clearInterval(runAnimation);
            //    //        }
            //    //    };
            //    //}
            //    window.scrollBy(0, distance);
            //    // If scrolling down
            //    //if (increments >= 0) {
            //    //    // Stop animation when you reach the anchor OR the bottom of the page
            //    //    stopAnimation = function () {
            //    //        var travelled = window.pageYOffset;
            //    //        if ((travelled >= (endLocation - increments)) || ((window.innerHeight + travelled) >= document.body.offsetHeight)) {
            //    //            clearInterval(runAnimation);
            //    //        }
            //    //    };
            //    //}
            //    // Loop the animation function
            //    //var runAnimation = setInterval(animateScroll, 16);
            //};

            this.ezGridProduto.appScopeProvider.comportamentoSlickArrows = () => {

                var _carousel = document.querySelector('#carousel-operadoras');
                var _next = _carousel.querySelector('.arrow-next');
                var _prev = _carousel.querySelector('.arrow-prev');

                if (_next && !_next.classList.contains('arrow-disabled'))
                    // Clique para avançar
                    _next.addEventListener('click', (event) => {
                        if (this.ezGridProduto.appScopeProvider.operadoraSelecionada.index < this.operadoras.length) {
                            this.$scope.$apply(() => {
                                this.ezGridProduto.appScopeProvider.ativarOperadora(this.ezGridProduto.appScopeProvider.operadoraSelecionada.index + 1);
                            });
                        }
                    });

                if (_prev && !_prev.classList.contains('arrow-disabled'))
                    // Click para voltar
                    _prev.addEventListener('click', (event) => {
                        if (this.ezGridProduto.appScopeProvider.operadoraSelecionada.index > 0) {
                            this.$scope.$apply(() => {
                                this.ezGridProduto.appScopeProvider.ativarOperadora(this.ezGridProduto.appScopeProvider.operadoraSelecionada.index - 1);
                            });
                        }
                    });
            };

            this.ezGridProduto.appScopeProvider.scrollList = () => {
                var divRows = $('.ui-grid-viewport').eq(0);
                var operadorasEl = [];

                this.ezGridProduto.appScopeProvider.operadoras.forEach((item, index) => {
                    if (index <= 1) {
                        operadorasEl.push($('.ui-grid-viewport .' + item.nome.slugify()));
                    }
                });

                var timer = null;
                $(window).on('scroll', () => {
                    if (timer !== null) {
                        clearTimeout(timer);
                    }
                    timer = setTimeout(() => {
                        var bodyPos = $(window).scrollTop();
                        if (this.operadoras) {
                            this.operadoras.forEach((operadora) => {
                                if (operadora.nome) {
                                    var opEl = $('.ui-grid-viewport .' + operadora.nome.slugify()).eq(0) || false;
                                    if (opEl) {
                                        var row = $(opEl).parent().parent();
                                        if ($('.ui-grid-viewport .' + operadora.nome.slugify()).eq(0).parent().parent().offset()) {
                                            var rowPos = $('.ui-grid-viewport .' + operadora.nome.slugify()).eq(0).parent()
                                                .parent().offset().top
                                                ? $('.ui-grid-viewport .' + operadora.nome.slugify()).eq(0).parent()
                                                    .parent().offset().top
                                                : null;
                                            var secondRow = $(operadorasEl).eq(1).parent().parent();
                                            if ((bodyPos + 250) >= rowPos) {
                                                this.$scope.$apply(() => {
                                                    this.ezGridProduto.appScopeProvider
                                                        .ativarOperadora(operadora.index, operadora.nome, false);
                                                });
                                            } else if (bodyPos <= 250) {
                                                this.$scope.$apply(() => {
                                                    this.ezGridProduto.appScopeProvider
                                                        .ativarOperadora(this.operadoras[0].index,
                                                        this.operadoras[0].nome,
                                                        false);
                                                });
                                            }
                                        }
                                    } // fecha if opEl
                                } else {
                                    return;
                                }
                            }); // fecha foreach operadoras 
                        } // fecha if operadoras

                    }, 350); // fecha timer
                }); // fecha evento de scroll
            }; // fecha ScrollList;




            this.ezGridProduto.appScopeProvider.scrollToOperadora = (seletor, primeiraExecucaoAbaSelecaoDePlano) => {
                if (!primeiraExecucaoAbaSelecaoDePlano) {
                    var divRows = document.querySelector('.ui-grid-viewport');
                    var rowEl = document.querySelector('.ui-grid-viewport .' + seletor);
                    var _dif = $(window).scrollTop();
                    //var modifier = $('body').hasClass('top-fixed') ? +180 : ;

                    if ($('body').hasClass('scroll-started')) {
                        if (!$('body').hasClass('top-fixed')) {
                            $('html, body').animate({
                                scrollTop: 461
                            }, 300);
                        }
                    }

                    if ($('body').hasClass('top-fixed')) {
                        var _rowTop = $('.plano-row.' + seletor).eq(0).parent().parent().offset().top;
                        _rowTop = _rowTop - 178;
                    } else {
                        var _rowTop = $('.plano-row.' + seletor).eq(0).parent().parent().offset().top;
                        _rowTop = _rowTop - 356;
                    }

                    if ($('body').hasClass('scroll-started')) {
                        $('html, body').animate({
                            scrollTop: _rowTop
                        }, 300);
                    }

                    $('body').addClass('scroll-started');
                }
            };

            this.ezGridProduto.appScopeProvider.ativarOperadora = (index, operadoraNome, run, primeiraExecucaoAbaSelecaoDePlano) => {
                var _run = run === false ? false : true;
                var _operadoraNome = operadoraNome;
                var _carousel = document.querySelector('#carousel-operadoras');
                var _next = _carousel.querySelector('.arrow-next');
                var _prev = _carousel.querySelector('.arrow-prev');

                var operadoras = _carousel.querySelectorAll('.operadora-item');

                for (let i in operadoras) {
                    if ($(operadoras[i]).length > 0) {
                        $(operadoras[i]).removeClass('selecionada');
                    }
                }

                var operadora = _carousel.querySelector('.' + this.ezGridProduto.appScopeProvider.operadoras[index].nome.slugify());

                this.ezGridProduto.appScopeProvider.operadoraSelecionada = this.ezGridProduto.appScopeProvider.operadoras[index];

                $(operadora).addClass('selecionada');

                var _grid = document.querySelector('.grid');
                var _rows = _grid.querySelectorAll('.ui-grid-row-ez');

                for (let i in _rows) {
                    $(_rows[i]).removeClass('inativa');
                    $(_rows[i]).addClass('inativa');
                }

                _rows = _grid.querySelectorAll('.' + this.ezGridProduto.appScopeProvider.replaceNomeOperadora(this.ezGridProduto.appScopeProvider.operadoraSelecionada.nome));

                for (let i in _rows) {
                    $(_rows[i]).removeClass('inativa');
                }

                if (_run) {
                    this.ezGridProduto.appScopeProvider.scrollToOperadora(this.ezGridProduto.appScopeProvider.operadoras[index].nome.slugify(), primeiraExecucaoAbaSelecaoDePlano);
                }

                //if (index == 0) {
                //    if (_prev) _prev.classList.add('arrow-disabled');
                //    if (_next) {
                //        if (_next.classList.contains('arrow-disabled'))
                //            $(_next).removeClass('.arrow-disabled');
                //    }
                //} else if (index >= this.ezGridProduto.appScopeProvider.operadoras.length - 1) {
                //    if (_next) _next.classList.add('arrow-disabled');
                //    if (_prev) {
                //        if (_prev.classList.contains('arrow-disabled'))
                //            $(_prev).removeClass('.arrow-disabled');
                //    }
                //} else {
                //    if (_prev) _prev.classList.remove('arrow-disabled');
                //    if (_next) _next.classList.remove('arrow-disabled');
                //}
            };



            this.ezGridProduto.appScopeProvider.openRowMenu = (event) => {
                var _row = angular.element(event.currentTarget); this.selecaoDeProdutoInput.id = this.entity.id;
                if (_row) {
                    var planoId = parseInt($(_row)[0].dataset['planoId']);
                    this.entity.produtoId = planoId;
                    //this.salvarContinuarDisabled = false;
                }
                setTimeout(this.ezGridProduto.appScopeProvider.closeRowMenu(event), 4000);
                _row.find('row-fake-select').addClass('active');
                _row.find('grid-details-menu').addClass('active');
            };

            this.ezGridProduto.appScopeProvider.closeRowMenu = (event) => {
                var detailsMenu = document.querySelectorAll('grid-details-menu');
                for (let i in detailsMenu) {
                    if (detailsMenu[i].classList && detailsMenu[i].classList.length > 0)
                        detailsMenu[i].classList.remove('active');
                }

                var fakeSelects = document.querySelectorAll('row-fake-select');
                for (let i in fakeSelects) {
                    if (fakeSelects[i].classList && fakeSelects[i].classList.length > 0)
                        fakeSelects[i].classList.remove('active');
                }
            }

            this.ezGridProduto.appScopeProvider.selecionarRegistro = (registro) => {
                var promisePermissao = this.permissaoDeVendaDePlanoDeSaudeParaCorretoraService
                    .permissoesDeCorretora(registro.id);

                promisePermissao.then((result) => {
                    if (result.temCorretoras == true) {
                        var input = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                        input.produtoId = registro.id;
                        input.propostaId = 0;
                        this.ezGridCorretora.serviceGetCallback = () => {
                            var inputProduto = new produtoDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput;
                            inputProduto.produtoDePlanoDeSaudeId = this.entity.produtoId;
                            inputProduto.proponenteId = this.entity.titularId;
                            return this.permissaoDeVendaDePlanoDeSaudeParaCorretoraService.getCorretorasByProduto(inputProduto);
                        };

                        this.ezGridCorretora.getRegistros();
                        var promise = this.produtoService.getDetalhesById(input);
                        promise.then((result) => {
                            this.planoModal = result;
                            this.planoModal.valor = registro.valor;
                            this.modalStatus = true;
                            this.modalCorretoraStatus = true;
                            this.modalCorretoraInstance = this.$uibModal.open({
                                templateUrl:
                                '~/App/common/views/modals/detalhesCorretoraProdutoDePlanoDeSaude/detalhesCorretoraProdutoDePlanoDeSaudeModal.cshtml',
                                controller: 'common.views.modals.detalhesCorretoraPlano as $ctrl',
                                backdrop: 'static',
                                scope: this.$scope
                            });
                        });

                        this.selecaoDeProdutoInput.produtoDePlanoDeSaudeId = registro.id;
                        this.salvarContinuarDisabled = false;
                        this.entity.produtoId = registro.id;
                    } else {
                        this.selecaoDeProdutoInput.corretoraId = result.corretora.id;
                        this.selecaoDeProdutoInput.produtoDePlanoDeSaudeId = registro.id;
                        this.salvarContinuarDisabled = false;
                        this.entity.produtoId = registro.id;
                        this.saveStep();
                    }
                });


            };



            this.ezGridProduto.appScopeProvider.detalhesPlano = (registro: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => {
                var input = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                input.produtoId = registro.id;
                input.propostaId = 0;
                var promise = this.produtoService.getDetalhesById(input);
                promise.then((result) => {
                    this.planoModal = result;
                    this.planoModal.valor = registro.valor;
                    this.modalStatus = true;
                    this.modalInstance = this.$uibModal.open({
                        templateUrl: '~/App/common/views/modals/detalhesProdutoDePlanoDeSaude/detalhesProdutoDePlanoDeSaudeModal.cshtml',
                        controller: 'common.views.modals.detalhesPlano as $ctrl',
                        backdrop: 'static',
                        scope: this.$scope
                        
                    });
                });
            }

            this.closeModal = () => {
                this.modalStatus = false;
                this.modalInstance.dismiss();
            }

            //this.destroyGridEvent = () => {
            //    $('.ui-grid-render-container').trigger($.Event('$destroy'));
            //}


            this.ezGridProduto.appScopeProvider.operadoras = this.operadoras;

            // Definir as colunas
            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: '',
                field: 'id',
                cellClass: 'plano-select',
                headerCellClass: 'plano-select header',
                width: 35,
                enableColumnMenu: false,
                cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope"><row-fake-select></row-fake-select></div>',
                sort: {
                    priority: 1
                }
            });

            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Nome'),
                field: 'nome',
                cellClass: 'plano-nome',
                headerCellClass: 'plano-nome header',
                width: 310,
                cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope">{{ row.entity.nome }}</div>',
                sort: {
                    priority: 1
                }
            });

            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoPlanoDeSaude.Administradora'),
                field: 'administradoraNomePessoa',
                cellClass: 'administradora-nome',
                headerCellClass: 'administradora-nome header',
                sort: {
                    priority: 1
                }
            });

            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: app.localize('PlanoDeSaude.Acomodacao'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity.planoDeSaude, "acomodacaoEnum", "acomodacao")}}\
                </div>',
                cellClass: 'plano-acomodacao',
                headerCellClass: 'plano-acomodacao header',
                sort: {
                    priority: 1
                }
            });

            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: app.localize('PlanoDeSaude.Abrangencia'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity.planoDeSaude, "abrangenciaDoPlanoEnum", "abrangencia")}}\
                </div>',
                cellClass: 'plano-abrangencia',
                headerCellClass: 'plano-abrangencia header',
                sort: {
                    priority: 1
                }
            });


            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Valor'),
                field: 'valor',
                cellClass: 'plano-valor',
                headerCellClass: 'plano-valor header',
                cellTemplate: '<div class="ui-grid-cell-contents">{{ row.entity.valor | currency:"R$":2 }}</div>',
                sort: {
                    priority: 1
                }
            });

            this.ezGridProduto.optionsGrid.columnDefs.push({
                name: 'Detalhes',
                field: 'operadoraId',
                cellClass: 'plano-detalhes',
                headerCellClass: 'plano-detalhes header',
                cellTemplate: '<a class="ui-grid-cell-contents ui-grid-link" ng-click="grid.appScope.detalhesPlano(row.entity); $event.stopPropagation()">+ Detalhes</a>',
                sort: {
                    priority: 1
                }
            });

            this.ezGridProduto.showPortletTitle = false;
            this.ezGridProduto.isGridPlano = true;

            this.ezGridProduto.appScopeProvider.operadoras = this.operadoras;

            ezFixes.uiGrid.uiTab.registerFix();
        }

        private ezGridCorretoraConfig() {
            this.ezGridCorretora.entityService = this.corretoraService;

            this.ezGridCorretora.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            this.ezGridCorretora.optionsGrid.enableRowSelection = true;
            this.ezGridCorretora.optionsGrid.enablePagination = false;
            this.ezGridCorretora.optionsGrid.enableHorizontalScrollbar = this.uiGridConstants.scrollbars.NEVER;
            this.ezGridCorretora.optionsGrid.enableVerticalScrollbar = this.uiGridConstants.scrollbars.NEVER;

            this.ezGridCorretora.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                (registro: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, row) => {
                    //seleciona o item clicado
                    this.selecaoDeProdutoInput.corretoraId = registro.id;
                    this.modalCorretoraStatus = false;
                    this.modalCorretoraInstance.dismiss();
                    this.saveStep();

                    //varre todos os itens do grid desmarcando o produto
                    for (var i = 0; i < this.ezGridCorretora.optionsGrid.data.length; i++) {
                        var rowData = this.ezGridCorretora.optionsGrid
                            .data[i] as corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto;
                        var gridRow = this.ezGridCorretora.gridApi.grid.getRow(rowData);
                        gridRow.isSelected = rowData.id == registro.id;
                    }
                },
                'Select',
                (row) => {
                    return true;
                })
            );

            this.ezGridCorretora.optionsGrid.columnDefs.push({
                name: app.localize('Corretora.Nome'),
                field: 'nome',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.ezGridCorretora.optionsGrid.columnDefs.push({
                name: app.localize('Produto.Valor'),
                field: 'produtoDePlanoDeSaude.valor'
            });

            ezFixes.uiGrid.uiTab.registerFix();
        }

        private ezGridVigenciaConfig() {
            this.ezGridVigencia.entityService = this.vigenciaService;

            this.ezGridVigencia.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            this.ezGridVigencia.optionsGrid.enableRowSelection = true;
            this.ezGridVigencia.optionsGrid.enablePagination = false;
            this.ezGridVigencia.optionsGrid.enableHorizontalScrollbar = this.uiGridConstants.scrollbars.NEVER;
            this.ezGridVigencia.optionsGrid.enableVerticalScrollbar = this.uiGridConstants.scrollbars.NEVER;

            this.ezGridVigencia.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                (registro: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto, row) => {
                    //seleciona o item clicado
                    this.dadosGeraisInput.vigenciaId = registro.id;
                    this.setInicioVigencia(registro);

                    //varre todos os itens do grid desmarcando o produto
                    for (var i = 0; i < this.ezGridVigencia.optionsGrid.data.length; i++) {
                        var rowData = this.ezGridVigencia.optionsGrid
                            .data[i] as vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto;
                        var gridRow = this.ezGridVigencia.gridApi.grid.getRow(rowData);
                        gridRow.isSelected = rowData.id == registro.id;
                    }
                },
                'Select',
                (row) => {
                    return true;
                })
            );

            this.ezGridVigencia.optionsGrid.columnDefs.push({
                name: app.localize('Vigencia.DataDeVigencia'),
                field: 'dataDeVigencia',
                width: 220,
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.ezGridVigencia.optionsGrid.columnDefs.push({
                name: app.localize('Vigencia.DataDePropostaInicial'),
                field: 'dataInicialDaProposta'
            });

            this.ezGridVigencia.optionsGrid.columnDefs.push({
                name: app.localize('Vigencia.DataDePropostaFinal'),
                field: 'dataFinalDaProposta'
            });

            this.ezGridVigencia.optionsGrid.columnDefs.push({
                name: app.localize('Vigencia.DataDeFechamento'),
                field: 'dataDeFechamento'
            });
            ezFixes.uiGrid.uiTab.registerFix();
        }

        private ezSelectEstadoConfig() {
            this.requestParamsEstado = new requestParam.RequestParam.RequestParams();

            this.estadoSelecionado = (registro) => {
                this.preCadastroInput.estadoId = registro.id;
                return registro.nome;
            }

            this.estadoDeselecionado = () => {
                this.estadoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstado = (termoDigitadoPesquisa) => {
                var filtro = null;
                var paisId = 1;

                if (!paisId) {
                    abp.message.info(app.localize('Pais.SelecaoDoPaisNecessaria'), '');
                } else {
                    filtro = new estadoDtos.Dtos.Estado.GetEstadoByPaisInput();
                    filtro.paisId = paisId;
                    filtro.nomeEstado = termoDigitadoPesquisa;
                    filtro.siglaEstado = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectEstado = new ezSelect.EzSelect
                .EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoEstado,
                this.requestParamsEstado,
                this.$uibModal,
                this.estadoSelecionado,
                this.estadoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstado.onEzGridCreated = () => {
                this.ezSelectEstado.ezGrid.serviceGetCallback = (filtro, requestParams) => {

                    return this.estadoService.getPaged(filtro, requestParams);
                };

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Sigla'),
                    field: 'sigla'
                });

                this.ezSelectEstado.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstado.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectEstado.ezGrid.getRegistros();
                //}
            };
        }

        private ezSelectGerenteConfig() {
            this.requestParamsGerente = new requestParam.RequestParam.RequestParams();

            this.gerenteSelecionado = (registro) => {
                this.dadosGeraisInput.gerenteId = registro.id;
                return registro.pessoa.nomePessoa;
            }

            this.gerenteDeselecionado = () => {
                this.dadosGeraisInput.gerenteId = 0;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoGerente = (termoDigitadoPesquisa) => {
                var filtro = new gerenteDtos.Dtos.EZLiv.Geral.GetGerenteCorretoraInput();
                filtro.corretoraId = this.entity.corretoraId;
                return filtro;
            }

            this.ezSelectGerente = new ezSelect.EzSelect
                .EzSelect<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.gerenteService,
                this.getFiltroParaPaginacaoGerente,
                this.requestParamsGerente,
                this.$uibModal,
                this.gerenteSelecionado,
                this.gerenteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGerente.onEzGridCreated = () => {
                this.ezSelectGerente.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    filtro = new gerenteDtos.Dtos.EZLiv.Geral.GetGerenteCorretoraInput();
                    filtro.corretoraId = this.entity.corretoraId;
                    return this.gerenteService.getGerentesByCorretoraId(filtro, requestParams);
                };

                this.ezSelectGerente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Gerente.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectGerente.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGerente.on.beforeOpenModalDialog = () => {
                this.ezSelectGerente.ezGrid.getRegistros();
            };
        }

        private ezSelectSupervisorConfig() {
            this.requestParamsSupervisor = new requestParam.RequestParam.RequestParams();

            this.supervisorSelecionado = (registro) => {
                this.dadosGeraisInput.supervisorId = registro.id;
                return registro.pessoa.nomePessoa;
            }

            this.supervisorDeselecionado = () => {
                this.dadosGeraisInput.supervisorId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoSupervisor = (termoDigitadoPesquisa) => {
                var filtro = new supervisorDtos.Dtos.EZLiv.Geral.GetSupervisorCorretoraInput();
                filtro.corretoraId = this.entity.corretoraId;
                return filtro;
            }

            this.ezSelectSupervisor = new ezSelect.EzSelect
                .EzSelect<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.supervisorService,
                this.getFiltroParaPaginacaoSupervisor,
                this.requestParamsSupervisor,
                this.$uibModal,
                this.supervisorSelecionado,
                this.supervisorDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSupervisor.onEzGridCreated = () => {
                this.ezSelectSupervisor.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    filtro = new supervisorDtos.Dtos.EZLiv.Geral.GetSupervisorCorretoraInput();
                    filtro.corretoraId = this.entity.corretoraId;
                    return this.supervisorService.getSupervisoresByCorretoraId(filtro, requestParams);
                };

                this.ezSelectSupervisor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Supervisor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectSupervisor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSupervisor.on.beforeOpenModalDialog = () => {
                this.ezSelectSupervisor.ezGrid.getRegistros();
            };
        }

        private ezSelectProfissaoConfig() {
            this.requestParamsProfissao = new requestParam.RequestParam.RequestParams();

            this.profissaoSelecionado = (registro) => {

                this.preCadastroInput.profissaoId = registro.id;

                // Usado para preencher a entidade quando retorna somente uma relacionado a profissão. 
                // NO MOMENTO NÃO ESSE FILTRO NÃO ESTÁ SENDO USADO 
                //this.unicaEntidade = false;
                //var filtro = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                //filtro.id = registro.id;
                //var requestParams = new requestParam.RequestParam.RequestParams();
                //var promiseEntidade = this.entidadeService.getByProfissaoId(filtro, requestParams);
                //promiseEntidade.then((result) => {
                //    if (result.items.length == 1) {
                //        var entidade = result.items[0];
                //        this.preCadastroInput.associacaoId = entidade.id;
                //        this.ezSelectEntidade.setInputText(entidade.pessoaJuridica.nomeFantasia);
                //        this.unicaEntidade = true;
                //    }
                //});

                return registro.titulo;
            }

            this.profissaoDeselecionado = () => {
                this.preCadastroInput.profissaoId = null;
                //this.preCadastroInput.associacaoId = null;
                //this.unicaEntidade = false;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoProfissao = (termoDigitadoPesquisa) => {
                var filtro = new profissaoDtos.Dtos.Profissao.GetProfissaoInput();
                filtro.titulo = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProfissao = new ezSelect.EzSelect
                .EzSelect<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.profissaoService,
                this.getFiltroParaPaginacaoProfissao,
                this.requestParamsEstado,
                this.$uibModal,
                this.profissaoSelecionado,
                this.profissaoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProfissao.onEzGridCreated = () => {
                this.ezSelectProfissao.ezGrid.optionsGrid.enableColumnResizing = true;
                this.ezSelectProfissao.ezGrid.optionsGrid.enablePagination = true;
                this.ezSelectProfissao.ezGrid.optionsGrid.enableHorizontalScrollbar = this.uiGridConstants.scrollbars.NEVER;
                this.ezSelectProfissao.ezGrid.optionsGrid.enableVerticalScrollbar = this.uiGridConstants.scrollbars.NEVER;

                this.ezSelectProfissao.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.profissaoService.getPaged(filtro, requestParams);
                    //return this.profissaoService.getPagedWithProduto(filtro, requestParams);
                };

                this.ezSelectProfissao.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Profissao.Titulo'),
                    field: 'titulo'
                });

                this.ezSelectProfissao.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Profissao.Codigo'),
                    field: 'codigo'
                });

                this.ezSelectProfissao.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectProfissao.on.beforeOpenModalDialog = () => {
                this.ezSelectProfissao.ezGrid.getRegistros();
            };
        }

        private ezSelectGrupoPessoaConfig() {
            this.requestParamsGrupoPessoa = new requestParam.RequestParam.RequestParams();

            this.grupoPessoaSelecionado = (registro) => {
                this.preCadastroInput.grupoPessoaId = registro.id;
                return registro.nome;
            }

            this.grupoPessoaDeselecionado = () => {
                this.preCadastroInput.grupoPessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoGrupoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput();
                filtro.grupoPessoaId = this.preCadastroInput.grupoPessoaId;
                filtro.tipoDePessoa = 1;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGrupoPessoa = new ezSelect.EzSelect
                .EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.grupoPessoaService,
                this.getFiltroParaPaginacaoGrupoPessoa,
                this.requestParamsGrupoPessoa,
                this.$uibModal,
                this.grupoPessoaSelecionado,
                this.grupoPessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGrupoPessoa.onEzGridCreated = () => {
                this.ezSelectGrupoPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.grupoPessoaService.getGruposPessoaExceptForId(filtro, requestParams);
                };

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Nome'),
                    field: 'nome'
                });

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectGrupoPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGrupoPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectGrupoPessoa.ezGrid.getRegistros();
            };
        }

        private tipoUsuario() {
            //Por padrão todos os campos são readonly
            this.eParaSerReadOnly = true;
            var promise = this.userService.getTipoDeUsuarioByUsuarioLogado();
            promise.then((result) => {

                this.tipoDeUsuarioAdministradora = result.tipoDeUsuario == 0 ? true : false; // para admin
                this.tipoDeUsuarioCorretor = result.tipoDeUsuario === TipoDeUsuarioEnum.Corretor;
                this.tipoDeUsuarioBeneficiario = result.tipoDeUsuario === TipoDeUsuarioEnum.Beneficiario;
                this.tipoDeUsuarioAdministradora = result.tipoDeUsuario === TipoDeUsuarioEnum.Administradora;
                this.tipoDeUsuarioCorretora = result.tipoDeUsuario === TipoDeUsuarioEnum.Corretora;

                //Somente corretores e beneficiários podem mexer na proposta
                if (this.tipoDeUsuarioCorretor || this.tipoDeUsuarioBeneficiario) {
                    this.eParaSerReadOnly = false;
                }

                if (this.eParaSerReadOnly)
                    this.salvarContinuarVisible = false;


                var promiseOperadoras = this.operadoraService.getAll();
                promiseOperadoras.then((operadoras) => {

                    this.ezGridProduto.on.onCompletedGetRegistros = () => {

                        var addPropsParaOperadorasNaSelecaoDePlano = (operadora: any) => {
                            if (operadora) {
                                operadora["selecionavel"] = "selecionavel";
                            }
                            return operadora;
                        }

                        var produtos = (this.ezGridProduto.optionsGrid.data as Array<produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>);

                        var operadorasIdsExistenteNaSelecaoDePlano: Array<number> = [];
                        var operadorasParaCarrousel: Array<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto> = [];

                        produtos.forEach((produto) => {
                            var i = operadorasIdsExistenteNaSelecaoDePlano.indexOf(produto.operadoraId);
                            if (i === -1) {
                                operadorasIdsExistenteNaSelecaoDePlano.push(produto.operadoraId);
                            }
                        });

                        var operadorasForaDaSelecaoDePlano = operadoras.items.filter((operadora) => {
                            var i = operadorasIdsExistenteNaSelecaoDePlano.indexOf(operadora.id);
                            return i === -1;
                        });

                        var operadorasExistentesNaSelecaoDePlano = operadoras.items.filter((operadora) => {
                            var i = operadorasIdsExistenteNaSelecaoDePlano.indexOf(operadora.id);
                            return i > -1;
                        });

                        operadorasExistentesNaSelecaoDePlano.forEach((operadora) => {
                            var operadoraComPropDinamicas = addPropsParaOperadorasNaSelecaoDePlano(operadora);
                            operadorasParaCarrousel.push(operadoraComPropDinamicas);
                        });

                        operadorasForaDaSelecaoDePlano.forEach((operadora) => {
                            operadorasParaCarrousel.push(operadora);
                        });

                        this.operadoras = operadorasParaCarrousel;

                        var slickConfig = {
                            dots: false,
                            arrows: false,
                            autoplay: false,
                            initialSlide: 0,
                            infinite: false,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            enable: true,
                            centerMode: false,
                            centerPadding: '0px',
                            focusOnSelect: false,
                            draggable: true,
                            prevArrow: '.arrow-prev',
                            nextArrow: '.arrow-next',
                            slide: '.operadora-item',
                            responsive: {
                                breakpoint: 900,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: false,
                                    dots: false
                                }
                            }
                        };
                        setTimeout(() => {
                            if (!$('#operadora-list').hasClass('slick-initialized')) {
                                if ($('ul > li.uib-tab.nav-item.pt-item.pristine.uib-tab.nav-item.pt-item.pristine.completo.active > a').text() == "Seleção de plano") {
                                    setTimeout(() => { this.selectTab(20, false) }, 1000);
                                }
                                $('#operadora-list').on('init', () => {
                                    this.ezGridProduto.appScopeProvider.comportamentoSlickArrows();
                                    this.ezGridProduto.appScopeProvider.ativarOperadora(0, null, null, true);
                                    this.ezGridProduto.appScopeProvider.scrollList();
                                });
                                if ($('#operadora-list').length > 0) {
                                    $('#operadora-list').slick(slickConfig);
                                }
                            } else {
                                if ($('#operadora-list').length > 0) {
                                    //$('#operadora-list').slick('unslick');
                                    $('#operadora-list').on('init', () => {
                                        this.ezGridProduto.appScopeProvider.comportamentoSlickArrows();
                                        this.ezGridProduto.appScopeProvider.ativarOperadora(0, null, null, true);
                                        this.ezGridProduto.appScopeProvider.scrollList();
                                    });
                                    if ($('#operadora-list').length > 0) {
                                        $('#operadora-list').slick(slickConfig);
                                    }
                                }
                            }
                        });
                        this.ezGridProdutoConfig();
                    }
                });
            });

        }

        private setInicioVigencia(vigencia) {

            var promise = this.propostaDeContratacaoService.getDataInicioDeVigencia(vigencia);

            if (promise) {
                promise.then(result => {
                    var data = new Date(result.inicioDaVigencia.toString());
                    data.setHours(data.getHours() + 3);
                    this.dadosGeraisInput.inicioDeVigencia = data;
                }).finally(() => {

                });
            }
        }

        private ezSelectPessoaFisicaConfig() {

            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();


            this.pessoaSelecionado = (registro) => {
                var promise = this.pessoaFisicaService.getById(registro.id);
                promise.then(result => {
                    this.entity.titular = new proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput();
                    //this.entity.titular.id = registro.id;
                    this.entity.titular.id = 0;
                    this.entity.estadoId = 0;
                    this.entity.profissaoId = 0;


                    //Pessoa Fisica
                    this.entity.titular.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput();
                    this.entity.titular.pessoaFisica.id = registro.id;
                    this.entity.titular.pessoaFisica.nome = result.nome;
                    this.entity.titular.pessoaFisica.cpfId = registro.cpfId;
                    this.entity.titular.pessoaFisica.cpf = registro.cpf;
                    this.entity.titular.pessoaFisica
                        .dataDeNascimento = result.dataDeNascimento != null
                            ? new Date(result.dataDeNascimento.toString())
                            : new Date();
                    this.entity.titular.pessoaFisica.grupoPessoaId = registro.grupoPessoaId;

                    //Documento
                    this.entity.titular.pessoaFisica.documentoPrincipal = new documentoDtos.Dtos.Documento
                        .DocumentoInput();
                    this.entity.titular.pessoaFisica.documentoPrincipal.numero = registro.cpf;
                    this.entity.titular.pessoaFisica.documentoPrincipal.id = registro.cpfId;
                    this.entity.titular.pessoaFisica.documentoPrincipal.pessoaId = registro.id;
                    this.entity.titular.pessoaFisica.documentoPrincipal.tipoDeDocumentoId = TipoDeDocumentoEnum.Cpf;

                    // Email
                    var emailInput = new enderecoEletronicoDtos.Dtos.EnderecoEletronico.GetEnderecoEletronicoInput();
                    emailInput.pessoaId = registro.id;

                    var promiseEmail = this.enderecoEletronicoService.getByPessoaId(emailInput.pessoaId);
                    promiseEmail.then(result => {
                        this.entity.titular.pessoaFisica.emailPrincipal = new enderecoEletronicoDtos.Dtos
                            .EnderecoEletronico.EnderecoEletronicoInput;
                        this.entity.titular.pessoaFisica.emailPrincipal = result;
                    });

                    //Telefone Celular
                    var telefoneInput = new telefoneDtos.Dtos.Telefone.GetTelefoneInput();
                    telefoneInput.pessoaId = registro.id;
                    var promiseTelefone = this.telefoneService.getByPessoaId(telefoneInput.pessoaId);

                    promiseTelefone.then((result) => {
                        //PreCadastro
                        this.entity.titular.pessoaFisica.telefoneCelular = new telefoneDtos.Dtos.Telefone
                            .TelefoneInput();
                        this.entity.titular.pessoaFisica.telefoneCelular = result;
                    }).finally(() => {
                        this.preencherPreCadastro(this.entity);
                        this.ezSelectPessoa.setInputText(registro.nome);

                    });
                });


                return registro.nome;
            }

            this.pessoaDeselecionado = () => {
                this.preCadastroInput.titularId = null;
                this.preCadastroInput.grupoPessoaId = null;
                this.preCadastroInput.nomeCompleto = "";
                this.preCadastroInput.cpf = "";
                this.preCadastroInput.cpfId = null;
                this.preCadastroInput.dataDeNascimento = new Date();
                this.preCadastroInput.email = "";
                this.preCadastroInput.emailId = null;
                this.preCadastroInput.celulaId = null;
                this.preCadastroInput.celular = "";
                this.preCadastroInput.profissaoId = null;
                this.preCadastroInput.estadoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput();
                filtro.cpfPessoaFisica = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect
                .EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaByCPFListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaFisicaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    //return this.pessoaFisicaService.getPessoaExceptForId (filtro, requestParams);
                    //if (filtro.cpf) {
                    return this.pessoaFisicaService.getPagedbyCPF(filtro, requestParams);
                    //}
                };

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.Nome'),
                    field: 'nome'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.DocumentoPrincipal.Numero'),
                    field: 'cpf'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            //this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
            //    this.ezSelectPessoa.ezGrid.getRegistros();
            //};
        }

        public saveButtonText() {
            if (this.activeStep != 4 && this.activeStep != 6) {
                return "Salvar e continuar";
            } else {
                return "Salvar";
            }
        }

        public cancelText() {
            if (this.activeStep == 6) {
                this.salvarContinuarVisible = false;
                return "Fechar";
            } else {
                return "Cancelar";
            }
        }

        public cancelarContratacao() {

            abp.message.confirm(
                'Cancelamento de Contratação', 'Deseja realmente cancelar a contratação?',
                isConfirmed => {
                    if (isConfirmed) {
                        if (this.entity.id) {
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            var promiseProposta = this.propostaDeContratacaoService.updateStatusEncerrado(input);
                            promiseProposta.then((result) => {
                                this.goToHome();
                            });
                        } else {
                            this.goToHome();
                        }
                    }
                }
            );
        }

        public goToHome() {
            this.$state.go("tenant.dashboard");
        }

        public voltarPasso() {
            if (this.activeStep === 2 && this.activeSubStep > 0) {
                if (this.preCadastroInput.titularResponsavelLegal && this.activeSubStep - 1 == 1)
                    this.activeSubStep = 0;
                else
                    this.activeSubStep = this.activeSubStep - 1;
            } else {
                this.activeStep = this.activeStep - 1;
                if (this.activeStep === 1) {
                    this.selectTab(20, false);
                }
            }
        }

        public saveStep() {
            switch (this.passoAtual) {
                case PassoDaPropostaEnum.PreCadastro:
                    {
                        var ehInclusao = (this.preCadastroInput.id == 0 || this.preCadastroInput.id == undefined);
                        var promiseProposta = this.propostaDeContratacaoService.savePassoPreCadastro(this.preCadastroInput);

                        promiseProposta.then((result) => {

                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            this.preCadastroInput.id = this.entity.id;
                            this.preencherPreCadastro(this.entity);

                            if (ehInclusao) {
                                ehInclusao = false;

                                var promiseEmail = this.propostaDeContratacaoService.sendEmailUserBeneficiario(result.usuarioTitular);
                                promiseEmail.then((result) => {
                                }).finally(() => {

                                });
                            }

                            this.dadosGeraisInput.dataDeNascimentoProponenteTitular = this.preCadastroInput.dataDeNascimento;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.SelecionarPlano, false);
                        }).finally(() => {

                        });
                        break;
                    }
                case PassoDaPropostaEnum.SelecionarPlano:
                    {
                        this.selecaoDeProdutoInput.id = this.entity.id;
                        this.selecaoDeProdutoInput.produtoDePlanoDeSaudeId = this.entity.produtoId;

                        var promise2 = this.propostaDeContratacaoService.savePassoSelecionarPlano(this.selecaoDeProdutoInput);
                        promise2.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.DadosProponenteTitular, false);
                        }).finally(() => {
                            if ($('ul > li.uib-tab.nav-item.pt-item.pristine.uib-tab.nav-item.pt-item.pristine.completo.active > a').text() == "Seleção de plano") {
                                setTimeout(() => { this.selectTab(20, false) }, 1000);
                            }
                        });

                        break;
                    }
                case PassoDaPropostaEnum.DadosProponenteTitular:
                    {
                        this.dadosGeraisInput.id = this.entity.id;
                        this.dadosGeraisInput.proponenteTitular = this.entity.titular;
                        this.dadosGeraisInput.titularResponsavelLegal = this.preCadastroInput.titularResponsavelLegal;


                        var promise3 = this.propostaDeContratacaoService.savePassoDadosProponenteTitular(this.dadosGeraisInput);
                        promise3.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            if (!this.preCadastroInput.titularResponsavelLegal) {
                                this.selectTab(PassoDaPropostaEnum.DadosResponsavel, false);
                            } else {
                                this.selectTab(PassoDaPropostaEnum.Endereco, false);
                            }
                        });
                        break;
                    }
                case PassoDaPropostaEnum.DadosResponsavel:
                    {
                        var promise3 = this.propostaDeContratacaoService.savePassoDadosResponsavel(this.dadosGeraisInput);
                        promise3.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            if (result.usuarioResponsavelId &&
                                result.usuarioResponsavelId > 0 &&
                                result.usuarioTitularId !== result.usuarioResponsavelId) {
                                var promiseEmail = this.propostaDeContratacaoService.sendEmailUserBeneficiario(result.usuarioResponsavel);
                                promiseEmail.then((result) => {
                                });
                            }
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.Endereco, false);
                        });
                        break;
                    }
                case PassoDaPropostaEnum.Endereco:
                    {
                        var promise3 = this.propostaDeContratacaoService.savePassoEndereco(this.dadosGeraisInput);
                        promise3.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.DadosDependentes, false);
                        });
                        break;
                    }
                case PassoDaPropostaEnum.DadosDependentes:
                    {
                        var promise3 = this.propostaDeContratacaoService.savePassoDadosDependentes(this.dadosGeraisInput);
                        promise3.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.Documentos, false);
                        });
                        break;
                    }
                case PassoDaPropostaEnum.Documentos:
                    {
                        if (this.entity.documentos.length < 2) {
                            abp.message.error(app.localize('PropostaDeContratacao.EmptyDocumento'), app.localize('ValidationError'));
                            return false;
                        }

                        var promise3 = this.propostaDeContratacaoService.savePassoEnvioDeDocumentos(this.dadosGeraisInput);
                        promise3.then((result) => {
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            input.id = this.entity.id;
                            if (this.eParaSerReadOnly == true) {
                                this.propostaDeContratacaoService.verificaDocumentosEmExigencia(input);
                            }
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.DadosVigencia, false);
                        });
                        break;
                    }
                case PassoDaPropostaEnum.DadosVigencia:
                    {
                        var promise3 = this.propostaDeContratacaoService.savePassoDadosVigencia(this.dadosGeraisInput);
                        promise3.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            if (this.tipoDeUsuarioCorretor) {
                                this.selectTab(PassoDaPropostaEnum.DadosGerenciais, false);
                            } else {
                                this.selectTab(PassoDaPropostaEnum.DadosDeclaracaoDeSaude, false);
                            }
                        });
                        break;
                    }
                case PassoDaPropostaEnum.DadosGerenciais:
                    {
                        var promise = this.propostaDeContratacaoService.savePassoDadosGerenciais(this.dadosGeraisInput);
                        promise.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.DadosDeclaracaoDeSaude, false);
                        });

                        if (this.entity.chancelaId > 0) {

                            var promiseChancela = this.chancelaService.getByIdParaPropostaDeContratacao(this.entity.chancelaId);
                            promiseChancela.then((result) => {
                                this.chancela = result;
                            }).finally(() => {

                            });
                        }
                        break;
                    }
                case PassoDaPropostaEnum.DadosDeclaracaoDeSaude:
                    {
                        this.protocolNumber = this.entity.numero;
                        this.dadosGeraisInput.id = this.entity.id;
                        this.dadosGeraisInput.proponenteTitular = this.entity.titular;

                        var promise3 = this.propostaDeContratacaoService.savePassoDeclaracaoDeSaude(this.dadosGeraisInput);
                        promise3.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                            input.id = this.entity.id;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.AceiteDoContrato, false);

                            if (!result.aceite) {
                                var promiseEmail = this.propostaDeContratacaoService.sendEmailConcluirAceite(input.id);
                                promiseEmail.then((result) => {
                                });
                            }
                        });
                        break;
                    }
                case PassoDaPropostaEnum.AceiteDoContrato:
                    {
                        this.protocolNumber = this.entity.numero;
                        this.aceiteDoContratoInput.id = this.entity.id;
                        if (this.tipoDeUsuarioBeneficiario && !this.aceiteDoContratoInput.aceite) {
                            abp.message.error(app.localize("PropostaDeContratacao.DeveAceitarOContratoError"),
                                app.localize("Error"));

                            break;
                        }

                        if (this.tipoDeUsuarioCorretor && !this.aceiteDoContratoInput.aceiteCorretor) {
                            abp.message.error(app.localize("PropostaDeContratacao.DeveAceitarOContratoError"),
                                app.localize("Error"));
                            break;
                        }

                        this.aceiteDoContratoInput.id = this.entity.id;
                        this.aceiteDoContratoInput.contratoId = this.contratoInput.contratoId;

                        var promise5 = this.propostaDeContratacaoService.savePassoAceiteDoContrato(this.aceiteDoContratoInput);
                        promise5.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            if (this.entity.aceite) {
                                var promiseEmailAceite = this.propostaDeContratacaoService.sendEmailAceite(this.entity.id);
                                promiseEmailAceite.then(() => {
                                    //pode ou não enviar o email.
                                    //o usuário posteriormente pode realizar o envio do email através da index de proposta
                                    //realizado dessa forma para que, ao dar erro no envio, não pare o processo de contratação
                                }).finally(() => {

                                });

                                if (this.entity.passoDaProposta === PassoDaPropostaEnum.AceiteDoContrato) {

                                    // Notifica o usuário que deve finalizar o aceite (è necessário 2 aceites [Corretor e beneficiário])
                                    if (!this.entity.aceite) {
                                        var promiseEmailAceiteBeneficiario = this.propostaDeContratacaoService.sendEmailConcluirAceiteOnlyBeneficiario(this.entity.id);
                                        promiseEmailAceiteBeneficiario.then(() => { }).finally(() => { });
                                    }

                                    if (!this.entity.aceiteCorretor) {
                                        var promiseEmailAceiteCorretor = this.propostaDeContratacaoService.sendEmailConcluirAceiteOnlyCorretor(this.entity.id);
                                        promiseEmailAceiteCorretor.then(() => { }).finally(() => { });
                                    }
                                }

                                if (this.entity.aceite
                                    && this.entity.aceiteCorretor
                                    && this.entity.tipoDeHomologacao != TipoDeHomologacaoEnum.Homologado) {
                                    var promiseEmailAceite = this.propostaDeContratacaoService.sendEmailHomologar(this.entity.id);
                                    promiseEmailAceite.then(() => { }).finally(() => { });
                                }

                                this.passoAtual = result.passoDaProposta;
                                this.selectTab(PassoDaPropostaEnum.Homologacao, false);
                            }

                        }).finally(() => {

                        });
                        break;
                    }
                case PassoDaPropostaEnum.Homologacao:
                    {
                        this.homologacaoInput.id = this.entity.id;

                        var promise2 = this.propostaDeContratacaoService.savePassoHomologacao(this.homologacaoInput);
                        promise2.then((result) => {
                            this.entity = result;
                            this.protocolNumber = this.entity.numero;
                            this.passoAtual = result.passoDaProposta;
                            this.selectTab(PassoDaPropostaEnum.Pagamento, false);

                            if (result.tipoDeHomologacao == TipoDeHomologacaoEnum.Homologado) {
                                var promiseEmailHomologacao = this.propostaDeContratacaoService.sendEmailHomologacaoConcluida(this.entity.id);
                                promiseEmailHomologacao.then(() => {
                                });
                            }
                        }).finally(() => {

                        });

                        this.saving = false;
                        break;
                    }
                case PassoDaPropostaEnum.Pagamento:
                    {
                        // this.linkParaPagamento --------------> Boleto
                        // this.pcDepositoInput ---------------->Deposito/Transferência

                        if (!this.linkParaPagamento
                            && (!this.pcDepositoInput || (this.pcDepositoInput && !this.pcDepositoInput.recebido))) {
                            abp.message.error("Nenhum pagamento foi identificado", "Pagamento ausente");
                            break;
                        }

                        if (this.linkParaPagamento) {
                            abp.message.info("Após o pagamento do boleto o sistema irá identificar automaticamente. Volte em breve.", "Pagamento em andamento");
                        }

                        if (this.pcDepositoInput && this.pcDepositoInput.recebido) {
                            var input = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPagamentoInput();
                            input.id = this.entity.id;
                            this.protocolNumber = this.entity.numero;
                            input.formaDePagamento = FormaDePagamentoEnum.Deposito;
                            input.recebidoDeposito = this.pcDepositoInput.recebido;

                            var promise = this.propostaDeContratacaoService.savePassoPagamento(input);
                            promise.then((result) => {
                                this.entity = result;
                                this.protocolNumber = this.entity.numero;
                                this.passoAtual = result.passoDaProposta;
                            }).finally(() => { });

                            this.saving = false;
                        }

                        break;
                    }
            }

        }

        public addDependente() {
            var dependente = new dependenteDto.Dtos.EZLiv.Geral.DependenteInput();

            if (this.entity.dependentes == null || this.entity.dependentes == undefined)
                this.entity.dependentes = [];
            if (this.respostas) {
                for (let resposta of this.respostas) {
                    var itemResposta = new respostasDto.Dtos.EZLiv.Geral.RespostaDeDeclaracaoDeSaudeInput();
                    itemResposta.beneficiarioId = dependente.id;
                    itemResposta.perguntaId = resposta.perguntaId;
                    itemResposta.pergunta = resposta.pergunta;
                    itemResposta.marcada = resposta.marcada;
                    itemResposta.anoDoEvento = resposta.anoDoEvento;
                    itemResposta.tipoDeItemDeDeclaracao = resposta.tipoDeItemDeDeclaracao;
                    itemResposta.observacao = '';
                    if (!dependente.respostasDeclaracaoDeSaude) {
                        dependente.respostasDeclaracaoDeSaude = [itemResposta];
                    } else {
                        dependente.respostasDeclaracaoDeSaude.push(itemResposta);
                    }
                }
            }

            this.entity.dependentes.push(dependente);

            if (this.entity.dependentes) {
                dependente.formId = this.entity.dependentes.length + 1;
            }

            this.PreencherDependentePorAba();

            setTimeout(() => {
                $(".ezs-container input:text:visible:not(:disabled)").first().focus();
            }, 1000);
        }

        public removeDependente(dependente) {
            var index = this.entity.dependentes.indexOf(dependente);
            if (index > -1) {
                ezFixes.uiGrid.fixGrid();
                this.entity.dependentes.splice(index, 1);
                this.PreencherDependentePorAba();
            }
        }

        public PreencherDependentePorAba() {
            if (this.entity != undefined) {
                if (this.entity.id > 0) {
                    this.entity.dependentes = this.entity.titular.dependentes;

                    for (let dependente of this.entity.dependentes) {
                        if (dependente.pessoaFisica) {
                            dependente.pessoaFisica
                                .dataDeNascimento = dependente.pessoaFisica.dataDeNascimento != null
                                    ? new Date(dependente.pessoaFisica.dataDeNascimento.toString())
                                    : new Date();
                        }
                    }
                }

                if (this.preCadastroInput != null) {
                    this.preCadastroInput.dependentes = this.entity.dependentes;
                }
                if (this.dadosGeraisInput != null) {
                    this.dadosGeraisInput.dependentes = this.entity.dependentes;
                }
            }
        }

        public getItensDeDeclaracaoDeSaude() {
            if (this.entity && this.entity.produto && this.entity.produto.questionarioDeDeclaracaoDeSaude) {
                var itensDeDeclaracao = this.entity.produto.questionarioDeDeclaracaoDeSaude.itensDeDeclaracaoDeSaude;
                if (itensDeDeclaracao) {
                    for (var i = 0; i < itensDeDeclaracao.length; i++) {
                        var itemResposta = new respostasDto.Dtos.EZLiv.Geral.RespostaDeDeclaracaoDeSaudeInput();
                        itemResposta.marcada = null;
                        itemResposta.observacao = '';
                        itemResposta.tipoDeItemDeDeclaracao = itensDeDeclaracao[i].tipoDeItemDeDeclaracao;
                        itemResposta.pergunta = itensDeDeclaracao[i].pergunta;
                        itemResposta.perguntaId = itensDeDeclaracao[i].id;
                        if (this.respostas) {
                            this.respostas.push(itemResposta);
                        } else {
                            this.respostas = [];
                            this.respostas.splice(0, 1, itemResposta);
                        }
                    }
                };
            }
        }

        public changeDocumentoEmExigencia(arquivoDocumento: arquivoDocumentoDto.Dtos.EZLiv.Geral.ArquivoDocumentoInput) {
            var promise = this.arquivoDocumentoService.changeArquivoDocumentoExigencia(arquivoDocumento);
            promise.then((result) => {

            })
                .catch((result) => {
                    abp.message.error(result.message, '');
                });
        }

        public getNomeTipoDeDocumentoEscaneadoEzLiv(tipoDeDocumentoEscaneadoEzLiv: TipoDeDocumentoEscaneadoEzLivEnum) {
            return TipoDeDocumentoEscaneadoEzLivEnum.getItemPorValor(tipoDeDocumentoEscaneadoEzLiv).nome;
        }

        public getValueTipoDeDocumentoEscaneadoEzLiv(tipoDeDocumentoEscaneadoEzLiv: TipoDeDocumentoEscaneadoEzLivEnum) {
            return TipoDeDocumentoEscaneadoEzLivEnum.getItemPorValor(tipoDeDocumentoEscaneadoEzLiv).valor;
        }

        public siloadConteudoContrato = null;
        public loadConteudoContrato = () => {
            this.siloadConteudoContrato = setInterval(() => {
                var elementoContrato = $('#contrato-content');
                if (elementoContrato.length > 0) {
                    elementoContrato.load(document.location.origin + '/relatorio/visualizador?sistema=9&tipoDeRelatorio=1&idDado=' + this.entity.id,
                        function loadConteudoContratoResponse() {
                        });
                    clearInterval(this.siloadConteudoContrato);
                    this.siloadConteudoContrato = null;
                }
            }, 100);

        };

        public getStatusAbaProposta(step) {
            var classe = '';
            if (this.entity && this.entity.passoDaProposta) {
                switch (step) {
                    case 0: //Pré-Cadastro
                        return classe = this.entity.passoDaProposta > 10 ? 'completo' : 'active';
                    case 1: //Seleção de Plano
                        return classe = this.entity.passoDaProposta > 20 ? 'completo' : this.entity.passoDaProposta == 20 ? 'pendente' : '';
                    case 2: //Dados Gerais
                        return classe = this.entity.passoDaProposta >= 40 ? 'completo' : this.entity.passoDaProposta >= 30 && this.entity.passoDaProposta <= 39 ? 'pendente' : '';
                    case 3: //Aceite do Contrato
                        return classe = this.entity.passoDaProposta > 50 ? 'completo' : this.entity.passoDaProposta == 50 ? 'pendente' : '';
                    case 4: //Homologação
                        if (this.entity.passoDaProposta > 60 ||
                            this.entity &&
                            this.entity.tipoDeHomologacao &&
                            this.entity.tipoDeHomologacao == TipoDeHomologacaoEnum.Homologado) {
                            return classe = 'completo';
                        }
                        else if (this.entity.passoDaProposta == 60) {
                            if (this.entity &&
                                this.entity.tipoDeHomologacao &&
                                this.entity.tipoDeHomologacao == TipoDeHomologacaoEnum.Declinado) {
                                return classe = 'rejeitada';
                            }

                            return classe = 'pendente';
                        }
                        else if (this.entity.passoDaProposta < 60) {
                            return classe = '';
                        }
                    case 5: //Pagamento
                        return classe = this.entity.passoDaProposta == 70 ? this.entity.statusDaProposta == 1 ? 'completo' : 'pendente' : '';
                }
            }

            return classe;
        }
    }

    // Trigger para remover os eventos customizados do Ui

    // $('.ui-grid-render-container').trigger($.Event('$destroy'));

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PropostaDeContratacaoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/propostaDeContratacao/propostaDeContratacaoForm.cshtml';
            this.controller = PropostaDeContratacaoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }

    export class ModalDetailController {

        constructor(public $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance, public produto: any) {
        }

        close() {
            this.$uibModalInstance.close();
        }
    }
}