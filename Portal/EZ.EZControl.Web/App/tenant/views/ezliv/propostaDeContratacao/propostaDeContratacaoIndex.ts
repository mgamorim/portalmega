﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as propostaDeContratacaoService from "../../../../services/domain/ezliv/propostaDeContratacao/propostaDeContratacaoService";
import * as userService from "../../../../services/domain/core/user/userService";
import * as propostaDeContratacaoDtos from "../../../../dtos/ezliv/geral/propostaDeContratacao/propostaDeContratacaoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";
import * as meusProcessosDtos from "../../../../dtos/ezliv/geral/meusProcessos/meusProcessosDtos";
import * as meusProcessosService from "../../../../services/domain/ezliv/meusProcessos/meusProcessosService";
import * as produtoService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as produtoDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";

export module EZLiv.PropostaDeContratacao {

    class PropostaDeContratacaoIndexController extends
        controllers.Controllers.ControllerIndexBase<propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto,
        propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {

        static $inject = ['$scope', '$state', '$rootScope', 'uiGridConstants', 'propostaDeContratacaoService', 'userService', 'meusProcessosService', 'produtoDePlanoDeSaudeService', '$uibModal'];
        public ezGrid: ezGrid.EzGrid.EzGrid<propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;
        public notificacaoPreCadastro: (registro: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto) => void;
        public notificacaoAceite: (registro: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto) => void;
        public notificacaoHomologacao: (registro: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto) => void;
        public notificacaoPagamento: (registro: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto) => void;
        private tipoDeUsuarioAdmOuCorretora: boolean;
        private strTipoDeUsuario: string;
        private aceite: boolean;
        private homologado: boolean;
        private pago: boolean;

        public planoModal: produtoDtos.Dtos.EZLiv.Geral.DetalhesProdutoDePlanoDeSaudeInput;
        public showMaisDetalhes: (registro: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput) => void;
        public closeModal: () => void;
        public modalStatus: boolean = false;
        public modalInstance: any;

        constructor(
            public $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $rootScope: any,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public propostaDeContratacaoService: propostaDeContratacaoService.Services.PropostaDeContratacaoService,
            public userService: userService.Services.UserService,
            public meusProcessosService: meusProcessosService.Services.MeusProcessosService,
            public produtoService: produtoService.Services.ProdutoDePlanoDeSaudeService,
            public uibModal: any
        ) {

            super(
                $state,
                uiGridConstants,
                propostaDeContratacaoService, {
                    rotaAlterarRegistro: 'tenant.ezliv.propostaDeContratacao.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.propostaDeContratacao.novo'
                }
            );


        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new propostaDeContratacaoDtos.Dtos.EZLiv.Geral.GetPropostaDeContratacaoInput();
                filtro.cpfDoBeneficiario = termoDigitadoPesquisa;
                filtro.nomeDoBeneficiario = termoDigitadoPesquisa;
                filtro.numeroDaProposta = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.propostaDeContratacaoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPropostaDeContratacao', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.PropostaDeContratacao.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            //this.ezGrid.serviceGetCallback = (filtro, requestParams) => {

            //    var promise = this.userService.getTipoDeUsuarioByUsuarioLogado();
            //    promise.then((result) => {
            //        if (result.tipoDeUsuario == TipoDeUsuarioEnum.Beneficiario)
            //            return this.propostaDeContratacaoService.getPaged(filtro, requestParams);
            //    });

            //    return this.propostaDeContratacaoService.getPaged(filtro, requestParams);
            //};

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            this.showMaisDetalhes = (registro) => {
                var input = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                input.produtoId = registro.produtoId;
                input.propostaId = registro.id;
                var promise = this.produtoService.getDetalhesById(input);
                promise.then((result) => {
                    this.planoModal = result;
                    //this.planoModal.valor = registro.produto.valor;
                    this.modalStatus = true;
                    this.modalInstance = this.uibModal.open({
                        templateUrl: '~/App/common/views/modals/detalhesProdutoDePlanoDeSaude/detalhesProdutoDePlanoDeSaudeModal.cshtml',
                        controller: 'common.views.modals.detalhesPlano as $ctrl',
                        backdrop: 'static',
                        scope: this.$scope
                    });
                });
            };

            this.closeModal = () => {
                this.modalStatus = false;
                this.modalInstance.dismiss();
            }

            var promise = this.userService.getTipoDeUsuarioByUsuarioLogado();

            promise.then((result) => {
                this.tipoDeUsuarioAdmOuCorretora = (result.tipoDeUsuario === TipoDeUsuarioEnum.Administradora || result.tipoDeUsuario === TipoDeUsuarioEnum.Corretor);

                this.strTipoDeUsuario = result.strRoles;

                // configurar o ezGrid
                this.ezActions();
                this.ezGridConfig();
                this.ezGrid.getRegistros();
            });
        }

        private ezActions() {

            if (this.tipoDeUsuarioAdmOuCorretora) {

                this.notificacaoPreCadastro = (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => {

                    var promiseEmailPreCadastro = this.propostaDeContratacaoService.sendEmailPreCadastro(registro.id);
                    promiseEmailPreCadastro.then(() => {
                        abp.message.success(app.localize('PropostaDeContratacao.Notificacao'), app.localize("Sucesso"));
                    }).catch((err) => {
                        abp.message.error(err.details, app.localize("PropostaDeContratacao.NotificacaoError"));
                    }).finally(() => {

                    });
                }

                this.notificacaoAceite = (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => {

                    var promiseEmailAceite = this.propostaDeContratacaoService.sendEmailAceite(registro.id);
                    promiseEmailAceite.then(() => {
                        abp.message.success(app.localize('PropostaDeContratacao.Notificacao'), app.localize("Sucesso"));
                    }).catch((err) => {
                        abp.message.error(err.message, app.localize("PropostaDeContratacao.NotificacaoError"));
                    }).finally(() => {

                    });
                }

                this.notificacaoHomologacao = (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => {

                    var promiseEmailHomologacao = this.propostaDeContratacaoService.sendEmailHomologacao(registro.id);
                    promiseEmailHomologacao.then(() => {
                        abp.message.success(app.localize('PropostaDeContratacao.Notificacao'), app.localize("Sucesso"));
                    }).catch((err) => {
                        abp.message.error(err.message, app.localize("PropostaDeContratacao.NotificacaoError"));
                    }).finally(() => {

                    });
                }

                this.notificacaoPagamento = (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => {

                    var promiseEmailBoleto = this.propostaDeContratacaoService.sendEmailBoletoGerado(registro.id);
                    promiseEmailBoleto.then(() => {
                        abp.message.success(app.localize('PropostaDeContratacao.Notificacao'), app.localize("Sucesso"));
                    }).catch((err) => {
                        abp.message.error(err.message, app.localize("PropostaDeContratacao.NotificacaoError"));
                    }).finally(() => {

                    });
                }

                this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.notificacaoPreCadastro, 'Notificar Pré-cadastro',
                    (row) => {
                        return true;
                    }
                ));

                this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.notificacaoAceite, 'Notificar Aceite',
                    (row) => {
                        return row.passoDaProposta > PassoDaPropostaEnum.AceiteDoContrato;
                    }
                ));

                this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.notificacaoHomologacao, 'Notificar Homologação',
                    (row) => {
                        return row.passoDaProposta > PassoDaPropostaEnum.Homologacao;
                    }
                ));

                this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.notificacaoPagamento, 'Notificar Pagamento',
                    (row) => {
                        return (row.passoDaProposta === PassoDaPropostaEnum.Pagamento);
                    }
                ));
            }

        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.PropostaDeContratacao.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.PropostaDeContratacao.Delete');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                (registro, row) => {
                    var instance = (registro as propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput);
                    this.showMaisDetalhes(instance);
                },
                'MaisDetalhes',
                (row) => {
                    return false;
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');
            this.ezGrid.optionsGrid.enablePagination = true;
            this.ezGrid.optionsGrid.rowHeight = 80;

            // Definir as colunas
            let width = 0;

            switch (this.strTipoDeUsuario) {
                case 'Admin': width = 115; //Adim
                    break;
                case 'Administradora': width = 130; //Administradora
                    break;
                case 'Corretora': width = 130; //Corretora
                    break;
                default: width = 156;
            }

            this.ezGrid.optionsGrid.columnDefs.push({
                name: 'acoes',
                displayName: app.localize('Actions'),
                enableSorting: false,
                cellTemplate: '<ez-grid-actions></ez-grid-actions>',
                cellClass: 'ez-grid-actions',
                headerCellClass: 'header',
                width: 100,
                enableColumnMenu: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PropostaDeContratacao.Protocolo'),
                field: 'numero',
                headerCellClass: 'header',
                width: width,
                enableHiding: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PropostaDeContratacao.Titular'),
                field: 'titular.pessoaFisica.nome',
                headerCellClass: 'header',
                width: width,
                enableHiding: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PropostaDeContratacao.Produto'),
                field: 'produto.nome',
                headerCellClass: 'header',
                width: width + 5,
                enableHiding: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoPlanoDeSaude.Administradora'),
                field: 'produto.administradora.pessoaJuridica.razaoSocial',
                headerCellClass: 'header',
                width: 150,
                enableHiding: false
            });

            if (this.strTipoDeUsuario == 'Administradora' || this.strTipoDeUsuario == 'Admin') {
                this.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ProdutoPlanoDeSaude.Corretora'),
                    field: 'corretora.nome',
                    headerCellClass: 'header',
                    width: width,
                    enableHiding: false
                });
            }

            if (this.strTipoDeUsuario == 'Corretora' || this.strTipoDeUsuario == 'Admin') {
                this.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ProdutoPlanoDeSaude.Corretor'),
                    field: 'corretora.corretor.pessoa.nomePessoa',
                    headerCellClass: 'header',
                    width: width,
                    enableHiding: false
                });
            }

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ProdutoPlanoDeSaude.Operadora'),
                field: 'produto.operadora.pessoaJuridica.razaoSocial',
                headerCellClass: 'header',
                width: width,
                enableHiding: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PropostaDeContratacao.Status'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeHomologacaoEnum", "tipoDeHomologacao")}}\
                </div>',
                headerCellClass: 'header',
                width: width,
                enableHiding: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PropostaDeContratacao.Aceite'),
                field: 'aceite',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html',
                headerCellClass: 'header',
                width: 75,
                enableHiding: false
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: '',
                field: 'id',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    <a ng-repeat="item in grid.appScope.actionLinks" ng-if="item.text == \'MaisDetalhes\'" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                </div>',
                width: 70,
                enableSorting: false,
                enableColumnMenu: false
            });
        }
    }

    export class PropostaDeContratacaoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/propostaDeContratacao/propostaDeContratacaoIndex.cshtml';
            this.controller = PropostaDeContratacaoIndexController;
        }
    }
}