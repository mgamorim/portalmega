﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as hospitalService from "../../../../services/domain/ezliv/hospital/hospitalService";
import * as hospitalDtos from "../../../../dtos/ezliv/geral/hospital/hospitalDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Hospital {

    class HospitalIndexController extends
        controllers.Controllers.ControllerIndexBase<hospitalDtos.Dtos.EZLiv.Geral.HospitalListDto,
        hospitalDtos.Dtos.EZLiv.Geral.HospitalInput> {

        static $inject = ['$state', 'uiGridConstants', 'hospitalService'];

        public ezGrid: ezGrid.EzGrid.EzGrid<hospitalDtos.Dtos.EZLiv.Geral.HospitalListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public hospitalService: hospitalService.Services.HospitalService) {

            super(
                $state,
                uiGridConstants,
                hospitalService, {
                    rotaAlterarRegistro: 'tenant.ezliv.hospital.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.hospital.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'site';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new hospitalDtos.Dtos.EZLiv.Geral.GetHospitalInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.email = termoDigitadoPesquisa;
                filtro.site = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<hospitalDtos.Dtos.EZLiv.Geral.HospitalListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.hospitalService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            //// objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewHospital', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Hospital.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Hospital.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Hospital.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Hospital.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Hospital.NomeFantasia'),
                field: 'pessoaJuridica.nomeFantasia',
            });


            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Hospital.RazaoSocial'),
                field: 'pessoaJuridica.razaoSocial',
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Hospital.Site'),
                field: 'site'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Hospital.Email'),
                field: 'email'
            });
        }
    }

    export class HospitalIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/hospital/hospitalIndex.cshtml';
            this.controller = HospitalIndexController;
        }
    }
}