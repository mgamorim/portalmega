﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as chancelaService from "../../../../services/domain/ezliv/chancela/chancelaService";
import * as chancelaDtos from "../../../../dtos/ezliv/geral/chancela/chancelaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.Chancela {

    class ChancelaIndexController extends controllers.Controllers.ControllerIndexBase <chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto, chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput>{

        static $inject = ['$state', 'uiGridConstants', 'chancelaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public chancelaService: chancelaService.Services.ChancelaService) {

            super(
                $state,
                uiGridConstants,
                chancelaService, {
                    rotaAlterarRegistro: 'tenant.ezliv.chancela.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.chancela.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.chancelaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewChancela', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Chancela.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Chancela.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Chancela.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Chancela.TaxaDeAdesao'),
                field: 'taxaDeAdesao',
                cellFilter: 'currency: "R$": 2'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Chancela.TaxaMensal'),
                field: 'taxaMensal',
                cellFilter: 'currency: "R$": 2'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Chancela.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class ChancelaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/chancela/chancelaIndex.cshtml';
            this.controller = ChancelaIndexController;
        }
    }
}