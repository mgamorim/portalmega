﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as chancelaService from "../../../../services/domain/ezliv/chancela/chancelaService";
import * as profissaoService from "../../../../services/domain/global/profissao/profissaoService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as chancelaDtos from "../../../../dtos/ezliv/geral/chancela/chancelaDtos";
import * as profissaoDtos from "../../../../dtos/global/profissao/profissaoDtos";

import * as entidadeService from "../../../../services/domain/ezliv/associacao/associacaoService";
import * as entidadeDtos from "../../../../dtos/ezliv/subtipoPessoa/associacao/associacaoDtos";

export module Forms {
  export class ChancelaFormController extends controllers.Controllers.ControllerFormComponentBase<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto, chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
    public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
    private requestParamsBanco: requestParam.RequestParam.RequestParams;
    private createDescription: () => any;

    public ezSelectProfissao: ezSelect.EzSelect.EzSelect<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParam.RequestParam.RequestParams>;
    public getFiltroParaProfissao: (termoDigitadoPesquisa: string) => any;
    private profissaoSelecionado: (registro: profissaoDtos.Dtos.Profissao.ProfissaoListDto) => string;
    private profissaoDeselecionado: () => void;
    private requestParamsProfissao: requestParam.RequestParam.RequestParams;

    private idsProfissoes: number[];

    public saving: boolean;
    public editing: boolean;
    public excluir: (valor: profissaoDtos.Dtos.Profissao.ProfissaoListDto) => void;
    public limparEntidade: () => void;
    public addProfissao: () => void;

    private setDataGridInput: (result: profissaoDtos.Dtos.Profissao.ProfissaoInput[]) => void;
    private gridApiInput: uiGrid.IGridApiOf<profissaoDtos.Dtos.Profissao.ProfissaoInput>;
    private requestParamsProfissoesInput: requestParam.RequestParam.RequestParams;
    private profissoesAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<profissaoDtos.Dtos.Profissao.ProfissaoInput>;
    public getProfissoesInput: () => void;
    public loadingValoresInput: boolean;
    public profissoesGridOptionsInput: uiGrid.IGridOptionsOf<profissaoDtos.Dtos.Profissao.ProfissaoInput>;
    private gridDataInput: profissaoDtos.Dtos.Profissao.ProfissaoInput[];

    public profissaoCurrent: profissaoDtos.Dtos.Profissao.ProfissaoInput;

    public ezSelectEntidade: ezSelect.EzSelect.EzSelect<entidadeDtos.Dtos.EZLiv.Geral.AssociacaoListDto, requestParam.RequestParam.RequestParams>;
    public getFiltroParaEntidade: (termoDigitadoPesquisa: string) => any;
    private entidadeSelecionado: (registro: entidadeDtos.Dtos.EZLiv.Geral.AssociacaoListDto) => string;
    private entidadeDeselecionado: () => void;
    private requestParamsEntidade: requestParam.RequestParam.RequestParams;

    static $inject = ['$state', '$stateParams', 'uiGridConstants', 'chancelaService', 'profissaoService', 'associacaoService', '$uibModal'];

    constructor(
      public $state: ng.ui.IStateService,
      public $stateParams: ng.ui.IStateParamsService,
      public uiGridConstants: uiGrid.IUiGridConstants,
      public chancelaService: chancelaService.Services.ChancelaService,
      public profissaoService: profissaoService.Services.ProfissaoService,
      public entidadeService: entidadeService.Services.AssociacaoService,
      public $uibModal: angular.ui.bootstrap.IModalService) {
      super(
        $state,
        $stateParams,
        $uibModal,
        uiGridConstants,
        chancelaService,
        'tenant.ezliv.chancela');

      this.getProfissoesInput = () => {
        this.loadingValoresInput = true;
        if (this.entity) {
          this.setDataGridInput(this.entity.profissoes);
        }

        this.loadingValoresInput = false;
      }

      this.setDataGridInput = (result: profissaoDtos.Dtos.Profissao.ProfissaoInput[]) => {
        this.gridDataInput.length = 0;

        for (var i = 0; i < result.length; i++) {
          this.gridDataInput.push(result[i]);
        }

        this.profissoesGridOptionsInput.totalItems = result.length;
      };

      this.excluir = (valor: profissaoDtos.Dtos.Profissao.ProfissaoListDto) => {
        this.editing = false;
        abp.message.confirm(
          'Deseja remover essa profissão?', 'Confirmação',
          isConfirmed => {
            if (isConfirmed) {
              for (var i = 0; i < this.entity.profissoes.length; i++) {
                if (this.entity.profissoes[i].id === valor.id) {
                  this.entity.profissoes.splice(i, 1);
                  this.getProfissoesInput();
                  abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                  break;
                }
              }
            }
          }
        );
      }

      this.addProfissao = () => {
        this.profissaoCurrent.isActive = true;
        this.entity.profissoes.push(this.profissaoCurrent);
        this.getProfissoesInput();
        this.profissaoCurrent = new profissaoDtos.Dtos.Profissao.ProfissaoInput();
        this.ezSelectProfissao.setInputText('');
      }
    }

    $onInit() {
      super.init();
      this.profissaoCurrent = new profissaoDtos.Dtos.Profissao.ProfissaoInput();

      this.events.onGetNewEntityInstance = () => {
        // evento para obter uma nova instancia para a entidade da tela
        // e tambem para inicializar suas variavéis com valores padrões
        var instance = new chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput();
        instance.id = 0;
        instance.isActive = true;
        instance.profissoes = new Array<profissaoDtos.Dtos.Profissao.ProfissaoInput>();
        return instance;
      };

      this.events.onGetEntity = (entity) => {
        if (entity.associacaoId > 0) {
          this.ezSelectEntidade.loading = true;
          var promiseAssociacao = this.entidadeService.getById(entity.associacaoId);
          promiseAssociacao.then(result => {
            this.ezSelectEntidade.setInputText(result.pessoaJuridica.nomeFantasia);
          }).finally(() => {
            this.ezSelectEntidade.loading = false;
          });
        }
        this.getProfissoesInput();
      }

      this.ezSelectProfissaoConfig();
      this.ezSelectEntidadeConfig();
      this.valorGridConfigInput();
      // método para carregar ou criar uma nova instancia da entidade.
      // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
      this.prepareEntityInstance();
    }

    private ezSelectProfissaoConfig() {
      this.requestParamsProfissao = new requestParam.RequestParam.RequestParams();

      this.profissaoSelecionado = (registro) => {
        this.profissaoCurrent = registro;
        return registro.titulo;
      }

      this.profissaoDeselecionado = () => {
        this.profissaoCurrent = null;
      }

      // callback para criar e definir o filtro para buscar os registros
      this.getFiltroParaProfissao = (termoDigitadoPesquisa) => {
        var filtro = new profissaoDtos.Dtos.Profissao.GetProfissaoInput();
        filtro.titulo = termoDigitadoPesquisa;
        filtro.codigo = termoDigitadoPesquisa;
        return filtro;
      }

      this.ezSelectProfissao = new ezSelect.EzSelect.EzSelect<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParam.RequestParam.RequestParams>(
        this.uiGridConstants,
        this.profissaoService,
        this.getFiltroParaProfissao,
        this.requestParamsProfissao,
        this.$uibModal,
        this.profissaoSelecionado,
        this.profissaoDeselecionado);

      // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
      // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
      // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
      this.ezSelectProfissao.onEzGridCreated = () => {

        this.ezSelectProfissao.ezGrid.serviceGetCallback = () => {
          var input = new profissaoDtos.Dtos.Profissao.GetProfissaoInput();
          return this.profissaoService.getPaged(input, this.requestParamsProfissao);
        };

        this.ezSelectProfissao.ezGrid.optionsGrid.columnDefs.push({
          name: app.localize('Profissao.Titulo'),
          field: 'titulo'
        });

        this.ezSelectProfissao.ezGrid.optionsGrid.columnDefs.push({
          name: app.localize('Profissao.Ativo'),
          field: 'isActive',
          cellTemplate: 'App/common/views/common/grid/estaChecado.html'
        });

        this.ezSelectProfissao.setLinkSelecionarPrimeiraColunaGrid();
      }

      // Eventos do ezSelect.
      // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
      this.ezSelectProfissao.on.beforeOpenModalDialog = () => {
        this.ezSelectProfissao.ezGrid.getRegistros();
      };
    }

    private ezSelectEntidadeConfig() {
      this.requestParamsEntidade = new requestParam.RequestParam.RequestParams();

      this.entidadeSelecionado = (registro) => {
        this.entity.associacaoId = registro.id;
        return registro.pessoaJuridica.nomeFantasia;
      }

      this.entidadeDeselecionado = () => {
        this.entity.associacaoId = null;
      }

      // callback para criar e definir o filtro para buscar os registros
      this.getFiltroParaEntidade = (termoDigitadoPesquisa) => {
        var filtro = new entidadeDtos.Dtos.EZLiv.Geral.GetAssociacaoInput();
        filtro.nomeFantasia = termoDigitadoPesquisa;
        filtro.razaoSocial = termoDigitadoPesquisa;
        return filtro;
      }

      this.ezSelectEntidade = new ezSelect.EzSelect.EzSelect<entidadeDtos.Dtos.EZLiv.Geral.AssociacaoListDto, requestParam.RequestParam.RequestParams>(
        this.uiGridConstants,
        this.entidadeService,
        this.getFiltroParaEntidade,
        this.requestParamsEntidade,
        this.$uibModal,
        this.entidadeSelecionado,
        this.entidadeDeselecionado);

      // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
      // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
      // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
      this.ezSelectEntidade.onEzGridCreated = () => {

        this.ezSelectEntidade.ezGrid.serviceGetCallback = () => {
          var input = new entidadeDtos.Dtos.EZLiv.Geral.GetAssociacaoInput();
          return this.entidadeService.getPaged(input, this.requestParamsEntidade);
        };

        this.ezSelectEntidade.ezGrid.optionsGrid.columnDefs.push({
          name: app.localize('Entidade.NomeFantasia'),
          field: 'pessoaJuridica.nomeFantasia'
        });

        this.ezSelectEntidade.ezGrid.optionsGrid.columnDefs.push({
          name: app.localize('Entidade.Ativo'),
          field: 'isActive',
          cellTemplate: 'App/common/views/common/grid/estaChecado.html'
        });

        this.ezSelectEntidade.setLinkSelecionarPrimeiraColunaGrid();
      }

      // Eventos do ezSelect.
      // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
      this.ezSelectEntidade.on.beforeOpenModalDialog = () => {
        this.ezSelectEntidade.ezGrid.getRegistros();
      };
    }

    private valorGridConfigInput() {
      this.requestParamsProfissoesInput = new requestParam.RequestParam.RequestParams();

      this.profissoesAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<profissaoDtos.Dtos.Profissao.ProfissaoInput>();

      this.profissoesAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
        (row) => {
          return true;
        }
      ));

      this.gridDataInput = new Array<profissaoDtos.Dtos.Profissao.ProfissaoInput>();

      this.profissoesGridOptionsInput = {
        enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
        enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
        paginationPageSizes: app.consts.grid.defaultPageSizes,
        paginationPageSize: app.consts.grid.defaultPageSize,
        useExternalPagination: true,
        showColumnFooter: true,
        rowHeight: 50,
        enableGridMenu: true,
        exporterMenuCsv: false,
        exporterMenuPdf: false,
        rowTemplate: 'App/common/views/common/grid/linha.html',
        columnDefs: [],
        appScopeProvider: this.profissoesAppScopeProviderInput,
        onRegisterApi: (api) => {
          this.gridApiInput = api;
          this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
            if (!sortColumns.length || !sortColumns[0].field) {
              this.requestParamsProfissoesInput.sorting = null;
            } else {
              this.requestParamsProfissoesInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
            }

            this.getProfissoesInput();
          });
          this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
            this.requestParamsProfissoesInput.skipCount = (pageNumber - 1) * pageSize;
            this.requestParamsProfissoesInput.maxResultCount = pageSize;

            this.getProfissoesInput();
          });
        }
      }

      this.profissoesGridOptionsInput.data = this.gridDataInput;

      this.profissoesGridOptionsInput.columnDefs.push({
        name: app.localize('Profissao.Titulo'),
        field: 'titulo',
        cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
      });

      this.getProfissoesInput();
    }
  }

  /*
  Os bindings do componente funcionam da seguinte maneira:
      @: deve ser usado para strings
      <: objetos
      &: funcoes
      =: não deve ser mais usado, está obsoleto
  */
  export class ChancelaFormComponent implements ng.IComponentOptions {
    public templateUrl: string;
    public controller: any;
    public bindings: any;

    constructor() {
      this.templateUrl = '~/app/tenant/views/ezliv/chancela/chancelaForm.cshtml';
      this.controller = ChancelaFormController;
      this.bindings = {
        operation: '@'
      }
    }
  }
}