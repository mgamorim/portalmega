﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// Services
import * as clienteEZLivService from "../../../../services/domain/ezliv/clienteEZLiv/clienteEZLivService";
import * as clienteService from "../../../../services/domain/global/cliente/clienteService";

//DTOS
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as clienteEZLivDtos from "../../../../dtos/ezliv/subtipoPessoa/clienteEZLiv/clienteEZLivDtos";
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Forms {
    export class ClienteEZLivFormController extends controllers.Controllers.ControllerFormComponentBase<clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivListDto, clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectCliente: ezSelect.EzSelect.EzSelect<clienteDtos.Dtos.Cliente.ClienteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaCliente: (termoDigitadoPesquisa: string) => any;
        private clienteSelecionado: (registro: clienteDtos.Dtos.Cliente.ClienteListDto) => string;
        private clienteDeselecionado: () => void;
        private requestParamsCliente: requestParam.RequestParam.RequestParams;

        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        public unidadePadraoEnum: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'clienteEZLivService', 'clienteService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public clienteEZLivService: clienteEZLivService.Services.ClienteEZLivService,
            public clienteService: clienteService.Services.ClienteService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                clienteEZLivService,
                'tenant.ezliv.clienteEZLiv');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivInput();
                instance.id = 0;
                instance.clienteId = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                if (entity.clienteId) {
                    this.ezSelectCliente.loading = true;
                    var promiseUnidadeMedida = this.clienteService.getById(entity.clienteId);
                    promiseUnidadeMedida.then(result => {
                        this.ezSelectCliente.setInputText(result.pessoa.tipoPessoa == TipoPessoaEnum.Fisica ? result.pessoaFisica.nome : result.pessoaJuridica.nomeFantasia);
                    }).finally(() => {
                        this.ezSelectCliente.loading = false;
                    });
                }
            }

            this.ezSelectClienteConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectClienteConfig() {
            this.requestParamsCliente = new requestParam.RequestParam.RequestParams();

            this.clienteSelecionado = (registro) => {
                this.entity.clienteId = registro.id;
                return registro.pessoa.tipoPessoa == TipoPessoaEnum.Fisica ? registro.pessoaFisica.nome : registro.pessoaJuridica.nomeFantasia;
            }

            this.clienteDeselecionado = () => {
                this.entity.clienteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaCliente = (termoDigitadoPesquisa) => {
                var filtro = new clienteDtos.Dtos.Cliente.ClienteInput();
                filtro.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput();
                filtro.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput();
                filtro.pessoaFisica.nome = termoDigitadoPesquisa;
                filtro.pessoaJuridica.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCliente = new ezSelect.EzSelect.EzSelect<clienteDtos.Dtos.Cliente.ClienteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.clienteService,
                this.getFiltroParaCliente,
                this.requestParamsCliente,
                this.$uibModal,
                this.clienteSelecionado,
                this.clienteDeselecionado);

            // Esse documento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse documento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCliente.onEzGridCreated = () => {
                this.ezSelectCliente.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.clienteEZLivService.getPagedExceptForId(filtro, requestParams);
                };
                this.ezSelectCliente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.NomePessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectCliente.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este documento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectCliente.on.beforeOpenModalDialog = () => {
                this.ezSelectCliente.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ClienteEZLivFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/clienteEZLiv/clienteEZLivForm.cshtml';
            this.controller = ClienteEZLivFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}