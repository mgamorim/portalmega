﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as clienteEZLivService from "../../../../services/domain/ezliv/clienteEZLiv/clienteEZLivService";
import * as clienteEZLivDtos from "../../../../dtos/ezliv/subtipoPessoa/clienteEZLiv/clienteEZLivDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as clienteService from "../../../../services/domain/global/cliente/clienteService";

export module EZLiv.ClienteEZLiv {

    class ClienteEZLivIndexController extends controllers.Controllers.ControllerIndexBase<clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivListDto, clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivInput>{

        static $inject = ['$state', 'uiGridConstants', 'clienteEZLivService', 'clienteService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public clienteEZLivService: clienteEZLivService.Services.ClienteEZLivService,
            public clienteService: clienteService.Services.ClienteService) {

            super(
                $state,
                uiGridConstants,
                clienteEZLivService, {
                    rotaAlterarRegistro: 'tenant.ezliv.clienteEZLiv.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.clienteEZLiv.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new clienteEZLivDtos.Dtos.ClienteEZLiv.GetClienteEZLivInput();
                filtro.clienteId = 0; //termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<clienteEZLivDtos.Dtos.ClienteEZLiv.ClienteEZLivListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.clienteEZLivService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('ClienteEZLiv.CreatingNew', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.ClienteEZLiv.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ClienteEZLiv.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ClienteEZLiv.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.NomePessoa'),
                field: 'cliente.pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ClienteEZLiv.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class ClienteEZLivIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/clienteEZLiv/clienteEZLivIndex.cshtml';
            this.controller = ClienteEZLivIndexController;
        }
    }
}