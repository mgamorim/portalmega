﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as indiceDeReajustePorFaixaEtariaService from "../../../../services/domain/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaService";
import * as indiceDeReajustePorFaixaEtariaDtos from "../../../../dtos/ezliv/geral/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.IndiceDeReajustePorFaixaEtaria {

    class IndiceDeReajustePorFaixaEtariaIndexController extends controllers.Controllers.ControllerIndexBase<indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaListDto, indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaInput>{

        static $inject = ['$state', 'uiGridConstants', 'indiceDeReajustePorFaixaEtariaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public indiceDeReajustePorFaixaEtariaService: indiceDeReajustePorFaixaEtariaService.Services.IndiceDeReajustePorFaixaEtariaService) {

            super(
                $state,
                uiGridConstants,
                indiceDeReajustePorFaixaEtariaService, {
                    rotaAlterarRegistro: 'tenant.ezliv.indiceDeReajustePorFaixaEtaria.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.indiceDeReajustePorFaixaEtaria.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetIndiceDeReajustePorFaixaEtariaInput();
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.indiceDeReajustePorFaixaEtariaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewIndiceDeReajustePorFaixaEtaria', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.IndiceDeReajustePorFaixaEtaria.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('IndiceDeReajustePorFaixaEtaria.FaixaEtaria'),
                field: 'faixaEtaria.descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('IndiceDeReajustePorFaixaEtaria.Operadora'),
                field: 'operadora.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('IndiceDeReajustePorFaixaEtaria.IndiceDeReajuste'),
                field: 'indiceDeReajuste',
                cellFilter: 'number: 2'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('IndiceDeReajustePorFaixaEtaria.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });




        }
    }

    export class IndiceDeReajustePorFaixaEtariaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaIndex.cshtml';
            this.controller = IndiceDeReajustePorFaixaEtariaIndexController;
        }
    }
}