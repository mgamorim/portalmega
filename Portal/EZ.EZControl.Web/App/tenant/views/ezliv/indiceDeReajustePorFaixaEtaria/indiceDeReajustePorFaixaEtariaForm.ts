﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as faixaEtariaService from "../../../../services/domain/ezliv/faixaEtaria/faixaEtariaService";
import * as operadoraService from "../../../../services/domain/ezliv/operadora/operadoraService";
import * as indiceDeReajustePorFaixaEtariaService from "../../../../services/domain/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as faixaEtariaDtos from "../../../../dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as indiceDeReajustePorFaixaEtariaDtos from "../../../../dtos/ezliv/geral/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaDtos";

export module Forms {
    export class IndiceDeReajustePorFaixaEtariaFormController extends controllers.Controllers.ControllerFormComponentBase<indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaListDto, indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{

        public ezSelectFaixaEtaria: ezSelect.EzSelect.EzSelect<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaFaixaEtaria: (termoDigitadoPesquisa: string) => any;
        private faixaEtariaSelecionado: (registro: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto) => string;
        private faixaEtariaDeselecionado: () => void;
        private requestParamsFaixaEtaria: requestParam.RequestParam.RequestParams;

        public ezSelectOperadora: ezSelect.EzSelect.EzSelect<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaOperadora: (termoDigitadoPesquisa: string) => any;
        private operadoraSelecionado: (registro: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto) => string;
        private operadoraDeselecionado: () => void;
        private requestParamsOperadora: requestParam.RequestParam.RequestParams;

        public getFiltroParaPaginacaoOperadora: (termoDigitadoPesquisa: string) => any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'indiceDeReajustePorFaixaEtariaService', 'operadoraService', 'faixaEtariaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public indiceDeReajustePorFaixaEtariaService: indiceDeReajustePorFaixaEtariaService.Services.IndiceDeReajustePorFaixaEtariaService,
            public operadoraService: operadoraService.Services.OperadoraService,
            public faixaEtariaService: faixaEtariaService.Services.FaixaEtariaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                indiceDeReajustePorFaixaEtariaService,
                'tenant.ezliv.indiceDeReajustePorFaixaEtaria');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                if (entity.operadoraId) {
                    this.ezSelectOperadora.loading = true;
                    var promiseOperadora = this.operadoraService.getById(entity.operadoraId);
                    promiseOperadora.then(result => {
                        this.ezSelectOperadora.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectOperadora.loading = false;
                    });
                }

                if (entity.faixaEtariaId) {
                    this.ezSelectFaixaEtaria.loading = true;
                    var promiseFaixaEtaria = this.faixaEtariaService.getById(entity.faixaEtariaId);
                    promiseFaixaEtaria.then(result => {
                        this.ezSelectFaixaEtaria.setInputText(result.descricao);
                    }).finally(() => {
                        this.ezSelectFaixaEtaria.loading = false;
                    });
                }
            }

            this.ezSelectFaixaEtariaConfig();
            this.ezSelectOperadoraConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectFaixaEtariaConfig() {
            this.requestParamsFaixaEtaria = new requestParam.RequestParam.RequestParams();

            this.faixaEtariaSelecionado = (registro) => {
                this.entity.faixaEtariaId = registro.id;
                this.entity.faixaEtaria = registro;
                return registro.descricao;
            }

            this.faixaEtariaDeselecionado = () => {
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaFaixaEtaria = (termoDigitadoPesquisa) => {
                var filtro = new faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectFaixaEtaria = new ezSelect.EzSelect.EzSelect<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.faixaEtariaService,
                this.getFiltroParaFaixaEtaria,
                this.requestParamsFaixaEtaria,
                this.$uibModal,
                this.faixaEtariaSelecionado,
                this.faixaEtariaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectFaixaEtaria.onEzGridCreated = () => {

                this.ezSelectFaixaEtaria.ezGrid.serviceGetCallback = () => {
                    var getids = new faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaExceptForIdsInput();
                    var ids = new Array();
                    ids.push(this.entity.faixaEtariaId);
                    getids.ids = ids;
                    return this.faixaEtariaService.getPaginadoExceptForIds(getids);
                };

                this.ezSelectFaixaEtaria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('FaixaEtaria.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectFaixaEtaria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('FaixaEtaria.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectFaixaEtaria.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectFaixaEtaria.on.beforeOpenModalDialog = () => {
                this.ezSelectFaixaEtaria.ezGrid.getRegistros();
            };
        }

        private ezSelectOperadoraConfig() {
            this.requestParamsOperadora = new requestParam.RequestParam.RequestParams();

            this.operadoraSelecionado = (registro) => {
                this.entity.operadoraId = registro.id;
                this.entity.operadora = registro;
                return registro.nome;
            }

            this.operadoraDeselecionado = () => {
                this.entity.operadoraId = null;
                this.entity.operadora = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoOperadora = (termoDigitadoPesquisa) => {
                var filtro = new operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectOperadora = new ezSelect.EzSelect.EzSelect<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.operadoraService,
                this.getFiltroParaPaginacaoOperadora,
                this.requestParamsOperadora,
                this.$uibModal,
                this.operadoraSelecionado,
                this.operadoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectOperadora.onEzGridCreated = () => {
                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Operadora.Nome'),
                    field: 'nome'
                });

                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica'),
                    field: 'nomePessoa'
                });

                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Operadora.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectOperadora.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectOperadora.on.beforeOpenModalDialog = () => {
                this.ezSelectOperadora.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class IndiceDeReajustePorFaixaEtariaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaForm.cshtml';
            this.controller = IndiceDeReajustePorFaixaEtariaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}