﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as questionarioDeDeclaracaoDeSaudeService from "../../../../services/domain/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeService";
import * as operadoraService from "../../../../services/domain/ezliv/operadora/operadoraService";
import * as itemDeDeclaracaoDeSaudeService from "../../../../services/domain/ezliv/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as questionarioDeDeclaracaoDeSaudeDtos from "../../../../dtos/ezliv/geral/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeDtos";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as perguntaDtos from "../../../../dtos/ezliv/geral/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeDtos";

export module Forms {
    export class QuestionarioDeDeclaracaoDeSaudeFormController extends controllers.Controllers.ControllerFormComponentBase<questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto, questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        private createDescription: () => any;

        public getFiltroParaPaginacaoOperadora: (termoDigitadoPesquisa: string) => any;
        public ezSelectOperadora: ezSelect.EzSelect.EzSelect<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaOperadora: (termoDigitadoPesquisa: string) => any;
        private operadoraSelecionado: (registro: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto) => string;
        private operadoraDeselecionado: () => void;
        private requestParamsOperadora: requestParam.RequestParam.RequestParams;
        private tipoDeItemDeDeclaracaoEnum: any;

        //Perguntas
        public addPergunta: () => void;
        public limparPergunta: () => void;
        public getPerguntaInput: () => void;
        public perguntaCurrent: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput;
        public savingPergunta: boolean;
        public editingPergunta: boolean;
        public editarPergunta: (valor: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput) => void;
        public excluirPergunta: (pergunta: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput) => void;
        public cancelEditPergunta: () => void;

        // Grid Perguntas
        private setDataGridInputVigencia: (result: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput[]) => void;
        private gridApiInputVigencia: uiGrid.IGridApiOf<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput>;
        private requestParamsValoresInputVigencia: requestParam.RequestParam.RequestParams;
        private perguntaAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput>;
        public loadingPerguntaInput: boolean;
        public perguntaGridOptionsInput: uiGrid.IGridOptionsOf<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput>;
        private gridDataInputPergunta: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput[];
        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'questionarioDeDeclaracaoDeSaudeService', 'operadoraService', 'itemDeDeclaracaoDeSaudeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public questionarioDeDeclaracaoDeSaudeService: questionarioDeDeclaracaoDeSaudeService.Services.QuestionarioDeDeclaracaoDeSaudeService,
            public operadoraService: operadoraService.Services.OperadoraService,
            public itemDeDeclaracaoDeSaudeService: itemDeDeclaracaoDeSaudeService.Services.ItemDeDeclaracaoDeSaudeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                questionarioDeDeclaracaoDeSaudeService,
                'tenant.ezliv.questionarioDeDeclaracaoDeSaude');


            //Pergunta
            this.addPergunta = () => {
                this.perguntaCurrent.isActive = true;
                if (this.perguntaCurrent.id > 0) {
                    this.itemDeDeclaracaoDeSaudeService.save(this.perguntaCurrent);
                } else if (!containsArray(this.entity.itensDeDeclaracaoDeSaude, this.perguntaCurrent)) {
                    this.entity.itensDeDeclaracaoDeSaude.push(this.perguntaCurrent);
                }

                this.getPerguntaInput();
                this.limparPergunta();
            }

            this.limparPergunta = () => {
                this.perguntaCurrent = new perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput();
            }

            this.getPerguntaInput = () => {
                this.loadingEntity = true;
                if (this.entity) {
                    this.setDataGridInputVigencia(this.entity.itensDeDeclaracaoDeSaude);
                }

                this.loadingEntity = false;
            }

            this.excluirPergunta = (pergunta: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput) => {
                this.editingPergunta = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.itensDeDeclaracaoDeSaude.length; i++) {
                                if (this.entity.itensDeDeclaracaoDeSaude[i].id === pergunta.id) {
                                    this.entity.itensDeDeclaracaoDeSaude.splice(i, 1);
                                    this.getPerguntaInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.editarPergunta = (valor: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput) => {
                this.editingPergunta = true;
                this.perguntaCurrent = valor;
                this.loadingPerguntaInput = false;
            }

            this.setDataGridInputVigencia = (result: perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput[]) => {
                this.gridDataInputPergunta.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInputPergunta.push(result[i]);
                }

                this.perguntaGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditPergunta = () => {
                this.perguntaCurrent = new perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput;
            }
        }

        $onInit() {
            super.init();

            this.tipoDeItemDeDeclaracaoEnum = ez.domain.enum.tipoDeItemDeDeclaracaoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeInput();
                instance.id = 0;
                instance.isActive = true;
                instance.itensDeDeclaracaoDeSaude = new Array<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput>();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                if (entity.operadoraId) {
                    this.ezSelectOperadora.loading = true;
                    var promiseOperadora = this.operadoraService.getById(entity.operadoraId);
                    promiseOperadora.then(result => {
                        this.ezSelectOperadora.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectOperadora.loading = false;
                    });
                }

                this.getPerguntaInput();
            }

            this.ezSelectOperadoraConfig();
            this.perguntaGridConfigInput();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectOperadoraConfig() {
            this.requestParamsOperadora = new requestParam.RequestParam.RequestParams();

            this.operadoraSelecionado = (registro) => {
                this.entity.operadoraId = registro.id;
                return registro.nome;
            }

            this.operadoraDeselecionado = () => {
                this.entity.operadoraId = null;
                this.entity.operadora = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoOperadora = (termoDigitadoPesquisa) => {
                var filtro = new operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectOperadora = new ezSelect.EzSelect.EzSelect<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.operadoraService,
                this.getFiltroParaPaginacaoOperadora,
                this.requestParamsOperadora,
                this.$uibModal,
                this.operadoraSelecionado,
                this.operadoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectOperadora.onEzGridCreated = () => {
                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Operadora.Nome'),
                    field: 'nome'
                });

                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica'),
                    field: 'nomePessoa'
                });

                this.ezSelectOperadora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Operadora.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectOperadora.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectOperadora.on.beforeOpenModalDialog = () => {
                this.ezSelectOperadora.ezGrid.getRegistros();
            };
        }

        private perguntaGridConfigInput() {
            this.requestParamsValoresInputVigencia = new requestParam.RequestParam.RequestParams();

            this.perguntaAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput>();

            this.perguntaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarPergunta, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ItemDeDeclaracaoDeSaude.Editar');
                }
            ));


            this.perguntaAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirPergunta, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.ItemDeDeclaracaoDeSaude.Delete');
                }
            ));

            this.gridDataInputPergunta = new Array<perguntaDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput>();

            this.perguntaGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.perguntaAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInputVigencia = api;
                    this.gridApiInputVigencia.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInputVigencia.sorting = null;
                        } else {
                            this.requestParamsValoresInputVigencia.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getPerguntaInput();
                    });
                    this.gridApiInputVigencia.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInputVigencia.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInputVigencia.maxResultCount = pageSize;

                        this.getPerguntaInput();
                    });
                }
            }

            this.perguntaGridOptionsInput.data = this.gridDataInputPergunta;

            this.perguntaGridOptionsInput.columnDefs.push({
                name: app.localize('ItemDeDeclaracaoDeSaude.Pergunta'),
                field: 'pergunta',
                cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
            });

            this.perguntaGridOptionsInput.columnDefs.push({
                name: app.localize('ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao'),
                field: 'tipoDeItemDeDeclaracao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeItemDeDeclaracaoEnum", "tipoDeItemDeDeclaracao")}}\
                </div>'
            });

            this.perguntaGridOptionsInput.columnDefs.push({
                name: app.localize('ItemDeDeclaracaoDeSaude.Ordenacao'),
                field: 'ordenacao',
                enableSorting: true,
                sort: { direction: this.uiGridConstants.ASC },
                type: 'number'
            });

            this.getPerguntaInput();
        }
    }

    function containsArray(arr, findValue) {
        var i = arr.length;

        while (i--) {
            if (arr[i] === findValue) return true;
        }
        return false;
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class QuestionarioDeDeclaracaoDeSaudeFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeForm.cshtml';
            this.controller = QuestionarioDeDeclaracaoDeSaudeFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}