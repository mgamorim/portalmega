﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as questionarioDeDeclaracaoDeSaudeService from "../../../../services/domain/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeService";
import * as questionarioDeDeclaracaoDeSaudeDtos from "../../../../dtos/ezliv/geral/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.QuestionarioDeDeclaracaoDeSaude {

    class QuestionarioDeDeclaracaoDeSaudeIndexController extends controllers.Controllers.ControllerIndexBase <questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto, questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeInput>{

        static $inject = ['$state', 'uiGridConstants', 'questionarioDeDeclaracaoDeSaudeService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public questionarioDeDeclaracaoDeSaudeService: questionarioDeDeclaracaoDeSaudeService.Services.QuestionarioDeDeclaracaoDeSaudeService) {

            super(
                $state,
                uiGridConstants,
                questionarioDeDeclaracaoDeSaudeService, {
                    rotaAlterarRegistro: 'tenant.ezliv.questionarioDeDeclaracaoDeSaude.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.questionarioDeDeclaracaoDeSaude.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.GetQuestionarioDeDeclaracaoDeSaudeInput();
                filtro.nome = termoDigitadoPesquisa; 
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.questionarioDeDeclaracaoDeSaudeService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewQuestionarioDeDeclaracaoDeSaude', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.QuestionarioDeDeclaracaoDeSaude.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('QuestionarioDeDeclaracaoDeSaude.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('QuestionarioDeDeclaracaoDeSaude.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('QuestionarioDeDeclaracaoDeSaude.Operadora'),
                field: 'operadora.nome'
            });
        }
    }

    export class QuestionarioDeDeclaracaoDeSaudeIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeIndex.cshtml';
            this.controller = QuestionarioDeDeclaracaoDeSaudeIndexController;
        }
    }
}