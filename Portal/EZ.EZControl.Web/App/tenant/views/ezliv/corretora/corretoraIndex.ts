﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as corretoraService from "../../../../services/domain/ezliv/corretora/corretoraService";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Corretora {

    class CorretoraIndexController extends
        controllers.Controllers.ControllerIndexBase<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto,
        corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput> {

        static $inject = ['$state', 'uiGridConstants', 'corretoraService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public corretoraService: corretoraService.Services.CorretoraService) {

            super(
                $state,
                uiGridConstants,
                corretoraService, {
                    rotaAlterarRegistro: 'tenant.ezliv.corretora.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.corretora.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'PessoaJuridica.NomeFantasia';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new corretoraDtos.Dtos.EZLiv.Geral.GetCorretoraInput();
                filtro.razaoSocial = termoDigitadoPesquisa;
                filtro.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretoraService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewCorretora', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretora.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretora.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretora.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Corretora.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Corretora.NomeFantasia'),
                field: 'pessoaJuridica.nomeFantasia'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Corretora.RazaoSocial'),
                field: 'pessoaJuridica.razaoSocial'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Corretora.CorretorPadrao'),
                field: 'corretor.pessoa.nomePessoa'
            });
            
        }
    }

    export class CorretoraIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/corretora/corretoraIndex.cshtml';
            this.controller = CorretoraIndexController;
        }
    }
}