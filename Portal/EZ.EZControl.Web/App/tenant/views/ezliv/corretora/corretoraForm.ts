﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezFile from "../../../../common/ezFile/ezFile";
// Servicos/DTOS
import * as corretoraService from "../../../../services/domain/ezliv/corretora/corretoraService";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as parametroPagSeguroDtos from "../../../../dtos/ezpag/parametroPagSeguro/parametroPagSeguroDtos";
import * as depositoTransferenciaBancariaDtos from "../../../../dtos/ezpag/depositoTransferenciaBancaria/depositoTransferenciaBancariaDtos";
import * as parametroPagSeguroService from "../../../../services/domain/ezpag/parametroPagSeguro/parametroPagSeguroService";
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";
import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as arquivoDtos from "../../../../dtos/core/arquivo/arquivoDtos";
//Corretor
import * as corretorService from "../../../../services/domain/ezliv/corretor/corretorService";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
//GerenteCorretora
import * as gerenteService from "../../../../services/domain/ezliv/gerenteCorretora/gerenteCorretoraService";
import * as gerenteDtos from "../../../../dtos/ezliv/subtipoPessoa/gerenteCorretora/gerenteCorretoraDtos";
//SupervisorCorretora
import * as supervisorService from "../../../../services/domain/ezliv/supervisorCorretora/supervisorCorretoraService";
import * as supervisorDtos from "../../../../dtos/ezliv/subtipoPessoa/supervisorCorretora/supervisorCorretoraDtos";

export module Forms {
    export class CorretoraFormController extends
        controllers.Controllers.ControllerFormComponentBase<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput,
        corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        public tipoPessoaEnum: any;
        public corretoraForm: ng.IFormController;
        public pagSeguroForm: ng.IFormController;

        // pessoa
        public pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        public ezSelectPessoaJuridica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaJuridica: (termoDigitadoPesquisa: string) => any;
        private pessoaJuridicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto) => string;
        private pessoaJuridicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaJuridica: (id: number) => void;

        //ParametroPagSeguro
        public parametroPagSeguroCurrent: parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput;

        //DepositoTransferenciaBancaria
        public depositoTransferenciaBancariaCurrent: depositoTransferenciaBancariaDtos.Dtos.EZPag.DepositoTransferenciaBancariaDtos.DepositoTransferenciaBancariaInput;

        public ezFile: ezFile.EZFile.EZFile;
        private imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;

        public upload: () => void;

        //Corretor
        public pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        public corretor: corretorDtos.Dtos.EZLiv.Geral.CorretorInput;
        public ezSelectCorretor: ezSelect.EzSelect.EzSelect<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCorretor: (termoDigitadoPesquisa: string) => any;
        private corretorSelecionado: (registro: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto) => string;
        private corretorDeselecionado: () => void;
        private requestParamsCorretor: requestParam.RequestParam.RequestParams;
        private carregarCorretor: (id: number) => void;

        // EzSelec GerenteCorretora
        public ezSelectGerente: ezSelect.EzSelect.EzSelect<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaGerenteCorretora: (termoDigitadoPesquisa: string) => any;
        private gerenteCorretoraSelecionado: (registro: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto) => string;
        private gerenteCorretoraDeselecionado: () => void;
        private requestParamsGerenteCorretora: requestParam.RequestParam.RequestParams;

        // EzSelec SupervisorCorretora
        public ezSelectSupervisor: ezSelect.EzSelect.EzSelect<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaSupervisorCorretora: (termoDigitadoPesquisa: string) => any;
        private supervisorCorretoraSelecionado: (registro: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto) => string;
        private supervisorCorretoraDeselecionado: () => void;
        private requestParamsSupervisorCorretora: requestParam.RequestParam.RequestParams;

        // Grid Gerentes
        private gridDataGerentesEnumerable: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto[];
        private gerenteAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>;
        public gerenteGridOptions: uiGrid.IGridOptionsOf<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput>;
        public editarGerente: (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto) => void;
        public excluirGerente: (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto) => void;
        public getGerenteGrid: () => void;

        private requestParamsGridGerente: requestParam.RequestParam.RequestParams;
        public gerenteCurrent: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput;
        public loadingGerenteInput: boolean;
        public editandoGerente: boolean;
        public limparGerente: () => void;
        public cancelEditGerente: () => void;
        private idsGerentes: number[];

        // Grid Supervisores
        private gridDataSupervisoresEnumerable: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto[];
        private supervisorAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>;
        public supervisorGridOptions: uiGrid.IGridOptionsOf<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput>;
        public editarSupervisor: (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto) => void;
        public excluirSupervisor: (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto) => void;
        public getSupervisorGrid: () => void;

        private requestParamsGridSupervisor: requestParam.RequestParam.RequestParams;
        public supervisorCurrent: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput;
        public loadingSupervisorInput: boolean;
        public editandoSupervisor: boolean;
        public limparSupervisor: () => void;
        public cancelEditSupervisor: () => void;
        private idsSupervisores: number[];

        static $inject = ['$scope', '$rootScope', '$state', '$stateParams', 'uiGridConstants', 'corretoraService', 'pessoaJuridicaService', 'parametroPagSeguroService', 'corretorService', '$uibModal', 'gerenteCorretoraService', 'supervisorCorretoraService', 'arquivoGlobalService', 'parametroGlobalService'];

        constructor(
            public $scope: ng.IScope,
            public $rootScope: any,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public corretoraService: corretoraService.Services.CorretoraService,
            public pessoaJuridicaService: pessoaJuridicaService.Services.PessoaJuridicaService,
            public parametroPagSeguroService: parametroPagSeguroService.Services.ParametroPagSeguroService,
            public corretorService: corretorService.Services.CorretorService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public gerenteService: gerenteService.Services.GerenteCorretoraService,
            public supervisorService: supervisorService.Services.SupervisorCorretoraService,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                corretoraService,
                'tenant.ezliv.corretora');

            this.editandoGerente = false;
            this.editandoSupervisor = false;

            this.carregarPessoaJuridica = (id: number) => {
                this.ezSelectPessoaJuridica.loading = true;
                var promise = this.pessoaJuridicaService.getById(id);
                promise.then(result => {
                    this.pessoaJuridica = result;
                    this.entity.pessoaJuridicaId = this.pessoaJuridica.id;
                    this.ezSelectPessoaJuridica.setInputText(result.nomeFantasia);
                }).finally(() => {
                    this.ezSelectPessoaJuridica.loading = false;
                });
            }

            this.carregarCorretor = (id: number) => {
                this.ezSelectCorretor.loading = true;
                var promise = this.corretorService.getById(id);
                promise.then(result => {
                    this.corretor = result;
                    this.entity.corretorId = this.corretor.id;
                    this.ezSelectCorretor.setInputText(result.pessoa.nomePessoa);
                }).finally(() => {
                    this.ezSelectCorretor.loading = false;
                });
            }

            //Gerente
            this.getGerenteGrid = () => {
                if (this.gerenteGridOptions) {
                    this.gerenteGridOptions.data = this.entity ? this.entity.gerentes : null;
                    this.gerenteGridOptions.totalItems = this.entity && this.entity.gerentes ? this.entity.gerentes.length : 0;
                }
            }

            this.cancelEditGerente = () => {
                this.gerenteCurrent = new gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput();
                this.ezSelectGerente.gridSelectClear();
                this.editandoGerente = false;
            }

            this.editarGerente = (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput) => {
                this.editandoGerente = true;
                this.gerenteCurrent = valor;
                this.ezSelectGerente.setInputText(valor.pessoa.nomePessoa);
                this.loadingGerenteInput = false;
            }

            this.excluirGerente = (valor: gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto) => {
                this.editandoGerente = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            
                            var promise = this.gerenteService.delete(valor.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarGerenteCurrent();
                            }).finally(() => {
                                
                            });

                            var index = this.entity.gerentes.indexOf(valor);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.gerentes.splice(index, 1);
                            }
                            this.editandoGerente = false;
                        }
                    }
                );
            }

            //Supervisor
            this.getSupervisorGrid = () => {
                if (this.supervisorGridOptions) {
                    this.supervisorGridOptions.data = this.entity ? this.entity.supervisores : null;
                    this.supervisorGridOptions.totalItems = this.entity && this.entity.supervisores ? this.entity.supervisores.length : 0;
                }
            }

            this.cancelEditSupervisor = () => {
                this.supervisorCurrent = new supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput();
                this.ezSelectSupervisor.gridSelectClear();
                this.editandoSupervisor = false;
            }

            this.editarSupervisor = (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput) => {
                this.editandoSupervisor = true;
                this.supervisorCurrent = valor;
                this.ezSelectSupervisor.setInputText(valor.pessoa.nomePessoa);
                this.loadingSupervisorInput = false;
            }

            this.excluirSupervisor = (valor: supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto) => {
                this.editandoSupervisor = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            
                            var promise = this.supervisorService.delete(valor.id);
                            promise.then(result => {
                                abp.notify.info(app.localize('SavedSuccessfully'), "");
                                this.inicializarSupervisorCurrent();
                            }).finally(() => {
                                
                            });

                            var index = this.entity.supervisores.indexOf(valor);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.supervisores.splice(index, 1);
                            }
                            this.editandoSupervisor = false;
                        }
                    }
                );
            }
        }

        $onInit() {
            super.init();

            this.ezFile = new ezFile.EZFile.EZFile();

            this.imagem = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            this.imagem.id = 0;
            this.imagem.isActive = true;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
                instance.id = 0;
                instance.isActive = true;
                instance.imagemId = 0;
                instance.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
                instance.pessoaJuridica = TipoPessoaEnum.Juridica;
                instance.corretor = new corretorDtos.Dtos.EZLiv.Geral.CorretorInput;
                instance.gerentes = new Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput>();
                instance.supervisores = new Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput>();
                instance.depositoTransferenciaBancaria = new depositoTransferenciaBancariaDtos.Dtos.EZPag.DepositoTransferenciaBancariaDtos.DepositoTransferenciaBancariaInput;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaJuridica) {
                    this.carregarPessoaJuridica(entity.pessoaJuridicaId);
                }

                if (entity.corretor) {
                    this.carregarCorretor(entity.corretorId);
                }

                if (entity.parametroPagSeguro != null) {
                    this.parametroPagSeguroCurrent = new parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput();
                    this.parametroPagSeguroCurrent.email = entity.parametroPagSeguro.email;
                    this.parametroPagSeguroCurrent.token = entity.parametroPagSeguro.token;
                    this.parametroPagSeguroCurrent.pessoaJuridicaId = entity.parametroPagSeguro.pessoaJuridicaId;
                    this.parametroPagSeguroCurrent.appId = entity.parametroPagSeguro.appId;
                    this.parametroPagSeguroCurrent.appKey = entity.parametroPagSeguro.appKey;
                }

                if (entity.depositoTransferenciaBancaria != null) {
                    this.depositoTransferenciaBancariaCurrent = new depositoTransferenciaBancariaDtos.Dtos.EZPag.DepositoTransferenciaBancariaDtos.DepositoTransferenciaBancariaInput();
                    this.depositoTransferenciaBancariaCurrent.id = entity.depositoTransferenciaBancaria.id;
                    this.depositoTransferenciaBancariaCurrent.banco = entity.depositoTransferenciaBancaria.banco;
                    this.depositoTransferenciaBancariaCurrent.agencia = entity.depositoTransferenciaBancaria.agencia;
                    this.depositoTransferenciaBancariaCurrent.conta = entity.depositoTransferenciaBancaria.conta;
                    this.depositoTransferenciaBancariaCurrent.titular = entity.depositoTransferenciaBancaria.titular;
                    this.depositoTransferenciaBancariaCurrent.empresaId = entity.depositoTransferenciaBancaria.empresaId;
                    this.depositoTransferenciaBancariaCurrent.pessoaJuridicaId = entity.depositoTransferenciaBancaria.pessoaJuridicaId;
                    this.depositoTransferenciaBancariaCurrent.comprovante = entity.depositoTransferenciaBancaria.comprovante;
                }

                this.ezFile.controller.arquivoId = entity.imagemId;
                this.ezFile.initialize();

                this.getGerenteGrid();
                this.getSupervisorGrid();
            };
            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
                this.entity.parametroPagSeguro = this.parametroPagSeguroCurrent;
                this.entity.depositoTransferenciaBancaria = this.depositoTransferenciaBancariaCurrent;
                this.entity.corretor = this.corretor;

            };

            this.events.onBeforeSaveEntity = () => {
                if (this.parametroPagSeguroCurrent != null) {
                    this.parametroPagSeguroCurrent.pessoaJuridicaId = this.entity.pessoaJuridicaId;
                    this.entity.parametroPagSeguro = this.parametroPagSeguroCurrent;
                }
                if (this.depositoTransferenciaBancariaCurrent != null) {
                    this.entity.depositoTransferenciaBancaria = this.depositoTransferenciaBancariaCurrent;
                    this.entity.depositoTransferenciaBancaria.pessoaJuridicaId = this.entity.pessoaJuridicaId;
                }
            };

            this.ezSelectPessoaConfig();
            this.ezSelectCorretorConfig();
            this.ezSelectGerenteConfig();
            this.ezSelectSupervisorConfig();

            this.gerenteGridConfig();
            this.supervisorGridConfig();
            this.ezFileConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private gerenteGridConfig() {
            this.requestParamsGridGerente = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridGerente.sorting = 'Id';

            this.gerenteAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>();
            this.gerenteAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarGerente, 'Edit', true));
            this.gerenteAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirGerente, 'Delete', true));

            this.gerenteGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.gerenteAppScopeProvider
            }

            var lista = this.entity ? this.entity.gerentes : new Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>();
            this.gerenteGridOptions.data = lista;
            this.gerenteGridOptions.totalItems = lista.length;

            this.gerenteGridOptions.columnDefs.push({
                name: app.localize('Gerente.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();

            this.getGerenteGrid();
        }

        private supervisorGridConfig() {
            this.requestParamsGridSupervisor = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridSupervisor.sorting = 'Id';

            this.supervisorAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>();
            this.supervisorAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarSupervisor, 'Edit', true));
            this.supervisorAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirSupervisor, 'Delete', true));

            this.supervisorGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.supervisorAppScopeProvider
            }

            var lista = this.entity ? this.entity.supervisores : new Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>();
            this.supervisorGridOptions.data = lista;
            this.supervisorGridOptions.totalItems = lista.length;

            this.supervisorGridOptions.columnDefs.push({
                name: app.localize('Supervisor.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();

            this.getSupervisorGrid();
        }

        private inicializarGerenteCurrent() {
            this.gerenteCurrent = new gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput();
            this.gerenteCurrent.isActive = true;
            if (this.entity)
                this.gerenteCurrent.corretoraId = this.entity.id;
        }

        private gravarGerente(item: any) {
            this.saving = true;
            this.gerenteCurrent.corretoraId = this.entity.id;
            var promise = this.gerenteService.save(item);
            promise.then((result) => {
                this.gerenteCurrent.id = result.id;
                if (!this.editandoGerente) {
                    if (!this.entity.gerentes)
                        this.entity.gerentes = new Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>();

                    this.entity.gerentes.push(this.gerenteCurrent as gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto);
                }
                this.editandoGerente = false;
                this.cancelEditGerente();
                this.inicializarGerenteCurrent();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarGerente() {
            if (!this.editandoGerente) {
                this.gravarGerente(this.gerenteCurrent);
            }
            else {
                var item = this.getGerenteByTempId(this.gerenteCurrent.id);
                angular.merge(item, this.gerenteCurrent);
                this.gravarGerente(item);
            }
        }

        private getGerenteByTempId(tempId: number): gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto {
            for (let i = 0; i < this.entity.gerentes.length; i++) {
                if (this.entity.gerentes[i].id === tempId) {
                    return this.entity.gerentes[i] as gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto;
                }
            }
        }

        private inicializarSupervisorCurrent() {
            this.supervisorCurrent = new supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput();
            this.supervisorCurrent.isActive = true;
            if (this.entity)
                this.supervisorCurrent.corretoraId = this.entity.id;
        }

        private gravarSupervisor(item: any) {
            this.saving = true;
            this.supervisorCurrent.corretoraId = this.entity.id;
            var promise = this.supervisorService.save(item);
            promise.then((result) => {
                this.supervisorCurrent.id = result.id;
                if (!this.editandoSupervisor) {

                    if (!this.entity.supervisores)
                        this.entity.supervisores = new Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>();

                    this.entity.supervisores.push(this.supervisorCurrent as supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto);
                }
                this.editandoSupervisor = false;
                this.getSupervisorGrid();
                this.cancelEditSupervisor();
                abp.notify.info(app.localize('SavedSuccessfully'), "");
            }).finally(() => {
                this.saving = false;
            });
        }

        private salvarSupervisor() {
            if (!this.editandoSupervisor) {
                this.gravarSupervisor(this.supervisorCurrent);
            }
            else {
                var item = this.getSupervisorByTempId(this.supervisorCurrent.id);
                angular.merge(item, this.supervisorCurrent);
                this.gravarSupervisor(item);
            }
        }

        private getSupervisorByTempId(tempId: number): supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto {
            for (let i = 0; i < this.entity.supervisores.length; i++) {
                if (this.entity.supervisores[i].id === tempId) {
                    return this.entity.supervisores[i] as supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto;
                }
            }
        }

        

        setCorretoraForm(form: ng.IFormController) {
            this.corretoraForm = form;
        }

        setPagSeguroForm(form: ng.IFormController) {
            this.pagSeguroForm = form;
        }

        private inicializarPagSeguroItemCurrent() {
            this.parametroPagSeguroCurrent = new parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput();
            this.parametroPagSeguroCurrent.email = "";
            this.parametroPagSeguroCurrent.token = "";
            this.parametroPagSeguroCurrent.appId = "";
            this.parametroPagSeguroCurrent.appKey = "";

            this.depositoTransferenciaBancariaCurrent = new depositoTransferenciaBancariaDtos.Dtos.EZPag.DepositoTransferenciaBancariaDtos.DepositoTransferenciaBancariaInput();
            this.depositoTransferenciaBancariaCurrent.banco = "";
            this.depositoTransferenciaBancariaCurrent.agencia = "";
            this.depositoTransferenciaBancariaCurrent.conta = "";
            this.depositoTransferenciaBancariaCurrent.titular = "";
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();

            this.pessoaJuridicaSelecionado = (registro) => {
                this.pessoaJuridica = registro;
                this.entity.pessoaJuridicaId = registro.id;
                this.entity.pessoaJuridica = registro;
                return registro.nomePessoa;
            }

            this.pessoaJuridicaDeselecionado = () => {
                this.pessoaJuridica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaJuridica = (termoDigitadoPesquisa) => {
                var filtro = new corretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora();
                filtro.nomePessoa = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaJuridica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaJuridicaService,
                this.getFiltroParaPaginacaoPessoaJuridica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaJuridicaSelecionado,
                this.pessoaJuridicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaJuridica.onEzGridCreated = () => {

                this.ezSelectPessoaJuridica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretoraService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.RazaoSocial'),
                    field: 'razaoSocial'
                });

                this.ezSelectPessoaJuridica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaJuridica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaJuridica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaJuridica.ezGrid.getRegistros();
            };
        }

        private ezSelectCorretorConfig() {
            this.requestParamsCorretor = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.corretorSelecionado = (registro) => {
                this.corretor = registro;
                this.entity.corretorId = registro.id;
                this.entity.corretor = registro;
                return registro.pessoa.nomePessoa;
            }

            this.corretorDeselecionado = () => {
                this.corretor = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCorretor = (termoDigitadoPesquisa) => {
                var filtro = new corretorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretor();
                filtro.nome = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectCorretor = new ezSelect.EzSelect.EzSelect<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretorService,
                this.getFiltroParaPaginacaoCorretor,
                this.requestParamsCorretor,
                this.$uibModal,
                this.corretorSelecionado,
                this.corretorDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCorretor.onEzGridCreated = () => {

                this.ezSelectCorretor.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectCorretor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('CorretorPessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectCorretor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCorretor.on.beforeOpenModalDialog = () => {
                this.ezSelectCorretor.ezGrid.getRegistros();
            };
        }

        private ezSelectGerenteConfig() {
            this.requestParamsGerenteCorretora = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.gerenteCorretoraSelecionado = (registro) => {
                this.gerenteCurrent = registro as gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput;
                return registro.pessoa.nomePessoa;
            }

            this.gerenteCorretoraDeselecionado = () => {
                this.gerenteCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaGerenteCorretora = (termoDigitadoPesquisa) => {
                var filtro = new gerenteDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteCorretora();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGerente = new ezSelect.EzSelect.EzSelect<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.gerenteService,
                this.getFiltroParaGerenteCorretora,
                this.requestParamsGerenteCorretora,
                this.$uibModal,
                this.gerenteCorretoraSelecionado,
                this.gerenteCorretoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGerente.onEzGridCreated = () => {

                this.ezSelectGerente.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPaged(filtro, requestParams);
                };

                this.ezSelectGerente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectGerente.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGerente.on.beforeOpenModalDialog = () => {
                this.ezSelectGerente.ezGrid.getRegistros();
            };
        }

        private ezSelectSupervisorConfig() {
            this.requestParamsSupervisorCorretora = new requestParam.RequestParam.RequestParams();
            //this.requestParamsPessoa.sorting = "Nome";

            this.supervisorCorretoraSelecionado = (registro) => {
                this.supervisorCurrent = registro as supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput;
                return registro.pessoa.nomePessoa;
            }

            this.supervisorCorretoraDeselecionado = () => {
                this.supervisorCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaSupervisorCorretora = (termoDigitadoPesquisa) => {
                var filtro = new supervisorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorCorretora();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSupervisor = new ezSelect.EzSelect.EzSelect<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.supervisorService,
                this.getFiltroParaSupervisorCorretora,
                this.requestParamsSupervisorCorretora,
                this.$uibModal,
                this.supervisorCorretoraSelecionado,
                this.supervisorCorretoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSupervisor.onEzGridCreated = () => {

                this.ezSelectSupervisor.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPaged(filtro, requestParams);
                };

                this.ezSelectSupervisor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectSupervisor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSupervisor.on.beforeOpenModalDialog = () => {
                this.ezSelectSupervisor.ezGrid.getRegistros();
            };
        }

        private ezFileConfig() {
          this.ezFile.loadImage = () => {
            var promise = this.arquivoService.getById(this.entity.imagemId);
            promise.then((result) => {
              this.ezFile.setImageInfo(result.path, result.tipoDeArquivoFixo);
            });
          }
          this.ezFile.getParametros = () => {
            var promise = this.parametroGlobalService.get();
            promise.then((result) => {
              this.ezFile.setParametos(result.extensoesImagem);
            });
          }
          this.ezFile.onUploadComplete = (fileName: string) => {
            var input = new arquivoDtos.Dtos.Arquivo.Core.ArquivoInput();
            input.nome = fileName;
            var promise = this.arquivoService.getIdByName(input);
            promise.then((result) => {
              this.entity.imagemId = result.id;
            });
          }
          this.ezFile.initialize = () => {
            this.ezFile.inicializado = true;
            this.ezFile.controller.sistema = "Estoque";
            this.ezFile.controller.autoUpload = true;
            if (this.ezFile.controller.arquivoId > 0) {
              if (this.ezFile.loadImage) {
                this.ezFile.loadImage();
              }
            }
            if (this.ezFile.onUploadComplete) {
              this.ezFile.controller.onUploadComplete = this.ezFile.onUploadComplete;
            }
          }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class CorretoraFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/corretora/corretoraForm.cshtml';
            this.controller = CorretoraFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}