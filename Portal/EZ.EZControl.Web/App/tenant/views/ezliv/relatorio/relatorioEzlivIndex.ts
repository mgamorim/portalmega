﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as relatorioEzlivService from "../../../../services/domain/ezliv/relatorio/relatorioEzlivService";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Relatorio {

    class RelatorioIndexController extends
      controllers.Controllers.ControllerIndexBase<relatorioDtos.Dtos.Global.Geral.RelatorioListDto,
      relatorioDtos.Dtos.Global.Geral.RelatorioInput> {

        static $inject = ['$state', 'uiGridConstants', 'relatorioEzlivService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<relatorioDtos.Dtos.Global.Geral.RelatorioListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public relatorioEzlivService: relatorioEzlivService.Services.RelatorioEzlivService) {

            super(
                $state,
                uiGridConstants,
                relatorioEzlivService, {
                    rotaAlterarRegistro: 'tenant.ezliv.relatorio.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.relatorio.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new relatorioDtos.Dtos.Global.Geral.GetRelatorioInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.sistema = SistemaEnum.EZLiv;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<relatorioDtos.Dtos.Global.Geral.RelatorioListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.relatorioEzlivService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewRelatorio', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Relatorio.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Relatorio.TipoDeRelatorio'),
                cellTemplate: '\
                  <div class="ui-grid-cell-contents">\
                      {{grid.appScope.getEnumValue(row.entity, "tipoDeRelatorioEzlivEnum", "tipoDeRelatorio")}}\
                  </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
              name: app.localize('Relatorio.Ativo'),
              field: 'isActive',
              cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
          
        }
    }

    export class RelatorioEzlivIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/relatorio/relatorioEzlivIndex.cshtml';
            this.controller = RelatorioIndexController;
        }
    }
}