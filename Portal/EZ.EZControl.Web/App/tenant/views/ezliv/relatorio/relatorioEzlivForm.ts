﻿import * as relatorioEzlivService from "../../../../services/domain/ezliv/relatorio/relatorioEzlivService";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
  export class RelatorioFormController extends controllers.Controllers.ControllerFormComponentBase<relatorioDtos.Dtos.Global.Geral.RelatorioListDto, relatorioDtos.Dtos.Global.Geral.RelatorioInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
    public getFiltroParaPaginacaoRelatorio: (termoDigitadoPesquisa: string) => any;
    private relatorioSelecionado: (registro: relatorioDtos.Dtos.Global.Geral.RelatorioListDto) => string;
    private relatorioDeselecionado: () => void;
    private requestParamsRelatorio: requestParam.RequestParam.RequestParams;

    public duplicar: (registroId: number) => void;
    public editarConteudo: () => void;
    public download: () => void;
    public transformaConteudoUpload: (files: any) => void;

    private tipoDeRelatorioEzlivEnum: any;

    static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'relatorioEzlivService', '$uibModal'];

    constructor(
      public $scope: ng.IScope,
      public $state: ng.ui.IStateService,
      public $stateParams: ng.ui.IStateParamsService,
      public uiGridConstants: uiGrid.IUiGridConstants,
      public relatorioService: relatorioEzlivService.Services.RelatorioEzlivService,
      public $uibModal: angular.ui.bootstrap.IModalService) {
      super(
        $state,
        $stateParams,
        $uibModal,
        uiGridConstants,
        relatorioService,
        'tenant.ezliv.relatorio');
    }

    $onInit() {
      super.init();
      this.tipoDeRelatorioEzlivEnum = ez.domain.enum.tipoDeRelatorioEzlivEnum.valores;

      this.events.onGetNewEntityInstance = () => {
        // evento para obter uma nova instancia para a entidade da tela
        // e tambem para inicializar suas variavéis com valores padrões
        var instance = new relatorioDtos.Dtos.Global.Geral.RelatorioInput();
        instance.id = 0;
        instance.sistema = SistemaEnum.EZLiv;
        instance.isActive = true;
        return instance;
      };

      this.transformaConteudoUpload = (file: any) => {
          var reader = new FileReader();

          reader.addEventListener("load", () => {
              this.entity.conteudo = reader.result;

              abp.message.warn("Ao preencher esse campo você vai sobrescrever o conteúdo do relatório. Se isso não é o seu desejo, edite o conteúdo clicando no botão 'EDITAR CONTEÚDO'", "Edição do conteúdo via upload");

              this.$scope.$apply();
          }, false);

          if (file) {
              reader.readAsText(file);
          }
      }

      this.download = () => {
          if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio')) {
              window.open('/relatorio/downloadMrt?id=' + this.entity.id);
          }
      }

      this.duplicar = () => {
          if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Create')) {
              this.relatorioService.duplicar(this.entity).then((relatorio) => {
                  abp.notify.success("Relatório duplicado com sucesso!", "Duplicar relatório");
                  window.open('/#/tenant/ezliv/relatorio/alterar/' + relatorio.id);
              });
          }
      }

      this.editarConteudo = () => {
          if (abp.auth.hasPermission('Pages.Tenant.EZLiv.Relatorio.Edit')) {
              window.open('/relatorio/editor?sistema=' + SistemaEnum.EZLiv + '&tipoDeRelatorio=' + this.entity.tipoDeRelatorio + '&relatorioId=' + this.entity.id);
          }
      }

      // método para carregar ou criar uma nova instancia da entidade.
      // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
      this.prepareEntityInstance();
    }
  }

  /*
  Os bindings do componente funcionam da seguinte maneira:
      @: deve ser usado para strings
      <: objetos
      &: funcoes
      =: não deve ser mais usado, está obsoleto
  */
  export class RelatorioEzlivFormComponent implements ng.IComponentOptions {
    public templateUrl: string;
    public controller: any;
    public bindings: any;

    constructor() {
      this.templateUrl = '~/app/tenant/views/ezliv/relatorio/relatorioEzlivForm.cshtml';
      this.controller = RelatorioFormController;
      this.bindings = {
        operation: '@'
      }
    }
  }
}