﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as meusProdutosService from "../../../../services/domain/ezliv/meusProdutos/meusProdutosService";
import * as meusProdutosDtos from "../../../../dtos/ezliv/geral/meusProdutos/meusProdutosDtos";
import * as produtoService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";
import * as produtoDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.MeusProdutos {

    class MeusProdutosIndexController extends
        controllers.Controllers.ControllerIndexBase<meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosListDto, meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput> {

        static $inject = ['$scope', '$state', 'uiGridConstants', 'meusProdutosService', '$uibModal', 'produtoDePlanoDeSaudeService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        public showMaisDetalhes: (registro: meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput) => void;
        public closeModal: () => void;
        public modalStatus: boolean = false;
        public modalInstance: any;

        public planoModal: produtoDtos.Dtos.EZLiv.Geral.DetalhesProdutoDePlanoDeSaudeInput;

        public loading: boolean = false;

        constructor(
            public $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public meusProdutosService: meusProdutosService.Services.MeusProdutosService,
            public uibModal: any,
            public produtoService: produtoService.Services.ProdutoDePlanoDeSaudeService) {

            super(
                $state,
                uiGridConstants,
                meusProdutosService, {
                    rotaAlterarRegistro: 'tenant.ezliv.administradora.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.administradora.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'Nome';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new meusProdutosDtos.Dtos.EZLiv.Geral.GetMeusProdutosInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.meusProdutosService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();

            this.showMaisDetalhes = (registro) => {
                var input = new produtoDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput();
                input.produtoId = registro.id;
                input.propostaId = registro.propostaId;
                var promise = this.produtoService.getDetalhesById(input);
                promise.then((result) => {
                    this.planoModal = result;
                    this.planoModal.valor = registro.valor;
                    this.modalStatus = true;
                    this.modalInstance = this.uibModal.open({
                        templateUrl: '~/App/common/views/modals/detalhesProdutoDePlanoDeSaude/detalhesProdutoDePlanoDeSaudeModal.cshtml',
                        controller: 'common.views.modals.detalhesPlano as $ctrl',
                        backdrop: 'static',
                        scope: this.$scope
                    });
                });
            };

            this.closeModal = () => {
                this.modalStatus = false;
                this.modalInstance.dismiss();
            }
        }

        private ezGridConfig() {
            this.ezGrid.optionsGrid.showColumnFooter = false;
            this.ezGrid.optionsGrid.enableGridMenu = false;

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                (registro, row) => {
                    var instance = (registro as meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput);
                    this.showMaisDetalhes(instance);
                },
                'MaisDetalhes',
                (row) => {
                    return true;
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(
                (registro, row) => {
                    this.$state.go('tenant.ezliv.propostaDeContratacao.alterar', { id: (registro as meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosListDto).propostaId });
                },
                'Visualizar',
                (row) => {
                    return true;
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Status'),
                field: 'statusProposta',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity) && item.text != \'MaisDetalhes\'" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Produto'),
                field: 'nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Administradora'),
                field: 'administradora'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Abrangencia'),
                field: 'abrangencia'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Reembolso'),
                field: 'reembolso',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Valor'),
                field: 'valor',
                cellFilter: 'currency: "R$ " : 2'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: '',
                field: 'id',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    <a ng-repeat="item in grid.appScope.actionLinks" ng-if="item.visible(row.entity) && item.text == \'MaisDetalhes\'" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                </div>',
                enableSorting: false
            });
        }
    }

    export class MeusProdutosIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/meusProdutos/meusProdutosIndex.cshtml';
            this.controller = MeusProdutosIndexController;
        }
    }
}