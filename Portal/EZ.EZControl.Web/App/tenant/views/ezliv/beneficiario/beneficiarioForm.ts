﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
//import * as ezGrid from "../../../../common/ezGrid/ezGrid";

import * as proponenteTitularService from "../../../../services/domain/ezliv/proponenteTitular/proponenteTitularService";
import * as proponenteTitularDtos from "../../../../dtos/ezliv/subtipoPessoa/proponenteTitular/proponenteTitularDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as responsavelDtos from "App/dtos/ezliv/subtipoPessoa/responsavel/responsavelDtos";
import * as pessoaDtos from "App/dtos/global/pessoa/pessoaDtos";
import * as enderecoEletronicoDtos from "App/dtos/global/enderecoEletronico/enderecoEletronicoDtos";
import * as dependenteDto from "../../../../dtos/ezliv/geral/dependente/dependenteDtos";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as grupoPessoaService from "../../../../services/domain/global/grupoPessoa/grupoPessoaService";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as profissaoDtos from "../../../../dtos/global/profissao/profissaoDtos";
import * as profissaoService from "../../../../services/domain/global/profissao/profissaoService";

export module Forms {
    export class BeneficiarioFormController extends
        controllers.Controllers.ControllerFormComponentBase<proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput,
        proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        private sexos: any;
        private estadosCivis: any;
        private tiposDeResponsavel: any;
        private grausDeParentesco: any;
        private idadeDoTitular: number;
        //Dados Gerais
        public proponenteTitularInput = new proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput();

        //Validar Documento
        public validaNumeroDocumento: () => any;

        // grupo pessoa
        public ezSelectGrupoPessoa: ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoGrupoPessoa: (termoDigitadoPesquisa: string) => any;
        private grupoPessoaSelecionado: (registro: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto) => string;
        private grupoPessoaDeselecionado: () => void;
        private requestParamsGrupoPessoa: requestParam.RequestParam.RequestParams;
        private carregarGrupoPessoa: (id: number) => void;

        // Estado
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private estadoDeselecionado: () => void;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;
        private carregarEstado: (id: number) => void;

        // Profissão
        public ezSelectProfissao: ezSelect.EzSelect.EzSelect<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoProfissao: (termoDigitadoPesquisa: string) => any;
        private profissaoSelecionado: (registro: profissaoDtos.Dtos.Profissao.ProfissaoListDto) => string;
        private profissaoDeselecionado: () => void;
        private requestParamsProfissao: requestParam.RequestParam.RequestParams;
        private carregarProfissao: (id: number) => void;

        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'proponenteTitularService', 'grupoPessoaService', 'estadoService', 'profissaoService', '$uibModal'];

        constructor(
            public $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public proponenteTitularService: proponenteTitularService.Services.ProponenteTitularService,
            public grupoPessoaService: grupoPessoaService.Services.GrupoPessoaService,
            public estadoService: estadoService.Services.EstadoService,
            public profissaoService: profissaoService.Services.ProfissaoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                proponenteTitularService,
                'tenant.ezliv.beneficiario');


            this.validaNumeroDocumento = () => {
                var listaServicosValidacao = [];
                listaServicosValidacao.push(abp.services.app.global.tipoDeDocumento.isCpf);
                return listaServicosValidacao;
            }
            
            this.carregarGrupoPessoa = (id: number) => {
                this.ezSelectGrupoPessoa.loading = true;
                var promise = this.grupoPessoaService.getById(id);
                promise.then(result => {
                    var grupoPessoa = result;
                    this.ezSelectGrupoPessoa.setInputText(grupoPessoa.nome);
                }).finally(() => {
                    this.ezSelectGrupoPessoa.loading = false;
                });
            }

            this.carregarEstado = (id: number) => {
                this.ezSelectEstado.loading = true;
                var promise = this.estadoService.getById(id);
                promise.then(result => {
                    var estado = result;
                    this.ezSelectEstado.setInputText(estado.nome);
                }).finally(() => {
                    this.ezSelectEstado.loading = false;
                });
            }

            this.carregarProfissao = (id: number) => {
                this.ezSelectProfissao.loading = true;
                var promise = this.profissaoService.getById(id);
                promise.then(result => {
                    var profissao = result;
                    this.ezSelectProfissao.setInputText(profissao.titulo);
                }).finally(() => {
                    this.ezSelectProfissao.loading = false;
                });
            }

        }

        $onInit() {
            super.init();

            this.sexos = ez.domain.enum.sexoEnum.valores;
            this.estadosCivis = ez.domain.enum.estadoCivilEnum.valores;
            this.tiposDeResponsavel = ez.domain.enum.tipoDeResponsavelEnum.valores;
            this.grausDeParentesco = ez.domain.enum.grauDeParentescoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput;
                instance.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (this.entity.pessoaFisica != null) {
                    this.entity.pessoaFisica.dataDeNascimento = this.entity.pessoaFisica.dataDeNascimento != null ? new Date(this.entity.pessoaFisica.dataDeNascimento.toString()) : new Date();
                }
                if (this.entity.responsavel != null && this.entity.responsavel.pessoaFisica != null){
                    this.entity.responsavel.pessoaFisica.dataDeNascimento = this.entity.responsavel.pessoaFisica.dataDeNascimento != null ? new Date(this.entity.responsavel.pessoaFisica.dataDeNascimento.toString()) : new Date();
                }

                for (let dependente of this.entity.dependentes) {
                    dependente.pessoaFisica.dataDeNascimento = dependente.pessoaFisica.dataDeNascimento != null ? new Date(dependente.pessoaFisica.dataDeNascimento.toString()) : new Date();
                }

                if (this.entity.pessoaFisica != null && this.entity.pessoaFisica.grupoPessoaId > 0)
                {
                    this.carregarGrupoPessoa(this.entity.pessoaFisica.grupoPessoaId);
                }

                this.entity.celular = this.entity.pessoaFisica.telefoneCelular ? this.entity.pessoaFisica.telefoneCelular.ddd + this.entity.pessoaFisica.telefoneCelular.numero : "";
                this.entity.celulaId = this.entity.pessoaFisica.telefoneCelular ? this.entity.pessoaFisica.telefoneCelular.id : 0;

                this.idadeDoTitular = this.entity.idade;
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.events.onBeforeSaveEntity = () => {
            };

            //Cerrega ezSelect GrupoPessoa
            this.ezSelectGrupoPessoaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

           
        }


        atualizarIdade = (entity: proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput) => {
            var nascimento = new Date(this.entity.pessoaFisica.dataDeNascimento);
            var dataCorrete = new Date();

            var ano_atual = dataCorrete.getFullYear();
            var mes_atual = dataCorrete.getMonth() + 1;
            var dia_atual = dataCorrete.getDate();

            var ano_aniversario = nascimento.getFullYear();
            var mes_aniversario = nascimento.getMonth();
            var dia_aniversario = nascimento.getDay();

            this.idadeDoTitular = ano_atual - ano_aniversario;

            if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
                this.idadeDoTitular--;
            }
        };

        private ezSelectGrupoPessoaConfig() {
            this.requestParamsGrupoPessoa = new requestParam.RequestParam.RequestParams();

            this.grupoPessoaSelecionado = (registro) => {
                this.entity.pessoaFisica.grupoPessoaId = registro.id;
                return registro.nome;
            }

            this.grupoPessoaDeselecionado = () => {
                this.entity.pessoaFisica.grupoPessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoGrupoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput();
                filtro.grupoPessoaId = this.entity.pessoaFisica.grupoPessoaId;
                filtro.tipoDePessoa = 1;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGrupoPessoa = new ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.grupoPessoaService,
                this.getFiltroParaPaginacaoGrupoPessoa,
                this.requestParamsGrupoPessoa,
                this.$uibModal,
                this.grupoPessoaSelecionado,
                this.grupoPessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGrupoPessoa.onEzGridCreated = () => {
                this.ezSelectGrupoPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.grupoPessoaService.getGruposPessoaExceptForId(filtro, requestParams);
                };

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Nome'),
                    field: 'nome'
                });

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectGrupoPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGrupoPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectGrupoPessoa.ezGrid.getRegistros();
            };
        }

        public addDependente() {
            var dependente = new dependenteDto.Dtos.EZLiv.Geral.DependenteInput();

            if (this.entity.dependentes == null || this.entity.dependentes == undefined)
                this.entity.dependentes = [];

            this.entity.dependentes.push(dependente);
        }

        public removeDependente(dependente) {
            var index = this.entity.dependentes.indexOf(dependente);
            if (index > -1) {
                ezFixes.uiGrid.fixGrid();
                this.entity.dependentes.splice(index, 1);
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class BeneficiarioFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/beneficiario/beneficiarioForm.cshtml';
            this.controller = BeneficiarioFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}