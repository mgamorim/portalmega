﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as proponenteTitularService from "../../../../services/domain/ezliv/proponenteTitular/proponenteTitularService";
import * as proponenteTitularDtos from "../../../../dtos/ezliv/subtipoPessoa/proponenteTitular/proponenteTitularDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Beneficiario {

    class BeneficiarioIndexController extends
        controllers.Controllers.ControllerIndexBase<proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularListDto,
        proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput> {

        static $inject = ['$state', 'uiGridConstants', 'proponenteTitularService'];

        public ezGrid: ezGrid.EzGrid.EzGrid<proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public proponenteTitularService: proponenteTitularService.Services.ProponenteTitularService) {

            super(
                $state,
                uiGridConstants,
                proponenteTitularService, {
                    rotaAlterarRegistro: 'tenant.ezliv.beneficiario.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.beneficiario.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'NomeDaMae';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new proponenteTitularDtos.Dtos.EZLiv.Geral.GetProponenteTitularInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.proponenteTitularService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            //This is the template that will be used to render subgrid.
            this.ezGrid.optionsGrid.expandableRowTemplate = 'dependentesSubGrid.html';
                //This will be the height of the subgrid
            this.ezGrid.optionsGrid.expandableRowHeight = 140;

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Beneficiario.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Beneficiario.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Beneficiario.Nome'),
                field: 'pessoaFisica.nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Beneficiario.Idade'),
                field: 'idade'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Responsavel'),
                field: 'nomeResponsavelGrid'
            });
        }
    }

    export class BeneficiarioIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/beneficiario/beneficiarioIndex.cshtml';
            this.controller = BeneficiarioIndexController;
        }
    }
}