﻿import * as parametroEZLivService from "../../../../services/domain/ezliv/parametro/parametroEZLivService";
import * as parametroEZLivDtos from "../../../../dtos/ezliv/parametro/parametroEZLivDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ParametroFormController extends controllers.Controllers.ControllerFormComponentBase<parametroEZLivDtos.Dtos.EZLiv.Parametro.ParametroEZLivListDto, parametroEZLivDtos.Dtos.EZLiv.Parametro.ParametroEZLivInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'parametroEZLivService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public parametroEZLivService: parametroEZLivService.Services.ParametroEZLivService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                parametroEZLivService,
                'tenant.ezliv.parametro');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new parametroEZLivDtos.Dtos.EZLiv.Parametro.ParametroEZLivInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ParametroFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/parametro/parametroForm.cshtml';
            this.controller = ParametroFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}