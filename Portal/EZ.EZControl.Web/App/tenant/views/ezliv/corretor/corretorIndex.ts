﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as corretorService from "../../../../services/domain/ezliv/corretor/corretorService";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Corretor {

    class CorretorIndexController extends
        controllers.Controllers.ControllerIndexBase<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto,
        corretorDtos.Dtos.EZLiv.Geral.CorretorInput> {

        

        static $inject = ['$state', 'uiGridConstants', 'corretorService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public corretorService: corretorService.Services.CorretorService) {

            super(
                $state,
                uiGridConstants,
                corretorService, {
                    rotaAlterarRegistro: 'tenant.ezliv.corretor.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.corretor.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            //this.requestParams.sorting = 'pessoa.nomePessoa';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new corretorDtos.Dtos.EZLiv.Geral.GetCorretorInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretorService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewCorretor', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretor.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretor.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.Corretor.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Corretor.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Corretor.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
            
        }
    }

    export class CorretorIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/corretor/corretorIndex.cshtml';
            this.controller = CorretorIndexController;
        }
    }
}