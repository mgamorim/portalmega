﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// Servicos/DTOS
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";


//Corretor
import * as corretorService from "../../../../services/domain/ezliv/corretor/corretorService";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";

//Corretoras
import * as corretoraService from "../../../../services/domain/ezliv/corretora/corretoraService";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";



export module Forms {
    export class CorretorFormController extends
        controllers.Controllers.ControllerFormComponentBase<corretorDtos.Dtos.EZLiv.Geral.CorretorInput,
        corretorDtos.Dtos.EZLiv.Geral.CorretorInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        public tipoPessoaEnum: any;
        public corretorForm: ng.IFormController;

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaInput;
        public pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        public ezSelectPessoaFisica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaFisica: (termoDigitadoPesquisa: string) => any;
        private pessoaFisicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto) => string;
        private pessoaFisicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaFisica: (id: number) => void;



        // Corretora
        //EzSelect
        public corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        public ezSelectCorretora: ezSelect.EzSelect.EzSelect<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCorretora: (termoDigitadoPesquisa: string) => any;
        private corretoraSelecionado: (registro: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput) => string;
        private corretoraDeselecionado: () => void;
        private requestParamsCorretora: requestParam.RequestParam.RequestParams;
        
        public addCorretora: () => void;
        public limparEntidade: () => void;
        public getCorretoraInput: () => void;
        public corretoraCurrent: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        public saving: boolean;
        public editing: boolean;
        public excluir: (corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput) => void;
        public cancelEditCorretora: () => void;


        private setDataGridInput: (result: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[]) => void;
        private gridApiInput: uiGrid.IGridApiOf<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        private requestParamsValoresInput: requestParam.RequestParam.RequestParams;
        private corretoraAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        public loadingCorretoraInput: boolean;
        public corretoraGridOptionsInput: uiGrid.IGridOptionsOf<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        private gridDataInput: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[];


        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'corretorService', 'pessoaFisicaService', 'corretoraService','pessoaService', '$uibModal'];

        constructor(
            public $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public corretorService: corretorService.Services.CorretorService,
            public pessoaFisicaService: pessoaFisicaService.Services.PessoaFisicaService,
            public corretoraService: corretoraService.Services.CorretoraService,
            public pessoaService: pessoaService.Services.PessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                corretorService,
                'tenant.ezliv.corretor');

            this.carregarPessoaFisica = (id: number) => {
                this.ezSelectPessoaFisica.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    this.entity.id = this.registroId;
                    this.ezSelectPessoaFisica.setInputText(result.nomePessoa);
                }).finally(() => {
                    this.ezSelectPessoaFisica.loading = false;
                });
            }


            //Corretoras
            this.addCorretora = () => {
                this.corretoraCurrent.isActive = true;
                this.entity.corretoras.push(this.corretoraCurrent);
                this.getCorretoraInput();
                this.limparEntidade();
            }

            this.limparEntidade = () => {
                this.corretoraCurrent = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput();
                this.ezSelectCorretora.gridSelectClear();
            }

            this.getCorretoraInput = () => {
                this.loadingEntity = true;
                if (this.entity) {
                    this.setDataGridInput(this.entity.corretoras);
                    ezFixes.uiGrid.fixGrid();
                }
                
                this.loadingEntity = false;
            }

            this.excluir = (corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput) => {
                this.editing = false;
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.corretoras.length; i++) {
                                if (this.entity.corretoras[i].id === corretora.id) {
                                    this.entity.corretoras.splice(i, 1);
                                    this.getCorretoraInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.setDataGridInput = (result: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[]) => {
                this.gridDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInput.push(result[i]);
                }

                this.corretoraGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditCorretora = () => {
                this.corretoraCurrent = new corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
                this.ezSelectCorretora.gridSelectClear();
            }


        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new corretorDtos.Dtos.EZLiv.Geral.CorretorInput;
                instance.id = 0;
                instance.isActive = true;
                instance.pessoa = new pessoaDtos.Dtos.Pessoa.PessoaInput;
                instance.corretoras = new Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>();
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                if (entity.pessoa) {
                    this.carregarPessoaFisica(entity.pessoa.id);
                }

                this.getCorretoraInput();
                ezFixes.uiGrid.fixGrid();
            };
            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.events.onBeforeSaveEntity = () => {
            };

            this.ezSelectPessoaFisicaConfig();

            this.ezSelectCorretoraConfig();

            this.corretoraGridConfigInput();


            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();


        }

        setCorretorForm(form: ng.IFormController) {
            this.corretorForm = form;
        }



        private ezSelectPessoaFisicaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
         
            this.pessoaFisicaSelecionado = (registro) => {
                this.pessoaFisica = registro;
                this.entity.pessoa = registro;
                this.entity.pessoa.id = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaFisicaDeselecionado = () => {
                this.pessoaFisica = null;
                this.entity.pessoaFisica = null;
                this.entity.pessoa = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaFisica = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaExceptForId();
                filtro.nome = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaFisica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaFisicaService,
                this.getFiltroParaPaginacaoPessoaFisica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaFisicaSelecionado,
                this.pessoaFisicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaFisica.onEzGridCreated = () => {

                this.ezSelectPessoaFisica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getPessoaExceptForCorretor(filtro, requestParams);
                };

                this.ezSelectPessoaFisica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.Nome'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaFisica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaFisica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaFisica.ezGrid.getRegistros();
            };
        }


        private ezSelectCorretoraConfig() {
            this.requestParamsCorretora = new requestParam.RequestParam.RequestParams();

            this.corretoraSelecionado = (registro) => {
                this.corretoraCurrent = registro;
                return registro.pessoaJuridica.nomeFantasia;
            }

            this.corretoraDeselecionado = () => {
                this.corretoraCurrent = null;
            }


            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCorretora = (termoDigitadoPesquisa) => {
                var filtro = new corretorDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForCorretor();
                filtro.nome = termoDigitadoPesquisa;
                filtro.corretoras = this.entity.corretoras;
                filtro.corretorId = this.entity.id;
                return filtro;
            }

            this.ezSelectCorretora = new ezSelect.EzSelect.EzSelect<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretoraService,
                this.getFiltroParaPaginacaoCorretora,
                this.requestParamsCorretora,
                this.$uibModal,
                this.corretoraSelecionado,
                this.corretoraDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCorretora.onEzGridCreated = () => {

                this.ezSelectCorretora.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.corretorService.getCorretoraExceptForCorretor(filtro, requestParams);
                };

                this.ezSelectCorretora.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaJuridica.NomeFantasia'),
                    field: 'pessoaJuridica.nomeFantasia'
                });

                this.ezSelectCorretora.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCorretora.on.beforeOpenModalDialog = () => {
                this.ezSelectCorretora.ezGrid.getRegistros();
            };

        }

        private corretoraGridConfigInput() {
            this.requestParamsValoresInput = new requestParam.RequestParam.RequestParams();

            this.corretoraAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>();


            this.corretoraAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.CorretorCorretora.Delete');
                }
            ));

            this.gridDataInput = new Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>();

            this.corretoraGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.corretoraAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInput = api;
                    this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsValoresInput.sorting = null;
                        } else {
                            this.requestParamsValoresInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getCorretoraInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsValoresInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsValoresInput.maxResultCount = pageSize;

                        this.getCorretoraInput();
                    });
                }
            }

            this.corretoraGridOptionsInput.data = this.gridDataInput;


            this.corretoraGridOptionsInput.columnDefs.push({
                name: app.localize('Corretora'),
                field: 'pessoaJuridica.nomeFantasia',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.getCorretoraInput();
            ezFixes.uiGrid.fixGrid();
        }

        
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class CorretorFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/corretor/corretorForm.cshtml';
            this.controller = CorretorFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}