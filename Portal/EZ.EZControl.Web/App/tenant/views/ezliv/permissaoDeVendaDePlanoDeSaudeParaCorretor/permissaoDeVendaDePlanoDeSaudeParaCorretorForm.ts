﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

// services
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorService from "../../../../services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorService";
import * as corretorService from "../../../../services/domain/ezliv/corretor/corretorService";
import * as produtoDePlanoDeSaudeService from "../../../../services/domain/ezliv/produtoDePlanoDeSaude/produtoDePlanoDeSaudeService";

// dtos
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorDtos";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as produtoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";

import * as entidadeService from "../../../../services/domain/ezliv/associacao/associacaoService";
import * as entidadeDtos from "../../../../dtos/ezliv/subtipoPessoa/associacao/associacaoDtos";

export module Forms {
    export class PermissaoDeVendaDePlanoDeSaudeParaCorretorFormController extends controllers.Controllers.ControllerFormComponentBase<permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;
        private createDescription: () => any;

        public ezSelectCorretor: ezSelect.EzSelect.EzSelect<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaCorretor: (termoDigitadoPesquisa: string) => any;
        private corretorSelecionado: (registro: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto) => string;
        private corretorDeselecionado: () => void;
        private requestParamsCorretor: requestParam.RequestParam.RequestParams;

        private idsCorretor: number[];

        public saving: boolean;
        public editing: boolean;
        public excluir: (valor: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto) => void;
        public excluirProduto: (valor: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => void;
        public limparEntidade: () => void;
        public addCorretor: () => void;
        public addProduto: () => void;

        private setDataGridInput: (result: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto[]) => void;
        private gridApiInput: uiGrid.IGridApiOf<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>;
        private requestParamsCorretorInput: requestParam.RequestParam.RequestParams;
        private corretorAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>;
        public getCorretorInput: () => void;
        public loadingValoresInput: boolean;
        public corretorGridOptionsInput: uiGrid.IGridOptionsOf<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>;
        private gridDataInput: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto[];

        private setDataGridProdutoInput: (result: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[]) => void;
        private gridApiProdutoInput: uiGrid.IGridApiOf<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>;
        private requestParamsProdutoDePlanoDeSaudesInput: requestParam.RequestParam.RequestParams;
        private produtoDePlanoDeSaudesAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>;
        public getProdutoDePlanoDeSaudesInput: () => void;
        public loadingValoresProdutoInput: boolean;
        public produtoDePlanoDeSaudesGridOptionsInput: uiGrid.IGridOptionsOf<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>;
        private gridProdutoDataInput: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];

        public corretorCurrent: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto;
        public produtoDePlanoDeSaudeCurrent: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto;

        public ezSelectProdutoDePlanoDeSaude: ezSelect.EzSelect.EzSelect<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaProdutoDePlanoDeSaude: (termoDigitadoPesquisa: string) => any;
        private produtoDePlanoDeSaudeSelecionado: (registro: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => string;
        private produtoDePlanoDeSaudeDeselecionado: () => void;
        private requestParamsProdutoDePlanoDeSaude: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'permissaoDeVendaDePlanoDeSaudeParaCorretorService',  'produtoDePlanoDeSaudeService', 'corretorService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public permissaoDeVendaDePlanoDeSaudeParaCorretorService: permissaoDeVendaDePlanoDeSaudeParaCorretorService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretorService,
            public produtoDePlanoDeSaudeService: produtoDePlanoDeSaudeService.Services.ProdutoDePlanoDeSaudeService,
            public corretorService: corretorService.Services.CorretorService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                permissaoDeVendaDePlanoDeSaudeParaCorretorService,
                'tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor');

            this.getCorretorInput = () => {
                this.loadingValoresInput = true;
                if (this.entity) {
                    this.setDataGridInput(this.entity.corretores);
                }

                this.loadingValoresInput = false;
            }

            this.getProdutoDePlanoDeSaudesInput = () => {
                this.loadingValoresProdutoInput = true;
                if (this.entity) {
                    this.setDataGridProdutoInput(this.entity.produtos);
                }

                this.loadingValoresProdutoInput = false;
            }

            this.setDataGridInput = (result: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto[]) => {
                this.gridDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInput.push(result[i]);
                }

                this.corretorGridOptionsInput.totalItems = result.length;
            };

            this.setDataGridProdutoInput = (result: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[]) => {
                this.gridProdutoDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridProdutoDataInput.push(result[i]);
                }

                this.produtoDePlanoDeSaudesGridOptionsInput.totalItems = result.length;
            };

            this.excluir = (valor: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto) => {
                this.editing = false;
                abp.message.confirm(
                    'Deseja remover essa corretor?', 'Confirmação',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.corretores.length; i++) {
                                if (this.entity.corretores[i].id === valor.id) {
                                    this.entity.corretores.splice(i, 1);
                                    this.getCorretorInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.excluirProduto = (valor: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto) => {
                this.editing = false;
                abp.message.confirm(
                    'Deseja remover esse produto?', 'Confirmação',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.produtos.length; i++) {
                                if (this.entity.produtos[i].id === valor.id) {
                                    this.entity.produtos.splice(i, 1);
                                    this.getProdutoDePlanoDeSaudesInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.addCorretor = () => {
                this.entity.corretores.push(this.corretorCurrent);
                this.getCorretorInput();
                this.corretorCurrent = new corretorDtos.Dtos.EZLiv.Geral.CorretorListDto();
                this.ezSelectCorretor.setInputText('');
            }

            this.addProduto = () => {
                this.entity.produtos.push(this.produtoDePlanoDeSaudeCurrent);
                this.getProdutoDePlanoDeSaudesInput();
                this.produtoDePlanoDeSaudeCurrent = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto();
                this.ezSelectProdutoDePlanoDeSaude.setInputText('');
            }
        }

        $onInit() {
            super.init();
            this.corretorCurrent = new corretorDtos.Dtos.EZLiv.Geral.CorretorListDto();
            this.produtoDePlanoDeSaudeCurrent = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorInput();
                instance.id = 0;
                instance.isActive = true;
                instance.corretores = new Array<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>();
                instance.produtos = new Array<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>();
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                this.getCorretorInput();
                this.getProdutoDePlanoDeSaudesInput();
            }

            this.ezSelectCorretorConfig();
            this.ezSelectProdutoDePlanoDeSaudeConfig();
            this.corretorGridConfigInput();
            this.produtoDePlanoDeSaudeGridConfigInput();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectCorretorConfig() {
            this.requestParamsCorretor = new requestParam.RequestParam.RequestParams();

            this.corretorSelecionado = (registro) => {
                this.corretorCurrent = registro;
                return registro.pessoa.nomePessoa;
            }

            this.corretorDeselecionado = () => {
                this.corretorCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaCorretor = (termoDigitadoPesquisa) => {
                var filtro = new corretorDtos.Dtos.EZLiv.Geral.GetCorretorInput();
                let exceptIds = new Array();
                this.entity.corretores.forEach(x => exceptIds.push(x.id));

                filtro.exceptIds = exceptIds;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCorretor = new ezSelect.EzSelect.EzSelect<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.corretorService,
                this.getFiltroParaCorretor,
                this.requestParamsCorretor,
                this.$uibModal,
                this.corretorSelecionado,
                this.corretorDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCorretor.onEzGridCreated = () => {

                this.ezSelectCorretor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Corretor.Nome'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectCorretor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCorretor.on.beforeOpenModalDialog = () => {
                this.ezSelectCorretor.ezGrid.getRegistros();
            };
        }

        private ezSelectProdutoDePlanoDeSaudeConfig() {
            this.requestParamsProdutoDePlanoDeSaude = new requestParam.RequestParam.RequestParams();

            this.produtoDePlanoDeSaudeSelecionado = (registro) => {
                this.produtoDePlanoDeSaudeCurrent = registro;
                return registro.nome;
            }

            this.produtoDePlanoDeSaudeDeselecionado = () => {
                this.produtoDePlanoDeSaudeCurrent = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaProdutoDePlanoDeSaude = (termoDigitadoPesquisa) => {
                var filtro = new produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput();
                let exceptIds = new Array();
                this.entity.produtos.forEach(x => exceptIds.push(x.id));

                filtro.exceptIds = exceptIds;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProdutoDePlanoDeSaude = new ezSelect.EzSelect.EzSelect<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoDePlanoDeSaudeService,
                this.getFiltroParaProdutoDePlanoDeSaude,
                this.requestParamsProdutoDePlanoDeSaude,
                this.$uibModal,
                this.produtoDePlanoDeSaudeSelecionado,
                this.produtoDePlanoDeSaudeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProdutoDePlanoDeSaude.onEzGridCreated = () => {

                this.ezSelectProdutoDePlanoDeSaude.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.Nome'),
                    field: 'nome'
                });

                this.ezSelectProdutoDePlanoDeSaude.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectProdutoDePlanoDeSaude.on.beforeOpenModalDialog = () => {
                this.ezSelectProdutoDePlanoDeSaude.ezGrid.getRegistros();
            };
        }

        private corretorGridConfigInput() {
            this.requestParamsCorretorInput = new requestParam.RequestParam.RequestParams();

            this.corretorAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>();

            this.corretorAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridDataInput = new Array<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>();

            this.corretorGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.corretorAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInput = api;
                    this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsCorretorInput.sorting = null;
                        } else {
                            this.requestParamsCorretorInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getCorretorInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsCorretorInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsCorretorInput.maxResultCount = pageSize;

                        this.getCorretorInput();
                    });
                }
            }

            this.corretorGridOptionsInput.data = this.gridDataInput;

            this.corretorGridOptionsInput.columnDefs.push({
                name: app.localize('Corretor.Nome'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.getCorretorInput();
        }

        private produtoDePlanoDeSaudeGridConfigInput() {
            this.requestParamsProdutoDePlanoDeSaude = new requestParam.RequestParam.RequestParams();

            this.produtoDePlanoDeSaudesAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>();

            this.produtoDePlanoDeSaudesAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirProduto, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridProdutoDataInput = new Array<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>();

            this.produtoDePlanoDeSaudesGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.produtoDePlanoDeSaudesAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiProdutoInput = api;
                    this.gridApiProdutoInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsProdutoDePlanoDeSaudesInput.sorting = null;
                        } else {
                            this.requestParamsProdutoDePlanoDeSaudesInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getProdutoDePlanoDeSaudesInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsProdutoDePlanoDeSaudesInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsProdutoDePlanoDeSaudesInput.maxResultCount = pageSize;

                        this.getProdutoDePlanoDeSaudesInput();
                    });
                }
            }

            this.produtoDePlanoDeSaudesGridOptionsInput.data = this.gridProdutoDataInput;

            this.produtoDePlanoDeSaudesGridOptionsInput.columnDefs.push({
                name: app.localize('PessoaFisica.Nome'),
                field: 'nome',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.getProdutoDePlanoDeSaudesInput();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PermissaoDeVendaDePlanoDeSaudeParaCorretorFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorForm.cshtml';
            this.controller = PermissaoDeVendaDePlanoDeSaudeParaCorretorFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}