﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorService from "../../../../services/domain/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorService";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor {

    class PermissaoDeVendaDePlanoDeSaudeParaCorretorIndexController extends controllers.Controllers.ControllerIndexBase <permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorInput>{

        static $inject = ['$state', 'uiGridConstants', 'permissaoDeVendaDePlanoDeSaudeParaCorretorService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public permissaoDeVendaDePlanoDeSaudeParaCorretorService: permissaoDeVendaDePlanoDeSaudeParaCorretorService.Services.PermissaoDeVendaDePlanoDeSaudeParaCorretorService) {

            super(
                $state,
                uiGridConstants,
                permissaoDeVendaDePlanoDeSaudeParaCorretorService, {
                    rotaAlterarRegistro: 'tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor.alterar',
                    rotaNovoRegistro: 'tenant.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.permissaoDeVendaDePlanoDeSaudeParaCorretorService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPermissaoDeVendaDePlanoDeSaudeParaCorretor', this.novo, abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Edit');
                }));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZLiv.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Delete');
                }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            }); 

             // Definir a coluna %
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('% de Comissão para o Grupo'),
                field: 'comissao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                    </span>\
                </div>'
            }); 


            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PermissaoDeVendaDePlanoDeSaudeParaCorretor.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });



        }
    }

    export class PermissaoDeVendaDePlanoDeSaudeParaCorretorIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezliv/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorIndex.cshtml';
            this.controller = PermissaoDeVendaDePlanoDeSaudeParaCorretorIndexController;
        }
    }
}