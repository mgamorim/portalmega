﻿import * as clienteService from "../../../../services/domain/global/cliente/clienteService";
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Forms {
    export class ClienteFormController extends controllers.Controllers.ControllerFormComponentBase<clienteDtos.Dtos.Cliente.ClienteListDto, clienteDtos.Dtos.Cliente.ClienteInput, clienteDtos.Dtos.Cliente.ClientePessoaIdDto>{
        public tipoPessoaEnum: any;
        public tipoDocumentoEnum: any;
        public clienteForm: ng.IFormController;
        private observacao: string;

        public getFiltroParaPaginacaoCliente: (termoDigitadoPesquisa: string) => any;
        private clienteSelecionado: (registro: clienteDtos.Dtos.Cliente.ClienteListDto) => string;
        private clienteDeselecionado: () => void;
        private requestParamsCliente: requestParam.RequestParam.RequestParams;

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'clienteService', 'pessoaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public clienteService: clienteService.Services.ClienteService,
            public pessoaService: pessoaService.Services.PessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                clienteService,
                'tenant.global.clientes');

            this.carregarPessoa = (id: number) => {
                this.ezSelectPessoa.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    this.entity.pessoa.id = this.pessoa.id;
                    this.ezSelectPessoa.setInputText(result.nomePessoa);
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                });
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new clienteDtos.Dtos.Cliente.ClienteInput();
                instance.id = 0;
                instance.pessoa = new pessoaDtos.Dtos.Pessoa.PessoaInput();
                instance.isActive = true;
                instance.pessoa.tipoPessoa = 2;
                instance.pessoa.grupoPessoaId = null;
                instance.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
                instance.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoa) {
                    this.carregarPessoa(entity.pessoa.id);
                }
            };
            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.clienteId;
                this.entity.pessoa.id = result.pessoaId;
            };

            this.tipoPessoaEnum = ez.domain.enum.tipoPessoaEnum.valores;
            this.tipoDocumentoEnum = ez.domain.enum.tipoDeDocumentoEnum.valores;
            this.ezSelectPessoaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoa) {
                this.carregarPessoa(this.pessoa.id);
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaSelecionado = (registro) => {
                this.pessoa = registro;
                this.entity.pessoa.id = registro.id;
                this.entity.pessoa.tipoPessoa = registro.tipoPessoa;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.pessoa = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new clienteDtos.Dtos.Cliente.GetPessoaExceptForCliente();
                filtro.nomePessoa = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {

                this.ezSelectPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.clienteService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.NomePessoa'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.TipoPessoa'),
                    cellTemplate: '\
                     <div class="ui-grid-cell-contents">\
                         {{grid.appScope.getEnumValue(row.entity, "tipoPessoaEnum", "tipoPessoa")}}\
                     </div>'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.clienteForm = form;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ClienteFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/clientes/clienteForm.cshtml';
            this.controller = ClienteFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}