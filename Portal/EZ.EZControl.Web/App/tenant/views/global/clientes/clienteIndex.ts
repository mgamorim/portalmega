﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as clienteService from "../../../../services/domain/global/cliente/clienteService";
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Cliente {

    class ClienteIndexController extends controllers.Controllers.ControllerIndexBase<clienteDtos.Dtos.Cliente.ClienteListDto, clienteDtos.Dtos.Cliente.ClienteInput>{

        static $inject = ['$state', 'uiGridConstants', 'clienteService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<clienteDtos.Dtos.Cliente.ClienteListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public clienteService: clienteService.Services.ClienteService) {

            super(
                $state,
                uiGridConstants,
                clienteService, {
                    rotaAlterarRegistro: 'tenant.global.clientes.alterar',
                    rotaNovoRegistro: 'tenant.global.clientes.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new clienteDtos.Dtos.Cliente.GetClienteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<clienteDtos.Dtos.Cliente.ClienteListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.clienteService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewCliente', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Cliente.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Cliente.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Cliente.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.NomePessoa'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.TipoPessoa'),
                field: 'pessoa.tipoPessoaValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.GrupoPessoa'),
                field: 'pessoa.grupoPessoa.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.DocumentoPrincipal.Numero'),
                field: 'pessoa.documentoPrincipal.numero'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cliente.DocumentoPrincipal.TipoDeDocumento'),
                field: 'tipoDeDocumentoFixoValue'
            });
        }
    }

    export class ClienteIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/clientes/clienteIndex.cshtml';
            this.controller = ClienteIndexController;
        }
    }
}