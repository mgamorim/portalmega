﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as bancoService from "../../../../services/domain/global/banco/bancoService";
import * as bancoDtos from "../../../../dtos/global/banco/bancoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Banco {

    class BancoIndexController extends controllers.Controllers.ControllerIndexBase<bancoDtos.Dtos.Banco.BancoListDto, bancoDtos.Dtos.Banco.BancoInput>{

        static $inject = ['$state', 'uiGridConstants', 'bancoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<bancoDtos.Dtos.Banco.BancoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public bancoService: bancoService.Services.BancoService) {

            super(
                $state,
                uiGridConstants,
                bancoService, {
                    rotaAlterarRegistro: 'tenant.global.banco.alterar',
                    rotaNovoRegistro: 'tenant.global.banco.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new bancoDtos.Dtos.Banco.GetBancoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<bancoDtos.Dtos.Banco.BancoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.bancoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewBanco', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Banco.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Banco.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Banco.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Banco.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Banco.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Banco.Codigo'),
                field: 'codigo'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Banco.Digito'),
                field: 'digitoVerificador'
            });
        }
    }

    export class BancoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/banco/bancoIndex.cshtml';
            this.controller = BancoIndexController;
        }
    }
}