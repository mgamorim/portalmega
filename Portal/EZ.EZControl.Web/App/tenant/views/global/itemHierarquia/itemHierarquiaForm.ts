﻿import * as itemHierarquiaService from "../../../../services/domain/global/itemHierarquia/itemHierarquiaService";
import * as itemHierarquiaDtos from "../../../../dtos/global/itemHierarquia/itemHierarquiaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ItemHierarquiaFormController extends controllers.Controllers.ControllerFormComponentBase<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto, itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectItemHierarquia: ezSelect.EzSelect.EzSelect<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoItemHierarquia: (termoDigitadoPesquisa: string) => any;
        private itemHierarquiaSelecionado: (registro: itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto) => string;
        private itemHierarquiaDeselecionado: () => void;
        private requestParamsItemHierarquia: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'itemHierarquiaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public itemHierarquiaService: itemHierarquiaService.Services.ItemHierarquiaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                itemHierarquiaService,
                'tenant.global.itemHierarquia');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (entity.itemHierarquiaPaiId) {
                    this.ezSelectItemHierarquia.loading = true;
                    var promise = this.itemHierarquiaService.getById(entity.itemHierarquiaPaiId);
                    promise.then(result => {
                        this.ezSelectItemHierarquia.setInputText(result.nome);
                    })
                        .finally(() => {
                            this.ezSelectItemHierarquia.loading = false;
                        });
                }
            };

            this.ezSelectItemHierarquiaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectItemHierarquiaConfig() {
            this.requestParamsItemHierarquia = new requestParam.RequestParam.RequestParams();
            this.requestParamsItemHierarquia.sorting = 'Nome';

            this.itemHierarquiaSelecionado = (registro) => {
                this.entity.itemHierarquiaPaiId = registro.id;
                return registro.nome;
            }

            this.itemHierarquiaDeselecionado = () => {
                this.entity.itemHierarquiaPaiId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoItemHierarquia = (termoDigitadoPesquisa) => {
                var filtro = new itemHierarquiaDtos.Dtos.ItemHierarquia.GetItemHierarquiaExceptForIdInput();
                filtro.itemHierarquiaId = this.entity.id;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectItemHierarquia = new ezSelect.EzSelect.EzSelect<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.itemHierarquiaService,
                this.getFiltroParaPaginacaoItemHierarquia,
                this.requestParamsItemHierarquia,
                this.$uibModal,
                this.itemHierarquiaSelecionado,
                this.itemHierarquiaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectItemHierarquia.onEzGridCreated = () => {
                this.ezSelectItemHierarquia.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.itemHierarquiaService.getItensHierarquiaExceptForId(filtro, requestParams);
                };

                this.ezSelectItemHierarquia.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemHierarquia.Nome'),
                    field: 'nome'
                });

                this.ezSelectItemHierarquia.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemHierarquia.Codigo'),
                    field: 'codigo'
                });

                this.ezSelectItemHierarquia.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemHierarquia.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectItemHierarquia.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemHierarquia.Mascara'),
                    field: 'mascara'
                });

                this.ezSelectItemHierarquia.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectItemHierarquia.on.beforeOpenModalDialog = () => {
                this.ezSelectItemHierarquia.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ItemHierarquiaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/itemHierarquia/itemHierarquiaForm.cshtml';
            this.controller = ItemHierarquiaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}