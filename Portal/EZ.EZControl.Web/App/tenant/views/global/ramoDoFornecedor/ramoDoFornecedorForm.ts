﻿import * as ramoDoFornecedorService from "../../../../services/domain/global/ramoDoFornecedor/ramoDoFornecedorService";
import * as ramoDoFornecedorDtos from "../../../../dtos/global/ramoDoFornecedor/ramoDoFornecedorDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class RamoDoFornecedorFormController extends controllers.Controllers.ControllerFormComponentBase<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto, ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoRamoDoFornecedor: (termoDigitadoPesquisa: string) => any;
        private ramoDoFornecedorSelecionado: (registro: ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto) => string;
        private ramoDoFornecedorDeselecionado: () => void;
        private requestParamsRamoDoFornecedor: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'ramoDoFornecedorService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public ramoDoFornecedorService: ramoDoFornecedorService.Services.RamoDoFornecedorService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                ramoDoFornecedorService,
                'tenant.global.ramoDoFornecedor');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorInput();
                instance.id = 0;
                instance.isActive = true;
                return instance; 
            };
            
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class RamoDoFornecedorFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/ramoDoFornecedor/ramoDoFornecedorForm.cshtml';
            this.controller = RamoDoFornecedorFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}