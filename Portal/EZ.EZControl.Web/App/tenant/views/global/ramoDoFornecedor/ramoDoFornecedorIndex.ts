﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as ramoDoFornecedorService from "../../../../services/domain/global/ramoDoFornecedor/ramoDoFornecedorService";
import * as ramoDoFornecedorDtos from "../../../../dtos/global/ramoDoFornecedor/ramoDoFornecedorDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.RamoDoFornecedor {

    class RamoDoFornecedorIndexController extends controllers.Controllers.ControllerIndexBase<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto, ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorInput>{

        static $inject = ['$state', 'uiGridConstants', 'ramoDoFornecedorService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public ramoDoFornecedorService: ramoDoFornecedorService.Services.RamoDoFornecedorService) {

            super(
                $state,
                uiGridConstants,
                ramoDoFornecedorService, {
                    rotaAlterarRegistro: 'tenant.global.ramoDoFornecedor.alterar',
                    rotaNovoRegistro: 'tenant.global.ramoDoFornecedor.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.GetRamoDoFornecedorInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.ramoDoFornecedorService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewRamoDoFornecedor', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.RamoDoFornecedor.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.RamoDoFornecedor.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.RamoDoFornecedor.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('RamoDoFornecedor.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('RamoDoFornecedor.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class RamoDoFornecedorIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/ramoDoFornecedor/ramoDoFornecedorIndex.cshtml';
            this.controller = RamoDoFornecedorIndexController;
        }
    }
}