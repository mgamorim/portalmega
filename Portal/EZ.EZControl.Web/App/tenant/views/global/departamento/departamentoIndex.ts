﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as departamentoService from "../../../../services/domain/global/departamento/departamentoService";
import * as departamentoDtos from "../../../../dtos/global/departamento/departamentoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Departamento {

    class DepartamentoIndexController extends controllers.Controllers.ControllerIndexBase<departamentoDtos.Dtos.Departamento.DepartamentoListDto, departamentoDtos.Dtos.Departamento.DepartamentoInput>{

        static $inject = ['$state', 'uiGridConstants', 'departamentoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<departamentoDtos.Dtos.Departamento.DepartamentoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public departamentoService: departamentoService.Services.DepartamentoService) {

            super(
                $state,
                uiGridConstants,
                departamentoService, {
                    rotaAlterarRegistro: 'tenant.global.departamento.alterar',
                    rotaNovoRegistro: 'tenant.global.departamento.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new departamentoDtos.Dtos.Departamento.GetDepartamentoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<departamentoDtos.Dtos.Departamento.DepartamentoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.departamentoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDepartamento', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Departamento.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Departamento.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Departamento.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Departamento.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Departamento.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Departamento.Descricao'),
                field: 'descricao'
            });
        }
    }

    export class DepartamentoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/departamento/departamentoIndex.cshtml';
            this.controller = DepartamentoIndexController;
        }
    }
}