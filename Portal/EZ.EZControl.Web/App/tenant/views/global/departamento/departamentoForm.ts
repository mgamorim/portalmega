﻿import * as departamentoService from "../../../../services/domain/global/departamento/departamentoService";
import * as departamentoDtos from "../../../../dtos/global/departamento/departamentoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class DepartamentoFormController extends controllers.Controllers.ControllerFormComponentBase<departamentoDtos.Dtos.Departamento.DepartamentoListDto, departamentoDtos.Dtos.Departamento.DepartamentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoDepartamento: (termoDigitadoPesquisa: string) => any;
        private departamentoSelecionado: (registro: departamentoDtos.Dtos.Departamento.DepartamentoListDto) => string;
        private departamentoDeselecionado: () => void;
        private requestParamsDepartamento: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'departamentoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public departamentoService: departamentoService.Services.DepartamentoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                departamentoService,
                'tenant.global.departamento');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new departamentoDtos.Dtos.Departamento.DepartamentoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;  
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DepartamentoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/departamento/departamentoForm.cshtml';
            this.controller = DepartamentoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}