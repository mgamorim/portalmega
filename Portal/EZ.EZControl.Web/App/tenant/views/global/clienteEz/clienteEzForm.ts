﻿import * as controllers from "../../../../common/controllers/controller";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as clienteEzService from "../../../../services/domain/global/clienteEz/clienteEzService";
import * as clienteEzDtos from "../../../../dtos/global/clienteEz/clienteEzDtos";

export module Forms {
    export class ClienteEzFormController extends controllers.Controllers.ControllerFormComponentBase<clienteEzDtos.Dtos.ClienteEz.ClienteEZDto, clienteEzDtos.Dtos.ClienteEz.ClienteEZInput, clienteEzDtos.Dtos.ClienteEz.ClienteEZPessoaIdDto>{
        public form: ng.IFormController;
        public clienteEzCopy: clienteEzDtos.Dtos.ClienteEz.ClienteEZInput;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'clienteEzService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public clienteEzService: clienteEzService.Services.ClienteEzService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                clienteEzService,
                'tenant.global.clienteEz');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new clienteEzDtos.Dtos.ClienteEz.ClienteEZInput();
                instance.id = 0;
                instance.isActive = true;

                this.clienteEzCopy = angular.copy(instance);

                return instance;
            };

            this.events.onGetEntity = (entity) => {
                this.clienteEzCopy = angular.copy(entity);
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.clienteEZId;

                this.clienteEzCopy = angular.copy(this.entity);
                // Definir o formulario como não interagido (pristine = true) com o usuário e limpo (dirty = false)
                this.form.$setPristine();
            };

            // Abortar para não ir para rota de listagem
            this.events.onBeforeCancel = () => {
                return true;
            }

            this.events.onAfterCancel = () => {
                this.entity = angular.copy(this.clienteEzCopy);
                this.form.$setPristine();
            }

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.form = form;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ClienteEzFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/clienteEz/clienteEzForm.cshtml';
            this.controller = ClienteEzFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}