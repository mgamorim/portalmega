﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as contatoService from "../../../../services/domain/global/contato/contatoService";
import * as contatoDtos from "../../../../dtos/global/contato/contatoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Contato {

    class ContatoIndexController extends controllers.Controllers.ControllerIndexBase<contatoDtos.Dtos.Contato.ContatoListDto, contatoDtos.Dtos.Contato.ContatoInput>{

        static $inject = ['$state', 'uiGridConstants', 'contatoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<contatoDtos.Dtos.Contato.ContatoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public contatoService: contatoService.Services.ContatoService) {

            super(
                $state,
                uiGridConstants,
                contatoService, {
                    rotaAlterarRegistro: 'tenant.global.contato.alterar',
                    rotaNovoRegistro: 'tenant.global.contato.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = "Nome";

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new contatoDtos.Dtos.Contato.GetContatoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.cargo = termoDigitadoPesquisa;
                filtro.setor = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<contatoDtos.Dtos.Contato.ContatoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.contatoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewContato', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Contato.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Contato.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Contato.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pessoa'),
                field: 'pessoa.nomePessoa'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.Tratamento'),
                field: 'tratamento.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.TipoDeContato'),
                field: 'tipoDeContato.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.Sexo'),
                field: 'sexoValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.Cargo'),
                field: 'cargo'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.Setor'),
                field: 'setor'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Contato.Idioma'),
                field: 'idioma'
            });
        }
    }

    export class ContatoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/contato/contatoIndex.cshtml';
            this.controller = ContatoIndexController;
        }
    }
}