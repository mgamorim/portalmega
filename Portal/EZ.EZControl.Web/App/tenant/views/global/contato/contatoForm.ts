﻿import * as contatoService from "../../../../services/domain/global/contato/contatoService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as contatoDtos from "../../../../dtos/global/contato/contatoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as tipoDeContatoDtos from "../../../../dtos/global/tipoDeContato/tipoDeContatoDtos";
import * as tratamentoDtos from "../../../../dtos/global/tratamento/tratamentoDtos";
import * as tipoDeContatoService from "../../../../services/domain/global/tipoDeContato/tipoDeContatoService";
import * as tratamentoService from "../../../../services/domain/global/tratamento/tratamentoService";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class ContatoFormController extends controllers.Controllers.ControllerFormComponentBase<contatoDtos.Dtos.Contato.ContatoListDto, contatoDtos.Dtos.Contato.ContatoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public sexos: any;

        // bindings do component
        public operation: string;
        public isDetail: boolean;
        public tipoPessoa: number;
        public pessoaId: number;
        public permissaoAlterarRegistro: string;
        public permissaoExcluirRegistro: string;
        public permissaoCriarRegistro: string;

        // Objetos, variaveis e métodos quando a entidade é um detalhe
        //============================================================
        //
        public saving: boolean;
        public editing: boolean;        
        public editar: (contato: contatoDtos.Dtos.Contato.ContatoListDto) => void;
        public excluir: (contato: contatoDtos.Dtos.Contato.ContatoListDto) => void;
        public limparEntidade: () => void;

        // grid
        private setDataGrid: (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<contatoDtos.Dtos.Contato.ContatoListDto>) => void;
        private gridApi: uiGrid.IGridApiOf<contatoDtos.Dtos.Contato.ContatoListDto>;
        private requestParamsContato: requestParam.RequestParam.RequestParams;
        private contatoAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<contatoDtos.Dtos.Contato.ContatoListDto>;
        public getContatos: () => void;
        public loadingContatos: boolean;
        public contatoGridOptions: uiGrid.IGridOptionsOf<contatoDtos.Dtos.Contato.ContatoListDto>;
        private gridData: contatoDtos.Dtos.Contato.ContatoListDto[];
        //===============================================================

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        // tipo de contato
        public tipoDeContatoFixo: any;
        public ezSelectTipoDeContato: ezSelect.EzSelect.EzSelect<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDeContato: (termoDigitadoPesquisa: string) => any;
        private tipoDeContatoSelecionado: (registro: tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto) => string;
        private tipoDeContatoDeselecionado: () => void;
        private requestParamsTipoDeContato: requestParam.RequestParam.RequestParams;

        // tratamento
        public ezSelectTratamento: ezSelect.EzSelect.EzSelect<tratamentoDtos.Dtos.Tratamento.TratamentoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTratamento: (termoDigitadoPesquisa: string) => any;
        private tratamentoSelecionado: (registro: tratamentoDtos.Dtos.Tratamento.TratamentoListDto) => string;
        private tratamentoDeselecionado: () => void;
        private requestParamsTratamento: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'contatoService', 'pessoaService', 'tipoDeContatoService', 'tratamentoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public contatoService: contatoService.Services.ContatoService,
            public pessoaService: pessoaService.Services.PessoaService,
            public tipoDeContatoService: tipoDeContatoService.Services.TipoDeContatoService,
            public tratamentoService: tratamentoService.Services.TratamentoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                contatoService,
                'tenant.global.contato');

            this.carregarPessoa = (id: number) => {
                this.ezSelectPessoa.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    if (!this.isDetail) {
                        this.ezSelectPessoa.setInputText(result.nomePessoa);
                    } else {
                        this.entity.pessoaId = this.pessoa.id;
                    }
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                });
            }

            this.getContatos = () => {
                this.loadingContatos = true;

                var filtro = new contatoDtos.Dtos.Contato.GetContatoInput();
                filtro.pessoaId = this.pessoaId;

                var promise = this.contatoService.getPaged(filtro, this.requestParamsContato);

                if (promise) {
                    promise.then(result => {
                        this.setDataGrid(result);
                    }).finally(() => {
                        this.loadingContatos = false;
                    });
                }
            }

            this.setDataGrid = (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<contatoDtos.Dtos.Contato.ContatoListDto>) => {
                this.gridData.length = 0;

                for (var i = 0; i < result.items.length; i++) {
                    this.gridData.push(result.items[i]);
                }

                this.contatoGridOptions.totalItems = result.totalCount;
            };

            this.editar = (contato: contatoDtos.Dtos.Contato.ContatoListDto) => {
                this.editing = true;
                this.getEntityById(contato.id);
            }

            this.excluir = (contato: contatoDtos.Dtos.Contato.ContatoListDto) => {
                this.editing = false;
                this.delete(contato);
            }

            this.limparEntidade = () => {
                this.entity = new contatoDtos.Dtos.Contato.ContatoInput();
                this.ezSelectTipoDeContato.gridSelectClear();
                this.ezSelectTipoDeContato.gridSelectClear();
                this.ezSelectTratamento.gridSelectClear();

                if (this.isDetail)
                    this.entity.pessoaId = this.pessoa.id;
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new contatoDtos.Dtos.Contato.ContatoInput();
                instance.id = 0;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaId) {
                    this.carregarPessoa(entity.pessoaId);
                }

                // tipo de contato
                if (entity.tipoDeContatoId) {
                    this.ezSelectTipoDeContato.loading = true;
                    // nesse caso usei let ao invés de var, para a variavel promise existir apenas no escopo desse if
                    let promise = this.tipoDeContatoService.getById(entity.tipoDeContatoId);
                    promise.then(result => {
                        this.ezSelectTipoDeContato.setInputText(result.descricao);
                    }).finally(() => {
                        this.ezSelectTipoDeContato.loading = false;
                    });
                }

                // tratamento
                if (entity.tratamentoId) {
                    this.ezSelectTratamento.loading = true;
                    let promiseTratamento = this.tratamentoService.getById(entity.tratamentoId);
                    promiseTratamento.then(result => {
                        this.ezSelectTratamento.setInputText(result.descricao);
                    }).finally(() => {
                        this.ezSelectTratamento.loading = false;
                    });
                }
            };
            this.events.onBeforeCancel = () => {
                // se caso for detalhe, o método cancel será abortado, para depois no evento onAfterCancel implementar alguma lógica necessária.
                return this.isDetail;
            };
            this.events.onAfterCancel = () => {
                // no cancel, a entidade da controller receberá uma nova instancia
                this.editing = false;
                this.limparEntidade();
            };
            this.events.onAfterSaveEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getContatos();
                    this.limparEntidade();
                    this.editing = false;
                }
            };
            this.events.onDeletedEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getContatos();
                }
            };

            this.sexos = ez.domain.enum.sexoEnum.valores;

            this.ezSelectPessoaConfig();
            this.ezSelectTipoDeContatoConfig();
            this.ezSelectTratamentoConfig();
            this.contatoGridConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaId) {
                this.carregarPessoa(this.pessoaId);
            }
        }

        $onChanges(changes) {
            if (changes.tipoPessoa) {
                if (changes.tipoPessoa.currentValue !== changes.tipoPessoa.previousValue) {

                    if (this.pessoa)
                        this.pessoa.tipoPessoa = changes.tipoPessoa.currentValue;
                }
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaSelecionado = (registro) => {
                this.pessoa = registro;
                this.entity.pessoaId = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.entity.pessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.NomePessoa'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.TipoPessoa'),
                    field: 'tipoPessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.ContatoPrincipal.Numero'),
                    field: 'contatoPrincipal.numero'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.ContatoPrincipal.TipoDeContato'),
                    field: 'tipoDeContatoFixoValue'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDeContatoConfig() {
            this.requestParamsTipoDeContato = new requestParam.RequestParam.RequestParams();
            this.requestParamsTipoDeContato.sorting = "Descricao";

            this.tipoDeContatoSelecionado = (registro) => {
                this.entity.tipoDeContatoId = registro.id;
                this.tipoDeContatoFixo = registro.tipoDeContatoFixo;
                return registro.descricao;
            }

            this.tipoDeContatoDeselecionado = () => {
                this.entity.tipoDeContatoId = null;
                this.tipoDeContatoFixo = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDeContato = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeContatoDtos.Dtos.TipoDeContato.GetTipoDeContatoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDeContato = new ezSelect.EzSelect.EzSelect<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeContatoService,
                this.getFiltroParaPaginacaoTipoDeContato,
                this.requestParamsTipoDeContato,
                this.$uibModal,
                this.tipoDeContatoSelecionado,
                this.tipoDeContatoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeContato.onEzGridCreated = () => {
                this.ezSelectTipoDeContato.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.tipoDeContatoService.getPaged(filtro, requestParams);
                };

                this.ezSelectTipoDeContato.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeContato.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTipoDeContato.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeContato.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTipoDeContato.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeContato.TipoPessoa'),
                    field: 'tipoPessoaValue'
                });

                this.ezSelectTipoDeContato.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeContato.TipoDeContatoFixo'),
                    field: 'tipoDeContatoFixoValue'
                });

                this.ezSelectTipoDeContato.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDeContato.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDeContato.ezGrid.getRegistros();
            };
        }

        private ezSelectTratamentoConfig() {
            this.requestParamsTratamento = new requestParam.RequestParam.RequestParams();

            this.tratamentoSelecionado = (registro) => {
                this.entity.tratamentoId = registro.id;
                return registro.descricao;
            };

            this.tratamentoDeselecionado = () => {
                this.entity.tratamentoId = null;
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTratamento = (termoDigitadoPesquisa) => {
                var filtro = new tratamentoDtos.Dtos.Tratamento.GetTratamentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTratamento = new ezSelect.EzSelect.EzSelect<tratamentoDtos.Dtos.Tratamento.TratamentoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tratamentoService,
                this.getFiltroParaPaginacaoTratamento,
                this.requestParamsTratamento,
                this.$uibModal,
                this.tratamentoSelecionado,
                this.tratamentoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTratamento.onEzGridCreated = () => {
                this.ezSelectTratamento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Tratamento.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTratamento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Tratamento.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTratamento.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTratamento.on.beforeOpenModalDialog = () => {
                this.ezSelectTratamento.ezGrid.getRegistros();
            };
        }

        // metodos usados quando a entidade é um detalhe
        private contatoGridConfig() {
            if (this.isDetail) {
                this.requestParamsContato = new requestParam.RequestParam.RequestParams();
                this.requestParamsContato.sorting = 'Nome';

                this.contatoAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<contatoDtos.Dtos.Contato.ContatoListDto>();

                this.contatoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoAlterarRegistro);
                    }
                ));

                this.contatoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoExcluirRegistro);
                    }
                ));

                this.gridData = new Array<contatoDtos.Dtos.Contato.ContatoListDto>();

                this.contatoGridOptions = {
                    enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    showColumnFooter: true,
                    rowHeight: 50,
                    enableGridMenu: true,
                    exporterMenuCsv: false,
                    exporterMenuPdf: false,
                    rowTemplate: 'App/common/views/common/grid/linha.html',
                    columnDefs: [],
                    appScopeProvider: this.contatoAppScopeProvider,
                    onRegisterApi: (api) => {
                        this.gridApi = api;
                        this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                this.requestParamsContato.sorting = null;
                            } else {
                                this.requestParamsContato.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            this.getContatos();
                        });
                        this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                            this.requestParamsContato.skipCount = (pageNumber - 1) * pageSize;
                            this.requestParamsContato.maxResultCount = pageSize;

                            this.getContatos();
                        });
                    }
                }

                this.contatoGridOptions.data = this.gridData;

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.nome'),
                    field: 'nome',
                    cellTemplate: '\
                        <div class="ui-grid-cell-contents">\
                            {{grid.getCellValue(row, col)}}<br />\
                            <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                <span>\
                                    <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                </span> \
                            </span>\
                        </div>'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Pessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.Tratamento'),
                    field: 'tratamento.descricao'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.TipoDeContato'),
                    field: 'tipoDeContato.descricao'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.Sexo'),
                    field: 'sexoValue'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.Cargo'),
                    field: 'cargo'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.Setor'),
                    field: 'setor'
                });

                this.contatoGridOptions.columnDefs.push({
                    name: app.localize('Contato.Idioma'),
                    field: 'idioma'
                });

                this.getContatos();
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ContatoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/contato/contatoForm.cshtml';
            this.controller = ContatoFormController;
            this.bindings = {
                operation: '@',
                isDetail: '<',
                tipoPessoa: '<',
                pessoaId: '<',
                permissaoAlterarRegistro: '@',
                permissaoExcluirRegistro: '@',
                permissaoCriarRegistro: '@'
            }
        }
    }
}