﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Estado {

    class EstadoIndexController extends controllers.Controllers.ControllerIndexBase<estadoDtos.Dtos.Estado.EstadoListDto, estadoDtos.Dtos.Estado.EstadoInput>{

        static $inject = ['$state', 'uiGridConstants', 'estadoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<estadoDtos.Dtos.Estado.EstadoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public estadoService: estadoService.Services.EstadoService) {

            super(
                $state,
                uiGridConstants,
                estadoService, {
                    rotaAlterarRegistro: 'tenant.global.estado.alterar',
                    rotaNovoRegistro: 'tenant.global.estado.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new estadoDtos.Dtos.Estado.GetEstadoInput();
                filtro.nomeEstado = termoDigitadoPesquisa;
                filtro.siglaEstado = termoDigitadoPesquisa;
                filtro.capital = termoDigitadoPesquisa;
                filtro.codigoIBGE = termoDigitadoPesquisa;
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<estadoDtos.Dtos.Estado.EstadoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewEstado', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Estado.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Estado.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Estado.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estado.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estado.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estado.CodigoIBGE'),
                field: 'codigoIBGE'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estado.Sigla'),
                field: 'sigla'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estado.Capital'),
                field: 'capital'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Estado.Pais'),
                field: 'pais.nome'
            });
        }
    }

    export class EstadoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/estado/estadoIndex.cshtml';
            this.controller = EstadoIndexController;
        }
    }
}