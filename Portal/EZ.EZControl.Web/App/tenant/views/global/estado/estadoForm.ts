﻿import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class EstadoFormController extends controllers.Controllers.ControllerFormComponentBase<estadoDtos.Dtos.Estado.EstadoListDto, estadoDtos.Dtos.Estado.EstadoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectPais: ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private paisDeselecionado: () => void;
        private requestParamsPais: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'estadoService', 'paisService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public estadoService: estadoService.Services.EstadoService,
            public paisService: paisService.Services.PaisService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                estadoService,
                'tenant.global.estado');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new estadoDtos.Dtos.Estado.EstadoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;  
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.ezSelectPais.loading = true;
                var promise = this.paisService.getById(entity.paisId);
                promise.then(result => {
                    this.ezSelectPais.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectPais.loading = false;
                });
            };

            this.ezSelectPaisConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectPaisConfig() {
            this.requestParamsPais = new requestParam.RequestParam.RequestParams();

            this.paisSelecionado = (registro) => {
                this.entity.paisId = registro.id;
                return registro.nome;
            }

            this.paisDeselecionado = () => {
                this.entity.paisId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPais = (termoDigitadoPesquisa) => {
                var filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPais = new ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paisService,
                this.getFiltroParaPaginacaoPais,
                this.requestParamsPais,
                this.$uibModal,
                this.paisSelecionado,
                this.paisDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPais.onEzGridCreated = () => {
                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nome'),
                    field: 'nome'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.CodigoISO'),
                    field: 'codigoISO'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nacionalidade'),
                    field: 'nacionalidade'
                });

                this.ezSelectPais.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPais.on.beforeOpenModalDialog = () => {
                this.ezSelectPais.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EstadoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/estado/estadoForm.cshtml';
            this.controller = EstadoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}