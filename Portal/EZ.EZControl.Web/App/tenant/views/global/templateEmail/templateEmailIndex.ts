﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as templateEmailService from "../../../../services/domain/global/templateEmail/templateEmailService";
import * as templateEmailDtos from "../../../../dtos/global/templateEmail/templateEmailDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.TemplateEmail {
    class TemplateEmailIndexController extends controllers.Controllers.ControllerIndexBase<templateEmailDtos.Dtos.TemplateEmail.TemplateEmailListDto, templateEmailDtos.Dtos.TemplateEmail.TemplateEmailInput>{
        static $inject = ['$state', 'uiGridConstants', 'templateEmailService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<templateEmailDtos.Dtos.TemplateEmail.TemplateEmailListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public templateEmailService: templateEmailService.Services.TemplateEmailService) {
            super(
                $state,
                uiGridConstants,
                templateEmailService, {
                    rotaAlterarRegistro: 'tenant.global.templateEmail.alterar',
                    rotaNovoRegistro: 'tenant.global.templateEmail.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new templateEmailDtos.Dtos.TemplateEmail.GetTemplateEmailInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.remetente = termoDigitadoPesquisa;
                filtro.assunto = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<templateEmailDtos.Dtos.TemplateEmail.TemplateEmailListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.templateEmailService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTemplateDeEmail', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.TemplateEmail.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TemplateEmail.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TemplateEmail.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TemplateDeEmail.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TemplateDeEmail.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TemplateDeEmail.Assunto'),
                field: 'assunto'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TemplateDeEmail.Remetente'),
                field: 'remetente'
            });
        }
    }

    export class TemplateEmailIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/templateEmail/templateEmailIndex.cshtml';
            this.controller = TemplateEmailIndexController;
        }
    }
}