﻿import * as templateEmailService from "../../../../services/domain/global/templateEmail/templateEmailService";
import * as templateEmailDtos from "../../../../dtos/global/templateEmail/templateEmailDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class TemplateEmailFormController extends controllers.Controllers.ControllerFormComponentBase<templateEmailDtos.Dtos.TemplateEmail.TemplateEmailListDto, templateEmailDtos.Dtos.TemplateEmail.TemplateEmailInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoTemplateEmail: (termoDigitadoPesquisa: string) => any;
        private templateEmailSelecionado: (registro: templateEmailDtos.Dtos.TemplateEmail.TemplateEmailListDto) => string;
        private templateEmailDeselecionado: () => void;
        private requestParamsTemplateEmail: requestParam.RequestParam.RequestParams;
        public tipoDeTemplateDeMensagemEnum: any;
        public modeloDeDado: string;
        public tinyMce: {};

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'templateEmailService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public templateEmailService: templateEmailService.Services.TemplateEmailService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                templateEmailService,
                'tenant.global.templateEmail');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new templateEmailDtos.Dtos.TemplateEmail.TemplateEmailInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.tipoChanged(entity.tipoDeTemplateDeMensagem);
            };
            this.tipoDeTemplateDeMensagemEnum = ez.domain.enum.tipoDeTemplateDeMensagemEnum.valores;
            this.modeloDeDado = '';
            this.tinyMce = {
                statusbar: true,
                menubar: false
            }

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        tipoChanged(change: string) {
            if (this.entity.tipoDeTemplateDeMensagem != null) {
                this.modeloDeDado = ez.domain.enum.tipoDeDocumentoEnum.getItemPorValor(this.entity.tipoDeTemplateDeMensagem).descricao;
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TemplateEmailFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/templateEmail/templateEmailForm.cshtml';
            this.controller = TemplateEmailFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}