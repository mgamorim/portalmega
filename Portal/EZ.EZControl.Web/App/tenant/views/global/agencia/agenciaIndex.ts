﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as agenciaService from "../../../../services/domain/global/agencia/agenciaService";
import * as agenciaDtos from "../../../../dtos/global/agencia/agenciaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Agencia {

    class AgenciaIndexController extends controllers.Controllers.ControllerIndexBase<agenciaDtos.Dtos.Agencia.AgenciaListDto, agenciaDtos.Dtos.Agencia.AgenciaInput>{

        static $inject = ['$state', 'uiGridConstants', 'agenciaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<agenciaDtos.Dtos.Agencia.AgenciaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public agenciaService: agenciaService.Services.AgenciaService) {

            super(
                $state,
                uiGridConstants,
                agenciaService, {
                    rotaAlterarRegistro: 'tenant.global.agencia.alterar',
                    rotaNovoRegistro: 'tenant.global.agencia.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new agenciaDtos.Dtos.Agencia.GetAgenciaInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                filtro.codigoBanco = termoDigitadoPesquisa;
                filtro.nomeBanco = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<agenciaDtos.Dtos.Agencia.AgenciaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.agenciaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewAgencia', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Agencia.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Agencia.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Agencia.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agencia.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agencia.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agencia.Codigo'),
                field: 'codigo'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agencia.Digito'),
                field: 'digitoVerificador'
            });
        }
    }

    export class AgenciaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/agencia/agenciaIndex.cshtml';
            this.controller = AgenciaIndexController;
        }
    }
}