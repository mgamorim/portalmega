﻿import * as agenciaService from "../../../../services/domain/global/agencia/agenciaService";
import * as bancoService from "../../../../services/domain/global/banco/bancoService";
import * as bancoDtos from "../../../../dtos/global/banco/bancoDtos";
import * as agenciaDtos from "../../../../dtos/global/agencia/agenciaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class AgenciaFormController extends controllers.Controllers.ControllerFormComponentBase<agenciaDtos.Dtos.Agencia.AgenciaListDto, agenciaDtos.Dtos.Agencia.AgenciaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectBanco: ezSelect.EzSelect.EzSelect<bancoDtos.Dtos.Banco.BancoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoBanco: (termoDigitadoPesquisa: string) => any;
        private bancoSelecionado: (registro: bancoDtos.Dtos.Banco.BancoListDto) => string;
        private bancoDeselecionado: () => void;
        private requestParamsBanco: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'agenciaService', 'bancoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public agenciaService: agenciaService.Services.AgenciaService,
            public bancoService: bancoService.Services.BancoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                agenciaService,
                'tenant.global.agencia');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new agenciaDtos.Dtos.Agencia.AgenciaInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.ezSelectBanco.loading = true;
                var promise = this.bancoService.getById(entity.bancoId);
                promise.then(result => {
                    this.ezSelectBanco.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectBanco.loading = false;
                });
            }
            this.ezSelectBancoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectBancoConfig() {
            this.requestParamsBanco = new requestParam.RequestParam.RequestParams();

            this.bancoSelecionado = (registro) => {
                this.entity.bancoId = registro.id;
                return registro.nome;
            }

            this.bancoDeselecionado = () => {
                this.entity.bancoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoBanco = (termoDigitadoPesquisa) => {
                var filtro = new bancoDtos.Dtos.Banco.GetBancoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectBanco = new ezSelect.EzSelect.EzSelect<bancoDtos.Dtos.Banco.BancoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.bancoService,
                this.getFiltroParaPaginacaoBanco,
                this.requestParamsBanco,
                this.$uibModal,
                this.bancoSelecionado,
                this.bancoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectBanco.onEzGridCreated = () => {
                this.ezSelectBanco.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Banco.Nome'),
                    field: 'nome'
                });

                this.ezSelectBanco.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Banco.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectBanco.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Banco.Codigo'),
                    field: 'codigo'
                });

                this.ezSelectBanco.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectBanco.on.beforeOpenModalDialog = () => {
                this.ezSelectBanco.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class AgenciaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/agencia/agenciaForm.cshtml';
            this.controller = AgenciaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}