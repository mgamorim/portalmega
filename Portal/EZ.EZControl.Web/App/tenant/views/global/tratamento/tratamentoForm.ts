﻿import * as tratamentoService from "../../../../services/domain/global/tratamento/tratamentoService";
import * as tratamentoDtos from "../../../../dtos/global/tratamento/tratamentoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class TratamentoFormController extends controllers.Controllers.ControllerFormComponentBase<tratamentoDtos.Dtos.Tratamento.TratamentoListDto, tratamentoDtos.Dtos.Tratamento.TratamentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoTratamento: (termoDigitadoPesquisa: string) => any;
        private tratamentoSelecionado: (registro: tratamentoDtos.Dtos.Tratamento.TratamentoListDto) => string;
        private tratamentoDeselecionado: () => void;
        private requestParamsTratamento: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'tratamentoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tratamentoService: tratamentoService.Services.TratamentoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                tratamentoService,
                'tenant.global.tratamento');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new tratamentoDtos.Dtos.Tratamento.TratamentoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;            
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TratamentoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tratamento/tratamentoForm.cshtml';
            this.controller = TratamentoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}