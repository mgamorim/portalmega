﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as tratamentoService from "../../../../services/domain/global/tratamento/tratamentoService";
import * as tratamentoDtos from "../../../../dtos/global/tratamento/tratamentoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Tratamento {

    class TratamentoIndexController extends controllers.Controllers.ControllerIndexBase<tratamentoDtos.Dtos.Tratamento.TratamentoListDto, tratamentoDtos.Dtos.Tratamento.TratamentoInput>{

        static $inject = ['$state', 'uiGridConstants', 'tratamentoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<tratamentoDtos.Dtos.Tratamento.TratamentoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tratamentoService: tratamentoService.Services.TratamentoService) {

            super(
                $state,
                uiGridConstants,
                tratamentoService, {
                    rotaAlterarRegistro: 'tenant.global.tratamento.alterar',
                    rotaNovoRegistro: 'tenant.global.tratamento.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new tratamentoDtos.Dtos.Tratamento.GetTratamentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<tratamentoDtos.Dtos.Tratamento.TratamentoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tratamentoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTratamento', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Tratamento.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Tratamento.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Tratamento.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Tratamento.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Tratamento.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class TratamentoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tratamento/tratamentoIndex.cshtml';
            this.controller = TratamentoIndexController;
        }
    }
}