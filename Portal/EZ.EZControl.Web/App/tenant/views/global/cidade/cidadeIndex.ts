﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Cidade {

    class CidadeIndexController extends controllers.Controllers.ControllerIndexBase<cidadeDtos.Dtos.Cidade.CidadeListDto, cidadeDtos.Dtos.Cidade.CidadeInput>{

        static $inject = ['$state', 'uiGridConstants', 'cidadeService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public cidadeService: cidadeService.Services.CidadeService) {

            super(
                $state,
                uiGridConstants,
                cidadeService, {
                    rotaAlterarRegistro: 'tenant.global.cidade.alterar',
                    rotaNovoRegistro: 'tenant.global.cidade.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new cidadeDtos.Dtos.Cidade.GetCidadeInput();
                filtro.nomeCidade = termoDigitadoPesquisa;
                filtro.codigoIBGE = termoDigitadoPesquisa;
                filtro.nomeEstado = termoDigitadoPesquisa;
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.cidadeService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewCidade', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Cidade.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Cidade.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Cidade.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cidade.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cidade.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pais'),
                field: 'estado.pais.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cidade.Estado'),
                field: 'estado.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Cidade.CodigoIBGE'),
                field: 'codigoIBGE'
            });
        }
    }

    export class CidadeIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/cidade/cidadeIndex.cshtml';
            this.controller = CidadeIndexController;
        }
    }
}