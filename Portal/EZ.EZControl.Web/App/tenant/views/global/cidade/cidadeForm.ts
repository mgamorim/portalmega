﻿import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class CidadeFormController extends controllers.Controllers.ControllerFormComponentBase<cidadeDtos.Dtos.Cidade.CidadeListDto, cidadeDtos.Dtos.Cidade.CidadeInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectPais: ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private paisDeselecionado: () => void;
        private estadoDeselecionado: () => void;
        private requestParamsPais: requestParam.RequestParam.RequestParams;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'cidadeService', 'paisService', 'estadoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public cidadeService: cidadeService.Services.CidadeService,
            public paisService: paisService.Services.PaisService,
            public estadoService: estadoService.Services.EstadoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                cidadeService,
                'tenant.global.cidade');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new cidadeDtos.Dtos.Cidade.CidadeInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pais
                this.ezSelectPais.loading = true;
                var promisePais = this.paisService.getById(entity.paisId);
                promisePais.then(result => {
                    this.ezSelectPais.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectPais.loading = false;
                });

                // estado
                this.ezSelectEstado.loading = true;
                var promiseEstado = this.estadoService.getById(entity.estadoId);
                promiseEstado.then(result => {
                    this.ezSelectEstado.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectEstado.loading = false;
                });
            };
            this.ezSelectPaisConfig();
            this.ezSelectEstadoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectPaisConfig() {
            this.requestParamsPais = new requestParam.RequestParam.RequestParams();

            this.paisSelecionado = (registro) => {
                this.entity.paisId = registro.id;
                return registro.nome;
            };

            this.paisDeselecionado = () => {
                this.entity.paisId = null;
                this.entity.estadoId = null;

                this.ezSelectEstado.gridSelectClear();
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPais = (termoDigitadoPesquisa) => {
                var filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPais = new ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paisService,
                this.getFiltroParaPaginacaoPais,
                this.requestParamsPais,
                this.$uibModal,
                this.paisSelecionado,
                this.paisDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPais.onEzGridCreated = () => {
                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nome'),
                    field: 'nome'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.CodigoISO'),
                    field: 'codigoISO'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nacionalidade'),
                    field: 'nacionalidade'
                });

                this.ezSelectPais.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPais.on.beforeOpenModalDialog = () => {
                this.ezSelectPais.ezGrid.getRegistros();
            };
        }

        private ezSelectEstadoConfig() {
            this.requestParamsEstado = new requestParam.RequestParam.RequestParams();

            this.estadoSelecionado = (registro) => {
                this.entity.estadoId = registro.id;
                return registro.nome;
            }

            this.estadoDeselecionado = () => {
                this.entity.estadoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstado = (termoDigitadoPesquisa) => {
                var filtro = null;
                if (!this.entity.paisId) {
                    abp.message.info(app.localize('Pais.SelecaoDoPaisNecessaria'), '');
                } else {
                    filtro = new estadoDtos.Dtos.Estado.GetEstadoByPaisInput();
                    filtro.paisId = this.entity.paisId;
                    filtro.nomeEstado = termoDigitadoPesquisa;
                    filtro.siglaEstado = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectEstado = new ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoEstado,
                this.requestParamsEstado,
                this.$uibModal,
                this.estadoSelecionado,
                this.estadoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstado.onEzGridCreated = () => {
                this.ezSelectEstado.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.estadoService.getEstadosByPaisId(filtro, requestParams);
                };

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Sigla'),
                    field: 'sigla'
                });

                this.ezSelectEstado.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstado.on.beforeOpenModalDialog = () => {
                if (this.entity.paisId) {
                    this.ezSelectEstado.ezGrid.getRegistros();
                }
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class CidadeFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/cidade/cidadeForm.cshtml';
            this.controller = CidadeFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}