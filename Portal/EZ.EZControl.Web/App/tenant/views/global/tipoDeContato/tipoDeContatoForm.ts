﻿import * as tipoDeContatoService from "../../../../services/domain/global/tipoDeContato/tipoDeContatoService";
import * as tipoDeContatoDtos from "../../../../dtos/global/tipoDeContato/tipoDeContatoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class TipoDeContatoFormController extends controllers.Controllers.ControllerFormComponentBase<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto, tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoTipoDeContato: (termoDigitadoPesquisa: string) => any;
        private tipoDeContatoSelecionado: (registro: tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto) => string;
        private tipoDeContatoDeselecionado: () => void;
        private requestParamsTipoDeContato: requestParam.RequestParam.RequestParams;
        public tipoDeContatoEnum: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'tipoDeContatoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeContatoService: tipoDeContatoService.Services.TipoDeContatoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                tipoDeContatoService,
                'tenant.global.tipoDeContato');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;    
            };
            this.tipoDeContatoEnum = ez.domain.enum.tipoDeContatoEnum.valores;

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TipoDeContatoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tipoDeContato/tipoDeContatoForm.cshtml';
            this.controller = TipoDeContatoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}