﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as tipoDeContatoService from "../../../../services/domain/global/tipoDeContato/tipoDeContatoService";
import * as tipoDeContatoDtos from "../../../../dtos/global/tipoDeContato/tipoDeContatoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.TipoDeContato {

    class TipoDeContatoIndexController extends controllers.Controllers.ControllerIndexBase<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto, tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoInput>{

        static $inject = ['$state', 'uiGridConstants', 'tipoDeContatoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeContatoService: tipoDeContatoService.Services.TipoDeContatoService) {

            super(
                $state,
                uiGridConstants,
                tipoDeContatoService, {
                    rotaAlterarRegistro: 'tenant.global.tipoDeContato.alterar',
                    rotaNovoRegistro: 'tenant.global.tipoDeContato.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeContatoDtos.Dtos.TipoDeContato.GetTipoDeContatoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeContatoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTipoDeContato', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.TipoDeContato.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TipoDeContato.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TipoDeContato.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeContato.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeContato.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeContato.TipoDeContatoFixo'),
                field: 'tipoDeContatoFixoValue'
            });
        }
    }

    export class TipoDeContatoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tipoDeContato/tipoDeContatoIndex.cshtml';
            this.controller = TipoDeContatoIndexController;
        }
    }
}