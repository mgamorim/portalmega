﻿import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as grupoPessoaService from "../../../../services/domain/global/grupoPessoa/grupoPessoaService";

export module Forms {
    export class PessoaFisicaFormController extends controllers.Controllers.ControllerFormComponentBase<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, pessoaDtos.Dtos.Pessoa.PessoaFisicaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public pessoaFisicaForm: ng.IFormController;
        private estadosCivis: any;
        private sexos: any;

        public getFiltroParaPaginacaoPessoaFisica: (termoDigitadoPesquisa: string) => any;
        private pessoaFisicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto) => string;
        private pessoaFisicaDeselecionado: () => void;
        private requestParamsPessoaFisica: requestParam.RequestParam.RequestParams;

        // grupo pessoa
        public ezSelectGrupoPessoa: ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoGrupoPessoa: (termoDigitadoPesquisa: string) => any;
        private grupoPessoaSelecionado: (registro: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto) => string;
        private grupoPessoaDeselecionado: () => void;
        private requestParamsGrupoPessoa: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'pessoaFisicaService', 'grupoPessoaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public pessoaFisicaService: pessoaFisicaService.Services.PessoaFisicaService,
            public grupoPessoaService: grupoPessoaService.Services.GrupoPessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                pessoaFisicaService,
                'tenant.global.pessoaFisica');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.tipoPessoa = 1;
                instance.grupoPessoaId = null;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // grupo pessoa
                if (entity.grupoPessoaId) {
                    this.ezSelectGrupoPessoa.loading = true;
                    var promise = this.grupoPessoaService.getById(entity.grupoPessoaId);
                    promise.then(result => {
                        this.ezSelectGrupoPessoa.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectGrupoPessoa.loading = false;
                    });
                };

                this.acertarDatas();
            };
            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.ezSelectGrupoPessoaConfig();

            this.sexos = ez.domain.enum.sexoEnum.valores;
            this.estadosCivis = ez.domain.enum.estadoCivilEnum.valores;
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectGrupoPessoaConfig() {
            this.requestParamsGrupoPessoa = new requestParam.RequestParam.RequestParams();

            this.grupoPessoaSelecionado = (registro) => {
                this.entity.grupoPessoaId = registro.id;
                return registro.nome;
            }

            this.grupoPessoaDeselecionado = () => {
                this.entity.grupoPessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoGrupoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput();
                filtro.grupoPessoaId = this.entity.grupoPessoaId;
                filtro.tipoDePessoa = 1;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGrupoPessoa = new ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.grupoPessoaService,
                this.getFiltroParaPaginacaoGrupoPessoa,
                this.requestParamsGrupoPessoa,
                this.$uibModal,
                this.grupoPessoaSelecionado,
                this.grupoPessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGrupoPessoa.onEzGridCreated = () => {
                this.ezSelectGrupoPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.grupoPessoaService.getGruposPessoaExceptForId(filtro, requestParams);
                };

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Nome'),
                    field: 'nome'
                });

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectGrupoPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGrupoPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectGrupoPessoa.ezGrid.getRegistros();
            };
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.pessoaFisicaForm = form;
        }

        private acertarDatas() {
            if (this.entity.dataDeNascimento)
                this.entity.dataDeNascimento = new Date(this.entity.dataDeNascimento.toString());
        }

    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PessoaFisicaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/pessoaFisica/pessoaFisicaForm.cshtml';
            this.controller = PessoaFisicaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}