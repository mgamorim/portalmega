﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.PessoaFisica {

    class PessoaFisicaIndexController extends controllers.Controllers.ControllerIndexBase<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, pessoaDtos.Dtos.Pessoa.PessoaFisicaInput>{

        static $inject = ['$state', 'uiGridConstants', 'pessoaFisicaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public pessoaFisicaService: pessoaFisicaService.Services.PessoaFisicaService) {

            super(
                $state,
                uiGridConstants,
                pessoaFisicaService, {
                    rotaAlterarRegistro: 'tenant.global.pessoaFisica.alterar',
                    rotaNovoRegistro: 'tenant.global.pessoaFisica.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'Nome';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.descricaoGrupoPessoa = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaFisicaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPessoaFisica', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisica.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisica.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisica.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Nome'),
                field: 'nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Sexo'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "sexoEnum", "sexo")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.EstadoCivil'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "estadoCivilEnum", "estadoCivil")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.DataDeNascimento'),
                field: 'dataDeNascimento',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Nacionalidade'),
                field: 'nacionalidade'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.GrupoPessoa'),
                field: 'grupoPessoa.nome'
            });
        }
    }

    export class PessoaFisicaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/pessoaFisica/pessoaFisicaIndex.cshtml';
            this.controller = PessoaFisicaIndexController;
        }
    }
}