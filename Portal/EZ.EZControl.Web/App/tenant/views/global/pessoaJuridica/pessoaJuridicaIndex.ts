﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.PessoaJuridica {

    class PessoaJuridicaIndexController extends controllers.Controllers.ControllerIndexBase<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput>{

        static $inject = ['$state', 'uiGridConstants', 'pessoaJuridicaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public pessoaJuridicaService: pessoaJuridicaService.Services.PessoaJuridicaService) {

            super(
                $state,
                uiGridConstants,
                pessoaJuridicaService, {
                    rotaAlterarRegistro: 'tenant.global.pessoaJuridica.alterar',
                    rotaNovoRegistro: 'tenant.global.pessoaJuridica.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'NomeFantasia';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.descricaoGrupoPessoa = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaJuridicaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPessoaJuridica', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.PessoaJuridica.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.PessoaJuridica.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.PessoaJuridica.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.NomeFantasia'),
                field: 'nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.RazaoSocial'),
                field: 'razaoSocial'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.GrupoPessoa'),
                field: 'grupoPessoa.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.DocumentoPrincipal.TipoDeDocumento'),
                field: 'tipoDeDocumentoFixoValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.DocumentoPrincipal.Numero'),
                field: 'documentoPrincipal.numero'
            });
        }
    }

    export class PessoaJuridicaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/pessoaJuridica/pessoaJuridicaIndex.cshtml';
            this.controller = PessoaJuridicaIndexController;
        }
    }
}