﻿import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class PaisFormController extends controllers.Controllers.ControllerFormComponentBase<paisDtos.Dtos.Pais.PaisListDto, paisDtos.Dtos.Pais.PaisInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private paisDeselecionado: () => void;
        private requestParamsPais: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'paisService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public paisService: paisService.Services.PaisService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                paisService,
                'tenant.global.pais');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new paisDtos.Dtos.Pais.PaisInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PaisFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/pais/paisForm.cshtml';
            this.controller = PaisFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}