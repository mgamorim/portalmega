﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Pais {

    class PaisIndexController extends controllers.Controllers.ControllerIndexBase<paisDtos.Dtos.Pais.PaisListDto, paisDtos.Dtos.Pais.PaisInput>{

        static $inject = ['$state', 'uiGridConstants', 'paisService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<paisDtos.Dtos.Pais.PaisListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public paisService: paisService.Services.PaisService) {

            super(
                $state,
                uiGridConstants,
                paisService, {
                    rotaAlterarRegistro: 'tenant.global.pais.alterar',
                    rotaNovoRegistro: 'tenant.global.pais.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                filtro.nacionalidade = termoDigitadoPesquisa;
                filtro.codigoISO = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<paisDtos.Dtos.Pais.PaisListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paisService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPais', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Pais.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Pais.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Pais.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pais.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pais.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pais.CodigoISO'),
                field: 'codigoISO'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pais.Nacionalidade'),
                field: 'nacionalidade'
            });
        }
    }

    export class PaisIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/pais/paisIndex.cshtml';
            this.controller = PaisIndexController;
        }
    }
}