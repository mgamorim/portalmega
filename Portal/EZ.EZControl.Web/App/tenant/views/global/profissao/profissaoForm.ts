﻿import * as profissaoService from "../../../../services/domain/global/profissao/profissaoService";
import * as profissaoDtos from "../../../../dtos/global/profissao/profissaoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ProfissaoFormController extends controllers.Controllers.ControllerFormComponentBase<profissaoDtos.Dtos.Profissao.ProfissaoListDto, profissaoDtos.Dtos.Profissao.ProfissaoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoProfissao: (termoDigitadoPesquisa: string) => any;
        private profissaoSelecionado: (registro: profissaoDtos.Dtos.Profissao.ProfissaoListDto) => string;
        private profissaoDeselecionado: () => void;
        private requestParamsProfissao: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'profissaoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public profissaoService: profissaoService.Services.ProfissaoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                profissaoService,
                'tenant.global.profissao');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new profissaoDtos.Dtos.Profissao.ProfissaoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance; 
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ProfissaoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/profissao/profissaoForm.cshtml';
            this.controller = ProfissaoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}