﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as profissaoService from "../../../../services/domain/global/profissao/profissaoService";
import * as profissaoDtos from "../../../../dtos/global/profissao/profissaoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Profissao {
    class ProfissaoIndexController extends controllers.Controllers.ControllerIndexBase<profissaoDtos.Dtos.Profissao.ProfissaoListDto, profissaoDtos.Dtos.Profissao.ProfissaoInput>{
        static $inject = ['$state', 'uiGridConstants', 'profissaoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public profissaoService: profissaoService.Services.ProfissaoService) {
            super(
                $state,
                uiGridConstants,
                profissaoService, {
                    rotaAlterarRegistro: 'tenant.global.profissao.alterar',
                    rotaNovoRegistro: 'tenant.global.profissao.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new profissaoDtos.Dtos.Profissao.GetProfissaoInput();
                filtro.codigo = termoDigitadoPesquisa;
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<profissaoDtos.Dtos.Profissao.ProfissaoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.profissaoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewProfissao', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Profissao.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Profissao.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Profissao.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Profissao.Titulo'),
                field: 'titulo',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Profissao.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Profissao.Codigo'),
                field: 'codigo'
            });
        }
    }

    export class ProfissaoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/profissao/profissaoIndex.cshtml';
            this.controller = ProfissaoIndexController;
        }
    }
}