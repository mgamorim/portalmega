﻿import * as tipoDeLogradouroService from "../../../../services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class TipoDeLogradouroFormController extends controllers.Controllers.ControllerFormComponentBase<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto, tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoTipoDeLogradouro: (termoDigitadoPesquisa: string) => any;
        private tipoDeLogradouroSelecionado: (registro: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto) => string;
        private tipoDeLogradouroDeselecionado: () => void;
        private requestParamsTipoDeLogradouro: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'tipoDeLogradouroService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeLogradouroService: tipoDeLogradouroService.Services.TipoDeLogradouroService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                tipoDeLogradouroService,
                'tenant.global.tipoDeLogradouro');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TipoDeLogradouroFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tipoDeLogradouro/tipoDeLogradouroForm.cshtml';
            this.controller = TipoDeLogradouroFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}