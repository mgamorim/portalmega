﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as tipoDeLogradouroService from "../../../../services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.TipoDeLogradouro {
    class TipoDeLogradouroIndexController extends controllers.Controllers.ControllerIndexBase<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto, tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroInput>{
        static $inject = ['$state', 'uiGridConstants', 'tipoDeLogradouroService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeLogradouroService: tipoDeLogradouroService.Services.TipoDeLogradouroService) {
            super(
                $state,
                uiGridConstants,
                tipoDeLogradouroService, {
                    rotaAlterarRegistro: 'tenant.global.tipoDeLogradouro.alterar',
                    rotaNovoRegistro: 'tenant.global.tipoDeLogradouro.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.GetTipoDeLogradouroInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeLogradouroService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTipoDeLogradouro', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.TipoDeLogradouro.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TipoDeLogradouro.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TipoDeLogradouro.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeLogradouro.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeLogradouro.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class TipoDeLogradouroIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tipoDeLogradouro/tipoDeLogradouroIndex.cshtml';
            this.controller = TipoDeLogradouroIndexController;
        }
    }
}