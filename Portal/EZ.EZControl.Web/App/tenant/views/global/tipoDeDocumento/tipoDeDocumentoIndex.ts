﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as tipoDeDocumentoService from "../../../../services/domain/global/tipoDeDocumento/tipoDeDocumentoService";
import * as tipoDeDocumentoDtos from "../../../../dtos/global/tipoDeDocumento/tipoDeDocumentoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.TipoDeDocumento {
    class TipoDeDocumentoIndexController extends controllers.Controllers.ControllerIndexBase<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto, tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoInput>{
        static $inject = ['$state', 'uiGridConstants', 'tipoDeDocumentoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeDocumentoService: tipoDeDocumentoService.Services.TipoDeDocumentoService) {
            super(
                $state,
                uiGridConstants,
                tipoDeDocumentoService, {
                    rotaAlterarRegistro: 'tenant.global.tipoDeDocumento.alterar',
                    rotaNovoRegistro: 'tenant.global.tipoDeDocumento.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeDocumentoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTipoDeDocumento', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.TipoDeDocumento.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TipoDeDocumento.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.TipoDeDocumento.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.TipoPessoa'),
                field: 'tipoPessoaValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.TipoDeDocumentoFixo'),
                field: 'tipoDeDocumentoFixoValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                field: 'principal',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.ObrigaAnexo'),
                field: 'obrigaAnexo',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.ObrigaOrgaoExpedidor'),
                field: 'obrigaOrgaoExpedidor',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('TipoDeDocumento.ObrigaDataExpedicao'),
                field: 'obrigaDataExpedicao',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class TipoDeDocumentoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tipoDeDocumento/tipoDeDocumentoIndex.cshtml';
            this.controller = TipoDeDocumentoIndexController;
        }
    }
}