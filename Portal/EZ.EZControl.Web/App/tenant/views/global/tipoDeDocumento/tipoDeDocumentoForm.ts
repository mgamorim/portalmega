﻿import * as tipoDeDocumentoService from "../../../../services/domain/global/tipoDeDocumento/tipoDeDocumentoService";
import * as tipoDeDocumentoDtos from "../../../../dtos/global/tipoDeDocumento/tipoDeDocumentoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class TipoDeDocumentoFormController extends controllers.Controllers.ControllerFormComponentBase<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto, tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoTipoDeDocumento: (termoDigitadoPesquisa: string) => any;
        private tipoDeDocumentoSelecionado: (registro: tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto) => string;
        private tipoDeDocumentoDeselecionado: () => void;
        private requestParamsTipoDeDocumento: requestParam.RequestParam.RequestParams;
        public tipoDeDocumentoEnum: any;
        public tipoPessoaEnum: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'tipoDeDocumentoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeDocumentoService: tipoDeDocumentoService.Services.TipoDeDocumentoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                tipoDeDocumentoService,
                'tenant.global.tipoDeDocumento');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.tipoPessoaChanged(entity.tipoPessoa);
            };
            this.tipoDeDocumentoEnum = ez.domain.enum.tipoDeDocumentoEnum.valores;
            this.tipoPessoaEnum = ez.domain.enum.tipoPessoaEnum.valores;

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        tipoPessoaChanged(change: string) {
            //TODO Corrigir o filtro de tipos de documento por tipo de pessoa
            //if (this.entity.tipoPessoa != null) {
            //    this.tipoDeDocumentoEnum = ez.domain.enum.tipoDeDocumentoEnum.getItemPorValor(this.entity.tipoPessoa);
            //}
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TipoDeDocumentoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/tipoDeDocumento/tipoDeDocumentoForm.cshtml';
            this.controller = TipoDeDocumentoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}