﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as empresaService from "../../../../services/domain/global/empresa/empresaService";
import * as empresaDtos from "../../../../dtos/global/empresa/empresaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Empresa {

    class EmpresaIndexController extends controllers.Controllers.ControllerIndexBase<empresaDtos.Dtos.Empresa.EmpresaListDto, empresaDtos.Dtos.Empresa.EmpresaInput>{

        static $inject = ['$state', 'uiGridConstants', 'empresaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public empresaService: empresaService.Services.EmpresaService) {

            super(
                $state,
                uiGridConstants,
                empresaService, {
                    rotaAlterarRegistro: 'tenant.global.empresa.alterar',
                    rotaNovoRegistro: 'tenant.global.empresa.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new empresaDtos.Dtos.Empresa.GetEmpresaInput();
                filtro.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<empresaDtos.Dtos.Empresa.EmpresaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.empresaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewEmpresa', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Empresa.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Empresa.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Empresa.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.NomeFantasia'),
                field: 'nomeFantasia',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaJuridica.RazaoSocial'),
                field: 'razaoSocial'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Empresa.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class EmpresaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/empresa/empresaIndex.cshtml';
            this.controller = EmpresaIndexController;
        }
    }
}