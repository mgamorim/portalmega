﻿import * as empresaService from "../../../../services/domain/global/empresa/empresaService";
import * as empresaDtos from "../../../../dtos/global/empresa/empresaDtos";
import * as pessoaJuridicaService from "../../../../services/domain/global/pessoaJuridica/pessoaJuridicaService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as clienteEzService from "../../../../services/domain/global/clienteEz/clienteEzService";
import * as clienteEzDtos from "../../../../dtos/global/clienteEz/clienteEzDtos";
import * as userDtos from "../../../../dtos/core/user/userDtos";
import * as userService from "../../../../services/domain/core/user/userService";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class EmpresaFormController extends controllers.Controllers.ControllerFormComponentBase<empresaDtos.Dtos.Empresa.EmpresaListDto, empresaDtos.Dtos.Empresa.EmpresaInput, empresaDtos.Dtos.Empresa.EmpresaPessoaIdDto>{
        public empresaForm: ng.IFormController;

        public getFiltroParaPaginacaoEmpresa: (termoDigitadoPesquisa: string) => any;
        private empresaSelecionado: (registro: empresaDtos.Dtos.Empresa.EmpresaListDto) => string;
        private empresaDeselecionado: () => void;
        private requestParamsEmpresa: requestParam.RequestParam.RequestParams;

        public ezSelectUsuario: ezSelect.EzSelect.EzSelect<userDtos.Dtos.User.Core.UserEditDto, requestParam.RequestParam.RequestParams>;
        public getFiltroUsuario: (termoDigitadoPesquisa: string) => any;
        private usuarioSelecionado: (registro: userDtos.Dtos.User.Core.UserEditDto) => string;
        private usuarioDeselecionado: () => void;
        private requestParamsUsuario: requestParam.RequestParam.RequestParams;

        public excluir: (usuario: userDtos.Dtos.User.Core.UserListDto) => void;
        public limparEntidade: () => void;
        public addUsuario: () => void;
        public SelectUsuarioAll: () => void;

        public usuarioSelecionadoId: number = 0;

        // grid de usuários
        private setDataGrid: (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<userDtos.Dtos.User.Core.UserListDto>) => void;
        private gridApi: uiGrid.IGridApiOf<userDtos.Dtos.User.Core.UserListDto>;
        private requestParamsGridUsuario: requestParam.RequestParam.RequestParams;
        private userAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<userDtos.Dtos.User.Core.UserListDto>;
        public getUsuarios: () => void;
        public loadingUsuarios: boolean;
        public usuarioGridOptions: uiGrid.IGridOptionsOf<userDtos.Dtos.User.Core.UserListDto>;
        private gridData: userDtos.Dtos.User.Core.UserListDto[];

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'empresaService', 'clienteEzService', 'userService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public empresaService: empresaService.Services.EmpresaService,
            public clienteEzService: clienteEzService.Services.ClienteEzService,
            public userService: userService.Services.UserService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                empresaService,
                'tenant.global.empresa');

            this.getUsuarios = () => {
                var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                input.id = this.entity.id;
                var promise = this.empresaService.getUsuariosEmpresa(input);
                promise.then((result) => {
                    this.setDataGrid(result);
                });
            };

            this.setDataGrid = (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<userDtos.Dtos.User.Core.UserListDto>) => {
                this.gridData.length = 0;

                for (var i = 0; i < result.items.length; i++) {
                    this.gridData.push(result.items[i]);
                }

                this.usuarioGridOptions.totalItems = result.totalCount;
            };

            this.excluir = (usuario: userDtos.Dtos.User.Core.UserListDto) => {
                var input = new empresaDtos.Dtos.Empresa.AssociacaoUsuarioEmpresaInput();
                input.empresaId = this.entity.id;
                input.usuarioId = usuario.id;
                var promise = empresaService.desassociaUsuarioEmpresa(input);
                promise.then(() => {
                    this.getUsuarios();
                });
            }

            this.addUsuario = () => {
                var input = new empresaDtos.Dtos.Empresa.AssociacaoUsuarioEmpresaInput();
                input.empresaId = this.entity.id;
                input.usuarioId = this.usuarioSelecionadoId;
                var promise = empresaService.associaUsuarioEmpresa(input);
                promise.then(() => {
                    this.getUsuarios();
                });
            };

            this.SelectUsuarioAll = () => {
                this.getUsuarios();
            };

        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new empresaDtos.Dtos.Empresa.EmpresaInput();
                instance.id = 0;
                instance.isActive = true;
                var promise = this.clienteEzService.get();
                promise.then(result => {
                    if (!result) {
                        abp.message.info('É necessário cadastrar o Cliente EZ.', '');
                    } else {
                        instance.clienteEzId = result.id;
                    };
                });
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
            };
            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.empresaId;
            };

            this.ezSelectUsuarioConfig();
            this.ezGridUsuarioConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();            
        }

        private ezGridUsuarioConfig() {
            this.requestParamsGridUsuario = new requestParam.RequestParam.RequestParams();
            this.requestParamsGridUsuario.sorting = 'Name';

            this.userAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<userDtos.Dtos.User.Core.UserListDto>();

            this.userAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridData = new Array<userDtos.Dtos.User.Core.UserListDto>();

            this.usuarioGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.userAppScopeProvider,
                onRegisterApi: (api) => {
                    this.gridApi = api;
                    this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsGridUsuario.sorting = null;
                        } else {
                            this.requestParamsGridUsuario.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getUsuarios();
                    });
                    this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsGridUsuario.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsGridUsuario.maxResultCount = pageSize;

                        this.getUsuarios();
                    });
                }
            }

            this.usuarioGridOptions.data = this.gridData;


            this.usuarioGridOptions.columnDefs.push({
                name: app.localize('Name'),
                field: 'name',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.usuarioGridOptions.columnDefs.push({
                name: app.localize('Surname'),
                field: 'surname'
            });

            this.usuarioGridOptions.columnDefs.push({
                name: app.localize('UserName'),
                field: 'userName'
            });

            if (this.entity && this.entity.id > 0)
                this.getUsuarios();
        }

        private slugifyNome() {
            this.entity.slug = this.entity.nomeFantasia.slugify();
            while (this.entity.slug.indexOf('-') > -1) {
                this.entity.slug = this.entity.slug.slugify().replace('-', '');
            }
        }

        private padronizaSlug() {
            this.entity.slug = this.entity.slug.slugify();
            while (this.entity.slug.indexOf('-') > -1) {
                this.entity.slug = this.entity.slug.slugify().replace('-', '');
            }
        }

        private ezSelectUsuarioConfig() {
            this.requestParamsUsuario = new requestParam.RequestParam.RequestParams();

            this.usuarioSelecionado = (registro) => {
                this.usuarioSelecionadoId = registro.id;
                return registro.userName;
            }

            this.usuarioDeselecionado = () => {
                this.usuarioSelecionadoId = 0;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroUsuario = (termoDigitadoPesquisa) => {
                var filtro = new userDtos.Dtos.User.Core.GetUsersInput();
                filtro.filter = termoDigitadoPesquisa;
                filtro.permission = termoDigitadoPesquisa;
                filtro.role = isNaN(parseInt(termoDigitadoPesquisa)) ? null : parseInt(termoDigitadoPesquisa);
                return filtro;
            }

            this.ezSelectUsuario = new ezSelect.EzSelect.EzSelect<userDtos.Dtos.User.Core.UserListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.userService,
                this.getFiltroUsuario,
                this.requestParamsUsuario,
                this.$uibModal,
                this.usuarioSelecionado,
                this.usuarioDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectUsuario.onEzGridCreated = () => {
                this.ezSelectUsuario.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    var input = new applicationServiceDtos.Dtos.ApplicationService.IdInput();
                    input.id = this.entity.id;
                    return this.empresaService.getUsuariosNaoAssociados(input);
                };

                this.ezSelectUsuario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Name'),
                    field: 'name'
                });

                this.ezSelectUsuario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Surname'),
                    field: 'surname'
                });

                this.ezSelectUsuario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('UserName'),
                    field: 'userName'
                });

                this.ezSelectUsuario.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('EmailAddress'),
                    field: 'emailAddress'
                });

                this.ezSelectUsuario.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectUsuario.on.beforeOpenModalDialog = () => {
                this.ezSelectUsuario.ezGrid.getRegistros();
            };
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.empresaForm = form;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EmpresaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/empresa/empresaForm.cshtml';
            this.controller = EmpresaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}