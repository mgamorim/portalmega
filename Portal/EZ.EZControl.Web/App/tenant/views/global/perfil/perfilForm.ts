﻿// componentes
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";

import * as perfilService from "../../../../services/domain/global/perfil/perfilService";
import * as perfilDtos from "../../../../dtos/global/perfil/perfilDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "App/dtos/global/pessoa/pessoaDtos";
import * as enderecoEletronicoDtos from "App/dtos/global/enderecoEletronico/enderecoEletronicoDtos";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as grupoPessoaService from "../../../../services/domain/global/grupoPessoa/grupoPessoaService";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as tipoDeLogradouroService from "../../../../services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";
import * as cidadeDtos from "App/dtos/global/cidade/cidadeDtos";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as enderecoDtos from "App/dtos/global/endereco/enderecoDtos";
import * as enderecoService from "../../../../services/domain/global/endereco/enderecoService";

export module Forms {
    export class PerfilFormController extends
        controllers.Controllers.ControllerFormComponentBase<perfilDtos.Dtos.EZLiv.Perfil.PerfilInput,
        perfilDtos.Dtos.EZLiv.Perfil.PerfilInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>
    {

        private sexos: any;
        private estadosCivis: any;
        private tiposDeResponsavel: any;
        private grausDeParentesco: any;
        private idadeDo: number;

        public isRgReadonly: boolean;
        public isCpfReadonly: boolean;

        //Dados Gerais
        public perfilInput = new perfilDtos.Dtos.EZLiv.Perfil.PerfilInput();

        private typeAccount: any = [
            { nome: "Selecione...", codigo: null, descricao: "Selecione..." },
            //{ nome: "Conta Corrente", codigo: null, descricao: "" },
            { nome: "Conta Corrente de Pessoa Física", codigo: '001', descricao: "" },
            //{ nome: "Conta Simples de Pessoa Física", codigo: '002', descricao: "" },
            { nome: "Conta Corrente de Pessoa Jurídica", codigo: '003', descricao: "" }
            //{ nome: "Poupança de Pessoa Física", codigo: '0013', descricao: "" },
            //{ nome: "Poupança de Pessoa Jurídica", codigo: '0022', descricao: "" }
        ];

        //Validar Documento
        public validaNumeroDocumento: () => any;

        // grupo pessoa
        public ezSelectGrupoPessoa: ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoGrupoPessoa: (termoDigitadoPesquisa: string) => any;
        private grupoPessoaSelecionado: (registro: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto) => string;
        private grupoPessoaDeselecionado: () => void;
        private requestParamsGrupoPessoa: requestParam.RequestParam.RequestParams;
        private carregarGrupoPessoa: (id: number) => void;

        // Tipo de logradouro
        public ezSelectTipoDeLogradouro: ezSelect.EzSelect.
        EzSelect<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDeLogradouro: (termoDigitadoPesquisa: string) => any;
        private tipoDeLogradouroSelecionado: (registro: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.
            TipoDeLogradouroListDto) => string;
        private tipoDeLogradouroDeselecionado: () => void;
        private requestParamsTipoDeLogradouro: requestParam.RequestParam.RequestParams;
        private carregarTipoDeLogradouro: (id: number) => void;

        // Cidade
        public ezSelectCidade: ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto,
        requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCidade: (termoDigitadoPesquisa: string) => any;
        private cidadeSelecionado: (registro: cidadeDtos.Dtos.Cidade.CidadeListDto) => string;
        private cidadeDeselecionado: () => void;
        private requestParamsCidade: requestParam.RequestParam.RequestParams;
        private carregarCidade: (id: number) => void;

        static $inject = ['$scope', '$state', '$stateParams', 'uiGridConstants', 'perfilService', 'grupoPessoaService', 'estadoService', 'tipoDeLogradouroService', 'cidadeService', 'enderecoService', '$uibModal'];

        constructor(
            public $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public perfilService: perfilService.Services.PerfilService,
            public grupoPessoaService: grupoPessoaService.Services.GrupoPessoaService,
            public estadoService: estadoService.Services.EstadoService,
            public tipoDeLogradouroService: tipoDeLogradouroService.Services.TipoDeLogradouroService,
            public cidadeService: cidadeService.Services.CidadeService,
            public enderecoService: enderecoService.Services.EnderecoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                perfilService,
                'tenant.perfil');


            this.validaNumeroDocumento = () => {
                var listaServicosValidacao = [];
                listaServicosValidacao.push(abp.services.app.global.tipoDeDocumento.isCpf);
                return listaServicosValidacao;
            }

            this.carregarGrupoPessoa = (id: number) => {
                this.ezSelectGrupoPessoa.loading = true;
                var promise = this.grupoPessoaService.getById(id);
                promise.then(result => {
                    var grupoPessoa = result;
                    this.ezSelectGrupoPessoa.setInputText(grupoPessoa.nome);
                }).finally(() => {
                    this.ezSelectGrupoPessoa.loading = false;
                });
            }

            this.carregarTipoDeLogradouro = (id: number) => {
                if (id > 0) {

                    var promise = this.tipoDeLogradouroService.getById(id);
                    promise.then(result => {
                        var tipoDeLogradouro = result;
                        this.ezSelectTipoDeLogradouro.setInputText(tipoDeLogradouro.descricao);
                    }).finally(() => {

                    });
                }
            }

            this.carregarCidade = (id: number) => {
                if (id > 0) {

                    var promise = this.cidadeService.getById(id);
                    promise.then(result => {
                        var cidade = result;
                        this.ezSelectCidade.setInputText(cidade.nome);
                    }).finally(() => {

                    });
                }
            }

        }

        $onInit() {
            super.init();

            this.sexos = ez.domain.enum.sexoEnum.valores;
            this.estadosCivis = ez.domain.enum.estadoCivilEnum.valores;
            this.tiposDeResponsavel = ez.domain.enum.tipoDeResponsavelEnum.valores;
            this.grausDeParentesco = ez.domain.enum.grauDeParentescoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new perfilDtos.Dtos.EZLiv.Perfil.PerfilInput;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (this.entity != null) {
                    this.entity.dataDeNascimento = this.entity.dataDeNascimento != null ? new Date(this.entity.dataDeNascimento.toString()) : new Date();
                }

                if (this.entity.cpf) {
                  this.isCpfReadonly = true;
                }

                if (this.entity.rg) {
                  this.isRgReadonly = true;
                }                

                //if (this.entity != null && this.entity.grupoPessoaId > 0)
                //    this.carregarGrupoPessoa(this.entity.grupoPessoaId);

                if (this.entity && this.entity.enderecoPrincipal) {
                    if (this.entity.enderecoPrincipal.tipoDeLogradouroId > 0)
                       this.carregarTipoDeLogradouro(this.entity.enderecoPrincipal.tipoDeLogradouroId);

                    if (this.entity.enderecoPrincipal.cidadeId > 0)
                        this.carregarCidade(this.entity.enderecoPrincipal.cidadeId);
                }

                if (this.entity['corretorDadosBancario'] == null) {
                    this.entity['corretorDadosBancario'] = new perfilDtos.Dtos.EZLiv.Perfil.CorretorDadosBancarioInput();
                    this.entity['corretorDadosBancario'].bancoId = 0;
                    this.entity['corretorDadosBancario'].tipoConta = this.typeAccount[0].codigo;
                }

            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.id;
            };

            this.events.onBeforeSaveEntity = () => {
            };

            this.carregarTipoDeLogradouro = (id: number) => {
                if (id > 0) {

                    var promise = this.tipoDeLogradouroService.getById(id);
                    promise.then(result => {
                        var tipoDeLogradouro = result;
                        this.ezSelectTipoDeLogradouro.setInputText(tipoDeLogradouro.descricao);
                    }).finally(() => {

                    });
                }
            }

            this.ezSelectGrupoPessoaConfig();
            this.ezSelectTipoDeLogradouroConfig();
            this.ezSelectCidadeConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();


        }

        private carregarEzSelects(endereco) {
            this.carregarCidade(endereco.cidadeId);
            this.carregarTipoDeLogradouro(endereco.tipoDeLogradouroId);
        }

        private clearComponents() {
            this.entity.enderecoPrincipal.logradouro = '';
            this.entity.enderecoPrincipal.bairro = '';
            this.entity.enderecoPrincipal.complemento = '';
            this.ezSelectTipoDeLogradouro.gridSelectClear();
            this.ezSelectCidade.gridSelectClear();
        }

        private ezSelectGrupoPessoaConfig() {
            this.requestParamsGrupoPessoa = new requestParam.RequestParam.RequestParams();

            this.grupoPessoaSelecionado = (registro) => {
                this.entity.grupoPessoaId = registro.id;
                return registro.nome;
            }

            this.grupoPessoaDeselecionado = () => {
                this.entity.grupoPessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoGrupoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput();
                filtro.grupoPessoaId = this.entity.grupoPessoaId;
                filtro.tipoDePessoa = 1;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGrupoPessoa = new ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.grupoPessoaService,
                this.getFiltroParaPaginacaoGrupoPessoa,
                this.requestParamsGrupoPessoa,
                this.$uibModal,
                this.grupoPessoaSelecionado,
                this.grupoPessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGrupoPessoa.onEzGridCreated = () => {
                this.ezSelectGrupoPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.grupoPessoaService.getGruposPessoaExceptForId(filtro, requestParams);
                };

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Nome'),
                    field: 'nome'
                });

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectGrupoPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGrupoPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectGrupoPessoa.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDeLogradouroConfig() {
            this.requestParamsTipoDeLogradouro = new requestParam.RequestParam.RequestParams();

            this.tipoDeLogradouroSelecionado = (registro) => {
                this.entity.enderecoPrincipal.tipoDeLogradouroId = registro.id;
                return registro.descricao;
            }

            this.tipoDeLogradouroDeselecionado = () => {
                this.entity.enderecoPrincipal.tipoDeLogradouroId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDeLogradouro = (termoDigitadoPesquisa) => {
                var filtro = null;

                filtro = new tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.GetTipoDeLogradouroInput();
                filtro.descricao = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectTipoDeLogradouro = new ezSelect.EzSelect
                .EzSelect<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto,
                requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoTipoDeLogradouro,
                this.requestParamsTipoDeLogradouro,
                this.$uibModal,
                this.tipoDeLogradouroSelecionado,
                this.tipoDeLogradouroDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeLogradouro.onEzGridCreated = () => {
                this.ezSelectTipoDeLogradouro.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.tipoDeLogradouroService.getPaged(filtro, requestParams);
                };

                this.ezSelectTipoDeLogradouro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeLogradouro.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTipoDeLogradouro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeLogradouro.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTipoDeLogradouro.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDeLogradouro.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectTipoDeLogradouro.ezGrid.getRegistros();
                //}
            };
        }

        private ezSelectCidadeConfig() {
            this.requestParamsCidade = new requestParam.RequestParam.RequestParams();

            this.cidadeSelecionado = (registro) => {
                this.entity.enderecoPrincipal.cidadeId = registro.id;
                return registro.nome;
            }

            this.cidadeDeselecionado = () => {
                this.entity.enderecoPrincipal.cidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCidade = (termoDigitadoPesquisa) => {
            }

            this.ezSelectCidade = new ezSelect.EzSelect
                .EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoCidade,
                this.requestParamsCidade,
                this.$uibModal,
                this.cidadeSelecionado,
                this.cidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCidade.onEzGridCreated = () => {
                this.ezSelectCidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.cidadeService.getPaged(filtro, requestParams);
                };

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectCidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCidade.on.beforeOpenModalDialog = () => {
                //if (this.paisId) {
                this.ezSelectCidade.ezGrid.getRegistros();
                //}
            };
        }

        getEndereco = () => {
            if (this.entity &&
                this.entity.enderecoPrincipal.cep &&
                this.entity.enderecoPrincipal.cep.length === 8) {
                var filtro = new enderecoDtos.Dtos.Endereco.GetEnderecoByCepInput();
                filtro.cep = this.entity.enderecoPrincipal.cep;
                var pessoaId = this.entity.id;
                var promise = this.enderecoService.getEnderecoByCep(filtro);

                if (promise) {
                    promise.then(result => {
                        this.entity.enderecoPrincipal= result;
                        this.carregarTipoDeLogradouro(result.tipoDeLogradouroId);
                        this.carregarCidade(result.cidadeId);
                        this.carregarEzSelects(this.entity.id);
                    }).finally(() => {

                    });
                }
            } else {
                this.clearComponents();
            }
        };
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PerfilFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/perfil/perfilForm.cshtml';
            this.controller = PerfilFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}