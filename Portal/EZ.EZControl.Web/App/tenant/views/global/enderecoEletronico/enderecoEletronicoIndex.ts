﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as enderecoService from "../../../../services/domain/global/endereco/enderecoService";
import * as enderecoDtos from "../../../../dtos/global/endereco/enderecoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Endereco {

    class EnderecoIndexController extends controllers.Controllers.ControllerIndexBase<enderecoDtos.Dtos.Endereco.EnderecoListDto, enderecoDtos.Dtos.Endereco.EnderecoInput>{

        static $inject = ['$state', 'uiGridConstants', 'enderecoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<enderecoDtos.Dtos.Endereco.EnderecoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public enderecoService: enderecoService.Services.EnderecoService) {

            super(
                $state,
                uiGridConstants,
                enderecoService, {
                    rotaAlterarRegistro: 'tenant.global.endereco.alterar',
                    rotaNovoRegistro: 'tenant.global.endereco.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = 'Descricao';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new enderecoDtos.Dtos.Endereco.GetEnderecoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<enderecoDtos.Dtos.Endereco.EnderecoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.enderecoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewEndereco', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Endereco.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Endereco.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Endereco.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pessoa'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Endereco.TipoEndereco'),
                field: 'tipoEnderecoValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Endereco.EnderecoCompleto'),
                field: 'enderecoCompleto'
            });
        }
    }

    export class EnderecoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/endereco/enderecoIndex.cshtml';
            this.controller = EnderecoIndexController;
        }
    }
}