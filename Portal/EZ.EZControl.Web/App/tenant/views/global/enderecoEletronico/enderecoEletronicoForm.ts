﻿import * as enderecoEletronicoService from "../../../../services/domain/global/enderecoEletronico/enderecoEletronicoService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as enderecoEletronicoDtos from "../../../../dtos/global/enderecoEletronico/enderecoEletronicoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as tipoDeLogradouroService from "../../../../services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class EnderecoEletronicoFormController extends controllers.Controllers.ControllerFormComponentBase<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto, enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public tipoDeEnderecosEletronicos : any;

        // bindings do component
        public operation: string;
        public isDetail: boolean;
        public tipoPessoa: number;
        public pessoaId: number;
        public permissaoAlterarRegistro: string;
        public permissaoExcluirRegistro: string;
        public permissaoCriarRegistro: string;

        // Objetos, variaveis e métodos quando a entidade é um detalhe
        //============================================================
        //
        public saving: boolean;
        public editing: boolean;        
        public editar: (enderecoEletronico: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto) => void;
        public excluir: (enderecoEletronico: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto) => void;
        public limparEntidade: () => void;

        // grid
        private setDataGrid: (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>) => void;
        private gridApi: uiGrid.IGridApiOf<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>;
        private requestParamsEnderecoEletronico: requestParam.RequestParam.RequestParams;
        private enderecoEletronicoAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>;
        public getEnderecoEletronicos: () => void;
        public loadingEnderecoEletronicos: boolean;
        public enderecoEletronicoGridOptions: uiGrid.IGridOptionsOf<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>;
        private gridData: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto[];
        //===============================================================

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'enderecoEletronicoService', 'pessoaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public enderecoEletronicoService: enderecoEletronicoService.Services.EnderecoEletronicoService,
            public pessoaService: pessoaService.Services.PessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                enderecoEletronicoService,
                'tenant.global.enderecoEletronico');

            this.carregarPessoa = (id: number) => {
                this.ezSelectPessoa.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    if (!this.isDetail) {
                        this.ezSelectPessoa.setInputText(result.nomePessoa);
                    } else {
                        this.entity.pessoaId = this.pessoa.id;
                    }
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                });
            }

            this.getEnderecoEletronicos = () => {
                this.loadingEnderecoEletronicos = true;

                var filtro = new enderecoEletronicoDtos.Dtos.EnderecoEletronico.GetEnderecoEletronicoInput();
                filtro.pessoaId = this.pessoaId;

                var promise = this.enderecoEletronicoService.getPaged(filtro, this.requestParamsEnderecoEletronico);

                if (promise) {
                    promise.then(result => {
                        this.setDataGrid(result);
                    }).finally(() => {
                        this.loadingEnderecoEletronicos = false;
                    });
                }
            }

            this.setDataGrid = (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>) => {
                this.gridData.length = 0;

                for (var i = 0; i < result.items.length; i++) {
                    this.gridData.push(result.items[i]);
                }

                this.enderecoEletronicoGridOptions.totalItems = result.totalCount;
            };

            this.editar = (enderecoEletronico: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto) => {
                this.editing = true;
                this.getEntityById(enderecoEletronico.id);
            }

            this.excluir = (enderecoEletronico: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto) => {
                this.editing = false;
                this.delete(enderecoEletronico);
            }

            this.limparEntidade = () => {
                this.entity = new enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput();

                if (this.isDetail)
                    this.entity.pessoaId = this.pessoa.id;
            }
        }

        $onInit() {
            super.init();

            this.tipoDeEnderecosEletronicos = ez.domain.enum.tipoDeEnderecoEletronicoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput();
                instance.id = 0;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaId) {
                    this.carregarPessoa(entity.pessoaId);
                }
            };
            this.events.onBeforeCancel = () => {
                // se caso for detalhe, o método cancel será abortado, para depois no evento onAfterCancel implementar alguma lógica necessária.
                return this.isDetail;
            };
            this.events.onAfterCancel = () => {
                // no cancel, a entidade da controller receberá uma nova instancia
                this.editing = false;
                this.limparEntidade();
            };
            this.events.onAfterSaveEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getEnderecoEletronicos();
                    this.limparEntidade();
                    this.editing = false;
                }
            };
            this.events.onDeletedEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getEnderecoEletronicos();
                }
            };

            this.ezSelectPessoaConfig();
            this.enderecoEletronicoGridConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaId) {
                this.carregarPessoa(this.pessoaId);
            }
        }

        $onChanges(changes) {
            if (changes.tipoPessoa) {
                if (changes.tipoPessoa.currentValue !== changes.tipoPessoa.previousValue) {

                    if (this.pessoa)
                        this.pessoa.tipoPessoa = changes.tipoPessoa.currentValue;
                }
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaSelecionado = (registro) => {
                this.pessoa = registro;
                this.entity.pessoaId = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.entity.pessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('EnderecoEletronico.Endereco'),
                    field: 'endereco'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('EnderecoEletronico.TipoDeEndereco'),
                    field: 'tipoDeEnderecoEletronicoValue'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        // metodos usados quando a entidade é um detalhe
        private enderecoEletronicoGridConfig() {
            if (this.isDetail) {
                this.requestParamsEnderecoEletronico = new requestParam.RequestParam.RequestParams();
                this.requestParamsEnderecoEletronico.sorting = 'Endereco';

                this.enderecoEletronicoAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>();

                this.enderecoEletronicoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoAlterarRegistro);
                    }
                ));

                this.enderecoEletronicoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoExcluirRegistro);
                    }
                ));

                this.gridData = new Array<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>();

                this.enderecoEletronicoGridOptions = {
                    enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    showColumnFooter: true,
                    rowHeight: 50,
                    enableGridMenu: true,
                    exporterMenuCsv: false,
                    exporterMenuPdf: false,
                    rowTemplate: 'App/common/views/common/grid/linha.html',
                    columnDefs: [],
                    appScopeProvider: this.enderecoEletronicoAppScopeProvider,
                    onRegisterApi: (api) => {
                        this.gridApi = api;
                        this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                this.requestParamsEnderecoEletronico.sorting = null;
                            } else {
                                this.requestParamsEnderecoEletronico.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            this.getEnderecoEletronicos();
                        });
                        this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                            this.requestParamsEnderecoEletronico.skipCount = (pageNumber - 1) * pageSize;
                            this.requestParamsEnderecoEletronico.maxResultCount = pageSize;

                            this.getEnderecoEletronicos();
                        });
                    }
                }

                this.enderecoEletronicoGridOptions.data = this.gridData;

                this.enderecoEletronicoGridOptions.columnDefs.push({
                    name: app.localize('EnderecoEletronico.Endereco'),
                    field: 'endereco',
                    cellTemplate: '\
                        <div class="ui-grid-cell-contents">\
                            {{grid.getCellValue(row, col)}}<br />\
                            <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                <span>\
                                    <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                </span> \
                            </span>\
                        </div>'
                });

                this.enderecoEletronicoGridOptions.columnDefs.push({
                    name: app.localize('EnderecoEletronico.TipoDeEndereco'),
                    field: 'tipoDeEnderecoEletronicoValue'
                });

                this.getEnderecoEletronicos();
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EnderecoEletronicoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/enderecoEletronico/enderecoEletronicoForm.cshtml';
            this.controller = EnderecoEletronicoFormController;
            this.bindings = {
                operation: '@',
                isDetail: '<',
                tipoPessoa: '<',
                pessoaId: '<',
                permissaoAlterarRegistro: '@',
                permissaoExcluirRegistro: '@',
                permissaoCriarRegistro: '@'
            }
        }
    }
}