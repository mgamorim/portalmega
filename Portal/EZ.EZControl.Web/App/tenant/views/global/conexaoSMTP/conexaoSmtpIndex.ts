﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as conexaoSmtpService from "../../../../services/domain/global/conexaoSmtp/conexaoSmtpService";
import * as conexaoSmtpDtos from "../../../../dtos/global/conexaoSmtp/conexaoSmtpDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.ConexaoSmtp {

    class ConexaoSmtpIndexController extends controllers.Controllers.ControllerIndexBase<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto, conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpInput>{

        static $inject = ['$state', 'uiGridConstants', 'conexaoSMTPService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public conexaoSmtpService: conexaoSmtpService.Services.ConexaoSmtpService) {

            super(
                $state,
                uiGridConstants,
                conexaoSmtpService, {
                    rotaAlterarRegistro: 'tenant.global.conexaoSMTP.alterar',
                    rotaNovoRegistro: 'tenant.global.conexaoSMTP.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new conexaoSmtpDtos.Dtos.ConexaoSmtp.GetConexaoSmtpInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.servidorSMTP = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.conexaoSmtpService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewConexaoSMTP', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.ConexaoSMTP.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.ConexaoSMTP.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.ConexaoSMTP.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ConexaoSMTP.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ConexaoSMTP.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ConexaoSMTP.ServidorSMTP'),
                field: 'servidorSMTP'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ConexaoSMTP.RequerConexaoCriptografada'),
                field: 'requerConexaoCriptografada',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ConexaoSMTP.RequerAutenticacao'),
                field: 'requerAutenticacao',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class ConexaoSmtpIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/conexaoSMTP/conexaoSmtpIndex.cshtml';
            this.controller = ConexaoSmtpIndexController;
        }
    }
}