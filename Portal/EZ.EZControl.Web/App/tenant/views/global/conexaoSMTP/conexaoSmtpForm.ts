﻿import * as conexaoSmtpService from "../../../../services/domain/global/conexaoSmtp/conexaoSmtpService";
import * as conexaoSmtpDtos from "../../../../dtos/global/conexaoSmtp/conexaoSmtpDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ConexaoSmtpFormController extends controllers.Controllers.ControllerFormComponentBase<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto, conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoConexaoSmtp: (termoDigitadoPesquisa: string) => any;
        private conexaoSmtpSelecionado: (registro: conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto) => string;
        private conexaoSmtpDeselecionado: () => void;
        private requestParamsConexaoSmtp: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'conexaoSMTPService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public conexaoSmtpService: conexaoSmtpService.Services.ConexaoSmtpService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                conexaoSmtpService,
                'tenant.global.conexaoSMTP');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpInput();
                instance.id = 0;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ConexaoSmtpFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/conexaoSmtp/conexaoSmtpForm.cshtml';
            this.controller = ConexaoSmtpFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}