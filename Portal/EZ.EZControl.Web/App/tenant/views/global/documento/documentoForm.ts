﻿import * as documentoService from "../../../../services/domain/global/documento/documentoService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as documentoDtos from "../../../../dtos/global/documento/documentoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as tipoDeDocumentoDtos from "../../../../dtos/global/tipoDeDocumento/tipoDeDocumentoDtos";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as tipoDeDocumentoService from "../../../../services/domain/global/tipoDeDocumento/tipoDeDocumentoService";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class DocumentoFormController extends controllers.Controllers.ControllerFormComponentBase<documentoDtos.Dtos.Documento.DocumentoListDto, documentoDtos.Dtos.Documento.DocumentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public validaNumeroDocumento: () => any;
        public mascaraDocumento: string;
        // bindings do component
        public operation: string;
        public isDetail: boolean;
        public tipoPessoa: number;
        public pessoaId: number;
        public permissaoAlterarRegistro: string;
        public permissaoExcluirRegistro: string;
        public permissaoCriarRegistro: string;

        // Objetos, variaveis e métodos quando a entidade é um detalhe
        //============================================================
        //
        public saving: boolean;
        public editing: boolean;        
        public editar: (documento: documentoDtos.Dtos.Documento.DocumentoListDto) => void;
        public excluir: (documento: documentoDtos.Dtos.Documento.DocumentoListDto) => void;
        public limparEntidade: () => void;

        // grid
        private setDataGrid: (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<documentoDtos.Dtos.Documento.DocumentoListDto>) => void;
        private gridApi: uiGrid.IGridApiOf<documentoDtos.Dtos.Documento.DocumentoListDto>;
        private requestParamsDocumento: requestParam.RequestParam.RequestParams;
        private documentoAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<documentoDtos.Dtos.Documento.DocumentoListDto>;
        public getDocumentos: () => void;
        public loadingDocumentos: boolean;
        public documentoGridOptions: uiGrid.IGridOptionsOf<documentoDtos.Dtos.Documento.DocumentoListDto>;
        private gridData: documentoDtos.Dtos.Documento.DocumentoListDto[];
        //===============================================================

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        // tipo de documento
        public tipoDeDocumentoFixo: any;
        public ezSelectTipoDeDocumento: ezSelect.EzSelect.EzSelect<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDeDocumento: (termoDigitadoPesquisa: string) => any;
        private tipoDeDocumentoSelecionado: (registro: tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto) => string;
        private tipoDeDocumentoDeselecionado: () => void;
        private requestParamsTipoDeDocumento: requestParam.RequestParam.RequestParams;

        // país
        public paisId: number;
        public ezSelectPais: ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private paisDeselecionado: () => void;
        private requestParamsPais: requestParam.RequestParam.RequestParams;

        // estado
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private estadoDeselecionado: () => void;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'documentoService', 'pessoaService', 'tipoDeDocumentoService', 'paisService', 'estadoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public documentoService: documentoService.Services.DocumentoService,
            public pessoaService: pessoaService.Services.PessoaService,
            public tipoDeDocumentoService: tipoDeDocumentoService.Services.TipoDeDocumentoService,
            public paisService: paisService.Services.PaisService,
            public estadoService: estadoService.Services.EstadoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                documentoService,
                'tenant.global.documento');

            this.validaNumeroDocumento = () => {
                var listaServicosValidacao = [];

                if (this.tipoDeDocumentoFixo === ez.domain.enum.tipoDeDocumentoEnum.cpf) {
                    listaServicosValidacao.push(abp.services.app.global.tipoDeDocumento.isCpf);
                }
                else if (this.tipoDeDocumentoFixo === ez.domain.enum.tipoDeDocumentoEnum.cnpj) {
                    listaServicosValidacao.push(abp.services.app.global.tipoDeDocumento.isCnpj);
                }
                else if (this.tipoDeDocumentoFixo === ez.domain.enum.tipoDeDocumentoEnum.pis) {
                    listaServicosValidacao.push(abp.services.app.global.tipoDeDocumento.isPis);
                }

                return listaServicosValidacao;
            }

            this.carregarPessoa = (id: number) => {
                this.ezSelectPessoa.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    if (!this.isDetail) {
                        this.ezSelectPessoa.setInputText(result.nomePessoa);
                    } else {
                        this.entity.pessoaId = this.pessoa.id;
                    }
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                });
            }

            this.getDocumentos = () => {
                this.loadingDocumentos = true;

                var filtro = new documentoDtos.Dtos.Documento.GetDocumentoInput();
                filtro.pessoaId = this.pessoaId;

                var promise = this.documentoService.getPaged(filtro, this.requestParamsDocumento);

                if (promise) {
                    promise.then(result => {
                        this.setDataGrid(result);
                    }).finally(() => {
                        this.loadingDocumentos = false;
                    });
                }
            }

            this.setDataGrid = (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<documentoDtos.Dtos.Documento.DocumentoListDto>) => {
                this.gridData.length = 0;

                for (var i = 0; i < result.items.length; i++) {
                    this.gridData.push(result.items[i]);
                }

                this.documentoGridOptions.totalItems = result.totalCount;
            };

            this.editar = (documento: documentoDtos.Dtos.Documento.DocumentoListDto) => {
                this.editing = true;
                this.getEntityById(documento.id);
            }

            this.excluir = (documento: documentoDtos.Dtos.Documento.DocumentoListDto) => {
                this.editing = false;
                this.delete(documento);
            }

            this.limparEntidade = () => {
                this.entity = new documentoDtos.Dtos.Documento.DocumentoInput();
                this.ezSelectTipoDeDocumento.gridSelectClear();
                this.ezSelectPais.gridSelectClear();
                this.ezSelectEstado.gridSelectClear();
                this.paisId = null;

                if (this.isDetail)
                    this.entity.pessoaId = this.pessoa.id;
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new documentoDtos.Dtos.Documento.DocumentoInput();
                instance.id = 0;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.acertarDatas();

                // pessoa
                if (entity.pessoaId) {
                    this.carregarPessoa(entity.pessoaId);
                }

                // tipo de documento
                if (entity.tipoDeDocumentoId) {
                    this.ezSelectTipoDeDocumento.loading = true;
                    var promise = this.tipoDeDocumentoService.getById(entity.tipoDeDocumentoId);
                    promise.then(result => {
                        this.ezSelectTipoDeDocumento.setInputText(result.descricao);
                        this.mascaraDocumento = result.mascara;
                        this.tipoDeDocumentoFixo = result.tipoDeDocumentoFixo;
                    }).finally(() => {
                        this.ezSelectTipoDeDocumento.loading = false;
                    });
                }

                // estado
                if (entity.estadoId) {
                    // primeiro carregar o pais
                    // o país não é persistido na tabela de documento, ele é usado apenas para filtrar os estados
                    this.ezSelectPais.loading = true;
                    var promisePais = this.paisService.getPaisByEstadoId(entity.estadoId);
                    promisePais.then(result => {
                        this.ezSelectPais.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectPais.loading = false;
                    });

                    this.ezSelectEstado.loading = true;
                    var promiseEstado = this.estadoService.getById(entity.estadoId);
                    promiseEstado.then(result => {
                        this.ezSelectEstado.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectEstado.loading = false;
                    });
                }
            };
            this.events.onBeforeCancel = () => {
                // se caso for detalhe, o método cancel será abortado, para depois no evento onAfterCancel implementar alguma lógica necessária.
                return this.isDetail;
            };
            this.events.onAfterCancel = () => {
                // no cancel, a entidade da controller receberá uma nova instancia
                this.editing = false;
                this.limparEntidade();
            };
            this.events.onAfterSaveEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getDocumentos();
                    this.limparEntidade();
                    this.editing = false;
                }
            };
            this.events.onDeletedEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getDocumentos();
                }
            };

            this.ezSelectPessoaConfig();
            this.ezSelectTipoDeDocumentoConfig();
            this.ezSelectPaisConfig();
            this.ezSelectEstadoConfig();
            this.documentoGridConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaId) {
                this.carregarPessoa(this.pessoaId);
            }
        }

        $onChanges(changes) {
            if (changes.tipoPessoa) {
                if (changes.tipoPessoa.currentValue !== changes.tipoPessoa.previousValue) {
                    this.mascaraDocumento = '';

                    if (this.pessoa)
                        this.pessoa.tipoPessoa = changes.tipoPessoa.currentValue;

                    if (!angular.isObject(changes.tipoPessoa.previousValue)) {
                        if (this.entity)
                            this.entity.numero = '';
                    }
                }
            }
        };

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaSelecionado = (registro) => {
                this.pessoa = registro;
                this.entity.pessoaId = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.limparEntidade();
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.NomePessoa'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.TipoPessoa'),
                    field: 'tipoPessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.DocumentoPrincipal.Numero'),
                    field: 'documentoPrincipal.numero'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.DocumentoPrincipal.TipoDeDocumento'),
                    field: 'tipoDeDocumentoFixoValue'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDeDocumentoConfig() {
            this.requestParamsTipoDeDocumento = new requestParam.RequestParam.RequestParams();
            this.requestParamsTipoDeDocumento.sorting = "Descricao";

            this.tipoDeDocumentoSelecionado = (registro) => {
                this.entity.tipoDeDocumentoId = registro.id;
                this.tipoDeDocumentoFixo = registro.tipoDeDocumentoFixo;
                this.mascaraDocumento = registro.mascara;
                return registro.descricao;
            }

            this.tipoDeDocumentoDeselecionado = () => {
                this.entity.tipoDeDocumentoId = null;
                this.entity.numero = '';
                this.tipoDeDocumentoFixo = null;
                this.mascaraDocumento = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDeDocumento = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                filtro.tipoPessoa = this.pessoa.tipoPessoa;
                return filtro;
            }

            this.ezSelectTipoDeDocumento = new ezSelect.EzSelect.EzSelect<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeDocumentoService,
                this.getFiltroParaPaginacaoTipoDeDocumento,
                this.requestParamsTipoDeDocumento,
                this.$uibModal,
                this.tipoDeDocumentoSelecionado,
                this.tipoDeDocumentoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeDocumento.onEzGridCreated = () => {
                this.ezSelectTipoDeDocumento.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.tipoDeDocumentoService.getPaged(filtro, requestParams);
                };

                this.ezSelectTipoDeDocumento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeDocumento.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTipoDeDocumento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeDocumento.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTipoDeDocumento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeDocumento.TipoPessoa'),
                    field: 'tipoPessoaValue'
                });

                this.ezSelectTipoDeDocumento.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeDocumento.TipoDeDocumentoFixo'),
                    field: 'tipoDeDocumentoFixoValue'
                });

                this.ezSelectTipoDeDocumento.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDeDocumento.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDeDocumento.ezGrid.getRegistros();
            };
        }

        private ezSelectPaisConfig() {
            this.requestParamsPais = new requestParam.RequestParam.RequestParams();

            this.paisSelecionado = (registro) => {
                this.paisId = registro.id;
                return registro.nome;
            };

            this.paisDeselecionado = () => {
                this.paisId = null;
                this.entity.estadoId = null;

                this.ezSelectEstado.gridSelectClear();
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPais = (termoDigitadoPesquisa) => {
                var filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPais = new ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paisService,
                this.getFiltroParaPaginacaoPais,
                this.requestParamsPais,
                this.$uibModal,
                this.paisSelecionado,
                this.paisDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPais.onEzGridCreated = () => {
                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nome'),
                    field: 'nome'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.CodigoISO'),
                    field: 'codigoISO'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nacionalidade'),
                    field: 'nacionalidade'
                });

                this.ezSelectPais.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPais.on.beforeOpenModalDialog = () => {
                this.ezSelectPais.ezGrid.getRegistros();
            };
        }

        private ezSelectEstadoConfig() {
            this.requestParamsEstado = new requestParam.RequestParam.RequestParams();

            this.estadoSelecionado = (registro) => {
                this.entity.estadoId = registro.id;
                return registro.nome;
            }

            this.estadoDeselecionado = () => {
                this.entity.estadoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstado = (termoDigitadoPesquisa) => {
                var filtro = null;
                if (!this.paisId) {
                    abp.message.info(app.localize('Pais.SelecaoDoPaisNecessaria'), '');
                } else {
                    filtro = new estadoDtos.Dtos.Estado.GetEstadoByPaisInput();
                    filtro.paisId = this.paisId;
                    filtro.nomeEstado = termoDigitadoPesquisa;
                    filtro.siglaEstado = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectEstado = new ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoEstado,
                this.requestParamsEstado,
                this.$uibModal,
                this.estadoSelecionado,
                this.estadoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstado.onEzGridCreated = () => {
                this.ezSelectEstado.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.estadoService.getEstadosByPaisId(filtro, requestParams);
                };

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Sigla'),
                    field: 'sigla'
                });

                this.ezSelectEstado.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstado.on.beforeOpenModalDialog = () => {
                if (this.paisId) {
                    this.ezSelectEstado.ezGrid.getRegistros();
                }
            };
        }

        private acertarDatas() {
            if (this.entity.dataDeValidade)
                this.entity.dataDeValidade = new Date(this.entity.dataDeValidade.toString());
            if (this.entity.dataExpedicao)
                this.entity.dataExpedicao = new Date(this.entity.dataExpedicao.toString());
        }

        // metodos usados quando a entidade é um detalhe
        private documentoGridConfig() {
            if (this.isDetail) {
                this.requestParamsDocumento = new requestParam.RequestParam.RequestParams();
                this.requestParamsDocumento.sorting = 'Numero';

                this.documentoAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<documentoDtos.Dtos.Documento.DocumentoListDto>();

                this.documentoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoAlterarRegistro);
                    }
                ));

                this.documentoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoExcluirRegistro);
                    }
                ));

                this.gridData = new Array<documentoDtos.Dtos.Documento.DocumentoListDto>();

                this.documentoGridOptions = {
                    enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    showColumnFooter: true,
                    rowHeight: 50,
                    enableGridMenu: true,
                    exporterMenuCsv: false,
                    exporterMenuPdf: false,
                    rowTemplate: 'App/common/views/common/grid/linha.html',
                    columnDefs: [],
                    appScopeProvider: this.documentoAppScopeProvider,
                    onRegisterApi: (api) => {
                        this.gridApi = api;
                        this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                this.requestParamsDocumento.sorting = null;
                            } else {
                                this.requestParamsDocumento.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            this.getDocumentos();
                        });
                        this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                            this.requestParamsDocumento.skipCount = (pageNumber - 1) * pageSize;
                            this.requestParamsDocumento.maxResultCount = pageSize;

                            this.getDocumentos();
                        });
                    }
                }

                this.documentoGridOptions.data = this.gridData;

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.TipoDeDocumento'),
                    field: 'tipoDeDocumento.descricao',
                    cellTemplate: '\
                        <div class="ui-grid-cell-contents">\
                            {{grid.getCellValue(row, col)}}<br />\
                            <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                <span>\
                                    <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                </span> \
                            </span>\
                        </div>'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.TipoDeDocumento'),
                    field: 'tipoDeDocumento.descricao'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.Numero'),
                    field: 'numero'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.OrgaoExpedidor'),
                    field: 'orgaoExpedidor'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.UF'),
                    field: 'estado.sigla'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.DataExpedicao'),
                    cellFilter: 'date:\'dd/MM/yyyy\'',
                    field: 'dataExpedicao'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.DataDeValidade'),
                    cellFilter: 'date:\'dd/MM/yyyy\'',
                    field: 'dataDeValidade'
                });

                this.documentoGridOptions.columnDefs.push({
                    name: app.localize('Documento.Serie'),
                    field: 'serie'
                });

                this.getDocumentos();
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DocumentoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/documento/documentoForm.cshtml';
            this.controller = DocumentoFormController;
            this.bindings = {
                operation: '@',
                isDetail: '<',
                tipoPessoa: '<',
                pessoaId: '<',
                permissaoAlterarRegistro: '@',
                permissaoExcluirRegistro: '@',
                permissaoCriarRegistro: '@'
            }
        }
    }
}