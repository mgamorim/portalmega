﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as documentoService from "../../../../services/domain/global/documento/documentoService";
import * as documentoDtos from "../../../../dtos/global/documento/documentoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Documento {

    class DocumentoIndexController extends controllers.Controllers.ControllerIndexBase<documentoDtos.Dtos.Documento.DocumentoListDto, documentoDtos.Dtos.Documento.DocumentoInput>{

        static $inject = ['$state', 'uiGridConstants', 'documentoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<documentoDtos.Dtos.Documento.DocumentoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public documentoService: documentoService.Services.DocumentoService) {

            super(
                $state,
                uiGridConstants,
                documentoService, {
                    rotaAlterarRegistro: 'tenant.global.documento.alterar',
                    rotaNovoRegistro: 'tenant.global.documento.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            this.requestParams.sorting = "Numero";

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new documentoDtos.Dtos.Documento.GetDocumentoInput();
                filtro.numero = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<documentoDtos.Dtos.Documento.DocumentoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.documentoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDocumento', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Documento.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Documento.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Documento.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pessoa'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.TipoDeDocumento'),
                field: 'tipoDeDocumento.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.Numero'),
                field: 'numero'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.OrgaoExpedidor'),
                field: 'orgaoExpedidor'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.UF'),
                field: 'estado.sigla'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.DataExpedicao'),
                cellFilter: 'date:\'dd/MM/yyyy\'',
                field: 'dataExpedicao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.DataDeValidade'),
                cellFilter: 'date:\'dd/MM/yyyy\'',
                field: 'dataDeValidade'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Documento.Serie'),
                field: 'serie'
            });
        }
    }

    export class DocumentoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/documento/documentoIndex.cshtml';
            this.controller = DocumentoIndexController;
        }
    }
}