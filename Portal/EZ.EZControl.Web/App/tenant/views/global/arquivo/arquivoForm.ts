﻿import * as arquivoService from "../../../../services/domain/global/arquivo/arquivoGlobalService";
import * as arquivoDtos from "../../../../dtos/global/arquivo/arquivoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as parametroGlobalService from "../../../../services/domain/global/parametro/parametroGlobalService";

export module Forms {
    export class ArquivoFormController extends controllers.Controllers.ControllerFormComponentBase<arquivoDtos.Dtos.Arquivo.Global.ArquivoGlobalListDto, arquivoDtos.Dtos.Arquivo.Global.ArquivoGlobalInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoArquivo: (termoDigitadoPesquisa: string) => any;
        private arquivoSelecionado: (registro: arquivoDtos.Dtos.Arquivo.Global.ArquivoGlobalListDto) => string;
        private arquivoDeselecionado: () => void;
        private requestParamsArquivo: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'arquivoGlobalService', 'parametroGlobalService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public arquivoService: arquivoService.Services.ArquivoGlobalService,
            public parametroGlobalService: parametroGlobalService.Services.ParametroGlobalService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                arquivoService,
                'tenant.global.arquivo');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new arquivoDtos.Dtos.Arquivo.Global.ArquivoGlobalInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class ArquivoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/arquivo/arquivoForm.cshtml';
            this.controller = ArquivoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}