﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as fornecedorService from "../../../../services/domain/global/fornecedor/fornecedorService";
import * as fornecedorDtos from "../../../../dtos/global/fornecedor/fornecedorDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Fornecedor {

    class FornecedorIndexController extends controllers.Controllers.ControllerIndexBase<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, fornecedorDtos.Dtos.Fornecedor.FornecedorInput>{

        static $inject = ['$state', 'uiGridConstants', 'fornecedorService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public fornecedorService: fornecedorService.Services.FornecedorService) {

            super(
                $state,
                uiGridConstants,
                fornecedorService, {
                    rotaAlterarRegistro: 'tenant.global.fornecedores.alterar',
                    rotaNovoRegistro: 'tenant.global.fornecedores.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new fornecedorDtos.Dtos.Fornecedor.GetFornecedorInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.fornecedorService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewFornecedor', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Fornecedor.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Fornecedor.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Fornecedor.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Fornecedor.NomePessoa'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Fornecedor.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Fornecedor.TipoPessoa'),
                field: 'pessoa.tipoPessoaValue'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Fornecedor.GrupoPessoa'),
                field: 'pessoa.grupoPessoa.descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Fornecedor.DocumentoPrincipal.Numero'),
                field: 'pessoa.documentoPrincipal.numero'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Fornecedor.DocumentoPrincipal.TipoDeDocumento'),
                field: 'tipoDeDocumentoFixoValue'
            });
        }
    }

    export class FornecedorIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/fornecedores/fornecedorIndex.cshtml';
            this.controller = FornecedorIndexController;
        }
    }
}