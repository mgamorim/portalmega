﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as usuarioPorPessoaDtos from "../../../../dtos/global/usuarioPorPessoa/usuarioPorPessoaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";
import * as usuarioPorPessoaService from "../../../../services/domain/global/usuarioPorPessoa/usuarioPorPessoaService";

export module Global.UsuarioPorPessoa {

    class UsuarioPorPessoaIndexController extends controllers.Controllers.ControllerIndexBase<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaListDto, usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaInput>{

        static $inject = ['$state', 'uiGridConstants', 'usuarioPorPessoaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public usuarioPorPessoaService: usuarioPorPessoaService.Services.UsuarioPorPessoaService) {

            super(
                $state,
                uiGridConstants,
                usuarioPorPessoaService, {
                    rotaAlterarRegistro: 'tenant.usuarioPorPessoa.alterar',
                    rotaNovoRegistro: 'tenant.usuarioPorPessoa.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.GetUsuarioInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.usuarioPorPessoaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewUsuarioPorPessoa', this.novo, abp.auth.hasPermission('Pages.Administration.Tenant.UsuarioPorPessoa.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Administration.Tenant.UsuarioPorPessoa.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Administration.Tenant.UsuarioPorPessoa.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Nome'),
                field: 'user.name',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Sobrenome'),
                field: 'user.surname'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('UsuarioPorPessoa.Login'),
                field: 'user.userName'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('UsuarioPorPessoa.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Email'),
                field: 'user.emailAddress'
            });

        }
    }

    export class UsuarioPorPessoaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/usuarioPorPessoa/usuarioPorPessoaIndex.cshtml';
            this.controller = UsuarioPorPessoaIndexController;
        }
    }
}