﻿import * as usuarioPorPessoaService from "../../../../services/domain/global/usuarioPorPessoa/usuarioPorPessoaService";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as usuarioPorPessoaDtos from "../../../../dtos/global/usuarioPorPessoa/usuarioPorPessoaDtos";

export module Forms {
    export class UsuarioPorPessoaFormController extends controllers.Controllers.ControllerFormComponentBase<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaInput, usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaInput, usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaPessoaIdDto>{
        public usuarioPorPessoaForm: ng.IFormController;

        public getFiltroParaPaginacaoMedico: (termoDigitadoPesquisa: string) => any;
        private ususarioSelecionado: (registro: usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaListDto) => string;
        private usuarioDeselecionado: () => void;
        private requestParamsUsuario: requestParam.RequestParam.RequestParams;

        // pessoa
        public pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        public ezSelectPessoaFisica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaFisica: (termoDigitadoPesquisa: string) => any;
        private pessoaFisicaSelecionado: (registro: usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPessoaFisicaListDto) => string;
        private pessoaFisicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaFisica: (id: number) => void;
        public tipoDeUsuarioEnum = ez.domain.enum.tipoDeUsuarioEnum.valores;

        public tipoDeUsuario : TipoDeUsuarioEnum;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'usuarioPorPessoaService', 'pessoaFisicaService',  '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public usuarioPorPessoaService: usuarioPorPessoaService.Services.UsuarioPorPessoaService,
            public pessoaFisicaService: pessoaFisicaService.Services.PessoaFisicaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                usuarioPorPessoaService,
                'tenant.usuarioPorPessoa');

            this.carregarPessoaFisica = (id: number) => {
                this.ezSelectPessoaFisica.loading = true;
                var promise = this.pessoaFisicaService.getById(id);
                promise.then(result => {
                    this.pessoaFisica = result;
                    this.entity.user.pessoa.id = this.pessoaFisica.id;
                    this.ezSelectPessoaFisica.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectPessoaFisica.loading = false;
                });

            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaInput();
                instance.id = 0;
                instance.user = new usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UserEditDto();
                instance.user.isActive = true;
                instance.user.pessoa = new pessoaDtos.Dtos.Pessoa.PessoaInput;
                instance.user.pessoa.tipoPessoa = 1;
                instance.user.pessoa.grupoPessoaId = null;
                instance.user.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
                instance.user.pessoaFisica.tipoPessoa = 1;
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                if (entity.user.pessoa) {
                    this.carregarPessoaFisica(entity.user.pessoa.id);
                }

                this.tipoDeUsuario = entity.user.tipoDeUsuario;

            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.usuarioPorPessoaId;
                this.entity.user.pessoa.id = result.pessoaId;
            };

            this.events.onBeforeSaveEntity = () => {
                this.entity.user.tipoDeUsuario = this.tipoDeUsuario;
            }


            this.ezSelectPessoaConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor

            
            this.prepareEntityInstance();

            if (this.pessoaFisica) {
                this.carregarPessoaFisica(this.pessoaFisica.id);
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();

            this.pessoaFisicaSelecionado = (registro) => {
                this.pessoaFisica = registro;
                this.entity.user.pessoaFisica.id = registro.id;
                this.entity.user.pessoaFisica.tipoPessoa = 1;
                this.entity.user.name = registro.nome.slice(0, registro.nome.indexOf(' '));
                this.entity.user.surname = registro.nome.slice(registro.nome.lastIndexOf(' ') + 1);
                this.entity.user.emailAddress = registro.emailPessoal;
                this.entity.user.userName = registro.emailPessoal;

                if (!registro.emailPessoal) {
                    abp.message.error(app.localize("UsuarioPorPessoa.NotFoundEmailPessoaError"), "");
                }

                if (!registro.cpf) {
                    abp.message.error(app.localize("UsuarioPorPessoa.NotFoundCpfPessoaError"), "");
                }

                return registro.nomePessoa;
            }

            this.pessoaFisicaDeselecionado = () => {
                this.pessoaFisica = null;
                this.entity.user.name = null;
                this.entity.user.surname = null;
                this.entity.user.emailAddress = null;
                this.entity.user.userName = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaFisica = (termoDigitadoPesquisa) => {
                var filtro = new usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.GetUsuarioInput;
                filtro.nome = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaFisica = new ezSelect.EzSelect.EzSelect<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPessoaFisicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaFisicaService,
                this.getFiltroParaPaginacaoPessoaFisica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaFisicaSelecionado,
                this.pessoaFisicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaFisica.onEzGridCreated = () => {

                this.ezSelectPessoaFisica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.usuarioPorPessoaService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoaFisica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.Nome'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaFisica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaFisica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaFisica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaFisica.ezGrid.getRegistros();
            };
        }

        tipoDeUsuarioSelected = () => {
            this.entity.roleName = this.tipoDeUsuario
                ? ez.domain.enum.tipoDeUsuarioEnum.getItemPorValor(this.tipoDeUsuario).nome
                    : null;
        };

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.usuarioPorPessoaForm = form;
        }

        
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class UsuarioPorPessoaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/usuarioPorPessoa/usuarioPorPessoaForm.cshtml';
            this.controller = UsuarioPorPessoaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}