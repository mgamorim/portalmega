﻿import * as enderecoService from "../../../../services/domain/global/endereco/enderecoService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as enderecoDtos from "../../../../dtos/global/endereco/enderecoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as tipoDeLogradouroService from "../../../../services/domain/global/tipoDeLogradouro/tipoDeLogradouroService";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class EnderecoFormController extends controllers.Controllers.ControllerFormComponentBase<enderecoDtos.Dtos.Endereco.EnderecoListDto, enderecoDtos.Dtos.Endereco.EnderecoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public paisId: number;
        public estadoId: number;
        public tipoEnderecos: any;

        // bindings do component
        public operation: string;
        public isDetail: boolean;
        public tipoPessoa: number;
        public pessoaId: number;
        public permissaoAlterarRegistro: string;
        public permissaoExcluirRegistro: string;
        public permissaoCriarRegistro: string;

        // Objetos, variaveis e métodos quando a entidade é um detalhe
        //============================================================
        //
        public saving: boolean;
        public editing: boolean;
        public editar: (endereco: enderecoDtos.Dtos.Endereco.EnderecoListDto) => void;
        public excluir: (endereco: enderecoDtos.Dtos.Endereco.EnderecoListDto) => void;
        public limparEntidade: () => void;

        // grid
        private setDataGrid: (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<enderecoDtos.Dtos.Endereco.EnderecoListDto>) => void;
        private gridApi: uiGrid.IGridApiOf<enderecoDtos.Dtos.Endereco.EnderecoListDto>;
        private requestParamsEndereco: requestParam.RequestParam.RequestParams;
        private enderecoAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<enderecoDtos.Dtos.Endereco.EnderecoListDto>;
        public getEnderecos: () => void;
        public loadingEnderecos: boolean;
        public enderecoGridOptions: uiGrid.IGridOptionsOf<enderecoDtos.Dtos.Endereco.EnderecoListDto>;
        private gridData: enderecoDtos.Dtos.Endereco.EnderecoListDto[];
        //===============================================================

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        // tipoDeLogradouro
        public ezSelectTipoDeLogradouro: ezSelect.EzSelect.EzSelect<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDeLogradouro: (termoDigitadoPesquisa: string) => any;
        private tipoDeLogradouroSelecionado: (registro: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto) => string;
        private tipoDeLogradouroDeselecionado: () => void;
        private requestParamsTipoDeLogradouro: requestParam.RequestParam.RequestParams;

        // país
        public ezSelectPais: ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private paisDeselecionado: () => void;
        private requestParamsPais: requestParam.RequestParam.RequestParams;

        // estado
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private estadoDeselecionado: () => void;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;

        // cidade
        public ezSelectCidade: ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCidade: (termoDigitadoPesquisa: string) => any;
        private cidadeSelecionado: (registro: cidadeDtos.Dtos.Cidade.CidadeListDto) => string;
        private cidadeDeselecionado: () => void;
        private requestParamsCidade: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'enderecoService', 'pessoaService', 'tipoDeLogradouroService', 'paisService', 'estadoService', 'cidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public enderecoService: enderecoService.Services.EnderecoService,
            public pessoaService: pessoaService.Services.PessoaService,
            public tipoDeLogradouroService: tipoDeLogradouroService.Services.TipoDeLogradouroService,
            public paisService: paisService.Services.PaisService,
            public estadoService: estadoService.Services.EstadoService,
            public cidadeService: cidadeService.Services.CidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                enderecoService,
                'tenant.global.endereco');

            this.carregarPessoa = (id: number) => {
                this.ezSelectPessoa.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    if (!this.isDetail) {
                        this.ezSelectPessoa.setInputText(result.nomePessoa);
                    } else {
                        this.entity.pessoaId = this.pessoa.id;
                    }
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                });
            }

            this.getEnderecos = () => {
                this.loadingEnderecos = true;

                var filtro = new enderecoDtos.Dtos.Endereco.GetEnderecoInput();
                filtro.pessoaId = this.pessoaId;

                var promise = this.enderecoService.getPaged(filtro, this.requestParamsEndereco);

                if (promise) {
                    promise.then(result => {
                        this.setDataGrid(result);
                    }).finally(() => {
                        this.loadingEnderecos = false;
                    });
                }
            }

            this.setDataGrid = (result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<enderecoDtos.Dtos.Endereco.EnderecoListDto>) => {
                this.gridData.length = 0;

                for (var i = 0; i < result.items.length; i++) {
                    this.gridData.push(result.items[i]);
                }

                this.enderecoGridOptions.totalItems = result.totalCount;
            };

            this.editar = (endereco: enderecoDtos.Dtos.Endereco.EnderecoListDto) => {
                this.editing = true;
                this.getEntityById(endereco.id);
            }

            this.excluir = (endereco: enderecoDtos.Dtos.Endereco.EnderecoListDto) => {
                this.editing = false;
                this.delete(endereco);
            }

            this.limparEntidade = () => {
                this.entity = new enderecoDtos.Dtos.Endereco.EnderecoInput();
                this.ezSelectTipoDeLogradouro.gridSelectClear();
                this.ezSelectPais.gridSelectClear();
                this.ezSelectEstado.gridSelectClear();
                this.ezSelectCidade.gridSelectClear();

                if (this.isDetail)
                    this.entity.pessoaId = this.pessoa.id;
            }
        }

        $onInit() {
            super.init();

            this.tipoEnderecos = ez.domain.enum.tipoEnderecoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new enderecoDtos.Dtos.Endereco.EnderecoInput();
                instance.id = 0;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaId) {
                    this.carregarPessoa(entity.pessoaId);
                }

                this.carregarEzSelects(entity);
            };
            this.events.onBeforeCancel = () => {
                // se caso for detalhe, o método cancel será abortado, para depois no evento onAfterCancel implementar alguma lógica necessária.
                return this.isDetail;
            };
            this.events.onAfterCancel = () => {
                // no cancel, a entidade da controller receberá uma nova instancia
                this.editing = false;
                this.limparEntidade();
            };
            this.events.onAfterSaveEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getEnderecos();
                    this.limparEntidade();
                    this.editing = false;
                }
            };
            this.events.onDeletedEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getEnderecos();
                }
            };

            this.ezSelectPessoaConfig();
            this.ezSelectTipoDeLogradouroConfig();
            this.ezSelectPaisConfig();
            this.ezSelectEstadoConfig();
            this.ezSelectCidadeConfig();
            this.enderecoGridConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaId) {
                this.carregarPessoa(this.pessoaId);
            }
        }

        $onChanges(changes) {
            if (changes.tipoPessoa) {
                if (changes.tipoPessoa.currentValue !== changes.tipoPessoa.previousValue) {

                    if (this.pessoa)
                        this.pessoa.tipoPessoa = changes.tipoPessoa.currentValue;
                }
            }
        }

        getEndereco = () => {
            if (this.entity.cep && this.entity.cep.length === 8) {
                var filtro = new enderecoDtos.Dtos.Endereco.GetEnderecoByCepInput();
                filtro.cep = this.entity.cep;
                var pessoaId = this.entity.pessoaId;
                var promise = this.enderecoService.getEnderecoByCep(filtro);

                if (promise) {
                    promise.then(result => {
                        this.entity = result;
                        this.entity.pessoaId = pessoaId;
                        this.carregarEzSelects(this.entity);
                    }).finally(() => {
                        this.loadingEnderecos = false;
                    });
                }
            } else {
                this.clearComponents();
            } 
        };

        private carregarEzSelects(entity) {
            // tipo de logradouro
            if (entity.tipoDeLogradouroId) {
                this.ezSelectTipoDeLogradouro.loading = true;
                // nesse caso usei let ao invés de var, para a variavel promise existir apenas no escopo desse if
                let promise = this.tipoDeLogradouroService.getById(entity.tipoDeLogradouroId);
                promise.then(result => {
                    this.ezSelectTipoDeLogradouro.setInputText(result.descricao);
                }).finally(() => {
                    this.ezSelectTipoDeLogradouro.loading = false;
                });
            }

            // cidade
            if (entity.cidadeId) {
                // primeiro carregar o pais
                // o país não é persistido na tabela de documento, ele é usado apenas para filtrar os estados e cidades
                this.ezSelectPais.loading = true;
                let promisePais = this.paisService.getPaisByCidadeId(entity.cidadeId);
                promisePais.then(result => {
                    this.paisId = result.id;
                    this.ezSelectPais.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectPais.loading = false;
                });

                // carregar o estado
                this.ezSelectEstado.loading = true;
                let promiseEstado = this.estadoService.getEstadoByCidadeId(entity.cidadeId);
                promiseEstado.then(result => {
                    this.estadoId = result.id;
                    this.ezSelectEstado.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectEstado.loading = false;
                });

                // carregar a cidade
                this.ezSelectCidade.loading = true;
                let promiseCidade = this.cidadeService.getById(entity.cidadeId);
                promiseCidade.then(result => {
                    this.ezSelectCidade.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectCidade.loading = false;
                });
            }
        }

        private clearComponents() {
            this.entity.logradouro = '';
            this.entity.bairro = '';
            this.entity.complemento = '';
            this.ezSelectTipoDeLogradouro.gridSelectClear();
            this.ezSelectPais.gridSelectClear();
            this.ezSelectEstado.gridSelectClear();
            this.ezSelectCidade.gridSelectClear();
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaSelecionado = (registro) => {
                this.pessoa = registro;
                this.entity.pessoaId = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.entity.pessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.NomePessoa'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.TipoPessoa'),
                    field: 'tipoPessoa'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.EnderecoPrincipal.Numero'),
                    field: 'enderecoPrincipal.numero'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.EnderecoPrincipal.TipoDeEndereco'),
                    field: 'tipoDeEnderecoFixoValue'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDeLogradouroConfig() {
            this.requestParamsTipoDeLogradouro = new requestParam.RequestParam.RequestParams();
            this.requestParamsTipoDeLogradouro.sorting = "Descricao";

            this.tipoDeLogradouroSelecionado = (registro) => {
                this.entity.tipoDeLogradouroId = registro.id;
                return registro.descricao;
            }

            this.tipoDeLogradouroDeselecionado = () => {
                this.entity.tipoDeLogradouroId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDeLogradouro = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.GetTipoDeLogradouroInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDeLogradouro = new ezSelect.EzSelect.EzSelect<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeLogradouroService,
                this.getFiltroParaPaginacaoTipoDeLogradouro,
                this.requestParamsTipoDeLogradouro,
                this.$uibModal,
                this.tipoDeLogradouroSelecionado,
                this.tipoDeLogradouroDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeLogradouro.onEzGridCreated = () => {
                this.ezSelectTipoDeLogradouro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeLogradouro.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTipoDeLogradouro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeLogradouro.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTipoDeLogradouro.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDeLogradouro.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDeLogradouro.ezGrid.getRegistros();
            };
        }

        private ezSelectPaisConfig() {
            this.requestParamsPais = new requestParam.RequestParam.RequestParams();

            this.paisSelecionado = (registro) => {
                this.paisId = registro.id;
                return registro.nome;
            };

            this.paisDeselecionado = () => {
                this.paisId = null;
                this.estadoId = null;
                this.entity.cidadeId = null;

                this.ezSelectEstado.gridSelectClear();
                this.ezSelectCidade.gridSelectClear();
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPais = (termoDigitadoPesquisa) => {
                var filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPais = new ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paisService,
                this.getFiltroParaPaginacaoPais,
                this.requestParamsPais,
                this.$uibModal,
                this.paisSelecionado,
                this.paisDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPais.onEzGridCreated = () => {
                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nome'),
                    field: 'nome'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.CodigoISO'),
                    field: 'codigoISO'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nacionalidade'),
                    field: 'nacionalidade'
                });

                this.ezSelectPais.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPais.on.beforeOpenModalDialog = () => {
                this.ezSelectPais.ezGrid.getRegistros();
            };
        }

        private ezSelectEstadoConfig() {
            this.requestParamsEstado = new requestParam.RequestParam.RequestParams();

            this.estadoSelecionado = (registro) => {
                this.estadoId = registro.id;
                return registro.nome;
            }

            this.estadoDeselecionado = () => {
                this.estadoId = null;
                this.entity.cidadeId = null;
                this.ezSelectCidade.gridSelectClear();
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstado = (termoDigitadoPesquisa) => {
                var filtro = null;
                if (!this.paisId) {
                    abp.message.info(app.localize('Pais.SelecaoDoPaisNecessaria'), '');
                } else {
                    filtro = new estadoDtos.Dtos.Estado.GetEstadoByPaisInput();
                    filtro.paisId = this.paisId;
                    filtro.nomeEstado = termoDigitadoPesquisa;
                    filtro.siglaEstado = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectEstado = new ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoEstado,
                this.requestParamsEstado,
                this.$uibModal,
                this.estadoSelecionado,
                this.estadoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstado.onEzGridCreated = () => {
                this.ezSelectEstado.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.estadoService.getEstadosByPaisId(filtro, requestParams);
                };

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Sigla'),
                    field: 'sigla'
                });

                this.ezSelectEstado.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstado.on.beforeOpenModalDialog = () => {
                if (this.paisId) {
                    this.ezSelectEstado.ezGrid.getRegistros();
                }
            };
        }

        private ezSelectCidadeConfig() {
            this.requestParamsCidade = new requestParam.RequestParam.RequestParams();

            this.cidadeSelecionado = (registro) => {
                this.entity.cidadeId = registro.id;
                return registro.nome;
            }

            this.cidadeDeselecionado = () => {
                this.entity.cidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCidade = (termoDigitadoPesquisa) => {
                var filtro = null;
                if (!this.estadoId) {
                    abp.message.info(app.localize('Cidade.EmptyEstadoError'), '');
                } else {
                    filtro = new cidadeDtos.Dtos.Cidade.GetCidadeByEstadoInput();
                    filtro.estadoId = this.estadoId;
                    filtro.nomeCidade = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectCidade = new ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.cidadeService,
                this.getFiltroParaPaginacaoCidade,
                this.requestParamsCidade,
                this.$uibModal,
                this.cidadeSelecionado,
                this.cidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCidade.onEzGridCreated = () => {
                this.ezSelectCidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.cidadeService.getCidadesByEstadoId(filtro, requestParams);
                };

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Estado'),
                    field: 'estado.nome'
                });

                this.ezSelectCidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCidade.on.beforeOpenModalDialog = () => {
                if (this.estadoId) {
                    this.ezSelectCidade.ezGrid.getRegistros();
                }
            };
        }

        // metodos usados quando a entidade é um detalhe
        private enderecoGridConfig() {
            if (this.isDetail) {
                this.requestParamsEndereco = new requestParam.RequestParam.RequestParams();
                this.requestParamsEndereco.sorting = 'Descricao';

                this.enderecoAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<enderecoDtos.Dtos.Endereco.EnderecoListDto>();

                this.enderecoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoAlterarRegistro);
                    }
                ));

                this.enderecoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                    (row) => {
                        return abp.auth.hasPermission(this.permissaoExcluirRegistro);
                    }
                ));

                this.gridData = new Array<enderecoDtos.Dtos.Endereco.EnderecoListDto>();

                this.enderecoGridOptions = {
                    enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    showColumnFooter: true,
                    rowHeight: 50,
                    enableGridMenu: true,
                    exporterMenuCsv: false,
                    exporterMenuPdf: false,
                    rowTemplate: 'App/common/views/common/grid/linha.html',
                    columnDefs: [],
                    appScopeProvider: this.enderecoAppScopeProvider,
                    onRegisterApi: (api) => {
                        this.gridApi = api;
                        this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                this.requestParamsEndereco.sorting = null;
                            } else {
                                this.requestParamsEndereco.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            this.getEnderecos();
                        });
                        this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                            this.requestParamsEndereco.skipCount = (pageNumber - 1) * pageSize;
                            this.requestParamsEndereco.maxResultCount = pageSize;

                            this.getEnderecos();
                        });
                    }
                }

                this.enderecoGridOptions.data = this.gridData;

                this.enderecoGridOptions.columnDefs.push({
                    name: app.localize('Endereco.TipoEndereco'),
                    field: 'tipoEnderecoValue',
                    cellTemplate: '\
                        <div class="ui-grid-cell-contents">\
                            {{grid.getCellValue(row, col)}}<br />\
                            <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                <span>\
                                    <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                </span> \
                            </span>\
                        </div>'
                });

                this.enderecoGridOptions.columnDefs.push({
                    name: app.localize('Endereco.EnderecoCompleto'),
                    field: 'enderecoCompleto'
                });

                this.getEnderecos();
            }
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EnderecoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/endereco/enderecoForm.cshtml';
            this.controller = EnderecoFormController;
            this.bindings = {
                operation: '@',
                isDetail: '<',
                tipoPessoa: '<',
                pessoaId: '<',
                permissaoAlterarRegistro: '@',
                permissaoExcluirRegistro: '@',
                permissaoCriarRegistro: '@'
            }
        }
    }
}