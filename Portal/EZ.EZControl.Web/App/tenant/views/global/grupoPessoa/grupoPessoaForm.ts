﻿import * as grupoPessoaService from "../../../../services/domain/global/grupoPessoa/grupoPessoaService";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class GrupoPessoaFormController extends controllers.Controllers.ControllerFormComponentBase<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectGrupoPessoa: ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoGrupoPessoa: (termoDigitadoPesquisa: string) => any;
        private grupoPessoaSelecionado: (registro: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto) => string;
        private grupoPessoaDeselecionado: () => void;
        private requestParamsGrupoPessoa: requestParam.RequestParam.RequestParams;
        
        public tipoPessoaEnum: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'grupoPessoaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public grupoPessoaService: grupoPessoaService.Services.GrupoPessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                grupoPessoaService,
                'tenant.global.grupoPessoa');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.tipoDePessoa = TipoPessoaEnum.Fisica;
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (entity.paiId) {
                    this.ezSelectGrupoPessoa.loading = true;
                    var promise = this.grupoPessoaService.getById(entity.paiId);
                    promise.then(result => {
                        this.ezSelectGrupoPessoa.setInputText(result.nome);
                    })
                    .finally(() => {
                        this.ezSelectGrupoPessoa.loading = false;
                    });
                }
                if (!entity.slug) {
                    this.slugifyNome();
                }
            };

            this.tipoPessoaEnum = ez.domain.enum.tipoPessoaEnum.valores;
            this.ezSelectGrupoPessoaConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private slugifyNome() {
            this.entity.slug = this.entity.nome.slugify();
            while (this.entity.slug.indexOf('-') > -1) {
                this.entity.slug = this.entity.slug.slugify().replace('-', '');
            }
        }

        private padronizaSlug() {
            this.entity.slug = this.entity.slug.slugify();
            while (this.entity.slug.indexOf('-') > -1) {
                this.entity.slug = this.entity.slug.slugify().replace('-', '');
            }
        }

        private ezSelectGrupoPessoaConfig() {
            this.requestParamsGrupoPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsGrupoPessoa.sorting = "Nome";

            this.grupoPessoaSelecionado = (registro) => {
                this.entity.paiId = registro.id;
                return registro.nome;
            }

            this.grupoPessoaDeselecionado = () => {
                this.entity.paiId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoGrupoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput();
                filtro.grupoPessoaId = this.entity.id;
                filtro.tipoDePessoa = this.entity.tipoDePessoa;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectGrupoPessoa = new ezSelect.EzSelect.EzSelect<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.grupoPessoaService,
                this.getFiltroParaPaginacaoGrupoPessoa,
                this.requestParamsGrupoPessoa,
                this.$uibModal,
                this.grupoPessoaSelecionado,
                this.grupoPessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectGrupoPessoa.onEzGridCreated = () => {
                this.ezSelectGrupoPessoa.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.grupoPessoaService.getGruposPessoaExceptForId(filtro, requestParams);
                };

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('EzEntityRecursive.Nome'),
                    field: 'nome'
                });

                this.ezSelectGrupoPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('GrupoPessoa.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectGrupoPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectGrupoPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectGrupoPessoa.ezGrid.getRegistros();
            };
        }

        tipoDePessoaSelected = () => {
            this.ezSelectGrupoPessoa.gridSelectClear();
        };
    }



    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class GrupoPessoaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/grupoPessoa/grupoPessoaForm.cshtml';
            this.controller = GrupoPessoaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}