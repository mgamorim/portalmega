﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as grupoPessoaService from "../../../../services/domain/global/grupoPessoa/grupoPessoaService";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.GrupoPessoa {

    class GrupoPessoaIndexController extends controllers.Controllers.ControllerIndexBase<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaInput>{

        static $inject = ['$state', 'uiGridConstants', 'grupoPessoaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public grupoPessoaService: grupoPessoaService.Services.GrupoPessoaService) {

            super(
                $state,
                uiGridConstants,
                grupoPessoaService, {
                    rotaAlterarRegistro: 'tenant.global.grupoPessoa.alterar',
                    rotaNovoRegistro: 'tenant.global.grupoPessoa.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.grupoPessoaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewGrupoPessoa', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.GrupoPessoa.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.GrupoPessoa.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.GrupoPessoa.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EzEntityRecursive.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('GrupoPessoa.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pessoa.TipoPessoa'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoPessoaEnum", "tipoDePessoa")}}\
                </div>'
            });

        }
    }

    export class GrupoPessoaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/grupoPessoa/grupoPessoaIndex.cshtml';
            this.controller = GrupoPessoaIndexController;
        }
    }
}