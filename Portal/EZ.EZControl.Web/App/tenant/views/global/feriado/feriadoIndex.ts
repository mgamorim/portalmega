﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as feriadoService from "../../../../services/domain/global/feriado/feriadoService";
import * as feriadoDtos from "../../../../dtos/global/feriado/feriadoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Feriado {

    class FeriadoIndexController extends controllers.Controllers.ControllerIndexBase<feriadoDtos.Dtos.Feriado.FeriadoListDto, feriadoDtos.Dtos.Feriado.FeriadoInput>{

        static $inject = ['$state', 'uiGridConstants', 'feriadoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<feriadoDtos.Dtos.Feriado.FeriadoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public feriadoService: feriadoService.Services.FeriadoService) {

            super(
                $state,
                uiGridConstants,
                feriadoService, {
                    rotaAlterarRegistro: 'tenant.global.feriado.alterar',
                    rotaNovoRegistro: 'tenant.global.feriado.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new feriadoDtos.Dtos.Feriado.GetFeriadoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.data = new Date(termoDigitadoPesquisa);
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<feriadoDtos.Dtos.Feriado.FeriadoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.feriadoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewFeriado', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Feriado.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Feriado.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Feriado.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Feriado.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Feriado.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Feriado.Data'),
                field: 'data',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });
        }
    }

    export class FeriadoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/feriado/feriadoIndex.cshtml';
            this.controller = FeriadoIndexController;
        }
    }
}