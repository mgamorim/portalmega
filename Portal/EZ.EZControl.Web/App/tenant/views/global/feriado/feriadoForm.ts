﻿import * as feriadoService from "../../../../services/domain/global/feriado/feriadoService";
import * as paisService from "../../../../services/domain/global/pais/paisService";
import * as estadoService from "../../../../services/domain/global/estado/estadoService";
import * as cidadeService from "../../../../services/domain/global/cidade/cidadeService";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as feriadoDtos from "../../../../dtos/global/feriado/feriadoDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class FeriadoFormController extends controllers.Controllers.ControllerFormComponentBase<feriadoDtos.Dtos.Feriado.FeriadoListDto, feriadoDtos.Dtos.Feriado.FeriadoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectPais: ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectEstado: ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectCidade: ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParam.RequestParam.RequestParams>;

        public getFiltroParaPaginacaoPais: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoEstado: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoCidade: (termoDigitadoPesquisa: string) => any;

        private paisSelecionado: (registro: paisDtos.Dtos.Pais.PaisListDto) => string;
        private estadoSelecionado: (registro: estadoDtos.Dtos.Estado.EstadoListDto) => string;
        private cidadeSelecionado: (registro: cidadeDtos.Dtos.Cidade.CidadeListDto) => string;

        private paisDeselecionado: () => void;
        private estadoDeselecionado: () => void;
        private cidadeDeselecionado: () => void;

        private requestParamsPais: requestParam.RequestParam.RequestParams;
        private requestParamsEstado: requestParam.RequestParam.RequestParams;
        private requestParamsCidade: requestParam.RequestParam.RequestParams;

        private abrangenciaEnum: any;
        private tipoDeFeriadoEnum: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'feriadoService', 'paisService', 'estadoService', 'cidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public feriadoService: feriadoService.Services.FeriadoService,
            public paisService: paisService.Services.PaisService,
            public estadoService: estadoService.Services.EstadoService,
            public cidadeService: cidadeService.Services.CidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                feriadoService,
                'tenant.global.feriado');
        }

        $onInit() {
            super.init();

            this.abrangenciaEnum = ez.domain.enum.abrangenciaDoFeriadoEnum.valores;
            this.tipoDeFeriadoEnum = ez.domain.enum.tipoDeFeriadoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new feriadoDtos.Dtos.Feriado.FeriadoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;    
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pais
                if (entity.paisId) {
                    this.ezSelectPais.loading = true;
                    var promisePais = this.paisService.getById(entity.paisId);
                    promisePais.then(result => {
                        this.ezSelectPais.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectPais.loading = false;
                    });
                }

                // estado
                if (entity.estadoId) {
                    this.ezSelectEstado.loading = true;
                    var promiseEstado = this.estadoService.getById(entity.estadoId);
                    promiseEstado.then(result => {
                        this.ezSelectEstado.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectEstado.loading = false;
                    });
                }

                // cidade
                if (entity.cidadeId) {
                    this.ezSelectCidade.loading = true;
                    var promiseCidade = this.cidadeService.getById(entity.cidadeId);
                    promiseCidade.then(result => {
                        this.ezSelectCidade.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectCidade.loading = false;
                    });
                }

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.data = new Date(this.entity.data.toString());
            };

            this.ezSelectPaisConfig();
            this.ezSelectEstadoConfig();
            this.ezSelectCidadeConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectPaisConfig() {
            this.requestParamsPais = new requestParam.RequestParam.RequestParams();

            this.paisSelecionado = (registro) => {
                this.entity.paisId = registro.id;
                return registro.nome;
            };

            this.paisDeselecionado = () => {
                this.entity.paisId = null;
                this.entity.estadoId = null;
                this.entity.cidadeId = null;

                this.ezSelectEstado.gridSelectClear();
                this.ezSelectCidade.gridSelectClear();
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPais = (termoDigitadoPesquisa) => {
                var filtro = new paisDtos.Dtos.Pais.GetPaisInput();
                filtro.nomePais = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPais = new ezSelect.EzSelect.EzSelect<paisDtos.Dtos.Pais.PaisListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paisService,
                this.getFiltroParaPaginacaoPais,
                this.requestParamsPais,
                this.$uibModal,
                this.paisSelecionado,
                this.paisDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPais.onEzGridCreated = () => {
                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nome'),
                    field: 'nome'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.CodigoISO'),
                    field: 'codigoISO'
                });

                this.ezSelectPais.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pais.Nacionalidade'),
                    field: 'nacionalidade'
                });

                this.ezSelectPais.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPais.on.beforeOpenModalDialog = () => {
                this.ezSelectPais.ezGrid.getRegistros();
            };
        }

        private ezSelectEstadoConfig() {
            this.requestParamsEstado = new requestParam.RequestParam.RequestParams();

            this.estadoSelecionado = (registro) => {
                this.entity.estadoId = registro.id;
                return registro.nome;
            }

            this.estadoDeselecionado = () => {
                this.entity.estadoId = null;
                this.entity.cidadeId = null;

                this.ezSelectCidade.gridSelectClear();
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEstado = (termoDigitadoPesquisa) => {
                var filtro = null;
                if (!this.entity.paisId) {
                    abp.message.info(app.localize('Pais.SelecaoDoPaisNecessaria'), '');
                } else {
                    filtro = new estadoDtos.Dtos.Estado.GetEstadoByPaisInput();
                    filtro.paisId = this.entity.paisId;
                    filtro.nomeEstado = termoDigitadoPesquisa;
                    filtro.siglaEstado = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectEstado = new ezSelect.EzSelect.EzSelect<estadoDtos.Dtos.Estado.EstadoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.estadoService,
                this.getFiltroParaPaginacaoEstado,
                this.requestParamsEstado,
                this.$uibModal,
                this.estadoSelecionado,
                this.estadoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEstado.onEzGridCreated = () => {
                this.ezSelectEstado.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.estadoService.getEstadosByPaisId(filtro, requestParams);
                };

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Nome'),
                    field: 'nome'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEstado.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Estado.Sigla'),
                    field: 'sigla'
                });

                this.ezSelectEstado.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEstado.on.beforeOpenModalDialog = () => {
                if (this.entity.paisId) {
                    this.ezSelectEstado.ezGrid.getRegistros();
                }
            };
        }

        private ezSelectCidadeConfig() {
            this.requestParamsCidade = new requestParam.RequestParam.RequestParams();

            this.cidadeSelecionado = (registro) => {
                this.entity.cidadeId = registro.id;
                return registro.nome;
            }

            this.cidadeDeselecionado = () => {
                this.entity.cidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCidade = (termoDigitadoPesquisa) => {
                var filtro = null;
                if (!this.entity.estadoId) {
                    abp.message.info(app.localize('Cidade.EmptyEstadoError'), '');
                } else {
                    filtro = new cidadeDtos.Dtos.Cidade.GetCidadeByEstadoInput();
                    filtro.estadoId = this.entity.estadoId;
                    filtro.nomeCidade = termoDigitadoPesquisa;
                }
                return filtro;
            }

            this.ezSelectCidade = new ezSelect.EzSelect.EzSelect<cidadeDtos.Dtos.Cidade.CidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.cidadeService,
                this.getFiltroParaPaginacaoCidade,
                this.requestParamsCidade,
                this.$uibModal,
                this.cidadeSelecionado,
                this.cidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCidade.onEzGridCreated = () => {
                this.ezSelectCidade.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.cidadeService.getCidadesByEstadoId(filtro, requestParams);
                };

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectCidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cidade.Estado'),
                    field: 'estado.nome'
                });

                this.ezSelectCidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCidade.on.beforeOpenModalDialog = () => {
                if (this.entity.paisId) {
                    this.ezSelectCidade.ezGrid.getRegistros();
                }
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class FeriadoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/feriado/feriadoForm.cshtml';
            this.controller = FeriadoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}