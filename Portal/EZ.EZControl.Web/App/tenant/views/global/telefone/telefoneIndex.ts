﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as telefoneService from "../../../../services/domain/global/telefone/telefoneService";
import * as telefoneDtos from "../../../../dtos/global/telefone/telefoneDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Telefone {

    class TelefoneIndexController extends controllers.Controllers.ControllerIndexBase<telefoneDtos.Dtos.Telefone.TelefoneListDto, telefoneDtos.Dtos.Telefone.TelefoneInput>{

        static $inject = ['$state', 'uiGridConstants', 'telefoneService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<telefoneDtos.Dtos.Telefone.TelefoneListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public telefoneService: telefoneService.Services.TelefoneService) {

            super(
                $state,
                uiGridConstants,
                telefoneService, {
                    rotaAlterarRegistro: 'tenant.global.telefone.alterar',
                    rotaNovoRegistro: 'tenant.global.telefone.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();
            //this.requestParams.sorting = 'Numero';

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new telefoneDtos.Dtos.Telefone.GetTelefoneInput();
                filtro.numero = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<telefoneDtos.Dtos.Telefone.TelefoneListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.telefoneService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTelefone', this.novo, abp.auth.hasPermission('Pages.Tenant.Global.Telefone.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Telefone.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Global.Telefone.Create');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pessoa'),
                field: 'pessoa.nomePessoa',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FormaDePagamento.TipoDePagamento'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoCheckoutPagSeguroEnum", "tipoDePagamento")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Telefone.Tipo'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoCheckoutPagSeguroEnum", "tipoDePagamento")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Telefone.Numero'),
                field: 'numero'
            });
        }
    }

    export class TelefoneIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/global/telefone/telefoneIndex.cshtml';
            this.controller = TelefoneIndexController;
        }
    }
}