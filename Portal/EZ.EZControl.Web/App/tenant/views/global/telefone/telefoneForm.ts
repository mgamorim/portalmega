﻿import * as telefoneService from "../../../../services/domain/global/telefone/telefoneService";
import * as pessoaService from "../../../../services/domain/global/pessoa/pessoaService";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as telefoneDtos from "../../../../dtos/global/telefone/telefoneDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {
    export class TelefoneFormController extends
        controllers.Controllers.ControllerFormComponentBase<telefoneDtos.Dtos.Telefone.TelefoneListDto,
        telefoneDtos.Dtos.Telefone.TelefoneInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{

        public tipoTelefones: TipoTelefoneEnum[];;

        //private tiposDeTelefone: TipoTelefoneEnum[];

        // bindings do component
        public operation: string;
        public isDetail: boolean;
        public tipoPessoa: number;
        public pessoaId: number;
        public permissaoAlterarRegistro: string;
        public permissaoExcluirRegistro: string;
        public permissaoCriarRegistro: string;

        // Objetos, variaveis e métodos quando a entidade é um detalhe
        //============================================================
        //
        public saving: boolean;
        public editing: boolean;
        public editar: (telefone: telefoneDtos.Dtos.Telefone.TelefoneListDto) => void;
        public excluir: (telefone: telefoneDtos.Dtos.Telefone.TelefoneListDto) => void;
        public limparEntidade: () => void;

        // grid
        private setDataGrid: (result:applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<telefoneDtos.Dtos.Telefone.TelefoneListDto>) => void;
        private gridApi: uiGrid.IGridApiOf<telefoneDtos.Dtos.Telefone.TelefoneListDto>;
        private requestParamsTelefone: requestParam.RequestParam.RequestParams;
        private telefoneAppScopeProvider:ezGrid.EzGrid.EzGridAppScopeProvider<telefoneDtos.Dtos.Telefone.TelefoneListDto>;
        public getTelefones: () => void;
        public loadingTelefones: boolean;
        public telefoneGridOptions: uiGrid.IGridOptionsOf<telefoneDtos.Dtos.Telefone.TelefoneListDto>;
        private gridData: telefoneDtos.Dtos.Telefone.TelefoneListDto[];
        //===============================================================

        // pessoa
        public pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        public ezSelectPessoa: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto,requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoa: (termoDigitadoPesquisa: string) => any;
        private pessoaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaListDto) => string;
        private pessoaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoa: (id: number) => void;

        // tipoTelefone
        public tipoDeTelefone: any;
        public ezSelectTipoDeTelefone: ezSelect.EzSelect.EzSelect<telefoneDtos.Dtos.Telefone.TelefoneListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDeTelefone: (termoDigitadoPesquisa: string) => any;
        private tipoDeTelefoneSelecionado: (registro: telefoneDtos.Dtos.Telefone.TelefoneListDto) => string;
        private tipoDeTelefoneDeselecionado: () => void;
        private requestParamsTipoDeTelefone: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'telefoneService', 'pessoaService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public telefoneService: telefoneService.Services.TelefoneService,
            public pessoaService: pessoaService.Services.PessoaService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                telefoneService,
                'tenant.global.telefone');

            this.carregarPessoa = (id: number) => {
                this.ezSelectPessoa.loading = true;
                var promise = this.pessoaService.getById(id);
                promise.then(result => {
                    this.pessoa = result;
                    if (!this.isDetail) {
                        this.ezSelectPessoa.setInputText(result.nomePessoa);
                    } else {
                        this.entity.pessoaId = this.pessoa.id;
                    }
                }).finally(() => {
                    this.ezSelectPessoa.loading = false;
                });
            }

            this.getTelefones = () => {
                this.loadingTelefones = true;

                var filtro = new telefoneDtos.Dtos.Telefone.GetTelefoneInput();
                filtro.pessoaId = this.pessoaId;

                var promise = this.telefoneService.getPaged(filtro, this.requestParamsTelefone);

                if (promise) {
                    promise.then(result => {
                        this.setDataGrid(result);
                    }).finally(() => {
                        this.loadingTelefones = false;
                    });
                }
            }

            this.setDataGrid = (result:applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<telefoneDtos.Dtos.Telefone.TelefoneListDto>) => {
                this.gridData.length = 0;

                for (var i = 0; i < result.items.length; i++) {
                    this.gridData.push(result.items[i]);
                }

                this.telefoneGridOptions.totalItems = result.totalCount;
            };

            this.editar = (telefone: telefoneDtos.Dtos.Telefone.TelefoneListDto) => {
                this.editing = true;
                this.getEntityById(telefone.id);
            }

            this.excluir = (telefone: telefoneDtos.Dtos.Telefone.TelefoneListDto) => {
                this.editing = false;
                this.delete(telefone);
            }

            this.limparEntidade = () => {
                this.entity = new telefoneDtos.Dtos.Telefone.TelefoneInput();

                if (this.isDetail)
                    this.entity.pessoaId = this.pessoa.id;
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new telefoneDtos.Dtos.Telefone.TelefoneInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            this.tipoTelefones = ez.domain.enum.tipoTelefoneEnum.valores;


            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // pessoa
                if (entity.pessoaId) {
                    this.carregarPessoa(entity.pessoaId);
                }
            };

            this.events.onBeforeCancel = () => {
                // se caso for detalhe, o método cancel será abortado, para depois no evento onAfterCancel implementar alguma lógica necessária.
                return this.isDetail;
            };
            this.events.onAfterCancel = () => {
                // no cancel, a entidade da controller receberá uma nova instancia
                this.editing = false;
                this.limparEntidade();
            };
            this.events.onAfterSaveEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getTelefones();
                    this.limparEntidade();
                    this.editing = false;
                }
            };
            this.events.onDeletedEntity = () => {
                // se caso for detalhe, após salvar a entidade carregar a lista da grid
                if (this.isDetail) {
                    this.getTelefones();
                }
            };

            this.ezSelectPessoaConfig();
            this.telefoneGridConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaId) {
                this.carregarPessoa(this.pessoaId);
            }
        }

        $onChanges(changes) {
            if (changes.tipoPessoa) {
                if (changes.tipoPessoa.currentValue !== changes.tipoPessoa.previousValue) {

                    if (this.pessoa)
                        this.pessoa.tipoPessoa = changes.tipoPessoa.currentValue;
                }
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaSelecionado = (registro) => {
                this.pessoa = registro;
                this.entity.pessoaId = registro.id;
                return registro.nomePessoa;
            }

            this.pessoaDeselecionado = () => {
                this.entity.pessoaId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoa = (termoDigitadoPesquisa) => {
                var filtro = new pessoaDtos.Dtos.Pessoa.GetPessoaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPessoa = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaListDto,
                requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaService,
                this.getFiltroParaPaginacaoPessoa,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaSelecionado,
                this.pessoaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoa.onEzGridCreated = () => {
                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.DDI'),
                    field: 'DDI'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.DDD'),
                    field: 'ddd'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Numero'),
                    field: 'telefone.numero'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Ramal'),
                    field: 'telefone.ramal'
                });

                this.ezSelectPessoa.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Telefone.Tipo'),
                    cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "TipoTelefoneEnum", "tipo")}}\
                </div>'
                });

                this.ezSelectPessoa.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no 
            ezGrid
            this.ezSelectPessoa.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoa.ezGrid.getRegistros();
            };
        }

        // metodos usados quando a entidade é um detalhe
        private telefoneGridConfig() {
            if (this.isDetail) {
                this.requestParamsTelefone = new requestParam.RequestParam.RequestParams();
                //this.requestParamsTelefone.sorting = 'Numero';

                this.telefoneAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<telefoneDtos.Dtos.Telefone.TelefoneListDto>();

                this.telefoneAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                    (row) => {
                        return abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisicaTelefone.Edit');
                    }
                ));

                this.telefoneAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                    (row) => {
                        return abp.auth.hasPermission('Pages.Tenant.Global.PessoaFisicaTelefone.Delete');
                    }
                ));

                this.gridData = new Array<telefoneDtos.Dtos.Telefone.TelefoneListDto>();

                this.telefoneGridOptions = {
                    enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                    paginationPageSizes: app.consts.grid.defaultPageSizes,
                    paginationPageSize: app.consts.grid.defaultPageSize,
                    useExternalPagination: true,
                    showColumnFooter: true,
                    rowHeight: 50,
                    enableGridMenu: true,
                    exporterMenuCsv: false,
                    exporterMenuPdf: false,
                    rowTemplate: 'App/common/views/common/grid/linha.html',
                    columnDefs: [],
                    appScopeProvider: this.telefoneAppScopeProvider,
                    onRegisterApi: (api) => {
                        this.gridApi = api;
                        this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                            if (!sortColumns.length || !sortColumns[0].field) {
                                this.requestParamsTelefone.sorting = null;
                            } else {
                                this.requestParamsTelefone.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                            }

                            this.getTelefones();
                        });
                        this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                            this.requestParamsTelefone.skipCount = (pageNumber - 1) * pageSize;
                            this.requestParamsTelefone.maxResultCount = pageSize;

                            this.getTelefones();
                        });
                    }
                }

                this.telefoneGridOptions.data = this.gridData;

                 
              this.telefoneGridOptions.columnDefs.push({
                    name: app.localize('Telefone.DDD'),
                    field: 'ddd',
                    cellTemplate: '\
                                   <div class="ui-grid-cell-contents">\
                                       {{grid.getCellValue(row, col)}}<br />\
                                       <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                                           <span>\
                                               <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                                           </span> \
                                       </span>\
                                   </div>'
                });

                this.telefoneGridOptions.columnDefs.push({
                    name: app.localize('Telefone.Numero'),
                    field: 'numero'
                    //field: 'telefoneCompleto'
                 });

                this.telefoneGridOptions.columnDefs.push({
                    name: app.localize('Telefone.Tipo'),
                    cellTemplate: '\
                                    <div class="ui-grid-cell-contents">\
                                        {{grid.appScope.getEnumValue(row.entity, "tipoTelefoneEnum", "tipo")}}\
                                    </div>'
                });

                

                this.getTelefones();
            }
        }
    }

/*
Os bindings do componente funcionam da seguinte maneira:
    @: deve ser usado para strings
    <: objetos
    &: funcoes
    =: não deve ser mais usado, está obsoleto
*/
export class TelefoneFormComponent implements ng.IComponentOptions {
    public templateUrl: string;
    public controller: any;
    public bindings: any;

    constructor() {
        this.templateUrl = '~/app/tenant/views/global/telefone/telefoneForm.cshtml';
        this.controller = TelefoneFormController;
        this.bindings = {
            operation: '@',
            isDetail: '<',
            tipoPessoa: '<',
            pessoaId: '<',
            permissaoAlterarRegistro: '@',
            permissaoExcluirRegistro: '@',
            permissaoCriarRegistro: '@'
            }
        }
    }
}