﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as servicoService from "../../../../services/domain/servicos/servico/servicoService";
import * as servicoDtos from "../../../../dtos/servicos/servico/servicoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Servicos.Servico {

    class ServicoIndexController extends controllers.Controllers.ControllerIndexBase<servicoDtos.Dtos.Servico.ServicoListDto, servicoDtos.Dtos.Servico.ServicoInput>{

        static $inject = ['$state', 'uiGridConstants', 'servicosServicoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<servicoDtos.Dtos.Servico.ServicoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public servicoService: servicoService.Services.ServicoService) {

            super(
                $state,
                uiGridConstants,
                servicoService, {
                    rotaAlterarRegistro: 'tenant.servicos.servico.alterar',
                    rotaNovoRegistro: 'tenant.servicos.servico.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new servicoDtos.Dtos.Servico.GetServicoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<servicoDtos.Dtos.Servico.ServicoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.servicoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewServico', this.novo, abp.auth.hasPermission('Pages.Tenant.Servicos.Servico.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Servicos.Servico.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Servicos.Servico.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Servico.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Servico.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Servico.Valor'),
                field: 'valor'
            });
        }
    }

    export class ServicoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/servicos/servico/servicoIndex.cshtml';
            this.controller = ServicoIndexController;
        }
    }
}