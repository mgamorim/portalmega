﻿(function () {
    appModule.controller('tenant.views.dashboard.index', [
        '$scope', 'abp.services.app.tenantDashboard',
        function ($scope, tenantDashboardService, $timeout) {
            var vm = this;
            var isOpen = false;
            function fastAccess() {
                var faccItem = document.querySelectorAll('.facc-item');
                var popups = document.querySelectorAll('.popupbox');
                
                popups.forEach(function (el) {
                    document.querySelector('body').appendChild(el);
                });

                faccItem.forEach(function (el) {
                    el.addEventListener('click', function () {
                        var popup = document.querySelector('.popup-' + this.dataset.target);
                        // document.querySelector('body').appendChild(popup);
                        document.querySelector('.main-wrapper').classList.add('blur');
                        document.querySelector('.page-overlay').classList.add('active');
                        popup.classList.add('open');
                        isOpen = true;
                    });
                });
            };

            function closePopup() {
                var overlay = document.querySelector('.page-overlay');
                var popup = document.querySelectorAll('.popupbox');
                window.addEventListener('keyup', function (event) {
                    if (event.keyCode == 27 && isOpen) {
                        overlay.classList.remove('active');
                        document.querySelector('.main-wrapper').classList.remove('blur');
                        popup.forEach(function (el) {
                            el.classList.remove('open');
                        });
                        isOpen = false;
                    }
                })
                overlay.addEventListener('click', function () {
                    overlay.classList.remove('active');
                    document.querySelector('.main-wrapper').classList.remove('blur');
                    popup.forEach(function (el) {
                        el.classList.remove('open');
                    });
                    isOpen = false;
                });
            };

            vm.getMemberActivity = function () {
                tenantDashboardService.getMemberActivity({}).then(function (result) {
                    $("#totalMembersChart").sparkline(result.data.totalMembers, {
                        type: 'bar',
                        width: '100',
                        barWidth: 6,
                        height: '45',
                        barColor: '#F36A5B',
                        negBarColor: '#e02222',
                        chartRangeMin: 0
                    });

                    $("#newMembersChart").sparkline(result.data.newMembers, {
                        type: 'bar',
                        width: '100',
                        barWidth: 6,
                        height: '45',
                        barColor: '#5C9BD1',
                        negBarColor: '#e02222',
                        chartRangeMin: 0
                    });
                });
            };
        }
    ]);
})();