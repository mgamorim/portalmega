﻿import * as configuracaoDeDisponibilidadeConsultaService from "../../../../services/domain/ezmedical/configuracaoDeDisponibilidadeConsulta/configuracaoDeDisponibilidadeConsultaService";
import * as configuracaoDeDisponibilidadeConsultaDtos from "../../../../dtos/ezmedical/configuracaoDeDisponibilidadeConsulta/configuracaoDeDisponibilidadeConsultaDtos";
import * as medicoDtos from "../../../../dtos/ezmedical/medico/medicoDtos";
import * as medicoService from "../../../../services/domain/ezmedical/medico/medicoService";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class DisponibilidadeConsultaFormController extends controllers.Controllers.ControllerFormComponentBase<configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaListDto, configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoDisponibilidade: (termoDigitadoPesquisa: string) => any;
        private disponibilidadeSelecionado: (registro: configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaListDto) => string;
        private disponibilidadeDeselecionado: () => void;
        private editWithEvento: () => void;
        private requestParamsDisponibilidade: requestParam.RequestParam.RequestParams;
        private sistemaEnum: any;
        private tipoDeDisponibilidadeEnum: any;
        private tipoDeEventoEnum: any;
        private dayOfWeek: any;

        public ezSelectMedico: ezSelect.EzSelect.EzSelect<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoMedico: (termoDigitadoPesquisa: string) => any;
        private medicoSelecionado: (registro: medicoDtos.Dtos.EZMedical.Medico.MedicoListDto) => string;
        private medicoDeselecionado: () => void;
        private requestParamsMedico: requestParam.RequestParam.RequestParams;

        public ezSelectEspecialidade: ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEspecialidade: (termoDigitadoPesquisa: string) => any;
        private especialidadeSelecionado: (registro: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto) => string;
        private especialidadeDeselecionado: () => void;
        private requestParamsEspecialidade: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'configuracaoDeDisponibilidadeConsultaService', 'medicoService', 'especialidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeDisponibilidadeConsultaService: configuracaoDeDisponibilidadeConsultaService.Services.ConfiguracaoDeDisponibilidadeConsultaService,
            public medicoService: medicoService.Services.MedicoService,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                configuracaoDeDisponibilidadeConsultaService,
                'tenant.ezmedical.disponibilidade');
        }

        $onInit() {
            super.init();

            this.sistemaEnum = ez.domain.enum.sistemaEnum.valores;
            this.tipoDeDisponibilidadeEnum = ez.domain.enum.tipoDeDisponibilidadeEnum.valores;
            this.tipoDeEventoEnum = ez.domain.enum.tipoDeEventoEnum.valores;
            this.dayOfWeek = ez.domain.enum.dayOfWeek.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.sistema = SistemaEnum.EZMedical;
                instance.tipoDeEvento = TipoDeEventoEnum.Consulta;
                return instance;
            };



            this.events.onGetEntity = () => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.dataEspecifica = this.entity.dataEspecifica != null ? new Date(this.entity.dataEspecifica.toString()) : null;
                this.entity.dataInicioValidade = this.entity.dataInicioValidade != null ? new Date(this.entity.dataInicioValidade.toString()) : null;
                this.entity.dataFimValidade = this.entity.dataFimValidade != null ? new Date(this.entity.dataFimValidade.toString()) : null;
            };

            this.editWithEvento = () => {
                var promise = this.configuracaoDeDisponibilidadeConsultaService.existeEvento(this.entity.id);
                promise.then(result => {
                    if (result) {
                        abp.message.confirm(
                            'Existem Eventos Gerados para Disponibilidades',
                            'Deseja realmente alterar?',
                            isConfirmed => {
                                if (isConfirmed) {
                                    this.save();
                                }
                            }
                        );
                    } else {
                        this.save();
                    }
                });
            };

            this.events.onBeforeSaveEntity = () => {
                this.entity.diasDaSemana = (this.entity.tipoDeDisponibilidade === 2) ? this.entity.semana.toString() : null;

                if (this.entity.tipoDeDisponibilidade === 1) {
                    this.entity.dataFimValidade = new Date();
                    this.entity.dataFimValidade.setDate(this.entity.dataInicioValidade.getDate() + this.entity.numeroDeDias);
                }
            };

            this.ezSelectEspecialidadeConfig();
            this.ezSelectMedicoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectMedicoConfig() {
            this.requestParamsMedico = new requestParam.RequestParam.RequestParams();

            this.medicoSelecionado = (registro) => {
                this.entity.medicoId = registro.id;
                return registro.pessoaFisica.nome;
            }

            this.medicoDeselecionado = () => {
                this.entity.medicoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoMedico = (termoDigitadoPesquisa) => {
                var filtro = new medicoDtos.Dtos.EZMedical.Medico.GetMedicoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.numeroDocumento = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectMedico = new ezSelect.EzSelect.EzSelect<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.medicoService,
                this.getFiltroParaPaginacaoMedico,
                this.requestParamsMedico,
                this.$uibModal,
                this.medicoSelecionado,
                this.medicoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectMedico.onEzGridCreated = () => {
                this.ezSelectMedico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Medico.Nome'),
                    field: 'nomePessoa'
                });

                this.ezSelectMedico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Medico.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectMedico.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectMedico.on.beforeOpenModalDialog = () => {
                this.ezSelectMedico.ezGrid.getRegistros();
            };
        }

        private ezSelectEspecialidadeConfig() {
            this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();

            this.especialidadeSelecionado = (registro) => {
                this.entity.especialidadeId = registro.id;
                return registro.nome;
            }

            this.especialidadeDeselecionado = () => {
                this.entity.especialidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEspecialidade = (termoDigitadoPesquisa) => {
                var filtro = new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEspecialidade = new ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoEspecialidade,
                this.requestParamsEspecialidade,
                this.$uibModal,
                this.especialidadeSelecionado,
                this.especialidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEspecialidade.onEzGridCreated = () => {
                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEspecialidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEspecialidade.on.beforeOpenModalDialog = () => {
                this.ezSelectEspecialidade.ezGrid.getRegistros();
            };
        }

        tipoDeDisponibilidadeSelected = () => {
            if (this.entity.tipoDeDisponibilidade !== 3) {
                this.entity.dataInicioValidade = new Date();
                this.entity.dataInicioValidade.setDate(this.entity.dataInicioValidade.getDate() + 1);
            }
        };

        calcValidade = () => {
            if (this.entity.dataInicioValidade != null) {
                this.entity.dataFimValidade = new Date(this.entity.dataInicioValidade);
                this.entity.dataFimValidade.setDate(this.entity.dataFimValidade.getDate() + this.entity.numeroDeDias);
            }
        };


    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DisponibilidadeFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/disponibilidadeConsulta/disponibilidadeConsultaForm.cshtml';
            this.controller = DisponibilidadeConsultaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}