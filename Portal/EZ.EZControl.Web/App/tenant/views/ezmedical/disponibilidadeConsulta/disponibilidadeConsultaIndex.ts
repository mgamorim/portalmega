﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as configuracaoDeDisponibilidadeConsultaService from "../../../../services/domain/ezmedical/configuracaoDeDisponibilidadeConsulta/configuracaoDeDisponibilidadeConsultaService";
import * as configuracaoDeDisponibilidadeConsultaDtos from "../../../../dtos/ezmedical/configuracaoDeDisponibilidadeConsulta/configuracaoDeDisponibilidadeConsultaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZMedical.Disponibilidade {
    class DisponibilidadeConsultaIndexController extends controllers.Controllers.ControllerIndexBase<configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaListDto, configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaInput>{
        static $inject = ['$state', 'uiGridConstants', 'configuracaoDeDisponibilidadeConsultaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeDisponibilidadeConsultaService: configuracaoDeDisponibilidadeConsultaService.Services.ConfiguracaoDeDisponibilidadeConsultaService) {
            super(
                $state,
                uiGridConstants,
                configuracaoDeDisponibilidadeConsultaService, {
                    rotaAlterarRegistro: 'tenant.ezmedical.disponibilidade.alterar',
                    rotaNovoRegistro: 'tenant.ezmedical.disponibilidade.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.GetConfiguracaoDeDisponibilidadeConsultaInput();
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.configuracaoDeDisponibilidadeConsultaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDisponibilidade', this.novo, abp.auth.hasPermission('Pages.Tenant.EZMedical.Disponibilidade.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Disponibilidade.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Disponibilidade.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Titulo'),
                field: 'titulo',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.TipoDeDisponibilidade'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeDisponibilidadeEnum", "tipoDeDisponibilidade")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Medico'),
                field: 'medicoNomePessoa'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Especialidade'),
                field: 'especialidadeNome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.DiaDaSemana'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValues(row.entity, "dayOfWeek", "diasDaSemana")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.NumeroDeDias'),
                field: 'numeroDeDias'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.Data'),
                field: 'dataEspecifica',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.HorarioInicio'),
                field: 'horarioInicio',
                cellFilter: 'date:"HH:mm\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Disponibilidade.HorarioFim'),
                field: 'horarioFim',
                cellFilter: 'date:"HH:mm\"'
            });
        }
    }

    export class DisponibilidadeConsultaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/disponibilidadeConsulta/disponibilidadeConsultaIndex.cshtml';
            this.controller = DisponibilidadeConsultaIndexController;
        }
    }
}