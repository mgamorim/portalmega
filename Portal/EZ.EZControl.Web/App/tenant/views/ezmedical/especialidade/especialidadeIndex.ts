﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZMedical.Especialidade {
    class EspecialidadeIndexController extends controllers.Controllers.ControllerIndexBase<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput>{
        static $inject = ['$state', 'uiGridConstants', 'especialidadeService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public especialidadeService: especialidadeService.Services.EspecialidadeService) {
            super(
                $state,
                uiGridConstants,
                especialidadeService, {
                    rotaAlterarRegistro: 'tenant.ezmedical.especialidade.alterar',
                    rotaNovoRegistro: 'tenant.ezmedical.especialidade.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput();
                filtro.codigo = termoDigitadoPesquisa;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewEspecialidade', this.novo, abp.auth.hasPermission('Pages.Tenant.EZMedical.Especialidade.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Especialidade.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Especialidade.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Especialidade.Codigo'),
                field: 'codigo',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Especialidade.Nome'),
                field: 'nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Especialidade.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class EspecialidadeIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/especialidade/especialidadeIndex.cshtml';
            this.controller = EspecialidadeIndexController;
        }
    }
}