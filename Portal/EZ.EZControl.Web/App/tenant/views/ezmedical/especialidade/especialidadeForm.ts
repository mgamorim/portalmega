﻿import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Forms {
    export class EspecialidadeFormController extends controllers.Controllers.ControllerFormComponentBase<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'especialidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                especialidadeService,
                'tenant.ezmedical.especialidade');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            this.events.onGetEntity = (entity) => {
            };

            this.events.onBeforeSaveEntity = () => {
            }

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class EspecialidadeFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/especialidade/especialidadeForm.cshtml';
            this.controller = EspecialidadeFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}