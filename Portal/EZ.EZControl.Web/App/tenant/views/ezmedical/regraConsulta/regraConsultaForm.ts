﻿import * as regraConsultaService from "../../../../services/domain/ezmedical/regraConsulta/regraConsultaService";
import * as regraConsultaDtos from "../../../../dtos/ezmedical/regraConsulta/regraConsultaDtos";
import * as medicoDtos from "../../../../dtos/ezmedical/medico/medicoDtos";
import * as medicoService from "../../../../services/domain/ezmedical/medico/medicoService";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class RegraConsultaFormController extends controllers.Controllers.ControllerFormComponentBase<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaListDto, regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoRegraConsulta: (termoDigitadoPesquisa: string) => any;
        private regraConsultaSelecionado: (registro: regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaListDto) => string;
        private regraConsultaDeselecionado: () => void;
        private requestParamsRegraConsulta: requestParam.RequestParam.RequestParams;

        public ezSelectMedico: ezSelect.EzSelect.EzSelect<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoMedico: (termoDigitadoPesquisa: string) => any;
        private medicoSelecionado: (registro: medicoDtos.Dtos.EZMedical.Medico.MedicoListDto) => string;
        private medicoDeselecionado: () => void;
        private requestParamsMedico: requestParam.RequestParam.RequestParams;

        public ezSelectEspecialidade: ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEspecialidade: (termoDigitadoPesquisa: string) => any;
        private especialidadeSelecionado: (registro: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto) => string;
        private especialidadeDeselecionado: () => void;
        private requestParamsEspecialidade: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'regraConsultaService', 'medicoService', 'especialidadeService', '$uibModal'];

        public sistema: any; 
        public tipoDeEvento: any; 
        public sistemaOnChange: (sistemaEnum: number) => void;
        private setTipoDeEvento: (sistemaEnum: number) => void;
        private setSistema: () => void;

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public regraConsultaService: regraConsultaService.Services.RegraConsultaService,
            public medicoService: medicoService.Services.MedicoService,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                regraConsultaService,
                'tenant.ezmedical.regras');
        }

        $onInit() {
            super.init();

            //this.setSistema();

            //this.events.onGetEntity = (regraBase) => {
            //    this.setTipoDeEvento(regraBase.sistema);  
            //};

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.sistema = SistemaEnum.EZMedical;
                instance.tipoDeEvento = TipoDeEventoEnum.Consulta;
                return instance;
            };

            this.ezSelectEspecialidadeConfig();
            this.ezSelectMedicoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

        }

        private ezSelectMedicoConfig() {
            this.requestParamsMedico = new requestParam.RequestParam.RequestParams();

            this.medicoSelecionado = (registro) => {
                this.entity.medicoId = registro.id;
                return registro.pessoaFisica.nome;
            }

            this.medicoDeselecionado = () => {
                this.entity.medicoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoMedico = (termoDigitadoPesquisa) => {
                var filtro = new medicoDtos.Dtos.EZMedical.Medico.GetMedicoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.numeroDocumento = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectMedico = new ezSelect.EzSelect.EzSelect<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.medicoService,
                this.getFiltroParaPaginacaoMedico,
                this.requestParamsMedico,
                this.$uibModal,
                this.medicoSelecionado,
                this.medicoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectMedico.onEzGridCreated = () => {
                this.ezSelectMedico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Medico.Nome'),
                    field: 'pessoaFisica.nomePessoa'
                });

                this.ezSelectMedico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Medico.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectMedico.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectMedico.on.beforeOpenModalDialog = () => {
                this.ezSelectMedico.ezGrid.getRegistros();
            };
        }

        private ezSelectEspecialidadeConfig() {
            this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();

            this.especialidadeSelecionado = (registro) => {
                this.entity.especialidadeId = registro.id;
                return registro.nome;
            }

            this.especialidadeDeselecionado = () => {
                this.entity.especialidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEspecialidade = (termoDigitadoPesquisa) => {
                var filtro = new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEspecialidade = new ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoEspecialidade,
                this.requestParamsEspecialidade,
                this.$uibModal,
                this.especialidadeSelecionado,
                this.especialidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEspecialidade.onEzGridCreated = () => {
                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEspecialidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEspecialidade.on.beforeOpenModalDialog = () => {
                this.ezSelectEspecialidade.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class RegraConsultaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/regraConsulta/regraConsultaForm.cshtml';
            this.controller = RegraConsultaFormController;
            this.bindings = {
                operation: '@'
            };
        }
    }
}