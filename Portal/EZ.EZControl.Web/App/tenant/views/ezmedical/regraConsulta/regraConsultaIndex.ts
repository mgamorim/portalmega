﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as regraConsultaService from "../../../../services/domain/ezmedical/regraConsulta/regraConsultaService";
import * as regraConsultaDtos from "../../../../dtos/ezmedical/regraConsulta/regraConsultaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZMedical.RegraConsulta {

    class RegraConsultaIndexController extends controllers.Controllers.ControllerIndexBase<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaListDto, regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaInput>{

        static $inject = ['$state', 'uiGridConstants', 'regraConsultaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public regraConsultaService: regraConsultaService.Services.RegraConsultaService) {

            super(
                $state,
                uiGridConstants,
                regraConsultaService, {
                    rotaAlterarRegistro: 'tenant.ezmedical.regras.alterar',
                    rotaNovoRegistro: 'tenant.ezmedical.regras.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new regraConsultaDtos.Dtos.EZMedical.RegraConsulta.GetRegraConsultaInput();
                filtro.sistema = termoDigitadoPesquisa;
                filtro.tipoDeEvento = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.regraConsultaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('EZMedical.RegraConsulta.CreatingNewRegra', this.novo, abp.auth.hasPermission('Pages.Tenant.EZMedical.RegraConsulta.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.RegraConsulta.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.RegraConsulta.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agenda.Parametros.Regras.Sistema'),
                field: 'sistema',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EZMedical.RegraConsulta.Medico'),
                field: 'medicoNomePessoa'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EZMedical.RegraConsulta.Especialidade'),
                field: 'especialidadeNome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agenda.Parametros.Regras.TipoDeEvento'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeEventoEnum", "tipoDeEvento")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Agenda.Parametros.Regras.IsActive'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class RegraConsultaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/regraConsulta/regraConsultaIndex.cshtml';
            this.controller = RegraConsultaIndexController;
        }
    }
}