﻿import Scheduler = require("../../../../common/ezScheduler/ezScheduler");
import * as eventoDtos from "../../../../dtos/ezmedical/evento/eventoConsultaDtos";
import * as regraService from "../../../../services/domain/ezmedical/regraConsulta/regraConsultaService";
import * as regrasDtos from "../../../../dtos/agenda/parametros/parametrosRegrasAgendaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";

export module EZMedical.EventoConsulta {

    class EventoConsultaIndexController {
        public ezScheduler: Scheduler.EzScheduler.EzScheduler;
        static $inject = ['$state', 'parametrosRegrasConsultaService'];
        private checkDisponibilidade: (start: Date, end: Date) => boolean;
        private configScheduler: (tempoConsulta: number) => void;
        private getRegras: () => void;

        constructor(public $state: ng.ui.IStateService, public regraBaseService: regraService.Services.RegraConsultaService) {
            this.ezScheduler = new Scheduler.EzScheduler.EzScheduler();
            this.ezScheduler.sistema = SistemaEnum.EZMedical;
            this.configScheduler = (tempoConsulta: number) => {
                this.ezScheduler.onSchedulerRendered = (scheduler: kendo.ui.Scheduler) => {
                    this.ezScheduler.dataSource.options.transport.read.url = '/api/services/app/ezMedical/eventoConsulta/read';
                    this.ezScheduler.dataSource.options.transport.read.data = () => {
                        var startDate = this.ezScheduler.scheduler.view().startDate();
                        var endDate = this.ezScheduler.scheduler.view().endDate();
                        var input = new eventoDtos.Dtos.EZMedical.EventoConsulta.GetEventoConsultaInput();
                        input.inicio = startDate;
                        input.termino = endDate;
                        input.tipo = TipoDeEventoEnum.Consulta;
                        input.sistema = SistemaEnum.EZMedical;
                        return input;
                    }

                    if (tempoConsulta > 0) {
                        this.ezScheduler.options.minorTickCount = tempoConsulta;
                    }

                    this.ezScheduler.options.edit = (e) => {
                        e.preventDefault();
                        if (e.event.start <= new Date() || e.event.end <= new Date()) {
                            abp.message.warn("Evento.InvalidDateWarningMessage", "Evento.WarninMessage");
                            return;
                        }
                        if (e.event.id == 0) {
                            this.$state.go('tenant.ezmedical.evento.novo', { 'start': e.event.start, 'end': e.event.end });
                        } else {
                            this.$state.go('tenant.ezmedical.evento.alterar', { "id": e.event.id });
                        }
                    };

                    this.ezScheduler.options.dataBound = (e) => {
                        var scheduler = e.sender;
                        $(".k-event").each(function () {
                            var uid = $(this).data("uid");
                            if (uid) {
                                var event = scheduler.occurrenceByUid(uid);
                                if (event) {
                                    $(this).find(".k-event-delete").click(function (clc) {
                                        clc.preventDefault();
                                        abp.message.confirm(app.localize("EventoConsulta.DeleteConfirmationMessage"), app.localize("Evento.ConfirmationMessage"), (isConfirmed) => {
                                            if (isConfirmed) {
                                                $state.go('tenant.ezmedical.evento.excluir', { 'id': event.id });
                                            }
                                        });
                                        clc.stopPropagation();
                                    });
                                }
                            }
                        });
                    };

                    this.ezScheduler.options.editable = {
                        resize: abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Edit'),
                        move: abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Edit'),
                        destroy: abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Delete'),
                        create: abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Create'),
                        update: abp.auth.hasPermission('Pages.Tenant.EZMedical.Evento.Edit'),
                        confirmation: false
                    };

                    this.ezScheduler.options.navigate = (e) => {
                        this.ezScheduler.scheduler.refresh();
                    };

                    this.ezScheduler.scheduler.setOptions(this.ezScheduler.options);
                    this.ezScheduler.scheduler.setDataSource(this.ezScheduler.dataSource);
                };
            };

            this.getRegras = () => {
                var promise = regraBaseService.get();
                promise.then(result => {
                    var espacosPorHora = 0;
                    if (result) {
                        var horasUnidadeDeTempo = result.unidadeDeTempo / 3600;
                        var minutosUnidadeDeTempo = result.unidadeDeTempo - (horasUnidadeDeTempo * 3600);

                        var unidadeDeTempoDate = new Date(2016, 11, 1, horasUnidadeDeTempo, minutosUnidadeDeTempo);

                        espacosPorHora = (60/((unidadeDeTempoDate.getTime() / (1000 * 60)) % 60));
                    } else {
                        abp.message.warn(app.localize("EventoConsulta.RegraNotFoundError"), app.localize("Evento.WarningMessage"));
                    }
                    this.configScheduler(espacosPorHora);
                });
            };

            this.getRegras();
        }

        $onInit() {   
        }
    }

    export class EventoConsultaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/evento/eventoConsultaIndex.cshtml';
            this.controller = EventoConsultaIndexController;
        }
    }
}