﻿import * as configuracaoDeBloqueioConsultaService from "../../../../services/domain/ezmedical/configuracaoDeBloqueioConsulta/configuracaoDeBloqueioConsultaService";
import * as configuracaoDeBloqueioConsultaDtos from "../../../../dtos/ezmedical/configuracaoDeBloqueioConsulta/configuracaoDeBloqueioConsultaDtos";
import * as medicoDtos from "../../../../dtos/ezmedical/medico/medicoDtos";
import * as medicoService from "../../../../services/domain/ezmedical/medico/medicoService";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class BloqueioConsultaFormController extends controllers.Controllers.ControllerFormComponentBase<configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaListDto, configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoBloqueio: (termoDigitadoPesquisa: string) => any;
        private bloqueioSelecionado: (registro: configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaListDto) => string;
        private bloqueioDeselecionado: () => void;
        private requestParamsBloqueio: requestParam.RequestParam.RequestParams;
        private sistemaEnum: any;
        private motivoDoBloqueioEnum: any;
        private tipoDeEventoEnum: any;
        private horarioInicioModel: Date;
        private horarioFimModel: Date;

        public ezSelectMedico: ezSelect.EzSelect.EzSelect<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoMedico: (termoDigitadoPesquisa: string) => any;
        private medicoSelecionado: (registro: medicoDtos.Dtos.EZMedical.Medico.MedicoListDto) => string;
        private medicoDeselecionado: () => void;
        private requestParamsMedico: requestParam.RequestParam.RequestParams;

        public ezSelectEspecialidade: ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoEspecialidade: (termoDigitadoPesquisa: string) => any;
        private especialidadeSelecionado: (registro: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto) => string;
        private especialidadeDeselecionado: () => void;
        private requestParamsEspecialidade: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'configuracaoDeBloqueioConsultaService', 'medicoService', 'especialidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeBloqueioConsultaService: configuracaoDeBloqueioConsultaService.Services.ConfiguracaoDeBloqueioConsultaService,
            public medicoService: medicoService.Services.MedicoService,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                configuracaoDeBloqueioConsultaService,
                'tenant.ezmedical.bloqueioConsulta');
        }

        $onInit() {
            super.init();

            this.sistemaEnum = ez.domain.enum.sistemaEnum.valores;
            this.motivoDoBloqueioEnum = ez.domain.enum.motivoDoBloqueioEnum.valores;
            this.tipoDeEventoEnum = ez.domain.enum.tipoDeEventoEnum.valores;

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.sistema = SistemaEnum.EZMedical;
                instance.tipoDeEvento = TipoDeEventoEnum.Consulta;
                return instance;
            };

            this.events.onBeforeSaveEntity = () => {
                var horarioInicio = this.horarioInicioModel;
                var horaInicio = (horarioInicio.getHours() < 10) ? "0" + horarioInicio.getHours().toString() : horarioInicio.getHours().toString();
                var minutoInicio = (horarioInicio.getMinutes() < 10) ? "0" + horarioInicio.getMinutes().toString() : horarioInicio.getMinutes().toString();
                this.entity.horarioInicio = horaInicio + ":" + minutoInicio;
                
                var horarioFim = this.horarioFimModel;
                var horaFim = (horarioFim.getHours() < 10) ? "0" + horarioFim.getHours().toString() : horarioFim.getHours().toString();
                var minutoFim = (horarioFim.getMinutes() < 10) ? "0" + horarioFim.getMinutes().toString() : horarioFim.getMinutes().toString();
                this.entity.horarioFim = horaFim + ":" + minutoFim;
            }

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // a data vem do servidor como string, então é necessário converter para date
                this.entity.inicioBloqueio = this.entity.inicioBloqueio != null ? new Date(this.entity.inicioBloqueio.toString()) : null;
                this.entity.fimBloqueio = this.entity.fimBloqueio != null ? new Date(this.entity.fimBloqueio.toString()) : null;

                var horaInicio = 0;
                var minutoInicio = 0;
                var horaFim = 0;
                var minutoFim = 0;

                var partesHorarioInicio = this.entity.horarioInicio.split(':');
                var partesHorarioFim = this.entity.horarioFim.split(':');

                horaInicio = parseInt(partesHorarioInicio[0], 0);
                minutoInicio = parseInt(partesHorarioInicio[1], 0);

                horaFim = parseInt(partesHorarioFim[0], 0);
                minutoFim = parseInt(partesHorarioFim[1], 0);

                this.horarioInicioModel = new Date(2016, 1, 1, horaInicio, minutoInicio);
                this.horarioFimModel = new Date(2016, 1, 1, horaFim, minutoFim);
            };

            this.ezSelectEspecialidadeConfig();
            this.ezSelectMedicoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectMedicoConfig() {
            this.requestParamsMedico = new requestParam.RequestParam.RequestParams();

            this.medicoSelecionado = (registro) => {
                this.entity.medicoId = registro.id;
                return registro.pessoaFisica.nome;
            }

            this.medicoDeselecionado = () => {
                this.entity.medicoId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoMedico = (termoDigitadoPesquisa) => {
                var filtro = new medicoDtos.Dtos.EZMedical.Medico.GetMedicoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.numeroDocumento = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectMedico = new ezSelect.EzSelect.EzSelect<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.medicoService,
                this.getFiltroParaPaginacaoMedico,
                this.requestParamsMedico,
                this.$uibModal,
                this.medicoSelecionado,
                this.medicoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectMedico.onEzGridCreated = () => {
                this.ezSelectMedico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Medico.Nome'),
                    field: 'nomePessoa'
                });

                this.ezSelectMedico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Medico.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectMedico.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectMedico.on.beforeOpenModalDialog = () => {
                this.ezSelectMedico.ezGrid.getRegistros();
            };
        }

        private ezSelectEspecialidadeConfig() {
            this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();

            this.especialidadeSelecionado = (registro) => {
                this.entity.especialidadeId = registro.id;
                return registro.nome;
            }

            this.especialidadeDeselecionado = () => {
                this.entity.especialidadeId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoEspecialidade = (termoDigitadoPesquisa) => {
                var filtro = new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.codigo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectEspecialidade = new ezSelect.EzSelect.EzSelect<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.especialidadeService,
                this.getFiltroParaPaginacaoEspecialidade,
                this.requestParamsEspecialidade,
                this.$uibModal,
                this.especialidadeSelecionado,
                this.especialidadeDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectEspecialidade.onEzGridCreated = () => {
                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Nome'),
                    field: 'nome'
                });

                this.ezSelectEspecialidade.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Especialidade.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectEspecialidade.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectEspecialidade.on.beforeOpenModalDialog = () => {
                this.ezSelectEspecialidade.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class BloqueioConsultaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/bloqueioConsulta/bloqueioConsultaForm.cshtml';
            this.controller = BloqueioConsultaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}