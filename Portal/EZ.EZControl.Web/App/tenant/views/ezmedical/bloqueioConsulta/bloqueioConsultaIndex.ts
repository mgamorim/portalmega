﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as configuracaoDeBloqueioConsultaService from "../../../../services/domain/ezmedical/configuracaoDeBloqueioConsulta/configuracaoDeBloqueioConsultaService";
import * as configuracaoDeBloqueioConsultaDtos from "../../../../dtos/ezmedical/configuracaoDeBloqueioConsulta/configuracaoDeBloqueioConsultaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZMedical.Bloqueio {
    class BloqueioConsultaIndexController extends controllers.Controllers.ControllerIndexBase<configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaListDto, configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaInput>{
        static $inject = ['$state', 'uiGridConstants', 'configuracaoDeBloqueioConsultaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public configuracaoDeBloqueioConsultaService: configuracaoDeBloqueioConsultaService.Services.ConfiguracaoDeBloqueioConsultaService) {
            super(
                $state,
                uiGridConstants,
                configuracaoDeBloqueioConsultaService, {
                    rotaAlterarRegistro: 'tenant.ezmedical.bloqueio.alterar',
                    rotaNovoRegistro: 'tenant.ezmedical.bloqueio.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.GetConfiguracaoDeBloqueioConsultaInput();
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.configuracaoDeBloqueioConsultaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewBloqueio', this.novo, abp.auth.hasPermission('Pages.Tenant.EZMedical.Bloqueio.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Bloqueio.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Bloqueio.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.Titulo'),
                field: 'titulo',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.MotivoDoBloqueio'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "motivoDoBloqueioEnum", "motivoDoBloqueio")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.Medico'),
                field: 'medicoNomePessoa'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.Especialidade'),
                field: 'especialidadeNome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.InicioDoBloqueio'),
                field: 'inicioBloqueio',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.HorarioInicio'),
                field: 'horarioInicio',
                cellFilter: 'date:"HH:mm\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.FimDoBloqueio'),
                field: 'fimBloqueio',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.HorarioFim'),
                field: 'horarioFim',
                cellFilter: 'date:"HH:mm\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Bloqueio.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class BloqueioConsultaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/bloqueioConsulta/bloqueioConsultaIndex.cshtml';
            this.controller = BloqueioConsultaIndexController;
        }
    }
}