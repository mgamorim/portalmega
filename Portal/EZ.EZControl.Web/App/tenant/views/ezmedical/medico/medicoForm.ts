﻿import * as medicoService from "../../../../services/domain/ezmedical/medico/medicoService";
import * as medicoDtos from "../../../../dtos/ezmedical/medico/medicoDtos";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as pessoaFisicaService from "../../../../services/domain/global/pessoaFisica/pessoaFisicaService";
import * as especialidadeService from "../../../../services/domain/ezmedical/especialidade/especialidadeService";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Forms {
    export class MedicoFormController extends controllers.Controllers.ControllerFormComponentBase<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, medicoDtos.Dtos.EZMedical.Medico.MedicoInput, medicoDtos.Dtos.EZMedical.Medico.MedicoPessoaIdDto>{
        public medicoForm: ng.IFormController;

        private idPicture = null;

        public getFiltroParaPaginacaoMedico: (termoDigitadoPesquisa: string) => any;
        private medicoSelecionado: (registro: medicoDtos.Dtos.EZMedical.Medico.MedicoListDto) => string;
        private medicoDeselecionado: () => void;
        private requestParamsMedico: requestParam.RequestParam.RequestParams;

        // pessoa
        public pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        public ezSelectPessoaFisica: ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPessoaFisica: (termoDigitadoPesquisa: string) => any;
        private pessoaFisicaSelecionado: (registro: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto) => string;
        private pessoaFisicaDeselecionado: () => void;
        private requestParamsPessoa: requestParam.RequestParam.RequestParams;
        private carregarPessoaFisica: (id: number) => void;

        public especialidades = [];
        public requestParamsEspecialidade: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'medicoService', 'pessoaFisicaService', 'especialidadeService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public medicoService: medicoService.Services.MedicoService,
            public pessoaFisicaService: pessoaFisicaService.Services.PessoaFisicaService,
            public especialidadeService: especialidadeService.Services.EspecialidadeService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                medicoService,
                'tenant.ezmedical.medico');

            this.carregarPessoaFisica = (id: number) => {
                this.ezSelectPessoaFisica.loading = true;
                var promise = this.pessoaFisicaService.getById(id);
                promise.then(result => {
                    this.pessoaFisica = result;
                    this.entity.pessoaFisica.id = this.pessoaFisica.id;
                    this.ezSelectPessoaFisica.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectPessoaFisica.loading = false;
                });

            }
        }

        $onInit() {
            super.init();
            this.paramTokenize();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new medicoDtos.Dtos.EZMedical.Medico.MedicoInput();
                instance.id = 0;
                instance.especialidades = [];
                instance.pessoa = new pessoaDtos.Dtos.Pessoa.PessoaInput();
                instance.isActive = true;
                instance.pessoa.tipoPessoa = 1;
                instance.pessoa.grupoPessoaId = null;
                instance.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
                instance.pessoaFisica.tipoPessoa = 1;

                this.getEspecialidades();

                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.entity.especialidades = entity.especialidades;

                if (entity.pessoa) {
                    this.carregarPessoaFisica(entity.pessoaFisica.id);
                }

                if (entity.pictureId) {
                    this.idPicture = entity.pictureId;
                }

                this.formataEspecialidades();
            };

            this.events.onAfterSaveEntity = (result) => {
                this.entity.id = result.medicoId;
                this.entity.pessoaFisica.id = result.pessoaId;
            };

            this.events.onBeforeSaveEntity = () => {
                this.entity.pictureId = this.idPicture;
                this.ajustaEspecialidades();
            }

            this.ezSelectPessoaConfig();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            if (this.pessoaFisica) {
                this.carregarPessoaFisica(this.pessoaFisica.id);
            }
        }

        private ezSelectPessoaConfig() {
            this.requestParamsPessoa = new requestParam.RequestParam.RequestParams();
            this.requestParamsPessoa.sorting = "NomePessoa";

            this.pessoaFisicaSelecionado = (registro) => {
                this.pessoaFisica = registro;
                this.entity.pessoaFisica.id = registro.id;
                this.entity.pessoaFisica.tipoPessoa = 1;
                return registro.nomePessoa;
            }

            this.pessoaFisicaDeselecionado = () => {
                this.pessoaFisica = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPessoaFisica = (termoDigitadoPesquisa) => {
                var filtro = new medicoDtos.Dtos.EZMedical.Medico.GetPessoaExceptForMedico();
                filtro.nomePessoa = termoDigitadoPesquisa;

                return filtro;
            }

            this.ezSelectPessoaFisica = new ezSelect.EzSelect.EzSelect<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pessoaFisicaService,
                this.getFiltroParaPaginacaoPessoaFisica,
                this.requestParamsPessoa,
                this.$uibModal,
                this.pessoaFisicaSelecionado,
                this.pessoaFisicaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPessoaFisica.onEzGridCreated = () => {

                this.ezSelectPessoaFisica.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.medicoService.getPagedExceptForId(filtro, requestParams);
                };

                this.ezSelectPessoaFisica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('PessoaFisica.Nome'),
                    field: 'nomePessoa'
                });

                this.ezSelectPessoaFisica.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.GrupoPessoa'),
                    field: 'grupoPessoa.descricao'
                });

                this.ezSelectPessoaFisica.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPessoaFisica.on.beforeOpenModalDialog = () => {
                this.ezSelectPessoaFisica.ezGrid.getRegistros();
            };
        }

        paramTokenize = () => {
            $('#especialidadesList').tokenize({
                searchMinLength: 3,
                valueField: "id",
                textField: "nome"
            });
        }

        formataEspecialidades = () => {
            if (this.entity.especialidades){
                var ids = new Array();
                this.entity.especialidades.forEach((item) => {
                    var id = item.id.toString();
                    $('#especialidadesList').tokenize().tokenAdd(id, item.nome);
                    ids[id] = id;
                });

                $('#especialidadesList').tokenize().remap();

                this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();
                var promise = this.especialidadeService.getPaged(new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput(), this.requestParamsEspecialidade);

                promise.then((result) => {
                    if (result.items.length > 0) {
                        for (var i = 0; i < result.items.length; i++) {
                            var out = result.items[i];

                            if (!ids[out.id]) {
                                this.especialidades.push({
                                    id: out.id,
                                    nome: out.nome
                                });
                            }
                        }
                    }
                });
            }
        }

        getEspecialidades = () => {
            this.requestParamsEspecialidade = new requestParam.RequestParam.RequestParams();

            var promise = this.especialidadeService.getPaged(new especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput(), this.requestParamsEspecialidade);

            promise.then((result) => {
                if (result.items.length > 0) {
                    for (var i = 0; i < result.items.length; i++) {
                        this.especialidades.push({
                            id: result.items[i].id,
                            nome: result.items[i].nome
                        });
                    }
                }
            });
        }

        ajustaEspecialidades = () => {
            this.entity.idsEspecialidades = [];
            var itens = $('#especialidadesList').next('div').find('.TokensContainer li.Token').toArray();
            itens.forEach((item) => {
                var id = isNaN($(item).data('value')) ? ($(item).data('value').indexOf(':') > -1 ? parseInt($(item).data('value').toString().split(':')[1]) : 0) : parseInt($(item).data('value'), 0);
                this.entity.idsEspecialidades.push(id);
            });
        }

        // Post explicando como acessar um form fora do seu scope
        // http://stackoverflow.com/questions/21574472/angularjs-cant-access-form-object-in-controller-scope/33418412#33418412
        // http://stackoverflow.com/questions/19568761/can-i-access-a-form-in-the-controller/22965461#22965461
        setFormScope(form: ng.IFormController) {
            this.medicoForm = form;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class MedicoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/medico/medicoForm.cshtml';
            this.controller = MedicoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}