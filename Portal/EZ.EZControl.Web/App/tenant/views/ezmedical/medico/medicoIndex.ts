﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as medicoService from "../../../../services/domain/ezmedical/medico/medicoService";
import * as medicoDtos from "../../../../dtos/ezmedical/medico/medicoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZMedical.Medico {

    class MedicoIndexController extends controllers.Controllers.ControllerIndexBase<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, medicoDtos.Dtos.EZMedical.Medico.MedicoInput>{

        static $inject = ['$state', 'uiGridConstants', 'medicoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public medicoService: medicoService.Services.MedicoService) {

            super(
                $state,
                uiGridConstants,
                medicoService, {
                    rotaAlterarRegistro: 'tenant.ezmedical.medico.alterar',
                    rotaNovoRegistro: 'tenant.ezmedical.medico.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new medicoDtos.Dtos.EZMedical.Medico.GetMedicoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.medicoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewMedico', this.novo, abp.auth.hasPermission('Pages.Tenant.EZMedical.Medico.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Medico.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZMedical.Medico.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('PessoaFisica.Nome'),
                field: 'pessoaFisica.nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Medico.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Medico.GrupoPessoa'),
                field: 'pessoaFisica.grupoPessoa.descricao'
            });
        }
    }

    export class MedicoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezmedical/medico/medicoIndex.cshtml';
            this.controller = MedicoIndexController;
        }
    }
}