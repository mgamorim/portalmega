﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as documentoAPagarService from "../../../../services/domain/financeiro/documento/documentoAPagarService";
import * as documentoAPagarDtos from "../../../../dtos/financeiro/documento/documentoAPagarDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Financeiro.DocumentoAPagar {

    class DocumentoAPagarIndexController extends controllers.Controllers.ControllerIndexBase<documentoAPagarDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarListDto, documentoAPagarDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarInput>{
        static $inject = ['$state', 'uiGridConstants', 'documentoAPagarService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<documentoAPagarDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;
        public estornar: (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => void;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public documentoAPagarService: documentoAPagarService.Services.DocumentoAPagarService) {

            super(
                $state,
                uiGridConstants,
                documentoAPagarService, {
                    rotaAlterarRegistro: 'tenant.financeiro.documentoAPagar.alterar',
                    rotaNovoRegistro: 'tenant.financeiro.documentoAPagar.novo'
                }
            );

            this.estornar = (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => {
                abp.message.confirm(app.localize("Documento.EstornoConfirmationMessage"), app.localize("Documento.EstornoConfirmationTitle"), (isConfirmed) => {
                    if (isConfirmed) {
                        this.$state.go('tenant.financeiro.documentoAPagar.estornar', { 'id': registro.id });
                    }
                });
            }
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new documentoAPagarDtos.Dtos.Financeiro.DocumentoAPagar.GetDocumentoAPagarInput();
                filtro.numero = termoDigitadoPesquisa;
                filtro.complemento = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<documentoAPagarDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.documentoAPagarService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDocumentoAPagar', this.novo, abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Delete');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.estornar, 'Estornar',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAPagar.Estornar');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity) && !row.entity.estornado && row.entity.valor > 0" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Numero'),
                field: 'numero'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Complemento'),
                field: 'complemento'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Fornecedor'),
                field: 'fornecedorNomePessoa'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Data'),
                field: 'data',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Valor'),
                field: 'valor',
                cellFilter: 'currency'
            });
            
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAPagar.Estornado'),
                field: 'estornado',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class DocumentoAPagarIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/financeiro/documentoAPagar/documentoAPagarIndex.cshtml';
            this.controller = DocumentoAPagarIndexController;
        }
    }
}