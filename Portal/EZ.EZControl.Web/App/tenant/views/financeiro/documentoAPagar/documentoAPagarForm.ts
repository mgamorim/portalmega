﻿import * as documentoService from "../../../../services/domain/financeiro/documento/documentoAPagarService";
import * as tipoDeDocumentoFinanceiroService from "../../../../services/domain/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroService";
import * as fornecedorService from "../../../../services/domain/global/fornecedor/fornecedorService";
import * as documentoDtos from "../../../../dtos/financeiro/documento/documentoAPagarDtos";
import * as tipoDeDocumentoDtos from "../../../../dtos/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroDtos";
import * as fornecedorDtos from "../../../../dtos/global/fornecedor/fornecedorDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class DocumentoAPagarFormController extends controllers.Controllers.ControllerFormComponentBase<documentoDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarListDto, documentoDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectFornecedor: ezSelect.EzSelect.EzSelect<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaFornecedor: (termoDigitadoPesquisa: string) => any;
        private fornecedorSelecionado: (registro: fornecedorDtos.Dtos.Fornecedor.FornecedorListDto) => string;
        private fornecedorDeselecionado: () => void;
        private requestParamsFornecedor: requestParam.RequestParam.RequestParams;

        public ezSelectTipoDeDocumentoFinanceiro: ezSelect.EzSelect.EzSelect<tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaTipoDeDocumentoFinanceiro: (termoDigitadoPesquisa: string) => any;
        private tipoDeDocumentoFinanceiroSelecionado: (registro: tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto) => string;
        private tipoDeDocumentoFinanceiroDeselecionado: () => void;
        private requestParamsTipoDeDocumentoFinanceiro: requestParam.RequestParam.RequestParams;

        private estornar: () => void;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'documentoAPagarService', 'fornecedorService', 'tipoDeDocumentoFinanceiroService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public documentoService: documentoService.Services.DocumentoAPagarService,
            public fornecedorService: fornecedorService.Services.FornecedorService,
            public tipoDeDocumentoFinanceiroService: tipoDeDocumentoFinanceiroService.Services.TipoDeDocumentoFinanceiroService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                documentoService,
                'tenant.financeiro.documentoAPagar');

            this.estornar = () => {
                var promise = this.documentoService.estornar(this.entity);
                promise.then(result => {
                    
                }).finally(() => {
                    abp.notify.info(app.localize('SavedSuccessfully'), "");
                    this.paginaDeListagem();
                });
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // documento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new documentoDtos.Dtos.Financeiro.DocumentoAPagar.DocumentoAPagarInput();
                instance.id = 0;
                instance.complemento = '';
                instance.estornado = false;
                return instance;
            };

            this.events.onGetEntity = (entity) => {

                this.ezSelectFornecedor.loading = true;
                var promiseFornecedor = this.fornecedorService.getById(entity.fornecedorId);
                promiseFornecedor.then(result => {
                    this.ezSelectFornecedor.setInputText(result.pessoa.tipoPessoa == TipoPessoaEnum.Fisica ? result.pessoaFisica.nome : result.pessoaJuridica.nomeFantasia);
                }).finally(() => {
                    this.ezSelectFornecedor.loading = false;
                });

                this.ezSelectTipoDeDocumentoFinanceiro.loading = true;
                var promiseTipoDeDocumento = this.tipoDeDocumentoFinanceiroService.getById(entity.tipoDeDocumentoFinanceiroId);
                promiseTipoDeDocumento.then(result => {
                    this.ezSelectTipoDeDocumentoFinanceiro.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectTipoDeDocumentoFinanceiro.loading = false;
                });

                this.entity.data = new Date(this.entity.data.toString());
                this.entity.competencia = new Date(this.entity.competencia.toString());
                this.entity.vencimento = new Date(this.entity.vencimento.toString());
                if (this.entity.dataEstorno != null) {
                    this.entity.dataEstorno = new Date(this.entity.dataEstorno.toString());
                }

                if (this.operation == 'estornar') {
                    this.entity.estornado = true;
                    this.entity.dataEstorno = new Date();
                }
            };

            this.ezSelectFornecedorConfig();

            this.ezSelectTipoDeDocumentoFinanceiroConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectFornecedorConfig() {
            this.requestParamsFornecedor = new requestParam.RequestParam.RequestParams();

            this.fornecedorSelecionado = (registro) => {
                this.entity.fornecedorId = registro.id;
                return registro.pessoa.tipoPessoa == TipoPessoaEnum.Fisica ? registro.pessoaFisica.nome : registro.pessoaJuridica.nomeFantasia;
            }

            this.fornecedorDeselecionado = () => {
                this.entity.fornecedorId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaFornecedor = (termoDigitadoPesquisa) => {
                var filtro = new fornecedorDtos.Dtos.Fornecedor.FornecedorInput();
                filtro.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput();
                filtro.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput();
                filtro.pessoaFisica.nome = termoDigitadoPesquisa;
                filtro.pessoaJuridica.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectFornecedor = new ezSelect.EzSelect.EzSelect<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.fornecedorService,
                this.getFiltroParaFornecedor,
                this.requestParamsFornecedor,
                this.$uibModal,
                this.fornecedorSelecionado,
                this.fornecedorDeselecionado);

            // Esse documento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse documento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectFornecedor.onEzGridCreated = () => {
                this.ezSelectFornecedor.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Fornecedor.NomePessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectFornecedor.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este documento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectFornecedor.on.beforeOpenModalDialog = () => {
                this.ezSelectFornecedor.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDeDocumentoFinanceiroConfig() {
            this.requestParamsTipoDeDocumentoFinanceiro = new requestParam.RequestParam.RequestParams();

            this.tipoDeDocumentoFinanceiroSelecionado = (registro) => {
                this.entity.tipoDeDocumentoFinanceiroId = registro.id;
                return registro.nome;
            }

            this.tipoDeDocumentoFinanceiroDeselecionado = () => {
                this.entity.tipoDeDocumentoFinanceiroId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaTipoDeDocumentoFinanceiro = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.GetTipoDeDocumentoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.tipo = TipoDeDocumentoFinanceiroEnum.APagar;
                return filtro;
            }

            this.ezSelectTipoDeDocumentoFinanceiro = new ezSelect.EzSelect.EzSelect<tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeDocumentoFinanceiroService,
                this.getFiltroParaTipoDeDocumentoFinanceiro,
                this.requestParamsTipoDeDocumentoFinanceiro,
                this.$uibModal,
                this.tipoDeDocumentoFinanceiroSelecionado,
                this.tipoDeDocumentoFinanceiroDeselecionado);

            // Esse documento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse documento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeDocumentoFinanceiro.onEzGridCreated = () => {
                this.ezSelectTipoDeDocumentoFinanceiro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeDocumentoFinanceiro.Nome'),
                    field: 'nome'
                });

                this.ezSelectTipoDeDocumentoFinanceiro.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este documento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectTipoDeDocumentoFinanceiro.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDeDocumentoFinanceiro.ezGrid.getRegistros();
            };
        }

        public setDates() {
            this.entity.competencia = this.entity.data;
            this.entity.vencimento = this.entity.data;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DocumentoAPagarFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/financeiro/documentoAPagar/documentoAPagarForm.cshtml';
            this.controller = DocumentoAPagarFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}