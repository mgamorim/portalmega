﻿import * as tipoDeDocumentoFinanceiroService from "../../../../services/domain/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroService";
import * as tipoDeDocumentoFinanceiroDtos from "../../../../dtos/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class TipoDeDocumentoFinanceiroFormController extends controllers.Controllers.ControllerFormComponentBase<tipoDeDocumentoFinanceiroDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto, tipoDeDocumentoFinanceiroDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public tipoDeDocumentoFinanceiroEnum = ez.domain.enum.tipoDeDocumentoFinanceiroEnum.valores;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'tipoDeDocumentoFinanceiroService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tipoDeDocumentoFinanceiroService: tipoDeDocumentoFinanceiroService.Services.TipoDeDocumentoFinanceiroService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                tipoDeDocumentoFinanceiroService,
                'tenant.financeiro.tipoDeDocumento');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new tipoDeDocumentoFinanceiroDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TipoDeDocumentoFinanceiroFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/financeiro/tipoDeDocumento/tipoDeDocumentoForm.cshtml';
            this.controller = TipoDeDocumentoFinanceiroFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}