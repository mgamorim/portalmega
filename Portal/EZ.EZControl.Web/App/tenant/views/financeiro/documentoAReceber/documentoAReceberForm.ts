﻿import * as documentoService from "../../../../services/domain/financeiro/documento/documentoAReceberService";
import * as tipoDeDocumentoFinanceiroService from "../../../../services/domain/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroService";
import * as clienteService from "../../../../services/domain/global/cliente/clienteService";
import * as documentoDtos from "../../../../dtos/financeiro/documento/documentoAReceberDtos";
import * as tipoDeDocumentoDtos from "../../../../dtos/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroDtos";
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class DocumentoAReceberFormController extends controllers.Controllers.ControllerFormComponentBase<documentoDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberListDto, documentoDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectCliente: ezSelect.EzSelect.EzSelect<clienteDtos.Dtos.Cliente.ClienteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaCliente: (termoDigitadoPesquisa: string) => any;
        private clienteSelecionado: (registro: clienteDtos.Dtos.Cliente.ClienteListDto) => string;
        private clienteDeselecionado: () => void;
        private requestParamsCliente: requestParam.RequestParam.RequestParams;

        public ezSelectTipoDeDocumentoFinanceiro: ezSelect.EzSelect.EzSelect<tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaTipoDeDocumentoFinanceiro: (termoDigitadoPesquisa: string) => any;
        private tipoDeDocumentoFinanceiroSelecionado: (registro: tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto) => string;
        private tipoDeDocumentoFinanceiroDeselecionado: () => void;
        private requestParamsTipoDeDocumentoFinanceiro: requestParam.RequestParam.RequestParams;

        private estornar: () => void;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'documentoAReceberService', 'clienteService', 'tipoDeDocumentoFinanceiroService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public documentoService: documentoService.Services.DocumentoAReceberService,
            public clienteService: clienteService.Services.ClienteService,
            public tipoDeDocumentoFinanceiroService: tipoDeDocumentoFinanceiroService.Services.TipoDeDocumentoFinanceiroService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                documentoService,
                'tenant.financeiro.documentoAReceber');

            this.estornar = () => {
                var promise = this.documentoService.estornar(this.entity);
                promise.then(result => {

                }).finally(() => {
                    abp.notify.info(app.localize('SavedSuccessfully'), "");
                    this.paginaDeListagem();
                });
            }
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // documento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new documentoDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput();
                instance.id = 0;
                instance.estornado = false;
                return instance;
            };

            this.events.onGetEntity = (entity) => {

                this.ezSelectCliente.loading = true;
                var promiseCliente = this.clienteService.getById(entity.clienteId);
                promiseCliente.then(result => {
                    this.ezSelectCliente.setInputText(result.pessoa.tipoPessoa == TipoPessoaEnum.Fisica ? result.pessoaFisica.nome : result.pessoaJuridica.nomeFantasia);
                }).finally(() => {
                    this.ezSelectCliente.loading = false;
                });

                this.ezSelectTipoDeDocumentoFinanceiro.loading = true;
                var promiseTipoDeDocumento = this.tipoDeDocumentoFinanceiroService.getById(entity.tipoDeDocumentoFinanceiroId);
                promiseTipoDeDocumento.then(result => {
                    this.ezSelectTipoDeDocumentoFinanceiro.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectTipoDeDocumentoFinanceiro.loading = false;
                });

                this.entity.data = new Date(this.entity.data.toString());
                this.entity.competencia = new Date(this.entity.competencia.toString());
                this.entity.vencimento = new Date(this.entity.vencimento.toString());
                if (this.entity.dataEstorno != null) {
                    this.entity.dataEstorno = new Date(this.entity.dataEstorno.toString());
                }

                if (this.operation == 'estornar') {
                    this.entity.estornado = true;
                    this.entity.dataEstorno = new Date();
                }
            };

            this.ezSelectClienteConfig();

            this.ezSelectTipoDeDocumentoFinanceiroConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectClienteConfig() {
            this.requestParamsCliente = new requestParam.RequestParam.RequestParams();

            this.clienteSelecionado = (registro) => {
                this.entity.clienteId = registro.id;
                return registro.pessoa.tipoPessoa == TipoPessoaEnum.Fisica ? registro.pessoaFisica.nome : registro.pessoaJuridica.nomeFantasia;
            }

            this.clienteDeselecionado = () => {
                this.entity.clienteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaCliente = (termoDigitadoPesquisa) => {
                var filtro = new clienteDtos.Dtos.Cliente.ClienteInput();
                filtro.pessoaFisica = new pessoaDtos.Dtos.Pessoa.PessoaFisicaInput();
                filtro.pessoaJuridica = new pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput();
                filtro.pessoaFisica.nome = termoDigitadoPesquisa;
                filtro.pessoaJuridica.nomeFantasia = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCliente = new ezSelect.EzSelect.EzSelect<clienteDtos.Dtos.Cliente.ClienteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.clienteService,
                this.getFiltroParaCliente,
                this.requestParamsCliente,
                this.$uibModal,
                this.clienteSelecionado,
                this.clienteDeselecionado);

            // Esse documento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse documento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCliente.onEzGridCreated = () => {
                this.ezSelectCliente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.NomePessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectCliente.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este documento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectCliente.on.beforeOpenModalDialog = () => {
                this.ezSelectCliente.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDeDocumentoFinanceiroConfig() {
            this.requestParamsTipoDeDocumentoFinanceiro = new requestParam.RequestParam.RequestParams();

            this.tipoDeDocumentoFinanceiroSelecionado = (registro) => {
                this.entity.tipoDeDocumentoFinanceiroId = registro.id;
                return registro.nome;
            }

            this.tipoDeDocumentoFinanceiroDeselecionado = () => {
                this.entity.tipoDeDocumentoFinanceiroId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaTipoDeDocumentoFinanceiro = (termoDigitadoPesquisa) => {
                var filtro = new tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.GetTipoDeDocumentoInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.tipo = TipoDeDocumentoFinanceiroEnum.AReceber;
                return filtro;
            }

            this.ezSelectTipoDeDocumentoFinanceiro = new ezSelect.EzSelect.EzSelect<tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tipoDeDocumentoFinanceiroService,
                this.getFiltroParaTipoDeDocumentoFinanceiro,
                this.requestParamsTipoDeDocumentoFinanceiro,
                this.$uibModal,
                this.tipoDeDocumentoFinanceiroSelecionado,
                this.tipoDeDocumentoFinanceiroDeselecionado);

            // Esse documento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse documento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDeDocumentoFinanceiro.onEzGridCreated = () => {
                this.ezSelectTipoDeDocumentoFinanceiro.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('TipoDeDocumentoFinanceiro.Nome'),
                    field: 'nome'
                });

                this.ezSelectTipoDeDocumentoFinanceiro.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este documento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectTipoDeDocumentoFinanceiro.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDeDocumentoFinanceiro.ezGrid.getRegistros();
            };
        }

        public setDates() {
            this.entity.competencia = this.entity.data;
            this.entity.vencimento = this.entity.data;
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class DocumentoAReceberFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/financeiro/documentoAReceber/documentoAReceberForm.cshtml';
            this.controller = DocumentoAReceberFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}