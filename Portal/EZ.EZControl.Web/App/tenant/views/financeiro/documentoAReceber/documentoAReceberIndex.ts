﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as documentoAReceberService from "../../../../services/domain/financeiro/documento/documentoAReceberService";
import * as documentoAReceberDtos from "../../../../dtos/financeiro/documento/documentoAReceberDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Financeiro.DocumentoAReceber {

    class DocumentoAReceberIndexController extends controllers.Controllers.ControllerIndexBase<documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberListDto, documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput>{
        static $inject = ['$state', 'uiGridConstants', 'documentoAReceberService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;
        public estornar: (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => void;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public documentoAReceberService: documentoAReceberService.Services.DocumentoAReceberService) {

            super(
                $state,
                uiGridConstants,
                documentoAReceberService, {
                    rotaAlterarRegistro: 'tenant.financeiro.documentoAReceber.alterar',
                    rotaNovoRegistro: 'tenant.financeiro.documentoAReceber.novo'
                }
            );

            this.estornar = (registro: applicationServiceDtos.Dtos.ApplicationService.IEzEntity) => {
                abp.message.confirm(app.localize("Documento.EstornoConfirmationMessage"), app.localize("Documento.EstornoConfirmationTitle"), (isConfirmed) => {
                    if (isConfirmed) {
                        this.$state.go('tenant.financeiro.documentoAReceber.estornar', { 'id': registro.id });
                    }
                });
            }
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.GetDocumentoAReceberInput();
                filtro.numero = termoDigitadoPesquisa;
                filtro.complemento = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.documentoAReceberService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewDocumentoAReceber', this.novo, abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Delete');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.estornar, 'Estornar',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Financeiro.DocumentoAReceber.Estornar');
                }
            ));
                

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity) && !row.entity.estornado && row.entity.valor > 0" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Numero'),
                field: 'numero'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Complemento'),
                field: 'complemento'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Cliente'),
                field: 'clienteNomePessoa'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Data'),
                field: 'data',
                cellFilter: 'date:"dd/MM/yyyy\"'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Valor'),
                field: 'valor',
                cellFilter: 'currency'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('DocumentoAReceber.Estornado'),
                field: 'estornado',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class DocumentoAReceberIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/financeiro/documentoAReceber/documentoAReceberIndex.cshtml';
            this.controller = DocumentoAReceberIndexController;
        }
    }
}