﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as pedidoService from "../../../../services/domain/vendas/pedido/pedidoService";
import * as pedidoDtos from "../../../../dtos/vendas/pedido/pedidoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Global.Pedido {

    class PedidoIndexController extends controllers.Controllers.ControllerIndexBase<pedidoDtos.Dtos.Pedido.PedidoListDto, pedidoDtos.Dtos.Pedido.PedidoInput>{

        static $inject = ['$state', 'uiGridConstants', 'pedidoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<pedidoDtos.Dtos.Pedido.PedidoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public pedidoService: pedidoService.Services.PedidoService) {

            super(
                $state,
                uiGridConstants,
                pedidoService, {
                    rotaAlterarRegistro: 'tenant.vendas.pedido.alterar',
                    rotaNovoRegistro: 'tenant.vendas.pedido.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new pedidoDtos.Dtos.Pedido.GetPedidoInput();
                filtro.nomeCliente = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<pedidoDtos.Dtos.Pedido.PedidoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.pedidoService,
                this.getFiltroParaPaginacao,
                this.requestParams);            

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewPedido', this.novo, abp.auth.hasPermission('Pages.Tenant.Vendas.Pedido.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Vendas.Pedido.Edit') && row.status === PedidoStatusEnum.Aberto;
                })
            );

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Pedido.CancelarPedido',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.Vendas.Pedido.Cancelar') && row.status === PedidoStatusEnum.Aberto;
                })
            );

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pedido.Id'),
                field: 'id',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pedido.Cliente'),
                field: 'nomeCliente'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pedido.Tipo'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "pedidoTipoEnum", "tipo")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pedido.Status'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "pedidoStatusEnum", "status")}}\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Pedido.Valor'),
                field: 'valor'
            });
        }
    }

    export class PedidoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/vendas/pedido/pedidoIndex.cshtml';
            this.controller = PedidoIndexController;
        }
    }
}