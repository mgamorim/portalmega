﻿import * as pedidoService from "../../../../services/domain/vendas/pedido/pedidoService";
import * as servicoService from "../../../../services/domain/servicos/servico/servicoService";
import * as clienteService from "../../../../services/domain/global/cliente/clienteService";
import * as produtoService from "../../../../services/domain/estoque/produto/produtoService";
import * as itemDePedidoService from "../../../../services/domain/vendas/itemDePedido/itemDePedidoService";
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as pedidoDtos from "../../../../dtos/vendas/pedido/pedidoDtos";
import * as itemDePedidoDtos from "../../../../dtos/vendas/itemDePedido/itemDePedidoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as produtoDtos from "../../../../dtos/estoque/produto/produtoDtos";
import * as servicoDtos from "../../../../dtos/servicos/servico/servicoDtos";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";

export module Forms {  

    class PedidoFormController extends controllers.Controllers.ControllerFormComponentBase<pedidoDtos.Dtos.Pedido.PedidoListDto, pedidoDtos.Dtos.Pedido.PedidoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        //ezSelectCliente
        public ezSelectCliente: ezSelect.EzSelect.EzSelect<clienteDtos.Dtos.Cliente.ClienteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCliente: (termoDigitadoPesquisa: string) => any;
        private clienteSelecionado: (registro: clienteDtos.Dtos.Cliente.ClienteListDto) => string;
        private clienteDeselecionado: () => void;
        private requestParamsCliente: requestParam.RequestParam.RequestParams;

        //ezSelectProduto
        public ezSelectProduto: ezSelect.EzSelect.EzSelect<produtoDtos.Dtos.Produto.ProdutoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoProduto: (termoDigitadoPesquisa: string) => any;
        private produtoSelecionado: (registro: produtoDtos.Dtos.Produto.ProdutoListDto) => string;
        private produtoDeselecionado: () => void;
        private requestParamsProduto: requestParam.RequestParam.RequestParams;

        //ezSelectServico
        public ezSelectServico: ezSelect.EzSelect.EzSelect<servicoDtos.Dtos.Servico.ServicoListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoServico: (termoDigitadoPesquisa: string) => any;
        private servicoSelecionado: (registro: servicoDtos.Dtos.Servico.ServicoListDto) => string;
        private servicoDeselecionado: () => void;
        private requestParamsServico: requestParam.RequestParam.RequestParams;

        // item de pedido                
        private requestParamsItemDePedido: requestParam.RequestParam.RequestParams;
        private itemDePedidoAppScopeProvider: ezGrid.EzGrid.EzGridAppScopeProvider<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>;
        private gridDataEnumerable: itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto[];
        public loadingItemDePedido: boolean;
        public itemDePedidoGridOptions: uiGrid.IGridOptionsOf<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>;
        public editarItem: (itemDePedido: itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto) => void;
        public excluirItem: (itemDePedido: itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto) => void;
        public limparItemDePedidoCurrent: () => void;
        public tipoDeItemDePedido: Array<EnumValue>;
        public itemDePedidoForm: ng.IFormController;
        public itemDePedidoCurrent: itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto;
        public editandoItem: boolean;

        //pedido
        public totalPedido: () => number;
        public tipoDePedido: Array<EnumValue>;
        public statusDoPedido: Array<EnumValue>;
        public pedidoForm: ng.IFormController;

        static $inject = ['$element', '$scope', '$state', '$stateParams', 'uiGridConstants', 'pedidoService', 'servicosServicoService', 'clienteService', 'estoqueProdutoService', 'itemDePedidoService', '$uibModal'];

        constructor(
            private $element: ng.IAugmentedJQuery,
            private $scope: ng.IScope,
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public pedidoService: pedidoService.Services.PedidoService,
            public servicoService: servicoService.Services.ServicoService,
            public clienteService: clienteService.Services.ClienteService,
            public produtoService: produtoService.Services.ProdutoService,
            public itemDePedidoService: itemDePedidoService.Services.ItemDePedidoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                pedidoService,
                'tenant.vendas.pedido');

            this.editandoItem = false;

            this.editarItem = (itemDePedido: itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto) => {
                this.editandoItem = true;
                this.itemDePedidoCurrent = angular.copy(itemDePedido);

                if (itemDePedido.tipoDeItemDePedido === TipoDeItemDePedidoEnum.Produto) {
                    this.ezSelectProduto.setInputText(itemDePedido.nome());
                } else if (itemDePedido.tipoDeItemDePedido === TipoDeItemDePedidoEnum.Servico) {
                    this.ezSelectServico.setInputText(itemDePedido.nome());
                }
            }

            this.excluirItem = (itemDePedido: itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto) => {
                abp.message.confirm(
                    '', 'Deseja realmente excluir este item?',
                    isConfirmed => {
                        if (isConfirmed) {
                            this.editandoItem = false;
                            var index = this.entity.items.indexOf(itemDePedido);
                            if (index > -1) {
                                ezFixes.uiGrid.fixGrid();
                                this.entity.items.splice(index, 1);
                            }
                        }
                    }
                );
            }

            this.limparItemDePedidoCurrent = () => {
                this.inicializarItemDePedidoCurrent();
                this.ezSelectProduto.gridSelectClear();
                this.ezSelectServico.gridSelectClear();
            }

            this.totalPedido = (): number => {
                if (this.gridDataEnumerable) {
                    var total = 0;
                    for (let i in this.gridDataEnumerable) {
                      let x = this.gridDataEnumerable[i];
                      total = total + (x.valor * x.quantidade);
                    }
                    return total;                  
                }

                return 0;
            }
        }

        $onInit() {
            super.init();
            
            this.statusDoPedido = PedidoStatusEnum.valores();
            this.tipoDePedido = PedidoTipoEnum.valores();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new pedidoDtos.Dtos.Pedido.PedidoInput();
                instance.id = 0;
                this.inicializarItemDePedidoCurrent();
                this.$scope.$watchCollection('entity.items', (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.ezSelectCliente.loading = true;
                var promise = this.clienteService.getById(entity.clienteId);
                promise.then(result => {
                    var nome = angular.isDefined(result.pessoaFisica)
                        ? result.pessoaFisica.nome
                        : result.pessoaJuridica.nomeFantasia;
                    this.ezSelectCliente.setInputText(nome);
                }).finally(() => {
                    this.ezSelectCliente.loading = false;
                });

                entity.items = itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto.cloneList(entity.items);
                this.$scope.$watchCollection(() => { return this.entity.items }, (newValues, oldValues) => {
                    this.gridDataEnumerable = this.entity.items;
                });
                this.itemDePedidoGridOptions.data = entity.items;
                this.itemDePedidoGridOptions.totalItems = entity.items.length;
                this.inicializarItemDePedidoCurrent();
            }

            this.ezSelectClienteConfig();
            this.ezSelectProdutoConfig();
            this.ezSelectServicoConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();

            this.itemDePedidoGridConfig();
        }

        setPedidoForm(form: ng.IFormController) {
            this.pedidoForm = form;
        }

        setItemDePedidoForm(form: ng.IFormController) {
            this.itemDePedidoForm = form;
        }

        tipoDeItemDePedidoChange() {
            this.itemDePedidoCurrent.produto = null;
            this.itemDePedidoCurrent.servico = null;
            this.itemDePedidoCurrent.valor = 0;
            this.itemDePedidoCurrent.quantidade = 0;
            this.ezSelectServico.gridSelectClear();
            this.ezSelectProduto.gridSelectClear();
        }

        getItemDePedidoByTempId(tempId: number): itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto {
            for (let i = 0; i < this.entity.items.length; i++) {
                if (this.entity.items[i].tempId === tempId) {
                    return this.entity.items[i];
                }
            }
        }

        cancelarItem() {
            this.limparItemDePedidoCurrent();
            this.editandoItem = false;
        }

        salvarItem() {
            if (!this.editandoItem) {
                this.entity.items.push(this.itemDePedidoCurrent);
            }
            else {
                var item = this.getItemDePedidoByTempId(this.itemDePedidoCurrent.tempId);
                angular.merge(item, this.itemDePedidoCurrent);
            }

            this.editandoItem = false;
            this.limparItemDePedidoCurrent();
            this.gridDataEnumerable = this.entity.items;
        }

        isQuantidadeValida(value: any): boolean {
            return value > 0;
        }

        private inicializarItemDePedidoCurrent() {
            this.itemDePedidoCurrent = new itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto();
            this.tipoDeItemDePedido = TipoDeItemDePedidoEnum.valores();
            this.itemDePedidoCurrent.tipoDeItemDePedido = TipoDeItemDePedidoEnum.getItemPorValor(1).valor;
            this.itemDePedidoCurrent.valor = 0;
            this.itemDePedidoCurrent.quantidade = 1;
            if (this.entity)
                this.itemDePedidoCurrent.pedidoId = this.entity.id;
        }

        private ezSelectClienteConfig() {
            this.requestParamsCliente = new requestParam.RequestParam.RequestParams();

            this.clienteSelecionado = (registro) => {
                this.entity.clienteId = registro.id;
                var nome = registro.pessoaFisica
                    ? registro.pessoaFisica.nome
                    : registro.pessoaJuridica
                        ? registro.pessoaJuridica.nomeFantasia
                        : '';

                return nome;
            }

            this.clienteDeselecionado = () => {
                this.entity.clienteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCliente = (termoDigitadoPesquisa) => {
                var filtro = new clienteDtos.Dtos.Cliente.GetClienteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCliente = new ezSelect.EzSelect.EzSelect<clienteDtos.Dtos.Cliente.ClienteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.clienteService,
                this.getFiltroParaPaginacaoCliente,
                this.requestParamsCliente,
                this.$uibModal,
                this.clienteSelecionado,
                this.clienteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCliente.onEzGridCreated = () => {
                this.ezSelectCliente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.NomePessoa'),
                    field: 'pessoa.nomePessoa'
                });

                this.ezSelectCliente.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Cliente.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectCliente.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCliente.on.beforeOpenModalDialog = () => {
                this.ezSelectCliente.ezGrid.getRegistros();
            };
        }

        private ezSelectProdutoConfig() {
            this.requestParamsProduto = new requestParam.RequestParam.RequestParams();

            this.produtoSelecionado = (registro) => {
                if (!this.itemDePedidoCurrent.produto) {
                    this.itemDePedidoCurrent.produto = new produtoDtos.Dtos.Produto.ProdutoListDto();
                }
                this.itemDePedidoCurrent.produto.id = registro.id;
                this.itemDePedidoCurrent.valor = registro.valor;
                this.itemDePedidoCurrent.produto.nome = registro.nome;
                return registro.nome;
            }

            this.produtoDeselecionado = () => {
                this.itemDePedidoCurrent.produto = null;
                this.itemDePedidoCurrent.valor = 0;
                this.itemDePedidoCurrent.quantidade = 0;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoProduto = (termoDigitadoPesquisa) => {
                var filtro = new produtoDtos.Dtos.Produto.GetProdutoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectProduto = new ezSelect.EzSelect.EzSelect<produtoDtos.Dtos.Produto.ProdutoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.produtoService,
                this.getFiltroParaPaginacaoProduto,
                this.requestParamsProduto,
                this.$uibModal,
                this.produtoSelecionado,
                this.produtoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectProduto.onEzGridCreated = () => {
                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Nome'),
                    field: 'nome'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectProduto.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Produto.Valor'),
                    field: 'valor'
                });

                this.ezSelectProduto.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectProduto.on.beforeOpenModalDialog = () => {
                this.ezSelectProduto.ezGrid.getRegistros();
            };
        }

        private ezSelectServicoConfig() {
            this.requestParamsServico = new requestParam.RequestParam.RequestParams();

            this.servicoSelecionado = (registro) => {
                if (!this.itemDePedidoCurrent.servico) {
                    this.itemDePedidoCurrent.servico = new servicoDtos.Dtos.Servico.ServicoListDto();
                }
                this.itemDePedidoCurrent.servico.id = registro.id;
                this.itemDePedidoCurrent.valor = registro.valor;
                this.itemDePedidoCurrent.servico.nome = registro.nome;
                return registro.nome;
            }

            this.servicoDeselecionado = () => {
                this.itemDePedidoCurrent.servico = null;
                this.itemDePedidoCurrent.valor = 0;
                this.itemDePedidoCurrent.quantidade = 0;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoServico = (termoDigitadoPesquisa) => {
                var filtro = new servicoDtos.Dtos.Servico.GetServicoInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectServico = new ezSelect.EzSelect.EzSelect<servicoDtos.Dtos.Servico.ServicoListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.servicoService,
                this.getFiltroParaPaginacaoServico,
                this.requestParamsServico,
                this.$uibModal,
                this.servicoSelecionado,
                this.servicoDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectServico.onEzGridCreated = () => {
                this.ezSelectServico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Servico.Nome'),
                    field: 'nome'
                });

                this.ezSelectServico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Servico.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectServico.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Servico.Valor'),
                    field: 'valor'
                });

                this.ezSelectServico.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectServico.on.beforeOpenModalDialog = () => {
                this.ezSelectServico.ezGrid.getRegistros();
            };
        }

        private itemDePedidoGridConfig() {
            this.requestParamsItemDePedido = new requestParam.RequestParam.RequestParams();
            this.requestParamsItemDePedido.sorting = 'Id';

            this.itemDePedidoAppScopeProvider = new ezGrid.EzGrid.EzGridAppScopeProvider<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>();
            this.itemDePedidoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editarItem, 'Edit', true));
            this.itemDePedidoAppScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluirItem, 'Delete', true));

            //callbacks
            var callBack: ezGrid.EzGrid.IEzGridAppScopeCallBack = {
                key: 'totalPedido',
                fn: this.totalPedido
            };
            this.itemDePedidoAppScopeProvider.addCallBack(callBack);

            this.itemDePedidoGridOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                showColumnFooter: true,
                rowHeight: 50,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.itemDePedidoAppScopeProvider
            }

            var lista = this.entity ? this.entity.items : new Array<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>();
            this.itemDePedidoGridOptions.data = lista;
            this.itemDePedidoGridOptions.totalItems = lista.length;

            this.itemDePedidoGridOptions.columnDefs.push({
                name: app.localize('ItemDePedidos.Descricao'),
                field: 'nome()',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.itemDePedidoGridOptions.columnDefs.push({
                name: app.localize('ItemDePedidos.TipoDeItem'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeItemDePedidoEnum", "tipoDeItemDePedido")}}\
                </div>'
            });

            this.itemDePedidoGridOptions.columnDefs.push({
                name: app.localize('ItemDePedidos.Valor'),
                field: 'valor'
            });

            this.itemDePedidoGridOptions.columnDefs.push({
                name: app.localize('ItemDePedidos.Quantidade'),
                field: 'quantidade'
            });

            this.itemDePedidoGridOptions.columnDefs.push({
                name: app.localize('ItemDePedidos.Total'),
                field: 'total()',
                footerCellTemplate: '<div class="ui-grid-cell-contents">Total: {{grid.appScope.executeCallBack("totalPedido")}}</div>'
            });

            ezFixes.uiGrid.uiTab.registerFix();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PedidoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/vendas/pedido/pedidoForm.cshtml';
            this.controller = PedidoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}