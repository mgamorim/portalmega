﻿import * as formaDePagamentoService from "../../../../services/domain/ezpag/formaDePagamento/formaDePagamentoService";
import * as formaDePagamentoDtos from "../../../../dtos/ezpag/formaDePagamento/formaDePagamentoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class FormaDePagamentoFormController extends
        controllers.Controllers.ControllerFormComponentBase<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto,
        formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoInput, applicationServiceDtos.Dtos.ApplicationService.IdInput> {

        public getFiltroParaPaginacaoFormaDePagamento: (termoDigitadoPesquisa: string) => any;
        private formaDePagamentoSelecionado: (registro: formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto) => string;
        private formaDePagamentoDeselecionado: () => void;
        private filterSelect: () => void;
        private requestParamsFormaDePagamento: requestParam.RequestParam.RequestParams;
        private tiposDePagamento: TipoCheckoutPagSeguroEnum[];

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'formaDePagamentoService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public formaDePagamentoService: formaDePagamentoService.Services.FormaDePagamentoService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                formaDePagamentoService,
                'tenant.ezpag.formaDePagamento');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            this.tiposDePagamento = ez.domain.enum.tipoCheckoutPagSeguroEnum.valores;
            this.tiposDePagamento = this.tiposDePagamento.filter(x => { if (x['valor'] != 2 && x['valor'] != 4) return true; });


            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class FormaDePagamentoFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezpag/formaDePagamento/formaDePagamentoForm.cshtml';
            this.controller = FormaDePagamentoFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}