﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as formaDePagamentoService from "../../../../services/domain/ezpag/formaDePagamento/formaDePagamentoService";
import * as formaDePagamentoDtos from "../../../../dtos/ezpag/formaDePagamento/formaDePagamentoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";


export module EzPag.FormaDePagamento {

    class FormaDePagamentoIndexController extends
        controllers.Controllers.ControllerIndexBase<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto,
        formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoInput> {

        static $inject = ['$state', 'uiGridConstants', 'formaDePagamentoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public formaDePagamentoService: formaDePagamentoService.Services.FormaDePagamentoService) {

            super(
                $state,
                uiGridConstants,
                formaDePagamentoService, {
                    rotaAlterarRegistro: 'tenant.ezpag.formaDePagamento.alterar',
                    rotaNovoRegistro: 'tenant.ezpag.formaDePagamento.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.GetFormaDePagamentoInput();
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.formaDePagamentoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewFormaDePagamento', this.novo, abp.auth.hasPermission('Pages.Tenant.EZPag.FormaDePagamento.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZPag.FormaDePagamento.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.EZPag.FormaDePagamento.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FormaDePagamento.Descricao'),
                field: 'descricao',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FormaDePagamento.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

           this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('FormaDePagamento.TipoDePagamento'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoCheckoutPagSeguroEnum", "tipoDePagamento")}}\
                </div>'
            });
        }
    }

    export class FormaDePagamentoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/ezpag/formaDePagamento/formaDePagamentoIndex.cshtml';
            this.controller = FormaDePagamentoIndexController;
        }
    }
}