﻿
import * as ezGrid from "../../../../common/ezgrid/ezgrid";
import * as transacaoService from "../../../../services/domain/ezpag/transacao/transacaoservice";
import * as transacaoDtos from "../../../../dtos/ezpag/transacao/transacaoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module EZPag.Transacao {

    class TransacaoIndexController extends controllers.Controllers.ControllerIndexBase<transacaoDtos.Dtos.EZPag.Transacao.TransacaoListDto, transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput>{

        static $inject = ['$state', 'uiGridConstants', 'transacaoService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<transacaoDtos.Dtos.EZPag.Transacao.TransacaoListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public transacaoService: transacaoService.Services.TransacaoService) {

            super(
                $state,
                uiGridConstants,
                transacaoService, {
                    rotaAlterarRegistro: 'tenant.ezpag.transacao.alterar',
                    rotaNovoRegistro: 'tenant.ezpag.transacao.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new transacaoDtos.Dtos.EZPag.Transacao.GetTransacaoInput();
                filtro.transactionId = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<transacaoDtos.Dtos.EZPag.Transacao.TransacaoListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.transacaoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            //this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
            //    (row) => {
            //        return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Edit');
            //    }));
            //this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
            //    (row) => {
            //        return abp.auth.hasPermission('Pages.Tenant.Estoque.Parametro.UnidadeMedida.Delete');
            //    }));
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EZPag.PedidoId'),
                field: 'pedidoId'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EZPag.TransactionId'),
                field: 'transactionId'
            });
           

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EZPag.Data'),
                cellFilter: 'date:\'dd/MM/yyyy HH:mm:ss\'',
                field: 'data'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('EZPag.StatusDoPagamento'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "statusDoPagamentoEnum", "statusDoPagamento")}}\
                </div>'
            });
        }
    }

    export class TransacaoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        
        constructor() {
            this.templateUrl = '~/app/tenant/views/ezpag/transacao/transacaoIndex.cshtml';
            this.controller = TransacaoIndexController;
        }
    }
}