﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as arquivoService from "../../../../services/domain/cms/arquivo/arquivoCmsService";
import * as arquivoDtos from "../../../../dtos/cms/arquivo/arquivoDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Cms.Arquivo {
    
    class ArquivoIndexController extends controllers.Controllers.ControllerIndexBase<arquivoDtos.Dtos.Arquivo.Cms.ArquivoCmsListDto, arquivoDtos.Dtos.Arquivo.Cms.ArquivoCmsInput>{

        static $inject = ['$state', 'uiGridConstants', 'arquivoCmsService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<arquivoDtos.Dtos.Arquivo.Cms.ArquivoCmsListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public arquivoService: arquivoService.Services.ArquivoCmsService) {

            super(
                $state,
                uiGridConstants,
                arquivoService, {
                    rotaAlterarRegistro: 'tenant.cms.arquivo.alterar',
                    rotaNovoRegistro: 'tenant.cms.arquivo.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new arquivoDtos.Dtos.Arquivo.Cms.GetArquivoCmsInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<arquivoDtos.Dtos.Arquivo.Cms.ArquivoCmsListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.arquivoService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewArquivo', this.novo, abp.auth.hasPermission('Pages.Tenant.CMS.Arquivo.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Arquivo.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Arquivo.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Arquivo.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Arquivo.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Arquivo.TipoDeArquivoFixo'),
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getEnumValue(row.entity, "tipoDeArquivoEnum", "tipoDeArquivoFixo")}}\
                </div>'
            });

        }
    }

    export class ArquivoIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/arquivo/arquivoIndex.cshtml';
            this.controller = ArquivoIndexController;
        }
    }
}