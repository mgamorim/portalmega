﻿import * as widgetService from "../../../../services/domain/cms/widget/widgetService";
import * as siteService from "../../../../services/domain/cms/site/siteService";
import * as templateService from "../../../../services/domain/cms/template/templateService";
import * as menuService from "../../../../services/domain/cms/menu/menuService";
import * as widgetDtos from "../../../../dtos/cms/widget/widgetDtos";
import * as templateDtos from "../../../../dtos/cms/template/templateDtos";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as menuDtos from "../../../../dtos/cms/menu/menuDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    import dtos = widgetDtos.Dtos.Cms.Widget;

    export class WidgetFormController extends controllers.Controllers.ControllerFormComponentBase<dtos.WidgetListDto, dtos.WidgetInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public getFiltroParaPaginacaoWidget: (termoDigitadoPesquisa: string) => any;
        private widgetSelecionado: (registro: dtos.WidgetListDto) => string;
        private widgetDeselecionado: () => void;
        private requestParamsWidget: requestParam.RequestParam.RequestParams;

        public ezSelectSite: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoSite: (termoDigitadoPesquisa: string) => any;
        private siteSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private siteDeselecionado: () => void;
        private requestParamsSite: requestParam.RequestParam.RequestParams;

        public ezSelectMenu: ezSelect.EzSelect.EzSelect<menuDtos.Dtos.Cms.Menu.MenuListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoMenu: (termoDigitadoPesquisa: string) => any;
        private menuSelecionado: (registro: menuDtos.Dtos.Cms.Menu.MenuListDto) => string;
        private menuDeselecionado: () => void;
        private requestParamsMenu: requestParam.RequestParam.RequestParams;

        public ezSelectTemplate: ezSelect.EzSelect.EzSelect<templateDtos.Dtos.Cms.Template.TemplateListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTemplate: (termoDigitadoPesquisa: string) => any;
        private templateSelecionado: (registro: templateDtos.Dtos.Cms.Template.TemplateListDto) => string;
        private templateDeselecionado: () => void;
        private requestParamsTemplate: requestParam.RequestParam.RequestParams;

        public sistemaEnum: any;
        public tipoWidgetEnum: any;

        public posicoes: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'widgetService', '$uibModal', 'siteService', 'templateService', 'menuService'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public widgetService: widgetService.Services.WidgetService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public siteService: siteService.Services.SiteService,
            public templateService: templateService.Services.TemplateService,
            public menuService: menuService.Services.MenuService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                widgetService,
                'tenant.cms.widget');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new dtos.WidgetInput();
                instance.id = 0;
                instance.isActive = true;
                instance.tipo = 0;
                return instance;
            };

            this.events.onGetEntity = (entity) => {

                this.ezSelectSite.loading = true;
                var promise = this.siteService.getById(entity.siteId);
                promise.then(result => {
                    this.ezSelectSite.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectSite.loading = false;
                });

                if (entity.tipo == 1)
                {
                    this.ezSelectMenu.loading = true;
                    var promise2 = this.menuService.getById(entity.menuId);
                    promise2.then(result => {
                        this.ezSelectMenu.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectMenu.loading = false;
                    });
                }

                this.ezSelectTemplate.loading = true;
                var promise3 = this.templateService.getById(entity.templateId);
                promise3.then(result => {
                    this.ezSelectTemplate.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectTemplate.loading = false;
                });

                this.getPosicoes(entity.templateId);
            };

            this.sistemaEnum = ez.domain.enum.sistemaEnum.valores;

            this.tipoWidgetEnum = ez.domain.enum.tipoDeWidgetEnum.valores;

            this.ezSelectSiteConfig();
            this.ezSelectMenuConfig();
            this.ezSelectTemplateConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private getPosicoes(templateId) {
            var promise = this.templateService.getListPosicoes(templateId);

            promise.then((result) => {
                var posicoes = result;
                this.posicoes = posicoes;
            });            
        }

        private ezSelectSiteConfig() {
            this.requestParamsSite = new requestParam.RequestParam.RequestParams();

            this.siteSelecionado = (registro) => {
                this.entity.siteId = registro.id;
                return registro.nome;
            }

            this.siteDeselecionado = () => {
                this.entity.siteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoSite = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.host = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSite = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoSite,
                this.requestParamsSite,
                this.$uibModal,
                this.siteSelecionado,
                this.siteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSite.onEzGridCreated = () => {
                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Nome'),
                    field: 'nome'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Host'),
                    field: 'site.host'
                });

                this.ezSelectSite.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSite.on.beforeOpenModalDialog = () => {
                this.ezSelectSite.ezGrid.getRegistros();
            };
        }

        private ezSelectMenuConfig() {
            this.requestParamsMenu = new requestParam.RequestParam.RequestParams();

            this.menuSelecionado = (registro) => {
                this.entity.menuId = registro.id;
                return registro.nome;
            }

            this.menuDeselecionado = () => {
                this.entity.menuId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoMenu = (termoDigitadoPesquisa) => {
                var filtro = new menuDtos.Dtos.Cms.Menu.GetMenuInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.siteId = this.entity.siteId;
                return filtro;
            }

            this.ezSelectMenu = new ezSelect.EzSelect.EzSelect<menuDtos.Dtos.Cms.Menu.MenuListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.menuService,
                this.getFiltroParaPaginacaoMenu,
                this.requestParamsMenu,
                this.$uibModal,
                this.menuSelecionado,
                this.menuDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectMenu.onEzGridCreated = () => {
                this.ezSelectMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Menu.Nome'),
                    field: 'nome'
                });

                this.ezSelectMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Menu.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectMenu.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectMenu.on.beforeOpenModalDialog = () => {
                this.ezSelectMenu.ezGrid.getRegistros();
            };
        }

        private ezSelectTemplateConfig() {
            this.requestParamsTemplate = new requestParam.RequestParam.RequestParams();
            
           this.templateSelecionado = (registro) => {
                this.entity.templateId = registro.id;
                this.getPosicoes(registro.id);
                return registro.nome;
            }
 
            this.templateDeselecionado = () => {
                this.entity.templateId = null;
                this.posicoes = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTemplate = (termoDigitadoPesquisa) => {
                var filtro = new templateDtos.Dtos.Cms.Template.GetTemplateInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTemplate = new ezSelect.EzSelect.EzSelect<templateDtos.Dtos.Cms.Template.TemplateListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.templateService,
                this.getFiltroParaPaginacaoTemplate,
                this.requestParamsTemplate,
                this.$uibModal,
                this.templateSelecionado,
                this.templateDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTemplate.onEzGridCreated = () => {
                this.ezSelectTemplate.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Template.Nome'),
                    field: 'nome'
                });

                this.ezSelectTemplate.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Template.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTemplate.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTemplate.on.beforeOpenModalDialog = () => {
                this.ezSelectTemplate.ezGrid.getRegistros();
            };
        }
            
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class WidgetFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/widget/widgetForm.cshtml';
            this.controller = WidgetFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}