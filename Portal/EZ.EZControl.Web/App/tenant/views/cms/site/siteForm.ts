﻿import * as siteService from "../../../../services/domain/cms/site/siteService";
import * as templateService from "../../../../services/domain/cms/template/templateService";
import * as hostService from "../../../../services/domain/cms/host/hostService";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as hostDtos from "../../../../dtos/cms/host/hostDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";
import * as templateDtos from "../../../../dtos/cms/template/templateDtos";
import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module Forms {
    export class SiteFormController extends controllers.Controllers.ControllerFormComponentBase<siteDtos.Dtos.CMS.Site.SiteListDto, siteDtos.Dtos.CMS.Site.SiteInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public tipoDePublicacaoEnum: any;
        public tipoDeConteudoRSSEnum: any;

        public newHostUrl: string;

        public ezSelectTemplate: ezSelect.EzSelect.EzSelect<templateDtos.Dtos.Cms.Template.TemplateListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTemplate: (termoDigitadoPesquisa: string) => any;
        private templateSelecionado: (registro: templateDtos.Dtos.Cms.Template.TemplateListDto) => string;
        private templateDeselecionado: () => void;
        private requestParamsTemplate: requestParam.RequestParam.RequestParams;

        public ezSelectHost: ezSelect.EzSelect.EzSelect<hostDtos.Dtos.CMS.Host.HostListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoHost: (termoDigitadoPesquisa: string) => any;
        private hostSelecionado: (registro: hostDtos.Dtos.CMS.Host.HostListDto) => string;
        private hostDeselecionado: () => void;

        public saving: boolean;
        public editing: boolean;
        public editar: (valor: hostDtos.Dtos.CMS.Host.HostListDto) => void;
        public excluir: (valor: hostDtos.Dtos.CMS.Host.HostListDto) => void;
        public limparEntidade: () => void;
        public addHost: () => void;
        public cancelEditHost: () => void;

        private setDataGridInput: (result: hostDtos.Dtos.CMS.Host.HostInput[]) => void;
        private gridApiInput: uiGrid.IGridApiOf<hostDtos.Dtos.CMS.Host.HostInput>;
        private requestParamsHostsInput: requestParam.RequestParam.RequestParams;
        private hostsAppScopeProviderInput: ezGrid.EzGrid.EzGridAppScopeProvider<hostDtos.Dtos.CMS.Host.HostInput>;
        public getHostsInput: () => void;
        public loadingHostsInput: boolean;
        public hostsGridOptionsInput: uiGrid.IGridOptionsOf<hostDtos.Dtos.CMS.Host.HostInput>;
        private gridDataInput: hostDtos.Dtos.CMS.Host.HostInput[];

        public hostCurrent: hostDtos.Dtos.CMS.Host.HostInput;

        public ezSelectTipoDePaginaParaPaginaDefault: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDePaginaParaPaginaDefault: (termoDigitadoPesquisa: string) => any;
        private tipoDePaginaParaPaginaDefaultSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private tipoDePaginaParaPaginaDefaultDeselecionado: () => void;
        private requestParamsTipoDePaginaParaPaginaDefault: requestParam.RequestParam.RequestParams;

        public ezSelectTipoDePaginaParaPostDefault: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDePaginaParaPostDefault: (termoDigitadoPesquisa: string) => any;
        private tipoDePaginaParaPostDefaultSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private tipoDePaginaParaPostDefaultDeselecionado: () => void;
        private requestParamsTipoDePaginaParaPostDefault: requestParam.RequestParam.RequestParams;

        public ezSelectTipoDePaginaParaCategoriaDefault: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDePaginaParaCategoriaDefault: (termoDigitadoPesquisa: string) => any;
        private tipoDePaginaParaCategoriaDefaultSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private tipoDePaginaParaCategoriaDefaultDeselecionado: () => void;
        private requestParamsTipoDePaginaParaCategoriaDefault: requestParam.RequestParam.RequestParams;

        public ezSelectTipoDePaginaParaPaginaInicialDefault: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDePaginaParaPaginaInicialDefault: (termoDigitadoPesquisa: string) => any;
        private tipoDePaginaParaPaginaInicialDefaultSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private tipoDePaginaParaPaginaInicialDefaultDeselecionado: () => void;
        private requestParamsTipoDePaginaParaPaginaInicialDefault: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'siteService', 'templateService', 'hostService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public siteService: siteService.Services.SiteService,
            public templateService: templateService.Services.TemplateService,
            public hostService: hostService.Services.HostService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                siteService,
                'tenant.cms.site');
        }

        $onInit() {
            super.init();

            this.tipoDePublicacaoEnum = ez.domain.enum.tipoDePublicacaoEnum.valores;
            this.tipoDeConteudoRSSEnum = ez.domain.enum.tipoDeConteudoRSSEnum.valores;

            this.getHostsInput = () => {
                this.loadingHostsInput = true;
                if (this.entity) {
                    this.setDataGridInput(this.entity.hosts);
                }

                this.loadingHostsInput = false;
            }

            this.setDataGridInput = (result: hostDtos.Dtos.CMS.Host.HostInput[]) => {
                this.gridDataInput.length = 0;

                for (var i = 0; i < result.length; i++) {
                    this.gridDataInput.push(result[i]);
                }

                this.hostsGridOptionsInput.totalItems = result.length;
            };

            this.cancelEditHost = () => {
                this.hostCurrent = new hostDtos.Dtos.CMS.Host.HostInput;
                this.hostCurrent.url = '';
            }

            this.editar = (valor: hostDtos.Dtos.CMS.Host.HostListDto) => {
                this.editing = true;
                this.hostCurrent = valor;
            }

            this.excluir = (valor: hostDtos.Dtos.CMS.Host.HostListDto) => {
                this.editing = false;
                abp.message.confirm(
                    'Deseja remover o host?', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            for (var i = 0; i < this.entity.hosts.length; i++) {
                                if (this.entity.hosts[i].url === valor.url) {
                                    this.entity.hosts.splice(i, 1);
                                    this.getHostsInput();
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                    break;
                                }
                            }
                        }
                    }
                );
            }

            this.limparEntidade = () => {
                this.hostCurrent = new hostDtos.Dtos.CMS.Host.HostInput();
            }

            this.addHost = () => {
                if (this.hostCurrent.url) {
                    if (this.editing) {
                        this.cancelEditHost();
                    } else {
                        this.hostCurrent.isActive = true;
                        this.hostCurrent.siteId = this.entity.id;
                        this.entity.hosts.push(this.hostCurrent);
                        this.getHostsInput();
                        this.limparEntidade();
                    }
                } else {
                    abp.notify.info('Preencha a URL do host!', '');
                }
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoHost = (termoDigitadoPesquisa) => {
                var filtro = new hostDtos.Dtos.CMS.Host.GetHostInput();
                filtro.url = termoDigitadoPesquisa;
                return filtro;
            }

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new siteDtos.Dtos.CMS.Site.SiteInput();
                instance.id = 0;
                instance.isActive = true;
                instance.hosts = new Array<hostDtos.Dtos.CMS.Host.HostInput>();
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                this.hostService.hosts = entity.hosts;

                for (var i = 0; i < this.entity.hosts.length; i++) {
                    if (this.entity.hosts[i].isPrincipal) {
                        this.entity.hostPrincipal = this.entity.hosts[i].url;
                        break;
                    }
                }

                // Template Default
                this.ezSelectTemplate.loading = true;
                var promiseTemplate = this.templateService.getById(entity.templateDefaultId);
                promiseTemplate.then(result => {
                    this.ezSelectTemplate.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectTemplate.loading = false;
                });

                //Tipos de Páginas
                //this.ezSelectTipoDePaginaParaPaginaDefault.loading = true;
                //this.ezSelectTipoDePaginaParaPostDefault.loading = true;
                //this.ezSelectTipoDePaginaParaPaginaInicialDefault.loading = true;
                //this.ezSelectTipoDePaginaParaCategoriaDefault.loading = true;

                var promiseSite = this.siteService.getById(entity.id);
                promiseSite.then(result => {
                    this.ezSelectTipoDePaginaParaPaginaDefault.setInputText(result.tipoDePaginaParaPaginaDefault);
                    this.ezSelectTipoDePaginaParaPostDefault.setInputText(result.tipoDePaginaParaPostDefault);
                    this.ezSelectTipoDePaginaParaPaginaInicialDefault.setInputText(result.tipoDePaginaParaPaginaInicialDefault);
                    this.ezSelectTipoDePaginaParaCategoriaDefault.setInputText(result.tipoDePaginaParaCategoriaDefault);
                });

                this.getHostsInput();
            }

            this.ezSelectTemplateConfig();
            this.ezSelectTipoDePaginaParaPaginaDefaultConfig();
            this.ezSelectTipoDePaginaParaCategoriaDefaultConfig();
            this.ezSelectTipoDePaginaParaPostDefaultConfig();
            this.ezSelectTipoDePaginaParaPaginaInicialDefaultConfig();
            this.hostGridConfigInput();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private hostGridConfigInput() {
            this.requestParamsHostsInput = new requestParam.RequestParam.RequestParams();

            this.hostsAppScopeProviderInput = new ezGrid.EzGrid.EzGridAppScopeProvider<hostDtos.Dtos.CMS.Host.HostInput>();

            this.hostsAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return true;
                }
            ));

            this.hostsAppScopeProviderInput.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return true;
                }
            ));

            this.gridDataInput = new Array<hostDtos.Dtos.CMS.Host.HostInput>();

            this.hostsGridOptionsInput = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.ALWAYS,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: true,
                rowHeight: 50,
                enableGridMenu: true,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                appScopeProvider: this.hostsAppScopeProviderInput,
                onRegisterApi: (api) => {
                    this.gridApiInput = api;
                    this.gridApiInput.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParamsHostsInput.sorting = null;
                        } else {
                            this.requestParamsHostsInput.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getHostsInput();
                    });
                    this.gridApiInput.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                        this.requestParamsHostsInput.skipCount = (pageNumber - 1) * pageSize;
                        this.requestParamsHostsInput.maxResultCount = pageSize;

                        this.getHostsInput();
                    });
                }
            }

            this.hostsGridOptionsInput.data = this.gridDataInput;


            this.hostsGridOptionsInput.columnDefs.push({
                name: app.localize('Host.URL'),
                field: 'url',
                cellTemplate: '\
                    <div class="ui-grid-cell-contents">\
                        {{grid.getCellValue(row, col)}}<br />\
                        <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                            <span>\
                                <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                            </span> \
                        </span>\
                    </div>'
            });

            this.hostsGridOptionsInput.columnDefs.push({
                name: app.localize('Host.Principal'),
                field: 'isPrincipal',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });


            this.getHostsInput();
        }

        private ezSelectTemplateConfig() {
            this.requestParamsTemplate = new requestParam.RequestParam.RequestParams();

            this.templateSelecionado = (registro) => {
                this.entity.templateDefaultId = registro.id;
                this.entity.tipoDePaginaParaPaginaDefault = registro.tipoDePaginaParaPaginaDefault;
                this.entity.tipoDePaginaParaCategoriaDefault = registro.tipoDePaginaParaCategoriaDefault;
                this.entity.tipoDePaginaParaPostDefault = registro.tipoDePaginaParaPostDefault;
                this.entity.tipoDePaginaParaPaginaInicialDefault = registro.tipoDePaginaParaPaginaInicialDefault;
                this.ezSelectTipoDePaginaParaPaginaDefault.setInputText(registro.tipoDePaginaParaPaginaDefault);
                this.ezSelectTipoDePaginaParaCategoriaDefault.setInputText(registro.tipoDePaginaParaCategoriaDefault);
                this.ezSelectTipoDePaginaParaPostDefault.setInputText(registro.tipoDePaginaParaPostDefault);
                this.ezSelectTipoDePaginaParaPaginaInicialDefault.setInputText(registro.tipoDePaginaParaPaginaInicialDefault);
                return registro.descricao;
            }

            this.templateDeselecionado = () => {
                this.entity.templateDefaultId = null;
                this.entity.tipoDePaginaParaPaginaDefault = null;
                this.entity.tipoDePaginaParaCategoriaDefault = null;
                this.entity.tipoDePaginaParaPostDefault = null;
                this.entity.tipoDePaginaParaPaginaInicialDefault = null;
                this.ezSelectTipoDePaginaParaPaginaDefault.setInputText('');
                this.ezSelectTipoDePaginaParaCategoriaDefault.setInputText('');
                this.ezSelectTipoDePaginaParaPostDefault.setInputText('');
                this.ezSelectTipoDePaginaParaPaginaInicialDefault.setInputText('');
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTemplate = (termoDigitadoPesquisa) => {
                var filtro = new templateDtos.Dtos.Cms.Template.GetTemplateInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTemplate = new ezSelect.EzSelect.EzSelect<templateDtos.Dtos.Cms.Template.TemplateListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.templateService,
                this.getFiltroParaPaginacaoTemplate,
                this.requestParamsTemplate,
                this.$uibModal,
                this.templateSelecionado,
                this.templateDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTemplate.onEzGridCreated = () => {

                this.ezSelectTemplate.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectTemplate.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectTemplate.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTemplate.on.beforeOpenModalDialog = () => {
                this.ezSelectTemplate.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDePaginaParaPaginaDefaultConfig() {
            this.requestParamsTemplate = new requestParam.RequestParam.RequestParams();

            this.tipoDePaginaParaPaginaDefaultSelecionado = (registro) => {
                this.entity.tipoDePaginaParaPaginaDefault = registro.valor;
                return registro.valor;
            }

            this.tipoDePaginaParaPaginaDefaultDeselecionado = () => {
                this.entity.tipoDePaginaParaPaginaDefault = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDePaginaParaPaginaDefault = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDePaginaParaPaginaDefault = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoTipoDePaginaParaPaginaDefault,
                this.requestParamsTipoDePaginaParaPaginaDefault,
                this.$uibModal,
                this.tipoDePaginaParaPaginaDefaultSelecionado,
                this.tipoDePaginaParaPaginaDefaultDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDePaginaParaPaginaDefault.onEzGridCreated = () => {

                this.ezSelectTipoDePaginaParaPaginaDefault.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.templateService.getListTiposDePaginas(this.entity.templateDefaultId);
                }

                this.ezSelectTipoDePaginaParaPaginaDefault.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.TipoDePaginaParaPaginaDefault'),
                    field: 'valor'
                });

                this.ezSelectTipoDePaginaParaPaginaDefault.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDePaginaParaPaginaDefault.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDePaginaParaPaginaDefault.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDePaginaParaCategoriaDefaultConfig() {
            this.requestParamsTemplate = new requestParam.RequestParam.RequestParams();

            this.tipoDePaginaParaCategoriaDefaultSelecionado = (registro) => {
                this.entity.tipoDePaginaParaCategoriaDefault = registro.valor;
                return registro.valor;
            }

            this.tipoDePaginaParaCategoriaDefaultDeselecionado = () => {
                this.entity.tipoDePaginaParaCategoriaDefault = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDePaginaParaCategoriaDefault = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDePaginaParaCategoriaDefault = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoTipoDePaginaParaCategoriaDefault,
                this.requestParamsTipoDePaginaParaCategoriaDefault,
                this.$uibModal,
                this.tipoDePaginaParaCategoriaDefaultSelecionado,
                this.tipoDePaginaParaCategoriaDefaultDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDePaginaParaCategoriaDefault.onEzGridCreated = () => {

                this.ezSelectTipoDePaginaParaCategoriaDefault.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.templateService.getListTiposDePaginas(this.entity.templateDefaultId);
                }

                this.ezSelectTipoDePaginaParaCategoriaDefault.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.TipoDePaginaParaCategoriaDefault'),
                    field: 'valor'
                });

                this.ezSelectTipoDePaginaParaCategoriaDefault.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDePaginaParaCategoriaDefault.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDePaginaParaCategoriaDefault.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDePaginaParaPostDefaultConfig() {
            this.requestParamsTemplate = new requestParam.RequestParam.RequestParams();

            this.tipoDePaginaParaPostDefaultSelecionado = (registro) => {
                this.entity.tipoDePaginaParaPostDefault = registro.valor;
                return registro.valor;
            }

            this.tipoDePaginaParaPostDefaultDeselecionado = () => {
                this.entity.tipoDePaginaParaPostDefault = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDePaginaParaPostDefault = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDePaginaParaPostDefault = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoTipoDePaginaParaPostDefault,
                this.requestParamsTipoDePaginaParaPostDefault,
                this.$uibModal,
                this.tipoDePaginaParaPostDefaultSelecionado,
                this.tipoDePaginaParaPostDefaultDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDePaginaParaPostDefault.onEzGridCreated = () => {

                this.ezSelectTipoDePaginaParaPostDefault.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.templateService.getListTiposDePaginas(this.entity.templateDefaultId);
                }

                this.ezSelectTipoDePaginaParaPostDefault.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.TipoDePaginaParaPostDefault'),
                    field: 'valor'
                });

                this.ezSelectTipoDePaginaParaPostDefault.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDePaginaParaPostDefault.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDePaginaParaPostDefault.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDePaginaParaPaginaInicialDefaultConfig() {
            this.requestParamsTemplate = new requestParam.RequestParam.RequestParams();

            this.tipoDePaginaParaPaginaInicialDefaultSelecionado = (registro) => {
                this.entity.tipoDePaginaParaPaginaInicialDefault = registro.valor;
                return registro.valor;
            }

            this.tipoDePaginaParaPaginaInicialDefaultDeselecionado = () => {
                this.entity.tipoDePaginaParaPaginaInicialDefault = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDePaginaParaPaginaInicialDefault = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDePaginaParaPaginaInicialDefault = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoTipoDePaginaParaPaginaInicialDefault,
                this.requestParamsTipoDePaginaParaPaginaInicialDefault,
                this.$uibModal,
                this.tipoDePaginaParaPaginaInicialDefaultSelecionado,
                this.tipoDePaginaParaPaginaInicialDefaultDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDePaginaParaPaginaInicialDefault.onEzGridCreated = () => {

                this.ezSelectTipoDePaginaParaPaginaInicialDefault.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.templateService.getListTiposDePaginas(this.entity.templateDefaultId);
                }

                this.ezSelectTipoDePaginaParaPaginaInicialDefault.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.TipoDePaginaParaPaginaInicialDefault'),
                    field: 'valor'
                });

                this.ezSelectTipoDePaginaParaPaginaInicialDefault.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDePaginaParaPaginaInicialDefault.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDePaginaParaPaginaInicialDefault.ezGrid.getRegistros();
            };
        }

        public ezSelectHostInitialize() {
            this.ezSelectHost.setInputText(this.entity.hostPrincipal);
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class SiteFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/site/siteForm.cshtml';
            this.controller = SiteFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}