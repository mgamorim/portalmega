﻿import * as tagService from "../../../../services/domain/cms/tag/tagService";
import * as tagDtos from "../../../../dtos/cms/tag/tagDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    import dtos = tagDtos.Dtos.Cms.Tag;

    export class TagFormController extends controllers.Controllers.ControllerFormComponentBase<dtos.TagListDto, dtos.TagInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectTag: ezSelect.EzSelect.EzSelect<dtos.TagListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTag: (termoDigitadoPesquisa: string) => any;
        private tagSelecionado: (registro: dtos.TagListDto) => string;
        private tagDeselecionado: () => void;
        private requestParamsTag: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'tagService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tagService: tagService.Services.TagService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                tagService,
                'tenant.cms.tag');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new dtos.TagInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        nomeNgChange = (entity: dtos.TagInput) => {

            this.entity.slug = (entity.nome as any).slugify(); // Cast pra any para usar slugify que é extendido pelo javascript
        };
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class TagFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/tag/tagForm.cshtml';
            this.controller = TagFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}