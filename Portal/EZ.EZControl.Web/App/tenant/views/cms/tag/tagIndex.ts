﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as tagService from "../../../../services/domain/cms/tag/tagService";
import * as tagDtos from "../../../../dtos/cms/tag/tagDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module CMS.Tag {
    import dtos = tagDtos.Dtos.Cms.Tag;

    class TagIndexController extends controllers.Controllers.ControllerIndexBase<dtos.TagListDto, dtos.TagInput>{

        static $inject = ['$state', 'uiGridConstants', 'tagService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<dtos.TagListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public tagService: tagService.Services.TagService) {

            super(
                $state,
                uiGridConstants,
                tagService, {
                    rotaAlterarRegistro: 'tenant.cms.tag.alterar',
                    rotaNovoRegistro: 'tenant.cms.tag.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new dtos.GetTagInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<dtos.TagListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.tagService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewTag', this.novo, abp.auth.hasPermission('Pages.Tenant.CMS.Tag.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Tag.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Tag.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Tag.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Tag.Descricao'),
                field: 'descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Tag.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class TagIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/tag/tagIndex.cshtml';
            this.controller = TagIndexController;
        }
    }
}