﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as categoriaService from "../../../../services/domain/cms/categoria/categoriaService";
import * as categoriaDtos from "../../../../dtos/cms/categoria/categoriaDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module CMS.Categoria {
    import dtos = categoriaDtos.Dtos.Cms.Categoria;

    class CategoriaIndexController extends controllers.Controllers.ControllerIndexBase<dtos.CategoriaListDto, dtos.CategoriaInput>{

        static $inject = ['$state', 'uiGridConstants', 'categoriaService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<dtos.CategoriaListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public categoriaService: categoriaService.Services.CategoriaService) {

            super(
                $state,
                uiGridConstants,
                categoriaService, {
                    rotaAlterarRegistro: 'tenant.cms.categoria.alterar',
                    rotaNovoRegistro: 'tenant.cms.categoria.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new dtos.GetCategoriaInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.nomeSite = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<dtos.CategoriaListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.categoriaService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewCategoria', this.novo, abp.auth.hasPermission('Pages.Tenant.CMS.Categoria.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Categoria.Edit');
                }
            ));

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Categoria.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Categoria.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Categoria.Descricao'),
                field: 'descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Categoria.Site'),
                field: 'site.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Categoria.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class CategoriaIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/categoria/categoriaIndex.cshtml';
            this.controller = CategoriaIndexController;
        }
    }
}