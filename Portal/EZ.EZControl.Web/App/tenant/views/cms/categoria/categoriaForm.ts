﻿import * as categoriaService from "../../../../services/domain/cms/categoria/categoriaService";
import * as linkService from "../../../../services/domain/cms/link/linkService";
import * as categoriaDtos from "../../../../dtos/cms/categoria/categoriaDtos";
import * as linkDtos from "../../../../dtos/cms/link/linkDtos";
import * as siteService from "../../../../services/domain/cms/site/siteService";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    import dtos = categoriaDtos.Dtos.Cms.Categoria;

    export class CategoriaFormController extends controllers.Controllers.ControllerFormComponentBase<dtos.CategoriaListDto, dtos.CategoriaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectCategoria: ezSelect.EzSelect.EzSelect<dtos.CategoriaListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectSite: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoCategoria: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoSite: (termoDigitadoPesquisa: string) => any;
        private categoriaSelecionado: (registro: dtos.CategoriaListDto) => string;
        private categoriaDeselecionado: () => void;
        private siteSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private siteDeselecionado: () => void;
        private requestParamsCategoria: requestParam.RequestParam.RequestParams;
        private requestParamsSite: requestParam.RequestParam.RequestParams;

        public link: string;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'categoriaService', 'siteService', 'linkService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public categoriaService: categoriaService.Services.CategoriaService,
            public siteService: siteService.Services.SiteService,
            public linkService: linkService.Services.LinkService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                categoriaService,
                'tenant.cms.categoria');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new dtos.CategoriaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.siteId = 0;
                return instance;
            };

            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                if (entity.categoriaPaiId) {
                    this.ezSelectCategoria.loading = true;
                    var promise = this.categoriaService.getById(entity.categoriaPaiId);
                    promise.then(result => {
                        this.ezSelectCategoria.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectCategoria.loading = false;
                    });
                }

                if (entity.siteId) {
                    this.ezSelectSite.loading = true;
                    var promiseSite = this.siteService.getById(entity.siteId);
                    promiseSite.then(result => {
                        this.ezSelectSite.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectSite.loading = false;
                    });
                }

                this.getLink(entity);
            };

            this.ezSelectCategoriaConfig();
            this.ezSelectSiteConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        nomeNgChange = (entity: dtos.CategoriaInput) => {
            this.entity.slug = (entity.nome as any).slugify(); // Cast pra any para usar slugify que é extendido pelo javascript
            this.getLink(entity);
        };

        getLink = (entity: dtos.CategoriaInput) => {
            var input = new linkDtos.Dtos.Cms.Link.LinkInput();
            input.categoriaPaiId = this.entity.categoriaPaiId;
            input.conteudoId = this.entity.id;
            input.siteId = this.entity.siteId;
            input.slug = this.entity.slug;
            input.tipoDeLink = ez.domain.enum.tipoDeLinkEnum.categoria;
            input.titulo = this.entity.nome;
            var promise = this.linkService.getLink(input);
            promise.then((result) => {
                this.link = result.link;
            });
        }

        private ezSelectCategoriaConfig() {
            this.requestParamsCategoria = new requestParam.RequestParam.RequestParams();

            this.categoriaSelecionado = (registro) => {
                this.entity.categoriaPaiId = registro.id;
                if (this.entity.nome) this.getLink(this.entity);
                return registro.nome;
            }

            this.categoriaDeselecionado = () => {
                this.entity.categoriaPaiId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCategoria = (termoDigitadoPesquisa) => {
                var filtro = new dtos.GetCategoriaExceptForIdInput();
                filtro.categoriaId = this.entity.id;
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCategoria = new ezSelect.EzSelect.EzSelect<dtos.CategoriaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.categoriaService,
                this.getFiltroParaPaginacaoCategoria,
                this.requestParamsCategoria,
                this.$uibModal,
                this.categoriaSelecionado,
                this.categoriaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCategoria.onEzGridCreated = () => {
                this.ezSelectCategoria.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.categoriaService.getCategoriasExceptForId(filtro, requestParams);
                };

                this.ezSelectCategoria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Nome'),
                    field: 'nome'
                });

                this.ezSelectCategoria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectCategoria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectCategoria.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCategoria.on.beforeOpenModalDialog = () => {
                this.ezSelectCategoria.ezGrid.getRegistros();
            };
        }

        private ezSelectSiteConfig() {
            this.requestParamsSite = new requestParam.RequestParam.RequestParams();

            this.siteSelecionado = (registro) => {
                this.entity.siteId = registro.id;
                return registro.nome;
            }

            this.siteDeselecionado = () => {
                this.entity.siteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoSite = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.host = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSite = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoSite,
                this.requestParamsSite,
                this.$uibModal,
                this.siteSelecionado,
                this.siteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSite.onEzGridCreated = () => {
                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Nome'),
                    field: 'nome'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Host'),
                    field: 'host'
                });

                this.ezSelectSite.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSite.on.beforeOpenModalDialog = () => {
                this.ezSelectSite.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class CategoriaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/categoria/categoriaForm.cshtml';
            this.controller = CategoriaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}