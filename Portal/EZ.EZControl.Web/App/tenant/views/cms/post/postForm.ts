﻿import * as postService from "../../../../services/domain/cms/post/postService";
import * as siteService from "../../../../services/domain/cms/site/siteService";
import * as linkService from "../../../../services/domain/cms/link/linkService";
import * as categoriaService from "../../../../services/domain/cms/categoria/categoriaService";
import * as tagService from "../../../../services/domain/cms/tag/tagService";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as linkDtos from "../../../../dtos/cms/link/linkDtos";
import * as publicacaoDtos from "../../../../dtos/cms/publicacao/publicacaoDtos";
import * as categoriaDtos from "../../../../dtos/cms/categoria/categoriaDtos";
import * as tagDtos from "../../../../dtos/cms/tag/tagDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    import dtos = publicacaoDtos.Dtos.Cms.Publicacao;

    export class PostFormController extends controllers.Controllers.ControllerFormComponentBase<dtos.PostListDto, dtos.PostInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectPost: ezSelect.EzSelect.EzSelect<dtos.PostListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPostcaoPublicacao: (termoDigitadoPesquisa: string) => any;
        private postSelecionado: (registro: dtos.PostListDto) => string;
        private postDeselecionado: () => void;
        private requestParamsPost: requestParam.RequestParam.RequestParams;

        public ezSelectSite: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPostcaoSite: (termoDigitadoPesquisa: string) => any;
        private siteSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private siteDeselecionado: () => void;
        private requestParamsSite: requestParam.RequestParam.RequestParams;

        public statusDaPublicacao = ez.domain.enum.statusDaPublicacaoEnum.valores;

        public categorias = [];
        public requestParamsCategoria: requestParam.RequestParam.RequestParams;
        public categoriasDoPost = [];

        public tags = [];
        public requestParamsTag: requestParam.RequestParam.RequestParams;
        public tagsDoPost = [];

        public linkInput: linkDtos.Dtos.Cms.Link.LinkInput;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'postService', 'siteService', 'linkService', 'categoriaService', 'tagService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public postService: postService.Services.PostService,
            public siteService: siteService.Services.SiteService,
            public linkService: linkService.Services.LinkService,
            public categoriaService: categoriaService.Services.CategoriaService,
            public tagService: tagService.Services.TagService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                postService,
                'tenant.cms.post');
        }

        $onInit() {
            super.init();

            this.getCategorias();
            this.getTags();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new dtos.PostInput();
                instance.id = 0;
                instance.isActive = true;
                instance.dataDePublicacao = new Date();
                instance.statusDaPublicacaoEnum = ez.domain.enum.statusDaPublicacaoEnum.rascunho;
                instance.categorias = [];
                instance.tags = [];
                instance.siteId = 0;
                return instance;
            };

            this.events.onGetEntity = (entity) => {

                this.ezSelectSite.loading = true;
                var promise = this.siteService.getById(entity.siteId);
                promise.then(result => {
                    this.ezSelectSite.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectSite.loading = false;
                });

                this.entity.categorias = entity.categorias;
                this.entity.tags = entity.tags;

                this.entity.dataDePublicacao = new Date();

                this.tituloNgChange(entity);
                this.getLink(entity);

                this.formataCategoriasETags();
            }

            this.events.onBeforeSaveEntity = () => {
                this.ajustaCategorias();
                this.ajustaTags();
            }

            this.ezSelectSiteConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectSiteConfig() {
            this.requestParamsSite = new requestParam.RequestParam.RequestParams();

            this.siteSelecionado = (registro) => {
                this.entity.siteId = registro.id;
                return registro.nome;
            }

            this.siteDeselecionado = () => {
                this.entity.siteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPostcaoSite = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.host = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSite = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPostcaoSite,
                this.requestParamsSite,
                this.$uibModal,
                this.siteSelecionado,
                this.siteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSite.onEzGridCreated = () => {
                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Nome'),
                    field: 'nome'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Host'),
                    field: 'site.host'
                });

                this.ezSelectSite.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma postda no ezGrid
            this.ezSelectSite.on.beforeOpenModalDialog = () => {
                this.ezSelectSite.ezGrid.getRegistros();
            };
        }

        formataCategoriasETags = () => {
            this.categoriasDoPost = this.entity.categorias;
            this.tagsDoPost = this.entity.tags;
            this.entity.categorias = [];
            this.entity.tags = [];
            this.categoriasDoPost.forEach((item) => {
                $('#categoriasList').tokenize().tokenAdd(item.id, item.nome);
            });
            $('#categoriasList').tokenize().remap();
            this.tagsDoPost.forEach((item) => {
                $('#tagsList').tokenize().tokenAdd(item.id, item.nome);
            });
            $('#tagsList').tokenize().remap();
        }

        getCategorias = () => {
            this.requestParamsCategoria = new requestParam.RequestParam.RequestParams();
            var promise = this.categoriaService.getPaged(new categoriaDtos.Dtos.Cms.Categoria.GetCategoriaInput(), this.requestParamsCategoria);
            promise.then((result) => {
                if (result.items.length > 0) {
                    for (var i = 0; i < result.items.length; i++) {
                        this.categorias.push({
                            id: result.items[i].id,
                            nome: result.items[i].nome
                        });
                    }
                }
            }).finally(() => {
                $('#categoriasList').tokenize({
                    searchMinLength: 3,
                    valueField: "id",
                    textField: "nome",
                    onAddToken: (value, text) => {
                        var cats = $('#categoriasList').next('div').find('.TokensContainer li.Token').toArray();
                        if (cats.length == 1) {
                            var cat = new categoriaDtos.Dtos.Cms.Categoria.CategoriaInput();
                            cat.id = isNaN(parseInt(value)) ? (value.toString().indexOf(':') > -1 ? parseInt(value.toString().split(':')[1]) : 0) : parseInt(value);
                            cat.nome = text;
                            this.getLink(this.entity, cat);
                        }
                    },
                    onRemoveToken: (value, text) => {
                        var cats = $('#categoriasList').next('div').find('.TokensContainer li.Token').toArray();
                        if (cats.length == 0) {
                            this.getLink(this.entity, null);
                        }
                    }
                });
            });
        };

        getTags = () => {
            this.requestParamsTag = new requestParam.RequestParam.RequestParams();
            var promise = this.tagService.getPaged(new tagDtos.Dtos.Cms.Tag.GetTagInput(), this.requestParamsTag);
            promise.then((result) => {
                if (result.items.length > 0) {
                    for (var i = 0; i < result.items.length; i++) {
                        this.tags.push({
                            id: result.items[i].id,
                            nome: result.items[i].nome
                        });
                    }
                }
            }).finally(() => {
                $('#tagsList').tokenize({
                    searchMinLength: 3,
                    valueField: "id",
                    textField: "nome"
                });
            });
        };

        tituloNgChange = (entity: dtos.PostInput) => {
            this.entity.slug = (entity.titulo as any).slugify(); // Cast pra any para usar slugify que é extendido pelo javascript
        };

        getLink = (entity: dtos.PostInput, categoria?: categoriaDtos.Dtos.Cms.Categoria.CategoriaInput) => {
            var promise = this.linkService.getLink({
                conteudoId: this.entity.id,
                siteId: this.entity.siteId,
                tipoDeLink: ez.domain.enum.tipoDeLinkEnum.post,
                slug: this.entity.slug,
                titulo: this.entity.titulo,
                categoriaPaiId: null,
                link: "",
                primeiraCategoria: categoria
            });
            promise.then((result) => {
                this.entity.link = result.link;
            });
        };

        ajustaCategorias = () => {
            this.entity.categorias = [];
            var itens = $('#categoriasList').next('div').find('.TokensContainer li.Token').toArray();
            itens.forEach((item) => {
                var categoria = new categoriaDtos.Dtos.Cms.Categoria.CategoriaInput();
                categoria.id = isNaN($(item).data('value')) ? ($(item).data('value').indexOf(':') > -1 ? parseInt($(item).data('value').toString().split(':')[1]) : 0) : parseInt($(item).data('value'), 0);
                categoria.nome = $(item).find('span').text();
                categoria.slug = $(item).find('span').text().slugify();
                categoria.siteId = this.entity.siteId;
                categoria.isActive = true;
                this.entity.categorias.push(categoria);
            });
        }

        ajustaTags = () => {
            this.entity.tags = [];
            var itens = $('#tagsList').next('div').find('.TokensContainer li.Token').toArray();
            itens.forEach((item) => {
                var tag = new tagDtos.Dtos.Cms.Tag.TagInput();
                tag.id = isNaN($(item).data('value')) ? ($(item).data('value').indexOf(':') > -1 ? parseInt($(item).data('value').toString().split(':')[1]) : 0) : parseInt($(item).data('value'), 0);
                tag.nome = $(item).find('span').text();
                tag.slug = $(item).find('span').text().slugify();
                tag.isActive = true;
                this.entity.tags.push(tag);
            });
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PostFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/post/postForm.cshtml';
            this.controller = PostFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}