﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as itemDeMenuService from "../../../../services/domain/cms/itemDeMenu/itemDeMenuService";
import * as itemDeMenuDtos from "../../../../dtos/cms/itemDeMenu/itemDeMenuDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module CMS.ItemDeMenu {
    class ItemDeMenuIndexController extends controllers.Controllers.ControllerIndexBase<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto, itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuInput>{
        static $inject = ['$state', 'uiGridConstants', 'itemDeMenuService'];

        public ezGrid: ezGrid.EzGrid.EzGrid<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public itemDeMenuService: itemDeMenuService.Services.ItemDeMenuService) {
            super(
                $state,
                uiGridConstants,
                itemDeMenuService, {
                    rotaAlterarRegistro: 'tenant.cms.itemDeMenu.alterar',
                    rotaNovoRegistro: 'tenant.cms.itemDeMenu.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new itemDeMenuDtos.Dtos.Cms.ItemDeMenu.GetItemDeMenuInput();
                filtro.titulo = termoDigitadoPesquisa;
                filtro.descricaoDoTitulo = termoDigitadoPesquisa;
                filtro.nomeMenu = termoDigitadoPesquisa;
                filtro.url = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.itemDeMenuService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewItemDeMenu', this.novo, abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ItemDeMenu.Titulo'),
                field: 'titulo',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ItemDeMenu.DescricaoDoTitulo'),
                field: 'descricaoDoTitulo'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ItemDeMenu.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ItemDeMenu.Menu'),
                field: 'menu.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('ItemDeMenu.Url'),
                field: 'url'
            });
        }
    }

    export class ItemDeMenuIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/itemDeMenu/itemDeMenuIndex.cshtml';
            this.controller = ItemDeMenuIndexController;
        }
    }
}