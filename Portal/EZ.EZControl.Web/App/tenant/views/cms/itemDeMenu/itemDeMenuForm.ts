﻿import * as itemDeMenuService from "../../../../services/domain/cms/itemDeMenu/itemDeMenuService";
import * as itemDeMenuDtos from "../../../../dtos/cms/itemDeMenu/itemDeMenuDtos";
import * as menuService from "../../../../services/domain/cms/menu/menuService";
import * as menuDtos from "../../../../dtos/cms/menu/menuDtos";
import * as categoriaService from "../../../../services/domain/cms/categoria/categoriaService";
import * as categoriaDtos from "../../../../dtos/cms/categoria/categoriaDtos";
import * as paginaService from "../../../../services/domain/cms/pagina/paginaService";
import * as paginaDtos from "../../../../dtos/cms/publicacao/publicacaoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class ItemDeMenuFormController extends controllers.Controllers.ControllerFormComponentBase<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto, itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuInput, applicationServiceDtos.Dtos.ApplicationService.IdInput> {
        public ezSelectItemDeMenu: ezSelect.EzSelect.EzSelect<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectMenu: ezSelect.EzSelect.EzSelect<menuDtos.Dtos.Cms.Menu.MenuListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectCategoria: ezSelect.EzSelect.EzSelect<categoriaDtos.Dtos.Cms.Categoria.CategoriaListDto, requestParam.RequestParam.RequestParams>;
        public ezSelectPagina: ezSelect.EzSelect.EzSelect<paginaDtos.Dtos.Cms.Publicacao.PaginaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoItemDeMenu: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoMenu: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoCategoria: (termoDigitadoPesquisa: string) => any;
        public getFiltroParaPaginacaoPagina: (termoDigitadoPesquisa: string) => any;
        private itemDeMenuSelecionado: (registro: itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto) => string;
        private itemDeMenuDeselecionado: () => void;
        private menuSelecionado: (registro: menuDtos.Dtos.Cms.Menu.MenuListDto) => string;
        private menuDeselecionado: () => void;
        private categoriaSelecionado: (registro: categoriaDtos.Dtos.Cms.Categoria.CategoriaListDto) => string;
        private categoriaDeselecionado: () => void;
        private paginaSelecionado: (registro: paginaDtos.Dtos.Cms.Publicacao.PaginaListDto) => string;
        private paginaDeselecionado: () => void;
        private requestParamsItemDeMenu: requestParam.RequestParam.RequestParams;
        private requestParamsMenu: requestParam.RequestParam.RequestParams;
        private requestParamsCategoria: requestParam.RequestParam.RequestParams;
        private requestParamsPagina: requestParam.RequestParam.RequestParams;
        public tipoDeItemDeMenu: any;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'itemDeMenuService', 'menuService', 'categoriaService', 'paginaService', '$uibModal', '$scope'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public itemDeMenuService: itemDeMenuService.Services.ItemDeMenuService,
            public menuService: menuService.Services.MenuService,
            public categoriaService: categoriaService.Services.CategoriaService,
            public paginaService: paginaService.Services.PaginaService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public $scope: ng.IScope) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                itemDeMenuService,
                'tenant.cms.itemDeMenu');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };

            this.tipoDeItemDeMenu = ez.domain.enum.tipoDeItemDeMenuEnum.valores;
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.

                // menu
                if (entity.menuId) {
                    this.ezSelectMenu.loading = true;
                    var promiseMenu = this.menuService.getById(entity.menuId);
                    promiseMenu.then(result => {
                        this.ezSelectMenu.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectMenu.loading = false;
                    });
                }

                if (entity.itemMenuId) {
                    // item de menu pai
                    this.ezSelectItemDeMenu.loading = true;
                    var promiseItemDeMenu = this.itemDeMenuService.getById(entity.itemMenuId);
                    promiseItemDeMenu.then(result => {
                        this.ezSelectItemDeMenu.setInputText(result.titulo);
                    }).finally(() => {
                        this.ezSelectItemDeMenu.loading = false;
                    });
                }

                if (entity.categoriaId) {
                    // categoria
                    this.ezSelectCategoria.loading = true;
                    var promiseCategoria = this.categoriaService.getById(entity.categoriaId);
                    promiseCategoria.then(result => {
                        this.ezSelectCategoria.setInputText(result.nome);
                    }).finally(() => {
                        this.ezSelectCategoria.loading = false;
                    });
                }

                if (entity.paginaId) {
                    // pagina
                    this.ezSelectPagina.loading = true;
                    var promisePagina = this.paginaService.getById(entity.paginaId);
                    promisePagina.then(result => {
                        this.ezSelectPagina.setInputText(result.titulo);
                    }).finally(() => {
                        this.ezSelectPagina.loading = false;
                    });
                }
            };

            this.ezSelectMenuConfig();
            this.ezSelectItemDeMenuConfig();
            this.ezSelectCategoriaConfig();
            this.ezSelectPaginaConfig();
            this.tipoDeMenuSelected();
            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private tipoDeMenuSelected() {
            if (this.entity) {
                this.entity.url = null;
            }
        };

        private ezSelectItemDeMenuConfig() {
            this.requestParamsItemDeMenu = new requestParam.RequestParam.RequestParams();

            this.itemDeMenuSelecionado = (registro) => {
                this.entity.itemMenuId = registro.id;
                return registro.titulo;
            };

            this.itemDeMenuDeselecionado = () => {
                this.entity.itemMenuId = null;
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoItemDeMenu = (termoDigitadoPesquisa) => {
                var filtro = new itemDeMenuDtos.Dtos.Cms.ItemDeMenu.GetItemDeMenuInput();
                filtro.id = this.entity.id;
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectItemDeMenu = new ezSelect.EzSelect.EzSelect<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.itemDeMenuService,
                this.getFiltroParaPaginacaoItemDeMenu,
                this.requestParamsItemDeMenu,
                this.$uibModal,
                this.itemDeMenuSelecionado,
                this.itemDeMenuDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectItemDeMenu.onEzGridCreated = () => {
                this.ezSelectItemDeMenu.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.itemDeMenuService.getItemDeMenuExceptForId(filtro, requestParams);
                };

                this.ezSelectItemDeMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemMenu.Titulo'),
                    field: 'titulo'
                });

                this.ezSelectItemDeMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('ItemMenu.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectItemDeMenu.setLinkSelecionarPrimeiraColunaGrid();

                // Eventos do ezSelect.
                // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
                this.ezSelectItemDeMenu.on.beforeOpenModalDialog = () => {
                    this.ezSelectItemDeMenu.ezGrid.getRegistros();
                };
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectItemDeMenu.on.beforeOpenModalDialog = () => {
                this.ezSelectItemDeMenu.ezGrid.getRegistros();
            };
        }

        private ezSelectMenuConfig() {
            this.requestParamsMenu = new requestParam.RequestParam.RequestParams();

            this.menuSelecionado = (registro) => {
                this.entity.menuId = registro.id;
                return registro.nome;
            };

            this.menuDeselecionado = () => {
                this.entity.menuId = null;
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoMenu = (termoDigitadoPesquisa) => {
                var filtro = new menuDtos.Dtos.Cms.Menu.GetMenuInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectMenu = new ezSelect.EzSelect.EzSelect<menuDtos.Dtos.Cms.Menu.MenuListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.menuService,
                this.getFiltroParaPaginacaoMenu,
                this.requestParamsMenu,
                this.$uibModal,
                this.menuSelecionado,
                this.menuDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectMenu.onEzGridCreated = () => {
                this.ezSelectMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Menu.Nome'),
                    field: 'nome'
                });

                this.ezSelectMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Menu.Site'),
                    field: 'site.nome'
                });

                this.ezSelectMenu.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Menu.Host'),
                    field: 'site.host'
                });

                this.ezSelectMenu.setLinkSelecionarPrimeiraColunaGrid();

                // Eventos do ezSelect.
                // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
                this.ezSelectMenu.on.beforeOpenModalDialog = () => {
                    this.ezSelectMenu.ezGrid.getRegistros();
                };
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectMenu.on.beforeOpenModalDialog = () => {
                this.ezSelectMenu.ezGrid.getRegistros();
            };
        }

        private ezSelectCategoriaConfig() {
            this.requestParamsCategoria = new requestParam.RequestParam.RequestParams();

            this.categoriaSelecionado = (registro) => {
                this.entity.categoriaId = registro.id;

                var linkInput = {
                    tipoDeLink: ez.domain.enum.tipoDeLinkEnum.categoria,
                    siteId: registro.siteId,
                    conteudoId: 0,
                    titulo: registro.nome,
                    slug: registro.slug
                };

                var promise = abp.services.app.cms.link.getLink(linkInput);
                promise.then((result) => {
                    this.entity.url = result.link;
                    this.$scope.$apply(this.entity.url);
                });


                return registro.nome;
            };

            this.categoriaDeselecionado = () => {
                this.entity.categoriaId = null;
                this.entity.url = null;
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoCategoria = (termoDigitadoPesquisa) => {
                var filtro = new categoriaDtos.Dtos.Cms.Categoria.GetCategoriaInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectCategoria = new ezSelect.EzSelect.EzSelect<categoriaDtos.Dtos.Cms.Categoria.CategoriaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.categoriaService,
                this.getFiltroParaPaginacaoCategoria,
                this.requestParamsCategoria,
                this.$uibModal,
                this.categoriaSelecionado,
                this.categoriaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectCategoria.onEzGridCreated = () => {
                this.ezSelectCategoria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Nome'),
                    field: 'nome'
                });

                this.ezSelectCategoria.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Categoria.Descricao'),
                    field: 'descricao'
                });

                this.ezSelectCategoria.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectCategoria.on.beforeOpenModalDialog = () => {
                this.ezSelectCategoria.ezGrid.getRegistros();
            };
        }

        private ezSelectPaginaConfig() {
            this.requestParamsPagina = new requestParam.RequestParam.RequestParams();

            this.paginaSelecionado = (registro) => {
                this.entity.paginaId = registro.id;

                var linkInput = {
                    tipoDeLink: ez.domain.enum.tipoDeLinkEnum.pagina,
                    siteId: registro.siteId,
                    conteudoId: 0,
                    titulo: registro.titulo,
                    slug: registro.slug
                };

                var promise = abp.services.app.cms.link.getLink(linkInput);
                promise.then((result) => {
                    this.entity.url = result.link;
                    this.$scope.$apply(this.entity.url);
                });

                return registro.titulo;
            };

            this.paginaDeselecionado = () => {
                this.entity.paginaId = null;
                this.entity.url = null;
            };

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoPagina = (termoDigitadoPesquisa) => {
                var filtro = new paginaDtos.Dtos.Cms.Publicacao.GetPaginaInput();
                filtro.titulo = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectPagina = new ezSelect.EzSelect.EzSelect<paginaDtos.Dtos.Cms.Publicacao.PaginaListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.paginaService,
                this.getFiltroParaPaginacaoPagina,
                this.requestParamsPagina,
                this.$uibModal,
                this.paginaSelecionado,
                this.paginaDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectPagina.onEzGridCreated = () => {
                this.ezSelectPagina.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Pagina.Titulo'),
                    field: 'titulo'
                });

                this.ezSelectPagina.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectPagina.on.beforeOpenModalDialog = () => {
                this.ezSelectPagina.ezGrid.getRegistros();
            };
        }
    }

    /*
   Os bindings do componente funcionam da seguinte maneira:
       @: deve ser usado para strings
       <: objetos
       &: funcoes
       =: não deve ser mais usado, está obsoleto
   */
    export class ItemDeMenuFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/itemDeMenu/itemDeMenuForm.cshtml';
            this.controller = ItemDeMenuFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}