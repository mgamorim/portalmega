﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as templateService from "../../../../services/domain/cms/template/templateService";
import * as templateDtos from "../../../../dtos/cms/template/templateDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module CMS.Template {
    import dtos = templateDtos.Dtos.Cms.Template;

    class TemplateIndexController extends controllers.Controllers.ControllerIndexBaseReadOnly<dtos.TemplateListDto, dtos.TemplateInput>{

        static $inject = ['$state', 'uiGridConstants', 'templateService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<dtos.TemplateListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public templateService: templateService.Services.TemplateService) {

            super(
                $state,
                uiGridConstants,
                templateService, { ///TODO: Verificar a implantação da ação Sincronizar
                    rotaAlterarRegistro: null,
                    rotaNovoRegistro: () => {
                        this.ezGrid.loading = true;
                        var promise = this.templateService.sincronizar();
                        promise.then((result) => {
                            this.ezGrid.loading = false;
                            this.ezGrid.getRegistros();
                        }).catch((result) => {
                            this.ezGrid.loading = false;
                            console.log(result);
                        });
                    }
                }
            );
        }

        $onInit() {
            super.init();

            this.ezGridCreateRecord.callback = () => {
                var promiseTemplate = this.templateService.sincronizar();
                promiseTemplate.then(result => {
                    abp.notify.info(app.localize('SynchronizeSuccessfully'), "");
                    this.ezGrid.getRegistros();
                }).finally(() => {
                })
            };

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new dtos.GetTemplateInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.autor = termoDigitadoPesquisa;
                filtro.descricao = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<dtos.TemplateListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.templateService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('SynchronizeAll', this.novo, abp.auth.hasPermission('Pages.Tenant.CMS.Template.Synchronize'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Template.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Template.Descricao'),
                field: 'descricao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Template.Versao'),
                field: 'versao'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Template.Autor'),
                field: 'autor'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Template.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });
        }
    }

    export class CMSTemplateIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/template/templateIndex.cshtml';
            this.controller = TemplateIndexController;
        }
    }
}