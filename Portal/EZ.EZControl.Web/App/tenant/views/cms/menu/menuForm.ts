﻿import * as menuService from "../../../../services/domain/cms/menu/menuService";
import * as siteService from "../../../../services/domain/cms/site/siteService";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as menuDtos from "../../../../dtos/cms/menu/menuDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    export class MenuFormController extends controllers.Controllers.ControllerFormComponentBase<menuDtos.Dtos.Cms.Menu.MenuListDto, menuDtos.Dtos.Cms.Menu.MenuInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectSite: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoSite: (termoDigitadoPesquisa: string) => any;
        private siteSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private siteDeselecionado: () => void;
        private requestParamsSite: requestParam.RequestParam.RequestParams;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'menuService', 'siteService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public menuService: menuService.Services.MenuService,
            public siteService: siteService.Services.SiteService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                menuService,
                'tenant.cms.menu');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new menuDtos.Dtos.Cms.Menu.MenuInput();
                instance.id = 0;
                instance.isActive = true;
                return instance;
            };
            this.events.onGetEntity = (entity) => {
                // evento que é disparado quando a controller acabou de obter a entidade do servidor.
                // aqui é o momento de carregar as referências da entidade da tela.
                this.ezSelectSite.loading = true;
                var promise = this.siteService.getById(entity.siteId);
                promise.then(result => {
                    this.ezSelectSite.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectSite.loading = false;
                });
            }
            this.ezSelectSiteConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectSiteConfig() {
            this.requestParamsSite = new requestParam.RequestParam.RequestParams();

            this.siteSelecionado = (registro) => {
                this.entity.siteId = registro.id;
                return registro.nome;
            }

            this.siteDeselecionado = () => {
                this.entity.siteId = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoSite = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.host = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSite = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoSite,
                this.requestParamsSite,
                this.$uibModal,
                this.siteSelecionado,
                this.siteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSite.onEzGridCreated = () => {
                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Nome'),
                    field: 'nome'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Host'),
                    field: 'site.host'
                });

                this.ezSelectSite.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSite.on.beforeOpenModalDialog = () => {
                this.ezSelectSite.ezGrid.getRegistros();
            };
        }
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class MenuFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/menu/menuForm.cshtml';
            this.controller = MenuFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}