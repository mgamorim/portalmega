﻿import * as ezGrid from "../../../../common/ezGrid/ezGrid";
import * as menuService from "../../../../services/domain/cms/menu/menuService";
import * as menuDtos from "../../../../dtos/cms/menu/menuDtos";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as requestParams from "../../../../common/requestParams/requestParams";

export module CMS.Menu {

    class MenuIndexController extends controllers.Controllers.ControllerIndexBase<menuDtos.Dtos.Cms.Menu.MenuListDto, menuDtos.Dtos.Cms.Menu.MenuInput>{

        static $inject = ['$state', 'uiGridConstants', 'menuService'];
        public ezGrid: ezGrid.EzGrid.EzGrid<menuDtos.Dtos.Cms.Menu.MenuListDto, requestParams.RequestParam.RequestParams>;
        public requestParams: requestParams.RequestParam.RequestParams;
        public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;

        constructor(
            public $state: ng.ui.IStateService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public menuService: menuService.Services.MenuService) {

            super(
                $state,
                uiGridConstants,
                menuService, {
                    rotaAlterarRegistro: 'tenant.cms.menu.alterar',
                    rotaNovoRegistro: 'tenant.cms.menu.novo'
                }
            );
        }

        $onInit() {
            super.init();

            this.requestParams = new requestParams.RequestParam.RequestParams();

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacao = (termoDigitadoPesquisa) => {
                var filtro = new menuDtos.Dtos.Cms.Menu.GetMenuInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.nomeSite = termoDigitadoPesquisa;
                return filtro;
            }

            // instanciar grid
            this.ezGrid = new ezGrid.EzGrid.EzGrid<menuDtos.Dtos.Cms.Menu.MenuListDto, requestParams.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.menuService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            // objeto para exibir o botão para inserir um novo registro
            this.ezGridCreateRecord.applySettings('CreatingNewMenu', this.novo, abp.auth.hasPermission('Pages.Tenant.CMS.Menu.Create'));
            this.ezGrid.ezGridCreateRecord = this.ezGridCreateRecord;

            // evento que é disparado quando um registro é excluído
            this.onDeletedEntity = () => {
                this.ezGrid.getRegistros();
            }

            // configurar o ezGrid
            this.ezGridConfig();
            this.ezGrid.getRegistros();
        }

        private ezGridConfig() {
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.editar, 'Edit',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Menu.Edit');
                }
            ));
            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.excluir, 'Delete',
                (row) => {
                    return abp.auth.hasPermission('Pages.Tenant.CMS.Menu.Delete');
                }
            ));

            this.ezGrid.wordToSearch.placeholderInputSearch = app.localize('SearchWithThreeDot');

            // Definir as colunas
            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Menu.Nome'),
                field: 'nome',
                cellTemplate: '\
                <div class="ui-grid-cell-contents">\
                    {{grid.getCellValue(row, col)}}<br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible(row.entity)" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Menu.Ativo'),
                field: 'isActive',
                cellTemplate: 'App/common/views/common/grid/estaChecado.html'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Menu.Site'),
                field: 'site.nome'
            });

            this.ezGrid.optionsGrid.columnDefs.push({
                name: app.localize('Menu.Host'),
                field: 'site.host'
            });

        }
    }

    export class MenuIndexComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/menu/menuIndex.cshtml';
            this.controller = MenuIndexController;
        }
    }
}