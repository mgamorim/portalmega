﻿import * as paginaService from "../../../../services/domain/cms/pagina/paginaService";
import * as siteService from "../../../../services/domain/cms/site/siteService";
import * as templateService from "../../../../services/domain/cms/template/templateService";
import * as linkService from "../../../../services/domain/cms/link/linkService";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as linkDtos from "../../../../dtos/cms/link/linkDtos";
import * as publicacaoDtos from "../../../../dtos/cms/publicacao/publicacaoDtos";
import * as requestParam from "../../../../common/requestParams/requestParams";
import * as controllers from "../../../../common/controllers/controller";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ezSelect from "../../../../common/ezSelect/ezSelect";

export module Forms {
    import dtos = publicacaoDtos.Dtos.Cms.Publicacao;

    export class PaginaFormController extends controllers.Controllers.ControllerFormComponentBase<dtos.PaginaListDto, dtos.PaginaInput, applicationServiceDtos.Dtos.ApplicationService.IdInput>{
        public ezSelectPagina: ezSelect.EzSelect.EzSelect<dtos.PaginaListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoPublicacao: (termoDigitadoPesquisa: string) => any;
        private paginaSelecionado: (registro: dtos.PaginaListDto) => string;
        private paginaDeselecionado: () => void;
        private requestParamsPagina: requestParam.RequestParam.RequestParams;

        public ezSelectTipoDePaginaParaPaginaDefault: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoTipoDePaginaParaPaginaDefault: (termoDigitadoPesquisa: string) => any;
        private tipoDePaginaParaPaginaDefaultSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private tipoDePaginaParaPaginaDefaultDeselecionado: () => void;
        private requestParamsTipoDePaginaParaPaginaDefault: requestParam.RequestParam.RequestParams;

        public ezSelectSite: ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>;
        public getFiltroParaPaginacaoSite: (termoDigitadoPesquisa: string) => any;
        private siteSelecionado: (registro: siteDtos.Dtos.CMS.Site.SiteListDto) => string;
        private siteDeselecionado: () => void;
        private requestParamsSite: requestParam.RequestParam.RequestParams;

        public statusDaPublicacao = ez.domain.enum.statusDaPublicacaoEnum.valores;

        public linkInput: linkDtos.Dtos.Cms.Link.LinkInput;

        static $inject = ['$state', '$stateParams', 'uiGridConstants', 'paginaService', 'siteService', 'linkService', 'templateService', '$uibModal'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public paginaService: paginaService.Services.PaginaService,
            public siteService: siteService.Services.SiteService,
            public linkService: linkService.Services.LinkService,
            public templateService: templateService.Services.TemplateService,
            public $uibModal: angular.ui.bootstrap.IModalService) {
            super(
                $state,
                $stateParams,
                $uibModal,
                uiGridConstants,
                paginaService,
                'tenant.cms.pagina');
        }

        $onInit() {
            super.init();

            this.events.onGetNewEntityInstance = () => {
                // evento para obter uma nova instancia para a entidade da tela
                // e tambem para inicializar suas variavéis com valores padrões
                var instance = new dtos.PaginaInput();
                instance.id = 0;
                instance.isActive = true;
                instance.dataDePublicacao = new Date();
                instance.siteId = 0;
                instance.statusDaPublicacaoEnum = ez.domain.enum.statusDaPublicacaoEnum.rascunho;
                return instance;
            };

            this.events.onGetEntity = (entity) => {

                this.ezSelectSite.loading = true;
                var promise = this.siteService.getById(entity.siteId);
                promise.then(result => {
                    this.ezSelectSite.setInputText(result.nome);
                }).finally(() => {
                    this.ezSelectSite.loading = false;
                });

                this.ezSelectTipoDePaginaParaPaginaDefault.setInputText(entity.tipoDePagina);
                this.entity.dataDePublicacao = new Date();

                this.tituloNgChange(entity);
                this.getLink(entity);
            }

            this.ezSelectSiteConfig();
            this.ezSelectTipoDePaginaParaPaginaDefaultConfig();

            // método para carregar ou criar uma nova instancia da entidade.
            // baseado na operação que foi definida no componente, a controller irá obter uma nova instancia ou buscar o registro do servidor
            this.prepareEntityInstance();
        }

        private ezSelectSiteConfig() {
            this.requestParamsSite = new requestParam.RequestParam.RequestParams();

            this.siteSelecionado = (registro) => {
                this.entity.siteId = registro.id;
                this.ezSelectTipoDePaginaParaPaginaDefault.setInputText(registro.tipoDePaginaParaPaginaDefault);
                return registro.nome;
            }

            this.siteDeselecionado = () => {
                this.entity.siteId = null;
                this.entity.tipoDePagina = null;
                this.ezSelectTipoDePaginaParaPaginaDefault.setInputText('');
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoSite = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                filtro.host = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectSite = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoSite,
                this.requestParamsSite,
                this.$uibModal,
                this.siteSelecionado,
                this.siteDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectSite.onEzGridCreated = () => {
                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Nome'),
                    field: 'nome'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Ativo'),
                    field: 'isActive',
                    cellTemplate: 'App/common/views/common/grid/estaChecado.html'
                });

                this.ezSelectSite.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.Host'),
                    field: 'site.host'
                });

                this.ezSelectSite.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectSite.on.beforeOpenModalDialog = () => {
                this.ezSelectSite.ezGrid.getRegistros();
            };
        }

        private ezSelectTipoDePaginaParaPaginaDefaultConfig() {
            this.requestParamsTipoDePaginaParaPaginaDefault = new requestParam.RequestParam.RequestParams();

            this.tipoDePaginaParaPaginaDefaultSelecionado = (registro) => {
                this.entity.tipoDePagina = registro.valor;
                return registro.valor;
            }

            this.tipoDePaginaParaPaginaDefaultDeselecionado = () => {
                this.entity.tipoDePagina = null;
            }

            // callback para criar e definir o filtro para buscar os registros
            this.getFiltroParaPaginacaoTipoDePaginaParaPaginaDefault = (termoDigitadoPesquisa) => {
                var filtro = new siteDtos.Dtos.CMS.Site.GetSiteInput();
                filtro.nome = termoDigitadoPesquisa;
                return filtro;
            }

            this.ezSelectTipoDePaginaParaPaginaDefault = new ezSelect.EzSelect.EzSelect<siteDtos.Dtos.CMS.Site.SiteListDto, requestParam.RequestParam.RequestParams>(
                this.uiGridConstants,
                this.siteService,
                this.getFiltroParaPaginacaoTipoDePaginaParaPaginaDefault,
                this.requestParamsTipoDePaginaParaPaginaDefault,
                this.$uibModal,
                this.tipoDePaginaParaPaginaDefaultSelecionado,
                this.tipoDePaginaParaPaginaDefaultDeselecionado);

            // Esse evento é utilizado para configurar o grid após o mesmo ser criado.
            // O ezSelect só cria o ezGrid quando o usuário clica no botão para selecionar algum registro,
            // por isso é disparado esse evento para definir as colunas ou alguma propriedade do ezGrid
            this.ezSelectTipoDePaginaParaPaginaDefault.onEzGridCreated = () => {

                this.ezSelectTipoDePaginaParaPaginaDefault.ezGrid.serviceGetCallback = (filtro, requestParams) => {
                    return this.templateService.getListTiposDePaginas(this.entity.siteId);
                }

                this.ezSelectTipoDePaginaParaPaginaDefault.ezGrid.optionsGrid.columnDefs.push({
                    name: app.localize('Site.TipoDePaginaParaPaginaDefault'),
                    field: 'valor'
                });

                this.ezSelectTipoDePaginaParaPaginaDefault.setLinkSelecionarPrimeiraColunaGrid();
            }

            // Eventos do ezSelect.
            // Este evento foi codificado para que quando abrir a janela modal, já exibir registros de forma paginada no ezGrid
            this.ezSelectTipoDePaginaParaPaginaDefault.on.beforeOpenModalDialog = () => {
                this.ezSelectTipoDePaginaParaPaginaDefault.ezGrid.getRegistros();
            };
        }

        tituloNgChange = (entity: dtos.PaginaInput) => {
            this.entity.slug = (entity.titulo as any).slugify(); // Cast pra any para usar slugify que é extendido pelo javascript
        };

        getLink = (entity: dtos.PaginaInput) => {
            var promise = this.linkService.getLink({
                conteudoId: this.entity.id,
                siteId: this.entity.siteId,
                tipoDeLink: ez.domain.enum.tipoDeLinkEnum.pagina,
                slug: this.entity.slug,
                titulo: this.entity.titulo,
                categoriaPaiId: null,
                link: "",
                primeiraCategoria: null
            });
            promise.then((result) => {
                this.entity.link = result.link;
            });
        };
    }

    /*
    Os bindings do componente funcionam da seguinte maneira:
        @: deve ser usado para strings
        <: objetos
        &: funcoes
        =: não deve ser mais usado, está obsoleto
    */
    export class PaginaFormComponent implements ng.IComponentOptions {
        public templateUrl: string;
        public controller: any;
        public bindings: any;

        constructor() {
            this.templateUrl = '~/app/tenant/views/cms/pagina/paginaForm.cshtml';
            this.controller = PaginaFormController;
            this.bindings = {
                operation: '@'
            }
        }
    }
}