﻿import { Routes as globalRoutes } from "routes/global";
import { Routes as cmsRoutes } from "routes/cms";
import { Routes as agendaRoutes } from "routes/agenda";
import { Routes as ezPagRoutes } from "routes/ezpag";
import { Routes as ezMedicalRoutes } from "routes/ezmedical";
import { Routes as estoqueRoutes } from "routes/estoque";
import { Routes as vendasRoutes } from "routes/vendas";
import { Routes as financeiroRoutes } from "routes/financeiro";
import { Routes as servicosRoutes } from "routes/servicos";
import { Routes as ezLivRoutes } from "routes/ezliv";
import { Routes as administrationRoutes } from "routes/administration";
import { Registrations as cmsComponents } from "registrations/cms";
import { Registrations as globalComponents } from "registrations/global";
import { Registrations as agendaComponents } from "registrations/agenda";
import { Registrations as ezMedicalComponents } from "registrations/ezmedical";
import { Registrations as estoqueComponents } from "registrations/estoque";
import { Registrations as vendasComponents } from "registrations/vendas";
import { Registrations as financeiroComponents } from "registrations/financeiro";
import { Registrations as servicosComponents } from "registrations/servicos";
import { Registrations as ezLivComponents } from "registrations/ezliv";
import { Registrations as ezPagComponents } from "registrations/ezpag";
import { Registrations as appComponents } from "registrations/appComponents";
import { Registrations as administrationComponents } from "registrations/administration";

appComponents.AppComponents.registerComponents();

globalRoutes.GlobalRoutes.defineRoutes();
globalComponents.GlobalComponents.registerComponents();

cmsRoutes.CmsRoutes.defineRoutes();
cmsComponents.CmsComponents.registerComponents();

agendaRoutes.AgendaRoutes.defineRoutes();
agendaComponents.AgendaComponents.registerComponents();

administrationRoutes.AdministrationRoutes.defineRoutes();
administrationComponents.AdministrationComponents.registerComponents();

ezMedicalRoutes.EZMedicalRoutes.defineRoutes();
ezMedicalComponents.EZMedicalComponents.registerComponents();

estoqueRoutes.EstoqueRoutes.defineRoutes();
estoqueComponents.EstoqueComponents.registerComponents();

vendasRoutes.VendasRoutes.defineRoutes();
vendasComponents.VendasComponents.registerComponents();

servicosRoutes.ServicosRoutes.defineRoutes();
servicosComponents.ServicosComponents.registerComponents();

financeiroRoutes.FinanceiroRoutes.defineRoutes();
financeiroComponents.FinanceiroComponents.registerComponents();

ezLivRoutes.EZLivRoutes.defineRoutes();
ezLivComponents.EZLivComponents.registerComponents();

ezPagRoutes.EZPagRoutes.defineRoutes();
ezPagComponents.EZPagComponents.registerComponents();