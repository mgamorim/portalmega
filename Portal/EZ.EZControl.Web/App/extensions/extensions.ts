﻿/*
    Para utilizar essas extensões é necessário incluir uma dependência para o requirejs fazer o download deste arquivo.
    Por exemplo, se quisermos utilizar método string.echo() em algum outro arquivo, é necessário digitar a linha abaixo no inicio do arquivo:

    /// <amd-dependency path="../../../../extensions/extensions" />

    No caso do método string.slugify(), ele funciona sem adicionar essa dependência pois o slugify já extende
    o prototype da string e o arquivo de sua implementação é feito o download de forma automática.
*/


// Lembrando que o valor de "this" nos métodos é um objeto "String" que é diferente de "string".
// Caso queira fazer alguma comparação talvez o ideal é converter o this.toString()
interface String {
    slugify: () => string;
    echo: () => void;
    inArray(list: Array<string>): boolean;
}

if (!String.prototype.echo) {
    String.prototype.echo = function() {
        console.log(this);
    }
}

if (!String.prototype.inArray) {
    String.prototype.inArray = function (list: Array<string> = []): boolean {
        var result: boolean = false;
        for (var i = 0; i !== list.length; i++) {
            if (list[i] === this.toString()) {
                result = true;
                break;
            }
        }
        return result;
    };
}