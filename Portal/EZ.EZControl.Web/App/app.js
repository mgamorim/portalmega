﻿/* 'app' MODULE DEFINITION */
var appModule = angular.module("app", [
    "ngTouch",
    'ng-currency',
    "ui.router",
    "ui.bootstrap",
    'ui.utils',
    "ui.jq",
    'ui.grid',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ui.mask',
    "oc.lazyLoad",
    "ngSanitize",
    'angularFileUpload',
    'daterangepicker',
    'angularMoment',
    'frapontillo.bootstrap-switch',
    'abp',
    'ui.tinymce',
    'ngFileUpload',
    'kendo.directives',
    'ngAnimate',
    'ngAria',
    'ngMaterial',
    'ng-slide-down',
    'ncy-angular-breadcrumb'
]);

/* LAZY LOAD CONFIG */

/* This application does not define any lazy-load yet but you can use $ocLazyLoad to define and lazy-load js/css files.
 * This code configures $ocLazyLoad plug-in for this application.
 * See it's documents for more information: https://github.com/ocombe/ocLazyLoad
 */
appModule.config(['$ocLazyLoadProvider', '$compileProvider', function ($ocLazyLoadProvider, $compileProvider) {
    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before', // load the css files before a LINK element with this ID.
        debug: false,
        events: true,
        modules: []
    });

    $compileProvider.debugInfoEnabled(false)
}]);

appModule.config(function ($breadcrumbProvider) {
    $breadcrumbProvider.setOptions({
        template: '<ul class="bc-list">' +
            '<li class="bc-item">' +
            '    <i class="eziicon-home"></i>' +
            '</li>' +
            '<li ng-repeat="step in steps | limitTo: 1" class="bc-item active">{{step.ncyBreadcrumbLabel}}</li>' +
        '</ul>'
    });
});

// constantes
appModule.constant("apiUrl", "api/files/");

/* THEME SETTINGS */
App.setAssetsPath(abp.appPath + 'metronic/assets/');
appModule.factory('settings', ['$rootScope', function ($rootScope) {
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: App.getAssetsPath() + 'admin/layout4/img/',
        layoutCssPath: App.getAssetsPath() + 'admin/layout4/css/',
        assetsPath: abp.appPath + 'metronic/assets',
        globalPath: abp.appPath + 'metronic/assets/global',
        layoutPath: abp.appPath + 'metronic/assets/layouts/layout4'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* ROUTE DEFINITIONS */

appModule.config([
    '$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        // Default route (overrided below if user has permission)
        $urlRouterProvider.otherwise("/welcome");

        //Welcome page
        $stateProvider.state('welcome', {
            url: '/welcome',
            templateUrl: '~/App/common/views/welcome/index.cshtml',
            ncyBreadcrumb: {
                label: 'Bem Vindo'
            }
        });

        //COMMON routes

        if (abp.auth.hasPermission('Pages.Administration.Roles')) {
            $stateProvider.state('roles', {
                url: '/roles',
                templateUrl: '~/App/common/views/roles/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Perfis'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Users')) {
            $stateProvider.state('users', {
                url: '/users?filterText',
                templateUrl: '~/App/common/views/users/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Usuários'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Languages')) {
            $stateProvider.state('languages', {
                url: '/languages',
                templateUrl: '~/App/common/views/languages/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Idiomas'
                }
            });

            if (abp.auth.hasPermission('Pages.Administration.Languages.ChangeTexts')) {
                $stateProvider.state('languageTexts', {
                    url: '/languages/texts/:languageName?sourceName&baseLanguageName&targetValueFilter&filterText',
                    templateUrl: '~/App/common/views/languages/texts.cshtml',
                    ncyBreadcrumb: {
                        label: 'Modificar textos'
                    }
                });
            }
        }

        if (abp.auth.hasPermission('Pages.Administration.AuditLogs')) {
            $stateProvider.state('auditLogs', {
                url: '/auditLogs',
                templateUrl: '~/App/common/views/auditLogs/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Logs de Auditoria'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.OrganizationUnits')) {
            $stateProvider.state('organizationUnits', {
                url: '/organizationUnits',
                templateUrl: '~/App/common/views/organizationUnits/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Unidades Organizacionais'
                }
            });
        }

        $stateProvider.state('notifications', {
            url: '/notifications',
            templateUrl: '~/App/common/views/notifications/index.cshtml',
            ncyBreadcrumb: {
                label: 'Notificações'
            }
        });

        //HOST routes

        $stateProvider.state('host', {
            'abstract': true,
            url: '/host',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenants')) {
            $urlRouterProvider.otherwise("/host/tenants"); //Entrance page for the host
            $stateProvider.state('host.tenants', {
                url: '/tenants?filterText',
                templateUrl: '~/App/host/views/tenants/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Tenants'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Editions')) {
            $stateProvider.state('host.editions', {
                url: '/editions',
                templateUrl: '~/App/host/views/editions/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Edições'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Host.Maintenance')) {
            $stateProvider.state('host.maintenance', {
                url: '/maintenance',
                templateUrl: '~/App/host/views/maintenance/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Manutenção'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Host.Settings')) {
            $stateProvider.state('host.settings', {
                url: '/settings',
                templateUrl: '~/App/host/views/settings/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Configurações'
                }
            });
        }

        //TENANT routes

        $stateProvider.state('tenant', {
            'abstract': true,
            url: '/tenant',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenant.Dashboard')) {
            $urlRouterProvider.otherwise("/tenant/dashboard"); //Entrance page for a tenant
            $stateProvider.state('tenant.dashboard', {
                url: '/dashboard',
                templateUrl: '~/App/tenant/views/dashboard/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Dashboard'
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Tenant.Settings')) {
            $stateProvider.state('tenant.settings', {
                url: '/settings',
                templateUrl: '~/App/tenant/views/settings/index.cshtml',
                ncyBreadcrumb: {
                    label: 'Configurações'
                }
            });
        }


        // CMS
        $stateProvider.state('tenant.cms', {
            'abstract': true,
            url: '/cms',
            template: '<div ui-view class="fade-in-up">AQUiii</div>'
        });

        // Tag
        if (abp.auth.hasPermission('Pages.Tenant.CMS.Tag')) {
            $stateProvider.state('tenant.cms.tag', {
                url: '/tag',
                templateUrl: '~/App/tenant/views/cms/tag/index.cshtml',
                menu: 'CMS.Tag.Tenant'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.CMS.Tag.Edit')) {
            $stateProvider.state('tenant.cms.tag.alterar', {
                url: '/alterar/:id',
                views: {
                    '@tenant': {
                        templateUrl: '~/App/tenant/views/cms/tag/edit.cshtml'
                    }
                }
            });
        }

        // Menu
        if (abp.auth.hasPermission('Pages.Tenant.CMS.Menu')) {
            $stateProvider.state('tenant.cms.menu', {
                url: '/menu',
                templateUrl: '~/App/tenant/views/cms/menu/index.cshtml',
                menu: 'CMS.Menu.Tenant'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.CMS.Menu.Create')) {
            $stateProvider.state('tenant.cms.menu.novo', {
                url: '/novo',
                views: {
                    '@tenant': {
                        templateUrl: '~/App/tenant/views/cms/menu/create.cshtml'
                    }
                }
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.CMS.Menu.Edit')) {
            $stateProvider.state('tenant.cms.menu.alterar', {
                url: '/alterar/:id',
                views: {
                    '@tenant': {
                        templateUrl: '~/App/tenant/views/cms/menu/edit.cshtml'
                    }
                }
            });
        }

        //// Item de Menu
        //if (abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu')) {
        //    $stateProvider.state('tenant.cms.itemDeMenu', {
        //        url: '/itemDeMenu',
        //        templateUrl: '~/App/tenant/views/cms/itemDeMenu/index.cshtml',
        //        menu: 'CMS.ItemDeMenu.Tenant'
        //    });
        //}

        //if (abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Create')) {
        //    $stateProvider.state('tenant.cms.itemDeMenu.novo', {
        //        url: '/novo',
        //        views: {
        //            '@tenant': {
        //                templateUrl: '~/App/tenant/views/cms/itemDeMenu/create.cshtml'
        //            }
        //        }
        //    });
        //}

        //if (abp.auth.hasPermission('Pages.Tenant.CMS.ItemDeMenu.Edit')) {
        //    $stateProvider.state('tenant.cms.itemDeMenu.alterar', {
        //        url: '/alterar/:id',
        //        views: {
        //            '@tenant': {
        //                templateUrl: '~/App/tenant/views/cms/itemDeMenu/edit.cshtml'
        //            }
        //        }
        //    });
        //}

    }
]);




//appModule.config(['$httpProvider', '$rootScope', function ($httpProvider, $rootScope) {
//    $httpProvider.responseInterceptors.push(function () {
//        return function (promise) {
//            return promise.then(function (response) {
//                $rootScope.ezLoadingSpinner.remove();
//                return response;
//            }, function (response) {
//                $rootScope.ezLoadingSpinner.remove();
//                return response;
//            });
//        };
//    });
//    return $httpProvider.defaults.transformRequest.push(function (data, headersGetter) {
//        $rootScope.ezLoadingSpinner.add();
//        return data;
//    });
//}]);

appModule.run(["$rootScope", "settings", "$state", 'i18nService', '$uibModalStack', function ($rootScope, settings, $state, i18nService, $uibModalStack) {
    $rootScope.$state = $state;
    $rootScope.$settings = settings;

    $rootScope.$on('$stateChangeSuccess', function () {
        $uibModalStack.dismissAll();
    });

    //Set Ui-Grid language
    //if (i18nService.get(abp.localization.currentCulture.name)) {
    //    i18nService.setCurrentLang(abp.localization.currentCulture.name);
    //} else {
    //    i18nService.setCurrentLang("en");
    //}
    i18nService.setCurrentLang("pt-BR");

    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase === '$apply' || phase === '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    var stAddEzLoadingSpinner;
    var stFnAddEzLoadingSpinner = function () {
        if (!stAddEzLoadingSpinner) {
            stAddEzLoadingSpinner = setTimeout(function () {
                $('#loading-overlay').fadeIn();
                stAddEzLoadingSpinner = null;
            }, 1000);
        }
    };

    var stRemoveEzLoadingSpinner;
    var stFnRemoveEzLoadingSpinner = function () {
        stRemoveEzLoadingSpinner = setTimeout(function () {
            if (!stAddEzLoadingSpinner) {
                $('#loading-overlay').fadeOut();
                $rootScope.ezLoadingSpinner.totalCount = 0;
                $rootScope.ezLoadingSpinner.ajaxCount = 0;
                $rootScope.ezLoadingSpinner.doneCount = 0;
                $rootScope.ezLoadingSpinner.hasMessage = false;
                $rootScope.ezLoadingSpinner.message = '';
                stRemoveEzLoadingSpinner = null;
            }
        },
            500);
    };

    $rootScope.ezLoadingSpinner = {
        ajaxCount: 0,
        totalCount: 0,
        doneCount: 0,
        message: '',
        hasMessage: false
    };

    // Função de inicialização do Spinner.
    $rootScope.ezLoadingSpinner.add = function (msg) {

        var mensagem = msg || 'Carregando dados...';

        $rootScope.ezLoadingSpinner.ajaxCount = $rootScope.ezLoadingSpinner.ajaxCount + 1;
        // Contabiliza quantas reqs foram iniciadas
        $rootScope.ezLoadingSpinner.totalCount = $rootScope.ezLoadingSpinner.totalCount + 1;
        
        // Verifica se há apenas uma requisição para colocar a mensagem personalizada, caso contrário usa a mensagem padrão.
        if ($rootScope.ezLoadingSpinner.totalCount === 1) {
            $rootScope.ezLoadingSpinner.message = mensagem;
            $rootScope.ezLoadingSpinner.hasMessage = true; 
        } else {
            $rootScope.ezLoadingSpinner.message = 'Carregando dados...';
            $rootScope.ezLoadingSpinner.hasMessage = false;
        }

        // Exibe o overlay caso a quantidade de requisições seja diferente de 0;
        stFnAddEzLoadingSpinner();
    };

    // Função para fechamento do Spinner.
    $rootScope.ezLoadingSpinner.remove = function () {
        $rootScope.ezLoadingSpinner.ajaxCount = $rootScope.ezLoadingSpinner.ajaxCount - 1;

        // Contabiliza quantas reqs já foram concluídas.
        $rootScope.ezLoadingSpinner.doneCount = $rootScope.ezLoadingSpinner.doneCount + 1;

        if ($rootScope.ezLoadingSpinner.ajaxCount === 0
            || ($rootScope.ezLoadingSpinner.doneCount ===  $rootScope.ezLoadingSpinner.totalCount)) {
            clearTimeout(stAddEzLoadingSpinner);
            stAddEzLoadingSpinner = null;
        }

        if (($rootScope.ezLoadingSpinner.ajaxCount === 0) || ($rootScope.ezLoadingSpinner.doneCount >= $rootScope.ezLoadingSpinner.totalCount)) {
            stFnRemoveEzLoadingSpinner();
        }
    };
}]);

//Fix temporário para o render correto da ui-grid
$("body").on("click", function () {
    setTimeout(function () {
        $(window).resize();
    }, 0);

    setTimeout(function () {
        $(window).resize();
    }, 1000);
});