﻿import * as requestParams from "../requestParams/requestParams";
import * as services from "../services/serviceEntityBase";
import * as ezGrid from "../../common/ezGrid/ezGrid";
import * as applicationServiceDtos from "../../application/services/dto/applicationServiceDtos";

export module Controllers {
    export interface IControllerIndexOptions<TEntityList extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity> {
        // rotas
        rotaAlterarRegistro: string;
        rotaNovoRegistro: any;
    }

    interface IControllerFormComponentEvents<TEntityList, TEntityInput, TIdInput> {
        onGetNewEntityInstance: () => TEntityInput;
        onGetEntity: (entity: TEntityInput) => void;
        onBeforeSaveEntity: () => void;
        onBeforeSaveEntityExecutePromise: <T>() => ng.IPromise<T>;
        onBeforeSaveEntityPromiseResolved: <T>(result: T) => void;
        onAfterSaveEntity: (result: TIdInput) => void;
        onBeforeCancel: () => boolean;
        onAfterCancel: () => void;
        onDeletedEntity: (entity: TEntityList) => void;
    }

    class ControllerFormComponentEvents<TEntityList, TEntityInput, TIdInput> implements IControllerFormComponentEvents<TEntityList, TEntityInput, TIdInput> {
        onGetNewEntityInstance: () => TEntityInput;
        onGetEntity: (entity: TEntityInput) => void;
        onBeforeSaveEntity: () => void;
        onBeforeSaveEntityExecutePromise: <T>() => ng.IPromise<T>;
        onBeforeSaveEntityPromiseResolved: <T>(result: T) => void;
        onAfterSaveEntity: (result: TIdInput) => void;
        onBeforeCancel: () => boolean;
        onAfterCancel: () => void;
        onDeletedEntity: (entity: TEntityList) => void;
    }

    export class ControllerIndexBase<TEntityList extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity, TEntityInput extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity> {
        public onDeletedEntity: () => void;
        public ezGridCreateRecord: ezGrid.EzGrid.EzGridCreateRecord;
        public editar: (registro: TEntityList) => void;
        public excluir: (registro: TEntityList) => void;
        public novo: () => void;

        static $inject = ['$state', 'uiGridConstants'];

        constructor(public $state: ng.ui.IStateService, public uiGridConstants: uiGrid.IUiGridConstants, public entityService: services.Services.IServiceEntity, public options: IControllerIndexOptions<TEntityList>) {
        }

        init() {
            this.ezGridCreateRecord = new ezGrid.EzGrid.EzGridCreateRecord();

            this.editar = (registro: TEntityList) => {
                this.$state.go(this.options.rotaAlterarRegistro, { id: registro.id });
            }

            this.excluir = (registro: TEntityList) => {
                abp.message.confirm(
                    '', '',
                    isConfirmed => {
                        if (isConfirmed) {
                            var promise = this.entityService.delete(registro.id);
                            promise.then(() => {
                                abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                            }).finally(() => {
                                if (this.onDeletedEntity)
                                    this.onDeletedEntity();
                            });
                        }
                    }
                );
            }

            this.novo = () => {
                if (jQuery.isFunction(this.options.rotaNovoRegistro)) {
                    this.options.rotaNovoRegistro();
                }
                else {
                    this.$state.go(this.options.rotaNovoRegistro);
                }
            }
        }
    }

    export class ControllerIndexBaseReadOnly<TEntityList extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity, TEntityInput extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity> {
        public onDeletedEntity: () => void;
        public ezGridCreateRecord: ezGrid.EzGrid.EzGridCreateRecord;
        public novo: () => void;

        static $inject = ['$state', 'uiGridConstants'];

        constructor(public $state: ng.ui.IStateService, public uiGridConstants: uiGrid.IUiGridConstants, public entityService: services.Services.IServiceEntityReadOnly, public options: IControllerIndexOptions<TEntityList>) {
        }

        init() {
            this.ezGridCreateRecord = new ezGrid.EzGrid.EzGridCreateRecord();

            this.novo = () => {
                if (jQuery.isFunction(this.options.rotaNovoRegistro)) {
                    this.options.rotaNovoRegistro();
                }
                else {
                    this.$state.go(this.options.rotaNovoRegistro);
                }
            }
        }
    }

    export class ControllerIndexBaseNoEntity {
        static $inject = ['$state'];
        constructor(public $state: ng.ui.IStateService) {
        }
    }

    export class ControllerFormComponentBase<TEntityList extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity, TEntityInput extends applicationServiceDtos.Dtos.ApplicationService.IEzEntity, TIdInput> {
        public events: ControllerFormComponentEvents<TEntityList, TEntityInput, TIdInput>;
        public createOrEdit: boolean;
        public operation: string;
        public saving: boolean;
        public loadingEntity: boolean;
        public registroId: number;
        public disableControls: boolean;
        public entity: TEntityInput;
        private get: () => void;
        public getEntityById: (id: number) => void;
        public save: () => void;
        public cancel: () => void;
        public delete: (registro: TEntityList) => void;
        public paginaDeListagem: () => void;

        static $inject = ['$state', '$stateParams', '$uibModal', 'uiGridConstants'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public uiGridConstants: uiGrid.IUiGridConstants,
            public entityService: services.Services.IServiceEntityBase,
            public rotaPaginaDeListagem: string) {

            this.events = new ControllerFormComponentEvents<TEntityList, TEntityInput, TIdInput>();

            ezFixes.uiGrid.uiTab.registerFix();
        }

        private canSave(arg: any): arg is services.Services.IServiceEntitySave {
            return arg.save !== undefined;
        }

        private canGetById(arg: any): arg is services.Services.IServiceEntityReadOnly {
            return arg.getById !== undefined;
        }

        private canGet(arg: any): arg is services.Services.IServiceEntityGetSingle {
            return arg.get !== undefined;
        }

        private canDelete(arg: any): arg is services.Services.IServiceEntityDelete {
            return arg.delete !== undefined;
        }

        // método para carregar ou criar uma nova instancia da entidade.
        prepareEntityInstance() {
            // quando a operação estiver como undefined, o formulario deve estar sendo utilizado como detalhe ou não foi definido esse atributo do componente na view que ele está definido.
            if (this.operation === undefined || this.operation === 'create') {
                if (this.events.onGetNewEntityInstance) {
                    this.entity = this.events.onGetNewEntityInstance();
                } else {
                    throw ('O callback getNewEntityInstance não está implementado.');
                }
            }
            else if (this.operation === 'delete' || this.operation === 'edit' || this.operation === 'estornar') {
                if (this.canGetById(this.entityService)) {
                    this.getEntityById(this.registroId);
                }
                else if (this.canGet(this.entityService)) {
                    this.get();
                }
            }
        }

        init() {
            this.saving = false;
            this.loadingEntity = false;
            this.registroId = this.$stateParams['id'];
            this.createOrEdit = this.operation === 'createOrEdit' || this.operation === 'create' || this.operation === 'edit';
            this.disableControls = this.operation === 'delete';

            this.get = () => {
                if (this.canGet(this.entityService)) {
                    this.loadingEntity = true;
                    var promise = (this.entityService as any as services.Services.IServiceEntityGetSingle).get<TEntityInput>();
                    promise.then(result => {
                        this.entity = result;

                        if (this.events.onGetEntity) {
                            this.events.onGetEntity(result);
                        }

                        if (!this.entity) {
                            this.entity = this.events.onGetNewEntityInstance();
                        }
                    }).finally(() => {
                        this.loadingEntity = false;
                    });
                }
            }

            this.getEntityById = (id) => {
                if (this.canGetById(this.entityService)) {
                    this.loadingEntity = true;
                    var promise = (this.entityService as any as services.Services.IServiceEntityReadOnly).getById<TEntityInput>(id);
                    promise.then(result => {
                        this.entity = result;
                        if (this.events.onGetEntity) {
                            this.events.onGetEntity(result);
                        }
                    }).finally(() => {
                        this.loadingEntity = false;
                    });
                }
            }

            this.save = (saveAndContinue?: boolean) => {
                if (this.canSave(this.entityService)) {
                    this.saving = true;

                    var doSave = () => {
                        var promise = (this.entityService as any as services.Services.IServiceEntitySave).save<TEntityInput, TIdInput>(this.entity);
                        promise.then(result => {
                            if (this.events.onAfterSaveEntity) {
                                this.events.onAfterSaveEntity(result);
                            }
                            abp.notify.info(app.localize('SavedSuccessfully'), "");
                            if (!saveAndContinue) {
                                this.paginaDeListagem();
                            }
                        }).finally(() => {
                            this.saving = false;
                        });
                    };

                    if (this.events.onBeforeSaveEntity)
                        this.events.onBeforeSaveEntity();

                    if (this.events.onBeforeSaveEntityExecutePromise) {
                        var promise = this.events.onBeforeSaveEntityExecutePromise();
                        promise.then(result => {
                            if (this.events.onBeforeSaveEntityPromiseResolved) {
                                this.events.onBeforeSaveEntityPromiseResolved(result);
                            }
                            doSave();
                        });
                    } else {
                        doSave();
                    }
                }
            }

            this.delete = (registro: TEntityList) => {
                if (this.canSave(this.entityService)) {
                    abp.message.confirm(
                        '', '',
                        isConfirmed => {
                            if (isConfirmed) {
                                var promise = (this.entityService as any as services.Services.IServiceEntityDelete).delete(registro.id);
                                promise.then(() => {
                                    abp.notify.info(app.localize('SuccessfullyDeleted'), '');
                                }).finally(() => {
                                    if (this.events.onDeletedEntity)
                                        this.events.onDeletedEntity(registro);
                                });
                            }
                        }
                    );
                }
            }

            this.cancel = () => {
                var abort: boolean = false;

                if (this.events.onBeforeCancel) {
                    abort = this.events.onBeforeCancel();
                }

                if (!abort) {
                    this.paginaDeListagem();
                }

                if (this.events.onAfterCancel) {
                    this.events.onAfterCancel();
                }
            }

            this.paginaDeListagem = () => {
                this.$state.go(this.rotaPaginaDeListagem);
            }
        }
    }

    export class ControllerFormComponentBaseNoEntity {
        static $inject = ['$state', '$stateParams', '$uibModal', 'uiGridConstants'];

        constructor(
            public $state: ng.ui.IStateService,
            public $stateParams: ng.ui.IStateParamsService,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public uiGridConstants: uiGrid.IUiGridConstants) {
            
            ezFixes.uiGrid.uiTab.registerFix();
        }
    }

}