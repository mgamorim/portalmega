﻿export interface ILoadingData {
    isLoadingData: (isLoading: boolean) => void;
}