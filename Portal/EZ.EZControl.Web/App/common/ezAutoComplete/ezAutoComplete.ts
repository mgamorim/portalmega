﻿import * as ezAutoCompleteComponent from "../../components/ezAutoComplete/ezAutoCompleteComponent"
export module EZAutoComplete {
    export class EZAutoComplete {
        private ezAutoCompleteController: ezAutoCompleteComponent.Components.EZAutoComplete.EZAutoCompleteController;
        public initialize: () => void;
        constructor(
            public querySearch: (query: string) => any,
            public onSelectRecord: (item: any) => void)
        {
            this.initialize = () => {
                this.ezAutoCompleteController.querySearch = this.querySearch;
                this.ezAutoCompleteController.onSelectRecord = this.onSelectRecord;
            }
        }
        setEZAutoCompleteController(controller: ezAutoCompleteComponent.Components.EZAutoComplete.EZAutoCompleteController) {
            this.ezAutoCompleteController = controller;
            this.initialize();
        }
    }
}