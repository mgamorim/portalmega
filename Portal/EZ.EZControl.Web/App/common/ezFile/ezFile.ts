﻿import * as ezFileComponent from "../../components/ezFile/ezFileComponent";
export module EZFile {
    export class EZFile {
        public controller: ezFileComponent.Components.EZFile.EZFileController;
        public initialize: () => void;
        public loadImage: () => void;
        public getParametros: () => void;
        public onUploadComplete: (fileName: string) => void;
        public onBeforeUpload: () => void;
        public tipoDocumento: TipoDeDocumentoEnum;
        public doSave: () => void;
        public loading: boolean;
        public inicializado: boolean;
        constructor() {
            this.doSave = () => {
                this.controller.doSaveFile();
            }
        }
        setEzFileController(controller: ezFileComponent.Components.EZFile.EZFileController) {
            if (!this.inicializado) {
                this.controller = controller;
                if (this.initialize) this.initialize();
                if (this.onBeforeUpload) this.controller.onBeforeUpload = this.onBeforeUpload;
                if (this.onUploadComplete) this.controller.onUploadComplete = this.onUploadComplete;
            }
        }
        setImageInfo(path: string, tipoDeArquivo: TipoDeArquivoEnum) {
            this.controller.imagem = path;
            this.controller.tipoArquivo = tipoDeArquivo;
        }
        setParametos(extensions: string) {
            this.controller.extensions = extensions;
        }
    }
}