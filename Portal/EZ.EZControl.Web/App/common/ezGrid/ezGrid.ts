﻿import * as requestParams from "../requestParams/requestParams";
import * as applicationServiceDtos from "../../application/services/dto/applicationServiceDtos";
import * as loadingData from "../interfaces/ILoadingData";
import * as wordToSearch from "../interfaces/IWordToSearch";
import * as services from "../services/serviceEntityBase";

export module EzGrid {

    export interface IEzGridEvents extends loadingData.ILoadingData {
        isLoadingData: (isLoading: boolean) => void;
        onCompletedGetRegistros: () => void;
    }

    export interface IEzGridCreateRecord {
        callback: () => void;
        permission: boolean;
        text: string;
        applySettings(text: string, callback: () => void, isGranted: boolean);
    }

    export interface IEzGridAppScopeCallBack {
        key: string;
        fn: (entity?: any) => any;
    }

    export interface IEzGridAppScopeProvider<TEntity> {
        actionLinks: EzGridActions<TEntity>[];
        localize: (word: string) => string;
        getEnumValue: (obj: TEntity, nomeEnumerado: string, fieldName: string) => any;
        getEnumValues: (obj: TEntity, nomeEnumerado: string, fieldName: string) => any;
        getSetDescendantProp(obj: any, desc: any, value: any): any;
        callBacks: Array<IEzGridAppScopeCallBack>;
        addCallBack: (callBack: IEzGridAppScopeCallBack) => void;
        executeCallBack: (key: string) => void;
        openRowMenu: (event: any) => void;
        closeRowMenu: (event: any) => void;
        detalhesPlano: (registro: TEntity) => void;
        selecionarRegistro: (registro: TEntity) => void;
        setSelected: (planoId: number) => void;
        executeAfterGetRegistros: () => void;
        operadoraSelecionada: any;
        operadoras: Array<any>;
        ativarOperadora: ($index: any, operadoraNome?: any, run?: boolean, primeiraExecucaoAbaSelecaoDePlano?: boolean) => void;
        scrollToOperadora: (seletor: string, primeiraExecucaoAbaSelecaoDePlano: boolean) => void;
        slickConfig: any;
        smoothScroll: (anchor: any) => void;
        trocarOperadoras: () => void;
        comportamentoSlickArrows: () => void;
        scrollList: () => void;
        replaceNomeOperadora: (nome: string) => string;
    }

    export interface IEzGridActions<TEntity> {
        text: string;
        visible: boolean | { (entity: TEntity): boolean };
        action(registro?: TEntity, row?: uiGrid.IGridRow): void;
    }

    export interface IEzGrid<TEntity> {
        gridApi: uiGrid.IGridApiOf<TEntity>;
        optionsGrid: uiGrid.IGridOptionsOf<TEntity>;
        appScopeProvider: IEzGridAppScopeProvider<TEntity>;
        wordToSearch: wordToSearch.IWordToSearch;
        ezGridCreateRecord: IEzGridCreateRecord;
        termoDigitadoPesquisa: string;
        getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any;
        entityService: services.Services.IServiceEntityReadOnly;
        loading: boolean;
    }

    export class EzGridCreateRecord implements IEzGridCreateRecord {
        callback: () => void;
        permission: boolean;
        text: string;
        applySettings(text: string, callback: () => void, isGranted: boolean) {
            this.callback = callback;
            this.permission = isGranted;
            this.text = app.localize(text);
        }
    }

    export class EzGridActions<TEntity> implements IEzGridActions<TEntity> {
        constructor(public action: (registro?: TEntity, row?: uiGrid.IGridRow) => void, public text: string, public visible: boolean | { (entity: TEntity): boolean }) {

        }
    }

    export class EzGridEvents implements IEzGridEvents {
        isLoadingData: (isLoading: boolean) => void;
        onCompletedGetRegistros: () => void;
    }

    export class EzGridAppScopeProvider<TEntity> implements IEzGridAppScopeProvider<TEntity> {
        actionLinks: EzGridActions<TEntity>[];

        constructor() {
            this.actionLinks = [];
            this.localize = app.localize;
            this.enums = ez.domain.enum;
            this.callBacks = new Array<IEzGridAppScopeCallBack>();
            this.getEnumValue = (obj: TEntity, nomeEnumerado: string, fieldName: string) => {
                var result = null;
                var enumOjb = this.enums[nomeEnumerado];
                for (var property in obj) {
                    if (obj.hasOwnProperty(property)) {
                        if (property === fieldName) {
                            var value = obj[property];
                            if (value) {
                                result = enumOjb.getItemPorValor(value).descricao;
                            }
                            break;
                        }
                    }
                }
                return result;
            }

            this.getEnumValues = (obj: TEntity, nomeEnumerado: string, fieldName: string) => {
                var lista = null;
                var enumOjb = this.enums[nomeEnumerado];
                for (var property in obj) {
                    if (obj.hasOwnProperty(property)) {
                        if (property === fieldName) {
                            var value = (obj[property] as any);
                            if (value != null) {
                                var item = value.split(',');
                                for (var i in item) {
                                    if (item.hasOwnProperty(i)) {
                                        if (lista === null) {
                                            lista = enumOjb.getItemPorValor(item[i]).descricao;
                                        } else {
                                            lista = lista + ' - ' + enumOjb.getItemPorValor(item[i]).descricao;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }

                return lista;
            }

            this.addCallBack = (callBack: IEzGridAppScopeCallBack) => {
                this.callBacks.push(callBack);
            }

            this.executeCallBack = (key: string, entity?: TEntity) => {
                var callBack: IEzGridAppScopeCallBack;
                for (let i = 0; i < this.callBacks.length; i++) {
                    if (key === this.callBacks[i].key) {
                        callBack = this.callBacks[i];
                        break;
                    }
                }

                if (callBack) {
                    return callBack.fn(entity);
                }
            }
        }

        enums: any;
        localize: (word: string) => string;
        callBacks: Array<IEzGridAppScopeCallBack>;
        addCallBack: (callBack: IEzGridAppScopeCallBack) => void;
        executeCallBack: (key: string) => void;
        getEnumValue: (obj: TEntity, nomeEnumerado: string, fieldName: string) => any;
        getEnumValues: (obj: TEntity, nomeEnumerado: string, fieldName: string) => any;

        getSetDescendantProp(obj: any, desc: string, value: string): any {
            var arr = desc ? desc.split(".") : [];

            while (arr.length && obj) {
                var comp = arr.shift();
                var match = new RegExp("(.+)\\[([0-9]*)\\]").exec(comp);

                if ((match !== null) && (match.length == 3)) {
                    var arrayData = {
                        arrName: match[1],
                        arrIndex: match[2]
                    };
                    if (obj[arrayData.arrName] !== undefined) {
                        if (value && arr.length === 0) {
                            obj[arrayData.arrName][arrayData.arrIndex] = value;
                        }
                        obj = obj[arrayData.arrName][arrayData.arrIndex];
                    } else {
                        obj = undefined;
                    }

                    continue;
                }

                if (value) {
                    if (obj[comp] === undefined) {
                        obj[comp] = {};
                    }

                    if (arr.length === 0) {
                        obj[comp] = value;
                    }
                }

                obj = obj[comp];
            }

            return obj;
        }

        openRowMenu: (event: any) => void;
        closeRowMenu: (event: any) => void;
        detalhesPlano: (registro: TEntity) => void;
        selecionarRegistro: (registro: TEntity) => void;
        setSelected: (planoId: number) => void;
        executeAfterGetRegistros: () => void;
        operadoraSelecionada: {
            id: 0,
            nome: '',
            index: 0
        };
        operadoras: Array<any>;
        ativarOperadora: ($index: any, operadoraNome?: any, run?: boolean) => void;
        scrollToOperadora: (seletor: string) => void;
        slickConfig: any = {
            dots: false,
            arrows: true,
            autoplay: false,
            initialSlide: 0,
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 1,
            enable: true,
            centerMode: true,
            centerPadding: '0px',
            focusOnSelect: false,
            draggable: true,
            event: {
                init: () => {
                    this.trocarOperadoras();
                    this.ativarOperadora(0);
                    this.scrollList();
                }
            }
        };
        trocarOperadoras: () => void;
        comportamentoSlickArrows: () => void;
        scrollList: () => void;
        smoothScroll: (anchor: any) => void;
        replaceNomeOperadora = (nome: string) => {
            return nome.slugify();
        }
    }

    export class EzGrid<TEntity, TRequestParams extends requestParams.RequestParam.IRequestParams> implements IEzGrid<TEntity>, wordToSearch.IWordToSearch {

        static $inject = ['uiGridConstants'];

        private gridData: TEntity[];
        public serviceGetCallback: <TEntityListDto>(filtro: any, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<TEntityListDto>>;

        public getRegistros: () => void;
        public onComponentDestroy: () => void;
        public clearFilter: () => void;
        public name: string;
        public isFiltered: boolean;

        // events
        public on: IEzGridEvents;

        // IEzGridWordToSearch
        public placeholderInputSearch: string;

        // IEzGrid
        public gridApi: uiGrid.IGridApiOf<TEntity>;
        public optionsGrid: uiGrid.IGridOptionsOf<TEntity>;
        public appScopeProvider: IEzGridAppScopeProvider<TEntity>;
        public wordToSearch: wordToSearch.IWordToSearch;
        public ezGridCreateRecord: IEzGridCreateRecord;
        public termoDigitadoPesquisa: string;
        public loading: boolean;

        public showPortletTitle: boolean = true;
        public isGridPlano: boolean = false;

        constructor(
            public uiGridConstants: uiGrid.IUiGridConstants,
            public entityService: services.Services.IServiceEntityReadOnly,
            public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any,
            // O request params será utilizado para fazer a paginação e ordenação do grid-ui
            public requestParams: TRequestParams) {

            this.on = new EzGridEvents();
            this.termoDigitadoPesquisa = '';
            this.loading = false;

            this.serviceGetCallback = entityService.getPaged;

            this.getRegistros = () => {
                this.loading = true;

                if (this.on.isLoadingData)
                    this.on.isLoadingData(true);

                var filtro: any;

                if (this.getFiltroParaPaginacao) {
                    filtro = this.getFiltroParaPaginacao(this.termoDigitadoPesquisa);
                } else {
                    throw ('É necessário implementar o callback getFiltroParaPaginacao');
                }

                var promise = this.serviceGetCallback.apply(entityService, [filtro, this.requestParams]);

                if (promise) {
                    promise.then(result => {
                        this.setDataGrid(result);

                        if (this.termoDigitadoPesquisa) {
                            this.isFiltered = true;
                        }

                        if (this.on.onCompletedGetRegistros) {
                            this.on.onCompletedGetRegistros()
                        }

                        if (this.appScopeProvider.executeAfterGetRegistros) {
                            this.appScopeProvider.executeAfterGetRegistros();
                        }

                    }).finally(() => {
                        this.loading = false;

                        if (this.on.isLoadingData)
                            this.on.isLoadingData(false);
                    });
                }
            }

            this.clearFilter = () => {
                this.termoDigitadoPesquisa = "";
                this.getRegistros();
                this.isFiltered = false;
            }

            this.wordToSearch = {
                placeholderInputSearch: app.localize('SearchWithThreeDot')
            }

            // ui grid
            this.appScopeProvider = new EzGridAppScopeProvider();
            this.optionsGrid = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.NEVER,
                enableVerticalScrollbar: this.uiGridConstants.scrollbars.NEVER,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                showColumnFooter: false,
                rowHeight: 50,
                enableGridMenu: false,
                exporterMenuCsv: false,
                exporterMenuPdf: false,
                rowTemplate: 'App/common/views/common/grid/linha.html',
                columnDefs: [],
                multiSelect: false,
                enableColumnResizing: true,
                enableInfiniteScroll: false,
                enablePagination: true,
                enablePaginationControls: true,
                showGridFooter: false,
                appScopeProvider: this.appScopeProvider,
                onRegisterApi: (api) => {
                    this.gridApi = api;
                    this.gridApi.core.on.sortChanged(null, (grid, sortColumns) => {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            this.requestParams.sorting = null;
                        } else {
                            this.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        this.getRegistros();
                    });
                    if (this.optionsGrid.enablePagination) {
                        this.gridApi.pagination.on.paginationChanged(null, (pageNumber, pageSize) => {
                            this.requestParams.skipCount = (pageNumber - 1) * pageSize;
                            this.requestParams.maxResultCount = pageSize;

                            this.getRegistros();
                        });
                    }
                }
            }


            this.gridData = new Array<TEntity>();
            this.optionsGrid.data = this.gridData;

            // evento que é disparado pelo EzGridComponentController
            this.onComponentDestroy = () => {
                this.gridData = null;
            }
        }

        setDataGrid(result: applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<TEntity>) {
            this.gridData.length = 0;

            for (var i = 0; i < result.items.length; i++) {
                this.gridData.push(result.items[i]);
            }

            this.optionsGrid.totalItems = result.totalCount;
        }
    }
}