﻿(function () {
    appModule.controller('common.views.layout.sidebar', [
        '$scope', '$state',
        function ($scope, $state) {
            $scope.$on('$includeContentLoaded', function () {
                Layout.initSidebar($state); // init sidebar
            });
        }
    ]);
})();