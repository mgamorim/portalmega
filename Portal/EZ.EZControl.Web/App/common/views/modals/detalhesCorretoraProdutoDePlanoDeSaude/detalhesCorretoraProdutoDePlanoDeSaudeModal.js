﻿(function () {
    appModule.controller('common.views.modals.detalhesCorretoraPlano', [
        '$scope',
        function ($scope) {
            var $ctrl = this;

            $ctrl.planoModal = $scope.$parent.$ctrl.planoModal;
            $ctrl.closeModal = $scope.$parent.$ctrl.closeCorretoraModal;

            $ctrl.enablePlanoNavigation = $scope.$parent.$ctrl.enablePlanoNavigation;
            $ctrl.modalCorretoraInstance = $scope.$parent.$ctrl.modalCorretoraInstance;

            $ctrl.ezGridCorretora = $scope.$parent.$ctrl.ezGridCorretora;
        }
    ]);
})();