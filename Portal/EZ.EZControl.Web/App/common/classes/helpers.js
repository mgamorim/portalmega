﻿window.ez = window.ez || {};
window.ez.helper = window.ez.helper || {};

/**
 * Overwrites obj1's values with obj2's and adds obj2's if non existent in obj1
 * @param obj1
 * @param obj2
 * @returns obj3 a new object based on obj1 and obj2
 */
ez.helper.mergeObjects = function EzHelperMergeObjects(objDefault, objChanged) {

    if (objChanged) {
        var objMerge = {};
        for (var attrname in objDefault) {
            objMerge[attrname] = objDefault[attrname];
        }
        for (var attrname in objChanged) {
            if (objChanged[attrname]) {
                objMerge[attrname] = objChanged[attrname];
            }
        }
        return objMerge;
    } else
        return objDefault;

}

ez.helper.getPropInScopeByString = function EzHelperGetPropInScopeByString($scope, str) {

    if (!str || !$scope) {
        throw ("É obrigatório os parametros $scope e string para esta função. Ex: ez.helper.getPropInScopeByString($scope, \"vm.minhapropriedade\") ");
    }

    var arr = str.split('.');
    var retorno = $scope;

    try {
        for (var i = 0; i < arr.length; i++) {
            retorno = retorno[arr[i]];
        }
    } catch (e) {
        throw ("Não foi possível recuperar a propriedade do scope pela string. Ex de variável suportada: vm.suavariavel.outravarivel \n" + e);
    }

    return retorno;
}

ez.helper.setPropInScopeByString = function EzHelperGetPropInScopeByString($scope, str, valueParam) {

    if (!str || !$scope || !valueParam) {
        throw ("É obrigatório os parametros $scope, string e value para esta função. Ex: ez.helper.getPropInScopeByString($scope, \"vm.minhapropriedade\", \"meu valor\") ");
    }

    var arr = str.split('.');
    var retorno = $scope;

    try {
        for (var i = 0; i < arr.length; i++) {

            var isLast = i === arr.length - 1;

            if (isLast) {
                retorno[arr[i]] = valueParam;
            } else {
                retorno = retorno[arr[i]];
            }
        }
    } catch (e) {
        throw ("Não foi possível setar a propriedade do scope pela string. Ex de variável suportada: vm.suavariavel.outravarivel \n" + e);
    }

    return retorno;
}

ez.helper.getPropInObjectByString = function EzHelperGetPropInScopeByString(obj, str) {

    if (!str || !obj) {
        throw ("É obrigatório os parametros obj e string para esta função. Ex: ez.helper.getPropInScopeByString(obj, \"vm.minhapropriedade\") ");
    }

    var arr = str.split('.');
    var retorno = obj;

    try {
        for (var i = 0; i < arr.length; i++) {
            retorno = retorno[arr[i]];
        }
    } catch (e) {
        throw ("Não foi possível recuperar a propriedade do scope pela string. Ex de variável suportada: obj.suavariavel.outravarivel \n" + e);
    }

    return retorno;
}