﻿export module RequestParam {
    export interface IRequestParams {
        skipCount: number;
        maxResultCount: number;
        sorting: string;
    }

    export class RequestParams implements IRequestParams {
        public skipCount: number;
        public maxResultCount: number;
        public sorting: string;

        constructor() {
            this.skipCount = 0;
            this.maxResultCount = app.consts.grid.defaultPageSize;
            this.sorting = '';
        }
    }
}