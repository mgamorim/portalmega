﻿import * as requestParams from "../requestParams/requestParams";
import * as ezGrid from "../ezGrid/ezGrid";
import * as ezSelectComponent from "../../components/ezSelect/ezSelectComponent";
import * as applicationServiceDtos from "../../application/services/dto/applicationServiceDtos";
import * as loadingData from "../interfaces/ILoadingData";
import * as wordToSearch from "../interfaces/IWordToSearch";
import * as services from "../services/serviceEntityBase";

export module EzSelect {

    export interface IEzSelectEvents {
        beforeOpenModalDialog: () => void;
    }

    export class EzSelectEvents implements IEzSelectEvents {
        beforeOpenModalDialog: () => void;
    }

    export class EzSelect<TEntity, TRequestParams extends requestParams.RequestParam.IRequestParams> {

        static $inject = ['uiGridConstants', '$uibModal'];
        private selecionarRegistro: (registro: TEntity) => void;
        private modalInstance: angular.ui.bootstrap.IModalServiceInstance;
        private ezSelectController: ezSelectComponent.Components.EzSelect.EzSelectController;
        public on: IEzSelectEvents;
        public ezGrid: ezGrid.EzGrid.EzGrid<TEntity, TRequestParams>;
        public onEzGridCreated: () => void;
        public onComponentDestroy: () => void;
        public name: string;
        public wordToSearch: wordToSearch.IWordToSearch;
        public loading: boolean;

        constructor(
            public uiGridConstants: uiGrid.IUiGridConstants,
            public entityService: services.Services.IServiceEntityReadOnly,
            public getFiltroParaPaginacao: (termoDigitadoPesquisa: string) => any,
            public requestParams: TRequestParams,
            public $uibModal: angular.ui.bootstrap.IModalService,
            public onSelectRecord: (registro: TEntity) => string,
            public onUnSelectRecord: () => void) {

            this.selecionarRegistro = (registro: TEntity) => {
                var valorTexto = this.onSelectRecord(registro);
                this.modalInstance.close();
                this.setInputText(valorTexto);
            };

            this.wordToSearch = {
                placeholderInputSearch: app.localize('SearchWithThreeDot')
            }

            this.on = new EzSelectEvents();
            this.loading = false;

            this.onComponentDestroy = () => {
                this.ezGrid = null;
            }
        }

        gridSelectOpen() {
            this.ezGrid = new ezGrid.EzGrid.EzGrid<TEntity, TRequestParams>(
                this.uiGridConstants,
                this.entityService,
                this.getFiltroParaPaginacao,
                this.requestParams);

            this.ezGrid.appScopeProvider.actionLinks.push(new ezGrid.EzGrid.EzGridActions(this.selecionarRegistro, 'Select', true));

            if (this.onEzGridCreated) {
                this.onEzGridCreated();
            } else {
                throw ('É necessário implementar o callback onInitializeGrid');
            }

            if (this.on.beforeOpenModalDialog) {
                this.on.beforeOpenModalDialog();
            }
            this.modalInstance = this.$uibModal.open({
                templateUrl: '/app/common/ezSelect/ezSelectModalTemplate.html',
                controller: ['$uibModalInstance', 'ezSelect', EzSelectModalController],
                controllerAs: '$ctrl',
                backdrop: 'static',
                resolve: {
                    ezSelect: () => {
                        return this;
                    }
                }
            });
        }

        gridSelectClear() {
            if (this.ezSelectController)
                this.ezSelectController.clearInputText();
            this.onUnSelectRecord();
        }

        setLinkSelecionarPrimeiraColunaGrid() {
            this.ezGrid.optionsGrid.columnDefs[0].cellTemplate = '\
                <div class="ui-grid-cell-contents">\
                    {{grid.appScope.getSetDescendantProp(row.entity, col.field, null)}} <br />\
                    <span class="ui-grid-cell-actions" ng-repeat="item in grid.appScope.actionLinks">\
                        <span>\
                            <a ng-if="item.visible" ng-click="item.action(row.entity)">{{grid.appScope.localize(item.text)}}</a>\
                        </span> \
                    </span>\
                </div>';
        }

        setEzGridController(controller: ezSelectComponent.Components.EzSelect.EzSelectController) {
            this.ezSelectController = controller;
        }

        setInputText(valor: string) {
            this.ezSelectController.setInputText(valor);
        }

        getInputText(): string {
            return this.ezSelectController.getInputText();
        }
    }

    export class EzSelectModalController {

        constructor(public $uibModalInstance: angular.ui.bootstrap.IModalServiceInstance, public ezSelect: any) {
        }

        close() {
            this.$uibModalInstance.close();
        }
    }
}