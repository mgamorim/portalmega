﻿import * as requestParams from "../requestParams/requestParams";
import * as applicationServiceDtos from "../../application/services/dto/applicationServiceDtos";

export module Services {

    export interface IServiceEntityBase {
        
    }

    export interface IServiceEntityGetSingle extends IServiceEntityBase {
        get: <TEntityInput>() => ng.IPromise<TEntityInput>;
    }

    export interface IServiceEntityGetPaged extends IServiceEntityBase {
        getPaged: <TEntityListDto>(filtro: any, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<TEntityListDto>>;
    }

    export interface IServiceEntityReadOnly extends IServiceEntityGetPaged{
        getById: <TEntityInput>(id: number) => ng.IPromise<TEntityInput>;
    }

    export interface IServiceEntitySave extends IServiceEntityBase{
        save: <TEntityInput, TIdInput>(input: TEntityInput) => ng.IPromise<TIdInput>;
    }

    export interface IServiceEntityDelete extends IServiceEntityBase {
        delete: (id: number) => ng.IPromise<any>;
    }

    export interface IServiceEntity extends IServiceEntityReadOnly, IServiceEntityDelete, IServiceEntitySave {
    }
}