﻿namespace ezFixes {

    function fixUiGridRender() {
        jQuery(window).resize();
    }

    export namespace uiGrid {
        export namespace uiTab {
            export function registerFix() {
                window.setTimeout(() => {
                    var element = jQuery('ul.nav.nav-tabs > li');
                    element.on('click', function (evt) {
                        fixUiGridRender();
                    });
                }, 0);
            }
        }

        export function fixGrid() {
            fixUiGridRender();
        }
    }
}