﻿export module EzScheduler {
    export interface IEzScheduler {
        tiposDeEventosPermitidos: Array<any>;
        sistema: any;
        options: kendo.ui.SchedulerOptions;
        dataSource: kendo.data.SchedulerDataSource;
        configureDataSource(): kendo.data.SchedulerDataSource;
        setScheduler(scde: kendo.ui.Scheduler);
        onSchedulerRendered: (scheduler: kendo.ui.Scheduler) => void;
    }

    export class EzScheduler implements IEzScheduler {
        tiposDeEventosPermitidos: Array<any>;
        sistema: any;
        options: kendo.ui.SchedulerOptions;
        setScheduler(scde: kendo.ui.Scheduler) {
            this.scheduler = scde;
        }
        onSchedulerRendered: (scheduler: kendo.ui.Scheduler) => void;

        constructor() {
            this.tiposDeEventosPermitidos = new Array<any>();
            this.setSchedulerDefaultOptions();
        }

        dataSource: kendo.data.SchedulerDataSource;

        public scheduler: kendo.ui.Scheduler;

        configureDataSource(): kendo.data.SchedulerDataSource {
            var dataSourceOptions: kendo.data.DataSourceOptions = {
                batch: true,
                serverFiltering: true,
                transport: {
                    read: {
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: '/api/services/app/agenda/eventoBase/read',
                        dataType: 'json'
                    },
                    update: {
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: '/api/services/app/agenda/eventoBase/update',
                        dataType: 'json'
                    },
                    create: {
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: '/api/services/app/agenda/eventoBase/create',
                        dataType: 'json'
                    },
                    destroy: {
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: '/api/services/app/agenda/eventoBase/destroy',
                        dataType: 'json'
                    },
                    parameterMap(options, operation) {
                        if (operation !== "read" && options.models) {
                            return kendo.stringify(options.models);
                        } else if (operation == "read") {
                            return kendo.stringify(options);
                        }
                    }
                },
                schema: {
                    data: (data: any) => {
                        if (data.hasOwnProperty('result') && data.result.hasOwnProperty('items')) {
                            return data.result.items;
                        } else if (data.hasOwnProperty('result')) {
                            return data.result;
                        } else {
                            return data;
                        }
                    },
                    parse: (data: any) => {
                        if (data.hasOwnProperty('result') && data.result.hasOwnProperty('items')) {
                            return data.result.items;
                        } else if (data.hasOwnProperty('result')) {
                            return data.result;
                        } else {
                            return data;
                        }
                    },
                    model: {
                        id: "id",
                        fields: {
                            id: { from: "id", editable: false, type: "number" },
                            title: { from: "titulo", defaulValue: "Sem título", validation: { required: true } },
                            start: { type: "date", from: "inicio" },
                            end: { type: "date", from: "termino" },
                            startTimezone: { from: "inicioTimezone" },
                            endTimezone: { from: "terminoTimezone" },
                            description: { from: "descricao" },
                            recurrenceId: { from: "recurrenceId" },
                            recurrenceRule: { from: "recurrenceRule" },
                            recurrenceException: { from: "recurrenceException" },
                            ownerId: { from: "ownerId", defaultValue: 1 },
                            isAllDay: { type: "boolean", from: "diaInteiro" },
                            tipoDeEvento: { type: "number", from: "tipoDeEvento", validation: { required: true } },
                            sistema: { type: "number", from: "sistema", validation: { required: true } },
                            disponivel: { type: "boolean", from: "disponivel" }
                        }
                    }
                }
            }

            // Por algum motivo, o datasource não funciona se caso for feito algo tipo
            //this.dataSource.options = dataSourceOptions;
            // Deve ser algum bug interno ou limitação do datasource
            this.dataSource = new kendo.data.SchedulerDataSource(dataSourceOptions);
            return this.dataSource;
        }

        setSchedulerDefaultOptions() {
            this.options = {
                date: new Date(),
                dateHeaderTemplate: "<strong class='k-nav-day'>#=kendo.toString(date, 'ddd dd/MM')#</strong>",
                workDayStart: new Date(2000, 1, 1, 0, 0, 0),
                workDayEnd: new Date(2099, 12, 31, 23, 59, 59),
                workWeekStart: 0,
                workWeekEnd: 6,
                allDaySlot: true,
                footer: false,
                currentTimeMarker: {
                    updateInterval: 100
                },
                editable: {
                    confirmation: false
                },
                minorTickCount: 2,
                views: [
                    {
                        type: "day", editable: false
                    },
                    {
                        type: "week", selected: true, editable: true
                    },
                    {
                        type: "month", editable: false
                    },
                    {
                        type: "agenda", editable: false
                    }
                ],
                timezone: "GMT/UTC",
                messages: {
                    recurrenceEditor: {
                        end: {
                            label: "Término",
                            after: "Após",
                            occurrence: "ocorrência(s)",
                            never: "nunca",
                            on: "Termina em"
                        }
                    }
                }
            }

            this.options.edit = (e) => {
                if (this.tiposDeEventosPermitidos.length > 1) {
                    var container = e.container;

                    var endContainer = container.find("[data-container-for=end]");

                    // BEGIN Tipo de Evento

                    var tipoDeEventoLabel = $('<div class="k-edit-label"><label for="end">Tipo de Evento</label></div>');
                    var selectTipoDeEvento = $(
                        '<div data-container-for="tipoDeEvento" class="k-edit-field">' +
                        '<select id="tipoDeEvento">' +
                        '</select>' +
                        '</div>');

                    endContainer.after(selectTipoDeEvento).after(tipoDeEventoLabel);

                    var eventos = [];

                    for (let i in this.tiposDeEventosPermitidos) {
                      let tipoDeEvento = ez.domain.enum.tipoDeEventoEnum.getItemPorValor(this.tiposDeEventosPermitidos[i].valor);
                      eventos.push(tipoDeEvento);
                    }

                    var tiposDeEventos = new kendo.data.ObservableArray(eventos);

                    var options: kendo.ui.DropDownListOptions = {
                        valuePrimitive: false,
                        dataTextField: "nome",
                        dataValueField: "valor",
                        dataSource: tiposDeEventos
                    };

                    var tipoDeEventoDropdown = container
                        .find('#tipoDeEvento')
                        .kendoDropDownList(options)
                        .data('kendoDropDownList');

                    tipoDeEventoDropdown.bind('change', (arg: kendo.ui.DropDownListEvent) => {
                        var value = arg.sender.value();
                        e.event.set('tipoDeEvento', value);
                    });

                    var tipoDeEventoValue = (e.event as any).tipoDeEvento;
                    var tipoDeEventoEnum = ez.domain.enum.tipoDeEventoEnum.getItemPorValor(tipoDeEventoValue);
                    if (angular.isDefined(tipoDeEventoEnum) && angular.isDefined(tipoDeEventoEnum)) {
                        tipoDeEventoDropdown.select(tipoDeEventoEnum.valor - 1);
                    }

                    // END Tipo de Evento
                }
            }
        };
    }
}