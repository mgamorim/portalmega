﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as produtoDtos from "../../estoque/produto/produtoDtos";
import * as servicoDtos from "../../servicos/servico/servicoDtos";

export module Dtos.ItemPedido {

    export class ItemDePedidoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        pedidoId: number;
        produto: produtoDtos.Dtos.Produto.ProdutoListDto;
        servico: servicoDtos.Dtos.Servico.ServicoListDto;
        tipoDeItemDePedido: number;
        quantidade: number;
        valor: number;
        tempId: number;

        static tempIdValue: number = 0;

        static cloneList(list: Array<ItemDePedidoListDto>): Array<ItemDePedidoListDto> {
            var newList = Array<ItemDePedidoListDto>();

            for (let i in list) {
              var newItem = new ItemDePedidoListDto();
              angular.merge(newItem, list[i]);
              newList.push(newItem);
            }
            return newList;
        }

        total(): number {
            return this.valor * this.quantidade;
        }

        nome(): string {
            if (this.produto) {
                return this.produto.nome;
            } else if (this.servico) {
                return this.servico.nome;
            }

            return '';            
        }

        constructor() {
            super();
            this.quantidade = 0;
            this.valor = 0;
            this.tempId = ItemDePedidoListDto.tempIdValue;
            ItemDePedidoListDto.tempIdValue = ItemDePedidoListDto.tempIdValue + -1;
        }
    }

    export class GetItemDePedidoInput {
        pedidoId: number;
    }
}