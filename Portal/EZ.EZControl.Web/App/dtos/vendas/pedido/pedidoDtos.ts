﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../global/pessoa/pessoaDtos";
import * as clienteDtos from "../../global/cliente/clienteDtos";
import * as itemDePedidoDtos from "../itemDePedido/itemDePedidoDtos";

export module Dtos.Pedido {

    export class PedidoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nomeCliente: string;
        clienteId: number;
        valor: number;
        status: PedidoStatusEnum;
        formaDePagamento: FormaDePagamentoEnum;
    }

    export class PedidoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        clienteId: number;
        items: Array<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>;
        tipo: PedidoTipoEnum;
        status: PedidoStatusEnum;
        formaDePagamento: FormaDePagamentoEnum;
        constructor() {
            super();
            this.items = new Array<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>();
            this.tipo = PedidoTipoEnum.Orcamento;
            this.status = PedidoStatusEnum.Aberto;
        }
    }

    export class GetPedidoInput {
        nomeCliente: string;
    }
}