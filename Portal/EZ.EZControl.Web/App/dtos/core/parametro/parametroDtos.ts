﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Parametro.Core {
    export class ParametroListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tamanhoMaximoMb: number;
        alturaMaximaPx: number;
        larguraMaximaPx: number;
        extensoesDocumento: string;
        extensoesImagem: string;
    }

    export class ParametroInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tamanhoMaximoMb: number;
        alturaMaximaPx: number;
        larguraMaximaPx: number;
        extensoesDocumento: string;
        extensoesImagem: string;
    }
}