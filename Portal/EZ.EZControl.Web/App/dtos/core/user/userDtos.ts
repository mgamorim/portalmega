﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.User.Core {
    export class UserEditDto {
        id: number;
        name: string;
        surname: string;
        userName: string;
        emailAddress: string;
    }

    export class GetUsersInput {
        filter: string;
        permission: string;
        role?: number;
    }

    export class UserListDto {
        id: number;
        name: string;
        surname: string;
        userName: string;
        emailAddress: string;
    }

    export class GetTipoDeUsuarioInput {
        tipoDeUsuario: TipoDeUsuarioEnum;
        userId: number;
        strRoles: string;
    }

    export class UserDto {
        id: number;
    }
}