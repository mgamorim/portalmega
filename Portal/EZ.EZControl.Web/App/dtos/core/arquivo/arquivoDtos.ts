﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Arquivo.Core {
    export class ArquivoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        conteudo: any[];
        path: string;
        tipoDeArquivoFixo: TipoDeArquivoEnum;
        tipoDeArquivoFixoValue: string;
        token: string;
        isImagem: boolean;
    }

    export class ArquivoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        idInput: applicationServiceDtos.Dtos.ApplicationService.IdInput;
        isActive: boolean;
        nome: string;
        conteudo: any[];
        path: string;
        tipoDeArquivoFixo: TipoDeArquivoEnum;
        token: string;
        isImagem: boolean;
    }
}