﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Picture.Core {
    export class PictureListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        fileName: string;
        x: number;
        y: number;
        width: number;
        height: number;
        isActive: boolean;
    }

    export class PictureInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        fileName: string;
        x: number;
        y: number;
        width: number;
        height: number;
        isActive: boolean;
    }
}