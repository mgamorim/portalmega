﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.Fornecedor {
    export class FornecedorListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto;
    }

    export class FornecedorInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInputAux;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInputAux;
    }

    export class FornecedorPessoaIdDto {
        fornecedorId: number;
        pessoaId: number;
    }

    export class GetFornecedorInput {
        nome: string;
        numeroDocumento: string;
        tipoDeDocumento: any;
    }

    export class GetPessoaExceptForFornecedor {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
    }
}