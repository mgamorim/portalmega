﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as tipoDeDocumentoDtos from "../tipoDeDocumento/tipoDeDocumentoDtos";
import * as estadoDtos from "../estado/estadoDtos";
import * as cidadeDtos from "../cidade/cidadeDtos";
import * as tipoDeLogradouroDtos from "../tipoDeLogradouro/tipoDeLogradouroDtos";
import * as documentoDtos from "../documento/documentoDtos";
import * as grupoPessoaDtos from "../grupoPessoa/grupoPessoaDtos";
import * as enderecoEletronicoDtos from "../enderecoEletronico/enderecoEletronicoDtos";
import * as telefoneDtos from "../telefone/telefoneDtos";
import * as enderecoDtos from "../endereco/enderecoDtos";

export module Dtos.Pessoa {

    // pessoa
    export class PessoaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nomePessoa: string;
        documentoPrincipal: documentoDtos.Dtos.Documento.DocumentoListDto;
        documentoPrincipalId: number;
        grupoPessoa: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto;
        grupoPessoaId: number;
        tipoPessoa: any;
        tipoPessoaValue: string;
        observacao: string;
        dataDeNascimento: Date;
        emailPrincipal: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput;
        telefoneComercial: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefoneCelular: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefoneResidencial: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefoneRecados: telefoneDtos.Dtos.Telefone.TelefoneInput;
        enderecoPrincipal: enderecoDtos.Dtos.Endereco.EnderecoInput;
        telefoneCelularId: number;
        email: string;
        celular: string;
    }

    export class PessoaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        documentoPrincipal: documentoDtos.Dtos.Documento.DocumentoInput;
        grupoPessoaId: number;
        tipoPessoa: any;
        observacao: string;
        emailPrincipal: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput;
        telefoneComercial: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefoneCelular: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefoneResidencial: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefoneRecados: telefoneDtos.Dtos.Telefone.TelefoneInput;
        enderecoPrincipal: enderecoDtos.Dtos.Endereco.EnderecoInput;
        nomePessoa: string;
        telefoneCelularId: number;
    }

    export class GetPessoaInput {
        nome: string;
        descricaoGrupoPessoa: string;
        tipoPessoa: any;
    }

    export class GetPessoaExceptForId {
        nome: string;
        tipoPessoa: any;
        ids: number[];
    }


    // pessoa fisica
    export class PessoaFisicaListDto extends PessoaListDto {
        nome: string;
        tratamentoId: number;
        sexo: any;
        possuiFilhos: boolean;
        estadoCivil: any;
        dataDeNascimento: Date;
        nacionalidade: string;
        cpfId: number;
        cpf: string;
        rgId: number;
        rg: string;
        orgaoExpedidor: string;
    }

    export class PessoaFisicaInput extends PessoaInput {
        nome: string;
        tratamentoId: number;
        sexo: any;
        possuiFilhos: boolean;
        estadoCivil: any;
        dataDeNascimento: Date;
        nacionalidade: string;
        cpfId: number;
        cpf: string;
        rgId: number;
        rg: string;
        orgaoExpedidor: string;
    }

    export class PessoaFisicaInputAux {
        nome: string;
        tratamentoId: number;
        sexo: any;
        possuiFilhos: boolean;
        estadoCivil: any;
        dataDeNascimento: Date;
    }

    export class GetPessoaFisicaByCPFInput {
        cpfPessoaFisica: string;
    }

    export class PessoaFisicaByCPFListDto extends PessoaListDto {
        //Id: number;
        nome: string;
        cpf: string;
        cpfId: number;
        grupoPessoaId: number;
        grupoPessoaNome: string;
    }

    export class GetPessoaExceptForIdInput {
        Id: number;
        nome: string;
        tipoDePessoa: TipoPessoaEnum;
        ids: number[];
    }


    // pessoa juridica
    export class PessoaJuridicaListDto extends PessoaListDto {
        razaoSocial: string;
        nomeFantasia: string;
    }

    export class PessoaJuridicaInput extends PessoaInput {
        razaoSocial: string;
        nomeFantasia: string;
    }

    export class PessoaJuridicaInputAux {
        razaoSocial: string;
        nomeFantasia: string;
    }
}