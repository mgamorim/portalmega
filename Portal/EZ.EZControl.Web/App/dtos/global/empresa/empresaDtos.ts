﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.Empresa {
    export class EmpresaListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        clienteEzId: number;
        razaoSocial: string;
        nomeFantasia: string;
        slug: string;
        cnpj: string;
    }

    export class EmpresaNomeListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
    }

    export class EmpresaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        razaoSocial: string;
        nomeFantasia: string;
        cnpj: string;
        slug: string;
        clienteEzId: number;
    }

    export class EmpresaPessoaIdDto {
        empresaId: number;
        pessoaId: number;
    }

    export class GetEmpresaInput {
        nomeFantasia: string;
        cnpj: string;
        slug: string;
    }

    export class GetPessoaExceptForEmpresa {
        nomePessoa: string;
        nomeFantasia: string;
    }

    export class AssociacaoUsuarioEmpresaInput {
        empresaId: number;
        usuarioId: number;
    }
}