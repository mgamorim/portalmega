﻿import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.EZLiv.Perfil {
    export class PerfilInput extends pessoaDtos.Dtos.Pessoa.PessoaFisicaInput {
        CorretorDadosBancarioInput: CorretorDadosBancarioInput;
    }

    export class PerfilListDto extends pessoaDtos.Dtos.Pessoa.PessoaListDto {
    }

    export class GetPerfilInput {
        nome: string;
    }

    export class CorretorDadosBancarioInput {
        bancoId: number;
        tipoConta: string;
    }

}