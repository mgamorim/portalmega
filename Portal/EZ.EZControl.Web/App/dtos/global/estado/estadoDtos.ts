﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as paisDtos from "../pais/paisDtos";

export module Dtos.Estado {

    export class EstadoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        isActive: boolean;
        sigla: string;
        capital: string;
        codigoIBGE: string;
        pais: paisDtos.Dtos.Pais.PaisListDto;
        paisId: number;
    }

    export class EstadoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        sigla: string;
        capital: string;
        codigoIBGE: string;
        paisId: number;
    }

    export class GetEstadoInput {
        nomeEstado: string;
        siglaEstado: string;
        capital: string;
        codigoIBGE: string;
        nomePais: string;
    }

    export class GetEstadoByPaisInput {
        paisId: number;
        nomeEstado: string;
        siglaEstado: string;
    }
}