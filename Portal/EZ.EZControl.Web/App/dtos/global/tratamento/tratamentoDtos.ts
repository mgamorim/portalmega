﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Tratamento {
    export class TratamentoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
    }

    export class TratamentoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
    }

    export class GetTratamentoInput {
        descricao: string;
    }
}