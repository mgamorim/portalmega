﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.EnderecoEletronico {
    export class EnderecoEletronicoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoaId: number;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        endereco: string;
        tipoDeEnderecoEletronico: any;
        tipoDeEnderecoEletronicoValue: string;
    }

    export class EnderecoEletronicoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoaId: number;
        endereco: string;
        tipoDeEnderecoEletronico: TipoDeEnderecoEletronicoEnum;
        contatoId: number;
    }

    export class GetEnderecoEletronicoInput {
        pessoaId: number;
        endereco: string;
        tipoDeEnderecoEletronico: any;
    }
}