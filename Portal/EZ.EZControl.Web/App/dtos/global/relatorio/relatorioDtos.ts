﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Global.Geral {
    export class RelatorioInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
      isActive: boolean;
      nome: string;
      sistema: any;
      tipoDeRelatorio: any;
      conteudo: string;
    }

    export class RelatorioListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
      isActive: boolean;
      nome: string;
      sistema: any;
      tipoDeRelatorio: any;
      conteudo: string;
    }

    export class GetRelatorioInput {
      nome: string;
      sistema: any;
      tipoDeRelatorio: number;
    }

    export class GetRelatorioExceptForIdInput{
      relatorioId: number;
      tipoDeRelatorio: number;
      nome: string;
      relatorios: RelatorioInput[]
    }
}