﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Agencia {

    export class AgenciaListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigo: string;
        digitoVerificador: string;
        bancoId: number;
    }

    export class AgenciaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigo: string;
        digitoVerificador: string;
        bancoId: number;
    }

    export class GetAgenciaInput {
        codigoBanco: string;
        nomeBanco: string;
        codigo: string;
        nome: string;
    }
}