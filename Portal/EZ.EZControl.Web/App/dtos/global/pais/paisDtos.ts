﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Pais {

    export class PaisListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        isActive: boolean;
        nacionalidade: string;
        codigoReceitaFederal: string;
        codigoInternacional: string;
        codigoISO: string;
        mascaraDoCodigoPostal: string;
        sigla2Caracteres: string;
        sigla3Caracteres: string;
    }

    export class PaisInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        nacionalidade: string;
        codigoReceitaFederal: string;
        codigoInternacional: string;
        codigoISO: string;
        mascaraDoCodigoPostal: string;
        sigla2Caracteres: string;
        sigla3Caracteres: string;
    }

    export class GetPaisInput {
        nomePais: string;
        nacionalidade: string;
        codigoISO: string;
    }
}