﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.RamoDoFornecedor {
    export class RamoDoFornecedorListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
    }

    export class RamoDoFornecedorInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
    }

    export class GetRamoDoFornecedorInput {
        nome: string;
    }
}