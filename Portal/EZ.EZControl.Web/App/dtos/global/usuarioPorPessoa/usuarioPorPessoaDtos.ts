﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.UsuarioPorPessoa {
    // pessoa
    export class UsuarioPorPessoaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        user: UserEditDto;
        userId: number;
        roleName: string;
        sendActivationEmail: boolean;
        setRandomPassword: boolean;
    }

    export class UsuarioPorPessoaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        user: UserEditDto;
        userId: number;
        roleName: string;
        sendActivationEmail: boolean;
        setRandomPassword: boolean;
    }

    export class UsuarioPorPessoaPessoaIdDto {
        usuarioPorPessoaId: number;
        usuarioId: number;
        pessoaId: number;
    }

    export class UsuarioPessoaFisicaListDto extends pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto {
        emailPessoal: string;
        cpf: string;
        rg: string;
    }

    export class GetPessoaExceptForUsuario {
        nome: string;
    }

    export class GetUsuarioInput {
        nome: string;
    }

    export class UserEditDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        name: string;
        surname: string;
        userName: string;
        emailAddress: string;
        phoneNumber: string;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        tipoDeUsuario: TipoDeUsuarioEnum;
        password: string;
        isActive: boolean;
        shouldChangePasswordOnNextLogin: boolean;
    }

}