﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as estadoDtos from "../estado/estadoDtos";

export module Dtos.Cidade {

    export class CidadeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        isActive: boolean;
        codigoIBGE: string;
        estadoId: number;
        estado: estadoDtos.Dtos.Estado.EstadoListDto;
    }

    export class CidadeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigoIBGE: string;
        paisId: number;
        estadoId: number;
    }

    export class GetCidadeInput {
        nomeCidade: string;
        codigoIBGE: string;
        nomeEstado: string;
        nomePais: string;
    }

    export class GetCidadeByEstadoInput {
        estadoId: number;
        nomeCidade: string;
    }
}