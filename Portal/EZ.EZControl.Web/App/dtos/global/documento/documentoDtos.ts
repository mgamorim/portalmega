﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as tipoDeDocumentoDtos from "../tipoDeDocumento/tipoDeDocumentoDtos";
import * as estadoDtos from "../estado/estadoDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.Documento {
    export class DocumentoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        pessoaId: number;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        tipoDeDocumentoId: number;
        tipoDeDocumento: tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto;
        tipoDeDocumentoFixoValue: any;
        numero: string;
        orgaoExpedidor: string;
        dataExpedicao: Date;
        estadoId: number;
        estado: estadoDtos.Dtos.Estado.EstadoListDto;
        dataDeValidade: Date;
        serie: string;}

    export class DocumentoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        pessoaId: number;
        tipoDeDocumentoId: number;
        numero: string;
        orgaoExpedidor: string;
        dataExpedicao: Date;
        estadoId: number;
        dataDeValidade: Date;
        serie: string;
    }

    export class GetDocumentoInput {
        pessoaId: number;
        numero: string;
    }
}