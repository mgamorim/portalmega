﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.ClienteEz {
    export class ClienteEZDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        razaoSocial: string;
        documentoPrincipal: string;
    }

    export class ClienteEZInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        razaoSocial: string;
        documentoPrincipal: string;
    }

    export class ClienteEZPessoaIdDto {
        clienteEZId: number;
        pessoaId: number;
    }
}