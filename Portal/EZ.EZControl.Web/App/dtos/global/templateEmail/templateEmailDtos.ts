﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.TemplateEmail {
    export class TemplateEmailListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        remetente: string;
        assunto: string;
        corpoMensagem: string;
        tipoDeTemplateDeMensagem: any;
    }

    export class TemplateEmailDto {
        remetente: string;
        assunto: string;
        corpoMensagem: string;
    }

    export class TemplateEmailInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        remetente: string;
        assunto: string;
        corpoMensagem: string;
        tipoDeTemplateDeMensagem: any;
    }

    export class GetTemplateEmailInput {
        nome: string;
        remetente: string;
        assunto: string;
    }
}