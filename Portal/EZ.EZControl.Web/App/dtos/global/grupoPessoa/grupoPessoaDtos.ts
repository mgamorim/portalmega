﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.GrupoPessoa {
    export class GrupoPessoaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntityRecursive {
        isActive: boolean;
        descricao: string;
        tipoDePessoa: TipoPessoaEnum;
        slug: string;
    }

    export class GrupoPessoaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntityRecursive {
        isActive: boolean;
        descricao: string;
        tipoDePessoa: TipoPessoaEnum;
        slug: string;
    }

    export class GetGrupoPessoaInput {
        nome: string;
    }

    export class GetGrupoPessoaExceptForIdInput {
        grupoPessoaId: number;
        nome: string;
        tipoDePessoa: TipoPessoaEnum;
    }
}