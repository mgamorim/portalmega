﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as parametroCoreDtos from "../../core/parametro/parametroDtos";

export module Dtos.Parametro.Global {
    export class ParametroGlobalListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        conexaoSMTPId: number;
        conexaoSMTPNome: string;
        conexaoSMTPEnderecoDeEmailPadrao: string;
        conexaoSMTPServidorSMTP: string;
        conexaoSMTPPorta: number;
        conexaoSMTPUsuarioAutenticacao: string;
        conexaoSMTPSenhaAutenticacao: string;
        conexaoSMTPRequerAutenticacao: boolean;
        conexaoSMTPRequerConexaoCriptografada: boolean;
    }

    export class ParametroGlobalInput extends parametroCoreDtos.Dtos.Parametro.Core.ParametroInput {
        conexaoSMTPId: number;
    }
}