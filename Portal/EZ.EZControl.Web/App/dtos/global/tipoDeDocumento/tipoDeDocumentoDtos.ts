﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.TipoDeDocumento {
    export class TipoDeDocumentoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoPessoa: any;
        tipoPessoaValue: string;
        tipoDeDocumentoFixo: any;
        tipoDeDocumentoFixoValue: string;
        descricao: string;
        mascara: string;
        principal: boolean;
        obrigaAnexo: boolean;
        prazoDeAtualizacaoDoAnexo: number;
        prioridade: number;
        obrigaOrgaoExpedidor: boolean;
        obrigaDataExpedicao: boolean;
        obrigaUF: boolean;
        obrigaDataDeValidade: boolean;
        serieObrigatoria: boolean;
        listaEmOutrosDocumentos: boolean;
        exibirNoCadastroSimplificadoDePessoa: boolean;
        permiteLancamentoDuplicado: boolean;
    }

    export class TipoDeDocumentoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoPessoa: any;
        tipoDeDocumentoFixo: any;
        descricao: string;
        mascara: string;
        principal: boolean;
        obrigaAnexo: boolean;
        prazoDeAtualizacaoDoAnexo: number;
        prioridade: number;
        obrigaOrgaoExpedidor: boolean;
        obrigaDataExpedicao: boolean;
        obrigaUF: boolean;
        obrigaDataDeValidade: boolean;
        serieObrigatoria: boolean;
        listaEmOutrosDocumentos: boolean;
        exibirNoCadastroSimplificadoDePessoa: boolean;
        permiteLancamentoDuplicado: boolean;
    }

    export class GetTipoDeDocumentoInput {
        descricao: string;
        tipoPessoa: any;
        tipoDeDocumento: TipoDeDocumentoEnum;
    }
}