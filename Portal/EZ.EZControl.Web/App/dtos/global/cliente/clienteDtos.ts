﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.Cliente {
    export class ClienteListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto;
    }

    export class ClienteInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInputAux;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInputAux;
    }

    export class ClientePessoaIdDto {
        clienteId: number;
        pessoaId: number;
    }

    export class GetClienteInput {
        nome: string;
        numeroDocumento: string;
        tipoDeDocumento: any;
    }

    export class GetPessoaExceptForCliente {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
    }
}