﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Profissao {
    export class ProfissaoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        codigo: string;
        titulo: string;
    }

    export class ProfissaoAutoCompleteListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        display: string;
        value: string;
    }

    export class ProfissaoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        codigo: string;
        titulo: string;
    }

    export class GetProfissaoInput {
        codigo: string;
        titulo: string;
    }
}