﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";

export module Dtos.Telefone {
    export class TelefoneListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoaId: number;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        ddi: string;
        ddd: string;
        numero: string;
        ramal: string;
        tipo: TipoTelefoneEnum;
        //tipoValue: string;
    }

    export class TelefoneInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoaId: number;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        ddi: string;
        ddd: string;
        numero: string;
        ramal: string;
        tipo: TipoTelefoneEnum;
        //tipoValue: string;
    }

    export class GetTelefoneInput {
        id: number;
        pessoaId: number;
        numero: string;
        tipo: TipoTelefoneEnum;
    }
}