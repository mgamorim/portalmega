﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.ConexaoSmtp {

    export class ConexaoSmtpListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        enderecoDeEmailPadrao: string;
        servidorSMTP: string;
        porta: number;
        usuarioAutenticacao: string;
        senhaAutenticacao: string;
        requerAutenticacao: boolean;
        requerConexaoCriptografada: boolean;
    }

    export class ConexaoSmtpInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        enderecoDeEmailPadrao: string;
        servidorSMTP: string;
        porta: number;
        usuarioAutenticacao: string;
        senhaAutenticacao: string;
        requerAutenticacao: boolean;
        requerConexaoCriptografada: boolean;
    }

    export class GetConexaoSmtpInput {
        nome: string;
        servidorSMTP: string;
    }
}