﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as arquivoDtos from "../../core/arquivo/arquivoDtos";

export module Dtos.Arquivo.Global {
    export class ArquivoGlobalListDto extends arquivoDtos.Dtos.Arquivo.Core.ArquivoListDto {
    }

    export class ArquivoGlobalInput extends arquivoDtos.Dtos.Arquivo.Core.ArquivoInput {
    }

    export class GetArquivoGlobalInput {
        nome: string;
    }
}