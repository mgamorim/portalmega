﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";
import * as cidadeDtos from "../cidade/cidadeDtos";
import * as contatoDtos from "../contato/contatoDtos";
import * as tipoDeLogradouroDtos from "../tipoDeLogradouro/tipoDeLogradouroDtos";

export module Dtos.Endereco {
    export class EnderecoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoEndereco: any;
        tipoEnderecoValue: string;
        descricao: string;
        logradouro: string;
        numero: string;
        complemento: string;
        bairro: string;
        cep: string;
        referencia: string;
        enderecoParaEntrega: string;
        cidadeId: number;
        cidade: cidadeDtos.Dtos.Cidade.CidadeListDto;
        pessoaId: number;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        contatoId?: number;
        contato: contatoDtos.Dtos.Contato.ContatoListDto;
        tipoDeLogradouroId: number;
        tipoDeLogradouro: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto;
        enderecoCompleto: string;
    }

    export class EnderecoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoEndereco: any;
        descricao: string;
        logradouro: string;
        numero: string;
        complemento: string;
        bairro: string;
        cep: string;
        referencia: string;
        enderecoParaEntrega: boolean;
        cidadeId: number;
        pessoaId: number;
        contatoId?: number;
        tipoDeLogradouroId: number;
    }

    export class GetEnderecoInput {
        pessoaId: number;
        descricao: string;
    }

    export class GetEnderecoByCepInput {
        cep: string;
    }
}