﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../pessoa/pessoaDtos";
import * as tipoDeContatoDtos from "../tipoDeContato/tipoDeContatoDtos";
import * as tratamentoDtos from "../tratamento/tratamentoDtos";
import * as enderecoDtos from "../endereco/enderecoDtos";

export module Dtos.Contato {
    export class ContatoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoaId: number;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaListDto;
        tipoDeContatoId: number;
        tipoDeContato: tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto;
        tratamentoId: number;
        tratamento: tratamentoDtos.Dtos.Tratamento.TratamentoListDto;
        enderecoPrincipalId: number;
        enderecoPrincipal: enderecoDtos.Dtos.Endereco.EnderecoListDto;
        nome: string;
        dataNascimento: Date;
        sexo: any;
        sexoValue: string;
        cargo: string;
        setor: string;
        idioma: string;
        observacao: string;
    }

    export class ContatoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        cargo: string;
        setor: string;
        idioma: string;
        pessoaId: number;
        sexo: any;
        tipoDeContatoId: number;
        tratamentoId: number;
        enderecoPrincipalId: number;
    }

    export class GetContatoInput {
        pessoaId: number;
        tipoDeContatoId: number;
        tratamentoId: number;
        nome: string;
        cargo: string;
        setor: string;
    }
}