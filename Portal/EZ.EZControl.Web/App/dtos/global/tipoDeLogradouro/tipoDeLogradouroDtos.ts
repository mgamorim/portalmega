﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.TipoDeLogradouro {
    export class TipoDeLogradouroListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
    }

    export class TipoDeLogradouroInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
    }

    export class GetTipoDeLogradouroInput {
        descricao: string;
    }
}