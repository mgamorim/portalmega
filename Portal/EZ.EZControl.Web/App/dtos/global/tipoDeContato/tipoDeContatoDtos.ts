﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.TipoDeContato {
    export class TipoDeContatoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        mascara: string;
        tipoDeContatoFixo: any;
        tipoDeContatoFixoValue: any;
    }

    export class TipoDeContatoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoDeContatoFixo: any;
        mascara: string;
        descricao: string;
    }

    export class GetTipoDeContatoInput {
        descricao: string;
    }
}