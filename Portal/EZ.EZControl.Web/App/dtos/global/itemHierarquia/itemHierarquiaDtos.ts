﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.ItemHierarquia {
    export class ItemHierarquiaListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigo: string;
        mascara: string;
        itemHierarquiaPaiId: number;
    }

    export class ItemHierarquiaInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigo: string;
        mascara: string;
        itemHierarquiaPaiId: number;
    }

    export class GetItemHierarquiaInput {
        nome: string;
        codigo: string;
        mascara: string;
    }

    export class GetItemHierarquiaExceptForIdInput {
        itemHierarquiaId: number;
        nome: string;
    }
}