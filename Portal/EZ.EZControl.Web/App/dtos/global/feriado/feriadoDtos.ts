﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Feriado {
    export class FeriadoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        data: Date;
        abrangencia: any;
        tipo: any;
        paisId: number;
        estadoId: number;
        cidadeId: number;
    }

    export class FeriadoInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        data: Date;
        abrangencia: any;
        tipo: any;
        paisId: number;
        estadoId: number;
        cidadeId: number;
    }

    export class GetFeriadoInput {
        data: Date;
        nome: string;
    }
}