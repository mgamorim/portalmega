﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ItemDeDeclaracaoDeSaudeDtos from
    "App/dtos/ezliv/geral/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeDtos";
import * as ProponenteTitularDtos from
    "App/dtos/ezliv/subtipoPessoa/proponenteTitular/proponenteTitularDtos";
import * as DependenteDtos from
    "App/dtos/ezliv/subtipoPessoa/dependente/dependenteDtos";

export module Dtos.EZLiv.Geral {
    export class DeclaracaoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        titular: ProponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput;
        titularId: number;
        dependentes: DependenteDtos.Dtos.EZLiv.Geral.DependenteInput[];
        dependentesIds: number[];
        itensDeDeclaracaoDeSaude: ItemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput[];
        itensDeDeclaracaoDeSaudeIds: number[];
    }

    export class DeclaracaoDeSaudeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        titular: ProponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularListDto;
        titularId: number;
        dependentes: DependenteDtos.Dtos.EZLiv.Geral.DependenteListDto[];
        dependentesIds: number[];
        itensDeDeclaracaoDeSaude: ItemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeListDto[];
        itensDeDeclaracaoDeSaudeIds: number[];
    }

    export class GetDeclaracaoDeSaudeInput {
        nome: string;
    }
}