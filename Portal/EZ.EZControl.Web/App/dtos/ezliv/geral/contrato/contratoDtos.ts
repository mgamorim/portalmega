﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as valorPorFaixaEtariaDtos from "App/dtos/ezliv/geral/valorPorFaixaEtaria/valorPorFaixaEtariaDtos";

export module Dtos.EZLiv.Geral {
    export class ContratoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        dataCriacao: Date;
        dataInicioVigencia: Date;
        dataFimVigencia: Date;
        conteudo: string;
        produtoDePlanoDeSaudeId: number;
        valores: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput[];
    }

    export class ContratoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;                     
        dataCriacao: Date;
        dataInicioVigencia: Date;
        dataFimVigencia: Date;
        conteudo: string;
        produtoDePlanoDeSaude: any;
        produtoDePlanoDeSaudeId: number;
        valores: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto[];
        conteudoOnline: string;
    }

    export class GetContratoInput  {
        conteudo: string;
        produtoDePlanoDeSaudeId: number;
    }
}