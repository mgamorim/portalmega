﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as operadoraDtos from "App/dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as itemDeDeclaracaoDtos from "App/dtos/ezliv/geral/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeDtos";

export module Dtos.EZLiv.Geral {
    export class QuestionarioDeDeclaracaoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        operadora: operadoraDtos.Dtos.EZLiv.Geral.OperadoraInput;
        operadoraId: number;
        itensDeDeclaracaoDeSaude: itemDeDeclaracaoDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput[];
    }

    export class QuestionarioDeDeclaracaoDeSaudeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        operadora: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto;
        operadoraId: number;
        itensDeDeclaracaoDeSaude: itemDeDeclaracaoDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeListDto[];
    }

    export class GetQuestionarioDeDeclaracaoDeSaudeInput {
        nome: string;
    }
}