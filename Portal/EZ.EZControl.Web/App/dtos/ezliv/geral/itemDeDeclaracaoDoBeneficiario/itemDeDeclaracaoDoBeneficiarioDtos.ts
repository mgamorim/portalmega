﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class ItemDeDeclaracaoDoBeneficiarioInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pergunta: string;
        ajuda: string;
        ordenacao: number;
    }

    export class ItemDeDeclaracaoDoBeneficiarioListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pergunta: string;
        ajuda: string;
        ordenacao: number;
    }

    export class GetItemDeDeclaracaoDoBeneficiarioInput {
        pergunta: string;
    }
}