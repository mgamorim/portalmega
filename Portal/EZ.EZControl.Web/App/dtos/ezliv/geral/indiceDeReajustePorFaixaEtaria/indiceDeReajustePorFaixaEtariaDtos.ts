﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as operadoraDtos from "App/dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as faixaEtariaDtos from "App/dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";

export module Dtos.EZLiv.Geral {
    export class IndiceDeReajustePorFaixaEtariaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        operadora: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto;
        operadoraId: number;
        faixaEtaria: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput;
        faixaEtariaId: number;
        indiceDeReajuste: number;
    }

    export class IndiceDeReajustePorFaixaEtariaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        operadora: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto;
        operadoraId: number;
        faixaEtaria: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto;
        faixaEtariaId: number;
        indiceDeReajuste: number;
    }

    export class GetIndiceDeReajustePorFaixaEtariaInput {
        indiceDeReajuste: number;
    }
}