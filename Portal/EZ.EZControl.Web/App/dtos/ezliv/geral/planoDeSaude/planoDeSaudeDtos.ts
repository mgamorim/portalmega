﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class PlanoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        numeroDeRegistro: string;
        reembolso: boolean;
        descricaoDoReembolso: string;
        abrangencia: AbrangenciaDoPlanoEnum;
        segmentacaoAssistencial: SegmentacaoAssistencialDoPlanoEnum;
        planoDeSaudeAns?: PlanoDeSaudeAnsEnum;
        acomodacao?: AcomodacaoEnum;
    }

    export class PlanoDeSaudeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        numeroDeRegistro: string;
        reembolso: boolean;
        descricaoDoReembolso: string;
        abrangencia: AbrangenciaDoPlanoEnum;
        segmentacaoAssistencial: SegmentacaoAssistencialDoPlanoEnum;
        planoDeSaudeAns?: PlanoDeSaudeAnsEnum;
        acomodacao?: AcomodacaoEnum;
    }

    export class GetPlanoDeSaudeInput {
        numeroDeRegistro: string;
    }
}