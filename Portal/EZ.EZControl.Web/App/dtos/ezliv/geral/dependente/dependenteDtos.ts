﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as beneficiarioDtos from "App/dtos/ezliv/subtipoPessoa/beneficiario/beneficiarioDtos";

export module Dtos.EZLiv.Geral {
    export class DependenteInput extends beneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput{
        grauDeParentesco: GrauDeParentescoEnum;
        email: string;
        formId: number;
    }

    export class DependenteListDto extends beneficiarioDtos.Dtos.EZLiv.Geral.BeneficiariolistDto {
        grauDeParentesco: GrauDeParentescoEnum;
    }
}