﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ItemDeDeclaracaoDeSaudeDtos from
    "App/dtos/ezliv/geral/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeDtos";

export module Dtos.EZLiv.Geral {
    export class RespostaDoItemDeDeclaracaoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        observacao: string;
        itemDeDeclaracaoDeSaudeInput: ItemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput;
        itemDeDeclaracaoDeSaudeId: number;
        respostaDoItemDeDeclaracaoDeSaudeFixo: RespostaDoItemDeDeclaracaoDeSaudeEnum;
        anoDoEvento: number;
    }

    export class RespostaDoItemDeDeclaracaoDeSaudeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        observacao: string;
        itemDeDeclaracaoDeSaudeInput: ItemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeListDto;
        itemDeDeclaracaoDeSaudeId: number;
        respostaDoItemDeDeclaracaoDeSaudeFixo: RespostaDoItemDeDeclaracaoDeSaudeEnum;
        anoDoEvento: number;
    }

    export class GetPropostaDeContratacaoInput {
        pergunta: string;
        observacao: string;
    }
}