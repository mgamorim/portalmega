﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as RedeCredenciadaDtos from "App/dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as UnidadeSaudeBaseDtos from "App/dtos/ezliv/geral/unidadeSaudeBase/unidadeSaudeBaseDtos";

export module Dtos.EZLiv.Geral {
    export class ItemDeRedeCredenciadaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        isRedeDiferenciada: boolean;
        redeCredenciadaId: number;
        redeCredenciada: RedeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto;
        unidadeDeSaudeId: number;
        unidadeDeSaude: UnidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto;       
        isHospital: boolean;
        isLaboratorio: boolean;
    }

    export class ItemDeRedeCredenciadaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        isRedeDiferenciada: boolean;
        redeCredenciadaId: number;
        redeCredenciada: RedeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto;
        unidadeDeSaudeId: number;
        unidadeDeSaude: UnidadeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto;
        isHospital: boolean;
        isLaboratorio: boolean;
    }

    export class GetItemDeRedeCredenciadaInput {
        id: number;
        redeCredenciadaId: number;
    }

    export class GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput {
        id: number;
    }
}