﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as arquivoDtos from "App/dtos/core/arquivo/arquivoDtos";

export module Dtos.EZLiv.Geral {
    export class ArquivoDocumentoInput extends arquivoDtos.Dtos.Arquivo.Core.ArquivoInput {
        tipo: TipoDeDocumentoEnum;
        emExigencia: boolean;
        motivo: string;
        propostaDeContratacaoId: number;
    }

    export class ArquivoDocumentoListDto extends arquivoDtos.Dtos.Arquivo.Core.ArquivoListDto {
        tipo: TipoDeDocumentoEnum;
        emExigencia: boolean;
        motivo: string;
        propostaDeContratacaoId: number;
    }

    export class GetArquivoDocumentoInput {
        nome: string;
    }
}