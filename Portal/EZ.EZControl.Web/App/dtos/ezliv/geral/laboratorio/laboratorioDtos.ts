﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as unidadeSaudeDtos from "../unidadeSaudeBase/unidadeSaudeBaseDtos";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";

export module Dtos.EZLiv.Geral {
    export class LaboratorioInput extends unidadeSaudeDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseInput {
        especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
    }

    export class LaboratorioListDto extends unidadeSaudeDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto {
    }

    export class GetLaboratorioInput  {
        nome: string;
        email: string;
        site: string;
    }

    export class GetPessoaExceptForLaboratorio {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
    }

    export class GetEspecialidadeExceptForLaboratorio {
        laboratorioId: number;
        especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
        nome: string;
        codigo: string;
    }
}