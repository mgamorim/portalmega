﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class RespostaDeDeclaracaoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        pergunta: string;
        tipoDeItemDeDeclaracao: TipoDeItemDeDeclaracaoEnum;
        perguntaId: number;
        marcada: string;
        observacao: string;
        beneficiarioId: number;
        propostaDeContratacaoId: number;
        ordenacao: number;
        anoDoEvento: number;
    }
}