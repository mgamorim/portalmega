﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as produtoDtos from "../../../estoque/produto/produtoDtos";
import * as planoDeSaudeDtos from "../../../ezliv/geral/planoDeSaude/planoDeSaudeDtos";
import * as contratoDtos from "../../../ezliv/geral/contrato/contratoDtos";
//import * as especialidadeDtos from "../../../ezmedical/especialidade/especialidadeDtos";
import * as operadoraDtos from "../../../ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as especialidadePorProdutoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/especialidadePorProdutoDePlanoDeSaude/especialidadePorProdutoDePlanoDeSaudeDtos";
import * as redeCredenciadaDtos from "../../../../dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as associacaoDtos from "../../../ezliv/subtipopessoa/associacao/associacaoDtos";
import * as chancelaDtos from "../../../ezliv/geral/chancela/chancelaDtos";
import * as vigenciaDtos from "../../../ezliv/geral/vigencia/vigenciaDtos";
import * as questionarioDeDeclaracaoDeSaudeDtos from "../../../ezliv/geral/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeDtos";
import * as declaracaoDoBeneficiarioDtos from "../../../ezliv/geral/declaracaoDoBeneficiario/declaracaoDoBeneficiarioDtos";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";

export module Dtos.EZLiv.Geral {

    export class ProdutoDePlanoDeSaudeInput extends produtoDtos.Dtos.Produto.ProdutoInput {
        planoDeSaude: planoDeSaudeDtos.Dtos.EZLiv.Geral.PlanoDeSaudeInput;
        planoDeSaudeId: number;
        operadora: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto;
        operadoraId: number;
        descricaoDaCarenciaEspecial: string;
        acompanhante: boolean;
        descricaoDoAcompanhante: string;
        coberturaExtra: boolean;
        descricaoDaCoberturaExtra: string;
        contrato: contratoDtos.Dtos.EZLiv.Geral.ContratoInput;
        linkRedeCredenciada: string;
        redeCredenciada: redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto;
        redeCredenciadaId?: number;
        questionarioDeDeclaracaoDeSaude: questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeInput;
        questionarioDeDeclaracaoDeSaudeId?: number;
        declaracaoDoBeneficiario: declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioInput;
        declaracaoDoBeneficiarioId?: number;
        relatorioFichaDeEntidadeId?: number;
        relatorioProspostaDeContratacaoId?: number;
        aditivos: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];
        especialidades: Array<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>;
        chancelas: Array<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>;
        constructor() {
            super();
            this.especialidades = new Array<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>();
            this.chancelas = new Array<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>();
        }
        cidades: cidadeDtos.Dtos.Cidade.CidadeInput[];
        estados: estadoDtos.Dtos.Estado.EstadoInput[];
        vigencias: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput[];
        numeroDeDiasParaEncerrarAsPropostasRelacionadas?: number;
        formaDeContratacao: FormaDeContratacaoEnum;
        cartaDeOrientacao: string;
    }

    export class ProdutoDePlanoDeSaudeListDto extends produtoDtos.Dtos.Produto.ProdutoListDto {
        planoDeSaude: planoDeSaudeDtos.Dtos.EZLiv.Geral.PlanoDeSaudeListDto;
        planoDeSaudeId: number;
        operadora: operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto;
        operadoraId: number;
        descricaoDaCarenciaEspecial: string;
        acompanhante: boolean;
        descricaoDoAcompanhante: string;
        coberturaExtra: boolean;
        descricaoDaCoberturaExtra: string;
        questionarioDeDeclaracaoDeSaudeId?: number;
        questionarioDeDeclaracaoDeSaude: questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto;
        declaracaoDoBeneficiario: declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto;
        declaracaoDoBeneficiarioId?: number;
        relatorioFichaDeEntidadeId?: number;
        relatorioProspostaDeContratacaoId?: number;
        detailProduto: GetDetailProdutoDePlanoDeSaude;
        contrato: contratoDtos.Dtos.EZLiv.Geral.ContratoListDto;
        linkRedeCredenciada: string;
        redeCredenciada: redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto;
        redeCredenciadaId?: number;
        numeroDeDiasParaEncerrarAsPropostasRelacionadas?: number;
        aditivos: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];
        especialidades: Array<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>;
        chancelas: Array<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>;
        constructor() {
            super();
            this.especialidades = new Array<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>();
            this.chancelas = new Array<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>();
        }
        cidades: cidadeDtos.Dtos.Cidade.CidadeInput[];
        estados: estadoDtos.Dtos.Estado.EstadoInput[];
        formaDeContratacao: FormaDeContratacaoEnum;
        vigencias: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput[];
        redeMedica: string;
        laboratorios: string;
    }

    export class GetDetailProdutoDePlanoDeSaude {
        descricaoRedes: string;
        descricaoAbrangencia: string;
        descricaoAcomodacao: string;
        descricaoSegmentacao: string;
        descricaoTipoDePlanoAns: string;
        nomeTitular: string;
        valorTitular: number;
        dependentes: DependenteDetailInput[];
    }

    export class GetProdutoDePlanoDeSaudeInput {
        nome: string;
        exceptIds: number[];
    }

    export class GetEstadoExceptForProduto {
        produtoDePlanoDeSaudeId: number;
        estados: estadoDtos.Dtos.Estado.EstadoInput[];
        nome: string;
    }
    export class GetCidadeExceptForProduto {
        produtoDePlanoDeSaudeId: number;
        cidades: cidadeDtos.Dtos.Cidade.CidadeInput[];
        nome: string;
    }

    export class GetValoresByProdutoInput {
        produtoDePlanoDeSaudeId: number;
        proponenteId: number;
        contratoId: number;
        profissaoId: number;
        estadoId: number;
    }

    export class GetValoresAndProdutoByCorretorInput {
        produtoDePlanoDeSaudeId: number;
        proponenteId: number;
        contratoId: number;
        corretorId: number;
        profissaoId: number;
        estadoId: number;
    }

    export class ValoresByProdutoListDto {
        produtoDePlanoDeSaudeId: number;
        titular: TitularDetailInput;
        dependentes: DependenteDetailInput[];
    }
    export class TitularDetailInput {
        nome: string;
        valor: number;
    }
    export class DependenteDetailInput {
        nome: string;
        valor: number;
    }
    export class DetalhesProdutoDePlanoDeSaudeInput {
        nome: string;
        administradora: string;
        operadora: string;
        operadoraLogo: string;
        acomodacao: string;
        abrangencia: string;
        reembolso: boolean;
        valor: number;
        descricao: string;
        redecredenciada: string;
    }
    export class GetDetalhesProdutoInput {
        produtoId: number;
        propostaId: number;
    }

}