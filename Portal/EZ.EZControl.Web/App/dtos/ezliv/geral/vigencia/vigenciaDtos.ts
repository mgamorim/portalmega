﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class VigenciaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        dataInicialDaProposta: number;
        dataFinalDaProposta: number;
        dataDeFechamento: number;
        dataDeVigencia: number;
        numeroDeBoletos: string;
    }

    export class VigenciaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        dataInicialDaProposta: number;
        dataFinalDaProposta: number;
        dataDeFechamento: number;
        dataDeVigencia: number;
        numeroDeBoletos: string;
    }

    export class GetVigenciaInput {
        numeroDeBoletos: string;
    }

    export class GetVigenciaByProdutoInput {
        numeroDeBoletos: string;
        produtoId: number;
    }
}