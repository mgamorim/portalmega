﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as administradoraDtos from "App/dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as corretoraDtos from "App/dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as produtoDtos from "App/dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as empresaDtos from "App/dtos/global/empresa/empresaDtos";

export module Dtos.EZLiv.Geral {
    export class PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        administradora: administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput;
        administradoraId: number;
        corretoras: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto[];
        produtos: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];
        empresa: empresaDtos.Dtos.Empresa.EmpresaInput;
    }

    export class PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        administradora: administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto;
        administradoraId: number;
        corretoras: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto[];
        produtos: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];
        empresa: empresaDtos.Dtos.Empresa.EmpresaListDto;
    }

    export class GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput  {
        nome: string;
    }

    export class GetPermissoesPorCorretoraInput {
        temCorretoras: boolean;
        corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto;
        valor: number;
    }
}