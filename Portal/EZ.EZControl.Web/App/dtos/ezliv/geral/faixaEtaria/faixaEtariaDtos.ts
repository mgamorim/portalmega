﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class FaixaEtariaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        idadeInicial: number;
        idadeFinal: number;
    }

    export class FaixaEtariaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        idadeInicial: number;
        idadeFinal: number;
    }

    export class GetFaixaEtariaInput {
        descricao: string;
    }

    export class GetFaixaEtariaExceptForIdsInput {
        descricao: string;
        ids: number[];
    }
}