﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class MeusProdutosInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        reembolso: boolean;
        abrangencia: string;
        valor: number;
        administradora: string;
        statusProposta: string;
        propostaId: number;
    }

    export class MeusProdutosListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        reembolso: boolean;
        abrangencia: string;
        valor: number;
        administradora: string;
        statusProposta: string;
        propostaId: number;
    }

    export class GetMeusProdutosInput {
        nome: string;
    }
}