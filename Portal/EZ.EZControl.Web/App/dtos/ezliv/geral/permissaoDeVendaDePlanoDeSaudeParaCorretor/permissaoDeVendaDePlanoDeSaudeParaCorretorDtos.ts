﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as administradoraDtos from "App/dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as corretoraDtos from "App/dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as corretorDtos from "App/dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as produtoDtos from "App/dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as empresaDtos from "App/dtos/global/empresa/empresaDtos";

export module Dtos.EZLiv.Geral {
    export class PermissaoDeVendaDePlanoDeSaudeParaCorretorInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        comissao: string;
        corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        corretoraId: number;
        corretores: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto[];
        produtos: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];
        empresa: empresaDtos.Dtos.Empresa.EmpresaInput;
    }

    export class PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        comissao: string;
        corretora: corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto;
        corretoraId: number;
        corretores: corretorDtos.Dtos.EZLiv.Geral.CorretorListDto[];
        produtos: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];
        empresa: empresaDtos.Dtos.Empresa.EmpresaInput;
    }

    export class GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput  {
        nome: string;
    }
}