﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as itemDeRedeCredenciadaDtos from "App/dtos/ezliv/geral/itemDeRedeCredenciada/itemDeRedeCredenciadaDtos";
import * as redeCredenciadaDtos from "App/dtos/ezliv/geral/RedeCredenciada/RedeCredenciadaDtos";

export module Dtos.EZLiv.Geral {
    export class RedeCredenciadaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;        
        items: Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>;
        constructor() {
            super();
            this.items = new Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>();
        }        
        hospitaisDiferenciados: Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>;
        laboratoriosDiferenciados: Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>;
    }

    export class RedeCredenciadaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;        
        hospitaisDiferenciados: Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>;
        laboratoriosDiferenciados: Array<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>;
    }

    export class GetRedeCredenciadaInput {
        id: number;
        nome: string;
    }
}