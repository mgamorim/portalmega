﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as unidadeSaudeDtos from "../unidadeSaudeBase/unidadeSaudeBaseDtos";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";

export module Dtos.EZLiv.Geral {
    export class HospitalInput extends unidadeSaudeDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseInput {
        especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
    }

    export class HospitalListDto extends unidadeSaudeDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto {
    }

    export class GetHospitalInput  {
        nome: string;
        email: string;
        site: string;
    }

    export class GetPessoaExceptForHospital {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
    }

    export class GetEspecialidadeExceptForHospital {
        laboratorioId: number;
        especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
        nome: string;
        codigo: string;
    }
}