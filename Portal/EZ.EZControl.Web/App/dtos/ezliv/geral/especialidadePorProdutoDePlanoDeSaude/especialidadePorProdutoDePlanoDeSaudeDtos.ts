﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as produtoDePlanoDeSaudeDtos from "App/dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as especialidadeDtos from "App/dtos/ezmedical/especialidade/especialidadeDtos";

export module Dtos.EZLiv.Geral {
    export class EspecialidadePorProdutoDePlanoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        produtoDePlanoDeSaudeId: number;
        especialidade: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto;
        especialidadeId: number;
    }

    export class EspecialidadePorProdutoDePlanoDeSaudeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        produtoDePlanoDeSaudeId: number;
        especialidade: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto;
        especialidadeId: number;
    }

    export class GetEspecialidadePorProdutoDePlanoDeSaudeInput {
        nome: string;
        produtoDePlanoDeSaudeId: number;
    }
}