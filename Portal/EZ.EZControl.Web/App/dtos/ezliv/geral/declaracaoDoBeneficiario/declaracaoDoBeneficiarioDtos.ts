﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ItemDeDeclaracaoDoBeneficiarioDtos from "App/dtos/ezliv/geral/itemDeDeclaracaoDoBeneficiario/itemDeDeclaracaoDoBeneficiarioDtos";

export module Dtos.EZLiv.Geral {
    export class DeclaracaoDoBeneficiarioInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        itensDeDeclaracaoDoBeneficiario: ItemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput[];
        itensDeDeclaracaoDoBeneficiarioIds: number[];
    }

    export class DeclaracaoDoBeneficiarioListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        itensDeDeclaracaoDoBeneficiario: ItemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioListDto[];
        itensDeDeclaracaoDoBeneficiarioIds: number[];
    }

    export class GetDeclaracaoDoBeneficiarioInput {
        nome: string;
    }
}