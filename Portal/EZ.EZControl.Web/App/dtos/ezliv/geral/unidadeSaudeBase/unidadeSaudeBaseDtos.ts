﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as telefoneDtos from "../../../global/telefone/telefoneDtos";
import * as enderecoDtos from "../../../global/endereco/enderecoDtos";
import * as pessoaDtos from "../../../global/pessoa/pessoaDtos";


export module Dtos.EZLiv.Geral {
    export class UnidadeSaudeBaseInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        isActive: boolean;
        email: string;
        site: string;
        pessoaJuridicaId: number;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        enderecoId: number;
        endereco: enderecoDtos.Dtos.Endereco.EnderecoInput;
        telefone1Id: number;
        telefone1: telefoneDtos.Dtos.Telefone.TelefoneInput;
        telefone2Id: number;
        telefone2: telefoneDtos.Dtos.Telefone.TelefoneInput;
        tipoDeUnidade: TipoDeUnidadeEnum;
    }

    export class UnidadeSaudeBaseListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        isActive: boolean;
        email: string;
        site: string;
        pessoaJuridicaId: number;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto;
        enderecoId: number;
        endereco: enderecoDtos.Dtos.Endereco.EnderecoListDto;
        telefone1Id: number;
        telefone1: telefoneDtos.Dtos.Telefone.TelefoneListDto;
        telefone2Id: number;
        telefone2: telefoneDtos.Dtos.Telefone.TelefoneListDto;
        tipoDeUnidade: TipoDeUnidadeEnum;
    }

    export class GetUnidadeSaudeBaseInput  {
        nome: string;
        email: string;
        site: string;
        ids: number[];
    }
}