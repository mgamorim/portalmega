﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as BeneficiarioDtos from "App/dtos/ezliv/subtipoPessoa/beneficiario/beneficiarioDtos";

export module Dtos.EZLiv.Geral {
    export class ItemDeDeclaracaoDeSaudeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pergunta: string;
        beneficiarioInput: BeneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput;
        beneficiarioId: number;
        ordenacao: number;
        tipoDeItemDeDeclaracao: TipoDeItemDeDeclaracaoEnum;
    }

    export class ItemDeDeclaracaoDeSaudeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pergunta: string;
        beneficiarioInput: BeneficiarioDtos.Dtos.EZLiv.Geral.BeneficiariolistDto;
        beneficiarioId: number;
        ordenacao: number;
        tipoDeItemDeDeclaracao: TipoDeItemDeDeclaracaoEnum;
    }

    export class GetItemDeDeclaracaoDeSaudeInput {
        pergunta: string;
    }
}