﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as contratoDtos from "App/dtos/ezliv/geral/contrato/contratoDtos";
import * as faixaEtariaDtos from "App/dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";

export module Dtos.EZLiv.Geral {
    export class ValorPorFaixaEtariaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        contratoId: number;
        faixaEtaria: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput;
        faixaEtariaId: number;
        valor: number;
    }

    export class ValorPorFaixaEtariaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        contratoId: number;
        faixaEtaria: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto;
        faixaEtariaId: number;
        valor: number;
    }

    export class GetValorPorFaixaEtariaInput {
        valor: number;
        contratoId: number;
    }
}