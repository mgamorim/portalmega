﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as ProponenteTitularDtos from "App/dtos/ezliv/subtipoPessoa/proponenteTitular/proponenteTitularDtos";
import * as ProdutoDePlanoDeSaudeDtos from "App/dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as ResponsavelDtos from "App/dtos/ezliv/subtipoPessoa/responsavel/responsavelDtos";
import * as ContratoDtos from "App/dtos/ezliv/geral/contrato/contratoDtos";
import * as CorretorDtos from "App/dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as CorretoraDtos from "App/dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as PedidoDtos from "App/dtos/vendas/pedido/pedidoDtos";
import * as arquivoDocumentoDtos from "App/dtos/ezliv/geral/arquivoDocumento/arquivoDocumentoDtos";
import * as enderecoDtos from "App/dtos/global/endereco/enderecoDtos";
import * as userDtos from "../../../../dtos/core/user/userDtos";
import * as estadoDtos from "App/dtos/global/estado/estadoDtos";
import * as chancelaDtos from "App/dtos/ezliv/geral/chancela/chancelaDtos";
import * as profissaoDtos from "App/dtos/global/profissao/profissaoDtos";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";

export module Dtos.EZLiv.Geral {
    export class PropostaDeContratacaoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        constructor() {
            super();
            this.titularResponsavelLegal = true;
        }
        numero: string;
        titular: ProponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput;
        titularId: number;
        produto: ProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput;
        produtoId: number;
        responsavel: ResponsavelDtos.Dtos.EZLiv.Geral.ResponsavelInput;
        responsavelId: number;
        aceite: boolean;
        tipoDeHomologacao: TipoDeHomologacaoEnum;
        dataHoraDaHomologacao: Date;
        dataHoraDoAceite: Date;
        inicioDeVigencia: Date;
        contratoVigenteNoAceite: ContratoDtos.Dtos.EZLiv.Geral.ContratoInput;
        contratoVigenteNoAceiteId: number;
        ultimoContratoAceito: ContratoDtos.Dtos.EZLiv.Geral.ContratoInput;
        ultimoContratoAceitoId: number;
        corretor: CorretorDtos.Dtos.EZLiv.Geral.CorretorInput;
        corretorId: number;
        corretora: CorretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        corretoraId: number;
        pedido: PedidoDtos.Dtos.Pedido.PedidoInput;
        pedidoId: number;
        passoDaProposta: PassoDaPropostaEnum;
        statusDaProposta: StatusDaPropostaEnum;
        formaDeContratacao: FormaDeContratacaoEnum;
        tipoDeProposta: TipoDePropostaEnum;
        profissaoId: number;
        profissao: profissaoDtos.Dtos.Profissao.ProfissaoListDto;
        documentos: any;
        dependentes: any;
        observacaoHomologacao: string;
        chancelaId: number;
        chancela: chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto;
        vigenciaId: number;
        usuarioTitular: userDtos.Dtos.User.Core.UserDto;
        usuarioTitularId?: number;
        usuarioResponsavel: userDtos.Dtos.User.Core.UserDto;
        usuarioResponsavelId?: number;
        aceiteDaDeclaracaoDeSaude: boolean;
        aditivos: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];
        estadoId: number;
        estado: estadoDtos.Dtos.Estado.EstadoListDto;
        titularResponsavelLegal: boolean;
        itemDeDeclaracaoDoBeneficiarioId: number;
        aceiteCorretor: boolean;
        dataAceiteCorretor?: Date;
        gerenteId?: number;
        supervisorId?: number;
    }

    export class PropostaDeContratacaoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {

        numero: string;
        titular: ProponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput;
        titularId: number;
        produto: ProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput;
        produtoId: number;
        responsavel: ResponsavelDtos.Dtos.EZLiv.Geral.ResponsavelInput;
        responsavelId: number;
        aceite: boolean;
        tipoDeHomologacao: TipoDeHomologacaoEnum;
        tipoDeProposta: TipoDePropostaEnum;
        dataHoraDaHomologacao: Date;
        dataHoraDoAceite: Date;
        contratoVigenteNoAceite: ContratoDtos.Dtos.EZLiv.Geral.ContratoInput;
        contratoVigenteNoAceiteId: number;
        ultimoContratoAceito: ContratoDtos.Dtos.EZLiv.Geral.ContratoInput;
        ultimoContratoAceitoId: number;
        corretor: CorretorDtos.Dtos.EZLiv.Geral.CorretorInput;
        corretorId: number;
        corretora: CorretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        corretoraId: number;
        pedido: PedidoDtos.Dtos.Pedido.PedidoInput;
        pedidoId: number;
        passoDaProposta: PassoDaPropostaEnum;
        statusDaProposta: StatusDaPropostaEnum;
        formaDeContratacao: FormaDeContratacaoEnum;
        aceiteDaDeclaracaoDeSaude: boolean;
        aditivos: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];
        estadoId: number;
        profissaoId: number;
        titularResponsavelLegal: boolean;
        itemDeDeclaracaoDoBeneficiarioId: number;
        aceiteCorretor: boolean;
        dataAceiteCorretor?: Date;
    }

    export class GetPropostaDeContratacaoInput {
        numeroDaProposta: string;
        nomeDoBeneficiario: string;
        cpfDoBeneficiario: string;
    }

    export class PropostaDeContratacaoPassoPreCadastroInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        constructor() {
            super();
            this.titularResponsavelLegal = true;
        }
        passoDaProposta: PassoDaPropostaEnum;
        nomeCompleto: string;
        titularId: number;
        email: string;
        emailId: number;
        cpf: string;
        cpfId: number;
        dataDeNascimento: Date;
        celular: string;
        celulaId: number;
        estadoId: number;
        profissaoId: number;
        grupoPessoaId: number;
        dependentes: any;
        pessoaId: number;
        formaDeContratacao: FormaDeContratacaoEnum;
        usuarioTitularId?: number;
        corretora: CorretoraDtos.Dtos.EZLiv.Geral.CorretoraInput;
        corretoraId: number;
        titularResponsavelLegal: boolean;
    }

    export class PropostaDeContratacaoPassoSelecionarPlanoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        passoDaProposta: PassoDaPropostaEnum;
        produtoDePlanoDeSaudeId: number;
        corretoraId: number;
        formaDeContratacao: FormaDeContratacaoEnum;
    }

    export class PropostaDeContratacaoPassoAceiteDoContratoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        passoDaProposta: PassoDaPropostaEnum;
        contratoId: number;
        enviaCopiaAdministradora: boolean;
        enviaCopiaCorretora: boolean;
        aceite: boolean;
        formaDeContratacao: FormaDeContratacaoEnum;
        aceiteCorretor: boolean;
        dataAceiteCorretor?: Date;
    }

    export class PropostaDeContratacaoPassoDadosGeraisInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        passoDaProposta: PassoDaPropostaEnum;
        dependentes: any;
        proponenteTitular: ProponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput;

        // Dados do Proponente Titular
        nomeProponenteTitular: string;
        cpfIdProponenteTitular: number;
        cpfProponenteTitular: string;
        dataDeNascimentoProponenteTitular: Date;
        rgIdProponenteTitular: number;
        rgProponenteTitular: string;
        orgaoExpedidorProponenteTitular: string;
        nacionalidadeProponenteTitular: string;
        sexoProponenteTitular: any;
        estadoCivilProponenteTitular: any;
        emailIdProponenteTitular: number;
        emailProponenteTitular: string;
        nomeDaMaeProponenteTitular: string;
        numeroDoCartaoNacionalDeSaudeProponenteTitular: string;
        declaracaoDeNascidoVivoProponenteTitular: string;
        matricula: string;
        boleto: string;
        folha: string;
        DebitoConta: string;
        FolhaFicha: string;
        //Dados do Responsável Legal
        tipoResponsavel: any;
        nomeResponsavel: string;
        cpfIdReponsavel: number;
        cpfReponsavel: string;
        dataDeNascimentoReponsavel: Date;
        nacionalidadeResponsavel: string;
        sexoResponsavel: any;
        estadoCivilResponsavel: any;
        endereco: enderecoDtos.Dtos.Endereco.EnderecoInput;
        formaDeContratacao: FormaDeContratacaoEnum;
        vigenciaId: number;
        inicioDeVigencia: Date;
        celular: string;
        celulaId: number;
        email: string;
        emailId: number;
        aceiteDaDeclaracaoDeSaude: boolean;
        itemDeDeclaracaoDoBeneficiarioId: number;
        aditivos: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];
        gerenteId?: number;
        supervisorId?: number;
        chancelaId?: number;
        titularResponsavelLegal: boolean;
    }

    export class PropostaDeContratacaoPassoEnvioDeDocumentosInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        passoDaProposta: PassoDaPropostaEnum;
        formaDeContratacao: FormaDeContratacaoEnum;
    }

    export class PropostaDeContratacaoPassoHomologacaoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        tipoDeHomologacao: TipoDeHomologacaoEnum;
        dataHoraDaHomologacao: Date;
        observacaoHomologacao: string;
        passoDaProposta: PassoDaPropostaEnum;
        formaDeContratacao: FormaDeContratacaoEnum;
    }

    export class PropostaDeContratacaoPassoPagamentoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        formaDePagamento: FormaDePagamentoEnum;
        recebidoDeposito: boolean;
    }

    export class PropostaDeContratacaoDataDeVigenciaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        inicioDaVigencia: string;
    }

    export class PropostaDeContratacaoDepositoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        propostaId: number;
        comprovante: string;
        formaDePagamento: number;
        recebido: boolean;
    }
}