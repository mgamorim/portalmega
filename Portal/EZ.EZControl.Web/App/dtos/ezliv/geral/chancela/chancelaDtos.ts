﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as profissaoDtos from "App/dtos/global/profissao/profissaoDtos";

export module Dtos.EZLiv.Geral {
    export class ChancelaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        taxaDeAdesao: number;
        taxaMensal: number;
        profissoes: profissaoDtos.Dtos.Profissao.ProfissaoInput[];
        associacaoId: number;
    }

    export class ChancelaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;                     
        nome: string;
        taxaDeAdesao: number;
        profissoes: profissaoDtos.Dtos.Profissao.ProfissaoListDto[];
    }

    export class GetChancelaInput  {
        nome: string;
    }
}