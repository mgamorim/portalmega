﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../global/pessoa/pessoaDtos";
import * as produtoDePlanoDeSaudeDtos from "../../../ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";

export module Dtos.EZLiv.Sync {
    export class EspecialidadeSyncInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
       isActive: boolean;
       especialidade: string;
       nome: string;
       logradouro: string;
       numero: string;
       complemento: string;
       bairro: string;
       municipio: string;
       estado: string;
       nomeEstado: string;
       telefone1: string;
       ramal1: string;
       telefone2: string;
       ramal2: string;
       municipioPai: string;
       email: string;
       homePage: string;
       produtosDePlanoDeSaude: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput[];
       produtosDePlanoDeSaudeIds: number[];
    }

    export class EspecialidadeSyncListDto extends pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto {
        isActive: boolean;
        especialidade: string;
        nome: string;
        logradouro: string;
        numero: string;
        complemento: string;
        bairro: string;
        municipio: string;
        estado: string;
        nomeEstado: string;
        telefone1: string;
        ramal1: string;
        telefone2: string;
        ramal2: string;
        municipioPai: string;
        email: string;
        homePage: string;
        produtosDePlanoDeSaude: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];
        produtosDePlanoDeSaudeIds: number[];
    }

    export class GetEspecialidadeSyncInput {
        nome: string;
        estado: string;
        municipio: string;
    }

    export class GetEspecialidadeSyncByNomeDto {
        nome: string;
    }   

    export class GetEspecialidadeSyncByEstadoDto {
       estado: string;
       nomeEstado: string;
    }

    export class GetEspecialidadeSyncByMunicipioDto {
        estado: string;
        nomeEstado: string;
        municipio: string;
    }
}