﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as arquivoDtos from "../../../core/arquivo/arquivoDtos";
import * as corretoraDtos from "../../../ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as gerenteDtos from "../../../ezliv/subtipoPessoa/gerenteAdministradora/gerenteAdministradoraDtos";
import * as supervisorDtos from "../../../ezliv/subtipoPessoa/supervisorAdministradora/supervisorAdministradoraDtos";
import * as homologadorDtos from "../../../ezliv/subtipoPessoa/homologadorAdministradora/homologadorAdministradoraDtos";

export module Dtos.EZLiv.Geral {
    export class AdministradoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nomeFantasia: string;
        razaoSocial: string;
        pessoaJuridica: any;
        parametroPagSeguro: any;
        pessoaJuridicaId: number;
        parametroPagSeguroId: number;
        isActive: boolean;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
        corretoras: Array<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput>;
        gerentes: Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput>;
        supervisores: Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput>;
        homologadores: Array<homologadorDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput>;
    }

    export class AdministradoraListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nomeFantasia: string;
        razaoSocial: string;
        pessoaJuridica: any;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
    }


    export class GetAdministradoraInput {
        nomeFantasia: string;
        razaoSocial: string;
    }

    export class GetParametroPagSeguroAdministradoraInput {
        pessoaJuridicaId: number;
    }

    export class GetPessoaExceptForAdministradora {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
    }
}