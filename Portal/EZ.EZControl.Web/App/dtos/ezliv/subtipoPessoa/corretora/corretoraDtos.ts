﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as arquivoDtos from "../../../core/arquivo/arquivoDtos";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as gerenteDtos from "../../../../dtos/ezliv/subtipoPessoa/gerenteCorretora/gerenteCorretoraDtos";
import * as supervisorDtos from "../../../../dtos/ezliv/subtipoPessoa/supervisorCorretora/supervisorCorretoraDtos";
import * as produtoDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as depositoTransferenciaBancariaDtos from "../../../../dtos/ezpag/depositoTransferenciaBancaria/depositoTransferenciaBancariaDtos";

export module Dtos.EZLiv.Geral {
    export class CorretoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity  {
        nomeFantasia: string;
        razaoSocial: string;
        pessoaJuridica: any;
        parametroPagSeguro: any;
        pessoaJuridicaId: number;
        corretor: any;
        corretorId: number;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
        parametroPagSeguroId: number;
        isActive: boolean;
        corretorIds: number[];
        nome: string;
        supervisores: Array<supervisorDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput>;
        gerentes: Array<gerenteDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput>;
        produtoDePlanoDeSaude: produtoDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto;
        depositoTransferenciaBancaria: any;
        depositoTransferenciaBancariaId: number;
    }

    export class CorretoraListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity{
        nomeFantasia: string;
        razaoSocial: string;
        pessoaJuridica: any;
        corretor: any;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
        nome: string;
        produtoDePlanoDeSaude: any;
    }


    export class GetCorretoraInput {
        nomeFantasia: string;
        razaoSocial: string;
        ids: number[];
        nome: string;
    }

    export class GetParametroPagSeguroCorretoraInput {
        pessoaJuridicaId: number;
    }

    export class GetPessoaExceptForCorretora {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
        nome: string;
    }
}