﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as BeneficiarioDtos from "App/dtos/ezliv/subtipoPessoa/beneficiario/beneficiarioDtos";

export module Dtos.EZLiv.Geral {
    export class DependenteInput extends BeneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput {
        grauDeParentesco: GrauDeParentescoEnum;
    }

    export class DependenteListDto extends BeneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput {
        grauDeParentesco: GrauDeParentescoEnum;
    }

    export class GetDependenteInput {
        nome: string;
    }
}