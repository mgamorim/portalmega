﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as PessoaDtos from "App/dtos/global/pessoa/pessoaDtos";
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";

export module Dtos.EZLiv.Geral {
    export class GerenteAdministradoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        administradoraId: number;
    }

    export class GerenteAdministradoraListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        administradoraId: number;
    }

    export class GetGerenteAdministradoraInput {
        nome: string;
    }
    export class GetPessoaExceptForGerenteAdministradora {
        nome: string;
       // tipoPessoa: TipoPessoaEnum;
    }

    export class GetAdministradoraExceptForGerenteAdministradora {
        corretorId: number;
        administradoraId: number;
        nome: string;
    }
}