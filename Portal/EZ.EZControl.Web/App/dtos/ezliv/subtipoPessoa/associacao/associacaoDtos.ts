﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZLiv.Geral {
    export class AssociacaoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity  {
        nomeFantasia: string;
        razaoSocial: string;
        pessoaJuridica: any;
        parametroPagSeguro: any;
        pessoaJuridicaId: number;
        parametroPagSeguroId: number;
        isActive: boolean;
        imagemId: number;
        instrucoes: string;
    }

    export class AssociacaoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity{
        nomeFantasia: string;
        razaoSocial: string;
        pessoaJuridica: any;
        imagemId: number;
        instrucoes: string;
    }


    export class GetAssociacaoInput {
        nomeFantasia: string;
        razaoSocial: string;
    }

    export class GetParametroPagSeguroAssociacaoInput {
        pessoaJuridicaId: number;
    }

    export class GetPessoaExceptForAssociacao {
        nomePessoa: string;
        tipoPessoa: TipoPessoaEnum;
    }
}