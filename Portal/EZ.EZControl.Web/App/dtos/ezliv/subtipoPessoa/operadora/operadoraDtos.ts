﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../global/pessoa/pessoaDtos";
import * as produtoDePlanoDeSaudeDtos from "../../../ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as arquivoDtos from "../../../core/arquivo/arquivoDtos";

export module Dtos.EZLiv.Geral {
    export class OperadoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
        isActive: boolean;
        codigoAns: string;
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaId: number;
        pessoaJuridica: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput;
        pessoaJuridicaId: number;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
    }

    export class OperadoraListDto extends pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto {
        nome: string;
        codigoAns: string;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
        urlImagem: string;
    }

    export class GetOperadoraInput {
        nome: string;
        codigoAns: string;
    }

    export class OperadoraPessoaIdDto {
       operadoraId: number;
       pessoaId: number;
    }   

    export class GetPessoaExceptForOperadora {
        nomePessoa: string;
    }
}