﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as PessoaDtos from "App/dtos/global/pessoa/pessoaDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";

export module Dtos.EZLiv.Geral {
    export class GerenteCorretoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        corretoraId: number;
    }

    export class GerenteCorretoraListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        corretoraId: number;
    }

    export class GetGerenteCorretoraInput {
        nome: string;
        corretoraId: number;
    }
    export class GetPessoaExceptForGerenteCorretora {
        nome: string;
       // tipoPessoa: TipoPessoaEnum;
    }

    export class GetCorretoraExceptForGerenteCorretora {
        corretorId: number;
        corretoraId: number;
        nome: string;
    }
}