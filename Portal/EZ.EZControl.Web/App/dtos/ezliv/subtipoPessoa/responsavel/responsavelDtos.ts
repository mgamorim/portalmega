﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as PessoaDtos from "App/dtos/global/pessoa/pessoaDtos";

export module Dtos.EZLiv.Geral {
    export class ResponsavelInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity  {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        tipoDeResponsavel: TipoDeResponsavelEnum;
    }

    export class ResponsavelListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaListDto;
        tipoDeResponsavel: TipoDeResponsavelEnum;
    }

    export class GetResponsavelInput {
        nome: string;
    }
}