﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as PessoaDtos from "App/dtos/global/pessoa/pessoaDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";

export module Dtos.EZLiv.Geral {
    export class SupervisorCorretoraInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        corretoraId: number;
    }

    export class SupervisorCorretoraListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        corretoraId: number;
    }

    export class GetSupervisorCorretoraInput {
        nome: string;
        corretoraId: number;
    }
    export class GetPessoaExceptForSupervisorCorretora {
        nome: string;
    }

    export class GetCorretoraExceptForSupervisorCorretora {
        corretorId: number;
        corretoraId: number;
        nome: string;
    }
}