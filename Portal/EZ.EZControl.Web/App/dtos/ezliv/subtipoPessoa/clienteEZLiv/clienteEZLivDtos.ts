﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as clienteDtos from "../../../global/cliente/clienteDtos";

export module Dtos.ClienteEZLiv {
    export class ClienteEZLivListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        cliente: clienteDtos.Dtos.Cliente.ClienteListDto;
        observacao: string;
        clienteId: number;
    }

    export class ClienteEZLivInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        cliente: clienteDtos.Dtos.Cliente.ClienteInput;
        clienteId: number;
    }

    export class GetClienteEZLivInput {
        clienteId: number;
        nomeCliente: string;
    }
}