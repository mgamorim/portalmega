﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as BeneficiarioDtos from "App/dtos/ezliv/subtipoPessoa/beneficiario/beneficiarioDtos";
import * as DependenteDtos from "App/dtos/ezliv/subtipoPessoa/dependente/dependenteDtos";
import * as ResponsavelDtos from "App/dtos/ezliv/subtipoPessoa/responsavel/responsavelDtos";

export module Dtos.EZLiv.Geral {

    export class ProponenteTitularInput extends BeneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput {
        responsavelId: number;
        dependenteIds: number[];
        dependentes: any;
        responsavel: ResponsavelDtos.Dtos.EZLiv.Geral.ResponsavelInput;
        matricula: string;
        boleto: boolean;
        folha: boolean;
        debitoConta: boolean;
        //DebitoContaNao: boolean;
        folhaFicha: boolean;
        //FolhaFichaNao: boolean;
        nome: string;
        cpfId: number;
        cpf: string;
        rgId: number;
        rg: string;
        orgaoExpedidor: string;
        dataDeNascimento: Date;
        rgTitular: string;
        orgaoExpedidorTitular: string;
        nacionalidade: string;
        emailId: number;
        email: string;
        sexo: any;
        estadoCivil: any;
        celular: string;
        celulaId: number;
        idade: number;
        nomeResponsavelGrid: string;
    }

    export class ProponenteTitularListDto extends BeneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput {
        responsavelId: number;
        dependenteIds: number[];
        dependentes: DependenteDtos.Dtos.EZLiv.Geral.DependenteListDto[];
        responsavel: ResponsavelDtos.Dtos.EZLiv.Geral.ResponsavelListDto;
        nome: string;
        idade: number;
        nomeResponsavelGrid: string;
    }

    export class GetProponenteTitularInput {
        nome: string;
    }
}