﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as PessoaDtos from  "App/dtos/global/pessoa/pessoaDtos";
import * as respostasDto from "../../../../dtos/ezliv/geral/respostaDeDeclaracaoDeSaude/respostaDeDeclaracaoDeSaudeDtos";

export module Dtos.EZLiv.Geral {
    export class BeneficiarioInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        tipoDeBeneficiario: TipoDeBeneficiarioEnum;
        nomeDaMae: string;
        numeroDoCartaoNacionalDeSaude: string;
        matricula: string;
        declaracaoDeNascidoVivo: string;
        respostasDeclaracaoDeSaude: respostasDto.Dtos.EZLiv.Geral.RespostaDeDeclaracaoDeSaudeInput[];
    }

    export class BeneficiariolistDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaListDto;
        tipoDeBeneficiario: TipoDeBeneficiarioEnum;
        nomeDaMae: string;
        numeroDoCartaoNacionalDeSaude: string;
        declaracaoDeNascidoVivo: string;
    }

    export class GetBeneficiarioInput {
        nomeDaMae: string;
    }

    export class BeneficiarioUserInput {
        userId: number;
        staticRoleName: string;
        tipoDeUsuario: TipoDeUsuarioEnum;
    }

    
}