﻿import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as PessoaDtos from "App/dtos/global/pessoa/pessoaDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";


export module Dtos.EZLiv.Geral {
    export class CorretorInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        corretoras: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[];
      
    }

    export class CorretorListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pessoa: PessoaDtos.Dtos.Pessoa.PessoaListDto;
        pessoaFisica: PessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
        nome: string;
        corretoras: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[];
    }

    export class GetCorretorInput {
        nome: string;
        exceptIds: number[];
    }
    export class GetPessoaExceptForCorretor {
        nome: string;
       // tipoPessoa: TipoPessoaEnum;
    }

    export class GetCorretoraExceptForCorretor {
        corretorId: number;
        corretoras: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[];
        nome: string;
    }
}