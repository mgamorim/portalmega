﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as parametroCoreDtos from "../../core/parametro/parametroDtos";

export module Dtos.EZLiv.Parametro {

    export class ParametroEZLivListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {

    }


    export class ParametroEZLivInput extends parametroCoreDtos.Dtos.Parametro.Core.ParametroInput {
        
    }
}