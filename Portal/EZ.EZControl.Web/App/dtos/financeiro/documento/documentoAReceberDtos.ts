﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as documentoBaseDtos from "../../../dtos/financeiro/documento/documentoBaseDtos";

export module Dtos.Financeiro.DocumentoAReceber {
    export class GetDocumentoAReceberInput extends documentoBaseDtos.Dtos.Financeiro.Documento.GetDocumentoBaseInput {

    }
    export class DocumentoAReceberInput extends documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseInput {
        clienteId: number;
    }
    export class DocumentoAReceberListDto extends documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseListDto {
        clienteNomePessoa: string;
    }
}