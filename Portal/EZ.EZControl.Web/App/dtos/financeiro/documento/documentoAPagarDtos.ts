﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as documentoBaseDtos from "../../../dtos/financeiro/documento/documentoBaseDtos";

export module Dtos.Financeiro.DocumentoAPagar {
    export class GetDocumentoAPagarInput extends documentoBaseDtos.Dtos.Financeiro.Documento.GetDocumentoBaseInput {
        
    }
    export class DocumentoAPagarInput extends documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseInput {
        fornecedorId: number;
    }
    export class DocumentoAPagarListDto extends documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseListDto {
        fornecedorNomePessoa: string;
    }
}