﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Financeiro.Documento {
    export class GetDocumentoBaseInput {
        numero: string;
        complemento: string;
    }
    
    export class DocumentoBaseInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        isActive: boolean;
        numero: string;
        complemento: string;
        valor: number;
        data: Date;
        competencia: Date;
        vencimento: Date;
        descricao: string;
        observacoes: string;
        tipoDeDocumentoFinanceiroId: number;
        estornado: boolean;
        dataEstorno?: Date;
        motivoEstorno: string;
    }
    export class DocumentoBaseListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        isActive: boolean;
        numero: string;
        complemento: string;
        valor: number;
        data: Date;
        competencia: Date;
        vencimento: Date;
        descricao: string;
        observacoes: string;
        tipoDeDocumentoFinanceiroId: number;
        estornado: boolean;
        dataEstorno?: Date;
        motivoEstorno: string;
    }
}