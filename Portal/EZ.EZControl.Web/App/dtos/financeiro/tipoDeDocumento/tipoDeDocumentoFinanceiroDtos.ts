﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Financeiro.TipoDeDocumentoFinanceiro {
    export class GetTipoDeDocumentoInput {
        nome: string;
        tipo: TipoDeDocumentoFinanceiroEnum;
    }
    export class TipoDeDocumentoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        isActive: boolean;
        nome: string;
        tipo: TipoDeDocumentoFinanceiroEnum;
    }
    export class TipoDeDocumentoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        isActive: boolean;
        nome: string;
        tipo: TipoDeDocumentoFinanceiroEnum;
    }
}