﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Servico {

    export class ServicoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        valor: number;
        isValorAjustavel: boolean;
    }

    export class ServicoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        valor: number;
        isValorAjustavel: boolean;
    }

    export class GetServicoInput {
        nome: string;
    }
}