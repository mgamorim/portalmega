﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as agendaDtos from "../../../dtos/agenda/evento/eventoDtos";

export module Dtos.EZMedical.EventoConsulta {
    export class GetEventoConsultaInput extends agendaDtos.Dtos.Agenda.Evento.GetEventoInput {
        
    }
    export class GetEventoConsultaExceptForInput extends agendaDtos.Dtos.Agenda.Evento.GetEventoExceptForInput {

    }
    export class EventoConsultaInput extends agendaDtos.Dtos.Agenda.Evento.EventoInput {
        disponivel: boolean;

        constructor() {
            super();
            this.sistema = SistemaEnum.EZMedical;
            this.tipo = TipoDeEventoEnum.Consulta;
            this.disponivel = true;
            this.statusDoEvento = StatusDoEventoEnum.Agendado;
        }
    }
    export class EventoConsultaListDto extends agendaDtos.Dtos.Agenda.Evento.EventoListDto {
        disponivel: boolean;

        constructor() {
            super();
            this.sistema = SistemaEnum.EZMedical;
            this.tipo = TipoDeEventoEnum.Consulta;
            this.disponivel = true;
            this.statusDoEvento = StatusDoEventoEnum.Agendado;
        }
    }
}