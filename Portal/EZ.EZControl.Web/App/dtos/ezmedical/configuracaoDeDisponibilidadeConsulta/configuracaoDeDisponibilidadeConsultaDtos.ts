﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta {
    export class GetConfiguracaoDeDisponibilidadeConsultaInput {
        titulo: string;
        medico: string;
        especialidade: string;
        tipoDeDisponibilidade: string;
    }

    export class ConfiguracaoDeDisponibilidadeConsultaInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeDisponibilidade: any;
        tipoDeEvento: any;
        sistema: any;
        diasDaSemana: string;
        numeroDeDias: number;
        dataEspecifica: Date;
        horarioInicio: any;
        horarioFim: any;
        dataInicioValidade: Date;
        dataFimValidade: Date;
        semana: any;
        disponibilidades: any;
        medicoId: number;
        especialidadeId: number;
    }

    export class ConfiguracaoDeDisponibilidadeConsultaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeDisponibilidade: any;
        tipoDeEvento: any;
        sistema: any;
        diasDaSemana: string;
        numeroDeDias: number;
        dataEspecifica: Date;
        horarioInicio: any;
        horarioFim: any;
        dataInicioValidade: Date;
        dataFimValidade: Date;
        semana: any;
        disponibilidades: any;
        medicoId: number;
        especialidadeId: number;
        medicoNomePessoa: string;
        especialidadeNome: string;
    }
}