﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as produtoDePlanoDeSaudeDtos from "../../ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";

export module Dtos.EZMedical.Especialidade {
    export class EspecialidadeInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigo: string;
        produtosDePlanoDeSaude: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput[];
        produtosDePlanoDeSaudeIds: number[];
    }

    export class EspecialidadeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        codigo: string;
        produtosDePlanoDeSaude: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto[];
        produtosDePlanoDeSaudeIds: number[];
    }

    export class GetEspecialidadeInput {
        codigo: string;
        nome: string;
        ids: number [];
    }

    export class GetEspecialidadeExceptForIdsInput {
        nome    : string;
        ids: number[];
    }
}