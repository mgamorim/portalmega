﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZMedical.RegraConsulta {

    export class RegraConsultaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity  {
        sistema: any;
        tipoDeEvento: any;
        diasExibidosDesdeHojeAteAgendamento: number;
        vagasDisponibilizadasPorDia: number;
        vagasReservadasPorDia: number;
        unidadeDeTempo: number;
        vagasPorUnidadeDeTempo: number;
        diasAntesDoAgendamentoProibidoAlterar: number;
        diasParaNovoAgendamentoPorParticipante: number;
        overBook: boolean;
        isActive: boolean;
        medicoId: number;
        especialidadeId: number;
    }

    export class RegraConsultaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        sistema: any;
        tipoDeEvento: any;
        diasExibidosDesdeHojeAteAgendamento: number;
        vagasDisponibilizadasPorDia: number;
        vagasReservadasPorDia: number;
        unidadeDeTempo: number;
        vagasPorUnidadeDeTempo: number;
        diasAntesDoAgendamentoProibidoAlterar: number;
        diasParaNovoAgendamentoPorParticipante: number;
        overBook: boolean;
        isActive: boolean;
        medicoId: number;
        especialidadeId: number;
        medicoNomePessoa: string;
        especialidadeNome: string;
    }

    export class GetRegraConsultaInput {
        sistema: any;
        tipoDeEvento: any;
        medico: string;
        especialidade: string;
    }

    export class RegraConsultaTipoDeEventoListDto {
        tiposDeEventos: Array<number>;
    }

    export class RegraConsultaSistemaListDto {
        sistemas: Array<number>;
    }
}