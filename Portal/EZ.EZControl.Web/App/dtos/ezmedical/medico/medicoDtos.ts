﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../global/pessoa/pessoaDtos";
import * as pictureDtos from "../../core/picture/pictureDtos";
import * as especialidadeDtos from "../../ezmedical/especialidade/especialidadeDtos";

export module Dtos.EZMedical.Medico {
    export class MedicoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pictureId: number;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto;
        especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
        idsEspecialidades: number[];
    }

    export class MedicoNomeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        nome: string;
    }

    export class MedicoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        pictureId: number;
        especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
        idsEspecialidades: number[];
        pessoa: pessoaDtos.Dtos.Pessoa.PessoaInput;
        pessoaFisica: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput;
    }

    export class MedicoPessoaIdDto {
        medicoId: number;
        pessoaId: number;
        pictureId: number;
    }

    export class GetMedicoInput {
        nome: string;
        numeroDocumento: string;
    }

    export class GetPessoaExceptForMedico {
        nomePessoa: string;
    }
}