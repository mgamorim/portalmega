﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta {
    export class GetConfiguracaoDeBloqueioConsultaInput {
        titulo: string;
        motivoDoBloqueio: string;
        medico: string;
        especialidade: string;
    }

    export class ConfiguracaoDeBloqueioConsultaInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeEvento: any;
        sistema: any;
        motivoDoBloqueio: any;
        horarioInicio: string;
        horarioFim: string;
        inicioBloqueio: Date;
        fimBloqueio: Date;
        bloqueios: any;
        medicoId: number;
        especialidadeId: number;
    }

    export class ConfiguracaoDeBloqueioConsultaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeEvento: any;
        sistema: any;
        motivoDoBloqueio: any;
        horarioInicio: string;
        horarioFim: string;
        inicioBloqueio: Date;
        fimBloqueio: Date;
        bloqueios: any;
        medicoId: number;
        especialidadeId: number;
        medicoNomePessoa: string;
        especialidadeNome: string;
    }
}