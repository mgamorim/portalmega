﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as menuDtos from "../menu/menuDtos";
import * as categoriaDtos from "../categoria/categoriaDtos";
import * as publicacaoDtos from "../publicacao/publicacaoDtos";

export module Dtos.Cms.ItemDeMenu {
    export class GetItemDeMenuInput {
        id: number;
        titulo: string;
        descricaoDoTitulo: string;
        nomeMenu: string;
        url: string;
    }

    export class GetItemDeMenuExceptForInput {
        id: number;
        titulo: string;
    }

    export class ItemDeMenuInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        descricaoDoTitulo: string;
        menuId: number;
        itemMenuId: number;
        categoriaId: number;
        paginaId: number;
        tipoDeItemDeMenu: any;
        url: string;
    }

    export class ItemDeMenuListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        descricaoDoTitulo: string;
        menu: menuDtos.Dtos.Cms.Menu.MenuListDto;
        itemMenu: ItemDeMenuListDto;
        categoria: categoriaDtos.Dtos.Cms.Categoria.CategoriaListDto;
        pagina: publicacaoDtos.Dtos.Cms.Publicacao.PaginaListDto;
        tipoDeItemDeMenu: any;
        menuId: number;
        itemMenuId: number;
        categoriaId: number;
        paginaId: number;
        url: string;
    }
}