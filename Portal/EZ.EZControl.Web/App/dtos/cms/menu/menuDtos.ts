﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as siteDtos from "../site/siteDtos";

export module Dtos.Cms.Menu {
    export class GetMenuInput {
        nome: string;
        siteId: number;
        nomeSite: string;
    }

    export class MenuInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        siteId: number;
    }

    export class MenuListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        siteId: number;
        site: siteDtos.Dtos.CMS.Site.SiteListDto;
    }
}