﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Cms.Widget {
    export class GetWidgetInput {
        titulo: string;
    }

    export class WidgetInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        titulo: string;
        exibirTitulo: boolean;
        sistema: any;
        siteId: number;
        templateId: number;
        tipo: any;
        posicao: string;
        isActive: boolean;
        menuId: number;
        conteudo: string;
    }

    export class WidgetListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipo: any;
        titulo: string;
    }
}