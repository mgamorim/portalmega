﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.CMS.Host {
    export class HostListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        url: string;
        isPrincipal: boolean;
        siteId: number;
    }

    export class HostInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        constructor() {
            super();
            this.isActive = true;
            this.isPrincipal = false;
        }
        isActive: boolean;
        url: string;
        isPrincipal: boolean;
        siteId: number;
    }

    export class GetHostInput {
        url: string;
    }
}