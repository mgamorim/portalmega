﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as categoriaDtos from "../categoria/categoriaDtos";

export module Dtos.Cms.Link {
    export class LinkInput {
        tipoDeLink: any;
        siteId: number;
        conteudoId: number;
        categoriaPaiId: number;
        titulo: string;
        slug: string;
        link: string;
        primeiraCategoria: categoriaDtos.Dtos.Cms.Categoria.CategoriaInput;
    }
}