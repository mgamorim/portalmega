﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as siteDtos from "../site/siteDtos";

export module Dtos.Cms.Tag {
    export class GetTagInput {
        nome: string;
    }

    export class TagInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        slug: string;
        descricao: string;
    }

    export class TagListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
    }

    export class GetTagExceptForIdInput {
        tagId: number;
        nome: string;
    }
}