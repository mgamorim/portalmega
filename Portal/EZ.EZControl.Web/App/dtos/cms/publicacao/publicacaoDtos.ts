﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as tagDtos from "../tag/tagDtos";
import * as categoriaDtos from "../categoria/categoriaDtos";

export module Dtos.Cms.Publicacao
{
    export class GetPublicacaoInput {
        titulo: string;
    }

    export class PesquisaPublicacaoDto {
        siteId: number;
        termpoPesquisa: string;
        incluiPosts: boolean;
        incluiPaginas: boolean;
    }

    export class PesquisaPublicacaoListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        site: string;
        publicacao: string;
        resumo: string;
        categoria: string;
        dataDePublicacao: Date;
        url: string;
    }

    export class PublicacaoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoDePublicacao: any;
        titulo: string;
        slug: string;
        conteudo: string;
        idImagemDestaque: number;
        siteId: number;
        statusDaPublicacaoEnum: any;
        dataDePublicacao: Date;
        link: string;
    }

    export class PublicacaoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipoDePublicacao: any;
        titulo: string;
        idImagemDestaque: number;
        dataDePublicacao: Date;
        conteudo: string;
        siteId: number;
        statusDaPublicacaoEnum: any;
        slug: string;
    }

    export class GetPostInput extends GetPublicacaoInput {
    }

    export class PostInput extends PublicacaoInput {
        categorias: categoriaDtos.Dtos.Cms.Categoria.CategoriaInput[];
        tags: tagDtos.Dtos.Cms.Tag.TagInput[];
    }

    export class PostListDto extends PublicacaoListDto {
        categorias: categoriaDtos.Dtos.Cms.Categoria.CategoriaListDto[];
        tags: tagDtos.Dtos.Cms.Tag.TagListDto[];
    }

    export class GetPaginaInput extends GetPublicacaoInput {
    }

    export class PaginaInput extends PublicacaoInput {
        tipoDePagina: string;
    }

    export class PaginaListDto extends PublicacaoListDto {
        tipoDePagina: string;
    }

    export class GetPaginaExceptForInput {
        siteId: number;
        titulo: string;
    }
}