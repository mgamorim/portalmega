﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as arquivoDtos from "../../core/arquivo/arquivoDtos";

export module Dtos.Arquivo.Cms {
    export class ArquivoCmsListDto extends arquivoDtos.Dtos.Arquivo.Core.ArquivoListDto {
    }

    export class ArquivoCmsInput extends arquivoDtos.Dtos.Arquivo.Core.ArquivoInput {
    }

    export class GetArquivoCmsInput {
        nome: string;
    }
}