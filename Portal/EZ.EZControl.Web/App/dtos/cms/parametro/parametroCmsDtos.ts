﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as parametroCoreDtos from "../../core/parametro/parametroDtos";

export module Dtos.CMS.Parametro {
    export class ParametroCmsListDto extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        conexaoSMTPId: number;
        conexaoSMTPNome: string;
        conexaoSMTPEnderecoDeEmailPadrao: string;
        conexaoSMTPServidorSMTP: string;
        conexaoSMTPPorta: number;
        conexaoSMTPUsuarioAutenticacao: string;
        conexaoSMTPSenhaAutenticacao: string;
        conexaoSMTPRequerAutenticacao: boolean;
        conexaoSMTPRequerConexaoCriptografada: boolean;
    }

    export class ParametroCmsInput extends parametroCoreDtos.Dtos.Parametro.Core.ParametroInput {
        
    }
}