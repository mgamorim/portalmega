﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Cms.Template {
    export class GetTemplateInput {
        nome: string;
        autor: string;
        descricao: string;
    }

    export class TemplateInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        autor: string;
        autorUrl: string;
        versao: string;
        nomeDoArquivo: string;
        posicoes: string;
        tiposDeWidgetSuportados: string;
        tiposDePaginasSuportadas: string;
        tipoDePaginaParaPaginaDefault: string;
        tipoDePaginaParaPostDefault: string;
        tipoDePaginaParaCategoriaDefault: string;
        tipoDePaginaParaPaginaInicialDefault: string;
    }

    export class TemplateListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        autor: string;
        autorUrl: string;
        versao: string;
        nomeDoArquivo: string;
        posicoes: string;
        tiposDeWidgetSuportados: string;
        tiposDePaginasSuportadas: string;
        tipoDePaginaParaPaginaDefault: string;
        tipoDePaginaParaPostDefault: string;
        tipoDePaginaParaCategoriaDefault: string;
        tipoDePaginaParaPaginaInicialDefault: string;
    }
}