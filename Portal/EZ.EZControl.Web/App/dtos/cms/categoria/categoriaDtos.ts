﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as siteDtos from "../../../dtos/cms/site/siteDtos";

export module Dtos.Cms.Categoria {
    export class CategoriaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        slug: string;
        link: string;
        categoriaPaiId: number;
        publicacaoId: number;
        siteId: number;
        site: siteDtos.Dtos.CMS.Site.SiteListDto;
    }

    export class CategoriaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        slug: string;
        link: string;
        categoriaPaiId: number;
        publicacaoId: number;
        siteId: number;
    }

    export class GetCategoriaInput {
        nome: string;
        nomeSite: string;
    }

    export class GetCategoriaExceptForIdInput {
        categoriaId: number;
        nome: string;
    }
}