﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.CMS.Site {
    export class SiteListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        hostPrincipal: string;
        metaKeywords: string;
        metaTags: string;
        owner: string;
        tipoDePublicacao: any;
        tipoDeConteudoRSS: any;
        limitePostPorPagina: number;
        limitePostPorRss: number;
        templateDefaultId: number;
        evitarMecanismoDeBusca: boolean;
        permitirComentarios: boolean;
        permitirLinks: boolean;
        autorInformaNomeEmail: boolean;
        notificarPorEmailNovoComentario: boolean;
        ativarModeracaoDeComentario: boolean;
        textoCss: string;
        textoJavaScript: string;
        habilitarPesquisaPost: boolean;
        habilitarPesquisaPagina: boolean;
        hosts: any;
        valor: string;
        tipoDePaginaParaPaginaDefault: string;
        tipoDePaginaParaPostDefault: string;
        tipoDePaginaParaCategoriaDefault: string;
        tipoDePaginaParaPaginaInicialDefault: string;
    }

    export class SiteInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        constructor() {
            super();
            this.isActive = true;
            this.limitePostPorPagina = 10;
            this.limitePostPorRss = 20;
            this.tipoDePublicacao = 1;
            this.tipoDeConteudoRSS = 2;
            this.limitePostPorPagina = 10;
            this.limitePostPorRss = 20;
            this.templateDefaultId = 0;
            this.evitarMecanismoDeBusca = true;
            this.permitirComentarios = false;
            this.permitirLinks = false;
            this.autorInformaNomeEmail = true;
            this.notificarPorEmailNovoComentario = true;
            this.ativarModeracaoDeComentario = true;
            this.textoCss = '';
            this.textoJavaScript = '';
            this.habilitarPesquisaPost = true;
            this.habilitarPesquisaPagina = true;
            this.hosts = [];
            this.tipoDePaginaParaPaginaDefault = '';
            this.tipoDePaginaParaPostDefault='';
            this.tipoDePaginaParaCategoriaDefault='';
            this.tipoDePaginaParaPaginaInicialDefault = '';
        }
        isActive: boolean;
        nome: string;
        descricao: string;
        hostPrincipal: string;
        metaKeywords: string;
        metaTags: string;
        owner: string;
        tipoDePublicacao: any;
        tipoDeConteudoRSS: any;
        limitePostPorPagina: number;
        limitePostPorRss: number;
        templateDefaultId: number;
        evitarMecanismoDeBusca: boolean;
        permitirComentarios: boolean;
        permitirLinks: boolean;
        autorInformaNomeEmail: boolean;
        notificarPorEmailNovoComentario: boolean;
        ativarModeracaoDeComentario: boolean;
        textoCss: string;
        textoJavaScript: string;
        habilitarPesquisaPost: boolean;
        habilitarPesquisaPagina: boolean;
        hosts: any;
        tipoDePaginaParaPaginaDefault: string;
        tipoDePaginaParaPostDefault: string;
        tipoDePaginaParaCategoriaDefault: string;
        tipoDePaginaParaPaginaInicialDefault: string;
    }

    export class GetSiteInput {
        nome: string;
        descricao: string;
        host: string;
    }
}