﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Agenda.ConfiguracaoDeBloqueio {
    export class GetConfiguracaoDeBloqueioInput {
        titulo: string;
        motivoDoBloqueio: string;
    }

    export class ConfiguracaoDeBloqueioInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeEvento: any;
        sistema: any;
        motivoDoBloqueio: any;
        horarioInicio: string;
        horarioFim: string;
        inicioBloqueio: Date;
        fimBloqueio: Date;
        bloqueios: any;
    }

    export class ConfiguracaoDeBloqueioListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeEvento: any;
        sistema: any;
        motivoDoBloqueio: any;
        horarioInicio: string;
        horarioFim: string;
        inicioBloqueio: Date;
        fimBloqueio: Date;
        bloqueios: any;
    }
}