﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as configuracaoDeDisponibilidadeDtos from "../../../dtos/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeDtos";

export module Dtos.Agenda.Disponibilidade {
    export class GetDisponibilidadeInput {
        data: string;
    }

    export class DisponibilidadeInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        data: Date;
        horario: Date;
        configuracaoDeDisponibilidade: configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto;
    }

    export class DisponibilidadeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        data: Date;
        horario: Date;
        configuracaoDeDisponibilidade: configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto;
    }
}