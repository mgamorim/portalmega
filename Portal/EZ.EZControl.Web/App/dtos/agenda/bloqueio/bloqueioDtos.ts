﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as configuracaoDeBloqueioDtos from "../../../dtos/agenda/configuracaoDeBloqueio/configuracaoDeBloqueioDtos";

export module Dtos.Agenda.Bloqueio {
    export class GetBloqueioInput {
        data: string;
    }

    export class BloqueioInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        data: Date;
        horario: Date;
        configuracaoDeBloqueio: configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioListDto;
    }

    export class BloqueioListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        data: Date;
        horario: Date;
        configuracaoDeBloqueio: configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioListDto;
    }
}