﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as parametroCoreDtos from "../../core/parametro/parametroDtos";

export module Dtos.Agenda.Parametros.Regras {

    export class RegraBaseInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity  {
        sistema: any;
        tipoDeEvento: any;
        diasExibidosDesdeHojeAteAgendamento: number;
        vagasDisponibilizadasPorDia: number;
        vagasReservadasPorDia: number;
        unidadeDeTempo: number;
        vagasPorUnidadeDeTempo: number;
        diasAntesDoAgendamentoProibidoAlterar: number;
        diasParaNovoAgendamentoPorParticipante: number;
        overBook: boolean;
        isActive: boolean;
    }

    export class RegraBaseListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        sistema: any;
        tipoDeEvento: any;
        diasExibidosDesdeHojeAteAgendamento: number;
        vagasDisponibilizadasPorDia: number;
        vagasReservadasPorDia: number;
        unidadeDeTempo: number;
        vagasPorUnidadeDeTempo: number;
        diasAntesDoAgendamentoProibidoAlterar: number;
        diasParaNovoAgendamentoPorParticipante: number;
        overBook: boolean;
        isActive: boolean;
    }

    export class GetRegraBaseInput {
        sistema: any;
        tipoDeEvento: any;
    }

    export class RegraBaseTipoDeEventoListDto {
        tiposDeEventos: Array<number>;
    }

    export class RegraBaseSistemaListDto {
        sistemas: Array<number>;
    }
}