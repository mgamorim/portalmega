﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as parametroCoreDtos from "../../core/parametro/parametroDtos";

export module Dtos.Agenda.Parametros.Padroes {

    export class ParametroAgendaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {

    }


    export class ParametroAgendaInput extends parametroCoreDtos.Dtos.Parametro.Core.ParametroInput {
        
    }
}