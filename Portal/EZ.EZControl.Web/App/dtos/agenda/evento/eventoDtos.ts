﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../dtos/global/pessoa/pessoaDtos";

export module Dtos.Agenda.Evento {
    export class GetEventoInput {
        titulo: string;
        inicio: Date;
        termino: Date;
        sistema: SistemaEnum;
        tipo: TipoDeEventoEnum;
    }

    export class GetEventoExceptForInput {
        id: number;
        titulo: string;
    }

    export class EventoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        isActive: boolean;
        descricao: string;
        termino: Date;
        terminoTimeZone: Date;
        diaInteiro: boolean;
        inicio: Date;
        inicioTimeZone: Date;
        eventoPaiId: number;
        ownerId: number;
        disponibilidadeId: number;
        recurrenceRule: string;
        recurrenceException: string;
        titulo: string;
        tipo: TipoDeEventoEnum;
        sistema: SistemaEnum;
        statusDoEvento: StatusDoEventoEnum;
        participantes: pessoaDtos.Dtos.Pessoa.PessoaInput[];
        idsParticipantes: number[];
        observacao: string;
    }
    export class EventoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        id: number;
        isActive: boolean;
        descricao: string;
        termino: Date;
        terminoTimeZone: Date;
        diaInteiro: boolean;
        inicio: Date;
        inicioTimeZone: Date;
        eventoPaiId: number;
        ownerId: number;
        disponibilidadeId: number;
        recurrenceRule: string;
        recurrenceException: string;
        titulo: string;
        tipo: TipoDeEventoEnum;
        sistema: SistemaEnum;
        statusDoEvento: StatusDoEventoEnum;
        participantes: pessoaDtos.Dtos.Pessoa.PessoaInput[];
        observacao: string;
    }
}