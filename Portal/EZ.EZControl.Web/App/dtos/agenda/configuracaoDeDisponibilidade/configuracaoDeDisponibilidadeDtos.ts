﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.Agenda.ConfiguracaoDeDisponibilidade {
    export class GetConfiguracaoDeDisponibilidadeInput {
        titulo: string;
        tipoDeDisponibilidade: string;
    }

    export class ConfiguracaoDeDisponibilidadeInput extends  applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeDisponibilidade: any;
        tipoDeEvento: any;
        sistema: any;
        diasDaSemana: string;
        numeroDeDias: number;
        dataEspecifica: Date;
        horarioInicio: any;
        horarioFim: any;
        dataInicioValidade: Date;
        dataFimValidade: Date;
        semana: any;
        disponibilidades: any;
    }

    export class ConfiguracaoDeDisponibilidadeListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        titulo: string;
        tipoDeDisponibilidade: any;
        tipoDeEvento: any;
        sistema: any;
        diasDaSemana: string;
        numeroDeDias: number;
        dataEspecifica: Date;
        horarioInicio: any;
        horarioFim: any;
        dataInicioValidade: Date;
        dataFimValidade: Date;
        semana: any;
        disponibilidades: any;
    }
}