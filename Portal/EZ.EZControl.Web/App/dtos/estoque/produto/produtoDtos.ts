﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as naturezaDtos from "../../estoque/parametro/naturezaDtos";
import * as unidadeMedidaDtos from "../../estoque/parametro/unidadeMedidaDtos";
import * as arquivoDtos from "../../core/arquivo/arquivoDtos";

export module Dtos.Produto {

    export class ProdutoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        valor: number;
        isValorAjustavel: boolean;
        tipoDeProduto: any;
        naturezaId: number;
        natureza: naturezaDtos.Dtos.Natureza.NaturezaListDto;
        unidadeMedidaId: number;
        unidadeMedida: unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
    }

    export class ProdutoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        nome: string;
        descricao: string;
        valor: number;
        isValorAjustavel: boolean;
        tipoDeProduto: any;
        naturezaId: number;
        unidadeMedidaId: number;
        imagemId: number;
        imagem: arquivoDtos.Dtos.Arquivo.Core.ArquivoInput;
    }

    export class GetProdutoInput {
        nome: string;
    }
}