﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as produtoDtos from "../../estoque/produto/produtoDtos";
import * as entradaDtos from "../../estoque/entrada/entradaDtos";
import * as localArmazenamentoDtos from "../../estoque/parametro/localArmazenamentoDtos";

export module Dtos.EntradaItem {

    export class EntradaItemListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        entradaId: number;
        entradaItem: entradaDtos.Dtos.Entrada.EntradaListDto;
        produtoId: number;
        produto: produtoDtos.Dtos.Produto.ProdutoListDto;
        localArmazenamentoId: number;
        localArmazenamento: localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto;
        quantidade: number;
        validade: Date;
        valorUnitario: number;
        total: number;        
    }

    export class EntradaItemInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        entradaId: number;
        produtoId: number;
        localArmazenamentoId: number;
        quantidade: number;
        validade: Date;
        valorUnitario: number;        
    }

    export class GetEntradaItemInput {
        entradaId: number;
        localArmazenamentoId: number;
        id: number;
    }
}