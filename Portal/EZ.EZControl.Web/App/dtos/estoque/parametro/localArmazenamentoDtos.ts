﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as empresaDtos from "../../global/empresa/empresaDtos";

export module Dtos.LocalArmazenamento {

    export class LocalArmazenamentoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        empresaId: number;
        empresa: empresaDtos.Dtos.Empresa.EmpresaListDto;        
    }

    export class LocalArmazenamentoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        empresaId: number;
    }

    export class GetLocalArmazenamentoInput {
        descricao: string;
    }
}