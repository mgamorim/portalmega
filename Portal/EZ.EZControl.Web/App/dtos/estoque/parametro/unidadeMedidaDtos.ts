﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.UnidadeMedida {

    export class UnidadeMedidaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        unidadePadrao: any;
        unidadePadraoValue: string;
        valor: number;
    }

    export class UnidadeMedidaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        unidadePadrao: any;
        valor: number;
    }

    export class GetUnidadeMedidaInput {
        descricao: string;
    }
}