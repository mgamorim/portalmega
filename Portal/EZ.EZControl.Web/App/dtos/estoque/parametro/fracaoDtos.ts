﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as unidadeMedidaDtos from "../parametro/unidadeMedidaDtos";

export module Dtos.Fracao {

    export class FracaoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        unidadeMedidaId: number;
        unidadeMedida: unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto;
        valor: number;
    }

    export class FracaoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        descricao: string;
        unidadeMedidaId: number;
        valor: number;
    }

    export class GetFracaoInput {
        descricao: string;
    }
}