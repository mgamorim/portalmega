﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as empresaDtos from "../../global/empresa/empresaDtos";
import * as pedidoDtos from "../../vendas/pedido/pedidoDtos";
import * as saidaItemDtos from "../../estoque/saidaItem/saidaItemDtos";

export module Dtos.Saida {

    export class SaidaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        empresaId: number;
        empresa: empresaDtos.Dtos.Empresa.EmpresaListDto;
        pedidoId: number;
        pedido: pedidoDtos.Dtos.Pedido.PedidoListDto;
        dataSaida: Date;   
        motivoSaida: any;
    }

    export class SaidaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        empresaId: number;
        pedidoId: number;
        dataSaida: Date;
        motivoSaida: any;
        quantidade: number;
        items: Array<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>;
        constructor() {
            super();
            this.items = new Array<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>();
        }
        gridItens: any;
    }

    export class GetSaidaInput {
        id: number;        
        pedidoId: number;
    }
}