﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as empresaDtos from "../../global/empresa/empresaDtos";
import * as fornecedorDtos from "../../global/fornecedor/fornecedorDtos";
import * as entradaItemDtos from "../../estoque/entradaItem/entradaItemDtos";

export module Dtos.Entrada {

    export class EntradaListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        empresaId: number;
        empresa: empresaDtos.Dtos.Empresa.EmpresaListDto;
        dataPedido: Date;
        dataEntrada: Date;
        FornecedorId: number;
        fornecedor: fornecedorDtos.Dtos.Fornecedor.FornecedorListDto;
        notaFiscal: string;
        valorFrete: number;
    }

    export class EntradaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        empresaId: number;
        dataPedido: Date;
        dataEntrada: Date;
        fornecedorId: number;
        notaFiscal: string;
        valorFrete: number;
        items: Array<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>;
        constructor() {
            super();
            this.items = new Array<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>();
        }
        gridItens: any;
    }

    export class GetEntradaInput {
        id: number;
        fornecedorId: number;
    }
}