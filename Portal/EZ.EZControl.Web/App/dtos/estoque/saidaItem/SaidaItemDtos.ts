﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as produtoDtos from "../../estoque/produto/produtoDtos";
import * as localArmazenamentoDtos from "../../estoque/parametro/localArmazenamentoDtos";
import * as saidaDtos from "../../estoque/saida/saidaDtos";

export module Dtos.SaidaItem {

    export class SaidaItemListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        saida: saidaDtos.Dtos.Saida.SaidaListDto;
        saidaId: number;
        localArmazenamento: localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto;
        localArmazenamentoId: number;
        produto: produtoDtos.Dtos.Produto.ProdutoListDto;
        produtoId: number;
        quantidade: number;
        valorUnitario: number;
        total: number;
    }

    export class SaidaItemInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        saidaId: number;
        localArmazenamentoId: number;
        produtoId: number;
        quantidade: number;
        valorUnitario: number;       
    }

    export class GetSaidaItemInput {
        saidaId: number;
        localArmazenamentoId: number;
        id: number;
    }
}