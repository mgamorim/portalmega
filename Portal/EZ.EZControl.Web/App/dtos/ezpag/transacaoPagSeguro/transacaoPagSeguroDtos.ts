﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as pedidoDtos from "../../vendas/pedido/pedidoDtos"
import * as ezpagDtos from "../geral/ezpagDtos"
import * as enderecoDtos from "../../global/endereco/enderecoDtos"

export module Dtos.EZPag.TransacaoPagSeguro {
    export class CheckoutInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        tipo: TipoCheckoutPagSeguroEnum;
        pedido: pedidoDtos.Dtos.Pedido.PedidoInput;
        senderHash: string;
        holder: ezpagDtos.Dtos.EZPag.Geral.HolderInput;
        tokenCartao: string;
        opcaoDeParcelamento: ezpagDtos.Dtos.EZPag.Geral.ParcelamentoInput;
        nomeDoBanco: string;
    }
}