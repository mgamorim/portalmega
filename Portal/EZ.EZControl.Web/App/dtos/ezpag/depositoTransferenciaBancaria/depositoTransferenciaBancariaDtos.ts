﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as empresaDtos from "../../global/empresa/empresaDtos";
import * as ezpagDtos from "../geral/ezpagDtos"

export module Dtos.EZPag.DepositoTransferenciaBancariaDtos {
    export class DepositoTransferenciaBancariaInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        public id: number;
        public empresaId: number;
        public pessoaJuridicaId: number;
        public comprovante: string;
        public banco: string;
        public agencia: string;
        public conta: string;
        public titular: string;
        public descricao: string;
        public isActive: boolean;
    }
}