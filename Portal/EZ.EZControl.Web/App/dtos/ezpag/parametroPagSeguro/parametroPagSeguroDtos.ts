﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as ezpagDtos from "../geral/ezpagDtos"

export module Dtos.EZPag.ParametroPagSeguroDtos {
    export class ParametroPagSeguroInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        public pessoaJuridicaId: number;
        public email: string;
        public token: string;
        public appId: string;
        public appKey: string;
    }
}