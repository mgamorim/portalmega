﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZPag.FormaDePagamento {
    export class FormaDePagamentoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipo: TipoCheckoutPagSeguroEnum;
        descricao: string;
    }
    export class FormaDePagamentoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isActive: boolean;
        tipo: TipoCheckoutPagSeguroEnum;
        descricao: string;
    }
    export class GetFormaDePagamentoInput {
        tipo: TipoCheckoutPagSeguroEnum;
        descricao: string;
    }
}