﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as ezpagDtos from "../geral/ezpagDtos"

export module Dtos.EZPag.Transacao {
    export class TransacaoListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        pedidoId: number;
        transactionId: string;
        referencia: string;
        data: Date;
        statusDoPagamento: StatusDoPagamentoEnum;
    }

    export class TransacaoInput extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        pedidoId: number;
        transactionId: string;
        referencia: string;
        data: Date;
        statusDoPagamento: StatusDoPagamentoEnum;
        linkParaPagamento: string;
        linkParaPagamentoCartao: string;
    }

    export class GetTransacaoInput {
        transactionId: string;
    }
}