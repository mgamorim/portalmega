﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";
import * as parametroCoreDtos from "../../core/parametro/parametroDtos";

export module Dtos.Ezpag.Parametro {

    export class ParametroEzpagListDto extends applicationServiceDtos.Dtos.ApplicationService.EzEntity {
        isTest: boolean;
    }


    export class ParametroEzpagInput extends parametroCoreDtos.Dtos.Parametro.Core.ParametroInput {
        isTest: boolean;
    }
}