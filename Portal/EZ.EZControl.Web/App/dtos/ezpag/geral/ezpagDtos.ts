﻿import * as applicationServiceDtos from "../../../application/services/dto/applicationServiceDtos";

export module Dtos.EZPag.Geral {
    export class HolderInput {
        nome: string;
        cpf: string;
        telefone: string;
        nascimento: Date;
    }

    export class ParcelamentoInput {
        numeroDeParcelas: number;
        valorDaParcela: number;
        numeroDeParcelasSemJuros: number;
    }

    export class SessionListDto {
        id: string;
    }
}