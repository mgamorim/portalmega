﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as medicoDtos from "../../../../dtos/ezmedical/medico/medicoDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IMedicoService extends services.Services.IServiceEntity {
    }

    export class MedicoService implements IMedicoService {
        static $inject = ['$q', 'abp.services.app.ezmedical.medico'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<medicoDtos.Dtos.EZMedical.Medico.MedicoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: medicoDtos.Dtos.EZMedical.Medico.GetMedicoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<medicoDtos.Dtos.EZMedical.Medico.MedicoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements medicoDtos.Dtos.EZMedical.Medico.GetMedicoInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                numeroDocumento: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                numeroDocumento: filtro.numeroDocumento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getMedicosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: medicoDtos.Dtos.EZMedical.Medico.GetPessoaExceptForMedico, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements medicoDtos.Dtos.EZMedical.Medico.GetPessoaExceptForMedico, requestParams.RequestParam.IRequestParams {
                nome: string;
                nomePessoa: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nomePessoa,
                nomePessoa: filtro.nomePessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForMedico(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: medicoDtos.Dtos.EZMedical.Medico.MedicoInput): ng.IPromise<medicoDtos.Dtos.EZMedical.Medico.MedicoPessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}