﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as configuracaoDeDisponibilidadeConsultaDtos from "../../../../dtos/ezmedical/configuracaoDeDisponibilidadeConsulta/configuracaoDeDisponibilidadeConsultaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IConfiguracaoDeDisponibilidadeConsulta extends services.Services.IServiceEntity {
    }

    export class ConfiguracaoDeDisponibilidadeConsultaService implements IConfiguracaoDeDisponibilidadeConsulta {
        static $inject = ['$q', 'abp.services.app.ezmedical.configuracaoDeDisponibilidadeConsulta'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.GetConfiguracaoDeDisponibilidadeConsultaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.ConfiguracaoDeDisponibilidadeConsultaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.GetConfiguracaoDeDisponibilidadeConsultaInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                medico: string;
                especialidade: string;
                tipoDeDisponibilidade: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                medico: filtro.medico,
                especialidade: filtro.especialidade,
                tipoDeDisponibilidade : filtro.tipoDeDisponibilidade,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getConfiguracaoDeDisponibilidadeConsultasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: configuracaoDeDisponibilidadeConsultaDtos.Dtos.EZMedical.ConfiguracaoDeDisponibilidadeConsulta.GetConfiguracaoDeDisponibilidadeConsultaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        existeEvento(id: number): ng.IPromise<boolean> {
            var deferred = this.$q.defer();

            this.service.withEvento({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}