﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as regraConsultaDtos from "../../../../dtos/ezmedical/regraConsulta/regraConsultaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IRegraConsultaService extends services.Services.IServiceEntity {
    }

    export class RegraConsultaService implements IRegraConsultaService {
        static $inject = ['$q', 'abp.services.app.ezmedical.regraConsulta'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTiposDeEventosBySistema(sistema: number): ng.IPromise<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaTipoDeEventoListDto> {

            var deferred = this.$q.defer();

            this.service.getTiposDeEventosBySistema({ sistema })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getSistemasCriamEventos(): ng.IPromise<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaSistemaListDto> {

            var deferred = this.$q.defer();

            this.service.getSistemasCriamEventos()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: regraConsultaDtos.Dtos.EZMedical.RegraConsulta.GetRegraConsultaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements regraConsultaDtos.Dtos.EZMedical.RegraConsulta.GetRegraConsultaInput, requestParams.RequestParam.IRequestParams {
                sistema: any;
                tipoDeEvento: any;
                medico: string;
                especialidade: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                sistema: filtro.sistema,
                tipoDeEvento: filtro.tipoDeEvento,
                medico: filtro.medico,
                especialidade: filtro.especialidade,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        get(): ng.IPromise<regraConsultaDtos.Dtos.EZMedical.RegraConsulta.RegraConsultaInput> {
            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}