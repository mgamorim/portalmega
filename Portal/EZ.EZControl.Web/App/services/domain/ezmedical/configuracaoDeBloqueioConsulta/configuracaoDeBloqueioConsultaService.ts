﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as configuracaoDeBloqueioConsultaDtos from "../../../../dtos/ezmedical/configuracaoDeBloqueioConsulta/configuracaoDeBloqueioConsultaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IConfiguracaoDeBloqueioConsulta extends services.Services.IServiceEntity {
    }

    export class ConfiguracaoDeBloqueioConsultaService implements IConfiguracaoDeBloqueioConsulta {
        static $inject = ['$q', 'abp.services.app.ezmedical.configuracaoDeBloqueioConsulta'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.GetConfiguracaoDeBloqueioConsultaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.ConfiguracaoDeBloqueioConsultaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.GetConfiguracaoDeBloqueioConsultaInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                motivoDoBloqueio: string;
                medico: string;
                especialidade: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                motivoDoBloqueio: filtro.motivoDoBloqueio,
                medico: filtro.medico,
                especialidade: filtro.especialidade,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: configuracaoDeBloqueioConsultaDtos.Dtos.EZMedical.ConfiguracaoDeBloqueioConsulta.GetConfiguracaoDeBloqueioConsultaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}