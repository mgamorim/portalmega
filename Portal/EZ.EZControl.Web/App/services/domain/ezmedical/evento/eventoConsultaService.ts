﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as eventoConsultaDtos from "../../../../dtos/ezmedical/evento/eventoConsultaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEventoConsultaService extends services.Services.IServiceEntity {
    }

    export class EventoConsultaService implements IEventoConsultaService {
        static $inject = ['$q', 'abp.services.app.ezmedical.eventoConsulta'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.EventoConsultaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.EventoConsultaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.EventoConsultaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.GetEventoConsultaInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                inicio: Date;
                termino: Date;
                tipo: TipoDeEventoEnum;
                sistema: SistemaEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                inicio: filtro.inicio,
                termino: filtro.termino,
                tipo: filtro.tipo,
                sistema: filtro.sistema,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEventoPaiExceptForId(filtro: eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.EventoConsultaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.EventoConsultaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.GetEventoConsultaExceptForInput, requestParams.RequestParam.IRequestParams {
                id: number;
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEventoExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: eventoConsultaDtos.Dtos.EZMedical.EventoConsulta.EventoConsultaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}