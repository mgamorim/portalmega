﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEspecialidadeService extends services.Services.IServiceEntity {
    }

    export class EspecialidadeService implements IEspecialidadeService {
        static $inject = ['$q', 'abp.services.app.ezmedical.especialidade'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput, requestParams.RequestParam.IRequestParams {
                codigo: string;
                nome: string;
                ids: number[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                codigo: filtro.codigo,
                nome: filtro.nome,
                ids: filtro.ids,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getEspecialidadesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaginadoExceptForIds(filtro: especialidadeDtos.Dtos.EZMedical.Especialidade.GetEspecialidadeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto>> {
            var deferred = this.$q.defer();

            this.service.getPaginadoExceptForIds(filtro)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}