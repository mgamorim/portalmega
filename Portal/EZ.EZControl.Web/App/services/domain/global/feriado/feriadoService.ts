﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as feriadoDtos from "../../../../dtos/global/feriado/feriadoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IFeriadoService extends services.Services.IServiceEntity {
        getFeriadosList: (filtro: feriadoDtos.Dtos.Feriado.GetFeriadoInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<feriadoDtos.Dtos.Feriado.FeriadoListDto>>;
    }

    export class FeriadoService implements IFeriadoService {
        static $inject = ['$q', 'abp.services.app.global.feriado'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<feriadoDtos.Dtos.Feriado.FeriadoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // TODO: Por manter compatilidade do codigo esse método foi criado
        getFeriadosList(filtro: feriadoDtos.Dtos.Feriado.GetFeriadoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<feriadoDtos.Dtos.Feriado.FeriadoListDto>> {
            return this.getPaged(filtro, requestParams);
        }

        getPaged(filtro: feriadoDtos.Dtos.Feriado.GetFeriadoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<feriadoDtos.Dtos.Feriado.FeriadoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements feriadoDtos.Dtos.Feriado.GetFeriadoInput, requestParams.RequestParam.IRequestParams {
                data: Date;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                data: filtro.data,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getFeriadosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: feriadoDtos.Dtos.Feriado.FeriadoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}