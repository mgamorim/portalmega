﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as perfilDtos from "../../../../dtos/global/perfil/perfilDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IPerfilService extends services.Services.IServiceEntity {
    }

    export class PerfilService implements IPerfilService {
        static $inject = ['$q', 'abp.services.app.global.perfil'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        save(input: perfilDtos.Dtos.EZLiv.Perfil.PerfilInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // Método está sendo redirecionado para o metodo Get pois não existe o GetById por usar convenções.
        getById(id: number): ng.IPromise<perfilDtos.Dtos.EZLiv.Perfil.PerfilInput> {
          var deferred = this.$q.defer();

          this.service.get()
            .success(result => {
              deferred.resolve(result);
            })
            .error(error => {
              deferred.reject(error);
            });

          return deferred.promise;
        }

        // Método não existe no AppService
        getPaged(filtro: perfilDtos.Dtos.EZLiv.Perfil.GetPerfilInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<perfilDtos.Dtos.EZLiv.Perfil.PerfilListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements perfilDtos.Dtos.EZLiv.Perfil.GetPerfilInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // Método não existe no AppService
        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}