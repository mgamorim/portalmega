﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as clienteDtos from "../../../../dtos/global/cliente/clienteDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IClienteService extends services.Services.IServiceEntity {
    }

    export class ClienteService implements IClienteService {
        static $inject = ['$q', 'abp.services.app.global.cliente'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<clienteDtos.Dtos.Cliente.ClienteInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getIds(): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.getIds()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: clienteDtos.Dtos.Cliente.GetClienteInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<clienteDtos.Dtos.Cliente.ClienteListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements clienteDtos.Dtos.Cliente.GetClienteInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                numeroDocumento: string;
                tipoDeDocumento: any;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                numeroDocumento: filtro.numeroDocumento,
                tipoDeDocumento: filtro.tipoDeDocumento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getClientesPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as clienteDtos.Dtos.Cliente.ClienteListDto).pessoa.tipoPessoaValue = ez.domain.enum.tipoPessoaEnum.getItemPorValor((result.items[i] as clienteDtos.Dtos.Cliente.ClienteListDto).pessoa.tipoPessoa).descricao;
                        if ((result.items[i] as clienteDtos.Dtos.Cliente.ClienteListDto).pessoa.documentoPrincipal)
                            (result.items[i] as clienteDtos.Dtos.Cliente.ClienteListDto).pessoa.documentoPrincipal.tipoDeDocumentoFixoValue = ez.domain.enum.tipoDeDocumentoEnum.getItemPorValor((result.items[i] as clienteDtos.Dtos.Cliente.ClienteListDto).pessoa.documentoPrincipal.tipoDeDocumento.tipoDeDocumentoFixo);
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: clienteDtos.Dtos.Cliente.GetPessoaExceptForCliente, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements clienteDtos.Dtos.Cliente.GetPessoaExceptForCliente, requestParams.RequestParam.IRequestParams {
                nomePessoa: string;
                tipoPessoa: any;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePessoa: filtro.nomePessoa,
                tipoPessoa: filtro.tipoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCliente(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: clienteDtos.Dtos.Cliente.ClienteInput): ng.IPromise<clienteDtos.Dtos.Cliente.ClientePessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}