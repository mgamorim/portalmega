﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as empresaDtos from "../../../../dtos/global/empresa/empresaDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as userDtos from "../../../../dtos/core/user/userDtos";

export module Services {
    type EmpresaListDto = empresaDtos.Dtos.Empresa.EmpresaNomeListDto;
    type ListResultEmpresaListDto = applicationServiceDtos.Dtos.ApplicationService.ListResultOutput<EmpresaListDto>;

    export interface IEmpresaService extends services.Services.IServiceEntity {
        escolherEmpresaAjax: (empresaId: number, onSuccess: (data: any) => void, onError: (data: any) => void) => void;
        escolherEmpresa: (empresaId: number) => ng.IPromise<EmpresaListDto>;
        getEmpresasPorUsuarioLogado: () => ng.IPromise<(ListResultEmpresaListDto | EmpresaListDto)[]>;
    }

    export class EmpresaService implements IEmpresaService {
        static $inject = ['$q', 'abp.services.app.global.empresa'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        escolherEmpresaAjax(empresaId: number, onSuccess: (data: any) => void, onError: (data: any) => void) {
            abp.ajax({
                url: abp.appPath + 'Account/EscolherEmpresaAjax',
                type: 'POST',
                data: JSON.stringify({
                    empresaId: empresaId
                }),
                success(data) {
                    if (onSuccess) {
                        onSuccess(data);
                    }
                },
                error(error) {
                    if (onError) {
                        onError(error);
                    }
                }
            });
        }

        escolherEmpresa(empresaId: number): ng.IPromise<EmpresaListDto> {
            var deferred = this.$q.defer<EmpresaListDto>();

            this.service.escolherEmpresa(empresaId)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEmpresasPorUsuarioLogado(): ng.IPromise<(ListResultEmpresaListDto | EmpresaListDto)[]> {
            var deferred = [
                this.$q.defer<ListResultEmpresaListDto>(),
                this.$q.defer<EmpresaListDto>()
            ];

            var promises = [deferred[0].promise, deferred[1].promise];

            this.service.getEmpresasPorUsuarioLogado()
                .success(result => {
                    (deferred[0] as ng.IDeferred<ListResultEmpresaListDto>).resolve(result);
                })
                .error(error => {
                    (deferred[0] as ng.IDeferred<ListResultEmpresaListDto>).reject(error);
                });

            this.service.getEmpresaLogada()
                .success(result => {
                    (deferred[1] as ng.IDeferred<EmpresaListDto>).resolve(result);
                })
                .error(error => {
                    (deferred[1] as ng.IDeferred<EmpresaListDto>).reject(error);
                });

            return this.$q.all<ListResultEmpresaListDto | EmpresaListDto>(promises);
        }

        getById(id: number): ng.IPromise<empresaDtos.Dtos.Empresa.EmpresaListDto> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: empresaDtos.Dtos.Empresa.GetEmpresaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<empresaDtos.Dtos.Empresa.EmpresaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements empresaDtos.Dtos.Empresa.GetEmpresaInput, requestParams.RequestParam.IRequestParams {
                nomeFantasia: string;
                cnpj: string;
                slug: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeFantasia: filtro.nomeFantasia,
                cnpj: filtro.cnpj,
                slug: filtro.slug,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEmpresasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: empresaDtos.Dtos.Empresa.GetPessoaExceptForEmpresa, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements empresaDtos.Dtos.Empresa.GetPessoaExceptForEmpresa, requestParams.RequestParam.IRequestParams {
                nomePessoa: string;
                nomeFantasia: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePessoa: filtro.nomePessoa,
                nomeFantasia: filtro.nomeFantasia,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForEmpresa(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: empresaDtos.Dtos.Empresa.EmpresaInput): ng.IPromise<empresaDtos.Dtos.Empresa.EmpresaPessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getUsuariosNaoAssociados(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<userDtos.Dtos.User.Core.UserListDto>> {
            var deferred = this.$q.defer();

            this.service.getUsuariosNaoAssociados(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getUsuariosEmpresa(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<userDtos.Dtos.User.Core.UserListDto>> {
            var deferred = this.$q.defer();

            this.service.getUsuariosAssociados(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        associaUsuarioEmpresa(input: empresaDtos.Dtos.Empresa.AssociacaoUsuarioEmpresaInput): ng.IPromise<any> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.associaUsuarioEmpresa(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        desassociaUsuarioEmpresa(input: empresaDtos.Dtos.Empresa.AssociacaoUsuarioEmpresaInput): ng.IPromise<any> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.desassociaUsuarioEmpresa(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}