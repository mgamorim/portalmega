﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as enderecoDtos from "../../../../dtos/global/endereco/enderecoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEnderecoService extends services.Services.IServiceEntity {
    }

    export class EnderecoService implements IEnderecoService {
        static $inject = ['$q', 'abp.services.app.global.endereco'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<enderecoDtos.Dtos.Endereco.EnderecoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEnderecoByCep(filtro: enderecoDtos.Dtos.Endereco.GetEnderecoByCepInput): ng.IPromise<enderecoDtos.Dtos.Endereco.EnderecoInput> {
            var deferred = this.$q.defer();
                
            this.service.getEnderecoByCep(filtro)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: enderecoDtos.Dtos.Endereco.GetEnderecoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<enderecoDtos.Dtos.Endereco.EnderecoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements enderecoDtos.Dtos.Endereco.GetEnderecoInput, requestParams.RequestParam.IRequestParams {
                pessoaId: number;
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pessoaId: filtro.pessoaId,
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEnderecosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        var item = ez.domain.enum.tipoEnderecoEnum.getItemPorValor((result.items[i] as enderecoDtos.Dtos.Endereco.EnderecoListDto).tipoEndereco);
                        if (item != null) {
                            (result.items[i] as enderecoDtos.Dtos.Endereco.EnderecoListDto).tipoEnderecoValue = item.descricao;
                        }
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: enderecoDtos.Dtos.Endereco.EnderecoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}