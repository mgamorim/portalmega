﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as itemHierarquiaDtos from "../../../../dtos/global/itemHierarquia/itemHierarquiaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IItemHierarquiaService extends services.Services.IServiceEntity {
        getItensHierarquiaExceptForId: (filtro: itemHierarquiaDtos.Dtos.ItemHierarquia.GetItemHierarquiaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto>>;
    }

    export class ItemHierarquiaService implements IItemHierarquiaService {
        static $inject = ['$q', 'abp.services.app.global.itemHierarquia'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getItensHierarquiaExceptForId(filtro: itemHierarquiaDtos.Dtos.ItemHierarquia.GetItemHierarquiaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements itemHierarquiaDtos.Dtos.ItemHierarquia.GetItemHierarquiaExceptForIdInput, requestParams.RequestParam.IRequestParams {
                itemHierarquiaId: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                itemHierarquiaId: filtro.itemHierarquiaId,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getItensHierarquiaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: itemHierarquiaDtos.Dtos.ItemHierarquia.GetItemHierarquiaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements itemHierarquiaDtos.Dtos.ItemHierarquia.GetItemHierarquiaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                codigo: string;
                mascara: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                codigo: filtro.codigo,
                mascara: filtro.mascara,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getItensHierarquiaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: itemHierarquiaDtos.Dtos.ItemHierarquia.ItemHierarquiaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}