﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as telefoneDtos from "../../../../dtos/global/telefone/telefoneDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface ITelefoneService extends
        services.Services.IServiceEntity {
        getTelefonesList:
        (
            filtro: telefoneDtos.Dtos.Telefone.GetTelefoneInput,
            requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<telefoneDtos.Dtos.Telefone.TelefoneListDto>>;


    }

    export class TelefoneService implements ITelefoneService {
        static $inject = ['$q', 'abp.services.app.global.telefone'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<telefoneDtos.Dtos.Telefone.TelefoneInput> {
            var deferred = this.$q.defer();

            //this.service.getTelefoneById({ id })
            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTelefonesList(filtro: telefoneDtos.Dtos.Telefone.TelefoneInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<telefoneDtos.Dtos.Telefone.TelefoneListDto>> {
            return this.getPaged(filtro, requestParams);
        }

        getPaged(filtro: telefoneDtos.Dtos.Telefone.GetTelefoneInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<telefoneDtos.Dtos.Telefone.TelefoneListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements telefoneDtos.Dtos.Telefone.GetTelefoneInput, requestParams.RequestParam.IRequestParams {
                id: number;
                pessoaId: number;
                numero: string;
                tipo: TipoTelefoneEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                pessoaId: filtro.pessoaId,
                numero: filtro.numero,
                tipo: filtro.tipo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            //this.service.getTelefonesPaginado(request)
            //    .success(result => {
            //        for (var i = 0; i < result.items.length; i++) {
            //            (result.items[i] as telefoneDtos.Dtos.Telefone.TelefoneListDto).tipoValue = ez.domain.enum.TipoTelefoneEnum.getItemPorValor((result.items[i] as telefoneDtos.Dtos.Telefone.TelefoneListDto).tipo);
            //        }
            //        deferred.resolve(result);
            //    })

            this.service.getTelefonesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            //this.service.getTelefonesPaginado(request)
            //    .success(result => {
            //        for (var i = 0; i < result.items.length; i++) {
            //            (result.items[i] as telefoneDtos.Dtos.Telefone.TelefoneListDto).tipoTelefoneValue = ez.domain.enum.tipoTelefoneEnum.getItemPorValor((result.items[i] as telefoneDtos.Dtos.Telefone.TelefoneListDto).tipoTelefone).;
            //        }
            //        deferred.resolve(result);
            //    })
            //    .error(error => {
            //        deferred.reject(error);
            //    });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: telefoneDtos.Dtos.Telefone.TelefoneInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getByPessoaId(id: number): ng.IPromise<telefoneDtos.Dtos.Telefone.TelefoneInput> {
            var deferred = this.$q.defer();

            this.service.getByPessoaId({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}