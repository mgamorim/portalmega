﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as tratamentoDtos from "../../../../dtos/global/tratamento/tratamentoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ITratamentoService extends services.Services.IServiceEntity {
    }

    export class TratamentoService implements ITratamentoService {
        static $inject = ['$q', 'abp.services.app.global.tratamento'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<tratamentoDtos.Dtos.Tratamento.TratamentoInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: tratamentoDtos.Dtos.Tratamento.GetTratamentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<tratamentoDtos.Dtos.Tratamento.TratamentoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements tratamentoDtos.Dtos.Tratamento.GetTratamentoInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTratamentosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: tratamentoDtos.Dtos.Tratamento.TratamentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}