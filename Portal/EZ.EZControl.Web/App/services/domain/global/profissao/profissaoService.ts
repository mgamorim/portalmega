﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as profissaoDtos from "../../../../dtos/global/profissao/profissaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IProfissaoService extends services.Services.IServiceEntity {
    }

    export class ProfissaoService implements IProfissaoService {
        static $inject = ['$q', 'abp.services.app.global.profissao'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<profissaoDtos.Dtos.Profissao.ProfissaoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: profissaoDtos.Dtos.Profissao.GetProfissaoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<profissaoDtos.Dtos.Profissao.ProfissaoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements profissaoDtos.Dtos.Profissao.GetProfissaoInput, requestParams.RequestParam.IRequestParams {
                codigo: string;
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                codigo: filtro.codigo,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getProfissoesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: profissaoDtos.Dtos.Profissao.ProfissaoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        get(filtro: profissaoDtos.Dtos.Profissao.GetProfissaoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<profissaoDtos.Dtos.Profissao.ProfissaoListDto>> {
            var deferred = this.$q.defer();

            this.service.get(filtro)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedWithProduto(filtro: profissaoDtos.Dtos.Profissao.GetProfissaoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<profissaoDtos.Dtos.Profissao.ProfissaoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements profissaoDtos.Dtos.Profissao.GetProfissaoInput, requestParams.RequestParam.IRequestParams {
                codigo: string;
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                codigo: filtro.codigo,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getWithProduto(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}