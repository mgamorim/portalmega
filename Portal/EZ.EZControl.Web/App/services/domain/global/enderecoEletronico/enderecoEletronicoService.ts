﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as enderecoEletronicoDtos from "../../../../dtos/global/enderecoEletronico/enderecoEletronicoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEnderecoEletronicoService extends services.Services.IServiceEntity {
    }

    export class EnderecoEletronicoService implements IEnderecoEletronicoService {
        static $inject = ['$q', 'abp.services.app.global.enderecoEletronico'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getByPessoaId(id: number): ng.IPromise<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput> {
            var deferred = this.$q.defer();

            this.service.getByPessoaId({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }



        getPaged(filtro: enderecoEletronicoDtos.Dtos.EnderecoEletronico.GetEnderecoEletronicoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements enderecoEletronicoDtos.Dtos.EnderecoEletronico.GetEnderecoEletronicoInput, requestParams.RequestParam.IRequestParams {
                pessoaId: number;
                endereco: string;
                tipoDeEnderecoEletronico: any;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pessoaId: filtro.pessoaId,
                endereco: filtro.endereco,
                tipoDeEnderecoEletronico: filtro.tipoDeEnderecoEletronico,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEnderecosEletronicosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto).tipoDeEnderecoEletronicoValue = ez.domain.enum.tipoDeEnderecoEletronicoEnum.getItemPorValor((result.items[i] as enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoListDto).tipoDeEnderecoEletronico).descricao;
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: enderecoEletronicoDtos.Dtos.EnderecoEletronico.EnderecoEletronicoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}