﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IPessoaService extends services.Services.IServiceEntityReadOnly {
    }

    export class PessoaService implements IPessoaService {
        static $inject = ['$q', 'abp.services.app.global.pessoa'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<pessoaDtos.Dtos.Pessoa.PessoaListDto> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

      

        getPaged(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                tipoPessoa: any;
                descricaoGrupoPessoa: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                tipoPessoa: filtro.tipoPessoa,
                descricaoGrupoPessoa : filtro.descricaoGrupoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoas(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaExceptForId, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaExceptForId, requestParams.RequestParam.IRequestParams {
                nome: string;
                tipoPessoa: any;
                ids: number[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                tipoPessoa: filtro.tipoPessoa,
                ids: filtro.ids,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoasExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}