﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as tipoDeContatoDtos from "../../../../dtos/global/tipoDeContato/tipoDeContatoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface ITipoDeContatoService extends services.Services.IServiceEntity {
    }

    export class TipoDeContatoService implements ITipoDeContatoService {
        static $inject = ['$q', 'abp.services.app.global.tipoDeContato'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: tipoDeContatoDtos.Dtos.TipoDeContato.GetTipoDeContatoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements tipoDeContatoDtos.Dtos.TipoDeContato.GetTipoDeContatoInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTiposDeContatosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoListDto).tipoDeContatoFixoValue = ez.domain.enum.tipoDeContatoEnum.getItemPorValor(result.items[i]['tipoDeContatoFixo']).descricao;
                    };
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: tipoDeContatoDtos.Dtos.TipoDeContato.TipoDeContatoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}