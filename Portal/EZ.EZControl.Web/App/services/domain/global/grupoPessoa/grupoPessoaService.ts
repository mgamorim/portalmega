﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as grupoPessoaDtos from "../../../../dtos/global/grupoPessoa/grupoPessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IGrupoPessoaService extends services.Services.IServiceEntity {
        getGruposPessoa: (filtro: grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto>>;
        getGruposPessoaExceptForId: (filtro: grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto>>;
    }

    export class GrupoPessoaService implements IGrupoPessoaService {
        static $inject = ['$q', 'abp.services.app.global.grupoPessoa'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // TODO: Por manter compatibilidade com o codigo esse metodo foi criado
        getGruposPessoa(filtro: grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto>> {
            return this.getPaged(filtro, requestParams);
        }
        
        getGruposPessoaExceptForId(filtro: grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams) : ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaExceptForIdInput, requestParams.RequestParam.IRequestParams {
                grupoPessoaId: number;
                nome: string;
                tipoDePessoa: TipoPessoaEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                grupoPessoaId: filtro.grupoPessoaId,
                nome: filtro.nome,
                tipoDePessoa: filtro.tipoDePessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getGruposPessoaExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements grupoPessoaDtos.Dtos.GrupoPessoa.GetGrupoPessoaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getGruposPessoaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: grupoPessoaDtos.Dtos.GrupoPessoa.GrupoPessoaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}