﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as tipoDeLogradouroDtos from "../../../../dtos/global/tipoDeLogradouro/tipoDeLogradouroDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface ITipoDeLogradouroService extends services.Services.IServiceEntity {
    }

    export class TipoDeLogradouroService implements ITipoDeLogradouroService {
        static $inject = ['$q', 'abp.services.app.global.tipoDeLogradouro'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.GetTipoDeLogradouroInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.GetTipoDeLogradouroInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTiposDeLogradouroPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: tipoDeLogradouroDtos.Dtos.TipoDeLogradouro.TipoDeLogradouroInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}