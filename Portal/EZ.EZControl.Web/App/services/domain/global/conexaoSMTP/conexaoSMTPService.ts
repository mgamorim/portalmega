﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as conexaoSmtpDtos from "../../../../dtos/global/conexaoSmtp/conexaoSmtpDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IConexaoSmtpService extends services.Services.IServiceEntity {
        getConexoesSMTP: (filtro, requestParams: requestParams.RequestParam.IRequestParams) => angular.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto>>;
    }

    export class ConexaoSmtpService implements IConexaoSmtpService {
        static $inject = ['$q', 'abp.services.app.global.conexaoSMTP'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        save(input: conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpInput): angular.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {

            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): angular.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getById(id: number): angular.IPromise<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // TODO: Por manter compatilidade com o código existente. O correto seria utilizar o GetPaged
        getConexoesSMTP(filtro, requestParams: requestParams.RequestParam.IRequestParams): angular.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto>> {
            return this.getPaged(filtro, requestParams);
        }

        getPaged(filtro, requestParams: requestParams.RequestParam.IRequestParams): angular.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<conexaoSmtpDtos.Dtos.ConexaoSmtp.ConexaoSmtpListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements conexaoSmtpDtos.Dtos.ConexaoSmtp.GetConexaoSmtpInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                servidorSMTP: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                servidorSMTP: filtro.servidorSMTP,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getConexoesSMTPPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}