﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as clienteEzDtos from "../../../../dtos/global/clienteEz/clienteEzDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IClienteEzService extends services.Services.IServiceEntityGetSingle, services.Services.IServiceEntitySave {
    }

    export class ClienteEzService implements IClienteEzService {
        static $inject = ['$q', 'abp.services.app.global.clienteEZ'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<clienteEzDtos.Dtos.ClienteEz.ClienteEZDto> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: clienteEzDtos.Dtos.ClienteEz.ClienteEZDto): ng.IPromise<clienteEzDtos.Dtos.ClienteEz.ClienteEZPessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}