﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as paisDtos from "../../../../dtos/global/pais/paisDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IPaisService extends services.Services.IServiceEntity {
        getPaisByEstadoId: (id: number) => ng.IPromise<paisDtos.Dtos.Pais.PaisListDto>;
        getPaisByCidadeId: (id: number) => ng.IPromise<paisDtos.Dtos.Pais.PaisListDto>;
    }

    export class PaisService implements IPaisService {
        static $inject = ['$q', 'abp.services.app.global.pais'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getPaisByCidadeId(id: number): ng.IPromise<paisDtos.Dtos.Pais.PaisListDto> {

            var deferred = this.$q.defer();

            this.service.getPaisByCidadeId({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaisByEstadoId(id: number): ng.IPromise<paisDtos.Dtos.Pais.PaisListDto>{

            var deferred = this.$q.defer();

            this.service.getPaisByEstadoId({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getById(id: number): ng.IPromise<paisDtos.Dtos.Pais.PaisInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: paisDtos.Dtos.Pais.GetPaisInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<paisDtos.Dtos.Pais.PaisListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements paisDtos.Dtos.Pais.GetPaisInput, requestParams.RequestParam.IRequestParams {
                nomePais: string;
                nacionalidade: string;
                codigoISO: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePais: filtro.nomePais,
                nacionalidade: filtro.nacionalidade,
                codigoISO: filtro.codigoISO,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaisesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: paisDtos.Dtos.Pais.PaisInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}