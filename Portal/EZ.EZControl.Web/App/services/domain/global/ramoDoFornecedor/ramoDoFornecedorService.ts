﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as ramoDoFornecedorDtos from "../../../../dtos/global/ramoDoFornecedor/ramoDoFornecedorDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IRamoDoFornecedorService extends services.Services.IServiceEntity {
        getRamosDoFornecedor: (filtro: ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.GetRamoDoFornecedorInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto>>;
    }

    export class RamoDoFornecedorService implements IRamoDoFornecedorService {
        static $inject = ['$q', 'abp.services.app.global.ramoDoFornecedor'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // TODO: Para manter compatibilidade com o codigo existente esse metodo foi criado
        getRamosDoFornecedor(filtro: ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.GetRamoDoFornecedorInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto>> {
            return this.getPaged(filtro, requestParams);
        }

        getPaged(filtro: ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.GetRamoDoFornecedorInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.GetRamoDoFornecedorInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getRamosDoFornecedorPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: ramoDoFornecedorDtos.Dtos.RamoDoFornecedor.RamoDoFornecedorInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}