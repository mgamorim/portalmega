﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as documentoDtos from "../../../../dtos/global/documento/documentoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IDocumentoService extends services.Services.IServiceEntity {
    }

    export class DocumentoService implements IDocumentoService {
        static $inject = ['$q', 'abp.services.app.global.documento'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<documentoDtos.Dtos.Documento.DocumentoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: documentoDtos.Dtos.Documento.GetDocumentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<documentoDtos.Dtos.Documento.DocumentoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements documentoDtos.Dtos.Documento.GetDocumentoInput, requestParams.RequestParam.IRequestParams {
                pessoaId: number;
                numero: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pessoaId: filtro.pessoaId,
                numero: filtro.numero,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getDocumentosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as documentoDtos.Dtos.Documento.DocumentoListDto).tipoDeDocumentoFixoValue = ez.domain.enum.tipoDeDocumentoEnum.getItemPorValor((result.items[i] as documentoDtos.Dtos.Documento.DocumentoListDto).tipoDeDocumento.tipoDeDocumentoFixo).descricao;
                    };
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: documentoDtos.Dtos.Documento.DocumentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}