﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IPessoaJuridicaService extends services.Services.IServiceEntity {
    }

    export class PessoaJuridicaService implements IPessoaJuridicaService {
        static $inject = ['$q', 'abp.services.app.global.pessoaJuridica'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                tipoPessoa: any;
                descricaoGrupoPessoa: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                tipoPessoa: filtro.tipoPessoa,
                descricaoGrupoPessoa: filtro.descricaoGrupoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: pessoaDtos.Dtos.Pessoa.PessoaJuridicaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}