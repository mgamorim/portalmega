﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IPessoaFisicaService extends services.Services.IServiceEntity {
        getPessoas: (filtro: pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>>;
        getPessoaExceptForId: (filtro: pessoaDtos.Dtos.Pessoa.GetPessoaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>>;
    }

    export class PessoaFisicaService implements IPessoaFisicaService {
        static $inject = ['$q', 'abp.services.app.global.pessoaFisica'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<pessoaDtos.Dtos.Pessoa.PessoaFisicaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                tipoPessoa: any;
                descricaoGrupoPessoa: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                tipoPessoa: filtro.tipoPessoa,
                descricaoGrupoPessoa: filtro.descricaoGrupoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: pessoaDtos.Dtos.Pessoa.PessoaFisicaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaByCpf(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput): ng.IPromise<pessoaDtos.Dtos.Pessoa.PessoaListDto> {
            var deferred = this.$q.defer();

            this.service.getPessoaByCpf(filtro)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedbyCPF(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaByCPFListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaFisicaByCPFInput, requestParams.RequestParam.IRequestParams {
                cpfPessoaFisica: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                cpfPessoaFisica: filtro.cpfPessoaFisica,
                skipCount: 0,
                maxResultCount: 1,
                sorting: null
            }

            this.service.getByCPFPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        getPessoaExceptForIdOLD(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaExceptForId, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaExceptForId, requestParams.RequestParam.IRequestParams {
                nome: string;
                tipoPessoa: any;
                ids: number[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                tipoPessoa: filtro.tipoPessoa,
                ids: filtro.ids,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // TODO: Por manter compatibilidade com o codigo esse metodo foi criado
        getPessoas(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>> {
            return this.getPaged(filtro, requestParams);
        }

        getPessoaExceptForId(filtro: pessoaDtos.Dtos.Pessoa.GetPessoaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements pessoaDtos.Dtos.Pessoa.GetPessoaExceptForIdInput, requestParams.RequestParam.IRequestParams {
                Id: number;
                nome: string;
                tipoDePessoa: TipoPessoaEnum;
                ids: number[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                Id: filtro.Id,
                nome: filtro.nome,
                tipoDePessoa: filtro.tipoDePessoa,
                ids: filtro.ids,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}