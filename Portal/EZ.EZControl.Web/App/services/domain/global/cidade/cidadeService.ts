﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface ICidadeService extends services.Services.IServiceEntity {
        getCidadesByEstadoId: (input: cidadeDtos.Dtos.Cidade.GetCidadeByEstadoInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<cidadeDtos.Dtos.Cidade.CidadeListDto>>;
    }

    export class CidadeService implements ICidadeService {
        static $inject = ['$q', 'abp.services.app.global.cidade'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<cidadeDtos.Dtos.Cidade.CidadeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCidadesByEstadoId(input: cidadeDtos.Dtos.Cidade.GetCidadeByEstadoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<cidadeDtos.Dtos.Cidade.CidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements cidadeDtos.Dtos.Cidade.GetCidadeByEstadoInput, requestParams.RequestParam.IRequestParams {
                estadoId: number;
                nomeCidade: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            };

            var request: RequestType = {
                estadoId: input.estadoId,
                nomeCidade: input.nomeCidade,
                maxResultCount: requestParams.maxResultCount,
                skipCount: requestParams.skipCount,
                sorting: requestParams.sorting
            }

            this.service.getCidadesByEstadoId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: cidadeDtos.Dtos.Cidade.GetCidadeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<cidadeDtos.Dtos.Cidade.CidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements cidadeDtos.Dtos.Cidade.GetCidadeInput, requestParams.RequestParam.IRequestParams {
                nomeCidade: string;
                codigoIBGE: string;
                nomeEstado: string;
                nomePais: string;
                siglaEstado: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeCidade: filtro.nomeCidade,
                codigoIBGE: filtro.codigoIBGE,
                nomeEstado: filtro.nomeEstado,
                nomePais: filtro.nomePais,
                siglaEstado: '',
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCidadesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: cidadeDtos.Dtos.Cidade.CidadeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}