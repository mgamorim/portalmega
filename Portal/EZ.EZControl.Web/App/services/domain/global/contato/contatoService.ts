﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as contatoDtos from "../../../../dtos/global/contato/contatoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IContatoService extends services.Services.IServiceEntity {
    }

    export class ContatoService implements IContatoService {
        static $inject = ['$q', 'abp.services.app.global.contato'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<contatoDtos.Dtos.Contato.ContatoInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: contatoDtos.Dtos.Contato.GetContatoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<contatoDtos.Dtos.Contato.ContatoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements contatoDtos.Dtos.Contato.GetContatoInput, requestParams.RequestParam.IRequestParams {
                pessoaId: number;
                tipoDeContatoId: number;
                tratamentoId: number;
                nome: string;
                cargo: string;
                setor: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pessoaId: filtro.pessoaId,
                tipoDeContatoId: filtro.tipoDeContatoId,
                tratamentoId: filtro.tratamentoId,
                nome: filtro.nome,
                cargo: filtro.cargo,
                setor: filtro.setor,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getContatosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        var item = ez.domain.enum.sexoEnum.getItemPorValor((result.items[i] as contatoDtos.Dtos.Contato.ContatoListDto).sexo);

                        if (item != null) {
                            (result.items[i] as contatoDtos.Dtos.Contato.ContatoListDto).sexoValue = item.descricao;
                        }
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: contatoDtos.Dtos.Contato.ContatoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}