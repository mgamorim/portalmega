﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroGlobalDtos from "../../../../dtos/global/parametro/parametroGlobalDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IParametroGlobalService extends services.Services.IServiceEntityGetSingle, services.Services.IServiceEntitySave {
    }

    export class ParametroGlobalService implements IParametroGlobalService {
        static $inject = ['$q', 'abp.services.app.global.parametroGlobal'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<parametroGlobalDtos.Dtos.Parametro.Global.ParametroGlobalInput> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: parametroGlobalDtos.Dtos.Parametro.Global.ParametroGlobalInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}