﻿(function () {
    "use strict";
    appModule.service('emailService', ['$q', 'abp.services.app.email', function ($q, service) {
        
        this.sendEmail = function (emailDto) {
            var deferred = $q.defer();
            service.sendEmail(emailDto)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.sendEmailInput = function (email) {
            var deferred = $q.defer();

            service.sendEmailInput(email)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.sendEmailSmtp = function (destinatario, smtpId, templateId) {
            var deferred = $q.defer();
            service.sendEmailSmtp(destinatario, smtpId, templateId)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.sendEmailParameter = function (destinatario, parametroId, templateId) {
            var deferred = $q.defer();
            service.sendEmailParameter(destinatario, parametroId, templateId)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

    }]);
})();