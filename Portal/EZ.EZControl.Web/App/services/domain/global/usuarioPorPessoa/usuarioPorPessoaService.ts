﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as usuarioPorPessoaDtos from "../../../../dtos/global/usuarioPorPessoa/usuarioPorPessoaDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IUsuarioPorPessoaService extends services.Services.IServiceEntity {
    }

    export class UsuarioPorPessoaService implements IUsuarioPorPessoaService {
        static $inject = ['$q', 'abp.services.app.global.usuarioPorPessoa'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.GetUsuarioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.GetUsuarioInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getUsuariosPorPessoaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.GetPessoaExceptForUsuario, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.GetPessoaExceptForUsuario, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForUsuario(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaInput): ng.IPromise<usuarioPorPessoaDtos.Dtos.UsuarioPorPessoa.UsuarioPorPessoaPessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}