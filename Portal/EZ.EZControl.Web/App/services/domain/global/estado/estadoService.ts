﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEstadoService extends services.Services.IServiceEntity {
        getEstadosByPaisId: (input: estadoDtos.Dtos.Estado.GetEstadoByPaisInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<estadoDtos.Dtos.Estado.EstadoListDto>>;
        getEstadoByCidadeId: (id: number) => ng.IPromise<estadoDtos.Dtos.Estado.EstadoListDto>;
    }

    export class EstadoService implements IEstadoService {
        static $inject = ['$q', 'abp.services.app.global.estado'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<estadoDtos.Dtos.Estado.EstadoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEstadoByCidadeId(id: number): ng.IPromise<estadoDtos.Dtos.Estado.EstadoListDto> {
            var deferred = this.$q.defer();

            this.service.getEstadoByCidadeId({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEstadosByPaisId(input: estadoDtos.Dtos.Estado.GetEstadoByPaisInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<estadoDtos.Dtos.Estado.EstadoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements estadoDtos.Dtos.Estado.GetEstadoByPaisInput, requestParams.RequestParam.IRequestParams {
                paisId: number;
                nomeEstado: string;
                siglaEstado: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            };

            var request: RequestType = {
                paisId: input.paisId,
                nomeEstado: input.nomeEstado,
                siglaEstado: input.siglaEstado,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            };

            this.service.getEstadosByPaisId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: estadoDtos.Dtos.Estado.GetEstadoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<estadoDtos.Dtos.Estado.EstadoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements estadoDtos.Dtos.Estado.GetEstadoInput, requestParams.RequestParam.IRequestParams {
                nomeEstado: string;
                siglaEstado: string;
                capital: string;
                nomePais: string;
                codigoIBGE: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeEstado: filtro.nomeEstado,
                siglaEstado: filtro.siglaEstado,
                capital: filtro.capital,
                nomePais: filtro.nomePais,
                codigoIBGE: filtro.codigoIBGE,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEstadosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: estadoDtos.Dtos.Estado.EstadoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}