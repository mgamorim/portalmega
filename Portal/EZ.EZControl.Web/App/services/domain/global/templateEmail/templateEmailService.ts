﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as templateEmailDtos from "../../../../dtos/global/templateEmail/templateEmailDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ITemplateEmailService extends services.Services.IServiceEntity {
    }

    export class TemplateEmailService implements ITemplateEmailService {
        static $inject = ['$q', 'abp.services.app.global.templateEmail'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<templateEmailDtos.Dtos.TemplateEmail.TemplateEmailInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: templateEmailDtos.Dtos.TemplateEmail.GetTemplateEmailInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<templateEmailDtos.Dtos.TemplateEmail.TemplateEmailListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements templateEmailDtos.Dtos.TemplateEmail.GetTemplateEmailInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                remetente: string;
                assunto: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                remetente: filtro.remetente,
                assunto: filtro.assunto,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTemplatesEmailPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: templateEmailDtos.Dtos.TemplateEmail.TemplateEmailInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}