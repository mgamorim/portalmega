﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as agenciaDtos from "../../../../dtos/global/agencia/agenciaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IAgenciaService extends services.Services.IServiceEntity {
    }

    export class AgenciaService implements IAgenciaService {
        static $inject = ['$q', 'abp.services.app.global.agencia'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<agenciaDtos.Dtos.Agencia.AgenciaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: agenciaDtos.Dtos.Agencia.GetAgenciaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<agenciaDtos.Dtos.Agencia.AgenciaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements agenciaDtos.Dtos.Agencia.GetAgenciaInput, requestParams.RequestParam.IRequestParams {
                codigoBanco: string;
                nomeBanco: string;
                codigo: string;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                codigo: filtro.codigo,
                codigoBanco: filtro.codigoBanco,
                nomeBanco: filtro.nomeBanco,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getAgenciasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: agenciaDtos.Dtos.Agencia.AgenciaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}