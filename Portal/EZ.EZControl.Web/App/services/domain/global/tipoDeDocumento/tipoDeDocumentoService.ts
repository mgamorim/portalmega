﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as tipoDeDocumentoDtos from "../../../../dtos/global/tipoDeDocumento/tipoDeDocumentoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface ITipoDeDocumentoService extends services.Services.IServiceEntity {
    }

    export class TipoDeDocumentoService implements ITipoDeDocumentoService {
        static $inject = ['$q', 'abp.services.app.global.tipoDeDocumento'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                tipoPessoa: any;
                tipoDeDocumento: TipoDeDocumentoEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                tipoPessoa: filtro.tipoPessoa,
                tipoDeDocumento: filtro.tipoDeDocumento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTipoDeDocumentosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto).tipoPessoaValue = ez.domain.enum.tipoPessoaEnum.getItemPorValor(result.items[i]['tipoPessoa']).descricao;
                        (result.items[i] as tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto).tipoDeDocumentoFixoValue = ez.domain.enum.tipoDeDocumentoEnum.getItemPorValor(result.items[i]['tipoDeDocumentoFixo']).descricao;
                    };
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getByTipoFixo(filtro: tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput): ng.IPromise<tipoDeDocumentoDtos.Dtos.TipoDeDocumento.TipoDeDocumentoListDto> {
            var deferred = this.$q.defer();

            class RequestType implements tipoDeDocumentoDtos.Dtos.TipoDeDocumento.GetTipoDeDocumentoInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                tipoPessoa: any;
                tipoDeDocumento: TipoDeDocumentoEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                tipoPessoa: filtro.tipoPessoa,
                tipoDeDocumento: filtro.tipoDeDocumento,
                skipCount: 0,
                maxResultCount: 1,
                sorting: null
            }

            this.service.getTipoDeDocumentoByTipoFixo(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}