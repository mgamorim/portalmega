﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as fornecedorDtos from "../../../../dtos/global/fornecedor/fornecedorDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IFornecedorService extends services.Services.IServiceEntity {
    }

    export class FornecedorService implements IFornecedorService {
        static $inject = ['$q', 'abp.services.app.global.fornecedor'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: fornecedorDtos.Dtos.Fornecedor.GetFornecedorInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<fornecedorDtos.Dtos.Fornecedor.FornecedorListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements fornecedorDtos.Dtos.Fornecedor.GetFornecedorInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                numeroDocumento: string;
                tipoDeDocumento: any;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                numeroDocumento: filtro.numeroDocumento,
                tipoDeDocumento: filtro.tipoDeDocumento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getFornecedoresPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as fornecedorDtos.Dtos.Fornecedor.FornecedorListDto).pessoa.tipoPessoaValue = ez.domain.enum.tipoPessoaEnum.getItemPorValor((result.items[i] as fornecedorDtos.Dtos.Fornecedor.FornecedorListDto).pessoa.tipoPessoa).descricao;
                        if ((result.items[i] as fornecedorDtos.Dtos.Fornecedor.FornecedorListDto).pessoa.documentoPrincipal)
                            (result.items[i] as fornecedorDtos.Dtos.Fornecedor.FornecedorListDto).pessoa.documentoPrincipal.tipoDeDocumentoFixoValue = ez.domain.enum.tipoDeDocumentoEnum.getItemPorValor((result.items[i] as fornecedorDtos.Dtos.Fornecedor.FornecedorListDto).pessoa.documentoPrincipal.tipoDeDocumento.tipoDeDocumentoFixo);
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: fornecedorDtos.Dtos.Fornecedor.GetPessoaExceptForFornecedor, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements fornecedorDtos.Dtos.Fornecedor.GetPessoaExceptForFornecedor, requestParams.RequestParam.IRequestParams {
                nomePessoa: string;
                tipoPessoa: any;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePessoa: filtro.nomePessoa,
                tipoPessoa: filtro.tipoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForFornecedor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: fornecedorDtos.Dtos.Fornecedor.FornecedorInput): ng.IPromise<fornecedorDtos.Dtos.Fornecedor.FornecedorPessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}