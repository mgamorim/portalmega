﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as itemDePedidoDtos from "../../../../dtos/vendas/itemDePedido/itemDePedidoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IItemDePedidoService extends services.Services.IServiceEntityGetPaged {
    }

    export class ItemDePedidoService implements IItemDePedidoService {
        static $inject = ['$q', 'abp.services.app.vendas.itemDePedido'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getPaged(filtro: itemDePedidoDtos.Dtos.ItemPedido.GetItemDePedidoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemDePedidoDtos.Dtos.ItemPedido.ItemDePedidoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements itemDePedidoDtos.Dtos.ItemPedido.GetItemDePedidoInput, requestParams.RequestParam.IRequestParams {
                pedidoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pedidoId: filtro.pedidoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPedidoPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}