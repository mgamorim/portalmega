﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pedidoDtos from "../../../../dtos/vendas/pedido/pedidoDtos";
import * as propostaDtos from "../../../../dtos/ezliv/geral/propostaDeContratacao/propostaDeContratacaoDtos";
import * as depositoTransferenciaBancariaDtos from "../../../../dtos/ezpag/depositoTransferenciaBancaria/depositoTransferenciaBancariaDtos";

export module Services {

    export interface IVendaService extends services.Services.IServiceEntitySave {
    }

    export class VendaService implements IVendaService {
        static $inject = ['$q', 'abp.services.app.vendas.venda'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        save(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<pedidoDtos.Dtos.Pedido.PedidoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.realizaVendaProdutoPlanoDeSaude(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        realizaVendaProdutoPlanoDeSaudeDeposito(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.realizaVendaProdutoPlanoDeSaudeDeposito(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        confirmaRecebimentoVendaProdutoPlanoDeSaudeDeposito(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.confirmaRecebimentoVendaProdutoPlanoDeSaudeDeposito(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getDadosDeposito(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDepositoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.getDadosDeposito(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getDadosDepositoBancarios(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<depositoTransferenciaBancariaDtos.Dtos.EZPag.DepositoTransferenciaBancariaDtos.DepositoTransferenciaBancariaInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.getDadosDepositoBancarios(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

    }
}