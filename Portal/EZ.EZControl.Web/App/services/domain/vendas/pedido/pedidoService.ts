﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as pedidoDtos from "../../../../dtos/vendas/pedido/pedidoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IPedidoService extends services.Services.IServiceEntity {
    }

    export class PedidoService implements IPedidoService {
        static $inject = ['$q', 'abp.services.app.vendas.pedido'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<pedidoDtos.Dtos.Pedido.PedidoInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: pedidoDtos.Dtos.Pedido.GetPedidoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pedidoDtos.Dtos.Pedido.PedidoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements pedidoDtos.Dtos.Pedido.GetPedidoInput, requestParams.RequestParam.IRequestParams {
                nomeCliente: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeCliente: filtro.nomeCliente,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPedidoPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: pedidoDtos.Dtos.Pedido.PedidoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}