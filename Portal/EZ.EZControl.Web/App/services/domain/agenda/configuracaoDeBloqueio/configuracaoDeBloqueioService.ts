﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as configuracaoDeBloqueioDtos from "../../../../dtos/agenda/configuracaoDeBloqueio/configuracaoDeBloqueioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IConfiguracaoDeBloqueio extends services.Services.IServiceEntity {
    }

    export class ConfiguracaoDeBloqueioService implements IConfiguracaoDeBloqueio {
        static $inject = ['$q', 'abp.services.app.agenda.configuracaoDeBloqueio'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.ConfiguracaoDeBloqueioListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.GetConfiguracaoDeBloqueioInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                motivoDoBloqueio: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                motivoDoBloqueio: filtro.motivoDoBloqueio,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: configuracaoDeBloqueioDtos.Dtos.Agenda.ConfiguracaoDeBloqueio.GetConfiguracaoDeBloqueioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}