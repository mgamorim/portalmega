﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroAgendaPadroesDtos from "../../../../dtos/agenda/parametros/parametrosPadroesAgendaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IParametroPadroesAgendaService extends services.Services.IServiceEntityGetSingle, services.Services.IServiceEntitySave {
    }

    export class ParametroPadroesAgendaService implements IParametroPadroesAgendaService {
        static $inject = ['$q', 'abp.services.app.agenda.parametroAgenda'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<parametroAgendaPadroesDtos.Dtos.Agenda.Parametros.Padroes.ParametroAgendaInput> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: parametroAgendaPadroesDtos.Dtos.Agenda.Parametros.Padroes.ParametroAgendaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}