﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as RegraBaseDtos from "../../../../dtos/agenda/parametros/parametrosRegrasAgendaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IRegraBaseService extends services.Services.IServiceEntity {
    }

    export class RegraBaseService implements IRegraBaseService {
        static $inject = ['$q', 'abp.services.app.agenda.regraBase'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<RegraBaseDtos.Dtos.Agenda.Parametros.Regras.RegraBaseInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTiposDeEventosBySistema(sistema: number): ng.IPromise<RegraBaseDtos.Dtos.Agenda.Parametros.Regras.RegraBaseTipoDeEventoListDto> {

            var deferred = this.$q.defer();

            this.service.getTiposDeEventosBySistema({ sistema })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getSistemasCriamEventos(): ng.IPromise<RegraBaseDtos.Dtos.Agenda.Parametros.Regras.RegraBaseSistemaListDto> {

            var deferred = this.$q.defer();

            this.service.getSistemasCriamEventos()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: RegraBaseDtos.Dtos.Agenda.Parametros.Regras.GetRegraBaseInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<RegraBaseDtos.Dtos.Agenda.Parametros.Regras.RegraBaseListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements RegraBaseDtos.Dtos.Agenda.Parametros.Regras.GetRegraBaseInput, requestParams.RequestParam.IRequestParams {
                sistema: any;
                tipoDeEvento: any;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                sistema: filtro.sistema,
                tipoDeEvento: filtro.tipoDeEvento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: RegraBaseDtos.Dtos.Agenda.Parametros.Regras.RegraBaseInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}