﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as disponibilidadeDtos from "../../../../dtos/agenda/disponibilidade/disponibilidadeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IDisponibilidadeService extends services.Services.IServiceEntity {
    }

    export class DisponibilidadeService implements IDisponibilidadeService {
        static $inject = ['$q', 'abp.services.app.agenda.disponibilidade'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<disponibilidadeDtos.Dtos.Agenda.Disponibilidade.DisponibilidadeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: disponibilidadeDtos.Dtos.Agenda.Disponibilidade.DisponibilidadeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<disponibilidadeDtos.Dtos.Agenda.Disponibilidade.DisponibilidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements disponibilidadeDtos.Dtos.Agenda.Disponibilidade.GetDisponibilidadeInput, requestParams.RequestParam.IRequestParams {
                data: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                data : filtro.data.toString(),
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getDisponibilidadesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: disponibilidadeDtos.Dtos.Agenda.Disponibilidade.GetDisponibilidadeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}