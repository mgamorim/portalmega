﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as configuracaoDeDisponibilidadeDtos from "../../../../dtos/agenda/configuracaoDeDisponibilidade/configuracaoDeDisponibilidadeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IConfiguracaoDeDisponibilidade extends services.Services.IServiceEntity {
    }

    export class ConfiguracaoDeDisponibilidadeService implements IConfiguracaoDeDisponibilidade {
        static $inject = ['$q', 'abp.services.app.agenda.configuracaoDeDisponibilidade'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.ConfiguracaoDeDisponibilidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.GetConfiguracaoDeDisponibilidadeInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                tipoDeDisponibilidade: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                tipoDeDisponibilidade : filtro.tipoDeDisponibilidade,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getConfiguracaoDeDisponibilidadesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: configuracaoDeDisponibilidadeDtos.Dtos.Agenda.ConfiguracaoDeDisponibilidade.GetConfiguracaoDeDisponibilidadeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        existeEvento(id: number): ng.IPromise<boolean> {
            var deferred = this.$q.defer();

            this.service.withEvento({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}