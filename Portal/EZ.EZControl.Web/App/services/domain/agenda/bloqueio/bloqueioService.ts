﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as bloqueioDtos from "../../../../dtos/agenda/bloqueio/bloqueioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IBloqueioService extends services.Services.IServiceEntity {
    }

    export class BloqueioService implements IBloqueioService {
        static $inject = ['$q', 'abp.services.app.agenda.bloqueio'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<bloqueioDtos.Dtos.Agenda.Bloqueio.BloqueioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: bloqueioDtos.Dtos.Agenda.Bloqueio.BloqueioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<bloqueioDtos.Dtos.Agenda.Bloqueio.BloqueioListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements bloqueioDtos.Dtos.Agenda.Bloqueio.GetBloqueioInput, requestParams.RequestParam.IRequestParams {
                data: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                data : filtro.data.toString(),
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getBloqueiosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: bloqueioDtos.Dtos.Agenda.Bloqueio.GetBloqueioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}