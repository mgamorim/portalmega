﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as eventoDtos from "../../../../dtos/agenda/evento/eventoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEventoService extends services.Services.IServiceEntity {
    }

    export class EventoService implements IEventoService {
        static $inject = ['$q', 'abp.services.app.agenda.eventoBase'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<eventoDtos.Dtos.Agenda.Evento.EventoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: eventoDtos.Dtos.Agenda.Evento.EventoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<eventoDtos.Dtos.Agenda.Evento.EventoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements eventoDtos.Dtos.Agenda.Evento.GetEventoInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                inicio: Date;
                termino: Date;
                tipo: TipoDeEventoEnum;
                sistema: SistemaEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                inicio: filtro.inicio,
                termino: filtro.termino,
                tipo: filtro.tipo,
                sistema: filtro.sistema,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEventosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEventoPaiExceptForId(filtro: eventoDtos.Dtos.Agenda.Evento.EventoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<eventoDtos.Dtos.Agenda.Evento.EventoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements eventoDtos.Dtos.Agenda.Evento.GetEventoExceptForInput, requestParams.RequestParam.IRequestParams {
                id: number;
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEventoExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: eventoDtos.Dtos.Agenda.Evento.GetEventoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}