﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as saidaDtos from "../../../../dtos/estoque/saida/saidaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ISaidaService extends services.Services.IServiceEntity {
    }

    export class SaidaService implements ISaidaService {
        static $inject = ['$q', 'abp.services.app.estoque.saida'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<saidaDtos.Dtos.Saida.SaidaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: saidaDtos.Dtos.Saida.GetSaidaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<saidaDtos.Dtos.Saida.SaidaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements saidaDtos.Dtos.Saida.GetSaidaInput, requestParams.RequestParam.IRequestParams {
                id: number;
                pedidoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,                
                pedidoId: filtro.pedidoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getSaidaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: saidaDtos.Dtos.Saida.SaidaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}