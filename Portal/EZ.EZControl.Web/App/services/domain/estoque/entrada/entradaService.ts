﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as entradaDtos from "../../../../dtos/estoque/entrada/entradaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IEntradaService extends services.Services.IServiceEntity {
    }

    export class EntradaService implements IEntradaService {
        static $inject = ['$q', 'abp.services.app.estoque.entrada'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<entradaDtos.Dtos.Entrada.EntradaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: entradaDtos.Dtos.Entrada.GetEntradaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<entradaDtos.Dtos.Entrada.EntradaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements entradaDtos.Dtos.Entrada.GetEntradaInput, requestParams.RequestParam.IRequestParams {
                id: number;
                fornecedorId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                fornecedorId: filtro.fornecedorId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEntradaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: entradaDtos.Dtos.Entrada.EntradaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}