﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as saidaItemDtos from "../../../../dtos/estoque/saidaItem/saidaItemDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ISaidaItemService extends services.Services.IServiceEntity {
        //getById(id: number): ng.IPromise<saidaItemDtos.Dtos.SaidaItem.SaidaItemInput>;
        //getPaged(filtro: saidaItemDtos.Dtos.SaidaItem.GetSaidaItemInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>>;
        //delete(id: number): ng.IPromise<any>;
        //save(input: saidaItemDtos.Dtos.SaidaItem.SaidaItemInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput>;
    }

    export class SaidaItemService implements ISaidaItemService {
        static $inject = ['$q', 'abp.services.app.estoque.saidaItem'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<saidaItemDtos.Dtos.SaidaItem.SaidaItemInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: saidaItemDtos.Dtos.SaidaItem.GetSaidaItemInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<saidaItemDtos.Dtos.SaidaItem.SaidaItemListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements saidaItemDtos.Dtos.SaidaItem.GetSaidaItemInput, requestParams.RequestParam.IRequestParams {
                id: number;
                saidaId: number;
                localArmazenamentoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                saidaId: filtro.saidaId,
                localArmazenamentoId: filtro.localArmazenamentoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getSaidaItemPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: saidaItemDtos.Dtos.SaidaItem.SaidaItemInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}