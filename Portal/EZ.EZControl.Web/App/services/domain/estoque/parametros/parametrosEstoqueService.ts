﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroEstoqueDtos from "../../../../dtos/estoque/parametro/parametroEstoqueDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IParametrosEstoqueService extends services.Services.IServiceEntityGetSingle, services.Services.IServiceEntitySave {
    }

    export class ParametrosEstoqueService implements IParametrosEstoqueService {
        static $inject = ['$q', 'abp.services.app.estoque.parametroEstoque'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<parametroEstoqueDtos.Dtos.ParametroEstoque.ParametroEstoqueInput> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: parametroEstoqueDtos.Dtos.ParametroEstoque.ParametroEstoqueInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}