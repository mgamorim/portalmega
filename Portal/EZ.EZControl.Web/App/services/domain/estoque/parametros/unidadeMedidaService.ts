﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as unidadeMedidaDtos from "../../../../dtos/estoque/parametro/unidadeMedidaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IUnidadeMedidaService extends services.Services.IServiceEntity {
    }

    export class UnidadeMedidaService implements IUnidadeMedidaService {
        static $inject = ['$q', 'abp.services.app.estoque.unidadeMedida'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: unidadeMedidaDtos.Dtos.UnidadeMedida.GetUnidadeMedidaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements unidadeMedidaDtos.Dtos.UnidadeMedida.GetUnidadeMedidaInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getUnidadeMedidaPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto).unidadePadraoValue = ez.domain.enum.unidadePadraoEnum.getItemPorValor((result.items[i] as unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaListDto).unidadePadrao).descricao;                        
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: unidadeMedidaDtos.Dtos.UnidadeMedida.UnidadeMedidaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}