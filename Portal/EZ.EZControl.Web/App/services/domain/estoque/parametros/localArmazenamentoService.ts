﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as localArmazenamentoDtos from "../../../../dtos/estoque/parametro/localArmazenamentoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ILocalArmazenamentoService extends services.Services.IServiceEntity {}

    export class LocalArmazenamentoService implements ILocalArmazenamentoService {
        static $inject = ['$q', 'abp.services.app.estoque.localArmazenamento'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: localArmazenamentoDtos.Dtos.LocalArmazenamento.GetLocalArmazenamentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements localArmazenamentoDtos.Dtos.LocalArmazenamento.GetLocalArmazenamentoInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getLocalArmazenamentoPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: localArmazenamentoDtos.Dtos.LocalArmazenamento.LocalArmazenamentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}