﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as naturezaDtos from "../../../../dtos/estoque/parametro/naturezaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface INaturezaService extends services.Services.IServiceEntity {
    }

    export class NaturezaService implements INaturezaService {
        static $inject = ['$q', 'abp.services.app.estoque.natureza'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<naturezaDtos.Dtos.Natureza.NaturezaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: naturezaDtos.Dtos.Natureza.GetNaturezaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<naturezaDtos.Dtos.Natureza.NaturezaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements naturezaDtos.Dtos.Natureza.GetNaturezaInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getNaturezaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: naturezaDtos.Dtos.Natureza.NaturezaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}