﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as entradaItemDtos from "../../../../dtos/estoque/entradaItem/entradaItemDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IEntradaItemService extends services.Services.IServiceEntity {
        //getById(id: number): ng.IPromise<entradaItemDtos.Dtos.EntradaItem.EntradaItemInput>;
        //getPaged(filtro: entradaItemDtos.Dtos.EntradaItem.GetEntradaItemInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>>;
        //delete(id: number): ng.IPromise<any>;
        //save(input: entradaItemDtos.Dtos.EntradaItem.EntradaItemInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput>;
    }

    export class EntradaItemService implements IEntradaItemService {
        static $inject = ['$q', 'abp.services.app.estoque.entradaItem'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<entradaItemDtos.Dtos.EntradaItem.EntradaItemInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: entradaItemDtos.Dtos.EntradaItem.GetEntradaItemInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<entradaItemDtos.Dtos.EntradaItem.EntradaItemListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements entradaItemDtos.Dtos.EntradaItem.GetEntradaItemInput, requestParams.RequestParam.IRequestParams {
                id: number;
                entradaId: number;
                localArmazenamentoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                entradaId: filtro.entradaId,
                localArmazenamentoId: filtro.localArmazenamentoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEntradaItemPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: entradaItemDtos.Dtos.EntradaItem.EntradaItemInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}