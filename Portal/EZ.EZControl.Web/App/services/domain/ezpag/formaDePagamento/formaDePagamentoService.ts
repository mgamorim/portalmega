﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as formaDePagamentoDtos from "../../../../dtos/ezpag/formaDePagamento/formaDePagamentoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IFormaDePagamentoService extends
        services.Services.IServiceEntity {
        getFormaDePagamentosList:
        (
            filtro: formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.GetFormaDePagamentoInput,
            requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto>>;
    }

    export class FormaDePagamentoService implements IFormaDePagamentoService {
        static $inject = ['$q', 'abp.services.app.ezpag.formaDePagamento'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        // TODO: Por manter compatilidade do codigo esse método foi criado
        getFormaDePagamentosList(filtro: formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.GetFormaDePagamentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto>> {
            return this.getPaged(filtro, requestParams);
        }

        getPaged(filtro: formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.GetFormaDePagamentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.GetFormaDePagamentoInput, requestParams.RequestParam.IRequestParams {
                tipo: TipoCheckoutPagSeguroEnum;
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                tipo: filtro.tipo,
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getIsFormaPg(id : number): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoListDto>> {
            var deferred = this.$q.defer();

            this.service.getIsFormaPagamento(id)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: formaDePagamentoDtos.Dtos.EZPag.FormaDePagamento.FormaDePagamentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}