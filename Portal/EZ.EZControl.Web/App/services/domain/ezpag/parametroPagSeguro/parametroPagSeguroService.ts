﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroPagSeguroDtos from "../../../../dtos/ezpag/parametroPagSeguro/parametroPagSeguroDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as ezPagDtos from "../../../../dtos/ezpag/geral/ezpagDtos"

export module Services {
    export interface IParametroPagSeguroService extends services.Services.IServiceEntityBase {
    }

    export class ParametroPagSeguroService implements IParametroPagSeguroService {
        static $inject = ['$q', 'abp.services.app.ezpag.parametroPagSeguro'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<parametroPagSeguroDtos.Dtos.EZPag.ParametroPagSeguroDtos.ParametroPagSeguroInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}