﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as transacaoDtos from "../../../../dtos/ezpag/transacao/transacaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as ezPagDtos from "../../../../dtos/ezpag/geral/ezpagDtos"

export module Services {
    export interface ITransacaoService extends services.Services.IServiceEntityBase {
    }

    export class TransacaoService implements ITransacaoService {
        static $inject = ['$q', 'abp.services.app.ezpag.transacao'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<transacaoDtos.Dtos.EZPag.Transacao.TransacaoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements transacaoDtos.Dtos.EZPag.Transacao.GetTransacaoInput, requestParams.RequestParam.IRequestParams {
                transactionId: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                transactionId: filtro.transactionId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTransacaoPorPedidoId(id: number): ng.IPromise<transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput> {
            var deferred = this.$q.defer();

            this.service.getTransacaoPorPedidoId({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}