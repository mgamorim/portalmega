﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as transacaoPagSeguroDtos from "../../../../dtos/ezpag/transacaoPagSeguro/transacaoPagSeguroDtos";
import * as transacaoDtos from "../../../../dtos/ezpag/transacao/transacaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as ezPagDtos from "../../../../dtos/ezpag/geral/ezpagDtos"
import * as propostaDeContratacaoDtos from "../../../../dtos/ezliv/geral/propostaDeContratacao/propostaDeContratacaoDtos";

export module Services {
    export interface ITransacaoPagSeguroService extends services.Services.IServiceEntityBase {
    }

    export class TransacaoPagSeguroService implements ITransacaoPagSeguroService {
        static $inject = ['$q', 'abp.services.app.ezpag.transacaoPagSeguro'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        criarSessao(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<ezPagDtos.Dtos.EZPag.Geral.SessionListDto> {
            var deferred = this.$q.defer();

            this.service.criarSessao(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        realizarCheckout(input: transacaoPagSeguroDtos.Dtos.EZPag.TransacaoPagSeguro.CheckoutInput): ng.IPromise<transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput> {
            var deferred = this.$q.defer();

            var request = input;


            //this.service.realizaTransacaoIUGU(request)
            this.service.realizaCheckout(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        realizarCheckout2(input: propostaDeContratacaoDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto): ng.IPromise<transacaoDtos.Dtos.EZPag.Transacao.TransacaoInput> {
            var deferred = this.$q.defer();

            var request = input;


            //this.service.realizaTransacaoIUGU(request)
            this.service.realizaCheckout(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

    }
}