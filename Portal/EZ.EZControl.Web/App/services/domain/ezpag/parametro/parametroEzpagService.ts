﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroEzpagDtos from "../../../../dtos/ezpag/parametro/parametroEzpagDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IParametroEzpagService extends
        services.Services.IServiceEntityGetSingle,
        services.Services.IServiceEntitySave {
    }

    export class ParametroEzpagService implements IParametroEzpagService
    {
        static $inject = ['$q', 'abp.services.app.ezpag.parametroEzpag'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<parametroEzpagDtos.Dtos.Ezpag.Parametro.ParametroEzpagInput> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: parametroEzpagDtos.Dtos.Ezpag.Parametro.ParametroEzpagInput):
            ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput>
        {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}