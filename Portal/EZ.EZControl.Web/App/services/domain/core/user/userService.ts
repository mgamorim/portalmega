﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as userDtos from "../../../../dtos/core/user/userDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IUserService extends services.Services.IServiceEntity {
    }

    export class UserService implements IUserService {
        static $inject = ['$q', 'abp.services.app.user'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<userDtos.Dtos.User.Core.UserEditDto> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTipoDeUsuarioByUsuarioLogado(): ng.IPromise<userDtos.Dtos.User.Core.GetTipoDeUsuarioInput> {
            var deferred = this.$q.defer();

            this.service.getTipoDeUsuarioByUsuarioLogado()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: userDtos.Dtos.User.Core.GetUsersInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<userDtos.Dtos.User.Core.UserListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements userDtos.Dtos.User.Core.GetUsersInput, requestParams.RequestParam.IRequestParams {
                filter: string;
                permission: string;
                role: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                filter: filtro.filter,
                permission: filtro.permission,
                role: filtro.role,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getUsers(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: userDtos.Dtos.User.Core.UserEditDto): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}