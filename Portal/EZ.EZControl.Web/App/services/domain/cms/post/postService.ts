﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as publicacaoDtos from "../../../../dtos/cms/publicacao/publicacaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    import dtos = publicacaoDtos.Dtos.Cms.Publicacao;

    export interface IPublicacaoService extends services.Services.IServiceEntity {
    }

    export class PostService implements IPublicacaoService {
        static $inject = ['$q', 'abp.services.app.cms.post'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<publicacaoDtos.Dtos.Cms.Publicacao.PostInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: publicacaoDtos.Dtos.Cms.Publicacao.GetPostInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<publicacaoDtos.Dtos.Cms.Publicacao.PublicacaoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements publicacaoDtos.Dtos.Cms.Publicacao.GetPostInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPostsPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: publicacaoDtos.Dtos.Cms.Publicacao.PostInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}