﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as menuDtos from "../../../../dtos/cms/menu/menuDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IMenuService extends services.Services.IServiceEntity {
    }

    export class MenuService implements IMenuService {
        static $inject = ['$q', 'abp.services.app.cms.menu'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<menuDtos.Dtos.Cms.Menu.MenuInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: menuDtos.Dtos.Cms.Menu.GetMenuInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<menuDtos.Dtos.Cms.Menu.MenuListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements menuDtos.Dtos.Cms.Menu.GetMenuInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                siteId: number;
                nomeSite: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                siteId: filtro.siteId,
                nomeSite : filtro.nomeSite,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getMenusPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: menuDtos.Dtos.Cms.Menu.MenuInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}