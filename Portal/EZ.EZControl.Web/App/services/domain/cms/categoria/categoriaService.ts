﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as categoriaDtos from "../../../../dtos/cms/categoria/categoriaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    import dtos = categoriaDtos.Dtos.Cms.Categoria;

    export interface ICategoriaService extends services.Services.IServiceEntity {
        getCategoriasExceptForId: (filtro: dtos.GetCategoriaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams) => ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.CategoriaListDto>>;
    }

    export class CategoriaService implements ICategoriaService {
        static $inject = ['$q', 'abp.services.app.cms.categoria'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<dtos.CategoriaInput> {

            

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    var linkInput = {
                        tipoDeLink: ez.domain.enum.tipoDeLinkEnum.categoria,
                        siteId: result.siteId,
                        conteudoId: 0,
                        titulo: result.nome,
                        slug: result.slug
                    };
                    var promise = abp.services.app.cms.link.getLink(linkInput);
                    promise.then(function (resultLink) {
                        resultLink.link = result.link;
                    });


                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCategoriasExceptForId(filtro: dtos.GetCategoriaExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.CategoriaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements dtos.GetCategoriaExceptForIdInput, requestParams.RequestParam.IRequestParams {
                categoriaId: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                categoriaId: filtro.categoriaId,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCategoriasExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: dtos.GetCategoriaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.CategoriaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements dtos.GetCategoriaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                nomeSite: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                nomeSite: filtro.nomeSite,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCategoriasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }



        save(input: dtos.CategoriaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}