﻿(function () {

    "use strict";

    appModule.service('categoriaService', ['$q', 'abp.services.app.categoria', function ($q, service) {

        this.getById = function (id) {

            var deferred = $q.defer();

            service.getById({ id })
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.getCategoriasList = function (filtro, requestParams) {
            return this.getCategorias(filtro, requestParams)
        };

        this.GetItemDeMenuExceptForId = function (id) {

            var deferred = $q.defer();

            service.getCategoriasExceptForId({ id })
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.getCategorias = function (filtro, requestParams) {

            var deferred = $q.defer();

            var request = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            service.getCategoriasPaginado(request)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.delete = function (id) {

            var deferred = $q.defer();

            service.delete({ id })
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.save = function (categoria) {

            var deferred = $q.defer();

            var request = categoria;

            service.save(request)
            .success(function (result) {
                deferred.resolve(result);
            })
            .error(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };
    }]);
})();