﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as hostDtos from "../../../../dtos/cms/host/hostDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ISiteService extends services.Services.IServiceEntityReadOnly {
    }

    export class HostService implements ISiteService {
        static $inject = ['$q', 'abp.services.app.cms.site'];
        public hosts: any;

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<hostDtos.Dtos.CMS.Host.HostInput> {

            var deferred = this.$q.defer();

            deferred.resolve(this.hosts[0]);

            return deferred.promise;
        }

        getPaged(filtro: hostDtos.Dtos.CMS.Host.GetHostInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<hostDtos.Dtos.CMS.Host.HostListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements hostDtos.Dtos.CMS.Host.GetHostInput, requestParams.RequestParam.IRequestParams {
                url: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                url: filtro.url,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            var result = new applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<hostDtos.Dtos.CMS.Host.HostListDto>();

            for (var i = 0; i < this.hosts.length; i++) {
                var hostListDto = new hostDtos.Dtos.CMS.Host.HostListDto();

                hostListDto.isActive = this.hosts[i].isActive;
                hostListDto.url = this.hosts[i].url;
                hostListDto.siteId = this.hosts[i].siteId;

                result.items.push(hostListDto);
            }

            if (result.totalCount == 0 && this.hosts.length > 0)
                result.totalCount = this.hosts.length;

            deferred.resolve(result);

            return deferred.promise;
        }
    }
}