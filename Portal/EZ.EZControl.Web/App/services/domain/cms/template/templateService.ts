﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as templateDtos from "../../../../dtos/cms/template/templateDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    import dtos = templateDtos.Dtos.Cms.Template;

    export interface ITemplateService extends services.Services.IServiceEntityReadOnly {
    }

    export class TemplateService implements ITemplateService {
        static $inject = ['$q', 'abp.services.app.cms.template'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<templateDtos.Dtos.Cms.Template.TemplateInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: templateDtos.Dtos.Cms.Template.GetTemplateInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<templateDtos.Dtos.Cms.Template.TemplateListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements templateDtos.Dtos.Cms.Template.GetTemplateInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                descricao: string;
                autor: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                descricao: filtro.descricao,
                autor: filtro.autor,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTemplatesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getListPosicoes(id: number) {
            var deferred = this.$q.defer();

            this.service.getListPosicoes({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getListTiposDePaginas(id: number) {
            var deferred = this.$q.defer();

            this.service.getTiposDePaginasSuportadas({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTiposDePaginasSuportadasBySite(id: number) {
            var deferred = this.$q.defer();

            this.service.getTiposDePaginasSuportadasBySite({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getListTiposDeWidget(id: number) {
            var deferred = this.$q.defer();

            this.service.getTiposDeWidgetSuportados({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        sincronizar() {
            var deferred = this.$q.defer();

            this.service.sincronizar()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise; 
        }
    }
}