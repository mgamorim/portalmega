﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as widgetDtos from "../../../../dtos/cms/widget/widgetDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    import dtos = widgetDtos.Dtos.Cms.Widget;

    export interface IWidgetService extends services.Services.IServiceEntity {
    }

    export class WidgetService implements IWidgetService {
        static $inject = ['$q', 'abp.services.app.cms.widget'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<widgetDtos.Dtos.Cms.Widget.WidgetInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: widgetDtos.Dtos.Cms.Widget.GetWidgetInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<widgetDtos.Dtos.Cms.Widget.WidgetListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements widgetDtos.Dtos.Cms.Widget.GetWidgetInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: widgetDtos.Dtos.Cms.Widget.WidgetInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}