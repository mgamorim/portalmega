﻿(function () {

    "use strict";

    appModule.service('paginaService', ['$q', 'abp.services.app.pagina', 'abp.services.app.categoria', 'abp.services.app.site', 'abp.services.app.tag', function ($q, service, categoriaService, siteService, tagService) {

        this.getById = function (id) {

            var deferred = $q.defer();

            service.getById({ id })
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.getPaginasList = function (filtro, requestParams) {
            return this.getPaginas(filtro, requestParams)
        };

        this.getPaginas = function (filtro, requestParams) {

            var deferred = $q.defer();

            var request = {
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            service.getPaginasPaginado(request)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.getPaginaExceptForId = function (filtro, requestParams) {

            var deferred = $q.defer();

            var request = {
                siteId: filtro.siteId,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            service.getPaginaExceptForId(request)
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.delete = function (id) {

            var deferred = $q.defer();

            service.delete({ id })
                .success(function (result) {
                    deferred.resolve(result);
                })
                .error(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        };

        this.save = function (pagina) {

            var deferred = $q.defer();

            var request = pagina;

            service.save(request)
            .success(function (result) {
                deferred.resolve(result);
            })
            .error(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };

        this.getSites = function () {
            var filtro = {
                nome: ''
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: 'Nome'
            }

            return siteService.getSitesPaginado(filtro, requestParams);
        };
    }]);
})();