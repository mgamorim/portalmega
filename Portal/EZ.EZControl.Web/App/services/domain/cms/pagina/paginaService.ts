﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as publicacaoDtos from "../../../../dtos/cms/publicacao/publicacaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    import dtos = publicacaoDtos.Dtos.Cms.Publicacao;

    export interface IPublicacaoService extends services.Services.IServiceEntity {
    }

    export class PaginaService implements IPublicacaoService {
        static $inject = ['$q', 'abp.services.app.cms.pagina'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<publicacaoDtos.Dtos.Cms.Publicacao.PaginaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: publicacaoDtos.Dtos.Cms.Publicacao.GetPaginaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<publicacaoDtos.Dtos.Cms.Publicacao.PaginaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements publicacaoDtos.Dtos.Cms.Publicacao.GetPaginaInput, requestParams.RequestParam.IRequestParams {
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPublicacaosExceptForId(filtro: dtos.GetPaginaExceptForInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.PaginaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements dtos.GetPaginaExceptForInput, requestParams.RequestParam.IRequestParams {
                siteId: number;
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                siteId: filtro.siteId,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPublicacaosExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: publicacaoDtos.Dtos.Cms.Publicacao.PaginaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}