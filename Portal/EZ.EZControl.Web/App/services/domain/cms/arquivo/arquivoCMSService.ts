﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as arquivoDtos from "../../../../dtos/cms/arquivo/arquivoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    import dtos = arquivoDtos.Dtos.Arquivo.Cms;

    export interface IArquivoCmsService extends services.Services.IServiceEntity {
    }

    export class ArquivoCmsService implements IArquivoCmsService {
        static $inject = ['$q', 'abp.services.app.cms.arquivoCMS'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<dtos.ArquivoCmsInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: dtos.GetArquivoCmsInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.ArquivoCmsListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements dtos.GetArquivoCmsInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getArquivosPaginado(request)
                .success(result => {
                    for (var i = 0; i < result.items.length; i++) {
                        (result.items[i] as dtos.ArquivoCmsListDto).tipoDeArquivoFixoValue = ez.domain.enum.tipoDeArquivoEnum.getItemPorValor(1);
                    }
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: dtos.ArquivoCmsInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}