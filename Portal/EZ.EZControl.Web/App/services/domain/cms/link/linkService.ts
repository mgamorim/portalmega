﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as linkDtos from "../../../../dtos/cms/link/linkDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    import dtos = linkDtos.Dtos.Cms.Link;

    export interface ILinkService extends services.Services.IServiceEntityBase {
    }

    export class LinkService implements ILinkService {
        static $inject = ['$q', 'abp.services.app.cms.link'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getLink(linkInput: linkDtos.Dtos.Cms.Link.LinkInput): ng.IPromise<linkDtos.Dtos.Cms.Link.LinkInput> {

            var deferred = this.$q.defer();

            this.service.getLink(linkInput)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}