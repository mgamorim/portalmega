﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as tagDtos from "../../../../dtos/cms/tag/tagDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    import dtos = tagDtos.Dtos.Cms.Tag;

    export interface ITagService extends services.Services.IServiceEntity {
    }

    export class TagService implements ITagService {
        static $inject = ['$q', 'abp.services.app.cms.tag'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<tagDtos.Dtos.Cms.Tag.TagInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: tagDtos.Dtos.Cms.Tag.GetTagInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<tagDtos.Dtos.Cms.Tag.TagListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements tagDtos.Dtos.Cms.Tag.GetTagInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTagsPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getTagsExceptForId(filtro: dtos.GetTagExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.TagListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements dtos.GetTagExceptForIdInput, requestParams.RequestParam.IRequestParams {
                tagId: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                tagId: filtro.tagId,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getTagsExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: tagDtos.Dtos.Cms.Tag.TagInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}