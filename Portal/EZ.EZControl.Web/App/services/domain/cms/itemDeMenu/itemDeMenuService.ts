﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as itemDeMenuDtos from "../../../../dtos/cms/itemDeMenu/itemDeMenuDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    import dtos = itemDeMenuDtos.Dtos.Cms.ItemDeMenu;

    export interface IItemDeMenuService extends services.Services.IServiceEntity {
    }

    export class ItemDeMenuService implements IItemDeMenuService {
        static $inject = ['$q', 'abp.services.app.cms.itemDeMenu'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getItensDeMenuList(filtro: itemDeMenuDtos.Dtos.Cms.ItemDeMenu.GetItemDeMenuInput, requestParams: requestParams.RequestParam.IRequestParams) {
            return this.getPaged(filtro, requestParams);
        }

        getPaged(filtro: itemDeMenuDtos.Dtos.Cms.ItemDeMenu.GetItemDeMenuInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements itemDeMenuDtos.Dtos.Cms.ItemDeMenu.GetItemDeMenuInput, requestParams.RequestParam.IRequestParams {
                id: number;
                titulo: string;
                descricaoDoTitulo: string;
                nomeMenu: string;
                url: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                titulo: filtro.titulo,
                descricaoDoTitulo: filtro.descricaoDoTitulo,
                nomeMenu: filtro.nomeMenu,
                url: filtro.url,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getItensDeMenuPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getItemDeMenuExceptForId(filtro: dtos.GetItemDeMenuExceptForInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<dtos.ItemDeMenuListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements dtos.GetItemDeMenuExceptForInput, requestParams.RequestParam.IRequestParams {
                id: number;
                titulo: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                titulo: filtro.titulo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getItemDeMenuExceptForId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: itemDeMenuDtos.Dtos.Cms.ItemDeMenu.ItemDeMenuInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}