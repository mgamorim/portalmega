﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as siteDtos from "../../../../dtos/cms/site/siteDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ISiteService extends services.Services.IServiceEntity {
    }

    export class SiteService implements ISiteService {
        static $inject = ['$q', 'abp.services.app.cms.site'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<siteDtos.Dtos.CMS.Site.SiteInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: siteDtos.Dtos.CMS.Site.GetSiteInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<siteDtos.Dtos.CMS.Site.SiteListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements siteDtos.Dtos.CMS.Site.GetSiteInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                descricao: string;
                host: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                descricao: filtro.descricao,
                host: filtro.host,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getSitesPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: siteDtos.Dtos.CMS.Site.SiteInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}