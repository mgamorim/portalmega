﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroCmsDtos from "../../../../dtos/cms/parametro/parametroCmsDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IParametroCmsService extends services.Services.IServiceEntityGetSingle, services.Services.IServiceEntitySave {
    }

    export class ParametroCmsService implements IParametroCmsService {
        static $inject = ['$q', 'abp.services.app.cms.parametroCMS'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<parametroCmsDtos.Dtos.CMS.Parametro.ParametroCmsInput> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: parametroCmsDtos.Dtos.CMS.Parametro.ParametroCmsInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}