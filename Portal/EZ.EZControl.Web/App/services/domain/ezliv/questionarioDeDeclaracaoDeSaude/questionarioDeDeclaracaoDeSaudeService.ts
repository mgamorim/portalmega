﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as questionarioDeDeclaracaoDeSaudeDtos from "../../../../dtos/ezliv/geral/questionarioDeDeclaracaoDeSaude/questionarioDeDeclaracaoDeSaudeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IQuestionarioDeDeclaracaoDeSaudeService extends services.Services.IServiceEntity {
    }

    export class QuestionarioDeDeclaracaoDeSaudeService implements IQuestionarioDeDeclaracaoDeSaudeService {
        static $inject = ['$q', 'abp.services.app.ezliv.questionarioDeDeclaracaoDeSaude'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.GetQuestionarioDeDeclaracaoDeSaudeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.GetQuestionarioDeDeclaracaoDeSaudeInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: questionarioDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaudeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}