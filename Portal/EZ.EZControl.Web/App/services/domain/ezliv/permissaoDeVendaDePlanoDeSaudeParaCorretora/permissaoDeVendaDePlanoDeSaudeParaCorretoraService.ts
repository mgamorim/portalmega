﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretora/permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos";
import * as produtoDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService extends services.Services.IServiceEntity {
    }

    export class PermissaoDeVendaDePlanoDeSaudeParaCorretoraService implements IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretora'];
        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        permissoesDeCorretora(id: number): ng.IPromise<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.GetPermissoesPorCorretoraInput> {
            var deferred = this.$q.defer();

            this.service.permissoesDeCorretora({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getProdutoECorretorasDaPermissao(id: number): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.GetPermissoesPorCorretoraInput>> {
            var deferred = this.$q.defer();

            this.service.getProdutoECorretorasDaPermissao({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCorretorasByProduto(input: produtoDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>> {
            var deferred = this.$q.defer();

            this.service.getCorretorasByProduto(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: permissaoDeVendaDePlanoDeSaudeParaCorretoraDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}