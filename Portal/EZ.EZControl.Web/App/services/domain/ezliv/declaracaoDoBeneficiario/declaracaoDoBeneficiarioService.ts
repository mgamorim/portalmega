﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as declaracaoDoBeneficiarioDtos from "../../../../dtos/ezliv/geral/declaracaoDoBeneficiario/declaracaoDoBeneficiarioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IDeclaracaoDoBeneficiarioService extends services.Services.IServiceEntity {
    }

    export class DeclaracaoDoBeneficiarioService implements IDeclaracaoDoBeneficiarioService {
        static $inject = ['$q', 'abp.services.app.ezliv.declaracaoDoBeneficiario'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.GetDeclaracaoDoBeneficiarioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.GetDeclaracaoDoBeneficiarioInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getDeclaracoesDoBeneficiarioPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: declaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.DeclaracaoDoBeneficiarioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}