﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as valorPorFaixaEtariaDtos from "../../../../dtos/ezliv/geral/valorPorFaixaEtaria/valorPorFaixaEtariaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IValorPorFaixaEtariaService extends services.Services.IServiceEntity {
    }

    export class ValorPorFaixaEtariaService implements IValorPorFaixaEtariaService {
        static $inject = ['$q', 'abp.services.app.ezliv.valorPorFaixaEtaria'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetValorPorFaixaEtariaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetValorPorFaixaEtariaInput, requestParams.RequestParam.IRequestParams {
                valor: number;
                contratoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                valor: filtro.valor,
                contratoId: filtro.contratoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaginadoByContratoId(filtro: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetValorPorFaixaEtariaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetValorPorFaixaEtariaInput, requestParams.RequestParam.IRequestParams {
                valor: number;
                contratoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                valor: filtro.valor,
                contratoId: filtro.contratoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginadoByContratoId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: valorPorFaixaEtariaDtos.Dtos.EZLiv.Geral.ValorPorFaixaEtariaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}