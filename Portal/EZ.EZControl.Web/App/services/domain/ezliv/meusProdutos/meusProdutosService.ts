﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as meusProdutosDtos from "../../../../dtos/ezliv/geral/meusProdutos/meusProdutosDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IMeusProdutosService extends services.Services.IServiceEntity {
    }

    export class MeusProdutosService implements IMeusProdutosService {
        static $inject = ['$q', 'abp.services.app.ezliv.meusProdutos'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: meusProdutosDtos.Dtos.EZLiv.Geral.GetMeusProdutosInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements meusProdutosDtos.Dtos.EZLiv.Geral.GetMeusProdutosInput, requestParams.RequestParam.IRequestParams {
                nome: string;;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.get(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            deferred.reject();

            return deferred.promise;
        }

        save(input: meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            deferred.reject();

            return deferred.promise;
        }

        saveAndReturn(input: meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput): ng.IPromise<meusProdutosDtos.Dtos.EZLiv.Geral.MeusProdutosInput> {
            var deferred = this.$q.defer();

            deferred.reject();

            return deferred.promise;
        }
    }
}