﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as itemDeRedeCredenciadaDtos from "../../../../dtos/ezliv/geral/itemDeRedeCredenciada/itemDeRedeCredenciadaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IItemDeRedeCredenciadaService extends services.Services.IServiceEntity {
    }

    export class ItemDeRedeCredenciadaService implements IItemDeRedeCredenciadaService {
        static $inject = ['$q', 'abp.services.app.ezliv.itemDeRedeCredenciada'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.GetItemDeRedeCredenciadaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.GetItemDeRedeCredenciadaInput, requestParams.RequestParam.IRequestParams {
                id: number;
                redeCredenciadaId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                redeCredenciadaId: filtro.redeCredenciadaId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getItemDeRedeCredenciadaPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.ItemDeRedeCredenciadaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}