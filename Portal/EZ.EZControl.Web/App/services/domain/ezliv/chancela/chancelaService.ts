﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as chancelaDtos from "../../../../dtos/ezliv/geral/chancela/chancelaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IChancelaService extends services.Services.IServiceEntity {
    }

    export class ChancelaService implements IChancelaService {
        static $inject = ['$q', 'abp.services.app.ezliv.chancela'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getByIdParaPropostaDeContratacao(id: number): ng.IPromise<chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput> {
          var deferred = this.$q.defer();

          this.service.getByIdParaPropostaDeContratacao({ id })
            .success(result => {
              deferred.resolve(result);
            })
            .error(error => {
              deferred.reject(error);
            });

          return deferred.promise;
        }

        getPaged(filtro: chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaginadoParaPropostaDeContratacao(filtro: chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>> {
          var deferred = this.$q.defer();

          class RequestType implements chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput, requestParams.RequestParam.IRequestParams {
            nome: string;
            skipCount: number;
            maxResultCount: number;
            sorting: string;
          }

          var request: RequestType = {
            nome: filtro.nome,
            skipCount: requestParams.skipCount,
            maxResultCount: requestParams.maxResultCount,
            sorting: requestParams.sorting
          }

          this.service.getPaginadoParaPropostaDeContratacao(request)
            .success(result => {
              deferred.resolve(result);
            })
            .error(error => {
              deferred.reject(error);
            });

          return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: chancelaDtos.Dtos.EZLiv.Geral.ChancelaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getByProfissaoId(filtro: applicationServiceDtos.Dtos.ApplicationService.IdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<chancelaDtos.Dtos.EZLiv.Geral.ChancelaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements chancelaDtos.Dtos.EZLiv.Geral.GetChancelaInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: '',
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getChancelasByProfissaoId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}