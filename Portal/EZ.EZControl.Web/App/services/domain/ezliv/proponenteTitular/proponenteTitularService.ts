﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as proponenteTitularDtos from "../../../../dtos/ezliv/subtipoPessoa/proponenteTitular/proponenteTitularDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
//import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IProponenteTitularService extends services.Services.IServiceEntity {
    }

    export class ProponenteTitularService implements IProponenteTitularService {
        static $inject = ['$q', 'abp.services.app.ezliv.proponenteTitular'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: proponenteTitularDtos.Dtos.EZLiv.Geral.GetProponenteTitularInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<proponenteTitularDtos.Dtos.EZLiv.Geral.ProponenteTitularListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements proponenteTitularDtos.Dtos.EZLiv.Geral.GetProponenteTitularInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: proponenteTitularDtos.Dtos.EZLiv.Geral.GetProponenteTitularInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        //getPagedExceptForId(filtro: beneficiarioDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
        //    var deferred = this.$q.defer();

        //    class RequestType implements corretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora, requestParams.RequestParam.IRequestParams {
        //        nomePessoa: string;
        //        tipoPessoa: TipoPessoaEnum;
        //        skipCount: number;
        //        maxResultCount: number;
        //        sorting: string;
        //    }

        //    var request: RequestType = {
        //        nomePessoa: filtro.nomePessoa,
        //        tipoPessoa: filtro.tipoPessoa,
        //        skipCount: requestParams.skipCount,
        //        maxResultCount: requestParams.maxResultCount,
        //        sorting: requestParams.sorting
        //    }

        //    this.service.getPessoaExceptForCorretora(request)
        //        .success(result => {
        //            deferred.resolve(result);
        //        })
        //        .error(error => {
        //            deferred.reject(error);
        //        });

        //    return deferred.promise;
        //}
    }
}