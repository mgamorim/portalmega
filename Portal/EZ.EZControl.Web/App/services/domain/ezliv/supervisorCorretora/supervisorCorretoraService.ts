﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as supervisorCorretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/supervisorCorretora/supervisorCorretoraDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface ISupervisorCorretoraService extends services.Services.IServiceEntity {
    }

    export class SupervisorCorretoraService implements ISupervisorCorretoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.supervisorCorretora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<supervisorCorretoraDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: supervisorCorretoraDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<supervisorCorretoraDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetSupervisorCorretoraInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                corretoraId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                corretoraId: filtro.corretoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getSupervisoresByCorretoraId(filtro: supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetSupervisorCorretoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<supervisorCorretoraDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetSupervisorCorretoraInput, requestParams.RequestParam.IRequestParams {
                corretoraId: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretoraId: filtro.corretoraId,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getSupervisoresByCorretoraId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: supervisorCorretoraDtos.Dtos.EZLiv.Geral.SupervisorCorretoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<supervisorCorretoraDtos.Dtos.EZLiv.Geral.SupervisorCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorCorretora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaExceptForCorretor(filtro: supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorCorretora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome:filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCorretoraExceptForCorretor(filtro: supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForSupervisorCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorCorretoraDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForSupervisorCorretora, requestParams.RequestParam.IRequestParams {
                corretorId: number;
                nome: string;
                corretoraId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretorId: filtro.corretorId,
                nome: filtro.nome,
                corretoraId: filtro.corretoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoraExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}