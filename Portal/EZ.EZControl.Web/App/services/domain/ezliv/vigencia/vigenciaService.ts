﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as vigenciaDtos from "../../../../dtos/ezliv/geral/vigencia/vigenciaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IVigenciaService extends services.Services.IServiceEntity {
    }

    export class VigenciaService implements IVigenciaService {
        static $inject = ['$q', 'abp.services.app.ezliv.vigencia'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaInput, requestParams.RequestParam.IRequestParams {
                numeroDeBoletos: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                numeroDeBoletos: filtro.numeroDeBoletos,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedByProduto(filtro: vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaByProdutoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements vigenciaDtos.Dtos.EZLiv.Geral.GetVigenciaByProdutoInput, requestParams.RequestParam.IRequestParams {
                numeroDeBoletos: string;
                produtoId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                numeroDeBoletos: filtro.numeroDeBoletos,
                produtoId: filtro.produtoId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginadoByProduto(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        saveAndReturn(input: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput): ng.IPromise<vigenciaDtos.Dtos.EZLiv.Geral.VigenciaInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveAndReturnEntity(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}