﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as operadoraDtos from "../../../../dtos/ezliv/subtipoPessoa/operadora/operadoraDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IOperadoraService extends services.Services.IServiceEntity {
    }

    export class OperadoraService implements IOperadoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.operadora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<operadoraDtos.Dtos.EZLiv.Geral.OperadoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements operadoraDtos.Dtos.EZLiv.Geral.GetOperadoraInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                codigoAns: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                codigoAns: filtro.codigoAns,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getOperadorasPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getAll(): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultOutput<operadoraDtos.Dtos.EZLiv.Geral.OperadoraListDto>> {
            var deferred = this.$q.defer();

            this.service.getAll()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: operadoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForOperadora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements operadoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForOperadora, requestParams.RequestParam.IRequestParams {
                nome: string;
                nomePessoa: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nomePessoa,
                nomePessoa: filtro.nomePessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForOperadora(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: operadoraDtos.Dtos.EZLiv.Geral.OperadoraInput): ng.IPromise<operadoraDtos.Dtos.EZLiv.Geral.OperadoraPessoaIdDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}