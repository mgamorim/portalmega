﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as faixaEtariaDtos from "../../../../dtos/ezliv/geral/faixaEtaria/faixaEtariaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IFaixaEtariaService extends services.Services.IServiceEntity {
    }

    export class FaixaEtariaService implements IFaixaEtariaService {
        static $inject = ['$q', 'abp.services.app.ezliv.faixaEtaria'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaInput, requestParams.RequestParam.IRequestParams {
                descricao: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                descricao: filtro.descricao,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaginadoExceptForIds(filtro: faixaEtariaDtos.Dtos.EZLiv.Geral.GetFaixaEtariaExceptForIdsInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaListDto>> {
            var deferred = this.$q.defer();

            this.service.getPaginadoExceptForIds(filtro)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: faixaEtariaDtos.Dtos.EZLiv.Geral.FaixaEtariaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}