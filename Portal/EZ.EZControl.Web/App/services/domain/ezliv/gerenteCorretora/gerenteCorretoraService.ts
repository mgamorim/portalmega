﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as gerenteCorretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/gerenteCorretora/gerenteCorretoraDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface IGerenteCorretoraService extends services.Services.IServiceEntity {
    }

    export class GerenteCorretoraService implements IGerenteCorretoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.gerenteCorretora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<gerenteCorretoraDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: gerenteCorretoraDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<gerenteCorretoraDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetGerenteCorretoraInput, requestParams.RequestParam.IRequestParams {
                corretoraId: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                corretoraId: filtro.corretoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getGerentesByCorretoraId(filtro: gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetGerenteCorretoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<gerenteCorretoraDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetGerenteCorretoraInput, requestParams.RequestParam.IRequestParams {
                corretoraId: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                corretoraId: filtro.corretoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getGerentesByCorretoraId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: gerenteCorretoraDtos.Dtos.EZLiv.Geral.GerenteCorretoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<gerenteCorretoraDtos.Dtos.EZLiv.Geral.GerenteCorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteCorretora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaExceptForCorretor(filtro: gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteCorretora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome:filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCorretoraExceptForCorretor(filtro: gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForGerenteCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteCorretoraDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForGerenteCorretora, requestParams.RequestParam.IRequestParams {
                corretorId: number;
                nome: string;
                corretoraId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretorId: filtro.corretorId,
                nome: filtro.nome,
                corretoraId: filtro.corretoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoraExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}