﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface IAdministradoraService extends services.Services.IServiceEntity {
    }

    export class AdministradoraService implements IAdministradoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.administradora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: administradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements administradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraInput, requestParams.RequestParam.IRequestParams {
                nomeFantasia: string;
                razaoSocial: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeFantasia: filtro.nomeFantasia,
                razaoSocial: filtro.razaoSocial,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }


            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: administradoraDtos.Dtos.EZLiv.Geral.AdministradoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: administradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements administradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForAdministradora, requestParams.RequestParam.IRequestParams {
                nomePessoa: string;
                tipoPessoa: TipoPessoaEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePessoa: filtro.nomePessoa,
                tipoPessoa: filtro.tipoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForAdministradora(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}