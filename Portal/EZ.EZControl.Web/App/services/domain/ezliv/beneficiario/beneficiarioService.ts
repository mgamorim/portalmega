﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as beneficiarioDtos from "../../../../dtos/ezliv/subtipoPessoa/beneficiario/beneficiarioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
//import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IBeneficiarioService extends services.Services.IServiceEntity {
    }

    export class BeneficiarioService implements IBeneficiarioService {
        static $inject = ['$q', 'abp.services.app.ezliv.beneficiario'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<beneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: beneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<beneficiarioDtos.Dtos.EZLiv.Geral.BeneficiariolistDto>> {
            var deferred = this.$q.defer();

            class RequestType implements beneficiarioDtos.Dtos.EZLiv.Geral.GetBeneficiarioInput, requestParams.RequestParam.IRequestParams {
                nomeDaMae: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeDaMae: filtro.nomeDaMae,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: beneficiarioDtos.Dtos.EZLiv.Geral.BeneficiarioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        //getPagedExceptForId(filtro: beneficiarioDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
        //    var deferred = this.$q.defer();

        //    class RequestType implements corretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora, requestParams.RequestParam.IRequestParams {
        //        nomePessoa: string;
        //        tipoPessoa: TipoPessoaEnum;
        //        skipCount: number;
        //        maxResultCount: number;
        //        sorting: string;
        //    }

        //    var request: RequestType = {
        //        nomePessoa: filtro.nomePessoa,
        //        tipoPessoa: filtro.tipoPessoa,
        //        skipCount: requestParams.skipCount,
        //        maxResultCount: requestParams.maxResultCount,
        //        sorting: requestParams.sorting
        //    }

        //    this.service.getPessoaExceptForCorretora(request)
        //        .success(result => {
        //            deferred.resolve(result);
        //        })
        //        .error(error => {
        //            deferred.reject(error);
        //        });

        //    return deferred.promise;
        //}
    }
}