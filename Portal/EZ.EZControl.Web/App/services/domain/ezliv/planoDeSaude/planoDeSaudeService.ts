﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as planoDeSaudeDtos from "../../../../dtos/ezliv/geral/planoDeSaude/planoDeSaudeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IPlanoDeSaudeService extends services.Services.IServiceEntity {
    }

    export class PlanoDeSaudeService implements IPlanoDeSaudeService {
        static $inject = ['$q', 'abp.services.app.ezliv.planoDeSaude'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<planoDeSaudeDtos.Dtos.EZLiv.Geral.PlanoDeSaudeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: planoDeSaudeDtos.Dtos.EZLiv.Geral.GetPlanoDeSaudeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<planoDeSaudeDtos.Dtos.EZLiv.Geral.PlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements planoDeSaudeDtos.Dtos.EZLiv.Geral.GetPlanoDeSaudeInput, requestParams.RequestParam.IRequestParams {
                numeroDeRegistro: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                numeroDeRegistro: filtro.numeroDeRegistro,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPlanosDeSaudePaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: planoDeSaudeDtos.Dtos.EZLiv.Geral.PlanoDeSaudeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}