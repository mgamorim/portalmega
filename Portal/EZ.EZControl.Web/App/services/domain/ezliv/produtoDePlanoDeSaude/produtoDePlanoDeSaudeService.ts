﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as produtoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/produtoDePlanoDeSaude/produtoDePlanoDeSaudeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as contratoDtos from "../../../../dtos/ezliv/geral/contrato/contratoDtos";
import * as estadoDtos from "../../../../dtos/global/estado/estadoDtos";
import * as cidadeDtos from "../../../../dtos/global/cidade/cidadeDtos";

export module Services {
    export interface IProdutoDePlanoDeSaudeService extends services.Services.IServiceEntity {
    }

    export class ProdutoDePlanoDeSaudeService implements IProdutoDePlanoDeSaudeService {
        static $inject = ['$q', 'abp.services.app.ezliv.produtoDePlanoDeSaude'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedProdutoAtivoComValor(input: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            this.service.getPagedProdutoAtivoComValor(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getValoresByContratoDetail(input: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput): ng.IPromise<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ValoresByProdutoListDto> {
            var deferred = this.$q.defer();

            this.service.getValoresByContratoDetail(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetProdutoDePlanoDeSaudeInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                exceptIds: number[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                exceptIds: filtro.exceptIds,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getProdutosDePlanoDeSaudePaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getProdutoDePlanoDeSaudeForCorretora(): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            this.service.getProdutoDePlanoDeSaudeForCorretora()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getProdutoDePlanoDeSaudeWithValorForCorretora(input: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetValoresByProdutoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            this.service.getProdutoDePlanoDeSaudeWithValorForCorretora(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getProdutoDePlanoDeSaudeWithValorForCorretor(input: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetValoresAndProdutoByCorretorInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            this.service.getProdutoDePlanoDeSaudeWithValorForCorretor(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getProdutoDePlanoDeSaudeExceptForContratoActive(): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            this.service.getProdutoDePlanoDeSaudeExceptForContratoActive()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.ProdutoDePlanoDeSaudeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getContrato(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<contratoDtos.Dtos.EZLiv.Geral.ContratoListDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.getContrato(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEstadoExceptForProduto(filtro: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEstadoExceptForProduto, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<estadoDtos.Dtos.Estado.EstadoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEstadoExceptForProduto, requestParams.RequestParam.IRequestParams {
                produtoDePlanoDeSaudeId: number;
                nome: string;
                estados: estadoDtos.Dtos.Estado.EstadoInput[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                produtoDePlanoDeSaudeId: filtro.produtoDePlanoDeSaudeId,
                nome: filtro.nome,
                estados: filtro.estados,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEstadoExceptForProduto(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEstadosByProdutos(): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<estadoDtos.Dtos.Estado.EstadoListDto>> {
            var deferred = this.$q.defer();

            this.service.getEstadosByProdutos()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCidadeExceptForProduto(filtro: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetCidadeExceptForProduto, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<cidadeDtos.Dtos.Cidade.CidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetCidadeExceptForProduto, requestParams.RequestParam.IRequestParams {
                produtoDePlanoDeSaudeId: number;
                nome: string;
                cidades: cidadeDtos.Dtos.Cidade.CidadeInput[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                produtoDePlanoDeSaudeId: filtro.produtoDePlanoDeSaudeId,
                nome: filtro.nome,
                cidades: filtro.cidades,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCidadeExceptForProduto(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getDetalhesById(input: produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetDetalhesProdutoInput): ng.IPromise<produtoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.DetalhesProdutoDePlanoDeSaudeInput> {
            var deferred = this.$q.defer();

            this.service.getDetalhesById(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}