﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as indiceDeReajustePorFaixaEtariaDtos from "../../../../dtos/ezliv/geral/indiceDeReajustePorFaixaEtaria/indiceDeReajustePorFaixaEtariaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IIndiceDeReajustePorFaixaEtariaService extends services.Services.IServiceEntity {
    }

    export class IndiceDeReajustePorFaixaEtariaService implements IIndiceDeReajustePorFaixaEtariaService {
        static $inject = ['$q', 'abp.services.app.ezliv.indiceDeReajustePorFaixaEtaria'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetIndiceDeReajustePorFaixaEtariaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.GetIndiceDeReajustePorFaixaEtariaInput, requestParams.RequestParam.IRequestParams {
                indiceDeReajuste: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                indiceDeReajuste: filtro.indiceDeReajuste,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: indiceDeReajustePorFaixaEtariaDtos.Dtos.EZLiv.Geral.IndiceDeReajustePorFaixaEtariaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}