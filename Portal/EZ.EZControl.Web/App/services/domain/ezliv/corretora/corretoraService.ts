﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface ICorretoraService extends services.Services.IServiceEntity {
    }

    export class CorretoraService implements ICorretoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.corretora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: corretoraDtos.Dtos.EZLiv.Geral.GetCorretoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements corretoraDtos.Dtos.EZLiv.Geral.GetCorretoraInput, requestParams.RequestParam.IRequestParams {
                nomeFantasia: string;
                nome: string;
                razaoSocial: string;
                ids: number[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeFantasia: filtro.nomeFantasia,
                razaoSocial: filtro.razaoSocial,
                ids: filtro.ids,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: corretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements corretoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretora, requestParams.RequestParam.IRequestParams {
                nomePessoa: string;
                tipoPessoa: TipoPessoaEnum;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePessoa: filtro.nomePessoa,
                tipoPessoa: filtro.tipoPessoa,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretora(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        //getCorretoresCorretora(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>> {
        //    var deferred = this.$q.defer();

        //    this.service.getUsuariosAssociados(input)
        //        .success(result => {
        //            deferred.resolve(result);
        //        })
        //        .error(error => {
        //            deferred.reject(error);
        //        });

        //    return deferred.promise;
        //}

        getPaginadoExceptForIds(filtro: corretoraDtos.Dtos.EZLiv.Geral.GetCorretoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>> {
            var deferred = this.$q.defer();

            this.service.getPaginadoCorretorasExceptForIds(filtro)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}