﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as especialidadePorProdutoDePlanoDeSaudeDtos from "../../../../dtos/ezliv/geral/especialidadePorProdutoDePlanoDeSaude/especialidadePorProdutoDePlanoDeSaudeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEspecialidadePorProdutoDePlanoDeSaudeService extends services.Services.IServiceEntity {
    }

    export class EspecialidadePorProdutoDePlanoDeSaudeService implements IEspecialidadePorProdutoDePlanoDeSaudeService {
        static $inject = ['$q', 'abp.services.app.ezliv.especialidadePorProdutoDePlanoDeSaude'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEspecialidadePorProdutoDePlanoDeSaudeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEspecialidadePorProdutoDePlanoDeSaudeInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                produtoDePlanoDeSaudeId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                produtoDePlanoDeSaudeId: filtro.produtoDePlanoDeSaudeId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaginadoByContratoId(filtro: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEspecialidadePorProdutoDePlanoDeSaudeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.GetEspecialidadePorProdutoDePlanoDeSaudeInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                produtoDePlanoDeSaudeId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                produtoDePlanoDeSaudeId: filtro.produtoDePlanoDeSaudeId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginadoByContratoId(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: especialidadePorProdutoDePlanoDeSaudeDtos.Dtos.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaudeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}