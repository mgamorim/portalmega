﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as itemDeDeclaracaoDoBeneficiarioDtos from "../../../../dtos/ezliv/geral/itemDeDeclaracaoDoBeneficiario/itemDeDeclaracaoDoBeneficiarioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IItemDeDeclaracaoDoBeneficiarioService extends services.Services.IServiceEntity {
    }

    export class ItemDeDeclaracaoDoBeneficiarioService implements IItemDeDeclaracaoDoBeneficiarioService {
        static $inject = ['$q', 'abp.services.app.ezliv.itemDeDeclaracaoDoBeneficiario'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.GetItemDeDeclaracaoDoBeneficiarioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.GetItemDeDeclaracaoDoBeneficiarioInput, requestParams.RequestParam.IRequestParams {
                pergunta: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pergunta: filtro.pergunta,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getItensDeDeclaracaoDoBeneficiarioPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        saveAndReturn(input: itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput): ng.IPromise<itemDeDeclaracaoDoBeneficiarioDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiarioInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveAndReturnEntity(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}