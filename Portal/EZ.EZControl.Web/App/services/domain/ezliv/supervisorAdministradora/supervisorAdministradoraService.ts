﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as supervisorAdministradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/supervisorAdministradora/supervisorAdministradoraDtos";
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface ISupervisorAdministradoraService extends services.Services.IServiceEntity {
    }

    export class SupervisorAdministradoraService implements ISupervisorAdministradoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.supervisorAdministradora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<supervisorAdministradoraDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: supervisorAdministradoraDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<supervisorAdministradoraDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetSupervisorAdministradoraInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: supervisorAdministradoraDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<supervisorAdministradoraDtos.Dtos.EZLiv.Geral.SupervisorAdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorAdministradora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaExceptForCorretor(filtro: supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForSupervisorAdministradora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome:filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getAdministradoraExceptForCorretor(filtro: supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraExceptForSupervisorAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements supervisorAdministradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraExceptForSupervisorAdministradora, requestParams.RequestParam.IRequestParams {
                corretorId: number;
                nome: string;
                administradoraId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretorId: filtro.corretorId,
                nome: filtro.nome,
                administradoraId: filtro.administradoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getAdministradoraExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}