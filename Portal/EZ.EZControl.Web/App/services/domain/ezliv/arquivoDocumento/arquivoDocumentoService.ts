﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as arquivoDocumentoDtos from "../../../../dtos/ezliv/geral/arquivoDocumento/arquivoDocumentoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IArquivoDocumentoService extends services.Services.IServiceEntity {
    }

    export class ArquivoDocumentoService implements IArquivoDocumentoService {
        static $inject = ['$q', 'abp.services.app.ezliv.arquivoDocumento'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<arquivoDocumentoDtos.Dtos.EZLiv.Geral.ArquivoDocumentoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: arquivoDocumentoDtos.Dtos.EZLiv.Geral.GetArquivoDocumentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<arquivoDocumentoDtos.Dtos.EZLiv.Geral.ArquivoDocumentoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements arquivoDocumentoDtos.Dtos.EZLiv.Geral.GetArquivoDocumentoInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: arquivoDocumentoDtos.Dtos.EZLiv.Geral.ArquivoDocumentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getArquivoByName(input: arquivoDocumentoDtos.Dtos.EZLiv.Geral.ArquivoDocumentoInput): ng.IPromise<arquivoDocumentoDtos.Dtos.EZLiv.Geral.ArquivoDocumentoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.getArquivoByName(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        changeArquivoDocumentoExigencia(input: arquivoDocumentoDtos.Dtos.EZLiv.Geral.ArquivoDocumentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.changeArquivoDocumentoExigencia(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}