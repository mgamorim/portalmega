﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as parametroEZLivDtos from "../../../../dtos/ezliv/parametro/parametroEZLivDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IParametroEZLivService extends services.Services.IServiceEntityGetSingle, services.Services.IServiceEntitySave {
    }

    export class ParametroEZLivService implements IParametroEZLivService {
        static $inject = ['$q', 'abp.services.app.ezliv.parametroEZLiv'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        get(): ng.IPromise<parametroEZLivDtos.Dtos.EZLiv.Parametro.ParametroEZLivInput> {

            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: parametroEZLivDtos.Dtos.EZLiv.Parametro.ParametroEZLivInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}