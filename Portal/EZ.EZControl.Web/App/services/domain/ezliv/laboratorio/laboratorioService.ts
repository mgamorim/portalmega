﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as laboratorioDtos from "../../../../dtos/ezliv/geral/laboratorio/laboratorioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as especialidadeDtos from "../../../../dtos/ezmedical/especialidade/especialidadeDtos";

export module Services {
    export interface ILaboratorioService extends services.Services.IServiceEntity {
    }

    export class LaboratorioService implements ILaboratorioService {
        static $inject = ['$q', 'abp.services.app.ezliv.laboratorio'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements laboratorioDtos.Dtos.EZLiv.Geral.GetLaboratorioInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                site: string;
                email: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                site: filtro.site,
                email: filtro.email,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: laboratorioDtos.Dtos.EZLiv.Geral.LaboratorioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEspecialidadeExceptForLaboratorio(filtro: laboratorioDtos.Dtos.EZLiv.Geral.GetEspecialidadeExceptForLaboratorio, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements laboratorioDtos.Dtos.EZLiv.Geral.GetEspecialidadeExceptForLaboratorio, requestParams.RequestParam.IRequestParams {
                laboratorioId: number;
                nome: string;
                codigo: string;
                especialidades: especialidadeDtos.Dtos.EZMedical.Especialidade.EspecialidadeInput[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                laboratorioId: filtro.laboratorioId,
                nome: filtro.nome,
                codigo: filtro.codigo,
                especialidades: filtro.especialidades,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getEspecialidadeExceptForLaboratorio(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        
    }
}