﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as relatorioDtos from "../../../../dtos/global/relatorio/relatorioDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IRelatorioEzlivService extends services.Services.IServiceEntity {
    }

    export class RelatorioEzlivService implements IRelatorioEzlivService {
        static $inject = ['$q', 'abp.services.app.ezliv.relatorioEzLiv'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<relatorioDtos.Dtos.Global.Geral.RelatorioInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: relatorioDtos.Dtos.Global.Geral.GetRelatorioInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<relatorioDtos.Dtos.Global.Geral.RelatorioListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements relatorioDtos.Dtos.Global.Geral.GetRelatorioInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                sistema: any;
                tipoDeRelatorio: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                sistema: filtro.sistema,
                tipoDeRelatorio: filtro.tipoDeRelatorio,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: relatorioDtos.Dtos.Global.Geral.RelatorioInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        saveAndReturn(input: relatorioDtos.Dtos.Global.Geral.RelatorioInput): ng.IPromise<relatorioDtos.Dtos.Global.Geral.RelatorioInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveAndReturnEntity(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        duplicar(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<relatorioDtos.Dtos.Global.Geral.RelatorioInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.duplicar(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getRelatorioExceptForIdInput(filtro: relatorioDtos.Dtos.Global.Geral.GetRelatorioExceptForIdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<relatorioDtos.Dtos.Global.Geral.RelatorioListDto>> {
          var deferred = this.$q.defer();

          class RequestType implements relatorioDtos.Dtos.Global.Geral.GetRelatorioExceptForIdInput, requestParams.RequestParam.IRequestParams {
            relatorioId: number;
            tipoDeRelatorio: number;
            nome: string;
            relatorios: relatorioDtos.Dtos.Global.Geral.RelatorioInput[];
            skipCount: number;
            maxResultCount: number;
            sorting: string;
          }

          var request: RequestType = {
            relatorioId: filtro.relatorioId,
            tipoDeRelatorio: filtro.tipoDeRelatorio,
            nome: filtro.nome,
            relatorios: filtro.relatorios,
            skipCount: requestParams.skipCount,
            maxResultCount: requestParams.maxResultCount,
            sorting: requestParams.sorting
          }

          this.service.getRelatorioExceptForIdInput(request)
            .success(result => {
              deferred.resolve(result);
            })
            .error(error => {
              deferred.reject(error);
            });

          return deferred.promise;
        }
    }
}