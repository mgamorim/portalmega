﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as contratoDtos from "../../../../dtos/ezliv/geral/contrato/contratoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IContratoService extends services.Services.IServiceEntity {
    }

    export class ContratoService implements IContratoService {
        static $inject = ['$q', 'abp.services.app.ezliv.contrato'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<contratoDtos.Dtos.EZLiv.Geral.ContratoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: contratoDtos.Dtos.EZLiv.Geral.GetContratoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<contratoDtos.Dtos.EZLiv.Geral.ContratoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements contratoDtos.Dtos.EZLiv.Geral.GetContratoInput, requestParams.RequestParam.IRequestParams {
                conteudo: string;
                produtoDePlanoDeSaudeId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                conteudo : filtro.conteudo,
                produtoDePlanoDeSaudeId: filtro.produtoDePlanoDeSaudeId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: contratoDtos.Dtos.EZLiv.Geral.ContratoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}