﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as meusProcessosDtos from "../../../../dtos/ezliv/geral/meusProcessos/meusProcessosDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IMeusProcessosService extends services.Services.IServiceEntity {
    }

    export class MeusProcessosService implements IMeusProcessosService {
        static $inject = ['$q', 'abp.services.app.ezliv.meusProcessos'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<meusProcessosDtos.Dtos.EZLiv.Geral.MeusProcessosInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: meusProcessosDtos.Dtos.EZLiv.Geral.GetMeusProcessosInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<meusProcessosDtos.Dtos.EZLiv.Geral.MeusProcessosListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements meusProcessosDtos.Dtos.EZLiv.Geral.GetMeusProcessosInput, requestParams.RequestParam.IRequestParams {
                nome: string;;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.get(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            deferred.reject();

            return deferred.promise;
        }

        save(input: meusProcessosDtos.Dtos.EZLiv.Geral.MeusProcessosInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            deferred.reject();

            return deferred.promise;
        }

        saveAndReturn(input: meusProcessosDtos.Dtos.EZLiv.Geral.MeusProcessosInput): ng.IPromise<meusProcessosDtos.Dtos.EZLiv.Geral.MeusProcessosInput> {
            var deferred = this.$q.defer();

            deferred.reject();

            return deferred.promise;
        }
    }
}