﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as permissaoDeVendaDePlanoDeSaudeParaCorretorDtos from "../../../../dtos/ezliv/geral/permissaoDeVendaDePlanoDeSaudeParaCorretor/permissaoDeVendaDePlanoDeSaudeParaCorretorDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IPermissaoDeVendaDePlanoDeSaudeParaCorretorService extends services.Services.IServiceEntity {
    }

    export class PermissaoDeVendaDePlanoDeSaudeParaCorretorService implements IPermissaoDeVendaDePlanoDeSaudeParaCorretorService {
        static $inject = ['$q', 'abp.services.app.ezliv.permissaoDeVendaDePlanoDeSaudeParaCorretor'];
        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: permissaoDeVendaDePlanoDeSaudeParaCorretorDtos.Dtos.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretorInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}