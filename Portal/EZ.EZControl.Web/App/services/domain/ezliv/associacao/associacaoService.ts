﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as associacaoDtos from "../../../../dtos/ezliv/subtipoPessoa/associacao/associacaoDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IAssociacaoService extends services.Services.IServiceEntity {
    }

    export class AssociacaoService implements IAssociacaoService {
        static $inject = ['$q', 'abp.services.app.ezliv.associacao'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: associacaoDtos.Dtos.EZLiv.Geral.GetAssociacaoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements associacaoDtos.Dtos.EZLiv.Geral.GetAssociacaoInput, requestParams.RequestParam.IRequestParams {
                nomeFantasia: string;
                razaoSocial: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomeFantasia: filtro.nomeFantasia,
                razaoSocial: filtro.razaoSocial,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: associacaoDtos.Dtos.EZLiv.Geral.AssociacaoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: associacaoDtos.Dtos.EZLiv.Geral.GetPessoaExceptForAssociacao, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaJuridicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements associacaoDtos.Dtos.EZLiv.Geral.GetPessoaExceptForAssociacao, requestParams.RequestParam.IRequestParams {
                nomePessoa: string;
                tipoPessoa: TipoPessoaEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nomePessoa: filtro.nomePessoa,
                tipoPessoa: filtro.tipoPessoa,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForAssociacao(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getByProfissaoId(input: applicationServiceDtos.Dtos.ApplicationService.IdInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<associacaoDtos.Dtos.EZLiv.Geral.AssociacaoListDto>> {
            var deferred = this.$q.defer();

            this.service.getPaginado(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}