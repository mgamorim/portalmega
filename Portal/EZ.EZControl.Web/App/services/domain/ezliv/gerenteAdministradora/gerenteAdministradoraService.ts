﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as gerenteAdministradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/gerenteAdministradora/gerenteAdministradoraDtos";
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface IGerenteAdministradoraService extends services.Services.IServiceEntity {
    }

    export class GerenteAdministradoraService implements IGerenteAdministradoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.gerenteAdministradora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetGerenteAdministradoraInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GerenteAdministradoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GerenteAdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteAdministradora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaExceptForCorretor(filtro: gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForGerenteAdministradora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome:filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getAdministradoraExceptForCorretor(filtro: gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraExceptForGerenteAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements gerenteAdministradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraExceptForGerenteAdministradora, requestParams.RequestParam.IRequestParams {
                corretorId: number;
                nome: string;
                administradoraId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretorId: filtro.corretorId,
                nome: filtro.nome,
                administradoraId: filtro.administradoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getAdministradoraExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}