﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as itemDeDeclaracaoDeSaudeDtos from "../../../../dtos/ezliv/geral/itemDeDeclaracaoDeSaude/itemDeDeclaracaoDeSaudeDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IItemDeDeclaracaoDeSaudeService extends services.Services.IServiceEntity {
    }

    export class ItemDeDeclaracaoDeSaudeService implements IItemDeDeclaracaoDeSaudeService {
        static $inject = ['$q', 'abp.services.app.ezliv.itemDeDeclaracaoDeSaude'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.GetItemDeDeclaracaoDeSaudeInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.GetItemDeDeclaracaoDeSaudeInput, requestParams.RequestParam.IRequestParams {
                pergunta: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                pergunta: filtro.pergunta,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getItensDeDeclaracaoDeSaudePaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        saveAndReturn(input: itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput): ng.IPromise<itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveAndReturnEntity(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        get(): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<itemDeDeclaracaoDeSaudeDtos.Dtos.EZLiv.Geral.ItemDeDeclaracaoDeSaudeListDto>> {
            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}