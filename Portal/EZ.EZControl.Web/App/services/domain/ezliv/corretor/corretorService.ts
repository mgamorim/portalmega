﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as corretorDtos from "../../../../dtos/ezliv/subtipoPessoa/corretor/corretorDtos";
import * as corretoraDtos from "../../../../dtos/ezliv/subtipoPessoa/corretora/corretoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
//import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface ICorretorService extends services.Services.IServiceEntity {
    }

    export class CorretorService implements ICorretorService {
        static $inject = ['$q', 'abp.services.app.ezliv.corretor'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<corretorDtos.Dtos.EZLiv.Geral.CorretorInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: corretorDtos.Dtos.EZLiv.Geral.GetCorretorInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements corretorDtos.Dtos.EZLiv.Geral.GetCorretorInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                exceptIds: number[]; 
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                exceptIds: filtro.exceptIds,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            //this.service.getPaginado(request)
            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: corretorDtos.Dtos.EZLiv.Geral.CorretorInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: corretorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretor, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretorDtos.Dtos.EZLiv.Geral.CorretorListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements corretorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretor, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            //this.service.getPessoaExceptForCorretor(request)
            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaExceptForCorretor(filtro: corretorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretor, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements corretorDtos.Dtos.EZLiv.Geral.GetPessoaExceptForCorretor, requestParams.RequestParam.IRequestParams {
                nome: string;
               // nomePessoa: string;
               // tipoPessoa: TipoPessoaEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome:filtro.nome,
                //nomePessoa: filtro.nome,
                //tipoPessoa: filtro.,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getCorretoraExceptForCorretor(filtro: corretorDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForCorretor, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<corretoraDtos.Dtos.EZLiv.Geral.CorretoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements corretorDtos.Dtos.EZLiv.Geral.GetCorretoraExceptForCorretor, requestParams.RequestParam.IRequestParams {
                corretorId: number;
                nome: string;
                corretoras: corretoraDtos.Dtos.EZLiv.Geral.CorretoraInput[];
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretorId: filtro.corretorId,
                nome: filtro.nome,
                corretoras: filtro.corretoras,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoraExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}