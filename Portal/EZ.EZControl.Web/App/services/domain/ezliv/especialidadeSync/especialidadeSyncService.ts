﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as especialidadeSyncDtos from "../../../../dtos/ezliv/sync/especialidadeSync/especialidadeSyncDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IEspecialidadeSyncService extends services.Services.IServiceEntity {
    }

    export class EspecialidadeSyncService implements IEspecialidadeSyncService {
        static $inject = ['$q', 'abp.services.app.ezliv.especialidadeSync'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                estado: string;
                municipio: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                estado: filtro.estado,
                municipio: filtro.municipio,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEspecialidadeSyncByEstado(input: especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncByEstadoDto): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput>> {
            var deferred = this.$q.defer();

            this.service.getEspecialidadeSyncByEstado(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEspecialidadeSyncByEstadoDistinct(input: especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncByEstadoDto): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput>> {
            var deferred = this.$q.defer();

            this.service.getEspecialidadeSyncByEstadoDistinct(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEspecialidadeSyncByMunicipio(input: especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncByMunicipioDto): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput>> {
            var deferred = this.$q.defer();

            this.service.getEspecialidadeSyncByMunicipio(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEspecialidadeSyncByMunicipioDistinct(input: especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncByMunicipioDto): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput>> {
            var deferred = this.$q.defer();

            this.service.getEspecialidadeSyncByMunicipioDistinct(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getEspecialidadeSyncByNomeDistinct(input: especialidadeSyncDtos.Dtos.EZLiv.Sync.GetEspecialidadeSyncByNomeDto): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<especialidadeSyncDtos.Dtos.EZLiv.Sync.EspecialidadeSyncInput>> {
            var deferred = this.$q.defer();

            this.service.getEspecialidadeSyncByNomeDistinct(input)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}