﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as homologadorAdministradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/homologadorAdministradora/homologadorAdministradoraDtos";
import * as administradoraDtos from "../../../../dtos/ezliv/subtipoPessoa/administradora/administradoraDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as pessoaDtos from "../../../../dtos/global/pessoa/pessoaDtos";

export module Services {
    export interface IHomologadorAdministradoraService extends services.Services.IServiceEntity {
    }

    export class HomologadorAdministradoraService implements IHomologadorAdministradoraService {
        static $inject = ['$q', 'abp.services.app.ezliv.homologadorAdministradora'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<homologadorAdministradoraDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: homologadorAdministradoraDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<homologadorAdministradoraDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetHomologadorAdministradoraInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: homologadorAdministradoraDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPagedExceptForId(filtro: homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForHomologadorAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<homologadorAdministradoraDtos.Dtos.EZLiv.Geral.HomologadorAdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForHomologadorAdministradora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getCorretoresPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPessoaExceptForCorretor(filtro: homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForHomologadorAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<pessoaDtos.Dtos.Pessoa.PessoaFisicaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetPessoaExceptForHomologadorAdministradora, requestParams.RequestParam.IRequestParams {
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome:filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPessoaExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getAdministradoraExceptForCorretor(filtro: homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraExceptForHomologadorAdministradora, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<administradoraDtos.Dtos.EZLiv.Geral.AdministradoraListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements homologadorAdministradoraDtos.Dtos.EZLiv.Geral.GetAdministradoraExceptForHomologadorAdministradora, requestParams.RequestParam.IRequestParams {
                corretorId: number;
                nome: string;
                administradoraId: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                corretorId: filtro.corretorId,
                nome: filtro.nome,
                administradoraId: filtro.administradoraId,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getAdministradoraExceptForCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}