﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as propostaDtos from "../../../../dtos/ezliv/geral/propostaDeContratacao/propostaDeContratacaoDtos";
import * as vigenciaDtos from "../../../../dtos/ezliv/geral/vigencia/vigenciaDtos";
import * as beneficiarioDtos from "../../../../dtos/ezliv/subtipoPessoa/beneficiario/beneficiarioDtos";
import * as userDtos from "../../../../dtos/core/user/userDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {
    export interface IPropostaDeContratacaoService extends services.Services.IServiceEntity {
        savePassoPreCadastro(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPreCadastroInput);
        createUserBeneficiario(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPreCadastroInput);
        savePassoSelecionarPlano(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoSelecionarPlanoInput);
        savePassoDadosGerais(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDadosGerais(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDadosProponenteTitular(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDadosResponsavel(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoEndereco(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDadosDependentes(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDadosVigencia(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDeclaracaoDeSaude(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoDadosGerenciais(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput);
        savePassoEnvioDeDocumentos(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoEnvioDeDocumentosInput);
        savePassoAceiteDoContrato(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoAceiteDoContratoInput);
        savePassoHomologacao(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoHomologacaoInput);
        savePassoPagamento(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPagamentoInput);
        runQueue(func: () => void);
    }

    export class PropostaDeContratacaoService implements IPropostaDeContratacaoService {
        static $inject = ['$q', 'abp.services.app.ezliv.propostaDeContratacao'];

        public enqueue: boolean = false;
        private queue: Array<any>;

        constructor(private $q: ng.IQService, private service: any) {
            this.queue = new Array<any>();
        }

        runQueue(func?: () => void): void {
            if (func) {
                this.$q.all(this.queue).then(func);
            } else {
                this.$q.all(this.queue);
            }
        };

        getById(id: number): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        getPaged(filtro: propostaDtos.Dtos.EZLiv.Geral.GetPropostaDeContratacaoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements propostaDtos.Dtos.EZLiv.Geral.GetPropostaDeContratacaoInput, requestParams.RequestParam.IRequestParams {
                numeroDaProposta: string;
                nomeDoBeneficiario: string;
                cpfDoBeneficiario: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                numeroDaProposta: filtro.numeroDaProposta,
                nomeDoBeneficiario: filtro.nomeDoBeneficiario,
                cpfDoBeneficiario: filtro.cpfDoBeneficiario,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }
            
            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        save(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        updateStatusEncerrado(input: applicationServiceDtos.Dtos.ApplicationService.IdInput) {
            var deferred = this.$q.defer();

            var request = input;

            this.service.updateStatusEncerrado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoPreCadastro(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPreCadastroInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoPreCadastro(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        createUserBeneficiario(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPreCadastroInput): ng.IPromise<userDtos.Dtos.User.Core.UserDto> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.createUserBeneficiario(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailUserBeneficiario(input: userDtos.Dtos.User.Core.UserDto) {
            var deferred = this.$q.defer();

            var request = input;

            this.service.sendEmailUserBeneficiario(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoSelecionarPlano(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoSelecionarPlanoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoSelecionarPlano(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDadosGerais(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoDadosGerais(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDadosProponenteTitular(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveDadosProponenteTitular(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDadosResponsavel(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveDadosResponsavel(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoEndereco(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveDadosEndereco(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDadosDependentes(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveDadosDependentes(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDadosVigencia(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveDadosVigencia(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDeclaracaoDeSaude(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.saveDadosDeclaracaoDeSaude(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoDadosGerenciais(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoDadosGerenciais(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoEnvioDeDocumentos(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoEnvioDeDocumentosInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoEnvioDeDocumentos(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoAceiteDoContrato(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoAceiteDoContratoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoAceiteDoContrato(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoHomologacao(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoHomologacaoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoHomologacao(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        savePassoPagamento(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoPagamentoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.savePassoPagamento(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        getDataInicioDeVigencia(input: vigenciaDtos.Dtos.EZLiv.Geral.VigenciaListDto): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoDataDeVigenciaInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.getDataInicioDeVigencia(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        verificaDocumentosEmExigencia(input: applicationServiceDtos.Dtos.ApplicationService.IdInput) {
            var deferred = this.$q.defer();

            var request = input;

            this.service.verificaDocumentosEmExigencia(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });
        }

        getRespostasDeclaracaoDeSaude(input: applicationServiceDtos.Dtos.ApplicationService.IdInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoDadosGeraisInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.getRespostasDeclaracaoDeSaude(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        atualizaStatusDaProposta(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput): ng.IPromise<propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.atualizaStatusDaProposta(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailBoletoGerado(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailBoletoGerado({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailPreCadastro(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailPreCadastro({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailAceite(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailAceite({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailHomologar(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailHomologar({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailHomologacaoConcluida(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailHomologacaoConcluida({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }


        sendEmailConcluirAceite(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailConcluirAceite({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailConcluirAceiteOnlyBeneficiario(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailConcluirAceiteOnlyBeneficiario({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailConcluirAceiteOnlyCorretor(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailConcluirAceiteOnlyCorretor({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailFormularios(input: propostaDtos.Dtos.EZLiv.Geral.PropostaDeContratacaoPassoAceiteDoContratoInput) {
            var deferred = this.$q.defer();
            var request = input;

            this.service.sendEmailFormularios(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailHomologacao(id: number) {
            var deferred = this.$q.defer();

            this.service.sendEmailHomologacao({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }

        sendEmailDepositoCorretor(input: applicationServiceDtos.Dtos.ApplicationService.IdInput) {
            var deferred = this.$q.defer();
            var request = input;

            this.service.sendEmailComprovanteDepositoCorretor(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            if (this.enqueue) this.queue.push(deferred);
            return deferred.promise;
        }
    }
}