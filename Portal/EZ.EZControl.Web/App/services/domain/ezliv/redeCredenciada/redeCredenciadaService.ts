﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as redeCredenciadaDtos from "../../../../dtos/ezliv/geral/redeCredenciada/redeCredenciadaDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";
import * as itemDeRedeCredenciadaDtos from "../../../../dtos/ezliv/geral/itemDeRedeCredenciada/itemDeRedeCredenciadaDtos";
import * as unidadeDeSaudeBaseDtos from "../../../../dtos/ezliv/geral/unidadeSaudeBase/unidadeSaudeBaseDtos";

export module Services {
    export interface IRedeCredenciadaService extends services.Services.IServiceEntity {
    }

    export class RedeCredenciadaService implements IRedeCredenciadaService {
        static $inject = ['$q', 'abp.services.app.ezliv.redeCredenciada'];

        constructor(private $q: ng.IQService, private service: any) {
        }

        getById(id: number): ng.IPromise<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaInput> {
            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: redeCredenciadaDtos.Dtos.EZLiv.Geral.GetRedeCredenciadaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements redeCredenciadaDtos.Dtos.EZLiv.Geral.GetRedeCredenciadaInput, requestParams.RequestParam.IRequestParams {
                id: number;
                nome: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                nome: filtro.nome,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {
            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        get(): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.ListResultDto<redeCredenciadaDtos.Dtos.EZLiv.Geral.RedeCredenciadaListDto>> {
            var deferred = this.$q.defer();

            this.service.get()
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getUnidadeDeSaudeExceptForItemRedeCredenciada(filtro: itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<unidadeDeSaudeBaseDtos.Dtos.EZLiv.Geral.UnidadeSaudeBaseListDto>> {
            var deferred = this.$q.defer();

            class RequestType implements itemDeRedeCredenciadaDtos.Dtos.EZLiv.Geral.GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput, requestParams.RequestParam.IRequestParams {
                id: number;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                id: filtro.id,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getUnidadeDeSaudeExceptForItemRedeCredenciada(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}