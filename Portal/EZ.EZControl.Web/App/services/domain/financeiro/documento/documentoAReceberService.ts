﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as documentoAReceberDtos from "../../../../dtos/financeiro/documento/documentoAReceberDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IDocumentoAReceberService extends services.Services.IServiceEntity {
    }

    export class DocumentoAReceberService implements IDocumentoAReceberService {
        static $inject = ['$q', 'abp.services.app.financeiro.documentoAReceber'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.GetDocumentoAReceberInput, requestParams.RequestParam.IRequestParams {
                numero: string;
                complemento: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                numero: filtro.numero,
                complemento: filtro.complemento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        estornar(input: documentoAReceberDtos.Dtos.Financeiro.DocumentoAReceber.DocumentoAReceberInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.estornar(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}