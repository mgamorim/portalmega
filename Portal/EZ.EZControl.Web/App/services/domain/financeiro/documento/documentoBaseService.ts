﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as documentoBaseDtos from "../../../../dtos/financeiro/documento/documentoBaseDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface IDocumentoBaseService extends services.Services.IServiceEntity {
    }

    export class DocumentoBaseService implements IDocumentoBaseService {
        static $inject = ['$q', 'abp.services.app.financeiro.documentoBase'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements documentoBaseDtos.Dtos.Financeiro.Documento.GetDocumentoBaseInput, requestParams.RequestParam.IRequestParams {
                numero: string;
                complemento: string;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                numero: filtro.numero,
                complemento: filtro.complemento,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getDocumentosPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: documentoBaseDtos.Dtos.Financeiro.Documento.DocumentoBaseInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}