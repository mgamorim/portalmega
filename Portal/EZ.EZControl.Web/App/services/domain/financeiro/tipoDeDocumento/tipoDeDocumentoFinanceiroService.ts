﻿import * as requestParams from "../../../../common/requestParams/requestParams";
import * as tipoDeDocumentoDtos from "../../../../dtos/financeiro/tipoDeDocumento/tipoDeDocumentoFinanceiroDtos";
import * as applicationServiceDtos from "../../../../application/services/dto/applicationServiceDtos";
import * as services from "../../../../common/services/serviceEntityBase";

export module Services {

    export interface ITipoDeDocumentoFinanceiroService extends services.Services.IServiceEntity {
    }

    export class TipoDeDocumentoFinanceiroService implements ITipoDeDocumentoFinanceiroService {
        static $inject = ['$q', 'abp.services.app.financeiro.tipoDeDocumentoFinanceiro'];

        constructor(private $q: ng.IQService, private service: any) {

        }

        getById(id: number): ng.IPromise<tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoInput> {

            var deferred = this.$q.defer();

            this.service.getById({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        getPaged(filtro: tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoInput, requestParams: requestParams.RequestParam.IRequestParams): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.PagedResultOutput<tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoListDto>> {

            var deferred = this.$q.defer();

            class RequestType implements tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.GetTipoDeDocumentoInput, requestParams.RequestParam.IRequestParams {
                nome: string;
                tipo: TipoDeDocumentoFinanceiroEnum;
                skipCount: number;
                maxResultCount: number;
                sorting: string;
            }

            var request: RequestType = {
                nome: filtro.nome,
                tipo: filtro.tipo,
                skipCount: requestParams.skipCount,
                maxResultCount: requestParams.maxResultCount,
                sorting: requestParams.sorting
            }

            this.service.getPaginado(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        delete(id: number): ng.IPromise<any> {

            var deferred = this.$q.defer();

            this.service.delete({ id })
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        save(input: tipoDeDocumentoDtos.Dtos.Financeiro.TipoDeDocumentoFinanceiro.TipoDeDocumentoInput): ng.IPromise<applicationServiceDtos.Dtos.ApplicationService.IdInput> {
            var deferred = this.$q.defer();

            var request = input;

            this.service.save(request)
                .success(result => {
                    deferred.resolve(result);
                })
                .error(error => {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
}