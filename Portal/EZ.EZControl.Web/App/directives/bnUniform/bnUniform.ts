﻿export module Directives {
    export class BnUniform implements ng.IDirective {
        static $inject: Array<string> = [''];
        public compile: (templateElement: JQuery, templateAttributes: angular.IAttributes, transclude: angular.ITranscludeFunction) => void;
        constructor() {
            this.compile = (element, attrs, transcludeFn) => {
                var checkboxs = element[0].querySelectorAll("input[type='checkbox']");
                for (var i = 0; i < checkboxs.length; i++) {
                    checkboxs[i].setAttribute('bn-uniform', '');
                };

                var textareas = element[0].querySelectorAll("textarea");
                for (var i = 0; i < textareas.length; i++) {
                    textareas[i].setAttribute('bn-uniform', '');
                }
            }
        }

        static instance(): ng.IDirective {
            return new BnUniform();
        }

        restrict: string = 'A';
    }
}