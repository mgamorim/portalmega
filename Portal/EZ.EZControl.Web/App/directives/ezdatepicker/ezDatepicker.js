﻿appModule.directive('ezdatepicker', function () {
    "use strict";

    function EzDatepicker($scope) {
        $scope.day = '01';
        $scope.month = '01';
        var year = new Date();
        $scope.year = year.getUTCFullYear();


        var dayLimit = 31;
        var monthLimit = 12;

        function isOnLimit(value, limit) {
            return value < limit ? true : false;
        };

        $scope.increment = function (target, type) {
            if (type == 'day') {
                if(isOnLimit(target, dayLimit)) {
                    target++;
                } else {
                    target = 1;
                }
                $scope.day = target;
                $scope.day = $scope.day < 10 ? '0' + $scope.day : $scope.day;
            }

            if (type == 'month') {
                if(isOnLimit(target, monthLimit)) {
                    target++;
                } else {
                    target = 1;
                }
                $scope.month = target;
                $scope.month = $scope.month < 10 ? '0' + $scope.month : $scope.month;
            }

            if (type == 'year') {
                target++
                $scope.year = target;
            }
        };

        $scope.decrement = function (target, type) {
            if (type == 'day') {
                target--;
                $scope.day = target == 0 ? 1 : target;
                $scope.day = $scope.day < 10 ? '0' + $scope.day : $scope.day;
            }

            if (type == 'month') {
                target--;
                $scope.month = target == 0 ? 1 : target;
                $scope.month = $scope.month < 10 ? '0' + $scope.month : $scope.month;
            }

            if (type == 'year') {
                target--;
                $scope.year = target;
            }
        }
    };

    return {
        restrict: 'AE',
        scope: {
            ngModel: '='
        },
        templateUrl: '/App/directives/ezdatepicker/ezDatepicker.html',
        controller: ['$scope', EzDatepicker],
    }
});