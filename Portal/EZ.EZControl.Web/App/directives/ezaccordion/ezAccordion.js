﻿appModule.directive('ezaccordion', function () {
    "use strict";
    var count = [];

    function EzAccordion($scope) {
        $scope.title = $scope.accordionTitle;
        
    };

    return {
        restrict: 'E',
        scope: {
            accordionTitle: '@',
            accordionGroup: '@'
        },
        replace: true,
        templateUrl: '/App/directives/ezaccordion/ezAccordion.html',
        controller: ['$scope', EzAccordion],
        link: function ($scope, el, attr) {
            if (!count[$scope.accordionGroup]) {
                count[$scope.accordionGroup] = 0;
            }

            count[$scope.accordionGroup] = ++count[$scope.accordionGroup];
            $scope.count = count[$scope.accordionGroup];
        }
    }
});