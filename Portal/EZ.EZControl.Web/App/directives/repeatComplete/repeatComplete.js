﻿appModule.directive("repeatComplete", function() {
    return function(scope, element, attrs) {
        if (scope.$last) {
            setTimeout(function () {
                scope.$eval(attrs.repeatComplete);
            }, 0);
        }
    };
});