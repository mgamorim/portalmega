﻿interface IEzValidationLocalScope extends ng.IScope {
    ezValidationLocal(value: any): boolean;
    vm: EzValidationLocalController;
}

class EzValidationLocalController {
    static $inject = ['$scope', '$element'];

    constructor(private scope: IEzValidationLocalScope, private element: ng.IAugmentedJQuery) {
        scope.vm = this;
    }
}

class EzValidationLocal implements ng.IDirective {
    constructor() {
        var directive: ng.IDirective = {};

        directive.link = (scope: IEzValidationLocalScope, instanceElement: JQuery, instanceAttributes: ng.IAttributes, controller: ng.INgModelController, transclude: ng.ITranscludeFunction) => {
            var valueParser = (value) => {
                var fn = scope.$eval(scope.ezValidationLocal);
                var valid = fn(value);
                controller.$setValidity('invalidValue', valid);
                return valid ? value : undefined;
            }

            controller.$parsers.push(valueParser);
        }

        directive.controller = EzValidationLocalController;
        directive.scope = {
            ezValidationLocal: '&'
        };
        directive.restrict = 'A';
        directive.require = 'ngModel';

        return directive;
    }

    static factory(): ng.IDirectiveFactory {
        const directive = () => new EzValidationLocal();
        return directive;
    }
}