﻿appModule.directive('rodape', function () {
    //var title = '';
    //var headerInfo = '';

    var compile = function (elem, attrs, transcludeFn) {
        //title = attrs['title'];
        //headerInfo = attrs['headerInfo'];
    };

    return {
        restrict: 'E',
        compile: compile,
        scope: false,
        templateUrl: '/App/directives/views/common/rodape/rodape.cshtml',
        controller: ['$scope', function ($scope) {
            var vm = this;
            //vm.title = title;
            //vm.headerInfo = headerInfo;
        }],
        controllerAs: 'vm'
    }
});