

enum TipoDeEnderecoEletronicoEnum {	
		
	Email = 1,	
	Email_NFe = 2,	
	EMail_Faturamento = 3,	
	WebSite = 4,	
	Blog = 5,	
	MSN = 6,	
	Twiter = 7,	
	Facebook = 8,	
	LinkedIn = 9,	
	IMessage = 10,	
	Outros = 11,	
	EmailPrincipal = 12
}
    
interface IEnums {
    tipoDeEnderecoEletronicoEnum : TipoDeEnderecoEletronicoEnum
}

namespace TipoDeEnderecoEletronicoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeEnderecoEletronicoEnum) {
            var obj = TipoDeEnderecoEletronicoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeEnderecoEletronicoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeEnderecoEletronicoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeEnderecoEletronicoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}