

enum CMSFrontEndResultStatusEnum {	
		
	Sucesso = 1,	
	Atencao = 2,	
	Erro = 3
}
    
interface IEnums {
    cmsFrontEndResultStatusEnum : CMSFrontEndResultStatusEnum
}

namespace CMSFrontEndResultStatusEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in CMSFrontEndResultStatusEnum) {
            var obj = CMSFrontEndResultStatusEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'CMSFrontEndResultStatusEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = CMSFrontEndResultStatusEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = CMSFrontEndResultStatusEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}