

enum TipoDeConteudoEnum {	
		
	Pagina = 1,	
	CategoriaComUltimosPosts = 2,	
	Post = 3,	
	PostsPorAno = 4,	
	PostsPorMes = 5,	
	CategoriaComPostsPorAno = 6,	
	CategoriaComPostsPorMes = 7
}
    
interface IEnums {
    tipoDeConteudoEnum : TipoDeConteudoEnum
}

namespace TipoDeConteudoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeConteudoEnum) {
            var obj = TipoDeConteudoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeConteudoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeConteudoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeConteudoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}