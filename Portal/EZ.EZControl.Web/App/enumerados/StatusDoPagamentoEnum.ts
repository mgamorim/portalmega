

enum StatusDoPagamentoEnum {	
		
	Iniciado = 1,	
	AguardandoPagamento = 2,	
	EmAnalise = 3,	
	Pago = 4,	
	Disponivel = 5,	
	EmDisputa = 6,	
	Reembolsado = 7,	
	Cancelado = 8
}
    
interface IEnums {
    statusDoPagamentoEnum : StatusDoPagamentoEnum
}

namespace StatusDoPagamentoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in StatusDoPagamentoEnum) {
            var obj = StatusDoPagamentoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'StatusDoPagamentoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = StatusDoPagamentoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = StatusDoPagamentoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}