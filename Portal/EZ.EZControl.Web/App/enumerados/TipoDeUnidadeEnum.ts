

enum TipoDeUnidadeEnum {	
		
	Laboratorio = 1,	
	Hospital = 2
}
    
interface IEnums {
    tipoDeUnidadeEnum : TipoDeUnidadeEnum
}

namespace TipoDeUnidadeEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeUnidadeEnum) {
            var obj = TipoDeUnidadeEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeUnidadeEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeUnidadeEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeUnidadeEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}