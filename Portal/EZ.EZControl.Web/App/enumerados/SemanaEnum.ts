

enum SemanaEnum {	
		
	Domingo = 1,	
	Segunda = 2,	
	Terca = 3,	
	Quarta = 4,	
	Quinta = 5,	
	Sexta = 6,	
	Sabado = 7
}
    
interface IEnums {
    semanaEnum : SemanaEnum
}

namespace SemanaEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in SemanaEnum) {
            var obj = SemanaEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'SemanaEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = SemanaEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = SemanaEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}