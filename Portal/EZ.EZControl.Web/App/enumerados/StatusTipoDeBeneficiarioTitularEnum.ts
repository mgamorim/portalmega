

enum StatusTipoDeBeneficiarioTitularEnum {	
		
	Ativo = 1,	
	PreCadastradoPeloCorretor = 2,	
	PreCadastrado = 3,	
	Inativo = 4
}
    
interface IEnums {
    statusTipoDeBeneficiarioTitularEnum : StatusTipoDeBeneficiarioTitularEnum
}

namespace StatusTipoDeBeneficiarioTitularEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in StatusTipoDeBeneficiarioTitularEnum) {
            var obj = StatusTipoDeBeneficiarioTitularEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'StatusTipoDeBeneficiarioTitularEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = StatusTipoDeBeneficiarioTitularEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = StatusTipoDeBeneficiarioTitularEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}