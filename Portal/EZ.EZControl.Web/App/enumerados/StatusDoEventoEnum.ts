

enum StatusDoEventoEnum {	
		
	Agendado = 1,	
	Iniciado = 2,	
	Cancelado = 3,	
	Transferido = 4,	
	Finalizado = 5,	
	Indefinido = 6
}
    
interface IEnums {
    statusDoEventoEnum : StatusDoEventoEnum
}

namespace StatusDoEventoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in StatusDoEventoEnum) {
            var obj = StatusDoEventoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'StatusDoEventoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = StatusDoEventoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = StatusDoEventoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}