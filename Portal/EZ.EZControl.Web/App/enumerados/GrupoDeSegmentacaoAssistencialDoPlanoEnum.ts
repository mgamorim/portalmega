

enum GrupoDeSegmentacaoAssistencialDoPlanoEnum {	
		
	NaoInformado = 0,	
	Ambulatorial = 1,	
	Hospitalar = 2,	
	HospitalarEAmbulatorial = 3,	
	Referencia = 4,	
	Odontologico = 5,	
	InformadoIncorretamente = 6
}
    
interface IEnums {
    grupoDeSegmentacaoAssistencialDoPlanoEnum : GrupoDeSegmentacaoAssistencialDoPlanoEnum
}

namespace GrupoDeSegmentacaoAssistencialDoPlanoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in GrupoDeSegmentacaoAssistencialDoPlanoEnum) {
            var obj = GrupoDeSegmentacaoAssistencialDoPlanoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'GrupoDeSegmentacaoAssistencialDoPlanoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = GrupoDeSegmentacaoAssistencialDoPlanoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = GrupoDeSegmentacaoAssistencialDoPlanoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}