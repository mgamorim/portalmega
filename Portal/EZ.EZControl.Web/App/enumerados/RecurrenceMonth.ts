

enum RecurrenceMonth {	
		
	None = 0,	
	January = 1,	
	February = 2,	
	March = 3,	
	April = 4,	
	May = 5,	
	June = 6,	
	July = 7,	
	August = 8,	
	September = 9,	
	October = 10,	
	November = 11,	
	December = 12
}
    
interface IEnums {
    recurrenceMonth : RecurrenceMonth
}

namespace RecurrenceMonth{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in RecurrenceMonth) {
            var obj = RecurrenceMonth;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'RecurrenceMonth' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = RecurrenceMonth.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = RecurrenceMonth.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}