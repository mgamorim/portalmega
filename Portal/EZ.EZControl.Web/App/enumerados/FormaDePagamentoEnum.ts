﻿

enum FormaDePagamentoEnum {	
		
	Deposito = 1,	
	Boleto = 2,	
    CartaoDeCredito = 3,
    DepositoEmConta = 4
}
    
interface IEnums {
    formaDePagamentoEnum : FormaDePagamentoEnum
}

namespace FormaDePagamentoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in FormaDePagamentoEnum) {
            var obj = FormaDePagamentoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'FormaDePagamentoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = FormaDePagamentoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = FormaDePagamentoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}