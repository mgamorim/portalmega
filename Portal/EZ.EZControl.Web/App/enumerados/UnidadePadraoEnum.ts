

enum UnidadePadraoEnum {	
		
	Quilograma = 1,	
	Litro = 2,	
	Metro = 3,	
	Segundo = 4,	
	Ampere = 5,	
	Celsius = 6,	
	Mol = 7,	
	Candela = 8,	
	Bar = 9,	
	Outros = 10
}
    
interface IEnums {
    unidadePadraoEnum : UnidadePadraoEnum
}

namespace UnidadePadraoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in UnidadePadraoEnum) {
            var obj = UnidadePadraoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'UnidadePadraoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = UnidadePadraoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = UnidadePadraoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}