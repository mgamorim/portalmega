﻿export module Enums.Helper {
    export class EnumHelper {
        static getDescription(e: any, id: number): string {
            return e[e[id].toString() + "Description"];
        }
    }
}
