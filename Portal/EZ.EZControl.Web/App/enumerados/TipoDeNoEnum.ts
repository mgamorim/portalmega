

enum TipoDeNoEnum {	
		
	Pagina = 1,	
	Categoria = 2,	
	Post = 3
}
    
interface IEnums {
    tipoDeNoEnum : TipoDeNoEnum
}

namespace TipoDeNoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeNoEnum) {
            var obj = TipoDeNoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeNoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeNoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeNoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}