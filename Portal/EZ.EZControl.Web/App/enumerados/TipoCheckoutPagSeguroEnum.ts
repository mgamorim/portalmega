

enum TipoCheckoutPagSeguroEnum {	
		
	BOLETO = 1,	
	ONLINE_DEBIT = 2,	
    CREDIT_CARD = 3,
    DEPOSIT_ACCOUNT = 4
}
    
interface IEnums {
    tipoCheckoutPagSeguroEnum : TipoCheckoutPagSeguroEnum
}

namespace TipoCheckoutPagSeguroEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoCheckoutPagSeguroEnum) {
            var obj = TipoCheckoutPagSeguroEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoCheckoutPagSeguroEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoCheckoutPagSeguroEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoCheckoutPagSeguroEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}