

enum RecurrenceDay {	
		
	None = 0,	
	Sunday = 1,	
	Monday = 2,	
	Tuesday = 4,	
	Wednesday = 8,	
	Thursday = 16,	
	Friday = 32,	
	Saturday = 64,	
	EveryDay = 127,	
	WeekDays = 62,	
	WeekendDays = 65
}
    
interface IEnums {
    recurrenceDay : RecurrenceDay
}

namespace RecurrenceDay{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in RecurrenceDay) {
            var obj = RecurrenceDay;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'RecurrenceDay' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = RecurrenceDay.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = RecurrenceDay.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}