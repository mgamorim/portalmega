﻿

enum PassoDaPropostaEnum {	
		
	PreCadastro = 10,	
	SelecionarPlano = 20,	
	PreenchimentoDosDados = 30,	
	DadosProponenteTitular = 31,	
	DadosResponsavel = 32,	
	Endereco = 33,	
	DadosDependentes = 34,	
	Documentos = 35,	
	DadosVigencia = 36,	
    DadosDeclaracaoDeSaude = 37,
    DadosGerenciais = 38,	
	EnvioDeDocumentos = 40,	
	AceiteDoContrato = 50,	
	Homologacao = 60,	
	Pagamento = 70
}
    
interface IEnums {
    passoDaPropostaEnum : PassoDaPropostaEnum
}

namespace PassoDaPropostaEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in PassoDaPropostaEnum) {
            var obj = PassoDaPropostaEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'PassoDaPropostaEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = PassoDaPropostaEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = PassoDaPropostaEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}