

enum TipoDeArquivoEnum {	
		
	PDF = 1,	
	TXT = 2,	
	XLS = 3,	
	DOC = 4,	
	JPG = 5,	
	PNG = 6,	
	GIF = 7,	
	PPT = 8,	
	JPEG = 9
}
    
interface IEnums {
    tipoDeArquivoEnum : TipoDeArquivoEnum
}

namespace TipoDeArquivoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeArquivoEnum) {
            var obj = TipoDeArquivoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeArquivoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeArquivoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeArquivoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}