

enum EstadoCivilEnum {	
		
	NaoInformado = 1,	
	Solteiro = 2,	
	Casado = 3,	
	Separado = 4,	
	Divorciado = 5,	
	Viuvo = 6,	
	UniaoEstavel = 7,	
	Outros = 8
}
    
interface IEnums {
    estadoCivilEnum : EstadoCivilEnum
}

namespace EstadoCivilEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in EstadoCivilEnum) {
            var obj = EstadoCivilEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'EstadoCivilEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = EstadoCivilEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = EstadoCivilEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}