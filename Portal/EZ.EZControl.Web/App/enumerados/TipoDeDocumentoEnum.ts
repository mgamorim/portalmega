

enum TipoDeDocumentoEnum {	
		
	Cpf = 1,	
	Cnpj = 2,	
	Rg = 3,	
	Passaporte = 4,	
	CarteiraDeTrabalho = 5,	
	Pis = 6
}
    
interface IEnums {
    tipoDeDocumentoEnum : TipoDeDocumentoEnum
}

namespace TipoDeDocumentoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeDocumentoEnum) {
            var obj = TipoDeDocumentoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeDocumentoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeDocumentoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeDocumentoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}