

enum TipoTelefoneEnum {	
		
	Comercial = 1,	
	Fax = 2,	
	Residencial = 3,	
	Celular = 4,	
	Recados = 5
}
    
interface IEnums {
    tipoTelefoneEnum : TipoTelefoneEnum
}

namespace TipoTelefoneEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoTelefoneEnum) {
            var obj = TipoTelefoneEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoTelefoneEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoTelefoneEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoTelefoneEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}