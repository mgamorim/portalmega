

enum AbrangenciaDoPlanoEnum {	
		
	NaoInformado = 0,	
	Nacional = 1,	
	GrupoDeEstados = 2,	
	Estadual = 3,	
	GrupoDeMunicipios = 4,	
	Municipal = 5,	
	Outra = 6
}
    
interface IEnums {
    abrangenciaDoPlanoEnum : AbrangenciaDoPlanoEnum
}

namespace AbrangenciaDoPlanoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in AbrangenciaDoPlanoEnum) {
            var obj = AbrangenciaDoPlanoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'AbrangenciaDoPlanoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = AbrangenciaDoPlanoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

    export function getEnumValues(valores: number[]) {
        var enums = AbrangenciaDoPlanoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}