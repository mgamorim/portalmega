

enum ModalidadeDeOperadoraEnum {	
		
	Administradora = 21,	
	CooperativaMedica = 22,	
	CooperativaOdontologica = 23,	
	Autogestao = 24,	
	MedicinadeGrupo = 25,	
	OdontologiaDeGrupo = 26,	
	Filantropia = 27,	
	SeguradoraEspecializadaEmSaude = 28,	
	Seguradora = 29,	
	AdministradoraDeBeneficios = 55
}
    
interface IEnums {
    modalidadeDeOperadoraEnum : ModalidadeDeOperadoraEnum
}

namespace ModalidadeDeOperadoraEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in ModalidadeDeOperadoraEnum) {
            var obj = ModalidadeDeOperadoraEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'ModalidadeDeOperadoraEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = ModalidadeDeOperadoraEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = ModalidadeDeOperadoraEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}