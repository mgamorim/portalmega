

enum StatusDaPropostaEnum {	
		
	Concluido = 1,	
	Pendente = 2,	
	PendenteDeDocumento = 3,	
	PendenteDePagamento = 4,	
	EmValidacao = 5,	
	Encerrado = 6
}
    
interface IEnums {
    statusDaPropostaEnum : StatusDaPropostaEnum
}

namespace StatusDaPropostaEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in StatusDaPropostaEnum) {
            var obj = StatusDaPropostaEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'StatusDaPropostaEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = StatusDaPropostaEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = StatusDaPropostaEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}