

enum TipoDePropostaEnum {	
		
	Nova = 1,	
	Migracao = 2
}
    
interface IEnums {
    tipoDePropostaEnum : TipoDePropostaEnum
}

namespace TipoDePropostaEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDePropostaEnum) {
            var obj = TipoDePropostaEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDePropostaEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDePropostaEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDePropostaEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}