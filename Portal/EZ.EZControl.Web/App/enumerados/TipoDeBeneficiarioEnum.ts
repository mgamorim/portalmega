

enum TipoDeBeneficiarioEnum {	
		
	Titular = 1,	
	Dependente = 2
}
    
interface IEnums {
    tipoDeBeneficiarioEnum : TipoDeBeneficiarioEnum
}

namespace TipoDeBeneficiarioEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeBeneficiarioEnum) {
            var obj = TipoDeBeneficiarioEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeBeneficiarioEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeBeneficiarioEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeBeneficiarioEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}