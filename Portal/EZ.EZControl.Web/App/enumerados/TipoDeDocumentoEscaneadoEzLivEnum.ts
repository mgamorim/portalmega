

enum TipoDeDocumentoEscaneadoEzLivEnum {	
		
	Formulario = 10,	
	Proposta = 11,	
	Aditivo = 12,	
	Chancela = 13,	
	ComprovanteDeclaracaoDoBeneficiario = 14,	
	Cpf = 1,	
	Cnpj = 2,	
	Rg = 3,	
	Passaporte = 4,	
	CarteiraDeTrabalho = 5,	
	Pis = 6,	
	ComprovanteDeResidencia = 7,	
	ComprovanteDeProfissao = 8,	
	Outros = 9
}
    
interface IEnums {
    tipoDeDocumentoEscaneadoEzLivEnum : TipoDeDocumentoEscaneadoEzLivEnum
}

namespace TipoDeDocumentoEscaneadoEzLivEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeDocumentoEscaneadoEzLivEnum) {
            var obj = TipoDeDocumentoEscaneadoEzLivEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeDocumentoEscaneadoEzLivEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeDocumentoEscaneadoEzLivEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeDocumentoEscaneadoEzLivEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}