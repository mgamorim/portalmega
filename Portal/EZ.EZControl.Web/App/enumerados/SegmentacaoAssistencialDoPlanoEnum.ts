

enum SegmentacaoAssistencialDoPlanoEnum {	
		
	//NaoInformado = 0,	
	//Ambulatorial = 1,	
	//HospComObstetricia = 2,	
	//HospSemObstetricia = 3,	
	//Odontologico = 4,	
	//Referencia = 5,	
	//HospComObstetriciaAmbulatorial = 6,	
	//HospSemObstetriciaAmbulatorial = 7,	
	//AmbulatorialMaisOdonto = 8,	
	//HospComOuSemObstetricia = 9,	
	//HospComObstetriciaMaisOdonto = 10,	
	//HospSemObstetriciaOdonto = 11,	
	//HospComOuSemObstetriciaMaisAmbulatorial = 12,	
	//HospComObstetriciaMaisAmbulatorialMaisOdonto = 13,	
	//HospSemObstetriciaMaisAmbulatorialMaisOdonto = 14,	
	//HospComOuSemObstetriciaMaisOdonto = 15,	
	//InformadoIncorretamente = 99

    NaoInformado = 0,
    Ambulatorial = 1,
    Dental = 2,
    Hospitalar = 3,
    HospitalarAmbulatorial = 4,
    HospitalarObstetricia = 5,
    HospitalarAmbulatorialObstetricia = 6,
    HospitalarAmbulatorialDental = 7,
    HospitalarObstetriciaDental = 8,
    HospitalarAmbulatorialObstetriciaDental = 9


}
    
interface IEnums {
    segmentacaoAssistencialDoPlanoEnum : SegmentacaoAssistencialDoPlanoEnum
}

namespace SegmentacaoAssistencialDoPlanoEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in SegmentacaoAssistencialDoPlanoEnum) {
            var obj = SegmentacaoAssistencialDoPlanoEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'SegmentacaoAssistencialDoPlanoEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = SegmentacaoAssistencialDoPlanoEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = SegmentacaoAssistencialDoPlanoEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}