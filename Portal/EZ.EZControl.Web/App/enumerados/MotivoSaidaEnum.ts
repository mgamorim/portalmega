

enum MotivoSaidaEnum {	
		
	Venda = 1,	
	Inventario = 2,	
	Defeito = 3,	
	Perda = 4,	
	Outros = 99
}
    
interface IEnums {
    motivoSaidaEnum : MotivoSaidaEnum
}

namespace MotivoSaidaEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in MotivoSaidaEnum) {
            var obj = MotivoSaidaEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'MotivoSaidaEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = MotivoSaidaEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = MotivoSaidaEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}