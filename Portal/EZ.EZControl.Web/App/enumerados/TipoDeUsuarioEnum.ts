﻿

enum TipoDeUsuarioEnum {	
		
	Administradora = 1,	
	Beneficiario = 2,	
	Corretor = 3,	
	Operadora = 4,	
	Associacao = 5,	
	Corretora = 6
}
    
interface IEnums {
    tipoDeUsuarioEnum : TipoDeUsuarioEnum
}

namespace TipoDeUsuarioEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in TipoDeUsuarioEnum) {
            var obj = TipoDeUsuarioEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'TipoDeUsuarioEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = TipoDeUsuarioEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = TipoDeUsuarioEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}