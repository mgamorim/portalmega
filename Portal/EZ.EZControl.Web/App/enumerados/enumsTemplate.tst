﻿${
    // Enable extension methods by adding using Typewriter.Extensions.*
    using Typewriter.Extensions.Types;	

    // Uncomment the constructor to change template settings.
    Template(Settings settings)
    {		
        settings.IncludeProject("EZ.EZControl.Core");        
		settings.OutputExtension = ".ts";
		//settings.OutputFilenameFactory = file => 
		//{
	    //return ToCamelCase(file.Name);
		//};
    }

    // Custom extension methods can be used in the template by adding a $ prefix e.g. $LoudName
    string LoudName(Property property)
    {				
        return property.Name.ToUpperInvariant();
    }

    string CamelCase(Enum c)
    {
		return ToCamelCase(c.ToString());
    }

    static string ToCamelCase(string s)
    {
        if (string.IsNullOrEmpty(s)) return s;
        if (char.IsUpper(s[0]) == false) return s;

        var chars = s.ToCharArray();

        for (var i = 0; i < chars.Length; i++)
        {
            var hasNext = (i + 1 < chars.Length);
            if (i > 0 && hasNext && char.IsUpper(chars[i + 1]) == false)
            {
                break;
            }

            chars[i] = char.ToLowerInvariant(chars[i]);
        }

        return new string(chars);
    }
}

$Enums(e => e.Namespace.ToLower().Contains("ez.ezcontrol"))[enum $Name {	
	$Values[	
	$Name = $Value][,]
}
    
interface IEnums {
    $CamelCase : $Name
}

namespace $Name{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in $Name) {
            var obj = $Name;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + '$Name' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = $Name.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = $Name.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}]