

enum PlanoDeSaudeAnsEnum {	
		
	Safira207 = 1,	
	Rubi207 = 2,	
	Diamante207 = 3,	
	Diamante217 = 4,	
	Rubi210 = 5,	
	Rubi230 = 6,	
	Safira210 = 7,	
	Safira230 = 8,	
	Diamante206 = 9,	
	Diamante216 = 10
}
    
interface IEnums {
    planoDeSaudeAnsEnum : PlanoDeSaudeAnsEnum
}

namespace PlanoDeSaudeAnsEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in PlanoDeSaudeAnsEnum) {
            var obj = PlanoDeSaudeAnsEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'PlanoDeSaudeAnsEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = PlanoDeSaudeAnsEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = PlanoDeSaudeAnsEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}