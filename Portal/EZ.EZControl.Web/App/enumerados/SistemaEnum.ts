

enum SistemaEnum {	
		
	Global = 1,	
	CMS = 2,	
	Agenda = 3,	
	EZMedical = 4,	
	Estoque = 5,	
	Vendas = 6,	
	Servicos = 7,	
	Financeiro = 8,	
	EZLiv = 9,	
	EZPag = 10,	
	CRM = 11
}
    
interface IEnums {
    sistemaEnum : SistemaEnum
}

namespace SistemaEnum{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in SistemaEnum) {
            var obj = SistemaEnum;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'SistemaEnum' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = SistemaEnum.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = SistemaEnum.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}