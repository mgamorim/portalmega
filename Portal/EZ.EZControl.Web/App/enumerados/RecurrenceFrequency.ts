

enum RecurrenceFrequency {	
		
	None = 0,	
	Hourly = 1,	
	Daily = 2,	
	Weekly = 3,	
	Monthly = 4,	
	Yearly = 5
}
    
interface IEnums {
    recurrenceFrequency : RecurrenceFrequency
}

namespace RecurrenceFrequency{
    export function valores() {
        const values = new Array<EnumValue>();
        for (let value in RecurrenceFrequency) {
            var obj = RecurrenceFrequency;
            if (typeof obj[value] === 'number'){
                var propriedade = 'Enum_' + 'RecurrenceFrequency' + '_' + value;
                var valorLocalizado = app.localize(propriedade);

                var enumValue = new EnumValue();
                enumValue.valor = (obj[value] as any);
                enumValue.nome = value;
                enumValue.descricao = valorLocalizado;

                values.push(enumValue);
            }
            else {
                continue;
            }
        }

        return values;
    }

    export function getItemPorValor(valor: number) {
        var valores = RecurrenceFrequency.valores();
        for (let i = 0; i < valores.length; i++) {
            if (valores[i].valor == valor) {
                return valores[i];
            }
        }
    }

      export function getEnumValues(valores: number[]) {
        var enums = RecurrenceFrequency.valores();
        const values = new Array<EnumValue>();

        valores.forEach(id => {
            for (let i = 0; i < enums.length; i++) {
                var e = enums[i];
                if (e && e.valor == id) {
                    values.push(e);
                }
            }
        });

        return values;
    }
}