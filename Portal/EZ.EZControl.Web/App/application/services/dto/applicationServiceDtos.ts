﻿export module Dtos.ApplicationService {

    export interface IEzEntity  {
        id: number;
        externalId: number;
        dateOfEditionIntegration: Date;
    }

    export interface IEzEntityRecursive {
        nome: string;
        paiId: number;
    }

    export class EzEntity implements IEzEntity {
        id: number;
        externalId: number;
        dateOfEditionIntegration: Date;
    }

    export class EzEntityRecursive extends EzEntity implements IEzEntityRecursive {
        nome: string;
        paiId: number;
    }

    export interface IListResult<T> {
        items: T[];
    }

    export interface IHasTotalCount {
        totalCount: number;
    }

    export interface IPagedResult<T> extends IListResult<T>, IHasTotalCount {
    }

    export class IdInput {
        id: number;
    }

    export class ListResultDto<T> implements IListResult<T> {
        constructor() {
            this.items = [];
        }
        items: T[];
    }

    export class ListResultOutput<T> extends ListResultDto<T>  {
    }

    export class PagedResultDto<T> implements ListResultDto<T>, IPagedResult<T> {
        constructor() {
            this.items = [];
            this.totalCount = 0;
        }
        items: T[];
        totalCount: number;
    }

    export class PagedResultOutput<T> extends PagedResultDto<T> {
        constructor() {
            super();
            this.items = [];
            this.totalCount = 0;
        }
    }
}