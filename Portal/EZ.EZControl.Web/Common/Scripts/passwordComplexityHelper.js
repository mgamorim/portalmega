﻿(function () {
    app.PasswordComplexityHelper = function () {

        function reviver(key, val) {
            if (key && key.charAt(0) !== key.charAt(0).toLowerCase())
                this[key.charAt(0).toLowerCase() + key.slice(1)] = val;
            else
                return val;
        };

        var _buildPasswordComplexityErrorMessage = function (setting) {
            var message = "<ul>";

            if (setting.minLength) {
                message += "<li>" + abp.utils.formatString(app.localize("PasswordComplexity_MinLength_Hint"), setting.minLength) + "</li>";
            }

            if (setting.maxLength) {
                message += "<li>" + abp.utils.formatString(app.localize("PasswordComplexity_MaxLength_Hint"), setting.maxLength) + "</li>";
            }

            if (setting.useUpperCaseLetters) {
                message += "<li>" + app.localize("PasswordComplexity_UseUpperCaseLetters_Hint") + "</li>";
            }

            if (setting.useLowerCaseLetters) {
                message += "<li>" + app.localize("PasswordComplexity_UseLowerCaseLetters_Hint") + "</li>";
            }

            if (setting.useNumbers) {
                message += "<li>" + app.localize("PasswordComplexity_UseNumbers_Hint") + "</li>";
            }

            if (setting.usePunctuations) {
                message += "<li>" + app.localize("PasswordComplexity_UsePunctuations_Hint") + "</li>";
            }

            return message + "</ul>";
        }

        var _setPasswordComplexityRules = function ($element, setting) {
            
            setting = JSON.parse(JSON.stringify(setting), reviver);

            if (setting) {
                var message = _buildPasswordComplexityErrorMessage(setting);
                
                jQuery.validator.addMethod("passwordComplexity", function (value, element) {
                    let valid = true;
                    if (value.length < setting.minLength) {
                        valid = false;
                    }

                    if (value.length > setting.maxLength) {
                        valid = false;
                    }

                    if (setting.useUpperCaseLetters && !/[A-Z]/.test(value)) {
                        valid = false;
                    }

                    if (setting.useLowerCaseLetters && !/[a-z]/.test(value)) {
                        valid = false;
                    }

                    if (setting.useNumbers && !/[0-9]/.test(value)) {
                        valid = false;
                    }                    
                        
                    var letraMaiscula = 0;
                    var numero = 0;
                    var caracterEspecial = 0;
                    var caracteresEspeciais = "/([~`.!@#$%\^&*+=\-\[\]\\';,/{}|\":<>\?])"; // adicione o que quiser pra validar como caracter especial

                    for (var i = 0; i <= value.length; i++) {
                        var valorAscii = value.charCodeAt(i);

                        // de A até Z
                        if (valorAscii >= 65 && valorAscii <= 90)
                            letraMaiscula++;

                        // de 0 até 9
                        if (valorAscii >= 48 && valorAscii <= 57)
                            numero++;

                        // indexOf retorna -1 quando NÃO encontra
                        if (caracteresEspeciais.indexOf(value[i]) != -1)
                            caracterEspecial++;
                    }

                    if (setting.useUpperCaseLetters && !((letraMaiscula >= 1) && (numero >= 1) && (caracterEspecial >= 1))) {
                        valid = false;
                    }                    

                    if (setting.usePunctuations && !/[!@#\$%\^\&*'"\/{}\[\]?,;|)\(+=._-]+/.test(value)) {
                        valid = false;
                    }

                    if (!valid) {
                        return false;
                    }

                    return true;
                }, message);

                $element.rules("add", "passwordComplexity");
            }
        };

        return {
            setPasswordComplexityRules: _setPasswordComplexityRules
        };
    };
})();