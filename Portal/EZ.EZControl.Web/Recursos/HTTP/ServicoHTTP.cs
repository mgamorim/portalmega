﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.Web.Recursos.HTTP
{
    public class ServicoHTTP
    {

        private HttpClient httpClient;

        public ServicoHTTP()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                (sender, certificate, chain, errors) => { return true; };
        }

        public Task<string> Get(string url)
        {
            using (httpClient = new HttpClient())
            {
                Uri uri = new Uri(url);

                httpClient.Timeout = new TimeSpan(0, 5, 0); // 5 minutos
                httpClient.BaseAddress = uri;

                Task<HttpResponseMessage> task = httpClient.GetAsync(uri);

                //var te = httpClient.DefaultRequestHeaders.GetValues("X-CallCenterProtocol");


                task.Wait();

                if (task.Result.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(task.Result.ReasonPhrase);
                }

                return task.Result.Content.ReadAsStringAsync();
            }
        }

        public string GetHeader(string url)
        {
            using (httpClient = new HttpClient())
            {
                var retorno = string.Empty;

                Uri uri = new Uri(url);

                httpClient.Timeout = new TimeSpan(0, 5, 0); // 5 minutos
                httpClient.BaseAddress = uri;

                Task<HttpResponseMessage> task = httpClient.GetAsync(uri);

                task.Wait();

                if (task.Result.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(task.Result.ReasonPhrase);
                }

                if (task.Result.Headers.Contains("X-CallCenterProtocol"))
                {
                    var Protocol = task.Result.Headers.GetValues("X-CallCenterProtocol");
                    retorno = task.Result.Content.ReadAsStringAsync().Result + "\n" + ((string[])Protocol)[0];
                }
                else
                {
                    retorno = task.Result.Content.ReadAsStringAsync().Result;
                }

                return retorno;
            }
        }

        public string GetQueryString(string url)
        {
            using (httpClient = new HttpClient())
            {
                var retorno = string.Empty;

                Uri uri = new Uri(url);

                httpClient.Timeout = new TimeSpan(0, 5, 0);
                httpClient.BaseAddress = uri;

                Task<HttpResponseMessage> task = httpClient.GetAsync(uri);

                task.Wait();

                retorno = task.Result.RequestMessage.RequestUri.AbsoluteUri;

                return retorno;
            }
        }

        public string Post(string url, string conteudo = "")
        {
            string retorno = string.Empty;

            Uri uri = new Uri(url);

            HttpClient httpClient = new HttpClient { BaseAddress = uri, Timeout = new TimeSpan(0, 0, 1500) };


            HttpContent httpContent = new StringContent(conteudo);

            Task<HttpResponseMessage> task = httpClient.PostAsync(uri, httpContent);

            task.Wait();

            HttpResponseMessage httpResponseMessage = task.Result;

            Task<string> taskContent = httpResponseMessage.Content.ReadAsStringAsync();

            taskContent.Wait();

            int httpRetorno = (int)httpResponseMessage.StatusCode;

            retorno = httpRetorno.ToString();

            return retorno;

        }


        public string PostHeader(string url, string conteudo = "")
        {
            try
            {

                string retorno = string.Empty;

                Uri uri = new Uri(url);

                HttpClient httpClient = new HttpClient { BaseAddress = uri, Timeout = new TimeSpan(0, 0, 1500) };


                HttpContent httpContent = new StringContent(conteudo);

                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                // httpClient.DefaultRequestHeaders.Add("Authorization", "Basic " + Enconding());
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

                Task<HttpResponseMessage> task = httpClient.PostAsync(uri, httpContent);

                task.Wait();

                HttpResponseMessage httpResponseMessage = task.Result;

                Task<string> taskContent = httpResponseMessage.Content.ReadAsStringAsync();

                taskContent.Wait();

                int httpRetorno = (int)httpResponseMessage.StatusCode;

                retorno = task.Result.Content.ReadAsStringAsync().Result;


                //retorno = httpRetorno.ToString();

                return retorno;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public string Enconding()
        {

            string plainText = "megavita.smsonline:RCrWLv3IGU";
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            string encodedText = Convert.ToBase64String(plainTextBytes);

            return encodedText;
        }




    }
}