﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Services.EZLiv;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EZ.EZControl.Web.Controllers
{
    public class FileDownloadController : EZControlControllerBase
    {
        private readonly PropostaDeContratacaoService _propostaDeContratacaoService;
        private readonly ProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;
        private readonly IWebUrlService _webUrlService;
        private readonly IRepository<Tenant> _tenantRepository;
        public FileDownloadController(PropostaDeContratacaoService propostaDeContratacaoService,
                                      ProdutoDePlanoDeSaudeService produtoDePlanoDeSaudeService,
                                      IRepository<Tenant> tenantRepository,
                                      IWebUrlService webUrlService)
        {
            _propostaDeContratacaoService = propostaDeContratacaoService;
            _produtoDePlanoDeSaudeService = produtoDePlanoDeSaudeService;
            _webUrlService = webUrlService;
            _tenantRepository = tenantRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}