﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Notifications;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Mvc.Controllers;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Geral.Perfil;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Notifications;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Web.Models.EZLiv;
using EZ.EZControl.Web.Models.SMS;
using EZ.EZControl.Web.Recursos.HTTP;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EZ.EZControl.Web.Controllers
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class EZLivController : AbpController
    {        
        private readonly IPerfilAppService _perfilAppService;
        private readonly ICorretoraAppService _corretoraAppService;
        private readonly ICorretoraService _corretoraService;
        private readonly ICorretorService _corretorService;
        private readonly IWebUrlService _webUrlService;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IAppNotifier _appNotifier;
        private readonly IBeneficiarioBaseService _beneficiarioService;

        private readonly UserManager _userManager;

        public EZLivController(IPerfilAppService perfilAppService,
            ICorretoraAppService corretoraAppService,
            ICorretoraService corretoraService,
            ICorretorService corretorService,
            IWebUrlService webUrlService,
            IRepository<Tenant> tenantRepository,
            IAppNotifier appNotifier,
            IBeneficiarioBaseService beneficiarioService,
            UserManager userManager)
        {            
            _perfilAppService = perfilAppService;
            _corretoraAppService = corretoraAppService;
            _corretoraService = corretoraService;
            _corretorService = corretorService;
            _webUrlService = webUrlService;
            _tenantRepository = tenantRepository;
            _appNotifier = appNotifier;
            _beneficiarioService = beneficiarioService;
            _userManager = userManager;
        }

        [HttpPost]
        [UnitOfWork]
        public async Task<JsonResult> Registro(RegistroEzlivViewModel model, string returnUrl = "", string returnUrlHash = "")
        {
            try
            {
                if (model.Cpf.Where(c => char.IsLetter(c)).Count() > 0)
                {
                    throw new Exception("CPF não está no formato correto!");
                }


                var idPessoaFisica = await _perfilAppService.Save(new PerfilInput()
                {
                    Cpf = model.Cpf,
                    EmailPrincipal = new EnderecoEletronicoInput()
                    {
                        Endereco = model.Email
                    },
                    Celular = model.Celular,
                    Nome = model.Nome,
                    TipoDeUsuario = model.TipoDeUsuario
                });

                if (model.TipoDeUsuario == TipoDeUsuarioEnum.Beneficiario)
                {

                    var beneficiario = await _beneficiarioService.CreateAndReturnEntity(new BeneficiarioBase()
                    {
                        PessoaFisicaId = idPessoaFisica.Id,
                        TenantId = 1
                    });

                    await UnitOfWorkManager.Current.SaveChangesAsync();

                    using (UnitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                    {
                        var beneficiarioUser = await _userManager.GetByPessoaId(beneficiario.PessoaFisicaId);



                        /*
                            User beneficiarioUser = new User();

                        beneficiarioUser.TenantId = beneficiario.TenantId;
                        beneficiarioUser.Id = idPessoaFisica.Id;
                        beneficiarioUser.EmailAddress = model.Email;
                        beneficiarioUser.UserName = model.Email;
                        beneficiarioUser.Name = model.Nome;
                        beneficiarioUser.Password = "123qwe";
                        beneficiarioUser.PasswordResetCode = "123qwe";

                        */

                        var corretora = await _corretoraService.GetCorretoraBySlug(model.slugCorretora);

                        _beneficiarioService.SendEmail(beneficiarioUser, model.slugCorretora, corretora);

                    }


                }


                if (model.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
                {
                    var corretor = await _corretorService
                        .CreateAndReturnEntity(new Corretor()
                        {
                            PessoaId = idPessoaFisica.Id,
                            TenantId = 1
                        });

                    await UnitOfWorkManager.Current.SaveChangesAsync();

                    using (UnitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                    {
                        var usuarioCorretor = await _userManager.GetByPessoaId(corretor.PessoaId);

                        if (!string.IsNullOrWhiteSpace(model.slugCorretora))
                        {
                            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
                            {
                                var corretoraEntity = await _corretoraService.GetCorretoraBySlug(model.slugCorretora);
                                

                                if (corretoraEntity != null)
                                {
                                    var corretoraInfo = _corretoraService.GetCorretoraNotificationInfo(corretoraEntity);
                                    //var usuarioCorretoraId = corretoraEntity.Gerentes.FirstOrDefault().PessoaId;
                                    var usuarioCorretora = await _userManager.GetByUserName(corretoraInfo.NomeFantasia.Replace(" ","").ToLower());

                                    // Envio email de notificação para o corretor
                                    _corretorService.SendEmailUsuarioCriado(usuarioCorretor, corretor.NomePessoa, corretoraInfo, model.slugCorretora);

                                    if (corretoraEntity?.PessoaJuridica?.EmailPrincipal != null)
                                    {
                                        // Envio email de notificação para o corretora
                                        _corretorService.SendEmailNovoCorretorParaCorretora(corretoraEntity.PessoaJuridica.EmailPrincipal.Endereco, corretor.NomePessoa, corretor.Id, corretoraInfo);
                                    }

                                    var link = _webUrlService.GetSiteRootAddress() + "#/tenant/perfil";
                                               
                                    var mensagem = string.Format("{0} <a href='{1}'> Link.</a>", "É necessário cadastrar os seus dados Bancários.", link);

                                    // Envio de Notificação corretor
                                    await _appNotifier.SendMessageAsync(usuarioCorretor.ToUserIdentifier(), mensagem);

                                    // Envio de Notificação corretora
                                    await _appNotifier.SendMessageAsync(usuarioCorretora.ToUserIdentifier(), String.Format("Novo corretor cadastrado: "+corretor.NomePessoa, corretor.NomePessoa));



                                    // ENVIAR SMS
                                    
                                    //ServicoHTTP servicohttp = new ServicoHTTP();
                                    //SMS SMS = new SMS();

                                    //if (corretor != null && corretoraEntity.Corretor.PessoaFisica.Celular != "")
                                    //{
                                    //    var conteudo = JsonConvert.SerializeObject(new {
                                    //                                                        Mensagem = "Novo Corretor Cadastrado: " + corretor.NomePessoa,
                                    //                                                        Titulo = "Portal MEGA",
                                    //                                                        Numero = "5521998016042",
                                    //                                                        senha = "010203"
                                    //                                                });

                                    //    var result = servicohttp.PostHeader("https://appparceiros.azurewebsites.net/API/SMS/EnviarSMS", conteudo);
                                    //}
                                    
                                }
                            }
                        }
                        else
                        {
                            // Envio email de notificação para o corretor
                            _corretorService.SendEmailUsuarioCriado(usuarioCorretor, corretor.NomePessoa, CorretoraNotificationInfo.CriarCorretoraDefault(), "evida");

                            // Envio de Notificação corretor
                            await _appNotifier.SendMessageAsync(usuarioCorretor.ToUserIdentifier(), String.Format("É necessário cadastrar os seus dados Bancários.", corretor.NomePessoa));

                            var corretora_Entity = await _corretoraService.GetCorretoraBySlug("evida");

                            if (corretora_Entity != null)
                            {
                                var usuarioCorretoraId = corretora_Entity.Gerentes.FirstOrDefault().PessoaId;
                                var usuarioCorretora = await _userManager.GetByPessoaId(usuarioCorretoraId);
                                // Envio de Notificação corretora
                                await _appNotifier.SendMessageAsync(usuarioCorretora.ToUserIdentifier(), String.Format("Novo corretor cadastrado: " + corretor.NomePessoa, corretor.NomePessoa));
                            }
                        }
                    }
                }

                return Json(idPessoaFisica);
            }
            catch (Exception ex)
            {
                return Json(new JsonException(ex.Message));
            }
        }
    }
}