﻿using System.Web.Mvc;

namespace EZ.EZControl.Web.Controllers
{
    public class HomeController : EZControlControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}