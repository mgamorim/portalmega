﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using EZ.EZControl.Services.EZLiv;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Web.Mvc;
using EZ.EZControl.Web.Models.Relatorio;
using EZ.EZControl.Domain.Core.Enums;
using Abp.Authorization;
using EZ.EZControl.Authorization;
using Abp.UI;
using Abp.Web.Mvc.Controllers;
using System;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Abp.AutoMapper;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Web.Controllers
{
    public class RelatorioController : AbpController
    {
        private readonly IPropostaDeContratacaoAppService _propostaAppService;
        private readonly IRelatorioService _relatorioService;

        public RelatorioController(
            PropostaDeContratacaoAppService propostaAppService,
            IRelatorioService relatorioService)
        {
            _propostaAppService = propostaAppService;
            _relatorioService = relatorioService;
        }

        public async Task<ActionResult> Visualizador(SistemaEnum sistema, int tipoDeRelatorio, int idDado, int? relatorioId = null)
        {
            #region Permissões

            if (AbpSession.UserId == null || AbpSession.TenantId == null)
            {
                return Redirect(Url.Action("Login", "Account", new { ReturnUrl = Request.Url.ToString() }));
            }

            if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio)
                    && !PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_AcessoLeitura))
            {
                throw new UserFriendlyException(L("Relatorio.SemPermissaoParaVisualizarError"));
            }

            #endregion

            if (!Enum.IsDefined(typeof(SistemaEnum), sistema)
                    || tipoDeRelatorio <= 0
                    || idDado <= 0)
            {
                throw new UserFriendlyException(L("Relatorio.SemDadosDeEntradaParaCarregarRelatorioError"));
            }

            object dadosFormulario = null;

            #region Definições para cada modulo
            if (sistema == SistemaEnum.EZLiv)
            {
                TipoDeRelatorioEzlivEnum tipoDeRelatorioEzlivEnum = (TipoDeRelatorioEzlivEnum)Enum.Parse(typeof(TipoDeRelatorioEzlivEnum), tipoDeRelatorio.ToString());

                if (!Enum.IsDefined(typeof(TipoDeRelatorioEzlivEnum), tipoDeRelatorioEzlivEnum))
                {
                    throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarOTipoDeRelatorioError"));
                }

                if (tipoDeRelatorioEzlivEnum == TipoDeRelatorioEzlivEnum.PropostaDeContratacao)
                {
                    var dados = await _propostaAppService.MontaPropostaDeContratacaoDoRelatorioV1(new IdInput(idDado));

                    if (dados == null)
                    {
                        throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarOsDadosDoRelatorioError"));
                    }

                    if (dados.Produto.RelatorioProspostaDeContratacaoId == 0)
                    {
                        throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarORelatorioError"));
                    }

                    relatorioId = dados.Produto.RelatorioProspostaDeContratacaoId;
                    dadosFormulario = dados;
                }

                if (tipoDeRelatorioEzlivEnum == TipoDeRelatorioEzlivEnum.FichaDeAssociacao)
                {
                    var dados = await _propostaAppService.MontaFichaDoRelatorioV1(new IdInput(idDado));

                    if (dados == null)
                    {
                        throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarOsDadosDoRelatorioError"));
                    }

                    if (dados.Produto.RelatorioFichaDeEntidadeId == 0)
                    {
                        throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarORelatorioError"));
                    }

                    relatorioId = dados.Produto.RelatorioFichaDeEntidadeId;
                    dadosFormulario = dados;
                }

                if (tipoDeRelatorioEzlivEnum == TipoDeRelatorioEzlivEnum.Aditivo)
                {
                    var dados = await _propostaAppService.MontaFichaDoRelatorioV1(new IdInput(idDado));

                    if (dados == null)
                    {
                        throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarOsDadosDoRelatorioError"));
                    }

                    if (relatorioId == null)
                    {
                        throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarORelatorioError"));
                    }

                    dadosFormulario = dados;
                }
            }
            #endregion

            if (dadosFormulario == null || relatorioId == null)
            {
                throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarORelatorioError"));
            }

            return View(new RelatorioViewModel()
            {
                DadosDoRelatorio = dadosFormulario,
                RelatorioId = relatorioId.Value
            });
        }

        public async Task<ActionResult> Editor(SistemaEnum sistema, int tipoDeRelatorio, int relatorioId)
        {
            #region Permissões

            if (AbpSession.UserId == null || AbpSession.TenantId == null)
            {
                return Redirect(Url.Action("Login", "Account", new { ReturnUrl = Request.Url.ToString() }));
            }

            if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio)
                    && !PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_AcessoLeitura))
            {
                throw new UserFriendlyException(L("Relatorio.SemPermissaoParaVisualizarError"));
            }

            #endregion

            if (!Enum.IsDefined(typeof(SistemaEnum), sistema)
                    || tipoDeRelatorio <= 0
                    || relatorioId <= 0)
            {
                throw new UserFriendlyException(L("Relatorio.SemDadosDeEntradaParaCarregarRelatorioError"));
            }

            object dadosFormulario = null;

            #region Definições para cada modulo
            if (sistema == SistemaEnum.EZLiv)
            {
                TipoDeRelatorioEzlivEnum tipoDeRelatorioEzlivEnum = (TipoDeRelatorioEzlivEnum)Enum.Parse(typeof(TipoDeRelatorioEzlivEnum), tipoDeRelatorio.ToString());

                if (!Enum.IsDefined(typeof(TipoDeRelatorioEzlivEnum), tipoDeRelatorioEzlivEnum))
                {
                    throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarOTipoDeRelatorioError"));
                }

                if (tipoDeRelatorioEzlivEnum == TipoDeRelatorioEzlivEnum.PropostaDeContratacao)
                {
                    // Para o editor não precisa de dados
                    var dados = new PropostaDeContratacaoDoRelatorioV1Input();
                    CriarInstanciasDoDtoSchemaParaEditor(dados);
                    dadosFormulario = dados;
                }

                if (tipoDeRelatorioEzlivEnum == TipoDeRelatorioEzlivEnum.FichaDeAssociacao)
                {
                    // Para o editor não precisa de dados
                    var dados = new FichaDoRelatorioV1Input();
                    CriarInstanciasDoDtoSchemaParaEditor(dados);
                    dadosFormulario = dados;
                }

                if (tipoDeRelatorioEzlivEnum == TipoDeRelatorioEzlivEnum.Aditivo)
                {
                    // Para o editor não precisa de dados
                    var dados = new FichaDoRelatorioV1Input();
                    CriarInstanciasDoDtoSchemaParaEditor(dados);
                    dadosFormulario = dados;
                }
            }
            #endregion

            if (dadosFormulario == null)
            {
                throw new UserFriendlyException(L("Relatorio.NaoFoiPossivelRecuperarORelatorioError"));
            }

            return View(new RelatorioViewModel()
            {
                DadosDoRelatorio = dadosFormulario,
                RelatorioId = relatorioId
            });
        }

        public async Task<ActionResult> DownloadMrt(int id)
        {
            #region Permissões

            if (AbpSession.UserId == null || AbpSession.TenantId == null)
            {
                return Redirect(Url.Action("Login", "Account", new { ReturnUrl = Request.Url.ToString() }));
            }

            if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio)
                    && !PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_AcessoLeitura))
            {
                throw new UserFriendlyException(L("Relatorio.SemPermissaoParaVisualizarError"));
            }

            #endregion

            var relatorio = await _relatorioService.GetById(id);

            if (relatorio == null)
            {
                throw new UserFriendlyException(L("Relatorio.SemDadosDeEntradaParaCarregarRelatorioError"));
            }

            byte[] fileBytes = System.Text.Encoding.UTF8.GetBytes(relatorio.Conteudo);
            string fileName = string.Concat(relatorio.Nome.Replace(' ', '_'), ".mrt");
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Text.Plain, fileName);
        }

        private void CriarInstanciasDoDtoSchemaParaEditor(object dto, int nivel = 1, int nivelParaListas = 1)
        {
            if (nivel <= 6 || nivelParaListas <= 4)
            {
                var props = dto.GetType().GetProperties()
                    .Where(x => x.CanWrite 
                        && !x.PropertyType.IsPrimitive
                        && !x.PropertyType.IsSealed);
                foreach (var prop in props)
                {
                    if (typeof(IEnumerable).IsAssignableFrom(prop.PropertyType))
                    {
                        var listType = typeof(List<>);
                        Type typeDtoEnumerable = prop.PropertyType.GenericTypeArguments[0];
                        var constructedListType = listType.MakeGenericType(typeDtoEnumerable);
                        var propDtoEnumerable = (IList)Activator.CreateInstance(constructedListType);
                        var propDto = Activator.CreateInstance(typeDtoEnumerable);
                        propDtoEnumerable.Add(propDto);
                        prop.SetValue(dto, propDtoEnumerable, null);
                        CriarInstanciasDoDtoSchemaParaEditor(propDto, nivel++, nivelParaListas++);
                    }
                    else
                    {
                        object propDto = Activator.CreateInstance(prop.PropertyType);
                        prop.SetValue(dto, propDto, null);
                        CriarInstanciasDoDtoSchemaParaEditor(propDto, nivel++, nivelParaListas++);
                    }
                }
            }
        }
    }
}