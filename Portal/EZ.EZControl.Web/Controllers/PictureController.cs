﻿using Abp.Auditing;
using Abp.Web.Mvc.Authorization;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Net.MimeTypes;
using EZ.EZControl.Services.Core.Geral.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EZ.EZControl.Web.Controllers
{
    [AbpMvcAuthorize]
    [DisableAuditing]
    public class PictureController : EZControlControllerBase
    {
        private readonly IPictureService _pictureService;
        private readonly ICorretoraService _corretoraService;
        private readonly IEmpresaAppService _empresaAppService;
        private readonly IArquivoGlobalService _arquivoGlobalService;
        public PictureController(IPictureService pictureService, ICorretoraService corretoraService, IEmpresaAppService empresaAppService, IArquivoGlobalService service)
        {
            _pictureService = pictureService;
            _corretoraService = corretoraService;
            _empresaAppService = empresaAppService;
            _arquivoGlobalService = service;
        }

        public async Task<FileResult> GetById(int id)
        {
            if (id == 0)
                return GetDefaultPicture();

            return await GetPictureById(id);
        }

        private FileResult GetDefaultPicture()
        {
            return File(Server.MapPath("~/Common/Images/default-profile-picture.png"), MimeTypeNames.ImagePng);
        }

        private async Task<FileResult> GetPictureById(int id)
        {
            var file = await _pictureService.GetById(id);
            if (file == null)
                return GetDefaultPicture();

            return File(file.Bytes, MimeTypeNames.ImageJpeg);
        }

        [HttpPost]
        public ActionResult GetPictureLogoByName(string name)
        {
            //var teste = GetLogoById(240);
            //var pictureEmpresa =_empresaAppService.GetEmpresaBySlug(new Dto.Global.SubtiposPessoa.Empresa.GetEmpresaInput() {NomeFantasia = name });

            var pictureLogo = _corretoraService.GetCorretoraByName(name);            
            return Json(pictureLogo != null ? new { src= pictureLogo.Imagem.Path, alt= pictureLogo.Nome } : new { src = "Uploads/logo-padrao.png", alt = "Corretora" }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ArquivoGlobal> GetLogoById(int input)
        {
            return await _arquivoGlobalService.GetById(input);
        }

    }
}