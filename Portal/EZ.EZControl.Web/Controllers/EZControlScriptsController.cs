﻿using System.Globalization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Extensions;
using Abp.Web.Authorization;
using Abp.Web.Features;
using Abp.Web.Localization;
using Abp.Web.MultiTenancy;
using Abp.Web.Navigation;
using Abp.Web.Sessions;
using Abp.Web.Settings;
using Abp.Web.Mvc.Controllers;
using EZ.EZControl.Web.Constant;

namespace EZ.EZControl.Web.Controllers
{
    /// <summary>
    /// This controller is used to create client side scripts
    /// to work with ABP.
    /// </summary>
    [Authorize]
    public class EZControlScriptsController : AbpController
    {
        private readonly IContanstScriptManager _contanstScriptManager;

        /// <summary>
        /// Constructor.
        /// </summary>
        public EZControlScriptsController(IContanstScriptManager enumScriptManager)
        {
            _contanstScriptManager = enumScriptManager;
        }

        /// <summary>
        /// Pega todos os scripts dinâmicos da ezcontrol
        /// </summary>
        [DisableAuditing]
        public async Task<ActionResult> GetScripts(string culture = "")
        {
            if (!culture.IsNullOrEmpty())
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            }

            var sb = new StringBuilder();

            sb.AppendLine(_contanstScriptManager.GetScript());
            sb.AppendLine();

            sb.AppendLine(GetTriggerScript());

            return Content(sb.ToString(), "application/x-javascript", Encoding.UTF8);
        }

        private static string GetTriggerScript()
        {
            var script = new StringBuilder();

            script.AppendLine("(function(){");
            script.AppendLine("    abp.event.trigger('abp.dynamicScriptsInitialized');");
            script.Append("})();");

            return script.ToString();
        }
    }
}
