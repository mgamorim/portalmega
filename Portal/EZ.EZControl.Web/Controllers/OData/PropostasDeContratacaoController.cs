﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.WebApi.OData.Controllers;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.EZLiv.Geral;
using System.Web.Mvc;

namespace EZ.EZControl.Web.Controllers
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao)]
    public class PropostasDeContratacaoController : AbpODataEntityController<PropostaDeContratacao>
    {
        public PropostasDeContratacaoController(IRepository<PropostaDeContratacao> repository)
        : base(repository)
        {
        }

    }
}