﻿using Abp.Web.Models;
using Abp.Web.Mvc.Controllers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.ADO.Repositorio;
using EZ.EZControl.ADO.Repositorio.Interface;
using System.Configuration;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Dto.Global.Geral.Perfil;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Domain.Core.Enums;
using Abp.Domain.Uow;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using Abp.Runtime.Security;
using Abp.Extensions;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Microsoft.Owin.Security;
using EZ.EZControl.Web.Auth;
using Abp.Authorization.Users;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Authorization;
using System.Threading;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Domain.EZLiv.Enums;
using System.Text;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using System.Data.Entity;

namespace EZ.EZControl.Web.Controllers
{
    [Abp.Web.Security.AntiForgery.DisableAbpAntiForgeryTokenValidation]
    public class SimuladorController : AbpController
    {

        #region VARIAVEIS

        private readonly IProdutoDePlanoDeSaudeAppService _produtoplanosaude;
        private readonly ICorretorService _corretorService;
        private readonly IPerfilAppService _perfilAppService;
        private readonly IBeneficiarioBaseService _beneficiarioService;
        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IAuthenticationManager _authenticationManager;
        private IRepositorioSimulador _repositorio = null;
        public readonly IPropostaDeContratacaoAppService _PropostaDeContratacaoAppService;
        private readonly IProfissaoService _profissaoService;
        private readonly IEstadoService _estadoService;
        private readonly ICorretoraService _corretoraService;
        private readonly UserManager _userManager;
        private readonly LogInManager _logInManager;
        private string UserPass;
        private readonly IEmpresaService _empresaService;
        public class lista
        {
            public int Id { get; set; }
            public String Descricao { get; set; }
        }

        #endregion


        #region CONSTRUTORES

        public SimuladorController(IProdutoDePlanoDeSaudeAppService produtoplanosaude,
                                   IPerfilAppService perfilAppService,
                                   IUnitOfWorkManager unitOfWorkManager,
                                   IBeneficiarioBaseService beneficiarioService,
                                   IAuthenticationManager authenticationManager,
                                   AbpLoginResultTypeHelper abpLoginResultTypeHelper,
                                   UserManager userManager,
                                   LogInManager logInManager,
                                   IPropostaDeContratacaoAppService PropostaDeContratacaoAppService,
                                   IProfissaoService profissaoService,
                                   IEstadoService estadoService, 
                                   IEmpresaService empresaService,
                                   ICorretoraService corretoraService,
                                   ICorretorService corretorService
            )
        {
            _produtoplanosaude = produtoplanosaude;
            _corretorService = corretorService;
            _perfilAppService = perfilAppService;
            _userManager = userManager;
            _beneficiarioService = beneficiarioService;
            _unitOfWorkManager = unitOfWorkManager;
            _authenticationManager = authenticationManager;
            _logInManager = logInManager;
            _profissaoService = profissaoService;
            _estadoService = estadoService;
            _empresaService = empresaService;
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
            _PropostaDeContratacaoAppService = PropostaDeContratacaoAppService;
            _corretoraService = corretoraService;
            _repositorio = new RepositorioSimulador(ConfigurationManager.ConnectionStrings["SimuladorDB"].ConnectionString);
        }

        #endregion


        #region AÇÕES

        // GET: Simulador
        public async Task<ActionResult> Index()
        {
            try
            {



                if (Request.QueryString["parceiro"] == null)
                        Session.Add("parceiro", "");
                else
                    Session.Add("parceiro", Request.QueryString["parceiro"]);


                if (Request.QueryString["slug"] == null)
                    throw new Exception();


                if (Request.QueryString["slug"] == "corretor")
                {
                    Session.Add("NomeEmpresa", Request.QueryString["slug"]);
                }
                else
                {
                    var ListaEmpresa = await BuscarEmpresas();
                    var EmpresaAtual = ListaEmpresa.FindAll(x => x.Slug == Request.QueryString["slug"]);

                    if (EmpresaAtual.Count == 0)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        Session.Add("NomeEmpresa", Request.QueryString["slug"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View();
        }





        #endregion


        #region MÉTODOS


        [HttpPost]
        public async Task<JsonResult> BuscarProfissao()
        {
            try
            {
                var ListaSistemaProfissao = await _profissaoService.GetAllListAsync();

                var listaProfissao = new List<lista>();

                foreach (var item in ListaSistemaProfissao)
                {
                    lista listaP = new lista();
                    listaP.Id = item.Id;
                    listaP.Descricao = item.Titulo;

                    listaProfissao.Add(listaP);
                }

                return Json(listaProfissao.OrderBy(x => x.Descricao));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }

        [HttpPost]
        public async Task<JsonResult> BuscarEstado()
        {
            try
            {
                var ListEstado = await _estadoService.GetAllListAsync();

                var listaEstado = new List<lista>();

                foreach (var item in ListEstado)
                {
                    lista listaE = new lista();
                    listaE.Id = item.Id;
                    listaE.Descricao = item.Nome;

                    listaEstado.Add(listaE);
                }

                return Json(listaEstado.OrderBy(x => x.Descricao));
            }
            catch (Exception ex)
            {
                return Json("");
            }
        }


        [HttpPost]
        public async Task<JsonResult> BuscarPlanos(SimulacaoModel model)
        {
            try
            {
                model.resultado = new SimulacaoModel.Result();
                var ListaProdutosComValor = new List<ProdutoDePlanoDeSaudeListDto>();
                var EmpresaAtual = new List<Empresa>();
                GetValoresAndProdutoByCorretorInput dados = new GetValoresAndProdutoByCorretorInput();

                if (Session["NomeEmpresa"] == null)
                {
                    model.resultado.Erro = "Parâmetro Empresa Ausente !";
                    return Json(model);
                }
                else
                {
                    if (Session["NomeEmpresa"].ToString() == "corretor")
                    {
                        var user = await BuscarUsuarioLogado();
                        var corretor = _corretorService.GetAll().Where(x => x.PessoaId == user.PessoaId);
                        var CorretorAtual = await corretor.ToListAsync();
                            dados.CorretorId = CorretorAtual.First().Id;
                        model.Empresa = "Corretor";

                        ListaProdutosComValor = await _produtoplanosaude.GetProdutoDePlanoDeSaudeWithValorForCorretorSimulador(dados.CorretorId , model);
                    }
                    else
                    {
                        var ListaEmpresa = await _empresaService.GetAllListAsync();
                        EmpresaAtual = ListaEmpresa.FindAll(x => x.Slug == Session["NomeEmpresa"].ToString());
                        model.Empresa = EmpresaAtual.First().NomeFantasia;
                        model.Parceiro = Session["parceiro"].ToString() == null ? "" : Session["parceiro"].ToString();

                        if (EmpresaAtual.Count == 0)
                        {
                            model.resultado.Erro = "Empresa Não Autorizada !";
                            return Json(model);

                        }
                        else
                        {
                            var corretora = await _corretoraService.GetCorretoraBySlug(EmpresaAtual.First().Slug);
                            dados.CorretorId = Convert.ToInt32(corretora.CorretorId);
                            ListaProdutosComValor = await _produtoplanosaude.GetProdutoDePlanoDeSaudeWithValorForCorretorSimulador(dados.CorretorId, model);
                        }
                    }
                }

                //objetos = objetos.OrderBy(o => o.Desc).ToList();

                ListaProdutosComValor = ListaProdutosComValor.OrderBy(x => x.Valor).ToList();


                model.DataNascimento = Convert.ToDateTime(model.DataNascimento).ToString("dd/MM/yyyy");

                model.Chave = "SM" + DateTime.Now.Year + DateTime.Now.Month +
                                     DateTime.Now.Day + DateTime.Now.Hour +
                                     DateTime.Now.Minute + DateTime.Now.Second +
                                     DateTime.Now.Millisecond;




                //  var ListaProdutosComValor = await BuscarPlanosSaude(model);
                var Simulacao = new { IdSimulacao = model.Chave };
                var resultado = new { ListaProdutosComValor, Simulacao };


                await GravarSimulacao(model);

                return Json(resultado);

            }
            catch (Exception ex)
            {
                model.resultado.Erro = ex.Message;
                return Json(model);
            }
        }

        private async Task<User> BuscarUsuarioLogado()
        {
            var beneficiarioUser = new User();

            try
            {
                beneficiarioUser = await _userManager.FindByIdAsync(AbpSession.UserId.Value);

            }
            catch (Exception ex)
            {


            }

            return beneficiarioUser;
        }


        [HttpPost]
        public async Task<JsonResult> CompreAgora(SimulacaoModel model)
        {
            try
            {
                var EmpresaAtual = new List<Empresa>();
                model.resultado = new SimulacaoModel.Result();

                if (Session["NomeEmpresa"] != null)
                {
                    var ListaEmpresa = await _empresaService.GetAllListAsync();
                    EmpresaAtual = ListaEmpresa.FindAll(x => x.Slug == Session["NomeEmpresa"].ToString());
                }
                else
                {
                    model.resultado.Erro = "Parâmetro Empresa Ausente !";
                    return Json(model);
                }

                var modeldb = _repositorio.BuscaSimulacao(model);

                model.Empresa = EmpresaAtual[0].NomeFantasia;
                model.idPlano = modeldb.idPlano;
                model.NomePlano = modeldb.NomePlano;
                model.DescricaoPlano = modeldb.DescricaoPlano;
                model.NomeOperadora = modeldb.NomeOperadora;
                model.NomeAdministradora = modeldb.NomeAdministradora;
                model.Abrangencia = modeldb.Abrangencia;
                model.Acomodacao = modeldb.Acomodacao;
                model.ValorTotal = modeldb.ValorTotal;

                

                //###############################################################################################################
                // CRIAÇÃO DO USUARIO E ENVIAR EMAIL PARA O USUARIO COM A SENHA
                UserPass = GerarSenha();
                var beneficiarioUser = await RegistroUsuario(model, UserPass, EmpresaAtual[0].PessoaJuridicaId);

                //###############################################################################################################
                if (beneficiarioUser.Id != 0)
                {

                    //###############################################################################################################
                    // AUTENTICAÇÃO DO USUARIO 

                    var ret = await AutenticacaoUsuario(beneficiarioUser, UserPass, EmpresaAtual[0]);

                    //###############################################################################################################





                    //###############################################################################################################
                    // CRIAÇÃO DA PROPOSTA DE CONTRATAÇÃO ATÉ O PASSO 3


                    //Dados Iniciais PRE CADASTRO
                    PropostaDeContratacaoPassoPreCadastroInput inputPrecadastro = new PropostaDeContratacaoPassoPreCadastroInput();

                    inputPrecadastro.TitularId = 0;
                    inputPrecadastro.NomeCompleto = model.Nome;
                    inputPrecadastro.DataDeNascimento = Convert.ToDateTime(model.DataNascimento);
                    inputPrecadastro.Cpf = model.Cpf;
                    inputPrecadastro.Email = model.Email;
                    inputPrecadastro.Celular = model.Celular;
                    inputPrecadastro.ProfissaoId = Convert.ToInt32(model.Profissao);
                    inputPrecadastro.EstadoId = Convert.ToInt32(model.Estado);
                    inputPrecadastro.TitularResponsavelLegal = model.TitularResponsavel == "True" ? true : false;
                    inputPrecadastro.PassoDaProposta = PassoDaPropostaEnum.PreCadastro;

                    //Action
                    var retornoPreCadastro = await _PropostaDeContratacaoAppService.SavePassoPreCadastroSimulador(inputPrecadastro, Convert.ToInt32(beneficiarioUser.Id), EmpresaAtual[0].Id, model.idPlano);


                    //Dados Iniciais SELECIONAR PLANO

                    PropostaDeContratacaoPassoSelecionarPlanoInput inputSelecionarPlano = new PropostaDeContratacaoPassoSelecionarPlanoInput();

                    inputSelecionarPlano.Id = retornoPreCadastro.Id;
                    inputSelecionarPlano.ProdutoDePlanoDeSaudeId = model.idPlano; // ALTERAR
                    inputSelecionarPlano.CorretoraId = Convert.ToInt32(retornoPreCadastro.CorretoraId);
                    inputSelecionarPlano.PassoDaProposta = PassoDaPropostaEnum.DadosProponenteTitular;

                    var retornoSelecionarPlano = await _PropostaDeContratacaoAppService.SavePassoSelecionarPlanoSimulador(inputSelecionarPlano);

                    //###############################################################################################################




                    //###############################################################################################################
                    // REDIRECT USUARIO PROPOSTA NO ID

                    model.RedirectUrl = "http://" + Request.Url.Authority +  string.Format("/#/tenant/ezliv/propostaDeContratacao/alterar/{0}", retornoPreCadastro.Id);
                    model.idProposta = retornoPreCadastro.Id.ToString();

                    
                    _repositorio.AtualizaSimulacao(model);


                    //###############################################################################################################
                }

                return Json(model);

            }
            catch (Exception ex)
            {
                if (ex.Message == "O CPF informado não é válido")
                        model.resultado.Erro = ex.Message;
                if (ex.Message == "Referência de objeto não definida para uma instância de um objeto.")
                    model.resultado.Erro = ex.Message;

                return Json(model);
            }
        }

        [HttpPost]
        public JsonResult ResumoSimulador(SimulacaoModel model)
        {
            try
            {

                //model.Nome = "Mauricio Amorim";
                //model.Email = "mauricioamorim22@gmail.com";
                //model.DataNascimento = "12/08/1988";
                //model.Cpf = "11722219793";
                //model.Celular = "21984993551";
                //model.Estado = "RJ";
                //model.Profissao = "Analista de sistemas";
                //model.PossuiDependentes = "true";
                //model.TitularResponsavel = "true";

                //model.Dependentes = new List<SimulacaoModel.Dependente>();

                //var dependente = new SimulacaoModel.Dependente();

                //dependente.Nome = "Jose Marcos Amorim";
                //dependente.Cpf = "70195371454";
                //dependente.DataNascimento = "05/06/2016";

                //model.Dependentes.Add(dependente);

                //model.idPlano = 25;
                //model.Empresa = "MEGAVITA";
                //model.NomePlano = "SALUTAR 600 COLETIVO";
                //model.DescricaoPlano = "SALUTAR 10 FAIXAS";
                //model.NomeOperadora = "SALUTAR SAUDE";
                //model.NomeAdministradora = "ABRACIM";
                //model.Abrangencia = "ESTADUAL";
                //model.Acomodacao = "ENFERMARIA";
                //model.ValorTotal = "540,21";





                 _repositorio.AtualizaSimulacao(model);

                return Json(model);

            }
            catch (Exception ex)
            {
                model.resultado.Erro = ex.Message;
                return Json(model.resultado.Erro);
            }
        }


        #endregion


        #region AUXILIARES

        private async Task SignInAsync(User user, ClaimsIdentity identity = null, bool rememberMe = false)
        {
            if (identity == null)
            {
                identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            }

            identity.AddClaim(new Claim("Session_Persisted", rememberMe.ToString()));

            _authenticationManager.SignOutAllAndSignIn(identity, rememberMe);
        }

        private async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            try
            {
                var loginResult = await _logInManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

                switch (loginResult.Result)
                {
                    case AbpLoginResultType.Success:
                        return loginResult;

                    default:
                        throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private async Task<User> RegistroUsuario(SimulacaoModel model, string UserPass, int? pessoaJuridicaId)
        {
            var beneficiarioUser = new User();

            try
            {
                var idPessoaFisica = await _perfilAppService.Save(new PerfilInput()
                {
                    Cpf = model.Cpf,
                    EmailPrincipal = new EnderecoEletronicoInput()
                    {
                        Endereco = model.Email
                    },
                    Celular = model.Celular,
                    Nome = model.Nome,
                    TipoDeUsuario = TipoDeUsuarioEnum.Beneficiario
                });



                var beneficiario = await _beneficiarioService.CreateAndReturnEntity(new BeneficiarioBase()
                {
                    PessoaFisicaId = idPessoaFisica.Id,
                    TenantId = 1

                });

                await UnitOfWorkManager.Current.SaveChangesAsync();

                using (UnitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    beneficiarioUser = await _userManager.GetByPessoaId(beneficiario.PessoaFisicaId);



                    var ListCorretora = await _corretoraService.GetAllListAsync();
                    var InfoCorretora = ListCorretora.FindAll(x => x.PessoaJuridicaId == pessoaJuridicaId);


                    var corretorainnfo = new CorretoraNotificationInfo();

                    corretorainnfo.Email = InfoCorretora[0].PessoaJuridica.EmailPrincipal == null ? "contato@portalmega.net" : InfoCorretora[0].PessoaJuridica.EmailPrincipal.Endereco;
                    corretorainnfo.Logo = InfoCorretora[0].Imagem == null ? "" : "https://portalmega.net/" + InfoCorretora[0].Imagem.Path;
                    corretorainnfo.NomeFantasia = InfoCorretora[0].PessoaJuridica.NomeFantasia.ToString();
                    corretorainnfo.Telefone = InfoCorretora[0].PessoaJuridica.TelefoneComercial.DDD.ToString() + "  " + InfoCorretora[0].PessoaJuridica.TelefoneComercial.Numero.ToString();
                    corretorainnfo.RazaoSocial = InfoCorretora[0].PessoaJuridica.RazaoSocial.ToString();


                    _beneficiarioService.SendEmailSimulador(beneficiarioUser, corretorainnfo, UserPass);

                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Substring(0,20) == "Já existe um usuário")
                    model.resultado.Erro = ex.Message;

                if (ex.Message == "O CPF informado não é válido")
                    model.resultado.Erro = ex.Message;

            }

            return beneficiarioUser;
        }

        private async Task<bool> AutenticacaoUsuario(User beneficiarioUser, string UserPass, Empresa empresa)
        {
            bool retorno = true;
            var tenantId = 1;
            var userId = Convert.ToInt64(beneficiarioUser.Id.ToString());


            _unitOfWorkManager.Current.SetTenantId(tenantId);

            var user = await _userManager.GetUserByIdAsync(userId);

            user.Password = new PasswordHasher().HashPassword(UserPass);
            user.PasswordResetCode = null;
            user.IsEmailConfirmed = true;
            user.ShouldChangePasswordOnNextLogin = false;


            //var corretoraEntity = await _corretoraService.GetCorretoraBySlug(model.slug);

                user.Empresas.Remove(user.Empresas.First());
                user.Empresas.Add(empresa);


            await _userManager.UpdateAsync(user);

            if (user.IsActive)
            {
                await SignInAsync(user);
            }

            var numEmpresas = user.Empresas.Count;

            if (numEmpresas == 1)
            {

                AbpLoginResult<Tenant, User> loginResult = await GetLoginResultAsync(user.UserName, UserPass, "Default");

                var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;

                bool isPersistent = true;

                var persistedClaim = claimsPrincipal.FindFirst("Session_Persisted");
                if (persistedClaim != null)
                {
                    isPersistent = Convert.ToBoolean(persistedClaim.Value);
                }

                loginResult.Identity.AddClaim(new Claim("EmpresaId", user.Empresas.First().Id.ToString()));

                _authenticationManager.SignOutAllAndSignIn(loginResult.Identity, isPersistent);

            }

            return retorno;

        }

        private string GerarSenha()
        {
            string senha = string.Empty;

            Random random = new Random();
            string validar = "abcdefghijklmnopqrstuvxz1234567890";
            StringBuilder str = new StringBuilder(100);
            var i = 0;
            while (i < 8)
            {
                str.Append(validar[random.Next(validar.Length)]);
                i++;
            }

            senha = str.ToString();

            return senha;

        }

        private async Task<bool> GravarSimulacao(SimulacaoModel model)
        {
            try
            {
                var resposta = await _repositorio.GravarSimulacao(model);

                return resposta;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //private async Task<List<ProdutoDePlanoDeSaudeListDto>> BuscarPlanosSaude(SimulacaoModel model)
        //{
        //    var ListaProdutosComValor = await _produtoplanosaude.GetSimulacaoProdutosComValores(model);

        //    return ListaProdutosComValor;
        //}

        private async Task<List<Empresa>> BuscarEmpresas()
        {
            var ListaEmpresa = await _empresaService.GetAllListAsync();

            return ListaEmpresa;
        }



        #endregion

    }

}