﻿using System.Web.Mvc;
using Abp.Auditing;

namespace EZ.EZControl.Web.Controllers
{
    public class ErrorController : EZControlControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}