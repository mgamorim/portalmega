﻿(function (abp, angular) {

    if (!angular) {
        return;
    }

    var eParaExecutaOShowError = function (config) {
        return config.url.indexOf("SendEmail") === -1;
        //return config.method === 'POST' && config.url.indexOf("app/chat") === -1;
    };

    abp.ng = abp.ng || {};

    abp.ng.http = {
        defaultError: {
            message: 'Ocorreu um erro!',
            details: 'Detalhes do erro não enviados pelo servidor.'
        },

        defaultError401: {
            message: 'Você não está autenticado!',
            details: 'Você deve ser autenticado (iniciar sessão) para executar esta operação.'
        },

        defaultError403: {
            message: 'Você não está autorizado!',
            details: 'Você não tem permissão para executar esta operação.'
        },

        defaultError404: {
            message: 'Recurso não encontrado!',
            details: 'O recurso solicitado não foi encontrado no servidor.'
        },

        logError: function (error) {
            abp.log.error(error);
        },

        showError: function (error) {
            if (error.details) {
                return abp.message.error(error.details, error.message || abp.ng.http.defaultError.message);
            } else {
                return abp.message.error(error.message || abp.ng.http.defaultError.message);
            }
        },

        handleTargetUrl: function (targetUrl) {
            if (!targetUrl) {
                location.href = abp.appPath;
            } else {
                location.href = targetUrl;
            }
        },

        handleNonAbpErrorResponse: function (response, defer) {
            if (response.config.abpHandleError !== false) {
                switch (response.status) {
                    case 401:
                        abp.ng.http.handleUnAuthorizedRequest(
                            abp.ng.http.showError(abp.ng.http.defaultError401),
                            abp.appPath
                        );
                        break;
                    case 403:
                        abp.ng.http.showError(abp.ajax.defaultError403);
                        break;
                    case 404:
                        abp.ng.http.showError(abp.ajax.defaultError404);
                        break;
                    default:
                        abp.ng.http.showError(abp.ng.http.defaultError);
                        break;
                }
            }

            defer.reject(response);
        },

        handleUnAuthorizedRequest: function (messagePromise, targetUrl) {
            if (messagePromise) {
                messagePromise.done(function () {
                    abp.ng.http.handleTargetUrl(targetUrl || abp.appPath);
                });
            } else {
                abp.ng.http.handleTargetUrl(targetUrl || abp.appPath);
            }
        },

        handleResponse: function (response, defer) {
            var originalData = response.data;

            if (originalData.success === true) {
                response.data = originalData.result;
                defer.resolve(response);

                if (originalData.targetUrl) {
                    abp.ng.http.handleTargetUrl(originalData.targetUrl);
                }
            } else if (originalData.success === false && eParaExecutaOShowError(response.config)) {
                var messagePromise = null;

                if (originalData.error) {
                    if (response.config.abpHandleError !== false) {
                        messagePromise = abp.ng.http.showError(originalData.error);
                    }
                } else {
                    originalData.error = defaultError;
                }

                abp.ng.http.logError(originalData.error);

                response.data = originalData.error;
                defer.reject(response);

                if (response.status == 401 && response.config.abpHandleError !== false) {
                    abp.ng.http.handleUnAuthorizedRequest(messagePromise, originalData.targetUrl);
                }
            } else { //not wrapped result
                if (eParaExecutaOShowError(response.config))
                    defer.resolve(response);
            }
        }
    }

    var abpModule = angular.module('abp', []);

    var eParaExecutaOLoader = function (config) {
        return config.url.indexOf("app/chat") === -1
                && config.url.indexOf("SendEmail") === -1;
        //return config.method === 'POST' && config.url.indexOf("app/chat") === -1;
    };

    abpModule.config([
        '$httpProvider', function ($httpProvider) {
            $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {

                return {

                    'request': function (config) {
                        if (eParaExecutaOLoader(config)) {
                            $rootScope.ezLoadingSpinner.add();
                        }

                        if (endsWith(config.url, '.cshtml')) {
                            config.url = abp.appPath + 'AbpAppView/Load?viewUrl=' + config.url + '&_t=' + abp.pageLoadTime.getTime();
                        }

                        return config;
                    },

                    'response': function (response) {
                        if (eParaExecutaOLoader(response.config)) {
                            $rootScope.ezLoadingSpinner.remove();
                        }

                        if (!response.data || !response.data.__abp) {
                            //Non ABP related return value
                            return response;
                        }

                        var defer = $q.defer();
                        abp.ng.http.handleResponse(response, defer);
                        return defer.promise;
                    },

                    'responseError': function (ngError) {
                        if (eParaExecutaOLoader(ngError.config)) {
                            $rootScope.ezLoadingSpinner.remove();
                        }

                        var defer = $q.defer();

                        if (!ngError.data || !ngError.data.__abp) {
                            abp.ng.http.handleNonAbpErrorResponse(ngError, defer);
                        } else {
                            abp.ng.http.handleResponse(ngError, defer);
                        }

                        return defer.promise;
                    }

                };
            }]);
        }
    ]);

    function endsWith(str, suffix) {
        if (suffix.length > str.length) {
            return false;
        }

        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    abp.event.on('abp.dynamicScriptsInitialized', function () {
        abp.ng.http.defaultError.message = abp.localization.abpWeb('DefaultError');
        abp.ng.http.defaultError.details = abp.localization.abpWeb('DefaultErrorDetail');
        abp.ng.http.defaultError401.message = abp.localization.abpWeb('DefaultError401');
        abp.ng.http.defaultError401.details = abp.localization.abpWeb('DefaultErrorDetail401');
        abp.ng.http.defaultError403.message = abp.localization.abpWeb('DefaultError403');
        abp.ng.http.defaultError403.details = abp.localization.abpWeb('DefaultErrorDetail403');
        abp.ng.http.defaultError404.message = abp.localization.abpWeb('DefaultError404');
        abp.ng.http.defaultError404.details = abp.localization.abpWeb('DefaultErrorDetail404');
    });

})((abp || (abp = {})), (angular || undefined));