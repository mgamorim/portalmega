

// Simulador                
$(document).ready(function () {

    // GLOBAL VARIABLES
    let isNav = false, isNavFinaly = false, onyCreatDataTable = true, resultPlans = [], resultSummary = [], _segmentacaoAssistencial = '';
    document.getElementById('customCheck1').checked = true; window.arrayImg = [];
    let arrayImg = [
        { title: 'Amex Saúde', src: '/libs/simulador/img/amex.png', description: '', id: 'img-1' },
        { title: 'Ônix', src: '/libs/simulador/img/onix.png', description: '', id: 'img-2' },
        { title: 'Salutar Saúde', src: '/libs/simulador/img/salutar.png', description: '', id: 'img-3' },
        { title: 'Caberj  Integral Saúde S/A', src: '/libs/simulador/img/caberj.png', description: '', id: 'img-4' },
        { title: 'Unimed Angra', src: '/libs/simulador/img/unimedangra.png', description: '', id: 'img-5' },
        { title: 'Unimed Rio', src: '/libs/simulador/img/unimedrio.png', description: '', id: 'img-6' },
        { title: 'Evercross', src: '/libs/simulador/img/evecross.png', description: '', id: 'img-7' },
        { title: 'Amil', src: '/libs/simulador/img/amil.png', description: '', id: 'img-8' },
        { title: 'Unimed Central Nacional', src: '/libs/simulador/img/logoCentralUnimed.jpg', description: '', id: 'img-9' },
        { title: 'Unimed Vale Do Aço', src: '/libs/simulador/img/unimed_valedoaco2.jpg', description: '', id: 'img-10' },
        { title: 'Next Saúde', src: '/libs/simulador/img/nextsaude.jpg', description: '', id: 'img-11' },
        { title: 'Assim Saúde', src: '/libs/simulador/img/assim.jpg', description: '', id: 'img-12' },
        { title: 'OneHealth', src: '/libs/simulador/img/logo-one-health.jpg', description: '', id: 'img-13' },
        { title: 'Unimed Paraná', src: '/libs/simulador/img/unimed_parana.jpg', description: '', id: 'img-14' },
        { title: 'UNIMED BH', src: '/libs/simulador/img/Unimed-Belo-Horizonte-1.png', description: '', id: 'img-15' },
        { title: 'Cemeru', src: '/libs/simulador/img/download.png', description: '', id: 'img-16' }
    ];

    arrayImg.forEach(element => {
            convertImgToBase64(`${'http://' + window.location.host + element.src}`, function(base64) {
                //base64Img = base64; 
                window.arrayImg.push({
                    imagem: base64,
                    op: element.title
                })
            });
    });

    if (screenfull.enabled){
        document.querySelector('#toview-table-pdf').style.display = null;
    }
    //let $inputCpf = $("#inputCpf");
    //$inputCpf.mask('000.000.000-00', { placeholder: "___.___.___-__" });

    // visão para corretores - /?corretor=yes   
    // if (url_query('corretor')) {
         let navStep3 = $('.steps-step-2')[2];
         navStep3.style.display = 'none';
    // }

    let $inputTel = $("#inputTel");
    $inputTel.mask('(00) 00000-0000', { placeholder: "(__) ____-_____" });
    //let $inputEmail = $("#inputEmail");   
    let navListItems = $('div.setup-panel-2 div'),
        allWells = $('.setup-content-2'); allWells.hide(); loadStep(navListItems);
    let idSimulacao = '';
    let dataSet = [];
    let resultProfissao = [];
    let resultEstado = [];
    BuscarEstado();
    BuscarProfissao();
    let obj = {};
    let abrangencia = {
        0: 'NaoInformado',
        1: 'Nacional',
        2: 'GrupoDeEstados',
        3: 'Estadual',
        4: 'GrupoDeMunicipios',
        5: 'Municipal',
        6: 'Outra'
    };
    window.abran = abrangencia;
    let acomodacao = {
        1: 'Enfermaria',
        2: 'Apartamento',
        3: 'NaoInformado'
    };
    window.acomod = acomodacao;
    let segmentacaoAssistencial = [

        { value: 0, descricao: "Não Informado" },
        { value: 1, descricao: "Ambulatorial" },
        { value: 2, descricao: "Dental" },
        { value: 3, descricao: "Hospitalar" },
        { value: 4, descricao: "Hospitalar + Ambulatorial" },
        { value: 5, descricao: "Hospitalar + Obstetricia" },
        { value: 6, descricao: "Hospitalar + Ambulatorial + Obstetrícia" },
        { value: 7, descricao: "Hospitalar + Ambulatorial + Dental" },
        { value: 8, descricao: "Hospitalar + Obstetrícia + Dental" },
        { value: 9, descricao: "Hospitalar + Ambulatorial + Obstetrícia + Dental" },


        /*
            { value: 0, descricao: "Não Informado" },
            { value: 1, descricao: "Ambulatorial" },
            { value: 2, descricao: "Dental" },
            { value: 3, descricao: "Hospitalar" },
            { value: 4, descricao: "Hospitalar + Ambulatorial" },
            { value: 5, descricao: "Hospitalar + Obstetricia" },
            { value: 6, descricao: "Hospitalar + Ambulatorial + Obstetrícia" },
            { value: 7, descricao: "Hospitalar + Ambulatorial + Dental" },
            { value: 8, descricao: "Hospitalar + Obstetrícia + Dental" },
            { value: 9, descricao: "Hospitalar + Ambulatorial + Obstetricia + Dental" },
            { value: 10, descricao: "Hospitalar com Obstetrícia + Odontológico" },
            { value: 11, descricao: "Hospitalar sem Obstetrícia + Odontológico" },
            { value: 12, descricao: "Hospitalar com ou sem Obstetrícia + Ambulatorial" },
            { value: 13, descricao: "Hospitalar com Obstetrícia + Ambulatorial + Odontológico" },
            { value: 14, descricao: "Hospitalar sem Obstetrícia + Ambulatorial + Odontológico" },
            { value: 15, descricao: "Hospitalar com ou sem Obstetrícia + Odontológico" },
            { value: 99, descricao: "Informado Incorretamente" }
    
            */

        /*
        NaoInformado = 0,
        Ambulatorial = 1,
        Dental = 2,
        Hospitalar = 3,
        HospitalarAmbulatorial = 4,
        HospitalarObstetricia = 5,
        HospitalarAmbulatorialObstetricia = 6,
        HospitalarAmbulatorialDental = 7,
        HospitalarObstetriciaDental = 8,
        HospitalarAmbulatorialObstetriciaDental = 9
        */



    ];
    window.segment = segmentacaoAssistencial;

    //EVENTS
    $("#addDependentes").click(function (e) {
        fnAddHtmlDependente(e, function (count) {
            //fnShowButtonSalvarInit(count);
        });
    });

    $('#faq-titulo-1').click(function () {
        $('#showSalvarInit').toggle(this.checked);
        $('.dependenteAll').toggle(this.checked);
        if (document.getElementById('customCheck2').checked) {
            if (!this.checked) {
                $('#dependenteButton').toggle(this.checked);
            } else {
                $('#dependenteButton').toggle(true);
            }
        } else {
            $('#dependenteButton').toggle(false);
        }
    });

    $('#customCheck2').click(function () {
        $("#dependenteButton").toggle(this.checked);
        if (!this.checked) {
            $('.checkDependente').each(function (index, value) {
                $('#' + $(this).attr('data-dep')).remove();
            });
            fnDataInfo(0);
        } else {
            fnAddHtmlDependente();
        }
    });

    $('#removeDependente').click(function () {
        let isCheked = false;
        $('.checkDependente').each(function (index, value) {
            if (this.checked) {
                $('#' + $(this).attr('data-dep')).remove(); isCheked = true;
            }
        });

        let indexLast = $('.dependenteAll').length - 1;
        //fnShowButtonSalvarInit(null, $('.checkDependente')[indexLast].dataset.dep);

        if (!isCheked) {
            alertBottom('Marque pelo menos <br> 1 dependente <br> para ser excluído!', 'attention');
        }

        if ($('.checkDependente').length == 0) {
            document.getElementById('customCheck2').checked = false;
            $("#dependenteButton").toggle(false);
        }

    });

    //$('input:not(input[type="checkbox"]), select:not(#selecione-categoriaPlans)').bind('change', function (e) {
    //    alterFields(this);
    //});

    navListItems.click(function (e) {
        e.preventDefault();
        if (isNav) {

            if (this.dataset.step == '#step-3' && !isNavFinaly) {
                alertBottom('Aperte o botão <br> Contrate online!');
                return false;
            }

            var $target = $($(this).attr('data-step')),
                $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('steps-selected');
                $item.addClass('steps-selected');
                allWells.hide();
                $target.show();

                switch ($target.attr('id')) {
                    case 'step-1':
                        var chk = document.getElementById("faq-titulo-1");
                        if (!chk.checked) {
                            chk.checked = true;
                        }
                        break;
                    case 'step-2':
                        var chk = document.getElementById("faq-titulo-2");
                        if (!chk.checked) {
                            chk.checked = true;
                        }
                        //loadPlanos(resultPlans);           
                        break;
                    case 'step-3':
                        var chk = document.getElementById("faq-titulo-3");
                        if (!chk.checked) {
                            chk.checked = true;
                        }
                        break;

                    default:
                        break;
                }

                $target.find('input[type=text]:eq(0)').focus();
            }
        } else if (this.dataset.step != '#step-1') {
            alertBottom('Preencha os dados <br> e aperte o botão <br> Simular!');
        }
    });

    //$("#selecione-categoriaPlans").change(function () {
    //    let search = $('input.searching').val();
    //    fnFilterCategory(search);
    //});

    $("#toview-table-pdf").click(function (e) {
        if (screenfull.enabled) {           
            e.preventDefault();
            e.stopPropagation();
            $('#simuladorHTML > body')[0].style.overflow = null;
            document.querySelector('.label-titulo-2').classList.add('labelHide');
            document.querySelector('#dropPdf').style.marginTop = '-30px';            
            document.querySelector('#dropPdf').style.position = 'absolute';
            document.querySelector('#dropPdf').style.marginLeft = '5%';
            document.querySelector('#dropPdf').style.marginRight = 'auto';
            document.querySelectorAll('.steps-step-2').forEach(step=>{
                step.style.zIndex = "-1";
            });
            document.querySelectorAll('.cards').forEach(step=>{
                step.style.zIndex = "-1";
            });
            document.querySelector('#exit').style.display = null;
            document.querySelector('#toview-table-pdf').style.display = 'none';
            document.querySelector('#download-table-pdf').style.display = 'none';
            document.querySelector('#contentSearching').style.zIndex = "-1";
            update(false, 'multiple');        
        }else{
            update(true, 'multiple');
        }
    });

    $('#exit').click(function (event) {
        if (screenfull.enabled) {
            $('#simuladorHTML > body')[0].style.overflow = null;            
            document.querySelector('#icon_full').classList.remove('ion-arrow-shrink');
            document.querySelector('#icon_full').classList.add('ion-arrow-expand');           
            document.querySelector('.label-titulo-2').classList.remove('labelHide');
            document.querySelector('#exit').style.display = 'none';
            document.querySelector('#toview-table-pdf').style.display = null;
            document.querySelector('#download-table-pdf').style.display = null;
            document.querySelector('#contentSearching').style.zIndex = null;
            document.querySelector('#dropPdf').style.marginTop = '20px';            
            document.querySelector('#dropPdf').style.position = null;
            document.querySelector('#dropPdf').style.marginLeft = null;
            document.querySelector('#dropPdf').style.marginRight = null;
            document.querySelectorAll('.steps-step-2').forEach(step=>{
                step.style.zIndex = null;
            });
            document.querySelectorAll('.cards').forEach(step=>{
                step.style.zIndex = null;
            });
            screenfull.exit();
            if(document.querySelector('#external-iframe')){
                document.querySelector('#external-iframe').remove();
            }
            event.preventDefault();
            event.stopPropagation();
        }
    });

    $(".searching").keyup(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode != '13') {

            let search = $('input.searching').val();
            //window.arrayImg = [];

            if (search.length > 2) {

                fnFilterCategory(search);

            } else {

                let arrayPlanos = [];

                arrayPlanos = fnSelectedFilterPlansSegment($('#selecione-categoriaPlans').val(), resultPlans);

                //if ($('#selecione-categoriaPlans').val() == 3) {
                //    arrayPlanos = resultPlans.slice();
                //} else if ($('#selecione-categoriaPlans').val() == 1) {
                //    let objCategory = resultPlans.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == 1 });
                //    arrayPlanos = objCategory.slice();
                //} else if ($('#selecione-categoriaPlans').val() != 1) {
                //    let objCategory = resultPlans.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial != 1 });
                //    arrayPlanos = objCategory.slice();
                //}

                result = arrayPlanos.slice();

                loadPlanos(result);
            }

        } else {
            event.preventDefault();
            event.stopPropagation();
        }

    });

    $('#icon_full').click(i=>{
        if (screenfull.enabled) {
            $('#simuladorHTML > body')[0].style.overflow = null
            if(document.querySelector('#icon_full').classList.contains('ion-arrow-shrink')){
                document.querySelector('#icon_full').classList.remove('ion-arrow-shrink')
                document.querySelector('#icon_full').classList.add('ion-arrow-expand')
            }else{
                document.querySelector('#icon_full').classList.remove('ion-arrow-expand')
                document.querySelector('#icon_full').classList.add('ion-arrow-shrink')
            }
            screenfull.toggle($('#simuladorHTML')[0]);
        }
    });

    // FORM DADOS INICIAIS     
    var forms = document.getElementsByClassName('needs-validation-form-init');
    var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
            event.preventDefault();
            event.stopPropagation();
            if (form.checkValidity()) {
                if (!validInput()) {
                    form.classList.add('was-validated');
                } else {
                    btnInicio(event);
                }
            } else {
                if (!validInput()) {
                    form.classList.add('was-validated');
                } else {
                    btnInicio(event);

                }
            }
        }, false);
    });
    
    $(".btnSimulacao").click(function (event) {
        $("#Modalgrid_simulacao").modal('hide');

        event.preventDefault();
        event.stopPropagation();

        let planoClicked = this.dataset.plano;
        $('.steps-selected').addClass('steps-selected-ok').removeClass('steps-selected');

        $.confirm({
            theme: 'modern',
            icon: 'fa fa-warning',
            title: 'Confirme!',
            content: `
                        <div class="form-group col-md-12">
                            <label class="col-md-12" for="inputBuyName" style="text-align: left;">NOME</label>
                            <input type="text" class="form-control form-control-not-focus form-input-data" id="inputBuyName" title="Nome" readonly>                           
                        </div>

                       <div class="form-group col-md-12">
                            <label class="col-md-12" for="inputBuyCpf" style="text-align: left;">CPF</label>
                            <input type="text" class="form-control form-input-data" id="inputBuyCpf" title="Campo Vazio!!!" placeholder="CPF do Cliente." required="">
                            <div class="invalid-feedback">
                                Preencha o seu CPF.
                            </div>
                        </div>
    
                     
            `  ,
            type: 'orange',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Confirmar',
                    btnClass: 'btn-blue',
                    keys: ['enter', 'shift'],
                    action: function (event1) {

                        if (!validBuyInput()) {
                            return false;
                        }

                        let dados = fillInData();

                        $('.modal-loader, .modal-content-loader').show();

                        $.ajax({
                            type: 'POST',
                            url: '/Simulador/CompreAgora',
                            headers: {
                                'Content-Type': 'application/json;charset=UTF-8',
                                'Access-Control-Allow-Origin': '*'
                            },
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            data: dados,
                            success: function (response) {

                                if (response.result.resultado.erro != null) {
                                    if (response.result.resultado.erro.indexOf("Já existe um usuário com o nome") != -1) {
                                        let emailArray = response.result.resultado.erro.split('-');
                                        let email = emailArray[1].slice(0, -1);
                                        myConfirm({
                                            url: window.location.origin + '/Account/Login',
                                            title: 'O usuário ' + email + ' já estava cadastrado!',
                                            msg: 'Click no botão abaixo para autenticar, também enviamos um e-mail, com os seus dados de acesso.',
                                            colorType: 'orange',
                                            colorBtn: 'btn-blue'
                                        });
                                    } else {
                                        let strArray = response.result.resultado.erro.split('-');
                                        let concatStr = strArray[0] + '</br>' + strArray[1]
                                        alertBottom(concatStr, 'erro');
                                    }
                                    $('.modal-loader, .modal-content-loader').hide();
                                } else {
                                    if (response.result.redirectUrl != null) {
                                        myConfirm({
                                            url: response.result.redirectUrl,
                                            title: 'Você está quase Lá!',
                                            msg: 'Click no botão abaixo para continuar sua contratação On-Line, também enviamos um e-mail.',
                                            colorType: 'green',
                                            colorBtn: 'btn-green'
                                        });
                                    } else {
                                        alertBottom('Ops! Algo deu Errado!', 'erro');
                                    }
                                }

                            },

                            error: function (error) {

                                let err = JSON.parse(error.responseText);
                                console.log(err); alertBottom('Desculpe! <br> Houve uma falha <br> de comunicação!', 'erro');
                                $('.modal-loader, .modal-content-loader').hide();

                            },
                            statusCode: {

                                404: function () {
                                    alert("page not found");
                                }
                            },
                            complete: function (data) {
                                $('.modal-loader, .modal-content-loader').hide();
                            }
                        });


                    }
                },
                cancel: {
                    text: 'Corrigir',
                    btnClass: 'btn-red',
                    keys: ['enter', 'shift'],
                    action: function () {
                    }
                }
            }
        });

        setTimeout(function () {

            //BuscarProfissao();
            $('#inputBuyName').val(resultSummary.nome);
            let $inputCpf = $("#inputBuyCpf");
            $inputCpf.mask('000.000.000-00', { placeholder: "___.___.___-__" });

        }, 500);

    });

    $(".collapse").on('show.bs.collapse', function () {
        $(window).scrollTop(0);
        $(this).parent().find(".txt-icon").text('-');
    }).on('hide.bs.collapse', function () {
        $(this).parent().find(".txt-icon").text('+');
    });

    $('#download-table-pdf').click( function (event) {
        update(true, 'multiple');
        event.preventDefault();
        event.stopPropagation();
    });

    //$(document).scroll(function (e) {
    //    if (element_in_scroll(".errors tbody tr:last")) {
    //        $(document).unbind('scroll');
    //        $.ajax({
    //            type: "POST",
    //            url: document.location.href,
    //            data: { text_filter: $('#text_filter').attr('value'), index_count: $('#index_count').attr('value'), json: "true" }
    //        }).done(function (msg) {
    //            $(".errors tbody ").append(msg.html);
    //            $('#index_count').attr('value', msg.index_count);
    //            if (msg.count != 0) {
    //                $(document).scroll(function (e) {
    //                    //callback to the method to check if the user scrolled to the last element of your list/feed 
    //                })
    //            }
    //        });
    //    };
    //});

    // FUNCTIONS

    //function element_in_scroll(elem) {
    //    var docViewTop = $(window).scrollTop();
    //    var docViewBottom = docViewTop + $(window).height();

    //    var elemTop = $(elem).offset().top;
    //    var elemBottom = elemTop + $(elem).height();

    //    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    //}

    function createElementIframe(params) {

            if(document.querySelector('#external-iframe')){
                document.querySelector('#external-iframe').remove();
            }

            const iframe = document.createElement('iframe');
        
            iframe.setAttribute('id', 'external-iframe');
            iframe.setAttribute('src', params);
            iframe.setAttribute('frameborder', 'no');
            iframe.style.position = 'absolute';
            iframe.style.top = '0';
            iframe.style.right = '0';
            iframe.style.bottom = '0';
            iframe.style.left = '0';
            iframe.style.width = '100%';
            iframe.style.height = '100%';
        
            $(document.body).prepend(iframe);
            document.body.style.overflow = 'hidden';        
    }

    function update(shouldDownload, funcStr) {
        if(funcStr != 'init'){

            let obj = JSON.parse(fillInData());
            obj.DataNascimento = fnDataToBr(obj.DataNascimento);
            let state = resultEstado.filter((o) => { return o.id == obj.Estado });
            obj.Estado = state[0].descricao;

            var doc = pdf[funcStr](obj);

            doc.setProperties({
                title: 'Simulação ' + funcStr,
                subject: 'Sistema de simulação Rede Evida (' + funcStr + ')'
            });

            if (shouldDownload) {
                doc.save('tabela_simulacao_rede_vida.pdf');
            } else {
                createElementIframe(doc.output('datauristring'));
            }
        }else{
            screenfull.exit();
        }
    }

    function myConfirm(obj) {
        obj.url = obj.url || 'https://portalmega.net';
        $.confirm({
            theme: 'modern',
            icon: 'fa fa-check',
            title: obj.title,
            content: obj.msg,
            closeAnimation: 'rotate',
            type: obj.colorType,
            typeAnimated: true,
            animationSpeed: 1000,
            buttons: {
                continuar: {
                    btnClass: obj.colorBtn,
                    action: function () {
                        window.open(obj.url, '_blank');
                    }
                }
            }
        });
    }

    function validBuyInput() {
        let objValidInput = {
            cpf: { validated: false, msg: "Informe o nº do cpf completo!" }
            //,profession: { validated: false, msg: "Selecione uma profissão!" }
        };
        if ($('#inputBuyCpf').val().length == 14) {
            objValidInput.cpf.validated = true;
            $("#inputBuyCpf + div.invalid-feedback").css("display", "");
            $("#inputBuyCpf").css("background-color", "");
        } else {
            $("#inputBuyCpf + div.invalid-feedback").css("display", "block");
            $("#inputBuyCpf").css("cssText", "background-color: #dc354514! important;");
            $("#inputBuyCpf + div.invalid-feedback").text(objValidInput.cpf.msg);
        }
        //if ($('#selectProfession').val() != '') {
        //    objValidInput.profession.validated = true;
        //    $("#selectProfession + div.invalid-feedback").css("display", "");
        //    $("#selectProfession").css("background-color", "");
        //} else {
        //    $("#selectProfession + div.invalid-feedback").css("display", "block");
        //    $("#selectProfession").css("cssText", "background-color: #dc354514! important;");
        //    $("#selectProfession + div.invalid-feedback").text(objValidInput.profession.msg);
        //}        

        for (var key in objValidInput) {
            if (objValidInput.hasOwnProperty(key)) {
                if (!objValidInput[key].validated) {
                    return objValidInput[key].validated;
                }
            }
        }

        return true;
    }

    function fnSelectedFilterPlansSegment(selected, result) {
        let arrayPlanos = [];
        let objCategory = [];

        switch (selected) {
            case "0":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[0].value });
                arrayPlanos = objCategory.slice();
                break;
            case "1":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[1].value });
                arrayPlanos = objCategory.slice();
                break;

            case "2":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[2].value });
                arrayPlanos = objCategory.slice();
                break;

            case "3":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[3].value });
                arrayPlanos = objCategory.slice();
                break;

            case "4":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[4].value });
                arrayPlanos = objCategory.slice();
                break;

            case "5":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[5].value });
                arrayPlanos = objCategory.slice();
                break;

            case "6":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[6].value });
                arrayPlanos = objCategory.slice();
                break;

            case "7":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[7].value });
                arrayPlanos = objCategory.slice();
                break;

            case "8":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[8].value });
                arrayPlanos = objCategory.slice();
                break;

            case "9":
                objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[9].value });
                arrayPlanos = objCategory.slice();
                break;

            //case "10":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[10].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            //case "11":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[11].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            //case "12":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[12].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            //case "13":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[13].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            //case "14":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[14].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            //case "15":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[15].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            //case "99":
            //    objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == segmentacaoAssistencial[99].value });
            //    arrayPlanos = objCategory.slice();
            //    break;

            default:
                arrayPlanos = result.slice();
                break;
        }

        return arrayPlanos;
    }

    function fnFilterCategory(search) {
        let arrayPlanos = [];

        arrayPlanos = fnSelectedFilterPlansSegment($('#selecione-categoriaPlans').val(), resultPlans);

        //if ($('#selecione-categoriaPlans').val() == 3) {
        //    arrayPlanos = resultPlans.slice();
        //} else if ($('#selecione-categoriaPlans').val() == 1) {
        //    let objCategory = resultPlans.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == 1 });
        //    arrayPlanos = objCategory.slice();
        //} else if ($('#selecione-categoriaPlans').val() != 1) {
        //    let objCategory = resultPlans.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial != 1 });
        //    arrayPlanos = objCategory.slice();
        //}

        result = arrayPlanos.slice();

        //let arrayPlanos = resultPlans.slice();

        let arrayObj = result.filter((obj, index, array) => {
            if ((stripVowelAccent(obj.operadoraNome).toUpperCase()).indexOf(stripVowelAccent(search).toUpperCase()) > -1) {
                return true;
            } else if ((stripVowelAccent(obj.nome).toUpperCase()).indexOf(stripVowelAccent(search).toUpperCase()) > -1) {
                return true;
            }
        });

        loadPlanos(arrayObj);
    }

    function alertBottom(msg, tipo = null) {
        var x = document.getElementById("snackbar");
        x.innerHTML = msg;
        x.className = "show";

        switch (tipo) {
            case 'confirm': x.style.backgroundColor = '#2cc14eeb';
                break;
            case 'attention': x.style.backgroundColor = '#ffca2b';
                break;
            case 'erro': x.style.backgroundColor = '#d20014d6';
                break;
            default: x.style.backgroundColor = '#333';
                break;
        }

        setTimeout(function () { x.className = x.className.replace("show", ""); }, 5000);
    }

    function fnDataInfo(params) {
        $('#pre').attr('data-count', params);
    }

    function fnAddHtmlDependente(e = null, callback) {
        if (e != null) {
            e.preventDefault();
        }
        let startNumber = 1;
        let count = startNumber + (parseInt($('#pre').attr('data-count')) || 0);
        //     let countOld = $('#pre').attr('data-count');
        //    $(`#labelNameOrdeBy-${countOld}`).html('DEPENDENTE ' + '<i><b>' + $(`#inputNome-${countOld}`).val() + '</i></b>');
        fnDataInfo(count);
        $('#showSalvarInit').before(`   
                <div class="col-lg-12 dependenteAll" id="dependente-${count}">
                                <div>
                                    <form>
                                            <div class="form-row">
                                            <div class="form-group col-12" style="margin-bottom: auto;">
                                            

                                            <div class="custom-control custom-checkbox" style="margin-bottom: 2rem;">
                                                    <input type="checkbox" class="custom-control-input checkDependente" id="customCheck1${count}" data-dep="dependente-${count}" readonly>
                                                    <label id="labelNameOrdeBy-${count}" class="custom-control-label" for="customCheck1${count}">Dependente</label>
                                            </div>

                                            </div>

                                                <div class="form-group col-md-6">
                                                    <label for="inputDependenteNome-${count}">Nome completo</label>
                                                    <input type="text" class="form-control inputDependenteNome form-input-data" id="inputDependenteNome-${count}" maxlength="100" placeholder="Nome completo do Cliente." required>
                                                    <div class="invalid-feedback">
                                                        Informe seu nome completo.
                                                    </div>
                                                </div>    
                                                

                                                <div class="form-group col-md-6">
                                                    <label for="inputDependenteDate-${count}">Data de nascimento</label>
                                                    <input type="date" class="form-control inputDependenteDate form-input-data" id="inputDependenteDate-${count}" placeholder="Data de Nascimento do Cliente." required>
                                                    <div class="invalid-feedback">
                                                        Informe sua data de nascimento.
                                                    </div>
                                                </div>
                                                                                                
                                            </div>                                                        
                                    </form>
                                </div>
                                <hr class="style18"> 
                            </div>

                   
        
        ` );

        setTimeout(function () {
            let $inputDependenteCpf = $(".inputDependenteCpf");
            $inputDependenteCpf.mask('000.000.000-00', { placeholder: "___.___.___-__" });

            $(".inputDependenteNome").bind("blur", function () {
                $('#labelNameOrdeBy-' + this.id.split('-')[1]).html('Dependente' + ': <i><b>' + this.value + '</i></b>');
            });

            $('input:not(input[type="checkbox"]), select').bind('change', function (e) {
                alterFields(this);
            });

        }, 1000);

        if (callback) {
            callback(count);
        }
    }

    function alterFields(element) {
        if (element.id != 'searching') {
            isNav = false, isNavFinaly = false;
        }
    }

    function loadStep(nav) {
        step = $('div#step-1');
        step.show();
        var chk = document.getElementById("faq-titulo-1");
        if (!chk.checked) {
            chk.checked = true;
        }
        let item = $('#navSteo-1');
        nav.removeClass('steps-selected');
        item.addClass('steps-selected');

        $('input[type=text]:eq(0)').focus();
    }

    function loadPlanos(arrayPlanos = []) {
        $('.cards').remove();        

        if (arrayPlanos.length == 0) {
            $('#notPlans').show();
            $('.two-card-accordion').hide();
            $('.card-accordion').hide();
            return false;
        } else {
            $('#notPlans').hide();
            $('.two-card-accordion').show();
            $('.card-accordion').show();
        }

        if (arrayPlanos.length == 0)
            $('.two-card-accordion').hide();
        else
            $('.two-card-accordion').show();


        var contOne = 0;
        
        window.dataPlanos = arrayPlanos;

        arrayPlanos.forEach((v, i, arr) => {

            let val = v.valor.toLocaleString('pt-BR');

            if (val.indexOf(',') > -1 && val[val.indexOf(',') + 2] == undefined) {
                val = val + '0'
            } else if (val.length == 3 || (val.length == 5 && val.indexOf(',') == -1)) {
                val = val + ',00'
            }            
            
            //console.log(val);
            if (url_query('plan')) {
                
                let queryUrl = decodeURIComponent(url_query('plan')).toUpperCase();

                if ($('#btn-accordion').text().trim() == '+' || $('#btn-accordion').text().trim() == '-')
                    $('#btn-accordion').append(decodeURIComponent(url_query('sigla')));

                if (v.nome.toUpperCase().indexOf(queryUrl) > -1) {

                    contOne++;

                    $('#collapseOne .card-accordion-body').append(`
                    <div class="cards col-2 mb-4" style="display: inline-table; margin: inherit;">
                    <div class="card">
                            <img class="card-img-top custom-card myImgModal" style="height: 118px;"
                                src="${'http://' + window.location.host + window.getImg(v.operadoraNome)}" 
                                alt="${v.operadoraNome}"
                                data-operadoraNome="${v.operadoraNome}"
                                data-adm="${v.administradoraNomePessoa}"
                                data-abrangencia="${abrangencia[v.planoDeSaude.abrangencia]}"
                                data-acomodacao="${acomodacao[v.planoDeSaude.acomodacao]}"
                                data-segmentacao="${v.planoDeSaude.segmentacaoAssistencial}"
                                data-valor="R$ ${val}"
                                data-vl="R$ ${val}"
                                data-info="${v.nome}"
                                data-idPlano="${v.id}"
                                data-link="${v.linkRedeCredenciada}">

                            <div class="card-body myCardBody" data-mycardplanoid="${v.id}">
                                <p class="card-title">${v.nome}</p>
                                <div style="display:none;">${v.descricao}</div> 
                                <h5 class="card-title" style="font-weight: 800;">R$ ${val}</h5>
                            </div>
                            <div class="card-body" style="padding-top: 0px;">
                                <button class="btn form-input-btn-details myModal" data-planoid="${v.id}">Mais detalhes</button>
                            </div>
                            <div class="card-footer" style="padding: 0px; background-color: #dde3ec;border: none; margin-bottom: 5px;">
                                <button class="btn btn-primary btnSelecaoPlano form-input-btn" style="background-color: #007bff !important; width: 90%;" data-indexid="${v.id}">Contrate online</button>
                            </div>
                        </div>
                    </div>
                  `);

                } else {

                    $('#collapseTwo .card-accordion-body').append(`
                    <div class="cards col-2 mb-4" style="display: inline-table; margin: inherit;">
                    <div class="card">
                            <img class="card-img-top custom-card myImgModal" style="height: 118px;"
                                src="${'http://' + window.location.host + window.getImg(v.operadoraNome)}" 
                                alt="${v.operadoraNome}"
                                data-operadoraNome="${v.operadoraNome}"
                                data-adm="${v.administradoraNomePessoa}"
                                data-abrangencia="${abrangencia[v.planoDeSaude.abrangencia]}"
                                data-acomodacao="${acomodacao[v.planoDeSaude.acomodacao]}"
                                data-segmentacao="${v.planoDeSaude.segmentacaoAssistencial}"
                                data-valor="R$ ${val}"
                                data-vl="R$ ${val}"
                                data-info="${v.nome}"
                                data-idPlano="${v.id}"
                                data-link="${v.linkRedeCredenciada}">

                            <div class="card-body myCardBody" data-mycardplanoid="${v.id}">
                                <p class="card-title">${v.nome}</p>
                                <div style="display:none;">${v.descricao}</div> 
                                <h5 class="card-title" style="font-weight: 800;">R$ ${val}</h5>
                            </div>
                            <div class="card-body" style="padding-top: 0px;">
                                <button class="btn form-input-btn-details myModal" data-planoid="${v.id}">Mais detalhes</button>
                            </div>
                            <div class="card-footer" style="padding: 0px; background-color: #dde3ec;border: none; margin-bottom: 5px;">
                                <button class="btn btn-primary btnSelecaoPlano form-input-btn" style="background-color: #007bff !important; width: 90%;" data-indexid="${v.id}">Contrate online</button>
                            </div>
                        </div>
                    </div>
                    `);
                }

                if (arr.length == 1) {
                    if (v.nome.toUpperCase().includes(queryUrl) && queryUrl.trim() != '') {
                        if (!$('#collapseOne').attr('class').includes('show')) {
                            $('#collapseOne').attr('class', 'collapse show');
                            $('#collapseTwo').removeClass('show');
                            $(window).scrollTop(0);
                            $('.accordion-1').text('-');
                            $('.accordion-2').text('+');
                        } 
                    } else {
                        if (!$('#collapseTwo').attr('class').includes('show')) {
                            $('#collapseTwo').attr('class', 'collapse show');
                            $('#collapseOne').removeClass('show');
                            $('.accordion-2').text('-');
                            $('.accordion-1').text('+');
                        }
                    }
                }              


            } else {

                $('#accordionExample').remove();

                $('.row .text-center').append(`
                    <div class="cards col-2 mb-4" style="display: inline-table; margin: inherit;">
                    <div class="card">
                            <img class="card-img-top custom-card myImgModal" style="height: 118px;"
                                src="${'http://' + window.location.host + window.getImg(v.operadoraNome)}" 
                                alt="${v.operadoraNome}"
                                data-operadoraNome="${v.operadoraNome}"
                                data-adm="${v.administradoraNomePessoa}"
                                data-abrangencia="${abrangencia[v.planoDeSaude.abrangencia]}"
                                data-acomodacao="${acomodacao[v.planoDeSaude.acomodacao]}"
                                data-segmentacao="${v.planoDeSaude.segmentacaoAssistencial}"
                                data-valor="R$ ${val}"
                                data-vl="R$ ${val}"
                                data-info="${v.nome}"
                                data-idPlano="${v.id}"
                                data-link="${v.linkRedeCredenciada}">

                            <div class="card-body myCardBody" data-mycardplanoid="${v.id}">
                                <p class="card-title">${v.nome}</p>
                                <div style="display:none;">${v.descricao}</div> 
                                <h5 class="card-title" style="font-weight: 800;">R$ ${val}</h5>
                            </div>
                            <div class="card-body" style="padding-top: 0px;">
                                <button class="btn form-input-btn-details myModal" data-planoid="${v.id}">Mais detalhes</button>
                            </div>
                            <div class="card-footer" style="padding: 0px; background-color: #dde3ec;border: none; margin-bottom: 5px;">
                                <button class="btn btn-primary btnSelecaoPlano form-input-btn" style="background-color: #007bff !important; width: 90%;" data-indexid="${v.id}">Contrate online</button>
                            </div>
                        </div>
                    </div>
                    `);
            }          

            // if (url_query('corretor')) {
            //     $('.card-footer').hide();
            // }

            let $itemPlano = document.querySelectorAll('.card');

            $($itemPlano).mouseover(function (item) {

                $(item.currentTarget).addClass('card-active');
            });

            $($itemPlano).mouseout(function () {
                $itemPlano.forEach(function (item) {
                    if (item.classList.contains('card-active')) {
                        $(item).removeClass('card-active');
                    }
                });
            });

        });

        // setTimeout(() => {
        //     let retorno = window.arrayImg.slice();
        //     window.arrayImg = [];
        //     arrayPlanos.forEach(p => {
        //         retorno.forEach(pImg => {
        //             if(p.operadora.nome == pImg.op){
        //                 window.arrayImg.push({
        //                     imagem: pImg.imagem,
        //                     op: pImg.op
        //                 });
        //             }
        //         });
        //     });
        // }, 500);        

        if (contOne == 0) {
            $('#collapseTwo').attr('class', 'collapse show');
            $('#collapseOne').removeClass('show');
        }
        else {
            $('#collapseOne').attr('class', 'collapse show');
            $('#collapseTwo').removeClass('show');
        }


        $(".btnSelecaoPlano").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            $('.steps-selected').addClass('steps-selected-ok').removeClass('steps-selected');
            // $('.steps-step-2').each(function name(index) {
            //     if (this.dataset.step == '#step-3') {
            //         this.classList.add('steps-selected');

            //         $('.setup-content-2').hide();
            //         $(this.dataset.step).show();

            //         var chk = document.getElementById("faq-titulo-2");
            //         if (chk.checked) {
            //             chk.checked = false;
            //         }
            //         var chk = document.getElementById("faq-titulo-3");
            //         if (!chk.checked) {
            //             chk.checked = true;
            //         }
            //         isNavFinaly = true;
            //     }
            // });

            let obj_plans = {};
            let el = $(`*[data-idPlano="${this.dataset.indexid}"]`)[0];
            _segmentacaoAssistencial = segmentacaoAssistencial.filter(v => v.value == el.dataset['segmentacao'])[0].descricao;

            obj_plans.id = el.dataset['idplano'];
            obj_plans.nome = el.dataset['info'];
            //obj_plans.DescricaoPlano = el.dataset['descricao'];            
            obj_plans.DescricaoPlano = $(`*[data-mycardplanoid="${this.dataset.indexid}"]`).clone().find('div')[0].innerText;
            obj_plans.NomeOperadora = el.dataset['operadoranome'];
            obj_plans.NomeAdministradora = el.dataset['adm'];
            obj_plans.Abrangencia = el.dataset['abrangencia'];
            obj_plans.Acomodacao = el.dataset['acomodacao'];
            obj_plans.ValorTotal = el.dataset['vl'];

            let dados = fillInData(obj_plans);
            ResumoSimulador(dados);
        });

        $(".myModal").click(function (event) {
            event.preventDefault();
            event.stopPropagation();
            let el = $(`*[data-idPlano="${this.dataset.planoid}"]`)[0];
            let elCopy = $(`*[data-mycardplanoid="${this.dataset.planoid}"]`).clone().find('div')[0];
            elCopy.style.display = 'block';
            //let el = $('.myImgModal')[index];
            let modal = $("#Modalgrid").modal('show');
            modal.find('.modal-logo').attr('src', el.src);
            $('.modal-adm').text(el.dataset.adm);
            $('.modal-abrangencia').text(el.dataset.abrangencia);
            $('.modal-acomodacao').text(el.dataset.acomodacao);
            $('.modal-valor').text(el.dataset.valor);
            $('.modal-segmentacao-assistencial').text(segmentacaoAssistencial.filter(v => v.value == el.dataset.segmentacao)[0].descricao);
            $('.modal-info').html(elCopy);
            $('.modal-link').attr("href", el.dataset.link);
        });

    }

    function stripVowelAccent(str) {
        var rExps = [
            { re: /[\xC0-\xC6]/g, ch: 'A' },
            { re: /[\xE0-\xE6]/g, ch: 'a' },
            { re: /[\xC8-\xCB]/g, ch: 'E' },
            { re: /[\xE8-\xEB]/g, ch: 'e' },
            { re: /[\xCC-\xCF]/g, ch: 'I' },
            { re: /[\xEC-\xEF]/g, ch: 'i' },
            { re: /[\xD2-\xD6]/g, ch: 'O' },
            { re: /[\xF2-\xF6]/g, ch: 'o' },
            { re: /[\xD9-\xDC]/g, ch: 'U' },
            { re: /[\xF9-\xFC]/g, ch: 'u' },
            { re: /[\xD1]/g, ch: 'N' },
            { re: /[\xF1]/g, ch: 'n' }
        ];

        for (var i = 0, len = rExps.length; i < len; i++)
            str = str.replace(rExps[i].re, rExps[i].ch);

        return str;
    }

    function btnInicio(event) {
        if (event.currentTarget.elements.saveInit) {
            BuscarPlanos();
        }
    }

    function validInput() {
        let objValidInput = {
            cpf: { validated: true, msg: "Informe o nº do cpf completo!" },
            date: { validated: false, msg: "Informe a data completo: dd/mm/aaaa" },
            name: { validated: false, msg: "Informe o nome completo!" },
            tel: { validated: false, msg: "Informe o nº do telefone completo!" }
        };
        //if ($('#inputCpf').val().length == 14) {
        //    objValidInput.cpf.validated = true;
        //    $("#inputCpf + div.invalid-feedback").css("display", "");
        //    $("#inputCpf").css("background-color", "");
        //} else {
        //    $("#inputCpf + div.invalid-feedback").css("display", "block");
        //    $("#inputCpf").css("background-color", "#dc354514");
        //    $("#inputCpf + div.invalid-feedback").text(objValidInput.cpf.msg);
        //}
        if ($('#inputData').val().length == 10) {
            objValidInput.date.validated = true;
            $("#inputData + div.invalid-feedback").css("display", "");
            $("#inputData").css("background-color", "");
        } else {
            $("#inputData + div.invalid-feedback").css("display", "block");
            $("#inputData").css("background-color", "#dc354514");
            $("#inputData + invalid-feedback").text(objValidInput.date.msg);
        }
        if ($('#inputNome').val().length > 5) {
            objValidInput.name.validated = true;
            $("#inputNome + div.invalid-feedback").css("display", "");
            $("#inputNome").css("background-color", "");
        } else {
            $("#inputNome + div.invalid-feedback").css("display", "block");
            $("#inputNome").css("background-color", "#dc354514");
            $("#inputNome + invalid-feedback").text(objValidInput.name.msg);
        }
        if ($('#inputTel').val().length > 13) {
            objValidInput.tel.validated = true;
            $("#inputTel + div.invalid-feedback").css("display", "");
            $("#inputTel").css("background-color", "");
        } else {
            $("#inputTel + div.invalid-feedback").css("display", "block");
            $("#inputTel").css("background-color", "#dc354514");
            $("#inputTel + div.invalid-feedback").text(objValidInput.tel.msg)
        }

        for (var key in objValidInput) {
            if (objValidInput.hasOwnProperty(key)) {
                if (!objValidInput[key].validated) {
                    return objValidInput[key].validated;
                }
            }
        }

        if (document.getElementById('customCheck2').checked) {

            let isArrayDependentes = true;

            $('.inputDependenteNome').each(function (index, element) {

                if (element.value == '' || $('.inputDependenteDate')[index].value == '') {
                    isArrayDependentes = false;
                }

            });

            return isArrayDependentes;
        }

        return true;
    }

    function fillInData(inputs = null) {

        let Dependentes = [];

        $('.inputDependenteNome').each(function (index, element) {

            let obj = {
                "Nome": element.value,
                //"Cpf": $('.inputDependenteCpf')[index].value,
                "DataNascimento": $('.inputDependenteDate')[index].value
            }

            Dependentes.push(obj);

        });

        obj = {
            "Empresa": null,
            "Nome": $('#inputNome').val(),
            "Cpf": $('#inputBuyCpf').val(),
            "DataNascimento": $('#inputData').val(),
            "Email": $('#inputEmail').val(),
            "Celular": $('#inputTel').val(),
            "Estado": $('#selectState').val(),
            "Profissao": $('#selectProfession').val(),
            "TitularResponsavel": document.getElementById('customCheck1').checked,
            Dependentes,
            "idPlano": inputs != null ? inputs.id : 0,
            "NomePlano": inputs != null ? inputs.nome : null,
            "DescricaoPlano": inputs != null ? inputs.DescricaoPlano : null,
            "NomeOperadora": inputs != null ? inputs.NomeOperadora : null,
            "NomeAdministradora": inputs != null ? inputs.NomeAdministradora : null,
            "Abrangencia": inputs != null ? inputs.Abrangencia : null,
            "Acomodacao": inputs != null ? inputs.Acomodacao : null,
            "ValorTotal": inputs != null ? inputs.ValorTotal : null,
            "PossuiDependentes": document.getElementById('customCheck2').checked,
            "DataSimulacao": "0001-01-01T00:00:00",
            "Chave": idSimulacao,
            "QtdDependentes": Dependentes.length,
            "idProposta": null,
            "RedirectUrl": null,
            "resultado": null
        }

        return JSON.stringify(obj);

    }

    function BuscarPlanos() {
        let dados = fillInData();
        $('.modal-loader, .modal-content-loader').show();
        $.ajax({
            type: 'POST',
            url: '/Simulador/BuscarPlanos',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Access-Control-Allow-Origin': '*'
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: dados,
            success: function (response) {

                if (response.result.listaProdutosComValor) {
                    if (response.result.listaProdutosComValor.length > 0) {

                        //if (event != null) {
                        //    if (event.currentTarget.elements.saveInit) {
                        $('.steps-selected').addClass('steps-selected-ok').removeClass('steps-selected');
                        $('.steps-step-2').each(function name(index) {
                            if (this.dataset.step == '#step-2') {
                                this.classList.add('steps-selected');

                                $('.setup-content-2').hide();
                                $(this.dataset.step).show();

                                var chk = document.getElementById("faq-titulo-1");
                                if (chk.checked) {
                                    chk.checked = false;
                                }
                                var chk = document.getElementById("faq-titulo-2");
                                if (!chk.checked) {
                                    chk.checked = true;
                                }
                                //let arrayPlanos = [];
                                //    arrayPlanos.push(
                                //        { title: 'Amex', src: 'libs/simulador/img/amex.png', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.', id: 'plano-1'},
                                //        { title: 'Ônix', src: 'libs/simulador/img/onix.png', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.', id: 'plano-2'},
                                //        { title: 'Salutar', src: 'libs/simulador/img/salutar.png', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.', id: 'plano-3'},
                                //        { title: 'Caberj', src: 'libs/simulador/img/caberj.png', text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente esse necessitatibus neque.', id: 'plano-4'}
                                //    );       
                                //loadPlanos(arrayPlanos); 
                                isNav = true;
                            }
                        });
                        //    }
                        //}

                        idSimulacao = response.result.simulacao.idSimulacao;
                        let result = response.result.listaProdutosComValor;
                        resultPlans = result.slice();
                        let arrayPlanos = [];

                        arrayPlanos = fnSelectedFilterPlansSegment($('#selecione-categoriaInit').val(), result);

                        //if ($('#selecione-categoriaInit').val() == 3) {
                        //    arrayPlanos = result.slice();
                        //} else if ($('#selecione-categoriaInit').val() == 1) {
                        //    let objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial == 1 });
                        //    arrayPlanos = objCategory.slice();
                        //} else if ($('#selecione-categoriaInit').val() != 1) {
                        //    let objCategory = result.filter((obj, index, array) => { return obj.planoDeSaude.segmentacaoAssistencial != 1 });
                        //    arrayPlanos = objCategory.slice();
                        //}

                        //document.getElementById('selecione-categoriaPlans').value = $('#selecione-categoriaInit').val();
                        document.getElementById('searching').value = '';
                        loadPlanos(arrayPlanos);
                    } else {
                        alertBottom('Desculpe! <br> Sua simulação retornou vazia <br> tente novamente <br> com outras informações!');
                    }
                } else {
                    location.reload();
                }

            },
            error: function (error) {
                let err = JSON.parse(error.responseText);
                console.log(err); alertBottom('Desculpe! <br> Houve uma falha <br> de comunicação!', 'erro');
                $('.modal-loader, .modal-content-loader').hide();
            },
            statusCode: {
                404: function () {
                    alert("page not found");
                }
            },
            complete: function (data) {
                $('.modal-loader, .modal-content-loader').hide();
            }
        });


    }

    function ResumoSimulador(dados) {
        $('.modal-loader, .modal-content-loader').show();
        $.ajax({
            type: 'POST',
            url: '/Simulador/ResumoSimulador',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Access-Control-Allow-Origin': '*'
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            data: dados,
            success: function (response) {
                if (response.result != null) {
                    //var oTable = $('#tableResumo').dataTable();
                    //oTable.fnDestroy();
                    resultSummary = response.result;
                    dataSet = [];
                    dataSet.push([response.result.chave, response.result.nome, response.result.qtdDependentes, response.result.nomePlano, response.result.valorTotal, '']);
                    //loadTable(true);
                    CompreAgora();
                } else {
                    alertBottom('Desculpe! <br> Retornou vazio <br> tente novamente <br> mais tarde!');
                }
            },
            error: function (error) {
                let err = JSON.parse(error.responseText);
                console.log(err); alertBottom('Desculpe! <br> Houve uma falha <br> de comunicação!', 'erro');
                $('.modal-loader, .modal-content-loader').hide();
            },
            statusCode: {
                404: function () {
                    alert("page not found");
                }
            },
            complete: function (data) {
                $('.modal-loader, .modal-content-loader').hide();
            }
        });
    }

    function CompreAgora() {
        $.confirm({
            theme: 'modern',
            icon: 'fa fa-warning',
            title: 'Confirme!',
            content: `
                        <div class="form-group col-md-12">
                            <label class="col-md-12" for="inputBuyName" style="text-align: left;">NOME</label>
                            <input type="text" class="form-control form-control-not-focus form-input-data" id="inputBuyName" title="Nome" readonly>                           
                        </div>

                       <div class="form-group col-md-12">
                            <label class="col-md-12" for="inputBuyCpf" style="text-align: left;">CPF</label>
                            <input type="text" class="form-control form-input-data" id="inputBuyCpf" title="Campo Vazio!!!" placeholder="CPF do Cliente." required="">
                            <div class="invalid-feedback">
                                Preencha o seu CPF.
                            </div>
                        </div>
    
                     
            `  ,
            type: 'orange',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Confirmar',
                    btnClass: 'btn-blue',
                    keys: ['enter', 'shift'],
                    action: function (event1) {

                        if (!validBuyInput()) {
                            return false;
                        }

                        let dados = fillInData();

                        $('.modal-loader, .modal-content-loader').show();

                        $.ajax({
                            type: 'POST',
                            url: '/Simulador/CompreAgora',
                            headers: {
                                'Content-Type': 'application/json;charset=UTF-8',
                                'Access-Control-Allow-Origin': '*'
                            },
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            data: dados,
                            success: function (response) {

                                if (response.result.resultado.erro != null) {
                                    if (response.result.resultado.erro.indexOf("Já existe um usuário com o nome") != -1) {
                                        let emailArray = response.result.resultado.erro.split('-');
                                        let email = emailArray[1].slice(0, -1);
                                        myConfirm({
                                            url: window.location.origin + '/Account/Login',
                                            title: 'O usuário ' + email + ' já estava cadastrado!',
                                            msg: 'Click no botão abaixo para autenticar, também enviamos um e-mail, com os seus dados de acesso.',
                                            colorType: 'orange',
                                            colorBtn: 'btn-blue'
                                        });
                                    } else {
                                        let strArray = response.result.resultado.erro.split('-');
                                        let concatStr = strArray[0] + '</br>' + strArray[1]
                                        alertBottom(concatStr, 'erro');
                                    }
                                    $('.modal-loader, .modal-content-loader').hide();
                                } else {
                                    if (response.result.redirectUrl != null) {
                                        myConfirm({
                                            url: response.result.redirectUrl,
                                            title: 'Você está quase Lá!',
                                            msg: 'Click no botão abaixo para continuar sua contratação On-Line, também enviamos um e-mail.',
                                            colorType: 'green',
                                            colorBtn: 'btn-green'
                                        });
                                    } else {
                                        alertBottom('Ops! Algo deu Errado!', 'erro');
                                    }
                                }

                            },

                            error: function (error) {

                                let err = JSON.parse(error.responseText);
                                console.log(err); alertBottom('Desculpe! <br> Houve uma falha <br> de comunicação!', 'erro');
                                $('.modal-loader, .modal-content-loader').hide();

                            },
                            statusCode: {

                                404: function () {
                                    alert("page not found");
                                }
                            },
                            complete: function (data) {
                                $('.modal-loader, .modal-content-loader').hide();
                            }
                        });


                    }
                },
                cancel: {
                    text: 'Corrigir',
                    btnClass: 'btn-red',
                    keys: ['enter', 'shift'],
                    action: function () {
                    }
                }
            }
        });

        setTimeout(function () {
            $('#inputBuyName').val(resultSummary.nome);
            let $inputCpf = $("#inputBuyCpf");
            $inputCpf.mask('000.000.000-00', { placeholder: "___.___.___-__" });

        }, 500);
    }

    function BuscarProfissao() {
        $.ajax({
            type: 'POST',
            url: '/Simulador/BuscarProfissao',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Access-Control-Allow-Origin': '*'
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                resultProfissao = response.result;
                helpers.buildDropdown(
                    response.result,
                    $('#selectProfession'),
                    'Selecione uma profissão'
                );
                $('#selectProfession').val('42');
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function BuscarEstado() {
        $.ajax({
            type: 'POST',
            url: '/Simulador/BuscarEstado',
            headers: {
                'Content-Type': 'application/json;charset=UTF-8',
                'Access-Control-Allow-Origin': '*'
            },
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function (response) {
                resultEstado = response.result;
                helpers.buildDropdown(
                    response.result,
                    $('#selectState'),
                    'Selecione um Estado'
                );

                if (url_query('sigla')) {
                    let queryUrl = decodeURIComponent(url_query('sigla')).toUpperCase()
                    let Estado_fiter = resultEstado.filter(x => x.descricao.toUpperCase() == queryUrl);
                    $('#selectState').val(Estado_fiter[0].id);
                }

            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function formatReal(int) {
        var tmp = int + '';
        tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
        if (tmp.length > 6)
            tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

        return tmp;
    }

    function loadTable(isReload = false) {

        //dataSet = [
        //    ["000", "Tiger Nixon", "5421", "System Architect", "$320,800", "" ],
        //    ["001", "Garrett Winters", "8422", "Tokyo", "$170,750", ""  ],
        //    ["002", "Ashton Cox", "1562", "San Francisco", "$86,000", ""  ],
        //    ["003", "Cedric Kelly", "6224", "Edinburgh", "$433,060", ""  ]
        //];

        if (isReload || onyCreatDataTable) {
            onyCreatDataTable = false;
            var table = $('#tableResumo').DataTable({
                //dom: "Bfrtip",  
                "searching": false,
                "paging": false,
                "ordering": false,
                "info": false,
                scrollX: true,
                data: dataSet,
                language: {
                    search: "BUSCAR: "
                },
                "aoColumns": [{
                    "sWidth": "4%",
                    "mData": 0
                }, {
                    "sWidth": "20%",
                    "mData": 1
                }, {
                    "sWidth": "8%",
                    "mData": 2
                }, {
                    "sWidth": "20%",
                    "mData": 3
                }, {
                    "sWidth": "8%",
                    "mData": 4
                }, {
                    "sWidth": "2%",
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (data, type, full) {
                        return '<a class="btn btn-info btn-sm btnEdit" data-row="' + full + '" href=#/' + full[0] + '>' + 'Resumo' + '</a>';
                    }
                }]
            });

            //table.buttons().container()
            //    .appendTo( '#example_wrapper .col-md-6:eq(0)' );

            //table.on( 'select', function ( e, dt, type, indexes ) {
            //    var rowData = table.rows( indexes ).data().toArray();
            //    $("#Modalgrid_simulacao").modal('show');                
            //    //events.prepend( '<div><b>'+type+' selection</b> - '+JSON.stringify( rowData )+'</div>' );
            //})

            //.on( 'deselect', function ( e, dt, type, indexes ) {
            //    var rowData = table.rows( indexes ).data().toArray();                $ 
            //    //events.prepend( '<div><b>'+type+' <i>de</i>selection</b> - '+JSON.stringify( rowData )+'</div>' );
            //});           

        }

        $('.btnEdit').click(function () {
            //let str = this.dataset.row.slice(0, -1);
            //let arrayRow = splitString(str, ',');
            //arrayRow.push(arrayRow.slice(4, 6).join(','));
            //arrayRow.splice(5, 1);
            //arrayRow.splice(4, 1);
            $("#Modalgrid_simulacao").modal('show');
            //console.log(arrayRow);
            //console.log(resultSummary);
            $('.contentDynamic').remove();
            $("#Modalgrid_simulacao .gridmodal #simulacaoModal").before(`               

                    <div class="row contentDynamic">

                        <div class="media">
                            <!-- <img class="mr-3" src="~/App/common/views/modals/detalhesProdutoDePlanoDeSaude/img/administradora-icon.png" style="height: 35px;"> -->
                            <i class="ion-ios-list-outline" style="font-size: 180%;margin-top: -7px;margin-right: 20px;"></i>
                            <div class="media-body">
                                <span>Dados do Titular</span>
                                <p></p>
                            </div>
                        </div>

                        <div class="col-lg-12 contentSimulation">
                                <div class="form-row">

                                    <div class="form-group-not-margin-bottom col-md-6">                                       
                                            <span>Nome do titular: </span>
                                            <label  id="resumoNomeTitular"></label>
                                    </div>

                                    <div class="form-group-not-margin-bottom col-md-4">                                       
                                            <span>Data de nascimento: </span>
                                            <label  id="resumoDataNascimento"></label>
                                    </div>                                    

                                    <div class="form-group-not-margin-bottom col-md-6">                                       
                                            <span>Celular: </span>
                                            <label id="resumoCelular"></label>
                                    </div>

                                    <div class="form-group-not-margin-bottom col-md-6">                                       
                                            <span>Estado: </span>
                                            <label id="resumoEstado"></label>
                                    </div>                                    

                                    <div class="form-group-not-margin-bottom col-md-6">                                       
                                            <span>E-mail: </span>
                                            <label id="resumoEmail"></label>
                                    </div>

                                </div>
                        </div>                    


                    </div>

            `);

            if (resultSummary.dependentes != null) {
                resultSummary.dependentes.forEach((el, i) => {
                    $("#dependenteRow").show();
                    $('#modal-content-dependente').append(`              
                

                 <div class="form-row contentDynamic" style="padding: margin-bottom: 5px;">

                        <div class="form-group-not-margin-bottom col-md-6">                                       
                                <span>Nome: </span>
                                <label  id="">${el.nome}</label>
                        </div>

                        <div class="form-group-not-margin-bottom col-md-6">                                       
                                <span>Data de nascimento: </span>
                                <label  id="">${fnDataToBr(el.dataNascimento)}</label>
                        </div>                         

                 </div> 

                `);

                });
            } else {
                $('#modal-content-dependente').empty();
                $("#dependenteRow").hide();
            }

            $("#Modalgrid_simulacao .gridmodal #modal-content-plano").append(`               

                <div class="form-row contentDynamic">

                     <div class="col-lg-4">                        

                        <div class="media">
                            <i class="fa fa-medkit circle-yes mr-3" style="line-height: 24px; width: 27px; height: 27px; font-size: medium;"></i>
                            <div class="media-body">
                                <span>Administradora</span>
                                <p class="modal-resumo-adm"></p>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4">
                        
                        <div class="media">
                            <i class="fa fa-location-arrow circle-yes mr-3" style="line-height: 24px; width: 27px; height: 27px; font-size: medium;"></i>
                            <div class="media-body">
                                <span>Abrangência</span>
                                <p class="modal-resumo-abrangencia"></p>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-4">                        

                        <div class="media">
                            <i class="fa fa-hospital-o circle-yes mr-3" style="line-height: 24px; width: 27px; height: 27px; font-size: medium;"></i>
                            <div class="media-body">
                                <span>Acomodação</span>
                                <p class="modal-resumo-acomodacao"></p>
                            </div>
                        </div>

                    </div>                    

                    <div class="col-lg-4">                        

                        <div class="media">
                            <i class="fa fa-money circle-yes mr-3" style="line-height: 24px; width: 27px; height: 27px; font-size: medium;"></i>
                            <div class="media-body">
                                <span>Valor do Plano</span>
                                <p class="modal-resumo-valor"></p>
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-8">

                        <div class="media">
                            <i class="fa fa-stethoscope circle-yes mr-3" style="line-height: 24px; width: 27px; height: 27px; font-size: medium;"></i>
                            <div class="media-body">
                                <span>Segmentação Assistencial</span>
                                <p class="modal-resumo-segmentacao-assistencial"></p>
                            </div>
                        </div>
                        
                    </div>

                </div>                

            `);

            $('#ModalLabel').text('Simulação: ' + resultSummary.chave);
            $('#resumoNomeTitular').text(resultSummary.nome);
            $('#resumoDataNascimento').text(fnDataToBr(resultSummary.dataNascimento));
            //$('#resumoCpf').text(resultSummary.cpf);
            $('#resumoCelular').text(resultSummary.celular);

            let state = resultEstado.filter((o) => { return o.id == resultSummary.estado });
            //let profession = resultProfissao.filter((o) => { return o.id == resultSummary.profissao });

            $('#resumoEstado').text(state[0].descricao);
            //$('#resumoProfissao').text(profession[0].descricao);

            $('#resumoEmail').text(resultSummary.email);
            $('#namePlan').text(resultSummary.nomePlano);

            $('.modal-resumo-adm').text(resultSummary.nomeAdministradora);
            $('.modal-resumo-abrangencia').text(resultSummary.abrangencia);
            $('.modal-resumo-acomodacao').text(resultSummary.acomodacao);

            $('.modal-resumo-valor').text(`${resultSummary.valorTotal.length < 4 ? resultSummary.valorTotal.toFixed(2) : resultSummary.valorTotal.toLocaleString('pt-BR')}`);

            $('.modal-resumo-segmentacao-assistencial').text(_segmentacaoAssistencial);
            $('.modal-resumo-info').text(resultSummary.descricaoPlano);

        });
        function splitString(stringToSplit, separator) {
            let arrayOfStrings = stringToSplit.split(separator);
            return arrayOfStrings;
        }
    }

    function fnDataToBr(data) {
        let split = data.split('-');
        let novadata = split[2] + "/" + split[1] + "/" + split[0];
        return novadata;
    }
    
    window.getImg = function loadImg(search) {
        let arrayObj = arrayImg.filter((obj, index, array) => {
            if ((stripVowelAccent(obj.title).toUpperCase()).indexOf(stripVowelAccent(search).toUpperCase()) > -1) {
                return true;
            }
        });
        if (arrayObj.length > 0) {
            return arrayObj[0].src;
        } else {
            return arrayObj.src;
        }
    }

    // Parse URL Queries
    function url_query(query) {
        query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)";
        var regex = new RegExp(expr);
        var results = regex.exec(window.location.href);
        if (results !== null) {
            return results[1];
        } else {
            return false;
        }
    }

    var helpers =
        {
            buildDropdown: function (result, dropdown, emptyMessage) {
                // Remove current options
                dropdown.html('');
                // Add the empty option with the empty message
                dropdown.append('<option value="">' + emptyMessage + '</option>');
                // Check result isnt empty
                if (result != '') {
                    // Loop through each of the results and append the option to the dropdown
                    $.each(result, function (k, v) {
                        dropdown.append('<option value="' + v.id + '">' + v.descricao + '</option>');
                    });
                }
            }
        }

});


(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);