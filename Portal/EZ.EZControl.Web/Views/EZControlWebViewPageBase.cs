﻿using Abp.Dependency;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Views;

namespace EZ.EZControl.Web.Views
{
    public abstract class EZControlWebViewPageBase : EZControlWebViewPageBase<dynamic>
    {
       
    }

    public abstract class EZControlWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        public IAbpSession AbpSession { get; private set; }
        
        protected EZControlWebViewPageBase()
        {
            AbpSession = IocManager.Instance.Resolve<IAbpSession>();
            LocalizationSourceName = EZControlConsts.LocalizationSourceName;
        }
    }
}