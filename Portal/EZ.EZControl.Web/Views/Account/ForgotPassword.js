﻿var CurrentPage = function() {

    var handleForgetPassword = function() {

        var $form = $('.forget-form');

        $form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {

            },

            invalidHandler: function(event, validator) {

            },

            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
            },

            submitHandler: function(form) {
                $form.find('.alert-danger').hide();
            }
        });

        $form.find('input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').valid()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        $form.submit(function(e) {
            e.preventDefault();

            if (!$form.valid()) {
                return;
            }

            abp.ui.setBusy(
                null,
                abp.ajax({
                    contentType: app.consts.contentTypes.formUrlencoded,
                    url: $form.attr('action'),
                    data: $form.serialize()
                }).done(function () {
                    abp.message.success(app.localize('PasswordResetMailSentMessage'), app.localize('MailSent'))
                        .done(function () {
                            let name = parameterUrl('slug');
                            if (name != '') {
                                location.href = abp.appPath + 'Account/Login' + `?slug=${name}`;
                            } else {
                                location.href = abp.appPath + 'Account/Login';
                            }
                        });
                })
            );
        });
    }

    let logo = function () {       

        let _logoForm = document.getElementsByClassName('_logoForm');
        if (localStorage.getItem("logo") != null && localStorage.getItem("logo") != 'http://' + window.location.host.concat('/' + "Uploads/logo-padrao.png")) {
            _logoForm[0].style.display = "inherit";
            _logoForm[0].src = localStorage.getItem("logo");
        } else {
            _logoForm[0].style.display = "none";
        }
        //$logoForm[0].alt = data.alt;      

        let name = parameterUrl('slug'); 
        if (name != '') {
            let $btnBackForgotPassword = document.getElementsByClassName('_btnBackForgotPassword');
            $btnBackForgotPassword[0].href = $btnBackForgotPassword[0].href + `?slug=${name}`;
        }
    }

    let parameterUrl = function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    return {
        init: function() {
            handleForgetPassword();
            logo();
        }
    };
}();