﻿var CurrentPage = function () {

    (function ($) {
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);

    jQuery.validator.addMethod("customUsername", function (value, element) {
        if (value === $('input[name="EmailAddress"]').val()) {
            return true;
        }

        return !$.validator.methods.email.apply(this, arguments);
    }, abp.localization.localize("RegisterFormUserNameInvalidMessage"));

    var _passwordComplexityHelper = new app.PasswordComplexityHelper();

    var handleRegister = function () {

        $("#celular").mask("(99) 99999-9999");
        $("#CPF").mask("999.999.999-99");  

        function submit() {

            var dataJson = $('form').serializeFormJSON();

            abp.ajax({
                url: '/Ezliv/Registro',
                data: JSON.stringify(dataJson),
            }).done(function (data) {
                if (data.id) { //sucesso
                    sweetAlert({
                        title: 'Usuário criado com sucesso, Favor Verifique seu Email !',
                        text: "",
                        type: 'success',
                        confirmButtonColor: '#8EBED4',
                        confirmButtonText: 'Fazer login!',
                        confirmButtonClass: 'btn btn-success'
                    }, function () {
                        let name = parameterUrl('corretora'); 
                        if (name != ''){
                            window.location.href = "/Account/login" + `?slug=${name}`;
                        } else {
                            window.location.href = "/Account/login";
                        }
                    })
                } else {
                    abp.message.error('', data.Message);
                }
            });
        }

        $('.register-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                PasswordRepeat: {
                    equalTo: "#RegisterPassword"
                },
                UserName: {
                    required: true,
                    customUsername: true
                }
            },

            messages: {

            },

            invalidHandler: function (event, validator) {

            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                if (element.closest('.input-icon').size() === 1) {
                    error.insertAfter(element.closest('.input-icon'));
                } else {
                    error.insertAfter(element);
                }
            },

            submitHandler: function (form) {
                submit();
            }
        });

        $('.register-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.register-form').valid()) {
                    submit();
                }
                return false;
            }
        });

        //var $element = $('#RegisterPassword');
        //_passwordComplexityHelper.setPasswordComplexityRules($element, window.passwordComplexitySetting);
    }

    let logo = function () {        

        let _logoForm = document.getElementsByClassName('_logoForm');
        if (localStorage.getItem("logo") != null && localStorage.getItem("logo") != 'http://' + window.location.host.concat('/' + "Uploads/logo-padrao.png")) {
            _logoForm[0].style.display = "inherit";
            _logoForm[0].src = localStorage.getItem("logo");
        } else {
            _logoForm[0].style.display = "none";
        }
        //$logoForm[0].alt = data.alt;

        let name = parameterUrl('corretora'); 
        if (name != '') {
            let $btnBackRegister = document.getElementsByClassName('_btnBackRegister');            
            $btnBackRegister[0].href = $btnBackRegister[0].href + `?slug=${name}`;
        }
    }

    let parameterUrl = function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    return {
        init: function () {
            handleRegister();
            logo();
        }
    };

}();