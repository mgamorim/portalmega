﻿var abp = abp || {};
(function ($) {

    var ezLoadingSpinner = {
        ajaxCount: 0,
        totalCount: 0,
        doneCount: 0,
        message: '',
        hasMessage: false
    };

    var stAddEzLoadingSpinner;
    var stFnAddEzLoadingSpinner = function () {
        if (!stAddEzLoadingSpinner) {
            stAddEzLoadingSpinner = setTimeout(function () {
                $('#loading-overlay').fadeIn();
                stAddEzLoadingSpinner = null;
            }, 1000);
        }
    };

    var stRemoveEzLoadingSpinner;
    var stFnRemoveEzLoadingSpinner = function () {
        stRemoveEzLoadingSpinner = setTimeout(function () {
            if (!stAddEzLoadingSpinner) {
                $('#loading-overlay').fadeOut();
                ezLoadingSpinner.totalCount = 0;
                ezLoadingSpinner.ajaxCount = 0;
                ezLoadingSpinner.doneCount = 0;
                ezLoadingSpinner.hasMessage = false;
                ezLoadingSpinner.message = '';
                stRemoveEzLoadingSpinner = null;
            }
        },
            500);
    };

    // Função de inicialização do Spinner.
    ezLoadingSpinner.add = function (msg) {

        var mensagem = msg || 'Carregando dados...';

        ezLoadingSpinner.ajaxCount = ezLoadingSpinner.ajaxCount + 1;
        // Contabiliza quantas reqs foram iniciadas
        ezLoadingSpinner.totalCount = ezLoadingSpinner.totalCount + 1;

        // Verifica se há apenas uma requisição para colocar a mensagem personalizada, caso contrário usa a mensagem padrão.
        if (ezLoadingSpinner.totalCount === 1) {
            ezLoadingSpinner.message = mensagem;
            ezLoadingSpinner.hasMessage = true;
        } else {
            ezLoadingSpinner.message = 'Carregando dados...';
            ezLoadingSpinner.hasMessage = false;
        }

        // Exibe o overlay caso a quantidade de requisições seja diferente de 0;
        stFnAddEzLoadingSpinner();
    };

    // Função para fechamento do Spinner.
    ezLoadingSpinner.remove = function () {
        ezLoadingSpinner.ajaxCount = ezLoadingSpinner.ajaxCount - 1;

        // Contabiliza quantas reqs já foram concluídas.
        ezLoadingSpinner.doneCount = ezLoadingSpinner.doneCount + 1;

        if (ezLoadingSpinner.ajaxCount === 0
            || (ezLoadingSpinner.doneCount === ezLoadingSpinner.totalCount)) {
            clearTimeout(stAddEzLoadingSpinner);
            stAddEzLoadingSpinner = null;
        }

        if ((ezLoadingSpinner.ajaxCount === 0) || (ezLoadingSpinner.doneCount >= ezLoadingSpinner.totalCount)) {
            stFnRemoveEzLoadingSpinner();
        }
    };

    if (!$) {
        return;
    }

    /* JQUERY ENHANCEMENTS ***************************************************/

    // abp.ajax -> uses $.ajax ------------------------------------------------

    abp.ajax = function (userOptions) {
        userOptions = userOptions || {};

        var options = $.extend({}, abp.ajax.defaultOpts, userOptions);
        options.success = undefined;
        options.error = undefined;

        return $.Deferred(function ($dfd) {
            ezLoadingSpinner.add();
            $.ajax(options)
                .done(function (data, textStatus, jqXHR) {
                    ezLoadingSpinner.remove();
                    if (data.__abp) {
                        abp.ajax.handleResponse(data, userOptions, $dfd, jqXHR);
                    } else {
                        $dfd.resolve(data);
                        userOptions.success && userOptions.success(data);
                    }
                }).fail(function (jqXHR) {
                    ezLoadingSpinner.remove();
                    if (jqXHR.responseJSON && jqXHR.responseJSON.__abp) {
                        abp.ajax.handleResponse(jqXHR.responseJSON, userOptions, $dfd, jqXHR);
                    } else {
                        abp.ajax.handleNonAbpErrorResponse(jqXHR, userOptions, $dfd);
                    }
                });
        });
    };

    $.extend(abp.ajax, {
        defaultOpts: {
            dataType: 'json',
            type: 'POST',
            contentType: 'application/json'
        },

        defaultError: {
            message: 'Ocorreu um erro!',
            details: 'Detalhes do erro não enviados pelo servidor.'
        },

        defaultError401: {
            message: 'Você não está autenticado!',
            details: 'Você deve ser autenticado (iniciar sessão) para executar esta operação.'
        },

        defaultError403: {
            message: 'Você não está autorizado!',
            details: 'Você não tem permissão para executar esta operação.'
        },

        defaultError404: {
            message: 'Recurso não encontrado!',
            details: 'O recurso solicitado não foi encontrado no servidor.'
        },

        logError: function (error) {
            abp.log.error(error);
        },

        showError: function (error) {
            if (error.details) {
                return abp.message.error(error.details, error.message);
            } else {
                return abp.message.error(error.message || abp.ajax.defaultError.message);
            }
        },

        handleTargetUrl: function (targetUrl) {
            if (!targetUrl) {
                location.href = abp.appPath;
            } else {
                location.href = targetUrl;
            }
        },

        handleNonAbpErrorResponse: function (jqXHR, userOptions, $dfd) {
            if (userOptions.abpHandleError !== false) {
                switch (jqXHR.status) {
                    case 401:
                        abp.ajax.handleUnAuthorizedRequest(
                            abp.ajax.showError(abp.ajax.defaultError401),
                            abp.appPath
                        );
                        break;
                    case 403:
                        abp.ajax.showError(abp.ajax.defaultError403);
                        break;
                    case 404:
                        abp.ajax.showError(abp.ajax.defaultError404);
                        break;
                    default:
                        abp.ajax.showError(abp.ajax.defaultError);
                        break;
                }
            }

            $dfd.reject.apply(this, arguments);
            userOptions.error && userOptions.error.apply(this, arguments);
        },

        handleUnAuthorizedRequest: function (messagePromise, targetUrl) {
            if (messagePromise) {
                messagePromise.done(function () {
                    abp.ajax.handleTargetUrl(targetUrl);
                });
            } else {
                abp.ajax.handleTargetUrl(targetUrl);
            }
        },

        handleResponse: function (data, userOptions, $dfd, jqXHR) {
            if (data) {
                if (data.success === true) {
                    $dfd && $dfd.resolve(data.result, data, jqXHR);
                    userOptions.success && userOptions.success(data.result, data, jqXHR);

                    if (data.targetUrl) {
                        abp.ajax.handleTargetUrl(data.targetUrl);
                    }
                } else if (data.success === false) {
                    var messagePromise = null;

                    if (data.error) {
                        if (userOptions.abpHandleError !== false) {
                            messagePromise = abp.ajax.showError(data.error);
                        }
                    } else {
                        data.error = abp.ajax.defaultError;
                    }

                    abp.ajax.logError(data.error);

                    $dfd && $dfd.reject(data.error, jqXHR);
                    userOptions.error && userOptions.error(data.error, jqXHR);

                    if (jqXHR.status === 401 && userOptions.abpHandleError !== false) {
                        abp.ajax.handleUnAuthorizedRequest(messagePromise, data.targetUrl);
                    }
                } else { //not wrapped result
                    $dfd && $dfd.resolve(data, null, jqXHR);
                    userOptions.success && userOptions.success(data, null, jqXHR);
                }
            } else { //no data sent to back
                $dfd && $dfd.resolve(jqXHR);
                userOptions.success && userOptions.success(jqXHR);
            }
        },

        blockUI: function (options) {
            if (options.blockUI) {
                if (options.blockUI === true) { //block whole page
                    abp.ui.setBusy();
                } else { //block an element
                    abp.ui.setBusy(options.blockUI);
                }
            }
        },

        unblockUI: function (options) {
            if (options.blockUI) {
                if (options.blockUI === true) { //unblock whole page
                    abp.ui.clearBusy();
                } else { //unblock an element
                    abp.ui.clearBusy(options.blockUI);
                }
            }
        },

        ajaxSendHandler: function (event, request, settings) {
            if (!settings.headers || settings.headers[abp.security.antiForgery.tokenHeaderName] === undefined) {
                request.setRequestHeader(abp.security.antiForgery.tokenHeaderName, abp.security.antiForgery.getToken());
            }
        }
    });

    $(document).ajaxSend(function (event, request, settings) {
        return abp.ajax.ajaxSendHandler(event, request, settings);
    });

    /* JQUERY PLUGIN ENHANCEMENTS ********************************************/

    /* jQuery Form Plugin 
     * http://www.malsup.com/jquery/form/
     */

    // abpAjaxForm -> uses ajaxForm ------------------------------------------

    if ($.fn.ajaxForm) {
        $.fn.abpAjaxForm = function (userOptions) {
            userOptions = userOptions || {};

            var options = $.extend({}, $.fn.abpAjaxForm.defaults, userOptions);

            options.beforeSubmit = function () {
                abp.ajax.blockUI(options);
                userOptions.beforeSubmit && userOptions.beforeSubmit.apply(this, arguments);
            };

            options.success = function (data) {
                abp.ajax.handleResponse(data, userOptions);
            };

            //TODO: Error?

            options.complete = function () {
                abp.ajax.unblockUI(options);
                userOptions.complete && userOptions.complete.apply(this, arguments);
            };

            return this.ajaxForm(options);
        };

        $.fn.abpAjaxForm.defaults = {
            method: 'POST'
        };
    }

    abp.event.on('abp.dynamicScriptsInitialized', function () {
        abp.ajax.defaultError.message = abp.localization.abpWeb('DefaultError');
        abp.ajax.defaultError.details = abp.localization.abpWeb('DefaultErrorDetail');
        abp.ajax.defaultError401.message = abp.localization.abpWeb('DefaultError401');
        abp.ajax.defaultError401.details = abp.localization.abpWeb('DefaultErrorDetail401');
        abp.ajax.defaultError403.message = abp.localization.abpWeb('DefaultError403');
        abp.ajax.defaultError403.details = abp.localization.abpWeb('DefaultErrorDetail403');
        abp.ajax.defaultError404.message = abp.localization.abpWeb('DefaultError404');
        abp.ajax.defaultError404.details = abp.localization.abpWeb('DefaultErrorDetail404');
    });

})(jQuery);