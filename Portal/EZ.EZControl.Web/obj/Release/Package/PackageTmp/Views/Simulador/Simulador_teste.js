﻿

function BuscarPlanos() {

    var formData = $("form.busca").serializeFormJSON();
    var dataJson = formData;

    abp.ajax({
        url: '/Simulador/BuscarPlanos',
        data: JSON.stringify(dataJson),
    }).done(function (data) {

        if (data.email) { //sucesso

            sweetAlert({
                title: 'Usuário criado com sucesso, Favor Verifique seu Email !',
                text: "",
                type: 'success',
                confirmButtonColor: '#8EBED4',
                confirmButtonText: 'Fazer login!',
                confirmButtonClass: 'btn btn-success'
            }, function () { window.location.href = "/Account/login"; })



        } else {

            abp.message.error('', data.erro);

        }
    });
}



function CompreAgora() {

    var formData = $("form.busca").serializeFormJSON();
    var dataJson = formData;

    abp.ajax({
        url: '/Simulador/CompreAgora',
        data: JSON.stringify(dataJson),
    }).done(function (data) {

        if (data.email) { //sucesso

            sweetAlert({
                title: 'Usuário criado com sucesso, Favor Verifique seu Email !',
                text: "",
                type: 'success',
                confirmButtonColor: '#8EBED4',
                confirmButtonText: 'Fazer login!',
                confirmButtonClass: 'btn btn-success'
            }, function () { window.location.href = "/Account/login"; })



        } else {

            abp.message.error('', data.erro);

        }
    });
}

function ResumoSimulador() {

    var formData = $("form.busca").serializeFormJSON();
    var dataJson = formData;

    abp.ajax({
        url: '/Simulador/ResumoSimulador',
        data: JSON.stringify(dataJson),
    }).done(function (data) {

        if (data.email) { //sucesso

            sweetAlert({
                title: 'Usuário criado com sucesso, Favor Verifique seu Email !',
                text: "",
                type: 'success',
                confirmButtonColor: '#8EBED4',
                confirmButtonText: 'Fazer login!',
                confirmButtonClass: 'btn btn-success'
            }, function () { window.location.href = "/Account/login"; })



        } else {

            abp.message.error('', data.erro);

        }
    });
}


(function ($) {
    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);