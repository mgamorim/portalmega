var faker = window.faker;
var base64Img = null;

var pdf = {};

pdf.multiple = function (persona) {
    var doc = new jsPDF();

    doc.setFontSize(15);
    doc.setFont("times");
    doc.setFontType("normal");
    doc.text(105, 20, 'Evida simulação de planos de saúde', null, null, 'center');
    doc.setFontSize(11);
    doc.text("Data da simulação: "+dataAtualFormatada(), 14, 35);
    doc.setTextColor(100);
    doc.text("Nome: "+persona.Nome, 14, 45);
    doc.text("Data de nascimento: "+persona.DataNascimento, 14, 50);
    doc.text("Telefone de Contato: "+persona.Celular, 14, 55);
    doc.text("Estado: "+persona.Estado, 14, 60);
    doc.text("E-mail: "+persona.Email, 14, 65);

    let nextTable = undefined;

    if(persona.Dependentes.length > 0){
        doc.setFontSize(12);       
        doc.text("Dependentes", 14, 77);
        doc.autoTable(getColumns('depend'), persona.Dependentes, {
            startY: 80,
            pageBreak: 'avoid',
            bodyStyles: { valign: 'middle', halign: 'center' },
            headerStyles: {
                fillColor: [133, 133, 173],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }
        });
        nextTable = doc.autoTable.previous.finalY;
    }else{
        nextTable = 65;       
    }

    doc.setFontSize(14);
    doc.setFont("times");
    doc.setFontType("normal");
    doc.text(105, nextTable + 20, 'Lista dos valores mais baixos', null, null, 'center');
    doc.autoTable(getColumns('multiple'), getDataPlanos(undefined, undefined, 'dif'), {
        startY: nextTable + 25,
        pageBreak: 'avoid',
        bodyStyles: { valign: 'middle', halign: 'center' },
        headerStyles: {
            fillColor: [51, 204, 51],
            valign: 'middle',
            halign: 'center',
            overflow: 'hidden',
            columnWidth: 'wrap'
        },
        styles: {
            overflow: 'linebreak',
            columnWidth: 'wrap',
            fontSize: 9.3
        }  
    });

    let isTable = {
         amil: false,
         assim: false,
         caberj: false, 
         evercross: false,
         nextsaude: false,
         onehealth: false,
         onix: false,
         Unimedcentralnacional: false,
         unimedvaledoaco: false,
         amexsaude: false
    };

    window.dataPlanos.forEach(v => {
        if(v.operadoraNome.indexOf('Amil') > -1){
            isTable.amil = true;
        }
        if(v.operadoraNome.indexOf('Assim Saúde') > -1){
            isTable.assim = true;
        }
        if(v.operadoraNome.indexOf('Caberj  Integral Saúde S/A') > -1){
            isTable.caberj = true;
        }
        if(v.operadoraNome.indexOf('Evercross') > -1){
            isTable.evercross = true;
        }
        if(v.operadoraNome.indexOf('Next Saúde') > -1){
            isTable.nextsaude = true;
        }
        if(v.operadoraNome.indexOf('OneHealth') > -1){
            isTable.onehealth = true;
        }
        if(v.operadoraNome.indexOf('Ônix') > -1){
            isTable.onix = true;
        }
        if(v.operadoraNome.indexOf('Unimed Central Nacional') > -1){
            isTable.Unimedcentralnacional = true;
        }
        if(v.operadoraNome.indexOf('Unimed Vale Do Aço') > -1){
            isTable.unimedvaledoaco = true;
        }
        if(v.operadoraNome.indexOf('Amex Saúde') > -1){
            isTable.amexsaude = true;
        }
    });

    doc.setFontSize(15);
    doc.setFont("times");
    doc.setFontType("normal");
    //doc.text('Outros valores para planos de saúde', 70, doc.autoTable.previous.finalY + 20);
    doc.text(105, doc.autoTable.previous.finalY + 20, 'Outros valores para planos de saúde', null, null, 'center');

    if(isTable.amil){       
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Amil'), {
            startY: doc.autoTable.previous.finalY + 30,
            theme: "grid",
            alternateRowStyles: { 
                fillColor: [244, 244, 244] 
            },
            bodyStyles: { 
                valign: 'middle', 
                halign: 'center' 
            },
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            },
            columnStyles: {
                0: {columnWidth: 120}
            }  
        });   
    }

    if(isTable.assim){
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Assim'), {
            startY: doc.autoTable.previous.finalY + (isTable.amil ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }  
        });
    }

    if(isTable.caberj){        
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Caberj'), {
            startY: doc.autoTable.previous.finalY + (isTable.assim ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 10
            }  
        });
    }

    if(isTable.evercross){
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Evercross'), {
            startY: doc.autoTable.previous.finalY + (isTable.caberj ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }  
        });
    }

    if(isTable.nextsaude){        
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Next Saúde'), {
            startY: doc.autoTable.previous.finalY + (isTable.evercross ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }  
        });
    }

    if(isTable.onehealth){
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'OneHealth'), {
            startY: doc.autoTable.previous.finalY + (isTable.nextsaude ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }  
        });
    }

    if(isTable.onix){    
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Ônix'), {
            startY: doc.autoTable.previous.finalY + (isTable.onehealth ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }  
        });
    }

    if(isTable.Unimedcentralnacional){
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Unimed Central Nacional'), {
            startY: doc.autoTable.previous.finalY + (isTable.onix ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 10
            }  
        });
    }

    if(isTable.unimedvaledoaco){
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Unimed Vale Do Aço'), {
            startY: doc.autoTable.previous.finalY + (isTable.Unimedcentralnacional ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 10
            }  
        });    
    }

    if(isTable.amexsaude){
        doc.autoTable(getColumns('multiple'), getDataPlanos(undefined,'Amex Saúde'), {
            startY: doc.autoTable.previous.finalY + (isTable.unimedvaledoaco ? 10 : 30),
            headerStyles: {
                fillColor: [128, 128, 128],
                valign: 'middle',
                halign: 'center',
                overflow: 'hidden',
                columnWidth: 'wrap'
            },
            styles: {
                overflow: 'linebreak',
                columnWidth: 'wrap',
                fontSize: 11
            }  
        });    
    }

    doc = addWaterMark(doc);

    return doc;
};

var getColumns = function (columns) {
        switch (columns) {
            case 'auto': 
                            return [
                                {title: "Imagem", dataKey: "imagem"},
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Abrangencia", dataKey: "abrangencia"},
                                {title: "Acomodacão", dataKey: "acomodacao"},
                                {title: "Segmentacão", dataKey: "segmentacao"},
                                {title: "Valor", dataKey: "valor"},
                                {title: "Info", dataKey: "info"}
                            ];        
            case 'minimal': 
                            return [
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Abrangencia", dataKey: "abrangencia"},
                                {title: "Acomodacão", dataKey: "acomodacao"},
                                {title: "Segmentacão", dataKey: "segmentacao"},
                                {title: "Valor", dataKey: "valor"}
                            ];        
            case 'long': 
                            return [
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Adm", dataKey: "administradora"},
                                {title: "Abrangencia", dataKey: "abrangencia"},
                                {title: "Acomodacão", dataKey: "acomodacao"},
                                {title: "Segmentacão", dataKey: "segmentacao"},
                                {title: "Valor", dataKey: "valor"},
                                {title: "Info", dataKey: "info"}    
                            ];        
            case 'content': 
                            return [
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Acomodacão", dataKey: "acomodacao"},
                                {title: "Segmentacão", dataKey: "segmentacao"},
                                {title: "Valor", dataKey: "valor"}
                            ];        
            case 'multiple': 
                            return [
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Abrangencia", dataKey: "abrangencia"},
                                {title: "Acomodacão", dataKey: "acomodacao"},
                                {title: "Segmentacão", dataKey: "segmentacao"},
                                {title: "Valor", dataKey: "valor"}
                            ];        
            case 'operadora/valor': 
                            return [
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Valor", dataKey: "valor"}
                            ];        
            case 'xxxxx': 
                            return [
                                {title: "ID", dataKey: "id"},
                                {title: "Operadora", dataKey: "operadora"},
                                {title: "Adm", dataKey: "administradora"},
                                {title: "Abrangencia", dataKey: "abrangencia"},
                                {title: "Acomodacão", dataKey: "acomodacao"},
                                {title: "Segmentacão", dataKey: "segmentacao"},
                                {title: "Valor", dataKey: "valor"},
                                {title: "Info", dataKey: "info"},
                                {title: "Link", dataKey: "link"},
                                {title: "Imagem", dataKey: "imagem"},
                                {title: "Descrição", dataKey: "descricao"}
                            ];        
            case 'depend': 
                            return [                                
                                {title: "Nome", dataKey: "Nome"},
                                {title: "Data de nascimento", dataKey: "DataNascimento"}
                            ];        
            default:
                            return [
                                {title: "ID", dataKey: "id"},
                                {title: "Name", dataKey: "name"},
                                {title: "Email", dataKey: "email"},
                                {title: "City", dataKey: "city"},
                                {title: "Expenses", dataKey: "expenses"}
                            ];   
        }    
};

var getDataPlanos = function (rowCount, type, isDif) {
    let array = [];
    window.nomeOperadora = [];
        
    let data = window.dataPlanos.slice();

    if(type){
        data = data.filter((p) => {
            return (p.operadoraNome.indexOf(type) > -1);
        });
    }
    if(isDif){
        let dt = [];
        let is = false;
        data.filter(function(item, pos, self) {    
            dt.forEach(v=>{
                if(v.operadoraNome == item.operadoraNome)
                {
                    is = true;
                }
            })                                
            if(!is){
                dt.push(item);                 
            }                  
            is = false;                      
        });        
        data = dt.slice();
    }
    if(rowCount){
        data.length = rowCount;
    }

    data.forEach((v, i, arr) => {
        let val = v.valor.toLocaleString('pt-BR');
        if (val.indexOf(',') > -1 && val[val.indexOf(',') + 2] == undefined) {
            val = val + '0'
        } else if (val.length == 3 || (val.length == 5 && val.indexOf(',') == -1)) {
            val = val + ',00'
        }            

        window.nomeOperadora.push({operadora: v.operadoraNome});
        
        array.push({
            id: v.id,
            operadora: v.operadoraNome,
            administradora: v.administradoraNomePessoa,
            abrangencia: window.abran[v.planoDeSaude.abrangencia],
            acomodacao: window.acomod[v.planoDeSaude.acomodacao],
            segmentacao: window.segment.filter(s => s.value == v.planoDeSaude.segmentacaoAssistencial)[0].descricao,
            valor: `R$ ${val}`,
            info: v.nome,
            link: v.linkRedeCredenciada,
            imagem: '',
            descricao: v.descricao
        });
    });

    return array;
}

function dataAtualFormatada(){
    var data = new Date();
    var dia = data.getDate();
    if (dia.toString().length == 1)
      dia = "0"+dia;
    var mes = data.getMonth()+1;
    if (mes.toString().length == 1)
      mes = "0"+mes;
    var ano = data.getFullYear();  
    return dia+"/"+mes+"/"+ano;
}

function addWaterMark(doc) {
    var totalPages = doc.internal.getNumberOfPages();
  
    let imgData = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAeIAAACUCAYAAABP5SrLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAATOUAAEzlAXXO8JUAAAAhdEVYdENyZWF0aW9uIFRpbWUAMjAxODowNToyNCAxNTo1NzozNZnce1oAAC/bSURBVHhe7Z0JYBT19cd/bzYJCRAUD0TFqliOBEGFRMUbbRX/ar1NQKlHiBxS61HqUVvE2sOzalWOQKlVCQGs920Rj6o1AeVKOLyriFZAuQLZ3Xn/92Z+C8nuzB7ZYzbJ+8DM/N5vZze7M7u/77zfvN/7ASIqQRAEQRC8wdBbQRAEQRA8QIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDxEhFgQBEEQPESEWBAEQRA8RIRYEARBEDwEEFEXhXQzZHpDH/DhqcqEQMAIPP9hxcAv9UNtDqgqzVWFh5yhEHorVA1qTc3LOMk09cOCIAhCnIgQZ4ghMxp+YYC6l4o5do1qVKa6tLayaJ622wzwSFlPle97kYqH2zUWC1Xwh7Nx5PObtC0IgiDEgQhxBiiZueIaUMZftNmcAJrmqLrKAXO0nfVoEV5AxSK7pgXvkhgPFzEWBEGIH7lHnGaiiDCTA4bxaEnVinJtZzUxRJgZqny7vQSzz+imbUEQBCEGIsRpJIYIh2gTYhyHCIcQMRYEQUgAEeI0EacIh8hqMU5AhEOIGAuCIMSJ3CNOAwmKcHMCqMxL6ioG1Gjbc1ohws1pN/eMi+96pzC/W5c9lS+vCwb9AAq3bPXv2LBqwpFp+2wwZ8TttNnNtsIAeBbLZr+iLU+AmpG3KcTu2mxJYOONeMlLW7XVAnj0/D1Vbt6t2gynAcurH9blmNB7OIrewyXaTAakY7qDtk1UaiLzB9puUAasU6b/I7V6/Rc4aUHA3jU7oWNxAx2LXtpsCcBK+r48pC0hyxAhTjFJiHCIrBHjJEU4RJsTYxbdgu67DzMATqSGrYQasf5U3cN+tCX06/kelFpNv6M6UPCGwsYFtZWDv9MPJwUJ8d/ob1+uzZYgvkaC9VNtZRx4dERflQertBnOf6jRP1qXI4DqC3srI/djbYbzEj33dF2OCYnPZbSZZVtphUW6lpaF9AudjxdXL7FqswT9W+XhkD67JoJt9DvcVwIpsxPpmk4hKRBhJode4zF6rTJte0KKRJhpE93UxuTJRsn0+uGlMxvmddmj+zckwk9T9XUkhCfQ1lGEGRLh3WlzJACMJ6NGGQXrSmfWv1o6o/6y3pPfyLf3aiWIs3UpEoCTYM7Ze2kr8+SB+/cT8XFdak90ouU4Wm6hX+iHdAHwDswpO8N6JBvI911EazcRZjoroxvvI2QhIsQpIkUiHMJTMU6hCIfIWjFmAS6tqh85pNdFK8AHPDb6AloKrAdbBzWG8BMSyll79OrxRcmMFbf0e/D91n3up/B1Wn9tGxHkKOhyti57gVujHlCBpqy5tZJGhirwPQdzRtD3+oIDdZ2XjNRbdwAu1SUhyxAhTgEpFuEQnohxGkQ4RNaJccnM5UNIgN9VBrAHx93PKYW85b0BjN8XFhR+VDKz/kqjbH40jyUCrKkJ0iaaqJ2vtxkF5lw4gDaH2lYEr+ElT3yry+0fgNOUL2+Jl94xeeeH0OZI24rKsXpfIcsQIU6SNIlwiIyKsRZh9sJSLcIhskKMWRBLZ9T/DpTvXTLjacCSwhJkBdOGnDrgjZIp9T/W1fERiNI9rdQp8Mh53DWeWSA3Sre02R67pWOxG3nHT5PIXantzII4gtb0NYsJ0L4/12UhixAhToI0i3CIjIhxMxFOuWcYhqdiPPj+JbsPPnXAc+TJTCYz167NGMdCHiweMrOeu7/jAi+u5gChNbYVQZ7Kzz9LlzOJW7f0VrURn9Lljgb3dkyF6hHOwXXpBICFOJxv9DacUWAY8Yi2kEFEiFtJhkQ4RFrFOIMiHMITMR7y0If7+brmvUmt0HBd5QWFhoK5fO9Y27FBrNYlJ+IW9VQA1WWcX7yfbUXwNI6r2aLL2cDdKhDsE9di+vsqPw6k55Qq0zxZmeRlIl5L9nRa6miJZ0IT+pnCNPKMj9d22tHno9i2WvAILRvtYjMADlazL+QARCGLkOFLrSDDItyclA9t8kCEm5OxoU1HTFvaIycn9w0qevE5HUGFf6yrKP6NNl2Bx8v6qRzfSm2Gs11ta+yBlz+5WdtphUTmj7S5ybbCMM0zcMScF7TlSsaGLyHejOXVf9JWUtDf2Y9e73ISsuvJdB47vYu1qmnHIBz1xHptpw2YM+JOek8Ttdmc82jhAK7ICzXEWXRcrtCWkAWIR5wgHoowk1LP2GMRZjLiGR9433sFOb7c56mYNSLMgIKbS2esGK9NV/DiGh6vu8i2IshX+fkZCRTSXZpu373/qdXfeppgJJ3QxcFaEq8/qMYg9wbE6n7fT+V1+qsupw2YbBgkws7Z+AJN75FX73xRBHABPDa8i7aELECEOAE8FuEQKRHjLBDhEGkX470Lu00n1SvRZnYBxv1DZqw4RlvumFGCtgzITPT0o2V8DHvbRhiINdmeeSoV4GU1/1Mj5rC3eadd48oImFN2oi6nhz5lPK75ANtoBuJHePH8r1WwiS8+nbrUC1XuHvwZhCxBhDhOskSEQyQlxlkkwiHSJsacWIM8z2RTIO5Apd5CxIdRmTegwgkK1URE9Sdq9J6ix9bq/VpDjgFGdWnVYudUliEM4Kky3e5Tng5VZ3fW5fSRA+4JIUzVYaKl0TSRPGROJ/mgrnIGfHfoUnrwOQZpscf7Jm/0MLL/cDkCiZ7OKkSI4yDLRDhEq8Q4C0U4RMrFeOCU+p4IkMx5W0gCc9HWDRv3rqsoOqFudPFVdRUD7qyrKH6odnTR3XWji26uHV187uLKAb1QBYaSIM+ipUk/NxF+pKAgaqPNXaO0WWhbEXRRhQVx31dtDVa3NKKzEJMHpi6pcW7w2zOrvuFgrrdtw5GjyCtOSxpSqCrliP8LbSsME1/VJeafetsSgJOhZmSkNy14gghxDLJUhEMkJMZZLMIhUirG+XlwB9gpKBOCxHQRC2ttRdGw2sqiefUTj4kaCGWSh1RXMfA9Eusr/P5gEb0AH+PEAHXl4JkrjtWWG1FSXhrp7Z6uLh9KjfePtBXObPYSdbnDoLviOVis0apwAny/1KXUUtj7VFrvaRstCKpGc9e9etPvLMR22z/KLgpeI0IchSwX4RBxiXEbEOEQKRHj0qplPBQl4S5pVHgfmI1DWVh1VUIsGXvoJ4teXfFT7sbWVfECPjTuMqKN8TQ3c6PKkw84cQY8cnJyua2j4eYNM0EzWtKRdg2Wzebo77tty5HTYfYFzjMiJQP4nFNaIr6HV9Rs0JbCEfM+oc0HthWBdE9nCSLELrQREQ4RVYzbkAiHSF6MIedmWif2/UY1sa6i+NraysF+XdMqzJoLgouvPJTvI/N93fgBNXTI9GXnaCsCHPEsjwvlfNhOdFN5PdLTDWpH5zp3gyLW6qjujsum4D20/sE2IjCULy+l84zriGe3PONP6u0uEOfpUjj9YPYI11myhMwhQuxAGxPhEI5i3AZFOESrxXjQlBUHkKgllOiCPWG+76vNpOHuaoWNY+mF1+mquECAX+uiM9GipwHSk9zDjs7dzzbCwI4TpOUGVtb8QGL3d21Gguh6cdUqcnb/Ga2dhx9hIFKIAebqUiQ+mQgiGxAhDqONinCIFmLchkU4RKvEOC/P4HtfObYVB6iW7PhS3aCtlEGe9Q8I6l5txgUoOLpk+vKh2ozkG/M5WjsnQAE4SwfxpBbDdexwQDWZHWGmpdgE1T90KRKAo6GqLHpUfCKA4RwtzbENdld0C3T3OWcHc6Isrbc0hLgQIW5GGxfhEJYY9531ckUbF+EQCYsxoLpYF+MCzcDVyyYVtyba2RVj8hs5Q2bUnw8KR+uquAGfUaGLEeA1NRwYFOn12HRXhb1P0eWUAGVlPhISt0CwBXhpTUIef7tlVA3fh+WJ+Z3wqUJrLuOkgb+V7UGb02wrAvdbISa6ecXdVace7GELHiJCrGknImzhh83ff1TwOKdObOsiHCJuMT5s6vLe5FY65d51AV+ru3KgNe4yFRwxY+nBpTMb/jRk/x7/NQDmU9PZVz8UN6jg/N6T34jmpUQJjkpx9PR5vpNovY9thGFih++WDmFFjSPy9KHOgK9Ul5Kjs8H36vNsowU8xty9dyJg9Vw4R7aDIUFbHiNCTLQzEf5uaecHNyMEDtZV7YW4xDgv1+fmLThjmvfpUqth77e0quGckpkNL+ZA7kdUdSNdDPS0H00cHnK15/57u2dlWrmOG3zn2XUAzoHJJ8ffLR8bt27pbWr7djfPvGMCEC3SfpDeJofzTEvMv7Fs9n91OQIcVfMFXSi8o81wTrNuYwme0eGFuLSq/lIR4TbDUGV0e9bqLnWB3JLY6SJ38e2itetf1uWEGTxtyf4kvrcO6dXjU/olPalndUrNbwqAPVFHrPGr6NrVuJfq3zMlqRX1/WbnVIiIz2Rqook2AwaX6ZITSfdO6WFQzjM7IcaO0AcrO5sTOaqTkdDtHCG1dGgh5gnilQGxcsa2CTqACNsAnKDOBdcoVFAQdxcgifYr5qQTE8qPbEyebJRMrx9eOqP+SV9O3mckvpOoOtXjRLfRm4t+79WM0j2NmJruaft+s1PSCP770i0dznYVbRjX/nrbeuxhUE5tdkAFmubrsju4jYcxBW0jDADpnvaQDi3Eg07tty9tethW26XDiPBOwLGbr7RqMXtwzpMSOACI7+piTHgaxdKZ9TcO6XXRGvDBi9Rw8cVAKruAmQ0kwHdsb8JDakcX3a/rnLFTSjpPJQhwrjX2N1nA59Yt/Z3a+lGrexLaLVfM+47W220jgm4w69xCXW4tbkk8/qXzSkcFy5/6hvblAE4nBsGcC4/QZSHDdGghbmz86n+0yaaJzBOm44kwAfCpLrXAxIKDaBP/8B2EBl1yhLNcDZ654qTSGQ3Vvpzc/9If5rlt4xb6BFhG3vkYZTYeQAJ847JxxTEjkXVKyWrbiqCn6l8eK11mVODBMzrRxrnnAXEuVtYmlfSkPaLPifu5y8tzHosdBySS3LXtLJTuXc6RRN03R8YUe0SHFuJVE4bzrDp/1mabo0OKsFIr1ddBx+hQ9JkJ9W4EIPCVLrbgsKnLu3MA3+CqFSt8ynhdgSoH50jVZGAhm0+idsqiygGH1VUUT6+tHLzNfihO/FGSezhNCJ8IexZyLmPnPN0A0i3tzvd664CRcHKanUCuszfMKU83BeMPmrPTpDoP1QMYmZZx6EJMOrQQM4srB/xRoYo+nVkWQiL8v+UdT4Q/VsGmn+qxtBH4TGNvXYwLY7vJKSN3UjJz2dElMxtm5eX6vuIAPhLfIv1QKvkC0fwteb8H1lYUXVg7uniBlYWrFeAl1ezRu+URPk9P5N86DMMtt/QnasScuLv0OyDuAWw50PrEGYhu0dIvWJm94kSnSXW7rbC36tYnrbN4Cc50eCHmRnDRlQOubktirEV4S7DjifBJOHK+W9IEZRpmQh5Hky+vsfiudwqHzGgYWzqz4QNQOe+ScvFsOgX2HimDlfYlZapzF3357SF1owfcTt7v1/qxZHHzinupx8qO0uWE0JmW3JI8dMiZlhLAvcseg60SYqguO5K81R9rsyVoxt8tvQv35yBK97QHdHghZtqSGIsIu2OYkJAHmJ+r7u+yR/evyG+cQubhdm1K4QCaPzf5g33qKopOr60seirRKO2YBJu4UeVkDpH4XDNiRSdvn/+jtfNFDfqlWzo6zlHJNq27vWH43LzhLWpzI6c8TYwNwWdo7XwbBOBMePR850h5IW2IEGvaghiLCMcAOBdGAgBcQetkI1nDYW9xIXmNI7Z/iQfUVhTdxFMj2g+lHn1c3rKtMBDPb1X3tAFu3dKLsHzeSl0WnHG/x4qQ8EWYHjPvFr3+DFY+nVhcAYHjajhA1U3A81ReJzfhF9KECHEzslmMRYRjY4KnEfDrablXmcFiEt9hdZUD5qQ6f3UUnLunAQ5Wj184WFtxoafYO9O2IhBvODYcbe5G4t8HO8UoD7OMJIhuUfPxEK17WsYUZxgR4jCyUYxFhOMDFfI4zsyC6m1ygUet//LbXiTA19dWHpp5j3FrkJM5ODfyhi+x7uncPViEnabYC6qA1Q0uRMd9liXDaE0mMrdo6Q1qjfmKLifO9nU8r7VzkBdAKcwuSyBfu5AsIsQOZJMYiwjHj8/ETM0E9AN/NwIB/8Da0UXH11UUPfbJpBPdEjmkHbyiZgNtXrKtCBITYkTnbmnE1/Hi+akKMGvPRLu/2iJKPxZ6LLdbitF/4qSaVve44KULttNrPK3NSHw+CdrKIIAoAZBucEKHIdNXPKBATdBVGUVEODGszFpGwVYqpmUsJP1SFoEypwa+D1Z/cP0g/jtZA9SM5PuIzh5rMDgIR9ZEy4NsYU2o4duNJ5Nwiu69HMtmu09+nwBQfWFvZeQ6ZwWjCwr6O3EPoaHPzVHus2wrDMSbsbyak7BkBGsMbrc+fEHm7OBsa+yWSH5u+mzn0obH/TrxC2UG39bl1mH4htHaeb5sxK/Uk+aBWFMTLfhMSBHiEUfBS89YRDhxaisH++lcrdFmqthGjdLfzKBZSp5vSW3FgBnZJsIWm7Y+S2vnRt4w4kvuAd3OorWTCDeq4A9ugiCE6Nz7QFq7tanft2KSDLduaeavJKQfJLW4iTADsL86B36iLSHNiBDHwAsxFhFuPQiKczCngo8R8fomf7BX7ejiCvoO1On6rERHzzp3NbpP7N8SA9yic5/Fkc9v0mXBDZ/qp0tOOKZldUNP93mGbXmEYUj3dIYQIY6DTIqxiHCSmKZbUvt4QPr/gkkN4KIv5/atG11875KxhyZ0X89Tgq4pLwfoXMWuwCPncTpLTmvphERLx4XhPmkCYr0uxYdRyN3SqU4skyjnQFWZe/CZkDJEiOMkE2IsIpw8OwLwKm0Sva+F9G92IOAfVFtRfMaiiqIXzEmTnJNkpIjSqvof8YQS2kwNa77hz84TmTiQE90rzs/nCR6cht6sVyuDboFgQnMAjtQlJ5bqbZxANozlLVCFrmPKhRQiQpwA6RRjEeHUwDMXkVu7UJvxAkGFd30wZtBybacVY/IbOdRo/92njNdKZjb8RlcnDU5awAkj5tpWGACx7hO7dUvPSyY6t6OgE284T9rPmOo9XYoJPHZ+DzpfPBe094AhY4ozgAhxgqRDjEWEUwsgPqaLcWMA3MOT/mszbfDfGNyrR5UCxRGrPlDq9pIZDdMtcU4Nbt3Th0PNyEN0uQU6paFzw49B6ZaOh/N8x9DaebYqnqN467b4YwxyO7EX6vZ94Kh2/s2kcllLixvHwuNlznmuhZQhw5daSaqGNokIp57ek9/I37NXj8+ouI9dEyeo7uD5gLWVcgZOqe+anwc81CbCO6Vf4bNgNpYnPBViGFZKy9llH5NHFfl9QrwBy6vv1NZOYE55JT1xujab85kaMad3qid5aI/Dl+h93E+bq20rDMSX6X0M11ZM6LXeoc1Q22rBZrV9XQ9rDHAKsb4z1eWchpXn83bi93ROfqfLQhoQj7iVpMIzFhFOD5xcA9FM/LyAuqF0RsO9KfROd1JatWwgiTB3Tzp2EZNnfJYyCl4deF/9HrqqVViiCeCc+tA1etr1PqDMtBQHMLmMJ3OINtSIJ1mIC3isjNuCo20rghdSLcKMPseOc3xrRiU1paYQExHiJEhGjEWE08sOP9xHm8QzQYG6dsj+PV4/fOqyaENR4qbfg+93K51Rf4cychaROcCudeWY/K7wFgdyabt1+F1zEJfCP8r212ULmHP2XiTQzkFj6Hfr5haa09cop/VethFBQG035+lybHKtmZacRc/EJ3Qp9QSi5q0+iDzmE3VZSAMixEnSGjEWEU4/y8YVb+GuSW0mBqjjcnJzlpbOrH94yPSGPro2IUqrFu9bMqNhcmFB4eckdL+mqviyfYEqVgbUlsxscA/8iQFeUs1BZ05RuqDyjLN12Qa6sO3UA/ABls9bocuCC1Y2LQOifc+exstqXCLZHXGLlt6mghtf0OWUgxdXL6FNg205ImOK04gIcQpIRIxJhL8VEc4Mi6489BFU6mVtJgS5JHm0Hmf41KrSGQ0LSmesGF9atbw/xwboXSI4bOry3iUz668kEX1RGQX/BVC/o53dAnii0YOe11IwE8fNmw2f8J/Hq0aCKEFa8VD442tp7d57EsQHdCkmMOfCQbQ51LbCQHwJL3kpvRndEKNN6nG+nplLSAMixCkiHjHWIrxVRDgz8DkJqMBoEuNEPJJweJbjYQqMh5ThaxhStWJj6cyG/9DyNIluDW2f0vb6vFzfx7TzNBJRDszh4Syt5dNNjZtv0+XWEbRmSoq8vwtwEsw615qDGaaUdaWNU7S0qZpMmWkpBvD4iFI6nu7nCfFNHFn9prbiIMd97DBA+rqlQ/ijTI3I83b7uic2gYgQNyLEKSSaGIsIe8OHFQO/BBN5jGzCk7K7wJmGOHHDz0h0OciJPVe2kwqyasYmVMFzVk04MqmUknTMP6eN06QAnVTnAjuH8B6+n9LaKbf0Qvx5zVe6LDhgTROYA5zf223+YboIMuMeI24FQ4FrEo8dKviD20T+KQNHVa+mzWLbcsAAGVOcJkSIU4yzGKO5vMvD60WEvaG2svh1ahWvpGKkh5hF0JvbGlTm2XUVhyaYhckVt+7p0PAg52FCaEq3dBSguvw05fOxpxtteNxcLK+Jf3akx8p4HDJPGuHEqxnL9W1G7Z4eBo+WJRdIKDgiQpwGwsX4+5w1S4Oqqch6sGOQNSIcoq6iaBaayOM8s1WMN5HnPnxxxYBEs4K507SDo3X9ttGC4Xo4ipMQb1eb0xid24bhfN0wZ8RjyjB4Uv1o8w6vV9uD1+hyfBhRhz9l7nwETB7G5PYbMVSuMUqXhRQiQpwmmotxo7E++6bNSx9ZJ8Ih6iqLH0TT5OhPJ3Hyki+VGRhGnnty88uGgaOeWE+bV2yrBQeox8s4SKuXbbbgOays+UGXOzQ84QHMKTuOxPdGWt5WkFuvAC7mh+w9HEGF5mi8tGadtmMCk0/mlKcXajMcv9oajHsccrLgqJovaPNv23IA4Ocypjj1SGatNMNRtj+e9czkNfmzbyGzvX+Bs1aEm8OTLfjQqKaz0VNXeclCZTaOrK0cnPiY5ziAmpHsaTl1Na+kJXJGJjTPxfI5T2krLWQss5b9GT+yizHh3yYPMetMC0e683eDvd7EfrOobsXy2ZO1FRcwe8TpygduQ5NeoeNxmi5nBDqmV9HGfQSIGTwGR9S8qy0hBYhHnGbYM/7o8p9N8qk8TjDRnmkTIsxw9+92Px5Bl6BpD4CJwjZqtCcu+nLuKekSYQv/Bp6j2KlHxmlaxA1qFaZtrKoH8Gc8M86F5/7laSCPo4WHEHGCjgRFGB9OVIQtfJAd3dIhcNt8WrsHN4LMU5xqRIgzAItxcMQ/rqcfqvtVZtumzYhwCJ6lqa6i6CwSwzJa/qurMwFPuTgvGGgaUDu66O50T7eox56yGMfDfJlpqVXQOcXb1MiahPPOQ9XZ7IG7jRkPKv+OtPZOOIHlT/HEEu7zegNcBI+c7BRtL7QSEeIMYeVzHVlzdTsU4zYnws0hMZy7/qtv+1JTOpHM9HmmdD1GHvg/zaB5JP3NixaPOYwnpcgMGIwvVWVQkni0grV0fM/C8upJrcrL3a3LWbS2xnU78BZe8sS3upxZMOo48u6qU4/wxDBCEogQZ5B2KMZtWoRD8CQR7J1uavz8YJLLy+j88NCUVAVPfEavdzs2YT/ywM9fdOWA+KfDSxWbP+GAre9swwXEL9SaBIbbCDyc6I9qU7AYy2uet6taAaJ7Eg8vuqVD7Gj6J69twwHpnk4pIsQZph2JcbsQ4easmjB8R21l8SO1o4tP3NFkHogKx9DCwzni9l5JvTmL14tUuikQUINrK4oOptf7bd244niDhlIOVtb66fsWa+KB2TjJTGs3eTuAo+0X0DJGbWvshWWzf5NMhDlUn9VdAbgFpvG5YDH0BLz0n9/Td+YlbTpxKjx+wb66LCSJRE17hDUEYHbZA/RDTGo+Y49odyIcC56eMK8z9lY+Y38w1Z5gqK6kuqDA3Goq4EZrrYE7Pk1r4FUSQM1InkTCPd2iHwfqySLSTgajphOFG0P2Ahtp2Ujn9Gv6fX5K2xW0Xaw2BN/BcTVbeMdUAHNGjKbXrdJmSxD/jeXVHDjmGVA9olwZLlNq2kykc3W3LgtJIELsIW1UjDucCAuCIKQT6Zr2kDbYTS0iLAiCkGJEiD2mDYmxiLAgCEIaECHOAtqAGIsIC4IgpAkR4iwhi8VYRFgQBCGNiBBnEVkoxiLCgiAIaUaiprOQLImmFhHOMmDaqqvp2vlgtcP/F7y6mGfJEQShHSAecRaSBZ6xiHCWAQ/U/4h+rvfQd2IfEWFBaF+IEGcpzcT4r7oqU4gIZyN5ueNp/aXatGWcXSEIQntBuqazHN1Nfb8C+IWuSiciwq0AJr+Rr/ba9yBlAKhvN3+OkwZv0w+lDJi2tIf6IbADfz243U/aD1PquyqVe4AC01TfBD/FScUpnxEKJk821N5lByjD2E2hfyOOG5DJGbhiApMXd1Z7Fxys0BdU+MmnOGG4e95nDdy/ZHeVm7e/Ms3tav139D080X0qQyGrECFuA2RIjEWEEwTuq99DFeTcQ6VyMkPTwvmVwmfUjsB1bl3IcP/KfVW+jyetp723D8AJAyOOOUxdc5UC9WdqVE/Dcf3e0dU7gXtX76W6WOkXZ+HYvldbdVNXT6LvyK+sHXaxnRae8KGW9r2b9l1q1Wpg2upf0/q32nTjfRzT5xRdjgCmrfkNbW60LRdQvY1j+0RNXQkPrzxQ+Xz3U5HnBs6xKnneZqUeV7jjBhx76Ea7ynrft9P6l9p041V63+fpsoUlVp0630rH9udkdrdrLXhyjj/Q8ZmhbQv6O/R+4Arl39obJxzOecRbAFNWHUNi/jIVb6K/Zd1KoudMpfXFXA6DhZGnGHxLmf7f4rjidVZtM2DaqoOUMu6gIk+N2Mmq5JSbqJ5Q2HSz0wUDnfdBdN4foOIJbFqVVopONV19s/ZWEmT+DghZjHRNtwF0N/Uv09hNLSKcIOQF56iC3JeoxLmOF1CjdzOdqRuo/DzVnac65b5NwrKntXM4nYwyWrPQ5Kuc/FFWXTiAebTuSldhPrsijLxt3OCS57izseYm2H6OUovovSzUy4dk8+98FDXWtTB1Vdj0dZBLK86bTfvtfE7LBXGxva8LGPq7vJ/D83kBfn13LIH0+TgX9pm0/3xabrCPqXqPlkoFnV6FyfX8d0LYfxPpAsPp7/GCapm9q419AdT5fXov9FtSK6jqFvps42nfu6icS8enii6A/mztvAs+vl1VLl0MOwJ8bPl9NH9v9nOUmkv1j+1cFD5NS5DqRysj93W4770C3jkEPNxwFJ0qPtYX0Pui7xFeZy2oXqP3fLEy8hbDtIYj7L1t6P32pvf9BhWPpGWWMtVEeu4kKn9Cz7lB9dyPJy0RshzxiNsQafKMRYRbAXkh59B5eJIayZvJ0/uTrrYgj4gad3iIHruNHuNGsQXkQb5PG56wn2fY2Z88qf5c3xx6jWtpfS+9xgn0Gm/p6p3Agx/urXK7fEuN7nTy4sZYddNW/4HWNyu/n7zs4nprRw08vOYU5bNm80HyxvqHvDHtzd6uAmYpXtWvVVM0khhMpkb/d/ReDgv3uOOF3sf1tLmb3l4Zjuk71661ode/h17/Ojpal+K4Pv+w6qatvpPWE1VToC/+omiNtWMM6HUW0OucpEysxHF9Z+pqC7hrVaHqZvBsQ0PpWBxGx8ISce3d0vFt3AfHDIqYGximrD5OGfAWHdXr6Tzda9VNWz2L1pepHzbvHn4rwfoNT1k5xXpNNK/Asf2sCSusC5H8znxx0FUFg2fh+P4tJuggz/tU8ryfpOK3akvjoXj9IP7+8Gd6kD7TeBUMDMXxRf/hOsbqeu85go4jnE+vd2L46wnZhXjEbYg0eMYiwg6UzFxRVjqjobZ0ZsPbpTPqT9bVYUCRtUH1lLVtzrqvp9N6EzWQ3FXYAvhrQx/alNITX6bnsjD2I895qPVgGsHxff5FX6BrqLibMnKyb5IRVPbxXBeIPJ7oZ4+VDl7k8YwXmLbqNHr+MCpODRdhBif226z8JGj8V3KMkXZt6rF+wwF8yDIABltbJr+AL673o9/29U6iieP6vULXbTdR8SDVpcC68NLwRdzXzUWYwUmTTBU077EMw2hx3AZOqe9J3++59P1eTss9pVWLuVdE8BAR4jZGCsVYRNiBkpnLB4EyZlNzXELmsQjwzBD2PsNB9ZW1NfACa9sMK0gGdxyk1q39qa7aRa7PnggekbxpP0/8HqSGkru308+HWx+jNXlocKZdkUWAsr+H+/gijyd7743+PdU3s6/UVa3AsO/ZInvdzuCEPkvodJyq1m3+va5KDz7cwy7ArqA+hHNo/b0KfPKoXeHAlh18AbGNjhXHJITg72FP8sKPtc1mvLH8fbV9W3c1vv8fdI1Fpzz4O73GhVQcQMt1CJ34lorgISLEbZAUiLGIsAuAxlG02fm7AKW6QF6nQdrcxRaTuwk/pz1ug2lramHKml/Bg/XF9oN0jsYeutExahVgJInBYhzbb5UlMKgWUl1Z+P3CdIBTB/PE9nyvtsjqumyOzyjgaGXHhe+Hx4XZ2fH5vJTNd77XHSIY5C7aTQqMx+l4/ouEZbwduGSD1xRvsLy8cCAn3r/J91C/wLF9PrFNZ3BM/1fTEfUeAh5atR99vTgYiw6XaU28Dw++1Im+aPQdw7po0dG6O7qWlsNhrPZiQbF3HaTCm3TcnqblMnhg+T78ENZcEMRfHva91V40g77Tx+iiBYDRwhYyjwhxGyUJMRYRjkIgGHiXNhxQE2ILQCN5Si2xuzKbTiEh/ReZJfRLukvl5q6ghvALvqcJU1YcYO+5C5i6aght+lELPMeuIRB54vXdVH73c+2KdIMcQZ2nCs4qtG0NqDeVkbvZcdln3/g8aPC96/h8XoYdNlzv5QiO7/+5MvEnVPqAzJPpxUhgjE/peK6k5VYrQt2JXLqwcPp7vJw0sHmXLAmg7sVoBkefk2j/uMXC0dupYLeur9N7r9VLHS2rVI7xBSnfUXTe78Zx/V639gsexJ+NL3Yi3l8EqDhqOlcV51nHA8f0eZ+O28+o+BEtvJ2lOnX6mi5kFtP38DprGFQkb+utBal0C1vIPCLEbZhWiLGIcAw+GDNoOSrzImqdWJAXUiP1f7WVg1m8IsAJAz4mD+snKoBFZN1Ez+EGbV8SteuUkbc8orsQgLulTYWBXUIMTXyfuIl+ipfaFWkG2SEioEu4d/k0LTx0J3KBIHn+cYDIn8X5NYJmzGxgOK5vrRrXfwh9sfm2wO/pvXIEcV9aJqmC3KX6/no482hx/psmfE3bENzWRXrlXeBmEu01LRafb4F+NDkQSFyRBZYveg6npS99pvm0PQrH9p1IW5tcHXGNEHvcL6DdZnfa5eXScXtJrZtdRIJ8PAs81TTQjkfQmb5H9Sx83wrsa0Yg4L+Mnvw4vZclqPCPYDba9+AFz5Co6XZAnNHUIsIZwGr0cjqPo3PxO2oQP1Prqvtyl6odxTqSxWgvqg+LgoajaVWg/NsPCo0pJu+JA6r+Sr75T6xAqzCsLs4cgz2oKeQVcZARR+u6Rk2HoNflMcmHq3H9uvCFHNlZETXthuWdGr6J9PpX0XF7Dcf0te6702dNKGqaPufH9P7y6P216KmAqat5bPOurlmAClo30jE9xDKnraaLXCvney+qi/Sop6wapgxjAYngNSSIPAaanxMRNQ1TV/WjHyqPN+6mgkE6p/13DgmDe5Z2UV0LNpEwPk8XduzVuqIjv49TH2zuom81OAJT1hTRpcdkKvK94Cp670ncXxfSjXjE7YA4PGMR4RRDje3d1ChGXPhw0gdq7G+j4t9or0PUHhdZDbrqUX4irfen5RPyfPJbLHx+2FvLyeckEzam2mRtfdRwOwGmnYwC9X5xYGUAU+owWpaF3zf0Eh46REL5EC3Ng5AsuMuaxImFkLxUGBY2ljgR6khke8GDK+zzoaFz9SItvw0tVBUutvbx9ft3s7bhgLK7zAGjngeOCSCxZlEsJI/7Kbhz8c7Xs+798rhmUEdFux9vnT9QfHtjCYswX6RYx42HpoWB4/o0kJfMx5O7rE+zKoWsRYS4nbBTjJXiQJDm3Y58f3OYiHCqgbOoUfyVa8OJaB9vw2d3hxoGd0uj2uEfTsJyfPNFrdvMHtkWer1Lrd4NBoKrra0tnJEYPp3YAbmhjY+e+7IQdKZ3wd3Q2cPHWznz0xX0vsjrdQGtqGpQe37hklgjBkGcbW1z86JnAAsHwT4Pvlzn8wBgnweEmF651fWukL3mA1S3wpaJQwA5or2H2qenc4IXZp/92FvnCzP71kbAupgaTxdrjrc17OA2XEvfRWnnsxw5Qe0IFmMsm32jQj8PS7hcYfB0tXJdCdVlVR7ddgLfD/0RidufwyOCrfSTAJdQca16c+ka7cVxtqT3ndJe6ihdFse+aspKu5s08BkHLX1Py2iYUt/TqtNYXZkKrqWiqUzzVbs2OuQ5HcnPpOIGtd0/1a7NDnQX67Mks8fR+4wY42x5saDOouJ/4sm57MiE/s/QX+LxuaNh6pqbI84Z2TBlNQvdzsh3CzA5a5VJLeV19nHfhZWpS1ld2RtV8BOOZo6NGbiV1l/R56kkj3bXOGIz8DCtP1Ng3Efv73i7cheW1wt0kY3qU2X6p3Gd/i7x7YRyK8FMGDB15dG0HkrPk2QeWY7cIxaEVqAzIXFAFydUWEXN4nO05cxLB9CjF9F2b2q+R+C4PjXw8OqzlQ+eoiulX+PYfo6BMdT4nkENJr/Gzvt5PCSKBID3/44aYA6u+VIZSKIM51PdQXwrAnWeaWbnPWKlXqT3ozM6QScqH0xb9uj89JxzuDvWfoyfo+8Ro3qFWoMNdq0DZqASxxVv0VYL6L3re8TqJXoNvnhwpnHDFXjN0Y3aaoE1VAmN9+h1eOhNHb0WXWCYG8lX6Ed17Ml3oouOk0N5t3feI1acClJt5jpHFiy9hIfxcNFOcWm8RiUW29X0N+h4I5+z/anuTPo7dJy4Kxorm2f3omNURZvRtHxGyxw6r+vpvPC+fJ73oteZgGP72Ek6CKd7xM2BqatG8jAtKr6jxvU7LnSbgC4ESpUBr1CxkF6T03y+Qe8J6BFORMI5s38gL/jU5vfy9XMWUpF7OjgK+y163ja6EBxIZR6TvY2E+0g6d/H3nAgZR4RYEFqJNaQmP/dOaio5E1PzccDUyOMNJHhWlihqyDnf70UqgL3xqr6fcl041rjQIwo5yjdXrdu8b2gsK3k6HPh1CxV5+E0ITuh/t3p9yR0hkWGaCXFz2NvkdJZvUukuO2nFLnYKcSyCwb1wfP/12mrBTiGOxSazmzXsywU7MMu4lz4ve7/Nsz2R8OA1JI7/1jZ/Vi3EMVi3Nrf5eG6YUt9VGTk3UYmFtYdda0Gij4/TOfoTCd1aXWdh3X7ouR9/Po4J2N2qtOH7ybfQhdPfbdMmphBbaS5Xsad9PIn6zrSdjOX95+ZxzwUPGQt57fz+nyNBnegkqHrSh79Q8SRaQr2cfHtqofL7f+EWuCdkDyLEgpAkVkKG3IP6kDh2VU3m2lRP3G813A+vOpi8Uh6GskUtbVwdLWK2rWN1AXfJP0SZwQJqoT63Ep+kGCuKvceFveng7kXWRvX6so+aX9Q4YXVnnzCoj/KZuys/rlcb5n7smGQkBVjBXF0724Fl5qaPcMKRMYPy7GkQO/Fn8ilo+qj5bFVCdiNCLAiCIAgeIsFagiAIguAhIsSCIAiC4CEixIIgCILgISLEgiAIguAhIsSCIAiC4CEixIIgCILgISLEgiAIguAhIsSCIAiC4CEixIIgCILgISLEgiAIguAhIsSCIAiC4CEixIIgCILgISLEgiAIguAhIsSCIAiC4CEixIIgCILgISLEgiAIguAhIsSCIAiC4BlK/T8ya7JLWUKneAAAAABJRU5ErkJggg==`;

    for (i = 1; i <= totalPages; i++) {
      doc.setPage(i);
      if(i == 1){
        doc.addImage(imgData, 'PNG', 175, 5, 30, 10);
        doc.setTextColor(150);
        //doc.text(50, doc.internal.pageSize.height - 30, 'Watermark');
        doc.text(165, 25, '(21) 3490-0330');
      }else{
        doc.addImage(imgData, 'PNG', 175, 275, 30, 10);
        doc.setTextColor(150);
        //doc.text(50, doc.internal.pageSize.height - 30, 'Watermark');
        //doc.text(150, 25, '(21) 3490-0330');
      }
    }
  
    return doc;
}

function convertImgToBase64(src, callback) {
    var outputFormat = src.substr(-3) === 'png' ? 'image/png' : 'image/jpeg';
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function() {
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        // set size proportional to image
        canvas.height = canvas.width * (img.height / img.width);
        // step 1 - resize to 50%
        var oc = document.createElement('canvas'),
            octx = oc.getContext('2d');

        oc.width = img.width * 0.5;
        oc.height = img.height * 0.5;
        octx.drawImage(img, 0, 0, oc.width, oc.height);

        // step 2
        octx.drawImage(oc, 0, 0, oc.width * 0.5, oc.height * 0.5);

        // step 3, resize to final size
        ctx.drawImage(oc, 0, 0, oc.width * 0.5, oc.height * 0.5,
        0, 0, canvas.width, canvas.height);
          
        dataURL = canvas.toDataURL(outputFormat, 0.9);
        callback(dataURL);
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}