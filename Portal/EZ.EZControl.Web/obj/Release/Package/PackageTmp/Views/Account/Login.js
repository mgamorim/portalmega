﻿var CurrentPage = function () {

    var handleLogin = function () {

        var $loginForm = $('.login-form');

        $loginForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                }
            },

            invalidHandler: function (event, validator) {
                $loginForm.find('.alert-danger').show();
            },

            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },

            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function (form) {
                $loginForm.find('.alert-danger').hide();
            }
        });

        $loginForm.find('input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').valid()) {
                    $('.login-form').submit();
                }
                return false;
            }
        });

        $loginForm.submit(function (e) {
            e.preventDefault();

            if (!$('.login-form').valid()) {
                return;
            }

            abp.ui.setBusy(
                null,
                abp.ajax({
                    contentType: app.consts.contentTypes.formUrlencoded,
                    url: $loginForm.attr('action'),
                    data: $loginForm.serialize()
                }).done(function (data) {
                    window.location.href = data;
                })
            );
        });

        $('a.social-login-icon').click(function () {
            var $a = $(this);
            var $form = $a.closest('form');
            $form.find('input[name=provider]').val($a.attr('data-provider'));
            $form.submit();
        });

        $loginForm.find('input[name=returnUrlHash]').val(location.hash);

        $('input[type=text]').first().focus();

        //abp.ajax({
        //    url: abp.appPath + 'Picture/GetPictureLogoByName',
        //    type: 'POST',
        //    data: JSON.stringify({
        //        name: 'megavita'
        //    }),
        //    success(data) {
        //        console.log(data);
        //    },
        //    error(error) {
        //        console.log(error);
        //    }
        //});

        //$.ajax({
        //    url: abp.appPath + 'Picture/GetPictureLogoByName',
        //    contentType: 'application/json; charset=utf-8',
        //    datatype: 'json',
        //    data: {
        //        name: 'megavita'
        //    },
        //    type: "POST",
        //    success: function (Data, Status, jqXHR) {
        //        if (Data != "") {
        //            alert("Empty");
        //            return;
        //        }
        //        //window.location ='@Url.Action("DonloadPic","Mycontroller")';
        //    },
        //    error: function (jqXHR, textStatus, errorThrown) {
        //        console.log(textStatus);
        //    }
        //});

    }

    let parameterUrl = function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    var getLogo = function () {
        let $logoForm = document.getElementsByClassName('_logoForm');

        //if (parameterUrl('slugAdm') != '') {

        //    let nameAdm = parameterUrl('slugAdm');
        //    getIdImgAdm(nameAdm, $logoForm);

        //} else
        if ((parameterUrl('slug') != '')) {
        //if (localStorage.getItem("logo") == null || (parameterUrl('slug') != '' && localStorage.getItem("logo") != null)) {
            let name = parameterUrl('slug');
            abp.ajax({
                url: abp.appPath + 'Account/GetPictureLogoByName',
                type: 'POST',
                data: JSON.stringify({
                    name: name
                }),
                success(data) {
                    if (data.alt != "Corretora") {

                        let src = 'http://' + window.location.host.concat('/' + data.src.replace('\\', '/'));
                        localStorage.setItem("logo", src);
                        $logoForm[0].src = src;
                        //$logoForm[0].alt = data.alt;
                        $logoForm[0].style.display = "inherit";      

                        let $forgotPassword = document.getElementsByClassName('forget-password');
                        $forgotPassword[0].href = $forgotPassword[0].href + `?slug=${name}`;

                        let $register = document.getElementsByClassName('_register');
                        $register[0].href = $register[0].href + `?corretora=${name}&tipo=3`;

                    } else {
                        getIdImgAdm(name, $logoForm);
                    }
                },
                error(error) {
                    console.log(error);
                }
            });
        } else {

            if (localStorage.getItem("logo") != 'http://' + window.location.host.concat('/' + "Uploads/logo-padrao.png"))
            {
                $logoForm[0].style.display = "none";
                let $register = document.getElementsByClassName('_register');
                $register[0].style.display = "none";
                //$logoForm[0].style.display = "inherit";
                //$logoForm[0].src = localStorage.getItem("logo");
            }
            else
            {
                $logoForm[0].style.display = "none";
                let $register = document.getElementsByClassName('_register');
                $register[0].style.display = "none";
            }

        }
    };


    let getIdImgAdm = function (strNomeFantasia, form) {

        if (strNomeFantasia != '') {

            abp.ajax({
                url: abp.appPath + 'api/services/app/ezliv/administradora/GetPaginado',
                type: 'POST',
                data: JSON.stringify(
                    { nomeFantasia: strNomeFantasia, razaoSocial: strNomeFantasia, skipCount: 0, maxResultCount: 10, sorting: "PessoaJuridica.NomeFantasia" }
                ),
                success(data) {
                    if (data.items.length > 0) {
                        getImgLogoAdm(data, form);
                    } else {
                        let $register = document.getElementsByClassName('_register');
                        $register[0].style.display = "none";
                    }
                },
                error(error) {
                    console.log(error);
                }
            });

        } else {
            form[0].style.display = "none";
            let $register = document.getElementsByClassName('_register');
            $register[0].style.display = "none";
        }

    };

    let getImgLogoAdm = function (data, form) {
        let id = data.items[0].imagemId
        abp.ajax({
            url: abp.appPath + 'api/services/app/global/arquivoGlobal/GetById',
            type: 'POST',
            data: JSON.stringify({ id }),
            success(data) {
                if (data != null) {
                    form[0].style.display = "inherit";
                    let src = 'http://' + window.location.host.concat('/' + data.path.replace('\\', '/'));
                    form[0].src = src;
                    localStorage.setItem("logo", src);
                    let $register = document.getElementsByClassName('_register');
                    $register[0].style.display = "none";
                } else {
                    form[0].style.display = "none";
                    let $register = document.getElementsByClassName('_register');
                    $register[0].style.display = "none";
                }
            },
            error(error) {
                console.log(error);
            }
        });
    };

    return {
        init: function () {
            handleLogin();
            getLogo();
        }
    };

}();