﻿window.ez = window.ez || {};
window.ez.ui = window.ez.ui || {};

window.ez.ui.directiveBase = (function () {

    var disposeCallbacks = [];

    function EzUiDirectiveBase($scope) {

        $scope.$on("$destroy", function () {
            if (disposeCallbacks.length > 0) {
                for (var i = 0; i < disposeCallbacks.length; i++) {
                    if (typeof disposeCallbacks[i] == "function") {
                        typeof disposeCallbacks[i]();
                    }
                }
            }
        });
    }

    EzUiDirectiveBase.prototype.onDispose = function (callback) {
        if (typeof callback == "function") {
            disposeCallbacks.push(callback);
        } else {
            throw ("Você não adicinou um callback no evento onDispose!")
        }
    }

    return EzUiDirectiveBase;

})();

window.ez.ui.DirectiveBaseForm = (function (DirectiveBase) {

    var disposeCallbacks = [];

    function EzUiDirectiveBaseForm($scope) {

        var vm = this;

        DirectiveBase.call(this, $scope);

        EzUiDirectiveBaseForm.prototype = Object.create(DirectiveBase);

        EzUiDirectiveBaseForm.prototype.onDispose = function (callback) {
            DirectiveBase.prototype.onDispose(callback);
        }

        DirectiveBase.prototype.onDispose(function () {
            vm.saving = null;
            vm.loading = null;
            vm.registroId = null;
            vm.createOrEdit = null;
            vm.disableControls = null;
        });



    }

    EzUiDirectiveBaseForm.prototype.onDispose = function (callback) {
        DirectiveBase.prototype.onDispose(callback);
    }

    return EzUiDirectiveBaseForm;
})(ez.ui.directiveBase);