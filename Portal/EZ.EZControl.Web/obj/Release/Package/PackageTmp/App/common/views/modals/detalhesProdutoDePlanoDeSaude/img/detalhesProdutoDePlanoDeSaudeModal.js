﻿(function () {
    appModule.controller('common.views.modals.detalhesPlano', [
        '$scope',
        function ($scope) {
            var $ctrl = this;

            $ctrl.planoModal = $scope.$parent.$ctrl.planoModal;
            $ctrl.closeModal = $scope.$parent.$ctrl.closeModal;

            $ctrl.enablePlanoNavigation = $scope.$parent.$ctrl.enablePlanoNavigation;
            $ctrl.modalInstance = $scope.$parent.$ctrl.modalInstance;

            $ctrl.statusProposta = $scope.$parent.$ctrl.statusProposta;
        }
    ]);
})();