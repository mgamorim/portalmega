﻿window.ez = window.ez || {};
window.ez.ui = window.ez.ui || {};

window.ez.ui.ControllerBase = (function () {

    var disposeCallbacks = [];
    var viewContentLoadedCallbacks = [];

    function EzUiControllerBase($scope) {

        var vm = this;

        $scope.$on('$viewContentLoaded', function () {
            
            App.initAjax();

            if (viewContentLoadedCallbacks.length > 0) {
                for (var i = 0; i < viewContentLoadedCallbacks.length; i++) {
                    if (typeof viewContentLoadedCallbacks[i] == "function") {
                        typeof viewContentLoadedCallbacks[i]();
                    }
                }
            }
        });

        $scope.$on("$destroy", function () {

            vm.gridOptions = null;
            vm.gridData = [];
            vm.loading = false;
            vm.registros = null;
            vm.options = null;
            vm.getRegistros = null;
            vm.grid = null;
            vm.gridData = null;

            if (disposeCallbacks.length > 0) {
                for (var i = 0; i < disposeCallbacks.length; i++) {
                    if (typeof disposeCallbacks[i] == "function") {
                        typeof disposeCallbacks[i]();
                    }
                }
            }
        });
    }

    EzUiControllerBase.prototype.viewContentLoaded = function (callback) {
        if (typeof callback == "function") {
            viewContentLoadedCallbacks.push(callback);
        } else {
            throw ("Você não adicinou um callback no evento viewContentLoadedCallbacks!")
        }
    }

    EzUiControllerBase.prototype.onDispose = function (callback) {
        if (typeof callback == "function") {
            disposeCallbacks.push(callback);
        } else {
            throw ("Você não adicinou um callback no evento onDispose!")
        }
    }

    return EzUiControllerBase;

})();

window.ez.ui.ControllerBaseIndex = (function (ControllerBase) {

    function EzUiControllerBaseIndex($scope) {

        ControllerBase.call(this, $scope);
    
        EzUiControllerBaseIndex.prototype = Object.create(ControllerBase);

        EzUiControllerBaseIndex.prototype.viewContentLoaded = function (callback) {
            ControllerBase.prototype.viewContentLoaded(callback);
        }

        EzUiControllerBaseIndex.prototype.onDispose = function (callback) {
            ControllerBase.prototype.onDispose(callback);
        }
    }

    return EzUiControllerBaseIndex;


})(ez.ui.ControllerBase);

window.ez.ui.ControllerBaseEdit = (function (ControllerBase) {

    function EzUiControllerBaseEdit($scope) {

        ControllerBase.call(this, $scope);

        EzUiControllerBaseEdit.prototype = Object.create(ControllerBase);

        EzUiControllerBaseEdit.prototype.onDispose = function (callback) {
            ControllerBase.prototype.onDispose(callback);
        }

    }

    return EzUiControllerBaseEdit;

})(ez.ui.ControllerBase);

window.ez.ui.ControllerBaseCreate = (function (ControllerBase) {

    function EzUiControllerBaseCreate($scope) {

        ControllerBase.call(this, $scope);

        EzUiControllerBaseCreate.prototype = Object.create(ControllerBase);

        EzUiControllerBaseCreate.prototype.onDispose = function (callback) {
            ControllerBase.prototype.onDispose(callback);
        }

    }

    return EzUiControllerBaseCreate;

})(ez.ui.ControllerBase);

window.ez.ui.ControllerBaseDelete = (function (ControllerBase) {

    function EzUiControllerBaseDelete($scope) {

        ControllerBase.call(this, $scope);

        EzUiControllerBaseDelete.prototype = Object.create(ControllerBase);

        EzUiControllerBaseDelete.prototype.onDispose = function (callback) {
            ControllerBase.prototype.onDispose(callback);
        }

    }

    return EzUiControllerBaseDelete;

})(ez.ui.ControllerBase);