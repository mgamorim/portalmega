﻿(function () {
    appModule.controller('common.views.layout.header', [
        '$rootScope', '$scope', '$uibModal', 'appSession', 'appUserNotificationHelper', 'abp.services.app.notification', 'abp.services.app.userLink', 'empresaService',
        function ($rootScope, $scope, $uibModal, appSession, appUserNotificationHelper, notificationService, userLinkService, empresaService) {
            var vm = this;

            $scope.$on('$includeContentLoaded', function () {
                Layout.initHeader(); // init header
                //var menuToggle = document.querySelector('.ezs-menu-wrapper');
                //REMOVER Esse bloco (Menu antigo do lado esquerdo)
                //menuToggle.addEventListener('click', function () {
                //    var pageSidebar = document.querySelector('.page-sidebar-wrapper');
                //    if (!document.querySelector('.ezs-menu-input').checked && pageSidebar) {
                //        pageSidebar.classList.remove('active');
                //    } else {
                //        pageSidebar.classList.add('active');
                //    }
                //});
                // REMOVER Esse bloco (Quando ficar liberado a informação dos menus novos)

                vm.menu = abp.nav.menus.MainMenu;

                var ezsMenuInput = document.querySelector('.ezs-menu-input');
                var pageSidebar = document.querySelector('.page-sidebar-wrapper');

                $(".page-sidebar .page-sidebar-menu > li a").click(function FizMenuItemRemove(e) {

                    var parent = $(e.target).parent();

                    if ($(e.target).attr("href") !== "javascript:;"
                        && parent.attr("href") !== "javascript:;"
                        && ezsMenuInput.checked) {
                        pageSidebar.classList.remove('active');
                        ezsMenuInput.checked = false;
                    }
                });
            });

            getIsCorretor();
            vm.languages = abp.localization.languages;
            vm.currentLanguage = abp.localization.currentLanguage;
            vm.isImpersonatedLogin = abp.session.impersonatorUserId;
            vm.notifications = [];
            vm.unreadNotificationCount = 0;
            vm.unreadChatMessageCount = 0;
            vm.recentlyUsedLinkedUsers = [];
            vm.empresaLogada = {};
            vm.empresas = [];
            vm.logo = {};

            vm.hasPerfil = function () {
                return window.abp.auth.grantedPermissions['Pages.Tenant.Global.Perfil']
                    && appSession.user.userName !== app.consts.userManagement.defaultAdminUserName;
            }

            vm.getEmpresasPorUsuarioLogado = function () {
                vm.loading = true;
                var promise = empresaService.getEmpresasPorUsuarioLogado();
                promise.then(function (result) {
                    vm.empresas = result[0].items;
                    vm.empresaLogada = result[1];
                }).finally(function () {
                    vm.loading = false;
                    vm.getImgLogo();
                });
            };

            vm.escolherEmpresa = function (empresaId) {
                empresaService.escolherEmpresaAjax(
                    empresaId,
                    function (result) {
                        console.log(result);
                        window.location.reload();
                    });
            };

            vm.getShownUserName = function () {
                return appSession.user.name;
                //if (!abp.multiTenancy.isEnabled) {
                //    return appSession.user.userName;
                //} else {
                //    if (appSession.tenant) {
                //        return appSession.tenant.tenancyName + '\\' + appSession.user.userName;
                //    } else {
                //        return '.\\' + appSession.user.userName;
                //    }
                //}
            };

            vm.getShownLinkedUserName = function (linkedUser) {
                return app.getShownLinkedUserName(linkedUser);
            };

            vm.editMySettings = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/profile/mySettingsModal.cshtml',
                    controller: 'common.views.profile.mySettingsModal as vm',
                    backdrop: 'static'
                });
            };

            vm.changePassword = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/profile/changePassword.cshtml',
                    controller: 'common.views.profile.changePassword as vm',
                    backdrop: 'static'
                });
            };

            vm.changePicture = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/profile/changePicture.cshtml',
                    controller: 'common.views.profile.changePicture as vm',
                    backdrop: 'static'
                });
            };

            vm.changeLanguage = function (languageName) {
                location.href = abp.appPath + 'AbpLocalization/ChangeCulture?cultureName=' + languageName + '&returnUrl=' + window.location.href;
            };

            vm.backToMyAccount = function () {
                abp.ajax({
                    url: abp.appPath + 'Account/BackToImpersonator'
                });
            };

            vm.loadNotifications = function () {
                notificationService.getUserNotifications({
                    maxResultCount: 3
                }).then(function (result) {
                    vm.unreadNotificationCount = result.data.unreadCount;
                    vm.notifications = [];
                    let rx = /<a\s+(?:[^>]*?\s+)?href=(["'])(.*?)\1/;
                    $.each(result.data.items, function (index, item) {                        
                        vm.notifications.push(appUserNotificationHelper.format(item));
                        vm.notifications.forEach(n => {
                            if (n.text.indexOf('<a') > 0) {
                                n.text = item.notification.data.message.split("<a")[0];
                                n.data.message = item.notification.data.message.split("href=")[0] + "href='https://" + item.notification.data.message.split("href='")[1];
                                n.url = n.data.message.match(rx)[2];
                            }
                        })    
                    });
                });
            };            

            vm.setAllNotificationsAsRead = function () {
                appUserNotificationHelper.setAllAsRead();
            };

            vm.setNotificationAsRead = function (userNotification) {
                appUserNotificationHelper.setAsRead(userNotification.userNotificationId);
            };

            vm.openNotificationSettingsModal = function () {
                appUserNotificationHelper.openSettingsModal();
            };

            vm.manageLinkedAccounts = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/common/views/profile/linkedAccountsModal.cshtml',
                    controller: 'common.views.profile.linkedAccountsModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.finally(function () {
                    vm.getRecentlyUsedLinkedUsers();
                });
            };

            vm.getRecentlyUsedLinkedUsers = function () {
                userLinkService.getRecentlyUsedLinkedUsers()
                   .then(function (result) {
                       vm.recentlyUsedLinkedUsers = result.data.items;
                   }).finally(function () {
                       vm.loading = false;
                   });
            };

            vm.getImgLogo = function () {             
                let strNomeFantasia = '';
                if (true) {   

                    strNomeFantasia = vm.getNameFilter(vm.empresaLogada);

                    abp.ajax({
                        url: abp.appPath + 'api/services/app/ezliv/administradora/GetPaginado',
                        type: 'POST',
                        data: JSON.stringify(
                            { nomeFantasia: strNomeFantasia, razaoSocial: strNomeFantasia, skipCount: 0, maxResultCount: 10, sorting: "PessoaJuridica.NomeFantasia" }
                        ),
                        success(data) {   
                            if (data.items.length > 0) {
                                vm.getImgLogoAdm(data);
                                //if (vm.empresaLogada.nomeFantasia.includes('- ADM')) {
                                //    vm.getImgLogoAdm(data);
                                //} else {
                                //    let strName = vm.getNameFilter(vm.empresaLogada);
                                //    vm.getImgLogoCorretora(strName);

                                //    setTimeout(function () {

                                //        if (!vm.logo.myLogo) {
                                //            vm.getImgLogoAdm(data);
                                //        }

                                //    }, 3000);
                                //}
                            } else {
                                vm.getImgLogoCorretora();
                            }
                        },
                        error(error) {       
                            console.log(error);
                        }
                    });
                }
            };

            vm.getImgLogoCorretora = function (str = '') {
                let strNomeFantasia = '';

                if (str != '') {
                    strNomeFantasia = str;
                } else {
                    strNomeFantasia = vm.empresaLogada.nomeFantasia;
                }

                let obj = { url: 'Picture/GetPictureLogoByName', name: strNomeFantasia };

                abp.ajax({
                    url: abp.appPath + obj.url,
                    type: 'POST',
                    data: JSON.stringify({
                        name: obj.name
                    }),
                    success(data) {
                        if (data.alt != "Corretora") {
                            vm.logo.myLogo = true;
                            vm.logo.src = data.src;
                            vm.logo.alt = data.alt;
                        } else {
                            vm.logo.myLogo = false;
                        }
                    },
                    error(error) {
                        console.log(error);
                    }
                });
            };

             function getIsCorretor() {
              
                 abp.ajax({
                     url: abp.appPath + 'Account/GetIsCorretor',
                     data: JSON.stringify({
                     }),  success(data) {
                         if (data == "nao") {
                             vm.IsCorretor = "nao";
                         } else {
                             vm.IsCorretor = "sim";
                         }
                     }
                 });
            };


            vm.getImgLogoAdm = function (data) {
                let id = data.items[0].imagemId
                abp.ajax({
                    url: abp.appPath + 'api/services/app/global/arquivoGlobal/GetById',
                    type: 'POST',
                    data: JSON.stringify({ id }),
                    success(data) {
                        if (data != null) {
                            vm.logo.myLogo = true;
                            vm.logo.src = 'http://' + window.location.host.concat('/' + data.path.replace('\\', '/'));
                            vm.logo.alt = vm.empresaLogada.nomeFantasia;
                        } else {
                            vm.logo.myLogo = false;
                        }
                    },
                    error(error) {
                        console.log(error);
                    }
                });
            };

            vm.getNameFilter = function (empresaLogada) {

                let strNomeFantasia = '';

                if (empresaLogada.nomeFantasia.indexOf('-') > -1) {
                    strNomeFantasia = empresaLogada.nomeFantasia.split('-')[0].trimEnd();
                } else {
                    strNomeFantasia = empresaLogada.nomeFantasia;
                }

                return strNomeFantasia;

            };

            vm.switchToUser = function (linkedUser) {

                abp.ajax({
                    url: abp.appPath + 'Account/SwitchToLinkedAccount',
                    data: JSON.stringify({
                        targetUserId: linkedUser.id,
                        targetTenantId: linkedUser.tenantId
                    })
                });

            };

            vm.showLoginAttempts = function () {
                $uibModal.open({
                    templateUrl: '~/App/common/views/users/loginAttemptsModal.cshtml',
                    controller: 'common.views.users.loginAttemptsModal as vm',
                    backdrop: 'static'
                });
            };

            abp.event.on('abp.notifications.received', function (userNotification) {
                appUserNotificationHelper.show(userNotification);
                vm.loadNotifications();
            });

            abp.event.on('app.notifications.refresh', function () {
                vm.loadNotifications();
            });

            abp.event.on('app.notifications.read', function (userNotificationId) {
                for (var i = 0; i < vm.notifications.length; i++) {
                    if (vm.notifications[i].userNotificationId === userNotificationId) {
                        vm.notifications[i].state = 'READ';
                    }
                }

                vm.unreadNotificationCount -= 1;
            });

            //Chat
            abp.event.on('app.chat.unreadMessageCountChanged', function (messageCount) {
                vm.unreadChatMessageCount = messageCount;
            });

            function init() {
                vm.loadNotifications();
                vm.getRecentlyUsedLinkedUsers();
                vm.getEmpresasPorUsuarioLogado();
                //vm.getImgLogo();
            };

            init();
        }
    ]);
})();