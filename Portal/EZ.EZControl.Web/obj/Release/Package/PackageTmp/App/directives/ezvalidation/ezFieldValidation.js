﻿appModule.directive('ezFieldValidation', function ($q) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, mCtrl) {

            var valueG;
            var chave = "EzFieldValidation" + angular.element(element).attr("ng-model").replace(/[.]/g, "");

            function validaServicoANZ(servico) {
                if (!servico) {
                    throw ("O callback de serviço passado não existe. Ex: abp.services.app.meuServicoSemApp.actionValidaCampo");
                }

                if (servico.toString().search("return abp.ajax") < 0) {
                    throw ("O callback de serviço passado deve ser um serviço gerado pelo aspnetZero. Ex: abp.services.app.meuServicoSemApp.actionValidaCampo");
                }
            };

            function validaArrayDeServicos(listaDeServicos) {
                if (!angular.isArray(listaDeServicos)) {
                    throw ("o retorno do callback passado pelo attributo ezvalidation deve retornar um array de callbacks de serviço de validação. Ex: [ abp.services.app.meuServicoSemApp.actionValidaCampo ]");
                }
                if (listaDeServicos.length == 0) {
                    throw ("o retorno do callback passado pelo attributo ezvalidation deve retornar um array de callbacks com pelo menos um serviço de validação. Ex: [ abp.services.app.meuServicoSemApp.actionValidaCampo ]");
                }
            };

            function validaSeEFuncao(fn) {
                if (!angular.isFunction(fn)) {
                    throw ("Não existe nenhum callback no atributo ezvalidation");
                }
            };

            function disableElement(elem) {
                if (!elem.disabled) {
                    angular.element(elem).attr("disabled", "disabled");
                }
            };

            function enableElement(elem) {
                angular.element(elem).removeAttr("disabled");
            };

            function EzFieldValidation() {

                if (valueG) {

                    var callback = ez.helper.getPropInScopeByString(scope, attr["ezFieldValidation"]);

                    validaSeEFuncao(callback)

                    var listaDeServicos = callback();

                    validaArrayDeServicos(listaDeServicos);

                    var deferred = $q.defer();
                    var promessas = [];

                    for (var i = 0; i < listaDeServicos.length; i++) {

                        var servicoFunc = listaDeServicos[i];

                        validaServicoANZ(servicoFunc);

                        disableElement(element);

                        abp.ui.setBusy($(element).parent());

                        promessas.push(servicoFunc(valueG));

                    }

                    $q.all(promessas).then(function (results) {
                        if (results.length > 0) {
                            var erros = [];
                            var resultFinal = [];

                            for (var i2 = 0; i2 < results.length; i2++) {

                                if (Object.prototype.toString.call(results[i2]) === '[object Array]') {

                                    for (var j = 0; j < results[i2].length; j++) {
                                        resultFinal.push(results[i2][j])
                                    }

                                } else {
                                    resultFinal.push(results[i2]);
                                }
                            }

                            for (var i3 = 0; i3 < resultFinal.length; i3++) {

                                if (resultFinal[i3].className !== "EzFieldValidation") {
                                    throw ("O retorno da validação deve ser o objeto EzFieldValidation ou uma lista do mesmo. Verifique a sua Action do ApplicationService respectivo.");
                                }

                                if (!resultFinal[i3].isValid) {
                                    erros.push(resultFinal[i3]);
                                }
                            }

                            if (erros.length > 0) {

                                var messageError = '';
                                mCtrl.$setValidity(chave, false);

                                for (var i = 0; i < erros.length; i++) {
                                    var name = erros[i].name ? erros[i].name + ": " : "";
                                    messageError += name + erros[i].messageError + "<br />"
                                }

                                abp.notify.warn(messageError, '');

                            } else {
                                mCtrl.$setValidity(chave, true);
                            }
                        }
                    }).finally(function () {
                        enableElement(element);
                        abp.ui.clearBusy($(element).parent());
                    });
                }
            };

            function atualizaValorDigitado(value) {
                valueG = value.toString().replace(/_/, "");
                return value;
            };

            mCtrl.$parsers.push(atualizaValorDigitado);

            angular.element(element).bind("blur", EzFieldValidation);

            angular.element(element).bind("focus", function () {
                if (valueG) {
                    mCtrl.$setValidity(chave, false);
                }
            });

        }
    };
});