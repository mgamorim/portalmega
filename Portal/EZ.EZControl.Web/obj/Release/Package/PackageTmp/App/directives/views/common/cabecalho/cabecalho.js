﻿appModule.directive('cabecalho', function () {
    var title = '';
    var headerInfo = '';

    var compile = function (elem, attrs, transcludeFn) {
        title = attrs['title'];
        headerInfo = attrs['headerInfo'];
        $('ul li.bc-item:last').html(title);
        $('.page-head').hide();
    };

    return {
        restrict: 'E',
        compile: compile,
        scope: {},
        templateUrl: '/App/directives/views/common/cabecalho/cabecalho.cshtml',
        controller: ['$scope', function ($scope) {
            var vm = this;
            vm.title = title;
            vm.headerInfo = headerInfo;
        }],
        controllerAs: 'vm'
    }
});