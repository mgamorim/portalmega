﻿appModule.directive('applyUniform', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (!element.parents(".checker").length) {
                element.show().uniform();
                // update selected item check mark
                setTimeout(function () {
                    $.uniform.update();
                }, 300);
            }
        }
    };
});