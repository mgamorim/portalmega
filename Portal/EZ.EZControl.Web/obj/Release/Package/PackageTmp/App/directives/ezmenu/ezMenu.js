﻿(function () {

    var isAnimating = false,
        isOpen = false;

    function toggleMenu(a, b) {

        var bodyEl = document.body,
            morphEl = document.getElementById('morph-shape'),
            s = Snap(morphEl.querySelector('svg')),
            path = s.select('path'),
            initialPath = path.attr('d'),
            pathOpen = morphEl.getAttribute('data-morph-open');

        if (isAnimating)
            return false;

        isAnimating = true;

        if (isOpen) {
            classie.remove(bodyEl, 'show-menu');
            // animate path
            setTimeout(function () {
                // reset path
                path.attr('d', initialPath);
                isAnimating = false;
            }, 300);
        }
        else {
            classie.add(bodyEl, 'show-menu');
            // animate path
            path.animate({ 'path': pathOpen }, 400, mina.easeinout, function () { isAnimating = false; });
        }

        isOpen = !isOpen;
    };

    appModule.directive('ezmenu', function () {
        "use strict";

        // Usando como exemplo, tem a necessidade de receber esse json através de outros dados.
        function ezMenu($scope) {

            $scope.menu = abp.nav.menus.MainMenu;

            $scope.menuStart = function () {

                // Fix para somente carregar quando existir os elementos
                var si = setInterval(fnMenuStart, 100);

                function fnMenuStart() {
                    var content = document.querySelector('.main-wrapper'),
                        openbtn = document.getElementById('open-menu'),
                        closebtn = document.getElementById('close-menu'),
                        menuToggle = document.querySelector('.ezs-menu-wrapper');

                    if (content && openbtn && closebtn && menuToggle) {
                        content.addEventListener('click',
                            function (ev) {
                                var target = ev.target;
                                if (isOpen || target === openbtn)
                                    toggleMenu();
                            });

                        openbtn.addEventListener('click', toggleMenu);
                        closebtn.addEventListener('click', toggleMenu);
                        menuToggle.addEventListener('click', toggleMenu);

                        // Esc to close menu if it's open
                        window.addEventListener('keyup',
                            function (event) {
                                if (event.keyCode === 27 && isOpen)
                                    toggleMenu();
                            });

                        clearInterval(si);
                    }
                };
            }

            $scope.openBlock = false;
        };

        return {
            restrict: 'E',
            scope: {},
            link: function ($scope, el, attr) {
                $scope.menuStart();
            },
            templateUrl: '/App/directives/ezmenu/ezMenu.html',
            controller: ['$scope', ezMenu]
        }
    });

    appModule.directive('ezMenuOnFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {

                if (element) {
                    element.unbind("click", toggleMenu);
                    element.bind("click", toggleMenu);
                }

                if (scope.$last === true) {
                    //var links = jQuery(".menu-content .item-link");
                    //console.log(links);
                    //links.bind("click", toggleMenu);
                }
            }
        }
    });

})();