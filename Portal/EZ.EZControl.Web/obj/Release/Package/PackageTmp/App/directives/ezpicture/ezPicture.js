﻿appModule.directive('ezpicture', function () {
    "use strict";

    function EzPicture($scope, $uibModal) {
        var vm = this;
        vm.changePicture = function () {
            $uibModal.open({
                templateUrl: '~/App/directives/ezpicture/ezmodalpicture.cshtml',
                controller: 'ezmodalcontroller as vm',
                backdrop: 'static'
            }).result.then(function (id) {
                vm.pictureId = id.$value;
                $('#EzPicture').attr('src', app.getPicturePath(vm.pictureId));
            });
        };

        vm.getPicture = function () {
            $('#EzPicture').attr('src', app.getPicturePath(vm.pictureId));
        };s
    };

    return {
        restrict: 'E',
        scope: {},
        bindToController: {
            pictureId: "=",
            saveCallback: "&"
        },
        templateUrl: '/App/directives/ezpicture/ezpicture.cshtml',
        controller: ['$scope', '$uibModal', EzPicture],
        controllerAs: 'vm'
    }
});