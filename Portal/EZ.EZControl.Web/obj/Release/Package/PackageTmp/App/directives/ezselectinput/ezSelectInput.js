﻿appModule.directive('ezselectinput', function () {
    "use strict";

    function ezSelectInput($scope) {
        $scope.selected = $scope.ezPlaceholder;
        $scope.options = $scope.ezOptions;

        console.debug($scope.ezOptions);
        console.debug($scope);
        $scope.select = function (item) {
            $scope.selected = item.descricao;
        };
    };

    return {
        restrict: 'E',
        scope: {
            ezOptions: '=ezOptions',
            ngModel: '=ngModel',
            ezPlaceholder: '@'
        },
        replace: false,
        templateUrl: '/App/directives/ezselectinput/ezSelectInput.html',
        controller: ['$scope', ezSelectInput]
    }
});