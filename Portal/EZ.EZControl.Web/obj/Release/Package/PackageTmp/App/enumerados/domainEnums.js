﻿window.ez = ez || {};
ez.domain = ez.domain || {};
ez.domain.localize = app.localize;
ez.domain.enums = (function () {
    var self = this;

    this.tipoPessoa = [
        { descricao: 'Pessoa Física', valor: 1, valorTexto: 'fisica' },
        { descricao: 'Pessoa Jurídica', valor: 2, valorTexto: 'juridica' }
    ];

    this.tipoDocumentoFixo = [
        { descricao: 'Cpf', valor: 1, tipoPessoa: function () { return self.tipoPessoa[0]; } },
        { descricao: 'Cnpj', valor: 2, tipoPessoa: function () { return self.tipoPessoa[1]; } },
        { descricao: 'Rg', valor: 3, tipoPessoa: function () { return self.tipoPessoa[0]; } },
        { descricao: 'Passaporte', valor: 4, tipoPessoa: function () { return self.tipoPessoa[0]; } },
        { descricao: 'Carteira de trabalho', valor: 5, tipoPessoa: function () { return self.tipoPessoa[0]; } },
        { descricao: 'Pis', valor: 6, tipoPessoa: function () { return self.tipoPessoa[0]; } }

    ];

    this.tipoDeContatoFixo = [
        { descricao: "E-mail", valor: 1, valorTexto: 'email' },
        { descricao: "Telefone", valor: 2, valorTexto: 'telefone' },
        { descricao: "Fax", valor: 3, valorTexto: 'fax' },
        { descricao: "Outros", valor: 4, valorTexto: 'outros' }
    ];

    this.tipoDeMenuFixo = [
        { descricao: "Pagina", valor: 1, valorTexto: 'pagina' },
        { descricao: "Link", valor: 2, valorTexto: 'link' },
        { descricao: "Categoria", valor: 3, valorTexto: 'categoria' }
    ];

    this.tipoDeArquivoFixo = [
       { descricao: "PDF", valor: 1, valorTexto: '.pdf' },
       { descricao: "TXT", valor: 2, valorTexto: '.txt' },
       { descricao: "XLS", valor: 3, valorTexto: '.xls, .xlsx' },
       { descricao: "DOC", valor: 4, valorTexto: '.doc, .docx' },
       { descricao: "JPG", valor: 5, valorTexto: '.jpg, .jpeg' },
       { descricao: "PNG", valor: 6, valorTexto: '.png' },
       { descricao: "GIF", valor: 7, valorTexto: '.gif' },
       { descricao: "PPT", valor: 8, valorTexto: '.ppt, .pptx' },
    ];

    this.tipoDeArquivoDeImagens = [
       { descricao: "JPG", valor: 5, valorTexto: '.jpg, .jpeg' },
       { descricao: "PNG", valor: 6, valorTexto: '.png' },
       { descricao: "GIF", valor: 7, valorTexto: '.gif' }
    ];

    this.tipoDeArquivoDeDocumentos = [
       { descricao: "PDF", valor: 1, valorTexto: '.pdf' },
       { descricao: "TXT", valor: 2, valorTexto: '.txt' },
       { descricao: "XLS", valor: 3, valorTexto: '.xls, .xlsx' },
       { descricao: "DOC", valor: 4, valorTexto: '.doc, .docx' },
       { descricao: "PPT", valor: 8, valorTexto: '.ppt, .pptx' }
    ]

    this.tipoDeTemplateDeMensagemFixo = [
        { descricao: "Boas Vindas", valor: 1, valorTexto: '{Modulo} {NomeDoUsuario}' },
        { descricao: "Despedida", valor: 2, valorTexto: '{Modulo} {NomeDoUsuario} {Mensagem}' }
    ];

    this.tipoDeEnderecoEletronico = [
        { descricao: 'E-mail', valor: 1, valorTexto: 'email' },
        { descricao: 'E-mail NFe', valor: 2, valorTexto: 'emailNfe' },
        { descricao: 'E-mail Faturamento', valor: 3, valorTexto: 'emailFaturamento' },
        { descricao: 'WebSite', valor: 4, valorTexto: 'webSite' },
        { descricao: 'Blog', valor: 5, valorTexto: 'blog' },
        { descricao: 'MSN', valor: 6, valorTexto: 'msn' },
        { descricao: 'Twiter', valor: 7, valorTexto: 'twiter' },
        { descricao: 'Facebook', valor: 8, valorTexto: 'facebook' },
        { descricao: 'LinkedIn', valor: 9, valorTexto: 'linkedIn' },
        { descricao: 'IMessage', valor: 10, valorTexto: 'iMessage' },
        { descricao: 'Outros', valor: 11, valorTexto: 'outros' },
        { descricao: 'E-mail Principal', valor: 12, valorTexto: 'emailPrincipal' }
    ];

    this.estados = [
        { nome: 'Acre', sigla: 'AC' },
        { nome: 'Alagoas', sigla: 'AL' },
        { nome: 'Amapá', sigla: 'AP' },
        { nome: 'Amazonas', sigla: 'AM' },
        { nome: 'Bahia', sigla: 'BA' },
        { nome: 'Ceará', sigla: 'CE' },
        { nome: 'Distrito Federal', sigla: 'DF' },
        { nome: 'Espírito Santo', sigla: 'ES' },
        { nome: 'Goiás', sigla: 'GO' },
        { nome: 'Maranhão', sigla: 'MA' },
        { nome: 'Mato Grosso', sigla: 'MT' },
        { nome: 'Mato Grosso do Sul', sigla: 'MS' },
        { nome: 'Minas Gerais', sigla: 'MG' },
        { nome: 'Pará', sigla: 'PA' },
        { nome: 'Paraíba', sigla: 'PB' },
        { nome: 'Paraná', sigla: 'PR' },
        { nome: 'Pernambuco', sigla: 'PE' },
        { nome: 'Piauí', sigla: 'PI' },
        { nome: 'Rio de Janeiro', sigla: 'RJ' },
        { nome: 'Rio Grande do Norte', sigla: 'RN' },
        { nome: 'Rio Grande do Sul', sigla: 'RS' },
        { nome: 'Rondônia', sigla: 'RO' },
        { nome: 'Roraima', sigla: 'RR' },
        { nome: 'Santa Catarina', sigla: 'SC' },
        { nome: 'São Paulo', sigla: 'SP' },
        { nome: 'Sergipe', sigla: 'SE' },
        { nome: 'Tocantins', sigla: 'TO' }
    ];

    this.abrangenciaDoFeriado = [
        { descricao: "Mundial", valor: 1 },
        { descricao: "Nacional", valor: 2 },
        { descricao: "Estadual", valor: 3 },
        { descricao: "Municipal", valor: 4 },
    ];

    this.tipoDeFeriado = [
        { descricao: "Bancário", valor: 1 },
        { descricao: "Classista", valor: 2 },
        { descricao: "Extraordinário", valor: 3 },
        { descricao: "Ordinário", valor: 4 },
        { descricao: "Ponto Facultativo", valor: 5 }
    ];

    this.getTipoPessoaPorValor = function (value) {
        for (var i = 0; i < self.tipoPessoa.length; i++) {
            if (self.tipoPessoa[i].valor == value)
                return self.tipoPessoa[i];
        }
    };

    this.getTipoDeContatoPorValor = function (value) {
        for (var i = 0; i < self.tipoDeContatoFixo.length; i++) {
            if (self.tipoDeContatoFixo[i].valor == value)
                return self.tipoDeContatoFixo[i];
        }
    };

    this.getTipoDocumentoFixoPorValor = function (value) {
        for (var i = 0; i < self.tipoDocumentoFixo.length; i++) {
            if (self.tipoDocumentoFixo[i].valor == value)
                return self.tipoDocumentoFixo[i];
        }
    };

    this.getTipoDeArquivoFixo = function (value) {
        for (var i = 0; i < self.tipoDeArquivoFixo.length; i++) {
            if (self.tipoDeArquivoFixo[i].valor == value)
                return self.tipoDeArquivoFixo[i];
        }
    };

    this.getTipoDeTemplateDeMensagem = function (value) {
        for (var i = 0; i < self.tipoDeTemplateDeMensagemFixo.length; i++) {
            if (self.tipoDeTemplateDeMensagemFixo[i].valor == value)
                return self.tipoDeTemplateDeMensagemFixo[i];
        }
    };

    this.getTipoDeEnderecoEletronicoPorValor = function (value) {
        for (var i = 0; i < self.tipoDeEnderecoEletronico.length; i++) {
            if (self.tipoDeEnderecoEletronico[i].valor == value)
                return self.tipoDeEnderecoEletronico[i];
        }
    };

    this.getTipoDeDocumentosPorPessoa = function (value) {
        var tipoDeDocumentos = [];

        if (value == undefined)
            return tipoDeDocumentos;

        for (var i = 0; i < self.tipoDocumentoFixo.length; i++) {
            if (self.tipoDocumentoFixo[i].tipoPessoa().valor == value || self.tipoDocumentoFixo[i].tipoPessoa().valorTexto == value) {
                tipoDeDocumentos.push(self.tipoDocumentoFixo[i]);
            }
        }

        return tipoDeDocumentos;
    };

    return this;
})();