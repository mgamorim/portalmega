﻿using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EZ.EZControl.Web.Models.Relatorio
{
    public class RelatorioViewModel
    {
        public int RelatorioId { get; set; }
        public object DadosDoRelatorio { get; set; }
    }
}