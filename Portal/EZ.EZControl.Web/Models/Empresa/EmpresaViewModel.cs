﻿using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Web.Models.Empresa
{
    public class EmpresaViewModel
    {
        [Required]
        public int EmpresaId { get; set; }
    }
}