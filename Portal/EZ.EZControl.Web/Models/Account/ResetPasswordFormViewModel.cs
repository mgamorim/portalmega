﻿using Abp.Auditing;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Web.Models.Account
{
    public class ResetPasswordFormViewModel
    {
        /// <summary>
        /// Encrypted tenant id.
        /// </summary>
        public string TenantId { get; set; }

        /// <summary>
        /// Encrypted user id.
        /// </summary>
        [Required]
        public string UserId { get; set; }

        [Required]
        public string ResetCode { get; set; }

        
        public string slug { get; set; }

        [Required(ErrorMessage = "Tamanho mínimo digititados é de 6 digitos!")]
        [DisableAuditing]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}