﻿using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using System.Collections.Generic;

namespace EZ.EZControl.Web.Models.EZLiv
{
    public class RegistroEzlivViewModel
    { 
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string slugCorretora { get; set; }
        public TipoDeUsuarioEnum TipoDeUsuario { get; set; }
    }
}