﻿//using Abp.Modules;
//using Abp.Web;
//using Abp.Web.Mvc;
//using Abp.WebApi;
//using EZ.EZControl.CMS.Infra;
//using System.Reflection;
//using System.Web.Mvc;
//using System.Web.Optimization;
//using System.Web.Routing;

//namespace EZ.EZControl.CMS.Frontend
//{
//    [DependsOn(typeof(AbpWebModule), typeof(AbpWebMvcModule), typeof(AbpWebApiModule))]
//    public class CMSFrontEndModule : AbpModule
//    {
//        public override void PreInitialize()
//        {
//        }

//        public override void Initialize()
//        {
//            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

//            AreaRegistration.RegisterAllAreas();
//            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
//            RouteConfig.RegisterRoutes(RouteTable.Routes);
//            BundleConfig.RegisterBundles(BundleTable.Bundles);
//        }

//        public override void PostInitialize()
//        {
//        }
//    }
//}