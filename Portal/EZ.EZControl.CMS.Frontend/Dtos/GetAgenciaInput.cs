﻿namespace EZ.EZControl.CMS.Frontend.Dtos
{
    public class GetAgenciaInput
    {
        public string CodigoBanco { get; set; }
        public string NomeBanco { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
    }
}