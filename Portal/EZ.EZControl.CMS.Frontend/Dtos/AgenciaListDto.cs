﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;

namespace EZ.EZControl.CMS.Frontend.Dtos
{
    public class AgenciaListDto : FullAuditedEntityDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string DigitoVerificador { get; set; }
        public int BancoId { get; set; }
    }
}