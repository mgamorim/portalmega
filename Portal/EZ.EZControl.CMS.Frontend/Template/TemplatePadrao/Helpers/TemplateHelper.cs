﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Link;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Widget;
using EZ.EZControl.CMS.Frontend.Infra.Entities.Enums;
using EZ.EZControl.CMS.Frontend.Infra.Enums;
using EZ.EZControl.CMS.Frontend.Infra.Extensions;
using EZ.EZControl.CMS.Frontend.Infra.Services;
using System.Text;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Helpers
{
    public partial class TemplateHelper
    {
        public static class TemplatePadrao
        {
            public static string GetTopMenu(string posicao, HttpRequestBase request)
            {
                var topMenu = GetWidget(posicao, request);
                StringBuilder sb = new StringBuilder();
                if (topMenu != null)
                {
                    if (topMenu.Tipo != TipoDeWidgetEnum.CmsMenu)
                    {
                        //TODO: Levantar exceção quando o widget não for o esperado
                    }
                    sb.AppendLine(MontaMenu(topMenu));
                }
                return sb.ToString();
            }
            
            public static string GetWidgets(string posicao, HttpRequestBase request)
            {
                var widgets = GetWidgetList(posicao, request);
                StringBuilder sb = new StringBuilder();
                foreach(var widget in widgets)
                {
                    switch (widget.Tipo)
                    {
                        case TipoDeWidgetEnum.CmsHtml:
                            {
                                sb.AppendLine(widget.Conteudo);
                                break;
                            }
                        case TipoDeWidgetEnum.CmsMenu:
                            {
                                sb.AppendLine(MontaMenu(widget));
                                break;
                            }
                    }
                }
                return sb.ToString();
            }

            private static string MontaMenu(WidgetFrontInput topMenu)
            {
                LinkService linkService = new LinkService();

                StringBuilder sb = new StringBuilder();

                sb.AppendLine("<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'><ul class='nav navbar-nav navbar-right'>");
                sb.AppendLine(@"<li class='hidden active'><a href ='#page-top'></a></li>");

                var siteId = topMenu.SiteId;

                foreach (var item in topMenu.Menu.ItensDeMenu)
                {
                    var href = string.Empty;
                    var titulo = string.Empty;

                    switch (item.TipoDeItemDeMenu)
                    {
                        case TipoDeItemDeMenuEnum.Link:
                            {
                                href = item.Url;
                                titulo = item.Titulo;
                                break;
                            }
                        case TipoDeItemDeMenuEnum.Categoria:
                            {
                                var task = linkService.GetLink(new LinkFrontInput() { SiteId = siteId, TipoDeLink = TipoDeLinkEnum.Categoria, Titulo = item.Titulo, Slug = item.Titulo.Slugify() });
                                href = task.Result.Link;
                                titulo = item.Titulo;
                                break;
                            }
                        case TipoDeItemDeMenuEnum.Pagina:
                            {
                                var task = linkService.GetLink(new LinkFrontInput() { SiteId = siteId, TipoDeLink = TipoDeLinkEnum.Pagina, Titulo = item.Titulo, Slug = item.Titulo.Slugify() });
                                href = task.Result.Link;
                                titulo = item.Titulo;
                                break;
                            }
                    }

                    sb.AppendLine(string.Format(@"<li><a class='page-scroll' href='{0}'>{1}</a></li>", href, titulo));
                }

                sb.AppendLine("</ul></div>");
                return sb.ToString();
            }
        }
    }
}