﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Template.DoutorEz.Helpers
{
    public class SearchHelper
    {
        public static string GetUrlSearchItemByPeriodoPesquisaEmprego(HttpRequestBase request, int value)
        {
            var nameValues = HttpUtility.ParseQueryString(request.QueryString.ToString());
            nameValues.Set("ppe", value.ToString());
            return String.Concat("?", nameValues.ToString());
        }

        //public static HtmlString GetIconOkByPeriodoPesquisaEmprego(HttpRequestBase request, int value)
        //{
        //    var ppeQs = request.QueryString["ppe"];
        //    string result = String.Empty;
        //    if (!string.IsNullOrEmpty(ppeQs))
        //    {
        //        if (ppeQs.To<int>() >= 0)
        //        {
        //            var ppe = ppeQs.To<int>();
        //            var ppeEnum = (PeriodoPesquisaEmpregoEnum)ppe;

        //            if (ppeEnum == (PeriodoPesquisaEmpregoEnum)value)
        //            {
        //                result = "<b class='glyphicon glyphicon-ok'></b>";
        //            }
        //        }
        //    }
        //    else if ((PeriodoPesquisaEmpregoEnum)value == PeriodoPesquisaEmpregoEnum.QualquerPeriodo)
        //    {
        //        result = "<b class='glyphicon glyphicon-ok'></b>";
        //    }
        //    return new HtmlString(result);
        //}

        //public static HtmlString GetNamePeriodoPesquisaEmprego(HttpRequestBase request)
        //{
        //    var ppeQs = request.QueryString["ppe"];
        //    string seta = "<b class='caret'></b>";
        //    string result;
        //    if (!string.IsNullOrWhiteSpace(ppeQs))
        //    {
        //        if (ppeQs.To<int>() > 0)
        //        {
        //            var ppe = ppeQs.To<int>();
        //            var ppeEnum = (PeriodoPesquisaEmpregoEnum)ppe;

        //            if (ppeEnum == PeriodoPesquisaEmpregoEnum.Hoje)
        //            {
        //                result = string.Concat("<b>", LocalizationHelper
        //                    .GetString(QualiJobsConsts.LocalizationSourceName,
        //                        "Enum_PeriodoPesquisaEmpregoEnum_Hoje"), "</b>", seta);
        //            }
        //            else if (ppeEnum == PeriodoPesquisaEmpregoEnum.Ontem)
        //            {
        //                result = string.Concat("<b>", LocalizationHelper
        //                    .GetString(QualiJobsConsts.LocalizationSourceName,
        //                        "Enum_PeriodoPesquisaEmpregoEnum_Ontem"), "</b>", seta);
        //            }
        //            else if (ppeEnum == PeriodoPesquisaEmpregoEnum.UltimaSemana)
        //            {
        //                result = string.Concat("<b>", LocalizationHelper
        //                    .GetString(QualiJobsConsts.LocalizationSourceName,
        //                        "Enum_PeriodoPesquisaEmpregoEnum_UltimaSemana"), "</b>", seta);
        //            }
        //            else if (ppeEnum == PeriodoPesquisaEmpregoEnum.UltimoMes)
        //            {
        //                result = string.Concat("<b>", LocalizationHelper
        //                    .GetString(QualiJobsConsts.LocalizationSourceName,
        //                        "Enum_PeriodoPesquisaEmpregoEnum_UltimoMes"), "</b>", seta);
        //            }
        //            else if (ppeEnum == PeriodoPesquisaEmpregoEnum.UltimoAno)
        //            {
        //                result = string.Concat("<b>", LocalizationHelper
        //                    .GetString(QualiJobsConsts.LocalizationSourceName,
        //                        "Enum_PeriodoPesquisaEmpregoEnum_UltimoAno"), "</b>", seta);
        //            }
        //            else
        //            {
        //                result = string.Concat(LocalizationHelper
        //                    .GetString(QualiJobsConsts.LocalizationSourceName,
        //                        "Enum_PeriodoPesquisaEmpregoEnum_QualquerPeriodo"), seta);
        //            }
        //        }
        //        else
        //        {
        //            result = string.Concat(LocalizationHelper
        //                .GetString(QualiJobsConsts.LocalizationSourceName,
        //                    "Enum_PeriodoPesquisaEmpregoEnum_QualquerPeriodo"), seta);
        //        }
        //    }
        //    else
        //    {
        //        result = string.Concat(LocalizationHelper
        //            .GetString(QualiJobsConsts.LocalizationSourceName,
        //                "Enum_PeriodoPesquisaEmpregoEnum_QualquerPeriodo"), seta);
        //    }

        //    return new HtmlString(result);
        //}

        public static HtmlString GetIconChecadoQualquerLocal(HttpRequestBase request)
        {
            string result = String.Empty;
            var cidadeId = request.QueryString["c"];

            if (string.IsNullOrWhiteSpace(cidadeId))
            {
                result = "<b class='glyphicon glyphicon-ok set'></b>";
            }

            return new HtmlString(result);
        }

        public static string GetUrlQualquerLocal(HttpRequestBase request)
        {
            var nameValues = HttpUtility.ParseQueryString(request.QueryString.ToString());
            nameValues.Set("l", "");
            nameValues.Set("c", "");
            return String.Concat("?", nameValues.ToString());
        }

        //public static HtmlString GetNameCidadeEscolhida(CidadeFrontInputDto cidadeEscolhida)
        //{
        //    string cidade;
        //    if (cidadeEscolhida != null)
        //    {
        //        cidade = string.Concat("<b>", cidadeEscolhida.Nome, ", ", cidadeEscolhida.Estado.Sigla, "</b>");
        //    }
        //    else
        //    {
        //        cidade = LocalizationHelper
        //            .GetString(QualiJobsConsts.LocalizationSourceName,
        //            "Frontend.QualquerLocal");
        //    }


        //    return new HtmlString(cidade);
        //}

        public static string GetUrlItemPaginacao(HttpRequestBase request, int numPagina)
        {
            var nameValues = HttpUtility.ParseQueryString(request.QueryString.ToString());
            var start = numPagina * AppConsts.DefaultPageSize - AppConsts.DefaultPageSize;
            nameValues.Set("start", start.ToString());
            return String.Concat("?", nameValues.ToString());
        }

        //public static string GetUrlItemCidade(HttpRequestBase request, EnderecoFrontInputDto endereco)
        //{
        //    var nameValues = HttpUtility.ParseQueryString(request.QueryString.ToString());
        //    nameValues.Set("c", endereco.CidadeId.ToString());
        //    nameValues.Set("q", "");
        //    nameValues.Set("ppe", "");
        //    nameValues.Set("start", "");
        //    return String.Concat("?", nameValues.ToString());
        //}

        //public static string GetNameCidadeEstado(CidadeFrontInputDto cidade)
        //{
        //    return string.Format("{0}, {1}", cidade.Nome, cidade.Estado.Sigla);
        //}

        public static HtmlString Truncate(string empregoDescricao, int maxChars)
        {
            return new HtmlString(empregoDescricao.Length <= maxChars ? empregoDescricao : empregoDescricao.Substring(0, maxChars) + "...");
        }

        //public static string GetDescricaoDataVaga(DateTime empregoCreationTime)
        //{
        //    var dias = DateTime.Today.AddDays(1).Subtract(empregoCreationTime).Days;
        //    string result;

        //    if (dias == 0)
        //    {
        //        result = LocalizationHelper
        //            .GetString(QualiJobsConsts.LocalizationSourceName,
        //            "Frontend.PublicadaHoje");
        //    }
        //    else if (dias == 1)
        //    {
        //        result = LocalizationHelper
        //            .GetString(QualiJobsConsts.LocalizationSourceName,
        //            "Frontend.PublicadaOntem");
        //    }
        //    else
        //    {
        //        result = string.Format(LocalizationHelper
        //            .GetString(QualiJobsConsts.LocalizationSourceName,
        //            "Frontend.publicadaHaNDias"), dias);
        //    }

        //    return result;
        //}

        //public static string GetUrlChangeLang(HttpRequestBase request, PaisFrontListDto pais)
        //{
        //    var nameValues = HttpUtility.ParseQueryString(request.QueryString.ToString());
        //    var path = request.Url.GetLeftPart(UriPartial.Path);
        //    nameValues.Set("c", "");
        //    nameValues.Set("l", "");
        //    nameValues.Set("start", "");
        //    nameValues.Set("lang", pais.Language);
        //    return String.Concat("?", nameValues.ToString());
        //}

        public static string GetLangCulture(HttpRequestBase request)
        {
            if (string.IsNullOrWhiteSpace(request.QueryString["lang"]))
            {
                var langCookie = request.Cookies["lang"];
                if (langCookie != null)
                {
                    return langCookie.Value;
                }
                return request.UserLanguages.FirstOrDefault();
            }
            return request.QueryString["lang"];
        }

        //public static HtmlString GetPaisSelecionado(HttpRequestBase request, IEnumerable<PaisFrontListDto> paises)
        //{
        //    string cultureNameSelected;
        //    var langCookie = request.Cookies["lang"];
        //    if (langCookie != null)
        //    {
        //        cultureNameSelected = langCookie.Value;
        //    }
        //    else
        //    {
        //        cultureNameSelected = request.QueryString["lang"];
        //    }

        //    var paisDto = paises.FirstOrDefault(x => x.Language == cultureNameSelected);

        //    if (paisDto != null)
        //    {
        //        return new HtmlString(string.Format("<span class='flag flag-{0}' data-pais='{1}' data-sigla='{2}' data-lang='{3}'></span> <span class='caret'></span>",
        //            paisDto.Sigla2.ToLower(),
        //            paisDto.Id,
        //            paisDto.Sigla2,
        //            paisDto.Language));
        //    }

        //    return new HtmlString("<span class='flag flag-br' data-pais='1' data-sigla='BR'></span> <span class='caret'></span>");
        //}

        //public static HtmlString GetPaisOpcoes(HttpRequestBase request, IEnumerable<PaisFrontListDto> paises)
        //{
        //    var langCookie = request.Cookies["lang"];
        //    var cultureNameSelected = langCookie != null ? langCookie.Value : request.QueryString["lang"];

        //    var sbResult = new StringBuilder();
        //    var cultureName = string.IsNullOrWhiteSpace(cultureNameSelected)
        //        ? "pt-BR"
        //        : cultureNameSelected;
        //    paises.Where(x => x.Language != cultureName).ForEach((pais) =>
        //    {
        //        sbResult.AppendFormat("<li><a href='javascript:void(0);'><span class='flag flag-{0}' data-pais='{1}' data-sigla='{2}' data-lang='{3}'></span><span class='text'>{2}</span></a></li>",
        //                pais.Sigla2.ToLower(),
        //                pais.Id,
        //                pais.Sigla2,
        //                pais.Language);
        //    });
        //    return new HtmlString(sbResult.ToString());
        //}
    }
}