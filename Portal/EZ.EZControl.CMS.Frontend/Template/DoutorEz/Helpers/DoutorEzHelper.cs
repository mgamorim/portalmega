﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace EZ.EZControl.CMS.Frontend.Template.DoutorEz.Helpers
{
    public static class QualiJobsHelper
    {
        public static bool IsValidCultureCode(string cultureCode)
        {
            if (string.IsNullOrWhiteSpace(cultureCode))
            {
                return false;
            }

            try
            {
                CultureInfo.GetCultureInfo(cultureCode);
                return true;
            }
            catch (CultureNotFoundException)
            {
                return false;
            }
        }

        public static string GetClassActiveMenuTermos(string termsViewContext, string terms)
        {
            return termsViewContext == terms ? "active" : String.Empty;
        }
    }
}