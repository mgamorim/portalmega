String.prototype.fmt = function () {
    var args = arguments;
    return this.replace(/\{(\d+)\}/g, function ($0, $1) {
        return args[$1] !== void 0 && args[$1] !== null ? args[$1] : '';
    });
};

function createCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
}

function deleteCookie(name) {
    createCookie(name, "", -1);
}   

$(document).ready(function(){

    $('#options-left > .dropdown').click(function (e) {
        if ($('[mobile]').attr('mobile') === '1') {
            var left = e.pageX - parseInt(this.clientWidth / 1.5);
            $('#options-left > .dropdown > ul').css({
                'position': 'fixed',
                'left': left,
                'top': parseInt($('#bar').height()) + parseInt($('#options').height())
            });
        } else {
            var fixFirstDiv = ($(this).hasClass('local')) ? -20 : 0;
            var left = this.offsetLeft + fixFirstDiv;
            $('#options-left > .dropdown > ul').css({
                'position': 'fixed',
                'left': left,
                'top': parseInt($('#bar').height()) + parseInt($('#options').height())
            });
        }
    });

    $('#options-left form.option-search button.psearch').click(function (e) {
        e.stopPropagation();
        $('#options-left .menu-flag.psearch').toggleClass('open').trigger('shown.bs.dropdown');
    });

    $('#options-left .menu-flag.psearch').click(function () {
        $('#options-left .menu-flag.psearch').trigger('hide.bs.dropdown');
    });

    $('.dropdown').on('hide.bs.dropdown', function (e) {
        $('#options-left .menu-flag.psearch').removeClass('open');
    });

    var seletor = $('.content-psearch-pagination .pagination > li > a');
    seletor.each(function(i,e){
        if ($(seletor[i]).attr('rel') === 'prev')
            $(seletor[i]).html('<i class="glyphicon glyphicon-circle-arrow-left"></i>');
        if ($(seletor[i]).attr('rel') === 'next')
            $(seletor[i]).html('<i class="glyphicon glyphicon-circle-arrow-right"></i>')
    });

    (function configuraSelectPaisNasPaginas() {
        function setCountrySearch(e) {
            var paisASelecionar = $(e);
            var textASelecionar = paisASelecionar.text();
            var paisASelecionarTodos = $(".menu-flag li a");
            var piSetASelecionar = paisASelecionar.children().attr("data-pais");
            var siglaASelecionar = paisASelecionar.children().attr("data-sigla");
            var langASelecionar = paisASelecionar.children().attr("data-lang");

            var paisSelecionadoButtonSpan = $('form.search button.psearch > span.flag');
            var piSetSelecionado = paisSelecionadoButtonSpan.attr("data-pais");
            var siglaASelecionado = paisSelecionadoButtonSpan.attr("data-sigla");
            var langASelecionado = paisSelecionadoButtonSpan.attr("data-lang");

            paisASelecionar.parent().prependTo(paisASelecionar.parent().parent());

            paisSelecionadoButtonSpan.removeClass().addClass("flag flag-" + textASelecionar.toLocaleLowerCase());
            paisSelecionadoButtonSpan.attr("data-pais", piSetASelecionar);
            paisSelecionadoButtonSpan.attr("data-sigla", siglaASelecionar);
            paisSelecionadoButtonSpan.attr("data-lang", langASelecionar);

            var paisASelecionarTodosSpansDasLi = paisASelecionarTodos
                .find("[data-pais].flag-" + textASelecionar.toLocaleLowerCase());
            paisASelecionarTodosSpansDasLi.removeClass().addClass("flag flag-" + siglaASelecionado.toLocaleLowerCase());
            paisASelecionarTodosSpansDasLi.attr("data-pais", piSetSelecionado);
            paisASelecionarTodosSpansDasLi.attr("data-sigla", siglaASelecionado);
            paisASelecionarTodosSpansDasLi.attr("data-lang", langASelecionado);
            paisASelecionarTodos.find(".text").text(siglaASelecionado);

            $("form.search input[name=p]").val(textASelecionar);
            $("form.search input[name=pi]").val(piSetASelecionar);
            $("form.search input[name=lang]").val(langASelecionar);

            createCookie("p", textASelecionar, 30);
            createCookie("pi", piSetASelecionar, 30);

            // Desmarca e coloca Qualquer local novamente na op��o local no menu options
            $("#options-left > .dropdown.first > a").html($("#options .dropdown-menu.local > li:first").text() + "<span class='caret'></span>");
        }

        function unSetCidade(e) {
            var paisSelecionado = $(e).children().attr("data-pais");
            var paisSetadoAtualmente = $('form.search button.psearch > span.flag').attr("data-pais");
            if (paisSelecionado !== paisSetadoAtualmente) {
                $("form.search input[name=l]").val("");
                $("form.search input[name=c]").val("");
            }
        }

        $("form.search ul.psearch > li > a")
            .click(function () {
                unSetCidade(this);
                setCountrySearch(this);
            });

        // Por padr�o coloca o idioma que estiver na tag html
        $("form.search input[name=lang]").val($("html").attr("lang"));

        //N�o Est� sendo mais pelo cookie
        //var p = $("form.search input[name=p]").val();
        //var pi = $('form.search button.psearch > span.flag').attr("data-pais");
        //var pCookie = readCookie("p");
        //var piCookie = readCookie("pi");

        //if (pCookie && piCookie) {
        //    setCountrySearch($('form.search ul.psearch > li > a > span.flag-' + pCookie.toLocaleLowerCase()).parent()[0]);
        //} else if (p && pi) {
        //    setCountrySearch($('form.search ul.psearch > li > a > span.flag-' + p.toLocaleLowerCase()).parent()[0]);
        //}
    })();

    //$("[data-toggle=tooltip]").tooltip();

});

var typeXMLEnum = new function() {
    this.rss = 1;
    this.atom = 2;
    this.proprio = 3;
    this.indeed = 4;
    this.trovit = 5;
    this.monster = 6;
    this.simplyhired = 7;
};

function autoComplete(seletor) {
    $(seletor).autocomplete({
        delay: 250,
        source: function (request, response) {

            var pi = $('form.search button.psearch > span.flag').attr("data-pais");
            var langB = $("form.search input[name=lang]").val();

            $.ajax({
                url: "/api/services/app/cidadefront/GetAutoComplete",
                type: "POST",
                dataType: "json",
                data: "nome=" + request.term + "&pi=" + pi + "&lang=" + langB,
                success: function (data) {
                    response(data.result.reverse());
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {

            setTimeout(function() {
                $("form.search input[name=l]").val(ui.item.label);
            }, 0);

            if (event.target.id === "local2") {
                $("#options-left > .dropdown.first").text(ui.item.label + "...");
            }

            $("form.search input[name=c]").val(ui.item.id);

            setTimeout(function () {
                if (event.target.id === "local2") {
                    $("form.search").submit();
                }
            }, 0);
        },
        open: function() {
            $('.ui-autocomplete').removeClass();
            $('#ui-id-1 a').removeClass();
            $('#ui-id-1').addClass('dropdown-menu');
            $('#ui-id-1').css('width','auto');
            $('.ui-helper-hidden-accessible').remove();
        },
        close: function() {

        }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        var regex = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        return $( "<li></li>" )
            .data( "ui-autocomplete-item", item )
            .prepend( "<a>" + item.label.replace(regex, "<b class='local-style'>$1</b>") + "</a>" )
            .prependTo( ul );
    };
}

function onChangeTypeXML(element, value) {

    value ? element.value = value : null;
    var valor = element.value === "" ? 0 : parseInt(element.value);

    if (valor !== 0) {

        $('form.register .url input').attr("required","true");
        $('form.register .url').fadeIn();

        if(valor === 1 || valor === 2) {
            $('form.register .city-state select').attr("required","true");
            $('form.register .city-state').fadeIn();
        } else {
            $('form.register .city-state select').removeAttr("required");
            $('form.register .city-state').fadeOut();
        }

    } else {
        $('form.register .url input').removeAttr("required");
        $('form.register .url').fadeOut();

        $('form.register .city-state select').removeAttr("required");
        $('form.register .city-state').fadeOut();
    }
}

function onChangeState(element, idCity) {

    $('.city-state #city').empty().append("<option>Loading...</option>");

    $.ajax({
        type: "get",
        url: '/search/ajaxCityByState/' + element.value,
        success: function(results) {

            var cities = JSON.parse(results);
            var html = "";

            for(var i in cities ){
                var defaulOption = i == 0 ? 'default' : '';
                html += '<option value="{0}" {1}>{2}</option>'.fmt(i, defaulOption, cities[i]);
            }

            $('.city-state #city').empty().append(html);

            if (idCity) {
                document.getElementById("city").value = idCity;
            }
        }
    });

}

/* Placeholders.js v3.0.2 */
(function(t){"use strict";function e(t,e,r){return t.addEventListener?t.addEventListener(e,r,!1):t.attachEvent?t.attachEvent("on"+e,r):void 0}function r(t,e){var r,n;for(r=0,n=t.length;n>r;r++)if(t[r]===e)return!0;return!1}function n(t,e){var r;t.createTextRange?(r=t.createTextRange(),r.move("character",e),r.select()):t.selectionStart&&(t.focus(),t.setSelectionRange(e,e))}function a(t,e){try{return t.type=e,!0}catch(r){return!1}}t.Placeholders={Utils:{addEventListener:e,inArray:r,moveCaret:n,changeType:a}}})(this),function(t){"use strict";function e(){}function r(){try{return document.activeElement}catch(t){}}function n(t,e){var r,n,a=!!e&&t.value!==e,u=t.value===t.getAttribute(V);return(a||u)&&"true"===t.getAttribute(D)?(t.removeAttribute(D),t.value=t.value.replace(t.getAttribute(V),""),t.className=t.className.replace(R,""),n=t.getAttribute(F),parseInt(n,10)>=0&&(t.setAttribute("maxLength",n),t.removeAttribute(F)),r=t.getAttribute(P),r&&(t.type=r),!0):!1}function a(t){var e,r,n=t.getAttribute(V);return""===t.value&&n?(t.setAttribute(D,"true"),t.value=n,t.className+=" "+I,r=t.getAttribute(F),r||(t.setAttribute(F,t.maxLength),t.removeAttribute("maxLength")),e=t.getAttribute(P),e?t.type="text":"password"===t.type&&M.changeType(t,"text")&&t.setAttribute(P,"password"),!0):!1}function u(t,e){var r,n,a,u,i,l,o;if(t&&t.getAttribute(V))e(t);else for(a=t?t.getElementsByTagName("input"):b,u=t?t.getElementsByTagName("textarea"):f,r=a?a.length:0,n=u?u.length:0,o=0,l=r+n;l>o;o++)i=r>o?a[o]:u[o-r],e(i)}function i(t){u(t,n)}function l(t){u(t,a)}function o(t){return function(){m&&t.value===t.getAttribute(V)&&"true"===t.getAttribute(D)?M.moveCaret(t,0):n(t)}}function c(t){return function(){a(t)}}function s(t){return function(e){return A=t.value,"true"===t.getAttribute(D)&&A===t.getAttribute(V)&&M.inArray(C,e.keyCode)?(e.preventDefault&&e.preventDefault(),!1):void 0}}function d(t){return function(){n(t,A),""===t.value&&(t.blur(),M.moveCaret(t,0))}}function g(t){return function(){t===r()&&t.value===t.getAttribute(V)&&"true"===t.getAttribute(D)&&M.moveCaret(t,0)}}function v(t){return function(){i(t)}}function p(t){t.form&&(T=t.form,"string"==typeof T&&(T=document.getElementById(T)),T.getAttribute(U)||(M.addEventListener(T,"submit",v(T)),T.setAttribute(U,"true"))),M.addEventListener(t,"focus",o(t)),M.addEventListener(t,"blur",c(t)),m&&(M.addEventListener(t,"keydown",s(t)),M.addEventListener(t,"keyup",d(t)),M.addEventListener(t,"click",g(t))),t.setAttribute(j,"true"),t.setAttribute(V,x),(m||t!==r())&&a(t)}var b,f,m,h,A,y,E,x,L,T,N,S,w,B=["text","search","url","tel","email","password","number","textarea"],C=[27,33,34,35,36,37,38,39,40,8,46],k="#ccc",I="placeholdersjs",R=RegExp("(?:^|\\s)"+I+"(?!\\S)"),V="data-placeholder-value",D="data-placeholder-active",P="data-placeholder-type",U="data-placeholder-submit",j="data-placeholder-bound",q="data-placeholder-focus",z="data-placeholder-live",F="data-placeholder-maxlength",G=document.createElement("input"),H=document.getElementsByTagName("head")[0],J=document.documentElement,K=t.Placeholders,M=K.Utils;if(K.nativeSupport=void 0!==G.placeholder,!K.nativeSupport){for(b=document.getElementsByTagName("input"),f=document.getElementsByTagName("textarea"),m="false"===J.getAttribute(q),h="false"!==J.getAttribute(z),y=document.createElement("style"),y.type="text/css",E=document.createTextNode("."+I+" { color:"+k+"; }"),y.styleSheet?y.styleSheet.cssText=E.nodeValue:y.appendChild(E),H.insertBefore(y,H.firstChild),w=0,S=b.length+f.length;S>w;w++)N=b.length>w?b[w]:f[w-b.length],x=N.attributes.placeholder,x&&(x=x.nodeValue,x&&M.inArray(B,N.type)&&p(N));L=setInterval(function(){for(w=0,S=b.length+f.length;S>w;w++)N=b.length>w?b[w]:f[w-b.length],x=N.attributes.placeholder,x?(x=x.nodeValue,x&&M.inArray(B,N.type)&&(N.getAttribute(j)||p(N),(x!==N.getAttribute(V)||"password"===N.type&&!N.getAttribute(P))&&("password"===N.type&&!N.getAttribute(P)&&M.changeType(N,"text")&&N.setAttribute(P,"password"),N.value===N.getAttribute(V)&&(N.value=x),N.setAttribute(V,x)))):N.getAttribute(D)&&(n(N),N.removeAttribute(V));h||clearInterval(L)},100)}M.addEventListener(t,"beforeunload",function(){K.disable()}),K.disable=K.nativeSupport?e:i,K.enable=K.nativeSupport?e:l}(this);