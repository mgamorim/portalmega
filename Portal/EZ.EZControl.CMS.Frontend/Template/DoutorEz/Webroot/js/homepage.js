var resizeAtive = function(){

    var fixMobile = (document.querySelector('[mobile]').getAttribute('mobile') === '1' && window.innerWidth <= 480) ? 0.75 : 1;


    var resizeSection = window.innerHeight - document.getElementById('content').offsetHeight - document.getElementById('bar').offsetHeight - document.getElementById('copyright').offsetHeight;
    resizeSection = (resizeSection > 0) ? resizeSection : 0;
    document.getElementById('content').style.marginTop = parseInt(resizeSection/2.5/fixMobile)+'px';
    document.getElementById('content').style.marginBottom = parseInt(resizeSection/1.5/fixMobile)+'px';
};
window.onload = function(){resizeAtive();};
window.onresize = function(){resizeAtive();};