var opacityContentPsearch = function () {
    $('section#content-psearch').css({
        'opacity': '0.5',
        'ms-opacity': '0.'
    });
}

$(document).ready(function(){

    $('#options-left li').click(function (e) {

        //ulPai e ulPaiOptionsLeft � para o controle do segundo menu do pais, para ter a fun��o de 2 menus corratamente, que fica na area de op��es
        var ulPai = e.target.nodeName.toLocaleUpperCase() === "SPAN"
            ? $(e.target).parent().parent().parent()[0]
            : $(e.target).parent().parent()[0];
        var ulPaiOptionsLeft = $('#options-left .menu-flag.psearch')[0];

        if ($(this).children('a').attr('href') !== "#" &&
            ulPai !== ulPaiOptionsLeft) {
            opacityContentPsearch();
        }
    });

    $('#bar form, #bar form').submit(function(){
        opacityContentPsearch();
    });

    $('.dropdown-menu').find('form').click(function (e) {
        e.stopPropagation();
    });

    //var textoDaCidade = $("form.search input[name=l]").val();
    //if (textoDaCidade !== "") {
    //    var textoFinal = $.parseHTML("<b>" + textoDaCidade + "</b><b class='caret'></b>");
    //    if (textoFinal && textoFinal[0] && textoFinal[1]) {
    //        $("#options-left > .dropdown.first > a").empty().append(textoFinal);
    //    }
    //}

    // Menu options scroll hidden
    $('#options .container-fluid').perfectScrollbar();

    //$('#df ul li').click(function (e) {
    //    if (e.currentTarget.id === 'df-first')
    //        $('#df > a')[0].outerHTML = this.textContent + ' <b class="caret"></b>';
    //    else
    //        $('#df > a')[0].outerHTML = '<b>' + this.textContent + '</b> <b class="caret"></b>';
    //});
});