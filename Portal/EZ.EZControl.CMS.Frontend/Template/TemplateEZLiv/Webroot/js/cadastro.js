﻿var request;

(function () {
    $('form > input').keyup(function () {

        var empty = false;
        $('form > input[required]').each(function () {
            if ($(this).val() == '') {
                empty = true;
            }
        });

        if (empty) {
            $('#cadastrar').attr('disabled', 'disabled');
        } else {
            $('#cadastrar').removeAttr('disabled');
        }
    });
})()

jQuery("#celular").mask("(99) 99999-9999");

this.autoComplete("#profissao", "http://ezcontrollocal.com/api/services/app/global/profissao/GetProfissao");
this.autoComplete("#corretora", "http://ezcontrollocal.com/api/services/app/ezliv/corretora/GetCorretora");
this.autoComplete("#grupoPessoa", "http://ezcontrollocal.com/api/services/app/global/grupoPessoa/GetGrupoPessoa");

$(document).ready(function() {
    //var cookieCorretora = getCookie('Corretora');
    var cookieGrupoPessoa = getCookie('GrupoDePessoa');

    if (cookieCorretora) {
      //  $("#corretora").prop('disabled', true);
        //$("#corretora").val(cookieCorretora);
    }
});

function autoComplete(nameInput, url) {
    $(nameInput).autocomplete({
        source: function (request, response) {

            $.ajax({
                url: url,
                type: 'POST',
                data: { titulo: request.term, nome: request.term },
                success: function (data) {
                    response(data.result);
                }
            });
        },
        minLength: 3
    });
}

$("#cadastroForm").submit(function (event) {

    event.preventDefault();

    var $form = $(this);

    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serialize();

    $inputs.prop("disabled", true);

    request = $.ajax({
        url: "http://ezcontrollocal.com/api/services/app/ezliv/beneficiarioBase/CreateUserPessoaBeneficiarioFront",
        type: "post",
        data: serializedData,
    });

    request.done(function (response, textStatus, jqXHR) {
        toastr.success("Cadastro realizado com sucesso!")
        $("#cadastroForm")[0].reset();
    });

    request.fail(function (jqXHR, textStatus, errorThrown) {
        var responseText = jQuery.parseJSON(jqXHR.responseText);
        toastr.error(responseText.error.message);
    });

    request.always(function () {
        $inputs.prop("disabled", false);
    });

});

function getCookie(name) {
    var value = document.cookie;
    var parts = value.split(name + "=");
    if (parts.length == 2) return parts.pop().split("&").shift();
}

function ValidaCPF(elemento) {
    cpf = elemento.value;
    cpf = cpf.replace(/[^\d]+/g, '');
    
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 ||
      cpf == "00000000000" ||
      cpf == "11111111111" ||
      cpf == "22222222222" ||
      cpf == "33333333333" ||
      cpf == "44444444444" ||
      cpf == "55555555555" ||
      cpf == "66666666666" ||
      cpf == "77777777777" ||
      cpf == "88888888888" ||
      cpf == "99999999999")
        return toastr.info("O CPF informado é inválido.");
    // Valida 1o digito 
    add = 0;
    for (i = 0; i < 9; i++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return toastr.info("O CPF informado é inválido.");
    // Valida 2o digito 
    add = 0;
    for (i = 0; i < 10; i++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return toastr.info("O CPF informado é inválido.");
}

$("#chkCorretora").change(function () {
    if (this.checked) {
        alert('Teste')
    }
});