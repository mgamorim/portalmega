﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Template;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMSFrontend.Template;
using EZ.EZControl.CMS.Frontend.Infra.Services;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EZ.EZControl.CMS.Frontend.Controllers
{
    public class HomeController : Controller
    {
        private static readonly SiteService SiteService = new SiteService();
        private static readonly TemplateService TemplateService = new TemplateService();
        private static readonly ConteudoService ConteudoService = new ConteudoService();

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            #region Carregar Site

            SiteFrontInput site;
            try
            {
                site = await SiteService.GetSiteAsync(Request);
                var segments = new Uri(Request.Url.ToString());
                var host = segments.Host;
                var path = segments.AbsolutePath;
                var corretora = host.Split('.')[0];
                var grupoDePessoa = path.Split('/')[path.Split('/').Length > 1 ? 1 : 0];
                if (!string.IsNullOrEmpty(corretora))
                {
                    Response.Cookies["EZLiv"]["Corretora"] = corretora;
                }
                if (!string.IsNullOrEmpty(grupoDePessoa))
                {
                    Response.Cookies["EZLiv"]["GrupoDePessoa"] = grupoDePessoa;
                }
            }
            catch (HttpException ex)
            {
                return GetViewPageError(ex.GetHttpCode(), "Não foi possível carregar esse site.", ex.Message);
            }
            catch (Exception ex)
            {
                return GetViewPageError(500, "Não foi possível carregar esse site.", ex.Message);
            }
            if (site == null)
            {
                return GetViewPageError(404, "Site não encontrado", "O site não está cadastrado corretamente ou não existe.");
            }

            #endregion Carregar Site

            #region Carregar Template

            TemplateFrontInput template;
            try
            {
                template = await TemplateService.GetAsync(new GetTemplateFrontInputDto() { Id = site.TemplateDefaultId, Host = Request.Url.Host });
            }
            catch (HttpException ex)
            {
                return GetViewPageError(ex.GetHttpCode(), "Não foi possível carregar o template default desse site.",
                    ex.Message);
            }
            catch (Exception ex)
            {
                return GetViewPageError(500, "Não foi possível carregar o template default desse site.", ex.Message);
            }
            if (template == null)
            {
                return GetViewPageError(503, "O template não encontrado", "O template não está cadastrado corretamente ou não existe.");
            }

            #endregion Carregar Template

            #region Construir viewName

            string viewName;
            try
            {
                //Tentar carregar a página
                if (Request.Url.Segments.Length > 1)
                {
                    var url = Request.Url.ToString();

                    try
                    {
                        var itensPorPagina = Convert.ToInt32(Request.QueryString["l"]);
                        var numeroDaPagina = Convert.ToInt32(Request.QueryString["p"]);

                        //ConteudoInput conteudo =
                        //    await ConteudoService.Get(new GetConteudoByUrlInput()
                        //    {
                        //        Url = url,
                        //        ItensPorPagina = itensPorPagina == default(int) ? 10 : itensPorPagina,
                        //        NumeroDaPagina = numeroDaPagina == default(int) ? 1 : numeroDaPagina
                        //    });

                        //ViewData.Model = conteudo;

                        viewName = String.Concat("~/Template/", template.NomeDoArquivo, "/",
                            template.TipoDePaginaParaPaginaDefault, ".cshtml");
                    }
                    catch (HttpException ex)
                    {
                        return GetViewPageError(ex.GetHttpCode(), "Não foi possível carregar a página desse site.",
                            ex.Message);
                    }
                    catch (Exception ex)
                    {
                        return GetViewPageError(500, "Não foi possível carregar a página desse site.", ex.Message);
                    }
                }
                else
                {
                    viewName = String.Concat("~/Template/", template.NomeDoArquivo, "/",
                        template.TipoDePaginaParaPaginaInicialDefault, ".cshtml");
                }
            }
            catch (HttpException ex)
            {
                return GetViewPageError(ex.GetHttpCode(), "Não foi possível carregar a página desse site.",
                    ex.Message);
            }
            catch (Exception ex)
            {
                return GetViewPageError(500, "Não foi possível carregar o página desse site.", ex.Message);
            }

            #endregion Construir viewName

            return View(viewName);
        }

        private ViewResult GetViewPageError(int numStatusCode, string title, string description)
        {
            var viewName = "~/Template/TemplateError/Default.cshtml";
            ViewBag.numStatusCode = numStatusCode;
            ViewBag.title = title;
            ViewBag.description = description;
            return View(viewName);
        }

        [HttpPost]
        public async Task<ActionResult> Index(FormDtoBase input)
        {
            object requestInput;

            try
            {
                requestInput = GetFormInput(Request, input);
            }
            catch (Exception ex)
            {
                return GetViewPageError(500, "Erro ao obter dados do formulário.", ex.Message);
            }

            var dados = await SiteService.PostAsync<FrontEndResultBaseDto<object>>(input.UrlBase, requestInput);

            return Json(SiteService.Object2JsonString(dados));
        }

        public object GetFormInput(HttpRequestBase request, FormDtoBase input)
        {
            if (string.IsNullOrEmpty(input.Name))
            {
                throw new Exception("O nome da classe utilizada no formulário não foi informada.");
            }

            var assembly = Assembly.GetAssembly(typeof(FormDtoBase));

            var inputType = assembly.GetType(string.Concat("EZ.EZControl.CMS.Frontend.Infra.", input.Name));

            object obj = Activator.CreateInstance(inputType);

            var propertiesInfo = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var propertyInfo in propertiesInfo)
            {
                if (request.Form.AllKeys.Contains(propertyInfo.Name, StringComparer.CurrentCultureIgnoreCase))
                {
                    propertyInfo.SetValue(obj, request.Form[propertyInfo.Name]);
                }
            }

            return obj;
        }
    }
}