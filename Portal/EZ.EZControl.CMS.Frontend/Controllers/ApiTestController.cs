﻿using EZ.EZControl.CMS.Infra.ApiClient;
using EZ.EZControl.CMS.Infra.Dtos;
using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EZ.EZControl.CMS.Frontend.Controllers
{
    public class ApiTestController : Controller
    {
        public ActionResult Index()
        {
            ApiSettingsDto model = new ApiSettingsDto();
            model.Host = ConfigurationManager.AppSettings["host"];
            model.ApiUrl = ConfigurationManager.AppSettings["apiUrl"];
            model.UserName = ConfigurationManager.AppSettings["userName"];
            model.UserPassword = ConfigurationManager.AppSettings["userPassword"];

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Post(ApiSettingsDto model)
        {
            var ezApiClient = new EZApiClient();
            ezApiClient.UserName = model.UserName;
            ezApiClient.Password = model.UserPassword;
            ezApiClient.BaseUrl = model.Host;
            ezApiClient.TenancyName = "default";
            ezApiClient.TokenBasedAuth();

            Uri baseUri = new Uri(model.Host);
            Uri uri = new Uri(baseUri, model.ApiUrl);

            GetAgenciaInput input = new GetAgenciaInput();

            var resultado = await ezApiClient.PostAsync<PagedResultDto<AgenciaListDto>>(uri.AbsoluteUri, input);

            model.ResultObject = resultado;

            return View("index", model);
        }
    }
}