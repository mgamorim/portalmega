﻿using Abp;
using Abp.IO.Extensions;
using Abp.Threading;
using Abp.Web.Models;
using Abp.WebApi.Client;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Net;
using System.Text;

namespace EZ.EZControl.CMS.Frontend.Infra
{
    public class EZApiClient : IEZApiClient
    {
        public readonly IAbpWebApiClient AbpWebApiClient;

        public EZApiClient(IAbpWebApiClient abpWebApiClient)
        {
            AbpWebApiClient = abpWebApiClient;
        }

        public string TenancyName { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string BaseUrl { get; set; }

        public void CookieBasedAuth()
        {
            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, "Account/Login");

            CookieBasedAuth(uri.AbsoluteUri);
        }

        public void TokenBasedAuth()
        {
            Uri baseUri = new Uri(BaseUrl);
            Uri uri = new Uri(baseUri, "api/Account/Authenticate");

            TokenBasedAuth(uri.AbsoluteUri);
        }

        //public async Task<ListResultOutput<RoleListDto>> GetRolesAsync()
        //{
        //    return await _abpWebApiClient.PostAsync<ListResultOutput<RoleListDto>>(
        //        BaseUrl + "api/services/app/role/GetRoles"
        //    );
        //}

        private void CookieBasedAuth(string url)
        {
            var requestBytes = Encoding.UTF8.GetBytes("TenancyName=" + TenancyName + "&UsernameOrEmailAddress=" + UserName + "&Password=" + Password);

            var request = WebRequest.CreateHttp(url);

            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            request.Accept = "application/json";
            request.CookieContainer = new CookieContainer();
            request.ContentLength = requestBytes.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(requestBytes, 0, requestBytes.Length);
                stream.Flush();

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseString = Encoding.UTF8.GetString(response.GetResponseStream().GetAllBytes());
                    var ajaxResponse = JsonString2Object<AjaxResponse>(responseString);

                    if (!ajaxResponse.Success)
                    {
                        throw new Exception("Could not login. Reason: " + ajaxResponse.Error.Message + " | " + ajaxResponse.Error.Details);
                    }

                    AbpWebApiClient.Cookies.Clear();
                    foreach (Cookie cookie in response.Cookies)
                    {
                        AbpWebApiClient.Cookies.Add(cookie);
                    }
                }
            }
        }

        private void TokenBasedAuth(string url)
        {
            var token = AsyncHelper.RunSync(() =>
                AbpWebApiClient.PostAsync<string>(
                    url,
                    new
                    {
                        TenancyName = TenancyName,
                        UsernameOrEmailAddress = UserName,
                        Password = Password
                    }));

            AbpWebApiClient.RequestHeaders.Add(new NameValue("Authorization", "Bearer " + token));

            #region Alternative implementation: Manual HTTP request

            //var requestBytes = Encoding.UTF8.GetBytes((new
            //{
            //    TenancyName = TenancyName,
            //    UsernameOrEmailAddress = UserName,
            //    Password = Password
            //}).ToJsonString());

            //var request = WebRequest.CreateHttp(url);

            //request.Method = WebRequestMethods.Http.Post;
            //request.ContentType = "application/json";
            //request.Accept = "application/json";
            //request.ContentLength = requestBytes.Length;

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(requestBytes, 0, requestBytes.Length);
            //    stream.Flush();

            //    using (var response = (HttpWebResponse)request.GetResponse())
            //    {
            //        var responseString = Encoding.UTF8.GetString(response.GetResponseStream().GetAllBytes());
            //        var ajaxResponse = JsonString2Object<AjaxResponse>(responseString);

            //        if (!ajaxResponse.Success)
            //        {
            //            throw new Exception("Could not login. Reason: " + ajaxResponse.Error.Message + " | " + ajaxResponse.Error.Details);
            //        }

            //        var token = ajaxResponse.Result.ToString();
            //        _abpWebApiClient.RequestHeaders.Add(new NameValue("Authorization", "Bearer " + token));
            //    }
            //}

            #endregion Alternative implementation: Manual HTTP request
        }

        private static TObj JsonString2Object<TObj>(string str)
        {
            return JsonConvert.DeserializeObject<TObj>(str,
                new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
        }
    }
}