﻿using EZ.EZControl.CMS.Frontend.Infra.Dtos.Abp;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Site;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Template;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMS.Widget;
using EZ.EZControl.CMS.Frontend.Infra.Services;
using System.Collections.Generic;
using System.Web;
using EZ.EZControl.CMS.Frontend.Infra.Dtos.CMSFrontend.Template;

namespace EZ.EZControl.CMS.Frontend.Helpers
{
    public abstract partial class TemplateHelper
    {
        private static readonly SiteService SiteService = new SiteService();
        private static readonly TemplateService TemplateService = new TemplateService();
        private static readonly WidgetService WidgetService = new WidgetService();

        public static TemplateFrontInput GetTemplate(HttpRequestBase request)
        {
            var site = SiteService.GetSite(request);

            if (site == null || request == null)
                return null;

            return
                TemplateService.Get(new GetTemplateFrontInputDto {Id = site.TemplateDefaultId, Host = request.Url.Host});
        }

        public static WidgetFrontInput GetWidget(string posicao, HttpRequestBase request)
        {
            var site = SiteService.GetSite(request);

            var widget = WidgetService.Get(new GetWidgetFrontInput
            {
                Posicao = posicao,
                SiteId = site.Id,
                TemplateId = site.TemplateDefaultId
            });

            return widget;
        }

        public static IEnumerable<WidgetFrontInput> GetWidgetList(string posicao, HttpRequestBase request)
        {
            var site = SiteService.GetSite(request);

            var widgets = WidgetService.GetList(new GetWidgetFrontInput
            {
                Posicao = posicao,
                SiteId = site.Id,
                TemplateId = site.TemplateDefaultId
            });

            return widgets;
        }
    }
}