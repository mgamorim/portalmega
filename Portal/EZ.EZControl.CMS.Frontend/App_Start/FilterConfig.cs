﻿using System.Web;
using System.Web.Mvc;

namespace EZ.EZControl.CMS.Frontend
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
