using Abp.WebApi.Controllers;

namespace EZ.EZControl.WebApi
{
    public abstract class EZControlApiControllerBase : AbpApiController
    {
        protected EZControlApiControllerBase()
        {
            LocalizationSourceName = EZControlConsts.LocalizationSourceName;
        }
    }
}