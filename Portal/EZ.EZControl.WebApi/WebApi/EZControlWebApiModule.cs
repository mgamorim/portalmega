﻿using Abp.Application.Services;
using Abp.Configuration.Startup;
using Abp.Modules;
using Abp.WebApi;
using Abp.WebApi.OData;
using Abp.WebApi.OData.Configuration;
using Swashbuckle.Application;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Cors;

namespace EZ.EZControl.WebApi
{
    /// <summary>
    /// Web API layer of the application.
    /// </summary>
    [DependsOn(typeof(AbpWebApiModule), typeof(AbpWebApiODataModule), typeof(EZControlApplicationModule))]
    public class EZControlWebApiModule : AbpModule
    {
        public override void PreInitialize()
        {
            ConfigureOData();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            EnableCors();

            //Automatically creates Web API controllers for all application services of the application
            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/global")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.Global."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/cms")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.CMS."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/cmsfrontend")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.CMSFrontEnd."))
                .Build();


            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/agenda")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.Agenda."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/ezmedical")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.EZMedical."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/vendas")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.Vendas."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/estoque")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.Estoque."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/servicos")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.Servicos."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/financeiro")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.Financeiro."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/ezliv")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.EZLiv."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app/ezpag")
                .Where(x => x.Namespace.Contains("EZ.EZControl.Services.EZPag."))
                .Build();

            Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
                .ForAll<IApplicationService>(typeof(EZControlApplicationModule).Assembly, "app")
                .Where(x =>
                    !x.Namespace.Contains("EZ.EZControl.Services.Global.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.CMS.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.Agenda.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.EZMedical.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.Vendas.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.Estoque.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.Servicos.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.Financeiro.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.CMSFrontEnd.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.EZPag.") &&
                    !x.Namespace.Contains("EZ.EZControl.Services.EZLiv."))
                .Build();

            Configuration.Modules.AbpWebApi().HttpConfiguration.Filters.Add(new HostAuthenticationFilter("Bearer"));

            InitializeFormatters(Configuration.Modules.AbpWebApi().HttpConfiguration);

            ConfigureSwaggerUi(); //Remove this line to disable swagger UI.
        }

        private void ConfigureOData()
        {
            var builder = Configuration.Modules.AbpWebApiOData().ODataModelBuilder;

            //Configure your entities here... see documentation: http://www.aspnetboilerplate.com/Pages/Documents/OData-Integration
            //builder.EntitySet<YourEntity>("YourEntities");
        }

        private void ConfigureSwaggerUi()
        {
            Configuration.Modules.AbpWebApi().HttpConfiguration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1", "EZ.EZControl.WebApi");
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                })
                .EnableSwaggerUi(c =>
                {
                    c.InjectJavaScript(Assembly.GetAssembly(typeof(EZControlWebApiModule)), "EZ.EZControl.WebApi.Scripts.Swagger-Custom.js");
                });
        }

        private static void InitializeFormatters(HttpConfiguration httpConfiguration)
        {
            httpConfiguration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }


        private static void EnableCors()
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            GlobalConfiguration.Configuration.EnableCors(cors);
        }
    }
}
