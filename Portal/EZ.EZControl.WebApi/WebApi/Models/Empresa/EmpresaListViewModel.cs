﻿using System.Collections.Generic;

namespace EZ.EZControl.WebApi.Models.Empresa
{
    public class EmpresaListViewModel
    {
        public IList<Domain.Global.SubtiposPessoa.Empresa> Empresas { get; set; }
    }
}