﻿using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.WebApi.Models.Empresa
{
    public class EmpresaViewModel
    {
        [Required]
        public int EmpresaId { get; set; }
    }
}