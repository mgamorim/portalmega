﻿using EZ.EZControl.ADO.Repositorio;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace EZ.EZControl.WebApi.Controllers
{
    [Route("api/Simulador")]   
    public class SimuladorController : ApiController
    {

        //private readonly IProdutoDePlanoDeSaudeAppService _produtoplanosaude;
        //private RepositorioSimulador _repositorio = null;


        //public SimuladorController(IProdutoDePlanoDeSaudeAppService produtoplanosaude)
        //{
        //    _produtoplanosaude = produtoplanosaude;
        //    _repositorio = new RepositorioSimulador();
        //}


        //[HttpPost]        
        //public HttpResponseMessage Teste()
        //{
        //    var result = new SimulacaoModel();
        //    try
        //    {
        //       result = new SimulacaoModel() { Nome = "Leandro Freitas" };
        //       return Request.CreateResponse(HttpStatusCode.OK, result);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, result);
        //    }
            
        //}

        [HttpPost]
        public async Task<SimulacaoModel> BuscarPlanos(SimulacaoModel model)
        {
            try
            {
                model.Nome = "Mauricio Amorim";
                model.Email = "mauricioamorim22@gmail.com";
                model.DataNascimento = "12/08/1988";
                model.Cpf = "11722219793";
                model.Celular = "21984993551";
                model.Estado = "RJ";
                model.Profissao = "Analista de sistemas";
                model.PossuiDependentes = "true";

                model.Dependentes = new List<SimulacaoModel.Dependente>();

                var dependente = new SimulacaoModel.Dependente();

                dependente.Nome = "Jose Marcos Amorim";
                dependente.Cpf = "701755955854";
                dependente.DataNascimento = "05/06/2016";

                model.Dependentes.Add(dependente);


                //var ListaProdutosComValor = await _produtoplanosaude.GetSimulacaoProdutosComValores(model);


                return model;
                //return Json(model);


            }
            catch (Exception ex)
            {
                model.resultado.Erro = ex.Message;
                //return Json(model);
                return model;
            }
        }

    }
}
