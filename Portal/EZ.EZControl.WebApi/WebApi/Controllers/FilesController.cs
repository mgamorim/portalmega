﻿using Abp.Application.Services.Dto;
using Abp.Domain.Uow;
using Abp.Runtime.Validation;
using Abp.WebApi.Authorization;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto;
using EZ.EZControl.Dto.CMS.Geral.Arquivo;
using EZ.EZControl.Dto.Core.Geral.Arquivo;
using EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento;
using EZ.EZControl.Dto.Global.Geral.Arquivo;
using EZ.EZControl.Services.CMS.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.WebApi.DataStreamProviders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EZ.EZControl.WebApi.Controllers
{
    [AbpApiAuthorize]
    public class FilesController : EZControlApiControllerBase
    {
        private readonly string workingFolder = HttpRuntime.AppDomainAppPath + @"\Uploads";
        private readonly UserManager _userManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IArquivoGlobalAppService _arquivoGlobalAppService;
        private readonly IArquivoCMSAppService _arquivoCMSAppService;
        private readonly IArquivoDocumentoAppService _arquivoDocumentoAppService;

        public FilesController(
            UserManager userManager,
            IUnitOfWorkManager unitOfWorkManager,
            IArquivoGlobalAppService arquivoGlobalAppService,
            IArquivoCMSAppService arquivoCMSAppService,
            IArquivoDocumentoAppService arquivoDocumentoAppService)
        {
            if (!Directory.Exists(workingFolder))
            {
                Directory.CreateDirectory(workingFolder);
            }

            _userManager = userManager;
            _unitOfWorkManager = unitOfWorkManager;
            _arquivoGlobalAppService = arquivoGlobalAppService;
            _arquivoCMSAppService = arquivoCMSAppService;
            _arquivoDocumentoAppService = arquivoDocumentoAppService;
        }

        /// <summary>
        ///   Get all files
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get()
        {
            var files = new List<FileDto>();

            var uploadFolder = new DirectoryInfo(workingFolder);

            await Task.Factory.StartNew(() =>
            {
                files = uploadFolder.EnumerateFiles()
                    //.Where(fi => new[] { ".jpg", ".bmp", ".png", ".gif", ".tiff" }
                    //  .Contains(fi.Extension.ToLower()))
                    .Select(fi => new FileDto
                    {
                        FileName = fi.Name,
                        Created = fi.CreationTime,
                        Modified = fi.LastWriteTime,
                        Size = fi.Length / 1024
                    })
                    .ToList();
            });

            return Ok(new { Files = files });
        }

        /// <summary>
        ///   Delete file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(String fileName)
        {
            if (!FileExists(fileName))
            {
                return NotFound();
            }

            try
            {
                var filePath = Directory.GetFiles(workingFolder, fileName).FirstOrDefault();

                await Task.Factory.StartNew(() =>
                {
                    if (filePath != null)
                        File.Delete(filePath);
                });

                var result = new FileActionResult
                {
                    Successful = true,
                    Message = fileName + "deleted successfully"
                };
                return Ok(new { message = result.Message });
            }
            catch (Exception ex)
            {
                var result = new FileActionResult
                {
                    Successful = false,
                    Message = "error deleting fileName " + ex.GetBaseException().Message
                };
                return BadRequest(result.Message);
            }
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteWithPath(String path)
        {

            try
            {
                var filePath = HttpRuntime.AppDomainAppPath + path;
                await Task.Factory.StartNew(() =>
                {
                    if (filePath != null)
                        File.Delete(filePath);
                });

                return Ok();
            }
            catch (Exception ex)
            {
                var result = new FileActionResult
                {
                    Successful = false,
                    Message = "error deleting fileName " + ex.GetBaseException().Message
                };
                return BadRequest(result.Message);
            }
        }

        /// <summary>
        ///   Add a file
        /// </summary>
        /// <returns></returns>
        public async Task<IHttpActionResult> Add()
        {
            String fileName = String.Empty;
            try
            {
                // Check if the request contains multipart/form-data.
               // return BadRequest("ddd");
                var provider = new CustomMultipartFormDataStreamProvider(workingFolder);

                await Request.Content.ReadAsMultipartAsync(provider);

                var dados = provider.FormData;

                var files =
                    provider.FileData
                        .Select(file => new FileInfo(file.LocalFileName))
                        .Select(fileInfo => new FileDto
                        {
                            FileName = fileInfo.Name,
                            Created = fileInfo.CreationTime,
                            Modified = fileInfo.LastWriteTime,
                            FileType = Path.GetExtension(fileInfo.Name),
                            Path = String.Format("{0}{1}{2}", "Uploads", @"\", fileInfo.Name),
                            Size = fileInfo.Length / 1024,
                            IsActive = Convert.ToBoolean(dados["ativo"]),
                            Nome = fileInfo.Name,
                            NomeSistema = dados["sistema"],
                        }).ToList();

                dados["nome"] = files[0].Nome;

                if (files[0].Size > 5000)
                {
                    return BadRequest("O Tamanho permitido para arquivo é no máximo 5 MB");
                }


                int arquivoId;
                int.TryParse(dados["arquivoId"], out arquivoId);

                FileDto fileDto = files.FirstOrDefault();
                ArquivoInput arquivo = new ArquivoInput();
                ArquivoInput arquivoDtoAux = null;
                SistemaEnum sistema = fileDto != null ? fileDto.Sistema : this.GetSistema(dados["sistema"]);


                if (arquivoId > 0)
                {
                    switch (sistema)
                    {
                        case SistemaEnum.CMS:
                            {
                                arquivoDtoAux = await _arquivoCMSAppService.GetById(new IdInput(arquivoId));
                                break;
                            }
                        case SistemaEnum.EZLiv:
                            {
                                arquivoDtoAux = await _arquivoDocumentoAppService.GetById(new IdInput(arquivoId));
                                break;
                            }
                        default:
                            {
                                arquivoDtoAux = (ArquivoInput)await _arquivoGlobalAppService.GetById(new IdInput(arquivoId));
                                break;
                            }
                    }
                }


                // Possíveis situações
                // 1) Usuário está salvado um novo arquivo
                if (arquivoId == 0 && fileDto != null)
                {
                    arquivo.Nome = dados["nome"];
                    arquivo.IsActive = Convert.ToBoolean(dados["ativo"]);
                    arquivo.Path = fileDto.Path;
                    arquivo.TipoDeArquivoFixo = fileDto.TipoDeArquivo;
                    arquivo.Token = fileDto.FileToken;
                    fileName = fileDto.FileName;
                }
                // 2) Usuário está editando as informações do arquivo sem fazer um novo upload
                else if (arquivoId > 0 && fileDto == null)
                {
                    arquivo.Id = arquivoId;
                    arquivo.Nome = dados["nome"];
                    arquivo.IsActive = Convert.ToBoolean(dados["ativo"]);
                    arquivo.Path = arquivoDtoAux.Path;
                    arquivo.TipoDeArquivoFixo = arquivoDtoAux.TipoDeArquivoFixo;
                    arquivo.Token = arquivoDtoAux.Token;
                }
                // 3) Usuário está editando as informações do arquivo e ainda subindo um novo arquivo
                else if (arquivoId > 0 && fileDto != null)
                {
                    //Deletando o Arquivo Anterior
                    await this.DeleteWithPath(arquivoDtoAux.Path);

                    arquivo.Id = arquivoId;
                    arquivo.Nome = dados["nome"];
                    arquivo.IsActive = Convert.ToBoolean(dados["ativo"]);
                    arquivo.Path = fileDto.Path;
                    arquivo.TipoDeArquivoFixo = fileDto.TipoDeArquivo;
                    arquivo.Token = arquivoDtoAux.Token;
                    fileName = fileDto.FileName;
                }

                switch (sistema)
                {
                    case SistemaEnum.CMS:
                        {
                            await _arquivoCMSAppService.Save(new ArquivoCMSInput
                            {
                                IsActive = arquivo.IsActive,
                                Id = arquivo.Id,
                                Conteudo = arquivo.Conteudo,
                                Nome = arquivo.Nome,
                                Path = arquivo.Path,
                                TipoDeArquivoFixo = arquivo.TipoDeArquivoFixo,
                                Token = arquivo.Token,
                                IsImagem = arquivo.IsActive,
                                IdInput = arquivo.IdInput
                            });
                            break;
                        }
                    case SistemaEnum.EZLiv:
                        {
                            await _arquivoDocumentoAppService.Save(new ArquivoDocumentoInput
                            {
                                IsActive = arquivo.IsActive,
                                Id = arquivo.Id,
                                Conteudo = arquivo.Conteudo,
                                Nome = arquivo.Nome,
                                Path = arquivo.Path,
                                TipoDeArquivoFixo = arquivo.TipoDeArquivoFixo,
                                Token = arquivo.Token,
                                IsImagem = arquivo.IsImagem,
                                IdInput = arquivo.IdInput,
                                Tipo = (TipoDeDocumentoEnum)Int32.Parse(dados["tipo"]),
                                PropostaDeContratacaoId = Int32.Parse(dados["refId"])
                            });
                            break;
                        }
                    default:
                        {
                            await _arquivoGlobalAppService.Save(new ArquivoGlobalInput
                            {
                                IsActive = arquivo.IsActive,
                                Id = arquivo.Id,
                                Conteudo = arquivo.Conteudo,
                                Nome = arquivo.Nome,
                                Path = arquivo.Path,
                                TipoDeArquivoFixo = arquivo.TipoDeArquivoFixo,
                                Token = arquivo.Token,
                                IsImagem = arquivo.IsActive,
                                IdInput = arquivo.IdInput
                            });
                            break;
                        }
                };

                return Ok();
            }
            catch (AbpValidationException vex)
            {
                await Delete(fileName);
                return BadRequest(vex.ValidationErrors[0].ErrorMessage);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        ///   Check if file exists on disk
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public bool FileExists(string fileName)
        {
            var file = Directory.GetFiles(workingFolder, fileName)
                .FirstOrDefault();

            return file != null;
        }

        private bool _isDisposed;

        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                //_unitOfWorkCompleteHandler.Complete();
                //_unitOfWorkCompleteHandler.Dispose();
            }

            _isDisposed = true;

            base.Dispose(disposing);
        }

        private SistemaEnum GetSistema(String sistema)
        {
            return (SistemaEnum)Enum.Parse(typeof(SistemaEnum), sistema, false);
        }
    }
}