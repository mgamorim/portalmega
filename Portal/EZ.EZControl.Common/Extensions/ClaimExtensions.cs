﻿using Castle.Core.Internal;
using System.Security.Claims;
using System.Security.Principal;

namespace EZ.EZControl.Common.Extensions
{
    public static class ClaimExtensions
    {
        public static void AddOrUpdateClaim(this IPrincipal currentPrincipal, string key, string value)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
                return;

            var existingClaim = identity.FindFirst(key);
            if (existingClaim != null)
            {
                RemoveClaim(currentPrincipal, key);
            }

            var claim = new Claim(key, value);
            identity.AddClaim(claim);
        }

        public static void RemoveClaim(this IPrincipal currentPrincipal, string key)
        {
            var identity = currentPrincipal.Identity as ClaimsIdentity;
            if (identity == null)
                return;

            var existingClaims = identity.FindAll(key);
            existingClaims.ForEach(c => identity.RemoveClaim(c));
        }
    }
}