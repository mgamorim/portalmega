﻿using System;
using System.Linq;
using System.Reflection;

namespace EZ.EZControl.Common.Extensions
{
    /// <summary>
    /// Classe de Extensão do MemberInfo
    /// </summary>
	public static class MemberInfoExtension
    {
        /// <summary>
        /// Retorna o Atributo Tipado do Membro da Classe
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static T GetAttribute<T>(this MemberInfo owner)
        {
            if (owner == null) return System.Activator.CreateInstance<T>();

            var attribute = owner.GetCustomAttributes(typeof(T), true)
                .FirstOrDefault();

            return (T)attribute;
        }

        /// <summary>
        /// Retorna o Atributo Tipado do Membro da Classe com a opção de não propagar a excessão do Cast
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="owner"></param>
        /// <param name="throwsCast"></param>
        /// <returns></returns>
        public static T GetAttribute<T>(this MemberInfo owner, Boolean throwsCast)
            where T : Attribute
        {
            var attribute = owner.GetCustomAttributes(typeof(T), true)
                .FirstOrDefault();

            if (throwsCast)
                return (T)attribute;

            return attribute as T;
        }
    }
}