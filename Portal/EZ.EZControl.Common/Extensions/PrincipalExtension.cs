﻿using Abp.Domain.Uow;
using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;

namespace EZ.EZControl.Common.Extensions
{
    public static class PrincipalExtension
    {
        public static Claim GetEmpresaClaim(this IPrincipal principal)
        {
            ClaimsPrincipal claimPrincipal = principal as ClaimsPrincipal;

            if (claimPrincipal == null)
                return null;

            ClaimsIdentity identity = claimPrincipal.Identity as ClaimsIdentity;

            if (identity == null)
                return null;

            var empresaClaim = identity.FindFirst("EmpresaId");

            return empresaClaim;
        }

        public static int? GetEmpresaIdFromClaim(this IPrincipal principal)
        {
            var empresaClaim = GetEmpresaClaim(principal);

            if (empresaClaim == null)
            {
                return null;
            }

            return Convert.ToInt32(empresaClaim.Value);
        }
    }
}