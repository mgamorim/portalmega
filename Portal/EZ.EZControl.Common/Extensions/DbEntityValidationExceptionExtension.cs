﻿using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace EZ.EZControl.Common.Extensions
{
    public static class DbEntityValidationExceptionExtension
    {
        public static string GetMessage(this DbEntityValidationException ex, bool showOriginalMessage = false)
        {
            var errorMessages = ex.EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => x.ErrorMessage);

            var validationErrorMessages = string.Join(System.Environment.NewLine, errorMessages);

            var exceptionMessage = showOriginalMessage ?
                string.Concat(ex.Message, " The validation errors are: ", System.Environment.NewLine, validationErrorMessages) :
                validationErrorMessages;

            return exceptionMessage;
        }

        public static IEnumerable<string> GetMessages(this DbEntityValidationException ex)
        {
            var errorMessages = ex.EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => x.ErrorMessage);

            return errorMessages;
        }
    }
}