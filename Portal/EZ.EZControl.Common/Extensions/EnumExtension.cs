﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace EZ.EZControl.Common.Extensions
{
    public static class EnumExtension
    {
        public static string GetDescription(this Enum owner)
        {
            FieldInfo fieldInfo = owner.GetType().GetField(owner.ToString());
            var attribute = fieldInfo.GetAttribute<DescriptionAttribute>();

            if (attribute != null)
                return attribute.Description;
            else
                //return Enum.GetName(owner.GetType(), owner);
                return owner.ToString(); // o mesmo efeito da linha de cima. guardei para referencia
        }

        public static IList ListDescriptions<T>() where T : struct, IConvertible
        {
            return GetDescriptions<T>(Enum.GetValues(typeof(T)), true);
        }

        public static IList ListDescriptions<T>(bool order) where T : struct, IConvertible
        {
            return GetDescriptions<T>(Enum.GetValues(typeof(T)), order);
        }

        public static IList ListDescriptions<T>(T[] enumValues) where T : struct, IConvertible
        {
            return GetDescriptions<T>(enumValues, true);
        }

        public static IList ListDescriptions<T>(T[] enumValues, bool order) where T : struct, IConvertible
        {
            return GetDescriptions<T>(enumValues, order);
        }

        private static IList GetDescriptions<T>(Array enumValues, bool order) where T : struct, IConvertible
        {
            ArrayList list = new ArrayList();

            foreach (Enum value in enumValues)
                list.Add(new KeyValuePair<Enum, string>(value, GetDescription(value)));

            if (order)
                list.Sort(new DescriptionEntryComparer());

            return list;
        }

        public static T GetAttribute<T>(this Enum owner)
        {
            FieldInfo fieldInfo = owner.GetType().GetField(owner.ToString());
            var attribute = fieldInfo.GetCustomAttributes(typeof(T), false).FirstOrDefault();

            return (T)attribute;
        }
    }

    public class DescriptionEntryComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            var left = (KeyValuePair<Enum, string>)x;
            var right = (KeyValuePair<Enum, string>)y;

            return left.Value.CompareTo(right.Value);
        }
    }
}