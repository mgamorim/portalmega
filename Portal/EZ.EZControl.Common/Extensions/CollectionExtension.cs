﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace EZ.EZControl.Common.Extensions
{
    /// <summary>
    /// Classe que extende algumas funcionalidades de tipos que são coleção.
    /// </summary>
    public static class CollectionExtension
    {
        public static void RemoveItem<T>(this ICollection<T> owner, Func<T, bool> predicate)
        {
            owner.Remove(
                owner.Where(predicate).FirstOrDefault()
                );
        }

        public static void ChangeItem<T>(this IList<T> owner, Func<T, bool> predicate, T newItem)
        {
            var item = owner.Where(predicate).FirstOrDefault();

            if (item != null)
            {
                var index = owner.IndexOf(item);

                owner[index] = newItem;
            }
        }

        public static bool CollectionEquals<T>(this IEnumerable<T> owner, IEnumerable<T> other)
        {
            return Enumerable.SequenceEqual<T>(owner, other);
        }

        public static Type GetCollectionItemType<T>(this IEnumerable<T> owner)
        {
            return owner.GetType().GetGenericArguments()[0];
        }

        public static IEnumerable<T> FastReverse<T>(this IEnumerable<T> owner)
        {
            for (int i = owner.Count() - 1; i >= 0; i--)
            {
                yield return owner.ToList()[i];
            }
        }

        public static void SafeAdd<T>(this ICollection<T> owner, T item)
        {
            if (!owner.Contains(item))
                owner.Add(item);
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem,
            Func<TSource, bool> canContinue)
        {
            for (var current = source; canContinue(current); current = nextItem(current))
            {
                yield return current;
            }
        }

        public static IEnumerable<TSource> FromHierarchy<TSource>(
            this TSource source,
            Func<TSource, TSource> nextItem)
            where TSource : class
        {
            return FromHierarchy(source, nextItem, s => s != null);
        }

        public static TSource SingleOrDefault<TSource>(this IQueryable<TSource> source, string errorMessage)
        {
            try
            {
                return source.SingleOrDefault();
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(FormatErrorMessage(ex, errorMessage), ex.InnerException);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(FormatErrorMessage(ex, errorMessage), ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(FormatErrorMessage(ex, errorMessage));
            }
        }

        private static string FormatErrorMessage(Exception ex, string errorMessage)
        {
            if (string.IsNullOrEmpty(errorMessage))
                return ex.Message;
            else
                return string.Format("{0} {1}Erro Original: {2}", errorMessage, System.Environment.NewLine, ex.Message);
        }

        public static TSource SingleOrDefault<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate, string errorMessage)
        {
            try
            {
                return source.SingleOrDefault(predicate);
            }
            catch (ArgumentNullException ex)
            {
                throw new ArgumentNullException(FormatErrorMessage(ex, errorMessage), ex.InnerException);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(FormatErrorMessage(ex, errorMessage), ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(FormatErrorMessage(ex, errorMessage));
            }
        }
    }
}