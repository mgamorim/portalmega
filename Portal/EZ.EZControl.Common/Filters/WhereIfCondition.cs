﻿using System;
using System.Linq.Expressions;

namespace EZ.EZControl.Common.Filters
{
    public class WhereIfCondition<T>
    {
        public bool Condition { get; set; }
        public Expression<Func<T, bool>> Expression { get; set; }

        public WhereIfCondition()
        {
        }

        public WhereIfCondition(bool condition, Expression<Func<T, bool>> expression)
        {
            Condition = condition;
            Expression = expression;
        }
    }
}