﻿using Abp.Modules;
using Castle.MicroKernel.Registration;
using EZ.EZControl.Common.Interceptors.DbEntityException;

namespace EZ.EZControl.Common
{
    public class EZControlCommonModule : AbpModule
    {
        public override void PreInitialize()
        {
            IocManager.IocContainer.Register(Component.For<DbEntityExceptionInterceptor>().LifestyleTransient());
            DbEntityExceptionInterceptorRegistrar.Initialize(IocManager.IocContainer.Kernel);
        }
    }
}