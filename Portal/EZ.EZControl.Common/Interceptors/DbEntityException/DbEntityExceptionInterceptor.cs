﻿using Abp.Localization;
using Abp.Localization.Sources;
using Abp.Threading;
using Abp.UI;
using Castle.DynamicProxy;
using EZ.EZControl.Common.Extensions;
using System.Data.Entity.Validation;
using System.Threading.Tasks;

namespace EZ.EZControl.Common.Interceptors.DbEntityException
{
    public class DbEntityExceptionInterceptor : IInterceptor
    {
        private const string LocalizationSourceName = "EZControl";
        private ILocalizationSource _localizationSource;
        private readonly ILocalizationManager _localizationManager;

        public DbEntityExceptionInterceptor(ILocalizationManager localizationManager)
        {
            _localizationManager = localizationManager;
            _localizationSource = _localizationManager.GetSource(LocalizationSourceName);
        }

        public void Intercept(IInvocation invocation)
        {
            if (AsyncHelper.IsAsyncMethod(invocation.Method))
            {
                InterceptAsync(invocation);
            }
            else
            {
                InterceptSync(invocation);
            }
        }

        private void InterceptAsync(IInvocation invocation)
        {
            invocation.Proceed();

            var task = ((Task)invocation.ReturnValue);

            if (task.IsFaulted)
            {
                if (task.Exception != null)
                {
                    if (task.Exception.InnerException is DbEntityValidationException)
                    {
                        var ex = (DbEntityValidationException)task.Exception.InnerException;
                        var details = ex.GetMessage();
                        var message = L("ErroAoProcessarDados");
                        throw new UserFriendlyException(0, message, details);
                    }
                }
            }
        }

        private void InterceptSync(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.GetMessage();
                throw new UserFriendlyException(0, L("ErroAoProcessarDados"), errorMessages);
            }
        }

        protected virtual string L(string name)
        {
            return _localizationSource.GetString(name);
        }
    }
}