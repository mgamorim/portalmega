﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Common.Helpers
{
    public class PessoaHelpers
    {
        public static int GetIdade(DateTime dataNascimento)
        {
            // Retorna o número de anos
            var anoCorrente = DateTime.Now.Year;
            var anoNascimento = dataNascimento.Year;
            int YearsAge = anoCorrente - anoNascimento;
            // Se a data de aniversário não ocorreu ainda este ano, subtrair um ano a partir da idade
            if (DateTime.Now.Month < dataNascimento.Month || (DateTime.Now.Month == dataNascimento.Month && DateTime.Now.Day < dataNascimento.Day))
            {
                YearsAge--;
            }
            return YearsAge;
        }
    }
}
