﻿using EZ.EZControl.ADO.Mensageria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Base
{
    public interface IRepositorioBase<T>
    {
        #region Propriedades

        bool PossuiResultados { get; }

        int QtdeRegistros { get; set; }

        List<Mensagem> Mensagens { get; set; }

        List<T> Resultados { get; set; }

        T Resultado { get; }

        #endregion
    }
}
