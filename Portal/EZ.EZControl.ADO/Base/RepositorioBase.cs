﻿using EZ.EZControl.ADO.Mensageria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Base
{
    public abstract class RepositorioBase<T> : IRepositorioBase<T> where T : class
    {
        #region Atributos

        private int qtdeRegistros;
        private List<Mensagem> mensagens;
        private List<T> resultados;

        #endregion

        #region Construtores

        protected RepositorioBase()
        {
            this.qtdeRegistros = 0;
            this.mensagens = new List<Mensagem>();
            this.resultados = new List<T>();
        }

        #endregion

        #region Propriedades

        public bool PossuiResultados
        {
            get { return qtdeRegistros > 0; }
        }

        public int QtdeRegistros
        {
            get { return qtdeRegistros; }
            set { qtdeRegistros = value; }
        }

        public List<Mensagem> Mensagens
        {
            get { return mensagens; }
            set { mensagens = value; }
        }

        public List<T> Resultados
        {
            get { return resultados; }
            set { if (resultados != null) resultados = value; }
        }

        public T Resultado
        {
            get { return resultados.Count > 0 ? resultados[0] : null; }
        }

        #endregion
    }
}
