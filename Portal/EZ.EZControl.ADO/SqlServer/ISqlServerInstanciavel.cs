﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.SqlServer
{
    public interface ISqlServerInstanciavel<T>
    {
        #region Operacoes

        T Instanciar(SqlDataReader reader);

        #endregion
    }
}
