﻿using EZ.EZControl.Domain.EZLiv.Geral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio.Interface
{
    public interface IRepositorioSimulador
    {
       Task<bool> GravarSimulacao(SimulacaoModel model);
        bool AtualizaSimulacao(SimulacaoModel model);
        SimulacaoModel BuscaSimulacao(SimulacaoModel model);

    }
}
