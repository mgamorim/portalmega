﻿using EZ.EZControl.Domain.EZPag.Geral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio.Interface
{
  public  interface IRepositorioFormasPagamento
    {
        IQueryable<FormaDePagamento> BuscaFormasPagamento(Int32 id);
    }
}
