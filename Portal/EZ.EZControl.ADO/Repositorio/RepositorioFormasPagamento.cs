﻿using EZ.EZControl.ADO.Repositorio.Interface;
using EZ.EZControl.ADO.SqlServer;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioFormasPagamento : ConexaoSqlServer<FormaDePagamento>, IRepositorioFormasPagamento, ISqlServerInstanciavel<FormaDePagamento>
    {
        public RepositorioFormasPagamento(String connString)
         : base(connString) { }


        public IQueryable<FormaDePagamento> BuscaFormasPagamento(int id)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  [id] ");
            sqlQuery.AppendLine(",[IsActive] ");
            sqlQuery.AppendLine(",[Descricao] ");
            sqlQuery.AppendLine(",[TipoDePagamento] ");
            sqlQuery.AppendLine(",[EmpresaId] from ");
            sqlQuery.AppendLine("[Pag_FormaDePagamento] ");
            sqlQuery.AppendFormat("where empresaid = ( select id FROM [dbo].[Glb_Empresa] where nomefantasia = (select nome FROM [dbo].[Sau_Corretora] where id = {0} )) ", id);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

            ExecuteReader(sqlCommand, this);

            var result = Resultados.AsQueryable();

            return result;
        }

        public FormaDePagamento Instanciar(SqlDataReader reader)
        {
            FormaDePagamento dados = new FormaDePagamento();

            dados.IsActive = (Convert.ToBoolean(reader["IsActive"]) != false) ? true : false;
            dados.Descricao = (reader["Descricao"] != DBNull.Value) ? reader["Descricao"].ToString() : null;
            dados.TipoDePagamento = (reader["TipoDePagamento"] != DBNull.Value) ? (TipoCheckoutPagSeguroEnum)Convert.ToInt32(reader["TipoDePagamento"]) : 0;


            return dados;


        }
    }
}
