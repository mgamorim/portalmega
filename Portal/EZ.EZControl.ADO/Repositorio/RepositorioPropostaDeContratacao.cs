﻿using EZ.EZControl.ADO.Enum;
using EZ.EZControl.ADO.Mensageria;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.ADO.SqlServer;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioPropostaDeContratacao : ConexaoSqlServer<PropostaDeContratacaoInput>, IRepositorioPropostaDeContratacao, ISqlServerInstanciavel<PropostaDeContratacaoInput>
    {
        private const string EMISSOR = "RepositorioPropostaDeContratacao";
        private const TipoEnum TIPO = TipoEnum.Persistencia;

        protected RepositorioPropostaDeContratacao(string connString) : base(connString)
        {
        }

        public List<PropostaDeContratacaoInput> CarregarGrid()
        {
            List<PropostaDeContratacaoInput> valores = new List<PropostaDeContratacaoInput>();
            PropostaDeContratacaoInput input4 = new PropostaDeContratacaoInput();


            var msgResposta = new MsgResposta<PropostaDeContratacaoInput>(EMISSOR, "ObterServicoPorId", TIPO, true);

            StringBuilder sql = new StringBuilder();

            sql.AppendLine("SELECT");
            sql.AppendLine(" ID, ");
            //sql.AppendLine("IdRequisicao , ");
            sql.AppendLine("Telefone, IdCanal , ");
            sql.AppendLine("Ativo, DataCriacao, SubscriptionId, ProtocoloAssinatura ");
            sql.AppendFormat("from HistoricoAssinaturaWeb where telefone = 12 and Idcanal = 44 and ativo = 1 ");
            var sqlCommand = new SqlCommand(sql.ToString());

            if (base.ExecuteReader(sqlCommand, this))
            {
                valores = Resultados;
            }
            else
            {
            }


            //input4.Numero = "00114";





            valores.Add(input4);

            return valores;
        }

        public PropostaDeContratacaoInput Instanciar(SqlDataReader reader)
        {
            throw new NotImplementedException();
        }
    }
}
