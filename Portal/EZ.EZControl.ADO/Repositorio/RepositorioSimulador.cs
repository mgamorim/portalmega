﻿using EZ.EZControl.ADO.Mensageria;
using EZ.EZControl.ADO.Repositorio.Interface;
using EZ.EZControl.ADO.SqlServer;
using EZ.EZControl.Domain.EZLiv.Geral;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioSimulador : ConexaoSqlServer<SimulacaoModel> , IRepositorioSimulador, ISqlServerInstanciavel<SimulacaoModel>
    {
        public RepositorioSimulador(String connString)
          : base(connString) { }


        public async Task<bool> GravarSimulacao(SimulacaoModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();
            sqlQuery.Clear();

            try
            {

            sqlQuery.AppendLine("INSERT INTO SIMULACAO_TB ");
            sqlQuery.AppendLine(" ( ");
            sqlQuery.AppendLine("CHAVE, ");
            sqlQuery.AppendLine("EMPRESA, ");
            sqlQuery.AppendLine("NOME, ");
            sqlQuery.AppendLine("PARCEIRO, ");
            sqlQuery.AppendLine("CPF, ");
            sqlQuery.AppendLine("DATANASCIMENTO, ");
            sqlQuery.AppendLine("EMAIL, ");
            sqlQuery.AppendLine("CELULAR, ");
            sqlQuery.AppendLine("ESTADO, ");
            sqlQuery.AppendLine("PROFISSAO, ");
            sqlQuery.AppendLine("TITULARRESPONSAVEL, ");
            sqlQuery.AppendLine("POSSUIDEPENDENTES, ");

            sqlQuery.AppendLine("QTDDEPENDENTES, ");
            sqlQuery.AppendLine("IDPLANO, ");
            sqlQuery.AppendLine("NOMEPLANO, ");
            sqlQuery.AppendLine("NOMEOPERADORA, ");
            sqlQuery.AppendLine("NOMEADMINISTRADORA, ");
            sqlQuery.AppendLine("ABRANGENCIA, ");
            sqlQuery.AppendLine("ACOMODACAO, ");
            sqlQuery.AppendLine("VALORTOTAL, ");
            sqlQuery.AppendLine("DESCRICAOPLANO, ");
            sqlQuery.AppendLine("DATASIMULACAO ");
            sqlQuery.AppendLine(") ");
            sqlQuery.AppendLine("VALUES ");
            sqlQuery.AppendLine("( ");                                  

            sqlQuery.AppendFormat(" '{0}' , ", model.Chave);
            sqlQuery.AppendFormat(" '{0}' , ", model.Empresa);
            sqlQuery.AppendFormat(" '{0}' , ", model.Nome);
            sqlQuery.AppendFormat(" '{0}' , ", model.Parceiro);
            sqlQuery.AppendFormat(" '{0}' , ", model.Cpf);
            sqlQuery.AppendFormat(" '{0}' , ", Convert.ToDateTime(model.DataNascimento).ToString("yyyyMMdd HH:mm:ss"));
            sqlQuery.AppendFormat(" '{0}' , ", model.Email);
            sqlQuery.AppendFormat(" '{0}' , ", model.Celular);
            sqlQuery.AppendFormat(" '{0}' , ", model.Estado);
            sqlQuery.AppendFormat(" '{0}' , ", model.Profissao);
            sqlQuery.AppendFormat(" {0} , ", model.TitularResponsavel == "True" ? 0 : 1);
            sqlQuery.AppendFormat(" {0} , ", model.PossuiDependentes == "True" ? 0 : 1);

            sqlQuery.AppendFormat(" '{0}' , ", model.QtdDependentes =  model.PossuiDependentes  == "True" ? model.Dependentes.Count : 0);
            sqlQuery.AppendFormat(" '{0}' , ", model.idPlano == 0 ? 0 : model.idPlano);
            sqlQuery.AppendFormat(" '{0}' , ", model.NomePlano == string.Empty ? string.Empty : model.NomePlano);
            sqlQuery.AppendFormat(" '{0}' , ", model.NomeOperadora == string.Empty ? string.Empty : model.NomeOperadora);
            sqlQuery.AppendFormat(" '{0}' , ", model.NomeAdministradora == string.Empty ? string.Empty : model.NomeAdministradora);
            sqlQuery.AppendFormat(" '{0}' , ", model.Abrangencia == string.Empty ? string.Empty : model.Abrangencia);
            sqlQuery.AppendFormat(" '{0}' , ", model.Acomodacao == string.Empty ? string.Empty : model.Acomodacao);
            sqlQuery.AppendFormat(" '{0}' , ", model.ValorTotal == string.Empty ? string.Empty : model.ValorTotal);
            sqlQuery.AppendFormat(" '{0}' ,", model.DescricaoPlano == string.Empty ? string.Empty : model.DescricaoPlano);
            sqlQuery.AppendFormat(" '{0}'  ", DateTime.Now.ToString("yyyyMMdd HH:mm:ss"));
            sqlQuery.AppendLine(")");

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());


                return  await ExecuteNonQuery2(sqlCommand);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool AtualizaSimulacao(SimulacaoModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("update [Simulacao].[dbo].[Simulacao_tb] ");
            sqlQuery.AppendFormat("set idplano = {0} ", model.idPlano == 0 ? 0 : model.idPlano );
            sqlQuery.AppendFormat(", cpf = '{0}'  ", model.Cpf == string.Empty ? string.Empty : model.Cpf);
            sqlQuery.AppendFormat(", profissao = '{0}'  ", model.Profissao == string.Empty ? string.Empty : model.Profissao);
            sqlQuery.AppendFormat(", NomePlano = '{0}' ", model.NomePlano == string.Empty ? string.Empty : model.NomePlano );
            sqlQuery.AppendFormat(", DescricaoPlano = '{0}' ", model.DescricaoPlano == string.Empty ? string.Empty : model.DescricaoPlano);
            sqlQuery.AppendFormat(", NomeOperadora = '{0}' ", model.NomeOperadora == string.Empty ? string.Empty : model.NomeOperadora);
            sqlQuery.AppendFormat(", NomeAdministradora = '{0}' ", model.NomeAdministradora == string.Empty ? string.Empty : model.NomeAdministradora);
            sqlQuery.AppendFormat(", Abrangencia = '{0}' ", model.Abrangencia == string.Empty ? string.Empty : model.Abrangencia);
            sqlQuery.AppendFormat(", Acomodacao = '{0}' ", model.Acomodacao == string.Empty ? string.Empty : model.Acomodacao);
            sqlQuery.AppendFormat(", ValorTotal = '{0}' ", model.ValorTotal == string.Empty ? string.Empty : model.ValorTotal);
            sqlQuery.AppendFormat(", IdProposta = '{0}' ", model.idProposta == string.Empty ? string.Empty : model.idProposta);
            
            sqlQuery.AppendFormat("where chave = '{0}' ", model.Chave);


            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());
            return ExecuteNonQuery(ref sqlCommand);

        }




        public SimulacaoModel BuscaSimulacao(SimulacaoModel model)
        {
            StringBuilder sqlQuery = new StringBuilder();

            sqlQuery.AppendLine("SELECT  [id] ");
            sqlQuery.AppendLine(",[Chave] ");
            sqlQuery.AppendLine(",[Nome] ");
            sqlQuery.AppendLine(",[Cpf] ");
            sqlQuery.AppendLine(",[DataNascimento] ");
            sqlQuery.AppendLine(",[Email] ");
            sqlQuery.AppendLine(",[Celular] ");
            sqlQuery.AppendLine(",[Estado] ");
            sqlQuery.AppendLine(",[Profissao] ");
            sqlQuery.AppendLine(",[TitularResponsavel] ");
            sqlQuery.AppendLine(",[PossuiDependentes] ");
            sqlQuery.AppendLine(",[QtdDependentes] ");
            sqlQuery.AppendLine(",[DataSimulacao] ");
            sqlQuery.AppendLine(",[idProposta] ");
            sqlQuery.AppendLine(",[Empresa] ");
            sqlQuery.AppendLine(",[idPlano] ");
            sqlQuery.AppendLine(",[NomePlano] ");
            sqlQuery.AppendLine(",[NomeOperadora] ");
            sqlQuery.AppendLine(",[NomeAdministradora] ");
            sqlQuery.AppendLine(",[Abrangencia] ");
            sqlQuery.AppendLine(",[Acomodacao] ");
            sqlQuery.AppendLine(",[ValorTotal] ");
            sqlQuery.AppendFormat(",[DescricaoPlano] FROM [Simulacao].[dbo].[Simulacao_tb] WHERE Chave = '{0}' ", model.Chave);

            SqlCommand sqlCommand = new SqlCommand(sqlQuery.ToString());

             ExecuteReader(sqlCommand , this );

            model = Resultado;

            return model;


        }



        public SimulacaoModel Instanciar(SqlDataReader reader)
        {
            var simulacao = new SimulacaoModel();
            try
            {
                simulacao.Chave = (reader["Chave"] != DBNull.Value) ? reader["Chave"].ToString() : null;
                simulacao.Nome = (reader["Nome"] != DBNull.Value) ? reader["Nome"].ToString() : null;
                simulacao.Cpf = (reader["Cpf"] != DBNull.Value) ? reader["Cpf"].ToString() : null;
                simulacao.DataNascimento = (reader["DataNascimento"] != DBNull.Value) ? reader["DataNascimento"].ToString() : null;
                simulacao.Email = (reader["Email"] != DBNull.Value) ? reader["Email"].ToString() : null;
                simulacao.Celular = (reader["Celular"] != DBNull.Value) ? reader["Celular"].ToString() : null;
                simulacao.Estado = (reader["Estado"] != DBNull.Value) ? reader["Estado"].ToString() : null;
                simulacao.Profissao = (reader["Profissao"] != DBNull.Value) ? reader["Profissao"].ToString() : null;
                simulacao.TitularResponsavel = (reader["TitularResponsavel"] != DBNull.Value) ? reader["TitularResponsavel"].ToString() : null;
                simulacao.PossuiDependentes = (reader["PossuiDependentes"] != DBNull.Value) ? reader["PossuiDependentes"].ToString() : null;
                simulacao.QtdDependentes = (reader["QtdDependentes"] != DBNull.Value) ? Convert.ToInt32(reader["QtdDependentes"]) : 0;
                simulacao.DataSimulacao = (reader["DataSimulacao"] != DBNull.Value) ? Convert.ToDateTime(reader["DataSimulacao"]) : DateTime.MinValue;
                simulacao.Empresa = (reader["Empresa"] != DBNull.Value) ? reader["Empresa"].ToString() : null;
                simulacao.idPlano = (reader["idPlano"] != DBNull.Value) ? Convert.ToInt32(reader["idPlano"]) : 0;
                simulacao.NomePlano = (reader["NomePlano"] != DBNull.Value) ? reader["NomePlano"].ToString() : null;
                simulacao.NomeOperadora = (reader["NomeOperadora"] != DBNull.Value) ? reader["NomeOperadora"].ToString() : null;
                simulacao.NomeAdministradora = (reader["NomeAdministradora"] != DBNull.Value) ? reader["NomeAdministradora"].ToString() : null;
                simulacao.Abrangencia = (reader["Abrangencia"] != DBNull.Value) ? reader["Abrangencia"].ToString() : null;
                simulacao.Acomodacao = (reader["Acomodacao"] != DBNull.Value) ? reader["Acomodacao"].ToString() : null;
                simulacao.ValorTotal = (reader["ValorTotal"] != DBNull.Value) ? reader["ValorTotal"].ToString() : null;
                simulacao.DescricaoPlano = (reader["DescricaoPlano"] != DBNull.Value) ? reader["DescricaoPlano"].ToString() : null;

            }
            catch (Exception exp)
            {
               
            }
            return simulacao;
        }

    }
}
