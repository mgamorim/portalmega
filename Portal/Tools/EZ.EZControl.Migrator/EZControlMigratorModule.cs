using System.Data.Entity;
using System.Reflection;
using Abp.Events.Bus;
using Abp.Modules;
using Castle.MicroKernel.Registration;
using EZ.EZControl.EntityFramework;

namespace EZ.EZControl.Migrator
{
    [DependsOn(typeof(EZControlDataModule))]
    public class EZControlMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<EZControlDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(typeof(IEventBus), () =>
            {
                IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                );
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}