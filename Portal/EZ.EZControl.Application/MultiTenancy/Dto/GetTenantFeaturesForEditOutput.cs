using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EZ.EZControl.Editions.Dto;

namespace EZ.EZControl.MultiTenancy.Dto
{
    public class GetTenantFeaturesForEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}