﻿using Abp.Application.Services;
using EZ.EZControl.Tenants.Dashboard.Dto;

namespace EZ.EZControl.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();
    }
}
