﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Enum
{
    public enum TipoEnum
    {
        Indefinido = 0,
        Persistencia = 1,
        Comunicacao = 2,
        Solicitacao = 3,
        Execucao = 4,
        Enfileiramento = 5,
        Gerenciamento = 6,
        Agendamento = 7
    }
}
