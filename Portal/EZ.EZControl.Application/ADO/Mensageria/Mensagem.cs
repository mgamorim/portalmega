﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Mensageria
{
    [Serializable]
    public class Mensagem
    {
        #region Atributos

        private string conteudo;
        private TipoMensagem tipo;

        #endregion

        #region Construtores

        public Mensagem()
        {
            this.conteudo = string.Empty;
            this.tipo = TipoMensagem.Informacao;
        }

        public Mensagem(string conteudo, TipoMensagem tipo)
        {
            this.conteudo = conteudo;
            this.tipo = tipo;
        }

        #endregion

        #region Propriedades

        [DataMember()]
        public string Conteudo
        {
            get { return conteudo; }
            set { conteudo = value; }
        }

        [DataMember()]
        public TipoMensagem Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }

        #endregion

        #region Operacoes

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Mensagem[ ");

            sb.Append("Conteúdo: "); sb.Append(conteudo);
            sb.Append(" | ");
            sb.Append("Tipo: "); sb.Append(tipo);
            sb.Append("]");

            return sb.ToString();
        }

        #endregion
    }
}
