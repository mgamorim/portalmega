﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Mensageria
{
    [Serializable]
    public enum TipoMensagem
    {
        [EnumMember]
        Informacao = 0,
        [EnumMember]
        Alerta = 1,
        [EnumMember]
        Erro = 2,
        [EnumMember]
        Excecao = 3,
        [EnumMember]
        Depuracao = 4
    }
}
