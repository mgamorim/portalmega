﻿using EZ.EZControl.ADO.Base;
using EZ.EZControl.ADO.Helpers;
using EZ.EZControl.ADO.Mensageria;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace EZ.EZControl.ADO.SqlServer
{
   public abstract class ConexaoSqlServer<T> : RepositorioBase<T> where T : class
    {
        #region Atributos

        private readonly string connString;
        private Dictionary<string, SqlParameter> parametros;

        #endregion

        #region Construtores

        protected ConexaoSqlServer(string connString) : base()
        {
            this.connString = connString;
            Parametros = new Dictionary<string, SqlParameter>();
        }

        #endregion

        #region Propriedades

        protected string ConnString
        {
            get { return connString; }
        }

        public Dictionary<string, SqlParameter> Parametros
        {
            get { return parametros; }
            set { parametros = value; }
        }

        #endregion

        #region Operacoes

        protected virtual bool Contar(SqlCommand cmd)
        {
            bool retorno;

            base.Mensagens.Clear();

            using (SqlConnection cnn = new SqlConnection(connString))
            {
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cnn;

                    cnn.Open();

                    base.QtdeRegistros = (int)cmd.ExecuteScalar();

                    retorno = true;
                }
                catch (Exception exception)
                {
                    base.Mensagens.Add(new Mensagem("Não foi possível efetuar a contagem de registros!", TipoMensagem.Erro));
                    base.Mensagens.Add(new Mensagem(ExceptionHelper.GetMessagesAsString(exception), TipoMensagem.Excecao));
                    base.QtdeRegistros = -1;
                    retorno = false;
                }
            }

            return retorno;
        }

        protected virtual bool ExecuteScalar(SqlCommand cmd)
        {
            bool retorno;

            base.Mensagens.Clear();

            using (SqlConnection cnn = new SqlConnection(connString))
            {
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cnn;

                    cnn.Open();

                    base.QtdeRegistros = (int)cmd.ExecuteScalar();

                    retorno = true;
                }
                catch (SqlException sqlException)
                {
                    AvaliarExcecao(sqlException);

                    base.QtdeRegistros = -1;
                    retorno = false;
                }
                catch (Exception exception)
                {
                    base.Mensagens.Add(new Mensagem("Não foi possível executar o comando solicitado!", TipoMensagem.Erro));
                    base.Mensagens.Add(new Mensagem(ExceptionHelper.GetMessagesAsString(exception), TipoMensagem.Excecao));
                    base.QtdeRegistros = -1;
                    retorno = false;
                }
            }

            return retorno;
        }

        protected bool ExecuteReader(SqlCommand cmd, ISqlServerInstanciavel<T> chamador)
        {
            bool retorno = true;

            base.Mensagens.Clear();
            base.Resultados.Clear();

            T objeto;

            using (SqlConnection cnn = new SqlConnection(WebConfigurationManager.ConnectionStrings["Default"].ToString()))
            {
                try
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnn;
                    cnn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            objeto = chamador.Instanciar(reader);

                            if (objeto != null)
                            {
                                base.Resultados.Add(objeto);
                            }
                            else
                            {
                                retorno = false;
                                break;
                            }
                        }
                    }

                    base.QtdeRegistros = base.Resultados.Count;

                    if (base.QtdeRegistros == 0)
                    {
                        base.Mensagens.Add(new Mensagem("Nenhum registro foi localizado no banco de dados para o(s) critério(s) especificado(s)!", TipoMensagem.Alerta));
                        //retorno = false; --> O retorno somente será false em caso de exceção. Os clientes dos repositórios deverão verificar o retorno true e a propriedade PossuiResultados, conforme cada caso.
                    }
                }
                catch (SqlException sqlException)
                {
                    AvaliarExcecao(sqlException);

                    base.Resultados.Clear();
                    base.QtdeRegistros = -1;
                    retorno = false;
                }
                catch (Exception exception)
                {
                    base.Mensagens.Add(new Mensagem("Não foi possível executar o comando solicitado!", TipoMensagem.Erro));
                    base.Mensagens.Add(new Mensagem(ExceptionHelper.GetMessagesAsString(exception), TipoMensagem.Excecao));
                    base.Resultados.Clear();
                    base.QtdeRegistros = -1;
                    retorno = false;
                }
            }

            return retorno;
        }

        protected bool ExecuteNonQuery(ref SqlCommand cmd, bool ignorarQtde = false)
        {
            bool retorno;

            base.Mensagens.Clear();

            using (SqlConnection cnn = new SqlConnection(connString))
            {
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cnn;
                    cnn.Open();

                    base.QtdeRegistros = cmd.ExecuteNonQuery();

                    if (ignorarQtde || base.QtdeRegistros > 0)
                    {
                        retorno = true;
                    }
                    else
                    {
                        base.Mensagens.Add(new Mensagem("Nenhum registro afetado no banco de dados!", TipoMensagem.Alerta));
                        retorno = false;
                    }

                }
                catch (SqlException sqlException)
                {
                    AvaliarExcecao(sqlException);

                    base.QtdeRegistros = -1;
                    retorno = false;
                }
                catch (Exception exception)
                {
                    base.Mensagens.Add(new Mensagem("Não foi possível executar o comando solicitado!", TipoMensagem.Erro));
                    base.Mensagens.Add(new Mensagem(ExceptionHelper.GetMessagesAsString(exception), TipoMensagem.Excecao));
                    base.QtdeRegistros = -1;
                    retorno = false;
                }
            }

            return retorno;
        }

        protected bool ExecuteQueryText(SqlCommand cmd, out string texto)
        {
            bool retorno = true;

            base.Mensagens.Clear();

            texto = string.Empty;

            using (SqlConnection cnn = new SqlConnection(connString))
            {
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cnn;
                    cnn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            texto = reader.GetString(0);
                        }
                        else
                        {
                            retorno = false;
                        }
                    }

                    if (!retorno)
                    {
                        base.Mensagens.Add(new Mensagem("Nenhum texto foi localizado no banco de dados para o(s) critério(s) especificado(s)!", TipoMensagem.Alerta));
                    }
                }
                catch (SqlException sqlException)
                {
                    AvaliarExcecao(sqlException);
                    retorno = false;
                }
                catch (Exception exception)
                {
                    base.Mensagens.Add(new Mensagem("Não foi possível executar o comando solicitado!", TipoMensagem.Erro));
                    base.Mensagens.Add(new Mensagem(ExceptionHelper.GetMessagesAsString(exception), TipoMensagem.Excecao));
                    retorno = false;
                }
            }

            return retorno;
        }

        #endregion

        #region Operacoes privadas

        private void AvaliarExcecao(SqlException sqlException)
        {
            // TODO: Avaliar outras possíveis exceções (constraints, etc.)
            switch (sqlException.Number)
            {
                case -1:
                case 2:
                case 4060:
                case 18456:
                    base.Mensagens.Add(new Mensagem("Não foi possível conectar ao banco de dados!", TipoMensagem.Erro));
                    break;

                case 547:
                    base.Mensagens.Add(new Mensagem("Não foi possível realizar a operação devido a dependência de dados!", TipoMensagem.Erro));
                    break;

                case 2601:
                case 2627:
                    base.Mensagens.Add(new Mensagem("Registro já existente no bando de dados com os dados informados!", TipoMensagem.Erro));
                    break;

                default:
                    base.Mensagens.Add(new Mensagem("Não foi possível executar a operação solicitada!", TipoMensagem.Erro));
                    break;
            }

            base.Mensagens.Add(new Mensagem(ExceptionHelper.GetMessagesAsString(sqlException), TipoMensagem.Excecao));
        }

        #endregion

    }
}
