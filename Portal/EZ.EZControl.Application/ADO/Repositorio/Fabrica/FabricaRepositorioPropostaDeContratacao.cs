﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio.Fabrica
{
    public class FabricaRepositorioPropostaDeContratacao
    {
        #region atributos

        private static volatile FabricaRepositorioPropostaDeContratacao _instancia;

        #endregion

        #region construtores
        public FabricaRepositorioPropostaDeContratacao()
        {
        }
        #endregion

        #region propriedades
        public static FabricaRepositorioPropostaDeContratacao Instancia
        {
            get { return _instancia ?? (_instancia = new FabricaRepositorioPropostaDeContratacao()); }
        }
        #endregion

        #region operacoes

        public RepositorioPropostaDeContratacao Criar(string provedor, string connString)
        {
            switch (provedor.Trim().ToLower())
            {
                case "sql":
                    return new RepositorioPropostaDeContratacao(connString);
                default:
                    throw new ArgumentException("Repositório inválido!");
            }
        }
        #endregion




    }
}
