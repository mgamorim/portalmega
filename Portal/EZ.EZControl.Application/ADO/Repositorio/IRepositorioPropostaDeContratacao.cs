﻿using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Repositorio
{
    public interface IRepositorioPropostaDeContratacao
    {
         List<PropostaDeContratacaoInput> CarregarGrid();
    }
}
