﻿using EZ.EZControl.ADO.Enum;
using EZ.EZControl.ADO.Mensageria;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZ.EZControl.ADO.SqlServer;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;

namespace EZ.EZControl.ADO.Repositorio
{
    public class RepositorioPropostaDeContratacao : ConexaoSqlServer<PropostaDeContratacaoInput>, IRepositorioPropostaDeContratacao, ISqlServerInstanciavel<PropostaDeContratacaoInput>
    {
        private const string EMISSOR = "RepositorioPropostaDeContratacao";
        private const TipoEnum TIPO = TipoEnum.Persistencia;

        public RepositorioPropostaDeContratacao(string connString) : base(connString)
        {
        }

        public List<PropostaDeContratacaoInput> CarregarGrid()
        {
            List<PropostaDeContratacaoInput> valores = new List<PropostaDeContratacaoInput>();


            var msgResposta = new MsgResposta<PropostaDeContratacaoInput>(EMISSOR, "ObterServicoPorId", TIPO, true);

            StringBuilder sql = new StringBuilder();

            sql.AppendLine("SELECT  top 300");
sql.AppendLine("[Extent1].[Id] AS[Id],                                                                                 ");
            sql.AppendLine("[Extent1].[Numero] AS[Numero],                                                             ");
sql.AppendLine("[Extent1].[TitularId] AS[TitularId],                                                                   ");
sql.AppendLine("[Extent1].[ProdutoId] AS[ProdutoId],                                                                   ");
sql.AppendLine("[Extent1].[ResponsavelId] AS[ResponsavelId],                                                           ");
sql.AppendLine("[Extent1].[Aceite] AS[Aceite],                                                                         ");
sql.AppendLine("[Extent1].[AceiteDaDeclaracaoDeSaude] AS[AceiteDaDeclaracaoDeSaude],                                   ");
sql.AppendLine("[Extent1].[TipoDeHomologacao] AS[TipoDeHomologacao],                                                   ");
sql.AppendLine("[Extent1].[DataHoraDaHomologacao] AS[DataHoraDaHomologacao],                                           ");
sql.AppendLine("[Extent1].[ObservacaoHomologacao] AS[ObservacaoHomologacao],                                           ");
sql.AppendLine("[Extent1].[DataHoraDoAceite] AS[DataHoraDoAceite],                                                     ");
sql.AppendLine("[Extent1].[ContratoVigenteNoAceiteId] AS[ContratoVigenteNoAceiteId],                                   ");
sql.AppendLine("[Extent1].[UltimoContratoAceitoId] AS[UltimoContratoAceitoId],                                         ");
sql.AppendLine("[Extent1].[CorretorId] AS[CorretorId],                                                                 ");
sql.AppendLine("[Extent1].[CorretoraId] AS[CorretoraId],                                                               ");
sql.AppendLine("[Extent1].[PedidoId] AS[PedidoId],                                                                     ");
sql.AppendLine("[Extent1].[StatusDaProposta] AS[StatusDaProposta],                                                     ");
sql.AppendLine("[Extent1].[PassoDaProposta] AS[PassoDaProposta],                                                       ");
sql.AppendLine("[Extent1].[FormaDeContratacao] AS[FormaDeContratacao],                                                 ");
sql.AppendLine("[Extent1].[VigenciaId] AS[VigenciaId],                                                                 ");
sql.AppendLine("[Extent1].[ChancelaId] AS[ChancelaId],                                                                 ");
sql.AppendLine("[Extent1].[InicioDeVigencia] AS[InicioDeVigencia],                                                     ");
sql.AppendLine("[Extent1].[TipoDeProposta] AS[TipoDeProposta],                                                         ");
sql.AppendLine("[Extent1].[UsuarioTitularId] AS[UsuarioTitularId],                                                     ");
sql.AppendLine("[Extent1].[UsuarioResponsavelId] AS[UsuarioResponsavelId],                                             ");
sql.AppendLine("[Extent1].[ProfissaoId] AS[ProfissaoId],                                                               ");
sql.AppendLine("[Extent1].[EServidorPublico] AS[EServidorPublico],                                                     ");
sql.AppendLine("[Extent1].[EstadoId] AS[EstadoId],                                                                     ");
sql.AppendLine("[Extent1].[TitularResponsavelLegal] AS[TitularResponsavelLegal],                                       ");
sql.AppendLine("[Extent1].[ItemDeDeclaracaoDoBeneficiarioId] AS[ItemDeDeclaracaoDoBeneficiarioId],                     ");
sql.AppendLine("[Extent1].[AceiteCorretor] AS[AceiteCorretor],                                                         ");
sql.AppendLine("[Extent1].[DataAceiteCorretor] AS[DataAceiteCorretor],                                                 ");
sql.AppendLine("[Extent1].[GerenteId] AS[GerenteId],                                                                   ");
sql.AppendLine("[Extent1].[SupervisorId] AS[SupervisorId],                                                             ");
sql.AppendLine("[Extent1].[TenantId] AS[TenantId],                                                                     ");
sql.AppendLine("[Extent1].[ExternalId] AS[ExternalId],                                                                 ");
sql.AppendLine("[Extent1].[DateOfEditionIntegration] AS[DateOfEditionIntegration],                                     ");
sql.AppendLine("[Extent1].[IsDeleted] AS[IsDeleted],                                                                   ");
sql.AppendLine("[Extent1].[DeleterUserId] AS[DeleterUserId],                                                           ");
sql.AppendLine("[Extent1].[DeletionTime] AS[DeletionTime],                                                             ");
sql.AppendLine("[Extent1].[LastModificationTime] AS[LastModificationTime],                                             ");
sql.AppendLine("[Extent1].[LastModifierUserId] AS[LastModifierUserId],                                                 ");
sql.AppendLine("[Extent1].[CreationTime] AS[CreationTime],                                                             ");
sql.AppendLine("[Extent1].[CreatorUserId] AS[CreatorUserId]                                                            ");
sql.AppendLine("FROM[dbo].[Sau_PropostaDeContratacao] AS[Extent1]                                                      ");
sql.AppendLine("WHERE 1 = 1                                                                                            ");
sql.AppendLine("--(([Extent1].[TitularId] = @DynamicFilterParam_000007) OR(@DynamicFilterParam_000008 IS NOT NULL))    ");
sql.AppendLine("--AND(([Extent1].[CorretorId] = @DynamicFilterParam_000009) OR(@DynamicFilterParam_000010 IS NOT NULL) ");
sql.AppendLine("AND([Extent1].[TenantId] IS not NULL) ");

            var sqlCommand = new SqlCommand(sql.ToString());

            if (base.ExecuteReader(sqlCommand, this))
            {
                valores = Resultados;
            }
            else
            {
            }

            return valores;
        }

        public PropostaDeContratacaoInput Instanciar(SqlDataReader reader)
        {
            var proposta = new PropostaDeContratacaoInput();

            proposta.Numero = (reader["Numero"] != DBNull.Value) ? reader["Numero"].ToString() : null;
            proposta.Aceite = (Convert.ToBoolean(reader["Aceite"]));
            
            return proposta;

        }
    }
}
