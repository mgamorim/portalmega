﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.ADO.Helpers
{
    public static class ExceptionHelper
    {
        #region Operations

        public static string GetMessagesAsString(Exception exception)
        {
            if (exception.InnerException == null)
            {
                return exception.Message;
            }

            return string.Concat(exception.Message, " | ", GetMessagesAsString(exception.InnerException));
        }

        #endregion
    }
}
