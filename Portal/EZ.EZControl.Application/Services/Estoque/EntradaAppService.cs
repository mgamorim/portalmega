﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.Entrada;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using System;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Estoque.Geral.EntradaItem;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EntradaAppService : EZControlAppServiceBase<Entrada>, IEntradaAppService
    {
        private readonly IEntradaService _servicoService;
        private readonly IEmpresaService _empresaService;
        private readonly IFornecedorService _fornecedorService;
        private readonly IEntradaItemAppService _entradaItemAppService;
        private readonly ISaldoEstoqueService _saldoEstoqueService;
        private readonly IRepository<EntradaItem> _entradaItemRepository;        

        public EntradaAppService(IRepository<Entrada> servicoRepository, 
            IEntradaService servicoService,
            IEmpresaService empresaService, 
            IFornecedorService fornecedorService,
            IEntradaItemAppService entradaItemAppService,
            ISaldoEstoqueService saldoEstoqueService,
            IRepository<EntradaItem> entradaItemRepository)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _empresaService = empresaService;
            _fornecedorService = fornecedorService;
            _entradaItemRepository = entradaItemRepository;
            _entradaItemAppService = entradaItemAppService;
            _saldoEstoqueService = saldoEstoqueService;
        }

        private async Task ProcessInput(Entrada entrada, EntradaInput input)
        {   
            #region Fornecedor
            if (input.FornecedorId.HasValue)
            {
                Fornecedor fornecedor = null;

                try
                {
                    fornecedor = await _fornecedorService.GetById(input.FornecedorId.Value);
                    entrada.AssociarFornecedor(fornecedor, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }                                               
            }
            #endregion            
        }

        private void ValidateInput(EntradaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.DataEntrada == default(DateTime))
            {
                validationErrors.Add(new ValidationResult(L("Entrada.DataEntradaEmptyError")));
            }                        

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Estoque_Entrada_Edit)]
        public async Task<IdInput> Save(EntradaInput input)
        {
            IdInput result = null;

            try
            {
                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Entrada_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, EntradaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Entrada_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, EntradaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;

            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.InnerException.InnerException.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Estoque_Entrada_Edit)]
        public async Task<EntradaInput> SaveAndReturnEntity(EntradaInput input)
        {
            EntradaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Entrada_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<EntradaInput, EntradaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Entrada_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<EntradaInput, EntradaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada_Delete)]
        public async Task Delete(IdInput input)
        {
            var itens = await _entradaItemAppService.GetItemsByEntrada(input.Id);
            foreach (EntradaItemListDto item in itens)
            {
                await _saldoEstoqueService.RemoverSaldoPorEntrada(input.Id);
                await _entradaItemAppService.Delete(new IdInput { Id = item.Id });
            }
            await base.DeleteEntity(_servicoService, input);
        }

        public async Task<PagedResultDto<EntradaListDto>> GetEntradaPaginado(GetEntradaInput input)
        {
            var condicoes = new List<WhereIfCondition<Entrada>>
            {
                new WhereIfCondition<Entrada>(
                        input.Id != default(int),
                    a =>
                        a.Id == input.Id)
            };
            var result = await base.GetListPaged<EntradaListDto, GetEntradaInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<EntradaInput> GetById(IdInput input)
        {
            EntradaInput entrada = await base.GetEntityById<EntradaInput, IdInput>(_servicoService, input);

            if (entrada != null)
            {
                GetEntradaItemInput item = new GetEntradaItemInput();
                item.EntradaId = input.Id;
                entrada.GridItens = await _entradaItemAppService.GetEntradaItemPaginado(item);
                entrada.Items = await _entradaItemAppService.GetItemsByEntrada(input.Id);
            }
            return entrada;
        }

        public async Task<EntradaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EntradaInput, IdInput>(_servicoService, input);
        }

        public async Task<PagedResultDto<EntradaItemListDto>> GetEntradaItemPaginado(GetEntradaItemInput input)
        {
            var result = await _entradaItemAppService.GetEntradaItemPaginado(input);
            return result;
        }
    }
}