﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.EntradaItem;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EntradaItemAppService : EZControlAppServiceBase<EntradaItem>, IEntradaItemAppService
    {
        private readonly IEntradaItemService _servicoService;
        private readonly IEntradaService _entradaService;
        private readonly IProdutoService _produtoService;
        private readonly ILocalArmazenamentoService _localArmazenamentoService;
        private readonly IRepository<EntradaItem> _servicoRepository;
        private readonly ISaldoEstoqueService _saldoEstoqueService;

        public EntradaItemAppService(IRepository<EntradaItem> servicoRepository, 
            IEntradaItemService servicoService,
            IEntradaService entradaService,
            IProdutoService produtoService,
            ILocalArmazenamentoService localArmazenamentoService,
            ISaldoEstoqueService saldoEstoqueService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _entradaService = entradaService;
            _produtoService = produtoService;
            _localArmazenamentoService = localArmazenamentoService;
            _servicoRepository = servicoRepository;
            _saldoEstoqueService = saldoEstoqueService;
        }

        private void ValidateInput(EntradaItemInput input, bool update)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.EntradaId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("Estoque.EntradaEmptyError")));
            }

            if (input.ProdutoId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("Estoque.EntradaItem.ProdutoEmptyError")));
            }

            if (!update)
            {
                if (CheckForDuplicateInstance(Repository, input.Id, x => x.ProdutoId == input.ProdutoId && x.EntradaId == input.EntradaId))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Estoque.EntradaItem.ProdutoDuplicateError"), input.ProdutoId)));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(EntradaItem entradaItem, EntradaItemInput input)
        {
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.EntradaItem.ProdutoNotFoundError"));
                }

                try
                {
                    entradaItem.AssociarProduto(produto, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.EntradaItem.ProdutoEmptyError"));
            }
            #endregion

            #region Entrada
            if (input.EntradaId > 0)
            {
                Entrada entrada = null;

                try
                {
                    entrada = await _entradaService.GetById(input.EntradaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.EntradaNotFoundError"));
                }

                try
                {
                    entradaItem.AssociarEntrada(entrada, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.EntradaEmptyError"));
            }
            #endregion

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    entradaItem.AssociarLocalArmazenamento(localArmazenamento, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task ProcessInputEntrada(SaldoEstoque saldoEstoque, SaldoEstoqueEntradaInput input)
        {
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                    saldoEstoque.AssociarProduto(produto, _saldoEstoqueService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    saldoEstoque.AssociarLocalArmazenamento(localArmazenamento, _saldoEstoqueService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region Entrada e Empresa
            if (input.EntradaId > 0)
            {
                Entrada entrada = null;

                try
                {
                    entrada = await _entradaService.GetById(input.EntradaId);
                    saldoEstoque.AssociarEntrada(entrada, _saldoEstoqueService);
                    saldoEstoque.AssociarEmpresa(entrada.Empresa, _saldoEstoqueService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task AtualizarSaldo(EntradaItemInput input, bool estorno)
        {
            SaldoEstoqueEntradaInput inputSaldo = new SaldoEstoqueEntradaInput { EntradaId = input.EntradaId, ProdutoId = input.ProdutoId, Quantidade = input.Quantidade, LocalArmazenamentoId = input.LocalArmazenamentoId };
            SaldoEstoque saldo = inputSaldo.MapTo<SaldoEstoque>();
            await ProcessInputEntrada(saldo, inputSaldo);
            saldo.PrepararEntrada(_saldoEstoqueService);

            if (estorno)
                await _saldoEstoqueService.EstornarEntrada(saldo);
            else
                await _saldoEstoqueService.AtualizarEntrada(saldo);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada_Edit)]
        public async Task<IdInput> Save(EntradaItemInput input)
        {
            IdInput result = null;

            if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Entrada_Edit))
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));

            if (input.Id == default(int))
            {                                                                  
                result = await base.CreateEntity<IdInput, EntradaItemInput>(_servicoService, input, () => ValidateInput(input, false), ProcessInput);
            }
            else
            {
                result = await base.UpdateEntity<IdInput, EntradaItemInput>(_servicoService, input, () => ValidateInput(input, true), ProcessInput);
            }

            await AtualizarSaldo(input, false);

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada_Edit)]
        public async Task Delete(IdInput input)
        {
            EntradaItem item = Repository.Get(input.Id);
            await base.DeleteEntity(_servicoService, input);

            await AtualizarSaldo(item.MapTo<EntradaItemInput>(), true);
        }

        public async Task<PagedResultDto<EntradaItemListDto>> GetEntradaItemPaginado(GetEntradaItemInput input)
        {
            var condicoes = new List<WhereIfCondition<EntradaItem>>
            {
                new WhereIfCondition<EntradaItem>(true, x => x.Entrada.Id == input.EntradaId)
            };
            var result = await base.GetListPaged<EntradaItemListDto, GetEntradaItemInput>(_servicoService, input, condicoes);

            foreach (EntradaItemListDto item in result.Items)
            {
                Produto prod = await _produtoService.GetById(item.ProdutoId);
                item.Produto = prod.MapTo<ProdutoListDto>();
                if (item.LocalArmazenamentoId.HasValue)
                {
                    LocalArmazenamento local = await _localArmazenamentoService.GetById(item.LocalArmazenamentoId.Value);
                    item.LocalArmazenamento = local.MapTo<LocalArmazenamentoListDto>();
                }
            }

            return result;
        }
        public async Task<IEnumerable<EntradaItemListDto>> GetItemsByEntrada(int entradaId)
        {
            
            List<EntradaItem> lista = await _servicoRepository.GetAllListAsync(x => x.EntradaId == entradaId);

            return lista.MapTo<List<EntradaItemListDto>>();
        }
    }
}