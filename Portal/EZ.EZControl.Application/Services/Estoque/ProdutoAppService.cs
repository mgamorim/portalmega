﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Services.Core.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Produto, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ProdutoAppService : EZControlAppServiceBase<Produto>, IProdutoAppService
    {
        private readonly IProdutoService _servicoService;
        private readonly IArquivoBaseService _arquivoServicoService;
        private readonly IUnidadeMedidaService _unidadeMedidaService;
        private readonly INaturezaService _naturezaService;
        private readonly IArquivoGlobalService _arquivoGlobalService;

        public ProdutoAppService(IRepository<Produto> servicoRepository, IProdutoService servicoService, IUnidadeMedidaService unidadeMedidaService, INaturezaService naturezaService, IArquivoGlobalService arquivoGlobalService, IArquivoBaseService arquivoServicoService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _unidadeMedidaService = unidadeMedidaService;
            _naturezaService = naturezaService;
            _arquivoGlobalService = arquivoGlobalService;
            _arquivoServicoService = arquivoServicoService;
        }

        private async Task ProcessInput(Produto produto, ProdutoInput input)
        {
            // Unidade de Medida
            if (input.UnidadeMedidaId.HasValue)
            {
                UnidadeMedida unidadeMedida = null;

                try
                {
                    unidadeMedida = await _unidadeMedidaService.GetById(input.UnidadeMedidaId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.UnidadeMedida.NotFoundError"));
                }

                try
                {
                    produto.AssociarUnidadeMedida(unidadeMedida, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.UnidadeMedida.EmptyError"));
            }

            // Natureza
            if (input.NaturezaId.HasValue)
            {
                Natureza natureza = null;

                try
                {
                    natureza = await _naturezaService.GetById(input.NaturezaId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.Natureza.NotFoundError"));
                }

                try
                {
                    produto.AssociarNatureza(natureza, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.Natureza.EmptyError"));
            }
            //Imagem
            if (input.ImagemId > 0)
            {
                try
                {
                    var imagem = await _arquivoGlobalService.GetById(input.ImagemId);
                    if (imagem != null)
                    {
                        produto.Imagem = imagem;
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        private void ValidateInput(ProdutoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Produto.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Produto.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Produto_Create, AppPermissions.Pages_Tenant_Estoque_Produto_Edit)]
        public async Task<IdInput> Save(ProdutoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Produto_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ProdutoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Produto_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ProdutoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Produto_Create, AppPermissions.Pages_Tenant_Estoque_Produto_Edit)]
        public async Task<ProdutoInput> SaveAndReturnEntity(ProdutoInput input)
        {
            ProdutoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Produto_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ProdutoInput, ProdutoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Produto_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ProdutoInput, ProdutoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ProdutoListDto>> GetProdutoPaginado(GetProdutoInput input)
        {
            var condicoes = new List<WhereIfCondition<Produto>>
            {
                new WhereIfCondition<Produto>(
                        !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<ProdutoListDto, GetProdutoInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<ProdutoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ProdutoInput, IdInput>(_servicoService, input);
        }

        public async Task<ProdutoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ProdutoInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Produto_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }
    }
}