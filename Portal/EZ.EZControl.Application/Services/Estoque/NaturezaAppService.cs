﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.Natureza;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class NaturezaAppService : EZControlAppServiceBase<Natureza>, INaturezaAppService
    {
        private readonly INaturezaService _servicoService;

        public NaturezaAppService(IRepository<Natureza> servicoRepository, INaturezaService servicoService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
        }

        private void ValidateInput(NaturezaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Estoque.Natureza.EmptyNomeError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Estoque.Natureza.DuplicateNomeError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Edit)]
        public async Task<IdInput> Save(NaturezaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, NaturezaInput>(_servicoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, NaturezaInput>(_servicoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Edit)]
        public async Task<NaturezaInput> SaveAndReturnEntity(NaturezaInput input)
        {
            NaturezaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<NaturezaInput, NaturezaInput>(_servicoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<NaturezaInput, NaturezaInput>(_servicoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        public async Task<PagedResultDto<NaturezaListDto>> GetNaturezaPaginado(GetNaturezaInput input)
        {
            var condicoes = new List<WhereIfCondition<Natureza>>
            {
                new WhereIfCondition<Natureza>(
                        !string.IsNullOrEmpty(input.Descricao),
                    a =>
                        a.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<NaturezaListDto, GetNaturezaInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<NaturezaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<NaturezaInput, IdInput>(_servicoService, input);
        }

        public async Task<NaturezaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<NaturezaInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Natureza_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }
    }
}