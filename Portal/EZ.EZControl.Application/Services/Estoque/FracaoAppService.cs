﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.Fracao;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using System;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class FracaoAppService : EZControlAppServiceBase<Fracao>, IFracaoAppService
    {
        private readonly IFracaoService _servicoService;
        private readonly IUnidadeMedidaService _unidadeMedidaService;

        public FracaoAppService(IRepository<Fracao> servicoRepository, IFracaoService servicoService, IUnidadeMedidaService unidadeMedidaService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _unidadeMedidaService = unidadeMedidaService;
        }

        private void ValidateInput(FracaoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Estoque.Fracao.EmptyNomeError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Estoque.Fracao.DuplicateNomeError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Edit)]
        public async Task<IdInput> Save(FracaoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, FracaoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, FracaoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Edit)]
        public async Task<FracaoInput> SaveAndReturnEntity(FracaoInput input)
        {
            FracaoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<FracaoInput, FracaoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<FracaoInput, FracaoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<FracaoListDto>> GetFracaoPaginado(GetFracaoInput input)
        {
            var condicoes = new List<WhereIfCondition<Fracao>>
            {
                new WhereIfCondition<Fracao>(
                        !string.IsNullOrEmpty(input.Descricao),
                    a =>
                        a.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<FracaoListDto, GetFracaoInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<FracaoInput> GetById(IdInput input)
        {   
            return await base.GetEntityById<FracaoInput, IdInput>(_servicoService, input);
        }

        public async Task<FracaoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<FracaoInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_Fracao_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }

        private async Task ProcessInput(Fracao fracao, FracaoInput input)
        {
            if (input.UnidadeMedidaId > 0)
            {   
                UnidadeMedida unidadeMedida = null;

                try
                {
                    unidadeMedida = await _unidadeMedidaService.GetById(input.UnidadeMedidaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.UnidadeMedida.NotFoundError"));
                }

                try
                {
                    fracao.AssociarUnidadeMedida(unidadeMedida, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.UnidadeMedida.EmptyError"));
            }
        }
    }
}