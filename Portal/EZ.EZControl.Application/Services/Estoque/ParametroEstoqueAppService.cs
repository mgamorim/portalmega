﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.Parametro;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ParametroEstoqueAppService : EZControlAppServiceBase<ParametroEstoque>, IParametroEstoqueAppService
    {
        private readonly IParametroEstoqueService parametroEstoqueService;

        public ParametroEstoqueAppService(IRepository<ParametroEstoque> parametroRepository,
            IParametroEstoqueService parametroEstoqueService)
            : base(parametroRepository)
        {
            this.parametroEstoqueService = parametroEstoqueService;
        }

        private void ValidateInput(ParametroEstoqueInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<IdInput> Save(ParametroEstoqueInput input)
        {
            var parametro = await Get();
            if (parametro != null)
                input.Id = parametro.Id;

            IdInput savedEntity;

            if (input.Id == default(int))
            {
                savedEntity = await base.CreateEntity<IdInput, ParametroEstoqueInput>(parametroEstoqueService, input, () => ValidateInput(input), null);
            } else
            {
                savedEntity = await base.UpdateEntity<IdInput, ParametroEstoqueInput>(parametroEstoqueService, input, () => ValidateInput(input), null);
            }

            return savedEntity;
        }

        public async Task<ParametroEstoqueInput> Get()
        {
            var condicoes = new List<WhereIfCondition<ParametroEstoque>>();
            condicoes.Add(new WhereIfCondition<ParametroEstoque>(true, x => x.Id > 0));
            var parametros = await base.GetList<ParametroEstoqueInput>(parametroEstoqueService, condicoes, x => x.Id);

            return parametros.Items.FirstOrDefault();
        }
    }
}