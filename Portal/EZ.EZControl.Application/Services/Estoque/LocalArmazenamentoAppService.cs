﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using System;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Domain.Global.SubtiposPessoa;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class LocalArmazenamentoAppService : EZControlAppServiceBase<LocalArmazenamento>, ILocalArmazenamentoAppService
    {
        private readonly ILocalArmazenamentoService _servicoService;
        private readonly IEmpresaService _empresaService;

        public LocalArmazenamentoAppService(IRepository<LocalArmazenamento> servicoRepository, 
            ILocalArmazenamentoService servicoService, 
            IEmpresaService empresaService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _empresaService = empresaService;
        }

        private async Task ProcessInput(LocalArmazenamento localArmazenamento, LocalArmazenamentoInput input)
        {
            // Empresa
            if (input.EmpresaId > 0)
            {
                Empresa empresa = null;

                try
                {
                    empresa = await _empresaService.GetById(input.EmpresaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                }

                try
                {
                    localArmazenamento.AssociarEmpresa(empresa, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.EmpresaEmptyError"));
            }
        }

        private void ValidateInput(LocalArmazenamentoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("LocalArmazenamento.DescricaoEmptyError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao && x.Empresa.Id == input.EmpresaId))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("LocalArmazenamento.DescricaoDuplicateError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Edit)]
        public async Task<IdInput> Save(LocalArmazenamentoInput input)
        {
            try
            {
                IdInput result = null;

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, LocalArmazenamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, LocalArmazenamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;
            }
            catch(Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Edit)]
        public async Task<LocalArmazenamentoInput> SaveAndReturnEntity(LocalArmazenamentoInput input)
        {
            LocalArmazenamentoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<LocalArmazenamentoInput, LocalArmazenamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<LocalArmazenamentoInput, LocalArmazenamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<LocalArmazenamentoListDto>> GetLocalArmazenamentoPaginado(GetLocalArmazenamentoInput input)
        {
            var condicoes = new List<WhereIfCondition<LocalArmazenamento>>
            {
                new WhereIfCondition<LocalArmazenamento>(
                        !string.IsNullOrEmpty(input.Descricao),
                    a =>
                        a.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<LocalArmazenamentoListDto, GetLocalArmazenamentoInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<LocalArmazenamentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<LocalArmazenamentoInput, IdInput>(_servicoService, input);
        }

        public async Task<LocalArmazenamentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<LocalArmazenamentoInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_LocalArmazenamento_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }
    }
}