﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.Saida;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using System;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Estoque.Geral.SaidaItem;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SaidaAppService : EZControlAppServiceBase<Saida>, ISaidaAppService
    {
        private readonly ISaidaService _servicoService;
        private readonly IEmpresaService _empresaService;
        private readonly IFornecedorService _fornecedorService;
        private readonly ISaidaItemAppService _saidaItemAppService;
        private readonly ISaldoEstoqueService _saldoEstoqueService;
        private readonly IRepository<SaidaItem> _saidaItemRepository;
        private readonly IPedidoService _pedidoService;

        public SaidaAppService(IRepository<Saida> servicoRepository,
            ISaidaService servicoService,
            IEmpresaService empresaService,
            IFornecedorService fornecedorService,
            ISaidaItemAppService saidaItemAppService,
            ISaldoEstoqueService saldoEstoqueService,
            IRepository<SaidaItem> saidaItemRepository,
            IPedidoService pedidoService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _empresaService = empresaService;
            _fornecedorService = fornecedorService;
            _saidaItemRepository = saidaItemRepository;
            _saidaItemAppService = saidaItemAppService;
            _pedidoService = pedidoService;
            _saldoEstoqueService = saldoEstoqueService;
        }

        private async Task ProcessInput(Saida saida, SaidaInput input)
        {
            #region Empresa
            if (input.EmpresaId > 0)
            {
                Empresa empresa = null;

                try
                {
                    empresa = await _empresaService.GetById(input.EmpresaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                }

                try
                {
                    saida.AssociarEmpresa(empresa, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.EmpresaEmptyError"));
            }
            #endregion

            #region Pedido
            if (input.PedidoId.HasValue)
            {
                Pedido pedido = null;

                try
                {
                    pedido = await _pedidoService.GetById(input.PedidoId.Value);
                    saida.AssociarPedido(pedido, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            
        }

        private void ValidateInput(SaidaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            //if (input.Items.Count() == 0)
            //{
            //    validationErrors.Add(new ValidationResult(L("Saida.EmptyNomeError")));
            //}
            //if (input.DataSaida == default(DateTime))
            //{
            //    validationErrors.Add(new ValidationResult(L("Saida.DataSaidaEmptyError")));
            //}            

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida_Create, AppPermissions.Pages_Tenant_Estoque_Saida_Edit)]
        public async Task<IdInput> Save(SaidaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Saida_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, SaidaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Saida_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, SaidaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida_Create, AppPermissions.Pages_Tenant_Estoque_Saida_Edit)]
        public async Task<SaidaInput> SaveAndReturnEntity(SaidaInput input)
        {
            SaidaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Saida_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<SaidaInput, SaidaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Saida_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<SaidaInput, SaidaInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida_Delete)]
        public async Task Delete(IdInput input)
        {
            var itens = await _saidaItemAppService.GetItemsBySaida(input.Id);
            foreach (SaidaItemListDto item in itens)
            {
                await _saldoEstoqueService.RemoverSaldoPorSaida(input.Id);
                await _saidaItemAppService.Delete(new IdInput { Id = item.Id });
            }
            await base.DeleteEntity(_servicoService, input);
        }

        public async Task<PagedResultDto<SaidaListDto>> GetSaidaPaginado(GetSaidaInput input)
        {
            var condicoes = new List<WhereIfCondition<Saida>>
            {
                new WhereIfCondition<Saida>(
                        input.Id.HasValue,
                    a =>
                        a.Id == input.Id)
            };
            var result = await base.GetListPaged<SaidaListDto, GetSaidaInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<SaidaInput> GetById(IdInput input)
        {
            SaidaInput saida = await base.GetEntityById<SaidaInput, IdInput>(_servicoService, input);

            if (saida != null)
            {
                GetSaidaItemInput item = new GetSaidaItemInput();
                item.SaidaId = input.Id;
                saida.GridItens = await _saidaItemAppService.GetSaidaItemPaginado(item);
                saida.Items = await _saidaItemAppService.GetItemsBySaida(input.Id);
            }
            return saida;
        }

        public async Task<SaidaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<SaidaInput, IdInput>(_servicoService, input);
        }

        public async Task<PagedResultDto<SaidaItemListDto>> GetSaidaItemPaginado(GetSaidaItemInput input)
        {
            var result = await _saidaItemAppService.GetSaidaItemPaginado(input);
            return result;
        }
    }
}