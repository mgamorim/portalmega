﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class UnidadeMedidaAppService : EZControlAppServiceBase<UnidadeMedida>, IUnidadeMedidaAppService
    {
        private readonly IUnidadeMedidaService _servicoService;

        public UnidadeMedidaAppService(IRepository<UnidadeMedida> servicoRepository, IUnidadeMedidaService servicoService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
        }

        private void ValidateInput(UnidadeMedidaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Estoque.UnidadeMedida.EmptyNomeError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Estoque.UnidadeMedida.DuplicateNomeError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Edit)]
        public async Task<IdInput> Save(UnidadeMedidaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, UnidadeMedidaInput>(_servicoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, UnidadeMedidaInput>(_servicoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Create, AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Edit)]
        public async Task<UnidadeMedidaInput> SaveAndReturnEntity(UnidadeMedidaInput input)
        {
            UnidadeMedidaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<UnidadeMedidaInput, UnidadeMedidaInput>(_servicoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<UnidadeMedidaInput, UnidadeMedidaInput>(_servicoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        public async Task<PagedResultDto<UnidadeMedidaListDto>> GetUnidadeMedidaPaginado(GetUnidadeMedidaInput input)
        {
            var condicoes = new List<WhereIfCondition<UnidadeMedida>>
            {
                new WhereIfCondition<UnidadeMedida>(
                        !string.IsNullOrEmpty(input.Descricao),
                    a =>
                        a.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<UnidadeMedidaListDto, GetUnidadeMedidaInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<UnidadeMedidaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<UnidadeMedidaInput, IdInput>(_servicoService, input);
        }

        public async Task<UnidadeMedidaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<UnidadeMedidaInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Parametro_UnidadeMedida_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }
    }
}