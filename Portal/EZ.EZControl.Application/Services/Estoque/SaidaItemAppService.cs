﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.Estoque.Geral.SaidaItem;
using EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SaidaItemAppService : EZControlAppServiceBase<SaidaItem>, ISaidaItemAppService
    {
        private readonly ISaidaItemService _servicoService;
        private readonly ISaidaService _saidaService;
        private readonly IProdutoService _produtoService;
        private readonly IRepository<SaidaItem> _servicoRepository;
        private readonly ILocalArmazenamentoService _localArmazenamentoService;
        private readonly ISaldoEstoqueService _saldoEstoqueService;
        private readonly IEmpresaService _empresaService;

        public SaidaItemAppService(IRepository<SaidaItem> servicoRepository, 
            ISaidaItemService servicoService,
            ISaidaService saidaService,
            IProdutoService produtoService,
            ILocalArmazenamentoService localArmazenamentoService,
            ISaldoEstoqueService saldoEstoqueService,
            IEmpresaService empresaService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _saidaService = saidaService;
            _produtoService = produtoService;
            _servicoRepository = servicoRepository;
            _localArmazenamentoService = localArmazenamentoService;
            _saldoEstoqueService = saldoEstoqueService;
            _empresaService = empresaService;
        }

        private void ValidateInput(SaidaItemInput input, bool update)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.SaidaId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("Estoque.SaidaEmptyError")));
            }

            if (input.ProdutoId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("Estoque.SaidaItem.ProdutoEmptyError")));
            }

            if (!update)
            {
                if (CheckForDuplicateInstance(Repository, input.Id, x => x.ProdutoId == input.ProdutoId && x.SaidaId == input.SaidaId))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Estoque.SaidaItem.ProdutoDuplicateError"), input.ProdutoId)));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(SaidaItem saidaItem, SaidaItemInput input)
        {
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.SaidaItem.ProdutoNotFoundError"));
                }

                try
                {
                    saidaItem.AssociarProduto(produto, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.SaidaItem.ProdutoEmptyError"));
            }
            #endregion

            #region Saida
            if (input.SaidaId > 0)
            {
                Saida saida = null;

                try
                {
                    saida = await _saidaService.GetById(input.SaidaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.SaidaNotFoundError"));
                }

                try
                {
                    saidaItem.AssociarSaida(saida, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.SaidaEmptyError"));
            }
            #endregion

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    saidaItem.AssociarLocalArmazenamento(localArmazenamento, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task ProcessInputSaida(SaldoEstoque saldoEstoque, SaldoEstoqueSaidaInput input)
        {   
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                    saldoEstoque.AssociarProduto(produto, _saldoEstoqueService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    saldoEstoque.AssociarLocalArmazenamento(localArmazenamento, _saldoEstoqueService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region Saida e Empresa
            if (input.SaidaId > 0)
            {
                Saida saida = null;

                try
                {
                    saida = await _saidaService.GetById(input.SaidaId);
                    saldoEstoque.AssociarSaida(saida, _saldoEstoqueService);
                    if (saida.EmpresaId > 0)
                    {
                        Empresa empresa = null;

                        try
                        {
                            empresa = await _empresaService.GetById(saida.EmpresaId);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                        }

                        saldoEstoque.AssociarEmpresa(empresa, _saldoEstoqueService);
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task AtualizarSaldo(SaidaItemInput input, bool estorno)
        {
            SaldoEstoqueSaidaInput inputSaldo = new SaldoEstoqueSaidaInput { SaidaId = input.SaidaId, ProdutoId = input.ProdutoId, Quantidade = input.Quantidade, LocalArmazenamentoId = input.LocalArmazenamentoId };
            SaldoEstoque saldo = inputSaldo.MapTo<SaldoEstoque>();
            await ProcessInputSaida(saldo, inputSaldo);
            saldo.PrepararSaida(_saldoEstoqueService);

            if (estorno)
                await _saldoEstoqueService.EstornarSaida(saldo);
            else
                await _saldoEstoqueService.AtualizarSaida(saldo);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida_Edit)]
        public async Task<IdInput> Save(SaidaItemInput input)
        {
            IdInput result = null;

            if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Estoque_Saida_Edit))
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));

            if (input.Id == default(int))
            {                                                                  
                result = await base.CreateEntity<IdInput, SaidaItemInput>(_servicoService, input, () => ValidateInput(input, false), ProcessInput);
            }
            else
            {
                result = await base.UpdateEntity<IdInput, SaidaItemInput>(_servicoService, input, () => ValidateInput(input, true), ProcessInput);
            }

            await AtualizarSaldo(input, false);

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida_Edit)]
        public async Task Delete(IdInput input)
        {
            SaidaItem item = Repository.Get(input.Id);
            await base.DeleteEntity(_servicoService, input);

            await AtualizarSaldo(item.MapTo<SaidaItemInput>(), true);            
        }

        public async Task<PagedResultDto<SaidaItemListDto>> GetSaidaItemPaginado(GetSaidaItemInput input)
        {
            var condicoes = new List<WhereIfCondition<SaidaItem>>
            {
                new WhereIfCondition<SaidaItem>(true, x => x.Saida.Id == input.SaidaId)
            };
            var result = await base.GetListPaged<SaidaItemListDto, GetSaidaItemInput>(_servicoService, input, condicoes);

            foreach(SaidaItemListDto item in result.Items)
            {
                Produto prod = await _produtoService.GetById(item.ProdutoId);
                item.Produto = prod.MapTo<ProdutoListDto>();
                if (item.LocalArmazenamentoId.HasValue)
                {
                    LocalArmazenamento local = await _localArmazenamentoService.GetById(item.LocalArmazenamentoId.Value);
                    item.LocalArmazenamento = local.MapTo<LocalArmazenamentoListDto>();
                }
            }

            return result;
        }

        public async Task<IEnumerable<SaidaItemListDto>> GetItemsBySaida(int saidaId)
        {
            
            List<SaidaItem> lista = await _servicoRepository.GetAllListAsync(x => x.SaidaId == saidaId);

            return lista.MapTo<List<SaidaItemListDto>>();
        }
    }
}