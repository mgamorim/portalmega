﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada, AppPermissions.Pages_Tenant_Estoque_Saida, AppPermissions.Pages_Tenant_Vendas_Pedido, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SaldoEstoqueAppService : EZControlAppServiceBase<SaldoEstoque>, ISaldoEstoqueAppService
    {
        private readonly ISaldoEstoqueService _servicoService;
        private readonly IEmpresaService _empresaService;
        private readonly ILocalArmazenamentoService _localArmazenamentoService;
        private readonly IProdutoService _produtoService;
        private readonly IEntradaService _entradaService;
        private readonly ISaidaService _saidaService;
        private readonly IPedidoService _pedidoService;

        public SaldoEstoqueAppService(IRepository<SaldoEstoque> servicoRepository,
            ISaldoEstoqueService servicoService,
            IEmpresaService empresaService,
            ILocalArmazenamentoService localArmazenamentoService,
            IProdutoService produtoService,
            IEntradaService entradaService,
            ISaidaService saidaService,
            IPedidoService pedidoService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
            _empresaService = empresaService;
            _localArmazenamentoService = localArmazenamentoService;
            _produtoService = produtoService;
            _entradaService = entradaService;
            _saidaService = saidaService;
            _pedidoService = pedidoService;
        }

        private async Task ProcessInputSaida(SaldoEstoque saldoEstoque, SaldoEstoqueSaidaInput input)
        {
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                    saldoEstoque.AssociarProduto(produto, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    saldoEstoque.AssociarLocalArmazenamento(localArmazenamento, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region Saida
            if (input.SaidaId > 0)
            {
                Saida saida = null;

                try
                {
                    saida = await _saidaService.GetById(input.SaidaId);
                    saldoEstoque.AssociarSaida(saida, _servicoService);
                    /* Empresa */
                    Empresa empresa = null;

                    try
                    {
                        empresa = await _empresaService.GetById(saida.EmpresaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                    }

                    saldoEstoque.AssociarEmpresa(empresa, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task ProcessInputEntrada(SaldoEstoque saldoEstoque, SaldoEstoqueEntradaInput input)
        {
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                    saldoEstoque.AssociarProduto(produto, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    saldoEstoque.AssociarLocalArmazenamento(localArmazenamento, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region Entrada
            if (input.EntradaId > 0)
            {
                Entrada entrada = null;

                try
                {
                    entrada = await _entradaService.GetById(input.EntradaId);
                    saldoEstoque.AssociarEntrada(entrada, _servicoService);
                    /* Empresa */
                    Empresa empresa = null;

                    try
                    {
                        empresa = await _empresaService.GetById(entrada.EmpresaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                    }

                    saldoEstoque.AssociarEmpresa(empresa, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task ProcessInputPedido(SaldoEstoque saldoEstoque, SaldoEstoquePedidoInput input)
        {
            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                    saldoEstoque.AssociarProduto(produto, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region LocalArmazenamento
            if (input.LocalArmazenamentoId.HasValue)
            {
                LocalArmazenamento localArmazenamento = null;

                try
                {
                    localArmazenamento = await _localArmazenamentoService.GetById(input.LocalArmazenamentoId.Value);
                    saldoEstoque.AssociarLocalArmazenamento(localArmazenamento, _servicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region Pedido
            if (input.PedidoId > 0)
            {
                Pedido pedido = null;

                try
                {
                    pedido = await _pedidoService.GetById(input.PedidoId);
                    saldoEstoque.AssociarPedido(pedido, _servicoService);
                    Empresa empresa = null;

                    try
                    {
                        empresa = await _empresaService.GetById(pedido.EmpresaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                    }

                    saldoEstoque.AssociarEmpresa(empresa, _servicoService);

                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            
        }

        private void ValidateEntradaInput(SaldoEstoqueEntradaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ProdutoId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("SaldoEstoque.ValidationError.Produto")));
            }

            if (input.EntradaId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("SaldoEstoque.ValidationError.Entrada")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private void ValidateSaidaInput(SaldoEstoqueSaidaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ProdutoId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("SaldoEstoque.ValidationError.Produto")));
            }

            if (input.SaidaId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("SaldoEstoque.ValidationError.Entrada")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private void ValidatePedidoInput(SaldoEstoquePedidoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ProdutoId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("SaldoEstoque.ValidationError.Produto")));
            }

            if (input.PedidoId == default(int))
            {
                validationErrors.Add(new ValidationResult(L("SaldoEstoque.ValidationError.Pedido")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<SaldoEstoqueEntradaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<SaldoEstoqueEntradaInput, IdInput>(_servicoService, input); ;
        }

        public async Task<SaldoEstoqueEntradaInput> GetByExternalId(IdInput input)
        {
            if (input.Id == default(int)) return new SaldoEstoqueEntradaInput();
            return await base.GetEntityByExternalId<SaldoEstoqueEntradaInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Entrada_Create, AppPermissions.Pages_Tenant_Estoque_Entrada_Edit)]
        public async Task<IdInput> AtualizarSaldoEntrada(SaldoEstoqueEntradaInput input)
        {
            ValidateEntradaInput(input);
            SaldoEstoque saldo = input.MapTo<SaldoEstoque>();
            await ProcessInputEntrada(saldo, input);
            saldo.PrepararSaida(_servicoService);

            int id = await _servicoService.AtualizarEntrada(saldo);
            return new IdInput(id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Estoque_Saida_Create, AppPermissions.Pages_Tenant_Estoque_Saida_Edit)]
        public async Task<IdInput> AtualizarSaldoSaida(SaldoEstoqueSaidaInput input)
        {
            ValidateSaidaInput(input);
            SaldoEstoque saldo = input.MapTo<SaldoEstoque>();
            await ProcessInputSaida(saldo, input);
            saldo.PrepararSaida(_servicoService);

            int id = await _servicoService.AtualizarSaida(saldo);
            return new IdInput(id);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Vendas_Pedido_Create, AppPermissions.Pages_Tenant_Vendas_Pedido_Edit)]
        public async Task<IdInput> AtualizarSaldoPedido(SaldoEstoquePedidoInput input)
        {
            ValidatePedidoInput(input);
            SaldoEstoque saldo = input.MapTo<SaldoEstoque>();
            await ProcessInputPedido(saldo, input);
            saldo.PrepararPedido(_servicoService);

            int id = await _servicoService.AtualizarPedido(saldo);
            return new IdInput(id);

            throw new NotImplementedException();
        }

        public async Task<decimal> GetSaldoProduto(GetSaldoEstoqueInput input)
        {
            SaldoEstoque saldo = input.MapTo<SaldoEstoque>();
            return await _servicoService.GetSaldoProduto(saldo);
        }
    }
}