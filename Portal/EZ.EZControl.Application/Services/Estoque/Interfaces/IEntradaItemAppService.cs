﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.Entrada;
using EZ.EZControl.Dto.Estoque.Geral.EntradaItem;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface IEntradaItemAppService : IApplicationService
    {                                                          
        Task<PagedResultDto<EntradaItemListDto>> GetEntradaItemPaginado(GetEntradaItemInput input);
        Task<IdInput> Save(EntradaItemInput input);
        Task Delete(IdInput input);
        Task<IEnumerable<EntradaItemListDto>> GetItemsByEntrada(int entradaId);
    }
}