﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Dto.Estoque.Geral.Produto;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface IProdutoAppService : IApplicationService
    {
        Task<IdInput> Save(ProdutoInput input);
        Task<ProdutoInput> SaveAndReturnEntity(ProdutoInput input);
        Task<PagedResultDto<ProdutoListDto>> GetProdutoPaginado(GetProdutoInput input);
        Task<ProdutoInput> GetById(IdInput input);
        Task<ProdutoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}