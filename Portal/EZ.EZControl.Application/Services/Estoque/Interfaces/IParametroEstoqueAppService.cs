﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.Parametro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Geral.Interfaces
{
    public interface IParametroEstoqueAppService : IApplicationService
    {
        Task<IdInput> Save(ParametroEstoqueInput input);

        Task<ParametroEstoqueInput> Get();
    }
}