﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Dto.Estoque.Geral.Natureza;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface INaturezaAppService : IApplicationService
    {
        Task<IdInput> Save(NaturezaInput input);
        Task<NaturezaInput> SaveAndReturnEntity(NaturezaInput input);
        Task<PagedResultDto<NaturezaListDto>> GetNaturezaPaginado(GetNaturezaInput input);
        Task<NaturezaInput> GetById(IdInput input);
        Task<NaturezaInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}