﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.Saida;
using EZ.EZControl.Dto.Estoque.Geral.SaidaItem;  
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface ISaidaAppService : IApplicationService
    {
        Task<IdInput> Save(SaidaInput input);
        Task<SaidaInput> SaveAndReturnEntity(SaidaInput input);
        Task<PagedResultDto<SaidaListDto>> GetSaidaPaginado(GetSaidaInput input);
        Task<SaidaInput> GetById(IdInput input);
        Task<SaidaInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);                                                             
        Task<PagedResultDto<SaidaItemListDto>> GetSaidaItemPaginado(GetSaidaItemInput input);
    }
}