﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Dto.Estoque.Geral.Fracao;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface IFracaoAppService : IApplicationService
    {
        Task<IdInput> Save(FracaoInput input);
        Task<FracaoInput> SaveAndReturnEntity(FracaoInput input);
        Task<PagedResultDto<FracaoListDto>> GetFracaoPaginado(GetFracaoInput input);
        Task<FracaoInput> GetById(IdInput input);
        Task<FracaoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}