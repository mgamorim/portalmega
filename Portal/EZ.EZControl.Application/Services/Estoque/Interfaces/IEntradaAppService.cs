﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.Entrada;
using EZ.EZControl.Dto.Estoque.Geral.EntradaItem;  
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface IEntradaAppService : IApplicationService
    {
        Task<IdInput> Save(EntradaInput input);
        Task<EntradaInput> SaveAndReturnEntity(EntradaInput input);
        Task<PagedResultDto<EntradaListDto>> GetEntradaPaginado(GetEntradaInput input);
        Task<EntradaInput> GetById(IdInput input);
        Task<EntradaInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);                                                             
        Task<PagedResultDto<EntradaItemListDto>> GetEntradaItemPaginado(GetEntradaItemInput input);
    }
}