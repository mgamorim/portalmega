﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface ISaldoEstoqueAppService : IApplicationService
    {   
        Task<IdInput> AtualizarSaldoEntrada(SaldoEstoqueEntradaInput input);
        Task<IdInput> AtualizarSaldoSaida(SaldoEstoqueSaidaInput input);
        Task<IdInput> AtualizarSaldoPedido(SaldoEstoquePedidoInput input);        
        Task<decimal> GetSaldoProduto(GetSaldoEstoqueInput input);
        Task<SaldoEstoqueEntradaInput> GetByExternalId(IdInput input);
    }
}