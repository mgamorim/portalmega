﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface ILocalArmazenamentoAppService : IApplicationService
    {
        Task<IdInput> Save(LocalArmazenamentoInput input);
        Task<LocalArmazenamentoInput> SaveAndReturnEntity(LocalArmazenamentoInput input);
        Task<PagedResultDto<LocalArmazenamentoListDto>> GetLocalArmazenamentoPaginado(GetLocalArmazenamentoInput input);
        Task<LocalArmazenamentoInput> GetById(IdInput input);
        Task<LocalArmazenamentoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}