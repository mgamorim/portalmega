﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface IUnidadeMedidaAppService : IApplicationService
    {
        Task<IdInput> Save(UnidadeMedidaInput input);
        Task<UnidadeMedidaInput> SaveAndReturnEntity(UnidadeMedidaInput input);
        Task<PagedResultDto<UnidadeMedidaListDto>> GetUnidadeMedidaPaginado(GetUnidadeMedidaInput input);
        Task<UnidadeMedidaInput> GetById(IdInput input);
        Task<UnidadeMedidaInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}