﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.SaidaItem;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Estoque.Interfaces
{
    public interface ISaidaItemAppService : IApplicationService
    {                                                          
        Task<PagedResultDto<SaidaItemListDto>> GetSaidaItemPaginado(GetSaidaItemInput input);
        Task<IdInput> Save(SaidaItemInput input);
        Task Delete(IdInput input);
        Task<IEnumerable<SaidaItemListDto>> GetItemsBySaida(int saidaId);
    }
}