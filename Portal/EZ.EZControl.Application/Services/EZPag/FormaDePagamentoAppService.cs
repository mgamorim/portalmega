﻿using EZ.EZControl.Services.EZPag.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral.FormaDePagamento;
using Abp.Domain.Repositories;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Common.Filters;
using Abp.UI;
using Abp.Authorization;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using System.ComponentModel.DataAnnotations;
using EZ.EZControl.Domain.EZPag.Geral;
using Abp.Extensions;
using EZ.EZControl.Dto.EZPag.Geral;
using Abp.AutoMapper;
using EZ.EZControl.ADO.Repositorio.Interface;
using EZ.EZControl.ADO.Repositorio;
using System.Configuration;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZPag.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class FormaDePagamentoAppService : EZControlAppServiceBase<FormaDePagamento>, IFormaDePagamentoAppService
    {
        public FormaDePagamentoAppService(
            IRepository<FormaDePagamento> servicoRepository)
            : base(servicoRepository)
        {

        }
        private readonly IFormaDePagamentoService _servicoService;
        //private readonly IEmpresaService _empresaService;
        //private readonly ICorretoraService _corretoraservice;
        private IRepositorioFormasPagamento _repositorio = null;

        public FormaDePagamentoAppService(IRepository<FormaDePagamento, int> repository,
            IFormaDePagamentoService servicoService) : base(repository)
        {
            this._servicoService = servicoService;
            //this._corretoraservice = corretora;
            //this._empresaService = empresa;
            _repositorio = new RepositorioFormasPagamento(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
        }

        private async Task ProcessInput(FormaDePagamento formaDePagamento, FormaDePagamentoInput input)
        {

        }

        private void ValidateInput(FormaDePagamentoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("FormaDePagamento.DescricaoEmptyError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }
            else if (input.Descricao.Length > FormaDePagamento.DescricaoMaxLength)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("FormaDePagamento.DescricaoMaxLengthError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("FormaDePagamento.DescricaoDuplicateError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Create, AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Edit)]
        public async Task<IdInput> Save(FormaDePagamentoInput input)
        {
            try
            {
                IdInput result = null;

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, FormaDePagamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, FormaDePagamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Create, AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Edit)]
        public async Task<FormaDePagamentoInput> SaveAndReturnEntity(FormaDePagamentoInput input)
        {
            FormaDePagamentoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<FormaDePagamentoInput, FormaDePagamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<FormaDePagamentoInput, FormaDePagamentoInput>(_servicoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<FormaDePagamentoListDto>> GetPaginado(GetFormaDePagamentoInput input)
        {
            try
            {
                var condicoes = new List<WhereIfCondition<FormaDePagamento>>
                {
                new WhereIfCondition<FormaDePagamento>(
                        !string.IsNullOrEmpty(input.Descricao),
                    a =>
                        a.Descricao.Contains(input.Descricao))
                };

                var result = await base.GetListPaged<FormaDePagamentoListDto, GetFormaDePagamentoInput>(_servicoService, input, condicoes);

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public PagedResultDto<FormaDePagamentoListDto> GetIsFormaPagamento(Int32 idCorretora)
        {
           // var corretora =  _corretoraservice.GetById(idCorretora);
           // var empresa = _empresaService.GetEmpresaByPessoaJuridicaId(corretora.Result.PessoaJuridicaId);

            var dados = BuscaFormasPagamento(idCorretora);


            // var dados =  _servicoService.GetAllFormasDePagamentos(idCorretora);

            var lst = new List<FormaDePagamentoListDto>();

            foreach (var item in dados)
            {
                var obj = new FormaDePagamentoListDto();
                obj.IsActive = item.IsActive;
                obj.TipoDePagamento = item.TipoDePagamento;
                lst.Add(obj);
            }
            var listDtos = new PagedResultDto<FormaDePagamentoListDto>(0, lst);
            return listDtos;
        }

        private IQueryable<FormaDePagamento> BuscaFormasPagamento(Int32 idCorretora)
        {
            try
            {
                var resposta = _repositorio.BuscaFormasPagamento(idCorretora);

                return resposta;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






        public async Task<FormaDePagamentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<FormaDePagamentoInput, IdInput>(_servicoService, input);
        }

        public async Task<FormaDePagamentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<FormaDePagamentoInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZPag_FormaDePagamento_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }
    }
}
