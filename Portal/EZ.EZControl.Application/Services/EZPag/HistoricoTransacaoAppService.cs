﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using EZ.EZControl.Dto.EZPag.Geral;
using Abp.AutoMapper;

namespace EZ.EZControl.Services.EZPag
{
    public class HistoricoTransacaoAppService : EZControlAppServiceBase<HistoricoTransacao>, IHistoricoTransacaoAppService
    {
        private readonly IHistoricoTransacaoService _servicoService;
        public HistoricoTransacaoAppService(IRepository<HistoricoTransacao, int> repository, IHistoricoTransacaoService servicoService) : base(repository)
        {
            _servicoService = servicoService;
        }
        public async Task Save(HistoricoTransacaoInput input)
        {
            var entity = input.MapTo<HistoricoTransacao>();
            await _servicoService.CreateEntity(entity);
        }
    }
}
