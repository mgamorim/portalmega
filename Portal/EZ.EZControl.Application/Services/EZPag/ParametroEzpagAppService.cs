﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Dto.EZPag.Geral.Parametro;
using EZ.EZControl.Services.EZPag.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZPag_ParametroEzpag, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ParametroEzpagAppService : EZControlAppServiceBase<ParametroEzpag>, IParametroEzpagAppService
    {
        private readonly IParametroEzpagService _parametroEzpagService;

        public ParametroEzpagAppService(IRepository<ParametroEzpag> parametroRepository,
           IParametroEzpagService parametroEzpagService)
                : base(parametroRepository)
        {
            this._parametroEzpagService = parametroEzpagService;
        }


        private void ValidateInput(ParametroEzpagInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<ParametroEzpagInput> Get()
        {
            var condicoes = new List<WhereIfCondition<ParametroEzpag>>();
            condicoes.Add(new WhereIfCondition<ParametroEzpag>(true, x => x.Id > 0));
            var parametros = await base.GetList<ParametroEzpagInput>(_parametroEzpagService, condicoes, x => x.Id);

            return parametros.Items.FirstOrDefault();
        }

        public async Task<IdInput> Save(ParametroEzpagInput input)
        {
            var parametro = await Get();
            if (parametro != null)
                input.Id = parametro.Id;

            IdInput savedEntity;

            if (input.Id == default(int))
            {
                savedEntity = await base.CreateEntity<IdInput, ParametroEzpagInput>(_parametroEzpagService, input, () => ValidateInput(input), null);
            }
            else
            {
                savedEntity = await base.UpdateEntity<IdInput, ParametroEzpagInput>(_parametroEzpagService, input, () => ValidateInput(input), null);
            }

            return savedEntity;
        }
    }
}
