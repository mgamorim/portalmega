﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Common.Filters;
using Abp.Authorization;
using EZ.EZControl.Authorization;
using Abp.AutoMapper;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Services.EZPag
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZPag_Transacao, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]

    public class TransacaoAppService : EZControlAppServiceBase<Transacao>, ITransacaoAppService
    {
        private readonly ITransacaoService _servicoService;
        private readonly IDepositoTransferenciaBancariaService _depositoTransferenciaBancariaService;
        private readonly IPropostaDeContratacaoService _propostaService;

        public TransacaoAppService(IRepository<Transacao, int> repository, ITransacaoService servicoService, IDepositoTransferenciaBancariaService depositoTransferenciaBancariaService, IPropostaDeContratacaoService propostaService) : base(repository)
        {
            _servicoService = servicoService;
            _depositoTransferenciaBancariaService = depositoTransferenciaBancariaService;
            _propostaService = propostaService;
        }

        public async Task<PagedResultDto<TransacaoListDto>> GetPaginado(GetTransacaoInput input)
        {
            var condicoes = new List<WhereIfCondition<Transacao>>
            {
                new WhereIfCondition<Transacao>(
                        !string.IsNullOrEmpty(input.TransactionId),
                    a =>
                        a.TransactionId.Contains(input.TransactionId))
            };
            var result = await base.GetListPaged<TransacaoListDto, GetTransacaoInput>(_servicoService, input, condicoes);

            return result;
        }

        public Task<TransacaoInput> getTransacaoDepositoBancarioPorIdCorretora(IdInput input)
        {
            throw new NotImplementedException();
        }

        public async Task<TransacaoInput> GetTransacaoPorPedidoId(IdInput input)
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var result = await _servicoService.FirstOrDefault(x => x.PedidoId == input.Id);
                var retorno = result.MapTo<TransacaoInput>();
                return retorno;
            }
        }
    }
}
