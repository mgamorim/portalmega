﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Geral.Interfaces;

namespace EZ.EZControl.Services.EZPag
{
    public class ParametroPagSeguroAppService : EZControlAppServiceBase<ParametroPagSeguro>, IParametroPagSeguroAppService
    {
        public ParametroPagSeguroAppService(
           IRepository<ParametroPagSeguro> Repository)
            : base(Repository)
        {

        }

        private readonly IParametroPagSeguroService _parametroPagSeguroService;

        public ParametroPagSeguroAppService(IRepository<ParametroPagSeguro, int> repository, 
            IParametroPagSeguroService parametroPagSeguroService) : base(repository)
        {
            _parametroPagSeguroService = parametroPagSeguroService;
        }

        public async Task<ParametroPagSeguroInput> GetById(IdInput input)
        {
           return await base.GetEntityById<ParametroPagSeguroInput, IdInput>(_parametroPagSeguroService, input);
        }
     
    }
}
