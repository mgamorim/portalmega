﻿using System.Threading.Tasks;
using Abp.Application.Services;
using System.Collections.Generic;
using Uol.PagSeguro.Domain;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Domain.EZPag.Geral;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface ITransacaoPagSeguroAppService : IApplicationService
    {
        Task<SessionListDto> CriarSessao(IdInput input);
        Task<TransacaoInput> RealizaCheckout(PropostaDeContratacaoInput ezCheckout);
        Task Retorno(RetornoPagSeguroInput input);
    }
}