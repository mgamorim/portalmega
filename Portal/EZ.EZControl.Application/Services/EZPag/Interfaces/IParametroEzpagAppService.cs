﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral.Parametro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IParametroEzpagAppService: IApplicationService
    {
        Task<IdInput> Save(ParametroEzpagInput input);

        Task<ParametroEzpagInput> Get();
    }
}
