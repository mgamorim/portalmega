﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.EZPag.Geral.FormaDePagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Geral.Interfaces
{
    public interface IParametroPagSeguroAppService : IApplicationService
    {
        Task<ParametroPagSeguroInput> GetById(IdInput input);
    }
}
