﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface ITransacaoAppService : IApplicationService
    {
        Task<PagedResultDto<TransacaoListDto>> GetPaginado(GetTransacaoInput input);

        Task<TransacaoInput> GetTransacaoPorPedidoId(IdInput input);
    }
}
