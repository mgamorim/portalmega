﻿using Abp.Application.Services;
using EZ.EZControl.Dto.EZPag.Geral;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Interfaces
{
    public interface IHistoricoTransacaoAppService : IApplicationService
    {
        Task Save(HistoricoTransacaoInput input);
    }
}
