﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.EZPag.Geral.FormaDePagamento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZPag.Geral.Interfaces
{
    public interface IFormaDePagamentoAppService : IApplicationService
    {
        Task<IdInput> Save(FormaDePagamentoInput input);
        Task<FormaDePagamentoInput> SaveAndReturnEntity(FormaDePagamentoInput input);
        Task<PagedResultDto<FormaDePagamentoListDto>> GetPaginado(GetFormaDePagamentoInput input);
        PagedResultDto<FormaDePagamentoListDto> GetIsFormaPagamento(Int32 id);
        Task<FormaDePagamentoInput> GetById(IdInput input);
        Task<FormaDePagamentoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
       
    }
}
