﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.EZPag.Geral.Serialization;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Uol.PagSeguro.Constants;
using Uol.PagSeguro.Domain;
using Uol.PagSeguro.Domain.Direct;
using Uol.PagSeguro.Exception;
using Uol.PagSeguro.Resources;
using Uol.PagSeguro.Service;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.EZLiv.MapToEntity;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;

namespace EZ.EZControl.Services.EZPag
{
    public class TransacaoPagSeguroAppService : EZControlAppServiceBase<Transacao>, ITransacaoPagSeguroAppService
    {
        private readonly ClienteService _clienteService;
        private bool isSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["PagSeguroSandbox"].ToString());
        private readonly ITransacaoPagSeguroService _transacaoPagSeguroService;
        private readonly IPedidoService _pedidoService;
        private readonly IPropostaDeContratacaoService _propostaService;
        private readonly IAdministradoraService _administradoraService;
        private readonly IParametroPagSeguroService _parametroPagSeguroService;
        private readonly IEmpresaService _empresaService;
        private readonly IProdutoDePlanoDeSaudeService _produtoService;
        private readonly ITransacaoService _transacaoService;
        private readonly IHistoricoTransacaoService _historicoTransacaoService;
        private readonly ICorretoraService _corretoraService;
        private readonly IProponenteTitularService _titularService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly UserManager _userManager;

        public TransacaoPagSeguroAppService(
            ClienteService clienteService,
            ITransacaoPagSeguroService transacaoPagSeguroService,
            IPedidoService pedidoService,
            IPropostaDeContratacaoService propostaService,
            IAdministradoraService administradoraService,
            IProponenteTitularService titularService,
            IParametroPagSeguroService parametroPagSeguroService,
            IEmpresaService empresaService,
            IProdutoDePlanoDeSaudeService produtoService,
            UserManager userManager,
            ITransacaoService transacaoService,
            IHistoricoTransacaoService historicoTransacaoService,
            IPessoaFisicaService pessoaFisicaService,
            IRepository<Transacao, int> transacaoRepository,
            ICorretoraService corretoraService) : base(transacaoRepository)
        {
            _clienteService = clienteService;
            _transacaoPagSeguroService = transacaoPagSeguroService;
            _pedidoService = pedidoService;
            _propostaService = propostaService;
            _administradoraService = administradoraService;
            _parametroPagSeguroService = parametroPagSeguroService;
            _empresaService = empresaService;
            _userManager = userManager;
            _titularService = titularService;
            _produtoService = produtoService;
            _pessoaFisicaService = pessoaFisicaService;
            _transacaoService = transacaoService;
            _historicoTransacaoService = historicoTransacaoService;
            _corretoraService = corretoraService;
        }
        public async Task<SessionListDto> CriarSessao(IdInput input)
        {
            EnvironmentConfiguration.ChangeEnvironment(isSandbox);

            try
            {
                AccountCredentials credentials = await GetCredentialsByProposta(input.Id);
                Session result = SessionService.CreateSession(credentials);
                return new SessionListDto(result.id);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<TransacaoInput> RealizaCheckout2(EZPagCheckout input)
        {
            EnvironmentConfiguration.ChangeEnvironment(isSandbox);

            try
            {
                AccountCredentials credentials = await GetCredentialsByPedido(input.Pedido.Id);
                input.EmailVendedor = credentials.Email;
                var Checkout = await _transacaoPagSeguroService.MapFromEzInput(input, _clienteService);

                Transaction result = TransactionService.CreateCheckout(credentials, Checkout);
                var retorno = await Save(input, result);
                return retorno;
            }
            catch (PagSeguroServiceException exception)
            {
                var validationErrors = new List<ValidationResult>();
                Console.WriteLine(exception.Message + "\n");
                foreach (ServiceError element in exception.Errors)
                {
                    var errorMessage = element.Message;
                    validationErrors.Add(new ValidationResult(errorMessage));
                }
                throw new AbpValidationException("Erros", validationErrors);
            }
            catch (WebException ex)
            {
                if (ex.Status != WebExceptionStatus.Timeout)
                {
                    if (ex.Response != null)
                    {
                        var response = (WebResponse)ex.Response;
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        string xmlString = sr.ReadToEnd();
                        var serializer = new XmlSerializer(typeof(errors));
                        errors result = new errors();
                        using (TextReader reader = new StringReader(xmlString))
                        {
                            result = (errors)serializer.Deserialize(reader);
                        }
                        var errorMessage = _transacaoPagSeguroService.TraduzErroPagSeguro(Convert.ToInt32(result.error.code));
                        throw new UserFriendlyException(errorMessage);
                    }
                    else
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
                else
                {
                    var transacao = await _transacaoService.GetAll().Where(x => x.PedidoId == input.Pedido.Id && !string.IsNullOrEmpty(x.LinkParaPagamento)).FirstOrDefaultAsync();
                    if (transacao != null)
                    {
                        return transacao.MapTo<TransacaoInput>();
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Transacao.TransacaoNotCreatedError"));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }


        public async Task<TransacaoInput> RealizaCheckout(PropostaDeContratacaoInput input)
        {
           // EnvironmentConfiguration.ChangeEnvironment(isSandbox);

            try
            {
                TransacaoInput transacao = new TransacaoInput();
                DadosIUGU dados = new DadosIUGU();


                var corretora = await _corretoraService.GetById(Convert.ToInt32(input.CorretoraId));
                var pedido = await _pedidoService.GetById(Convert.ToInt32(input.PedidoId));

                dados.idUser = input.Titular.Id.ToString();



                dados.Nome = input.Titular.PessoaFisica.Nome;
                dados.Email = input.Titular.PessoaFisica.EmailPrincipal.Endereco.ToString();
                dados.Cpf = input.Titular.PessoaFisica.Cpf;
                dados.Telefone = input.Titular.PessoaFisica.TelefoneCelular.Numero;
                dados.DDD = input.Titular.PessoaFisica.TelefoneCelular.DDD;

                dados.Cep = input.Titular.Pessoa.EnderecoPrincipal.CEP;
                dados.Endereco = input.Titular.Pessoa.EnderecoPrincipal.Logradouro;
                dados.Complemento = input.Titular.Pessoa.EnderecoPrincipal.Complemento;
                dados.Numero = input.Titular.Pessoa.EnderecoPrincipal.Numero;
                dados.Bairro = input.Titular.Pessoa.EnderecoPrincipal.Bairro;
                dados.Estado = input.Titular.Pessoa.EnderecoPrincipal.Cidade.Estado.Sigla;
                dados.Cidade = input.Titular.Pessoa.EnderecoPrincipal.Cidade.Nome;
               


                dados.TipoPagamento = pedido.FormaDePagamento.ToString();
                dados.TipoCliente = "Beneficiario";
                dados.Empresa = corretora.Nome;
                dados.IdProduto = input.ProdutoId.ToString();
                dados.IdProposta = input.Id.ToString();
                dados.NomeProduto = pedido.ItensDePedidoReadOnly.First().Produto.Nome;
                dados.Valor = pedido.ItensDePedidoReadOnly.First().Valor.ToString();
                dados.ValorFicha = input.Chancela != null ? input.Chancela.TaxaDeAdesao.ToString() + "00" : null;


                var json = JsonConvert.SerializeObject(dados);


                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(ConfigurationManager.AppSettings["UrlApiIUGU"].ToString(), new StringContent(json, Encoding.UTF8, "application/json"));
                    string resultContent = await response.Content.ReadAsStringAsync();
                    transacao.LinkParaPagamento = resultContent;
                }
               

                var retorno = await SaveIUGU(input, "XXXXXXX", transacao.LinkParaPagamento);

                //using (var stream = request.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}

                //var response = (HttpWebResponse)request.GetResponse();

                //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();


                var teste = 1;



                /*
                      {
                           "iduser":"14",
                           "Nome":"Mauricio Goes Amorim",
                           "Email":"Mauricioamorim22@gmail.com",
                           "Cpf":"11722219793",
                           "Cep":"28970000",
                           "Endereco":"Avenida Salvador Alende",
                           "Complemento":"Apto 406",
                           "Numero":"109",
                           "Bairro":"Recreio dos Bandeirantes",
                           "Estado":"RJ",
                           "Cidade":"Rio de Janeiro",
                           "Telefone":"998016042",
                           "DDD":"021",
                           "TipoPagamento":"Boleto",
                           "TipoCliente":"Beneficiario",
                           "Empresa":"Evida",
                           "IdProduto":"1",
                           "IdProposta":"147",
                           "Nomeproduto":"Caberj Quarto Coletivo",
                           "Valor":"1200"
                       }
               */












                //AccountCredentials credentials = await GetCredentialsByPedido(input.Pedido.Id);

                //input.EmailVendedor = credentials.Email;

                // var Checkout = await _transacaoPagSeguroService.MapFromEzInput(input, _clienteService);

                //Transaction result = TransactionService.CreateCheckout(credentials, Checkout);
                //var retorno = await Save(input, result);






                return transacao;
            }
            catch (PagSeguroServiceException exception)
            {
                var validationErrors = new List<ValidationResult>();
                Console.WriteLine(exception.Message + "\n");
                foreach (ServiceError element in exception.Errors)
                {
                    var errorMessage = element.Message;
                    validationErrors.Add(new ValidationResult(errorMessage));
                }
                throw new AbpValidationException("Erros", validationErrors);
            }
            catch (WebException ex)
            {
                if (ex.Status != WebExceptionStatus.Timeout)
                {
                    if (ex.Response != null)
                    {
                        var response = (WebResponse)ex.Response;
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        string xmlString = sr.ReadToEnd();
                        var serializer = new XmlSerializer(typeof(errors));
                        errors result = new errors();
                        using (TextReader reader = new StringReader(xmlString))
                        {
                            result = (errors)serializer.Deserialize(reader);
                        }
                        var errorMessage = _transacaoPagSeguroService.TraduzErroPagSeguro(Convert.ToInt32(result.error.code));
                        throw new UserFriendlyException(errorMessage);
                    }
                    else
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }
                else
                {
                    var transacao = await _transacaoService.GetAll().Where(x => x.PedidoId == input.Pedido.Id && !string.IsNullOrEmpty(x.LinkParaPagamento)).FirstOrDefaultAsync();
                    if (transacao != null)
                    {
                        return transacao.MapTo<TransacaoInput>();
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Transacao.TransacaoNotCreatedError"));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }



        private async Task<AccountCredentials> GetCredentialsByPedido(int pedidoId)
        {
            CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter);
            var proposta = await _propostaService.GetAll().Where(x => x.PedidoId == pedidoId).FirstOrDefaultAsync();

            var corretora = await _corretoraService.GetAll().Where(x => x.Id == proposta.CorretoraId).FirstOrDefaultAsync();
            var parametroPagSeguro = await _parametroPagSeguroService.GetAll().Where(x => x.PessoaJuridicaId == corretora.PessoaJuridicaId).FirstOrDefaultAsync();

            AccountCredentials credentials = null;

            if (parametroPagSeguro != null)
            {
                credentials = new AccountCredentials(parametroPagSeguro.Email, parametroPagSeguro.Token);
                return credentials;
            }

            var administradora = await _administradoraService.GetAll().Where(x => x.Id == proposta.Produto.AdministradoraId).FirstOrDefaultAsync();
            parametroPagSeguro = await _parametroPagSeguroService.GetAll().Where(x => x.PessoaJuridicaId == administradora.PessoaJuridicaId).FirstOrDefaultAsync();

            if (parametroPagSeguro != null)
            {
                credentials = new AccountCredentials(parametroPagSeguro.Email, parametroPagSeguro.Token);
                return credentials;
            }

            throw new UserFriendlyException("Não foi possível encontrar as configurações de pagamento.");
        }
        private async Task<AccountCredentials> GetCredentialsByProposta(int PropostaId)
        {
            CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter);
            var proposta = await _propostaService.GetById(PropostaId);
            var pessoaJuridicaId = 0;
            var parametroPagSeguro = new ParametroPagSeguro();
            if (proposta.CorretoraId.HasValue && proposta.CorretoraId > 0)
            {
                var corretora = await _corretoraService.GetById(proposta.CorretoraId.Value);
                pessoaJuridicaId = proposta.Corretora.PessoaJuridicaId;
                parametroPagSeguro = await _parametroPagSeguroService.GetAll().Where(x => x.PessoaJuridicaId == pessoaJuridicaId).FirstOrDefaultAsync();
                if (parametroPagSeguro != null)
                {
                    AccountCredentials credentials = new AccountCredentials(parametroPagSeguro.Email, parametroPagSeguro.Token);
                    return credentials;
                }
            }

            var produto = await _produtoService.GetById(proposta.Produto.Id);
            if (produto.Administradora != null)
            {
                pessoaJuridicaId = produto.Administradora.PessoaJuridicaId;
                parametroPagSeguro = await _parametroPagSeguroService.GetAll().Where(x => x.PessoaJuridicaId == pessoaJuridicaId).FirstOrDefaultAsync();
                if (parametroPagSeguro != null)
                {
                    AccountCredentials credentials = new AccountCredentials(parametroPagSeguro.Email, parametroPagSeguro.Token);
                    return credentials;
                }
            }

            throw new UserFriendlyException("Não foi possível encontrar as configurações de pagamento.");
        }
        private async Task<TransacaoInput> Save(EZPagCheckout input, Transaction transacaoPagSeguro)
        {
            try
            {
                var pedido = await _pedidoService.GetById(input.Pedido.Id);
                Transacao transaction = new Transacao()
                {
                    Data = DateTime.Now,
                    Pedido = pedido,
                    Referencia = transacaoPagSeguro.Reference,
                    TransactionId = transacaoPagSeguro.Code,
                    LinkParaPagamento = transacaoPagSeguro.PaymentLink
                };
                var transacao = await _transacaoPagSeguroService.CreateOrUpdateAndReturnSavedEntity(transaction);
                return transacao.MapTo<TransacaoInput>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private async Task<TransacaoInput> SaveIUGU(PropostaDeContratacaoInput input, string CodTransacao, string Link)
        {
            try
            {
                var pedido = await _pedidoService.GetById(Convert.ToInt32(input.PedidoId));
                Transacao transaction = new Transacao()
                {
                    Data = DateTime.Now,
                    Pedido = pedido,
                    Referencia = "IUGU",
                    TransactionId = CodTransacao,
                    LinkParaPagamento = Link
                };
                var transacao = await _transacaoPagSeguroService.CreateOrUpdateAndReturnSavedEntity(transaction);
                return transacao.MapTo<TransacaoInput>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task Retorno(RetornoPagSeguroInput input)
        {
            if (!string.IsNullOrEmpty(input.notificationType) && input.notificationType == "transaction" && !string.IsNullOrEmpty(input.notificationCode))
            {
                using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
                {
                    CurrentUnitOfWork.SetTenantId(1);
                    var transacoesPendentes = await _transacaoService.GetAll().Where(x => x.StatusDoPagamento != Domain.EZPag.Enums.StatusDoPagamentoEnum.Pago).ToListAsync();
                    foreach (var transacao in transacoesPendentes)
                    {
                        var proposta = await _propostaService.GetAll().Where(x => x.PedidoId == transacao.PedidoId).FirstOrDefaultAsync();
                        if (proposta != null)
                        {
                            //Preciso carregar todas as entidades manualmente
                            //Não sei se por ter desativado o filtro de empresa, afetou de alguma forma o lazyload
                            var produto = await _produtoService.GetById(proposta.Produto.Id);
                            var administradora = await _administradoraService.GetAll().Where(x => x.Id == proposta.Produto.AdministradoraId).FirstOrDefaultAsync();
                            var parametroPagSeguro = await _parametroPagSeguroService.GetAll().Where(x => x.PessoaJuridicaId == administradora.PessoaJuridicaId).FirstOrDefaultAsync();
                            AccountCredentials credentials = new AccountCredentials(parametroPagSeguro.Email, parametroPagSeguro.Token);
                            var transaction = TransactionSearchService.SearchByCode(credentials, transacao.TransactionId);
                            //sempre será atualizada a transação realizada na proposta
                            transacao.StatusDoPagamento = _transacaoService.TraduzirStatusPagSeguro(transaction.TransactionStatus);
                            await _transacaoPagSeguroService.UpdateEntity(transacao);
                            var historicoTransacao = new HistoricoTransacao()
                            {
                                Data = DateTime.Now,
                                Status = _transacaoService.TraduzirStatusPagSeguro(transaction.TransactionStatus),
                                TransacaoId = transacao.Id,
                                EmpresaId = transacao.EmpresaId,
                                TenantId = 1
                            };
                            //sempre será realizada a gravação do histórico da transação
                            await _historicoTransacaoService.CreateEntity(historicoTransacao);
                            //caso o status venha como PAGO, atualiza também o status da PROPOSTA
                            if ((int)transaction.TransactionStatus == TransactionStatus.Paid)
                            {
                                proposta = await _propostaService
                                                    .GetAll()
                                                    .Where(x => x.PedidoId == transacao.PedidoId)
                                                    .FirstOrDefaultAsync();
                                if (proposta != null)
                                {
                                    proposta.StatusDaProposta = StatusDaPropostaEnum.Concluido;
                                    // TODO Atualizar o snapshot
                                    await _propostaService.UpdateEntity(proposta);
                                    _propostaService.SendEmailPagamentoConcluidoBeneficiario(proposta);
                                    _propostaService.SendEmailPagamentoConcluidoAdmCorretora(proposta);
                                    await _propostaService.SendNotificationPagamentoConcluido(proposta);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}