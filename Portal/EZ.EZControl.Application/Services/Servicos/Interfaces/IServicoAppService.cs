﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;
using EZ.EZControl.Dto.Servicos.Geral.Servicos;

namespace EZ.EZControl.Services.Servicos.Interfaces
{
    public interface IServicoAppService : IApplicationService
    {
        Task<IdInput> Save(ServicoInput input);
        Task<ServicoInput> SaveAndReturnEntity(ServicoInput input);
        Task<PagedResultDto<ServicoListDto>> GetServicosPaginado(GetServicoInput input);
        Task<ServicoInput> GetById(IdInput input);
        Task<ServicoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}