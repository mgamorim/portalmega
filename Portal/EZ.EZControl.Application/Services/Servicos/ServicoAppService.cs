﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Dto.Servicos.Geral.Servicos;
using EZ.EZControl.Services.Servicos.Geral.Interfaces;
using EZ.EZControl.Services.Servicos.Interfaces;

namespace EZ.EZControl.Services.Servicos
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Servicos_Servico, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ServicoAppService : EZControlAppServiceBase<Servico>, IServicoAppService
    {
        private readonly IServicoService _servicoService;

        public ServicoAppService(IRepository<Servico> servicoRepository, IServicoService servicoService)
            : base(servicoRepository)
        {
            _servicoService = servicoService;
        }

        private void ValidateInput(ServicoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Servico.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Servico.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Servicos_Servico_Create, AppPermissions.Pages_Tenant_Servicos_Servico_Edit)]
        public async Task<IdInput> Save(ServicoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Servicos_Servico_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ServicoInput>(_servicoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Servicos_Servico_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ServicoInput>(_servicoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Servicos_Servico_Create, AppPermissions.Pages_Tenant_Servicos_Servico_Edit)]
        public async Task<ServicoInput> SaveAndReturnEntity(ServicoInput input)
        {
            ServicoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Servicos_Servico_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ServicoInput, ServicoInput>(_servicoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Servicos_Servico_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ServicoInput, ServicoInput>(_servicoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        public async Task<PagedResultDto<ServicoListDto>> GetServicosPaginado(GetServicoInput input)
        {
            var condicoes = new List<WhereIfCondition<Servico>>
            {
                new WhereIfCondition<Servico>(
                        !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<ServicoListDto, GetServicoInput>(_servicoService, input, condicoes);

            return result;
        }

        public async Task<ServicoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ServicoInput, IdInput>(_servicoService, input);
        }

        public async Task<ServicoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ServicoInput, IdInput>(_servicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Servicos_Servico_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_servicoService, input);
        }
    }
}