﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Disponibilidade;
using EZ.EZControl.Dto.Agenda.Regra;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using Abp.Extensions;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda;

namespace EZ.EZControl.Services.Agenda.Regra
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RegraBaseAppService : EZControlAppServiceBase<RegraBase>, IRegraBaseAppService
    {
        private readonly IRegraBaseService _regraBaseService;
        public RegraBaseAppService(IRepository<RegraBase> repository,
                                         IRegraBaseService regraBaseService)
            : base(repository)
        {
            this._regraBaseService = regraBaseService;
        }

        private void ValidateInput(RegraBaseInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (!Enum.IsDefined(typeof(SistemaEnum), input.Sistema))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Agenda.Parametros.Regras.EmptySistemaError"), input.Sistema), new[] { "sistema".ToCamelCase() }));
            }
            if (!Enum.IsDefined(typeof(TipoDeEventoEnum), input.TipoDeEvento))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Agenda.Parametros.Regras.EmptyTipoDeEventoError"), input.Sistema), new[] { "tipoDeEvento".ToCamelCase() }));
            }
            if (input.DiasExibidosDesdeHojeAteAgendamento <= 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Agenda.Parametros.Regras.EmptyDiasExibidosDesdeHojeAteAgendamentoError"), input.DiasExibidosDesdeHojeAteAgendamento), new[] { "diasExibidosDesdeHojeAteAgendamento".ToCamelCase() }));
            }
            if (input.VagasDisponibilizadasPorDia <= 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Agenda.Parametros.Regras.EmptyVagasDisponibilizadasPorDiaError"), input.VagasDisponibilizadasPorDia), new[] { "vagasDisponibilizadasPorDia".ToCamelCase() }));
            }
            if (input.UnidadeDeTempo <= 0 )
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Agenda.Parametros.Regras.EmptyUnidadeDeTempoError"), input.UnidadeDeTempo), new[] { "unidadeDeTempo".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Sistema == input.Sistema && x.TipoDeEvento == input.TipoDeEvento))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Agenda.Parametros.Regras.DuplicateUnicidadeError"), input.Sistema), new[] { "sistema".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit)]
        public async Task<IdInput> Save(RegraBaseInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, RegraBaseInput>(_regraBaseService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, RegraBaseInput>(_regraBaseService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit)]
        public async Task<RegraBaseInput> SaveAndReturnEntity(RegraBaseInput input)
        {
            RegraBaseInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<RegraBaseInput, RegraBaseInput>(_regraBaseService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<RegraBaseInput, RegraBaseInput>(_regraBaseService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<RegraBaseListDto>> GetPaginado(GetRegraBaseInput input)
        {
            IList<WhereIfCondition<RegraBase>> condicoes = new List<WhereIfCondition<RegraBase>>();
            condicoes.Add(new WhereIfCondition<RegraBase>(!input.Sistema.IsNullOrEmpty(), x => x.Sistema.ToString().Contains(input.Sistema)));
            var result = await base.GetListPaged<RegraBaseListDto, GetRegraBaseInput>(_regraBaseService, input, condicoes);
            return result;
        }

        public async Task<RegraBaseInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RegraBaseInput, IdInput>(_regraBaseService, input);
        }

        public async Task<RegraBaseInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RegraBaseInput, IdInput>(_regraBaseService, input);
        }

        public async Task<RegraBaseTipoDeEventoListDto> GetTiposDeEventosBySistema(GetTiposDeEventosBySistemaDto dto)
        {
            var regraBaseTipoDeEventoListDto = new RegraBaseTipoDeEventoListDto()
            {
                TiposDeEventos = _regraBaseService.GetTiposDeEventosBySistema(dto.Sistema)
            };

            return await Task.FromResult(regraBaseTipoDeEventoListDto);
        }

        public async Task<RegraBaseSistemaListDto> GetSistemasCriamEventos()
        {
            var regraBaseTipoDeEventoListDto = new RegraBaseSistemaListDto()
            {
                Sistemas = _regraBaseService.GetSistemasCriamEventos()
            };

            return await Task.FromResult(regraBaseTipoDeEventoListDto);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_regraBaseService, input);
        }
    }
}