﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Disponibilidade;
using EZ.EZControl.Dto.Agenda.Regra;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;

namespace EZ.EZControl.Services.Agenda.Regra
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RegraTarefaAppService : EZControlAppServiceBase<RegraTarefa>, IRegraTarefaAppService
    {
        private readonly IRegraTarefaService _regraTarefaService;
        public RegraTarefaAppService(IRepository<RegraTarefa> repository,
                                         IRegraTarefaService regraTarefaService)
            : base(repository)
        {
            this._regraTarefaService = regraTarefaService;
        }

        private void ValidateInput(RegraTarefaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit)]
        public async Task<IdInput> Save(RegraTarefaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, RegraTarefaInput>(_regraTarefaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, RegraTarefaInput>(_regraTarefaService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit)]
        public async Task<RegraTarefaInput> SaveAndReturnEntity(RegraTarefaInput input)
        {
            RegraTarefaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<RegraTarefaInput, RegraTarefaInput>(_regraTarefaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<RegraTarefaInput, RegraTarefaInput>(_regraTarefaService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<RegraTarefaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RegraTarefaInput, IdInput>(_regraTarefaService, input);
        }

        public async Task<RegraTarefaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RegraTarefaInput, IdInput>(_regraTarefaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_regraTarefaService, input);
        }
    }
}