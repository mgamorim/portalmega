﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Disponibilidade;
using EZ.EZControl.Dto.Agenda.Regra;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;

namespace EZ.EZControl.Services.Agenda.Regra
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RegraReuniaoAppService : EZControlAppServiceBase<RegraReuniao>, IRegraReuniaoAppService
    {
        private readonly IRegraReuniaoService _regraReuniaoService;
        public RegraReuniaoAppService(IRepository<RegraReuniao> repository,
                                         IRegraReuniaoService regraReuniaoService)
            : base(repository)
        {
            this._regraReuniaoService = regraReuniaoService;
        }

        private void ValidateInput(RegraReuniaoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit)]
        public async Task<IdInput> Save(RegraReuniaoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, RegraReuniaoInput>(_regraReuniaoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, RegraReuniaoInput>(_regraReuniaoService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Create, AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Edit)]
        public async Task<RegraReuniaoInput> SaveAndReturnEntity(RegraReuniaoInput input)
        {
            RegraReuniaoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<RegraReuniaoInput, RegraReuniaoInput>(_regraReuniaoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<RegraReuniaoInput, RegraReuniaoInput>(_regraReuniaoService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<RegraReuniaoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RegraReuniaoInput, IdInput>(_regraReuniaoService, input);
        }

        public async Task<RegraReuniaoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RegraReuniaoInput, IdInput>(_regraReuniaoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_regraReuniaoService, input);
        }
    }
}