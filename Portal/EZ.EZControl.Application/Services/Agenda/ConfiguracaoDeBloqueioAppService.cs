﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Bloqueio;
using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ConfiguracaoDeBloqueioAppService : EZControlAppServiceBase<ConfiguracaoDeBloqueio>,
        IConfiguracaoDeBloqueioAppService
    {
        private readonly IConfiguracaoDeBloqueioService _configuracaoDeBloqueioService;
        private readonly IBloqueioService _bloqueioService;
        private readonly IRegraBaseService _regraService;
        private readonly IBloqueioAppService _bloqueioAppService;
        public ConfiguracaoDeBloqueioAppService(IRepository<ConfiguracaoDeBloqueio> repository,
            IConfiguracaoDeBloqueioService configuracaoDeBloqueioService,
            IBloqueioService bloqueioService,
            IRegraBaseService regraService,
            IBloqueioAppService bloqueioAppService)
            : base(repository)
        {
            _configuracaoDeBloqueioService = configuracaoDeBloqueioService;
            _regraService = regraService;
            _bloqueioService = bloqueioService;
            _bloqueioAppService = bloqueioAppService;
        }

        private async Task ProcessInput(ConfiguracaoDeBloqueio entity, ConfiguracaoDeBloqueioInput input)
        {
            entity.HorarioInicio = TimeSpan.Parse(input.HorarioInicio);
            entity.HorarioFim = TimeSpan.Parse(input.HorarioFim);

            var regra = _regraService.GetAll().Where(x => x.Sistema == input.Sistema && x.TipoDeEvento == input.TipoDeEvento).FirstOrDefault();
            if (regra == null)
            {
                throw new UserFriendlyException(L("ConfiguracaoDeBloqueio.RegraNotFoundError", input.Sistema, input.TipoDeEvento));
            }

            if (entity.Bloqueios != null && entity.Bloqueios.Any())
            {
                var count = entity.Bloqueios.Count - 1;
                while(entity.Bloqueios.Count > 0)
                {
                    var bloqueio = entity.Bloqueios.ElementAt(count);
                    await _bloqueioAppService.Delete(new IdInput(bloqueio.Id));
                    entity.Bloqueios.Remove(bloqueio);
                    count = count - 1;
                }
            }

            var dataControle = input.InicioBloqueio;
            while (dataControle <= input.FimBloqueio)
            {
                var horarioControle = TimeSpan.Parse(input.HorarioInicio);
                while (horarioControle <= TimeSpan.Parse(input.HorarioFim))
                {
                    var bloqueio = new Bloqueio()
                    {
                        Data = dataControle,
                        Horario = horarioControle,
                        IsActive = true
                    };
                    entity.Bloqueios.Add(bloqueio);
                    horarioControle = horarioControle.Add(TimeSpan.FromMinutes(regra.UnidadeDeTempo));
                }
                dataControle = dataControle.AddDays(1);
            }
        }

        private void ValidateInput(ConfiguracaoDeBloqueioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.InicioBloqueio == null)
            {
                validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Bloqueio.RequiredInicioBloqueioError"), input.InicioBloqueio),
                            new[] { "inicioBloqueio".ToCamelCase() }));
            }

            if (input.FimBloqueio == null)
            {
                validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Bloqueio.RequiredFimBloqueioError"), input.FimBloqueio),
                            new[] { "fimBloqueio".ToCamelCase() }));
            }

            if (input.InicioBloqueio != null && input.FimBloqueio != null)
            {
                if (input.FimBloqueio < input.InicioBloqueio)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Bloqueio.PeriodoInvalidoError"), input.FimBloqueio),
                            new[] { "fimBloqueio".ToCamelCase() }));
                }
            }

            if (DynamicQueryable.Any(validationErrors))
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create, AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ConfiguracaoDeBloqueioInput input)
        {
            IdInput result = null;

            try
            {
                using (var tran = UnitOfWorkManager.Begin())
                {
                    if (input.Id == default(int))
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create))
                            throw new UserFriendlyException(L("PermissionToCreateRecord"));

                        await base.CreateAndReturnEntity<ConfiguracaoDeBloqueioInput, ConfiguracaoDeBloqueioInput>(_configuracaoDeBloqueioService, input, () => ValidateInput(input), ProcessInput);
                    }
                    else
                    {
                        await base.UpdateAndReturnEntity<ConfiguracaoDeBloqueioInput, ConfiguracaoDeBloqueioInput>(_configuracaoDeBloqueioService, input, () => ValidateInput(input), ProcessInput);
                    }
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create, AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit)]
        public async Task<ConfiguracaoDeBloqueioInput> SaveAndReturnEntity(ConfiguracaoDeBloqueioInput input)
        {
            ConfiguracaoDeBloqueioInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ConfiguracaoDeBloqueioInput, ConfiguracaoDeBloqueioInput>(_configuracaoDeBloqueioService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ConfiguracaoDeBloqueioInput, ConfiguracaoDeBloqueioInput>(_configuracaoDeBloqueioService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<ConfiguracaoDeBloqueioListDto>> GetPaginado(GetConfiguracaoDeBloqueioInput input)
        {
            IList<WhereIfCondition<ConfiguracaoDeBloqueio>> condicoes = new List<WhereIfCondition<ConfiguracaoDeBloqueio>>();
            condicoes.Add(new WhereIfCondition<ConfiguracaoDeBloqueio>(!input.Titulo.IsNullOrEmpty(), x => x.Titulo.ToString().Contains(input.Titulo)));
            var result = await base.GetListPaged<ConfiguracaoDeBloqueioListDto, GetConfiguracaoDeBloqueioInput>(_configuracaoDeBloqueioService, input, condicoes);
            return result;
        }

        public async Task<ConfiguracaoDeBloqueioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ConfiguracaoDeBloqueioInput, IdInput>(_configuracaoDeBloqueioService, input);
        }

        public async Task<ConfiguracaoDeBloqueioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ConfiguracaoDeBloqueioInput, IdInput>(_configuracaoDeBloqueioService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Delete)]
        public async Task Delete(IdInput input)
        {
            var bloqueios = _bloqueioService.GetAll().Where(x => x.ConfiguracaoDeBloqueio.Id == input.Id);

            foreach(Bloqueio b in bloqueios)
            {
                await _bloqueioService.Delete(b.Id);
            }

            await base.DeleteEntity(_configuracaoDeBloqueioService, input);
        }
    }
}