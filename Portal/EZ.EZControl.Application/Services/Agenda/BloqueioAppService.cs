﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Bloqueio;
using EZ.EZControl.Services.Agenda.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class BloqueioAppService : EZControlAppServiceBase<Bloqueio>, IBloqueioAppService
    {
        private readonly IBloqueioService _bloqueioService;
        public BloqueioAppService(IRepository<Bloqueio> repository,
                                         IBloqueioService bloqueioService)
            : base(repository)
        {
            this._bloqueioService = bloqueioService;
        }

        private void ValidateInput(BloqueioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ConfiguracaoDeBloqueioId == 0)
            {
                validationErrors.Add(
                    new ValidationResult(
                               string.Format(L("Bloqueio.EmptyConfiguracaoDeBloqueioError"), input.ConfiguracaoDeBloqueioId),
                               new[] { "configuracaoDeBloqueioId".ToCamelCase() }));
            }

            if (input.Data == null)
            {
                validationErrors.Add(
                    new ValidationResult(
                               string.Format(L("Bloqueio.RequiredDataError"), input.Data),
                               new[] { "data".ToCamelCase() }));
            }

            if (input.Horario == null)
            {
                validationErrors.Add(
                    new ValidationResult(
                               string.Format(L("Bloqueio.RequiredHorarioError"), input.Horario),
                               new[] { "horario".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public void Insert(IEnumerable<BloqueioInput> input)
        {
            base.Insert(_bloqueioService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create, AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit)]
        public async Task<IdInput> Save(BloqueioInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, BloqueioInput>(_bloqueioService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, BloqueioInput>(_bloqueioService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create, AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit)]
        public async Task<BloqueioInput> SaveAndReturnEntity(BloqueioInput input)
        {
            BloqueioInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<BloqueioInput, BloqueioInput>(_bloqueioService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<BloqueioInput, BloqueioInput>(_bloqueioService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<BloqueioListDto>> GetPaginado(GetBloqueioInput input)
        {
            IList<WhereIfCondition<Bloqueio>> condicoes = new List<WhereIfCondition<Bloqueio>>();
            condicoes.Add(new WhereIfCondition<Bloqueio>(!input.Data.IsNullOrEmpty(), x => x.Data.ToString().Contains(input.Data)));
            var result = await base.GetListPaged<BloqueioListDto, GetBloqueioInput>(_bloqueioService, input, condicoes);
            return result;
        }

        public async Task<BloqueioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<BloqueioInput, IdInput>(_bloqueioService, input);
        }

        public async Task<BloqueioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<BloqueioInput, IdInput>(_bloqueioService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Bloqueio_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_bloqueioService, input);
        }
    }
}