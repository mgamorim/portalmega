﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using EZ.EZControl.Services.Agenda.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoTarefaAppService : EZControlAppServiceBase<EventoTarefa>, IEventoTarefaAppService
    {
        private readonly IEventoTarefaService _service;

        public EventoTarefaAppService(
            IRepository<EventoTarefa, int> repository,
            IEventoTarefaService service)
            : base(repository)
        {
            _service = service;
        }

        public async Task<IEnumerable<EventoTarefaInput>> Update(IEnumerable<EventoTarefaInput> input)
        {
            var result = new List<EventoTarefaInput>();

            foreach (var item in input)
            {
                EventoTarefa evento = await _service.GetById(item.Id);
                if (evento != null)
                {
                    item.MapTo(evento);
                    await _service.CreateOrUpdateAndReturnSavedEntity(evento);
                    result.Add(evento.MapTo<EventoTarefaInput>());
                }
            }

            return result;
        }

        public async Task<IEnumerable<EventoTarefaInput>> Create(IEnumerable<EventoTarefaInput> input)
        {
            var result = new List<EventoTarefaInput>();

            foreach (var item in input)
            {
                var evento = new EventoTarefa();
                item.MapTo(evento);
                var id = await _service.CreateEntity(evento);
                evento.Id = id;
                result.Add(evento.MapTo<EventoTarefaInput>());
            }

            return result;
        }

        public async Task<IEnumerable<EventoTarefaInput>> Destroy(IEnumerable<EventoTarefaInput> input)
        {
            var result = new List<EventoTarefaInput>();

            foreach (var item in input)
            {
                await _service.Delete(item.Id);
                result.Add(item);
            }

            return result;
        }

        public async Task<ListResultDto<EventoTarefaInput>> Read()
        {
            try
            {
                var lista = _service.GetAll().ToList();
                var listaDto = lista.MapTo<List<EventoTarefaInput>>();
                return new ListResultDto<EventoTarefaInput>(listaDto);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}