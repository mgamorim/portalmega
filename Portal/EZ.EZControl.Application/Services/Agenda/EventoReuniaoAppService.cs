﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using EZ.EZControl.Services.Agenda.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoReuniaoAppService : EZControlAppServiceBase<EventoReuniao>, IEventoReuniaoAppService
    {
        private readonly IEventoReuniaoService _service;

        public EventoReuniaoAppService(
            IRepository<EventoReuniao, int> repository,
            IEventoReuniaoService service)
            : base(repository)
        {
            _service = service;
        }

        public async Task<IEnumerable<EventoReuniaoInput>> Update(IEnumerable<EventoReuniaoInput> input)
        {
            var result = new List<EventoReuniaoInput>();

            foreach (var item in input)
            {
                EventoReuniao evento = await _service.GetById(item.Id);
                if (evento != null)
                {
                    item.MapTo(evento);
                    await _service.CreateOrUpdateAndReturnSavedEntity(evento);
                    result.Add(evento.MapTo<EventoReuniaoInput>());
                }
            }

            return result;
        }

        public async Task<IEnumerable<EventoReuniaoInput>> Create(IEnumerable<EventoReuniaoInput> input)
        {
            var result = new List<EventoReuniaoInput>();

            foreach (var item in input)
            {
                var evento = new EventoReuniao();
                item.MapTo(evento);
                var id = await _service.CreateEntity(evento);
                evento.Id = id;
                result.Add(evento.MapTo<EventoReuniaoInput>());
            }

            return result;
        }

        public async Task<IEnumerable<EventoReuniaoInput>> Destroy(IEnumerable<EventoReuniaoInput> input)
        {
            var result = new List<EventoReuniaoInput>();

            foreach (var item in input)
            {
                await _service.Delete(item.Id);
                result.Add(item);
            }

            return result;
        }

        public async Task<ListResultDto<EventoReuniaoInput>> Read()
        {
            try
            {
                var lista = _service.GetAll().ToList();
                var listaDto = lista.MapTo<List<EventoReuniaoInput>>();
                return new ListResultDto<EventoReuniaoInput>(listaDto);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}