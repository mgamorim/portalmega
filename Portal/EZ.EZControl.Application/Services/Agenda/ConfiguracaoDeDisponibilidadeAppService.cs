﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ConfiguracaoDeDisponibilidadeAppService : EZControlAppServiceBase<ConfiguracaoDeDisponibilidade>, IConfiguracaoDeDisponibilidadeAppService
    {
        private readonly IConfiguracaoDeDisponibilidadeService _configuracaoDeDisponibilidadeService;
        private readonly IDisponibilidadeAppService _disponibilidadeAppService;
        private readonly IRegraBaseService _regraService;
        private readonly IDisponibilidadeService _disponibilidadeService;
        private readonly IEventoBaseService _eventoService;

        public ConfiguracaoDeDisponibilidadeAppService(IRepository<ConfiguracaoDeDisponibilidade> repository,
            IConfiguracaoDeDisponibilidadeService configuracaoDeDisponibilidadeService,
            IDisponibilidadeAppService disponibilidadeAppService,
            IRegraBaseService regraService,
            IDisponibilidadeService disponibilidadeService,
            IEventoBaseService eventoService)
            : base(repository)
        {
            this._configuracaoDeDisponibilidadeService = configuracaoDeDisponibilidadeService;
            this._regraService = regraService;
            this._disponibilidadeService = disponibilidadeService;
            this._disponibilidadeAppService = disponibilidadeAppService;
            this._eventoService = eventoService;
        }

        private void ValidateInput(ConfiguracaoDeDisponibilidadeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Titulo == input.Titulo))
            {
                validationErrors.Add(
                    new ValidationResult(string.Format(L("Disponibilidade.DuplicateTituloError"), input.Titulo),
                        new[] { "titulo".ToCamelCase() }));
            }

            if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.Semanal)
            {
                if (input.DataInicioValidade == null)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredDataInicioValidadeError"), input.DataInicioValidade),
                            new[] { "dataInicioValidade".ToCamelCase() }));
                }
                else
                {
                    if (input.DataInicioValidade.Value.Date < DateTime.Now.Date)
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Disponibilidade.DataInicioValidadeError"), input.DataInicioValidade),
                                new[] { "dataInicioValidade".ToCamelCase() }));
                }
                if (input.DataFimValidade == null)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredDataFimValidadeError"), input.DataFimValidade),
                            new[] { "dataFimValidade".ToCamelCase() }));
                }
                else
                {
                    if (input.DataInicioValidade != null &&
                        input.DataFimValidade.Value.Date < input.DataInicioValidade.Value.Date)
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Disponibilidade.DataFimValidadeError"), input.DataInicioValidade),
                                new[] { "dataFimValidade".ToCamelCase() }));
                }

                if (!DynamicQueryable.Any(input.Semana))
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredDiaDaSemana"), input.DiasDaSemana),
                            new[] { "diaDaSemana".ToCamelCase() }));

            }

            if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.DiasCorridos)
            {
                if (input.NumeroDeDias == 0)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredNumeroDeDias"), input.NumeroDeDias),
                            new[] { "numeroDeDias".ToCamelCase() }));
                }
            }

            if (input.HorarioInicio > input.HorarioFim)
                validationErrors.Add(
                    new ValidationResult(
                        string.Format(L("Disponibilidade.RangeHora"), input.HorarioInicio),
                        new[] { "horarioInicio".ToCamelCase() }));


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ConfiguracaoDeDisponibilidade entity, ConfiguracaoDeDisponibilidadeInput input)
        {
            if ((input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.Semanal ||
                 input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.DiasCorridos) &&
                 input.DataInicioValidade != null && input.DataFimValidade != null)
            {
                if (entity.Disponibilidades != null && entity.Disponibilidades.Any())
                {
                    var idsDisponibilidade = entity.Disponibilidades.Select(dispo => dispo.Id);
                    var eventos = _eventoService
                                         .GetAll()
                                         .Where(evento => idsDisponibilidade.Contains(evento.Disponibilidade.Id))
                                         .ToList();

                    if (eventos.Any())
                    {
                        foreach(EventoBase ev in eventos)
                        {
                            ev.Disponibilidade = null;
                            ev.Historico.Add(new HistoricoDoEvento()
                            {
                                Data = DateTime.Now,
                                Evento = ev,
                                Status = StatusDoEventoEnum.Indefinido
                            });
                            await _eventoService.UpdateEntity(ev);
                        }
                            
                    }

                    var count = entity.Disponibilidades.Count - 1;
                    while (entity.Disponibilidades.Count > 0)
                    {
                        var disponibilidade = entity.Disponibilidades.ElementAt(count);
                        await _disponibilidadeAppService.Delete(new IdInput(disponibilidade.Id));
                        entity.Disponibilidades.Remove(disponibilidade);
                        count = count - 1;
                    }
                }

                var disponibilidades = GetDisponibilidades(input, entity);
                if (disponibilidades.Any())
                    _disponibilidadeService.Insert(disponibilidades);
            }

        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create,
             AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ConfiguracaoDeDisponibilidadeInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                IdInput result = null;

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ConfiguracaoDeDisponibilidadeInput>(_configuracaoDeDisponibilidadeService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ConfiguracaoDeDisponibilidadeInput>(_configuracaoDeDisponibilidadeService, input, () => ValidateInput(input), ProcessInput);
                }

                trans.Complete();
                return result;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create, AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit)]
        public async Task<ConfiguracaoDeDisponibilidadeInput> SaveAndReturnEntity(ConfiguracaoDeDisponibilidadeInput input)
        {
            ConfiguracaoDeDisponibilidadeInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ConfiguracaoDeDisponibilidadeInput, ConfiguracaoDeDisponibilidadeInput>(_configuracaoDeDisponibilidadeService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ConfiguracaoDeDisponibilidadeInput, ConfiguracaoDeDisponibilidadeInput>(_configuracaoDeDisponibilidadeService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<ConfiguracaoDeDisponibilidadeListDto>> GetConfiguracaoDeDisponibilidadesPaginado(GetConfiguracaoDeDisponibilidadeInput input)
        {
            IList<WhereIfCondition<ConfiguracaoDeDisponibilidade>> condicoes = new List<WhereIfCondition<ConfiguracaoDeDisponibilidade>>();
            condicoes.Add(new WhereIfCondition<ConfiguracaoDeDisponibilidade>(!input.Titulo.IsNullOrEmpty(), x => x.Titulo.ToString().Contains(input.Titulo)));
            var result = await base.GetListPaged<ConfiguracaoDeDisponibilidadeListDto, GetConfiguracaoDeDisponibilidadeInput>(_configuracaoDeDisponibilidadeService, input, condicoes);
            return result;
        }

        public async Task<ConfiguracaoDeDisponibilidadeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ConfiguracaoDeDisponibilidadeInput, IdInput>(_configuracaoDeDisponibilidadeService, input);
        }

        public async Task<ConfiguracaoDeDisponibilidadeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ConfiguracaoDeDisponibilidadeInput, IdInput>(_configuracaoDeDisponibilidadeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_configuracaoDeDisponibilidadeService, input);
        }

        public async Task<bool> WithEvento(IdInput input)
        {
            var entity = await Repository.FirstOrDefaultAsync(x => x.Id == input.Id);
            if (entity == null) return false;

            var idsDisponibilidade = entity.Disponibilidades.Select(dispo => dispo.Id);
            var eventos = _eventoService
                .GetAll()
                .Where(evento => idsDisponibilidade.Contains(evento.Disponibilidade.Id))
                .ToList();
            return eventos.Any();
        }

        private IEnumerable<Disponibilidade> GetDisponibilidades(ConfiguracaoDeDisponibilidadeInput input, ConfiguracaoDeDisponibilidade configuracaoDeDisponibilidade)
        {
            var regra = _regraService.GetAll().FirstOrDefault(x => x.Sistema == input.Sistema && x.TipoDeEvento == input.TipoDeEvento);

            if (regra == null)
                throw new UserFriendlyException(L("Disponibilidade.RegraNotFound"));

            var disponibilidades = new List<Disponibilidade>();

            if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.Semanal)
            {
                var dtInicial = input.DataInicioValidade.Value.Date;
                var dtFinal = input.DataFimValidade.Value.Date;

                while (dtInicial != dtFinal)
                {
                    var inicial = dtInicial;
                    inicial = inicial.AddHours(input.HorarioInicio.Hours)
                                     .AddMinutes(input.HorarioInicio.Minutes);

                    foreach(DayOfWeek diaDaSemana in input.Semana)
                    {
                        if (inicial.DayOfWeek == diaDaSemana)
                        {
                            disponibilidades.Add(new Disponibilidade
                            {
                                ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidade,
                                Data = inicial,
                                Horario = inicial.TimeOfDay
                            });

                            while (inicial.Hour <= input.HorarioFim.Hours)
                            {
                                inicial = inicial.AddMinutes(regra.UnidadeDeTempo);

                                if (inicial.TimeOfDay <= input.HorarioFim)
                                {
                                    var dispo = new Disponibilidade
                                    {
                                        ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidade,
                                        Horario = inicial.TimeOfDay,
                                        Data = inicial
                                    };

                                    disponibilidades.Add(dispo);
                                }
                            }
                        }
                    }

                    dtInicial = dtInicial.AddDays(1);
                }
            }

            if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.DiasCorridos)
            {
                var dtInicial = input.DataInicioValidade.Value.Date;
                var dtFinal = input.DataFimValidade.Value.Date;

                while (dtInicial != dtFinal)
                {
                    var inicial = dtInicial;
                    inicial = inicial.AddHours(input.HorarioInicio.Hours)
                                     .AddMinutes(input.HorarioInicio.Minutes);

                    disponibilidades.Add(new Disponibilidade
                    {
                        ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidade,
                        Data = inicial,
                        Horario = inicial.TimeOfDay
                    });

                    while (inicial.Hour <= input.HorarioFim.Hours)
                    {
                        inicial = inicial.AddMinutes(regra.UnidadeDeTempo);

                        if (inicial.TimeOfDay <= input.HorarioFim)
                        {
                            var dispo = new Disponibilidade
                            {
                                ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidade,
                                Horario = inicial.TimeOfDay,
                                Data = inicial
                            };
                            disponibilidades.Add(dispo);
                        }
                    }

                    dtInicial = dtInicial.AddDays(1);
                }
            }

            return disponibilidades;
        }
    }
}