﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Parametro;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Services.Agenda
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Padroes, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ParametroAgendaAppService : EZControlAppServiceBase<ParametroAgenda>, IParametroAgendaAppService
    {
        private readonly IParametroAgendaService _parametroAgendaService;

        public ParametroAgendaAppService(IRepository<ParametroAgenda> parametroRepository,
            IParametroAgendaService parametroAgendaService)
            : base(parametroRepository)
        {
            this._parametroAgendaService = parametroAgendaService;
        }

        private void ValidateInput(ParametroAgendaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<IdInput> Save(ParametroAgendaInput input)
        {
            var parametro = await Get();
            if (parametro != null)
                input.Id = parametro.Id;

            IdInput savedEntity;

            if (input.Id == default(int))
            {
                savedEntity = await base.CreateEntity<IdInput, ParametroAgendaInput>(_parametroAgendaService, input, () => ValidateInput(input), null);
            } else
            {
                savedEntity = await base.UpdateEntity<IdInput, ParametroAgendaInput>(_parametroAgendaService, input, () => ValidateInput(input), null);
            }

            return savedEntity;
        }

        public async Task<ParametroAgendaInput> Get()
        {
            var condicoes = new List<WhereIfCondition<ParametroAgenda>>();
            condicoes.Add(new WhereIfCondition<ParametroAgenda>(true, x => x.Id > 0));
            var parametros = await base.GetList<ParametroAgendaInput>(_parametroAgendaService, condicoes, x => x.Id);

            return parametros.Items.FirstOrDefault();
        }
    }
}