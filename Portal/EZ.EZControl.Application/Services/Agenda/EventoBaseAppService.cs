﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    public class EventoBaseAppService : EZControlAppServiceBase<EventoBase>, IEventoBaseAppService
    {
        private readonly IEventoBaseService _eventoBaseService;
        private readonly IDisponibilidadeService _disponibilidadeService;
        private readonly IPessoaService _pessoaService;


        public EventoBaseAppService(
            IRepository<EventoBase, int> repository,
            IEventoBaseService eventoBaseService,
            IDisponibilidadeService disponibilidadeService,
            IPessoaService pessoaService)
            : base(repository)
        {
            _eventoBaseService = eventoBaseService;
            _disponibilidadeService = disponibilidadeService;
            _pessoaService = pessoaService;
        }

        private void ValidateInput(EventoInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(input.Titulo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Evento.EmptyTituloError"), input.Titulo),
                    new[] {"titulo".ToCamelCase()}));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(EventoBase evento, EventoInput input)
        {
            if (input.EventoPaiId > 0)
            {
                try
                {
                    evento.EventoPai =
                        await _eventoBaseService.GetAll().FirstOrDefaultAsync(x => x.Id == input.EventoPaiId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Evento.EventoPaiNotFound"), ex.Message);
                }
            }

            if (input.OwnerId > 0)
            {
                try
                {
                    evento.Owner = await _pessoaService.GetAll().FirstOrDefaultAsync(x => x.Id == input.OwnerId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Evento.OwnerNotFound"), ex.Message);
                }
            }


            if (input.IdsParticipantes != null &&
                input.IdsParticipantes.Any())
            {
                evento.Participantes = new List<Pessoa>();
                foreach( int id in input.IdsParticipantes)
                {
                    try
                    {
                        var pessoa = await _pessoaService.GetAll().FirstOrDefaultAsync(x => x.Id == id);
                        evento.Participantes.Add(pessoa);
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(L("Evento.OwnerNotFound"), ex.Message);
                    }
                }

            }

            var disponibilidade = await _disponibilidadeService.GetAll().FirstOrDefaultAsync(x => x.Data == input.Inicio);
            if (disponibilidade != null)
                evento.Disponibilidade = disponibilidade;
            else
                throw new UserFriendlyException(L("Evento.DisponibilidadeNotFound"));
        }


        public async Task<IEnumerable<EventoInput>> Update(IEnumerable<EventoInput> input)
        {
            var result = new List<EventoInput>();

            foreach (var item in input)
            {
                EventoBase evento = await _eventoBaseService.GetById(item.Id);
                if (evento != null)
                {
                    item.MapTo(evento);
                    await _eventoBaseService.CreateOrUpdateAndReturnSavedEntity(evento);
                    result.Add(evento.MapTo<EventoInput>());
                }
            }

            return result;
        }

        public async Task<IEnumerable<EventoInput>> Create(IEnumerable<EventoInput> input)
        {
            var result = new List<EventoInput>();

            foreach (var item in input)
            {
                var evento = new EventoBase();
                item.MapTo(evento);
                var id = await _eventoBaseService.CreateEntity(evento);
                evento.Id = id;
                result.Add(evento.MapTo<EventoInput>());
            }

            return result;
        }

        public async Task<IEnumerable<EventoInput>> Destroy(IEnumerable<EventoInput> input)
        {
            var result = new List<EventoInput>();

            foreach (var item in input)
            {
                await _eventoBaseService.Delete(item.Id);
                result.Add(item);
            }

            return result;
        }

        public async Task<ListResultDto<EventoInput>> Read()
        {
            try
            {
                var lista = _eventoBaseService.GetAll().ToList();
                if (!lista.Any())
                {
                    DateTime now = DateTime.Now;

                    EventoBase ev1 = new EventoBase()
                    {
                        Descricao = "Almoço",
                        Inicio = new DateTime(now.Year, now.Month, now.Day, 11, 0, 0, 0),
                        Termino = new DateTime(now.Year, now.Month, now.Day, 12, 0, 0, 0),
                        Titulo = "Almoço",
                        Sistema = SistemaEnum.Global,
                        TipoDeEvento = TipoDeEventoEnum.Tarefa
                    };

                    EventoBase ev2 = new EventoBase()
                    {
                        Descricao = "Consulta",
                        Inicio = new DateTime(now.Year, now.Month, now.Day, 13, 0, 0, 0),
                        Termino = new DateTime(now.Year, now.Month, now.Day, 14, 0, 0, 0),
                        Titulo = "Consulta",
                        Sistema = SistemaEnum.Global,
                        TipoDeEvento = TipoDeEventoEnum.Consulta
                    };

                    EventoBase ev3 = new EventoBase()
                    {
                        Descricao = "Reunião",
                        Inicio = new DateTime(now.Year, now.Month, now.Day, 14, 0, 0, 0),
                        Termino = new DateTime(now.Year, now.Month, now.Day, 15, 0, 0, 0),
                        Titulo = "Reunião",
                        Sistema = SistemaEnum.Agenda,
                        TipoDeEvento = TipoDeEventoEnum.Reuniao
                    };

                    await _eventoBaseService.CreateOrUpdateAndReturnSavedEntity(ev1);
                    await _eventoBaseService.CreateOrUpdateAndReturnSavedEntity(ev2);
                    await _eventoBaseService.CreateOrUpdateAndReturnSavedEntity(ev3);

                    lista.Add(ev1);
                    lista.Add(ev2);
                    lista.Add(ev3);
                }
                var listaDto = lista.MapTo<List<EventoInput>>();
                return new ListResultDto<EventoInput>(listaDto);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PagedResultDto<EventoListDto>> GetEventosPaginado(GetEventoInput input)
        {
            IList<WhereIfCondition<EventoBase>> condicoes = new List<WhereIfCondition<EventoBase>>();
            condicoes.Add(new WhereIfCondition<EventoBase>(!input.Titulo.IsNullOrEmpty(),
                x => x.Titulo.Contains(input.Titulo)));
            var result = await base.GetListPaged<EventoListDto, GetEventoInput>(_eventoBaseService, input, condicoes);
            return result;
        }

        public async Task<IdInput> Save(EventoInput input)
        {
            try
            {
                IdInput result = null;

                if (input.Id == default(int))
                {
                    result = await base.CreateEntity<IdInput, EventoInput>(_eventoBaseService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    result = await base.UpdateEntity<IdInput, EventoInput>(_eventoBaseService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;
            }
            catch (Exception ex)
            {
                    
                throw ex;
            }
        }

        public async Task<ListResultDto<EventoListDto>> GetEventoExceptForId(GetEventoExceptForInput input)
        {
            var query = await Repository
               .GetAll()
               .Where(x => x.Id != input.Id)
               .WhereIf(!string.IsNullOrWhiteSpace(input.Titulo), x => x.Titulo.Contains(input.Titulo))
               .OrderBy(x => x.Titulo)
               .ToListAsync();

            return new ListResultDto<EventoListDto>(query.MapTo<List<EventoListDto>>());
        }

        public async Task<EventoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EventoInput, IdInput>(_eventoBaseService, input);
        }

        public async Task<EventoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EventoInput, IdInput>(_eventoBaseService, input);
        }

        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_eventoBaseService, input);
        }
    }
}