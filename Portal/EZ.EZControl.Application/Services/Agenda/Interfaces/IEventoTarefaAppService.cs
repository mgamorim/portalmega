﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IEventoTarefaAppService : IApplicationService
    {
        Task<IEnumerable<EventoTarefaInput>> Update(IEnumerable<EventoTarefaInput> input);

        Task<IEnumerable<EventoTarefaInput>> Create(IEnumerable<EventoTarefaInput> input);

        Task<IEnumerable<EventoTarefaInput>> Destroy(IEnumerable<EventoTarefaInput> input);

        Task<ListResultDto<EventoTarefaInput>> Read();
    }
}