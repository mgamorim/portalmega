﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Regra;

namespace EZ.EZControl.Services.Agenda.Interfaces.Regra
{
    public interface IRegraTarefaAppService : IApplicationService
    {
        Task<IdInput> Save(RegraTarefaInput input);

        Task<RegraTarefaInput> SaveAndReturnEntity(RegraTarefaInput input);

        Task<RegraTarefaInput> GetById(IdInput input);

        Task<RegraTarefaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}