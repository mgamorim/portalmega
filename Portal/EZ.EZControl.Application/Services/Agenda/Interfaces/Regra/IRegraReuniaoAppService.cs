﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Regra;

namespace EZ.EZControl.Services.Agenda.Interfaces.Regra
{
    public interface IRegraReuniaoAppService : IApplicationService
    {
        Task<IdInput> Save(RegraReuniaoInput input);

        Task<RegraReuniaoInput> SaveAndReturnEntity(RegraReuniaoInput input);

        Task<RegraReuniaoInput> GetById(IdInput input);

        Task<RegraReuniaoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}