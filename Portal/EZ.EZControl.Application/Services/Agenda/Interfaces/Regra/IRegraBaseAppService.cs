﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda;
using EZ.EZControl.Dto.Agenda.Regra;

namespace EZ.EZControl.Services.Agenda.Interfaces.Regra
{
    public interface IRegraBaseAppService : IApplicationService
    {
        Task<IdInput> Save(RegraBaseInput input);

        Task<RegraBaseInput> SaveAndReturnEntity(RegraBaseInput input);

        Task<RegraBaseInput> GetById(IdInput input);

        Task<RegraBaseInput> GetByExternalId(IdInput input);

        Task<RegraBaseTipoDeEventoListDto> GetTiposDeEventosBySistema(GetTiposDeEventosBySistemaDto dto);

        Task<RegraBaseSistemaListDto> GetSistemasCriamEventos();

        Task<PagedResultDto<RegraBaseListDto>> GetPaginado(GetRegraBaseInput input);

        Task Delete(IdInput input);
    }
}