﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IConfiguracaoDeBloqueioAppService : IApplicationService
    {
        Task<IdInput> Save(ConfiguracaoDeBloqueioInput input);

        Task<ConfiguracaoDeBloqueioInput> SaveAndReturnEntity(ConfiguracaoDeBloqueioInput input);

        Task<PagedResultDto<ConfiguracaoDeBloqueioListDto>> GetPaginado(GetConfiguracaoDeBloqueioInput input);

        Task<ConfiguracaoDeBloqueioInput> GetById(IdInput input);

        Task<ConfiguracaoDeBloqueioInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}