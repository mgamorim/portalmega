﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IConfiguracaoDeDisponibilidadeAppService : IApplicationService
    {
        Task<IdInput> Save(ConfiguracaoDeDisponibilidadeInput input);

        Task<ConfiguracaoDeDisponibilidadeInput> SaveAndReturnEntity(ConfiguracaoDeDisponibilidadeInput input);

        Task<PagedResultDto<ConfiguracaoDeDisponibilidadeListDto>> GetConfiguracaoDeDisponibilidadesPaginado(GetConfiguracaoDeDisponibilidadeInput input);

        Task<ConfiguracaoDeDisponibilidadeInput> GetById(IdInput input);

        Task<ConfiguracaoDeDisponibilidadeInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<bool> WithEvento(IdInput input);
    }
}