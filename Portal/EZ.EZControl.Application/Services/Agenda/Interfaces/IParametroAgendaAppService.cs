﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.Parametro;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IParametroAgendaAppService : IApplicationService
    {
        Task<IdInput> Save(ParametroAgendaInput input);

        Task<ParametroAgendaInput> Get();
    }
}