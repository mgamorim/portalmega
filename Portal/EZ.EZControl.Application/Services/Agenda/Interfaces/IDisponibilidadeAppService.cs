﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.Disponibilidade;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IDisponibilidadeAppService : IApplicationService
    {
        void Insert(IEnumerable<DisponibilidadeInput> input);

        Task<IdInput> Save(DisponibilidadeInput input);

        Task<DisponibilidadeInput> SaveAndReturnEntity(DisponibilidadeInput input);

        Task<PagedResultDto<DisponibilidadeListDto>> GetDisponibilidadesPaginado(GetDisponibilidadeInput input);

        Task<DisponibilidadeInput> GetById(IdInput input);

        Task<DisponibilidadeInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}