﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.Bloqueio;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IBloqueioAppService : IApplicationService
    {
        void Insert(IEnumerable<BloqueioInput> input);

        Task<IdInput> Save(BloqueioInput input);

        Task<BloqueioInput> SaveAndReturnEntity(BloqueioInput input);

        Task<PagedResultDto<BloqueioListDto>> GetPaginado(GetBloqueioInput input);

        Task<BloqueioInput> GetById(IdInput input);

        Task<BloqueioInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}