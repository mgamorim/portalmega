﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IEventoBaseAppService : IApplicationService
    {
        Task<IEnumerable<EventoInput>> Update(IEnumerable<EventoInput> input);

        Task<IEnumerable<EventoInput>> Create(IEnumerable<EventoInput> input);

        Task<IEnumerable<EventoInput>> Destroy(IEnumerable<EventoInput> input);

        Task<ListResultDto<EventoInput>> Read();

        Task<PagedResultDto<EventoListDto>> GetEventosPaginado(GetEventoInput input);

        Task<ListResultDto<EventoListDto>> GetEventoExceptForId(GetEventoExceptForInput input);

        Task<IdInput> Save(EventoInput input);

        Task<EventoInput> GetById(IdInput input);

        Task<EventoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}