﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda.Interfaces
{
    public interface IEventoReuniaoAppService : IApplicationService
    {
        Task<IEnumerable<EventoReuniaoInput>> Update(IEnumerable<EventoReuniaoInput> input);

        Task<IEnumerable<EventoReuniaoInput>> Create(IEnumerable<EventoReuniaoInput> input);

        Task<IEnumerable<EventoReuniaoInput>> Destroy(IEnumerable<EventoReuniaoInput> input);

        Task<ListResultDto<EventoReuniaoInput>> Read();
    }
}