﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Agenda.Geral.Disponibilidade;
using EZ.EZControl.Services.Agenda.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Agenda
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class DisponibilidadeAppService : EZControlAppServiceBase<Disponibilidade>, IDisponibilidadeAppService
    {
        private readonly IDisponibilidadeService _disponibilidadeService;
        public DisponibilidadeAppService(IRepository<Disponibilidade> repository,
                                         IDisponibilidadeService disponibilidadeService)
            : base(repository)
        {
            this._disponibilidadeService = disponibilidadeService;
        }

        private void ValidateInput(DisponibilidadeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public void Insert(IEnumerable<DisponibilidadeInput> input)
        {
            base.Insert(_disponibilidadeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create, AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit)]
        public async Task<IdInput> Save(DisponibilidadeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, DisponibilidadeInput>(_disponibilidadeService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, DisponibilidadeInput>(_disponibilidadeService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create, AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit)]
        public async Task<DisponibilidadeInput> SaveAndReturnEntity(DisponibilidadeInput input)
        {
            DisponibilidadeInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<DisponibilidadeInput, DisponibilidadeInput>(_disponibilidadeService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<DisponibilidadeInput, DisponibilidadeInput>(_disponibilidadeService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<DisponibilidadeListDto>> GetDisponibilidadesPaginado(GetDisponibilidadeInput input)
        {
            IList<WhereIfCondition<Disponibilidade>> condicoes = new List<WhereIfCondition<Disponibilidade>>();
            condicoes.Add(new WhereIfCondition<Disponibilidade>(!input.Data.IsNullOrEmpty(), x => x.Data.ToString().Contains(input.Data)));
            var result = await base.GetListPaged<DisponibilidadeListDto, GetDisponibilidadeInput>(_disponibilidadeService, input, condicoes);
            return result;
        }

        public async Task<DisponibilidadeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<DisponibilidadeInput, IdInput>(_disponibilidadeService, input);
        }

        public async Task<DisponibilidadeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<DisponibilidadeInput, IdInput>(_disponibilidadeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_disponibilidadeService, input);
        }
    }
}