﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Evento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EventoConsultaAppService : EZControlAppServiceBase<EventoConsulta>, IEventoConsultaAppService
    {
        private readonly IEventoConsultaService _eventoConsultaService;
        private readonly IPessoaService _pessoaService;
        private readonly IDisponibilidadeService _disponibilidadeService;
        private readonly IRegraBaseService _regraBaseService;

        public EventoConsultaAppService(
            IRepository<EventoConsulta, int> repository,
            IEventoConsultaService eventoConsultaService,
            IPessoaService pessoaService,
            IDisponibilidadeService disponibilidadeService,
            IRegraBaseService regraBaseService)
            : base(repository)
        {
            _eventoConsultaService = eventoConsultaService;
            _pessoaService = pessoaService;
            _disponibilidadeService = disponibilidadeService;
            _regraBaseService = regraBaseService;
        }

        private void ValidateInput(EventoConsultaInput input)
        {
            var validationErrors = new List<ValidationResult>();
            var statusQueRequeremObservacao = new[] { StatusDoEventoEnum.Cancelado, StatusDoEventoEnum.Transferido };
            if (string.IsNullOrEmpty(input.Titulo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Evento.EmptyTituloError"), input.Titulo),
                    new[] { "titulo".ToCamelCase() }));
            }
            if (input.StatusDoEvento != StatusDoEventoEnum.Agendado)
            {
                if (statusQueRequeremObservacao.Contains(input.StatusDoEvento) && string.IsNullOrEmpty(input.Observacao))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("Evento.ObservacaoRequiredError"), input.StatusDoEvento.ToString()),
                    new[] { "statusDoEvento".ToCamelCase() }));
                }
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(EventoConsulta evento, EventoConsultaInput input)
        {
            if (input.EventoPaiId > 0)
            {
                try
                {
                    evento.EventoPai =
                        await _eventoConsultaService.GetAll().FirstOrDefaultAsync(x => x.Id == input.EventoPaiId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Evento.EventoPaiNotFound"), ex.Message);
                }
            }

            if (input.OwnerId > 0)
            {
                try
                {
                    evento.Owner = await _pessoaService.GetAll().FirstOrDefaultAsync(x => x.Id == input.OwnerId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Evento.OwnerNotFound"), ex.Message);
                }
            }

            if (input.IdsParticipantes != null &&
                input.IdsParticipantes.Any())
            {
                evento.Participantes = new List<Pessoa>();
                foreach( int id in input.IdsParticipantes)
                {
                    try
                    {
                        var pessoa = _pessoaService.GetAll().FirstOrDefault(x => x.Id == id);
                        evento.Participantes.Add(pessoa);
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(L("Evento.ParticipanteNotFound"), ex.Message);
                    }
                }
            }

            if (input.Id > 0 && input.StatusDoEvento != StatusDoEventoEnum.Agendado)
            {
                evento.Historico.Add(new HistoricoDoEvento()
                {
                    Data = DateTime.Now,
                    Evento = evento,
                    Observacao = input.Observacao,
                    Status = input.StatusDoEvento
                });
            }
            else
            {
                if (input.Id == 0)
                {
                    evento.Historico = new List<HistoricoDoEvento>();
                    evento.Historico.Add(new HistoricoDoEvento()
                    {
                        Data = DateTime.Now,
                        Evento = evento,
                        Status = StatusDoEventoEnum.Agendado
                    });
                }
            }

            var disponibilidade = _disponibilidadeService.GetAll().FirstOrDefault(x => x.Data == input.Inicio);
            if (disponibilidade == null)
                throw new UserFriendlyException(L("Evento.DisponibilidadeNotFound"));
            else
                evento.Disponibilidade = disponibilidade;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Evento_Edit)]
        public async Task<List<EventoConsultaInput>> Update(List<EventoConsultaInput> input)
        {
            var evento = new EventoConsulta();
            var retorno = new List<EventoConsultaInput>();
            foreach (var item in input)
            {
                item.TipoDeEvento = TipoDeEventoEnum.Consulta;
                item.Sistema = SistemaEnum.EZMedical;
                item.MapTo(evento);
                evento.Owner = await _pessoaService.GetById(item.OwnerId);
                await _eventoConsultaService.CreateOrUpdateAndReturnSavedEntity(evento);
                retorno.Add(evento.MapTo<EventoConsultaInput>());
            }
            return retorno;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Evento_Create)]
        public async Task<List<EventoConsultaInput>> Create(List<EventoConsultaInput> input)
        {
            var evento = new EventoConsulta();
            var retorno = new List<EventoConsultaInput>();
            foreach (var item in input)
            {
                item.TipoDeEvento = TipoDeEventoEnum.Consulta;
                item.Sistema = SistemaEnum.EZMedical;
                item.MapTo(evento);
                evento.Owner = await _pessoaService.GetById(item.OwnerId);
                var id = await _eventoConsultaService.CreateEntity(evento);
                evento.Id = id;
                retorno.Add(evento.MapTo<EventoConsultaInput>());
            }
            return retorno;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Evento_Delete)]
        public async Task<List<EventoConsultaInput>> Destroy(List<EventoConsultaInput> input)
        {
            var retorno = new List<EventoConsultaInput>();
            foreach (var item in input)
            {
                await _eventoConsultaService.Delete(item.Id);
                retorno.Add(item);
            }
            return retorno;
        }

        public async Task<List<EventoConsultaInput>> Read(GetEventoConsultaInput input)
        {
            var lista = _eventoConsultaService
                        .GetAll()
                        .Where(
                            x => x.TipoDeEvento == input.Tipo &&
                            x.Sistema == input.Sistema &&
                            (x.Inicio >= input.Inicio &&
                            x.Termino <= input.Termino))
                        .ToList();
            lista = (from e in lista
                     where
                     e.Historico.OrderByDescending(h => h.Data).FirstOrDefault().Status != StatusDoEventoEnum.Cancelado &&
                     e.Historico.OrderByDescending(h => h.Data).FirstOrDefault().Status != StatusDoEventoEnum.Transferido
                     select e).ToList();
            return lista.MapTo<List<EventoConsultaInput>>();
        }

        public async Task<ListResultDto<EventoConsultaListDto>> GetEventoExceptForId(GetEventoConsultaExceptForInput input)
        {
            var query = await Repository
               .GetAll()
               .Where(x => x.Id != input.Id)
               .WhereIf(!string.IsNullOrWhiteSpace(input.Titulo), x => x.Titulo.Contains(input.Titulo))
               .OrderBy(x => x.Titulo)
               .ToListAsync();

            return new ListResultDto<EventoConsultaListDto>(query.MapTo<List<EventoConsultaListDto>>());
        }

        public async Task<EventoConsultaInput> GetById(IdInput input)
        {
            var evento = await _eventoConsultaService.GetById(input.Id);
            evento.StatusDoEvento = evento.Historico.OrderByDescending(x => x.Data).First().Status;
            return evento.MapTo<EventoConsultaInput>();
        }

        public async Task<ListResultDto<EventoConsultaListDto>> GetPaginado(GetEventoConsultaInput input)
        {
            throw new NotImplementedException();
        }

        public async Task<IdInput> Save(EventoConsultaInput input)
        {
            IdInput result = null;

            input.Sistema = SistemaEnum.EZMedical;
            input.TipoDeEvento = TipoDeEventoEnum.Consulta;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Evento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, EventoConsultaInput>(_eventoConsultaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Evento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, EventoConsultaInput>(_eventoConsultaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }
    }
}