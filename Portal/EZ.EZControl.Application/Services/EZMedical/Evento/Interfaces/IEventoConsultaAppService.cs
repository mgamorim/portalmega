﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Interfaces
{
    public interface IEventoConsultaAppService : IApplicationService
    {
        Task<List<EventoConsultaInput>> Update(List<EventoConsultaInput> input);
        Task<List<EventoConsultaInput>> Create(List<EventoConsultaInput> input);
        Task<List<EventoConsultaInput>> Destroy(List<EventoConsultaInput> input);
        Task<List<EventoConsultaInput>> Read(GetEventoConsultaInput input);
        Task<EventoConsultaInput> GetById(IdInput input);
        Task<ListResultDto<EventoConsultaListDto>> GetEventoExceptForId(GetEventoConsultaExceptForInput input);
        Task<ListResultDto<EventoConsultaListDto>> GetPaginado(GetEventoConsultaInput input);
        Task<IdInput> Save(EventoConsultaInput input);
    }
}