﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using EZ.EZControl.Services.EZMedical.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Bloqueio, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ConfiguracaoDeBloqueioConsultaAppService : EZControlAppServiceBase<ConfiguracaoDeBloqueioConsulta>,
        IConfiguracaoDeBloqueioConsultaAppService
    {
        private readonly IConfiguracaoDeBloqueioConsultaService _configuracaoDeBloqueioConsultaService;
        private readonly IBloqueioService _bloqueioService;
        private readonly IRegraConsultaService _regraService;
        private readonly IBloqueioAppService _bloqueioAppService;
        private readonly IMedicoService _medicoService;
        private readonly IEspecialidadeService _especialidadeService;
        public ConfiguracaoDeBloqueioConsultaAppService(IRepository<ConfiguracaoDeBloqueioConsulta> repository,
            IConfiguracaoDeBloqueioConsultaService configuracaoDeBloqueioConsultaService,
            IBloqueioService bloqueioService,
            IRegraConsultaService regraService,
            IBloqueioAppService bloqueioAppService,
            IMedicoService medicoService,
            IEspecialidadeService especialidadeService)
            : base(repository)
        {
            _configuracaoDeBloqueioConsultaService = configuracaoDeBloqueioConsultaService;
            _regraService = regraService;
            _bloqueioService = bloqueioService;
            _bloqueioAppService = bloqueioAppService;
            _medicoService = medicoService;
            _especialidadeService = especialidadeService;
        }

        private async Task ProcessInput(ConfiguracaoDeBloqueioConsulta entity, ConfiguracaoDeBloqueioConsultaInput input)
        {
            if (input.MedicoId > 0)
            {
                var medico = await _medicoService.GetById(input.MedicoId);
                if (medico != null)
                {
                    entity.Medico = medico;
                }
                else
                {
                    throw new UserFriendlyException(L("ConfiguracaoDeBloqueio.MedicoNotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("ConfiguracaoDeBloqueio.EmptyMedicoError"));
            }

            if (input.EspecialidadeId > 0)
            {
                var especialidade = await _especialidadeService.GetById(input.EspecialidadeId);
                if (especialidade != null)
                {
                    entity.Especialidade = especialidade;
                }
                else
                {
                    throw new UserFriendlyException(L("ConfiguracaoDeBloqueio.EspecialidadeNotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("ConfiguracaoDeBloqueio.EmptyEspecialidadeError"));
            }

            entity.HorarioInicio = TimeSpan.Parse(input.HorarioInicio);
            entity.HorarioFim = TimeSpan.Parse(input.HorarioFim);

            var regra = _regraService.GetAll().Where(x => x.Sistema == input.Sistema && x.TipoDeEvento == input.TipoDeEvento).FirstOrDefault();
            if (regra == null)
            {
                throw new UserFriendlyException(L("ConfiguracaoDeBloqueio.RegraNotFoundError", input.Sistema, input.TipoDeEvento));
            }

            if (entity.Bloqueios != null && entity.Bloqueios.Any())
            {
                var count = entity.Bloqueios.Count - 1;
                while (entity.Bloqueios.Count > 0)
                {
                    var bloqueio = entity.Bloqueios.ElementAt(count);
                    await _bloqueioAppService.Delete(new IdInput(bloqueio.Id));
                    entity.Bloqueios.Remove(bloqueio);
                    count = count - 1;
                }
            }

            var dataControle = input.InicioBloqueio;
            while (dataControle <= input.FimBloqueio)
            {
                var horarioControle = TimeSpan.Parse(input.HorarioInicio);
                while (horarioControle <= TimeSpan.Parse(input.HorarioFim))
                {
                    var bloqueio = new Bloqueio()
                    {
                        Data = dataControle,
                        Horario = horarioControle,
                        IsActive = true
                    };
                    entity.Bloqueios.Add(bloqueio);
                    horarioControle = horarioControle.Add(TimeSpan.FromMinutes(regra.UnidadeDeTempo));
                }
                dataControle = dataControle.AddDays(1);
            }
        }

        private void ValidateInput(ConfiguracaoDeBloqueioConsultaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.InicioBloqueio == null)
            {
                validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("BloqueioConsulta.RequiredInicioBloqueioError"), input.InicioBloqueio),
                            new[] { "inicioBloqueioConsulta".ToCamelCase() }));
            }

            if (input.FimBloqueio == null)
            {
                validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("BloqueioConsulta.RequiredFimBloqueioError"), input.FimBloqueio),
                            new[] { "fimBloqueioConsulta".ToCamelCase() }));
            }

            if (input.InicioBloqueio != null && input.FimBloqueio != null)
            {
                if (input.FimBloqueio < input.InicioBloqueio)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("BloqueioConsulta.PeriodoInvalidoError"), input.FimBloqueio),
                            new[] { "fimBloqueioConsulta".ToCamelCase() }));
                }
            }

            if (DynamicQueryable.Any(validationErrors))
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Create, AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ConfiguracaoDeBloqueioConsultaInput input)
        {
            IdInput result = null;

            try
            {
                using (var tran = UnitOfWorkManager.Begin())
                {
                    if (input.Id == default(int))
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Create))
                            throw new UserFriendlyException(L("PermissionToCreateRecord"));

                        await base.CreateAndReturnEntity<ConfiguracaoDeBloqueioConsultaInput, ConfiguracaoDeBloqueioConsultaInput>(_configuracaoDeBloqueioConsultaService, input, () => ValidateInput(input), ProcessInput);
                    }
                    else
                    {
                        await base.UpdateAndReturnEntity<ConfiguracaoDeBloqueioConsultaInput, ConfiguracaoDeBloqueioConsultaInput>(_configuracaoDeBloqueioConsultaService, input, () => ValidateInput(input), ProcessInput);
                    }
                    tran.Complete();
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Create, AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Edit)]
        public async Task<ConfiguracaoDeBloqueioConsultaInput> SaveAndReturnEntity(ConfiguracaoDeBloqueioConsultaInput input)
        {
            ConfiguracaoDeBloqueioConsultaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ConfiguracaoDeBloqueioConsultaInput, ConfiguracaoDeBloqueioConsultaInput>(_configuracaoDeBloqueioConsultaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ConfiguracaoDeBloqueioConsultaInput, ConfiguracaoDeBloqueioConsultaInput>(_configuracaoDeBloqueioConsultaService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<ConfiguracaoDeBloqueioConsultaListDto>> GetPaginado(GetConfiguracaoDeBloqueioConsultaInput input)
        {
            IList<WhereIfCondition<ConfiguracaoDeBloqueioConsulta>> condicoes = new List<WhereIfCondition<ConfiguracaoDeBloqueioConsulta>>();
            condicoes.Add(new WhereIfCondition<ConfiguracaoDeBloqueioConsulta>(!input.Titulo.IsNullOrEmpty(), x => x.Titulo.ToString().Contains(input.Titulo)));
            condicoes.Add(new WhereIfCondition<ConfiguracaoDeBloqueioConsulta>(!input.Especialidade.IsNullOrEmpty(), x => x.Especialidade.Nome.ToString().Contains(input.Especialidade)));
            condicoes.Add(new WhereIfCondition<ConfiguracaoDeBloqueioConsulta>(!input.Medico.IsNullOrEmpty(), x => x.Medico.NomePessoa.ToString().Contains(input.Medico)));
            var result = await base.GetListPaged<ConfiguracaoDeBloqueioConsultaListDto, GetConfiguracaoDeBloqueioConsultaInput>(_configuracaoDeBloqueioConsultaService, input, condicoes);
            return result;
        }

        public async Task<ConfiguracaoDeBloqueioConsultaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ConfiguracaoDeBloqueioConsultaInput, IdInput>(_configuracaoDeBloqueioConsultaService, input);
        }

        public async Task<ConfiguracaoDeBloqueioConsultaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ConfiguracaoDeBloqueioConsultaInput, IdInput>(_configuracaoDeBloqueioConsultaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Bloqueio_Delete)]
        public async Task Delete(IdInput input)
        {
            var bloqueios = _bloqueioService.GetAll().Where(x => x.ConfiguracaoDeBloqueio.Id == input.Id);
            foreach(Bloqueio b in bloqueios)
            {
                await _bloqueioService.Delete(b.Id);
            }

           await base.DeleteEntity(_configuracaoDeBloqueioConsultaService, input);
        }
    }
}