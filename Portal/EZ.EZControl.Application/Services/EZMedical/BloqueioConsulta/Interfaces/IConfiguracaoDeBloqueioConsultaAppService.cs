﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Interfaces
{
    public interface IConfiguracaoDeBloqueioConsultaAppService : IApplicationService
    {
        Task<IdInput> Save(ConfiguracaoDeBloqueioConsultaInput input);

        Task<ConfiguracaoDeBloqueioConsultaInput> SaveAndReturnEntity(ConfiguracaoDeBloqueioConsultaInput input);

        Task<PagedResultDto<ConfiguracaoDeBloqueioConsultaListDto>> GetPaginado(GetConfiguracaoDeBloqueioConsultaInput input);

        Task<ConfiguracaoDeBloqueioConsultaInput> GetById(IdInput input);

        Task<ConfiguracaoDeBloqueioConsultaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}