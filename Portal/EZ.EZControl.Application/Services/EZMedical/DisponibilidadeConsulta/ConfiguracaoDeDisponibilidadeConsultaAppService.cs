﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using EZ.EZControl.Services.EZMedical.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ConfiguracaoDeDisponibilidadeConsultaAppService : EZControlAppServiceBase<ConfiguracaoDeDisponibilidadeConsulta>, IConfiguracaoDeDisponibilidadeConsultaAppService
    {
        private readonly IConfiguracaoDeDisponibilidadeConsultaService _configuracaoDeDisponibilidadeConsultaService;
        private readonly IDisponibilidadeAppService _disponibilidadeAppService;
        private readonly IRegraConsultaService _regraService;
        private readonly IDisponibilidadeService _disponibilidadeService;
        private readonly IEventoBaseService _eventoService;
        private readonly IMedicoService _medicoService;
        private readonly IEspecialidadeService _especialidadeService;
        public ConfiguracaoDeDisponibilidadeConsultaAppService(IRepository<ConfiguracaoDeDisponibilidadeConsulta> repository,
            IConfiguracaoDeDisponibilidadeConsultaService configuracaoDeDisponibilidadeConsultaService,
            IDisponibilidadeAppService disponibilidadeAppService,
            IRegraConsultaService regraService,
            IDisponibilidadeService disponibilidadeService,
            IEventoBaseService eventoService,
            IMedicoService medicoService,
            IEspecialidadeService especialidadeService)
            : base(repository)
        {
            this._configuracaoDeDisponibilidadeConsultaService = configuracaoDeDisponibilidadeConsultaService;
            this._regraService = regraService;
            this._disponibilidadeService = disponibilidadeService;
            this._disponibilidadeAppService = disponibilidadeAppService;
            this._eventoService = eventoService;
            _medicoService = medicoService;
            _especialidadeService = especialidadeService;
        }

        private void ValidateInput(ConfiguracaoDeDisponibilidadeConsultaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Titulo == input.Titulo))
            {
                validationErrors.Add(
                    new ValidationResult(string.Format(L("Disponibilidade.DuplicateTituloError"), input.Titulo),
                        new[] { "titulo".ToCamelCase() }));
            }

            if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.Semanal)
            {
                if (input.DataInicioValidade == null)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredDataInicioValidadeError"), input.DataInicioValidade),
                            new[] { "dataInicioValidade".ToCamelCase() }));
                }
                else
                {
                    if (input.DataInicioValidade.Value.Date < DateTime.Now.Date)
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Disponibilidade.DataInicioValidadeError"), input.DataInicioValidade),
                                new[] { "dataInicioValidade".ToCamelCase() }));
                }
                if (input.DataFimValidade == null)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredDataFimValidadeError"), input.DataFimValidade),
                            new[] { "dataFimValidade".ToCamelCase() }));
                }
                else
                {
                    if (input.DataInicioValidade != null &&
                        input.DataFimValidade.Value.Date < input.DataInicioValidade.Value.Date)
                        validationErrors.Add(
                            new ValidationResult(
                                string.Format(L("Disponibilidade.DataFimValidadeError"), input.DataInicioValidade),
                                new[] { "dataFimValidade".ToCamelCase() }));
                }

                if (!DynamicQueryable.Any(input.Semana))
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredDiaDaSemana"), input.DiasDaSemana),
                            new[] { "diaDaSemana".ToCamelCase() }));

            }

            if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.DiasCorridos)
            {
                if (input.NumeroDeDias == 0)
                {
                    validationErrors.Add(
                        new ValidationResult(
                            string.Format(L("Disponibilidade.RequiredNumeroDeDias"), input.NumeroDeDias),
                            new[] { "numeroDeDias".ToCamelCase() }));
                }
            }

            if (input.HorarioInicio > input.HorarioFim)
                validationErrors.Add(
                    new ValidationResult(
                        string.Format(L("Disponibilidade.RangeHora"), input.HorarioInicio),
                        new[] { "horarioInicio".ToCamelCase() }));


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ConfiguracaoDeDisponibilidadeConsulta entity, ConfiguracaoDeDisponibilidadeConsultaInput input)
        {
            if (input.MedicoId > 0)
            {
                var medico = await _medicoService.GetById(input.MedicoId);
                if (medico != null)
                {
                    entity.Medico = medico;
                }
                else
                {
                    throw new UserFriendlyException(L("ConfiguracaoDeDisponibilidade.MedicoNotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("ConfiguracaoDeDisponibilidade.EmptyMedicoError"));
            }

            if (input.EspecialidadeId > 0)
            {
                var especialidade = await _especialidadeService.GetById(input.EspecialidadeId);
                if (especialidade != null)
                {
                    entity.Especialidade = especialidade;
                }
                else
                {
                    throw new UserFriendlyException(L("ConfiguracaoDeDisponibilidade.EspecialidadeNotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("ConfiguracaoDeDisponibilidade.EmptyEspecialidadeError"));
            }

            if ((input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.Semanal ||
                 input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.DiasCorridos) &&
                 input.DataInicioValidade != null && input.DataFimValidade != null)
            {
                if (entity.Disponibilidades != null && entity.Disponibilidades.Any())
                {
                    var idsDisponibilidadeConsulta = entity.Disponibilidades.Select(dispo => dispo.Id);
                    var eventos = _eventoService
                                         .GetAll()
                                         .Where(evento => idsDisponibilidadeConsulta.Contains(evento.Disponibilidade.Id))
                                         .ToList();

                    if (eventos.Any())
                    {
                        foreach(EventoBase ev in eventos)
                        {
                            ev.Disponibilidade = null;
                            ev.Historico.Add(new HistoricoDoEvento()
                            {
                                Data = DateTime.Now,
                                Evento = ev,
                                Status = StatusDoEventoEnum.Indefinido
                            });
                            await _eventoService.UpdateEntity(ev);
                        }

                    }

                    var count = entity.Disponibilidades.Count - 1;
                    while (entity.Disponibilidades.Count > 0)
                    {
                        var disponibilidade = entity.Disponibilidades.ElementAt(count);
                        await _disponibilidadeAppService.Delete(new IdInput(disponibilidade.Id));
                        entity.Disponibilidades.Remove(disponibilidade);
                        count = count - 1;
                    }
                }

                var disponibilidades = GetDisponibilidadeConsultas(input, entity);
                if (disponibilidades.Any())
                    _disponibilidadeService.Insert(disponibilidades);
            }

        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Create,
             AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ConfiguracaoDeDisponibilidadeConsultaInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                IdInput result = null;

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ConfiguracaoDeDisponibilidadeConsultaInput>(_configuracaoDeDisponibilidadeConsultaService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ConfiguracaoDeDisponibilidadeConsultaInput>(_configuracaoDeDisponibilidadeConsultaService, input, () => ValidateInput(input), ProcessInput);
                }

                trans.Complete();
                return result;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Create, AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Edit)]
        public async Task<ConfiguracaoDeDisponibilidadeConsultaInput> SaveAndReturnEntity(ConfiguracaoDeDisponibilidadeConsultaInput input)
        {
            ConfiguracaoDeDisponibilidadeConsultaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ConfiguracaoDeDisponibilidadeConsultaInput, ConfiguracaoDeDisponibilidadeConsultaInput>(_configuracaoDeDisponibilidadeConsultaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ConfiguracaoDeDisponibilidadeConsultaInput, ConfiguracaoDeDisponibilidadeConsultaInput>(_configuracaoDeDisponibilidadeConsultaService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<ConfiguracaoDeDisponibilidadeConsultaListDto>> GetConfiguracaoDeDisponibilidadeConsultasPaginado(GetConfiguracaoDeDisponibilidadeConsultaInput input)
        {
            IList<WhereIfCondition<ConfiguracaoDeDisponibilidadeConsulta>> condicoes = new List<WhereIfCondition<ConfiguracaoDeDisponibilidadeConsulta>>();
            condicoes.Add(new WhereIfCondition<ConfiguracaoDeDisponibilidadeConsulta>(!input.Titulo.IsNullOrEmpty(), x => x.Titulo.ToString().Contains(input.Titulo)));
            var result = await base.GetListPaged<ConfiguracaoDeDisponibilidadeConsultaListDto, GetConfiguracaoDeDisponibilidadeConsultaInput>(_configuracaoDeDisponibilidadeConsultaService, input, condicoes);
            return result;
        }

        public async Task<ConfiguracaoDeDisponibilidadeConsultaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ConfiguracaoDeDisponibilidadeConsultaInput, IdInput>(_configuracaoDeDisponibilidadeConsultaService, input);
        }

        public async Task<ConfiguracaoDeDisponibilidadeConsultaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ConfiguracaoDeDisponibilidadeConsultaInput, IdInput>(_configuracaoDeDisponibilidadeConsultaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Disponibilidade_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_configuracaoDeDisponibilidadeConsultaService, input);
        }

        public async Task<bool> WithEvento(IdInput input)
        {
            var entity = await Repository.FirstOrDefaultAsync(x => x.Id == input.Id);
            if (entity == null) return false;

            var idsDisponibilidadeConsulta = entity.Disponibilidades.Select(dispo => dispo.Id);
            var eventos = _eventoService
                .GetAll()
                .Where(evento => idsDisponibilidadeConsulta.Contains(evento.Disponibilidade.Id))
                .ToList();
            return eventos.Any();
        }

        private IEnumerable<Disponibilidade> GetDisponibilidadeConsultas(ConfiguracaoDeDisponibilidadeConsultaInput input, ConfiguracaoDeDisponibilidadeConsulta configuracaoDeDisponibilidadeConsulta)
        {
            return this.ApplyEmpresaFilter<IEnumerable<Disponibilidade>>(() =>
            {
                var regra = _regraService.GetAll().FirstOrDefault(x => x.Sistema == input.Sistema && x.TipoDeEvento == input.TipoDeEvento);

                if (regra == null)
                    throw new UserFriendlyException(L("Disponibilidade.RegraNotFound"));

                var disponibilidades = new List<Disponibilidade>();

                if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.Semanal)
                {
                    var dtInicial = input.DataInicioValidade.Value.Date;
                    var dtFinal = input.DataFimValidade.Value.Date;

                    while (dtInicial != dtFinal)
                    {
                        var inicial = dtInicial;
                        inicial = inicial.AddHours(input.HorarioInicio.Hours)
                                         .AddMinutes(input.HorarioInicio.Minutes);

                        foreach(DayOfWeek diaDaSemana in input.Semana)
                        {
                            if (inicial.DayOfWeek == diaDaSemana)
                            {
                                disponibilidades.Add(new Disponibilidade
                                {
                                    ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidadeConsulta,
                                    Data = inicial,
                                    Horario = inicial.TimeOfDay
                                });

                                while (inicial.Hour <= input.HorarioFim.Hours)
                                {
                                    inicial = inicial.AddMinutes(regra.UnidadeDeTempo);

                                    if (inicial.TimeOfDay <= input.HorarioFim)
                                    {
                                        var dispo = new Disponibilidade
                                        {
                                            ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidadeConsulta,
                                            Horario = inicial.TimeOfDay,
                                            Data = inicial
                                        };

                                        disponibilidades.Add(dispo);
                                    }
                                }
                            }
                        }
                        dtInicial = dtInicial.AddDays(1);
                    }
                }

                if (input.TipoDeDisponibilidade == TipoDeDisponibilidadeEnum.DiasCorridos)
                {
                    var dtInicial = input.DataInicioValidade.Value.Date;
                    var dtFinal = input.DataFimValidade.Value.Date;

                    while (dtInicial != dtFinal)
                    {
                        var inicial = dtInicial;
                        inicial = inicial.AddHours(input.HorarioInicio.Hours)
                                         .AddMinutes(input.HorarioInicio.Minutes);

                        disponibilidades.Add(new Disponibilidade
                        {
                            ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidadeConsulta,
                            Data = inicial,
                            Horario = inicial.TimeOfDay
                        });

                        while (inicial.Hour <= input.HorarioFim.Hours)
                        {
                            inicial = inicial.AddMinutes(regra.UnidadeDeTempo);

                            if (inicial.TimeOfDay <= input.HorarioFim)
                            {
                                var dispo = new Disponibilidade
                                {
                                    ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidadeConsulta,
                                    Horario = inicial.TimeOfDay,
                                    Data = inicial
                                };
                                disponibilidades.Add(dispo);
                            }
                        }

                        dtInicial = dtInicial.AddDays(1);
                    }
                }

                return disponibilidades;
            });
        }
    }
}