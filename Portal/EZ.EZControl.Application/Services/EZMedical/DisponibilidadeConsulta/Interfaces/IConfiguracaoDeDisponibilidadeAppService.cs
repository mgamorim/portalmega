﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Interfaces
{
    public interface IConfiguracaoDeDisponibilidadeConsultaAppService : IApplicationService
    {
        Task<IdInput> Save(ConfiguracaoDeDisponibilidadeConsultaInput input);

        Task<ConfiguracaoDeDisponibilidadeConsultaInput> SaveAndReturnEntity(ConfiguracaoDeDisponibilidadeConsultaInput input);

        Task<PagedResultDto<ConfiguracaoDeDisponibilidadeConsultaListDto>> GetConfiguracaoDeDisponibilidadeConsultasPaginado(GetConfiguracaoDeDisponibilidadeConsultaInput input);

        Task<ConfiguracaoDeDisponibilidadeConsultaInput> GetById(IdInput input);

        Task<ConfiguracaoDeDisponibilidadeConsultaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<bool> WithEvento(IdInput input);
    }
}