﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Services.EZMedical.Especialidade.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Especialidade
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Especialidade, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EspecialidadeAppService : EZControlAppServiceBase<Domain.EZMedical.Geral.Especialidade>,
        IEspecialidadeAppService
    {
        private readonly IEspecialidadeService _especialidadeService;
        private readonly IEspecialidadeSyncService _especialidadeSyncService;

        public EspecialidadeAppService(IRepository<Domain.EZMedical.Geral.Especialidade, int> repository,
            IEspecialidadeService especialidadeService,
            IEspecialidadeSyncService especialidadeSyncService) : base(repository)
        {
            _especialidadeService = especialidadeService;
            _especialidadeSyncService = especialidadeSyncService;
        }

        private void ValidateInput(EspecialidadeInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Codigo))
                validationErrors.Add(new ValidationResult(L("Especialidade.RequiredCodigoError"),
                    new[] { "codigo".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("Especialidade.RequiredNomeError"),
                    new[] { "nome".ToCamelCase() }));

            if (_especialidadeService.CheckForFieldDuplicated(input.Id, input.Codigo, x => x.Codigo == input.Codigo))
            {
                validationErrors.Add(new ValidationResult("Código existente.", new[] { "codigo".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Create,
             AppPermissions.Pages_Tenant_EZMedical_Especialidade_Edit)]
        public async Task<IdInput> Save(EspecialidadeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result =
                    await
                        base.CreateEntity<IdInput, EspecialidadeInput>(_especialidadeService, input,
                            () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result =
                    await
                        base.UpdateEntity<IdInput, EspecialidadeInput>(_especialidadeService, input,
                            () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Create,
             AppPermissions.Pages_Tenant_EZMedical_Especialidade_Edit)]
        public async Task<EspecialidadeInput> SaveAndReturnEntity(EspecialidadeInput input)
        {
            EspecialidadeInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result =
                    await
                        base.CreateAndReturnEntity<EspecialidadeInput, EspecialidadeInput>(_especialidadeService, input,
                            () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result =
                    await
                        base.UpdateAndReturnEntity<EspecialidadeInput, EspecialidadeInput>(_especialidadeService, input,
                            () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<EspecialidadeListDto>> GetEspecialidadesPaginado(GetEspecialidadeInput input)
        {
            var condicoes = new List<WhereIfCondition<Domain.EZMedical.Geral.Especialidade>>
            {
                new WhereIfCondition<Domain.EZMedical.Geral.Especialidade>(
                    !string.IsNullOrEmpty(input.Codigo) ||
                    !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Codigo.Contains(input.Codigo) ||
                        a.Nome.Contains(input.Nome))
            };

            var result =
                await
                    base.GetListPaged<EspecialidadeListDto, GetEspecialidadeInput>(_especialidadeService, input,
                        condicoes);
            return result;
        }

        public async Task<EspecialidadeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EspecialidadeInput, IdInput>(_especialidadeService, input);
        }

        public async Task<EspecialidadeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EspecialidadeInput, IdInput>(_especialidadeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Especialidade_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_especialidadeService, input);
        }

        public async Task<PagedResultDto<EspecialidadeListDto>> GetPaginadoExceptForIds(GetEspecialidadeInput input)
        {
            var condicoes = new List<WhereIfCondition<Domain.EZMedical.Geral.Especialidade>>
            {
                new WhereIfCondition<Domain.EZMedical.Geral.Especialidade>(
                    input.Ids.Length > 0,
                    a => !input.Ids.Contains(a.Id))
            };

            var result =
                await
                    base.GetListPaged<EspecialidadeListDto, GetEspecialidadeInput>(_especialidadeService, input,
                        condicoes);
            return result;
        }

        public async void SincronizarEspecialidade()
        {
            await _especialidadeSyncService.Sincronizar();
        }
    }
}
