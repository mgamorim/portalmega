﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Especialidade.Interfaces
{
    public interface IEspecialidadeAppService : IApplicationService
    {
        Task<IdInput> Save(EspecialidadeInput input);

        Task<EspecialidadeInput> SaveAndReturnEntity(EspecialidadeInput input);

        Task<PagedResultDto<EspecialidadeListDto>> GetEspecialidadesPaginado(GetEspecialidadeInput input);

        Task<EspecialidadeInput> GetById(IdInput input);

        Task<EspecialidadeInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<EspecialidadeListDto>> GetPaginadoExceptForIds(GetEspecialidadeInput input);

        void SincronizarEspecialidade();
    }
}
