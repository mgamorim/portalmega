﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZMedical.Geral.Regra;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces.RegraConsulta;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Regra
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras, AppPermissions.Pages_Tenant_EZMedical_RegraConsulta, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RegraConsultaAppService : EZControlAppServiceBase<RegraConsulta>, IRegraConsultaAppService
    {
        private readonly IRegraConsultaService _regraConsultaService;
        private readonly IMedicoService _medicoService;
        private readonly IEspecialidadeService _especialidadeService;

        public RegraConsultaAppService(IRepository<RegraConsulta> repository,
                                         IRegraConsultaService regraConsultaService,
                                         IMedicoService medicoService,
                                         IEspecialidadeService especialidadeService)
            : base(repository)
        {
            _regraConsultaService = regraConsultaService;
            _medicoService = medicoService;
            _especialidadeService = especialidadeService;
        }

        private void ValidateInput(RegraConsultaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(RegraConsulta entity, RegraConsultaInput input)
        {
            if (input.MedicoId > 0)
            {
                var medico = await _medicoService.GetById(input.MedicoId);
                if (medico != null)
                {
                    entity.Medico = medico;
                }
                else
                {
                    throw new UserFriendlyException(L("Medico.NotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("RegraConsulta.EmtptyMedicoError"));
            }

            if (input.EspecialidadeId == 0)
            {
                throw new UserFriendlyException(L("RegraConsulta.EmtptyEspecialidadeError"));

            }
            else
            {
                var especialidade = await _especialidadeService.GetById(input.EspecialidadeId);
                if (especialidade != null)
                {
                    entity.Especialidade = especialidade;
                }
                else
                {
                    throw new UserFriendlyException(L("Especialidade.NotFoundError"));
                }
            }
        }

        [AbpAuthorize(
            AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Create,
            AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(RegraConsultaInput input)
        {
            using (var tran = UnitOfWorkManager.Begin())
            {
                try
                {
                    IdInput result = null;

                    if (input.Id == default(int))
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Create))
                            throw new UserFriendlyException(L("PermissionToCreateRecord"));

                        result = await base.CreateEntity<IdInput, RegraConsultaInput>(_regraConsultaService, input, () => ValidateInput(input), ProcessInput);
                    }
                    else
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Edit))
                            throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                        result = await base.UpdateEntity<IdInput, RegraConsultaInput>(_regraConsultaService, input, () => ValidateInput(input), ProcessInput);
                    }

                    await tran.CompleteAsync();

                    return result;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message, ex.InnerException);
                }
            }
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Create,
             AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Edit)]
        public async Task<RegraConsultaInput> SaveAndReturnEntity(RegraConsultaInput input)
        {
            RegraConsultaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Create) &&
                    !PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result =
                    await
                        base.CreateAndReturnEntity<RegraConsultaInput, RegraConsultaInput>(_regraConsultaService, input,
                            () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Agenda_Disponibilidade_Edit) &&
                    !PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result =
                    await
                        base.UpdateAndReturnEntity<RegraConsultaInput, RegraConsultaInput>(_regraConsultaService, input,
                            () => ValidateInput(input));
            }

            return result;
        }

        public async Task<RegraConsultaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RegraConsultaInput, IdInput>(_regraConsultaService, input);
        }

        public async Task<RegraConsultaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RegraConsultaInput, IdInput>(_regraConsultaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Agenda_Parametros_Regras_Delete, AppPermissions.Pages_Tenant_EZMedical_RegraConsulta_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_regraConsultaService, input);
        }

        public async Task<RegraConsultaInput> Get()
        {
            var regra = await _regraConsultaService.GetAll()
                .Where(x => x.Sistema == SistemaEnum.EZMedical && x.TipoDeEvento == TipoDeEventoEnum.Consulta)
                .FirstOrDefaultAsync();
            return regra.MapTo<RegraConsultaInput>();
        }

        public async Task<PagedResultDto<RegraConsultaListDto>> GetPaginado(GetRegraConsultaInput input)
        {
            IList<WhereIfCondition<RegraConsulta>> condicoes = new List<WhereIfCondition<RegraConsulta>>();
            condicoes.Add(new WhereIfCondition<RegraConsulta>(!string.IsNullOrEmpty(input.Medico), x => x.Medico.NomePessoa.Contains(input.Medico)));
            condicoes.Add(new WhereIfCondition<RegraConsulta>(!string.IsNullOrEmpty(input.Especialidade), x => x.Especialidade.Nome.Contains(input.Especialidade)));
            var result = await base.GetListPaged<RegraConsultaListDto, GetRegraConsultaInput>(_regraConsultaService, input, condicoes);
            return result;
        }
    }
}