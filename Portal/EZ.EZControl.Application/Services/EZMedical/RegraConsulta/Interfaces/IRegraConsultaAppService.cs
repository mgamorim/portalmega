﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.Geral.Regra;

namespace EZ.EZControl.Services.EZMedical.Interfaces.RegraConsulta
{
    public interface IRegraConsultaAppService : IApplicationService
    {
        Task<IdInput> Save(RegraConsultaInput input);

        Task<RegraConsultaInput> SaveAndReturnEntity(RegraConsultaInput input);

        Task<RegraConsultaInput> GetById(IdInput input);

        Task<RegraConsultaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
        Task<RegraConsultaInput> Get();
        Task<PagedResultDto<RegraConsultaListDto>> GetPaginado(GetRegraConsultaInput input);
    }
}