﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Medico.Interfaces
{
    public interface IMedicoAppService : IApplicationService
    {
        Task<MedicoPessoaIdDto> Save(MedicoInput input);

        Task<MedicoListDto> SaveAndReturnEntity(MedicoInput input);

        Task<PagedResultDto<MedicoListDto>> GetMedicosPaginado(GetMedicoInput input);

        Task<MedicoInput> GetById(IdInput input);

        Task<MedicoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForMedico(GetPessoaExceptForMedico input);
    }
}
