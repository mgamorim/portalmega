﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.Core.Geral.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.EZMedical.Medico.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical.Medico
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Medico, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class MedicoAppService : EZControlAppServiceBase<Domain.EZMedical.SubtiposPessoa.Medico>, IMedicoAppService
    {
        private readonly IMedicoService _medicoService;
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaFisicaAppService _pessoaFisicaAppService;
        private readonly IPictureService _pictureService;
        private readonly IEspecialidadeService _especialidadeService;

        public MedicoAppService(IRepository<Domain.EZMedical.SubtiposPessoa.Medico, int> repository,
            IMedicoService medicoService, IPessoaFisicaService pessoaFisicaService,
            IPessoaFisicaAppService pessoaFisicaAppService, IPessoaService pessoaService,
            IPictureService pictureService,
            IEspecialidadeService especialidadeService)
            : base(repository)
        {
            _medicoService = medicoService;
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaFisicaAppService = pessoaFisicaAppService;
            _pessoaService = pessoaService;
            _pictureService = pictureService;
            _especialidadeService = especialidadeService;
        }

        private void ValidateInput(MedicoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            //if (CheckForFieldDuplicated(Repository, input.Id, input.Nome, x => x.Nome == input.Nome))
            //{
            //    validationErrors.Add(new ValidationResult("Já existe um cliente com esse nome.", new[] { "nome".ToCamelCase() }));
            //}

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        private async Task ProcessInput(Domain.EZMedical.SubtiposPessoa.Medico medico, MedicoInput input)
        {
            try
            {
                if (medico.PessoaFisica != null &&
                    medico.PessoaFisica.Id > 0)
                {
                    var pf = await _pessoaFisicaService.GetById(input.PessoaFisica.Id);
                    medico.PessoaFisica = pf;
                }
                else
                {
                    PessoaFisicaInput pessoaFisicaInput = new PessoaFisicaInput();
                    input.PessoaFisica.MapTo(pessoaFisicaInput);
                    input.Pessoa.MapTo(pessoaFisicaInput);

                    var pessoaId = await _pessoaFisicaAppService.Save(pessoaFisicaInput);
                    PessoaFisica pessoaFisica = await _pessoaFisicaService.GetById(pessoaId.Id);
                    medico.PessoaFisica = pessoaFisica;
                }

                if (input.IdsEspecialidades != null &&
                    input.IdsEspecialidades.Any())
                {
                    medico.Especialidades = new List<Domain.EZMedical.Geral.Especialidade>();
                    foreach(int id in input.IdsEspecialidades)
                    {
                        try
                        {
                            var especialidade = _especialidadeService.GetAll().FirstOrDefault(x => x.Id == id);
                            medico.Especialidades.Add(especialidade);
                        }
                        catch (Exception ex)
                        {
                            throw new UserFriendlyException(L("Medico.EspecialidadeNotFound"), ex.Message);
                        }
                    }
                }
                else
                    throw new UserFriendlyException(L("Medico.RequiredEspecialidadeError"));

                if (input.PictureId > 0)
                {
                    var picture = await _pictureService.GetById(input.PictureId);
                    medico.Picture = picture;
                }
                else
                    throw new UserFriendlyException(L("Medico.RequiredPictureError"));
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Medico_Create,
             AppPermissions.Pages_Tenant_EZMedical_Medico_Edit)]
        public async Task<MedicoPessoaIdDto> Save(MedicoInput input)
        {
            MedicoListDto medico = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Medico_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                medico = await base.CreateAndReturnEntity<MedicoListDto, MedicoInput>(_medicoService, input,
                            () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Medico_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                medico = await base.UpdateAndReturnEntity<MedicoListDto, MedicoInput>(_medicoService, input,
                            () => ValidateInput(input), ProcessInput);
            }

            MedicoPessoaIdDto result = new MedicoPessoaIdDto
            {
                MedicoId = medico.Id,
                PessoaId = medico.PessoaFisica.Id,
                PictureId = medico.PictureId
            };

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Medico_Create, AppPermissions.Pages_Tenant_EZMedical_Medico_Edit)]
        public async Task<MedicoListDto> SaveAndReturnEntity(MedicoInput input)
        {
            MedicoListDto result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Medico_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result =
                    await
                        base.CreateAndReturnEntity<MedicoListDto, MedicoInput>(_medicoService, input,
                            () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZMedical_Medico_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result =
                    await
                        base.UpdateAndReturnEntity<MedicoListDto, MedicoInput>(_medicoService, input,
                            () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<MedicoListDto>> GetMedicosPaginado(GetMedicoInput input)
        {
            try
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    var queryMedico = Repository.GetAll();
                    var queryPessoa = _pessoaService.GetAll();
                    var queryPessoaFisica = _pessoaFisicaService.GetAll();

                    var query = from medico in queryMedico
                                join pessoa in queryPessoa on medico.PessoaFisica.Id equals pessoa.Id
                                join pessoaFisica in queryPessoaFisica on pessoa.Id equals pessoaFisica.Id
                                select new
                                {
                                    medico,
                                    pessoa,
                                    pessoaFisica
                                };

                    if (input != null)
                    {
                        if (!string.IsNullOrEmpty(input.Nome))
                            query = query.Where(x =>
                            x.pessoaFisica.Nome.Contains(input.Nome));

                        if (!string.IsNullOrEmpty(input.NumeroDocumento))
                            query = query.Where(x => x.pessoa.Documentos.Any(y => y.Numero == input.NumeroDocumento));
                    }

                    var count = await query.CountAsync();
                    var dados = await query
                        .OrderBy("medico.Id")
                        .PageBy(input)
                        .ToListAsync();

                    List<MedicoListDto> listDtos = new List<MedicoListDto>();

                    foreach (var item in dados)
                    {
                        MedicoListDto medicoListDto = item.medico.MapTo<MedicoListDto>();

                        listDtos.Add(medicoListDto);
                    }

                    return new PagedResultDto<MedicoListDto>(count, listDtos);
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<MedicoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<MedicoInput, IdInput>(_medicoService, input);
        }

        public async Task<MedicoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<MedicoInput, IdInput>(_medicoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZMedical_Medico_Delete)]
        public async Task Delete(IdInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                var medico = _medicoService.GetAll().FirstOrDefault(x => x.Id == input.Id);

                if (medico != null && medico.Picture != null)
                    await _pictureService.Delete(medico.Picture.Id);

                await base.DeleteEntity(_medicoService, input);
                trans.Complete();
            }
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForMedico(GetPessoaExceptForMedico input)
        {
            try
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    if (input == null)
                        throw new Exception("É necessário definir um critério para buscar por pessoas.");

                    var idsMedicos = Repository.GetAll().Select(y => y.PessoaFisica.Id);

                    var queryPessoaFisica = _pessoaFisicaService
                        .GetAll()
                        .Where(x => !idsMedicos.Contains(x.Id))
                        .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.Nome.Contains(input.NomePessoa))
                        .Select(x => new PessoaFisicaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.Nome,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Fisica
                        });

                    IQueryable<PessoaListDto> query = queryPessoaFisica;

                    var count = await query.CountAsync();

                    List<PessoaListDto> dados = await query
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    var listDtos = dados.MapTo<List<PessoaListDto>>();
                    return new PagedResultDto<PessoaListDto>(count, listDtos);
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}