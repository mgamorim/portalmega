﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PlanoDeSaudeAppService : EZControlAppServiceBase<PlanoDeSaude>, IPlanoDeSaudeAppService
    {
        private readonly IPlanoDeSaudeService _planoDeSaudeService;

        public PlanoDeSaudeAppService(IRepository<PlanoDeSaude> planoDeSaudeRepository,
                                      IPlanoDeSaudeService planoDeSaudeService)
            : base(planoDeSaudeRepository)
        {
            this._planoDeSaudeService = planoDeSaudeService;
        }

        private void ValidateInput(PlanoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.NumeroDeRegistro))
                validationErrors.Add(new ValidationResult(L("PlanoDeSaude.EmptyNumeroDeRegistro")));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.PlanoDeSaudeAns == input.PlanoDeSaudeAns))
                validationErrors.Add(new ValidationResult(string.Format(L("PlanoDeSaude.DuplicateTipoError"), input.PlanoDeSaudeAns), new[] { "planoDeSaudeAns".ToCamelCase() }));


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(PlanoDeSaude planoDeSaude, PlanoDeSaudeInput planoDeSaudeInput)
        {
            var planoPorTipo =
                await _planoDeSaudeService.GetAll()
                        .FirstOrDefaultAsync(x => x.PlanoDeSaudeAns == planoDeSaudeInput.PlanoDeSaudeAns);

            if (planoPorTipo != null)
                throw new UserFriendlyException(L("PlanoDeSaude.PorTipo"));
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Edit)]
        public async Task<IdInput> Save(PlanoDeSaudeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, PlanoDeSaudeInput>(_planoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, PlanoDeSaudeInput>(_planoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Edit)]
        public async Task<PlanoDeSaudeInput> SaveAndReturnEntity(PlanoDeSaudeInput input)
        {
            PlanoDeSaudeInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<PlanoDeSaudeInput, PlanoDeSaudeInput>(_planoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<PlanoDeSaudeInput, PlanoDeSaudeInput>(_planoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<PlanoDeSaudeListDto>> GetPlanosDeSaudePaginado(GetPlanoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<PlanoDeSaude>>
            {
                new WhereIfCondition<PlanoDeSaude>(!input.NumeroDeRegistro.IsNullOrEmpty(), r => r.NumeroDeRegistro.Contains(input.NumeroDeRegistro))
            };
            var result = await base.GetListPaged<PlanoDeSaudeListDto, GetPlanoDeSaudeInput>(_planoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<PlanoDeSaudeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<PlanoDeSaudeInput, IdInput>(_planoDeSaudeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PlanoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_planoDeSaudeService, input);
        }

        public async Task<PlanoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PlanoDeSaudeInput, IdInput>(_planoDeSaudeService, input);
        }
    }
}
