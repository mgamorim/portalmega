﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretor, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class CorretorAppService : EZControlAppServiceBase<Corretor>, ICorretorAppService
    {
        private readonly ICorretorService _corretorService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly ICorretoraService _corretoraService;
        private readonly UserManager _userManager;
        private readonly IPessoaService _pessoaService;
        private readonly IEmpresaService _empresaService;
        private readonly IAdministradoraService _administradoraService;
        public CorretorAppService(IRepository<Corretor> corretorRepository,
                                          ICorretorService corretorService,
                                          IPessoaFisicaService pessoaFisicaService,
                                          ICorretoraService corretoraService,
                                          UserManager userManager,
                                          IPessoaService pessoaService,
                                          IEmpresaService empresaService,
                                          IAdministradoraService administradoraService
                                          )
            : base(corretorRepository)
        {
            _corretorService = corretorService;
            _pessoaFisicaService = pessoaFisicaService;
            _corretoraService = corretoraService;
            _userManager = userManager;
            _pessoaService = pessoaService;
            _empresaService = empresaService;
            _administradoraService = administradoraService;
        }

        private void ValidateInput(CorretorInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        //private async Task<PagedResultDto<CorretorListDto>> UsersPaginado(PagedResultDto<CorretorListDto> result)
        //{
        //    var empresaLogado = await _empresaService.GetEmpresaLogada();

        //    string[] ArrayStr = new string[empresaLogado.Usuarios.Count];
        //    var i = 0;
        //    foreach (var item in empresaLogado.Usuarios)
        //    {
        //        ArrayStr[i] = item.EmailAddress;
        //        i++;
        //    }

        //    var retorno1 = result.Items.Where(cp => cp.Pessoa.Email == "diretoria.megavita@gmail.com").ToList();


        //    var idsPessoaFisica = _corretorService.GetAll().Where(x => x.Pessoa.NomePessoa.Contains(input.Nome));

        //    var queryPessoaFisica = _pessoaFisicaService
        //    .GetAll()
        //    .Where(x => !idsPessoaFisica.Contains(x.Id));

        //    var count = await queryPessoaFisica.CountAsync();
        //    var dados = await queryPessoaFisica
        //    .OrderBy(input.Sorting)
        //    .PageBy(input)
        //    .ToListAsync();
        //    var listDtos = dados.MapTo<List<PessoaListDto>>();
        //    return new PagedResultDto<PessoaListDto>(count, listDtos);


        //    PagedResultDto<CorretorListDto> retorno = new PagedResultDto<CorretorListDto>();

        //    return retorno;

        //}

        private async Task ProcessInput(Corretor corretor, CorretorInput corretorInput)
        {
            if (corretorInput.Pessoa.Id > 0)
            {
                try
                {
                    // corretorInput.Pessoa.GrupoPessoaId = null;
                    var pessoa = await _pessoaService.GetById(corretorInput.Pessoa.Id);

                    if (pessoa != null)
                    {
                        corretor.Pessoa = pessoa;
                        corretor.PessoaId = corretorInput.Pessoa.Id;
                    }

                    //var pessoaFisica = await _pessoaFisicaService.GetById(corretorInput.Pessoa.Id);
                    //if (pessoaFisica != null)
                    //{
                    //    corretor.Pessoa = pessoa;
                    //    corretor.Pessoa = pessoaFisica;
                    //    corretor.PessoaId = corretorInput.Pessoa.Id;
                    //}
                    else
                    {
                        throw new UserFriendlyException(L("Corretora.PessoaNotFoundError"));
                    }

                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            var validationErrors = new List<ValidationResult>();

            if (corretor.IsPessoaFisica)
            {
                var errors = corretor.IsValidCorretor();
                if (errors.Any())
                {
                    foreach (var valid in errors)
                    {
                        validationErrors.Add(new ValidationResult(L(valid)));
                    }
                }
            }


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }

            corretor.Corretoras.Clear();

            if (corretorInput.Corretoras != null && corretorInput.Corretoras.Any())
            {
                foreach (var item in corretorInput.Corretoras)
                {
                    var corretora = await _corretoraService.GetById(item.Id);
                    corretor.Corretoras.Add(corretora);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretor_Create, AppPermissions.Pages_Tenant_EZLiv_Corretor_Edit)]
        public async Task<IdInput> Save(CorretorInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                try
                {
                    result = await base.CreateEntity<IdInput, CorretorInput>(_corretorService, input, () => ValidateInput(input), ProcessInput);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                try
                {
                    result = await base.UpdateEntity<IdInput, CorretorInput>(_corretorService, input, () => ValidateInput(input), ProcessInput);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretor_Create, AppPermissions.Pages_Tenant_EZLiv_Corretor_Edit)]
        public async Task<CorretorInput> SaveAndReturnEntity(CorretorInput input)
        {
            CorretorInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<CorretorInput, CorretorInput>(_corretorService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<CorretorInput, CorretorInput>(_corretorService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<CorretorListDto>> GetCorretoresPaginado(GetCorretorInput input)
        {
            var empresaLogado = await _empresaService.GetEmpresaLogada();
            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());

            var queryPessoaFisica = _corretorService.GetAll();
            var dados = await queryPessoaFisica.OrderBy(input.Sorting).ToListAsync();


            var resultado = new List<User>();

            for (int u = 0; u < dados.Count; u++)
            {
                var item = await _userManager.GetByPessoaId(dados[u].PessoaId);

                if (item != null)
                    resultado.Add(item);
            }

            var FiltrandoCorretor = resultado.Where(x => x.TipoDeUsuario == Domain.Core.Enums.TipoDeUsuarioEnum.Corretor);

            var FiltrandoEmpresa = new List<User>();

            if (user.UserName == "admin" || user.UserName == "mgamorim")
            {
                FiltrandoEmpresa = FiltrandoCorretor.ToList();// FiltrandoCorretor.Where(x => x.Empresas.First().Id == empresaLogado.Id).ToList();
            }
            else
            {
                FiltrandoEmpresa = FiltrandoCorretor.Where(x => x.Empresas.First().Id == user.Empresas.First().Id).ToList();
            }


            List<Corretor> CorretorEmpresa = new List<Corretor>();

            foreach (var item in FiltrandoEmpresa)
            {
                var CorretorFiltro = await _corretorService.GetAll().Where(x => x.PessoaId == item.PessoaId).ToListAsync();
                CorretorEmpresa.Add(CorretorFiltro == null ? null : CorretorFiltro[0]);
            }

            dados = CorretorEmpresa;

            List<Corretor> lst = new List<Corretor>();

            lst = dados;

            if (input.ExceptIds != null)
            {
                if (input.ExceptIds.Count() > 0)
                {
                    List<dynamic> lstCorretor = new List<dynamic>();
                    foreach (int item in input.ExceptIds)
                    {
                        lst = lst.Where(x => x.Id != item).ToList();
                    }
                }
            }

                var count = lst.Count();
                var listDtos = lst.MapTo<List<CorretorListDto>>();
                return new PagedResultDto<CorretorListDto>(count, listDtos);
          
        }

        public async Task<CorretorInput> GetById(IdInput input)
        {
            return await base.GetEntityById<CorretorInput, IdInput>(_corretorService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretor_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_corretorService, input);
        }

        public async Task<CorretorInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<CorretorInput, IdInput>(_corretorService, input);
        }

        public async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForCorretor(GetPessoaExceptForCorretor input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var idsPessoaFisica = _corretorService.GetAll().Select(x => x.Pessoa.Id);

                var queryPessoaFisica = _pessoaFisicaService
                .GetAll()
                .Where(x => !idsPessoaFisica.Contains(x.Id));

                var count = await queryPessoaFisica.CountAsync();
                var dados = await queryPessoaFisica
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public virtual async Task<PagedResultDto<CorretoraListDto>> GetCorretoraExceptForCorretor(GetCorretoraExceptForCorretor input)
        {
            try
            {
                if (input == null)
                    throw new UserFriendlyException(L("Corretor.FiltroCorretorNotFoundError"));

                var idsCorretor = input.Corretoras.Select(x => x.Id);

                var queryCorretora = _corretoraService
                    .GetAll()
                    .Where(x => !idsCorretor.Contains(x.Id));


                var count = await queryCorretora.CountAsync();
                var dados = await queryCorretora
               .OrderBy(input.Sorting)
               .PageBy(input)
               .ToListAsync();
                var listDtos = dados.MapTo<List<CorretoraListDto>>();


                //IQueryable<CorretoraListDto> query = queryCorretora;

                //var count = await query.CountAsync();

                //List<CorretoraListDto> dados = await query
                //    .OrderBy(input.Sorting)
                //    .PageBy(input)
                //    .ToListAsync();

                //var listDtos = dados.MapTo<List<CorretoraListDto>>();
                return new PagedResultDto<CorretoraListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
