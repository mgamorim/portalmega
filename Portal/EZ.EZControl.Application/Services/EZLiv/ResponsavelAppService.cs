﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Responsavel, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ResponsavelAppService : EZControlAppServiceBase<Responsavel>, IResponsavelAppService
    {
        private readonly IResponsavelService _responsavelService;

        public ResponsavelAppService(IRepository<Responsavel> _responsavelRepository,
                                          IResponsavelService _responsavelService)
            : base(_responsavelRepository)
        {
            this._responsavelService = _responsavelService;
        }

        private void ValidateInput(ResponsavelInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Responsavel _responsavel, ResponsavelInput _responsavelInput)
        {
            var validationErrors = new List<ValidationResult>();

            var errors = _responsavel.IsValidResponsavel();

            if (errors.Any())
            {
                foreach (string erro in errors)
                {
                    validationErrors.Add(new ValidationResult(L(erro)));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Create, AppPermissions.Pages_Tenant_EZLiv_Responsavel_Edit)]
        public async Task<IdInput> Save(ResponsavelInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ResponsavelInput>(_responsavelService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ResponsavelInput>(_responsavelService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Create, AppPermissions.Pages_Tenant_EZLiv_Responsavel_Edit)]
        public async Task<ResponsavelInput> SaveAndReturnEntity(ResponsavelInput input)
        {
            ResponsavelInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ResponsavelInput, ResponsavelInput>(_responsavelService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ResponsavelInput, ResponsavelInput>(_responsavelService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ResponsavelListDto>> GetResponsaveisPaginado(GetResponsavelInput input)
        {
            var condicoes = new List<WhereIfCondition<Responsavel>>
            {
                new WhereIfCondition<Responsavel>(!input.Nome.IsNullOrEmpty(), r => r.PessoaFisica.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<ResponsavelListDto, GetResponsavelInput>(_responsavelService, input, condicoes);
            return result;
        }

        public async Task<ResponsavelInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ResponsavelInput, IdInput>(_responsavelService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Responsavel_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_responsavelService, input);
        }

        public async Task<ResponsavelInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ResponsavelInput, IdInput>(_responsavelService, input);
        }
    }
}
