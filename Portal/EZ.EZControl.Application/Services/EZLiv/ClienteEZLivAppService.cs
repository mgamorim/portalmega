﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ClienteEZLiv;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ClienteEZLivAppService : EZControlAppServiceBase<ClienteEZLiv>, IClienteEZLivAppService
    {
        private readonly IClienteEZLivService _clienteEZLivService;
        private readonly IClienteService _clienteService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;

        public ClienteEZLivAppService(IRepository<ClienteEZLiv> clienteEZLivRepository,
            IClienteEZLivService clienteEZLivService,
            IClienteService clienteService)
            : base(clienteEZLivRepository)
        {
            this._clienteEZLivService = clienteEZLivService;
            this._clienteService = clienteService;
        }

        private void ValidateInput(ClienteEZLivInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ClienteId == default(int))
                validationErrors.Add(new ValidationResult(L("ClienteEZLiv.EmptyCliente")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ClienteEZLiv clienteEZLiv, ClienteEZLivInput input)
        {
            #region Cliente
            if (input.ClienteId > 0)
            {
                Cliente cliente = null;

                try
                {
                    cliente = await _clienteService.GetById(input.ClienteId);

                    clienteEZLiv.AssociarCliente(_clienteEZLivService, cliente);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion                          
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Create, AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Edit)]
        public async Task<IdInput> Save(ClienteEZLivInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ClienteEZLivInput>(_clienteEZLivService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ClienteEZLivInput>(_clienteEZLivService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Create, AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Edit)]
        public async Task<ClienteEZLivInput> SaveAndReturnEntity(ClienteEZLivInput input)
        {
            ClienteEZLivInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ClienteEZLivInput, ClienteEZLivInput>(_clienteEZLivService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ClienteEZLivInput, ClienteEZLivInput>(_clienteEZLivService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ClienteEZLivListDto>> GetPaginado(GetClienteEZLivInput input)
        {
            var condicoes = new List<WhereIfCondition<ClienteEZLiv>>
            {
                new WhereIfCondition<ClienteEZLiv>(input.ClienteId > 0, r => r.ClienteId == input.ClienteId),
                new WhereIfCondition<ClienteEZLiv>(!string.IsNullOrEmpty(input.NomeCliente), r => r.Cliente.Pessoa.NomePessoa.Contains(input.NomeCliente))
            };
            var result = await base.GetListPaged<ClienteEZLivListDto, GetClienteEZLivInput>(_clienteEZLivService, input, condicoes);
            return result;
        }

        public async Task<ClienteEZLivInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ClienteEZLivInput, IdInput>(_clienteEZLivService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ClienteEZLiv_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_clienteEZLivService, input);
        }

        public async Task<ClienteEZLivInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ClienteEZLivInput, IdInput>(_clienteEZLivService, input);
        }

        public virtual async Task<PagedResultDto<ClienteListDto>> GetPessoaExceptForClienteEZLiv(GetClienteEZLivInput input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var idsClientes = _clienteEZLivService.GetAll().Select(y => y.ClienteId);

                var queryCliente = _clienteService
                    .GetAll()
                    .Where(x => !idsClientes.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.NomeCliente), x => x.NomePessoa.Contains(input.NomeCliente))
                    .Select(x => x);

                IQueryable<Cliente> query = queryCliente;

                var count = await query.CountAsync();

                List<Cliente> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<ClienteListDto>>();
                return new PagedResultDto<ClienteListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
