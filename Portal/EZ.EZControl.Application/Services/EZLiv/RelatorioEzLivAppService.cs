﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Serialization.Formatters;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Relatorio, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RelatorioEzLivAppService : EZControlAppServiceBase<Relatorio>, IRelatorioEzLivAppService
    {
        private readonly IRelatorioService _RelatorioService;
        public RelatorioEzLivAppService(IRepository<Relatorio> RelatorioRepository,
                                  IRelatorioService RelatorioService)
            : base(RelatorioRepository)
        {
            this._RelatorioService = RelatorioService;
        }

        private void ValidateInput(RelatorioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            var jsonSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Objects,
                TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
            };

            if (!string.IsNullOrWhiteSpace(input.Conteudo))
            {
                var conteudo = JsonConvert.DeserializeObject<object>(input.Conteudo, jsonSettings);
                if (conteudo == null)
                {
                    validationErrors.Add(new ValidationResult("O Conteúdo deve ser no formato JSON."));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Relatorio relatorio, RelatorioInput relatorioInput)
        {
            relatorio.Sistema = SistemaEnum.EZLiv;

            var validationErrors = new List<ValidationResult>();

            // O automapper não está funcionando para string's muito grandes
            if (!string.IsNullOrWhiteSpace(relatorioInput.Conteudo))
            {
                relatorio.Conteudo = relatorioInput.Conteudo;
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Create, AppPermissions.Pages_Tenant_EZLiv_Relatorio_Edit)]
        public async Task<IdInput> Save(RelatorioInput input)
        {
            try
            {


                IdInput result = null;

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, RelatorioInput>(_RelatorioService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, RelatorioInput>(_RelatorioService, input, () => ValidateInput(input), ProcessInput);
                }


                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Create, AppPermissions.Pages_Tenant_EZLiv_Relatorio_Edit)]
        public async Task<RelatorioInput> SaveAndReturnEntity(RelatorioInput input)
        {
            RelatorioInput Relatorio = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                Relatorio = await base.CreateAndReturnEntity<RelatorioInput, RelatorioInput>(_RelatorioService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                Relatorio = await base.UpdateAndReturnEntity<RelatorioInput, RelatorioInput>(_RelatorioService, input, () => ValidateInput(input));
            }

            return Relatorio;
        }

        public async Task<PagedResultDto<RelatorioListDto>> GetPaginado(GetRelatorioInput input)
        {
            if (!Enum.IsDefined(typeof(SistemaEnum), input.Sistema))
            {
                throw new UserFriendlyException(L("Relatorio.EmptySistemaError"));
            }
            var condicoes = new List<WhereIfCondition<Relatorio>>
            {
                new WhereIfCondition<Relatorio>(!input.Nome.IsNullOrEmpty(), r => r.Nome.Contains(input.Nome)),
                new WhereIfCondition<Relatorio>(true, r => r.Sistema == input.Sistema)
            };

            if (input.TipoDeRelatorio > 0)
            {
                condicoes.Add(new WhereIfCondition<Relatorio>(true, r => r.TipoDeRelatorio == input.TipoDeRelatorio));
            }

            var result = await base.GetListPaged<RelatorioListDto, GetRelatorioInput>(_RelatorioService, input, condicoes);
            return result;
        }

        public async Task<RelatorioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RelatorioInput, IdInput>(_RelatorioService, input);
        }

        public async Task<RelatorioInput> GetByIdParaVisualizadorDeRelatorio(IdInput input)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var entity = await Repository.GetAsync(input.Id);
                return entity.MapTo<RelatorioInput>();
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_RelatorioService, input);
        }

        public async Task<RelatorioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RelatorioInput, IdInput>(_RelatorioService, input);
        }

        public async Task<PagedResultDto<RelatorioListDto>> GetRelatorioExceptForIdInput(GetRelatorioExceptForIdInput input)
        {
            try
            {
                var idsRelatorio = input.Relatorios.Select(x => x.Id);

                var queryRelatorio = _RelatorioService
                    .GetAll()
                    .Where(x => !idsRelatorio.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), a => a.Nome.Contains(input.Nome));

                if (input.TipoDeRelatorio > 0)
                {
                    queryRelatorio = queryRelatorio.Where(x => x.TipoDeRelatorio == input.TipoDeRelatorio);
                }

                var count = await queryRelatorio.CountAsync();

                List<Relatorio> dados = await queryRelatorio
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<RelatorioListDto>>();
                return new PagedResultDto<RelatorioListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Relatorio_Create)]
        public async Task<RelatorioInput> Duplicar(IdInput input)
        {
            var relatorio = await _RelatorioService.GetById(input.Id);

            if (relatorio == null)
            {
                throw new UserFriendlyException("Não foi possível encontrar o relatório a ser copiado.");
            }

            var relatorioCopia = relatorio.MapTo<RelatorioInput>();
            relatorioCopia.Id = 0;
            relatorioCopia.Nome = string.Concat(relatorioCopia.Nome, " - Cópia");

            var result = await SaveAndReturnEntity(relatorioCopia);

            return result;
        }
    }
}