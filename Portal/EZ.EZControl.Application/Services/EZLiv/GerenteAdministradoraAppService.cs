﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteAdministradora;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class GerenteAdministradoraAppService : EZControlAppServiceBase<GerenteAdministradora>, IGerenteAdministradoraAppService
    {
        private readonly ICorretorService _corretorService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IAdministradoraService _administradoraService;
        private readonly IPessoaService _pessoaService;
        private readonly IGerenteAdministradoraService _gerenteService;
        private readonly IEmpresaService _empresaService;
        public GerenteAdministradoraAppService(IRepository<GerenteAdministradora> corretorRepository,
                                          ICorretorService corretorService,
                                          IPessoaFisicaService pessoaFisicaService,
                                          IAdministradoraService administradoraService,
                                          IPessoaService pessoaService,
                                          IGerenteAdministradoraService gerenteService, 
                                          IEmpresaService empresaService
                                          )
            : base(corretorRepository)
        {
            _corretorService = corretorService;
            _pessoaFisicaService = pessoaFisicaService;
            _administradoraService = administradoraService;
            _pessoaService = pessoaService;
            _gerenteService = gerenteService;
            _empresaService = empresaService;
        }
        private void ValidateInput(GerenteAdministradoraInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (input.AdministradoraId == 0)
            {
                validationErrors.Add(new ValidationResult("Administradora não informada"));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        private async Task ProcessInput(GerenteAdministradora entity, GerenteAdministradoraInput input)
        {
            if (input.Pessoa.Id > 0)
            {
                try
                {
                    var pessoa = await _pessoaService.GetById(input.Pessoa.Id);
                    if (pessoa != null)
                    {
                        entity.Pessoa = pessoa;
                        entity.PessoaId = input.Pessoa.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Administradora.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            if (input.AdministradoraId > 0)
            {
                var corretora = await _administradoraService.GetById(input.AdministradoraId);
                if (corretora != null)
                {
                    entity.AdministradoraId = input.AdministradoraId;
                    entity.Administradora = corretora;
                }
                else
                {
                    throw new UserFriendlyException(L("Administradora.NotFoundError"));
                }
            }
            var validationErrors = new List<ValidationResult>();
            if (entity.IsPessoaFisica)
            {
                var errors = entity.IsValidGerenteAdministradora();
                if (errors.Any())
                {
                    foreach (var valid in errors)
                    {
                        validationErrors.Add(new ValidationResult(L(valid)));
                    }
                }
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Create, AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Edit)]
        public async Task<IdInput> Save(GerenteAdministradoraInput input)
        {
            IdInput result = null;
            var gerenteAdministradora = await _gerenteService.GetAll().Where(x => x.AdministradoraId == input.AdministradoraId && x.PessoaId == input.Pessoa.Id).FirstOrDefaultAsync();
            if (gerenteAdministradora == null)
            {
                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == input.Pessoa.Id).FirstOrDefaultAsync();
                var pessoa = await _pessoaService.GetById(input.Pessoa.Id);

                gerenteAdministradora = new GerenteAdministradora() {
                    AdministradoraId = input.AdministradoraId,
                    PessoaId = input.Pessoa.Id,
                    Pessoa = pessoa,
                    IsActive = true,
                    TenantId = 1
                };

                var id = await Repository.InsertAndGetIdAsync(gerenteAdministradora);

                //TODO não precisa ser um corretor, colocar coo pessoa física
                //var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
                //_gerenteService.AddGerenteInRole(corretor, empresa);

                result = new IdInput(id);
            }else
            {
                throw new UserFriendlyException(L("GerenteAdministradora.PessoaGerenteExistenteError"));
            }
            return result;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Create, AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Edit)]
        public async Task<GerenteAdministradoraInput> SaveAndReturnEntity(GerenteAdministradoraInput input)
        {
            GerenteAdministradoraInput result = null;
            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<GerenteAdministradoraInput, GerenteAdministradoraInput>(_gerenteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<GerenteAdministradoraInput, GerenteAdministradoraInput>(_gerenteService, input, () => ValidateInput(input), ProcessInput);
            }
            return result;
        }
        public async Task<PagedResultDto<GerenteAdministradoraListDto>> GetGerenteAdministradorasPaginado(GetGerenteAdministradoraInput input)
        {
            var condicoes = new List<WhereIfCondition<GerenteAdministradora>>
            {
                new WhereIfCondition<GerenteAdministradora>(!input.Nome.IsNullOrEmpty(), r => r.NomePessoa.Contains(input.Nome))
            };
            var result = await base.GetListPaged<GerenteAdministradoraListDto, GetGerenteAdministradoraInput>(_gerenteService, input, condicoes);
            return result;
        }
        public async Task<GerenteAdministradoraInput> GetById(IdInput input)
        {
            return await base.GetEntityById<GerenteAdministradoraInput, IdInput>(_gerenteService, input);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteAdministradora_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_gerenteService, input);
        }
        public async Task<GerenteAdministradoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<GerenteAdministradoraInput, IdInput>(_gerenteService, input);
        }
    }
}