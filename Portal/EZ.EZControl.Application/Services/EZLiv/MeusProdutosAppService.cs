﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.MeusProdutos;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Data.Entity;
using Abp.Collections.Extensions;
using Abp.UI;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Filters;
using EZ.EZControl.Common.Extensions;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class MeusProdutosAppService : EZControlAppServiceBase<ProdutoDePlanoDeSaude>, IMeusProdutosAppService
    {
        private readonly IPropostaDeContratacaoService _propostaService;
        private readonly IBeneficiarioBaseService _beneficiarioService;
        public MeusProdutosAppService(IRepository<ProdutoDePlanoDeSaude> produtoDePlanoDeSaudeRepository,
            IPropostaDeContratacaoService propostaService,
            IBeneficiarioBaseService beneficiarioService)
            : base(produtoDePlanoDeSaudeRepository)
        {
            _propostaService = propostaService;
            _beneficiarioService = beneficiarioService;
        }

        public async Task<PagedResultDto<MeusProdutosListDto>> Get(GetMeusProdutosInput input)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var pessoa = user.Pessoa;
                var beneficiario = await _beneficiarioService.GetAll().Where(x => x.PessoaFisica.Id == pessoa.Id).FirstOrDefaultAsync();
                var proposta = _propostaService
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .WhereIf(true, x => (x.TitularId == beneficiario.Id || (x.ResponsavelId.HasValue && x.ResponsavelId == beneficiario.Id)) && x.Produto != null)
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), x => x.Produto.Nome.Contains(input.Nome));
                var count = proposta.Count();
                var dados = proposta.OrderBy(x => x.Produto.Nome);
                var listDtos = dados.Select(x => new MeusProdutosListDto()
                {
                    StatusProposta = x.StatusDaProposta.GetDescription(),
                    Abrangencia = x.Produto != null ? x.Produto.PlanoDeSaude.Abrangencia.GetDescription() : string.Empty,
                    Administradora = x.Produto != null ? x.Produto.Administradora.NomePessoa : string.Empty,
                    Id = x.Produto != null ? x.Produto.Id : 0,
                    Nome = x.Produto != null ? x.Produto.Nome : string.Empty,
                    Reembolso = x.Produto != null ? x.Produto.PlanoDeSaude.Reembolso : false,
                    Valor = x.Pedido != null ? x.Pedido.ItensDePedidoReadOnly.Sum(p => p.Valor) : 0,
                    PropostaId = x.Id
                }).ToList();
                return new PagedResultDto<MeusProdutosListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
        public async Task<MeusProdutosInput> GetById(IdInput input)
        {
            return null;
        }
    }
}