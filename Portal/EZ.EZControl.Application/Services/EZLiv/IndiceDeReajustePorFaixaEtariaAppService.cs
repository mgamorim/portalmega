﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class IndiceDeReajustePorFaixaEtariaAppService : EZControlAppServiceBase<IndiceDeReajustePorFaixaEtaria>, IIndiceDeReajustePorFaixaEtariaAppService
    {
        private readonly IIndiceDeReajustePorFaixaEtariaService _indiceDeReajustePorFaixaEtariaService;
        private readonly IOperadoraService _operadoraService;
        private readonly IFaixaEtariaService _faixaEtariaService;

        public IndiceDeReajustePorFaixaEtariaAppService(IRepository<IndiceDeReajustePorFaixaEtaria> IndiceDeReajustePorFaixaEtariaRepository,
                                             IIndiceDeReajustePorFaixaEtariaService indiceDeReajustePorFaixaEtariaService,
                                             IOperadoraService operadoraService,
                                             IFaixaEtariaService faixaEtariaService)
            : base(IndiceDeReajustePorFaixaEtariaRepository)
        {
            this._indiceDeReajustePorFaixaEtariaService = indiceDeReajustePorFaixaEtariaService;
            this._operadoraService = operadoraService;
            this._faixaEtariaService = faixaEtariaService;
        }

        private void ValidateInput(IndiceDeReajustePorFaixaEtariaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.FaixaEtariaId == 0)
                validationErrors.Add(new ValidationResult(L("IndiceDeReajustePorFaixaEtaria.RequiredFaixaEtaria")));

            if (input.OperadoraId == 0)
                validationErrors.Add(new ValidationResult(L("IndiceDeReajustePorFaixaEtaria.RequiredOperadora")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(IndiceDeReajustePorFaixaEtaria indiceDeReajustePorFaixaEtaria, IndiceDeReajustePorFaixaEtariaInput indiceDeReajustePorFaixaEtariaInput)
        {
            if (indiceDeReajustePorFaixaEtaria.OperadoraId > 0)
            {
                try
                {
                    indiceDeReajustePorFaixaEtaria.Operadora = await _operadoraService.GetById(indiceDeReajustePorFaixaEtaria.OperadoraId);

                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("IndiceDeReajustePorFaixaEtaria.NotFoundOperadoraError"));
                }
            }

            var existeFaixaCadastrada = await _indiceDeReajustePorFaixaEtariaService.FirstOrDefault(x => x.Id == 0 && x.OperadoraId == indiceDeReajustePorFaixaEtariaInput.OperadoraId && x.FaixaEtariaId == indiceDeReajustePorFaixaEtariaInput.FaixaEtariaId);

            if (existeFaixaCadastrada != null)
                throw new UserFriendlyException(L("IndiceDeReajustePorFaixaEtaria.FaixaEtariaExistente"));

            if (indiceDeReajustePorFaixaEtaria.FaixaEtariaId > 0)
            {
                try
                {
                    indiceDeReajustePorFaixaEtaria.FaixaEtaria = await _faixaEtariaService.GetById(indiceDeReajustePorFaixaEtaria.FaixaEtariaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("IndiceDeReajustePorFaixaEtaria.NotFoundFaixaEtariaError"));
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Create, AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Edit)]
        public async Task<IdInput> Save(IndiceDeReajustePorFaixaEtariaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, IndiceDeReajustePorFaixaEtariaInput>(_indiceDeReajustePorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, IndiceDeReajustePorFaixaEtariaInput>(_indiceDeReajustePorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Create, AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Edit)]
        public async Task<IndiceDeReajustePorFaixaEtariaInput> SaveAndReturnEntity(IndiceDeReajustePorFaixaEtariaInput input)
        {
            IndiceDeReajustePorFaixaEtariaInput IndiceDeReajustePorFaixaEtaria = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                IndiceDeReajustePorFaixaEtaria = await base.CreateAndReturnEntity<IndiceDeReajustePorFaixaEtariaInput, IndiceDeReajustePorFaixaEtariaInput>(_indiceDeReajustePorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                IndiceDeReajustePorFaixaEtaria = await base.UpdateAndReturnEntity<IndiceDeReajustePorFaixaEtariaInput, IndiceDeReajustePorFaixaEtariaInput>(_indiceDeReajustePorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }

            return IndiceDeReajustePorFaixaEtaria;
        }

        public async Task<PagedResultDto<IndiceDeReajustePorFaixaEtariaListDto>> GetPaginado(GetIndiceDeReajustePorFaixaEtariaInput input)
        {
            var condicoes = new List<WhereIfCondition<IndiceDeReajustePorFaixaEtaria>>();

            condicoes.Add(new WhereIfCondition<IndiceDeReajustePorFaixaEtaria>(input.IndiceDeReajuste > 0, r => r.IndiceDeReajuste == input.IndiceDeReajuste));

            var result = await base.GetListPaged<IndiceDeReajustePorFaixaEtariaListDto, GetIndiceDeReajustePorFaixaEtariaInput>(_indiceDeReajustePorFaixaEtariaService, input, condicoes);
            return result;
        }

        public async Task<IndiceDeReajustePorFaixaEtariaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<IndiceDeReajustePorFaixaEtariaInput, IdInput>(_indiceDeReajustePorFaixaEtariaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_IndiceDeReajustePorFaixaEtaria_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_indiceDeReajustePorFaixaEtariaService, input);
        }

        public async Task<IndiceDeReajustePorFaixaEtariaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<IndiceDeReajustePorFaixaEtariaInput, IdInput>(_indiceDeReajustePorFaixaEtariaService, input);
        }
    }
}