﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EspecialidadePorProdutoDePlanoDeSaudeAppService : EZControlAppServiceBase<EspecialidadePorProdutoDePlanoDeSaude>, IEspecialidadePorProdutoDePlanoDeSaudeAppService
    {
        private readonly IEspecialidadePorProdutoDePlanoDeSaudeService _especialidadePorProdutoDePlanoDeSaudeService;
        private readonly IProdutoDePlanoDeSaudeService _contratoService;
        private readonly IEspecialidadeService _especialidadeService;

        public EspecialidadePorProdutoDePlanoDeSaudeAppService(IRepository<EspecialidadePorProdutoDePlanoDeSaude> especialidadePorProdutoDePlanoDeSaudeRepository,
                                             IEspecialidadePorProdutoDePlanoDeSaudeService especialidadePorProdutoDePlanoDeSaudeService,
                                             IProdutoDePlanoDeSaudeService contratoService,
                                             IEspecialidadeService especialidadeService)
            : base(especialidadePorProdutoDePlanoDeSaudeRepository)
        {
            this._especialidadePorProdutoDePlanoDeSaudeService = especialidadePorProdutoDePlanoDeSaudeService;
            this._contratoService = contratoService;
            this._especialidadeService = especialidadeService;
        }

        private void ValidateInput(EspecialidadePorProdutoDePlanoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ProdutoDePlanoDeSaudeId == 0)
                validationErrors.Add(new ValidationResult(L("EspecialidadePorProdutoDePlanoDeSaude.RequiredProdutoDePlanoDeSaude")));

            if (input.EspecialidadeId == 0)
                validationErrors.Add(new ValidationResult(L("EspecialidadePorProdutoDePlanoDeSaude.RequiredEspecialidade")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(EspecialidadePorProdutoDePlanoDeSaude especialidadePorProdutoDePlanoDeSaude, EspecialidadePorProdutoDePlanoDeSaudeInput especialidadePorProdutoDePlanoDeSaudeInput)
        {
            if (especialidadePorProdutoDePlanoDeSaude.ProdutoDePlanoDeSaudeId > 0)
            {
                try
                {
                    especialidadePorProdutoDePlanoDeSaude.ProdutoDePlanoDeSaude = await _contratoService.GetById(especialidadePorProdutoDePlanoDeSaude.ProdutoDePlanoDeSaudeId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("EspecialidadePorProdutoDePlanoDeSaude.NotFoundProdutoDePlanoDeSaudeError"));
                }
            }

            if (especialidadePorProdutoDePlanoDeSaude.EspecialidadeId > 0)
            {
                try
                {
                    especialidadePorProdutoDePlanoDeSaude.Especialidade = await _especialidadeService.GetById(especialidadePorProdutoDePlanoDeSaude.EspecialidadeId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("EspecialidadePorProdutoDePlanoDeSaude.NotFoundEspecialidadeError"));
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Edit)]
        public async Task<IdInput> Save(EspecialidadePorProdutoDePlanoDeSaudeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, EspecialidadePorProdutoDePlanoDeSaudeInput>(_especialidadePorProdutoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, EspecialidadePorProdutoDePlanoDeSaudeInput>(_especialidadePorProdutoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Edit)]
        public async Task<EspecialidadePorProdutoDePlanoDeSaudeInput> SaveAndReturnEntity(EspecialidadePorProdutoDePlanoDeSaudeInput input)
        {
            EspecialidadePorProdutoDePlanoDeSaudeInput especialidadePorProdutoDePlanoDeSaude = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                especialidadePorProdutoDePlanoDeSaude = await base.CreateAndReturnEntity<EspecialidadePorProdutoDePlanoDeSaudeInput, EspecialidadePorProdutoDePlanoDeSaudeInput>(_especialidadePorProdutoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                especialidadePorProdutoDePlanoDeSaude = await base.UpdateAndReturnEntity<EspecialidadePorProdutoDePlanoDeSaudeInput, EspecialidadePorProdutoDePlanoDeSaudeInput>(_especialidadePorProdutoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return especialidadePorProdutoDePlanoDeSaude;
        }

        public async Task<PagedResultDto<EspecialidadePorProdutoDePlanoDeSaudeListDto>> GetPaginadoByProdutoDePlanoDeSaudeId(GetEspecialidadePorProdutoDePlanoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<EspecialidadePorProdutoDePlanoDeSaude>>();

            if (input.ProdutoDePlanoDeSaudeId > 0)
                condicoes.Add(new WhereIfCondition<EspecialidadePorProdutoDePlanoDeSaude>(true, r => r.ProdutoDePlanoDeSaudeId == input.ProdutoDePlanoDeSaudeId));
            else
                return null;

            var result = await base.GetListPaged<EspecialidadePorProdutoDePlanoDeSaudeListDto, GetEspecialidadePorProdutoDePlanoDeSaudeInput>(_especialidadePorProdutoDePlanoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<PagedResultDto<EspecialidadePorProdutoDePlanoDeSaudeListDto>> GetPaginado(GetEspecialidadePorProdutoDePlanoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<EspecialidadePorProdutoDePlanoDeSaude>>();

            if (input.ProdutoDePlanoDeSaudeId > 0)
                condicoes.Add(new WhereIfCondition<EspecialidadePorProdutoDePlanoDeSaude>(true, r => r.ProdutoDePlanoDeSaudeId == input.ProdutoDePlanoDeSaudeId));

            var result = await base.GetListPaged<EspecialidadePorProdutoDePlanoDeSaudeListDto, GetEspecialidadePorProdutoDePlanoDeSaudeInput>(_especialidadePorProdutoDePlanoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<EspecialidadePorProdutoDePlanoDeSaudeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EspecialidadePorProdutoDePlanoDeSaudeInput, IdInput>(_especialidadePorProdutoDePlanoDeSaudeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadePorProdutoDePlanoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_especialidadePorProdutoDePlanoDeSaudeService, input);
        }

        public async Task<EspecialidadePorProdutoDePlanoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EspecialidadePorProdutoDePlanoDeSaudeInput, IdInput>(_especialidadePorProdutoDePlanoDeSaudeService, input);
        }
    }
}