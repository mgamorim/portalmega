﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Hospital, AppPermissions.Pages_Tenant_EZLiv_Laboratorio, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class UnidadeDeSaudeAppService : EZControlAppServiceBase<UnidadeDeSaudeBase>, IUnidadeDeSaudeAppService
    {
        private readonly IUnidadeDeSaudeBaseService _unidadeDeSaudeBaseService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IEnderecoService _enderecoService;
        private readonly ITelefoneService _telefoneService;
        private readonly IEspecialidadeService _especialidadeService;

        public UnidadeDeSaudeAppService(IRepository<UnidadeDeSaudeBase> UnidadeDeSaudeBaseRepository,
            IUnidadeDeSaudeBaseService unidadeDeSaudeBaseService)
            : base(UnidadeDeSaudeBaseRepository)
        {
            _unidadeDeSaudeBaseService = unidadeDeSaudeBaseService;
        }

        public async Task<PagedResultDto<UnidadeDeSaudeBaseListDto>> GetPaginado(GetUnidadeDeSaudeBaseInput input)
        {
            var condicoes = new List<WhereIfCondition<UnidadeDeSaudeBase>>
            {
                new WhereIfCondition<UnidadeDeSaudeBase>(!input.Email.IsNullOrEmpty(), r => r.Email.Contains(input.Email))
            };
            return await base.GetListPaged<UnidadeDeSaudeBaseListDto, GetUnidadeDeSaudeBaseInput>(_unidadeDeSaudeBaseService, input, condicoes);
        }

        public async Task<UnidadeDeSaudeBaseInput> GetById(IdInput input)
        {
            return await base.GetEntityById<UnidadeDeSaudeBaseInput, IdInput>(_unidadeDeSaudeBaseService, input);
        }

        public async Task<UnidadeDeSaudeBaseInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<UnidadeDeSaudeBaseInput, IdInput>(_unidadeDeSaudeBaseService, input);
        }

        public async Task<PagedResultDto<UnidadeDeSaudeBaseListDto>> GetPaginadoExceptForIds(GetUnidadeDeSaudeBaseInput input)
        {
            var condicoes = new List<WhereIfCondition<Domain.EZLiv.Geral.UnidadeDeSaudeBase>>
            {
                new WhereIfCondition<Domain.EZLiv.Geral.UnidadeDeSaudeBase>(
                    input.Ids.Length > 0,
                    a => !input.Ids.Contains(a.Id))
            };

            var result =
                await
                    base.GetListPaged<UnidadeDeSaudeBaseListDto, GetUnidadeDeSaudeBaseInput>(_unidadeDeSaudeBaseService, input,
                        condicoes);
            return result;
        }
    }
}
