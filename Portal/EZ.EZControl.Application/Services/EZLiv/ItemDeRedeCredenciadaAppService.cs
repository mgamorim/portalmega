﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ItemDeRedeCredenciadaAppService : EZControlAppServiceBase<ItemDeRedeCredenciada>, IItemDeRedeCredenciadaAppService
    {
        private readonly IItemDeRedeCredenciadaService _itemDeRedeCredenciadaService;
        private readonly IRedeCredenciadaService _redeCredenciadaService;
        private readonly IUnidadeDeSaudeBaseService _unidadeDeSaudeBaseService;

        public ItemDeRedeCredenciadaAppService(IRepository<ItemDeRedeCredenciada> itemDeRedeCredenciadaRepository,
            IItemDeRedeCredenciadaService itemDeRedeCredenciadaService,
            IRedeCredenciadaService redeCredenciadaService,
            IUnidadeDeSaudeBaseService unidadeDeSaudeBaseService)
            : base(itemDeRedeCredenciadaRepository)
        {
            this._itemDeRedeCredenciadaService = itemDeRedeCredenciadaService;
            this._redeCredenciadaService = redeCredenciadaService;
            this._unidadeDeSaudeBaseService = unidadeDeSaudeBaseService;
        }

        private void ValidateInput(ItemDeRedeCredenciadaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.RedeCredenciadaId == default(int))
                validationErrors.Add(new ValidationResult(L("ItemDeRedeCredenciada.EmptyRedeCredenciada")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ItemDeRedeCredenciada itemDeRedeCredenciada, ItemDeRedeCredenciadaInput input)
        {
            #region UnidadeDeSaude
            if (input.UnidadeDeSaudeId > 0)
            {
                UnidadeDeSaudeBase unidadeDeSaude = null;

                try
                {
                    unidadeDeSaude = await _unidadeDeSaudeBaseService.GetById(input.UnidadeDeSaudeId);
                    itemDeRedeCredenciada.AssociarUnidadeDeSaude(_itemDeRedeCredenciadaService, unidadeDeSaude);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion

            #region RedeCredenciada
            if (input.RedeCredenciadaId > 0)
            {
                RedeCredenciada redeCredenciada = null;

                try
                {
                    redeCredenciada = await _redeCredenciadaService.GetById(input.RedeCredenciadaId);

                    itemDeRedeCredenciada.AssociarRedeCredenciada(_itemDeRedeCredenciadaService, redeCredenciada);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion                          
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Create, AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Edit)]
        public async Task<IdInput> Save(ItemDeRedeCredenciadaInput input)
        {
            IdInput result = null;

            try
            {

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ItemDeRedeCredenciadaInput>(_itemDeRedeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ItemDeRedeCredenciadaInput>(_itemDeRedeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Create, AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Edit)]
        public async Task<ItemDeRedeCredenciadaInput> SaveAndReturnEntity(ItemDeRedeCredenciadaInput input)
        {
            ItemDeRedeCredenciadaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ItemDeRedeCredenciadaInput, ItemDeRedeCredenciadaInput>(_itemDeRedeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ItemDeRedeCredenciadaInput, ItemDeRedeCredenciadaInput>(_itemDeRedeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ItemDeRedeCredenciadaListDto>> GetPaginado(GetItemDeRedeCredenciadaInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemDeRedeCredenciada>>
            {
                new WhereIfCondition<ItemDeRedeCredenciada>(input.RedeCredenciadaId > 0, r => r.RedeCredenciadaId == input.RedeCredenciadaId)
            };
            var result = await base.GetListPaged<ItemDeRedeCredenciadaListDto, GetItemDeRedeCredenciadaInput>(_itemDeRedeCredenciadaService, input, condicoes);
            return result;
        }

        public async Task<ItemDeRedeCredenciadaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ItemDeRedeCredenciadaInput, IdInput>(_itemDeRedeCredenciadaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeRedeCredenciada_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_itemDeRedeCredenciadaService, input);
        }

        public async Task<ItemDeRedeCredenciadaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ItemDeRedeCredenciadaInput, IdInput>(_itemDeRedeCredenciadaService, input);
        }

        public async Task<PagedResultDto<ItemDeRedeCredenciadaListDto>> GetPaginadoByRedeCredenciada(GetItemDeRedeCredenciadaInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemDeRedeCredenciada>>
            {
                new WhereIfCondition<ItemDeRedeCredenciada>(true, x => x.RedeCredenciada.Id == input.RedeCredenciadaId)
            };
            return await base.GetListPaged<ItemDeRedeCredenciadaListDto, GetItemDeRedeCredenciadaInput>(_itemDeRedeCredenciadaService, input, condicoes);
        }

        public async Task<IEnumerable<ItemDeRedeCredenciadaListDto>> GetItemsByRedeCredenciada(int redeCredenciadaId)
        {

            List<ItemDeRedeCredenciada> lista = await _itemDeRedeCredenciadaService.GetAllListAsync(x => x.RedeCredenciadaId == redeCredenciadaId);

            return lista.MapTo<List<ItemDeRedeCredenciadaListDto>>();
        }
    }
}