﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDeSaude;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class DeclaracaoDeSaudeAppService : EZControlAppServiceBase<DeclaracaoDeSaude>, IDeclaracaoDeSaudeAppService
    {
        private readonly IDeclaracaoDeSaudeService _declaracaoDeSaudeService;

        public DeclaracaoDeSaudeAppService(IRepository<DeclaracaoDeSaude> _declaracaoDeSaudeRepository,
                                          IDeclaracaoDeSaudeService _declaracaoDeSaudeService)
            : base(_declaracaoDeSaudeRepository)
        {
            this._declaracaoDeSaudeService = _declaracaoDeSaudeService;
        }

        private void ValidateInput(DeclaracaoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Dependentes != null &&
                input.Dependentes.Count > 4)
            {
                validationErrors.Add(new ValidationResult(L("DeclaracaoDeSaude.CountDependente")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(DeclaracaoDeSaude _declaracaoDeSaude, DeclaracaoDeSaudeInput _declaracaoDeSaudeInput)
        {
            if (_declaracaoDeSaude.ItensDeDeclaracaoDeSaude != null && _declaracaoDeSaude.ItensDeDeclaracaoDeSaude.Any())
            {
                foreach (var item in _declaracaoDeSaude.ItensDeDeclaracaoDeSaude)
                {
                    var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();
                    if (empresaId != null)
                    {
                        item.EmpresaId = empresaId.Value;
                    }
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Edit, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<IdInput> Save(DeclaracaoDeSaudeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, DeclaracaoDeSaudeInput>(_declaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, DeclaracaoDeSaudeInput>(_declaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Edit, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<DeclaracaoDeSaudeInput> SaveAndReturnEntity(DeclaracaoDeSaudeInput input)
        {
            var result = await ApplyEmpresaFilter<Task<DeclaracaoDeSaudeInput>>(async () =>
           {
               DeclaracaoDeSaudeInput declaracao = null;

               if (input.Id == default(int))
               {
                   if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Create))
                       throw new UserFriendlyException(L("PermissionToCreateRecord"));

                   declaracao = await base.CreateAndReturnEntity<DeclaracaoDeSaudeInput, DeclaracaoDeSaudeInput>(_declaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
               }
               else
               {
                   if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Edit))
                       throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                   declaracao = await base.UpdateAndReturnEntity<DeclaracaoDeSaudeInput, DeclaracaoDeSaudeInput>(_declaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
               }

               return declaracao;
           });

            return result;
        }

        public async Task<PagedResultDto<DeclaracaoDeSaudeListDto>> GetDeclaracoesDeSaudePaginado(GetDeclaracaoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<DeclaracaoDeSaude>>
            {
                new WhereIfCondition<DeclaracaoDeSaude>(!input.Nome.IsNullOrEmpty(), r => r.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<DeclaracaoDeSaudeListDto, GetDeclaracaoDeSaudeInput>(_declaracaoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<DeclaracaoDeSaudeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<DeclaracaoDeSaudeInput, IdInput>(_declaracaoDeSaudeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_declaracaoDeSaudeService, input);
        }

        public async Task<DeclaracaoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<DeclaracaoDeSaudeInput, IdInput>(_declaracaoDeSaudeService, input);
        }

        public Task ResponderTodosQuestionarios(DeclaracaoDeSaudeInput input)
        {
            throw new NotImplementedException();
        }
    }
}