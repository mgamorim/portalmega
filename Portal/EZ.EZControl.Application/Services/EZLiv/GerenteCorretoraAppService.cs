﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class GerenteCorretoraAppService : EZControlAppServiceBase<GerenteCorretora>, IGerenteCorretoraAppService
    {
        private readonly ICorretorService _corretorService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly ICorretoraService _corretoraService;
        private readonly IPessoaService _pessoaService;
        private readonly IGerenteCorretoraService _gerenteService;
        private readonly IEmpresaService _empresaService;
        public GerenteCorretoraAppService(IRepository<GerenteCorretora> corretorRepository,
                                          ICorretorService corretorService,
                                          IPessoaFisicaService pessoaFisicaService,
                                          ICorretoraService corretoraService,
                                          IPessoaService pessoaService,
                                          IGerenteCorretoraService gerenteService, 
                                          IEmpresaService empresaService
                                          )
            : base(corretorRepository)
        {
            _corretorService = corretorService;
            _pessoaFisicaService = pessoaFisicaService;
            _corretoraService = corretoraService;
            _pessoaService = pessoaService;
            _gerenteService = gerenteService;
            _empresaService = empresaService;
        }
        private void ValidateInput(GerenteCorretoraInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (input.CorretoraId == 0)
            {
                validationErrors.Add(new ValidationResult("Corretora não informada"));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        private async Task ProcessInput(GerenteCorretora entity, GerenteCorretoraInput input)
        {
            if (input.Pessoa.Id > 0)
            {
                try
                {
                    var pessoa = await _pessoaService.GetById(input.Pessoa.Id);
                    if (pessoa != null)
                    {
                        entity.Pessoa = pessoa;
                        entity.PessoaId = input.Pessoa.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Corretora.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            if (input.CorretoraId > 0)
            {
                var corretora = await _corretoraService.GetById(input.CorretoraId);
                if (corretora != null)
                {
                    entity.CorretoraId = input.CorretoraId;
                    entity.Corretora = corretora;
                }
                else
                {
                    throw new UserFriendlyException(L("Corretora.NotFoundError"));
                }
            }
            var validationErrors = new List<ValidationResult>();
            if (entity.IsPessoaFisica)
            {
                var errors = entity.IsValidGerenteCorretora();
                if (errors.Any())
                {
                    foreach (var valid in errors)
                    {
                        validationErrors.Add(new ValidationResult(L(valid)));
                    }
                }
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Create, AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Edit)]
        public async Task<IdInput> Save(GerenteCorretoraInput input)
        {
            IdInput result = null;
            var gerenteCorretora = await _gerenteService.GetAll().Where(x => x.CorretoraId == input.CorretoraId && x.PessoaId == input.Pessoa.Id).FirstOrDefaultAsync();
            if (gerenteCorretora == null)
            {
                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == input.Pessoa.Id).FirstOrDefaultAsync();
                var pessoa = await _pessoaService.GetById(input.Pessoa.Id);

                gerenteCorretora = new GerenteCorretora() {
                    CorretoraId = input.CorretoraId,
                    PessoaId = input.Pessoa.Id,
                    Pessoa = pessoa,
                    IsActive = true,
                    TenantId = 1
                };

                var id = await Repository.InsertAndGetIdAsync(gerenteCorretora);

                //TODO não precisa ser um corretor, colocar coo pessoa física
                //var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
                //_gerenteService.AddGerenteInRole(corretor, empresa);

                result = new IdInput(id);
            }else
            {
                throw new UserFriendlyException(L("GerenteCorretora.PessoaGerenteExistenteError"));
            }
            return result;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Create, AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Edit)]
        public async Task<GerenteCorretoraInput> SaveAndReturnEntity(GerenteCorretoraInput input)
        {
            GerenteCorretoraInput result = null;
            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<GerenteCorretoraInput, GerenteCorretoraInput>(_gerenteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<GerenteCorretoraInput, GerenteCorretoraInput>(_gerenteService, input, () => ValidateInput(input), ProcessInput);
            }
            return result;
        }
        public async Task<PagedResultDto<GerenteCorretoraListDto>> GetGerenteCorretorasPaginado(GetGerenteCorretoraInput input)
        {
            var condicoes = new List<WhereIfCondition<GerenteCorretora>>
            {
                new WhereIfCondition<GerenteCorretora>(!input.Nome.IsNullOrEmpty(), r => r.NomePessoa.Contains(input.Nome))
            };
            var result = await base.GetListPaged<GerenteCorretoraListDto, GetGerenteCorretoraInput>(_gerenteService, input, condicoes);
            return result;
        }
        public async Task<PagedResultDto<GerenteCorretoraListDto>> GetGerentesByCorretoraId(GetGerenteCorretoraInput input)
        {
            var condicoes = new List<WhereIfCondition<GerenteCorretora>>
            {
                new WhereIfCondition<GerenteCorretora>(input.CorretoraId > 0, r => r.CorretoraId == input.CorretoraId)
            };
            var result = await base.GetListPaged<GerenteCorretoraListDto, GetGerenteCorretoraInput>(_gerenteService, input, condicoes);
            return result;
        }
        public async Task<GerenteCorretoraInput> GetById(IdInput input)
        {
            return await base.GetEntityById<GerenteCorretoraInput, IdInput>(_gerenteService, input);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_GerenteCorretora_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_gerenteService, input);
        }
        public async Task<GerenteCorretoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<GerenteCorretoraInput, IdInput>(_gerenteService, input);
        }
    }
}