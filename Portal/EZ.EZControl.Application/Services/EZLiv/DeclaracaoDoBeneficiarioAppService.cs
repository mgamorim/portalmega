﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class DeclaracaoDoBeneficiarioAppService : EZControlAppServiceBase<DeclaracaoDoBeneficiario>, IDeclaracaoDoBeneficiarioAppService
    {
        private readonly IDeclaracaoDoBeneficiarioService _declaracaoDoBeneficiarioService;
        private readonly IItemDeDeclaracaoDoBeneficiarioService _itemDeDeclaracaoDoBeneficiarioService;
        private readonly IEmpresaService _empresaService;
        private readonly IAdministradoraService _administradoraService;

        public DeclaracaoDoBeneficiarioAppService(IRepository<DeclaracaoDoBeneficiario> declaracaoDoBeneficiarioRepository,
                                                  IDeclaracaoDoBeneficiarioService declaracaoDoBeneficiarioService,
                                                  IItemDeDeclaracaoDoBeneficiarioService itemDeDeclaracaoDoBeneficiarioService,
                                                  IEmpresaService empresaService,
                                                  IAdministradoraService administradoraService)
            : base(declaracaoDoBeneficiarioRepository)
        {
            _declaracaoDoBeneficiarioService = declaracaoDoBeneficiarioService;
            _itemDeDeclaracaoDoBeneficiarioService = itemDeDeclaracaoDoBeneficiarioService;
            _empresaService = empresaService;
            _administradoraService = administradoraService;
        }

        private void ValidateInput(DeclaracaoDoBeneficiarioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (!input.ItensDeDeclaracaoDoBeneficiario.Any())
                validationErrors.Add(new ValidationResult(L("DeclaracaoDoBeneficiario.EmptyItensDeDeclaracao")));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(DeclaracaoDoBeneficiario _declaracaoDoBeneficiario, DeclaracaoDoBeneficiarioInput _declaracaoDoBeneficiarioInput)
        {
            _declaracaoDoBeneficiario.TenantId = AbpSession.TenantId;

            _declaracaoDoBeneficiario.ItensDeDeclaracaoDoBeneficiario.Clear();
            if (_declaracaoDoBeneficiario.ItensDeDeclaracaoDoBeneficiario != null && _declaracaoDoBeneficiarioInput.ItensDeDeclaracaoDoBeneficiario.Any())
            {
                foreach (var item in _declaracaoDoBeneficiarioInput.ItensDeDeclaracaoDoBeneficiario)
                {
                    var itemDeDeclaracao = item.MapTo<ItemDeDeclaracaoDoBeneficiario>();
                    if (item.Id > 0)
                        itemDeDeclaracao = await _itemDeDeclaracaoDoBeneficiarioService.GetById(item.Id);

                    itemDeDeclaracao.TenantId = AbpSession.TenantId;
                    _declaracaoDoBeneficiario.ItensDeDeclaracaoDoBeneficiario.Add(itemDeDeclaracao);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Create, AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Edit, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<IdInput> Save(DeclaracaoDoBeneficiarioInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, DeclaracaoDoBeneficiarioInput>(_declaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, DeclaracaoDoBeneficiarioInput>(_declaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Create, AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Edit, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<DeclaracaoDoBeneficiarioInput> SaveAndReturnEntity(DeclaracaoDoBeneficiarioInput input)
        {
            DeclaracaoDoBeneficiarioInput declaracao = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                declaracao = await base.CreateAndReturnEntity<DeclaracaoDoBeneficiarioInput, DeclaracaoDoBeneficiarioInput>(_declaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                declaracao = await base.UpdateAndReturnEntity<DeclaracaoDoBeneficiarioInput, DeclaracaoDoBeneficiarioInput>(_declaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }

            return declaracao;
        }

        public async Task<PagedResultDto<DeclaracaoDoBeneficiarioListDto>> GetDeclaracoesDoBeneficiarioPaginado(GetDeclaracaoDoBeneficiarioInput input)
        {
            var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);

            var administradora = await _administradoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();

            var condicoes = new List<WhereIfCondition<DeclaracaoDoBeneficiario>>
            {
                new WhereIfCondition<DeclaracaoDoBeneficiario>(!input.Nome.IsNullOrEmpty(), r => r.Nome.Contains(input.Nome))
            };

            if (administradora != null)
                condicoes.Add(new WhereIfCondition<DeclaracaoDoBeneficiario>(true, r => r.Empresa.PessoaJuridicaId == administradora.PessoaJuridicaId));

            var result = await base.GetListPaged<DeclaracaoDoBeneficiarioListDto, GetDeclaracaoDoBeneficiarioInput>(_declaracaoDoBeneficiarioService, input, condicoes);
            return result;
        }

        public async Task<DeclaracaoDoBeneficiarioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<DeclaracaoDoBeneficiarioInput, IdInput>(_declaracaoDoBeneficiarioService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_DeclaracaoDoBeneficiario_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_declaracaoDoBeneficiarioService, input);
        }

        public async Task<DeclaracaoDoBeneficiarioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<DeclaracaoDoBeneficiarioInput, IdInput>(_declaracaoDoBeneficiarioService, input);
        }
    }
}