﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Beneficiario, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class BeneficiarioBaseAppService : EZControlAppServiceBase<BeneficiarioBase>, IBeneficiarioBaseAppService
    {
        private readonly IBeneficiarioBaseService _beneficiarioService;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IDocumentoService _documentoService;
        private readonly ITelefoneService _telefoneService;
        private readonly IPessoaService _pessoaService;
        private readonly IGrupoPessoaService _grupoPessoaService;
        private readonly IEstadoService _estadoService;
        private readonly IProfissaoService _profissaoService;
        private readonly ICorretoraService _corretoraService;
        private readonly IPropostaDeContratacaoService _propostaService;
        private readonly IProponenteTitularService _titularService;
        private readonly UserManager _userManager;
        private readonly EmpresaService _empresaService;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly RoleManager _roleManager;
        private readonly IAdministradoraService _administradoraService;
        public BeneficiarioBaseAppService(IRepository<BeneficiarioBase> beneficiarioRepository,
                                          IBeneficiarioBaseService beneficiarioService,
                                          ITipoDeDocumentoService tipoDeDocumentoService,
                                          IDocumentoService documentoService,
                                          ITelefoneService telefoneService,
                                          IPessoaService pessoaService,
                                          IGrupoPessoaService grupoPessoaService,
                                          IEstadoService estadoService,
                                          IProfissaoService profissaoService,
                                          ICorretoraService corretoraService,
                                          IPropostaDeContratacaoService propostaService,
                                          IProponenteTitularService titularService,
                                          UserManager userManager,
                                          EmpresaService empresaService,
                                          RoleManager roleManager,
                                          IUnitOfWorkManager unitOfWorkManager,
                                          IAdministradoraService administradoraService)
            : base(beneficiarioRepository)
        {
            _beneficiarioService = beneficiarioService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _documentoService = documentoService;
            _telefoneService = telefoneService;
            _pessoaService = pessoaService;
            _grupoPessoaService = grupoPessoaService;
            _estadoService = estadoService;
            _profissaoService = profissaoService;
            _corretoraService = corretoraService;
            _propostaService = propostaService;
            _titularService = titularService;
            _empresaService = empresaService;
            _unitOfWorkManager = unitOfWorkManager;
            _roleManager = roleManager;
            _userManager = userManager;
            _administradoraService = administradoraService;
        }

        private void ValidateInput(BeneficiarioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(BeneficiarioBase beneficiario, BeneficiarioInput beneficiarioInput)
        {

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Create, AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Edit)]
        public async Task<IdInput> Save(BeneficiarioInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, BeneficiarioInput>(_beneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, BeneficiarioInput>(_beneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Create, AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Edit)]
        public async Task<BeneficiarioInput> SaveAndReturnEntity(BeneficiarioInput input)
        {
            BeneficiarioInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<BeneficiarioInput, BeneficiarioInput>(_beneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<BeneficiarioInput, BeneficiarioInput>(_beneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<BeneficiarioListDto>> GetBeneficiariosPaginado(GetBeneficiarioInput input)
        {
            var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);

            var administradora = await _administradoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();
            var corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();

            var condicoes = new List<WhereIfCondition<BeneficiarioBase>>();
            condicoes.Add(new WhereIfCondition<BeneficiarioBase>(!input.Nome.IsNullOrEmpty(), r => r.PessoaFisica.Nome.Contains(input.Nome)));

            if (administradora != null)
            {
                var idsCorretoras = administradora.Corretoras.Select(x => x.Id);
                condicoes.Add(new WhereIfCondition<BeneficiarioBase>(true, r => r.Corretoras.Any(x => idsCorretoras.Contains(x.Id))));
            }

            if (corretora != null)
                condicoes.Add(new WhereIfCondition<BeneficiarioBase>(true, r => r.Corretoras.Any(x => x.Id == corretora.Id)));

            var result = await base.GetListPaged<BeneficiarioListDto, GetBeneficiarioInput>(_beneficiarioService, input, condicoes);
            return result;
        }

        public async Task<BeneficiarioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<BeneficiarioInput, IdInput>(_beneficiarioService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Beneficiario_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_beneficiarioService, input);
        }

        public async Task<BeneficiarioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<BeneficiarioInput, IdInput>(_beneficiarioService, input);
        }

        public async Task IsValidBeneficiario(IdInput input)
        {
            var beneficiario = await _beneficiarioService.GetById(input.Id);
            _beneficiarioService.IsValidPreCadastro(beneficiario);
        }

        public async Task<User> CreateUserBeneficiario(BeneficiarioUserInput input)
        {
            var beneficiario = await _beneficiarioService.GetById(input.Id);
            var empresa = await _empresaService.GetEmpresaDefault();
            return _beneficiarioService.CreateUserBeneficiario(beneficiario, empresa, input.TipoDeUsuario, input.StaticRoleName);
        }

        public async Task SendEmailUserBeneficiario(BeneficiarioUserInput input)
        {
            var user = await _userManager.GetUserByIdAsync(input.UserId);
            Corretora corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == user.Empresas.First().PessoaJuridicaId).FirstOrDefaultAsync();
            _beneficiarioService.SendEmail(user, user.Empresas.First().Slug, corretora);
        }

        private async Task CreateUserBeneficiario(BeneficiarioUserPessoaInput input)
        {
            var userlogado = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(userlogado.Empresas.First().Id);

            //var empresa = await _empresaService.GetEmpresaDefault();
            var empresaId = empresa.Id;

            using (
                CurrentUnitOfWork.SetFilterParameter(AbpDataFilters.MustHaveTenant, AbpDataFilters.Parameters.TenantId,
                    empresa.TenantId))
            {
                using (
                    CurrentUnitOfWork.SetFilterParameter(AbpDataFilters.MayHaveTenant,
                        AbpDataFilters.Parameters.TenantId, empresa.TenantId))
                {
                    Corretora corretora = null;
                    if (!string.IsNullOrEmpty(input.CorretoraSlug))
                        corretora = await _corretoraService.GetCorretoraBySlug(input.CorretoraSlug);

                    if (!string.IsNullOrEmpty(input.Corretora) && corretora == null)
                        corretora = _corretoraService.GetCorretoraByName(input.Corretora);

                    var pessoaFisica = new PessoaFisica { TenantId = corretora.TenantId };

                    #region Documento

                    var tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);


                    Documento documento = new Documento
                    {
                        Numero = input.Cpf.Replace(".", string.Empty).Replace("-", string.Empty),
                        Empresa = empresa,
                        EmpresaId = empresaId,
                        TenantId = empresa.TenantId
                    };

                    if (!_tipoDeDocumentoService.IsCpf(documento.Numero))
                        throw new UserFriendlyException(L("TipoDeDocumento.MessagemDeErroValidacaoCpf"));

                    documento.AssociarTipoDocumento(tipo, _documentoService);

                    #endregion

                    #region Email

                    EnderecoEletronico enderecoEletronico = new EnderecoEletronico
                    {
                        TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal,
                        Endereco = input.Email,
                        Empresa = empresa,
                        EmpresaId = empresaId,
                        TenantId = empresa.TenantId
                    };

                    #endregion

                    #region Celular

                    var telefone = new Telefone
                    {
                        Tipo = TipoTelefoneEnum.Celular,
                        Empresa = empresa,
                        EmpresaId = empresaId,
                        TenantId = empresa.TenantId
                    };
                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);

                    #endregion

                    pessoaFisica.Nome = input.NomeCompleto;
                    pessoaFisica.DataDeNascimento = input.DataDeNascimento;
                    pessoaFisica.Documentos = new List<Documento> { documento };
                    pessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico> { enderecoEletronico };
                    pessoaFisica.Telefones = new List<Telefone> { telefone };

                    //Cria o usuário
                    var user = CreateUser(pessoaFisica, TipoDeUsuarioEnum.Beneficiario, empresa,StaticRoleNames.Tenants.Beneficiario);
                    
                    _beneficiarioService.SendEmail(user, user.Empresas.First().Slug, corretora);
                }
            }
        }

        private async Task<Empresa> GetEmpresa(int IdCorretora)
        {
            //TODO: Está pegando o Id 1 (Megavita) temporariamente, mover seleção de corretora para alguma aba
            return await _empresaService.GetById(IdCorretora);
        }

        public async Task CreateUserPessoaBeneficiario(BeneficiarioUserPessoaInput input)
        {
            await this.CreateUserBeneficiario(input);
        }

        [AbpAllowAnonymous]
        public async Task CreateUserPessoaBeneficiarioFront(BeneficiarioUserPessoaInput input)
        {
            await this.CreateUserBeneficiario(input);
        }

        private User CreateUser(PessoaFisica pessoaFisica, TipoDeUsuarioEnum tipoDeUsuario, Empresa empresa, string staticRoleName)
        {
            var user = User.CreateUserPorPessoa(pessoaFisica, tipoDeUsuario, string.Empty);

            if (!user.TenantId.HasValue || user.TenantId.Value == 0)
                user.TenantId = empresa.TenantId;

            var role = _roleManager.Roles.FirstOrDefault(r => r.TenantId == user.TenantId && r.Name == staticRoleName);
            if (role == null)
                _roleManager.Create(new Role(user.TenantId, staticRoleName, staticRoleName) { IsStatic = true });
            else
            {
                user.Roles = new List<UserRole>() { };
                user.Roles.Add(new UserRole(user.TenantId, user.Id, role.Id));
            }

            user.Empresas = new List<Empresa> { empresa };

            _userManager.Create(user);
            _unitOfWorkManager.Current.SaveChanges();

            try
            {
                if (role == null)
                    _userManager.AddToRole(user.Id, staticRoleName);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return user;
        }
    }
}
