﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Dependente, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class DependenteAppService : EZControlAppServiceBase<Dependente>, IDependenteAppService
    {
        private readonly IDependenteService _dependenteService;

        public DependenteAppService(IRepository<Dependente> _dependenteRepository,
                                          IDependenteService _dependenteService)
            : base(_dependenteRepository)
        {
            this._dependenteService = _dependenteService;
        }

        private void ValidateInput(DependenteInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Dependente _dependente, DependenteInput _dependenteInput)
        {
            var validationErrors = new List<ValidationResult>();

            var errors = _dependente.IsValidDependente();

            if (errors.Any())
            {
                foreach (string erro in errors)
                {
                    validationErrors.Add(new ValidationResult(L(erro)));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }


        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Dependente_Create, AppPermissions.Pages_Tenant_EZLiv_Dependente_Edit)]
        public async Task<IdInput> Save(DependenteInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Dependente_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, DependenteInput>(_dependenteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Dependente_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, DependenteInput>(_dependenteService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Dependente_Create, AppPermissions.Pages_Tenant_EZLiv_Dependente_Edit)]
        public async Task<DependenteInput> SaveAndReturnEntity(DependenteInput input)
        {
            DependenteInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Dependente_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<DependenteInput, DependenteInput>(_dependenteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Dependente_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<DependenteInput, DependenteInput>(_dependenteService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<DependenteListDto>> GetDependentesPaginado(GetDependenteInput input)
        {
            var condicoes = new List<WhereIfCondition<Dependente>>
            {
                new WhereIfCondition<Dependente>(!input.Nome.IsNullOrEmpty(), r => r.PessoaFisica.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<DependenteListDto, GetDependenteInput>(_dependenteService, input, condicoes);
            return result;
        }

        public async Task<DependenteInput> GetById(IdInput input)
        {
            return await base.GetEntityById<DependenteInput, IdInput>(_dependenteService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Dependente_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_dependenteService, input);
        }

        public async Task<DependenteInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<DependenteInput, IdInput>(_dependenteService, input);
        }
    }
}
