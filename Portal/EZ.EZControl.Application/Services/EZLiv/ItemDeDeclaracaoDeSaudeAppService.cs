﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    //[AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude)]
    public class ItemDeDeclaracaoDeSaudeAppService : EZControlAppServiceBase<ItemDeDeclaracaoDeSaude>, IItemDeDeclaracaoDeSaudeAppService
    {
        private readonly IItemDeDeclaracaoDeSaudeService _itemDeDeclaracaoDeSaudeService;
        private readonly IQuestionarioDeDeclaracaoDeSaudeService _questionarioDeDeclaracaoDeSaudeService;

        public ItemDeDeclaracaoDeSaudeAppService(IRepository<ItemDeDeclaracaoDeSaude> _itemDeDeclaracaoDeSaudeRepository,
                                          IItemDeDeclaracaoDeSaudeService _itemDeDeclaracaoDeSaudeService,
                                          IQuestionarioDeDeclaracaoDeSaudeService questionarioDeDeclaracaoDeSaudeService)
            : base(_itemDeDeclaracaoDeSaudeRepository)
        {
            this._itemDeDeclaracaoDeSaudeService = _itemDeDeclaracaoDeSaudeService;
            this._questionarioDeDeclaracaoDeSaudeService = questionarioDeDeclaracaoDeSaudeService;
        }

        private void ValidateInput(ItemDeDeclaracaoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Ordenacao == 0)
                validationErrors.Add(new ValidationResult(L("ItemDeDeclaracaoDeSaude.OrdenacaoMaiorQueZero")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ItemDeDeclaracaoDeSaude _itemDeDeclaracaoDeSaude, ItemDeDeclaracaoDeSaudeInput _itemDeDeclaracaoDeSaudeInput)
        {
            var questionario = await _questionarioDeDeclaracaoDeSaudeService.FirstOrDefault(x => x.ItensDeDeclaracaoDeSaude.Any(y => y.Id == _itemDeDeclaracaoDeSaudeInput.Id));

            if (questionario != null)
            {
                var existeValorOrdenacao = questionario.ItensDeDeclaracaoDeSaude.Any(x => x.Ordenacao == _itemDeDeclaracaoDeSaude.Ordenacao && _itemDeDeclaracaoDeSaude.Id != x.Id);

                if (existeValorOrdenacao)
                    throw new UserFriendlyException(L("ItemDeDeclaracaoDeSaude.OrdemJaExistente"));
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Edit)]
        public async Task<IdInput> Save(ItemDeDeclaracaoDeSaudeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                //if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Create))
                //    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ItemDeDeclaracaoDeSaudeInput>(_itemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ItemDeDeclaracaoDeSaudeInput>(_itemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        //[AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Edit)]
        public async Task<ItemDeDeclaracaoDeSaudeInput> SaveAndReturnEntity(ItemDeDeclaracaoDeSaudeInput input)
        {
            ItemDeDeclaracaoDeSaudeInput result = null;

            if (input.Id == default(int))
            {
                //if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Create))
                //    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ItemDeDeclaracaoDeSaudeInput, ItemDeDeclaracaoDeSaudeInput>(_itemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ItemDeDeclaracaoDeSaudeInput, ItemDeDeclaracaoDeSaudeInput>(_itemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ItemDeDeclaracaoDeSaudeListDto>> GetItensDeDeclaracaoDeSaudePaginado(GetItemDeDeclaracaoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemDeDeclaracaoDeSaude>>
            {
                new WhereIfCondition<ItemDeDeclaracaoDeSaude>(!input.Pergunta.IsNullOrEmpty(), r => r.Pergunta.Contains(input.Pergunta))
            };
            var result = await base.GetListPaged<ItemDeDeclaracaoDeSaudeListDto, GetItemDeDeclaracaoDeSaudeInput>(_itemDeDeclaracaoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<ItemDeDeclaracaoDeSaudeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ItemDeDeclaracaoDeSaudeInput, IdInput>(_itemDeDeclaracaoDeSaudeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_itemDeDeclaracaoDeSaudeService, input);
        }

        public async Task<ItemDeDeclaracaoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ItemDeDeclaracaoDeSaudeInput, IdInput>(_itemDeDeclaracaoDeSaudeService, input);
        }

        public async Task<ListResultDto<ItemDeDeclaracaoDeSaudeListDto>> Get()
        {
            var condicoes = new List<WhereIfCondition<ItemDeDeclaracaoDeSaude>>
            {
                new WhereIfCondition<ItemDeDeclaracaoDeSaude>(true, r => r.Id > 0)
            };
            var result = await base.GetList<ItemDeDeclaracaoDeSaudeListDto>(_itemDeDeclaracaoDeSaudeService, condicoes, x => x.Pergunta);
            return result;
        }
    }
}
