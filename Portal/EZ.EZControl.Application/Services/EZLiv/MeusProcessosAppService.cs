﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.MeusProcessos;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Data.Entity;
using Abp.Collections.Extensions;
using Abp.UI;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Filters;
using EZ.EZControl.Common.Extensions;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_MeusProcessos, AppPermissions.Pages_Tenant_EZLiv_MeusProcessos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class MeusProcessosAppService : EZControlAppServiceBase<ProdutoDePlanoDeSaude>, IMeusProcessosAppService
    {
        private readonly IPropostaDeContratacaoService _propostaService;
        private readonly ICorretorService _corretorService;
        public MeusProcessosAppService(IRepository<ProdutoDePlanoDeSaude> produtoDePlanoDeSaudeRepository,
            IPropostaDeContratacaoService propostaService,
            ICorretorService corretorService)
            : base(produtoDePlanoDeSaudeRepository)
        {
            _propostaService = propostaService;
            _corretorService = corretorService;
        }

        public async Task<PagedResultDto<MeusProcessosListDto>> Get(GetMeusProcessosInput input)
        {
            try
            {
                var user = await GetCurrentUserAsync();
                var pessoa = user.Pessoa;
                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == pessoa.Id).FirstOrDefaultAsync();
                var proposta = _propostaService
                    .GetAll()
                    .Where(x => !x.IsDeleted)
                    .WhereIf(true, x => x.CorretorId == corretor.Id && x.Produto != null)
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), x => x.Produto.Nome.Contains(input.Nome));
                var count = proposta.Count();
                var dados = proposta.OrderBy(x => x.Produto.Nome);
                var listDtos = dados.Select(x => new MeusProcessosListDto()
                {
                    StatusProposta = x.StatusDaProposta.GetDescription(),
                    Abrangencia = x.Produto != null ? x.Produto.PlanoDeSaude.Abrangencia.GetDescription() : string.Empty,
                    Administradora = x.Produto != null ? x.Produto.Administradora.NomePessoa : string.Empty,
                    Id = x.Produto != null ? x.Produto.Id : 0,
                    Nome = x.Produto != null ? x.Produto.Nome : string.Empty,
                    Reembolso = x.Produto != null ? x.Produto.PlanoDeSaude.Reembolso : false,
                    Valor = x.Pedido != null ? x.Pedido.ItensDePedidoReadOnly.Sum(p => p.Valor) : 0,
                    PropostaId = x.Id
                }).ToList();
                return new PagedResultDto<MeusProcessosListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
        public async Task<MeusProcessosInput> GetById(IdInput input)
        {
            return null;
        }
    }
}