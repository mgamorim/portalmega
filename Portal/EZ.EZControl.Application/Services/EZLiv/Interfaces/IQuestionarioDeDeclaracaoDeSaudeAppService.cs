﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IQuestionarioDeDeclaracaoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(QuestionarioDeDeclaracaoDeSaudeInput input);
        Task<QuestionarioDeDeclaracaoDeSaudeInput> SaveAndReturnEntity(QuestionarioDeDeclaracaoDeSaudeInput input);
        Task<PagedResultDto<QuestionarioDeDeclaracaoDeSaudeListDto>> GetPaginado(GetQuestionarioDeDeclaracaoDeSaudeInput input);
        Task<QuestionarioDeDeclaracaoDeSaudeInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task<QuestionarioDeDeclaracaoDeSaudeInput> GetByExternalId(IdInput input);
    }
}
