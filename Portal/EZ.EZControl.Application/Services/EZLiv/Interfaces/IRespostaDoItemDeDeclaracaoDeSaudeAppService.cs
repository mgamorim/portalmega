﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IRespostaDoItemDeDeclaracaoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(RespostaDoItemDeDeclaracaoDeSaudeInput input);

        Task<RespostaDoItemDeDeclaracaoDeSaudeInput> SaveAndReturnEntity(RespostaDoItemDeDeclaracaoDeSaudeInput input);

        Task<PagedResultDto<RespostaDoItemDeDeclaracaoDeSaudeListDto>> GetDeclaracoesDeSaudePaginado(GetRespostaDoItemDeDeclaracaoDeSaudeInput input);

        Task<RespostaDoItemDeDeclaracaoDeSaudeInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<RespostaDoItemDeDeclaracaoDeSaudeInput> GetByExternalId(IdInput input);
    }
}
