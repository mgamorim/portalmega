﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.MeusProcessos;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IMeusProcessosAppService : IApplicationService
    {
        Task<PagedResultDto<MeusProcessosListDto>> Get(GetMeusProcessosInput input);
        Task<MeusProcessosInput> GetById(IdInput input);
    }
}
