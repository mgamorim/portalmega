﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IVigenciaAppService : IApplicationService
    {
        Task<IdInput> Save(VigenciaInput input);
        Task<VigenciaInput> SaveAndReturnEntity(VigenciaInput input);
        Task<PagedResultDto<VigenciaListDto>> GetPaginado(GetVigenciaInput input);
        Task<PagedResultDto<VigenciaListDto>> GetPaginadoByProduto(GetVigenciaByProdutoInput input);
        Task<VigenciaInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task<VigenciaInput> GetByExternalId(IdInput input);
    }
}
