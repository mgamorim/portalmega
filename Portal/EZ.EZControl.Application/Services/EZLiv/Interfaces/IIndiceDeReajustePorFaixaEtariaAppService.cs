﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IIndiceDeReajustePorFaixaEtariaAppService : IApplicationService
    {
        Task<IdInput> Save(IndiceDeReajustePorFaixaEtariaInput input);

        Task<IndiceDeReajustePorFaixaEtariaInput> SaveAndReturnEntity(IndiceDeReajustePorFaixaEtariaInput input);

        Task<PagedResultDto<IndiceDeReajustePorFaixaEtariaListDto>> GetPaginado(GetIndiceDeReajustePorFaixaEtariaInput input);

        Task<IndiceDeReajustePorFaixaEtariaInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<IndiceDeReajustePorFaixaEtariaInput> GetByExternalId(IdInput input);
    }
}
