﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IEspecialidadeSyncAppService : IApplicationService
    {
        Task<IdInput> Save(EspecialidadeSyncInput input);

        Task<EspecialidadeSyncInput> SaveAndReturnEntity(EspecialidadeSyncInput input);

        Task<PagedResultDto<EspecialidadeSyncListDto>> GetPaginado(GetEspecialidadeSyncInput input);

        Task<EspecialidadeSyncInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<EspecialidadeSyncInput> GetByExternalId(IdInput input);

        Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByEstado(GetEspecialidadeSyncByEstadoDto estadoDto);

        Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByEstadoDistinct(GetEspecialidadeSyncByEstadoDto estadoDto);

        Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByMunicipio(GetEspecialidadeSyncByMunicipioDto municipioDto);

        Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByMunicipioDistinct(GetEspecialidadeSyncByMunicipioDto municipioDto);

        Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByNomeDistinct(GetEspecialidadeSyncByNomeDto nomeDto);
    }
}
