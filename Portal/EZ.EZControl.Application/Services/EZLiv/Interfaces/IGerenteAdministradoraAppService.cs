﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteAdministradora;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IGerenteAdministradoraAppService : IApplicationService
    {
        Task<IdInput> Save(GerenteAdministradoraInput input);

        Task<GerenteAdministradoraInput> SaveAndReturnEntity(GerenteAdministradoraInput input);

        Task<PagedResultDto<GerenteAdministradoraListDto>> GetGerenteAdministradorasPaginado(GetGerenteAdministradoraInput input);

        Task<GerenteAdministradoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<GerenteAdministradoraInput> GetByExternalId(IdInput input);
    }
}
