﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Parametro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IParametroEZLivAppService : IApplicationService
    {
        Task<IdInput> Save(ParametroEZLivInput input);

        Task<ParametroEZLivInput> Get();
    }
}
