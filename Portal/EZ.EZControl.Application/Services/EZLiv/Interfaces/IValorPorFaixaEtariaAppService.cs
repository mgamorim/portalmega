﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IValorPorFaixaEtariaAppService : IApplicationService
    {
        Task<IdInput> Save(ValorPorFaixaEtariaInput input);

        Task<ValorPorFaixaEtariaInput> SaveAndReturnEntity(ValorPorFaixaEtariaInput input);

        Task<PagedResultDto<ValorPorFaixaEtariaListDto>> GetPaginado(GetValorPorFaixaEtariaInput input);

        Task<PagedResultDto<ValorPorFaixaEtariaListDto>> GetPaginadoByContratoId(GetValorPorFaixaEtariaInput input);

        Task<ValorPorFaixaEtariaInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ValorPorFaixaEtariaInput> GetByExternalId(IdInput input);
    }
}
