﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorCorretora;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ISupervisorCorretoraAppService : IApplicationService
    {
        Task<IdInput> Save(SupervisorCorretoraInput input);

        Task<SupervisorCorretoraInput> SaveAndReturnEntity(SupervisorCorretoraInput input);

        Task<PagedResultDto<SupervisorCorretoraListDto>> GetSupervisorCorretorasPaginado(GetSupervisorCorretoraInput input);

        Task<PagedResultDto<SupervisorCorretoraListDto>> GetSupervisoresByCorretoraId(GetSupervisorCorretoraInput input);

        Task<SupervisorCorretoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<SupervisorCorretoraInput> GetByExternalId(IdInput input);
    }
}
