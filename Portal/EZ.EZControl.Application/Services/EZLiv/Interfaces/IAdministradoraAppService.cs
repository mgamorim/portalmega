﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IAdministradoraAppService : IApplicationService
    {
        Task<IdInput> Save(AdministradoraInput input);

        Task<AdministradoraInput> SaveAndReturnEntity(AdministradoraInput input);

        Task<PagedResultDto<AdministradoraListDto>> GetPaginado(GetAdministradoraInput input);

        Task<AdministradoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<AdministradoraInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForAdministradora(GetPessoaExceptForAdministradora input);

    }
}
