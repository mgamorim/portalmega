﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IContratoAppService : IApplicationService
    {
        Task<IdInput> Save(ContratoInput input);

        Task<ContratoInput> SaveAndReturnEntity(ContratoInput input);

        Task<PagedResultDto<ContratoListDto>> GetPaginado(GetContratoInput input);

        Task Delete(IdInput input);

        Task<ContratoInput> GetById(IdInput input);

        Task<ContratoInput> GetByExternalId(IdInput input);
    }
}
