﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Laboratorio;
using EZ.EZControl.Dto.EZLiv.Getal.Laboratorio;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ILaboratorioAppService : IApplicationService
    {
        Task<IdInput> Save(LaboratorioInput input);

        Task<LaboratorioInput> SaveAndReturnEntity(LaboratorioInput input);

        Task<PagedResultDto<LaboratorioListDto>> GetPaginado(GetLaboratorioInput input);

        Task<LaboratorioInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<LaboratorioInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<EspecialidadeListDto>> GetEspecialidadeExceptForLaboratorio(GetEspecialidadeExceptForLaboratorio input);

    }
}
