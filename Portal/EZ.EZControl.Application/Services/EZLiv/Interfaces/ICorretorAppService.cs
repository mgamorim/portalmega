﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ICorretorAppService : IApplicationService
    {
        Task<IdInput> Save(CorretorInput input);

        Task<CorretorInput> SaveAndReturnEntity(CorretorInput input);

        Task<PagedResultDto<CorretorListDto>> GetCorretoresPaginado(GetCorretorInput input);

        Task<CorretorInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<CorretorInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForCorretor(GetPessoaExceptForCorretor input);

        //Task<PagedResultDto<CorretoraListDto>> GetCorretorasAssociadas(IdInput input);

        Task<PagedResultDto<CorretoraListDto>> GetCorretoraExceptForCorretor(GetCorretoraExceptForCorretor input);
    }
}
