﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.MeusProdutos;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IMeusProdutosAppService : IApplicationService
    {
        Task<PagedResultDto<MeusProdutosListDto>> Get(GetMeusProdutosInput input);
        Task<MeusProdutosInput> GetById(IdInput input);
    }
}
