﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IDependenteAppService : IApplicationService
    {
        Task<IdInput> Save(DependenteInput input);

        Task<DependenteInput> SaveAndReturnEntity(DependenteInput input);

        Task<PagedResultDto<DependenteListDto>> GetDependentesPaginado(GetDependenteInput input);

        Task<DependenteInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<DependenteInput> GetByExternalId(IdInput input);
    }
}
