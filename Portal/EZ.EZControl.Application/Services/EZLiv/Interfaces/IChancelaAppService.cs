﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IChancelaAppService : IApplicationService
    {
        Task<IdInput> Save(ChancelaInput input);

        Task<ChancelaInput> SaveAndReturnEntity(ChancelaInput input);

        Task<PagedResultDto<ChancelaListDto>> GetPaginado(GetChancelaInput input);

        Task<PagedResultDto<ChancelaListDto>> GetPaginadoParaPropostaDeContratacao(GetChancelaInput input);

        Task Delete(IdInput input);

        Task<ChancelaInput> GetById(IdInput input);
        Task<ChancelaInput> GetByIdParaPropostaDeContratacao(IdInput input);

        Task<ChancelaInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<ChancelaListDto>> GetChancelasByProfissaoId(IdInput input);
    }
}
