﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IFaixaEtariaAppService : IApplicationService
    {
        Task<IdInput> Save(FaixaEtariaInput input);

        Task<FaixaEtariaInput> SaveAndReturnEntity(FaixaEtariaInput input);

        Task<PagedResultDto<FaixaEtariaListDto>> GetPaginado(GetFaixaEtariaInput input);

        Task<PagedResultDto<FaixaEtariaListDto>> GetPaginadoExceptForIds(GetFaixaEtariaExceptForIdsInput input);

        Task<FaixaEtariaInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<FaixaEtariaInput> GetByExternalId(IdInput input);
    }
}
