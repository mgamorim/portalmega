﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IPropostaDeContratacaoAppService : IApplicationService
    {
        Task<IdInput> Save(PropostaDeContratacaoInput input);
        Task<PropostaDeContratacaoInput> SaveAndReturnEntity(PropostaDeContratacaoInput input);
        Task<PagedResultDto<PropostaDeContratacaoInput>> GetPaginado(GetPropostaDeContratacaoInput input);
        Task<PropostaDeContratacaoInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task SendEmailEdicaoDeProposta(IdInput input);
        Task SendEmailFormularios(PropostaDeContratacaoPassoAceiteDoContratoInput input);
        Task<PropostaDeContratacaoDataDeVigenciaInput> GetDataInicioDeVigencia(VigenciaListDto input);
        Task<PropostaDeContratacaoInput> GetByExternalId(IdInput input);
        Task<PropostaDeContratacaoInput> SavePassoPreCadastro(PropostaDeContratacaoPassoPreCadastroInput input);
        Task<PropostaDeContratacaoInput> SavePassoPreCadastroSimulador(PropostaDeContratacaoPassoPreCadastroInput input, int IdUser, int IdCorretora, int IdPlano );
        Task<UserDto> CreateUserBeneficiario(PropostaDeContratacaoPassoPreCadastroInput input);
        Task<PropostaDeContratacaoInput> SavePassoSelecionarPlano(PropostaDeContratacaoPassoSelecionarPlanoInput input);
        Task<PropostaDeContratacaoInput> SavePassoSelecionarPlanoSimulador(PropostaDeContratacaoPassoSelecionarPlanoInput input);
        Task<PropostaDeContratacaoInput> SavePassoDadosGerais(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task<PropostaDeContratacaoInput> SavePassoEnvioDeDocumentos(PropostaDeContratacaoPassoEnvioDeDocumentosInput input);
        Task<PropostaDeContratacaoInput> SavePassoAceiteDoContrato(PropostaDeContratacaoPassoAceiteDoContratoInput input);
        Task<PropostaDeContratacaoInput> SavePassoHomologacao(PropostaDeContratacaoPassoHomologacaoInput input);
        Task<PropostaDeContratacaoInput> SavePassoPagamento(PropostaDeContratacaoPassoPagamentoInput input);
        Task<PropostaDeContratacaoInput> SavePassoDadosGerenciais(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task VerificaDocumentosEmExigencia(IdInput input);
        Task<PropostaDeContratacaoPassoDadosGeraisInput> GetRespostasDeclaracaoDeSaude(IdInput input);
        Task<PropostaDeContratacaoInput> AtualizaStatusDaProposta(PropostaDeContratacaoInput input);
        Task SendEmailUserBeneficiario(UserDto input);
        Task SendEmailBoletoGerado(IdInput input);
        Task SendEmailPreCadastro(IdInput input);
        Task SendEmailAceite(IdInput input);
        Task SendEmailHomologacao(IdInput input);
        Task SendEmailHomologacaoConcluida(IdInput input);
        Task SendEmailHomologar(IdInput input);
        Task SendEmailConcluirAceite(IdInput input);
        Task SendEmailConcluirAceiteOnlyBeneficiario(IdInput input);
        Task SendEmailConcluirAceiteOnlyCorretor(IdInput input);
        Task<FormularioDeContratacaoInput> MontaFormulario(IdInput input);
        Task<PropostaDeContratacaoDoRelatorioV1Input> MontaPropostaDeContratacaoDoRelatorioV1(IdInput input);
        Task<FichaDoRelatorioV1Input> MontaFichaDoRelatorioV1(IdInput input);
        Task<FormularioDeAssociacaoInput> MontaFormularioAssociacao(IdInput input);
        Task<PropostaDeContratacaoInput> SaveDadosProponenteTitular(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task<PropostaDeContratacaoInput> SaveDadosResponsavel(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task<PropostaDeContratacaoInput> SaveDadosEndereco(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task<PropostaDeContratacaoInput> SaveDadosDependentes(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task<PropostaDeContratacaoInput> SaveDadosVigencia(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task<PropostaDeContratacaoInput> SaveDadosDeclaracaoDeSaude(PropostaDeContratacaoPassoDadosGeraisInput input);
        Task UpdateStatusEncerrado(IdInput input);
        Task SendEmailComprovanteDepositoCorretor(IdInput input);
    }
}
