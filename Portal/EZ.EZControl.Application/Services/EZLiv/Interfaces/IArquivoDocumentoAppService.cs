﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IArquivoDocumentoAppService : IApplicationService
    {
        Task<IdInput> Save(ArquivoDocumentoInput input);
        Task<ArquivoDocumentoInput> SaveAndReturnEntity(ArquivoDocumentoInput input);
        Task<PagedResultDto<ArquivoDocumentoListDto>> GetPaginado(GetArquivoDocumentoInput input);
        Task<ArquivoDocumentoInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task<ArquivoDocumentoInput> GetByExternalId(IdInput input);
        Task<ArquivoDocumentoInput> GetArquivoByName(ArquivoDocumentoInput input);
        Task<IdInput> ChangeArquivoDocumentoExigencia(ArquivoDocumentoInput input);
    }
}
