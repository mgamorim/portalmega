﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorAdministradora;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ISupervisorAdministradoraAppService : IApplicationService
    {
        Task<IdInput> Save(SupervisorAdministradoraInput input);

        Task<SupervisorAdministradoraInput> SaveAndReturnEntity(SupervisorAdministradoraInput input);

        Task<PagedResultDto<SupervisorAdministradoraListDto>> GetSupervisorAdministradorasPaginado(GetSupervisorAdministradoraInput input);

        Task<SupervisorAdministradoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<SupervisorAdministradoraInput> GetByExternalId(IdInput input);
    }
}
