﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IResponsavelAppService : IApplicationService
    {
        Task<IdInput> Save(ResponsavelInput input);

        Task<ResponsavelInput> SaveAndReturnEntity(ResponsavelInput input);

        Task<PagedResultDto<ResponsavelListDto>> GetResponsaveisPaginado(GetResponsavelInput input);

        Task<ResponsavelInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ResponsavelInput> GetByExternalId(IdInput input);
    }
}
