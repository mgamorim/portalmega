﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IBeneficiarioBaseAppService : IApplicationService
    {
        Task<IdInput> Save(BeneficiarioInput input);

        Task<BeneficiarioInput> SaveAndReturnEntity(BeneficiarioInput input);

        Task<PagedResultDto<BeneficiarioListDto>> GetBeneficiariosPaginado(GetBeneficiarioInput input);

        Task<BeneficiarioInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<BeneficiarioInput> GetByExternalId(IdInput input);

        Task IsValidBeneficiario(IdInput input);

        Task<User> CreateUserBeneficiario(BeneficiarioUserInput input);

        Task CreateUserPessoaBeneficiario(BeneficiarioUserPessoaInput input);

        Task CreateUserPessoaBeneficiarioFront(BeneficiarioUserPessoaInput input);
    }
}
