﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IRelatorioEzLivAppService : IApplicationService
    {
        Task<IdInput> Save(RelatorioInput input);
        Task<RelatorioInput> SaveAndReturnEntity(RelatorioInput input);
        Task<RelatorioInput> Duplicar(IdInput input);
        Task<PagedResultDto<RelatorioListDto>> GetPaginado(GetRelatorioInput input);
        Task<RelatorioInput> GetById(IdInput input);
        Task<RelatorioInput> GetByIdParaVisualizadorDeRelatorio(IdInput input);
        Task Delete(IdInput input);
        Task<RelatorioInput> GetByExternalId(IdInput input);
        Task<PagedResultDto<RelatorioListDto>> GetRelatorioExceptForIdInput(GetRelatorioExceptForIdInput input);
    }
}
