﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretor;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IPermissaoDeVendaDePlanoDeSaudeParaCorretorAppService : IApplicationService
    {
        Task<IdInput> Save(PermissaoDeVendaDePlanoDeSaudeParaCorretorInput input);

        Task<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> SaveAndReturnEntity(PermissaoDeVendaDePlanoDeSaudeParaCorretorInput input);

        Task<PagedResultDto<PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto>> GetPaginado(GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput input);

        Task Delete(IdInput input);

        Task<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> GetById(IdInput input);

        Task<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> GetByExternalId(IdInput input);
    }
}
