﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IItemDeDeclaracaoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(ItemDeDeclaracaoDeSaudeInput input);

        Task<ItemDeDeclaracaoDeSaudeInput> SaveAndReturnEntity(ItemDeDeclaracaoDeSaudeInput input);

        Task<PagedResultDto<ItemDeDeclaracaoDeSaudeListDto>> GetItensDeDeclaracaoDeSaudePaginado(GetItemDeDeclaracaoDeSaudeInput input);

        Task<ItemDeDeclaracaoDeSaudeInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ItemDeDeclaracaoDeSaudeInput> GetByExternalId(IdInput input);
        Task<ListResultDto<ItemDeDeclaracaoDeSaudeListDto>> Get();
    }
}
