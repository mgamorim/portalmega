﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IPermissaoDeVendaDePlanoDeSaudeParaCorretoraAppService : IApplicationService
    {
        Task<IdInput> Save(PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input);

        Task<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> SaveAndReturnEntity(PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input);

        Task<PagedResultDto<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto>> GetPaginado(GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input);

        Task Delete(IdInput input);

        Task<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> GetById(IdInput input);

        Task<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> GetByExternalId(IdInput input);

        Task<GetPermissoesPorCorretoraInput> PermissoesDeCorretora(IdInput input);

        Task<PagedResultDto<GetPermissoesPorCorretoraInput>> GetProdutoECorretorasDaPermissao(IdInput input);

        Task<PagedResultDto<CorretoraListDto>> GetCorretorasByProduto(GetValoresByProdutoInput input);
    }
}
