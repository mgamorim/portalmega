﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ClienteEZLiv;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IClienteEZLivAppService : IApplicationService
    {
        Task<IdInput> Save(ClienteEZLivInput input);

        Task<ClienteEZLivInput> SaveAndReturnEntity(ClienteEZLivInput input);

        Task<PagedResultDto<ClienteEZLivListDto>> GetPaginado(GetClienteEZLivInput input);

        Task<ClienteEZLivInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ClienteEZLivInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<ClienteListDto>> GetPessoaExceptForClienteEZLiv(GetClienteEZLivInput input);
    }
}
