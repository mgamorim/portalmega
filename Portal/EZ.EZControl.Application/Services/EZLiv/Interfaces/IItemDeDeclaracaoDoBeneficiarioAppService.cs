﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IItemDeDeclaracaoDoBeneficiarioAppService : IApplicationService
    {
        Task<IdInput> Save(ItemDeDeclaracaoDoBeneficiarioInput input);

        Task<ItemDeDeclaracaoDoBeneficiarioInput> SaveAndReturnEntity(ItemDeDeclaracaoDoBeneficiarioInput input);

        Task<PagedResultDto<ItemDeDeclaracaoDoBeneficiarioListDto>> GetItensDeDeclaracaoDoBeneficiarioPaginado(GetItemDeDeclaracaoDoBeneficiarioInput input);

        Task<ItemDeDeclaracaoDoBeneficiarioInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ItemDeDeclaracaoDoBeneficiarioInput> GetByExternalId(IdInput input);
    }
}
