﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface ICorretoraAppService : IApplicationService
    {
        Task<IdInput> Save(CorretoraInput input);

        Task<CorretoraInput> SaveAndReturnEntity(CorretoraInput input);

        Task<PagedResultDto<CorretoraListDto>> GetPaginado(GetCorretoraInput input);

        Task<CorretoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<CorretoraInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForCorretora(GetPessoaExceptForCorretora input);

        Task<PagedResultDto<CorretoraListDto>> GetPaginadoCorretorasExceptForIds(GetCorretoraInput input);

        Task<List<string>> GetCorretora(GetCorretoraInput input);
    }
}
