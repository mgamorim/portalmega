﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IOperadoraAppService : IApplicationService
    {
        Task<OperadoraPessoaIdDto> Save(OperadoraInput input);

        Task<OperadoraListDto> SaveAndReturnEntity(OperadoraInput input);

        Task<PagedResultDto<OperadoraListDto>> GetOperadorasPaginado(GetOperadoraInput input);

        Task<OperadoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<OperadoraInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForOperadora(GetPessoaExceptForOperadora input);

        Task<ListResultDto<OperadoraListDto>> GetAll();
    }
}
