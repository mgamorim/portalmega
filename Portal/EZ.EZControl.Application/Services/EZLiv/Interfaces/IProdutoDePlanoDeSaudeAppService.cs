﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IProdutoDePlanoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(ProdutoDePlanoDeSaudeInput input);
        Task<ProdutoDePlanoDeSaudeInput> SaveAndReturnEntity(ProdutoDePlanoDeSaudeInput input);
        Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutosDePlanoDeSaudePaginado(GetProdutoDePlanoDeSaudeInput input);
        Task<ProdutoDePlanoDeSaudeInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task<ProdutoDePlanoDeSaudeInput> GetByExternalId(IdInput input);
        Task<ContratoListDto> GetContrato(IdInput input);
        Task<ContratoInput> GetContratoByProdutoId(IdInput input);
        Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeExceptForContratoActive();
        Task<PagedResultDto<EstadoListDto>> GetEstadoExceptForProduto(GetEstadoExceptForProduto input);
        Task<PagedResultDto<CidadeListDto>> GetCidadeExceptForProduto(GetCidadeExceptForProduto input);
        Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetPagedProdutoAtivoComValor(GetValoresByProdutoInput input);
        Task<ValoresByProdutoListDto> GetValoresByContratoDetail(GetValoresByProdutoInput input);
        Task<DetalhesProdutoDePlanoDeSaudeInput> GetDetalhesById(GetDetalhesProdutoInput input);

        //Task<List<ProdutoDePlanoDeSaudeListDto>> GetSimulacaoProdutosComValores(SimulacaoModel model, List<ProdutoDePlanoDeSaude> produtosCorretor);
        Task<List<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeWithValorForCorretorSimulador(int id, SimulacaoModel model);

        Task<PagedResultDto<EstadoListDto>> GetEstadosByProdutos();
        Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeForCorretora();
        Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeWithValorForCorretora(GetValoresByProdutoInput input);

        Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeWithValorForCorretor(GetValoresAndProdutoByCorretorInput input);

        
    }
}
