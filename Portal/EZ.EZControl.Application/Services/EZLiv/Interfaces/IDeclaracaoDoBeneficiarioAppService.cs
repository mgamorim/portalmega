﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IDeclaracaoDoBeneficiarioAppService : IApplicationService
    {
        Task<IdInput> Save(DeclaracaoDoBeneficiarioInput input);

        Task<DeclaracaoDoBeneficiarioInput> SaveAndReturnEntity(DeclaracaoDoBeneficiarioInput input);

        Task<PagedResultDto<DeclaracaoDoBeneficiarioListDto>> GetDeclaracoesDoBeneficiarioPaginado(GetDeclaracaoDoBeneficiarioInput input);

        Task<DeclaracaoDoBeneficiarioInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<DeclaracaoDoBeneficiarioInput> GetByExternalId(IdInput input);
    }
}
