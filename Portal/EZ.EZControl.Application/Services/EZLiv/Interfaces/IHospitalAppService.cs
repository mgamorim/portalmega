﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.Hospital;
using EZ.EZControl.Dto.EZLiv.Getal.Hospital;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IHospitalAppService : IApplicationService
    {
        Task<IdInput> Save(HospitalInput input);

        Task<HospitalInput> SaveAndReturnEntity(HospitalInput input);

        Task<PagedResultDto<HospitalListDto>> GetPaginado(GetHospitalInput input);

        Task<HospitalInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<HospitalInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForHospital(GetPessoaExceptForHospital input);

        Task<PagedResultDto<EspecialidadeListDto>> GetEspecialidadeExceptForHospital(GetEspecialidadeExceptForHospital input);
    }
}
