﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IRedeCredenciadaAppService : IApplicationService
    {
        Task<IdInput> Save(RedeCredenciadaInput input);

        Task<RedeCredenciadaInput> SaveAndReturnEntity(RedeCredenciadaInput input);

        Task<PagedResultDto<RedeCredenciadaListDto>> GetPaginado(GetRedeCredenciadaInput input);

        Task<RedeCredenciadaInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<RedeCredenciadaInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<UnidadeDeSaudeBaseListDto>> GetUnidadeDeSaudeExceptForItemRedeCredenciada(GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput input);
    }
}
