﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IItemDeRedeCredenciadaAppService : IApplicationService
    {
        Task<IdInput> Save(ItemDeRedeCredenciadaInput input);

        Task<ItemDeRedeCredenciadaInput> SaveAndReturnEntity(ItemDeRedeCredenciadaInput input);

        Task<PagedResultDto<ItemDeRedeCredenciadaListDto>> GetPaginado(GetItemDeRedeCredenciadaInput input);

        Task<ItemDeRedeCredenciadaInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ItemDeRedeCredenciadaInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<ItemDeRedeCredenciadaListDto>> GetPaginadoByRedeCredenciada(GetItemDeRedeCredenciadaInput input);
        Task<IEnumerable<ItemDeRedeCredenciadaListDto>> GetItemsByRedeCredenciada(int redeCredenciadaId);
    }
}
