﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDeSaude;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IDeclaracaoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(DeclaracaoDeSaudeInput input);

        Task<DeclaracaoDeSaudeInput> SaveAndReturnEntity(DeclaracaoDeSaudeInput input);

        Task<PagedResultDto<DeclaracaoDeSaudeListDto>> GetDeclaracoesDeSaudePaginado(GetDeclaracaoDeSaudeInput input);

        Task<DeclaracaoDeSaudeInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<DeclaracaoDeSaudeInput> GetByExternalId(IdInput input);

        Task ResponderTodosQuestionarios(DeclaracaoDeSaudeInput input);
    }
}
