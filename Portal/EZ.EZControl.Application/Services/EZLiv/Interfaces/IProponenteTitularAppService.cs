﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IProponenteTitularAppService : IApplicationService
    {
        Task<IdInput> Save(ProponenteTitularInput input);

        Task<ProponenteTitularInput> SaveAndReturnEntity(ProponenteTitularInput input);

        Task<PagedResultDto<ProponenteTitularListDto>> GetPaginado(GetProponenteTitularInput input);

        Task<ProponenteTitularInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ProponenteTitularInput> GetByExternalId(IdInput input);
    }
}
