﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IUnidadeDeSaudeAppService : IApplicationService
    {
        Task<PagedResultDto<UnidadeDeSaudeBaseListDto>> GetPaginado(GetUnidadeDeSaudeBaseInput input);
        Task<UnidadeDeSaudeBaseInput> GetById(IdInput input);
        Task<UnidadeDeSaudeBaseInput> GetByExternalId(IdInput input);
        Task<PagedResultDto<UnidadeDeSaudeBaseListDto>> GetPaginadoExceptForIds(GetUnidadeDeSaudeBaseInput input);
    }
}
