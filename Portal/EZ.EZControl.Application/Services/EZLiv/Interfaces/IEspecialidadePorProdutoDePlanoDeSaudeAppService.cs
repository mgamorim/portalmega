﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IEspecialidadePorProdutoDePlanoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(EspecialidadePorProdutoDePlanoDeSaudeInput input);

        Task<EspecialidadePorProdutoDePlanoDeSaudeInput> SaveAndReturnEntity(EspecialidadePorProdutoDePlanoDeSaudeInput input);

        Task<PagedResultDto<EspecialidadePorProdutoDePlanoDeSaudeListDto>> GetPaginado(GetEspecialidadePorProdutoDePlanoDeSaudeInput input);

        Task<PagedResultDto<EspecialidadePorProdutoDePlanoDeSaudeListDto>> GetPaginadoByProdutoDePlanoDeSaudeId(GetEspecialidadePorProdutoDePlanoDeSaudeInput input);

        Task<EspecialidadePorProdutoDePlanoDeSaudeInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<EspecialidadePorProdutoDePlanoDeSaudeInput> GetByExternalId(IdInput input);
    }
}
