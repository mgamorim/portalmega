﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.HomologadorAdministradora;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IHomologadorAdministradoraAppService : IApplicationService
    {
        Task<IdInput> Save(HomologadorAdministradoraInput input);

        Task<HomologadorAdministradoraInput> SaveAndReturnEntity(HomologadorAdministradoraInput input);

        Task<PagedResultDto<HomologadorAdministradoraListDto>> GetHomologadorAdministradorasPaginado(GetHomologadorAdministradoraInput input);

        Task<HomologadorAdministradoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<HomologadorAdministradoraInput> GetByExternalId(IdInput input);
    }
}
