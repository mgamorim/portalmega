﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IGerenteCorretoraAppService : IApplicationService
    {
        Task<IdInput> Save(GerenteCorretoraInput input);

        Task<GerenteCorretoraInput> SaveAndReturnEntity(GerenteCorretoraInput input);

        Task<PagedResultDto<GerenteCorretoraListDto>> GetGerenteCorretorasPaginado(GetGerenteCorretoraInput input);

        Task<PagedResultDto<GerenteCorretoraListDto>> GetGerentesByCorretoraId(GetGerenteCorretoraInput input);

        Task<GerenteCorretoraInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<GerenteCorretoraInput> GetByExternalId(IdInput input);
    }
}
