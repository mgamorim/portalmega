﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IAssociacaoAppService : IApplicationService
    {
        Task<IdInput> Save(AssociacaoInput input);

        Task<AssociacaoInput> SaveAndReturnEntity(AssociacaoInput input);

        Task<PagedResultDto<AssociacaoListDto>> GetPaginado(GetAssociacaoInput input);

        Task<AssociacaoInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<AssociacaoInput> GetByExternalId(IdInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForAssociacao(GetPessoaExceptForAssociacao input);

        Task<PagedResultDto<AssociacaoListDto>> GetByProfissaoId(IdInput input);
    }
}
