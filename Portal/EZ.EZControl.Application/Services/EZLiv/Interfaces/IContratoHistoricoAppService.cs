﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.ContratoHistorico;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IContratoHistoricoAppService : IApplicationService
    {
        Task<IdInput> Save(ContratoHistoricoInput input);

        Task<ContratoHistoricoInput> SaveAndReturnEntity(ContratoHistoricoInput input);

        Task<PagedResultDto<ContratoHistoricoListDto>> GetContratoHistoricoPaginado(GetContratoHistoricoInput input);

        Task<ContratoHistoricoInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ContratoHistoricoInput> GetByExternalId(IdInput input);
    }
}
