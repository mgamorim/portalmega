﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv.Interfaces
{
    public interface IPlanoDeSaudeAppService : IApplicationService
    {
        Task<IdInput> Save(PlanoDeSaudeInput input);

        Task<PlanoDeSaudeInput> SaveAndReturnEntity(PlanoDeSaudeInput input);

        Task<PagedResultDto<PlanoDeSaudeListDto>> GetPlanosDeSaudePaginado(GetPlanoDeSaudeInput input);

        Task<PlanoDeSaudeInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<PlanoDeSaudeInput> GetByExternalId(IdInput input);
    }
}
