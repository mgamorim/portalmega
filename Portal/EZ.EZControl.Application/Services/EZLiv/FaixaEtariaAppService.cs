﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class FaixaEtariaAppService : EZControlAppServiceBase<FaixaEtaria>, IFaixaEtariaAppService
    {
        private readonly IFaixaEtariaService _faixaEtariaService;

        public FaixaEtariaAppService(IRepository<FaixaEtaria> faixaEtariaRepository,
                                          IFaixaEtariaService faixaEtariaService)
            : base(faixaEtariaRepository)
        {
            this._faixaEtariaService = faixaEtariaService;
        }

        private void ValidateInput(FaixaEtariaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
                validationErrors.Add(new ValidationResult(L("FaixaEtaria.Descricao")));

            if (input.IdadeFinal != null && input.IdadeInicial > input.IdadeFinal.Value)
                validationErrors.Add(new ValidationResult(L("FaixaEtaria.IdadeFinalError")));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao))
                validationErrors.Add(new ValidationResult(string.Format(L("FaixaEtaria.DuplicateDescricaoError"), input.Descricao), new[] { "descricao".ToCamelCase() }));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Create, AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Edit)]
        public async Task<IdInput> Save(FaixaEtariaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, FaixaEtariaInput>(_faixaEtariaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, FaixaEtariaInput>(_faixaEtariaService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Create, AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Edit)]
        public async Task<FaixaEtariaInput> SaveAndReturnEntity(FaixaEtariaInput input)
        {
            FaixaEtariaInput faixaEtaria = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                faixaEtaria = await base.CreateAndReturnEntity<FaixaEtariaInput, FaixaEtariaInput>(_faixaEtariaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                faixaEtaria = await base.UpdateAndReturnEntity<FaixaEtariaInput, FaixaEtariaInput>(_faixaEtariaService, input, () => ValidateInput(input));
            }

            return faixaEtaria;
        }

        public async Task<PagedResultDto<FaixaEtariaListDto>> GetPaginado(GetFaixaEtariaInput input)
        {
            var condicoes = new List<WhereIfCondition<FaixaEtaria>>
            {
                new WhereIfCondition<FaixaEtaria>(!input.Descricao.IsNullOrEmpty(), r => r.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<FaixaEtariaListDto, GetFaixaEtariaInput>(_faixaEtariaService, input, condicoes);
            return result;
        }

        public async Task<PagedResultDto<FaixaEtariaListDto>> GetPaginadoExceptForIds(GetFaixaEtariaExceptForIdsInput input)
        {
            var faixas = await Repository
                                .GetAll()
                                .Where(x => input.Ids.All(id => id != x.Id))
                                .ToListAsync();

            var listDtos = faixas.MapTo<List<FaixaEtariaListDto>>();
            return new PagedResultDto<FaixaEtariaListDto>(listDtos.Count, listDtos);
        }

        public async Task<FaixaEtariaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<FaixaEtariaInput, IdInput>(_faixaEtariaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_FaixaEtaria_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_faixaEtariaService, input);
        }

        public async Task<FaixaEtariaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<FaixaEtariaInput, IdInput>(_faixaEtariaService, input);
        }
    }
}