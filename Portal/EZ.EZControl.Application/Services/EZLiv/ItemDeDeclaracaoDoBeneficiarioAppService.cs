﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    public class ItemDeDeclaracaoDoBeneficiarioAppService : EZControlAppServiceBase<ItemDeDeclaracaoDoBeneficiario>, IItemDeDeclaracaoDoBeneficiarioAppService
    {
        private readonly IItemDeDeclaracaoDoBeneficiarioService _itemDeDeclaracaoDoBeneficiarioService;
        private readonly IDeclaracaoDoBeneficiarioService _declaracaoDoBeneficiarioService;

        public ItemDeDeclaracaoDoBeneficiarioAppService(IRepository<ItemDeDeclaracaoDoBeneficiario> _itemDeDeclaracaoDoBeneficiarioRepository,
                                          IItemDeDeclaracaoDoBeneficiarioService _itemDeDeclaracaoDoBeneficiarioService,
                                          IDeclaracaoDoBeneficiarioService declaracaoDoBeneficiarioService)
            : base(_itemDeDeclaracaoDoBeneficiarioRepository)
        {
            this._itemDeDeclaracaoDoBeneficiarioService = _itemDeDeclaracaoDoBeneficiarioService;
            this._declaracaoDoBeneficiarioService = declaracaoDoBeneficiarioService;
        }

        private void ValidateInput(ItemDeDeclaracaoDoBeneficiarioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Ordenacao == 0)
                validationErrors.Add(new ValidationResult(L("ItemDeDeclaracaoDeSaude.OrdenacaoMaiorQueZero")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ItemDeDeclaracaoDoBeneficiario _itemDeDeclaracaoDoBeneficiario, ItemDeDeclaracaoDoBeneficiarioInput _itemDeDeclaracaoDoBeneficiarioInput)
        {
            _itemDeDeclaracaoDoBeneficiario.TenantId = AbpSession.TenantId;
            var declaracao = await _declaracaoDoBeneficiarioService.FirstOrDefault(x => x.ItensDeDeclaracaoDoBeneficiario.Any(y => y.Id == _itemDeDeclaracaoDoBeneficiarioInput.Id));

            if (declaracao != null)
            {
                var existeValorOrdenacao = declaracao.ItensDeDeclaracaoDoBeneficiario.Any(x => x.Ordenacao == _itemDeDeclaracaoDoBeneficiarioInput.Ordenacao && _itemDeDeclaracaoDoBeneficiarioInput.Id != x.Id);

                if (existeValorOrdenacao)
                    throw new UserFriendlyException(L("ItemDeDeclaracaoDeSaude.OrdemJaExistente"));
            }
        }

        public async Task<IdInput> Save(ItemDeDeclaracaoDoBeneficiarioInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ItemDeDeclaracaoDoBeneficiarioInput>(_itemDeDeclaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ItemDeDeclaracaoDoBeneficiario_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ItemDeDeclaracaoDoBeneficiarioInput>(_itemDeDeclaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<ItemDeDeclaracaoDoBeneficiarioInput> SaveAndReturnEntity(ItemDeDeclaracaoDoBeneficiarioInput input)
        {
            ItemDeDeclaracaoDoBeneficiarioInput result = null;

            if (input.Id == default(int))
                result = await base.CreateAndReturnEntity<ItemDeDeclaracaoDoBeneficiarioInput, ItemDeDeclaracaoDoBeneficiarioInput>(_itemDeDeclaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);
            else
                result = await base.UpdateAndReturnEntity<ItemDeDeclaracaoDoBeneficiarioInput, ItemDeDeclaracaoDoBeneficiarioInput>(_itemDeDeclaracaoDoBeneficiarioService, input, () => ValidateInput(input), ProcessInput);

            return result;
        }

        public async Task<PagedResultDto<ItemDeDeclaracaoDoBeneficiarioListDto>> GetItensDeDeclaracaoDoBeneficiarioPaginado(GetItemDeDeclaracaoDoBeneficiarioInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemDeDeclaracaoDoBeneficiario>>
            {
                new WhereIfCondition<ItemDeDeclaracaoDoBeneficiario>(!input.Pergunta.IsNullOrEmpty(), r => r.Pergunta.Contains(input.Pergunta))
            };
            var result = await base.GetListPaged<ItemDeDeclaracaoDoBeneficiarioListDto, GetItemDeDeclaracaoDoBeneficiarioInput>(_itemDeDeclaracaoDoBeneficiarioService, input, condicoes);
            return result;
        }

        public async Task<ItemDeDeclaracaoDoBeneficiarioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ItemDeDeclaracaoDoBeneficiarioInput, IdInput>(_itemDeDeclaracaoDoBeneficiarioService, input);
        }

        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_itemDeDeclaracaoDoBeneficiarioService, input);
        }

        public async Task<ItemDeDeclaracaoDoBeneficiarioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ItemDeDeclaracaoDoBeneficiarioInput, IdInput>(_itemDeDeclaracaoDoBeneficiarioService, input);
        }
    }
}
