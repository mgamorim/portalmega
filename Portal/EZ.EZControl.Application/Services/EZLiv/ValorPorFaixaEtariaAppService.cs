﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ValorPorFaixaEtariaAppService : EZControlAppServiceBase<ValorPorFaixaEtaria>, IValorPorFaixaEtariaAppService
    {
        private readonly IValorPorFaixaEtariaService _valorPorFaixaEtariaService;
        private readonly IContratoService _contratoService;
        private readonly IFaixaEtariaService _faixaEtariaService;
        private readonly IProponenteTitularService _proponenteTitularService;

        public ValorPorFaixaEtariaAppService(IRepository<ValorPorFaixaEtaria> valorPorFaixaEtariaRepository,
                                             IValorPorFaixaEtariaService valorPorFaixaEtariaService,
                                             IContratoService contratoService,
                                              IProponenteTitularService proponenteTitularService,
                                             IFaixaEtariaService faixaEtariaService)
            : base(valorPorFaixaEtariaRepository)
        {
            this._valorPorFaixaEtariaService = valorPorFaixaEtariaService;
            this._contratoService = contratoService;
            this._proponenteTitularService = proponenteTitularService;
            this._faixaEtariaService = faixaEtariaService;
        }

        private void ValidateInput(ValorPorFaixaEtariaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Valor == 0)
                validationErrors.Add(new ValidationResult(L("ValorPorFaixaEtaria.RequiredValor")));

            if (input.ContratoId == 0)
                validationErrors.Add(new ValidationResult(L("ValorPorFaixaEtaria.RequiredContrato")));

            if (input.FaixaEtariaId == 0)
                validationErrors.Add(new ValidationResult(L("ValorPorFaixaEtaria.RequiredFaixaEtaria")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ValorPorFaixaEtaria valorPorFaixaEtaria, ValorPorFaixaEtariaInput valorPorFaixaEtariaInput)
        {
            if (valorPorFaixaEtaria.ContratoId > 0)
            {
                try
                {
                    valorPorFaixaEtaria.Contrato = await _contratoService.GetById(valorPorFaixaEtaria.ContratoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("ValorPorFaixaEtaria.NotFoundContratoError"));
                }
            }

            if (valorPorFaixaEtaria.FaixaEtariaId > 0)
            {
                try
                {
                    valorPorFaixaEtaria.FaixaEtaria = await _faixaEtariaService.GetById(valorPorFaixaEtaria.FaixaEtariaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("ValorPorFaixaEtaria.NotFoundFaixaEtariaError"));
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Create, AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Edit)]
        public async Task<IdInput> Save(ValorPorFaixaEtariaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ValorPorFaixaEtariaInput>(_valorPorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ValorPorFaixaEtariaInput>(_valorPorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Create, AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Edit)]
        public async Task<ValorPorFaixaEtariaInput> SaveAndReturnEntity(ValorPorFaixaEtariaInput input)
        {
            ValorPorFaixaEtariaInput valorPorFaixaEtaria = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                valorPorFaixaEtaria = await base.CreateAndReturnEntity<ValorPorFaixaEtariaInput, ValorPorFaixaEtariaInput>(_valorPorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                valorPorFaixaEtaria = await base.UpdateAndReturnEntity<ValorPorFaixaEtariaInput, ValorPorFaixaEtariaInput>(_valorPorFaixaEtariaService, input, () => ValidateInput(input), ProcessInput);
            }

            return valorPorFaixaEtaria;
        }

        public async Task<PagedResultDto<ValorPorFaixaEtariaListDto>> GetPaginadoByContratoId(GetValorPorFaixaEtariaInput input)
        {
            var condicoes = new List<WhereIfCondition<ValorPorFaixaEtaria>>();

            if (input.ContratoId > 0)
                condicoes.Add(new WhereIfCondition<ValorPorFaixaEtaria>(true, r => r.ContratoId == input.ContratoId));
            else
                return null;

            if (input.Valor > 0)
                condicoes.Add(new WhereIfCondition<ValorPorFaixaEtaria>(true, r => r.Valor == input.Valor));

            var result = await base.GetListPaged<ValorPorFaixaEtariaListDto, GetValorPorFaixaEtariaInput>(_valorPorFaixaEtariaService, input, condicoes);
            return result;
        }

        public async Task<PagedResultDto<ValorPorFaixaEtariaListDto>> GetPaginado(GetValorPorFaixaEtariaInput input)
        {
            var condicoes = new List<WhereIfCondition<ValorPorFaixaEtaria>>();

            if (input.Valor > 0)
                condicoes.Add(new WhereIfCondition<ValorPorFaixaEtaria>(true, r => r.Valor == input.Valor));

            if (input.ContratoId > 0)
                condicoes.Add(new WhereIfCondition<ValorPorFaixaEtaria>(true, r => r.ContratoId == input.ContratoId));

            var result = await base.GetListPaged<ValorPorFaixaEtariaListDto, GetValorPorFaixaEtariaInput>(_valorPorFaixaEtariaService, input, condicoes);
            return result;
        }

        public async Task<ValorPorFaixaEtariaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ValorPorFaixaEtariaInput, IdInput>(_valorPorFaixaEtariaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ValorPorFaixaEtaria_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_valorPorFaixaEtariaService, input);
        }

        public async Task<ValorPorFaixaEtariaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ValorPorFaixaEtariaInput, IdInput>(_valorPorFaixaEtariaService, input);
        }
    }
}