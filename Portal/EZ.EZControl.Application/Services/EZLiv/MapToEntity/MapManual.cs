﻿using Abp.AutoMapper;
using EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora;
using EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EZ.EZControl.Authorization.Users.Dto.UserListDto;

namespace EZ.EZControl.Services.EZLiv.MapToEntity
{
    public class MapManual
    {
        public dynamic ListEntity_PropostaContratacao(dynamic listInput)
        {
            List<PropostaDeContratacaoInput> entityOutput = new List<PropostaDeContratacaoInput>();

            foreach (var itemInput in listInput)
            {
                var objOutPut = new PropostaDeContratacaoInput();
                objOutPut.Id = (int)itemInput.Id;
                objOutPut.Numero = itemInput.Numero;
                objOutPut.Titular = new ProponenteTitularInput();
                if(itemInput.Corretora != null)
                {
                    objOutPut.Corretora = new CorretoraInput();
                    objOutPut.Corretora.Nome = itemInput.Corretora.Nome;
                    if (itemInput.Corretor != null)
                    {
                        objOutPut.Corretora.Corretor = new CorretorInput();
                        if(itemInput.Corretor.Pessoa != null)
                        {
                            objOutPut.Corretora.Corretor.Pessoa = new PessoaInput();
                            objOutPut.Corretora.Corretor.Pessoa.NomePessoa = itemInput.Corretor.Pessoa.NomePessoa;
                        }
                        
                    }
                }                
                if (itemInput.Titular != null)
                {
                    objOutPut.Titular.PessoaFisica = new PessoaFisicaInput();
                    objOutPut.Titular.PessoaFisica.Nome = itemInput.Titular.PessoaFisica != null ? itemInput.Titular.PessoaFisica.Nome : "";
                }
                objOutPut.Produto = new ProdutoDePlanoDeSaudeInput();
                objOutPut.Produto.Operadora = new OperadoraInput();
                objOutPut.Produto.Operadora.PessoaJuridica = new PessoaJuridicaInput();
                objOutPut.Produto.Administradora = new AdministradoraInput();
                objOutPut.Produto.Administradora.PessoaJuridica = new PessoaJuridicaInput();
                if (itemInput.Produto != null)
                {
                    objOutPut.Produto.Nome = itemInput.Produto != null ? itemInput.Produto.Nome : "";
                    if (itemInput.Produto.Administradora != null)
                    {
                        objOutPut.Produto.Administradora.PessoaJuridica.NomePessoa = itemInput.Produto.Administradora.PessoaJuridica != null ? itemInput.Produto.Administradora.PessoaJuridica.NomePessoa : "";
                        objOutPut.Produto.Administradora.PessoaJuridica.RazaoSocial = itemInput.Produto.Administradora.PessoaJuridica != null ? itemInput.Produto.Administradora.PessoaJuridica.RazaoSocial : "";
                    }
                    if (itemInput.Produto.Operadora != null)
                    {
                        objOutPut.Produto.Operadora.Nome = itemInput.Produto.Operadora.Nome;
                        objOutPut.Produto.Operadora.PessoaJuridica.RazaoSocial = itemInput.Produto.Operadora.PessoaJuridica != null ? itemInput.Produto.Operadora.PessoaJuridica.RazaoSocial : "";
                    }
                }                
                objOutPut.ProdutoId = (int?)itemInput.ProdutoId ?? 0;    
                objOutPut.Aceite = itemInput.Aceite;
                objOutPut.TipoDeHomologacao = itemInput.TipoDeHomologacao;
                objOutPut.ObservacaoHomologacao = itemInput.ObservacaoHomologacao;
                objOutPut.PassoDaProposta = itemInput.PassoDaProposta;
                objOutPut.StatusDaProposta = itemInput.StatusDaProposta;
                objOutPut.TipoDeProposta = itemInput.TipoDeProposta;
                objOutPut.AceiteCorretor = itemInput.AceiteCorretor;

                entityOutput.Add(objOutPut);
            }

            return entityOutput;
        }


        public dynamic ListEntity_PermissaoVendaPlanoSaudeCorretora(dynamic listInput)
        {
            List<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto> entityOutput = new List<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto>();

            foreach (var itemInput in listInput)
            {
                var objOutPut = new PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto();

                objOutPut.AdministradoraId = itemInput.AdministradoraId;
                objOutPut.EmpresaId = itemInput.EmpresaId;
                objOutPut.ExternalId = itemInput.ExternalId;
                objOutPut.Id = (int)itemInput.Id;
                objOutPut.IsActive = itemInput.IsActive;
                objOutPut.Nome = itemInput.Nome;
               // objOutPut.TenantId = itemInput.TenantId;

              //  objOutPut.Corretoras = itemInput.Corretoras.MapTo<List<CorretoraInput>>();
                //objOutPut.Administradora = itemInput.Administradora;
                //objOutPut.Empresa = itemInput.Empresa;
                //objOutPut.Produtos = itemInput.Produtos.MapTo<List<ProdutoDePlanoDeSaudeListDto>>(); 

                entityOutput.Add(objOutPut);
            }

            return entityOutput;
        }

        public async Task<PropostaDeContratacaoInput> MapManualTask(PropostaDeContratacao entity)
        {
            try
            {
                var input = new PropostaDeContratacaoInput();


                //   Corretor = entity.Corretor.MapTo<CorretorInput>(),
                input.Corretor = new CorretorInput();

                input.GerenteId = entity.GerenteId;
                input.SupervisorId = entity.SupervisorId;
                input.Corretor.Id = entity.Corretor.Id;
                input.Corretor.ExternalId = entity.Corretor.ExternalId;
                //input.Corretor.Corretoras = entity.Corretor.Corretoras;
                input.Corretor.IsActive = entity.Corretor.IsActive;
                //input.Corretor.Pessoa = entity.Corretor.Pessoa;

                input.ContratoVigenteNoAceite = entity.ContratoVigenteNoAceite.MapTo<ContratoInput>();

                //  Chancela = entity.Chancela.MapTo<ChancelaInput>(),
                input.Chancela = new ChancelaInput();

                if (entity.Chancela != null)
                {
                    input.Chancela = new ChancelaInput()
                    {
                        Nome = entity.Chancela.Nome,
                        TaxaDeAdesao = entity.Chancela.TaxaDeAdesao,
                        TaxaMensal = entity.Chancela.TaxaMensal,
                        Id = entity.Chancela.Id,
                        AssociacaoId = entity.Chancela.AssociacaoId

                    };

                }
                else
                {
                    if (entity.Produto != null)
                    {
                        if (entity.Produto.Chancelas != null)
                        {
                            input.Chancela = new ChancelaInput()
                            {
                                Nome = entity.Produto.Chancelas.First().Nome,
                                TaxaDeAdesao = entity.Produto.Chancelas.First().TaxaDeAdesao,
                                TaxaMensal = entity.Produto.Chancelas.First().TaxaMensal,
                                Id = entity.Produto.Chancelas.First().Id,
                                AssociacaoId = entity.Produto.Chancelas.First().AssociacaoId

                            };
                        }
                    }
                }

                input.Pedido = entity.Pedido.MapTo<PedidoInput>();
                input.PedidoId = entity.PedidoId;
                input.Estado = entity.Estado.MapTo<EstadoInput>();
                input.EstadoId = entity.EstadoId == null ? 0 : (int)entity.EstadoId;
                input.TipoDeHomologacao = entity.TipoDeHomologacao;
                input.ObservacaoHomologacao = entity.ObservacaoHomologacao;


                // Corretora = entity.Corretora.MapTo<CorretoraInput>(),
                input.Corretora = new CorretoraInput();




                input.UsuarioTitular = entity.UsuarioTitular.MapTo<UserDto>();
                // input.UsuarioTitular = new UserDto();
                // input.UsuarioTitular.Id = entity.UsuarioTitular.Id;


                //input.UsuarioResponsavel = new UserDto();
                input.UsuarioResponsavel = entity.UsuarioResponsavel.MapTo<UserDto>();
                input.Profissao = entity.Profissao.MapTo<ProfissaoInput>();

                


                input.ChancelaId = entity.ChancelaId;
                input.CorretorId = entity.CorretorId;

                input.CorretoraId = entity.CorretoraId;
                input.EstadoId = entity.EstadoId.HasValue ? entity.EstadoId.Value : 0;
                input.Id = entity.Id;
                input.TitularId = entity.TitularId;
                input.TitularResponsavelLegal = entity.TitularResponsavelLegal;
                input.UsuarioTitularId = entity.UsuarioTitularId.HasValue ? (int)entity.UsuarioTitularId : 0;

                input.UsuarioResponsavelId = entity.UsuarioResponsavelId.HasValue ? (int)entity.UsuarioResponsavelId : 0;
                input.Numero = entity.Numero;
                input.PassoDaProposta = entity.PassoDaProposta;

                input.ProfissaoId = entity.ProfissaoId.HasValue ? entity.ProfissaoId.Value : 0;
                input.StatusDaProposta = entity.StatusDaProposta;
                input.TipoDeProposta = entity.TipoDeProposta;
                input.InicioDeVigencia = entity.InicioDeVigencia;
                input.VigenciaId = entity.VigenciaId;
                input.Aceite = entity.Aceite;
                input.AceiteCorretor = entity.AceiteCorretor;


                input.Titular = new ProponenteTitularInput();

                input.Titular.Id = entity.Titular.Id;

                input.Titular.NomeDaMae = entity.Titular.NomeDaMae;
                input.Titular.matricula = entity.Titular.matricula;
                input.Titular.boleto = entity.Titular.boleto;
                input.Titular.DebitoConta = entity.Titular.DebitoConta;
                input.Titular.folha = entity.Titular.folha;
                input.Titular.FolhaFicha = entity.Titular.FolhaFicha;
                input.Titular.NumeroDoCartaoNacionalDeSaude = entity.Titular.NumeroDoCartaoNacionalDeSaude;
                input.Titular.DeclaracaoDeNascidoVivo = entity.Titular.DeclaracaoDeNascidoVivo;

                //input.Titular.Responsavel = entity.Titular.Responsavel.MapTo<ResponsavelInput>();
                input.Responsavel = entity.Responsavel.MapTo<ResponsavelInput>();
                //input.Responsavel.TipoDeResponsavel = entity.Responsavel.TipoDeResponsavel;

                input.Titular.PessoaFisica = new PessoaFisicaInput();

                input.Titular.PessoaFisica.TelefoneCelular = entity.Titular.PessoaFisica.TelefoneCelular.MapTo<TelefoneInput>();
                input.Titular.PessoaFisica.EmailPrincipal = entity.Titular.PessoaFisica.EmailPrincipal != null ? entity.Titular.PessoaFisica.EmailPrincipal.MapTo<EnderecoEletronicoInput>() : new EnderecoEletronicoInput();
                input.Titular.PessoaFisica.Cpf = entity.Titular.PessoaFisica.Cpf;
                input.Titular.PessoaFisica.CpfId = entity.Titular.PessoaFisica.CpfId;
                input.Titular.PessoaFisica.DataDeNascimento = entity.Titular.PessoaFisica.DataDeNascimento;
                input.Titular.PessoaFisica.EmailId = entity.Titular.PessoaFisica.EmailPrincipal != null ? entity.Titular.PessoaFisica.EmailPrincipal.Id : 0;
                input.Titular.PessoaFisica.EstadoCivil = entity.Titular.PessoaFisica.EstadoCivil;
                input.Titular.PessoaFisica.Nacionalidade = entity.Titular.PessoaFisica.Nacionalidade;
                input.Titular.PessoaFisica.Nome = entity.Titular.PessoaFisica.Nome;
                input.Titular.PessoaFisica.Rg = entity.Titular.PessoaFisica.Rg;
                input.Titular.PessoaFisica.RgId = entity.Titular.PessoaFisica.RgId;
                input.Titular.PessoaFisica.Sexo = entity.Titular.PessoaFisica.Sexo;
                input.Titular.PessoaFisica.OrgaoExpedidor = entity.Titular.PessoaFisica.OrgaoExpedidor;

                input.Titular.Pessoa = new PessoaInput();

                input.Titular.Pessoa.EnderecoPrincipal = entity.Titular.Pessoa.EnderecoPrincipal != null ? entity.Titular.Pessoa.EnderecoPrincipal.MapTo<EnderecoInput>() : null;

                if (input.Titular.Pessoa.EnderecoPrincipal == null)
                {
                    if (entity.Titular.Pessoa.Enderecos == null)
                    {
                        input.Titular.Pessoa.EnderecoPrincipal = new EnderecoInput();
                    }
                    else
                    {
                        input.Titular.Pessoa.EnderecoPrincipal = entity.Titular.Pessoa.Enderecos.Count > 0 ? entity.Titular.Pessoa.Enderecos.First().MapTo<EnderecoInput>() : new EnderecoInput();
                        //var teste = entity.Titular.Pessoa.Enderecos.First().MapTo<EnderecoInput>()
                    }
                }

                input.Produto = entity.Produto.MapTo<ProdutoDePlanoDeSaudeInput>();

                if (input.Produto != null)
                {
                    input.ProdutoId = (int)entity.ProdutoId;
                    input.AceiteDaDeclaracaoDeSaude = entity.AceiteDaDeclaracaoDeSaude;
                    input.ItemDeDeclaracaoDoBeneficiarioId = entity.ItemDeDeclaracaoDoBeneficiarioId;
                }



                input.Documentos = new List<ArquivoDocumentoInput>();

                foreach (var item in entity.Documentos)
                {

                    var arquivo = new ArquivoDocumentoInput();
                    arquivo.Id = item.Id;
                    arquivo.Conteudo = item.Conteudo;
                    arquivo.EmExigencia = item.EmExigencia;
                    arquivo.IsActive = item.IsActive;
                    arquivo.IsImagem = item.IsImagem;
                    arquivo.Motivo = item.Motivo;
                    arquivo.Nome = item.Nome;
                    arquivo.Path = item.Path;
                    arquivo.PropostaDeContratacaoId = item.PropostaDeContratacaoId;
                    arquivo.Tipo = item.Tipo;
                    arquivo.TipoDeArquivoFixo = item.TipoDeArquivoFixo;
                    arquivo.Token = item.Token;

                    input.Documentos.Add(arquivo);
                }

                //var respostasTitular = _respostaService.GetAll().Where(x => x.BeneficiarioId == input.Titular.Id && x.PropostaDeContratacaoId == entity.Id).ToListAsync();
                //// input.Titular.RespostasDeclaracaoDeSaude = respostasTitular.MapTo<List<RespostaDeDeclaracaoDeSaudeInput>>();


                //if (respostasTitular.Result.Count > 0)
                //{
                //    input.Titular.RespostasDeclaracaoDeSaude = new List<RespostaDeDeclaracaoDeSaudeInput>();

                //    foreach (var resposta in respostasTitular.Result)
                //    {
                //        var respostaunica = new RespostaDeDeclaracaoDeSaudeInput();
                //        respostaunica.AnoDoEvento = resposta.AnoDoEvento;
                //        respostaunica.Id = resposta.Id;

                //        respostaunica.Marcada = resposta.Marcada;
                //        respostaunica.Observacao = resposta.Observacao;
                //        respostaunica.Ordenacao = resposta.ItemDeDeclaracaoDeSaude.Ordenacao;
                //        respostaunica.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                //        respostaunica.TipoDeItemDeDeclaracao = resposta.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao;
                //        respostaunica.PropostaDeContratacaoId = resposta.PropostaDeContratacaoId;
                //        respostaunica.AnoDoEvento = resposta.AnoDoEvento;
                //        input.Titular.RespostasDeclaracaoDeSaude.Add(respostaunica);
                //    }
                //}

                input.Titular.Dependentes = new List<DependenteInput>();

                if (entity.Titular.Dependentes.Count >= 1)
                {
                    foreach (var dependente in entity.Titular.Dependentes)
                    {

                        var dep = new DependenteInput()
                        {
                            Id = dependente.Id,
                            GrauDeParentesco = dependente.GrauDeParentesco,
                            DeclaracaoDeNascidoVivo = dependente.DeclaracaoDeNascidoVivo,
                            NomeDaMae = dependente.NomeDaMae,
                            NumeroDoCartaoNacionalDeSaude = dependente.NumeroDoCartaoNacionalDeSaude,
                            TipoDeBeneficiario = dependente.TipoDeBeneficiario,
                            PessoaFisica = new PessoaFisicaInput()
                            {
                                EmailPrincipal = dependente.PessoaFisica.EmailPrincipal != null ? dependente.PessoaFisica.EmailPrincipal.MapTo<EnderecoEletronicoInput>() : new EnderecoEletronicoInput(),
                                Email = dependente.PessoaFisica.EmailPrincipal != null ? dependente.PessoaFisica.EmailPrincipal.MapTo<EnderecoEletronicoInput>().Endereco : "",
                                Cpf = dependente.PessoaFisica.Cpf,
                                CpfId = dependente.PessoaFisica.CpfId,
                                DataDeNascimento = dependente.PessoaFisica.DataDeNascimento,
                                EmailId = dependente.PessoaFisica.EmailPrincipal != null ? dependente.PessoaFisica.EmailPrincipal.Id : 0,
                                EstadoCivil = dependente.PessoaFisica.EstadoCivil,
                                Nacionalidade = dependente.PessoaFisica.Nacionalidade,
                                Nome = dependente.PessoaFisica.Nome,
                                CPFmae = dependente.PessoaFisica.CPFmae,
                                identidademae = dependente.PessoaFisica.identidademae,
                                identidade = dependente.PessoaFisica.identidade,
                                Sexo = dependente.PessoaFisica.Sexo

                            }
                        };


                        // var respostasDependentes =  _respostaService.GetAll().Where(x => x.BeneficiarioId == dep.Id && x.PropostaDeContratacaoId == entity.Id).ToListAsync();
                        ////dep.RespostasDeclaracaoDeSaude = respostasDependentes.MapTo<List<RespostaDeDeclaracaoDeSaudeInput>>();
                        //dep.RespostasDeclaracaoDeSaude = new List<RespostaDeDeclaracaoDeSaudeInput>();


                        //if (respostasDependentes.Result.Count > 0)
                        //{

                        //    foreach (var resposta in respostasDependentes.Result)
                        //    {
                        //        var respostaunica = new RespostaDeDeclaracaoDeSaudeInput();
                        //        respostaunica.AnoDoEvento = resposta.AnoDoEvento;
                        //        respostaunica.Id = resposta.Id;
                        //        respostaunica.Marcada = resposta.Marcada;
                        //        respostaunica.Observacao = resposta.Observacao;
                        //        respostaunica.Ordenacao = resposta.ItemDeDeclaracaoDeSaude.Ordenacao;
                        //        respostaunica.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                        //        respostaunica.TipoDeItemDeDeclaracao = resposta.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao;
                        //        dep.RespostasDeclaracaoDeSaude.Add(respostaunica);
                        //    }
                        //}

                        input.Titular.Dependentes.Add(dep);
                    }
                }

                return input;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public PropostaDeContratacaoInput MapManualProposta(PropostaDeContratacao entity)
        {
            try
            {
                var input = new PropostaDeContratacaoInput();


                //   Corretor = entity.Corretor.MapTo<CorretorInput>(),
                input.Corretor = new CorretorInput();

                input.Corretor.Id = entity.Corretor.Id;
                input.Corretor.ExternalId = entity.Corretor.ExternalId;
                //input.Corretor.Corretoras = entity.Corretor.Corretoras;
                input.Corretor.IsActive = entity.Corretor.IsActive;
                //input.Corretor.Pessoa = entity.Corretor.Pessoa;

                input.ContratoVigenteNoAceite = entity.ContratoVigenteNoAceite.MapTo<ContratoInput>();

                //  Chancela = entity.Chancela.MapTo<ChancelaInput>(),
                input.Chancela = new ChancelaInput();

                if (entity.Chancela != null)
                {
                    input.Chancela = new ChancelaInput()
                    {
                        Nome = entity.Chancela.Nome,
                        TaxaDeAdesao = entity.Chancela.TaxaDeAdesao,
                        TaxaMensal = entity.Chancela.TaxaMensal,
                        Id = entity.Chancela.Id,
                        AssociacaoId = entity.Chancela.AssociacaoId

                    };

                }
                else
                {


                    if (entity.Produto != null)
                    {
                        if (entity.Produto.Chancelas != null)
                        {
                            input.Chancela = new ChancelaInput()
                            {
                                Nome = entity.Produto.Chancelas.First().Nome,
                                TaxaDeAdesao = entity.Produto.Chancelas.First().TaxaDeAdesao,
                                TaxaMensal = entity.Produto.Chancelas.First().TaxaMensal,
                                Id = entity.Produto.Chancelas.First().Id,
                                AssociacaoId = entity.Produto.Chancelas.First().AssociacaoId

                            };


                        }
                    }


                }

                input.Pedido = entity.Pedido.MapTo<PedidoInput>();
                input.PedidoId = entity.PedidoId;
                input.Estado = entity.Estado.MapTo<EstadoInput>();
                input.EstadoId = entity.EstadoId == null ? 0 : (int)entity.EstadoId;
                input.TipoDeHomologacao = entity.TipoDeHomologacao;
                input.ObservacaoHomologacao = entity.ObservacaoHomologacao;

                // Corretora = entity.Corretora.MapTo<CorretoraInput>(),
                input.Corretora = new CorretoraInput();




                input.UsuarioTitular = entity.UsuarioTitular.MapTo<UserDto>();
                // input.UsuarioTitular = new UserDto();
                // input.UsuarioTitular.Id = entity.UsuarioTitular.Id;


                //input.UsuarioResponsavel = new UserDto();
                input.UsuarioResponsavel = entity.UsuarioResponsavel.MapTo<UserDto>();
                input.Profissao = entity.Profissao.MapTo<ProfissaoInput>();




                input.ChancelaId = entity.ChancelaId;
                input.CorretorId = entity.CorretorId;

                input.CorretoraId = entity.CorretoraId;
                input.EstadoId = entity.EstadoId.HasValue ? entity.EstadoId.Value : 0;
                input.Id = entity.Id;
                input.TitularId = entity.TitularId;
                input.TitularResponsavelLegal = entity.TitularResponsavelLegal;
                input.UsuarioTitularId = entity.UsuarioTitularId.HasValue ? (int)entity.UsuarioTitularId : 0;

                input.UsuarioResponsavelId = entity.UsuarioResponsavelId.HasValue ? (int)entity.UsuarioResponsavelId : 0;
                input.Numero = entity.Numero;
                input.PassoDaProposta = entity.PassoDaProposta;

                input.ProfissaoId = entity.ProfissaoId.HasValue ? entity.ProfissaoId.Value : 0;
                input.StatusDaProposta = entity.StatusDaProposta;
                input.TipoDeProposta = entity.TipoDeProposta;
                input.InicioDeVigencia = entity.InicioDeVigencia;
                input.VigenciaId = entity.VigenciaId;
                input.Aceite = entity.Aceite;
                input.AceiteCorretor = entity.AceiteCorretor;


                input.Titular = new ProponenteTitularInput();

                input.Titular.Id = entity.Titular.Id;

                input.Titular.NomeDaMae = entity.Titular.NomeDaMae;
                input.Titular.matricula = entity.Titular.matricula;
                input.Titular.boleto = entity.Titular.boleto;
                input.Titular.DebitoConta = entity.Titular.DebitoConta;
                input.Titular.folha = entity.Titular.folha;
                input.Titular.FolhaFicha = entity.Titular.FolhaFicha;
                input.Titular.NumeroDoCartaoNacionalDeSaude = entity.Titular.NumeroDoCartaoNacionalDeSaude;
                input.Titular.DeclaracaoDeNascidoVivo = entity.Titular.DeclaracaoDeNascidoVivo;


                input.Responsavel = entity.Responsavel.MapTo<ResponsavelInput>();

                input.Titular.PessoaFisica = new PessoaFisicaInput();

                input.Titular.PessoaFisica.TelefoneCelular = entity.Titular.PessoaFisica.TelefoneCelular.MapTo<TelefoneInput>();
                input.Titular.PessoaFisica.EmailPrincipal = entity.Titular.PessoaFisica.EmailPrincipal != null ? entity.Titular.PessoaFisica.EmailPrincipal.MapTo<EnderecoEletronicoInput>() : new EnderecoEletronicoInput();
                input.Titular.PessoaFisica.Cpf = entity.Titular.PessoaFisica.Cpf;
                input.Titular.PessoaFisica.CpfId = entity.Titular.PessoaFisica.CpfId;
                input.Titular.PessoaFisica.DataDeNascimento = entity.Titular.PessoaFisica.DataDeNascimento;
                input.Titular.PessoaFisica.EmailId = entity.Titular.PessoaFisica.EmailPrincipal != null ? entity.Titular.PessoaFisica.EmailPrincipal.Id : 0;
                input.Titular.PessoaFisica.EstadoCivil = entity.Titular.PessoaFisica.EstadoCivil;
                input.Titular.PessoaFisica.Nacionalidade = entity.Titular.PessoaFisica.Nacionalidade;
                input.Titular.PessoaFisica.Nome = entity.Titular.PessoaFisica.Nome;
                input.Titular.PessoaFisica.Rg = entity.Titular.PessoaFisica.Rg;
                input.Titular.PessoaFisica.RgId = entity.Titular.PessoaFisica.RgId;
                input.Titular.PessoaFisica.Sexo = entity.Titular.PessoaFisica.Sexo;
                input.Titular.PessoaFisica.OrgaoExpedidor = entity.Titular.PessoaFisica.OrgaoExpedidor;

                input.Titular.Pessoa = new PessoaInput();

                input.Titular.Pessoa.EnderecoPrincipal = entity.Titular.Pessoa.EnderecoPrincipal != null ? entity.Titular.Pessoa.EnderecoPrincipal.MapTo<EnderecoInput>() : null;

                if (input.Titular.Pessoa.EnderecoPrincipal == null)
                {
                    if (entity.Titular.Pessoa.Enderecos == null)
                    {
                        input.Titular.Pessoa.EnderecoPrincipal = new EnderecoInput();
                    }
                    else
                    {
                        input.Titular.Pessoa.EnderecoPrincipal = entity.Titular.Pessoa.Enderecos.Count > 0 ? entity.Titular.Pessoa.Enderecos.First().MapTo<EnderecoInput>() : new EnderecoInput();
                        //var teste = entity.Titular.Pessoa.Enderecos.First().MapTo<EnderecoInput>()
                    }
                }

                input.Produto = entity.Produto.MapTo<ProdutoDePlanoDeSaudeInput>();

                if (input.Produto != null)
                {
                    input.ProdutoId = (int)entity.ProdutoId;
                    input.AceiteDaDeclaracaoDeSaude = entity.AceiteDaDeclaracaoDeSaude;
                    input.ItemDeDeclaracaoDoBeneficiarioId = entity.ItemDeDeclaracaoDoBeneficiarioId;
                }



                input.Documentos = new List<ArquivoDocumentoInput>();

                if (entity.Documentos != null)
                {
                    foreach (var item in entity.Documentos)
                    {

                        var arquivo = new ArquivoDocumentoInput();
                        arquivo.Id = item.Id;
                        arquivo.Conteudo = item.Conteudo;
                        arquivo.EmExigencia = item.EmExigencia;
                        arquivo.IsActive = item.IsActive;
                        arquivo.IsImagem = item.IsImagem;
                        arquivo.Motivo = item.Motivo;
                        arquivo.Nome = item.Nome;
                        arquivo.Path = item.Path;
                        arquivo.PropostaDeContratacaoId = item.PropostaDeContratacaoId;
                        arquivo.Tipo = item.Tipo;
                        arquivo.TipoDeArquivoFixo = item.TipoDeArquivoFixo;
                        arquivo.Token = item.Token;

                        input.Documentos.Add(arquivo);
                    }
                }


                //var respostasTitular = _respostaService.GetAll().Where(x => x.BeneficiarioId == input.Titular.Id && x.PropostaDeContratacaoId == entity.Id).ToListAsync();
                //// input.Titular.RespostasDeclaracaoDeSaude = respostasTitular.MapTo<List<RespostaDeDeclaracaoDeSaudeInput>>();


                //if (respostasTitular.Result.Count > 0)
                //{
                //    input.Titular.RespostasDeclaracaoDeSaude = new List<RespostaDeDeclaracaoDeSaudeInput>();

                //    foreach (var resposta in respostasTitular.Result)
                //    {
                //        var respostaunica = new RespostaDeDeclaracaoDeSaudeInput();
                //        respostaunica.AnoDoEvento = resposta.AnoDoEvento;
                //        respostaunica.Id = resposta.Id;

                //        respostaunica.Marcada = resposta.Marcada;
                //        respostaunica.Observacao = resposta.Observacao;
                //        respostaunica.Ordenacao = resposta.ItemDeDeclaracaoDeSaude.Ordenacao;
                //        respostaunica.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                //        respostaunica.TipoDeItemDeDeclaracao = resposta.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao;
                //        respostaunica.PropostaDeContratacaoId = resposta.PropostaDeContratacaoId;
                //        respostaunica.AnoDoEvento = resposta.AnoDoEvento;
                //        input.Titular.RespostasDeclaracaoDeSaude.Add(respostaunica);
                //    }
                //}

                input.Titular.Dependentes = new List<DependenteInput>();

                if (entity.Titular.Dependentes.Count >= 1)
                {
                    foreach (var dependente in entity.Titular.Dependentes)
                    {

                        var dep = new DependenteInput()
                        {
                            Id = dependente.Id,
                            GrauDeParentesco = dependente.GrauDeParentesco,
                            DeclaracaoDeNascidoVivo = dependente.DeclaracaoDeNascidoVivo,
                            NomeDaMae = dependente.NomeDaMae,
                            NumeroDoCartaoNacionalDeSaude = dependente.NumeroDoCartaoNacionalDeSaude,
                            TipoDeBeneficiario = dependente.TipoDeBeneficiario,
                            PessoaFisica = new PessoaFisicaInput()
                            {
                                EmailPrincipal = dependente.PessoaFisica.EmailPrincipal != null ? dependente.PessoaFisica.EmailPrincipal.MapTo<EnderecoEletronicoInput>() : new EnderecoEletronicoInput(),
                                Email = dependente.PessoaFisica.EmailPrincipal != null ? dependente.PessoaFisica.EmailPrincipal.MapTo<EnderecoEletronicoInput>().Endereco : "",
                                Cpf = dependente.PessoaFisica.Cpf,
                                CpfId = dependente.PessoaFisica.CpfId,
                                DataDeNascimento = dependente.PessoaFisica.DataDeNascimento,
                                EmailId = dependente.PessoaFisica.EmailPrincipal != null ? dependente.PessoaFisica.EmailPrincipal.Id : 0,
                                EstadoCivil = dependente.PessoaFisica.EstadoCivil,
                                Nacionalidade = dependente.PessoaFisica.Nacionalidade,
                                Nome = dependente.PessoaFisica.Nome,
                                CPFmae = dependente.PessoaFisica.CPFmae,
                                identidademae = dependente.PessoaFisica.identidademae,
                                identidade = dependente.PessoaFisica.identidade,
                                Sexo = dependente.PessoaFisica.Sexo
                            }
                        };


                        // var respostasDependentes =  _respostaService.GetAll().Where(x => x.BeneficiarioId == dep.Id && x.PropostaDeContratacaoId == entity.Id).ToListAsync();
                        ////dep.RespostasDeclaracaoDeSaude = respostasDependentes.MapTo<List<RespostaDeDeclaracaoDeSaudeInput>>();
                        ////dep.RespostasDeclaracaoDeSaude = new List<RespostaDeDeclaracaoDeSaudeInput>();


                        //if (respostasDependentes.Result.Count > 0)
                        //{

                        //    foreach (var resposta in respostasDependentes.Result)
                        //    {
                        //        var respostaunica = new RespostaDeDeclaracaoDeSaudeInput();
                        //        respostaunica.AnoDoEvento = resposta.AnoDoEvento;
                        //        respostaunica.Id = resposta.Id;
                        //        respostaunica.Marcada = resposta.Marcada;
                        //        respostaunica.Observacao = resposta.Observacao;
                        //        respostaunica.Ordenacao = resposta.ItemDeDeclaracaoDeSaude.Ordenacao;
                        //        respostaunica.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                        //        respostaunica.TipoDeItemDeDeclaracao = resposta.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao;
                        //        dep.RespostasDeclaracaoDeSaude.Add(respostaunica);
                        //    }
                        //}

                        input.Titular.Dependentes.Add(dep);
                    }
                }

                return input;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public dynamic MapUserListDto(IQueryable<User> users)
        {
            var lst = new List<UserListDto>();
            var lstRole = new List<UserListRoleDto>();

            foreach (var item in users)
            {
                var userListDto = new UserListDto();
                userListDto.Id = item.Id;
                userListDto.Name = item.Name;
                userListDto.Surname = item.Surname;
                userListDto.UserName = item.UserName;
                userListDto.EmailAddress = item.EmailAddress;
                userListDto.PhoneNumber = item.PhoneNumber;
                userListDto.ProfilePictureId = item.ProfilePictureId;
                userListDto.IsEmailConfirmed = item.IsEmailConfirmed;
                userListDto.Roles = new List<UserListRoleDto>();
                //userListDto.Roles = item.Roles.MapTo<List<UserListRoleDto>>();
                userListDto.PessoaFisica = new PessoaFisicaListDto();                
                userListDto.TipoDeUsuario = new TipoDeUsuarioEnum();
                userListDto.TipoDeUsuario = item.TipoDeUsuario;
                userListDto.LastLoginTime = item.LastLoginTime;
                userListDto.IsActive = item.IsActive;
                userListDto.CreationTime = item.CreationTime;

                lst.Add(userListDto);
            }

            return lst;
        }

        //public ProdutoDePlanoDeSaudeInput ProdutoEntity_to_ProdutoPlanoDeSaudeInput(dynamic entity)
        //{
        //    var entityOutPut = new ProdutoDePlanoDeSaudeInput();
        //    entityOutPut.Nome = entity.Nome;
        //    entityOutPut.Descricao = entity.Descricao;
        //    entityOutPut.Valor = entity.Valor;

        //    entityOutPut.Administradora = new AdministradoraInput();
        //    entityOutPut.Administradora.PessoaJuridica = new PessoaJuridicaInput();
        //    entityOutPut.Administradora.PessoaJuridica.NomePessoa = entity.Administradora.PessoaJuridica.NomePessoa;
        //    entityOutPut.Administradora.PessoaJuridica.NomeFantasia = entity.Administradora.PessoaJuridica.NomeFantasia;

        //    entityOutPut.PlanoDeSaude = new PlanoDeSaudeInput();
        //    entityOutPut.PlanoDeSaude.Abrangencia = entity.PlanoDeSaude.Abrangencia;
        //    entityOutPut.PlanoDeSaude.Acomodacao = entity.PlanoDeSaude.Acomodacao;
        //    entityOutPut.PlanoDeSaude.ExternalId = entity.PlanoDeSaude.ExternalId;
        //    entityOutPut.PlanoDeSaude.Id = entity.PlanoDeSaude.Id;
        //    //entityOutPut.PlanoDeSaude.IsIntegration = entity.PlanoDeSaude.IsIntegration;
        //    entityOutPut.PlanoDeSaude.Reembolso = entity.PlanoDeSaude.Reembolso;
        //    entityOutPut.PlanoDeSaude.DateOfEditionIntegration = entity.PlanoDeSaude.DateOfEditionIntegration;

        //    entityOutPut.Operadora = new OperadoraInput();
        //    entityOutPut.Operadora.Nome = entity.Operadora.Nome;
        //    entityOutPut.Operadora.ImagemId = entityOutPut.Operadora.ImagemId;

        //    return entityOutPut;
        //}
    }
}
