﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Parametro;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ParametroEZLiv, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ParametroEZLivAppService : EZControlAppServiceBase<ParametroEZLiv>, IParametroEZLivAppService
    {

        private readonly IParametroEZLivService _parametroEZLivService;

        public ParametroEZLivAppService(IRepository<ParametroEZLiv> parametroRepository,
           IParametroEZLivService parametroEZLivService)
            : base(parametroRepository)
        {
            this._parametroEZLivService = parametroEZLivService;
        }


        private void ValidateInput(ParametroEZLivInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<ParametroEZLivInput> Get()
        {
            var condicoes = new List<WhereIfCondition<ParametroEZLiv>>();
            condicoes.Add(new WhereIfCondition<ParametroEZLiv>(true, x => x.Id > 0));
            var parametros = await base.GetList<ParametroEZLivInput>(_parametroEZLivService, condicoes, x => x.Id);

            return parametros.Items.FirstOrDefault();
        }

        public async Task<IdInput> Save(ParametroEZLivInput input)
        {
            var parametro = await Get();
            if (parametro != null)
                input.Id = parametro.Id;

            IdInput savedEntity;

            if (input.Id == default(int))
            {
                savedEntity = await base.CreateEntity<IdInput, ParametroEZLivInput>(_parametroEZLivService, input, () => ValidateInput(input), null);
            }
            else
            {
                savedEntity = await base.UpdateEntity<IdInput, ParametroEZLivInput>(_parametroEZLivService, input, () => ValidateInput(input), null);
            }

            return savedEntity;
        }
    }
}
