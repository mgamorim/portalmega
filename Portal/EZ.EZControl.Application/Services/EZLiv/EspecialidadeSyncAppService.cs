﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.Sync;
using EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EspecialidadeSyncAppService : EZControlAppServiceBase<EspecialidadeSync>, IEspecialidadeSyncAppService
    {
        private readonly IEspecialidadeSyncService _especialidadeSyncService;
        private readonly IProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;

        public EspecialidadeSyncAppService(IRepository<EspecialidadeSync> especialidadeSyncRepository,
                                          IEspecialidadeSyncService especialidadeSyncService,
                                          IProdutoDePlanoDeSaudeService produtoDePlanoDeSaudeService)
            : base(especialidadeSyncRepository)
        {
            this._especialidadeSyncService = especialidadeSyncService;
            _produtoDePlanoDeSaudeService = produtoDePlanoDeSaudeService;
        }

        private void ValidateInput(EspecialidadeSyncInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(string.Format(L("EspecialidadeSync.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(EspecialidadeSync especialidadeSync, EspecialidadeSyncInput especialidadeSyncInput)
        {
            if (especialidadeSyncInput.ProdutosDePlanoDeSaudeIds != null &&
                especialidadeSyncInput.ProdutosDePlanoDeSaudeIds.Any())
            {
                especialidadeSync.ProdutosDePlanoDeSaude = new List<ProdutoDePlanoDeSaude>();
                foreach (int id in especialidadeSyncInput.ProdutosDePlanoDeSaudeIds)
                {
                    try
                    {
                        var produtos = _produtoDePlanoDeSaudeService.GetAll().FirstOrDefault(x => x.Id == id);
                        especialidadeSync.ProdutosDePlanoDeSaude.Add(produtos);
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(L("Operadora.ProdutoDePlanoDeSaudeNotFound"), ex.Message);
                    }
                }
            }
            else
                throw new UserFriendlyException(L("Operadora.RequiredProdutoDePlanoDeSaudeError"));
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Create, AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Edit)]
        public async Task<IdInput> Save(EspecialidadeSyncInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, EspecialidadeSyncInput>(_especialidadeSyncService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, EspecialidadeSyncInput>(_especialidadeSyncService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Create, AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Edit)]
        public async Task<EspecialidadeSyncInput> SaveAndReturnEntity(EspecialidadeSyncInput input)
        {
            EspecialidadeSyncInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<EspecialidadeSyncInput, EspecialidadeSyncInput>(_especialidadeSyncService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<EspecialidadeSyncInput, EspecialidadeSyncInput>(_especialidadeSyncService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<EspecialidadeSyncInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EspecialidadeSyncInput, IdInput>(_especialidadeSyncService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_especialidadeSyncService, input);
        }

        public async Task<EspecialidadeSyncInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EspecialidadeSyncInput, IdInput>(_especialidadeSyncService, input);
        }

        public async Task<PagedResultDto<EspecialidadeSyncListDto>> GetPaginado(GetEspecialidadeSyncInput input)
        {
            var condicoes = new List<WhereIfCondition<EspecialidadeSync>>
            {
                new WhereIfCondition<EspecialidadeSync>(!string.IsNullOrEmpty(input.Nome),
                    x => x.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<EspecialidadeSyncListDto, GetEspecialidadeSyncInput>(_especialidadeSyncService, input, condicoes);

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Get, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByEstado(GetEspecialidadeSyncByEstadoDto estadoDto)
        {
            var listEspecialidades = await _especialidadeSyncService.GetEspecialidadeSyncByEstado(estadoDto.Estado);
            return new ListResultDto<EspecialidadeSyncInput>(listEspecialidades.MapTo<List<EspecialidadeSyncInput>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Get, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByEstadoDistinct(GetEspecialidadeSyncByEstadoDto estadoDto)
        {
            var listEspecialidades = await _especialidadeSyncService.GetEspecialidadeSyncByEstadoDistinct(estadoDto.Estado);
            return new ListResultDto<EspecialidadeSyncInput>(listEspecialidades.MapTo<List<EspecialidadeSyncInput>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Get, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByMunicipio(GetEspecialidadeSyncByMunicipioDto municipioDto)
        {
            var listEspecialidades = await _especialidadeSyncService.GetEspecialidadeSyncByMunicipio(municipioDto.Estado, municipioDto.Municipio);
            return new ListResultDto<EspecialidadeSyncInput>(listEspecialidades.MapTo<List<EspecialidadeSyncInput>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Get, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByMunicipioDistinct(GetEspecialidadeSyncByMunicipioDto municipioDto)
        {
            var listEspecialidades = await _especialidadeSyncService.GetEspecialidadeSyncByMunicipioDistinct(municipioDto.Estado, municipioDto.Municipio);
            return new ListResultDto<EspecialidadeSyncInput>(listEspecialidades.MapTo<List<EspecialidadeSyncInput>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_EspecialidadeSync_Get, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<ListResultDto<EspecialidadeSyncInput>> GetEspecialidadeSyncByNomeDistinct(GetEspecialidadeSyncByNomeDto nomeDto)
        {
            var listEspecialidades = await _especialidadeSyncService.GetEspecialidadeSyncByNomeDistinct(nomeDto.Nome);
            return new ListResultDto<EspecialidadeSyncInput>(listEspecialidades.MapTo<List<EspecialidadeSyncInput>>());
        }
    }
}
