﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorCorretora;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SupervisorCorretoraAppService : EZControlAppServiceBase<SupervisorCorretora>, ISupervisorCorretoraAppService
    {
        private readonly ICorretorService _corretorService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly ICorretoraService _corretoraService;
        private readonly IPessoaService _pessoaService;
        private readonly ISupervisorCorretoraService _supervisorService;
        private readonly IEmpresaService _empresaService;
        public SupervisorCorretoraAppService(IRepository<SupervisorCorretora> corretorRepository,
                                          ICorretorService corretorService,
                                          IPessoaFisicaService pessoaFisicaService,
                                          ICorretoraService corretoraService,
                                          IPessoaService pessoaService,
                                          ISupervisorCorretoraService supervisorService,
                                          IEmpresaService empresaService
                                          )
            : base(corretorRepository)
        {
            _corretorService = corretorService;
            _pessoaFisicaService = pessoaFisicaService;
            _corretoraService = corretoraService;
            _pessoaService = pessoaService;
            _supervisorService = supervisorService;
            _empresaService = empresaService;
        }
        private void ValidateInput(SupervisorCorretoraInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (input.CorretoraId == 0)
            {
                validationErrors.Add(new ValidationResult("Corretora não informada"));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        private async Task ProcessInput(SupervisorCorretora entity, SupervisorCorretoraInput input)
        {
            if (input.Pessoa.Id > 0)
            {
                try
                {
                    var pessoa = await _pessoaService.GetById(input.Pessoa.Id);
                    if (pessoa != null)
                    {
                        entity.Pessoa = pessoa;
                        entity.PessoaId = input.Pessoa.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Corretora.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            if (input.CorretoraId > 0)
            {
                var corretora = await _corretoraService.GetById(input.CorretoraId);
                if (corretora != null)
                {
                    entity.CorretoraId = input.CorretoraId;
                    entity.Corretora = corretora;
                }
                else
                {
                    throw new UserFriendlyException(L("Corretora.NotFoundError"));
                }
            }
            var validationErrors = new List<ValidationResult>();
            if (entity.IsPessoaFisica)
            {
                var errors = entity.IsValidSupervisorCorretora();
                if (errors.Any())
                {
                    foreach (var valid in errors)
                    {
                        validationErrors.Add(new ValidationResult(L(valid)));
                    }
                }
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Create, AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Edit)]
        public async Task<IdInput> Save(SupervisorCorretoraInput input)
        {
            IdInput result = null;
            var supervisorCorretora = await _supervisorService.GetAll().Where(x => x.CorretoraId == input.CorretoraId && x.PessoaId == input.Pessoa.Id).FirstOrDefaultAsync();
            if (supervisorCorretora == null)
            {
                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == input.Pessoa.Id).FirstOrDefaultAsync();
                var pessoa = await _pessoaService.GetById(input.Pessoa.Id);

                supervisorCorretora = new SupervisorCorretora()
                {
                    CorretoraId = input.CorretoraId,
                    PessoaId = input.Pessoa.Id,
                    Pessoa = pessoa,
                    IsActive = true,
                    TenantId = 1
                };

                var id = await Repository.InsertAndGetIdAsync(supervisorCorretora);

                //TODO não precisa ser um corretor, colocar coo pessoa física
                //var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
                //_supervisorService.AddSupervisorInRole(corretor, empresa);

                result = new IdInput(id);
            }
            else
            {
                throw new UserFriendlyException(L("GerenteCorretora.PessoaGerenteExistenteError"));
            }
            return result;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Create, AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Edit)]
        public async Task<SupervisorCorretoraInput> SaveAndReturnEntity(SupervisorCorretoraInput input)
        {
            SupervisorCorretoraInput result = null;
            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<SupervisorCorretoraInput, SupervisorCorretoraInput>(_supervisorService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<SupervisorCorretoraInput, SupervisorCorretoraInput>(_supervisorService, input, () => ValidateInput(input), ProcessInput);
            }
            return result;
        }
        public async Task<PagedResultDto<SupervisorCorretoraListDto>> GetSupervisorCorretorasPaginado(GetSupervisorCorretoraInput input)
        {
            var condicoes = new List<WhereIfCondition<SupervisorCorretora>>
            {
                new WhereIfCondition<SupervisorCorretora>(!input.Nome.IsNullOrEmpty(), r => r.NomePessoa.Contains(input.Nome))
            };
            var result = await base.GetListPaged<SupervisorCorretoraListDto, GetSupervisorCorretoraInput>(_supervisorService, input, condicoes);
            return result;
        }
        public async Task<PagedResultDto<SupervisorCorretoraListDto>> GetSupervisoresByCorretoraId(GetSupervisorCorretoraInput input)
        {
            var condicoes = new List<WhereIfCondition<SupervisorCorretora>>
            {
                new WhereIfCondition<SupervisorCorretora>(input.CorretoraId > 0, r => r.CorretoraId == input.CorretoraId)
            };
            var result = await base.GetListPaged<SupervisorCorretoraListDto, GetSupervisorCorretoraInput>(_supervisorService, input, condicoes);
            return result;
        }
        public async Task<SupervisorCorretoraInput> GetById(IdInput input)
        {
            return await base.GetEntityById<SupervisorCorretoraInput, IdInput>(_supervisorService, input);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorCorretora_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_supervisorService, input);
        }
        public async Task<SupervisorCorretoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<SupervisorCorretoraInput, IdInput>(_supervisorService, input);
        }
    }
}