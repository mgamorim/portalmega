﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento;
using EZ.EZControl.Services.Core.Geral.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ArquivoDocumentoAppService : EZControlAppServiceBase<ArquivoDocumento>, IArquivoDocumentoAppService
    {
        private readonly IArquivoDocumentoService _arquivoDocumentoService;
        private readonly IPropostaDeContratacaoService _propostaService;
        public ArquivoDocumentoAppService(IRepository<ArquivoDocumento> arquivoDocumentoRepository,
                                          IArquivoDocumentoService arquivoDocumentoService,
                                          IPropostaDeContratacaoService propostaService,
                                          IArquivoBaseService arquivoBaseService)
            : base(arquivoDocumentoRepository)
        {
            _arquivoDocumentoService = arquivoDocumentoService;
            _propostaService = propostaService;
        }

        private void ValidateInput(ArquivoDocumentoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ArquivoDocumento entity, ArquivoDocumentoInput input)
        {
            if (input.PropostaDeContratacaoId > 0)
            {
                var proposta = await _propostaService.GetById(input.PropostaDeContratacaoId);
                if (proposta != null)
                {
                    entity.PropostaDeContratacao = proposta;
                }
                else
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.NotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("ArquivoDocumento.PropostaDeContratacaoEmptyError"));
            }
            entity.TenantId = AbpSession.TenantId.Value;
            //entity.EmpresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create, AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit)]
        public async Task<IdInput> Save(ArquivoDocumentoInput input)
        {

            try
            {
                IdInput result = null;

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ArquivoDocumentoInput>(_arquivoDocumentoService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ArquivoDocumentoInput>(_arquivoDocumentoService, input, () => ValidateInput(input), ProcessInput);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create, AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit)]
        public async Task<ArquivoDocumentoInput> SaveAndReturnEntity(ArquivoDocumentoInput input)
        {
            ArquivoDocumentoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ArquivoDocumentoInput, ArquivoDocumentoInput>(_arquivoDocumentoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ArquivoDocumentoInput, ArquivoDocumentoInput>(_arquivoDocumentoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ArquivoDocumentoListDto>> GetPaginado(GetArquivoDocumentoInput input)
        {
            var condicoes = new List<WhereIfCondition<ArquivoDocumento>>
            {
                 new WhereIfCondition<ArquivoDocumento>(
                    !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<ArquivoDocumentoListDto, GetArquivoDocumentoInput>(_arquivoDocumentoService, input, condicoes);
            return result;
        }

        public async Task<ArquivoDocumentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ArquivoDocumentoInput, IdInput>(_arquivoDocumentoService, input);
        }

        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_arquivoDocumentoService, input);
        }

        public async Task<ArquivoDocumentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ArquivoDocumentoInput, IdInput>(_arquivoDocumentoService, input);
        }

        public async Task<ArquivoDocumentoInput> GetArquivoByName(ArquivoDocumentoInput input)
        {
            var arquivo = await _arquivoDocumentoService.GetAll().Where(x => x.Nome.ToLower() == input.Nome.ToLower()).FirstOrDefaultAsync();
            return arquivo.MapTo<ArquivoDocumentoInput>();
        }

        public async Task<IdInput> ChangeArquivoDocumentoExigencia(ArquivoDocumentoInput input)
        {
            var entity = await _arquivoDocumentoService.GetById(input.Id);
            entity.EmExigencia = input.EmExigencia;
            entity.Motivo = entity.EmExigencia ? input.Motivo : string.Empty;
            var id = await _arquivoDocumentoService.CreateOrUpdateEntity(entity);
            return new IdInput(id);
        }
    }
}
