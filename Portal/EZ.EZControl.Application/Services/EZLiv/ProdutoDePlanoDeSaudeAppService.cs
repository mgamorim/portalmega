﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using Abp.UI;
using Castle.Core.Internal;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Common.Helpers;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class ProdutoDePlanoDeSaudeAppService : EZControlAppServiceBase<ProdutoDePlanoDeSaude>, IProdutoDePlanoDeSaudeAppService
    {
        private readonly IProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;
        private readonly IEspecialidadeService _especialidadeService;
        private readonly IProdutoService _produtoService;
        private readonly IUnidadeMedidaService _unidadeMedidaService;
        private readonly INaturezaService _naturezaService;
        private readonly IAdministradoraService _administradoraService;
        private readonly IOperadoraService _operadoraService;
        private readonly IArquivoGlobalService _arquivoGlobalService;
        private readonly IPlanoDeSaudeService _planoDeSaudeService;
        private readonly IContratoService _contratoService;
        private readonly IEmpresaService _empresaService;
        private readonly IRedeCredenciadaService _redeCredenciadaService;
        private readonly IEspecialidadePorProdutoDePlanoDeSaudeService _especialidadePorProdutoDePlanoDeSaudeService;
        private readonly IItemDeRedeCredenciadaService _itemDeRedeCredenciadaService;
        private readonly IItemDeDeclaracaoDeSaudeService _itemDeDeclaracaoDeSaudeService;
        private readonly IRelatorioService _relatorioService;
        private readonly IEstadoService _estadoService;
        private readonly ICidadeService _cidadeService;
        private readonly IAssociacaoService _associacaoService;
        private readonly IChancelaService _chancelaService;
        private readonly IVigenciaService _vigenciaService;
        private readonly IValorPorFaixaEtariaService _valorPorFaixaEtaria;
        private readonly IProponenteTitularService _proponenteTitularService;
        private readonly IQuestionarioDeDeclaracaoDeSaudeService _questionarioService;
        private readonly IPropostaDeContratacaoService _propostaDeContratacaoService;
        private readonly IDeclaracaoDoBeneficiarioService _declaracaoDoBeneficiarioService;
        private readonly ICorretoraService _corretoraService;
        private readonly ICorretorService _corretorService;
        private readonly IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService _permissaoDeVendaDePlanoDeSaudeParaCorretoraService;
        private readonly IPermissaoDeVendaDePlanoDeSaudeParaCorretorService _permissaoDeVendaDePlanoDeSaudeParaCorretorService;
        private readonly UserManager _userManager;

        public ProdutoDePlanoDeSaudeAppService(IRepository<ProdutoDePlanoDeSaude> produtoDePlanoDeSaudeRepository,
                                               IProdutoDePlanoDeSaudeService produtoDePlanoDeSaudeService,
                                               IUnidadeMedidaService unidadeMedidaService,
                                               INaturezaService naturezaService,
                                               IArquivoGlobalService arquivoGlobalService,
                                               IProdutoService produtoService,
                                               IPlanoDeSaudeService planoDeSaudeService,
                                               IContratoService contratoService,
                                               IAdministradoraService administradoraService,
                                               IOperadoraService operadoraService,
                                               IEspecialidadeService especialidadeService,
                                               IEmpresaService empresaService,
                                               IRedeCredenciadaService redeCredenciadaService,
                                               IEspecialidadePorProdutoDePlanoDeSaudeService especialidadePorProdutoDePlanoDeSaudeService,
                                               IItemDeRedeCredenciadaService itemDeRedeCredenciadaService,
                                               IItemDeDeclaracaoDeSaudeService itemDeDeclaracaoDeSaudeService,
                                               IEstadoService estadoService,
                                               ICidadeService cidadeService,
                                               IAssociacaoService associacaoService,
                                               IChancelaService chancelaService,
                                               IVigenciaService vigenciaService,
                                               IValorPorFaixaEtariaService valorPorFaixaEtaria,
                                               IProponenteTitularService proponenteTitularService,
                                               IQuestionarioDeDeclaracaoDeSaudeService questionarioService,
                                               IPropostaDeContratacaoService propostaDeContratacaoService,
                                               IDeclaracaoDoBeneficiarioService declaracaoDoBeneficiarioService,
                                               ICorretoraService corretoraService,
                                               ICorretorService corretorService,
                                               IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService permissaoDeVendaDePlanoDeSaudeParaCorretoraService,
                                               IPermissaoDeVendaDePlanoDeSaudeParaCorretorService permissaoDeVendaDePlanoDeSaudeParaCorretorService,
                                               UserManager userManager,
                                               IRelatorioService relatorioService)
            : base(produtoDePlanoDeSaudeRepository)
        {
            _produtoDePlanoDeSaudeService = produtoDePlanoDeSaudeService;
            _unidadeMedidaService = unidadeMedidaService;
            _naturezaService = naturezaService;
            _arquivoGlobalService = arquivoGlobalService;
            _produtoService = produtoService;
            _planoDeSaudeService = planoDeSaudeService;
            _contratoService = contratoService;
            _administradoraService = administradoraService;
            _operadoraService = operadoraService;
            _especialidadeService = especialidadeService;
            _empresaService = empresaService;
            _redeCredenciadaService = redeCredenciadaService;
            _especialidadePorProdutoDePlanoDeSaudeService = especialidadePorProdutoDePlanoDeSaudeService;
            _itemDeRedeCredenciadaService = itemDeRedeCredenciadaService;
            _itemDeDeclaracaoDeSaudeService = itemDeDeclaracaoDeSaudeService;
            _estadoService = estadoService;
            _cidadeService = cidadeService;
            _associacaoService = associacaoService;
            _chancelaService = chancelaService;
            _vigenciaService = vigenciaService;
            _valorPorFaixaEtaria = valorPorFaixaEtaria;
            _proponenteTitularService = proponenteTitularService;
            _questionarioService = questionarioService;
            _propostaDeContratacaoService = propostaDeContratacaoService;
            _declaracaoDoBeneficiarioService = declaracaoDoBeneficiarioService;
            _corretoraService = corretoraService;
            _corretorService = corretorService;
            _permissaoDeVendaDePlanoDeSaudeParaCorretoraService = permissaoDeVendaDePlanoDeSaudeParaCorretoraService;
            _permissaoDeVendaDePlanoDeSaudeParaCorretorService = permissaoDeVendaDePlanoDeSaudeParaCorretorService;
            _userManager = userManager;
            _relatorioService = relatorioService;
        }

        private void ValidateInput(ProdutoDePlanoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(string.Format(L("Produto.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (input.CarenciaEspecial && string.IsNullOrEmpty(input.DescricaoDaCarenciaEspecial))
                validationErrors.Add(new ValidationResult(string.Format(L("ProdutoDePlanoDeSaude.EmptyDescricaoDaCarenciaEspecialError"), input.DescricaoDaCarenciaEspecial), new[] { "descricaoDaCarenciaEspecial".ToCamelCase() }));

            if (input.PlanoDeSaude != null && input.PlanoDeSaude.Reembolso && string.IsNullOrEmpty(input.PlanoDeSaude.DescricaoDoReembolso))
                validationErrors.Add(new ValidationResult(string.Format(L("ProdutoDePlanoDeSaude.EmptyDescricaoDoReembolsolError"), input.PlanoDeSaude.Reembolso), new[] { "descricaoDoReembolso".ToCamelCase() }));

            if (input.Acompanhante && string.IsNullOrEmpty(input.DescricaoDoAcompanhante))
                validationErrors.Add(new ValidationResult(string.Format(L("ProdutoDePlanoDeSaude.EmptyDescricaoDoAcompanhanteError"), input.DescricaoDoAcompanhante), new[] { "descricaoDoAcompanhante".ToCamelCase() }));

            if (input.CoberturaExtra && string.IsNullOrEmpty(input.DescricaoDaCoberturaExtra))
                validationErrors.Add(new ValidationResult(string.Format(L("ProdutoDePlanoDeSaude.EmptyDescricaoDaCoberturaExtraError"), input.DescricaoDaCoberturaExtra), new[] { "descricaoDaCoberturaExtra".ToCamelCase() }));

            if (input.OperadoraId == default(int))
                validationErrors.Add(new ValidationResult(L("ProdutoDePlanoDeSaude.EmptyOperadoraError")));

            if (input.FormaDeContratacao == 0)
                validationErrors.Add(new ValidationResult(L("ProdutoDePlanoDeSaude.FormaDeContratacaoEmpty")));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
                validationErrors.Add(new ValidationResult(string.Format(L("ProdutoDePlanoDeSaude.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));


            if ((input.Id > 0 && (input.FormaDeContratacao == FormaDeContratacaoEnum.Adesao ||
                input.FormaDeContratacao == FormaDeContratacaoEnum.Corporativo) &&
                input.Vigencias != null && !input.Vigencias.Any()))
            {
                validationErrors.Add(new ValidationResult(L("ProdutoDePlanoDeSaude.VigenciaPorForma")));
            }

            var listSegmentacao = new SegmentacaoAssistencialDoPlanoEnum[]
            {
                SegmentacaoAssistencialDoPlanoEnum.Ambulatorial,
                SegmentacaoAssistencialDoPlanoEnum.Dental,
                SegmentacaoAssistencialDoPlanoEnum.Hospitalar,
                SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorial,
                SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialDental,
                SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia,
                SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetriciaDental,
                SegmentacaoAssistencialDoPlanoEnum.HospitalarObstetricia,
                SegmentacaoAssistencialDoPlanoEnum.HospitalarObstetriciaDental,
                SegmentacaoAssistencialDoPlanoEnum.NaoInformado,
                //SegmentacaoAssistencialDoPlanoEnum.HospComOuSemObstetriciaMaisOdonto,
            };

            if (input.PlanoDeSaude != null
                && listSegmentacao.Contains(input.PlanoDeSaude.SegmentacaoAssistencial)
                && !input.PlanoDeSaude.Acomodacao.HasValue)
            {
                validationErrors.Add(new ValidationResult(L("ProdutoDePlanoDeSaude.EmptyAcomadacaoError")));
            }

            //if (input.PlanoDeSaude != null
            //    && listSegmentacao.Contains(input.PlanoDeSaude.SegmentacaoAssistencial)
            //    && !input.PlanoDeSaude.PlanoDeSaudeAns.HasValue)
            //{
            //    validationErrors.Add(new ValidationResult(L("ProdutoDePlanoDeSaude.EmptyPlanoDeSaudeAnsError")));
            //}

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ProdutoDePlanoDeSaude entity, ProdutoDePlanoDeSaudeInput input)
        {
            var administradora = await _administradoraService.GetByEmpresaLogada();
            if (administradora == null)
            {
                throw new UserFriendlyException("Não foi possível recuperar a administradora a partir da empresa logada.");
            }
            entity.Administradora = administradora;

            if (input.PlanoDeSaudeId > 0)
            {
                try
                {
                    entity.PlanoDeSaude = await _planoDeSaudeService.GetById(input.PlanoDeSaudeId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.NotFoundPlanoDeSaudeError"));
                }
            }

            if (input.OperadoraId > 0)
            {
                try
                {
                    entity.Operadora = await _operadoraService.GetById(input.OperadoraId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.NotFoundOperadoraError"));
                }
            }

            if (input.DeclaracaoDoBeneficiarioId.HasValue && input.DeclaracaoDoBeneficiarioId.Value > 0)
            {
                try
                {
                    entity.DeclaracaoDoBeneficiario = await _declaracaoDoBeneficiarioService.GetById(input.DeclaracaoDoBeneficiarioId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.NotFoundOperadoraError"));
                }
            }

            // Unidade de Medida
            if (input.UnidadeMedidaId.HasValue && input.UnidadeMedidaId.Value > 0)
            {
                UnidadeMedida unidadeMedida = null;

                try
                {
                    unidadeMedida = await _unidadeMedidaService.GetById(input.UnidadeMedidaId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.UnidadeMedida.NotFoundError"));
                }

                try
                {
                    entity.AssociarUnidadeMedida(unidadeMedida, _produtoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.UnidadeMedida.EmptyError"));
            }

            // Natureza
            if (input.NaturezaId.HasValue && input.NaturezaId.Value > 0)
            {
                Natureza natureza = null;

                try
                {
                    natureza = await _naturezaService.GetById(input.NaturezaId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.Natureza.NotFoundError"));
                }

                try
                {
                    entity.AssociarNatureza(natureza, _produtoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Estoque.Natureza.EmptyError"));
            }

            //Imagem
            if (input.ImagemId > 0)
            {
                try
                {
                    var imagem = await _arquivoGlobalService.GetById(input.ImagemId);
                    if (imagem != null)
                    {
                        entity.Imagem = imagem;
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            //Questionario
            if (input.QuestionarioDeDeclaracaoDeSaudeId > 0)
            {
                try
                {
                    var questionario = await _questionarioService.GetById(input.QuestionarioDeDeclaracaoDeSaudeId);
                    if (questionario != null)
                    {
                        if (input.QuestionarioDeDeclaracaoDeSaude != null && input.QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoDeSaude.Any())
                        {
                            entity.QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoDeSaude.Clear();
                            foreach (var item in input.QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoDeSaude)
                            {
                                var itemDeDeclaracao = await _itemDeDeclaracaoDeSaudeService.GetById(item.Id);
                                entity.QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoDeSaude.Add(itemDeDeclaracao);
                            }
                        }
                        entity.QuestionarioDeDeclaracaoDeSaude = questionario;
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }


            Empresa empresa = null;
            try
            {
                empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
            }

            // Rede Credenciada
            if (input.RedeCredenciadaId > 0)
            {
                RedeCredenciada redeCredenciada = null;
                try
                {
                    redeCredenciada = await _redeCredenciadaService.GetById(input.RedeCredenciadaId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.NotFoundRedeCredenciadaError"));
                }

                try
                {
                    entity.AssociarRedeCredenciada(redeCredenciada, _produtoDePlanoDeSaudeService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            var empresaLogada = await _empresaService.GetEmpresaLogada();

            entity.Estados.Clear();
            if (input.Estados != null && input.Estados.Any())
            {
                foreach (var item in input.Estados)
                {
                    var estado = await _estadoService.GetById(item.Id);
                    entity.Estados.Add(estado);
                }
            }

            entity.Cidades.Clear();
            if (input.Cidades != null && input.Cidades.Any())
            {
                foreach (var item in input.Cidades)
                {
                    var cidade = await _cidadeService.GetById(item.Id);
                    entity.Cidades.Add(cidade);
                }
            }

            entity.Aditivos.Clear();
            if (input.Aditivos != null && input.Aditivos.Any())
            {
                foreach (var item in input.Aditivos)
                {
                    var relatorio = await _relatorioService.GetById(item.Id);
                    entity.Aditivos.Add(relatorio);
                }
            }

            //if (input.AssociacaoId.HasValue && input.AssociacaoId.Value > 0)
            //{
            //    try
            //    {
            //        entity.Associacao = await _associacaoService.GetById(input.AssociacaoId.Value);
            //    }
            //    catch
            //    {
            //        throw new UserFriendlyException(L("Associacao.NotFoundError"));
            //    }
            //}

            entity.Chancelas.Clear();
            if (input.Chancelas != null && input.Chancelas.Any())
            {
                foreach (var item in input.Chancelas)
                {
                    var chancela = await _chancelaService.GetById(item.Id);
                    chancela.EmpresaId = empresaLogada.Id;
                    entity.Chancelas.Add(chancela);
                }
            }

            entity.Vigencias.Clear();
            if (input.Vigencias != null && input.Vigencias.Any())
            {
                foreach (var item in input.Vigencias)
                {
                    var vigencia = item.MapTo<Vigencia>();
                    if (vigencia.Id > 0)
                        vigencia = await _vigenciaService.GetById(item.Id);

                    vigencia.EmpresaId = empresaLogada.Id;
                    entity.Vigencias.Add(vigencia);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Edit)]
        //[UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ProdutoDePlanoDeSaudeInput input)
        {
            IdInput result = null;
            using (var uow = this.UnitOfWorkManager.Begin())
            {
                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ProdutoDePlanoDeSaudeInput>(
                        _produtoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ProdutoDePlanoDeSaudeInput>(
                        _produtoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
                }

                await uow.CompleteAsync();
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Edit)]
        public async Task<ProdutoDePlanoDeSaudeInput> SaveAndReturnEntity(ProdutoDePlanoDeSaudeInput input)
        {
            ProdutoDePlanoDeSaudeInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ProdutoDePlanoDeSaudeInput, ProdutoDePlanoDeSaudeInput>(_produtoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ProdutoDePlanoDeSaudeInput, ProdutoDePlanoDeSaudeInput>(_produtoDePlanoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutosDePlanoDeSaudePaginado(GetProdutoDePlanoDeSaudeInput input)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());

            var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);

            var administradora = await _administradoraService.GetAll().FirstOrDefaultAsync(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId);

            var corretora = await _corretoraService.FirstOrDefault(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId.Value);

            var entityAdm = await _permissaoDeVendaDePlanoDeSaudeParaCorretoraService.GetAllListAsync();

            var lstResult = new List<PermissaoDeVendaDePlanoDeSaudeParaCorretora>();

            var condicoes = new List<WhereIfCondition<ProdutoDePlanoDeSaude>>();



           

            if (user.Name != "admin" && user.Name != "mauricio" && administradora == null)
            {
                

                foreach (var adm in entityAdm)
                {
                    foreach (var item in adm.Corretoras)
                    {
                        if (corretora.Id == item.Id)
                        {
           
                            //condicoes.Add(new WhereIfCondition<ProdutoDePlanoDeSaude>(true, r => r.AdministradoraId == 3 || r.AdministradoraId == 4));


                            lstResult.Add(adm);
                        }
                    }
                }

              
              


            }
            //condicoes.Add(new WhereIfCondition<ProdutoDePlanoDeSaude>(true, r => r.IsActive == true));
            condicoes.Add(new WhereIfCondition<ProdutoDePlanoDeSaude>(!input.Nome.IsNullOrEmpty(), r => r.Nome.Contains(input.Nome)));
            condicoes.Add(new WhereIfCondition<ProdutoDePlanoDeSaude>(input.ExceptIds != null && input.ExceptIds.Count() > 0, a => !input.ExceptIds.Contains(a.Id)));

            if (administradora != null)
                condicoes.Add(new WhereIfCondition<ProdutoDePlanoDeSaude>(true, r => r.AdministradoraId == administradora.Id));



            //var result = new PagedResultDto<ProdutoDePlanoDeSaudeListDto>();

            //for (int i = 0; i < condicoes.Count; i++)
            //{
            //    result = await base.GetListPaged<ProdutoDePlanoDeSaudeListDto, GetProdutoDePlanoDeSaudeInput>(_produtoDePlanoDeSaudeService, input, condicoes);
            //}

            var result = await base.GetListPaged<ProdutoDePlanoDeSaudeListDto, GetProdutoDePlanoDeSaudeInput>(_produtoDePlanoDeSaudeService, input, condicoes);

            

            //result.Items.Where(x => x)


            if (corretora != null)
            {
                var lst = new List<ProdutoDePlanoDeSaudeListDto>();


                for (int i = 0; i < lstResult.Count; i++)
                {
                    foreach (var itemPermissaoCorretora in lstResult[i].Produtos)
                    {
                        foreach (var item in result.Items)
                        {
                            if (itemPermissaoCorretora.Id == item.Id)
                            {
                                if (item.IsActive)
                                    lst.Add(item);
                            }
                        }

                    }
                }

                return new PagedResultDto<ProdutoDePlanoDeSaudeListDto>(lst.Count, lst);
            }
            else
            {
                return result;
            }









        }

        public async Task<ProdutoDePlanoDeSaudeInput> GetById(IdInput input)
        {
            var entity = await _produtoService.GetById(input.Id);
            var produto = entity.MapTo<ProdutoDePlanoDeSaudeInput>();
            if (produto != null)
            {
                produto.Contrato = await GetContratoByProdutoId(new IdInput(produto.Id));
                produto.Especialidades = await GetEspecialidadeByProdutoId(new IdInput(produto.Id));
                if (produto.RedeCredenciadaId > 0)
                {
                    produto.RedeCredenciada = await GetRedeCredenciadaById(new IdInput(produto.RedeCredenciadaId.Value));
                }
            }

            return produto;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProdutoDePlanoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_produtoDePlanoDeSaudeService, input);
        }

        public async Task<ProdutoDePlanoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ProdutoDePlanoDeSaudeInput, IdInput>(_produtoDePlanoDeSaudeService, input);
        }

        public async Task<ContratoListDto> GetContrato(IdInput input)
        {
            var contrato = _contratoService.GetUltimoContratoByProdutoDePlanoDeSaudeId(input.Id);
            return contrato.MapTo<ContratoListDto>();
        }

        public async Task<PagedResultDto<EstadoListDto>> GetEstadosByProdutos()
        {
            var estados = await Repository.GetAll().SelectMany(x => x.Estados).Distinct().ToListAsync();

            var listDtos = estados.MapTo<List<EstadoListDto>>();
            return new PagedResultDto<EstadoListDto>(listDtos.Count, listDtos);
        }

        public async Task<ContratoInput> GetContratoByProdutoId(IdInput input)
        {
            var contratos = await _contratoService.GetAllListAsync(x => x.ProdutoDePlanoDeSaudeId == input.Id && x.IsActive);
            return contratos.FirstOrDefault().MapTo<ContratoInput>();
        }

        public async Task<IEnumerable<EspecialidadePorProdutoDePlanoDeSaudeListDto>> GetEspecialidadeByProdutoId(IdInput input)
        {
            var especialidades = await _especialidadePorProdutoDePlanoDeSaudeService.GetAllListAsync(x => x.ProdutoDePlanoDeSaudeId == input.Id && x.IsActive);
            return especialidades.MapTo<List<EspecialidadePorProdutoDePlanoDeSaudeListDto>>();
        }

        public async Task<RedeCredenciadaInput> GetRedeCredenciadaById(IdInput input)
        {
            RedeCredenciadaInput result = null;
            var rede = await _redeCredenciadaService.GetById(input.Id);
            var items = await _itemDeRedeCredenciadaService.GetAllListAsync(x => x.RedeCredenciada.Id == input.Id);
            result = rede.MapTo<RedeCredenciadaInput>();
            result.Items = items.MapTo<List<ItemDeRedeCredenciadaListDto>>();

            return result;
        }

        public async Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeExceptForContratoActive()
        {
            var idsProdutosContratosAtivos = await _contratoService
                                                        .GetAll()
                                                        .Where(x => x.IsActive)
                                                        .Select(x => x.ProdutoDePlanoDeSaudeId)
                                                        .ToListAsync();
            var produtos = await Repository
                                        .GetAll()
                                        .Where(x => idsProdutosContratosAtivos.All(id => id != x.Id))
                                        .ToListAsync();

            var listDtos = produtos.MapTo<List<ProdutoDePlanoDeSaudeListDto>>();
            return new PagedResultDto<ProdutoDePlanoDeSaudeListDto>(listDtos.Count, listDtos);
        }

        public async Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeForCorretora()
        {
            var empresa = await _empresaService.GetEmpresaLogada();
            if (empresa != null)
            {
                var corretora = await _corretoraService.FirstOrDefault(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId.Value);

                if (corretora == null)
                    throw new UserFriendlyException(L("PermissaoDeVendaDePlanoDeSaudeParaCorretor.EmptyCorretoraError"));

                var produtos = await _permissaoDeVendaDePlanoDeSaudeParaCorretoraService
                                      .GetAll()
                                      .Where(x => x.Corretoras.Any(y => y.Id == corretora.Id))
                                      .SelectMany(x => x.Produtos)
                                      .Distinct()
                                      .ToListAsync();

                var listProduto = produtos.MapTo<List<ProdutoDePlanoDeSaudeListDto>>();

                return new PagedResultDto<ProdutoDePlanoDeSaudeListDto>(listProduto.Count, listProduto);
            }

            return null;
        }

        public async Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeWithValorForCorretora(GetValoresByProdutoInput input)
        {
            var produtos = await _permissaoDeVendaDePlanoDeSaudeParaCorretoraService
                                  .GetAll()
                                  .SelectMany(x => x.Produtos)
                                  .Distinct()
                                  .ToListAsync();

            var listProduto = await GetProdutosComValores(produtos, input.ProponenteId, 0,0);

            return new PagedResultDto<ProdutoDePlanoDeSaudeListDto>(listProduto.Count, listProduto);

        }

        public async Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeWithValorForCorretor(GetValoresAndProdutoByCorretorInput input)
        {
            if (input.CorretorId == 0)
            {
                var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
                if (user != null && user.PessoaId.HasValue)
                {
                    var corretor = await _corretorService.FirstOrDefault(x => x.PessoaId == user.PessoaId.Value);

                    if (corretor != null)
                    {
                        input.CorretorId = corretor.Id;
                    }
                    else
                    {
                        var corretoraAtual = await _corretoraService.GetCorretoraBySlug(user.Empresas.First().Slug);
                        input.CorretorId = Convert.ToInt32(corretoraAtual.CorretorId);
                    }
                }
            }
            

            var produtos = await _permissaoDeVendaDePlanoDeSaudeParaCorretorService
                                  .GetAll()
                                  .Where(x => x.Corretores.Any(y => y.Id == input.CorretorId))
                                  .SelectMany(x => x.Produtos)
                                  .Distinct()
                                  .ToListAsync();

            var listProduto = await GetProdutosComValores(produtos, input.ProponenteId, input.profissaoId, input.estadoId);

            return new PagedResultDto<ProdutoDePlanoDeSaudeListDto>(listProduto.Count, listProduto);

        }

        public virtual async Task<PagedResultDto<EstadoListDto>> GetEstadoExceptForProduto(GetEstadoExceptForProduto input)
        {
            try
            {
                if (input == null)
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.FiltroEstadoNotFoundError"));

                var idsEstado = input.Estados.Select(x => x.Id);

                var queryEstado = _estadoService
                    .GetAll()
                    .Where(x => !idsEstado.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), a => a.Nome.Contains(input.Nome));

                IQueryable<Estado> query = queryEstado;

                var count = await query.CountAsync();

                List<Estado> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<EstadoListDto>>();
                return new PagedResultDto<EstadoListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public virtual async Task<PagedResultDto<CidadeListDto>> GetCidadeExceptForProduto(GetCidadeExceptForProduto input)
        {
            try
            {
                if (input == null)
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.FiltroCidadeNotFoundError"));

                var idsCidade = input.Cidades.Select(x => x.Id);

                var queryCidade = _cidadeService
                    .GetAll()
                    .Where(x => !idsCidade.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), a => a.Nome.Contains(input.Nome));

                IQueryable<Cidade> query = queryCidade;

                var count = await query.CountAsync();

                List<Cidade> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<CidadeListDto>>();
                return new PagedResultDto<CidadeListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PagedResultDto<ProdutoDePlanoDeSaudeListDto>> GetPagedProdutoAtivoComValor(GetValoresByProdutoInput input)
        {
            var produtos = await _produtoDePlanoDeSaudeService.GetAllListAtivoAsync();
            var listProduto = await GetProdutosComValores(produtos, input.ProponenteId, input.profissaoId, input.estadoId);

            return new PagedResultDto<ProdutoDePlanoDeSaudeListDto>(listProduto.Count, listProduto);
        }

        public async Task<ValoresByProdutoListDto> GetValoresByContratoDetail(GetValoresByProdutoInput input)
        {
            var contrato = _contratoService.GetUltimoContratoByProdutoDePlanoDeSaudeId(input.ProdutoDePlanoDeSaudeId);
            var valoresPorFaixa = await _valorPorFaixaEtaria.GetAllListAsync(x => x.ContratoId == contrato.Id);
            var proponente = await _proponenteTitularService.GetById(input.ProponenteId);
            var valores = new ValoresByProdutoListDto();

            if (proponente.PessoaFisica != null &&
                proponente.PessoaFisica.DataDeNascimento.HasValue)
            {
                var idade = PessoaHelpers.GetIdade(proponente.PessoaFisica.DataDeNascimento.Value);
                valores.Titular = new TitularDetailInput
                {
                    Nome = proponente.NomePessoa,
                    Valor = GetValorPorFaixa(idade, valoresPorFaixa)
                };

                valores.Dependentes = new List<DependenteDetailInput>();
                foreach (var dependente in proponente.Dependentes)
                {
                    var idadeDependente = PessoaHelpers.GetIdade(dependente.PessoaFisica.DataDeNascimento.Value);

                    valores.Dependentes.Add(new DependenteDetailInput
                    {
                        Nome = dependente.NomePessoa,
                        Valor = GetValorPorFaixa(idadeDependente, valoresPorFaixa)
                    });
                }

            }

            return valores;
        }

        private async Task<List<ProdutoDePlanoDeSaudeListDto>> GetProdutosComValores(List<ProdutoDePlanoDeSaude> produtos, int proponenteId, int profissaoId, int estadoId)
        {
            var listProduto = new List<ProdutoDePlanoDeSaudeListDto>();
            var ProdutosAtivos = produtos.Where(x => x.IsActive == true);


            // FILTRO CHANCELA PARA CADA PLANO DE SAUDE


            var PodutosAtivosChancelado = new List<ProdutoDePlanoDeSaude>();

            foreach (var produto in ProdutosAtivos)
            {
                foreach (var chancelas in produto.Chancelas)
                {
                    foreach (var profissoes in chancelas.Profissoes)
                    {
                        if (profissoes.Id == Convert.ToInt32(profissaoId)) //  || profissaoId == 0
                        {
                            PodutosAtivosChancelado.Add(produto);
                        }
                    }
                }
            }


            foreach (var produto in PodutosAtivosChancelado)
            {
                var contrato = _contratoService.GetUltimoContratoByProdutoDePlanoDeSaudeId(produto.Id);
                if (contrato != null)
                {
                    var valoresPorFaixa = await _valorPorFaixaEtaria.GetAllListAsync(x => x.ContratoId == contrato.Id);

                    var proponente = await _proponenteTitularService.GetById(proponenteId);

                    if (proponente.PessoaFisica != null &&
                        proponente.PessoaFisica.DataDeNascimento.HasValue)
                    {
                        var valores = new List<decimal>();
                        var idade = PessoaHelpers.GetIdade(proponente.PessoaFisica.DataDeNascimento.Value);
                        valores.Add(GetValorPorFaixa(idade, valoresPorFaixa));

                        foreach (var dependente in proponente.Dependentes)
                        {
                            var idadeDependente =
                                PessoaHelpers.GetIdade(dependente.PessoaFisica.DataDeNascimento.Value);
                            valores.Add(GetValorPorFaixa(idadeDependente, valoresPorFaixa));
                        }



                        var ProdutoPorCidade = new ProdutoDePlanoDeSaude();

                        foreach (var item in produto.Estados)
                        {
                            if (item.Id == Convert.ToInt32(estadoId))
                            {
                                ProdutoPorCidade = produto;




                                var produtoFinal = ProdutoPorCidade.MapTo<ProdutoDePlanoDeSaudeListDto>();
                                produtoFinal.Valor = valores.Sum();

                                if (produtoFinal.RedeCredenciadaId > 0)
                                {
                                    var hospitaisDiferenciados = await _itemDeRedeCredenciadaService.GetAll().Where(x =>
                                        x.RedeCredenciadaId == produtoFinal.RedeCredenciadaId &&
                                        x.UnidadeDeSaude.TipoDeUnidade == TipoDeUnidadeEnum.Hospital &&
                                        x.IsRedeDiferenciada == true).ToListAsync();
                                    var laboratoriosDiferenciados = await _itemDeRedeCredenciadaService.GetAll().Where(x =>
                                        x.RedeCredenciadaId == produtoFinal.RedeCredenciadaId &&
                                        x.UnidadeDeSaude.TipoDeUnidade == TipoDeUnidadeEnum.Laboratorio &&
                                        x.IsRedeDiferenciada == true).ToListAsync();

                                    foreach (var hospital in hospitaisDiferenciados)
                                    {
                                        produtoFinal.RedeMedica = hospital.UnidadeDeSaude.Nome;
                                        if (hospitaisDiferenciados.Count > 1)
                                        {
                                            produtoFinal.RedeMedica += string.Format(" E MAIS [{0}] OPÇÕES",
                                                (hospitaisDiferenciados.Count - 1).ToString());
                                        }
                                        break;
                                    }

                                    foreach (var laboratorio in laboratoriosDiferenciados)
                                    {
                                        produtoFinal.Laboratorios = laboratorio.UnidadeDeSaude.Nome;
                                        if (laboratoriosDiferenciados.Count > 1)
                                        {
                                            produtoFinal.Laboratorios += string.Format(" E MAIS [{0}] OPÇÕES",
                                                (laboratoriosDiferenciados.Count - 1).ToString());
                                        }
                                        break;
                                    }
                                }

                                listProduto.Add(produtoFinal);
                            }
                        }
                    }
                }
            }

            return listProduto;
        }

      
        private async Task<List<ProdutoDePlanoDeSaudeListDto>> GetSimulacaoProdutosComValores(SimulacaoModel model, List<ProdutoDePlanoDeSaude> produtosCorretor)
        {
            var listProduto = new List<ProdutoDePlanoDeSaudeListDto>();
            // var Produtos = new List<ProdutoDePlanoDeSaude>();
            
            var ProdutosAtivos = produtosCorretor.Where(x => x.IsActive == true);

            /// FILTRO CHANCELA PARA CADA PLANO DE SAUDE 
            
            
            var PodutosAtivosChancelado = new List<ProdutoDePlanoDeSaude>();
           
            foreach (var produto in ProdutosAtivos)
            {
                foreach (var chancelas in produto.Chancelas)
                {
                    foreach (var profissoes in chancelas.Profissoes)
                    {
                        if (profissoes.Id == Convert.ToInt32(model.Profissao))
                        {
                            PodutosAtivosChancelado.Add(produto);
                        }
                    }
                }
            }
            


            foreach (var produto in PodutosAtivosChancelado)
            {
                var contrato = _contratoService.GetUltimoContratoByProdutoDePlanoDeSaudeId(produto.Id);
                if (contrato != null)
                {
                    var valoresPorFaixa = await _valorPorFaixaEtaria.GetAllListAsync(x => x.ContratoId == contrato.Id);

                    var valores = new List<decimal>();
                    var idade = PessoaHelpers.GetIdade(Convert.ToDateTime(model.DataNascimento));
                    valores.Add(GetValorPorFaixa(idade, valoresPorFaixa));

                    if (model.Dependentes != null)
                    {
                        foreach (var dependente in model.Dependentes)
                        {
                            var idadeDependente =
                                PessoaHelpers.GetIdade(Convert.ToDateTime(dependente.DataNascimento));
                            valores.Add(GetValorPorFaixa(idadeDependente, valoresPorFaixa));

                        }
                    }

                    var ProdutoPorCidade = new ProdutoDePlanoDeSaude();

                    foreach (var item in produto.Estados)
                    {
                        if (item.Id == Convert.ToInt32(model.Estado))
                        {
                            ProdutoPorCidade = produto;


                            //if (produto.Cidades.Count > 0)
                            //    ProdutoPorCidade =   // produto.Where(x => x.Id == Convert.ToInt32(model.Estado));



                            var produtoFinal = ProdutoPorCidade.MapTo<ProdutoDePlanoDeSaudeListDto>();
                            produtoFinal.Valor = valores.Sum();

                            if (produtoFinal.Valor != 0)
                            {
                                if (produtoFinal.RedeCredenciadaId > 0)
                                {
                                    var hospitaisDiferenciados = await _itemDeRedeCredenciadaService.GetAll().Where(x =>
                                        x.RedeCredenciadaId == produtoFinal.RedeCredenciadaId &&
                                        x.UnidadeDeSaude.TipoDeUnidade == TipoDeUnidadeEnum.Hospital &&
                                        x.IsRedeDiferenciada == true).ToListAsync();
                                    var laboratoriosDiferenciados = await _itemDeRedeCredenciadaService.GetAll().Where(x =>
                                        x.RedeCredenciadaId == produtoFinal.RedeCredenciadaId &&
                                        x.UnidadeDeSaude.TipoDeUnidade == TipoDeUnidadeEnum.Laboratorio &&
                                        x.IsRedeDiferenciada == true).ToListAsync();

                                    foreach (var hospital in hospitaisDiferenciados)
                                    {
                                        produtoFinal.RedeMedica = hospital.UnidadeDeSaude.Nome;
                                        if (hospitaisDiferenciados.Count > 1)
                                        {
                                            produtoFinal.RedeMedica += string.Format(" E MAIS [{0}] OPÇÕES",
                                                (hospitaisDiferenciados.Count - 1).ToString());
                                        }
                                        break;
                                    }

                                    foreach (var laboratorio in laboratoriosDiferenciados)
                                    {
                                        produtoFinal.Laboratorios = laboratorio.UnidadeDeSaude.Nome;
                                        if (laboratoriosDiferenciados.Count > 1)
                                        {
                                            produtoFinal.Laboratorios += string.Format(" E MAIS [{0}] OPÇÕES",
                                                (laboratoriosDiferenciados.Count - 1).ToString());
                                        }
                                        break;
                                    }
                                }

                                listProduto.Add(produtoFinal);
                            }

                        }
                    }
                }
            }

            return listProduto;
        }


        [AbpAllowAnonymous]
        public async Task<List<ProdutoDePlanoDeSaudeListDto>> GetProdutoDePlanoDeSaudeWithValorForCorretorSimulador(int CorretorId , SimulacaoModel model)
        {
            if (CorretorId == 0)
            {
                var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());
                if (user != null && user.PessoaId.HasValue)
                {
                    var corretor = await _corretorService.FirstOrDefault(x => x.PessoaId == user.PessoaId.Value);

                    if (corretor != null)
                    {
                        CorretorId = corretor.Id;
                    }
                    else
                    {
                        var corretoraAtual = await _corretoraService.GetCorretoraBySlug(user.Empresas.First().Slug);
                        CorretorId = Convert.ToInt32(corretoraAtual.CorretorId);
                    }
                }
            }
            var produtos = await _permissaoDeVendaDePlanoDeSaudeParaCorretorService
                                  .GetAll()
                                  .Where(x => x.Corretores.Any(y => y.Id == CorretorId))
                                  .SelectMany(x => x.Produtos)
                                  .Distinct()
                                  .ToListAsync();


            var listProduto = await GetSimulacaoProdutosComValores(model, produtos);

            return listProduto;
        }


        private decimal GetValorPorFaixa(int idade, List<ValorPorFaixaEtaria> valores)
        {
            var valorPorFaixa =
                    valores.FirstOrDefault(x => idade.IsBetween(x.FaixaEtaria.IdadeInicial, (x.FaixaEtaria.IdadeFinal.HasValue ? x.FaixaEtaria.IdadeFinal.Value : 200)));

            return valorPorFaixa != null ? valorPorFaixa.Valor : 0;
        }

        public async Task<DetalhesProdutoDePlanoDeSaudeInput> GetDetalhesById(GetDetalhesProdutoInput input)
        {

            #region Validate
           
            if (input.ProdutoId == 0)
            {
                throw new UserFriendlyException(L("Observacoes"), L("EspecialidadePorProdutoDePlanoDeSaude.NotFoundProdutoDePlanoDeSaudeError"));                
            }

            #endregion

            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var entity = await _produtoService.GetById(input.ProdutoId);
                var produto = entity.MapTo<ProdutoDePlanoDeSaudeInput>();
                //var produto = new MapManual().ProdutoEntity_to_ProdutoPlanoDeSaudeInput(entity);
                ArquivoGlobal imagem = new ArquivoGlobal();
                if (produto.Operadora.ImagemId > 0)
                    imagem = await _arquivoGlobalService.GetById(produto.Operadora.ImagemId);

                var retorno = new DetalhesProdutoDePlanoDeSaudeInput()
                {
                    Abrangencia = produto.PlanoDeSaude.Abrangencia.GetDescription(),
                    Acomodacao = produto.PlanoDeSaude.Acomodacao.GetDescription(),
                    Administradora = (entity as ProdutoDePlanoDeSaude).Administradora.PessoaJuridica.NomePessoa,
                    Id = produto.Id,
                    Nome = produto.Nome,
                    Operadora = produto.Operadora.PessoaJuridica.NomePessoa,
                    Reembolso = produto.PlanoDeSaude.Reembolso,
                    OperadoraLogo = imagem.Id > 0 ? imagem.Path : string.Empty,
                    Descricao = produto.Descricao,
                    RedeCredenciada = produto.LinkRedeCredenciada,

                };

                if (input.PropostaId > 0)
                {
                    var proposta = await _propostaDeContratacaoService.GetById(input.PropostaId);
                    retorno.StatusProposta = new StatusDaPropostaInput()
                    {
                        Id = proposta.Id,
                        PassoDaProposta = (int)proposta.PassoDaProposta,
                        StatusDaProposta = (int)proposta.StatusDaProposta,

                    };

                    if (proposta.Pedido != null)
                    {
                        if (proposta.Pedido.ItensDePedidoReadOnly.Count > 1)
                        {
                            for (int i = 0; i < proposta.Pedido.ItensDePedidoReadOnly.Count; i++)
                            {
                                retorno.Valor += proposta.Pedido.ItensDePedidoReadOnly[i].Valor;
                            }
                        }
                        else
                        {
                            retorno.Valor = proposta.Pedido.ItensDePedidoReadOnly[0].Valor;
                        }
                    }


                    if (proposta.Produto?.RelatorioProspostaDeContratacaoId != null && proposta.Produto.RelatorioProspostaDeContratacaoId.Value > 0)
                    {
                        retorno.StatusProposta.Documentos = new List<DocumentoDaPropostaInput>();
                        retorno.StatusProposta.Documentos.Add(new DocumentoDaPropostaInput()
                        {
                            Nome = "Proposta de Contratação",
                            Url = String.Concat("/relatorio/visualizador?sistema=", (int)SistemaEnum.EZLiv, "&tipoDeRelatorio=", (int)TipoDeRelatorioEzlivEnum.PropostaDeContratacao, "&idDado=", proposta.Id)
                        });
                    }

                    if (proposta.Produto?.RelatorioFichaDeEntidadeId != null && proposta.Produto.RelatorioFichaDeEntidadeId.Value > 0)
                    {
                        if (retorno.StatusProposta.Documentos.Count == 0)
                            retorno.StatusProposta.Documentos = new List<DocumentoDaPropostaInput>();
                        retorno.StatusProposta.Documentos.Add(new DocumentoDaPropostaInput()
                        {
                            Nome = "Ficha da Entidade",
                            Url = String.Concat("/relatorio/visualizador?sistema=", (int)SistemaEnum.EZLiv, "&tipoDeRelatorio=", (int)TipoDeRelatorioEzlivEnum.FichaDeAssociacao, "&idDado=", proposta.Id)
                        });
                    }

                    if (proposta.Aditivos?.Count > 0)
                    {
                        if (retorno.StatusProposta.Documentos.Count == 0)
                            retorno.StatusProposta.Documentos = new List<DocumentoDaPropostaInput>();

                        var count = 1;
                        foreach (var aditivo in proposta.Aditivos)
                        {
                            retorno.StatusProposta.Documentos.Add(new DocumentoDaPropostaInput()
                            {
                                Nome = string.Concat("Aditivo ", count),
                                Url = String.Concat("/relatorio/visualizador?sistema=", (int)SistemaEnum.EZLiv, "&tipoDeRelatorio=", (int)TipoDeRelatorioEzlivEnum.Aditivo, "&idDado=", proposta.Id, "&relatorioId=", aditivo.Id)
                            });
                            count = count + 1;
                        }
                    }
                }

                return retorno;
            }
        }

       
    }
}
