﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RedeCredenciadaAppService : EZControlAppServiceBase<RedeCredenciada>, IRedeCredenciadaAppService
    {
        private readonly IRedeCredenciadaService _redeCredenciadaService;
        private readonly IEmpresaService _empresaService;
        private readonly IItemDeRedeCredenciadaAppService _itemDeRedeCredenciadaAppService;
        private readonly IItemDeRedeCredenciadaService _itemDeRedeCredenciadaService;
        private readonly IUnidadeDeSaudeBaseService _unidadeDeSaudeBaseService;

        public RedeCredenciadaAppService(IRepository<RedeCredenciada> redeCredenciadaRepository,
            IRedeCredenciadaService redeCredenciadaService,
            IItemDeRedeCredenciadaAppService itemDeRedeCredenciadaAppService,
            IItemDeRedeCredenciadaService itemDeRedeCredenciadaService,
            IUnidadeDeSaudeBaseService unidadeDeSaudeBaseService,
            IEmpresaService empresaService)
            : base(redeCredenciadaRepository)
        {
            this._redeCredenciadaService = redeCredenciadaService;
            this._empresaService = empresaService;
            this._itemDeRedeCredenciadaAppService = itemDeRedeCredenciadaAppService;
            this._itemDeRedeCredenciadaService = itemDeRedeCredenciadaService;
            this._unidadeDeSaudeBaseService = unidadeDeSaudeBaseService;
        }

        private void ValidateInput(RedeCredenciadaInput input)
        {
            var validationErrors = new List<ValidationResult>();


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(RedeCredenciada redeCredenciada, RedeCredenciadaInput input)
        {
            Empresa empresa = null;
            try
            {
                empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
                redeCredenciada.Empresa = empresa;
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Create, AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Edit)]
        public async Task<IdInput> Save(RedeCredenciadaInput input)
        {
            IdInput result = null;
            //try
            //{
            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, RedeCredenciadaInput>(_redeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, RedeCredenciadaInput>(_redeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
            //}
            //catch (Exception ex)
            //{
            //    throw new UserFriendlyException(ex.Message);
            //}
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Create, AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Edit)]
        public async Task<RedeCredenciadaInput> SaveAndReturnEntity(RedeCredenciadaInput input)
        {
            RedeCredenciadaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<RedeCredenciadaInput, RedeCredenciadaInput>(_redeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<RedeCredenciadaInput, RedeCredenciadaInput>(_redeCredenciadaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<RedeCredenciadaListDto>> GetPaginado(GetRedeCredenciadaInput input)
        {
            var condicoes = new List<WhereIfCondition<RedeCredenciada>>
            {
                new WhereIfCondition<RedeCredenciada>(!string.IsNullOrEmpty(input.Nome), r => r.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<RedeCredenciadaListDto, GetRedeCredenciadaInput>(_redeCredenciadaService, input, condicoes);
            return result;
        }

        public async Task<RedeCredenciadaInput> GetById(IdInput input)
        {
            RedeCredenciadaInput redeCredenciada = await base.GetEntityById<RedeCredenciadaInput, IdInput>(_redeCredenciadaService, input);

            if (redeCredenciada != null)
            {
                GetItemDeRedeCredenciadaInput item = new GetItemDeRedeCredenciadaInput();
                item.RedeCredenciadaId = input.Id;
                redeCredenciada.Items = await _itemDeRedeCredenciadaAppService.GetItemsByRedeCredenciada(input.Id);
            }
            return redeCredenciada;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RedeCredenciada_Delete)]
        public async Task Delete(IdInput input)
        {
            var itens = await _itemDeRedeCredenciadaService.GetAllListAsync(x => x.RedeCredenciada.Id == input.Id);
            foreach (var item in itens)
            {
                await _itemDeRedeCredenciadaService.Delete(item.Id);
            }

            await base.DeleteEntity(_redeCredenciadaService, input);
        }

        public async Task<RedeCredenciadaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RedeCredenciadaInput, IdInput>(_redeCredenciadaService, input);
        }

        public virtual async Task<PagedResultDto<UnidadeDeSaudeBaseListDto>> GetUnidadeDeSaudeExceptForItemRedeCredenciada(GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por unidades de saúde.");

                var idsUnidadeDeSaude = _itemDeRedeCredenciadaService.GetAll().Select(x => x.UnidadeDeSaudeId);

                var queryUnidadeDeSaudeBase = _unidadeDeSaudeBaseService.GetAll()
                    .Where(x => !idsUnidadeDeSaude.Contains(x.Id));

                IQueryable<UnidadeDeSaudeBase> query = queryUnidadeDeSaudeBase;

                var count = await query.CountAsync();

                List<UnidadeDeSaudeBase> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<UnidadeDeSaudeBaseListDto>>();
                return new PagedResultDto<UnidadeDeSaudeBaseListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
