﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Common.Helpers;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraAppService : EZControlAppServiceBase<PermissaoDeVendaDePlanoDeSaudeParaCorretora>, IPermissaoDeVendaDePlanoDeSaudeParaCorretoraAppService
    {
        private readonly IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService _permissaoDeVendaDePlanoDeSaudeParaCorretoraService;
        private readonly IPermissaoDeVendaDePlanoDeSaudeParaCorretorService _permissaoDeVendaDePlanoDeSaudeParaCorretorService;
        private readonly IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretora> _permissaoDeVendaDePlanoDeSaudeParaCorretoraRepository;
        private readonly IProdutoDePlanoDeSaudeService _produtoService;
        private readonly IAdministradoraService _administradoraService;
        private readonly ICorretoraService _corretoraService;
        private readonly IEmpresaService _empresaService;
        private readonly IContratoService _contratoService;
        private readonly IValorPorFaixaEtariaService _valorPorFaixaEtaria;
        private readonly IProponenteTitularService _proponenteTitularService;

        public PermissaoDeVendaDePlanoDeSaudeParaCorretoraAppService(IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretora> permissaoDeVendaDePlanoDeSaudeParaCorretoraRepository,
                                  IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService permissaoDeVendaDePlanoDeSaudeParaCorretoraService,
                                  IPermissaoDeVendaDePlanoDeSaudeParaCorretorService permissaoDeVendaDePlanoDeSaudeParaCorretorService,
                                  IProdutoDePlanoDeSaudeService produtoService,
                                  IAdministradoraService administradoraService,
                                  ICorretoraService corretoraService,
                                  IEmpresaService empresaService,
                                  IContratoService contratoService,
                                  IValorPorFaixaEtariaService valorPorFaixaEtaria,
                                  IProponenteTitularService proponenteTitularService)
            : base(permissaoDeVendaDePlanoDeSaudeParaCorretoraRepository)
        {
            _permissaoDeVendaDePlanoDeSaudeParaCorretoraService = permissaoDeVendaDePlanoDeSaudeParaCorretoraService;
            _permissaoDeVendaDePlanoDeSaudeParaCorretorService = permissaoDeVendaDePlanoDeSaudeParaCorretorService;
            _permissaoDeVendaDePlanoDeSaudeParaCorretoraRepository = permissaoDeVendaDePlanoDeSaudeParaCorretoraRepository;
            _produtoService = produtoService;
            _administradoraService = administradoraService;
            _corretoraService = corretoraService;
            _empresaService = empresaService;
            _contratoService = contratoService;
            _valorPorFaixaEtaria = valorPorFaixaEtaria;
            _proponenteTitularService = proponenteTitularService;
        }

        private void ValidateInput(PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (!input.Produtos.Any())
                validationErrors.Add(new ValidationResult(L("PermissaoDeVendaDePlanoDeSaudeParaCorretora.NotFoundProdutos")));

            if (!input.Corretoras.Any())
                validationErrors.Add(new ValidationResult(L("PermissaoDeVendaDePlanoDeSaudeParaCorretora.NotFoundCorretoras")));

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("PermissaoDeVendaDePlanoDeSaudeParaCorretora.EmptyNomeError")));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(PermissaoDeVendaDePlanoDeSaudeParaCorretora entity, PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input)
        {
            var empresa = await _empresaService.GetEmpresaLogada();
            if (empresa != null)
            {
                entity.Empresa = empresa;
                entity.EmpresaId = empresa.Id;
                entity.TenantId = AbpSession.TenantId.Value;
                var administradoras = await _administradoraService.GetAllListAsync(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId.Value);

                if (!administradoras.Any())
                    throw new UserFriendlyException(L("PermissaoDeVendaDePlanoDeSaudeParaCorretora.EmptyAdministradoraError"));

                entity.Administradora = administradoras.FirstOrDefault();
            }

            entity.Produtos.Clear();
            foreach (var produtoInput in input.Produtos)
            {
                var produto = await _produtoService.GetById(produtoInput.Id);
                entity.Produtos.Add(produto);
            }

            //var permissoes = await base.GetEntityById<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, IdInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input);
            //return permissoes;

            entity.Corretoras.Clear();
            foreach (var corretoraInput in input.Corretoras)
            {
                var corretora = await _corretoraService.GetById(corretoraInput.Id);
                entity.Corretoras.Add(corretora);

                var retorno = await _permissaoDeVendaDePlanoDeSaudeParaCorretorService.GetAllListAsync(x => x.CorretoraId == corretora.Id);
                var lst = new List<ProdutoDePlanoDeSaude>();

                if (retorno.Count != 0)
                {
                    foreach (var item in entity.Produtos)
                    {
                        foreach (var i in retorno.FirstOrDefault().Produtos)
                        {
                            if (item.Id == i.Id)
                                lst.Add(item);
                        }

                    }

                    retorno.FirstOrDefault().Produtos.Clear();
                    retorno.FirstOrDefault().Produtos = lst;

                    var up = await _permissaoDeVendaDePlanoDeSaudeParaCorretorService.UpdateEntity(retorno.FirstOrDefault());
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create, AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit)]
        public async Task<IdInput> Save(PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input)
        {
            try
            {
                IdInput result = null;
                if (input.Id == default(int))
                {
                    if (
                        !PermissionChecker.IsGranted(
                            AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result =
                        await base.CreateEntity<IdInput, PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>(
                            _permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input,
                            () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (
                        !PermissionChecker.IsGranted(
                            AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result =
                        await base.UpdateEntity<IdInput, PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>(
                            _permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create, AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit)]
        public async Task<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> SaveAndReturnEntity(PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input)
        {
            PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput permissao = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                permissao = await base.CreateAndReturnEntity<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                permissao = await base.UpdateAndReturnEntity<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input, () => ValidateInput(input));
            }

            return permissao;
        }

        public async Task<PagedResultDto<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto>> GetPaginado(GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput input)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value;

            var condicoes = new List<WhereIfCondition<PermissaoDeVendaDePlanoDeSaudeParaCorretora>>
            {
                new WhereIfCondition<PermissaoDeVendaDePlanoDeSaudeParaCorretora>(
                        !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Nome.Contains(input.Nome)),
                new WhereIfCondition<PermissaoDeVendaDePlanoDeSaudeParaCorretora>(true, r => r.EmpresaId == empresaId)
            };
            var result = await base.GetListPaged<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, GetPermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input, condicoes);
            return result;
        }

        public async Task<GetPermissoesPorCorretoraInput> PermissoesDeCorretora(IdInput input)
        {
            var permissoes =
                await Repository.GetAll().Where(x => x.Produtos.Any(y => y.Id == input.Id)).ToListAsync();

            var permissoesPorCorretora = new GetPermissoesPorCorretoraInput();

            if (permissoes != null)
            {
                permissoesPorCorretora.Corretora = permissoes.FirstOrDefault().Corretoras.FirstOrDefault().MapTo<CorretoraListDto>();
                permissoesPorCorretora.TemCorretoras = permissoes.Count > 1 ? true : false;
            }

            return permissoesPorCorretora;
        }

        public async Task<PagedResultDto<GetPermissoesPorCorretoraInput>> GetProdutoECorretorasDaPermissao(IdInput input)
        {
            var corretoras =
                await Repository.GetAll().Where(x => x.Produtos.Any(y => y.Id == input.Id)).SelectMany(x => x.Corretoras).ToListAsync();

            var produto = await _produtoService.GetById(input.Id);
            var listProdutoECorretoras = new List<GetPermissoesPorCorretoraInput>();

            foreach (var corretora in corretoras)
            {
                listProdutoECorretoras.Add(new GetPermissoesPorCorretoraInput
                {
                    Corretora = corretora.MapTo<CorretoraListDto>(),
                    Valor = produto.Valor
                });
            }

            return new PagedResultDto<GetPermissoesPorCorretoraInput>(listProdutoECorretoras.Count, listProdutoECorretoras);
        }

        public async Task<PagedResultDto<CorretoraListDto>> GetCorretorasByProduto(GetValoresByProdutoInput input)
        {
            var produto = await _produtoService.GetById(input.ProdutoDePlanoDeSaudeId);
            var corretoras =
                await Repository.GetAll().Where(x => x.Produtos.Any(y => y.Id == input.ProdutoDePlanoDeSaudeId)).SelectMany(x => x.Corretoras).Distinct().ToListAsync();

            var listProdutoECorretoras = corretoras.MapTo<List<CorretoraListDto>>();

            foreach (var corretora in listProdutoECorretoras)
                corretora.ProdutoDePlanoDeSaude = await GetProdutoComValor(produto, input.ProponenteId);

            return new PagedResultDto<CorretoraListDto>(listProdutoECorretoras.Count, listProdutoECorretoras);
        }

        private async Task<ProdutoDePlanoDeSaudeListDto> GetProdutoComValor(ProdutoDePlanoDeSaude produto, int proponenteId)
        {
            var contrato = _contratoService.GetUltimoContratoByProdutoDePlanoDeSaudeId(produto.Id);
            if (contrato != null)
            {
                var valoresPorFaixa = await _valorPorFaixaEtaria.GetAllListAsync(x => x.ContratoId == contrato.Id);
                var proponente = await _proponenteTitularService.GetById(proponenteId);

                if (proponente.PessoaFisica != null &&
                    proponente.PessoaFisica.DataDeNascimento.HasValue)
                {
                    var valores = new List<decimal>();
                    var idade = PessoaHelpers.GetIdade(proponente.PessoaFisica.DataDeNascimento.Value);
                    valores.Add(GetValorPorFaixa(idade, valoresPorFaixa));

                    foreach (var dependente in proponente.Dependentes)
                    {
                        var idadeDependente = PessoaHelpers.GetIdade(dependente.PessoaFisica.DataDeNascimento.Value);
                        valores.Add(GetValorPorFaixa(idadeDependente, valoresPorFaixa));
                    }

                    var produtoFinal = produto.MapTo<ProdutoDePlanoDeSaudeListDto>();
                    produtoFinal.Valor = valores.Sum();

                    return produtoFinal;
                }
            }

            return null;
        }

        private decimal GetValorPorFaixa(int idade, List<ValorPorFaixaEtaria> valores)
        {
            var valorPorFaixa =
                    valores.FirstOrDefault(x => idade.IsBetween(x.FaixaEtaria.IdadeInicial, (x.FaixaEtaria.IdadeFinal.HasValue ? x.FaixaEtaria.IdadeFinal.Value : 200)));

            return valorPorFaixa != null ? valorPorFaixa.Valor : 0;
        }


        public async Task<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> GetById(IdInput input)
        {
            var entity = await _permissaoDeVendaDePlanoDeSaudeParaCorretoraService.GetById(input.Id);

            PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput retorno = new PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput();

            retorno.AdministradoraId = entity.AdministradoraId;
            retorno.Nome = entity.Nome;
            retorno.Id = entity.Id;
            retorno.IsActive = entity.IsActive;

            retorno.Produtos = new List<ProdutoDePlanoDeSaudeListDto>();


            foreach (var item in entity.Produtos)
            {

                var produto = new ProdutoDePlanoDeSaudeListDto();

                produto.Id = item.Id;
                produto.Nome = item.Nome;
                produto.IsActive = item.IsActive;

                retorno.Produtos.Add(produto);
            }

            retorno.Corretoras = new List<CorretoraInput>();



            foreach (var item in entity.Corretoras)
            {

                var corretora = new CorretoraInput();

                corretora.Id = item.Id;
                corretora.Nome = item.Nome;

                retorno.Corretoras.Add(corretora);
            }


            //var permissoes = await base.GetEntityById<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, IdInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input);
            return retorno;
        }

        public async Task<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, IdInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretora_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_permissaoDeVendaDePlanoDeSaudeParaCorretoraService, input);
        }
    }
}
