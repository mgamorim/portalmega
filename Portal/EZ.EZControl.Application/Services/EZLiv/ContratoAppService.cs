﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Contrato, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ContratoAppService : EZControlAppServiceBase<Contrato>, IContratoAppService
    {
        private readonly IContratoService _contratoService;
        private readonly IValorPorFaixaEtariaService _valorPorFaixaEtariaService;
        public ContratoAppService(IRepository<Contrato> contratoRepository,
                                  IContratoService contratoService,
                                  IValorPorFaixaEtariaService valorPorFaixaEtariaService)
            : base(contratoRepository)
        {
            this._contratoService = contratoService;
            this._valorPorFaixaEtariaService = valorPorFaixaEtariaService;
        }

        private void ValidateInput(ContratoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Valores == null || !input.Valores.Any())
                validationErrors.Add(new ValidationResult(L("ValorPorFaixaEtaria.NotFoundValorFaixaEtariaNoContrato")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Contrato_Create, AppPermissions.Pages_Tenant_EZLiv_Contrato_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ContratoInput input)
        {
            IdInput result = null;
            using (var uow = this.UnitOfWorkManager.Begin())
            {

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Contrato_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ContratoInput>(_contratoService, input, () => ValidateInput(input));
                    await SaveValores(result.Id, input);

                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Contrato_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ContratoInput>(_contratoService, input, () => ValidateInput(input));
                    await SaveValores(result.Id, input);
                }

                await uow.CompleteAsync();
            }

            return result;

        }

        private async Task SaveValores(int contratoId, ContratoInput contratoInput)
        {
            Contrato contrato = Repository.Get(contratoId);

            var valores = contratoInput.Valores.MapTo<List<ValorPorFaixaEtaria>>();
            var valoresASeremExcluidos = await _valorPorFaixaEtariaService.GetAllListAsync(x => x.ContratoId == contratoId);

            var idsASeremExcluidos = valoresASeremExcluidos
                                    .Select(x => x.Id)
                                    .Except(valores.Where(x => x.Id > 0).Select(x => x.Id));

            foreach (int id in idsASeremExcluidos)
            {
                await _valorPorFaixaEtariaService.Delete(id);
            }
            foreach (var valor in valores)
            {
                var valorAlterador = valor;
                if (valor.Id > 0)
                    valorAlterador = await _valorPorFaixaEtariaService.GetById(valor.Id);

                valorAlterador.ContratoId = contrato.Id;
                valorAlterador.FaixaEtaria = null;
                valorAlterador.TenantId = AbpSession.TenantId.Value;
                valorAlterador.Valor = valor.Valor;
                await _valorPorFaixaEtariaService.CreateOrUpdateEntity(valorAlterador);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Contrato_Create, AppPermissions.Pages_Tenant_EZLiv_Contrato_Edit)]
        public async Task<ContratoInput> SaveAndReturnEntity(ContratoInput input)
        {
            ContratoInput contrato = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Contrato_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                contrato = await base.CreateAndReturnEntity<ContratoInput, ContratoInput>(_contratoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Contrato_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                contrato = await base.UpdateAndReturnEntity<ContratoInput, ContratoInput>(_contratoService, input, () => ValidateInput(input));
            }

            return contrato;
        }

        public async Task<PagedResultDto<ContratoListDto>> GetPaginado(GetContratoInput input)
        {
            var condicoes = new List<WhereIfCondition<Contrato>>
            {
                new WhereIfCondition<Contrato>(
                        !string.IsNullOrEmpty(input.Conteudo) ||
                        input.ProdutoDePlanoDeSaudeId > 0,
                    a =>
                        a.Conteudo.Contains(input.Conteudo) ||
                        a.ProdutoDePlanoDeSaudeId == input.ProdutoDePlanoDeSaudeId)
            };
            var result = await base.GetListPaged<ContratoListDto, GetContratoInput>(_contratoService, input, condicoes);
            return result;
        }

        public async Task<ContratoInput> GetById(IdInput input)
        {
            var contrato = await base.GetEntityById<ContratoInput, IdInput>(_contratoService, input);
            var valores = await _valorPorFaixaEtariaService.GetAllListAsync(x => x.ContratoId == contrato.Id);
            contrato.Valores = valores.MapTo<List<ValorPorFaixaEtariaInput>>();
            return contrato;
        }

        public async Task<ContratoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ContratoInput, IdInput>(_contratoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Contrato_Delete)]
        public async Task Delete(IdInput input)
        {
            await _valorPorFaixaEtariaService.Delete(x => x.ContratoId == input.Id);
            await base.DeleteEntity(_contratoService, input);
        }
    }
}
