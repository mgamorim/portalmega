﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using System.Threading;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Administradora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class AdministradoraAppService : EZControlAppServiceBase<Administradora>, IAdministradoraAppService
    {
        private readonly IAdministradoraService _administradoraService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IParametroPagSeguroService _parametroPagSeguroService;
        private readonly IArquivoGlobalService _arquivoGlobalService;
        private readonly ICorretoraService _corretoraService;
        private readonly IEmpresaService _empresaService;
        public AdministradoraAppService(IRepository<Administradora> administradoraRepository,
                                        IAdministradoraService administradoraService,
                                        IPessoaJuridicaService pessoaJuridicaService,
                                        IParametroPagSeguroService parametroPagSeguroService,
                                        IArquivoGlobalService arquivoGlobalService,
                                        ICorretoraService corretoraService,
                                        IEmpresaService empresaService)
            : base(administradoraRepository)
        {
            _administradoraService = administradoraService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _parametroPagSeguroService = parametroPagSeguroService;
            _arquivoGlobalService = arquivoGlobalService;
            _corretoraService = corretoraService;
            _empresaService = empresaService;
        }

        private void ValidateInput(AdministradoraInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (input.Id == default(int))
                {
                    var existePessoaJuridicaCadastrada = _administradoraService.GetAll().Any(x => x.PessoaJuridicaId == input.PessoaJuridicaId);

                    if (existePessoaJuridicaCadastrada)
                        validationErrors.Add(new ValidationResult(L("Administradora.ExistePessoaJuridicaAssociadaAAdministradora")));

                    var existePessoaJuridicaCadastradaRazaoSocialIgual = _administradoraService.GetAll().Any(x => x.PessoaJuridica.RazaoSocial == input.PessoaJuridica.RazaoSocial);

                    if (existePessoaJuridicaCadastradaRazaoSocialIgual)
                        validationErrors.Add(new ValidationResult(L("Administradora.ExistePessoaJuridicaAssociadaAAdministradoraComMesmaRazaoSocial")));
                }

                if (input.ParametroPagSeguro != null)
                {
                    if (input.ParametroPagSeguro.Email.IsValidEmail() == false)
                        validationErrors.Add(new ValidationResult(L("EnderecoEletronico.InvalidEnderecoError")));
                }

            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }


        private async Task ProcessInput(Administradora entity, AdministradoraInput input)
        {
            var validationErrors = new List<ValidationResult>();


            //Imagem
            if (input.ImagemId > 0)
            {
                try
                {
                    var imagem = await _arquivoGlobalService.GetById(input.ImagemId);
                    if (imagem != null)
                    {
                        entity.Imagem = imagem;
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.PessoaJuridicaId > 0)
            {
                using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
                {
                    var pj = await _pessoaJuridicaService.GetById(input.PessoaJuridicaId);
                    entity.PessoaJuridica = pj;

                    var errors = entity.IsValidAdministradora();
                    if (errors.Any())
                    {
                        foreach (string erro in errors)
                        {
                            validationErrors.Add(new ValidationResult(L(erro)));
                        }
                    }

                    if (validationErrors.Any())
                    {
                        throw new AbpValidationException(L("ValidationError"), validationErrors);
                    }
                }
            }

            entity.Corretoras.Clear();
            if (input.Corretoras != null && input.Corretoras.Any())
            {
                foreach (var item in input.Corretoras)
                {
                    var corretora = await _corretoraService.GetById(item.Id);
                    entity.Corretoras.Add(corretora);
                }
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Administradora_Create, AppPermissions.Pages_Tenant_EZLiv_Administradora_Edit)]
        public async Task<IdInput> Save(AdministradoraInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Administradora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, AdministradoraInput>(_administradoraService, input, () => ValidateInput(input), ProcessInput);
                await SaveParametroPagSeguro(input.ParametroPagSeguro);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Administradora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, AdministradoraInput>(_administradoraService, input, () => ValidateInput(input), ProcessInput);
                await SaveParametroPagSeguro(input.ParametroPagSeguro);
            }
            return result;
        }

        private async Task SaveParametroPagSeguro(ParametroPagSeguroInput parametroPagseguroInput)
        {
            if (parametroPagseguroInput != null)
            {
                var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(parametroPagseguroInput.PessoaJuridicaId);

                if (parametroPagSeguro == null)
                {
                    parametroPagSeguro = new ParametroPagSeguro
                    {
                        PessoaJuridicaId = parametroPagseguroInput.PessoaJuridicaId
                    };
                }

                parametroPagSeguro.Email = parametroPagseguroInput.Email;
                parametroPagSeguro.Token = parametroPagseguroInput.Token;
                parametroPagSeguro.AppId = parametroPagseguroInput.AppId;
                parametroPagSeguro.AppKey = parametroPagseguroInput.AppKey;

                await _parametroPagSeguroService.SaveParametroPagSeguro(parametroPagSeguro);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Administradora_Create, AppPermissions.Pages_Tenant_EZLiv_Administradora_Edit)]
        public async Task<AdministradoraInput> SaveAndReturnEntity(AdministradoraInput input)
        {
            AdministradoraInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Administradora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<AdministradoraInput, AdministradoraInput>(_administradoraService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Administradora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<AdministradoraInput, AdministradoraInput>(_administradoraService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAllowAnonymous]
        public async Task<PagedResultDto<AdministradoraListDto>> GetPaginado(GetAdministradoraInput input)
        {
            var condicoes = new List<WhereIfCondition<Administradora>>
            {
                 new WhereIfCondition<Administradora>(
                    !string.IsNullOrEmpty(input.NomeFantasia) ||
                    !string.IsNullOrEmpty(input.RazaoSocial),
                    a =>
                        a.PessoaJuridica.NomeFantasia.Contains(input.NomeFantasia) ||
                        a.PessoaJuridica.RazaoSocial.Contains(input.RazaoSocial))
            };

            var result = await base.GetListPaged<AdministradoraListDto, GetAdministradoraInput>(_administradoraService, input, condicoes);
            return result;
        }

        public async Task<AdministradoraInput> GetById(IdInput input)
        {
            var administradoraInput = await base.GetEntityById<AdministradoraInput, IdInput>(_administradoraService, input);
            var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(administradoraInput.PessoaJuridicaId);

            administradoraInput.ParametroPagSeguro = parametroPagSeguro.MapTo<ParametroPagSeguroInput>();

            return administradoraInput;
        }

        public async Task<int> GetImagemIdById(int id = 0)
        {
            IdInput input = new IdInput();

            if (id != 0)
            {                
              input = new IdInput() { Id = id };
            }

            var administradoraInput = await base.GetEntityById<AdministradoraInput, IdInput>(_administradoraService, input);
            //var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(administradoraInput.PessoaJuridicaId);

            //administradoraInput.ParametroPagSeguro = parametroPagSeguro.MapTo<ParametroPagSeguroInput>();

            return administradoraInput.ImagemId;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Administradora_Delete)]
        public async Task Delete(IdInput input)
        {
            var administradora = _administradoraService.GetAll().FirstOrDefault(x => x.Id == input.Id);
            var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(administradora.PessoaJuridicaId);

            if (parametroPagSeguro != null)
            {
                await _parametroPagSeguroService.Delete(x => x.Id == parametroPagSeguro.Id);
            }
            await base.DeleteEntity(_administradoraService, input);
        }

        public async Task<AdministradoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<AdministradoraInput, IdInput>(_administradoraService, input);
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForAdministradora(GetPessoaExceptForAdministradora input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var idsEmpresas = _administradoraService.GetAll().Select(y => y.PessoaJuridicaId);

                var queryPessoaJuridica = _pessoaJuridicaService
                    .GetAll()
                    .Where(x => !idsEmpresas.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                    .Select(x => new PessoaJuridicaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.NomeFantasia,
                        NomeFantasia = x.NomeFantasia,
                        RazaoSocial = x.RazaoSocial,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Juridica
                    });

                IQueryable<PessoaListDto> query = queryPessoaJuridica;

                var count = await query.CountAsync();

                List<PessoaListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
