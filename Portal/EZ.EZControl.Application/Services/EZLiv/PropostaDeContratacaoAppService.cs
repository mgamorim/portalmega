﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Common.Helpers;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZLiv.Geral.RespostaDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Notifications;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZLiv.MapToEntity;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PropostaDeContratacaoAppService : EZControlAppServiceBase<PropostaDeContratacao>, IPropostaDeContratacaoAppService
    {
        private readonly IPropostaDeContratacaoService _propostaService;
        private readonly IProdutoDePlanoDeSaudeService _produtoService;
        private readonly IContratoService _contratoService;
        private readonly IResponsavelService _responsavelService;
        private readonly IProponenteTitularService _titularService;
        private readonly ICorretorService _corretorService;
        private readonly IPedidoService _pedidoService;
        private readonly IProponenteTitularService _proponenteTitularService;
        private readonly IEnderecoEletronicoService _enderecoEletronicoService;
        private readonly IDocumentoService _documentoService;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly ITelefoneService _telefoneService;
        private readonly IEstadoService _estadoService;
        private readonly IProfissaoService _profissaoService;
        private readonly IEmailSender _emailSender;
        private readonly IEmpresaService _empresaService;
        private readonly ICorretoraService _corretoraService;
        private readonly IDependenteService _dependenteService;
        private readonly IRespostaDoItemDeDeclaracaoDeSaudeService _respostaService;
        private readonly IItemDeDeclaracaoDeSaudeService _itemDeDeclaracaoDeSaudeService;
        private readonly IGrupoPessoaService _grupoPessoaService;
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IArquivoDocumentoService _arquivoDocumentoService;
        private readonly ICidadeService _cidadeService;
        private readonly ITipoDeLogradouroService _tipoDeLogradouroService;
        private readonly IEnderecoService _enderecoService;
        private readonly IBeneficiarioBaseService _beneficiarioBaseService;
        private readonly IVigenciaService _vigenciaService;
        private readonly IChancelaService _chancelaService;
        private readonly UserManager _userManager;
        private readonly IAssociacaoService _associacaoService;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;
        private readonly IValorPorFaixaEtariaService _valorPorFaixaEtariaService;
        private readonly IIndiceDeReajustePorFaixaEtariaService _indiceService;
        private readonly IItemDeDeclaracaoDoBeneficiarioService _itemDeDeclaracaoDoBeneficiarioService;
        private readonly IGerenteCorretoraService _gerenteCorretoraService;
        private readonly ISupervisorCorretoraService _supervisorCorretoraService;
        private bool _dependenteParticipaComoUsuario = Convert.ToBoolean(ConfigurationManager.AppSettings["DependenteParticipaComoUsuario"].ToString());
        private readonly IAdministradoraService _administradoraService;
        private readonly ISnapshotPropostaDeContratacaoService _snapshotService;
        private readonly IRelatorioService _relatorioService;
        public PropostaDeContratacaoAppService(IRepository<PropostaDeContratacao> propostaRepository,
                                          IPropostaDeContratacaoService propostaService,
                                          IProdutoDePlanoDeSaudeService produtoService,
                                          IContratoService contratoService,
                                          IResponsavelService responsavelService,
                                          IProponenteTitularService titularService,
                                          ICorretorService corretorService,
                                          IPedidoService pedidoService,
                                          IProponenteTitularService proponenteTitularService,
                                          IEnderecoEletronicoService enderecoEletronicoService,
                                          IDocumentoService documentoService,
                                          ITipoDeDocumentoService tipoDeDocumentoService,
                                          ITelefoneService telefoneService,
                                          IEstadoService estadoService,
                                          IProfissaoService profissaoService,
                                          IEmailSender emailSender,
                                          IEmpresaService empresaService,
                                          ICorretoraService corretoraService,
                                          IDependenteService dependenteService,
                                          IRespostaDoItemDeDeclaracaoDeSaudeService respostaService,
                                          IItemDeDeclaracaoDeSaudeService itemDeDeclaracaoDeSaudeService,
                                          IGrupoPessoaService grupoPessoaService,
                                          IPessoaService pessoaService,
                                          IPessoaFisicaService pessoaFisicaService,
                                          IArquivoDocumentoService arquivoDocumentoService,
                                          ICidadeService cidadeService,
                                          ITipoDeLogradouroService tipoDeLogradouroService,
                                          IEnderecoService enderecoService,
                                          IBeneficiarioBaseService beneficiarioBaseService,
                                          IVigenciaService vigenciaService,
                                          IChancelaService chancelaService,
                                          UserManager userManager,
                                          IAssociacaoService associacaoService,
                                          INotificationSubscriptionManager notificationSubscriptionManager,
                                          IAppNotifier appNotifier,
                                          IValorPorFaixaEtariaService valorPorFaixaEtariaService,
                                          IIndiceDeReajustePorFaixaEtariaService indiceService,
                                          IItemDeDeclaracaoDoBeneficiarioService itemDeDeclaracaoDoBeneficiarioService,
                                          IGerenteCorretoraService gerenteCorretoraService,
                                          ISupervisorCorretoraService supervisorCorretoraService,
                                          IAdministradoraService administradoraService,
                                          ISnapshotPropostaDeContratacaoService snapshotService,
                                          IRelatorioService relatorioService)
            : base(propostaRepository)
        {
            _propostaService = propostaService;
            _produtoService = produtoService;
            _contratoService = contratoService;
            _responsavelService = responsavelService;
            _titularService = titularService;
            _corretorService = corretorService;
            _pedidoService = pedidoService;
            _proponenteTitularService = proponenteTitularService;
            _enderecoEletronicoService = enderecoEletronicoService;
            _documentoService = documentoService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _telefoneService = telefoneService;
            _estadoService = estadoService;
            _profissaoService = profissaoService;
            _emailSender = emailSender;
            _empresaService = empresaService;
            _corretoraService = corretoraService;
            _dependenteService = dependenteService;
            _respostaService = respostaService;
            _itemDeDeclaracaoDeSaudeService = itemDeDeclaracaoDeSaudeService;
            _grupoPessoaService = grupoPessoaService;
            _pessoaService = pessoaService;
            _pessoaFisicaService = pessoaFisicaService;
            _arquivoDocumentoService = arquivoDocumentoService;
            _cidadeService = cidadeService;
            _tipoDeLogradouroService = tipoDeLogradouroService;
            _enderecoService = enderecoService;
            _beneficiarioBaseService = beneficiarioBaseService;
            _vigenciaService = vigenciaService;
            _chancelaService = chancelaService;
            _userManager = userManager;
            _associacaoService = associacaoService;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
            _valorPorFaixaEtariaService = valorPorFaixaEtariaService;
            _indiceService = indiceService;
            _itemDeDeclaracaoDoBeneficiarioService = itemDeDeclaracaoDoBeneficiarioService;
            _gerenteCorretoraService = gerenteCorretoraService;
            _supervisorCorretoraService = supervisorCorretoraService;
            _administradoraService = administradoraService;
            _snapshotService = snapshotService;
            _relatorioService = relatorioService;
        }

        private async Task<Empresa> GetEmpresa(int IdCorretora)
        {
            //TODO: Está pegando o Id 1 (Megavita) temporariamente, mover seleção de corretora para alguma aba
            return await _empresaService.GetById(IdCorretora);
        }

        private void ValidateInput(PropostaDeContratacaoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.TitularId == 0)
            {
                validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyTitularError")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(PropostaDeContratacao entity, PropostaDeContratacaoInput input)
        {
            if (input.TitularId > 0)
            {
                try
                {
                    var titular = await _titularService.GetById(input.TitularId);
                    if (titular != null)
                    {
                        entity.Titular = titular;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.TitularNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.ProdutoId > 0)
            {
                try
                {
                    var produto = await _produtoService.GetById(input.ProdutoId);
                    if (produto != null)
                    {
                        entity.Produto = produto;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.ProdutoNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.ResponsavelId > 0)
            {
                try
                {
                    var responsavel = await _responsavelService.GetById(input.ResponsavelId);
                    if (responsavel != null)
                    {
                        entity.Responsavel = responsavel;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.ResponsavelNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.ContratoVigenteNoAceiteId.HasValue)
            {
                try
                {
                    var contratoVigente = await _contratoService.GetById(input.ContratoVigenteNoAceiteId.Value);
                    if (contratoVigente != null)
                    {
                        entity.ContratoVigenteNoAceite = contratoVigente;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.ContratoNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.UltimoContratoAceitoId.HasValue)
            {
                try
                {
                    var ultimoContrato = await _contratoService.GetById(input.UltimoContratoAceitoId.Value);
                    if (ultimoContrato != null)
                    {
                        entity.UltimoContratoAceito = ultimoContrato;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.ContratoNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            try
            {
                entity = await ValidaUsuarioLogado(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            if (input.PedidoId.HasValue)
            {
                try
                {
                    var pedido = await _pedidoService.GetById(input.PedidoId.Value);

                    if (pedido != null)
                    {
                        entity.Pedido = pedido;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.PedidoNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

        }

        private async Task<PropostaDeContratacao> ValidaUsuarioLogado(PropostaDeContratacao entity)
        {
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);

            if (user.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
            {
                var pessoaId = user.Pessoa != null ? user.Pessoa.Id : 0;

                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == pessoaId).FirstOrDefaultAsync();


                if (corretor != null)
                {
                    entity.Corretor = corretor;
                }
                else
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.CorretorNotFoundError"));
                }
            }
            else
            {
                //TODO: Está pegando o Id 1 (Megavita) temporariamente, mover seleção de corretora para alguma aba
                var empresa = await _empresaService.GetById(user.Empresas.First().Id);
                //var empresa = await _empresaService.GetEmpresaLogada();

                var corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();
                if (corretora != null)
                {
                    if (corretora.Corretor != null)
                    {
                        entity.Corretor = corretora.Corretor;
                        entity.CorretorId = corretora.CorretorId.Value;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Corretora.CorretorNotSetError"));
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("Corretora.NotFoundError"));
                }
            }
            return entity;
        }

        private async Task<PropostaDeContratacao> ValidaUsuarioLogadoSimulador(PropostaDeContratacao entity, int IdUser, int IdCorretora)
        {
            var user = await UserManager.FindByIdAsync(IdUser);
            if (user.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
            {
                var pessoaId = user.Pessoa != null ? user.Pessoa.Id : 0;
                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == pessoaId).FirstOrDefaultAsync();
                if (corretor != null)
                {
                    entity.Corretor = corretor;
                }
                else
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.CorretorNotFoundError"));
                }
            }
            else
            {
                //TODO: Está pegando o Id 1 (Megavita) temporariamente, mover seleção de corretora para alguma aba
                var empresa = await _empresaService.GetById(IdCorretora);
                //var empresa = await _empresaService.GetEmpresaLogada();

                var corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();
                if (corretora != null)
                {
                    if (corretora.Corretor != null)
                    {
                        entity.Corretor = corretora.Corretor;
                        entity.CorretorId = corretora.CorretorId.Value;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Corretora.CorretorNotSetError"));
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("Corretora.NotFoundError"));
                }
            }
            return entity;
        }

        public async Task<PropostaDeContratacaoDataDeVigenciaInput> GetDataInicioDeVigencia(VigenciaListDto input)
        {
            var today = DateTime.Now;
            var day = input.DataDeVigencia;
            var month = day < input.DataDeFechamento ? today.AddMonths(1) : today.AddMonths(2);
            var year = today.Year;

            var result = new PropostaDeContratacaoDataDeVigenciaInput
            {
                InicioDaVigencia = new DateTime(year, month.Month, day)
            };

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create, AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit)]
        public async Task<IdInput> Save(PropostaDeContratacaoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, PropostaDeContratacaoInput>(_propostaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, PropostaDeContratacaoInput>(_propostaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task UpdateStatusEncerrado(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            proposta.StatusDaProposta = StatusDaPropostaEnum.Encerrado;
            Repository.Update(proposta.MapTo<PropostaDeContratacao>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create, AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit)]
        public async Task<PropostaDeContratacaoInput> SaveAndReturnEntity(PropostaDeContratacaoInput input)
        {
            PropostaDeContratacaoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<PropostaDeContratacaoInput, PropostaDeContratacaoInput>(_propostaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<PropostaDeContratacaoInput, PropostaDeContratacaoInput>(_propostaService, input, () => ValidateInput(input), ProcessInput);

            }

            return result;
        }

        public async Task<PagedResultDto<PropostaDeContratacaoInput>> GetPaginado(GetPropostaDeContratacaoInput input)
        {

            try
            {
                var roles = await _userManager.GetRolesAsync(AbpSession.UserId.Value);

                var user = await _userManager.FindByIdAsync(AbpSession.UserId.Value);

                var isBeneficiario = roles.Where(x => x == StaticRoleNames.Tenants.Beneficiario).ToList().Count() > 0;
                var isAdministradora = roles.Where(x => x == StaticRoleNames.Tenants.Administradora).ToList().Count() > 0;
                var isCorretor = roles.Where(x => x == StaticRoleNames.Tenants.Corretor).ToList().Count() > 0;
                var isCorretora = roles.Where(x => x == StaticRoleNames.Tenants.Corretora).ToList().Count() > 0;

                var condicoes = new List<WhereIfCondition<PropostaDeContratacao>>
            {

                 new WhereIfCondition<PropostaDeContratacao>(
                        !string.IsNullOrEmpty(input.NumeroDaProposta) ||
                        !string.IsNullOrEmpty(input.CPFDoBeneficiario) ||
                        !string.IsNullOrEmpty(input.NomeDoBeneficiario),
                    a =>
                        a.Numero.Contains(input.NumeroDaProposta) ||
                        a.Titular.PessoaFisica.Nome.ToLower().Contains(input.NomeDoBeneficiario.ToLower()) ||
                        a.Titular.PessoaFisica.Documentos
                            .FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf)
                            .Numero == input.CPFDoBeneficiario),

                 new WhereIfCondition<PropostaDeContratacao>(isBeneficiario, x => x.StatusDaProposta != StatusDaPropostaEnum.Encerrado),
                 new WhereIfCondition<PropostaDeContratacao>(isCorretor, x => x.StatusDaProposta != StatusDaPropostaEnum.Encerrado)
            };

                condicoes.Add(new WhereIfCondition<PropostaDeContratacao>(
                            isBeneficiario,
                            x => x.UsuarioResponsavelId == AbpSession.UserId.Value || x.UsuarioTitularId == AbpSession.UserId.Value
                        ));

                if (isAdministradora)
                {
                    var administradora = await _administradoraService.GetByEmpresaLogada();
                    if (administradora == null)
                    {
                        // Mensagem somente para a EZLiv - Não deve aparecer para outros usuários
                        throw new UserFriendlyException("A administradora não foi encontrada a partir da empresa logada. Você está na empresa correta?");
                    }
                    condicoes.Add(new WhereIfCondition<PropostaDeContratacao>(true, r => r.Produto.AdministradoraId == administradora.Id));
                }

                if (isCorretor)
                {
                    var corretor = await _corretorService.GetAll().Where(x => x.PessoaId == user.PessoaId).FirstOrDefaultAsync();
                    condicoes.Add(new WhereIfCondition<PropostaDeContratacao>(true, r => r.CorretorId == corretor.Id));
                }

                if (isCorretora)
                {
                    var corretora = await _corretoraService.GetByEmpresaLogada();
                    if (corretora == null)
                    {
                        // Mensagem somente para a EZLiv - Não deve aparecer para outros usuários 
                        throw new UserFriendlyException("A corretora não foi encontrada a partir da empresa logada. Você está na empresa correta?");
                    }
                    condicoes.Add(new WhereIfCondition<PropostaDeContratacao>(true, r => r.CorretoraId == corretora.Id));
                }
                return await base.GetListPaged<PropostaDeContratacaoInput, GetPropostaDeContratacaoInput>(_propostaService, input, condicoes, true);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PropostaDeContratacaoInput> GetById(IdInput input)
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                //  var proposta = await base.GetEntityById<PropostaDeContratacaoInput, IdInput>(_propostaService, input);
                var DtoPropostaContratacao = await _propostaService.GetById(input.Id);
                MapManual map = new MapManual();
                var MapProposta = await map.MapManualTask(DtoPropostaContratacao);


                //if (proposta.PassoDaProposta >= PassoDaPropostaEnum.Homologacao)
                //{
                //    var snapshot = await _snapshotService.GetAll().Where(x => x.PropostaDeContratacaoId == proposta.Id).FirstOrDefaultAsync();
                //    string jsonString = Encoding.ASCII.GetString(snapshot.Json);
                //    var jsonSettings = new JsonSerializerSettings
                //    {
                //        TypeNameHandling = TypeNameHandling.Objects,
                //        TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
                //    };
                //    proposta = JsonConvert.DeserializeObject<PropostaDeContratacaoInput>(jsonString, jsonSettings);
                //}

                return MapProposta;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PropostaDeContratacao_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_propostaService, input);
        }

        public async Task<PropostaDeContratacaoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PropostaDeContratacaoInput, IdInput>(_propostaService, input);
        }

        #region Passo Pre-Cadastro       

        private async Task<PropostaDeContratacao> ProcessInputPassoPreCadastro(PropostaDeContratacao proposta, PropostaDeContratacaoPassoPreCadastroInput input, bool Simulador, int IdUser)
        {
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);

            //NESTE MOMENTO, TODAS AS PROPOSTAS SÃO POR ADESÃO
            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;

            #region Validate

            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (string.IsNullOrEmpty(input.NomeCompleto))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNome")));

                if (!string.IsNullOrEmpty(input.NomeCompleto) && !input.NomeCompleto.Contains(" "))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNomeCompleto")));

                if (string.IsNullOrEmpty(input.Cpf))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCpf")));

                if (input.DataDeNascimento.Date.ToString() != "01/01/0001 00:00:00")
                {
                    var dateOfBirth = input.DataDeNascimento;
                    var today = DateTime.Today;
                    var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                    var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

                    if (input.TitularResponsavelLegal && ((a - b) / 10000) < 18)
                    {
                        validationErrors.Add(new ValidationResult(L("Proponente.DataDeNascimentoOfAgeError")));
                    }

                }
                else
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyDataDeNascimentoError")));
                }


                if (string.IsNullOrEmpty(input.Celular))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCelular")));

                if (string.IsNullOrEmpty(input.Email))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEmail")));

                if (input.EstadoId == default(int))
                    validationErrors.Add(new ValidationResult("Selecione um Estado!"));

                if (input.ProfissaoId == default(int))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyProfissaoError")));

                var usuario = await UserManager.FindByEmailAsync(input.Email);
                if (usuario != null && usuario.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
                    validationErrors.Add(new ValidationResult("E-mail do Usuário já está cadastrado como corretor!"));

                if (input.Dependentes != null && input.Dependentes.Any())
                    ValidationDependente(validationErrors, input.Dependentes);

                if (validationErrors.Any())
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
            }

            #endregion

            #region Beneficiario
            if (input.TitularId > 0)
            {
                ProponenteTitular titular = null;
                try
                {
                    titular = await _proponenteTitularService.GetById(input.TitularId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.TitularNotFoundError"));
                }

                proposta.Titular = titular;
            }
            else
            {
                proposta.Titular = new ProponenteTitular();
            };

            var pessoaFisica = await _pessoaFisicaService.GetPessoaByCpf(input.Cpf);
            if (pessoaFisica != null)
            {
                if (pessoaFisica.Nome != input.NomeCompleto)
                    pessoaFisica.Nome = input.NomeCompleto;

                if (pessoaFisica.DataDeNascimento != input.DataDeNascimento)
                    pessoaFisica.DataDeNascimento = input.DataDeNascimento;

                var celular = pessoaFisica.Telefones.FirstOrDefault(x => x.Tipo == TipoTelefoneEnum.Celular);
                if (celular != null)
                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(celular, input.Celular);

                var pf = await _pessoaFisicaService.UpdateAndReturnEntity(pessoaFisica);
                proposta.Titular.PessoaFisica = pf;
            }
            else
            {
                if (input.PessoaId > 0)
                {
                    PessoaFisica pf = null;
                    try
                    {
                        pf = await _pessoaFisicaService.GetById(input.PessoaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.PessoaFisicaNotFoundError"));
                    }
                    proposta.Titular.PessoaFisica = pf;
                }
                else
                    proposta.Titular.PessoaFisica = new PessoaFisica();

                proposta.Titular.PessoaFisica.Nome = input.NomeCompleto;
                proposta.Titular.PessoaFisica.DataDeNascimento = input.DataDeNascimento;

                #region CPF  
                Documento documento = null;
                if (input.CpfId > 0 && proposta.Titular.PessoaFisica.Documentos != null)
                //if (input.CpfId > 0 )
                {
                    try
                    {
                        documento = await _documentoService.GetById(input.CpfId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                    }
                    proposta.Titular.PessoaFisica.Documentos.First(x => x.Id == input.CpfId).Numero = input.Cpf;
                }
                else
                {
                    TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                    documento = new Documento();
                    documento.Numero = input.Cpf;
                    documento.AssociarTipoDocumento(tipo, _documentoService);
                    documento.Empresa = empresa;
                    documento.EmpresaId = empresa.Id;
                    proposta.Titular.PessoaFisica.Documentos = new List<Documento>();
                    proposta.Titular.PessoaFisica.Documentos.Add(documento);
                }
                #endregion

                #region Email
                EnderecoEletronico email = null;
                //if (input.EmailId > 0 && proposta.Titular.PessoaFisica.EnderecosEletronicos != null)
                if (input.EmailId > 0)
                {
                    try
                    {
                        email = await _enderecoEletronicoService.GetById(input.EmailId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                    }
                    email.Endereco = input.Email;
                    proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
                }
                else
                {
                    email = new EnderecoEletronico();
                    email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                    email.Endereco = input.Email;
                    email.Empresa = empresa;
                    email.EmpresaId = empresa.Id;

                    proposta.Titular.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                    proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
                }
                #endregion

                #region Celular
                Telefone telefone = null;
                if (input.CelulaId > 0)
                {
                    try
                    {
                        telefone = await _telefoneService.GetById(input.CelulaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.CelularNotFoundError"));
                    }

                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                }
                else
                {
                    telefone = new Telefone();
                    telefone.Tipo = TipoTelefoneEnum.Celular;
                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                    telefone.Empresa = empresa;
                    telefone.EmpresaId = empresa.Id;

                    proposta.Titular.PessoaFisica.Telefones = new List<Telefone>();
                    proposta.Titular.PessoaFisica.Telefones.Add(telefone);
                }
                #endregion
            }
            #endregion

            #region Grupo de Pessoa
            if (input.GrupoPessoaId > 0)
            {
                try
                {
                    var grupoPessoa = await _grupoPessoaService.GetById(input.GrupoPessoaId);
                    proposta.Titular.PessoaFisica.AssociarGrupoPessoa(grupoPessoa, _pessoaService);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.GrupoPessoaNotFoundError"));
                }
            }
            #endregion

            #region Estado
            Estado estado = null;
            if (input.EstadoId > 0)
            {
                try
                {
                    estado = await _estadoService.GetById(input.EstadoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.EstadoNotFoundError"));
                }

                proposta.Estado = estado;
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.EstadoNotFoundError"));
            }
            #endregion

            #region Profissão
            Profissao profissao = null;
            if (input.ProfissaoId > 0)
            {
                try
                {
                    profissao = await _profissaoService.GetById(input.ProfissaoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.ProfissaoNotFoundError"));
                }

                proposta.Profissao = profissao;
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.ProfissaoNotFoundError"));
            }
            #endregion

            #region Corretora
            try
            {
                try
                {
                    //TODO: Está pegando o Id 1 (Megavita) temporariamente, mover seleção de corretora para alguma aba
                    empresa = await _empresaService.GetById(user.Empresas.First().Id);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                }

                Corretora corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();
                if (corretora != null)
                {
                    proposta.Corretora = corretora;
                    proposta.CorretoraId = corretora.Id;
                }
                else
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.CorretoraNotFoundError"));
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            #endregion

            #region Corretor
            try
            {
                proposta = await ValidaUsuarioLogado(proposta);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            #endregion

            #region Dependentes

            if (proposta.Titular != null)
            {
                await PreencherDependentes(proposta, input.Dependentes);
            }

            #endregion

            #region User

            if (proposta.Id == 0)
            {
                //var empresaUser = await _empresaService.GetEmpresaDefault();
                user = _beneficiarioBaseService.CreateUserBeneficiario(proposta.Titular, user.Empresas.First(), TipoDeUsuarioEnum.Beneficiario, StaticRoleNames.Tenants.Beneficiario);
                proposta.UsuarioTitular = user;
                proposta.UsuarioTitularId = user.Id;
            }
            #endregion

            proposta.TitularResponsavelLegal = input.TitularResponsavelLegal;

            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.PreCadastro)
                proposta.PassoDaProposta = PassoDaPropostaEnum.SelecionarPlano;

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputPassoPreCadastroSimulador(PropostaDeContratacao proposta, PropostaDeContratacaoPassoPreCadastroInput input, bool Simulador, int IdUser, int IdCorretora, int IdPlano)
        {


            var empresa = await GetEmpresa(IdCorretora);
            //NESTE MOMENTO, TODAS AS PROPOSTAS SÃO POR ADESÃO
            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;

            #region Validate

            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (string.IsNullOrEmpty(input.NomeCompleto))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNome")));

                if (!string.IsNullOrEmpty(input.NomeCompleto) && !input.NomeCompleto.Contains(" "))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNomeCompleto")));

                if (string.IsNullOrEmpty(input.Cpf))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCpf")));

                if (input.DataDeNascimento.Date.ToString() != "01/01/0001 00:00:00")
                {
                    var dateOfBirth = input.DataDeNascimento;
                    var today = DateTime.Today;
                    var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                    var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

                    if (input.TitularResponsavelLegal && ((a - b) / 10000) < 18)
                    {
                        validationErrors.Add(new ValidationResult(L("Proponente.DataDeNascimentoOfAgeError")));
                    }

                }
                else
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyDataDeNascimentoError")));
                }


                if (string.IsNullOrEmpty(input.Celular))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCelular")));

                if (string.IsNullOrEmpty(input.Email))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEmail")));

                if (input.ProfissaoId == default(int))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyProfissaoError")));

                if (input.Dependentes != null && input.Dependentes.Any())
                    ValidationDependente(validationErrors, input.Dependentes);

                if (validationErrors.Any())
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
            }

            #endregion

            #region Beneficiario
            if (input.TitularId > 0)
            {
                ProponenteTitular titular = null;
                try
                {
                    titular = await _proponenteTitularService.GetById(input.TitularId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.TitularNotFoundError"));
                }

                proposta.Titular = titular;
            }
            else
            {
                proposta.Titular = new ProponenteTitular();
            };

            var pessoaFisica = await _pessoaFisicaService.GetPessoaByCpf(input.Cpf);
            if (pessoaFisica != null)
            {
                if (pessoaFisica.Nome != input.NomeCompleto)
                    pessoaFisica.Nome = input.NomeCompleto;

                if (pessoaFisica.DataDeNascimento != input.DataDeNascimento)
                    pessoaFisica.DataDeNascimento = input.DataDeNascimento;

                var celular = pessoaFisica.Telefones.FirstOrDefault(x => x.Tipo == TipoTelefoneEnum.Celular);
                if (celular != null)
                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(celular, input.Celular);

                var pf = await _pessoaFisicaService.UpdateAndReturnEntity(pessoaFisica);
                proposta.Titular.PessoaFisica = pf;
            }
            else
            {
                if (input.PessoaId > 0)
                {
                    PessoaFisica pf = null;
                    try
                    {
                        pf = await _pessoaFisicaService.GetById(input.PessoaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.PessoaFisicaNotFoundError"));
                    }
                    proposta.Titular.PessoaFisica = pf;
                }
                else
                    proposta.Titular.PessoaFisica = new PessoaFisica();

                proposta.Titular.PessoaFisica.Nome = input.NomeCompleto;
                proposta.Titular.PessoaFisica.DataDeNascimento = input.DataDeNascimento;

                #region CPF  
                Documento documento = null;
                if (input.CpfId > 0 && proposta.Titular.PessoaFisica.Documentos != null)
                //if (input.CpfId > 0 )
                {
                    try
                    {
                        documento = await _documentoService.GetById(input.CpfId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                    }
                    proposta.Titular.PessoaFisica.Documentos.First(x => x.Id == input.CpfId).Numero = input.Cpf;
                }
                else
                {
                    TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                    documento = new Documento();
                    documento.Numero = input.Cpf;
                    documento.AssociarTipoDocumento(tipo, _documentoService);
                    documento.Empresa = empresa;
                    documento.EmpresaId = empresa.Id;
                    proposta.Titular.PessoaFisica.Documentos = new List<Documento>();
                    proposta.Titular.PessoaFisica.Documentos.Add(documento);
                }
                #endregion

                #region Email
                EnderecoEletronico email = null;
                //if (input.EmailId > 0 && proposta.Titular.PessoaFisica.EnderecosEletronicos != null)
                if (input.EmailId > 0)
                {
                    try
                    {
                        email = await _enderecoEletronicoService.GetById(input.EmailId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                    }
                    email.Endereco = input.Email;
                    proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
                }
                else
                {
                    email = new EnderecoEletronico();
                    email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                    email.Endereco = input.Email;
                    email.Empresa = empresa;
                    email.EmpresaId = empresa.Id;

                    proposta.Titular.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                    proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
                }
                #endregion

                #region Celular
                Telefone telefone = null;
                if (input.CelulaId > 0)
                {
                    try
                    {
                        telefone = await _telefoneService.GetById(input.CelulaId);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("PropostaDeContratacao.CelularNotFoundError"));
                    }

                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                }
                else
                {
                    telefone = new Telefone();
                    telefone.Tipo = TipoTelefoneEnum.Celular;
                    _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                    telefone.Empresa = empresa;
                    telefone.EmpresaId = empresa.Id;

                    proposta.Titular.PessoaFisica.Telefones = new List<Telefone>();
                    proposta.Titular.PessoaFisica.Telefones.Add(telefone);
                }
                #endregion
            }
            #endregion

            #region Grupo de Pessoa
            if (input.GrupoPessoaId > 0)
            {
                try
                {
                    var grupoPessoa = await _grupoPessoaService.GetById(input.GrupoPessoaId);
                    proposta.Titular.PessoaFisica.AssociarGrupoPessoa(grupoPessoa, _pessoaService);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.GrupoPessoaNotFoundError"));
                }
            }
            #endregion

            #region Estado
            Estado estado = null;
            if (input.EstadoId > 0)
            {
                try
                {
                    estado = await _estadoService.GetById(input.EstadoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.EstadoNotFoundError"));
                }

                proposta.Estado = estado;
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.EstadoNotFoundError"));
            }
            #endregion

            #region Profissão
            Profissao profissao = null;
            if (input.ProfissaoId > 0)
            {
                try
                {
                    profissao = await _profissaoService.GetById(input.ProfissaoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.ProfissaoNotFoundError"));
                }

                proposta.Profissao = profissao;
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.ProfissaoNotFoundError"));
            }
            #endregion

            #region Corretora
            try
            {
                try
                {
                    //TODO: Está pegando o Id 1 (Megavita) temporariamente, mover seleção de corretora para alguma aba
                    empresa = await _empresaService.GetById(IdCorretora);

                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
                }

                Corretora corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();
                if (corretora != null)
                {
                    proposta.Corretora = corretora;
                    proposta.CorretoraId = corretora.Id;

                }
                else
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.CorretoraNotFoundError"));
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            #endregion

            #region Corretor
            try
            {
                proposta = await ValidaUsuarioLogadoSimulador(proposta, IdUser, IdCorretora);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
            #endregion

            #region Dependentes

            if (proposta.Titular != null)
            {
                await PreencherDependentes(proposta, input.Dependentes);
            }

            #endregion

            #region User

            if (proposta.Id == 0)
            {
                var empresaUser = new Empresa();

                if (empresa == null)
                    empresa = await _empresaService.GetEmpresaDefault();


                var user = _beneficiarioBaseService.CreateUserBeneficiario(proposta.Titular, empresa, TipoDeUsuarioEnum.Beneficiario, StaticRoleNames.Tenants.Beneficiario);
                proposta.UsuarioTitular = user;
                proposta.UsuarioTitularId = user.Id;
            }
            #endregion

            proposta.TitularResponsavelLegal = input.TitularResponsavelLegal;

            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.PreCadastro)
                proposta.PassoDaProposta = PassoDaPropostaEnum.SelecionarPlano;

            return proposta;
        }

        private async Task PreencherDependentes(PropostaDeContratacao proposta, List<DependenteInput> listDependenteInput)
        {
            if (proposta.Titular != null && proposta.Titular.Dependentes != null && proposta.Titular.Dependentes.Any())
            {
                var idsInput = listDependenteInput.Where(x => x.Id > 0).Select(x => x.Id);
                var idsDependentesParaExcluir = (listDependenteInput.Any()) ?
                                        proposta.Titular.Dependentes.Where(x => !idsInput.Contains(x.Id)).Select(x => x.Id).Cast<int>().ToList()
                                        : proposta.Titular.Dependentes.Select(x => x.Id).Cast<int>().ToList();

                for (int i = 0; i < idsDependentesParaExcluir.Count(); i++)
                {
                    if (idsDependentesParaExcluir[i] > 0)
                        if (proposta.Id > 0)
                        {
                            var dependenteId = idsDependentesParaExcluir[i];
                            var respostasDependente = await _respostaService.GetAll().Where(x => x.PropostaDeContratacaoId == proposta.Id && x.BeneficiarioId == dependenteId).ToListAsync();
                            if (respostasDependente.Any())
                            {
                                var idsRespostas = respostasDependente.Select(x => x.Id).ToList();
                                await _respostaService.Delete(x => idsRespostas.Contains(x.Id));
                            }
                        }

                    await _dependenteService.Delete(idsDependentesParaExcluir[i]);
                }
            }

            if (proposta.Titular.Dependentes == null || !proposta.Titular.Dependentes.Any())
                proposta.Titular.Dependentes = new List<Dependente>();

            Empresa empresa = null;

            Administradora administradora = proposta.ProdutoId.HasValue && proposta.ProdutoId.Value > 0 ?
                await _administradoraService.GetById(proposta.Produto.AdministradoraId.Value) : null;

            Corretora corretora = proposta.CorretoraId.HasValue && proposta.CorretoraId.Value > 0 ?
                await _corretoraService.GetById(proposta.CorretoraId.Value) : null;

            try
            {
                if (corretora != null)
                {
                    empresa = await _empresaService
                    .GetAll()
                    .Where(x => x.PessoaJuridicaId == corretora.PessoaJuridicaId)
                    .FirstOrDefaultAsync();
                }
                else
                {
                    empresa = await _empresaService.GetEmpresaDefault();
                }
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
            }


            if (listDependenteInput != null)
            {
                foreach (var dependenteInput in listDependenteInput)
                {
                    var dependente = (dependenteInput.Id > 0) ? proposta.Titular.Dependentes.FirstOrDefault(x => x.Id == dependenteInput.Id) : new Dependente();
                    dependente.GrauDeParentesco = dependenteInput.GrauDeParentesco;

                    if (dependente.PessoaFisica == null)
                        dependente.PessoaFisica = new PessoaFisica();

                    dependente.PessoaFisica.Documentos = new List<Documento>();
                    dependente.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();

                    dependente.PessoaFisica.Nome = dependenteInput.PessoaFisica.Nome;
                    dependente.PessoaFisica.DataDeNascimento = dependenteInput.PessoaFisica.DataDeNascimento;
                    dependente.PessoaFisica.Sexo = dependenteInput.PessoaFisica.Sexo;
                    dependente.PessoaFisica.EstadoCivil = dependenteInput.PessoaFisica.EstadoCivil;
                    dependente.PessoaFisica.Nacionalidade = dependenteInput.PessoaFisica.Nacionalidade;
                    dependente.NomeDaMae = dependenteInput.NomeDaMae;
                    dependente.NumeroDoCartaoNacionalDeSaude = dependenteInput.NumeroDoCartaoNacionalDeSaude;
                    dependente.DeclaracaoDeNascidoVivo = dependenteInput.DeclaracaoDeNascidoVivo;

                    dependente.PessoaFisica.identidade = dependenteInput.PessoaFisica.identidade;
                    dependente.PessoaFisica.identidademae = dependenteInput.PessoaFisica.identidademae;
                    dependente.PessoaFisica.CPFmae = dependenteInput.PessoaFisica.CPFmae;



                    dependente.TenantId = AbpSession.TenantId.Value;

                    if (dependente.Administradoras == null) dependente.Administradoras = new List<Administradora>();

                    if (administradora != null && !dependente.Administradoras.Any(x => x.Id == administradora.Id))
                    {
                        dependente.Administradoras.Add(administradora);
                    }

                    if (dependente.Corretoras == null) dependente.Corretoras = new List<Corretora>();

                    if (corretora != null && !dependente.Corretoras.Any(x => x.Id == corretora.Id))
                    {
                        dependente.Corretoras.Add(corretora);
                    }

                    #region CPF  
                    Documento documento = null;
                    if (dependenteInput.PessoaFisica.CpfId.HasValue && dependenteInput.PessoaFisica.CpfId > 0)
                    {
                        try
                        {
                            documento = await _documentoService.GetById(dependenteInput.PessoaFisica.CpfId.Value);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                        }
                    }
                    else if (!string.IsNullOrEmpty(dependenteInput.PessoaFisica.Cpf))
                    {
                        if (!_tipoDeDocumentoService.IsCpf(dependenteInput.PessoaFisica.Cpf))
                        {
                            throw new UserFriendlyException(L("TipoDeDocumento.MessagemDeErroValidacaoCpf"));
                        }

                        TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                        documento = new Documento();
                        documento.Numero = String.IsNullOrEmpty(dependenteInput.PessoaFisica.Cpf) ? string.Empty : dependenteInput.PessoaFisica.Cpf;
                        documento.AssociarTipoDocumento(tipo, _documentoService);
                        documento.Empresa = empresa;
                        documento.EmpresaId = empresa.Id;
                        dependente.PessoaFisica.Documentos.Add(documento);
                    }

                    #endregion

                    #region Email
                    EnderecoEletronico email = null;
                    if (dependenteInput.PessoaFisica.EmailId > 0)
                    {
                        try
                        {
                            email = await _enderecoEletronicoService.GetById(dependenteInput.PessoaFisica.EmailId);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dependenteInput.PessoaFisica.Email))
                        {
                            email = new EnderecoEletronico();
                            email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                            email.Endereco = dependenteInput.PessoaFisica.Email;
                            email.Empresa = empresa;
                            email.EmpresaId = empresa.Id;

                            dependente.PessoaFisica.EnderecosEletronicos.Add(email);
                        }

                    }
                    #endregion

                    if (dependenteInput.Id == 0)
                        proposta.Titular.Dependentes.Add(dependente);
                }
            }

            #region User

            foreach (var dependente in proposta.Titular.Dependentes.Where(d => d.PessoaFisica.EnderecosEletronicos.Any()))
            {
                var emailTitular = proposta.Titular.PessoaFisica.Email;
                var emailDependente = dependente.PessoaFisica.Email;
                var emailResponsavel = proposta.Responsavel != null
                    ? proposta.Responsavel.PessoaFisica.Email
                    : string.Empty;

                bool mesmoEmail = false;
                if (!string.IsNullOrEmpty(emailDependente) && _dependenteParticipaComoUsuario)
                {
                    mesmoEmail = (!string.IsNullOrEmpty(emailTitular) && emailDependente == emailTitular) ||
                                 (!string.IsNullOrEmpty(emailResponsavel) && emailDependente == emailResponsavel);

                    if (!mesmoEmail)
                    {
                        var empresaUser = await _empresaService.GetEmpresaDefault();
                        var user = _dependenteService.CreateUserDependente(dependente, empresaUser, TipoDeUsuarioEnum.Beneficiario, StaticRoleNames.Tenants.Beneficiario);
                        if (proposta.UsuariosDependentes == null) proposta.UsuariosDependentes = new List<User>();
                        proposta.UsuariosDependentes.Add(user);
                        await SendEmailUserBeneficiario(user.MapTo<UserDto>());
                    }
                }


            }

            #endregion
        }

        private void ValidationDependente(List<ValidationResult> validationErrors, List<DependenteInput> dependentes)
        {
            foreach (var dependente in dependentes)
            {
                if (dependente.PessoaFisica == null || string.IsNullOrEmpty(dependente.PessoaFisica.Nome))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNomeDependenteError")));

                if (dependente.PessoaFisica == null || !dependente.PessoaFisica.DataDeNascimento.HasValue)
                    validationErrors.Add(
                        new ValidationResult(L("PropostaDeContratacao.EmptyDataNascimentoDependenteError")));

                if (dependente.PessoaFisica == null || dependente.PessoaFisica.Sexo == null)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptySexoDependenteError")));

                if (dependente.PessoaFisica == null || dependente.PessoaFisica.EstadoCivil == null)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEstadoCivilDependenteError")));

                if (dependente.PessoaFisica == null || dependente.GrauDeParentesco == 0)
                    validationErrors.Add(
                        new ValidationResult(L("PropostaDeContratacao.EmptyGrauParentescoDependenteError")));

                var dateNascidoVivo = new DateTime(2010, 01, 01);
                if (dependente.PessoaFisica != null
                    && dependente.PessoaFisica.DataDeNascimento.HasValue
                    && dependente.PessoaFisica.DataDeNascimento.Value >= dateNascidoVivo
                    && string.IsNullOrEmpty(dependente.DeclaracaoDeNascidoVivo))
                {
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.ObrigaNascidoVivo")));
                }
            }
        }

        public async Task<PropostaDeContratacaoInput> SavePassoPreCadastro(PropostaDeContratacaoPassoPreCadastroInput input)
        {                       

            try
            {               

                PropostaDeContratacao proposta = (input.Id > 0) ? await _propostaService.GetById(input.Id) : new PropostaDeContratacao();
                this.SetPassoAceite(proposta);
                proposta = await ProcessInputPassoPreCadastro(proposta, input, false, 0);
                proposta.TenantId = AbpSession.TenantId;
                proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
                var entity = await _propostaService.SavePassoPreCadastro(proposta);
                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);

            }
            catch (AbpValidationException ex)
            {
                throw new AbpValidationException(ex.Message, ex.ValidationErrors);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message, ex.InnerException);
            }
            
        }



        [AbpAllowAnonymous]
        public async Task<PropostaDeContratacaoInput> SavePassoPreCadastroSimulador(PropostaDeContratacaoPassoPreCadastroInput input, int IdUser, int IdCorretora, int IdPlano)
        {
            try
            {
                PropostaDeContratacao proposta = (input.Id > 0) ? await _propostaService.GetById(input.Id) : new PropostaDeContratacao();
                this.SetPassoAceite(proposta);
                proposta = await ProcessInputPassoPreCadastroSimulador(proposta, input, true, IdUser, IdCorretora, IdPlano);
                proposta.TenantId = 1;
                proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
                var entity = await _propostaService.SavePassoPreCadastro(proposta);
                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);

            }
            catch (AbpValidationException ex)
            {
                throw new AbpValidationException(ex.Message, ex.ValidationErrors);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message, ex.InnerException);
            }
        }



        public async Task<UserDto> CreateUserBeneficiario(PropostaDeContratacaoPassoPreCadastroInput input)
        {
            PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
            Empresa empresa = await _empresaService.GetEmpresaDefault();
            var user = _beneficiarioBaseService.CreateUserBeneficiario(proposta.Titular, empresa, TipoDeUsuarioEnum.Beneficiario, StaticRoleNames.Tenants.Beneficiario);
            return user.MapTo<UserDto>();
        }

        public async Task SendEmailUserBeneficiario(UserDto input)
        {
            try
            {


                var user = await _userManager.GetUserByIdAsync(input.Id);
                Corretora corretora = await _corretoraService.GetCorretoraBySlug(user.Empresas.First().Slug);

                if (user != null)
                    _beneficiarioBaseService.SendEmail(user, user.Empresas.First().Slug, corretora);


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion

        #region Passo selecionar plano

        private async Task<PropostaDeContratacao> ProcessInputPassoSelecionarPlano(PropostaDeContratacao proposta, PropostaDeContratacaoPassoSelecionarPlanoInput input)
        {
            if (input.ProdutoDePlanoDeSaudeId > 0)
            {
                var produto = await _produtoService.GetById(input.ProdutoDePlanoDeSaudeId);
                if (produto != null)
                {
                    proposta.Produto = produto;
                    if (!proposta.Titular.Administradoras.Any(x => x.Id == produto.AdministradoraId))
                    {
                        var administradora = await _administradoraService.GetById(produto.AdministradoraId.Value);
                        proposta.Titular.Administradoras.Add(administradora);
                    }
                    if (proposta.Titular.Dependentes.Any())
                    {
                        foreach (var dependente in proposta.Titular.Dependentes)
                        {
                            if (!dependente.Administradoras.Any(x => x.Id == produto.AdministradoraId))
                            {
                                var administradora = await _administradoraService.GetById(produto.AdministradoraId.Value);
                                dependente.Administradoras.Add(administradora);
                            }
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("ProdutoDePlanoDeSaude.NotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyProdutoDePlanoDeSaudeError"));
            }

            if (input.CorretoraId > 0)
            {
                var corretora = await _corretoraService.GetById(Convert.ToInt32(proposta.CorretoraId));
                if (corretora != null)
                {
                    proposta.Corretora = corretora;

                    if (!proposta.Titular.Corretoras.Any(x => x.Id == corretora.Id))
                    {
                        proposta.Titular.Corretoras.Add(corretora);
                    }
                    if (proposta.Titular.Dependentes.Any())
                    {
                        foreach (var dependente in proposta.Titular.Dependentes)
                        {
                            if (!dependente.Corretoras.Any(x => x.Id == corretora.Id))
                            {
                                dependente.Corretoras.Add(corretora);
                            }
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("Corretora.NotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyProdutoDePlanoDeSaudeError"));
            }

            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.SelecionarPlano)
                proposta.PassoDaProposta = PassoDaPropostaEnum.DadosProponenteTitular;

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;

            return proposta;
        }

        [AbpAllowAnonymous]
        public async Task<PropostaDeContratacaoInput> SavePassoSelecionarPlano(PropostaDeContratacaoPassoSelecionarPlanoInput input)
        {
            try
            {
                if (input.Id > 0)
                {
                    PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);

                    this.SetPassoAceite(proposta);
                    proposta = await ProcessInputPassoSelecionarPlano(proposta, input);
                    proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
                    var entity = await _propostaService.SavePassoSelecionarPlano(proposta);
                    //return entity.MapTo<PropostaDeContratacaoInput>();

                    if (entity.Produto.Chancelas != null || entity.Produto.Chancelas.Count > 0)
                        foreach (var item in entity.Produto.Chancelas)
                        {
                            entity.Chancela = item;
                            entity.ChancelaId = item.Id;
                        }

                    MapManual map = new MapManual();
                    return map.MapManualProposta(entity);
                }

                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }


        [AbpAllowAnonymous]
        public async Task<PropostaDeContratacaoInput> SavePassoSelecionarPlanoSimulador(PropostaDeContratacaoPassoSelecionarPlanoInput input)
        {
            try
            {
                if (input.Id > 0)
                {
                    PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);

                    proposta.Titular.Administradoras = await _administradoraService.GetAllListAsync();
                    proposta.Titular.Corretoras = await _corretoraService.GetAllListAsync();


                    this.SetPassoAceite(proposta);
                    proposta = await ProcessInputPassoSelecionarPlano(proposta, input);
                    proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
                    var entity = await _propostaService.SavePassoSelecionarPlano(proposta);
                    //return entity.MapTo<PropostaDeContratacaoInput>();
                    MapManual map = new MapManual();
                    return map.MapManualProposta(entity);
                }

                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }


        #endregion

        #region Passo dados gerais

        private async Task<PropostaDeContratacao> ProcessInputPassoDadosGerais(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            //var empresa = await GetEmpresa(8);
            //var empresaUser = await _empresaService.GetEmpresaDefault();

            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);


            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();
                var dateNascidoVivo = new DateTime(2010, 01, 01);
                //user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);

                if (!input.VigenciaId.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyVigencia")));

                if (!input.AceiteDaDeclaracaoDeSaude)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyAceiteDaDeclaracao")));

                if (input.DataDeNascimentoProponenteTitular >= dateNascidoVivo && string.IsNullOrEmpty(input.DeclaracaoDeNascidoVivoProponenteTitular))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.ObrigaNascidoVivo")));

                if (user.TipoDeUsuario == TipoDeUsuarioEnum.Beneficiario &&
                    proposta.Produto.DeclaracaoDoBeneficiarioId.HasValue &&
                    !input.ItemDeDeclaracaoDoBeneficiarioId.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyItemDeDeclaracaoDoBeneficiario")));

                if (input.Endereco != null)
                {
                    if (string.IsNullOrEmpty(input.Endereco.CEP))
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCepError")));
                    }

                    if (input.Endereco.TipoDeLogradouroId == 0)
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyTipoDeLogradouroError")));
                    }

                    if (string.IsNullOrEmpty(input.Endereco.Logradouro))
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyLogradouroError")));
                    }

                    if (string.IsNullOrEmpty(input.Endereco.Numero))
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNumeroError")));
                    }

                    if (input.Endereco.CidadeId == 0)
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCidadeError")));
                    }
                }
                else
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEnderecoError")));

                if (input.Dependentes != null && input.Dependentes.Any())
                {
                    ValidationDependente(validationErrors, input.Dependentes);
                }


                if (!proposta.TitularResponsavelLegal)
                {
                    if (!input.TipoResponsavel.HasValue)
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyTipoResponsavelError")));

                    if (string.IsNullOrEmpty(input.NomeResponsavel))
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNomeResponsavelError")));

                    if (string.IsNullOrEmpty(input.CpfReponsavel))
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCPFResponsavelError")));

                    if (input.DataDeNascimentoReponsavel == DateTime.Now.AddYears(-100))
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyDataNascimentoResponsavelError")));

                    if (string.IsNullOrEmpty(input.NacionalidadeResponsavel))
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNacionalidadeResponsavelError")));

                    if (!input.SexoResponsavel.HasValue)
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptySexoResponsavelError")));

                    if (!input.EstadoCivilResponsavel.HasValue)
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEstadoCivilResponsavelError")));

                    if (string.IsNullOrEmpty(input.Email))
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEmailResponsavelError")));
                }

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            #region Dados do Proponente Titular

            proposta.Titular.PessoaFisica.Nome = input.NomeProponenteTitular;
            proposta.Titular.PessoaFisica.DataDeNascimento = input.DataDeNascimentoProponenteTitular;
            proposta.Titular.PessoaFisica.Nacionalidade = input.NacionalidadeProponenteTitular;
            proposta.Titular.NomeDaMae = input.NomeDaMaeProponenteTitular;
            proposta.Titular.NumeroDoCartaoNacionalDeSaude = input.NumeroDoCartaoNacionalDeSaudeProponenteTitular;
            proposta.Titular.DeclaracaoDeNascidoVivo = input.DeclaracaoDeNascidoVivoProponenteTitular;
            proposta.Titular.PessoaFisica.Sexo = input.SexoProponenteTitular;
            proposta.Titular.PessoaFisica.EstadoCivil = input.EstadoCivilProponenteTitular;

            #region RG  
            Documento documento = null;
            if (input.RgIdProponenteTitular > 0)
            {
                try
                {
                    documento = await _documentoService.GetById(input.RgIdProponenteTitular);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                }

                proposta.Titular.PessoaFisica.Documentos.First(x => x.Id == input.RgIdProponenteTitular).Numero = input.RgProponenteTitular;
            }
            else
            {
                TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Rg);
                documento = new Documento();
                documento.Numero = string.IsNullOrEmpty(input.RgProponenteTitular) ? string.Empty : input.RgProponenteTitular;
                documento.OrgaoExpedidor = input.OrgaoExpedidorProponenteTitular;
                documento.AssociarTipoDocumento(tipo, _documentoService);
                documento.Empresa = empresa;
                documento.EmpresaId = empresa.Id;

                if (proposta.Titular.PessoaFisica.Documentos == null) proposta.Titular.PessoaFisica.Documentos = new List<Documento>();

                proposta.Titular.PessoaFisica.Documentos.Add(documento);
            }
            #endregion

            #region Email
            EnderecoEletronico email = null;
            if (input.EmailIdProponenteTitular > 0)
            {
                try
                {
                    email = await _enderecoEletronicoService.GetById(input.EmailIdProponenteTitular);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                }

                email.Endereco = input.EmailProponenteTitular;
                proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
            }
            else
            {
                email = new EnderecoEletronico();
                email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                email.Endereco = input.EmailProponenteTitular;
                email.Empresa = empresa;
                email.EmpresaId = empresa.Id; // Garante pegar a empresa certa

                proposta.Titular.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
            }
            #endregion

            #region Endereço

            if (PessoaHelpers.GetIdade(proposta.Titular.PessoaFisica.DataDeNascimento.Value) >= 18)
            {
                var endereco = input.Endereco.MapTo<Endereco>();
                endereco.EmpresaId = empresa.Id;

                if (input.Endereco.CidadeId > 0)
                {
                    var cidade = await _cidadeService.GetById(input.Endereco.CidadeId);
                    endereco.CidadeId = input.Endereco.CidadeId;
                    endereco.Cidade = cidade;
                }

                endereco.PessoaId = proposta.Titular.Pessoa.Id;
                endereco.Pessoa = proposta.Titular.Pessoa;

                if (input.Endereco.TipoDeLogradouroId > 0)
                {
                    var tipoDeLogradouro = await _tipoDeLogradouroService.GetById(input.Endereco.TipoDeLogradouroId);
                    endereco.TipoDeLogradouroId = tipoDeLogradouro.Id;
                    endereco.AssociarTipoDeLogradouro(tipoDeLogradouro, _enderecoService);
                }

                var enderecoExistente = await _enderecoService
                    .GetAll()
                    .Where(x => x.PessoaId == proposta.Titular.Pessoa.Id && x.TipoDeLogradouroId == endereco.TipoDeLogradouroId && x.Logradouro == endereco.Logradouro && x.Numero == endereco.Numero && x.Complemento == endereco.Complemento && x.Bairro == endereco.Bairro && x.CidadeId == endereco.CidadeId)
                    .FirstOrDefaultAsync();

                if (enderecoExistente == null)
                {
                    endereco.TipoEndereco = TipoEnderecoEnum.Residencial;
                    proposta.Titular.PessoaFisica.Enderecos.Add(endereco);

                    proposta.Titular.Pessoa.EnderecoPrincipal = endereco;
                }
                else
                {
                    proposta.Titular.Pessoa.EnderecoPrincipal = enderecoExistente;
                }
            }

            #endregion

            #endregion

            #region Dados Responsável Legal

            if (input.TipoResponsavel.HasValue
                && !string.IsNullOrEmpty(input.NomeResponsavel)
                && input.DataDeNascimentoReponsavel != null
                && !string.IsNullOrEmpty(input.NacionalidadeResponsavel)
                && input.SexoResponsavel.HasValue
                && input.EstadoCivilResponsavel.HasValue
                && !string.IsNullOrEmpty(input.CpfReponsavel)
            )
            {
                if (proposta.Responsavel == null) proposta.Responsavel = new Responsavel();

                var pessoaFisica = await _pessoaFisicaService.GetPessoaByCpf(input.CpfReponsavel);

                if (pessoaFisica != null)
                    proposta.Responsavel.PessoaFisica = pessoaFisica;
                else
                {
                    proposta.Responsavel.PessoaFisica = new PessoaFisica();
                    proposta.Responsavel.TipoDeResponsavel = input.TipoResponsavel.Value;
                    proposta.Responsavel.PessoaFisica.Nome = input.NomeResponsavel;
                    proposta.Responsavel.PessoaFisica.DataDeNascimento = input.DataDeNascimentoReponsavel;
                    proposta.Responsavel.PessoaFisica.Nacionalidade = input.NacionalidadeResponsavel;
                    proposta.Responsavel.PessoaFisica.Sexo = input.SexoResponsavel;
                    proposta.Responsavel.PessoaFisica.EstadoCivil = input.EstadoCivilResponsavel;

                    var pessoaFisicaResponsavel =
                        await _pessoaFisicaService.CreateOrUpdateAndReturnSavedEntity(proposta.Responsavel.PessoaFisica);

                    #region Cpf Responsável  

                    Documento documentoResponsavel = null;
                    if (input.CpfIdReponsavel > 0)
                    {
                        try
                        {
                            documentoResponsavel = await _documentoService.GetById(input.CpfIdReponsavel);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                        }

                        proposta.Responsavel.PessoaFisica.Documentos.First(x => x.Id == input.CpfIdReponsavel).Numero =
                            input.CpfReponsavel;
                    }
                    else
                    {
                        TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                        documentoResponsavel = new Documento();
                        documentoResponsavel.Numero = string.IsNullOrEmpty(input.CpfReponsavel)
                            ? string.Empty
                            : input.CpfReponsavel;
                        documentoResponsavel.AssociarTipoDocumento(tipo, _documentoService);
                        documentoResponsavel.Empresa = empresa;
                        documentoResponsavel.EmpresaId = empresa.Id;

                        if (proposta.Responsavel.PessoaFisica.Documentos == null)
                            proposta.Responsavel.PessoaFisica.Documentos = new List<Documento>();

                        proposta.Responsavel.PessoaFisica.Documentos.Add(documentoResponsavel);
                    }

                    #endregion

                    #region Celular

                    Telefone telefone = null;
                    if (input.CelularId > 0)
                    {
                        try
                        {
                            telefone = await _telefoneService.GetById(input.CelularId);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.CelularNotFoundError"));
                        }

                        _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                    }
                    else
                    {
                        telefone = new Telefone();
                        telefone.Tipo = TipoTelefoneEnum.Celular;
                        _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                        telefone.Empresa = empresa; // Garante pegar a empresa certa
                        telefone.EmpresaId = empresa.Id; // Garante pegar a empresa certa

                        proposta.Responsavel.PessoaFisica.Telefones = new List<Telefone>();
                        proposta.Responsavel.PessoaFisica.Telefones.Add(telefone);
                    }

                    #endregion

                    #region Email

                    EnderecoEletronico emailResponsavel = null;
                    if (input.EmailId > 0)
                    {
                        try
                        {
                            emailResponsavel = await _enderecoEletronicoService.GetById(input.EmailId);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                        }
                        emailResponsavel.Endereco = input.Email;
                        await _enderecoEletronicoService.UpdateAndReturnEntity(emailResponsavel);
                    }
                    else
                    {
                        emailResponsavel = new EnderecoEletronico();
                        emailResponsavel.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                        emailResponsavel.Endereco = input.Email;
                        emailResponsavel.Empresa = empresa;
                        emailResponsavel.EmpresaId = empresa.Id;

                        proposta.Responsavel.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                        proposta.Responsavel.PessoaFisica.EnderecosEletronicos.Add(emailResponsavel);
                    }

                    #endregion
                }

                #region Endereço

                var endereco = input.Endereco.MapTo<Endereco>();
                endereco.EmpresaId = empresa.Id;

                if (input.Endereco.CidadeId > 0)
                {
                    var cidade = await _cidadeService.GetById(input.Endereco.CidadeId);
                    endereco.CidadeId = input.Endereco.CidadeId;
                    endereco.Cidade = cidade;
                }

                endereco.PessoaId = proposta.Responsavel.Pessoa.Id;
                endereco.Pessoa = proposta.Responsavel.Pessoa;

                if (input.Endereco.TipoDeLogradouroId > 0)
                {
                    var tipoDeLogradouro = await _tipoDeLogradouroService.GetById(input.Endereco.TipoDeLogradouroId);
                    endereco.TipoDeLogradouroId = tipoDeLogradouro.Id;
                    endereco.AssociarTipoDeLogradouro(tipoDeLogradouro, _enderecoService);
                }

                var enderecoExistente = await _enderecoService
                    .GetAll()
                    .Where(
                        x =>
                            x.PessoaId == proposta.Responsavel.Pessoa.Id &&
                            x.TipoDeLogradouroId == endereco.TipoDeLogradouroId && x.Logradouro == endereco.Logradouro &&
                            x.Numero == endereco.Numero && x.Complemento == endereco.Complemento &&
                            x.Bairro == endereco.Bairro && x.CidadeId == endereco.CidadeId)
                    .FirstOrDefaultAsync();
                if (enderecoExistente == null)
                {

                    if (proposta.Responsavel.PessoaFisica.Enderecos == null)
                        proposta.Responsavel.PessoaFisica.Enderecos = new List<Endereco>();

                    endereco.TipoEndereco = TipoEnderecoEnum.Residencial;
                    proposta.Responsavel.PessoaFisica.Enderecos.Add(endereco);

                    proposta.Responsavel.Pessoa.EnderecoPrincipal = endereco;
                }
                else
                {
                    proposta.Responsavel.Pessoa.EnderecoPrincipal = enderecoExistente;
                }

                #endregion

                #region User

                if (_dependenteParticipaComoUsuario)
                {
                    user = _responsavelService.CreateUserResponsavel(proposta.Responsavel, empresa,
                        TipoDeUsuarioEnum.Beneficiario, StaticRoleNames.Tenants.Beneficiario);
                    proposta.UsuarioResponsavel = user;
                    proposta.UsuarioResponsavelId = user.Id;
                }

                #endregion
            }

            proposta.Titular.Responsavel = proposta.Responsavel;

            #endregion


            #region Vigencia

            if (input.VigenciaId.HasValue)
            {
                var vigencia = await _vigenciaService.GetById(input.VigenciaId.Value);
                if (vigencia != null)
                {
                    proposta.InicioDeVigencia = input.InicioDeVigencia;
                    proposta.Vigencia = vigencia;
                    proposta.VigenciaId = vigencia.Id;
                }
                else
                    throw new UserFriendlyException(L("Vigencia.NotFoundError"));
            }
            #endregion

            #region Item da Declaração do Beneficiário

            if (input.ItemDeDeclaracaoDoBeneficiarioId.HasValue)
            {
                var declaracao = await _itemDeDeclaracaoDoBeneficiarioService.GetById(input.ItemDeDeclaracaoDoBeneficiarioId.Value);
                if (declaracao != null)
                    proposta.ItemDeDeclaracaoDoBeneficiarioId = declaracao.Id;
            }
            #endregion

            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.PreenchimentoDosDados)
                proposta.PassoDaProposta = PassoDaPropostaEnum.AceiteDoContrato;

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            proposta.AceiteDaDeclaracaoDeSaude = input.AceiteDaDeclaracaoDeSaude;

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDadosProponenteTitular(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {


            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);
            //var empresaUser = await _empresaService.GetEmpresaDefault();

            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();
                var dateNascidoVivo = new DateTime(2010, 01, 01);

                if (input.DataDeNascimentoProponenteTitular >= dateNascidoVivo && string.IsNullOrEmpty(input.DeclaracaoDeNascidoVivoProponenteTitular))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.ObrigaNascidoVivo")));

                if (input.SexoProponenteTitular == SexoEnum.NaoInformado)
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptySexoError")));

                if (string.IsNullOrEmpty(input.NomeDaMaeProponenteTitular))
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyNomeDaMaeError")));

                //if (string.IsNullOrEmpty(input.RgProponenteTitular))
                //    validationErrors.Add(new ValidationResult(L("Proponente.EmptyRgError")));

                //if (string.IsNullOrEmpty(input.OrgaoExpedidorProponenteTitular))
                //    validationErrors.Add(new ValidationResult(L("Proponente.EmptyOrgaoExpeditorError")));

                if (input.EstadoCivilProponenteTitular == EstadoCivilEnum.NaoInformado)
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyEstadoCivilError")));

                if (string.IsNullOrEmpty(input.NacionalidadeProponenteTitular))
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyNacionalidadeError")));

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            #region Dados do Proponente Titular

            proposta.Titular.PessoaFisica.Nome = input.NomeProponenteTitular;
            proposta.Titular.PessoaFisica.DataDeNascimento = input.DataDeNascimentoProponenteTitular;
            proposta.Titular.PessoaFisica.Nacionalidade = input.NacionalidadeProponenteTitular;
            proposta.Titular.NomeDaMae = input.NomeDaMaeProponenteTitular;
            proposta.Titular.NumeroDoCartaoNacionalDeSaude = input.NumeroDoCartaoNacionalDeSaudeProponenteTitular;
            proposta.Titular.DeclaracaoDeNascidoVivo = input.DeclaracaoDeNascidoVivoProponenteTitular;
            proposta.Titular.PessoaFisica.Sexo = input.SexoProponenteTitular;
            proposta.Titular.PessoaFisica.EstadoCivil = input.EstadoCivilProponenteTitular;
            proposta.Titular.matricula = input.matricula;

            if (input.boleto == "Boleto")
            {
                proposta.Titular.boleto = true;
                proposta.Titular.folha = false;
            }
            else
            {
                proposta.Titular.boleto = false;
                proposta.Titular.folha = true;
            }

            if (input.DebitoConta == "sim")
                proposta.Titular.DebitoConta = true;
            else
                proposta.Titular.DebitoConta = false;


            if (input.FolhaFicha == "sim")
                proposta.Titular.FolhaFicha = true;
            else
                proposta.Titular.FolhaFicha = false;


            #region RG  
            Documento documento = null;
            if (input.RgIdProponenteTitular > 0)
            {
                try
                {
                    documento = await _documentoService.GetById(input.RgIdProponenteTitular);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                }

                proposta.Titular.PessoaFisica.Documentos.First(x => x.Id == input.RgIdProponenteTitular).Numero = input.RgProponenteTitular;
                proposta.Titular.PessoaFisica.Documentos.First(x => x.Id == input.RgIdProponenteTitular).OrgaoExpedidor = input.OrgaoExpedidorProponenteTitular;


            }
            else
            {
                TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Rg);
                documento = new Documento();
                documento.Numero = string.IsNullOrEmpty(input.RgProponenteTitular) ? string.Empty : input.RgProponenteTitular;
                documento.OrgaoExpedidor = input.OrgaoExpedidorProponenteTitular;
                documento.AssociarTipoDocumento(tipo, _documentoService);
                documento.Empresa = empresa;
                documento.EmpresaId = empresa.Id;

                if (proposta.Titular.PessoaFisica.Documentos == null) proposta.Titular.PessoaFisica.Documentos = new List<Documento>();

                proposta.Titular.PessoaFisica.Documentos.Add(documento);
            }
            #endregion

            #region Email
            EnderecoEletronico email = null;
            if (input.EmailIdProponenteTitular > 0)
            {
                try
                {
                    email = await _enderecoEletronicoService.GetById(input.EmailIdProponenteTitular);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                }

                email.Endereco = input.EmailProponenteTitular;
                proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
            }
            else
            {
                email = new EnderecoEletronico();
                email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                email.Endereco = input.EmailProponenteTitular;
                email.Empresa = empresa;
                email.EmpresaId = empresa.Id; // Garante pegar a empresa certa

                proposta.Titular.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                proposta.Titular.PessoaFisica.EnderecosEletronicos.Add(email);
            }
            #endregion

            #endregion

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            if (input.TitularResponsavelLegal)
            {
                proposta.PassoDaProposta = PassoDaPropostaEnum.Endereco;
            }
            else
            {
                proposta.PassoDaProposta = PassoDaPropostaEnum.DadosResponsavel;
            }

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDadosResponsavel(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);
            var empresaUser = empresa;
            //var empresa = await GetEmpresa(8);
            // var empresaUser = await _empresaService.GetEmpresaDefault();

            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();

                if (!input.TipoResponsavel.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyTipoResponsavelError")));

                if (string.IsNullOrEmpty(input.NomeResponsavel))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNomeResponsavelError")));

                if (string.IsNullOrEmpty(input.CpfReponsavel))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCPFResponsavelError")));

                if (input.DataDeNascimentoReponsavel == DateTime.Now.AddYears(-100))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyDataNascimentoResponsavelError")));

                if (string.IsNullOrEmpty(input.NacionalidadeResponsavel))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNacionalidadeResponsavelError")));

                if (!input.SexoResponsavel.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptySexoResponsavelError")));

                if (!input.EstadoCivilResponsavel.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEstadoCivilResponsavelError")));

                if (string.IsNullOrEmpty(input.Email))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEmailResponsavelError")));

                if (input.CpfProponenteTitular == input.CpfReponsavel)
                    validationErrors.Add(new ValidationResult("Numero de CPF do Responsável legal não pode ser igual ao do Titular"));

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            #region Dados Responsável Legal

            if (input.TipoResponsavel.HasValue
                && !string.IsNullOrEmpty(input.NomeResponsavel)
                && input.DataDeNascimentoReponsavel != null
                && !string.IsNullOrEmpty(input.NacionalidadeResponsavel)
                && input.SexoResponsavel.HasValue
                && input.EstadoCivilResponsavel.HasValue
                && !string.IsNullOrEmpty(input.CpfReponsavel)
            )
            {
                if (proposta.Responsavel == null) proposta.Responsavel = new Responsavel();

                var pessoaFisica = await _pessoaFisicaService.GetPessoaByCpf(input.CpfReponsavel);

                if (pessoaFisica != null)
                    proposta.Responsavel.PessoaFisica = pessoaFisica;
                else
                {
                    proposta.Responsavel.PessoaFisica = new PessoaFisica();
                    proposta.Responsavel.TipoDeResponsavel = input.TipoResponsavel.Value;
                    proposta.Responsavel.PessoaFisica.Nome = input.NomeResponsavel;
                    proposta.Responsavel.PessoaFisica.DataDeNascimento = input.DataDeNascimentoReponsavel;
                    proposta.Responsavel.PessoaFisica.Nacionalidade = input.NacionalidadeResponsavel;
                    proposta.Responsavel.PessoaFisica.Sexo = input.SexoResponsavel;
                    proposta.Responsavel.PessoaFisica.EstadoCivil = input.EstadoCivilResponsavel;


                    var pessoaFisicaResponsavel =
                        await _pessoaFisicaService.CreateOrUpdateAndReturnSavedEntity(proposta.Responsavel.PessoaFisica);

                    #region Cpf Responsável  

                    Documento documentoResponsavel = null;
                    if (input.CpfIdReponsavel > 0)
                    {
                        try
                        {
                            documentoResponsavel = await _documentoService.GetById(input.CpfIdReponsavel);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.CpfNotFoundError"));
                        }

                        proposta.Responsavel.PessoaFisica.Documentos.First(x => x.Id == input.CpfIdReponsavel).Numero =
                            input.CpfReponsavel;
                    }
                    else
                    {
                        TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                        documentoResponsavel = new Documento();
                        documentoResponsavel.Numero = string.IsNullOrEmpty(input.CpfReponsavel)
                            ? string.Empty
                            : input.CpfReponsavel;
                        documentoResponsavel.AssociarTipoDocumento(tipo, _documentoService);
                        documentoResponsavel.Empresa = empresa;
                        documentoResponsavel.EmpresaId = empresa.Id;

                        if (proposta.Responsavel.PessoaFisica.Documentos == null)
                            proposta.Responsavel.PessoaFisica.Documentos = new List<Documento>();

                        proposta.Responsavel.PessoaFisica.Documentos.Add(documentoResponsavel);
                    }

                    #endregion

                    #region Celular

                    Telefone telefone = null;
                    if (input.CelularId > 0)
                    {
                        try
                        {
                            telefone = await _telefoneService.GetById(input.CelularId);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.CelularNotFoundError"));
                        }

                        _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                    }
                    else
                    {
                        telefone = new Telefone();
                        telefone.Tipo = TipoTelefoneEnum.Celular;
                        _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                        telefone.Empresa = empresa; // Garante pegar a empresa certa
                        telefone.EmpresaId = empresa.Id; // Garante pegar a empresa certa

                        proposta.Responsavel.PessoaFisica.Telefones = new List<Telefone>();
                        proposta.Responsavel.PessoaFisica.Telefones.Add(telefone);
                    }

                    #endregion

                    #region Email

                    EnderecoEletronico emailResponsavel = null;
                    if (input.EmailId > 0)
                    {
                        try
                        {
                            emailResponsavel = await _enderecoEletronicoService.GetById(input.EmailId);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmailNotFoundError"));
                        }
                        emailResponsavel.Endereco = input.Email;
                        await _enderecoEletronicoService.UpdateAndReturnEntity(emailResponsavel);
                    }
                    else
                    {
                        emailResponsavel = new EnderecoEletronico();
                        emailResponsavel.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                        emailResponsavel.Endereco = input.Email;
                        emailResponsavel.Empresa = empresa;
                        emailResponsavel.EmpresaId = empresa.Id;

                        proposta.Responsavel.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                        proposta.Responsavel.PessoaFisica.EnderecosEletronicos.Add(emailResponsavel);
                    }

                    #endregion
                }

                #region User

                user = _responsavelService.CreateUserResponsavel(proposta.Responsavel, empresaUser,
                    TipoDeUsuarioEnum.Beneficiario, StaticRoleNames.Tenants.Beneficiario);
                proposta.UsuarioResponsavel = user;
                proposta.UsuarioResponsavelId = user.Id;

                #endregion
            }


            proposta.Titular.Responsavel = proposta.Responsavel;
            proposta.Titular.Responsavel.TipoDeResponsavel = (TipoDeResponsavelEnum)input.TipoResponsavel;
            proposta.Titular.Responsavel.Pessoa.NomePessoa = input.NomeResponsavel;
            proposta.Responsavel.PessoaFisica.DataDeNascimento = input.DataDeNascimentoReponsavel;
            proposta.Responsavel.PessoaFisica.Nacionalidade = input.NacionalidadeResponsavel;
            proposta.Responsavel.PessoaFisica.Sexo = input.SexoResponsavel;
            proposta.Responsavel.PessoaFisica.EstadoCivil = input.EstadoCivilResponsavel;


            #endregion

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            proposta.PassoDaProposta = PassoDaPropostaEnum.Endereco;



            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDadosEndereco(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            //var empresa = await GetEmpresa(8);
            //var empresaUser = await _empresaService.GetEmpresaDefault();
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);

            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();

                if (input.Endereco != null)
                {
                    if (String.IsNullOrEmpty(input.Endereco.CEP))
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCepError")));
                    }

                    if (input.Endereco.TipoDeLogradouroId == 0)
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyTipoDeLogradouroError")));
                    }

                    if (String.IsNullOrEmpty(input.Endereco.Logradouro))
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyLogradouroError")));
                    }

                    if (String.IsNullOrEmpty(input.Endereco.Numero))
                    {
                        //validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyNumeroError")));
                        input.Endereco.Numero = "S/Nº";
                    }

                    if (input.Endereco.CidadeId == 0)
                    {
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyCidadeError")));
                    }
                }
                else
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyEnderecoError")));

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            #region Endereço

            var endereco = input.Endereco.MapTo<Endereco>();
            endereco.EmpresaId = empresa.Id;

            if (input.Endereco.CidadeId > 0)
            {
                var cidade = await _cidadeService.GetById(input.Endereco.CidadeId);
                endereco.CidadeId = input.Endereco.CidadeId;
                endereco.Cidade = cidade;
            }

            endereco.PessoaId = proposta.Titular.Pessoa.Id;
            endereco.Pessoa = proposta.Titular.Pessoa;

            if (input.Endereco.TipoDeLogradouroId > 0)
            {
                var tipoDeLogradouro = await _tipoDeLogradouroService.GetById(input.Endereco.TipoDeLogradouroId);
                endereco.TipoDeLogradouroId = tipoDeLogradouro.Id;
                endereco.AssociarTipoDeLogradouro(tipoDeLogradouro, _enderecoService);
            }

            var enderecoExistente = await _enderecoService
                .GetAll()
                .Where(x => x.PessoaId == proposta.Titular.Pessoa.Id && x.TipoDeLogradouroId == endereco.TipoDeLogradouroId && x.Logradouro == endereco.Logradouro && x.Numero == endereco.Numero && x.Complemento == endereco.Complemento && x.Bairro == endereco.Bairro && x.CidadeId == endereco.CidadeId)
                .FirstOrDefaultAsync()
                .ConfigureAwait(false);

            if (enderecoExistente == null)
            {
                endereco.TipoEndereco = TipoEnderecoEnum.Residencial;
                proposta.Titular.PessoaFisica.Enderecos.Add(endereco);

                proposta.Titular.Pessoa.EnderecoPrincipal = endereco;
            }

            else
            {
                proposta.Titular.Pessoa.EnderecoPrincipal = enderecoExistente;
            }

            #endregion

            if (proposta.ResponsavelId.HasValue && proposta.ResponsavelId > 0
            )
            {
                if (enderecoExistente == null)
                {
                    if (proposta.Responsavel.PessoaFisica.Enderecos.Any())
                    {
                        if (!proposta.Responsavel.PessoaFisica.Enderecos.Any(x => x.Id == endereco.Id))
                        {
                            endereco.TipoEndereco = TipoEnderecoEnum.Residencial;
                            proposta.Responsavel.PessoaFisica.Enderecos.Add(endereco);

                            proposta.Responsavel.Pessoa.EnderecoPrincipal = endereco;
                        }
                    }
                    else
                    {
                        // endereco.TipoEndereco = TipoEnderecoEnum.Residencial;
                        // proposta.Responsavel.PessoaFisica.Enderecos.Add(endereco);

                        //  proposta.Responsavel.Pessoa.EnderecoPrincipal = endereco;
                    }
                }
                else
                {
                    proposta.Responsavel.Pessoa.EnderecoPrincipal = enderecoExistente;
                }
            }

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            proposta.PassoDaProposta = PassoDaPropostaEnum.DadosDependentes;

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDadosDependentes(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);

            #region Validations
            var validationErrors = new List<ValidationResult>();

            if (input.Dependentes != null && input.Dependentes.Any())
            {
                ValidationDependente(validationErrors, input.Dependentes);
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
            #endregion

            #region Dependentes

            await PreencherDependentes(proposta, input.Dependentes);

            #endregion

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            proposta.PassoDaProposta = PassoDaPropostaEnum.Documentos;

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDadosVigencia(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            //var empresa = await GetEmpresa(8);
            //var empresaUser = await _empresaService.GetEmpresaDefault();
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);

            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();

                if (!input.VigenciaId.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyVigencia")));

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            #region Vigencia

            if (input.VigenciaId.HasValue)
            {
                var vigencia = await _vigenciaService.GetById(input.VigenciaId.Value);
                if (vigencia != null)
                {
                    proposta.InicioDeVigencia = input.InicioDeVigencia;
                    proposta.Vigencia = vigencia;
                    proposta.VigenciaId = vigencia.Id;
                }
                else
                    throw new UserFriendlyException(L("Vigencia.NotFoundError"));
            }
            #endregion

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            //user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            if (user.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
            {
                proposta.PassoDaProposta = PassoDaPropostaEnum.DadosGerenciais;
            }
            else
            {
                proposta.PassoDaProposta = PassoDaPropostaEnum.DadosDeclaracaoDeSaude;
            }

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDeclaracaoDeSaude(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);

            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();

                if (!input.VigenciaId.HasValue)
                {
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyVigencia")));
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }

                if (!input.AceiteDaDeclaracaoDeSaude)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyAceiteDaDeclaracao")));

                if (proposta.Produto.DeclaracaoDoBeneficiarioId.HasValue && !input.ItemDeDeclaracaoDoBeneficiarioId.HasValue)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyItemDeDeclaracaoDoBeneficiario")));

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            #region Item da Declaração do Beneficiário
            if (input.ItemDeDeclaracaoDoBeneficiarioId.HasValue)
            {
                var declaracao = await _itemDeDeclaracaoDoBeneficiarioService.GetById(input.ItemDeDeclaracaoDoBeneficiarioId.Value);
                if (declaracao != null)
                    proposta.ItemDeDeclaracaoDoBeneficiarioId = declaracao.Id;
            }
            #endregion

            proposta.PassoDaProposta = PassoDaPropostaEnum.AceiteDoContrato;

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            proposta.AceiteDaDeclaracaoDeSaude = input.AceiteDaDeclaracaoDeSaude;

            return proposta;
        }

        private async Task<PropostaDeContratacao> ProcessInputDadosGerenciais(PropostaDeContratacao proposta, PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            var empresa = await GetEmpresa(user.Empresas.First().Id);

            #region Validation

            if (input != null)
            {
                var validationErrors = new List<ValidationResult>();

                if (input.GerenteId == null || input.GerenteId == 0)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyGerenteError")));

                if (input.SupervisorId == null || input.SupervisorId == 0)
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptySupervisorError")));

                if (proposta.FormaDeContratacao == FormaDeContratacaoEnum.Adesao && (input.ChancelaId == null || input.ChancelaId == 0))
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyChancelaError")));

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion

            var gerente = await _gerenteCorretoraService.GetAll().Where(x => x.CorretoraId == proposta.CorretoraId && x.Id == input.GerenteId).FirstOrDefaultAsync();

            if (gerente != null)
            {
                proposta.GerenteId = gerente.Id;
            }
            else
            {
                throw new UserFriendlyException(L("Gerente.NotFoundError"));
            }

            var supervisor = await _supervisorCorretoraService.GetAll().Where(x => x.CorretoraId == proposta.CorretoraId && x.Id == input.SupervisorId).FirstOrDefaultAsync();

            if (supervisor != null)
            {
                proposta.SupervisorId = supervisor.Id;
            }
            else
            {
                throw new UserFriendlyException(L("Supervisor.NotFoundError"));
            }

            if (input.ChancelaId.HasValue)
            {
                var chancela = await _chancelaService.GetById(input.ChancelaId.Value);
                if (chancela != null)
                {
                    proposta.ChancelaId = chancela.Id;
                    proposta.Chancela = chancela;
                }
                else
                {
                    throw new UserFriendlyException(L("Chancela.NotFoundError"));
                }
            }

            proposta.Aditivos.Clear();
            if (input.Aditivos != null && input.Aditivos.Any())
            {
                using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
                {
                    foreach (var item in input.Aditivos)
                    {
                        var aditivo = await _relatorioService.GetById(item.Id);
                        proposta.Aditivos.Add(aditivo);
                    }
                }
            }

            return proposta;
        }

        public async Task<PropostaDeContratacaoInput> SavePassoDadosGerais(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                this.SetPassoAceite(proposta);
                proposta = await ProcessInputPassoDadosGerais(proposta, input);
                var entity = await _propostaService.SavePassoDadosGerais(proposta);

                return entity.MapTo<PropostaDeContratacaoInput>();
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        public async Task<PropostaDeContratacaoInput> SaveDadosProponenteTitular(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                this.SetPassoAceite(proposta);
                proposta = await ProcessInputDadosProponenteTitular(proposta, input);
                var entity = await _propostaService.SavePassoDadosGerais(proposta);
                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);
            }
            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        public async Task<PropostaDeContratacaoInput> SaveDadosResponsavel(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                this.SetPassoAceite(proposta);
                proposta = await ProcessInputDadosResponsavel(proposta, input);
                var entity = await _propostaService.SavePassoDadosGerais(proposta);
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        public async Task<PropostaDeContratacaoInput> SaveDadosEndereco(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            try
            {
                if (input.Id > 0)
                {
                    PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                    this.SetPassoAceite(proposta);
                    proposta = await ProcessInputDadosEndereco(proposta, input);
                    var entity = await _propostaService.SavePassoDadosGerais(proposta);
                    // return entity.MapTo<PropostaDeContratacaoInput>();
                    MapManual map = new MapManual();
                    return map.MapManualProposta(entity);
                }

                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
            }
            catch (AbpValidationException ex)
            {
                throw new AbpValidationException(ex.Message, ex.ValidationErrors);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message, ex.InnerException);
            }
        }

        public async Task<PropostaDeContratacaoInput> SaveDadosDependentes(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            try
            {
                if (input.Id > 0)
                {
                    PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                    this.SetPassoAceite(proposta);
                    proposta = await ProcessInputDadosDependentes(proposta, input);
                    var entity = await _propostaService.SavePassoDadosGerais(proposta);
                    // return entity.MapTo<PropostaDeContratacaoInput>();
                    MapManual map = new MapManual();
                    return map.MapManualProposta(entity);
                }
                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
            }
            catch (AbpValidationException ex)
            {
                throw new AbpValidationException(L("ValidationError"), ex.ValidationErrors);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message, ex.InnerException);
            }
        }

        public async Task<PropostaDeContratacaoInput> SaveDadosVigencia(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                proposta = await ProcessInputDadosVigencia(proposta, input);
                var entity = await _propostaService.SavePassoDadosGerais(proposta);
                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        public async Task<PropostaDeContratacaoInput> SaveDadosDeclaracaoDeSaude(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                proposta = await ProcessInputDeclaracaoDeSaude(proposta, input);
                var entity = await _propostaService.SavePassoDadosGerais(proposta);

                this.SetPassoAceite(proposta);

                #region Dependentes

                if (proposta.Titular != null)
                {
                    await PreencherDependentes(proposta, input.Dependentes);

                    if (input.ProponenteTitular.RespostasDeclaracaoDeSaude != null)
                    {
                        var semMarcacao =
                            input.ProponenteTitular.RespostasDeclaracaoDeSaude.Any(x => string.IsNullOrEmpty(x.Marcada) && x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico);

                        var peso = input.ProponenteTitular
                                            .RespostasDeclaracaoDeSaude.FirstOrDefault(x => x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso);

                        if (peso == null || string.IsNullOrEmpty(peso.Observacao))
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPeso"));

                        var altura = input.ProponenteTitular
                                            .RespostasDeclaracaoDeSaude.FirstOrDefault(x => x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura);

                        if (altura == null || string.IsNullOrEmpty(altura.Observacao))
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyAltura"));

                        if (semMarcacao)
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyResposta"));

                        var comMarcacaoSemObservacao =
                            input.ProponenteTitular.RespostasDeclaracaoDeSaude.Any(x => !string.IsNullOrEmpty(x.Marcada) && x.Marcada.ToUpper() == "S".ToUpper() && string.IsNullOrEmpty(x.Observacao));

                        if (comMarcacaoSemObservacao)
                            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyObservacao"));

                        foreach (var itemResposta in input.ProponenteTitular.RespostasDeclaracaoDeSaude)
                        {
                            var item = await _itemDeDeclaracaoDeSaudeService.GetById(itemResposta.PerguntaId);
                            var beneficiario = await _titularService.GetById(input.ProponenteTitular.Id);
                            var resposta = _respostaService.GetAll().Where(x => x.BeneficiarioId == beneficiario.Id && x.ItemDeDeclaracaoDeSaudeId == item.Id).FirstOrDefault();
                            if (resposta == null) resposta = new RespostaDoItemDeDeclaracaoDeSaude();
                            resposta.IsActive = true;
                            resposta.ItemDeDeclaracaoDeSaudeId = item.Id;
                            resposta.Observacao = itemResposta.Observacao;
                            resposta.PropostaDeContratacaoId = proposta.Id;
                            resposta.Marcada = (itemResposta.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura ||
                                                itemResposta.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso)
                                                ? "S"
                                                : itemResposta.Marcada;
                            resposta.BeneficiarioId = beneficiario.Id;
                            resposta.AnoDoEvento = itemResposta.AnoDoEvento;
                            resposta.TenantId = AbpSession.TenantId.Value;
                            await _respostaService.CreateOrUpdateEntity(resposta);
                        }
                    }
                }

                foreach (var dependente in input.Dependentes)
                {
                    var semMarcacaoDependente =
                          dependente.RespostasDeclaracaoDeSaude.Any(x => string.IsNullOrEmpty(x.Marcada) && x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico);

                    if (semMarcacaoDependente)
                        throw new UserFriendlyException(L("PropostaDeContratacao.EmptyRespostaDependente"));

                    var comMarcacaoSemObservacaoDependente =
                        dependente.RespostasDeclaracaoDeSaude.Any(x => !string.IsNullOrEmpty(x.Marcada) && x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico && x.Marcada.ToUpper() == "S".ToUpper() && string.IsNullOrEmpty(x.Observacao));

                    if (comMarcacaoSemObservacaoDependente)
                        throw new UserFriendlyException(L("PropostaDeContratacao.EmptyObservacaoDependente"));

                    var peso = dependente
                                    .RespostasDeclaracaoDeSaude.FirstOrDefault(x => x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso);

                    if (peso == null || string.IsNullOrEmpty(peso.Observacao))
                        throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPesoDependente"));

                    var altura = dependente
                                    .RespostasDeclaracaoDeSaude.FirstOrDefault(x => x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura);

                    if (altura == null || string.IsNullOrEmpty(altura.Observacao))
                        throw new UserFriendlyException(L("PropostaDeContratacao.EmptyAlturaDependente"));

                    if (dependente.Id == 0)
                    {
                        var dependenteEntity = proposta.Titular.Dependentes
                            .FirstOrDefault(x => x.PessoaFisica.Nome == dependente.PessoaFisica.Nome
                                                 && x.NomeDaMae == dependente.NomeDaMae);

                        if (dependenteEntity == null)
                            await _dependenteService.CreateEntity(dependenteEntity);

                        dependente.Id = dependenteEntity.Id;
                    }

                    foreach (var itemResposta in dependente.RespostasDeclaracaoDeSaude)
                    {
                        var item = await _itemDeDeclaracaoDeSaudeService.GetById(itemResposta.PerguntaId);

                        if (dependente.Id > 0)
                        {
                            var beneficiario = await _dependenteService.GetById(dependente.Id);
                            var resposta = _respostaService.GetAll().Where(x => x.BeneficiarioId == beneficiario.Id && x.ItemDeDeclaracaoDeSaudeId == item.Id).FirstOrDefault();
                            if (resposta == null) resposta = new RespostaDoItemDeDeclaracaoDeSaude();
                            resposta.IsActive = true;
                            resposta.ItemDeDeclaracaoDeSaude = item;
                            resposta.Observacao = itemResposta.Observacao;
                            resposta.Marcada = (itemResposta.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura ||
                                                itemResposta.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso)
                                                ? "S"
                                                : itemResposta.Marcada;
                            resposta.AnoDoEvento = itemResposta.AnoDoEvento;
                            resposta.PropostaDeContratacao = proposta;
                            resposta.Beneficiario = beneficiario;
                            await _respostaService.CreateOrUpdateEntity(resposta);
                        }
                    }
                }

                #endregion

                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        public async Task<PropostaDeContratacaoInput> SavePassoDadosGerenciais(PropostaDeContratacaoPassoDadosGeraisInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                this.SetPassoDeclaracaoSaude(proposta);
                await _propostaService.SendNotificationDeclaracaoSaude(proposta);
                proposta = await ProcessInputDadosGerenciais(proposta, input);
                var entity = await _propostaService.SavePassoDadosGerais(proposta);
                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        #endregion

        #region Passo envio de documentos

        private async Task<PropostaDeContratacao> ProcessInputPassoEnvioDeDocumentos(PropostaDeContratacao proposta, PropostaDeContratacaoPassoEnvioDeDocumentosInput input)
        {
            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.EnvioDeDocumentos)
                proposta.PassoDaProposta = PassoDaPropostaEnum.AceiteDoContrato;

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;

            return proposta;
        }

        public async Task<PropostaDeContratacaoInput> SavePassoEnvioDeDocumentos(PropostaDeContratacaoPassoEnvioDeDocumentosInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                proposta = await ProcessInputPassoEnvioDeDocumentos(proposta, input);
                var entity = await _propostaService.SavePassoEnvioDeDocumentos(proposta);
                //return entity.MapTo<PropostaDeContratacaoInput>();
                MapManual map = new MapManual();
                return map.MapManualProposta(entity);
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        #endregion

        #region Passo aceite do contrato

        private async Task<PropostaDeContratacao> ProcessInputPassoAceiteDoContrato(PropostaDeContratacao proposta, PropostaDeContratacaoPassoAceiteDoContratoInput input)
        {
            #region Validate

            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }

            #endregion

            if (input.ContratoId > 0)
            {
                var contrato = await _contratoService.GetById(input.ContratoId);
                if (contrato != null)
                {
                    var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);

                    proposta.ContratoVigenteNoAceite = contrato;
                    proposta.UltimoContratoAceito = contrato;

                    if (user.TipoDeUsuario == TipoDeUsuarioEnum.Beneficiario)
                    {
                        if (!proposta.Aceite && input.Aceite)
                        {
                            proposta.Aceite = true;
                            proposta.DataHoraDoAceite = DateTime.Now;
                        }
                    }

                    if (user.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
                    {
                        if (!proposta.AceiteCorretor && input.AceiteCorretor)
                        {
                            proposta.AceiteCorretor = true;
                            proposta.DataAceiteCorretor = DateTime.Now;
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException(L("Contrato.NotFoundError"));
                }
            }
            else
            {
                throw new UserFriendlyException(L("PropostaDeContratacao.EmptyContratoError"));
            }



            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.AceiteDoContrato)
            {
                if (proposta.Aceite && proposta.AceiteCorretor)
                    proposta.PassoDaProposta = PassoDaPropostaEnum.Homologacao;
            }

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;

            return proposta;
        }

        public async Task<PropostaDeContratacaoInput> SavePassoAceiteDoContrato(PropostaDeContratacaoPassoAceiteDoContratoInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                proposta = await ProcessInputPassoAceiteDoContrato(proposta, input);
                var entity = await _propostaService.SavePassoAceiteDoContrato(proposta);

                MapManual map = new MapManual();
                var retorno = map.MapManualProposta(entity);

                if (entity.Aceite && entity.AceiteCorretor && entity.PassoDaProposta == PassoDaPropostaEnum.Homologacao)
                {
                    await CreateSnapshot(retorno);
                }

                return retorno;
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        #endregion

        #region Passo passo homologação

        private async Task<PropostaDeContratacao> ProcessInputPassoHomologacao(PropostaDeContratacao proposta, PropostaDeContratacaoPassoHomologacaoInput input)
        {

            #region Validate

            var validationErrors = new List<ValidationResult>();

            if (input.TipoDeHomologacao == TipoDeHomologacaoEnum.EmExigencia ||
                input.TipoDeHomologacao == TipoDeHomologacaoEnum.Declinado)
            {
                if (string.IsNullOrEmpty(input.ObservacaoHomologacao))
                {
                    validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.EmptyObservacaoHomologacaoError")));
                }

                if (validationErrors.Any())
                {
                    throw new AbpValidationException(L("ValidationError"), validationErrors);
                }
            }

            #endregion
            proposta.TipoDeHomologacao = input.TipoDeHomologacao;
            proposta.ObservacaoHomologacao = input.ObservacaoHomologacao.ToUpper();
            proposta.DataHoraDaHomologacao = input.DataHoraDaHomologacao;
            if (proposta.PassoDaProposta <= PassoDaPropostaEnum.Homologacao && proposta.TipoDeHomologacao == TipoDeHomologacaoEnum.Homologado)
                proposta.PassoDaProposta = PassoDaPropostaEnum.Pagamento;

            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;

            return proposta;
        }

        public async Task<PropostaDeContratacaoInput> SavePassoHomologacao(PropostaDeContratacaoPassoHomologacaoInput input)
        {
            if (input.Id > 0)
            {
                PropostaDeContratacao proposta = await _propostaService.GetById(input.Id);
                proposta = await ProcessInputPassoHomologacao(proposta, input);

                if (proposta.TipoDeHomologacao == TipoDeHomologacaoEnum.EmExigencia)
                {
                    SetPassoAceite(proposta);
                }

                if (proposta.TipoDeHomologacao == TipoDeHomologacaoEnum.Declinado)
                {
                    proposta.StatusDaProposta = StatusDaPropostaEnum.Encerrado;
                }

                var entity = await _propostaService.SavePassoHomologacao(proposta);
                MapManual map = new MapManual();
                var retorno = map.MapManualProposta(entity);

                if (entity.Aceite && entity.AceiteCorretor && entity.PassoDaProposta >= PassoDaPropostaEnum.Homologacao)
                {
                    await CreateSnapshot(retorno);
                }

                return retorno;
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        #endregion

        #region Passo pagamento

        private async Task<PropostaDeContratacao> ProcessInputPassoPagamento(PropostaDeContratacao proposta, PropostaDeContratacaoPassoPagamentoInput input)
        {
            proposta.FormaDeContratacao = FormaDeContratacaoEnum.Adesao;
            return proposta;
        }

        public async Task<PropostaDeContratacaoInput> SavePassoPagamento(PropostaDeContratacaoPassoPagamentoInput input)
        {
            if (input.Id > 0)
            {
                var proposta = await _propostaService.GetById(input.Id);
                if (proposta == null)
                {
                    throw new UserFriendlyException("Proposta não encontrada.");
                }

                if (input.FormaDePagamento == FormaDePagamentoEnum.Deposito)
                {
                    if (input.RecebidoDeposito)
                    {
                        proposta.StatusDaProposta = StatusDaPropostaEnum.Concluido;
                        // TODO Criar um novo passoDaProposta ou informar como concluído na tela
                    }
                    else
                    {
                        throw new UserFriendlyException("É necessário que o corretor confirma o pagamento do depósito");
                    }
                }
                else
                {
                    throw new UserFriendlyException("A forma de pagamento selecionada não está disponível");
                }

                var propostaUpdated = await _propostaService.SavePassoPagamento(proposta);
                MapManual map = new MapManual();
                var retorno = map.MapManualProposta(propostaUpdated);

                if (propostaUpdated.Aceite && propostaUpdated.AceiteCorretor && propostaUpdated.PassoDaProposta >= PassoDaPropostaEnum.Pagamento)
                {
                    await CreateSnapshot(retorno);
                }

                return retorno;
            }

            throw new UserFriendlyException("Proposta não encontrada.");
        }

        #endregion

        public async Task SendEmailBoletoGerado(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailBoletoGerado(proposta);
        }

        public async Task SendEmailHomologar(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            await _propostaService.SendEmailHomologar(proposta);
            await _propostaService.SendNotificationNovaHomologacao(proposta);
        }


        public async Task SendEmailEdicaoDeProposta(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailEdicaoDeProposta(proposta);
        }

        public async Task SendEmailPreCadastro(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailPreCadastro(proposta);
        }

        public async Task SendEmailConcluirAceite(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailConcluirAceite(proposta);
            await _propostaService.SendNotificationAceite(proposta);
        }

        public async Task SendEmailConcluirAceiteOnlyBeneficiario(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailConcluirAceiteOnlyBeneficiario(proposta);
            await _propostaService.SendNotificationAceite(proposta);
        }

        public async Task SendEmailConcluirAceiteOnlyCorretor(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailConcluirAceiteOnlyCorretor(proposta);
        }

        public async Task SendEmailHomologacaoConcluida(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailHomologacaoConcluida(proposta);
            await _propostaService.SendNotificationHomologacaoConcluida(proposta);
        }

        public async Task SendEmailFormularios(PropostaDeContratacaoPassoAceiteDoContratoInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            _propostaService.SendEmailFormularios(proposta, input.EnviaCopiaAdministradora, input.EnviaCopiaCorretora);
        }

        public async Task SendEmailAceite(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            await _propostaService.SendEmailAceiteDoContrato(proposta);
        }

        public async Task SendEmailHomologacao(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            await _propostaService.SendEmailHomologacaoDoContrato(proposta);
        }

        public async Task SendEmailComprovanteDepositoCorretor(IdInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            await _propostaService.SendEmailComprovanteDepositoCorretor(proposta);
        }

        public async Task VerificaDocumentosEmExigencia(IdInput input)
        {
            if (input.Id > 0)
            {
                var documentosEmExigencia = await _arquivoDocumentoService.GetDocumentosEmExigenciaByPropostaId(input.Id);
                var proposta = await _propostaService.GetById(input.Id);

                if (proposta.PassoDaProposta <= PassoDaPropostaEnum.EnvioDeDocumentos)
                    proposta.PassoDaProposta = PassoDaPropostaEnum.AceiteDoContrato;

                if (documentosEmExigencia != null && documentosEmExigencia.Count > 0)
                    _propostaService.SendEmailDocumentoEmExigencia(proposta, documentosEmExigencia);

                return;
            }

            throw new UserFriendlyException(L("PropostaDeContratacao.EmptyPreCadastro"));
        }

        public async Task<PropostaDeContratacaoPassoDadosGeraisInput> GetRespostasDeclaracaoDeSaude(IdInput input)
        {
            var proposta = await GetById(input);
            var dadosGeraisResposta = new PropostaDeContratacaoPassoDadosGeraisInput();

            dadosGeraisResposta.ProponenteTitular = proposta.Titular.MapTo<ProponenteTitularInput>();
            dadosGeraisResposta.Dependentes = proposta.Titular.Dependentes.MapTo<List<DependenteInput>>();

            var respostasTitular = await _respostaService.GetAllListAsync(x => x.BeneficiarioId == dadosGeraisResposta.ProponenteTitular.Id);

            foreach (var resposta in respostasTitular)
            {
                if (dadosGeraisResposta.ProponenteTitular.RespostasDeclaracaoDeSaude == null) dadosGeraisResposta.ProponenteTitular.RespostasDeclaracaoDeSaude = new List<RespostaDeDeclaracaoDeSaudeInput>();
                dadosGeraisResposta.ProponenteTitular.RespostasDeclaracaoDeSaude.Add(new RespostaDeDeclaracaoDeSaudeInput()
                {
                    Marcada = resposta.Marcada,
                    Observacao = resposta.Observacao,
                    AnoDoEvento = resposta.AnoDoEvento,
                    PropostaDeContratacaoId = proposta.Id,
                    TipoDeItemDeDeclaracao = resposta.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao,
                    PerguntaId = resposta.ItemDeDeclaracaoDeSaude.Id,
                    Ordenacao = resposta.ItemDeDeclaracaoDeSaude.Ordenacao,
                    Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta
                });
            }
            var dependentes = 0;
            foreach (var dependente in dadosGeraisResposta.Dependentes)
            {
                var respostasDependente = await _respostaService.GetAllListAsync(x => x.BeneficiarioId == dependente.Id);

                foreach (var resposta in respostasDependente)
                {
                    if (dadosGeraisResposta.Dependentes[dependentes].RespostasDeclaracaoDeSaude == null) dadosGeraisResposta.Dependentes[dependentes].RespostasDeclaracaoDeSaude = new List<RespostaDeDeclaracaoDeSaudeInput>();
                    dadosGeraisResposta.Dependentes[dependentes].RespostasDeclaracaoDeSaude.Add(new RespostaDeDeclaracaoDeSaudeInput()
                    {
                        Marcada = resposta.Marcada,
                        Observacao = resposta.Observacao,
                        AnoDoEvento = resposta.AnoDoEvento,
                        PropostaDeContratacaoId = proposta.Id,
                        TipoDeItemDeDeclaracao = resposta.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao,
                        PerguntaId = resposta.ItemDeDeclaracaoDeSaude.Id,
                        Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta,
                        Ordenacao = resposta.ItemDeDeclaracaoDeSaude.Ordenacao
                    });
                }
                dependentes = dependentes + 1;
            }

            return dadosGeraisResposta;
        }

        public async Task<PropostaDeContratacaoInput> AtualizaStatusDaProposta(PropostaDeContratacaoInput input)
        {
            var proposta = await _propostaService.GetById(input.Id);
            proposta.StatusDaProposta = input.StatusDaProposta;
            await _propostaService.UpdateEntity(proposta);
            proposta = await _propostaService.GetById(input.Id);

            MapManual map = new MapManual();
            var retorno = map.MapManualProposta(proposta);

            if (proposta.Aceite && proposta.AceiteCorretor && proposta.PassoDaProposta >= PassoDaPropostaEnum.Pagamento)
            {
                await CreateSnapshot(retorno);
            }

            return retorno;
        }

        [Obsolete]
        public async Task<FormularioDeContratacaoInput> MontaFormulario(IdInput input)
        {
            var proposta = await Repository.GetAsync(input.Id);
            var contrato = _contratoService.GetAll().Where(x => x.ProdutoDePlanoDeSaudeId == proposta.ProdutoId).FirstOrDefault();
            var declaracaoDeSaude = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id && x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico)
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();
            var alturas = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id && x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura)
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();

            var pesos = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id && x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso)
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();

            var detalhesDeclaracao = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id && x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico && !string.IsNullOrEmpty(x.Observacao))
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();

            var itemDeDeclaracaoDoBeneficiarioMarcado = await _itemDeDeclaracaoDoBeneficiarioService
                .GetAll().FirstOrDefaultAsync(x => x.Id == proposta.ItemDeDeclaracaoDoBeneficiarioId);

            List<DeclaracaoDeSaudeFormularioInput> declaracaoFormulario = new List<DeclaracaoDeSaudeFormularioInput>();
            DeclaracaoDeSaudeFormularioInput itemDeclaracaoFormulario = new DeclaracaoDeSaudeFormularioInput();
            for (var i = 0; i < declaracaoDeSaude.Count; i++)
            {
                var resposta = declaracaoDeSaude[i];
                if (string.IsNullOrEmpty(itemDeclaracaoFormulario.Pergunta) || resposta.ItemDeDeclaracaoDeSaude.Pergunta == itemDeclaracaoFormulario.Pergunta)
                {
                    itemDeclaracaoFormulario.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                    if (string.IsNullOrEmpty(itemDeclaracaoFormulario.MarcadaTitular))
                    {
                        itemDeclaracaoFormulario.MarcadaTitular = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(itemDeclaracaoFormulario.MarcadaDep1))
                    {
                        itemDeclaracaoFormulario.MarcadaDep1 = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(itemDeclaracaoFormulario.MarcadaDep2))
                    {
                        itemDeclaracaoFormulario.MarcadaDep2 = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(itemDeclaracaoFormulario.MarcadaDep3))
                    {
                        itemDeclaracaoFormulario.MarcadaDep3 = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(itemDeclaracaoFormulario.MarcadaDep4))
                    {
                        itemDeclaracaoFormulario.MarcadaDep4 = resposta.Marcada;
                    }
                }
                if (i != declaracaoDeSaude.Count - 1)
                {
                    var proxResposta = declaracaoDeSaude[i + 1];
                    if (resposta.ItemDeDeclaracaoDeSaude.Pergunta != proxResposta.ItemDeDeclaracaoDeSaude.Pergunta)
                    {
                        declaracaoFormulario.Add(itemDeclaracaoFormulario);

                        itemDeclaracaoFormulario = new DeclaracaoDeSaudeFormularioInput();
                    }
                }
            }

            List<PesoAlturaFormularioInput> pesoAlturaFormulario = new List<PesoAlturaFormularioInput>();
            PesoAlturaFormularioInput itemPesoAlturaFormulario = new PesoAlturaFormularioInput();
            for (var i = 0; i <= alturas.Count - 1; i++)
            {
                var resposta = alturas[i];
                itemPesoAlturaFormulario.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                if (string.IsNullOrEmpty(itemPesoAlturaFormulario.AlturaTitular))
                {
                    itemPesoAlturaFormulario.AlturaTitular = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.AlturaDep1))
                {
                    itemPesoAlturaFormulario.AlturaDep1 = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.AlturaDep2))
                {
                    itemPesoAlturaFormulario.AlturaDep2 = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.AlturaDep3))
                {
                    itemPesoAlturaFormulario.AlturaDep3 = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.AlturaDep4))
                {
                    itemPesoAlturaFormulario.AlturaDep4 = resposta.Observacao;
                }
                if (i == alturas.Count - 1)
                {
                    pesoAlturaFormulario.Add(itemPesoAlturaFormulario);
                }
            }
            itemPesoAlturaFormulario = new PesoAlturaFormularioInput();
            for (var i = 0; i <= pesos.Count - 1; i++)
            {
                var resposta = pesos[i];
                itemPesoAlturaFormulario.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                if (string.IsNullOrEmpty(itemPesoAlturaFormulario.PesoTitular))
                {
                    itemPesoAlturaFormulario.PesoTitular = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.PesoDep1))
                {
                    itemPesoAlturaFormulario.PesoDep1 = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.PesoDep2))
                {
                    itemPesoAlturaFormulario.PesoDep2 = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.PesoDep3))
                {
                    itemPesoAlturaFormulario.PesoDep3 = resposta.Observacao;
                }
                else if (string.IsNullOrEmpty(itemPesoAlturaFormulario.PesoDep4))
                {
                    itemPesoAlturaFormulario.PesoDep4 = resposta.Observacao;
                }
                if (i == pesos.Count - 1)
                {
                    pesoAlturaFormulario.Add(itemPesoAlturaFormulario);
                }
            }

            var valoresPorFaixaEtaria = await _valorPorFaixaEtariaService
                .GetAll()
                .Where(x => x.ContratoId == contrato.Id)
                .ToListAsync();

            var valores = valoresPorFaixaEtaria.OrderBy(x => x.FaixaEtaria.IdadeInicial).ToList();

            decimal valorFaixa = 0;
            List<PercentualPorFaixaEtariaInput> percentuais = new List<PercentualPorFaixaEtariaInput>();
            foreach (var valor in valores)
            {
                var valorAtual = valor.Valor;
                string descricao = string.Empty;
                decimal percentual = 0;

                if (valor.FaixaEtaria.IdadeInicial == 0)
                {
                    valorFaixa = valorAtual;
                    descricao = valor.FaixaEtaria.Descricao;
                }
                else
                {
                    var diferenca = valor.Valor - valorFaixa;
                    percentual = Math.Round((diferenca * 100) / valorAtual, 2);
                    valorFaixa = valor.Valor;
                }

                percentuais.Add(new PercentualPorFaixaEtariaInput
                {
                    FaixaEtaria = valor.FaixaEtaria.Descricao,
                    Percentual = percentual
                });
            }


            var idadeTitular = PessoaHelpers.GetIdade(proposta.Titular.PessoaFisica.DataDeNascimento.Value);

            var vt = valoresPorFaixaEtaria.FirstOrDefault(x => idadeTitular >= x.FaixaEtaria.IdadeInicial && idadeTitular <= x.FaixaEtaria.IdadeFinal);
            var valorTitular = vt != null ? vt.Valor : 0;

            List<ValorPorFaixaEtariaFormularioInput> valoresFormulario = new List<ValorPorFaixaEtariaFormularioInput>();

            valoresFormulario.Add(new ValorPorFaixaEtariaFormularioInput()
            {
                Beneficiario = "Titular",
                Idade = idadeTitular,
                Valor = valorTitular
            });

            for (var i = 0; i <= proposta.Titular.Dependentes.Count - 1; i++)
            {
                var dependente = proposta.Titular.Dependentes.ToList()[i];
                var idade = PessoaHelpers.GetIdade(dependente.PessoaFisica.DataDeNascimento.Value);

                var vr = valoresPorFaixaEtaria.FirstOrDefault(x => idade >= x.FaixaEtaria.IdadeInicial && idade <= x.FaixaEtaria.IdadeFinal);
                var valor = vr != null ? vr.Valor : 0;

                valoresFormulario.Add(new ValorPorFaixaEtariaFormularioInput()
                {
                    Beneficiario = "Dep. " + (i + 1),
                    Idade = idade,
                    Valor = valor
                });
            }

            var chancela = proposta.ChancelaId.HasValue ? await _chancelaService.GetById(proposta.ChancelaId.Value) : null;

            var carenciasHTML = CarenciaANS.GetHTMLCarenciasANS();
            var indicesHTML = await _indiceService.GetHTMLFormulario(proposta.Produto.OperadoraId);

            var conteudoContrato = contrato.ConteudoOnline.Replace("{TABELA_CARENCIAS}", carenciasHTML);
            conteudoContrato = conteudoContrato.Replace("{TABELA_REAJUSTES}", indicesHTML);

            var formulario = new FormularioDeContratacaoInput()
            {
                Proposta = proposta.MapTo<PropostaDeContratacaoInput>(),
                Contrato = contrato.MapTo<ContratoInput>(),
                PesoAltura = pesoAlturaFormulario,
                DeclaracaoDeSaude = declaracaoFormulario,
                ItemDeDeclaracaoDoBeneficiarioMarcado = itemDeDeclaracaoDoBeneficiarioMarcado.MapTo<ItemDeDeclaracaoDoBeneficiarioInput>(),
                DetalhesDeclaracaoDeSaude = detalhesDeclaracao.Select(x => new DetalhesDeclaracaoDeSaudeInput
                {
                    Pergunta = x.ItemDeDeclaracaoDeSaude.Pergunta,
                    Nome = x.Beneficiario.PessoaFisica.NomePessoa,
                    Ano = x.AnoDoEvento.ToString(),
                    Detalhes = x.Observacao
                }).ToList(),
                PercentuaisPorFaixaEtaria = percentuais,
                ValoresPorFaixaEtaria = valoresFormulario,
                ValorDoPlano = valoresFormulario.Sum(x => x.Valor),
                Chancela = chancela.MapTo<ChancelaInput>(),
                EstadoNome = proposta.Estado.Nome,
                PathLogoOperadora = proposta.Produto.Operadora.ImagemId > 0 ? proposta.Produto.Operadora.Imagem.Path : "https://www.theclementimall.com/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png",
                PathLogoAdministradora = proposta.Produto.Administradora.Imagem != null ? proposta.Produto.Administradora.Imagem.Path : "https://www.theclementimall.com/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png"
            };

            formulario.Contrato.Conteudo = conteudoContrato;

            return formulario;
        }

        public async Task<PropostaDeContratacaoDoRelatorioV1Input> MontaPropostaDeContratacaoDoRelatorioV1(IdInput input)
        {
            var proposta = await Repository.GetAsync(input.Id);
            var contrato = _contratoService.GetAll().Where(x => x.ProdutoDePlanoDeSaudeId == proposta.ProdutoId).FirstOrDefault();

            #region Declaração de Saúde
            var declaracaoDeSaude = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id && x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico)
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();
            var alturasPesos = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id
                    && (x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura
                        || x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso))
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();

            var detalhesDeclaracao = await _respostaService.GetAll()
                .Where(x => x.PropostaDeContratacaoId == proposta.Id && x.ItemDeDeclaracaoDeSaude.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Generico && !string.IsNullOrEmpty(x.Observacao))
                .OrderBy(x => x.ItemDeDeclaracaoDeSaudeId)
                .ThenBy(x => x.BeneficiarioId)
                .ToListAsync();

            var itemDeDeclaracaoDoBeneficiarioMarcado = await _itemDeDeclaracaoDoBeneficiarioService
                .GetAll().FirstOrDefaultAsync(x => x.Id == proposta.ItemDeDeclaracaoDoBeneficiarioId);

            List<PerguntaDaDeclaracaoDeSaudeV1Input> perguntasDaDeclaracaoDeSaudeV1 = new List<PerguntaDaDeclaracaoDeSaudeV1Input>();
            var perguntaDaDeclaracaoDeSaudeV1 = new PerguntaDaDeclaracaoDeSaudeV1Input();
            var perguntaCount = 1;
            for (var i = 0; i < declaracaoDeSaude.Count; i++)
            {
                var resposta = declaracaoDeSaude[i];
                if (string.IsNullOrEmpty(perguntaDaDeclaracaoDeSaudeV1.Pergunta) || resposta.ItemDeDeclaracaoDeSaude.Pergunta == perguntaDaDeclaracaoDeSaudeV1.Pergunta)
                {
                    perguntaDaDeclaracaoDeSaudeV1.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                    perguntaDaDeclaracaoDeSaudeV1.PerguntaEnumerada = string.Concat(perguntaCount, " - ", resposta.ItemDeDeclaracaoDeSaude.Pergunta);
                    perguntaDaDeclaracaoDeSaudeV1.Numero = perguntaCount.ToString();
                    perguntaDaDeclaracaoDeSaudeV1.Ano = resposta.AnoDoEvento.ToString();
                    perguntaDaDeclaracaoDeSaudeV1.Detalhes = resposta.Observacao;

                    if (string.IsNullOrEmpty(perguntaDaDeclaracaoDeSaudeV1.RespostaTitular))
                    {
                        perguntaDaDeclaracaoDeSaudeV1.RespostaTitular = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(perguntaDaDeclaracaoDeSaudeV1.RespostaDep1))
                    {
                        perguntaDaDeclaracaoDeSaudeV1.RespostaDep1 = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(perguntaDaDeclaracaoDeSaudeV1.RespostaDep2))
                    {
                        perguntaDaDeclaracaoDeSaudeV1.RespostaDep2 = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(perguntaDaDeclaracaoDeSaudeV1.RespostaDep3))
                    {
                        perguntaDaDeclaracaoDeSaudeV1.RespostaDep3 = resposta.Marcada;
                    }
                    else if (string.IsNullOrEmpty(perguntaDaDeclaracaoDeSaudeV1.RespostaDep4))
                    {
                        perguntaDaDeclaracaoDeSaudeV1.RespostaDep4 = resposta.Marcada;
                    }
                }
                if (i <= declaracaoDeSaude.Count - 1)
                {
                    if (i == declaracaoDeSaude.Count - 1)
                    {
                        perguntasDaDeclaracaoDeSaudeV1.Add(perguntaDaDeclaracaoDeSaudeV1);
                    }
                    else
                    {
                        var proxResposta = declaracaoDeSaude[i + 1];
                        if (resposta.ItemDeDeclaracaoDeSaude.Pergunta != proxResposta.ItemDeDeclaracaoDeSaude.Pergunta)
                        {
                            perguntasDaDeclaracaoDeSaudeV1.Add(perguntaDaDeclaracaoDeSaudeV1);
                            perguntaDaDeclaracaoDeSaudeV1 = new PerguntaDaDeclaracaoDeSaudeV1Input();
                            perguntaCount++;
                        }
                    }
                }
            }

            List<PerguntaDaDeclaracaoDeSaudePositivaV1Input> perguntasDaDeclaracaoDeSaudePositivaV1 = new List<PerguntaDaDeclaracaoDeSaudePositivaV1Input>();
            for (var i = 0; i < declaracaoDeSaude.Count; i++)
            {
                var resposta = declaracaoDeSaude[i];

                if (resposta.Marcada == "S")
                {
                    var perguntaDaDeclaracaoDeSaudePositivaV1 = new PerguntaDaDeclaracaoDeSaudePositivaV1Input();

                    if (resposta.Beneficiario.TipoDeBeneficiario == TipoDeBeneficiarioEnum.Titular)
                    {
                        perguntaDaDeclaracaoDeSaudePositivaV1.NomeDoBeneficiarioComTipoDeBeneficiario = string.Concat("Titular - ", resposta.Beneficiario.NomePessoa);
                    }
                    else if (resposta.Beneficiario.TipoDeBeneficiario == TipoDeBeneficiarioEnum.Dependente)
                    {
                        perguntaDaDeclaracaoDeSaudePositivaV1.NomeDoBeneficiarioComTipoDeBeneficiario = string.Concat("Dep. - ", resposta.Beneficiario.NomePessoa);
                    }

                    var numero = perguntasDaDeclaracaoDeSaudeV1.Where(x => x.Pergunta == resposta.ItemDeDeclaracaoDeSaude.Pergunta).First().Numero;

                    perguntaDaDeclaracaoDeSaudePositivaV1.NomeDoBeneficiario = resposta.Beneficiario.NomePessoa;
                    perguntaDaDeclaracaoDeSaudePositivaV1.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                    perguntaDaDeclaracaoDeSaudePositivaV1.Numero = numero;
                    perguntaDaDeclaracaoDeSaudePositivaV1.PerguntaEnumerada = string.Concat(numero, " - ", resposta.ItemDeDeclaracaoDeSaude.Pergunta);
                    perguntaDaDeclaracaoDeSaudePositivaV1.Ano = resposta.AnoDoEvento.ToString();
                    perguntaDaDeclaracaoDeSaudePositivaV1.Resposta = resposta.Marcada;
                    perguntaDaDeclaracaoDeSaudePositivaV1.Detalhes = resposta.Observacao;

                    perguntasDaDeclaracaoDeSaudePositivaV1.Add(perguntaDaDeclaracaoDeSaudePositivaV1);
                }
            }

            List<PesoAlturaV1Input> perguntasPesoEAlturaV1 = new List<PesoAlturaV1Input>();
            PesoAlturaV1Input itemPesoAlturaV1 = new PesoAlturaV1Input();
            for (var i = 0; i < alturasPesos.Count; i++)
            {
                var resposta = alturasPesos[i];
                if (string.IsNullOrEmpty(itemPesoAlturaV1.Pergunta) || resposta.ItemDeDeclaracaoDeSaude.Pergunta == itemPesoAlturaV1.Pergunta)
                {
                    itemPesoAlturaV1.Pergunta = resposta.ItemDeDeclaracaoDeSaude.Pergunta;
                    if (string.IsNullOrEmpty(itemPesoAlturaV1.RespostaTitular))
                    {
                        itemPesoAlturaV1.RespostaTitular = resposta.Observacao;
                    }
                    else if (string.IsNullOrEmpty(itemPesoAlturaV1.RespostaDep1))
                    {
                        itemPesoAlturaV1.RespostaDep1 = resposta.Observacao;
                    }
                    else if (string.IsNullOrEmpty(itemPesoAlturaV1.RespostaDep2))
                    {
                        itemPesoAlturaV1.RespostaDep2 = resposta.Observacao;
                    }
                    else if (string.IsNullOrEmpty(itemPesoAlturaV1.RespostaDep3))
                    {
                        itemPesoAlturaV1.RespostaDep3 = resposta.Observacao;
                    }
                    else if (string.IsNullOrEmpty(itemPesoAlturaV1.RespostaDep4))
                    {
                        itemPesoAlturaV1.RespostaDep4 = resposta.Observacao;
                    }
                }
                if (i <= alturasPesos.Count - 1)
                {
                    if (i == alturasPesos.Count - 1)
                    {
                        perguntasPesoEAlturaV1.Add(itemPesoAlturaV1);
                    }
                    else
                    {
                        var proxResposta = alturasPesos[i + 1];
                        if (resposta.ItemDeDeclaracaoDeSaude.Pergunta != proxResposta.ItemDeDeclaracaoDeSaude.Pergunta)
                        {
                            perguntasPesoEAlturaV1.Add(itemPesoAlturaV1);
                            itemPesoAlturaV1 = new PesoAlturaV1Input();
                        }
                    }
                }
            }

            var itensDeDeclaracaoDoBeneficiarioV1 = new List<ItemDeDeclaracaoDoBeneficiarioV1Input>();
            if (proposta.Produto != null
                && proposta.Produto.DeclaracaoDoBeneficiario != null
                && itemDeDeclaracaoDoBeneficiarioMarcado != null)
            {
                foreach (var itemDeDeclaracaoDoBeneficiario in proposta.Produto.DeclaracaoDoBeneficiario.ItensDeDeclaracaoDoBeneficiario)
                {
                    var item = new ItemDeDeclaracaoDoBeneficiarioV1Input()
                    {
                        Termo = itemDeDeclaracaoDoBeneficiario.Pergunta
                    };

                    if (itemDeDeclaracaoDoBeneficiario.Pergunta == itemDeDeclaracaoDoBeneficiarioMarcado.Pergunta)
                    {
                        item.EoTermoEscolhido = true;
                    }

                    itensDeDeclaracaoDoBeneficiarioV1.Add(item);
                }
            }
            #endregion

            #region Valores Por Beneficiario Por Idade

            var valoresPorFaixaEtaria = await _valorPorFaixaEtariaService
                .GetAll()
                .Where(x => x.ContratoId == contrato.Id)
                .ToListAsync();

            var idadeTitular = PessoaHelpers.GetIdade(proposta.Titular.PessoaFisica.DataDeNascimento.Value);

            var vt = valoresPorFaixaEtaria.FirstOrDefault(x => idadeTitular >= x.FaixaEtaria.IdadeInicial && idadeTitular <= x.FaixaEtaria.IdadeFinal);
            var valorTitular = vt != null ? vt.Valor : 0;

            List<ValorPorBeneficiarioPorIdadeV1Input> valorPorBeneficiarioPorIdadeV1 = new List<ValorPorBeneficiarioPorIdadeV1Input>();

            valorPorBeneficiarioPorIdadeV1.Add(new ValorPorBeneficiarioPorIdadeV1Input()
            {
                Beneficiario = "Titular",
                Idade = idadeTitular,
                Valor = valorTitular
            });

            for (var i = 0; i <= proposta.Titular.Dependentes.Count - 1; i++)
            {
                var dependente = proposta.Titular.Dependentes.ToList()[i];
                var idade = PessoaHelpers.GetIdade(dependente.PessoaFisica.DataDeNascimento.Value);

                var vr = valoresPorFaixaEtaria.FirstOrDefault(x => idade >= x.FaixaEtaria.IdadeInicial && idade <= x.FaixaEtaria.IdadeFinal);
                var valor = vr != null ? vr.Valor : 0;

                valorPorBeneficiarioPorIdadeV1.Add(new ValorPorBeneficiarioPorIdadeV1Input()
                {
                    Beneficiario = "Dep. " + (i + 1),
                    Idade = idade,
                    Valor = valor
                });
            }

            #endregion

            #region Chancela
            ChancelaV1Input chancelaV1 = null;
            if (proposta.Chancela != null)
            {
                chancelaV1 = new ChancelaV1Input()
                {
                    Nome = proposta.Chancela.Nome,
                    TaxaDeAdesao = proposta.Chancela.TaxaDeAdesao,
                    TaxaMensal = proposta.Chancela.TaxaMensal
                };

                if (proposta.Chancela.Associacao != null)
                {
                    chancelaV1.Entidade = new EntidadeV1Input()
                    {
                        Nome = proposta.Chancela.Associacao.NomePessoa
                    };
                }
            }
            #endregion

            #region Estado
            EstadoV1Input estadoV1 = null;
            if (proposta.Estado != null)
            {
                estadoV1 = new EstadoV1Input()
                {
                    Nome = proposta.Estado.Nome.ToUpper()
                };
            }
            #endregion

            #region Corretora, Gerente da Corretora e Supervisor da corretora
            CorretoraV1Input corretoraV1 = null;
            if (proposta.Corretora != null)
            {
                corretoraV1 = new CorretoraV1Input()
                {
                    Nome = proposta.Corretora.Nome.ToUpper()
                };
                if (proposta.Corretora.PessoaJuridica.TelefoneComercial != null)
                {
                    corretoraV1.Telefone = proposta.Corretora.PessoaJuridica.TelefoneComercial.Tostring();

                }
                if (proposta.Corretora.PessoaJuridica.EmailPrincipal != null)
                {
                    corretoraV1.Email = proposta.Corretora.PessoaJuridica.EmailPrincipal.Endereco;

                }


                SupervisorCorretoraV1Input supervisorCorretoraV1 = null;
                if (proposta.SupervisorCorretora != null)
                {
                    supervisorCorretoraV1 = new SupervisorCorretoraV1Input()
                    {
                        Nome = proposta.SupervisorCorretora.NomePessoa.ToUpper(),
                        CPF = proposta.SupervisorCorretora.PessoaFisica.Cpf != null ? proposta.SupervisorCorretora.PessoaFisica.Cpf : string.Empty,
                        email = proposta.SupervisorCorretora.PessoaFisica.Email != null ? proposta.SupervisorCorretora.PessoaFisica.Email : string.Empty,

                        celular = proposta.SupervisorCorretora.PessoaFisica.TelefoneCelular.Numero != null && proposta.SupervisorCorretora.PessoaFisica.TelefoneCelular.DDD != null
                        ? proposta.SupervisorCorretora.PessoaFisica.TelefoneCelular.DDD + proposta.SupervisorCorretora.PessoaFisica.TelefoneCelular.Numero : string.Empty

                    };
                }
                corretoraV1.Supervisor = supervisorCorretoraV1;

                GerenteCorretoraV1Input gerenteCorretoraV1 = null;
                if (proposta.GerenteCorretora != null)
                {
                    gerenteCorretoraV1 = new GerenteCorretoraV1Input()
                    {
                        Nome = proposta.GerenteCorretora.NomePessoa.ToUpper(),
                        CPF = proposta.GerenteCorretora.PessoaFisica.Cpf != null ? proposta.GerenteCorretora.PessoaFisica.Cpf : string.Empty,
                        email = proposta.GerenteCorretora.PessoaFisica.Email != null ? proposta.GerenteCorretora.PessoaFisica.Email : string.Empty,

                        celular = proposta.GerenteCorretora.PessoaFisica.TelefoneCelular.Numero != null && proposta.GerenteCorretora.PessoaFisica.TelefoneCelular.DDD != null
                        ? proposta.GerenteCorretora.PessoaFisica.TelefoneCelular.DDD + proposta.GerenteCorretora.PessoaFisica.TelefoneCelular.Numero : string.Empty
                    };
                }
                corretoraV1.Gerente = gerenteCorretoraV1;
            }
            #endregion

            #region Corretor
            CorretorV1Input corretorV1 = null;
            if (proposta.Corretor != null)
            {
                string assinaturaCompleta = "Assinatura ausente. Aceite ou documento não encontrado";
                string assinaturaCurta = "Assinatura ausente. Aceite ou documento não encontrado";

                if (proposta.AceiteCorretor && proposta.Corretor.PessoaFisica.Documentos != null)
                {
                    assinaturaCurta = proposta.Corretor.NomePessoa.ToUpper();
                }

                if (proposta.AceiteCorretor && proposta.Corretor.PessoaFisica.DocumentoPrincipalPessoa != null)
                {
                    assinaturaCompleta = string.Format(
                        "Eu, {0}, inscrito no CPF {1}, declaro de que estou habilitado conforme a <a href='http://www.planalto.gov.br/ccivil_03/Leis/L4594.htm' target='_blank' title='Lei Nº 4.594'>Lei Nº 4.594</a> e que sou responsável pela orientação do preenchimento dessa proposta. Assinado digitalmente.",
                        proposta.Corretor.NomePessoa.ToUpper(),
                        proposta.Corretor.PessoaFisica.DocumentoPrincipalPessoa);
                }

                corretorV1 = new CorretorV1Input()
                {
                    Nome = proposta.Corretor.NomePessoa.ToUpper(),
                    AssinaturaCompleta = assinaturaCompleta,
                    AssinaturaCurta = assinaturaCurta
                };
                if (proposta.Corretor.PessoaFisica.TelefoneCelular != null)
                {
                    corretorV1.Telefone = proposta.Corretor.PessoaFisica.TelefoneCelular.Tostring();

                }
                if (proposta.Corretor.PessoaFisica.Cpf != null)
                {
                    corretorV1.CPF = proposta.Corretor.PessoaFisica.Cpf;
                }
                if (proposta.Corretor.PessoaFisica.Email != null)
                {
                    corretorV1.Email = proposta.Corretor.PessoaFisica.Email;
                }

                

            }
            #endregion

            #region Profissão
            ProfissaoV1Input profissaoV1 = null;
            if (proposta.Profissao != null)
            {
                profissaoV1 = new ProfissaoV1Input()
                {
                    Nome = proposta.Profissao.Titulo.ToUpper()
                };
            }
            #endregion

            #region Produto, Adminitradora e Operadora
            ProdutoDePlanoDeSaudeV1Input produtoV1 = null;
            if (proposta.Produto != null)
            {
                produtoV1 = new ProdutoDePlanoDeSaudeV1Input()
                {
                    Nome = proposta.Produto.Nome.ToUpper(),
                    Descricao = proposta.Produto.Descricao.ToUpper(),
                    LinkRedeCredenciada = proposta.Produto.LinkRedeCredenciada,
                    CartaDeOrientacao = proposta.Produto.CartaDeOrientacao,
                    NumeroDeRegistro = proposta.Produto.PlanoDeSaude.NumeroDeRegistro,
                    Reembolso = proposta.Produto.PlanoDeSaude.Reembolso,
                    DescricaoDoReembolso = proposta.Produto.PlanoDeSaude.DescricaoDoReembolso,
                    Abrangencia = L(string.Concat("Enum_AbrangenciaDoFeriadoEnum_", proposta.Produto.PlanoDeSaude.Abrangencia)),
                    SegmentacaoAssistencial = L(string.Concat("Enum_SegmentacaoAssistencialDoPlanoEnum_", proposta.Produto.PlanoDeSaude.SegmentacaoAssistencial)),
                    Acomodacao = L(string.Concat("Enum_AcomodacaoEnum_", proposta.Produto.PlanoDeSaude.Acomodacao)).ToString().ToUpper()
                };

                if (proposta.Produto.Administradora != null)
                {
                    var administradora = new AdministradoraV1Input()
                    {
                        Nome = proposta.Produto.Administradora.PessoaJuridica.RazaoSocial.ToUpper()
                    };

                    if (proposta.Produto.Administradora.PessoaJuridica.DocumentoPrincipalPessoa != null)
                    {
                        administradora.CNPJ = proposta.Produto.Administradora.PessoaJuridica.DocumentoPrincipalPessoa;
                    }

                    administradora.AssinaturaCompleta = "Assinatura ausente. Proposta não homologada";
                    administradora.AssinaturaCurta = "Assinatura ausente. Proposta não homologada";

                    if (proposta.TipoDeHomologacao >= TipoDeHomologacaoEnum.Homologado
                        && proposta.Produto.Administradora.PessoaJuridica.Documentos != null)
                    {
                        administradora.AssinaturaCurta = proposta.Produto.Administradora.PessoaJuridica.RazaoSocial.ToUpper();
                    }

                    if (proposta.TipoDeHomologacao >= TipoDeHomologacaoEnum.Homologado
                        && proposta.Produto.Administradora.PessoaJuridica.DocumentoPrincipalPessoa != null)
                    {
                        administradora.AssinaturaCompleta = string.Format(
                            "EU, {0}, INSCRITO(A) NO CNPJ {1}, DECLARO QUE A PROPOSTA ESTÁ HOMOLOGADA. Assinado digitalmente.",
                        proposta.Produto.Administradora.PessoaJuridica.RazaoSocial.ToUpper(),
                        proposta.Produto.Administradora.PessoaJuridica.DocumentoPrincipalPessoa);
                    }

                    produtoV1.Administradora = administradora;
                }

                if (proposta.Produto.Operadora != null)
                {
                    var operadora = new OperadoraV1Input()
                    {
                        Nome = proposta.Produto.Operadora.NomePessoa.ToUpper()
                    };
                    produtoV1.Operadora = operadora;
                }

                if (proposta.Produto.RelatorioProspostaDeContratacaoId.HasValue)
                {
                    produtoV1.RelatorioProspostaDeContratacaoId = proposta.Produto.RelatorioProspostaDeContratacaoId.Value;
                }

                if (proposta.Produto.RelatorioFichaDeEntidadeId.HasValue)
                {
                    produtoV1.RelatorioFichaDeEntidadeId = proposta.Produto.RelatorioFichaDeEntidadeId.Value;
                }
            }
            #endregion

            #region Titular, Endereço, Dependentes e o Responsável
            TitularV1Input titularV1 = null;
            if (proposta.Titular != null)
            {
                titularV1 = new TitularV1Input()
                {
                    Nome = proposta.Titular.PessoaFisica.Nome.ToUpper(),
                    Idade = PessoaHelpers.GetIdade(proposta.Titular.PessoaFisica.DataDeNascimento.Value),
                    EstadoCivil = L(string.Concat("Enum_EstadoCivilEnum_", proposta.Titular.PessoaFisica.EstadoCivil)),
                    DataDeNascimento = proposta.Titular.PessoaFisica.DataDeNascimento.Value.ToString("dd/MM/yyyy"),
                    Sexo = L(string.Concat("Enum_SexoEnum_", proposta.Titular.PessoaFisica.Sexo)).ToString().ToUpper(),
                    Nacionalidade = proposta.Titular.PessoaFisica.Nacionalidade.ToUpper(),
                    NomeDaMae = proposta.Titular.NomeDaMae.ToUpper(),
                    NumeroDoCartaoNacionalDeSaude = proposta.Titular.NumeroDoCartaoNacionalDeSaude,
                    DeclaracaoDeNascidoVivo = proposta.Titular.DeclaracaoDeNascidoVivo,
                    Identidade = proposta.Titular.PessoaFisica.Rg,
                    OrgaoExpedidorDaIdentidade = proposta.Titular.PessoaFisica.OrgaoExpedidor.ToUpper(),
                    Cpf = proposta.Titular.PessoaFisica.Cpf

                    //Boleto = proposta.Titular.boleto == true ? "X" : "" ,
                    //DescontoFolha = proposta.Titular.folha == true ? "X" : "",
                    //DebitoContaSim = proposta.Titular.DebitoConta == true ? "X" : "",
                    //DebitoContaNao = proposta.Titular.DebitoConta == false ? "X" : "",
                    //FolhaFichaSim = proposta.Titular.FolhaFicha == true ? "X" : "",
                    //FolhaFichaNao = proposta.Titular.FolhaFicha == false ? "X" : "",
                    //Matricula = proposta.Titular.matricula
                };

                if (proposta.Titular.matricula == null)
                {
                    titularV1.Boleto = string.Empty;
                    titularV1.DescontoFolha = string.Empty;
                    titularV1.DebitoContaSim = string.Empty;
                    titularV1.DebitoContaNao = string.Empty;
                    titularV1.FolhaFichaSim = string.Empty;
                    titularV1.FolhaFichaNao = string.Empty;
                    titularV1.Matricula = string.Empty;
                }
                else
                {
                    titularV1.Boleto = proposta.Titular.boleto == true ? "X" : string.Empty;
                    titularV1.DescontoFolha = proposta.Titular.folha == true ? "X" : string.Empty;
                    titularV1.DebitoContaSim = proposta.Titular.DebitoConta == true ? "X" : string.Empty;
                    titularV1.DebitoContaNao = proposta.Titular.DebitoConta == false ? "X" : string.Empty;
                    titularV1.FolhaFichaSim = proposta.Titular.FolhaFicha == true ? "X" : string.Empty;
                    titularV1.FolhaFichaNao = proposta.Titular.FolhaFicha == false ? "X" : string.Empty;
                    titularV1.Matricula = proposta.Titular.matricula;
                }



                if (proposta.Aceite && proposta.Titular.PessoaFisica?.Documentos != null)
                {
                    if (proposta.TitularResponsavelLegal)
                        titularV1.AssinaturaCurta = proposta.Titular.NomePessoa.ToUpper();
                    else
                        titularV1.AssinaturaCurta = proposta.Titular.Responsavel.NomePessoa.ToUpper();
                }
                else
                {
                    titularV1.AssinaturaCurta = "Assinatura ausente. Aceite não encontrado";
                }

                if (proposta.Aceite && proposta.Titular.PessoaFisica?.Documentos != null)
                {
                    titularV1.AssinaturaCompleta = string.Format(
                        "EU, {0}, INSCRITO(A) NO CPF {1}, TENHO CIÊNCIA E CONCORDO QUE TODAS AS INFORMAÇÕES DESTA PÁGINA AQUI PRESTADAS DE ACORDO COM A RESOLUÇÃO NORMATIVA - RN Nº 413, DE 11 DE NOVEMBRO DE 2016, SEÇÃO IV - Art. 7º E OS DOCUMENTOS APRESENTADOS SÃO ABSOLUTAMENTE VERDADEIROS E COMPLETOS, E ME RESPONSABILIZO CIVIL E CRIMINALMENTE POR ELES. ASSINATURA ELETRÔNICA. Assinado digitalmente.",
                        proposta.Titular.NomePessoa,
                        proposta.Titular.PessoaFisica.DocumentoPrincipalPessoa);

                }
                else
                {
                    titularV1.AssinaturaCompleta = "Assinatura ausente. Aceite ou documento não encontrado";
                }

                if (proposta.Titular.PessoaFisica.EmailPrincipal != null)
                {
                    titularV1.EmailPrincipal = proposta.Titular.PessoaFisica.EmailPrincipal.Endereco;
                }

                if (proposta.Titular.PessoaFisica.TelefoneCelular != null)
                {
                    titularV1.TelefoneCelular = string.Concat("(", proposta.Titular.PessoaFisica.TelefoneCelular.DDD,
                        ")", proposta.Titular.PessoaFisica.TelefoneCelular.Numero);
                }

                if (proposta.Titular.PessoaFisica.EnderecoPrincipal != null)
                {
                    var tipoDeLogradouro = proposta.Titular.PessoaFisica.EnderecoPrincipal.TipoDeLogradouro != null
                            ? string.Concat(proposta.Titular.PessoaFisica.EnderecoPrincipal.TipoDeLogradouro.Descricao.ToUpper(), " ")
                            : string.Empty;

                    titularV1.Endereco = new EnderecoV1Input()
                    {
                        Logradouro = string.Concat(tipoDeLogradouro, proposta.Titular.PessoaFisica.EnderecoPrincipal.Logradouro).ToString().ToUpper(),
                        Numero = proposta.Titular.PessoaFisica.EnderecoPrincipal.Numero,
                        Complemento = proposta.Titular.PessoaFisica.EnderecoPrincipal.Complemento.ToString().ToUpper(),
                        Bairro = proposta.Titular.PessoaFisica.EnderecoPrincipal.Bairro.ToString().ToUpper(),
                        CEP = proposta.Titular.PessoaFisica.EnderecoPrincipal.CEP,
                        Referencia = proposta.Titular.PessoaFisica.EnderecoPrincipal.Referencia,
                        Estado = proposta.Titular.PessoaFisica.EnderecoPrincipal.Cidade.Estado.Nome.ToString().ToUpper(),
                        Cidade = proposta.Titular.PessoaFisica.EnderecoPrincipal.Cidade.Nome.ToString().ToUpper(),
                        UF = proposta.Titular.PessoaFisica.EnderecoPrincipal.Cidade.Estado.Sigla,
                        LogradouroComNumeroEComplemento = string.Concat(proposta.Titular.PessoaFisica.EnderecoPrincipal.LogradouroComTipo, " ",
                            proposta.Titular.PessoaFisica.EnderecoPrincipal.Numero, " ", proposta.Titular.PessoaFisica.EnderecoPrincipal.Complemento).ToString().ToUpper()
                    };
                }
                else
                {
                    var tipoDeLogradouro = proposta.Titular.PessoaFisica.Enderecos.First().TipoDeLogradouro != null
                           ? string.Concat(proposta.Titular.PessoaFisica.Enderecos.First().TipoDeLogradouro.Descricao.ToString().ToUpper(), " ")
                           : string.Empty;

                    titularV1.Endereco = new EnderecoV1Input()
                    {
                        Logradouro = string.Concat(tipoDeLogradouro, proposta.Titular.PessoaFisica.Enderecos.First().Logradouro).ToString().ToUpper(),
                        Numero = proposta.Titular.PessoaFisica.Enderecos.First().Numero,
                        Complemento = proposta.Titular.PessoaFisica.Enderecos.First().Complemento.ToString().ToUpper(),
                        Bairro = proposta.Titular.PessoaFisica.Enderecos.First().Bairro.ToString().ToUpper(),
                        CEP = proposta.Titular.PessoaFisica.Enderecos.First().CEP,
                        Referencia = proposta.Titular.PessoaFisica.Enderecos.First().Referencia,
                        Estado = proposta.Titular.PessoaFisica.Enderecos.First().Cidade.Estado.Nome.ToString().ToUpper(),
                        Cidade = proposta.Titular.PessoaFisica.Enderecos.First().Cidade.Nome.ToString().ToUpper(),
                        UF = proposta.Titular.PessoaFisica.Enderecos.First().Cidade.Estado.Sigla,
                        LogradouroComNumeroEComplemento = string.Concat(proposta.Titular.PessoaFisica.Enderecos.First().LogradouroComTipo, " ",
                            proposta.Titular.PessoaFisica.Enderecos.First().Numero, " ", proposta.Titular.PessoaFisica.Enderecos.First().Complemento).ToString().ToUpper()
                    };
                }

                if (proposta.Titular.Responsavel != null)
                {
                    titularV1.Responsavel = new ResponsavelV1Input()
                    {
                        Nome = proposta.Titular.Responsavel.NomePessoa,
                        TipoDeResponsavel = L(string.Concat("Enum_TipoDeResponsavelEnum_", proposta.Titular.Responsavel.TipoDeResponsavel)),
                        Cpf = proposta.Titular.Responsavel.PessoaFisica.Cpf,
                        EstadoCivil = L(string.Concat("Enum_EstadoCivilEnum_", proposta.Titular.Responsavel.PessoaFisica.EstadoCivil)),
                        DataDeNascimento = proposta.Titular.Responsavel.PessoaFisica.DataDeNascimento.Value.ToString("dd/MM/yyyy"),
                        Sexo = L(string.Concat("Enum_SexoEnum_", proposta.Titular.Responsavel.PessoaFisica.Sexo)),
                        Nacionalidade = proposta.Titular.Responsavel.PessoaFisica.Nacionalidade,
                    };

                    if (proposta.Aceite && proposta.TitularResponsavelLegal && proposta.Responsavel.PessoaFisica.Documentos != null)
                    {
                        titularV1.Responsavel.AssinaturaCurta = proposta.Titular.Responsavel.NomePessoa;
                    }
                    else
                    {
                        titularV1.Responsavel.AssinaturaCurta = "Assinatura ausente. Aceite não encontrado";
                    }

                    if (proposta.Aceite && !proposta.TitularResponsavelLegal && proposta.Responsavel.PessoaFisica?.DocumentoPrincipal != null)
                    {
                        titularV1.Responsavel.AssinaturaCompleta = string.Format(
                            "EU, {0}, INSCRITO(A) NO CPF {1}, TENHO CIÊNCIA E CONCORDO QUE TODAS AS INFORMAÇÕES DESTA PÁGINA AQUI PRESTADAS DE ACORDO COM A RESOLUÇÃO NORMATIVA - RN Nº 413, DE 11 DE NOVEMBRO DE 2016, SEÇÃO IV - Art. 7º E OS DOCUMENTOS APRESENTADOS SÃO ABSOLUTAMENTE VERDADEIROS E COMPLETOS, E ME RESPONSABILIZO CIVIL E CRIMINALMENTE POR ELES. ASSINATURA ELETRÔNICA. Assinado digitalmente.",
                            proposta.Titular.Responsavel.NomePessoa,
                            proposta.Titular.Responsavel.PessoaFisica.DocumentoPrincipal.Numero);
                    }
                    else
                    {
                        titularV1.Responsavel.AssinaturaCompleta = "Assinatura ausente. Aceite ou documento não encontrado";
                    }

                    if (proposta.Titular.Responsavel.PessoaFisica.TelefoneCelular != null)
                    {
                        titularV1.Responsavel.TelefoneCelular = string.Concat("(", proposta.Titular.Responsavel.PessoaFisica.TelefoneCelular.DDD,
                            ")", proposta.Titular.Responsavel.PessoaFisica.TelefoneCelular.Numero);
                    }

                    if (proposta.Titular.Responsavel.PessoaFisica.EmailPrincipal != null)
                    {
                        titularV1.Responsavel.EmailPrincipal = proposta.Titular.Responsavel.PessoaFisica.EmailPrincipal.Endereco;
                    }
                }

                if (proposta.Titular.Dependentes?.Count > 0)
                {
                    titularV1.Dependentes = new List<DependenteV1Input>();
                    foreach (var dependente in proposta.Titular.Dependentes)
                    {
                        var dependenteInputV1 = new DependenteV1Input()
                        {
                            Nome = dependente.NomePessoa.ToString().ToUpper(),
                            Cpf = dependente.PessoaFisica.Cpf,
                            Identidade = dependente.PessoaFisica.identidade,

                            NomeDaMae = dependente.NomeDaMae == null ? "" : dependente.NomeDaMae.ToString().ToUpper(),
                            CpfDaMae = dependente.PessoaFisica.CPFmae,
                            IdentidadedaMae = dependente.PessoaFisica.identidademae,

                            DataDeNascimento = dependente.PessoaFisica.DataDeNascimento.Value.ToString("dd/MM/yyyy"),
                            Sexo = L(string.Concat("Enum_SexoEnum_", dependente.PessoaFisica.Sexo)).ToString().ToUpper(),
                            Nacionalidade = dependente.PessoaFisica.Nacionalidade.ToString().ToUpper(),
                            NumeroDoCartaoNacionalDeSaude = dependente.NumeroDoCartaoNacionalDeSaude,
                            DeclaracaoDeNascidoVivo = dependente.DeclaracaoDeNascidoVivo,
                            GrauDeParentesco = L(string.Concat("Enum_GrauDeParentescoEnum_", dependente.GrauDeParentesco)).ToString().ToUpper()

                        };

                        if (dependente.PessoaFisica.EmailPrincipal != null)
                        {
                            dependenteInputV1.EmailPrincipal = dependente.PessoaFisica.EmailPrincipal.Endereco;
                        }

                        titularV1.Dependentes.Add(dependenteInputV1);
                    }
                }
            }
            #endregion

            var valorDaProposta = valorPorBeneficiarioPorIdadeV1.Sum(x => x.Valor);
            var taxaDeAdesao = chancelaV1 != null ? chancelaV1.TaxaDeAdesao : 0;
            var taxaDeAngariacao = valorDaProposta + taxaDeAdesao;

            var dataAceite = new DataAceite()
            {
                Dia = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(Convert.ToDateTime(proposta.DataHoraDaHomologacao).Day),
                Mes = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(TradutorMes(Convert.ToDateTime(proposta.DataHoraDaHomologacao).Month)),
                Ano = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(Convert.ToDateTime(proposta.DataHoraDaHomologacao).Year)
            };





            var resultado = new PropostaDeContratacaoDoRelatorioV1Input()
            {
                ValorDaProposta = valorDaProposta,
                Numero = proposta.Numero,
                FormaDeContratacao = L(string.Concat("Enum_FormaDeContratacaoEnum_", proposta.FormaDeContratacao)),
                TipoDeProposta = L(string.Concat("Enum_TipoDePropostaEnum_", proposta.TipoDeProposta)),
                TaxaDeAngariacao = taxaDeAngariacao,
                TitularEResponsavelLegal = proposta.TitularResponsavelLegal,
                InicioDeVigencia = proposta.InicioDeVigencia == null ? null : proposta.InicioDeVigencia.Value.ToString("dd/MM/yyyy"),

                Titular = titularV1,
                Produto = produtoV1,
                Profissao = profissaoV1,
                Corretor = corretorV1,
                Corretora = corretoraV1,
                Chancela = chancelaV1,
                Estado = estadoV1,
                ValoresPorPorBeneficiarioPorIdade = valorPorBeneficiarioPorIdadeV1,
                PerguntasDaDeclaracaoDeSaude = perguntasDaDeclaracaoDeSaudeV1,
                PerguntasDaDeclaracaoDeSaudePositivas = perguntasDaDeclaracaoDeSaudePositivaV1,
                PerguntasDePesoEAltura = perguntasPesoEAlturaV1,
                TermosEAceiteDaDeclaracaoDeSaude = itensDeDeclaracaoDoBeneficiarioV1,
                DataAceite = dataAceite,
                DataAceiteBeneficiario = proposta.DataHoraDoAceite == null ? null :  proposta.DataHoraDoAceite.Value.AddHours(-3).ToString("dd/MM/yyyy HH:mm:ss"),
                ProtocoloAceiteBeneficiario = proposta.DataHoraDoAceite == null ? null : proposta.DataHoraDoAceite.Value.ToString("001yyyyMMddHHmmss"),
                DataAceiteCorretor = proposta.DataAceiteCorretor == null ? null : proposta.DataAceiteCorretor.Value.AddHours(-3).ToString("dd/MM/yyyy HH:mm:ss"),
                ProtocoloAceiteCorretor = proposta.DataAceiteCorretor == null ? null : proposta.DataAceiteCorretor.Value.ToString("002yyyyMMddHHmmss"),
                DataAceiteAdm = proposta.DataHoraDaHomologacao == null ? null : proposta.DataHoraDaHomologacao.Value.AddHours(-3).ToString("dd/MM/yyyy HH:mm:ss"),
                ProtocoloAceiteAdm = proposta.DataHoraDaHomologacao == null ? null : proposta.DataHoraDaHomologacao.Value.ToString("003yyyyMMddHHmmss"),

            };

            return resultado;
        }

        private void SetPassoAceite(PropostaDeContratacao proposta)
        {
            if (proposta.PassoDaProposta >= PassoDaPropostaEnum.AceiteDoContrato)
            {
                proposta.StatusDaProposta = StatusDaPropostaEnum.Pendente;
                proposta.PassoDaProposta = PassoDaPropostaEnum.AceiteDoContrato;
                proposta.DataAceiteCorretor = null;
                proposta.DataHoraDoAceite = null;
                proposta.Aceite = false;
                proposta.AceiteCorretor = false;
            }
        }


        private void SetPassoDeclaracaoSaude(PropostaDeContratacao proposta)
        {
            if (proposta.PassoDaProposta >= PassoDaPropostaEnum.DadosGerenciais)
            {
                proposta.StatusDaProposta = StatusDaPropostaEnum.Pendente;
                proposta.PassoDaProposta = PassoDaPropostaEnum.DadosDeclaracaoDeSaude;
                proposta.DataAceiteCorretor = null;
                proposta.DataHoraDoAceite = null;
                proposta.Aceite = false;
                proposta.AceiteCorretor = false;
            }
        }

        private string TradutorMes(int mes)
        {
            string DescricaoMes = string.Empty;

            switch (mes)
            {
                case 1:
                    DescricaoMes = "Janeiro";
                    break;
                case 2:
                    DescricaoMes = "Fevereiro";
                    break;
                case 3:
                    DescricaoMes = "Março";
                    break;
                case 4:
                    DescricaoMes = "Abril";
                    break;
                case 5:
                    DescricaoMes = "Maio";
                    break;
                case 6:
                    DescricaoMes = "Junho";
                    break;
                case 7:
                    DescricaoMes = "Julho";
                    break;
                case 8:
                    DescricaoMes = "Agosto";
                    break;
                case 9:
                    DescricaoMes = "Setembro";
                    break;
                case 10:
                    DescricaoMes = "Outubro";
                    break;
                case 11:
                    DescricaoMes = "Novembro";
                    break;
                case 12:
                    DescricaoMes = "Dezembro";
                    break;
            }

            return DescricaoMes;
        }


        [Obsolete]
        public async Task<FormularioDeAssociacaoInput> MontaFormularioAssociacao(IdInput input)
        {
            var proposta = await Repository.GetAsync(input.Id);

            for (var i = 0; i <= proposta.Titular.Dependentes.Count - 1; i++)
            {
                var dependente = proposta.Titular.Dependentes.ToList()[i];
            }

            var chancela = proposta.ChancelaId.HasValue ? await _chancelaService.GetById(proposta.ChancelaId.Value) : null;

            var dataAceite = new DataAceite()
            {
                Dia = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(Convert.ToDateTime(proposta.DataHoraDoAceite).Day),
                Mes = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(TradutorMes(Convert.ToDateTime(proposta.DataHoraDoAceite).Month)),
                Ano = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(Convert.ToDateTime(proposta.DataHoraDoAceite).Year)
            };

            var formulario = new FormularioDeAssociacaoInput()
            {
                Proposta = proposta.MapTo<PropostaDeContratacaoInput>(),
                Chancela = chancela.MapTo<ChancelaInput>(),
                EstadoNome = proposta.Estado.Nome,
                PathLogoAssociacao = proposta.ChancelaId.HasValue ? (proposta.Chancela.Associacao != null && proposta.Chancela.Associacao.Imagem != null) ? proposta.Chancela.Associacao.Imagem.Path : "https://www.theclementimall.com/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png" : "https://www.theclementimall.com/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png",
                Instrucoes = proposta.ChancelaId.HasValue ? proposta.Chancela.Associacao.Instrucoes : string.Empty,
                DataAceite = dataAceite,
                numero = proposta.Numero

            };

            return formulario;
        }

        public async Task<FichaDoRelatorioV1Input> MontaFichaDoRelatorioV1(IdInput input) // S
        {
            var proposta = await Repository.GetAsync(input.Id);

            #region Chancela
            ChancelaV1Input chancelaV1 = null;
            if (proposta.Chancela != null)
            {
                chancelaV1 = new ChancelaV1Input()
                {
                    Nome = proposta.Chancela.Nome,
                    TaxaDeAdesao = proposta.Chancela.TaxaDeAdesao,
                    TaxaMensal = proposta.Chancela.TaxaMensal
                };
            }
            #endregion

            #region Estado
            EstadoV1Input estadoV1 = null;
            if (proposta.Estado != null)
            {
                estadoV1 = new EstadoV1Input()
                {
                    Nome = proposta.Estado.Nome
                };
            }
            #endregion

            #region Corretora
            CorretoraV1Input corretoraV1 = null;
            if (proposta.Corretora != null)
            {
                corretoraV1 = new CorretoraV1Input()
                {
                    Nome = proposta.Corretora.Nome
                };
                if (proposta.Corretora.PessoaJuridica.TelefoneComercial != null)
                {
                    corretoraV1.Telefone = proposta.Corretora.PessoaJuridica.TelefoneComercial.Tostring();
                }
            }
            #endregion

            #region Corretor
            CorretorV1Input corretorV1 = null;
            if (proposta.Corretor != null)
            {

                corretorV1 = new CorretorV1Input()
                {
                    Nome = proposta.Corretor.NomePessoa.ToString().ToUpper()
                };

                if (proposta.AceiteCorretor && proposta.Corretor.PessoaFisica.Documentos != null)
                {
                    corretorV1.AssinaturaCurta = proposta.Corretor.NomePessoa.ToString().ToUpper();
                }
                if (proposta.Corretor.PessoaFisica.TelefoneCelular != null)
                {
                    corretorV1.Telefone = proposta.Corretor.PessoaFisica.TelefoneCelular.Tostring();
                }
                if (proposta.Corretor.PessoaFisica.DocumentoPrincipal != null)
                {
                    corretorV1.CPF = proposta.Corretor.PessoaFisica.DocumentoPrincipal.Numero;
                }
                if (proposta.Corretor.PessoaFisica.Cpf != null)
                {
                    corretorV1.CPF = proposta.Corretor.PessoaFisica.Cpf;
                }
                if (proposta.Corretor.PessoaFisica.EmailPrincipal != null)
                {
                    corretorV1.Email = proposta.Corretor.PessoaFisica.EmailPrincipal.Endereco;
                }
            }
            #endregion

            #region Profissão
            ProfissaoV1Input profissaoV1 = null;
            if (proposta.Profissao != null)
            {
                profissaoV1 = new ProfissaoV1Input()
                {
                    Nome = proposta.Profissao.Titulo.ToString().ToUpper()
                };
            }
            #endregion

            #region Produto, Adminitradora e Operadora
            ProdutoDePlanoDeSaudeV1Input produtoV1 = null;
            if (proposta.Produto != null)
            {
                produtoV1 = new ProdutoDePlanoDeSaudeV1Input()
                {
                    Nome = proposta.Produto.Nome,
                    Descricao = proposta.Produto.Descricao,
                    LinkRedeCredenciada = proposta.Produto.LinkRedeCredenciada,
                    CartaDeOrientacao = proposta.Produto.CartaDeOrientacao,
                    NumeroDeRegistro = proposta.Produto.PlanoDeSaude.NumeroDeRegistro,
                    Reembolso = proposta.Produto.PlanoDeSaude.Reembolso,
                    DescricaoDoReembolso = proposta.Produto.PlanoDeSaude.DescricaoDoReembolso,
                    Abrangencia = L(string.Concat("Enum_AbrangenciaDoFeriadoEnum_", proposta.Produto.PlanoDeSaude.Abrangencia)),
                    SegmentacaoAssistencial = L(string.Concat("Enum_SegmentacaoAssistencialDoPlanoEnum_", proposta.Produto.PlanoDeSaude.SegmentacaoAssistencial)),
                    Acomodacao = L(string.Concat("Enum_AcomodacaoEnum_", proposta.Produto.PlanoDeSaude.Acomodacao))
                };

                if (proposta.Produto.Administradora != null)
                {
                    var administradora = new AdministradoraV1Input()
                    {
                        Nome = proposta.Produto.Administradora.NomePessoa
                    };
                    if (proposta.Produto.Administradora.PessoaJuridica.DocumentoPrincipal != null)
                    {
                        administradora.CNPJ = proposta.Produto.Administradora.PessoaJuridica.DocumentoPrincipal.Numero;
                    }
                    produtoV1.Administradora = administradora;
                }

                if (proposta.Produto.Operadora != null)
                {
                    var operadora = new OperadoraV1Input()
                    {
                        Nome = proposta.Produto.Operadora.NomePessoa
                    };
                    produtoV1.Operadora = operadora;
                }

                if (proposta.Produto.RelatorioProspostaDeContratacaoId.HasValue)
                {
                    produtoV1.RelatorioProspostaDeContratacaoId = proposta.Produto.RelatorioProspostaDeContratacaoId.Value;
                }

                if (proposta.Produto.RelatorioFichaDeEntidadeId.HasValue)
                {
                    produtoV1.RelatorioFichaDeEntidadeId = proposta.Produto.RelatorioFichaDeEntidadeId.Value;
                }
            }
            #endregion

            #region Titular, Endereço, Dependentes e o Responsável
            TitularV1Input titularV1 = null;
            if (proposta.Titular != null)
            {
                titularV1 = new TitularV1Input()
                {
                    Nome = proposta.Titular.PessoaFisica.Nome.ToUpper(),
                    Idade = PessoaHelpers.GetIdade(proposta.Titular.PessoaFisica.DataDeNascimento.Value),
                    EstadoCivil = L(string.Concat("Enum_EstadoCivilEnum_", proposta.Titular.PessoaFisica.EstadoCivil)).ToString().ToUpper(),
                    DataDeNascimento = proposta.Titular.PessoaFisica.DataDeNascimento.Value.ToString("dd/MM/yyyy"),
                    Sexo = L(string.Concat("Enum_SexoEnum_", proposta.Titular.PessoaFisica.Sexo)).ToString().ToUpper(),
                    Nacionalidade = proposta.Titular.PessoaFisica.Nacionalidade,
                    NomeDaMae = proposta.Titular.NomeDaMae,
                    NumeroDoCartaoNacionalDeSaude = proposta.Titular.NumeroDoCartaoNacionalDeSaude,
                    DeclaracaoDeNascidoVivo = proposta.Titular.DeclaracaoDeNascidoVivo,
                    Identidade = proposta.Titular.PessoaFisica.Rg,
                    OrgaoExpedidorDaIdentidade = proposta.Titular.PessoaFisica.OrgaoExpedidor,
                    Cpf = proposta.Titular.PessoaFisica.Cpf
                    //DebitoContaSim = proposta.Titular.DebitoConta == true ? "X" : "",
                    //DebitoContaNao = proposta.Titular.DebitoConta == false ? "X" : "",
                    //FolhaFichaSim = proposta.Titular.FolhaFicha == true ? "X" : "",
                    //FolhaFichaNao = proposta.Titular.FolhaFicha == false ? "X" : ""
                };


                if (proposta.Titular.matricula == null)
                {
                    titularV1.Boleto = string.Empty;
                    titularV1.DescontoFolha = string.Empty;
                    titularV1.DebitoContaSim = string.Empty;
                    titularV1.DebitoContaNao = string.Empty;
                    titularV1.FolhaFichaSim = string.Empty;
                    titularV1.FolhaFichaNao = string.Empty;
                    titularV1.Matricula = string.Empty;
                }
                else
                {
                    titularV1.Boleto = proposta.Titular.boleto == true ? "X" : string.Empty;
                    titularV1.DescontoFolha = proposta.Titular.folha == true ? "X" : string.Empty;
                    titularV1.DebitoContaSim = proposta.Titular.DebitoConta == true ? "X" : string.Empty;
                    titularV1.DebitoContaNao = proposta.Titular.DebitoConta == false ? "X" : string.Empty;
                    titularV1.FolhaFichaSim = proposta.Titular.FolhaFicha == true ? "X" : string.Empty;
                    titularV1.FolhaFichaNao = proposta.Titular.FolhaFicha == false ? "X" : string.Empty;
                    titularV1.Matricula = proposta.Titular.matricula;
                }


                if (proposta.Aceite && proposta.Titular.PessoaFisica?.Documentos != null)
                {
                    if (proposta.TitularResponsavelLegal)
                        titularV1.AssinaturaCurta = proposta.Titular.NomePessoa.ToUpper();
                    else
                        titularV1.AssinaturaCurta = proposta.Titular.Responsavel.NomePessoa.ToUpper();
                }
                else
                {
                    titularV1.AssinaturaCurta = "Assinatura ausente. Aceite não encontrado";
                }


                if (proposta.Titular.PessoaFisica.EmailPrincipal != null)
                {
                    titularV1.EmailPrincipal = proposta.Titular.PessoaFisica.EmailPrincipal.Endereco;
                }

                if (proposta.Titular.PessoaFisica.TelefoneCelular != null)
                {
                    titularV1.TelefoneCelular = string.Concat("(", proposta.Titular.PessoaFisica.TelefoneCelular.DDD,
                        ")", proposta.Titular.PessoaFisica.TelefoneCelular.Numero);
                }

                if (proposta.Titular.PessoaFisica.EnderecoPrincipal != null)
                {
                    titularV1.Endereco = new EnderecoV1Input()
                    {
                        Logradouro = proposta.Titular.PessoaFisica.EnderecoPrincipal.Logradouro.ToString().ToUpper(),
                        Numero = proposta.Titular.PessoaFisica.EnderecoPrincipal.Numero,
                        Complemento = proposta.Titular.PessoaFisica.EnderecoPrincipal.ToString().ToUpper(),
                        Bairro = proposta.Titular.PessoaFisica.EnderecoPrincipal.Bairro.ToString().ToUpper(),
                        CEP = proposta.Titular.PessoaFisica.EnderecoPrincipal.CEP,
                        Referencia = proposta.Titular.PessoaFisica.EnderecoPrincipal.Referencia == null ? null : proposta.Titular.PessoaFisica.EnderecoPrincipal.Referencia,
                        Estado = proposta.Titular.PessoaFisica.EnderecoPrincipal.Cidade.Estado.Nome.ToString().ToUpper(),
                        Cidade = proposta.Titular.PessoaFisica.EnderecoPrincipal.Cidade.Nome.ToString().ToUpper(),
                        UF = proposta.Titular.PessoaFisica.EnderecoPrincipal.Cidade.Estado.Sigla.ToString().ToUpper(),
                        LogradouroComNumeroEComplemento = string.Concat(proposta.Titular.PessoaFisica.EnderecoPrincipal.Logradouro, " ",
                            proposta.Titular.PessoaFisica.EnderecoPrincipal.Numero, " ", proposta.Titular.PessoaFisica.EnderecoPrincipal.Complemento).ToString().ToUpper()
                    };
                }
                else
                {
                    titularV1.Endereco = new EnderecoV1Input()
                    {
                        Logradouro = proposta.Titular.PessoaFisica.Enderecos.First().Logradouro.ToString().ToUpper(),
                        Numero = proposta.Titular.PessoaFisica.Enderecos.First().Numero,
                        Complemento = proposta.Titular.PessoaFisica.Enderecos.First().Complemento.ToString().ToUpper(),
                        Bairro = proposta.Titular.PessoaFisica.Enderecos.First().Bairro.ToString().ToUpper(),
                        CEP = proposta.Titular.PessoaFisica.Enderecos.First().CEP,
                        Referencia = proposta.Titular.PessoaFisica.Enderecos.First().Referencia == null ? null : proposta.Titular.PessoaFisica.Enderecos.First().Referencia.ToString().ToUpper(),
                        Estado = proposta.Titular.PessoaFisica.Enderecos.First().Cidade.Estado.Nome.ToString().ToUpper(),
                        Cidade = proposta.Titular.PessoaFisica.Enderecos.First().Cidade.Nome.ToString().ToUpper(),
                        UF = proposta.Titular.PessoaFisica.Enderecos.First().Cidade.Estado.Sigla.ToString().ToUpper(),
                        LogradouroComNumeroEComplemento = string.Concat(proposta.Titular.PessoaFisica.Enderecos.First().Logradouro, " ",
                            proposta.Titular.PessoaFisica.Enderecos.First().Numero, " ", proposta.Titular.PessoaFisica.Enderecos.First().Complemento).ToString().ToUpper()
                    };


                }

                if (proposta.Titular.Responsavel != null)
                {
                    titularV1.Responsavel = new ResponsavelV1Input()
                    {
                        Nome = proposta.Titular.Responsavel.NomePessoa,
                        TipoDeResponsavel = L(string.Concat("Enum_TipoDeResponsavelEnum_", proposta.Titular.Responsavel.TipoDeResponsavel)),
                        Cpf = proposta.Titular.Responsavel.PessoaFisica.Cpf,
                        EstadoCivil = L(string.Concat("Enum_EstadoCivilEnum_", proposta.Titular.Responsavel.PessoaFisica.EstadoCivil)),
                        DataDeNascimento = proposta.Titular.Responsavel.PessoaFisica.DataDeNascimento.Value.ToString("dd/MM/yyyy"),
                        Sexo = L(string.Concat("Enum_SexoEnum_", proposta.Titular.Responsavel.PessoaFisica.Sexo)),
                        Nacionalidade = proposta.Titular.Responsavel.PessoaFisica.Nacionalidade,
                    };

                    if (proposta.Titular.Responsavel.PessoaFisica.TelefoneCelular != null)
                    {
                        titularV1.Responsavel.TelefoneCelular = string.Concat("(", proposta.Titular.Responsavel.PessoaFisica.TelefoneCelular.DDD,
                            ")", proposta.Titular.Responsavel.PessoaFisica.TelefoneCelular.Numero);
                    }

                    if (proposta.Titular.Responsavel.PessoaFisica.EmailPrincipal != null)
                    {
                        titularV1.Responsavel.EmailPrincipal = proposta.Titular.Responsavel.PessoaFisica.EmailPrincipal.Endereco;
                    }
                }

                if (proposta.Titular.Dependentes?.Count > 0)
                {
                    titularV1.Dependentes = new List<DependenteV1Input>();
                    foreach (var dependente in proposta.Titular.Dependentes)
                    {
                        var dependenteInputV1 = new DependenteV1Input()
                        {
                            Nome = dependente.NomePessoa.ToString().ToUpper(),
                            NomeDaMae = dependente.NomeDaMae == null ? "" : dependente.NomeDaMae.ToString().ToUpper(),
                            Cpf = dependente.PessoaFisica.Cpf,
                            DataDeNascimento = dependente.PessoaFisica.DataDeNascimento.Value.ToString("dd/MM/yyyy"),
                            Sexo = L(string.Concat("Enum_SexoEnum_", dependente.PessoaFisica.Sexo)).ToString().ToUpper(),
                            Nacionalidade = dependente.PessoaFisica.Nacionalidade.ToString().ToUpper(),
                            NumeroDoCartaoNacionalDeSaude = dependente.NumeroDoCartaoNacionalDeSaude,
                            DeclaracaoDeNascidoVivo = dependente.DeclaracaoDeNascidoVivo,
                            GrauDeParentesco = L(string.Concat("Enum_GrauDeParentescoEnum_", dependente.GrauDeParentesco)).ToString().ToUpper()
                        };

                        if (dependente.PessoaFisica.EmailPrincipal != null)
                        {
                            dependenteInputV1.EmailPrincipal = dependente.PessoaFisica.EmailPrincipal.Endereco;
                        }

                        titularV1.Dependentes.Add(dependenteInputV1);
                    }
                }
            }
            #endregion


            var dataAceite = new DataAceite()
            {
                Dia = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(Convert.ToDateTime(proposta.DataHoraDaHomologacao).Day),
                Mes = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(TradutorMes(Convert.ToDateTime(proposta.DataHoraDaHomologacao).Month)),
                Ano = proposta.DataHoraDoAceite == null ? "" : Convert.ToString(Convert.ToDateTime(proposta.DataHoraDaHomologacao).Year)
            };


            var resultado = new FichaDoRelatorioV1Input()
            {
                TitularEResponsavelLegal = proposta.TitularResponsavelLegal,

                Titular = titularV1,
                Produto = produtoV1,
                Profissao = profissaoV1,
                Corretor = corretorV1,
                Corretora = corretoraV1,
                Chancela = chancelaV1,
                Estado = estadoV1,
                DataAceite = dataAceite,
                numero = proposta.Numero
            };

            return resultado;
        }

        private async Task CreateSnapshot(PropostaDeContratacaoInput input)
        {
            string jsonSnapshot = JsonConvert.SerializeObject(input);
            var snapShotExistente = await _snapshotService.GetAll().Where(x => x.PropostaDeContratacaoId == input.Id).FirstOrDefaultAsync();
            if (snapShotExistente != null)
            {
                await _snapshotService.Delete(snapShotExistente.Id);
            }
            var snapShot = new SnapshotPropostaDeContratacao()
            {
                PropostaDeContratacaoId = input.Id,
                Json = Encoding.ASCII.GetBytes(jsonSnapshot)
            };
            await _snapshotService.CreateEntity(snapShot);
        }
    }
}
