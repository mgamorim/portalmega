﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class CorretoraAppService : EZControlAppServiceBase<Corretora>, ICorretoraAppService
    {
        private readonly ICorretoraService _corretoraService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IEmpresaService _empresaService;
        private readonly IParametroPagSeguroService _parametroPagSeguroService;
        private readonly IDepositoTransferenciaBancariaService _depositoTransferenciaBancariaService;
        private readonly IArquivoGlobalService _arquivoGlobalService;
        private readonly ICorretorService _corretorService;
        private readonly IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService _permissaoDeVendaDePlanoDeSaudeParaCorretoraService;

        public CorretoraAppService(IRepository<Corretora> corretoraRepository,
                                          ICorretoraService corretoraService,
                                          IPessoaJuridicaService pessoaJuridicaService,
                                          IEmpresaService empresaService,
                                          IParametroPagSeguroService parametroPagSeguroService,
                                          IDepositoTransferenciaBancariaService depositoTransferenciaBancariaService,
                                          IArquivoGlobalService arquivoGlobalService,
                                          ICorretorService corretorService,
                                          IPermissaoDeVendaDePlanoDeSaudeParaCorretoraService permissaoDeVendaDePlanoDeSaudeParaCorretoraService)
            : base(corretoraRepository)
        {
            _corretoraService = corretoraService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _empresaService = empresaService;
            _parametroPagSeguroService = parametroPagSeguroService;
            _depositoTransferenciaBancariaService = depositoTransferenciaBancariaService;
            _arquivoGlobalService = arquivoGlobalService;
            _corretorService = corretorService;
            _permissaoDeVendaDePlanoDeSaudeParaCorretoraService = permissaoDeVendaDePlanoDeSaudeParaCorretoraService;
        }

        private void ValidateInput(CorretoraInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (input.Id == default(int))
                {
                    var existePessoaJuridicaCadastrada = _corretoraService.GetAll().Any(x => x.PessoaJuridicaId == input.PessoaJuridicaId);

                    if (existePessoaJuridicaCadastrada)
                        validationErrors.Add(new ValidationResult(L("Corretora.ExistePessoaJuridicaAssociadaACorretora")));
                }

                if (input.PessoaJuridicaId == default(int))
                    validationErrors.Add(new ValidationResult(L("Corretora.PessoaEmpty")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<dynamic> GetImgById(int ImagemId)
        {
            try
            {
                var imagem = await _arquivoGlobalService.GetById(ImagemId);
                return imagem;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        private async Task ProcessInput(Corretora corretora, CorretoraInput corretoraInput)
        {
            //Imagem
            if (corretoraInput.ImagemId > 0)
            {
                try
                {
                    var imagem = await _arquivoGlobalService.GetById(corretoraInput.ImagemId);
                    if (imagem != null)
                    {
                        corretora.Imagem = imagem;
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (corretoraInput.PessoaJuridicaId > 0)
            {
                try
                {
                    var pessoaJuridica = await _pessoaJuridicaService.GetById(corretoraInput.PessoaJuridicaId);
                    if (pessoaJuridica != null)
                    {
                        corretora.PessoaJuridica = pessoaJuridica;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Corretora.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            if (corretoraInput.CorretorId > 0)
            {
                try
                {
                    var corretor = await _corretorService.GetById(corretoraInput.CorretorId);
                    if (corretor != null)
                    {
                        corretora.Corretor = corretor;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Corretor.CorretorNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretora_Create, AppPermissions.Pages_Tenant_EZLiv_Corretora_Edit)]
        public async Task<IdInput> Save(CorretoraInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, CorretoraInput>(_corretoraService, input, () => ValidateInput(input), ProcessInput);
                await SaveParametroPagSeguro(input.ParametroPagSeguro);
                await SaveDepositoTransferenciaBancaria(input.DepositoTransferenciaBancaria);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, CorretoraInput>(_corretoraService, input, () => ValidateInput(input), ProcessInput);
                await SaveParametroPagSeguro(input.ParametroPagSeguro);
                await SaveDepositoTransferenciaBancaria(input.DepositoTransferenciaBancaria);
            }
            return result;
        }


        private async Task SaveParametroPagSeguro(ParametroPagSeguroInput parametroPagseguroInput)
        {
            if (parametroPagseguroInput != null)
            {
                var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(parametroPagseguroInput.PessoaJuridicaId);

                if (parametroPagSeguro == null)
                {
                    parametroPagSeguro = new ParametroPagSeguro();
                    parametroPagSeguro.PessoaJuridicaId = parametroPagseguroInput.PessoaJuridicaId;
                }

                parametroPagSeguro.Token = parametroPagseguroInput.Token;
                parametroPagSeguro.Email = parametroPagseguroInput.Email;
                parametroPagSeguro.AppId = parametroPagseguroInput.AppId;
                parametroPagSeguro.AppKey = parametroPagseguroInput.AppKey;

                await _parametroPagSeguroService.SaveParametroPagSeguro(parametroPagSeguro);
            }
        }

        private async Task SaveDepositoTransferenciaBancaria(DepositoTransferenciaBancariaInput depositoTransferenciaBancariaInput)
        {
            if (depositoTransferenciaBancariaInput != null)
            {
                var depositoTransferenciaBancaria = _depositoTransferenciaBancariaService.GetDepositoBancarioById(depositoTransferenciaBancariaInput.Id);

                if (depositoTransferenciaBancaria == null)
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretora_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    //var empresa = await _empresaService.GetEmpresaByPessoaJuridicaId(depositoTransferenciaBancariaInput.PessoaJuridicaId);

                    depositoTransferenciaBancaria = new DepositoTransferenciaBancaria();
                    depositoTransferenciaBancaria.Banco = depositoTransferenciaBancariaInput.Banco;
                    depositoTransferenciaBancaria.Agencia = depositoTransferenciaBancariaInput.Agencia;
                    depositoTransferenciaBancaria.Conta = depositoTransferenciaBancariaInput.Conta;
                    depositoTransferenciaBancaria.Titular = depositoTransferenciaBancariaInput.Titular;
                    depositoTransferenciaBancaria.PessoaJuridicaId = depositoTransferenciaBancariaInput.PessoaJuridicaId;
                    depositoTransferenciaBancaria.Comprovante = depositoTransferenciaBancariaInput.Comprovante;
                    await _depositoTransferenciaBancariaService.SaveDepositoBancario(depositoTransferenciaBancaria);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretora_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    depositoTransferenciaBancaria.Banco = depositoTransferenciaBancariaInput.Banco;
                    depositoTransferenciaBancaria.Agencia = depositoTransferenciaBancariaInput.Agencia;
                    depositoTransferenciaBancaria.Conta = depositoTransferenciaBancariaInput.Conta;
                    depositoTransferenciaBancaria.Titular = depositoTransferenciaBancariaInput.Titular;
                    depositoTransferenciaBancaria.IsActive = depositoTransferenciaBancariaInput.IsActive;
                    depositoTransferenciaBancaria.Comprovante = depositoTransferenciaBancariaInput.Comprovante;
                    await _depositoTransferenciaBancariaService.UpdateEntity(depositoTransferenciaBancaria);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretora_Create, AppPermissions.Pages_Tenant_EZLiv_Corretora_Edit)]
        public async Task<CorretoraInput> SaveAndReturnEntity(CorretoraInput input)
        {
            CorretoraInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<CorretoraInput, CorretoraInput>(_corretoraService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Corretora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<CorretoraInput, CorretoraInput>(_corretoraService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<CorretoraListDto>> GetPaginado(GetCorretoraInput input)
        {
            var condicoes = new List<WhereIfCondition<Corretora>>
            {
                 new WhereIfCondition<Corretora>(
                    !string.IsNullOrEmpty(input.NomeFantasia) ||
                    !string.IsNullOrEmpty(input.RazaoSocial),
                    a =>
                        a.PessoaJuridica.NomeFantasia.Contains(input.NomeFantasia) ||
                        a.PessoaJuridica.RazaoSocial.Contains(input.RazaoSocial)),
                new WhereIfCondition<Corretora>(
                    input.Ids != null && input.Ids.Count() > 0,
                    a => !input.Ids.Contains(a.Id))
            };
            var result = await base.GetListPaged<CorretoraListDto, GetCorretoraInput>(_corretoraService, input, condicoes);
            return result;
        }

        public async Task<CorretoraInput> GetById(IdInput input)
        {
            try
            {
                var corretoraInput = await base.GetEntityById<CorretoraInput, IdInput>(_corretoraService, input);
                var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(corretoraInput.PessoaJuridicaId);
                corretoraInput.ParametroPagSeguro = parametroPagSeguro.MapTo<ParametroPagSeguroInput>();
                //var empresaObj = await _empresaService.GetEmpresaByPessoaJuridicaId(corretoraInput.PessoaJuridicaId);
                var depositoTransferenciaBancaria = _depositoTransferenciaBancariaService.GetDepositoBancarioByPessoaJuridicaId(corretoraInput.PessoaJuridicaId);

                corretoraInput.DepositoTransferenciaBancaria = new DepositoTransferenciaBancariaInput();
                if (depositoTransferenciaBancaria != null)
                {
                    corretoraInput.DepositoTransferenciaBancaria.Id = depositoTransferenciaBancaria.Id;
                    corretoraInput.DepositoTransferenciaBancaria.Banco = depositoTransferenciaBancaria.Banco;
                    corretoraInput.DepositoTransferenciaBancaria.Agencia = depositoTransferenciaBancaria.Agencia;
                    corretoraInput.DepositoTransferenciaBancaria.Conta = depositoTransferenciaBancaria.Conta;
                    corretoraInput.DepositoTransferenciaBancaria.Titular = depositoTransferenciaBancaria.Titular;
                    corretoraInput.DepositoTransferenciaBancaria.EmpresaId = depositoTransferenciaBancaria.EmpresaId;
                    corretoraInput.DepositoTransferenciaBancaria.PessoaJuridicaId = depositoTransferenciaBancaria.PessoaJuridicaId;
                    corretoraInput.DepositoTransferenciaBancaria.Comprovante = depositoTransferenciaBancaria.Comprovante;
                }
                //corretoraInput.DepositoTransferenciaBancaria = depositoTransferenciaBancaria.MapTo<DepositoTransferenciaBancariaInput>();
                //var corretor = _corretorService.GetById(corretoraInput.CorretorId);
                //corretoraInput.Corretor = corretor;
                return corretoraInput;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Corretora_Delete)]
        public async Task Delete(IdInput input)
        {
            try
            {
                _corretoraService.CanDelete(input.Id);
                var corretora = _corretoraService.GetAll().FirstOrDefault(x => x.Id == input.Id);
                var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(corretora.PessoaJuridicaId);
                if (parametroPagSeguro != null)
                    await _parametroPagSeguroService.Delete(x => x.Id == parametroPagSeguro.Id);
                await base.DeleteEntity(_corretoraService, input);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<CorretoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<CorretoraInput, IdInput>(_corretoraService, input);
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForCorretora(GetPessoaExceptForCorretora input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var idsPessoaJuridica = _corretoraService.GetAll().Select(y => y.PessoaJuridicaId);

                var queryPessoaJuridica = _pessoaJuridicaService
                    .GetAll()
                    .Where(x => !idsPessoaJuridica.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                    .Select(x => new PessoaJuridicaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.NomeFantasia,
                        NomeFantasia = x.NomeFantasia,
                        RazaoSocial = x.RazaoSocial,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Juridica
                    });

                IQueryable<PessoaListDto> query = queryPessoaJuridica;

                var count = await query.CountAsync();

                List<PessoaListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PagedResultDto<CorretoraListDto>> GetPaginadoCorretorasExceptForIds(GetCorretoraInput input)
        {
            var condicoes = new List<WhereIfCondition<Domain.EZLiv.SubtiposPessoa.Corretora>>
            {
                new WhereIfCondition<Domain.EZLiv.SubtiposPessoa.Corretora>(
                    input.Ids.Length > 0,
                    a => !input.Ids.Contains(a.Id))
            };

            var result =
                await
                    base.GetListPaged<CorretoraListDto, GetCorretoraInput>(_corretoraService, input,
                        condicoes);
            return result;
        }

        [AbpAllowAnonymous]
        public async Task<List<string>> GetCorretora(GetCorretoraInput input)
        {
            return _corretoraService.GetCorretorasByName(input.Nome);
        }
    }
}
