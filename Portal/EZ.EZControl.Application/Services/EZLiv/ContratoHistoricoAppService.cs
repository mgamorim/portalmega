﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.ContratoHistorico;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ContratoHistoricoAppService : EZControlAppServiceBase<ContratoHistorico>, IContratoHistoricoAppService
    {
        private readonly IContratoHistoricoService _contratoHistoricoService;
        private readonly IContratoService _contratoService;
        private readonly IProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;

        public ContratoHistoricoAppService(IRepository<ContratoHistorico> contratoHistoricoRepository,
            IContratoHistoricoService contratoHistoricoService,
            IContratoService contratoService,
            IProdutoDePlanoDeSaudeService produtoDePlanoDeSaudeService)
            : base(contratoHistoricoRepository)
        {
            this._contratoHistoricoService = contratoHistoricoService;
            this._contratoService = contratoService;
            this._produtoDePlanoDeSaudeService = produtoDePlanoDeSaudeService;
        }

        private void ValidateInput(ContratoHistoricoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.ProdutoDePlanoDeSaudeId == default(int))
                validationErrors.Add(new ValidationResult(L("ContratoHistorico.EmptyProdutoDePlanoDeSaude")));
            if (input.ContratoId == default(int))
                validationErrors.Add(new ValidationResult(L("ContratoHistorico.EmptyContrato")));
            if (input.UserId == default(int))
                validationErrors.Add(new ValidationResult(L("ContratoHistorico.EmptyUsuario")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ContratoHistorico contratoHistorico, ContratoHistoricoInput input)
        {
            #region Contrato
            if (input.ContratoId > 0)
            {
                Contrato contrato = null;

                try
                {
                    contrato = await _contratoService.GetById(input.ContratoId);

                    contratoHistorico.AssociarContrato(contrato, _contratoHistoricoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion

            #region Produto
            if (input.ContratoId > 0)
            {
                ProdutoDePlanoDeSaude produto = null;

                try
                {
                    produto = await _produtoDePlanoDeSaudeService.GetById(input.ProdutoDePlanoDeSaudeId);

                    contratoHistorico.AssociarProdutoDePlanoDeSaude(produto, _contratoHistoricoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Create, AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Edit)]
        public async Task<IdInput> Save(ContratoHistoricoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ContratoHistoricoInput>(_contratoHistoricoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ContratoHistoricoInput>(_contratoHistoricoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Create, AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Edit)]
        public async Task<ContratoHistoricoInput> SaveAndReturnEntity(ContratoHistoricoInput input)
        {
            ContratoHistoricoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ContratoHistoricoInput, ContratoHistoricoInput>(_contratoHistoricoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ContratoHistoricoInput, ContratoHistoricoInput>(_contratoHistoricoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ContratoHistoricoListDto>> GetContratoHistoricoPaginado(GetContratoHistoricoInput input)
        {
            var condicoes = new List<WhereIfCondition<ContratoHistorico>>
            {
                new WhereIfCondition<ContratoHistorico>(input.ContratoId > 0, r => r.ContratoId == input.ContratoId)
            };
            var result = await base.GetListPaged<ContratoHistoricoListDto, GetContratoHistoricoInput>(_contratoHistoricoService, input, condicoes);
            return result;
        }

        public async Task<ContratoHistoricoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ContratoHistoricoInput, IdInput>(_contratoHistoricoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ContratoHistorico_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_contratoHistoricoService, input);
        }

        public async Task<ContratoHistoricoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ContratoHistoricoInput, IdInput>(_contratoHistoricoService, input);
        }
    }
}
