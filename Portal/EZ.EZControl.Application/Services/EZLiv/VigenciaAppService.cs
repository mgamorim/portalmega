﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Vigencia, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class VigenciaAppService : EZControlAppServiceBase<Vigencia>, IVigenciaAppService
    {
        private readonly IVigenciaService _vigenciaService;
        private readonly IProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;
        public VigenciaAppService(IRepository<Vigencia> vigenciaRepository,
                                  IProdutoDePlanoDeSaudeService produtoDePlanoDeSaudeService,
                                  IVigenciaService vigenciaService)
            : base(vigenciaRepository)
        {
            this._vigenciaService = vigenciaService;
            this._produtoDePlanoDeSaudeService = produtoDePlanoDeSaudeService;
        }

        private void ValidateInput(VigenciaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.DataInicialDaProposta == 0)
                validationErrors.Add(new ValidationResult(L("Vigencia.DataDePropostaInicialRange")));

            if (input.DataFinalDaProposta == 0)
                validationErrors.Add(new ValidationResult(L("Vigencia.DataDePropostaFinalRange")));

            if (input.DataDeFechamento == 0)
                validationErrors.Add(new ValidationResult(L("Vigencia.DataDeFechamentoRange")));

            if (input.DataDeVigencia == 0)
                validationErrors.Add(new ValidationResult(L("Vigencia.DataDeVigenciaRange")));

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Create, AppPermissions.Pages_Tenant_EZLiv_Vigencia_Edit)]
        public async Task<IdInput> Save(VigenciaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, VigenciaInput>(_vigenciaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, VigenciaInput>(_vigenciaService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Create, AppPermissions.Pages_Tenant_EZLiv_Vigencia_Edit)]
        public async Task<VigenciaInput> SaveAndReturnEntity(VigenciaInput input)
        {
            VigenciaInput vigencia = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                vigencia = await base.CreateAndReturnEntity<VigenciaInput, VigenciaInput>(_vigenciaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                vigencia = await base.UpdateAndReturnEntity<VigenciaInput, VigenciaInput>(_vigenciaService, input, () => ValidateInput(input));
            }

            return vigencia;
        }

        public async Task<PagedResultDto<VigenciaListDto>> GetPaginado(GetVigenciaInput input)
        {
            var condicoes = new List<WhereIfCondition<Vigencia>>
            {
                new WhereIfCondition<Vigencia>(!input.NumeroDeBoletos.IsNullOrEmpty(), r => r.NumeroDeBoletos.Contains(input.NumeroDeBoletos))
            };
            var result = await base.GetListPaged<VigenciaListDto, GetVigenciaInput>(_vigenciaService, input, condicoes);
            return result;
        }


        public async Task<PagedResultDto<VigenciaListDto>> GetPaginadoByProduto(GetVigenciaByProdutoInput input)
        {
            var query = _produtoDePlanoDeSaudeService
                .GetAll()
                .Where(x => x.Id == input.ProdutoId)
                .SelectMany(x => x.Vigencias)
                .WhereIf(!input.NumeroDeBoletos.IsNullOrEmpty(), r => r.NumeroDeBoletos.Contains(input.NumeroDeBoletos));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<VigenciaListDto>>();

            return new PagedResultDto<VigenciaListDto>(count, listDtos);
        }

        public async Task<VigenciaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<VigenciaInput, IdInput>(_vigenciaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Vigencia_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_vigenciaService, input);
        }

        public async Task<VigenciaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<VigenciaInput, IdInput>(_vigenciaService, input);
        }
    }
}