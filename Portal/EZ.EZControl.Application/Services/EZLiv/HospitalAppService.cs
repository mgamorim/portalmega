﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZLiv.Geral.Hospital;
using EZ.EZControl.Dto.EZLiv.Getal.Hospital;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Hospital, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class HospitalAppService : EZControlAppServiceBase<Hospital>, IHospitalAppService
    {
        private readonly IHospitalService _hospitalService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IEnderecoService _enderecoService;
        private readonly ITelefoneService _telefoneService;
        private readonly IEspecialidadeService _especialidadeService;

        public HospitalAppService(IRepository<Hospital> HospitalRepository,
            IHospitalService hospitalService,
            IPessoaJuridicaService pessoaJuridicaService,
            IEnderecoService enderecoService,
            ITelefoneService telefoneService,
            IEspecialidadeService especialidadeService)
            : base(HospitalRepository)
        {
            _hospitalService = hospitalService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _enderecoService = enderecoService;
            _telefoneService = telefoneService;
            _especialidadeService = especialidadeService;
        }

        private void ValidateInput(HospitalInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (String.IsNullOrEmpty(input.Nome))
                    validationErrors.Add(new ValidationResult(L("Hospital.NomeEmpty")));

                if (input.PessoaJuridicaId == default(int))
                    validationErrors.Add(new ValidationResult(L("Hospital.PessoaEmpty")));

                if (input.EnderecoId == default(int))
                    validationErrors.Add(new ValidationResult(L("Hospital.EnderecoEmpty")));

                if (!input.Telefone1Id.HasValue && !input.Telefone2Id.HasValue)
                    validationErrors.Add(new ValidationResult(L("Hospital.TelefoneEmpty")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Hospital hospital, HospitalInput input)
        {
            if (input.PessoaJuridicaId > 0)
            {
                try
                {
                    var pessoaJuridica = await _pessoaJuridicaService.GetById(input.PessoaJuridicaId);
                    if (pessoaJuridica != null)
                    {
                        hospital.PessoaJuridicaId = input.PessoaJuridicaId;
                        hospital.PessoaJuridica = pessoaJuridica;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Hospital.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.EnderecoId > 0)
            {
                try
                {
                    var endereco = await _enderecoService.GetById(input.EnderecoId);
                    if (endereco != null)
                    {
                        hospital.EnderecoId = input.EnderecoId;
                        hospital.Endereco = endereco;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Hospital.EnderecoNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.Telefone1Id > 0)
            {
                try
                {
                    var telefone = await _telefoneService.GetById(input.Telefone1Id.Value);
                    if (telefone != null)
                    {
                        hospital.Telefone1Id = input.Telefone1Id;
                        hospital.Telefone1 = telefone;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Hospital.TelefoneNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
                hospital.Telefone1Id = null;

            if (input.Telefone2Id > 0)
            {
                try
                {
                    var telefone = await _telefoneService.GetById(input.Telefone2Id.Value);
                    if (telefone != null)
                    {
                        hospital.Telefone2Id = input.Telefone2Id;
                        hospital.Telefone2 = telefone;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Hospital.TelefoneNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else { hospital.Telefone2Id = null; }


            hospital.Especialidades.Clear();

            if (input.Especialidades != null && input.Especialidades.Any())
            {
                foreach (var item in input.Especialidades)
                {
                    var especialidade = await _especialidadeService.GetById(item.Id);
                    hospital.Especialidades.Add(especialidade);
                }
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Hospital_Create, AppPermissions.Pages_Tenant_EZLiv_Hospital_Edit)]
        public async Task<IdInput> Save(HospitalInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Hospital_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, HospitalInput>(_hospitalService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Hospital_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, HospitalInput>(_hospitalService, input, () => ValidateInput(input), ProcessInput);
            }
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Hospital_Create, AppPermissions.Pages_Tenant_EZLiv_Hospital_Edit)]
        public async Task<HospitalInput> SaveAndReturnEntity(HospitalInput input)
        {
            HospitalInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Hospital_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<HospitalInput, HospitalInput>(_hospitalService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Hospital_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<HospitalInput, HospitalInput>(_hospitalService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<HospitalListDto>> GetPaginado(GetHospitalInput input)
        {
            var condicoes = new List<WhereIfCondition<Hospital>>
            {
                new WhereIfCondition<Hospital>(
                    !input.Email.IsNullOrEmpty() ||
                    !input.Nome.IsNullOrEmpty() ||
                    !input.Site.IsNullOrEmpty(),
                    a =>
                        a.Email.ToLower().Contains(input.Email.ToLower()) ||
                        a.Nome.ToLower().Contains(input.Nome.ToLower()) ||
                        a.Site.ToLower().Contains(input.Site.ToLower())
                        )

            };
            return await base.GetListPaged<HospitalListDto, GetHospitalInput>(_hospitalService, input, condicoes);
        }

        public async Task<HospitalInput> GetById(IdInput input)
        {
            return await base.GetEntityById<HospitalInput, IdInput>(_hospitalService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Hospital_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_hospitalService, input);
        }

        public async Task<HospitalInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<HospitalInput, IdInput>(_hospitalService, input);
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForHospital(GetPessoaExceptForHospital input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var idsPessoaJuridica = _hospitalService.GetAll().Select(y => y.PessoaJuridicaId);

                var queryPessoaJuridica = _pessoaJuridicaService
                    .GetAll()
                    .Where(x => !idsPessoaJuridica.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                    .Select(x => new PessoaJuridicaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.NomeFantasia,
                        NomeFantasia = x.NomeFantasia,
                        RazaoSocial = x.RazaoSocial,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Juridica
                    });

                IQueryable<PessoaListDto> query = queryPessoaJuridica;

                var count = await query.CountAsync();

                List<PessoaListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public virtual async Task<PagedResultDto<EspecialidadeListDto>> GetEspecialidadeExceptForHospital(GetEspecialidadeExceptForHospital input)
        {
            try
            {
                if (input == null)
                    throw new UserFriendlyException(L("Hospital.FiltroEspecialidadeNotFoundError"));

                var idsLaboratorio = input.Especialidades.Select(x => x.Id);

                var queryEspecialidade = _especialidadeService
                    .GetAll()
                    .Where(x => !idsLaboratorio.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.Codigo) ||
                    !string.IsNullOrEmpty(input.Nome), a =>
                       a.Codigo.Contains(input.Codigo) ||
                       a.Nome.Contains(input.Nome))
                    .Select(x => new EspecialidadeListDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Nome = x.Nome
                    });

                IQueryable<EspecialidadeListDto> query = queryEspecialidade;

                var count = await query.CountAsync();

                List<EspecialidadeListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<EspecialidadeListDto>>();
                return new PagedResultDto<EspecialidadeListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
