﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorAdministradora;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SupervisorAdministradoraAppService : EZControlAppServiceBase<SupervisorAdministradora>, ISupervisorAdministradoraAppService
    {
        private readonly ICorretorService _corretorService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IAdministradoraService _administradoraService;
        private readonly IPessoaService _pessoaService;
        private readonly ISupervisorAdministradoraService _gerenteService;
        private readonly IEmpresaService _empresaService;
        public SupervisorAdministradoraAppService(IRepository<SupervisorAdministradora> corretorRepository,
                                          ICorretorService corretorService,
                                          IPessoaFisicaService pessoaFisicaService,
                                          IAdministradoraService administradoraService,
                                          IPessoaService pessoaService,
                                          ISupervisorAdministradoraService gerenteService, 
                                          IEmpresaService empresaService
                                          )
            : base(corretorRepository)
        {
            _corretorService = corretorService;
            _pessoaFisicaService = pessoaFisicaService;
            _administradoraService = administradoraService;
            _pessoaService = pessoaService;
            _gerenteService = gerenteService;
            _empresaService = empresaService;
        }
        private void ValidateInput(SupervisorAdministradoraInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (input.AdministradoraId == 0)
            {
                validationErrors.Add(new ValidationResult("Administradora não informada"));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        private async Task ProcessInput(SupervisorAdministradora entity, SupervisorAdministradoraInput input)
        {
            if (input.Pessoa.Id > 0)
            {
                try
                {
                    var pessoa = await _pessoaService.GetById(input.Pessoa.Id);
                    if (pessoa != null)
                    {
                        entity.Pessoa = pessoa;
                        entity.PessoaId = input.Pessoa.Id;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Administradora.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            if (input.AdministradoraId > 0)
            {
                var corretora = await _administradoraService.GetById(input.AdministradoraId);
                if (corretora != null)
                {
                    entity.AdministradoraId = input.AdministradoraId;
                    entity.Administradora = corretora;
                }
                else
                {
                    throw new UserFriendlyException(L("Administradora.NotFoundError"));
                }
            }
            var validationErrors = new List<ValidationResult>();
            if (entity.IsPessoaFisica)
            {
                var errors = entity.IsValidSupervisorAdministradora();
                if (errors.Any())
                {
                    foreach (var valid in errors)
                    {
                        validationErrors.Add(new ValidationResult(L(valid)));
                    }
                }
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Create, AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Edit)]
        public async Task<IdInput> Save(SupervisorAdministradoraInput input)
        {
            IdInput result = null;
            var gerenteAdministradora = await _gerenteService.GetAll().Where(x => x.AdministradoraId == input.AdministradoraId && x.PessoaId == input.Pessoa.Id).FirstOrDefaultAsync();
            if (gerenteAdministradora == null)
            {
                var corretor = await _corretorService.GetAll().Where(x => x.Pessoa.Id == input.Pessoa.Id).FirstOrDefaultAsync();
                var pessoa = await _pessoaService.GetById(input.Pessoa.Id);

                gerenteAdministradora = new SupervisorAdministradora() {
                    AdministradoraId = input.AdministradoraId,
                    PessoaId = input.Pessoa.Id,
                    Pessoa = pessoa,
                    IsActive = true,
                    TenantId = 1
                };

                var id = await Repository.InsertAndGetIdAsync(gerenteAdministradora);

                //TODO não precisa ser um corretor, colocar coo pessoa física
                //var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
                //_gerenteService.AddSupervisorInRole(corretor, empresa);

                result = new IdInput(id);
            }else
            {
                throw new UserFriendlyException(L("SupervisorAdministradora.PessoaSupervisorExistenteError"));
            }
            return result;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Create, AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Edit)]
        public async Task<SupervisorAdministradoraInput> SaveAndReturnEntity(SupervisorAdministradoraInput input)
        {
            SupervisorAdministradoraInput result = null;
            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<SupervisorAdministradoraInput, SupervisorAdministradoraInput>(_gerenteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<SupervisorAdministradoraInput, SupervisorAdministradoraInput>(_gerenteService, input, () => ValidateInput(input), ProcessInput);
            }
            return result;
        }
        public async Task<PagedResultDto<SupervisorAdministradoraListDto>> GetSupervisorAdministradorasPaginado(GetSupervisorAdministradoraInput input)
        {
            var condicoes = new List<WhereIfCondition<SupervisorAdministradora>>
            {
                new WhereIfCondition<SupervisorAdministradora>(!input.Nome.IsNullOrEmpty(), r => r.NomePessoa.Contains(input.Nome))
            };
            var result = await base.GetListPaged<SupervisorAdministradoraListDto, GetSupervisorAdministradoraInput>(_gerenteService, input, condicoes);
            return result;
        }
        public async Task<SupervisorAdministradoraInput> GetById(IdInput input)
        {
            return await base.GetEntityById<SupervisorAdministradoraInput, IdInput>(_gerenteService, input);
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_SupervisorAdministradora_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_gerenteService, input);
        }
        public async Task<SupervisorAdministradoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<SupervisorAdministradoraInput, IdInput>(_gerenteService, input);
        }
    }
}