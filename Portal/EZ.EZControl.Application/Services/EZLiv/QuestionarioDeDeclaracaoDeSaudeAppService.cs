﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class QuestionarioDeDeclaracaoDeSaudeAppService : EZControlAppServiceBase<QuestionarioDeDeclaracaoDeSaude>, IQuestionarioDeDeclaracaoDeSaudeAppService
    {
        private readonly IQuestionarioDeDeclaracaoDeSaudeService _questionarioDeDeclaracaoDeSaudeService;
        private readonly IOperadoraService _operadoraService;
        private readonly IItemDeDeclaracaoDeSaudeService _itemDeDeclaracaoDeSaudeService;
        public QuestionarioDeDeclaracaoDeSaudeAppService(IRepository<QuestionarioDeDeclaracaoDeSaude> questionarioDeDeclaracaoDeSaudeRepository,
                                                         IOperadoraService operadoraService,
                                                         IItemDeDeclaracaoDeSaudeService itemDeDeclaracaoDeSaudeService,
                                                         IQuestionarioDeDeclaracaoDeSaudeService questionarioDeDeclaracaoDeSaudeService)
                                                            : base(questionarioDeDeclaracaoDeSaudeRepository)
        {
            this._questionarioDeDeclaracaoDeSaudeService = questionarioDeDeclaracaoDeSaudeService;
            this._operadoraService = operadoraService;
            this._itemDeDeclaracaoDeSaudeService = itemDeDeclaracaoDeSaudeService;
        }

        private void ValidateInput(QuestionarioDeDeclaracaoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("QuestionarioDeDeclaracaoDeSaude.EmptyNome")));

            if (input.OperadoraId == 0)
                validationErrors.Add(new ValidationResult(L("QuestionarioDeDeclaracaoDeSaude.EmptyOperadora")));

            if (input.ItensDeDeclaracaoDeSaude == null || !input.ItensDeDeclaracaoDeSaude.Any())
                validationErrors.Add(new ValidationResult(L("QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoNotFoundError")));

            if (input.ItensDeDeclaracaoDeSaude != null &&
                !input.ItensDeDeclaracaoDeSaude.Any(x => x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Altura))
                validationErrors.Add(new ValidationResult(L("QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoAlturaEmpty")));

            if (input.ItensDeDeclaracaoDeSaude != null &&
                !input.ItensDeDeclaracaoDeSaude.Any(x => x.TipoDeItemDeDeclaracao == TipoDeItemDeDeclaracaoEnum.Peso))
                validationErrors.Add(new ValidationResult(L("QuestionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoPesoEmpty")));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(QuestionarioDeDeclaracaoDeSaude questionarioDeDeclaracaoDeSaude, QuestionarioDeDeclaracaoDeSaudeInput input)
        {
            try
            {
                var operadora = await _operadoraService.GetById(input.OperadoraId);
                questionarioDeDeclaracaoDeSaude.Operadora = operadora;
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("QuestionarioDeDeclaracaoDeSaude.OperadoraNotFoundError"));
            }

            questionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoDeSaude.Clear();
            if (input.ItensDeDeclaracaoDeSaude != null && input.ItensDeDeclaracaoDeSaude.Any())
            {
                foreach (var item in input.ItensDeDeclaracaoDeSaude)
                {
                    var itemDeDeclaracao = item.MapTo<ItemDeDeclaracaoDeSaude>();
                    if (itemDeDeclaracao.Id > 0)
                        itemDeDeclaracao = await _itemDeDeclaracaoDeSaudeService.GetById(item.Id);

                    questionarioDeDeclaracaoDeSaude.ItensDeDeclaracaoDeSaude.Add(itemDeDeclaracao);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit)]
        public async Task<IdInput> Save(QuestionarioDeDeclaracaoDeSaudeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, QuestionarioDeDeclaracaoDeSaudeInput>(_questionarioDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, QuestionarioDeDeclaracaoDeSaudeInput>(_questionarioDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit)]
        public async Task<QuestionarioDeDeclaracaoDeSaudeInput> SaveAndReturnEntity(QuestionarioDeDeclaracaoDeSaudeInput input)
        {
            QuestionarioDeDeclaracaoDeSaudeInput questionarioDeDeclaracaoDeSaude = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                questionarioDeDeclaracaoDeSaude = await base.CreateAndReturnEntity<QuestionarioDeDeclaracaoDeSaudeInput, QuestionarioDeDeclaracaoDeSaudeInput>(_questionarioDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                questionarioDeDeclaracaoDeSaude = await base.UpdateAndReturnEntity<QuestionarioDeDeclaracaoDeSaudeInput, QuestionarioDeDeclaracaoDeSaudeInput>(_questionarioDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return questionarioDeDeclaracaoDeSaude;
        }

        public async Task<PagedResultDto<QuestionarioDeDeclaracaoDeSaudeListDto>> GetPaginado(GetQuestionarioDeDeclaracaoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<QuestionarioDeDeclaracaoDeSaude>>
            {
                new WhereIfCondition<QuestionarioDeDeclaracaoDeSaude>(!input.Nome.IsNullOrEmpty(), r => r.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<QuestionarioDeDeclaracaoDeSaudeListDto, GetQuestionarioDeDeclaracaoDeSaudeInput>(_questionarioDeDeclaracaoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<QuestionarioDeDeclaracaoDeSaudeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<QuestionarioDeDeclaracaoDeSaudeInput, IdInput>(_questionarioDeDeclaracaoDeSaudeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_QuestionarioDeDeclaracaoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_questionarioDeDeclaracaoDeSaudeService, input);
        }

        public async Task<QuestionarioDeDeclaracaoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<QuestionarioDeDeclaracaoDeSaudeInput, IdInput>(_questionarioDeDeclaracaoDeSaudeService, input);
        }
    }
}