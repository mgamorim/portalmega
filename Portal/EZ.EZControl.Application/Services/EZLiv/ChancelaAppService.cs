﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using EZ.EZControl.Filters;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Chancela, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ChancelaAppService : EZControlAppServiceBase<Chancela>, IChancelaAppService
    {
        private readonly IChancelaService _chancelaService;
        private readonly IProfissaoService _profissaoService;
        private readonly IRepository<Chancela> _chancelaRepository;
        private readonly IAdministradoraService _administradoraService;

        public ChancelaAppService(IRepository<Chancela> chancelaRepository,
                                  IChancelaService chancelaService,
                                  IProfissaoService profissaoService,
                                  IAdministradoraService administradoraService)
            : base(chancelaRepository)
        {
            _chancelaService = chancelaService;
            _profissaoService = profissaoService;
            _administradoraService = administradoraService;
            _chancelaRepository = chancelaRepository;
        }

        private void ValidateInput(ChancelaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.AssociacaoId == 0)
            {
                validationErrors.Add(new ValidationResult(L("Chancela.EmptyEntidadeError")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Chancela entity, ChancelaInput input)
        {
            var administradora = await _administradoraService.GetByEmpresaLogada();
            if (administradora == null)
            {
                throw new UserFriendlyException("Não foi possível recuperar a administradora a partir da empresa logada.");
            }
            entity.Administradora = administradora;

            entity.Profissoes.Clear();
            foreach (var profissaoInput in input.Profissoes)
            {
                var profissao = await _profissaoService.GetById(profissaoInput.Id);
                entity.Profissoes.Add(profissao);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Chancela_Create, AppPermissions.Pages_Tenant_EZLiv_Chancela_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(ChancelaInput input)
        {
            IdInput result = null;
            using (var uow = this.UnitOfWorkManager.Begin())
            {

                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Chancela_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result = await base.CreateEntity<IdInput, ChancelaInput>(_chancelaService, input, () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Chancela_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result = await base.UpdateEntity<IdInput, ChancelaInput>(_chancelaService, input, () => ValidateInput(input), ProcessInput);
                }

                await uow.CompleteAsync();
            }

            return result;

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Chancela_Create, AppPermissions.Pages_Tenant_EZLiv_Chancela_Edit)]
        public async Task<ChancelaInput> SaveAndReturnEntity(ChancelaInput input)
        {
            ChancelaInput chancela = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Chancela_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                chancela = await base.CreateAndReturnEntity<ChancelaInput, ChancelaInput>(_chancelaService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Chancela_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                chancela = await base.UpdateAndReturnEntity<ChancelaInput, ChancelaInput>(_chancelaService, input, () => ValidateInput(input));
            }

            return chancela;
        }

        public async Task<PagedResultDto<ChancelaListDto>> GetPaginado(GetChancelaInput input)
        {
            var condicoes = new List<WhereIfCondition<Chancela>>
            {
                new WhereIfCondition<Chancela>(
                        !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<ChancelaListDto, GetChancelaInput>(_chancelaService, input, condicoes);
            return result;
        }

        public async Task<PagedResultDto<ChancelaListDto>> GetPaginadoParaPropostaDeContratacao(GetChancelaInput input)
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var query = await _chancelaService.GetAllByUsuarioLogado();

                if (!string.IsNullOrEmpty(input.Nome))
                {
                    query = query.Where(x => x.Nome.Contains(input.Nome));
                }

                var count = await query.CountAsync();
                var dados = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

                var listDtos = dados.MapTo<List<ChancelaListDto>>();

                return new PagedResultDto<ChancelaListDto>(count, listDtos);
            }
        }

        public async Task<ChancelaInput> GetById(IdInput input)
        {
            var chancela = await base.GetEntityById<ChancelaInput, IdInput>(_chancelaService, input);
            return chancela;
        }

        //TODO: Configurar validacões para os usuários pegarem somente as chancelas que podem visualizar
        public async Task<ChancelaInput> GetByIdParaPropostaDeContratacao(IdInput input)
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var chancela = await Repository.GetAsync(input.Id);
                return chancela.MapTo<ChancelaInput>();
            }
        }

        public async Task<ChancelaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ChancelaInput, IdInput>(_chancelaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Chancela_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_chancelaService, input);
        }

        public async Task<PagedResultDto<ChancelaListDto>> GetChancelasByProfissaoId(IdInput input)
        {
            IQueryable<Chancela> query = _chancelaRepository.GetAll().Where(c => c.Profissoes.Any(p => p.Id == input.Id));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(x => x.Nome)
                .PageBy(new GetChancelaInput())
                .ToListAsync();

            var listDtos = dados.MapTo<List<ChancelaListDto>>();

            return new PagedResultDto<ChancelaListDto>(count, listDtos);



            //var condicoes = new List<WhereIfCondition<Chancela>>
            //{
            //    new WhereIfCondition<Chancela>(true, a => a.Profissoes.Any(p => p.Id == input.Id))
            //};
            //var getChancelaInput = new GetChancelaInput();
            //getChancelaInput.Sorting = "Nome";
            //var result = await base.GetListPaged<ChancelaListDto, GetChancelaInput>(_chancelaService, getChancelaInput, condicoes);
            //return result;
        }
    }
}
