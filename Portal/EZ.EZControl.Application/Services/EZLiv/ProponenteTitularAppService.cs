﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ProponenteTitularAppService : EZControlAppServiceBase<ProponenteTitular>, IProponenteTitularAppService
    {
        private readonly IProponenteTitularService _proponenteTitularService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaFisicaAppService _pessoaFisicaAppService;
        private readonly IDocumentoService _documentoService;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IEnderecoEletronicoService _enderecoEletronicoService;
        private readonly IEmpresaService _empresaService;
        private readonly ICorretoraService _corretoraService;
        private readonly IDependenteService _dependenteService;
        private readonly IEstadoService _estadoService;
        private readonly IGrupoPessoaService _grupoPessoaService;
        private readonly IPessoaService _pessoaService;
        private readonly ITelefoneService _telefoneService;
        private readonly IProfissaoService _profissaoService;
        private readonly IAdministradoraService _administradoraService;
        public ProponenteTitularAppService(IRepository<ProponenteTitular> proponenteTitularRepository, IProponenteTitularService proponenteTitularService,
                                           IPessoaFisicaAppService pessoaFisicaAppService, IPessoaFisicaService pessoaFisicaService,
                                           IDocumentoService documentoService, ITipoDeDocumentoService tipoDeDocumentoService,
                                           IEnderecoEletronicoService enderecoEletronicoService, IEmpresaService empresaService,
                                           ICorretoraService corretoraService, IDependenteService dependenteService,
                                           IEstadoService estadoService, IGrupoPessoaService grupoPessoaService,
                                           IPessoaService pessoaService, ITelefoneService telefoneService,
                                           IProfissaoService profissaoService,
                                           IAdministradoraService administradoraService
            )
            : base(proponenteTitularRepository)
        {
            _proponenteTitularService = proponenteTitularService;
            _pessoaFisicaAppService = pessoaFisicaAppService;
            _pessoaFisicaService = pessoaFisicaService;
            _documentoService = documentoService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _enderecoEletronicoService = enderecoEletronicoService;
            _empresaService = empresaService;
            _corretoraService = corretoraService;
            _dependenteService = dependenteService;
            _estadoService = estadoService;
            _grupoPessoaService = grupoPessoaService;
            _pessoaService = pessoaService;
            _telefoneService = telefoneService;
            _profissaoService = profissaoService;
            _administradoraService = administradoraService;
        }

        private async Task<Empresa> GetEmpresa()
        {
            return await _empresaService.GetEmpresaLogada();
        }

        private void ValidateInput(ProponenteTitularInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {

                #region  Dados do beneficiario titular

                if (String.IsNullOrEmpty(input.PessoaFisica.Cpf))
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyCPF")));
                }

                if (!input.PessoaFisica.GrupoPessoaId.HasValue || input.PessoaFisica.GrupoPessoaId == default(int))
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyGrupoPessoaError")));
                }

                if (String.IsNullOrEmpty(input.PessoaFisica.Nome))
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyNome")));
                }

                if (!input.PessoaFisica.DataDeNascimento.HasValue)
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyDataDeNascimento")));
                }
                else
                {
                    var dateNascidoVivo = new DateTime(2010, 01, 01);
                    if (input.PessoaFisica.DataDeNascimento.Value >= dateNascidoVivo && string.IsNullOrEmpty(input.DeclaracaoDeNascidoVivo))
                        validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.ObrigaNascidoVivo")));
                }

                if (input.EstadoId == default(int))
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyEstadoError")));
                }

                if (input.ProfissaoId == default(int))
                {
                    validationErrors.Add(new ValidationResult(L("Proponente.EmptyProfissaoError")));
                }

                #endregion

                #region Responsavel

                //TODO: Adicionar um campo bool para verificar se o titular é responsável
                //if (input.Idade < 18)
                //{
                //    if (input.Responsavel == null || input.Responsavel.TipoDeResponsavel == 0)
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptyTipoResponsavelError")));
                //    }

                //    if (input.Responsavel == null || input.Responsavel.PessoaFisica == null || string.IsNullOrEmpty(input.Responsavel.PessoaFisica.Nome))
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptyNomeError")));
                //    }

                //    if (input.Responsavel == null || input.Responsavel.PessoaFisica == null || string.IsNullOrEmpty(input.Responsavel.PessoaFisica.Cpf))
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptyCpfError")));
                //    }

                //    if (input.Responsavel == null || input.Responsavel.PessoaFisica == null || input.Responsavel.PessoaFisica.DataDeNascimento == null)
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptyDataDeNascimentoError")));
                //    }

                //    if (input.Responsavel == null || input.Responsavel.PessoaFisica == null || string.IsNullOrEmpty(input.Responsavel.PessoaFisica.Nacionalidade))
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptyNacionalidadeError")));
                //    }

                //    if (input.Responsavel == null || input.Responsavel.PessoaFisica == null || !input.PessoaFisica.Sexo.HasValue)
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptySexoError")));
                //    }

                //    if (input.Responsavel == null || input.Responsavel.PessoaFisica == null || !input.PessoaFisica.EstadoCivil.HasValue)
                //    {
                //        validationErrors.Add(new ValidationResult(L("Responsavel.EmptyEstadoCivilError")));
                //    }
                //}

                #endregion

                #region Dependentes

                if (input.Dependentes != null && input.Dependentes.Any())
                {
                    foreach (var dependente in input.Dependentes)
                    {
                        if (dependente.PessoaFisica == null || string.IsNullOrEmpty(dependente.PessoaFisica.Nome))
                        {
                            validationErrors.Add(new ValidationResult(L("Proponente.EmptyNomeDependenteError")));
                        }

                        if (dependente.PessoaFisica == null || !dependente.PessoaFisica.DataDeNascimento.HasValue)
                        {
                            validationErrors.Add(new ValidationResult(L("Proponente.EmptyDataNascimentoDependenteError")));
                        }
                        else
                        {
                            var dateNascidoVivo = new DateTime(2010, 01, 01);
                            if (dependente.PessoaFisica.DataDeNascimento.Value >= dateNascidoVivo && string.IsNullOrEmpty(dependente.DeclaracaoDeNascidoVivo))
                                validationErrors.Add(new ValidationResult(L("PropostaDeContratacao.ObrigaNascidoVivo")));
                        }

                        if (dependente.PessoaFisica == null || dependente.PessoaFisica.Sexo == null)
                        {
                            validationErrors.Add(new ValidationResult(L("Proponente.EmptySexoDependenteError")));
                        }

                        if (dependente.PessoaFisica == null || dependente.PessoaFisica.EstadoCivil == null)
                        {
                            validationErrors.Add(new ValidationResult(L("Proponente.EmptyEstadoCivilDependenteError")));
                        }

                        if (dependente.GrauDeParentesco == 0)
                        {
                            validationErrors.Add(new ValidationResult(L("Proponente.EmptyGrauParentescoDependenteError")));
                        }

                    }
                }

                #endregion
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task<ProponenteTitular> ProcessInput(ProponenteTitular proponenteTitular, ProponenteTitularInput input)
        {
            var empresa = await GetEmpresa();
            #region Dados do beneficiario titular

            proponenteTitular.Pessoa.NomePessoa = input.PessoaFisica.Nome;
            proponenteTitular.PessoaFisica.DataDeNascimento = input.PessoaFisica.DataDeNascimento;
            proponenteTitular.PessoaFisica.Nacionalidade = input.PessoaFisica.Nacionalidade;
            proponenteTitular.PessoaFisica.Sexo = input.PessoaFisica.Sexo;
            proponenteTitular.PessoaFisica.EstadoCivil = input.PessoaFisica.EstadoCivil;
            proponenteTitular.NomeDaMae = input.NomeDaMae;
            proponenteTitular.NumeroDoCartaoNacionalDeSaude = input.NumeroDoCartaoNacionalDeSaude;
            proponenteTitular.DeclaracaoDeNascidoVivo = input.DeclaracaoDeNascidoVivo;
            proponenteTitular.matricula = input.matricula;
            //proponenteTitular.boleto = input.boleto;
            //proponenteTitular.folha = input.folha;

            if (input.PessoaFisica.GrupoPessoaId > 0)
            {
                try
                {
                    var grupoPessoa = await _grupoPessoaService.GetById(input.PessoaFisica.GrupoPessoaId.Value);
                    proponenteTitular.PessoaFisica.AssociarGrupoPessoa(grupoPessoa, _pessoaService);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Proponente.GrupoPessoaNotFoundError"));
                }
            }

            #region CPF  
            Documento documento = null;
            if (input.PessoaFisica.CpfId.HasValue && input.PessoaFisica.CpfId > 0)
            {
                try
                {
                    documento = await _documentoService.GetById(input.PessoaFisica.CpfId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Proponente.CpfNotFoundError"));
                }

                proponenteTitular.PessoaFisica.Documentos.First(x => x.Id == input.PessoaFisica.CpfId).Numero = input.PessoaFisica.Cpf;
            }
            else
            {
                TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                documento = new Documento();
                documento.Numero = input.PessoaFisica.Cpf;
                documento.AssociarTipoDocumento(tipo, _documentoService);
                documento.Empresa = empresa;
                documento.EmpresaId = empresa.Id;
                proponenteTitular.PessoaFisica.Documentos = new List<Documento>();
                proponenteTitular.PessoaFisica.Documentos.Add(documento);
            }
            #endregion

            #region RG  
            Documento documentoRG = null;
            if (input.PessoaFisica.RgId.HasValue && input.PessoaFisica.RgId > 0)
            {
                try
                {
                    documentoRG = await _documentoService.GetById(input.PessoaFisica.RgId.Value);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Proponente.RGNotFoundError"));
                }

                proponenteTitular.PessoaFisica.Documentos.First(x => x.Id == input.PessoaFisica.RgId).Numero = input.PessoaFisica.Rg;
                proponenteTitular.PessoaFisica.Documentos.First(x => x.Id == input.PessoaFisica.RgId).OrgaoExpedidor = input.PessoaFisica.OrgaoExpedidor;
            }
            else
            {
                TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Rg);
                documentoRG = new Documento();
                documentoRG.Numero = string.IsNullOrEmpty(input.PessoaFisica.Rg) ? string.Empty : input.PessoaFisica.Rg;
                documentoRG.OrgaoExpedidor = input.PessoaFisica.OrgaoExpedidor;
                documentoRG.AssociarTipoDocumento(tipo, _documentoService);
                documentoRG.Empresa = empresa;
                documentoRG.EmpresaId = empresa.Id;

                if (proponenteTitular.PessoaFisica.Documentos == null) proponenteTitular.PessoaFisica.Documentos = new List<Documento>();

                proponenteTitular.PessoaFisica.Documentos.Add(documentoRG);
            }
            #endregion

            #region Email
            EnderecoEletronico email = null;
            if (input.PessoaFisica.EmailPrincipal != null && input.PessoaFisica.EmailPrincipal.Id > 0)
            {
                try
                {
                    email = await _enderecoEletronicoService.GetById(input.PessoaFisica.EmailPrincipal.Id);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Proponente.EmailNotFoundError"));
                }

                email.Endereco = input.PessoaFisica.EmailPrincipal.Endereco;
                proponenteTitular.PessoaFisica.EnderecosEletronicos.Add(email);
            }
            else if (input.PessoaFisica.EmailPrincipal != null && !string.IsNullOrEmpty(input.PessoaFisica.EmailPrincipal.Endereco))
            {
                email = new EnderecoEletronico();
                email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                email.Endereco = input.PessoaFisica.EmailPrincipal.Endereco;
                email.Empresa = empresa;
                email.EmpresaId = empresa.Id;

                proponenteTitular.PessoaFisica.EnderecosEletronicos = new List<EnderecoEletronico>();
                proponenteTitular.PessoaFisica.EnderecosEletronicos.Add(email);
            }
            #endregion

            #region Celular
            Telefone telefone = null;
            if (input.PessoaFisica.TelefoneCelular != null && input.PessoaFisica.TelefoneCelular.Id > 0)
            {
                try
                {
                    telefone = await _telefoneService.GetById(input.PessoaFisica.TelefoneCelular.Id);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Proponente.CelularNotFoundError"));
                }

                _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
            }
            else if (!string.IsNullOrEmpty(input.Celular))
            {
                telefone = new Telefone();
                telefone.Tipo = TipoTelefoneEnum.Celular;
                _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, input.Celular);
                telefone.Empresa = empresa;
                telefone.EmpresaId = empresa.Id;

                proponenteTitular.PessoaFisica.Telefones = new List<Telefone>();
                proponenteTitular.PessoaFisica.Telefones.Add(telefone);
            }
            #endregion

            #endregion

            #region Dados do responsavel

            if (input.Idade < 18)
            {
                if (proponenteTitular.Responsavel == null) proponenteTitular.Responsavel = new Responsavel();
                if (proponenteTitular.Responsavel.PessoaFisica == null) proponenteTitular.Responsavel.PessoaFisica = new PessoaFisica();

                proponenteTitular.Responsavel.TipoDeResponsavel = input.Responsavel.TipoDeResponsavel;
                proponenteTitular.Responsavel.PessoaFisica.Nome = input.Responsavel.PessoaFisica.Nome;
                proponenteTitular.Responsavel.PessoaFisica.DataDeNascimento = input.Responsavel.PessoaFisica.DataDeNascimento;
                proponenteTitular.Responsavel.PessoaFisica.Nacionalidade = input.Responsavel.PessoaFisica.Nacionalidade;
                proponenteTitular.Responsavel.PessoaFisica.Sexo = input.Responsavel.PessoaFisica.Sexo;
                proponenteTitular.Responsavel.PessoaFisica.EstadoCivil = input.Responsavel.PessoaFisica.EstadoCivil;

                #region Cpf Responsável  
                Documento documentoResponsavel = null;
                if (input.Responsavel.PessoaFisica != null && input.Responsavel.PessoaFisica.CpfId.HasValue && input.Responsavel.PessoaFisica.CpfId > 0)
                {
                    try
                    {
                        documentoResponsavel = await _documentoService.GetById(input.Responsavel.PessoaFisica.CpfId.Value);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("Proponente.CpfNotFoundError"));
                    }

                    proponenteTitular.Responsavel.PessoaFisica.Documentos.First(x => x.Id == input.Responsavel.PessoaFisica.CpfId).Numero = input.Responsavel.PessoaFisica.Cpf;
                }
                else
                {
                    TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                    documentoResponsavel = new Documento();
                    documentoResponsavel.Numero = string.IsNullOrEmpty(input.Responsavel.PessoaFisica.Cpf) ? string.Empty : input.Responsavel.PessoaFisica.Cpf;
                    documentoResponsavel.AssociarTipoDocumento(tipo, _documentoService);
                    documentoResponsavel.Empresa = empresa;
                    documentoResponsavel.EmpresaId = empresa.Id;

                    if (proponenteTitular.Responsavel.PessoaFisica.Documentos == null)
                        proponenteTitular.Responsavel.PessoaFisica.Documentos = new List<Documento>();

                    proponenteTitular.Responsavel.PessoaFisica.Documentos.Add(documentoResponsavel);
                }
                #endregion
            }
            #endregion

            #region Dependentes

            if (proponenteTitular != null && input.Dependentes != null)
            {
                await PreencherDependentes(proponenteTitular, input.Dependentes);
            }

            #endregion

            return proponenteTitular;

        }

        private async Task PreencherDependentes(ProponenteTitular proponenteTitular, List<DependenteInput> listDependenteInput)
        {
            var empresa = await GetEmpresa();
            if (proponenteTitular != null && proponenteTitular.Dependentes != null && proponenteTitular.Dependentes.Any())
            {
                var idsInput = listDependenteInput.Where(x => x.Id > 0).Select(x => x.Id);
                var idsDependentesParaExcluir = (listDependenteInput.Any()) ?
                                        proponenteTitular.Dependentes.Where(x => !idsInput.Contains(x.Id)).Select(x => x.Id).Cast<int>().ToList()
                                        : proponenteTitular.Dependentes.Select(x => x.Id).Cast<int>().ToList();

                for (int i = 0; i < idsDependentesParaExcluir.Count(); i++)
                {
                    if (idsDependentesParaExcluir[i] > 0)
                        await _dependenteService.Delete(idsDependentesParaExcluir[i]);
                }
            }

            if (proponenteTitular.Dependentes == null)
                proponenteTitular.Dependentes = new List<Dependente>();

            foreach (var dependenteInput in listDependenteInput)
            {
                var dependente = (dependenteInput.Id > 0) ? proponenteTitular.Dependentes.FirstOrDefault(x => x.Id == dependenteInput.Id) : new Dependente();
                dependente.GrauDeParentesco = dependenteInput.GrauDeParentesco;

                if (dependente.PessoaFisica == null)
                    dependente.PessoaFisica = new PessoaFisica();

                dependente.PessoaFisica.Nome = dependenteInput.PessoaFisica.Nome;
                dependente.PessoaFisica.DataDeNascimento = dependenteInput.PessoaFisica.DataDeNascimento;
                dependente.PessoaFisica.Sexo = dependenteInput.PessoaFisica.Sexo;
                dependente.PessoaFisica.EstadoCivil = dependenteInput.PessoaFisica.EstadoCivil;
                dependente.PessoaFisica.Nacionalidade = dependenteInput.PessoaFisica.Nacionalidade;
                dependente.NomeDaMae = dependenteInput.NomeDaMae;
                dependente.NumeroDoCartaoNacionalDeSaude = dependenteInput.NumeroDoCartaoNacionalDeSaude;
                dependente.DeclaracaoDeNascidoVivo = dependenteInput.DeclaracaoDeNascidoVivo;

                dependente.TenantId = AbpSession.TenantId.Value;
                //dependente.EmpresaId = empresa.Id;
                //dependente.Empresa = empresa;

                #region CPF  
                Documento documento = null;
                if (dependenteInput.PessoaFisica.CpfId.HasValue && dependenteInput.PessoaFisica.CpfId > 0)
                {
                    try
                    {
                        documento = await _documentoService.GetById(dependenteInput.PessoaFisica.CpfId.Value);
                    }
                    catch (Exception)
                    {
                        throw new UserFriendlyException(L("Proponente.CpfNotFoundError"));
                    }

                    dependente.PessoaFisica.Documentos.First(x => x.Id == dependenteInput.PessoaFisica.CpfId).Numero = dependenteInput.PessoaFisica.Cpf;
                }
                else if (!String.IsNullOrEmpty(dependenteInput.PessoaFisica.Cpf))
                {
                    TipoDeDocumento tipo = _tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                    documento = new Documento();
                    documento.Numero = String.IsNullOrEmpty(dependenteInput.PessoaFisica.Cpf) ? string.Empty : dependenteInput.PessoaFisica.Cpf;
                    documento.AssociarTipoDocumento(tipo, _documentoService);
                    documento.Empresa = empresa;
                    documento.EmpresaId = empresa.Id;
                    dependente.PessoaFisica.Documentos = new List<Documento>();
                    dependente.PessoaFisica.Documentos.Add(documento);
                }

                #endregion

                if (dependenteInput.Id == 0)
                    proponenteTitular.Dependentes.Add(dependente);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Create, AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Edit)]
        public async Task<IdInput> Save(ProponenteTitularInput input)
        {
            IdInput result = null;

            if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Edit))
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));

            ValidateInput(input);
            ProponenteTitular proponenteTitular = await _proponenteTitularService.GetById(input.Id);
            proponenteTitular = await ProcessInput(proponenteTitular, input);
            var entity = await _proponenteTitularService.UpdateAndReturnEntity(proponenteTitular);
            result = new IdInput(entity.Id);
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Create, AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Edit)]
        public async Task<ProponenteTitularInput> SaveAndReturnEntity(ProponenteTitularInput input)
        {
            ProponenteTitularInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ProponenteTitularInput, ProponenteTitularInput>(_proponenteTitularService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ProponenteTitularInput, ProponenteTitularInput>(_proponenteTitularService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<ProponenteTitularListDto>> GetPaginado(GetProponenteTitularInput input)
        {
            var empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);

            var administradora = await _administradoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();
            var corretora = await _corretoraService.GetAll().Where(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId).FirstOrDefaultAsync();

            var condicoes = new List<WhereIfCondition<ProponenteTitular>>();
            condicoes.Add(new WhereIfCondition<ProponenteTitular>(!input.Nome.IsNullOrEmpty(), r => r.PessoaFisica.Nome.Contains(input.Nome)));

            if (administradora != null)
            {
                var idsCorretoras = administradora.Corretoras.Select(x => x.Id);
                condicoes.Add(new WhereIfCondition<ProponenteTitular>(true, r => r.Corretoras.Any(x => idsCorretoras.Contains(x.Id))));
            }

            if (corretora != null)
                condicoes.Add(new WhereIfCondition<ProponenteTitular>(true, r => r.Corretoras.Any(x => x.Id == corretora.Id)));

            return await base.GetListPaged<ProponenteTitularListDto, GetProponenteTitularInput>(_proponenteTitularService, input, condicoes);
        }

        public async Task<ProponenteTitularInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ProponenteTitularInput, IdInput>(_proponenteTitularService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_ProponenteTitular_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_proponenteTitularService, input);
        }

        public async Task<ProponenteTitularInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ProponenteTitularInput, IdInput>(_proponenteTitularService, input);
        }
    }
}
