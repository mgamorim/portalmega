﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Associacao, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class AssociacaoAppService : EZControlAppServiceBase<Associacao>, IAssociacaoAppService
    {
        private readonly IAssociacaoService _associacaoService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IParametroPagSeguroService _parametroPagSeguroService;
        private readonly IProfissaoService _profissaoService;
        private readonly IChancelaService _chancelaService;
        private readonly IArquivoGlobalService _arquivoGlobalService;

        public AssociacaoAppService(IRepository<Associacao> associacaoRepository,
                                          IAssociacaoService associacaoService,
                                          IPessoaJuridicaService pessoaJuridicaService,
                                          IParametroPagSeguroService parametroPagSeguroService,
                                          IProfissaoService profissaoService,
                                          IArquivoGlobalService arquivoGlobalService,
                                          IChancelaService chancelaService)
            : base(associacaoRepository)
        {
            _associacaoService = associacaoService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _parametroPagSeguroService = parametroPagSeguroService;
            _profissaoService = profissaoService;
            _arquivoGlobalService = arquivoGlobalService;
            _chancelaService = chancelaService;
        }

        private void ValidateInput(AssociacaoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (input.Id == default(int))
                {
                    var existePessoaJuridicaCadastrada = _associacaoService.GetAll().Any(x => x.PessoaJuridicaId == input.PessoaJuridicaId);

                    if (existePessoaJuridicaCadastrada)
                        validationErrors.Add(new ValidationResult(L("Associacao.ExistePessoaJuridicaAssociadaAAssociacao")));
                }

                if (input.PessoaJuridicaId == default(int))
                    validationErrors.Add(new ValidationResult(L("Associacao.PessoaEmpty")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Associacao entity, AssociacaoInput input)
        {
            //Imagem
            if (input.ImagemId > 0)
            {
                try
                {
                    var imagem = await _arquivoGlobalService.GetById(input.ImagemId);
                    if (imagem != null)
                    {
                        entity.Imagem = imagem;
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Associacao_Create, AppPermissions.Pages_Tenant_EZLiv_Associacao_Edit)]
        public async Task<IdInput> Save(AssociacaoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Associacao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, AssociacaoInput>(_associacaoService, input, () => ValidateInput(input), ProcessInput);
                await SaveParametroPagSeguro(input.ParametroPagSeguro);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Associacao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, AssociacaoInput>(_associacaoService, input, () => ValidateInput(input), ProcessInput);
                await SaveParametroPagSeguro(input.ParametroPagSeguro);
            }
            return result;
        }


        private async Task SaveParametroPagSeguro(ParametroPagSeguroInput parametroPagseguroInput)
        {
            if (parametroPagseguroInput != null)
            {
                var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(parametroPagseguroInput.PessoaJuridicaId);

                if (parametroPagSeguro == null)
                {
                    parametroPagSeguro = new ParametroPagSeguro();
                    parametroPagSeguro.PessoaJuridicaId = parametroPagseguroInput.PessoaJuridicaId;
                }

                parametroPagSeguro.Token = parametroPagseguroInput.Token;
                parametroPagSeguro.Email = parametroPagseguroInput.Email;

                await _parametroPagSeguroService.SaveParametroPagSeguro(parametroPagSeguro);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Associacao_Create, AppPermissions.Pages_Tenant_EZLiv_Associacao_Edit)]
        public async Task<AssociacaoInput> SaveAndReturnEntity(AssociacaoInput input)
        {
            AssociacaoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Associacao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<AssociacaoInput, AssociacaoInput>(_associacaoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Associacao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<AssociacaoInput, AssociacaoInput>(_associacaoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<AssociacaoListDto>> GetPaginado(GetAssociacaoInput input)
        {
            var condicoes = new List<WhereIfCondition<Associacao>>
            {
                 new WhereIfCondition<Associacao>(
                    !string.IsNullOrEmpty(input.NomeFantasia) ||
                    !string.IsNullOrEmpty(input.RazaoSocial),
                    a =>
                        a.PessoaJuridica.NomeFantasia.Contains(input.NomeFantasia) ||
                        a.PessoaJuridica.RazaoSocial.Contains(input.RazaoSocial))
            };
            var result = await base.GetListPaged<AssociacaoListDto, GetAssociacaoInput>(_associacaoService, input, condicoes);
            return result;
        }

        public async Task<AssociacaoInput> GetById(IdInput input)
        {
            var associacaoInput = await base.GetEntityById<AssociacaoInput, IdInput>(_associacaoService, input);
            var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(associacaoInput.PessoaJuridicaId);
            associacaoInput.ParametroPagSeguro = parametroPagSeguro.MapTo<ParametroPagSeguroInput>();

            return associacaoInput;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Associacao_Delete)]
        public async Task Delete(IdInput input)
        {
            var associacao = _associacaoService.GetAll().FirstOrDefault(x => x.Id == input.Id);
            if (associacao != null)
            {
                var parametroPagSeguro = _parametroPagSeguroService.GetParametroByPessoaJuridicaId(associacao.PessoaJuridicaId);
                await _parametroPagSeguroService.Delete(x => x.Id == parametroPagSeguro.Id);
            }
            await base.DeleteEntity(_associacaoService, input);
        }

        public async Task<AssociacaoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<AssociacaoInput, IdInput>(_associacaoService, input);
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForAssociacao(GetPessoaExceptForAssociacao input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var idsPessoaJuridica = _associacaoService.GetAll().Select(y => y.PessoaJuridicaId);

                var queryPessoaJuridica = _pessoaJuridicaService
                    .GetAll()
                    .Where(x => !idsPessoaJuridica.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                    .Select(x => new PessoaJuridicaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.NomeFantasia,
                        NomeFantasia = x.NomeFantasia,
                        RazaoSocial = x.RazaoSocial,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Juridica
                    });

                IQueryable<PessoaListDto> query = queryPessoaJuridica;

                var count = await query.CountAsync();

                List<PessoaListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PagedResultDto<AssociacaoListDto>> GetByProfissaoId(IdInput input)
        {
            //Busco todos os produtos que possuam chancelas que possuam profissoes
            var chancelas = await _chancelaService.GetAll().Where(c => c.Profissoes.Any(p => p.Id == input.Id)).ToListAsync();
            //Seleciono somente os ids dessas profissoes
            var q = (from c in chancelas
                     select c.AssociacaoId).ToList();

            var condicoes = new List<WhereIfCondition<Associacao>>
            {
                new WhereIfCondition<Associacao>(true,
                    x => q.Contains(x.Id))
            };

            var result = await base.GetListPaged<AssociacaoListDto, GetAssociacaoInput>(_associacaoService, new GetAssociacaoInput(), condicoes);
            return result;
        }
    }
}
