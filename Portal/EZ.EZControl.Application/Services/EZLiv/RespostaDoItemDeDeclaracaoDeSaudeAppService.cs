﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RespostaDoItemDeDeclaracaoDeSaudeAppService : EZControlAppServiceBase<RespostaDoItemDeDeclaracaoDeSaude>, IRespostaDoItemDeDeclaracaoDeSaudeAppService
    {
        private readonly IRespostaDoItemDeDeclaracaoDeSaudeService _respostaDoItemDeDeclaracaoDeSaudeService;

        public RespostaDoItemDeDeclaracaoDeSaudeAppService(IRepository<RespostaDoItemDeDeclaracaoDeSaude> _respostaDoItemDeDeclaracaoDeSaudeRepository,
                                          IRespostaDoItemDeDeclaracaoDeSaudeService _respostaDoItemDeDeclaracaoDeSaudeService)
            : base(_respostaDoItemDeDeclaracaoDeSaudeRepository)
        {
            this._respostaDoItemDeDeclaracaoDeSaudeService = _respostaDoItemDeDeclaracaoDeSaudeService;
        }

        private void ValidateInput(RespostaDoItemDeDeclaracaoDeSaudeInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(RespostaDoItemDeDeclaracaoDeSaude _respostaDoItemDeDeclaracaoDeSaude, RespostaDoItemDeDeclaracaoDeSaudeInput _respostaDoItemDeDeclaracaoDeSaudeInput)
        {

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Edit)]
        public async Task<IdInput> Save(RespostaDoItemDeDeclaracaoDeSaudeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, RespostaDoItemDeDeclaracaoDeSaudeInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, RespostaDoItemDeDeclaracaoDeSaudeInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Create, AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Edit)]
        public async Task<RespostaDoItemDeDeclaracaoDeSaudeInput> SaveAndReturnEntity(RespostaDoItemDeDeclaracaoDeSaudeInput input)
        {
            RespostaDoItemDeDeclaracaoDeSaudeInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<RespostaDoItemDeDeclaracaoDeSaudeInput, RespostaDoItemDeDeclaracaoDeSaudeInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<RespostaDoItemDeDeclaracaoDeSaudeInput, RespostaDoItemDeDeclaracaoDeSaudeInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<RespostaDoItemDeDeclaracaoDeSaudeListDto>> GetDeclaracoesDeSaudePaginado(GetRespostaDoItemDeDeclaracaoDeSaudeInput input)
        {
            var condicoes = new List<WhereIfCondition<RespostaDoItemDeDeclaracaoDeSaude>>
            {
                new WhereIfCondition<RespostaDoItemDeDeclaracaoDeSaude>(!input.Observacao.IsNullOrEmpty(), r => r.Observacao.Contains(input.Observacao))
            };
            var result = await base.GetListPaged<RespostaDoItemDeDeclaracaoDeSaudeListDto, GetRespostaDoItemDeDeclaracaoDeSaudeInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input, condicoes);
            return result;
        }

        public async Task<RespostaDoItemDeDeclaracaoDeSaudeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RespostaDoItemDeDeclaracaoDeSaudeInput, IdInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_RespostaDoItemDeDeclaracaoDeSaude_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_respostaDoItemDeDeclaracaoDeSaudeService, input);
        }

        public async Task<RespostaDoItemDeDeclaracaoDeSaudeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RespostaDoItemDeDeclaracaoDeSaudeInput, IdInput>(_respostaDoItemDeDeclaracaoDeSaudeService, input);
        }
    }
}
