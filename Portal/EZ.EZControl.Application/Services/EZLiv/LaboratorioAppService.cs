﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Laboratorio;
using EZ.EZControl.Dto.EZLiv.Getal.Laboratorio;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Laboratorio, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class LaboratorioAppService : EZControlAppServiceBase<Laboratorio>, ILaboratorioAppService
    {
        private readonly ILaboratorioService _laboratorioService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IEnderecoService _enderecoService;
        private readonly ITelefoneService _telefoneService;
        private readonly IEspecialidadeService _especialidadeService;

        public LaboratorioAppService(IRepository<Laboratorio> LaboratorioRepository,
            ILaboratorioService laboratorioService,
            IPessoaJuridicaService pessoaJuridicaService,
            IEnderecoService enderecoService,
            ITelefoneService telefoneService,
            IEspecialidadeService especialidadeService)
            : base(LaboratorioRepository)
        {
            _laboratorioService = laboratorioService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _enderecoService = enderecoService;
            _telefoneService = telefoneService;
            _especialidadeService = especialidadeService;
        }

        private void ValidateInput(LaboratorioInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                if (String.IsNullOrEmpty(input.Nome))
                    validationErrors.Add(new ValidationResult(L("Laboratorio.NomeEmpty")));

                if (input.PessoaJuridicaId == default(int))
                    validationErrors.Add(new ValidationResult(L("Laboratorio.PessoaEmpty")));

                if (input.EnderecoId == default(int))
                    validationErrors.Add(new ValidationResult(L("Laboratorio.EnderecoEmpty")));

                if (!input.Telefone1Id.HasValue && !input.Telefone2Id.HasValue)
                    validationErrors.Add(new ValidationResult(L("Laboratorio.TelefoneEmpty")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Laboratorio laboratorio, LaboratorioInput input)
        {
            if (input.PessoaJuridicaId > 0)
            {
                try
                {
                    var pessoaJuridica = await _pessoaJuridicaService.GetById(input.PessoaJuridicaId);
                    if (pessoaJuridica != null)
                    {
                        laboratorio.PessoaJuridicaId = input.PessoaJuridicaId;
                        laboratorio.PessoaJuridica = pessoaJuridica;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Laboratorio.PessoaNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.EnderecoId > 0)
            {
                try
                {
                    var endereco = await _enderecoService.GetById(input.EnderecoId);
                    if (endereco != null)
                    {
                        laboratorio.EnderecoId = input.EnderecoId;
                        laboratorio.Endereco = endereco;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Laboratorio.EnderecoNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.Telefone1Id > 0)
            {
                try
                {
                    var telefone = await _telefoneService.GetById(input.Telefone1Id.Value);
                    if (telefone != null)
                    {
                        laboratorio.Telefone1Id = input.Telefone1Id;
                        laboratorio.Telefone1 = telefone;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Laboratorio.TelefoneNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
                laboratorio.Telefone1Id = null;

            if (input.Telefone2Id > 0)
            {
                try
                {
                    var telefone = await _telefoneService.GetById(input.Telefone2Id.Value);
                    if (telefone != null)
                    {
                        laboratorio.Telefone2Id = input.Telefone2Id;
                        laboratorio.Telefone2 = telefone;
                    }
                    else
                    {
                        throw new UserFriendlyException(L("Laboratorio.TelefoneNotFoundError"));
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else { laboratorio.Telefone2Id = null; }

            laboratorio.Especialidades.Clear();

            if (input.Especialidades != null && input.Especialidades.Any())
            {
                foreach (var item in input.Especialidades)
                {
                    var especialidade = await _especialidadeService.GetById(item.Id);
                    laboratorio.Especialidades.Add(especialidade);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Create, AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Edit)]
        public async Task<IdInput> Save(LaboratorioInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, LaboratorioInput>(_laboratorioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, LaboratorioInput>(_laboratorioService, input, () => ValidateInput(input), ProcessInput);
            }
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Create, AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Edit)]
        public async Task<LaboratorioInput> SaveAndReturnEntity(LaboratorioInput input)
        {
            LaboratorioInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<LaboratorioInput, LaboratorioInput>(_laboratorioService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<LaboratorioInput, LaboratorioInput>(_laboratorioService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<LaboratorioListDto>> GetPaginado(GetLaboratorioInput input)
        {
            var condicoes = new List<WhereIfCondition<Laboratorio>>
            {
                  new WhereIfCondition<Laboratorio>(
                    !input.Email.IsNullOrEmpty() ||
                    !input.Nome.IsNullOrEmpty() ||
                    !input.Site.IsNullOrEmpty(),
                    a =>
                        a.Email.ToLower().Contains(input.Email.ToLower()) ||
                        a.Nome.ToLower().Contains(input.Nome.ToLower()) ||
                        a.Site.ToLower().Contains(input.Site.ToLower())
                        )
            };
            return await base.GetListPaged<LaboratorioListDto, GetLaboratorioInput>(_laboratorioService, input, condicoes);
        }

        public async Task<LaboratorioInput> GetById(IdInput input)
        {
            return await base.GetEntityById<LaboratorioInput, IdInput>(_laboratorioService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Laboratorio_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_laboratorioService, input);
        }

        public async Task<LaboratorioInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<LaboratorioInput, IdInput>(_laboratorioService, input);
        }

        public virtual async Task<PagedResultDto<EspecialidadeListDto>> GetEspecialidadeExceptForLaboratorio(GetEspecialidadeExceptForLaboratorio input)
        {
            try
            {
                if (input == null)
                    throw new UserFriendlyException(L("Laboratorio.FiltroEspecialidadeNotFoundError"));

                var idsLaboratorio = input.Especialidades.Select(x => x.Id);

                var queryEspecialidade = _especialidadeService
                    .GetAll()
                    .Where(x => !idsLaboratorio.Contains(x.Id))
                    .WhereIf(!string.IsNullOrEmpty(input.Codigo) ||
                    !string.IsNullOrEmpty(input.Nome), a =>
                       a.Codigo.Contains(input.Codigo) ||
                       a.Nome.Contains(input.Nome))
                    .Select(x => new EspecialidadeListDto
                    {
                        Id = x.Id,
                        Codigo = x.Codigo,
                        Nome = x.Nome
                    });

                IQueryable<EspecialidadeListDto> query = queryEspecialidade;

                var count = await query.CountAsync();

                List<EspecialidadeListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<EspecialidadeListDto>>();
                return new PagedResultDto<EspecialidadeListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
