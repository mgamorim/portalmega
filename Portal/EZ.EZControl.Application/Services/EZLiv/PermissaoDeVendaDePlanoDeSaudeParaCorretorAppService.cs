﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretor;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretorAppService : EZControlAppServiceBase<PermissaoDeVendaDePlanoDeSaudeParaCorretor>, IPermissaoDeVendaDePlanoDeSaudeParaCorretorAppService
    {
        private readonly IPermissaoDeVendaDePlanoDeSaudeParaCorretorService _permissaoDeVendaDePlanoDeSaudeParaCorretorService;
        private readonly IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretor> _permissaoDeVendaDePlanoDeSaudeParaCorretorRepository;
        private readonly IProdutoDePlanoDeSaudeService _produtoService;
        private readonly ICorretoraService _corretoraService;
        private readonly ICorretorService _corretorService;
        private readonly IEmpresaService _empresaService;

        public PermissaoDeVendaDePlanoDeSaudeParaCorretorAppService(IRepository<PermissaoDeVendaDePlanoDeSaudeParaCorretor> permissaoDeVendaDePlanoDeSaudeParaCorretorRepository,
                                  IPermissaoDeVendaDePlanoDeSaudeParaCorretorService permissaoDeVendaDePlanoDeSaudeParaCorretorService,
                                  IProdutoDePlanoDeSaudeService produtoService,
                                  ICorretoraService corretoraService,
                                  ICorretorService corretorService,
                                  IEmpresaService empresaService)
            : base(permissaoDeVendaDePlanoDeSaudeParaCorretorRepository)
        {
            _permissaoDeVendaDePlanoDeSaudeParaCorretorService = permissaoDeVendaDePlanoDeSaudeParaCorretorService;
            _permissaoDeVendaDePlanoDeSaudeParaCorretorRepository = permissaoDeVendaDePlanoDeSaudeParaCorretorRepository;
            _produtoService = produtoService;
            _corretoraService = corretoraService;
            _corretorService = corretorService;
            _empresaService = empresaService;
        }

        private void ValidateInput(PermissaoDeVendaDePlanoDeSaudeParaCorretorInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (!input.Produtos.Any())
                validationErrors.Add(new ValidationResult(L("PermissaoDeVendaDePlanoDeSaudeParaCorretor.NotFoundProdutos")));

            if (!input.Corretores.Any())
                validationErrors.Add(new ValidationResult(L("PermissaoDeVendaDePlanoDeSaudeParaCorretor.NotFoundCorretoras")));

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("PermissaoDeVendaDePlanoDeSaudeParaCorretor.EmptyNomeError")));

            if (string.IsNullOrEmpty(input.Comissao))
                validationErrors.Add(new ValidationResult("Comissão para o Grupo não Informado"));

            if (input.Comissao.Where(c => char.IsLetter(c)).Count() > 0)
                validationErrors.Add(new ValidationResult("Favor Informar Somente Numeros!"));


            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(PermissaoDeVendaDePlanoDeSaudeParaCorretor entity, PermissaoDeVendaDePlanoDeSaudeParaCorretorInput input)
        {
            var empresa = await _empresaService.GetEmpresaLogada();
            if (empresa != null)
            {
                entity.Empresa = empresa;
                entity.EmpresaId = empresa.Id;
                entity.TenantId = AbpSession.TenantId.Value;
                var corretoras = await _corretoraService.GetAllListAsync(x => x.PessoaJuridicaId == empresa.PessoaJuridicaId.Value);

                if (!corretoras.Any())
                    throw new UserFriendlyException(L("PermissaoDeVendaDePlanoDeSaudeParaCorretor.EmptyCorretoraError"));

                entity.Corretora = corretoras.FirstOrDefault();
            }

            entity.Produtos.Clear();
            foreach (var produtoInput in input.Produtos)
            {
                var produto = await _produtoService.GetById(produtoInput.Id);
                entity.Produtos.Add(produto);
            }

            entity.Corretores.Clear();
            foreach (var corretorInput in input.Corretores)
            {
                var corretor = await _corretorService.GetById(corretorInput.Id);
                entity.Corretores.Add(corretor);
            }
        }


        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create, AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit)]
        public async Task<IdInput> Save(PermissaoDeVendaDePlanoDeSaudeParaCorretorInput input)
        {
            try
            {
                IdInput result = null;
                if (input.Id == default(int))
                {
                    if (
                        !PermissionChecker.IsGranted(
                            AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    result =
                        await base.CreateEntity<IdInput, PermissaoDeVendaDePlanoDeSaudeParaCorretorInput>(
                            _permissaoDeVendaDePlanoDeSaudeParaCorretorService, input,
                            () => ValidateInput(input), ProcessInput);
                }
                else
                {
                    if (
                        !PermissionChecker.IsGranted(
                            AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    result =
                        await base.UpdateEntity<IdInput, PermissaoDeVendaDePlanoDeSaudeParaCorretorInput>(
                            _permissaoDeVendaDePlanoDeSaudeParaCorretorService, input, () => ValidateInput(input), ProcessInput);
                }

                return result;
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create, AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit)]
        public async Task<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> SaveAndReturnEntity(PermissaoDeVendaDePlanoDeSaudeParaCorretorInput input)
        {
            PermissaoDeVendaDePlanoDeSaudeParaCorretorInput permissao = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                permissao = await base.CreateAndReturnEntity<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, PermissaoDeVendaDePlanoDeSaudeParaCorretorInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                permissao = await base.UpdateAndReturnEntity<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, PermissaoDeVendaDePlanoDeSaudeParaCorretorInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input, () => ValidateInput(input));
            }

            return permissao;
        }

        public async Task<PagedResultDto<PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto>> GetPaginado(GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput input)
        {
            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value;

            var condicoes = new List<WhereIfCondition<PermissaoDeVendaDePlanoDeSaudeParaCorretor>>
            {
                new WhereIfCondition<PermissaoDeVendaDePlanoDeSaudeParaCorretor>(
                        !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Nome.Contains(input.Nome)),
                new WhereIfCondition<PermissaoDeVendaDePlanoDeSaudeParaCorretor>(true, r => r.EmpresaId == empresaId)
        };

            var result = await base.GetListPaged<PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, GetPermissaoDeVendaDePlanoDeSaudeParaCorretorInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input, condicoes);
            return result;
        }

        public async Task<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> GetById(IdInput input)
        {
            var permissoes = await base.GetEntityById<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, IdInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input);
            return permissoes;
        }

        public async Task<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, IdInput>(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_PermissaoDeVendaDePlanoDeSaudeParaCorretor_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_permissaoDeVendaDePlanoDeSaudeParaCorretorService, input);
        }
    }
}
