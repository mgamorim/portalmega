﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZLiv
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Operadora, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class OperadoraAppService : EZControlAppServiceBase<Operadora>, IOperadoraAppService
    {
        private readonly IOperadoraService _operadoraService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;
        private readonly IArquivoGlobalService _arquivoGlobalService;

        public OperadoraAppService(IRepository<Operadora> operadoraRepository,
                                   IOperadoraService operadoraService,
                                   IPessoaJuridicaService pessoaJuridicaService,
                                   IProdutoDePlanoDeSaudeService produtoDePlanoDeSaudeService,
                                   IArquivoGlobalService arquivoGlobalService)
            : base(operadoraRepository)
        {
            this._operadoraService = operadoraService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _produtoDePlanoDeSaudeService = produtoDePlanoDeSaudeService;
            _arquivoGlobalService = arquivoGlobalService;
        }

        private void ValidateInput(OperadoraInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(string.Format(L("Operadora.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.CodigoAns))
                validationErrors.Add(new ValidationResult(string.Format(L("Operadora.EmptyCodigoAnsError"), input.Nome), new[] { "codigoAns".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
                validationErrors.Add(new ValidationResult(string.Format(L("Operadora.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (DynamicQueryable.Any(validationErrors))
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Operadora operadora, OperadoraInput operadoraInput)
        {
            try
            {
                var idPessoa = (operadora.PessoaJuridica != null && operadora.PessoaJuridica.Id > 0)
                    ? operadora.PessoaJuridica.Id
                    : (operadoraInput.PessoaJuridica != null && operadoraInput.PessoaJuridica.Id > 0)
                        ? operadoraInput.PessoaJuridica.Id
                        : 0;

                if (idPessoa > 0)
                {
                    var pj = await _pessoaJuridicaService.GetById(idPessoa);
                    operadora.PessoaJuridica = pj;
                }

                //Imagem
                if (operadoraInput.ImagemId > 0)
                {
                    try
                    {
                        var imagem = await _arquivoGlobalService.GetById(operadoraInput.ImagemId);
                        if (imagem != null)
                        {
                            operadora.Imagem = imagem;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }
                }


                //if (operadoraInput.ProdutosDePlanoDeSaudeIds != null &&
                //    operadoraInput.ProdutosDePlanoDeSaudeIds.Any())
                //{
                //    operadora.ProdutosDePlanoDeSaude = new List<ProdutoDePlanoDeSaude>();
                //    operadoraInput.ProdutosDePlanoDeSaudeIds.ExecuteForEach(id =>
                //    {
                //        try
                //        {
                //            var produtos = _produtoDePlanoDeSaudeService.GetAll().FirstOrDefault(x => x.Id == id);
                //            operadora.ProdutosDePlanoDeSaude.Add(produtos);
                //        }
                //        catch (Exception ex)
                //        {
                //            throw new UserFriendlyException(L("Operadora.ProdutoDePlanoDeSaudeNotFound"), ex.Message);
                //        }
                //    });
                //}
                //else
                //    throw new UserFriendlyException(L("Operadora.RequiredProdutoDePlanoDeSaudeError"));
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Operadora_Create, AppPermissions.Pages_Tenant_EZLiv_Operadora_Edit)]
        public async Task<OperadoraPessoaIdDto> Save(OperadoraInput input)
        {
            OperadoraListDto operadora = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Operadora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                operadora = await base.CreateAndReturnEntity<OperadoraListDto, OperadoraInput>(_operadoraService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Operadora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                operadora = await base.UpdateAndReturnEntity<OperadoraListDto, OperadoraInput>(_operadoraService, input, () => ValidateInput(input), ProcessInput);
            }


            OperadoraPessoaIdDto result = new OperadoraPessoaIdDto
            {
                PessoaId = operadora.Id,
                OperadoraId = operadora.Id
            };

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Operadora_Create, AppPermissions.Pages_Tenant_EZLiv_Operadora_Edit)]
        public async Task<OperadoraListDto> SaveAndReturnEntity(OperadoraInput input)
        {
            OperadoraListDto result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Operadora_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<OperadoraListDto, OperadoraInput>(_operadoraService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_EZLiv_Operadora_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<OperadoraListDto, OperadoraInput>(_operadoraService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<OperadoraListDto>> GetOperadorasPaginado(GetOperadoraInput input)
        {
            var condicoes = new List<WhereIfCondition<Operadora>>
            {
                new WhereIfCondition<Operadora>(
                    !string.IsNullOrEmpty(input.Nome) ||
                    !string.IsNullOrEmpty(input.CodigoAns),
                    a =>
                        a.Nome.Contains(input.Nome) ||
                        a.CodigoAns.Contains(input.CodigoAns))
            };
            var result = await base.GetListPaged<OperadoraListDto, GetOperadoraInput>(_operadoraService, input, condicoes);
            return result;
        }

        public async Task<OperadoraInput> GetById(IdInput input)
        {
            return await base.GetEntityById<OperadoraInput, IdInput>(_operadoraService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_EZLiv_Operadora_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_operadoraService, input);
        }

        public async Task<OperadoraInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<OperadoraInput, IdInput>(_operadoraService, input);
        }

        public async Task<ListResultDto<OperadoraListDto>> GetAll()
        {
            var operadoras = await _operadoraService.GetAllListAsync();
            var operadorasFinal = new List<OperadoraListDto>();
            foreach (var operadora in operadoras.OrderBy(x => x.Nome))
            {
                var imagem = new ArquivoGlobal();
                var urlImg = "https://www.theclementimall.com/assets/camaleon_cms/image-not-found-4a963b95bf081c3ea02923dceaeb3f8085e1a654fc54840aac61a57a60903fef.png";
                if (operadora.ImagemId > 0)
                {
                    imagem = await _arquivoGlobalService.GetById(operadora.ImagemId.Value);
                    urlImg = imagem.Path;
                }
                var operadoraFinal = operadora.MapTo<OperadoraListDto>();
                operadoraFinal.UrlImagem = urlImg;
                operadorasFinal.Add(operadoraFinal);

            }
            return new ListResultDto<OperadoraListDto>(operadorasFinal);
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForOperadora(GetPessoaExceptForOperadora input)
        {
            try
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    if (input == null)
                        throw new Exception("É necessário definir um critério para buscar por pessoas.");

                    var idsOperadoras = Repository.GetAll().Select(y => y.PessoaJuridica.Id);

                    var queryPessoaJuridica = _pessoaJuridicaService
                        .GetAll()
                        .Where(x => !idsOperadoras.Contains(x.Id))
                        .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.RazaoSocial.Contains(input.NomePessoa) || x.NomeFantasia.Contains(input.NomePessoa))
                        .Select(x => new PessoaJuridicaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.RazaoSocial,
                            RazaoSocial = x.RazaoSocial,
                            NomeFantasia = x.NomeFantasia,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Juridica
                        });

                    IQueryable<PessoaListDto> query = queryPessoaJuridica;

                    var count = await query.CountAsync();

                    List<PessoaListDto> dados = await query
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    var listDtos = dados.MapTo<List<PessoaListDto>>();
                    return new PagedResultDto<PessoaListDto>(count, listDtos);
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
