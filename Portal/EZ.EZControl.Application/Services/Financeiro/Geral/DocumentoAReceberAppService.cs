﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using EZ.EZControl.Services.Financeiro.Geral.Interfaces;
using EZ.EZControl.Services.Financiero.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    public class DocumentoAReceberAppService : EZControlAppServiceBase<DocumentoAReceber>, IDocumentoAReceberAppService
    {
        private readonly IDocumentoAReceberService _documentoBaseService;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IClienteService _clienteService;
        public DocumentoAReceberAppService(
            IRepository<DocumentoAReceber, int> repository,
            IDocumentoAReceberService documentoBaseService,
            ITipoDeDocumentoService tipoDeDocumentoService,
            IClienteService clienteService)
            : base(repository)
        {
            _documentoBaseService = documentoBaseService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _clienteService = clienteService;
        }

        private void ValidateLancamentoInput(DocumentoAReceberInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Numero))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyNumeroError"), input.Numero),
                    new[] { "numero".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyDescricaoError"), input.Descricao),
                    new[] { "descricao".ToCamelCase() }));
            }
            
            if (input.TipoDeDocumentoFinanceiroId == 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyTipoDeDocumentoFinanceiroError"), input.TipoDeDocumentoFinanceiroId),
                    new[] { "tipoDeDocumentoFinanceiro".ToCamelCase() }));
            }

            if (input.ClienteId == 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("DocumentoAReceber.EmptyClienteError"), input.ClienteId),
                    new[] { "cliente".ToCamelCase() }));
            }

            if (!_documentoBaseService.CanCreateDocumentoAReceber(input.Id, input.Numero, input.Complemento, input.ClienteId))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("DocumentoAReceber.DuplicateDocumentoError"), input.Numero, input.Complemento),
                    new[] { "numero".ToCamelCase(), "complemento".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private void ValidateEstornoInput(DocumentoAReceberInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Estornado && input.DataEstorno == null)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyDataEstornoError"), input.DataEstorno),
                    new[] { "dataEstorno".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(DocumentoAReceber documento, DocumentoAReceberInput input)
        {
            if (input.TipoDeDocumentoFinanceiroId > 0)
            {
                try
                {
                    documento.TipoDeDocumentoFinanceiro = await _tipoDeDocumentoService.GetAll().Where(t => t.Id == input.TipoDeDocumentoFinanceiroId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("TipoDeDocumentoFinanceiro.NotFoundError"), ex.Message);
                }
            }

            if (input.ClienteId > 0)
            {
                try
                {
                    documento.Cliente = await _clienteService.GetAll().Where(f => f.Id == input.ClienteId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Cliente.NotFoundError"), ex.Message);
                }
            }
        }

        public async Task<DocumentoAReceberInput> GetById(IdInput input)
        {
            var documentoBase = await _documentoBaseService.GetById(input.Id);
            return documentoBase.MapTo<DocumentoAReceberInput>();
        }

        public async Task<ListResultDto<DocumentoAReceberListDto>> GetPaginado(GetDocumentoAReceberInput input)
        {
            var condicoes = new List<WhereIfCondition<DocumentoAReceber>>();
            condicoes.Add(new WhereIfCondition<DocumentoAReceber>(!string.IsNullOrEmpty(input.Numero), d => d.Numero.Contains(input.Numero)));
            var result = await base.GetListPaged<DocumentoAReceberListDto, GetDocumentoAReceberInput>(_documentoBaseService, input, condicoes);
            return result;
        }

        public async Task<IdInput> Save(DocumentoAReceberInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                result = await base.CreateEntity<IdInput, DocumentoAReceberInput>(_documentoBaseService, input, () => ValidateLancamentoInput(input), ProcessInput);
            }
            else
            {
                result = await base.UpdateEntity<IdInput, DocumentoAReceberInput>(_documentoBaseService, input, () => ValidateLancamentoInput(input), ProcessInput);
            }

            return result;
        }
        public async Task<IdInput> Estornar(DocumentoAReceberInput input)
        {
            //using (var tran = UnitOfWorkManager.Begin())
            //{
            //    try
            //    {
            var result = await base.UpdateEntity<IdInput, DocumentoAReceberInput>(_documentoBaseService, input, () => ValidateLancamentoInput(input), ProcessInput);

            var documentoEstorno = input;
            documentoEstorno.DataEstorno = null;
            documentoEstorno.Valor = documentoEstorno.Valor * -1;
            documentoEstorno.Id = 0;
            documentoEstorno.Estornado = false;

            var documentoEstornado = await base.CreateEntity<IdInput, DocumentoAReceberInput>(_documentoBaseService, documentoEstorno, null, ProcessInput);

            //tran.Complete();

            return result;
            //    }
            //    catch (Exception ex)
            //    {
            //        throw new UserFriendlyException(ex.Message);
            //    } 
            //}
        }
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity<IdInput>(_documentoBaseService, input);
        }
    }
}