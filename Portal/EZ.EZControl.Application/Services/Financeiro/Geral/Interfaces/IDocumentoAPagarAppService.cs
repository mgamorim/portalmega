﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Financeiro.Geral.Interfaces
{
    public interface IDocumentoAPagarAppService : IApplicationService
    {
        Task<DocumentoAPagarInput> GetById(IdInput input);
        Task<ListResultDto<DocumentoAPagarListDto>> GetPaginado(GetDocumentoAPagarInput input);
        Task<IdInput> Save(DocumentoAPagarInput input);
        Task<IdInput> Estornar(DocumentoAPagarInput input);
        Task Delete(IdInput input);
    }
}