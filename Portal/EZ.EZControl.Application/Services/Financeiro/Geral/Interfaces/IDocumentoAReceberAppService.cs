﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Financeiro.Geral.Interfaces
{
    public interface IDocumentoAReceberAppService : IApplicationService
    {
        Task<DocumentoAReceberInput> GetById(IdInput input);
        Task<ListResultDto<DocumentoAReceberListDto>> GetPaginado(GetDocumentoAReceberInput input);
        Task<IdInput> Save(DocumentoAReceberInput input);
        Task<IdInput> Estornar(DocumentoAReceberInput input);
        Task Delete(IdInput input);
    }
}