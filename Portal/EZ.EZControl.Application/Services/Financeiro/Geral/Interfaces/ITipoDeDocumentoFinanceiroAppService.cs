﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Financeiro.Geral.Interfaces
{
    public interface ITipoDeDocumentoFinanceiroAppService : IApplicationService
    {
        Task<TipoDeDocumentoFinanceiroInput> GetById(IdInput input);
        Task<ListResultDto<TipoDeDocumentoFinanceiroListDto>> GetPaginado(GetTipoDeDocumentoFinanceiroInput input);
        Task<IdInput> Save(TipoDeDocumentoFinanceiroInput input);
        Task Delete(IdInput input);
    }
}