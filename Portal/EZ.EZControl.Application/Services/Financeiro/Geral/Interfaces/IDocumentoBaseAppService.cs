﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Financeiro.Geral.Interfaces
{
    public interface IDocumentoBaseAppService : IApplicationService
    {
        Task<DocumentoBaseInput> GetById(IdInput input);
        Task<ListResultDto<DocumentoBaseListDto>> GetPaginado(GetDocumentoBaseInput input);
        Task<IdInput> Save(DocumentoBaseInput input);
        Task Delete(IdInput input);
    }
}