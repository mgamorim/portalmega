﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using EZ.EZControl.Services.Financeiro.Geral.Interfaces;
using EZ.EZControl.Services.Financiero.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    public class DocumentoBaseAppService : EZControlAppServiceBase<DocumentoBase>, IDocumentoBaseAppService
    {
        private readonly IDocumentoBaseService _documentoBaseService;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        public DocumentoBaseAppService(
            IRepository<DocumentoBase, int> repository,
            IDocumentoBaseService documentoBaseService,
            ITipoDeDocumentoService tipoDeDocumentoService)
            : base(repository)
        {
            _documentoBaseService = documentoBaseService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
        }

        private void ValidateInput(DocumentoBaseInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Numero))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyNumeroError"), input.Numero),
                    new[] { "numero".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyDescricaoError"), input.Descricao),
                    new[] { "descricao".ToCamelCase() }));
            }
            
            if (input.TipoDeDocumentoFinanceiroId == 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyTipoDeDocumentoFinanceiroError"), input.TipoDeDocumentoFinanceiroId),
                    new[] { "tipoDeDocumentoFinanceiro".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(DocumentoBase documento, DocumentoBaseInput input)
        {
            if (input.TipoDeDocumentoFinanceiroId > 0)
            {
                try
                {
                    documento.TipoDeDocumentoFinanceiro = await _tipoDeDocumentoService.GetAll().Where(t => t.Id == input.TipoDeDocumentoFinanceiroId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("TipoDeDocumentoFinanceiro.NotFoundError"), ex.Message);
                }
            }
        }

        public async Task<DocumentoBaseInput> GetById(IdInput input)
        {
            var documentoBase = await _documentoBaseService.GetById(input.Id);
            return documentoBase.MapTo<DocumentoBaseInput>();
        }

        public async Task<ListResultDto<DocumentoBaseListDto>> GetPaginado(GetDocumentoBaseInput input)
        {
            var condicoes = new List<WhereIfCondition<DocumentoBase>>();
            condicoes.Add(new WhereIfCondition<DocumentoBase>(!string.IsNullOrEmpty(input.Numero), d => d.Numero.Contains(input.Numero)));
            var result = await base.GetListPaged<DocumentoBaseListDto, GetDocumentoBaseInput>(_documentoBaseService, input, condicoes);
            return result;
        }

        public async Task<IdInput> Save(DocumentoBaseInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                result = await base.CreateEntity<IdInput, DocumentoBaseInput>(_documentoBaseService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                result = await base.UpdateEntity<IdInput, DocumentoBaseInput>(_documentoBaseService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity<IdInput>(_documentoBaseService, input);
        }
    }
}