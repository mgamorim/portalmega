﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro;
using EZ.EZControl.Services.Financeiro.Geral.Interfaces;
using EZ.EZControl.Services.Financiero.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TipoDeDocumentoFinanceiroAppService : EZControlAppServiceBase<TipoDeDocumentoFinanceiro>, ITipoDeDocumentoFinanceiroAppService
    {
        private readonly ITipoDeDocumentoService _tipoDeDocumentoFinanceiroService;
        public TipoDeDocumentoFinanceiroAppService(
            IRepository<TipoDeDocumentoFinanceiro, int> repository,
            ITipoDeDocumentoService tipoDeDocumentoFinanceiroService)
            : base(repository)
        {
            _tipoDeDocumentoFinanceiroService = tipoDeDocumentoFinanceiroService;
        }

        private void ValidateInput(TipoDeDocumentoFinanceiroInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("TipoDeDocumentoFinanceiro.EmptyNomeError"), input.Nome),
                    new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(TipoDeDocumentoFinanceiro entity, TipoDeDocumentoFinanceiroInput input)
        {
            if (!_tipoDeDocumentoFinanceiroService.CanCreate(input.Nome, input.Tipo))
            {
                throw new UserFriendlyException(String.Format(L("TipoDeDocumentoFinanceiro.DuplicateNomeETipoError"), input.Nome, input.Tipo.ToString()));
            }
        }

        public async Task<TipoDeDocumentoFinanceiroInput> GetById(IdInput input)
        {
            var tipoDeDocumento = await _tipoDeDocumentoFinanceiroService.GetById(input.Id);
            return tipoDeDocumento.MapTo<TipoDeDocumentoFinanceiroInput>();
        }

        public async Task<ListResultDto<TipoDeDocumentoFinanceiroListDto>> GetPaginado(GetTipoDeDocumentoFinanceiroInput input)
        {
            var condicoes = new List<WhereIfCondition<TipoDeDocumentoFinanceiro>>();
            if (input.Tipo == TipoDeDocumentoFinanceiroEnum.APagar)
            {
                condicoes.Add(new WhereIfCondition<TipoDeDocumentoFinanceiro>(true, t => t.Tipo == TipoDeDocumentoFinanceiroEnum.APagar));
            }
            if (input.Tipo == TipoDeDocumentoFinanceiroEnum.AReceber)
            {
                condicoes.Add(new WhereIfCondition<TipoDeDocumentoFinanceiro>(true, t => t.Tipo == TipoDeDocumentoFinanceiroEnum.AReceber));
            }
            condicoes.Add(new WhereIfCondition<TipoDeDocumentoFinanceiro>(!string.IsNullOrEmpty(input.Nome), t => t.Nome.Contains(input.Nome)));
            var result = await base.GetListPaged<TipoDeDocumentoFinanceiroListDto, GetTipoDeDocumentoFinanceiroInput>(_tipoDeDocumentoFinanceiroService, input, condicoes);
            return result;
        }

        public async Task<IdInput> Save(TipoDeDocumentoFinanceiroInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, TipoDeDocumentoFinanceiroInput>(_tipoDeDocumentoFinanceiroService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento_Edit))
                    throw new UserFriendlyException(L("PermissionToEditRecord"));

                result = await base.UpdateEntity<IdInput, TipoDeDocumentoFinanceiroInput>(_tipoDeDocumentoFinanceiroService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenant_Financeiro_TipoDeDocumento_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity<IdInput>(_tipoDeDocumentoFinanceiroService, input);
        }
    }
}