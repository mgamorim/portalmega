﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using EZ.EZControl.Services.Financeiro.Geral.Interfaces;
using EZ.EZControl.Services.Financiero.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.EZMedical
{
    public class DocumentoAPagarAppService : EZControlAppServiceBase<DocumentoAPagar>, IDocumentoAPagarAppService
    {
        private readonly IDocumentoAPagarService _documentoBaseService;
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IFornecedorService _fornecedorService;
        public DocumentoAPagarAppService(
            IRepository<DocumentoAPagar, int> repository,
            IDocumentoAPagarService documentoBaseService,
            ITipoDeDocumentoService tipoDeDocumentoService,
            IFornecedorService fornecedorService)
            : base(repository)
        {
            _documentoBaseService = documentoBaseService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _fornecedorService = fornecedorService;
        }

        private void ValidateLancamentoInput(DocumentoAPagarInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Numero))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyNumeroError"), input.Numero),
                    new[] { "numero".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyDescricaoError"), input.Descricao),
                    new[] { "descricao".ToCamelCase() }));
            }

            if (input.TipoDeDocumentoFinanceiroId == 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyTipoDeDocumentoFinanceiroError"), input.TipoDeDocumentoFinanceiroId),
                    new[] { "tipoDeDocumentoFinanceiro".ToCamelCase() }));
            }

            if (input.FornecedorId == 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("DocumentoAPagar.EmptyFornecedorError"), input.FornecedorId),
                    new[] { "fornecedor".ToCamelCase() }));
            }

            if (!_documentoBaseService.CanCreateDocumentoAPagar(input.Id, input.Numero, input.Complemento, input.FornecedorId))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("DocumentoAPagar.DuplicateDocumentoError"), input.Numero, input.Complemento),
                    new[] { "numero".ToCamelCase(), "complemento".ToCamelCase() }));
            }

            if (input.Valor == 0)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.InvalidValorError"), input.Valor),
                    new[] { "valor".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private void ValidateEstornoInput(DocumentoAPagarInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.Estornado && input.DataEstorno == null)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Documento.EmptyDataEstornoError"), input.DataEstorno),
                    new[] { "dataEstorno".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(DocumentoAPagar documento, DocumentoAPagarInput input)
        {
            if (input.TipoDeDocumentoFinanceiroId > 0)
            {
                try
                {
                    documento.TipoDeDocumentoFinanceiro = await _tipoDeDocumentoService.GetAll().Where(t => t.Id == input.TipoDeDocumentoFinanceiroId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("TipoDeDocumentoFinanceiro.NotFoundError"), ex.Message);
                }
            }

            if (input.FornecedorId > 0)
            {
                try
                {
                    documento.Fornecedor = await _fornecedorService.GetAll().Where(f => f.Id == input.FornecedorId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Fornecedor.NotFoundError"), ex.Message);
                }
            }
        }

        public async Task<DocumentoAPagarInput> GetById(IdInput input)
        {
            var documentoBase = await _documentoBaseService.GetById(input.Id);
            return documentoBase.MapTo<DocumentoAPagarInput>();
        }

        public async Task<ListResultDto<DocumentoAPagarListDto>> GetPaginado(GetDocumentoAPagarInput input)
        {
            var condicoes = new List<WhereIfCondition<DocumentoAPagar>>();
            condicoes.Add(new WhereIfCondition<DocumentoAPagar>(!string.IsNullOrEmpty(input.Numero), d => d.Numero.Contains(input.Numero)));
            var result = await base.GetListPaged<DocumentoAPagarListDto, GetDocumentoAPagarInput>(_documentoBaseService, input, condicoes);
            return result;
        }
        public async Task<IdInput> Save(DocumentoAPagarInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                result = await base.CreateEntity<IdInput, DocumentoAPagarInput>(_documentoBaseService, input, () => ValidateLancamentoInput(input), ProcessInput);
            }
            else
            {
                result = await base.UpdateEntity<IdInput, DocumentoAPagarInput>(_documentoBaseService, input, () => ValidateLancamentoInput(input), ProcessInput);
            }

            return result;
        }

        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity<IdInput>(_documentoBaseService, input);
        }
        //[UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Estornar(DocumentoAPagarInput input)
        {
            //using (var tran = UnitOfWorkManager.Begin())
            //{
            //    try
            //    {
            var result = await base.UpdateEntity<IdInput, DocumentoAPagarInput>(_documentoBaseService, input, () => ValidateLancamentoInput(input), ProcessInput);

            var documentoEstorno = input;
            documentoEstorno.DataEstorno = null;
            documentoEstorno.Valor = documentoEstorno.Valor * -1;
            documentoEstorno.Id = 0;
            documentoEstorno.Estornado = false;

            var documentoEstornado = await base.CreateEntity<IdInput, DocumentoAPagarInput>(_documentoBaseService, documentoEstorno, null, ProcessInput);

            //tran.Complete();

            return result;
            //    }
            //    catch (Exception ex)
            //    {
            //        throw new UserFriendlyException(ex.Message);
            //    } 
            //}
        }
    }
}