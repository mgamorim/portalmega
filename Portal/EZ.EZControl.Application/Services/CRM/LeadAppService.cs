﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CRM.Geral;
using EZ.EZControl.Dto.CRM.Geral.Lead;
using EZ.EZControl.Services.CRM.Interfaces;
using EZ.EZControl.Services.Global.Pessoa;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CRM
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CRM_Lead, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class LeadAppService : EZControlAppServiceBase<Lead>, ILeadAppService
    {
        private readonly LeadService _leadService;
        private readonly PessoaFisicaService _pessoaFisicaService;
        private readonly IRepository<Lead> _leadRepository;
        public LeadAppService(IRepository<Lead> leadRepository,
                              PessoaFisicaService pessoaFisicaService,
                              LeadService leadService)
            : base(leadRepository)
        {
            _leadService = leadService;
            _leadRepository = leadRepository;
            _pessoaFisicaService = pessoaFisicaService;
        }

        private void ValidateInput(LeadInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.PessoaFisicaId == 0)
            {
                validationErrors.Add(new ValidationResult(L("Lead.EmptyPessoaFisicaError")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Lead entity, LeadInput input)
        {
            if (input.PessoaFisicaId > 0)
            {
                var pessoaFisica = await _pessoaFisicaService.GetById(input.PessoaFisicaId);
                if (pessoaFisica != null)
                    entity.PessoaFisica = pessoaFisica;
                else
                    throw new UserFriendlyException(L("Lead.PessoaFisicaNotFoundError"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CRM_Lead_Create, AppPermissions.Pages_Tenant_CRM_Lead_Edit)]
        public async Task<IdInput> Save(LeadInput input)
        {
            try
            {
                IdInput result = null;
                using (var uow = this.UnitOfWorkManager.Begin())
                {

                    if (input.Id == default(int))
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CRM_Lead_Create))
                            throw new UserFriendlyException(L("PermissionToCreateRecord"));

                        result = await base.CreateEntity<IdInput, LeadInput>(_leadService, input, () => ValidateInput(input), ProcessInput);
                    }
                    else
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CRM_Lead_Edit))
                            throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                        result = await base.UpdateEntity<IdInput, LeadInput>(_leadService, input, () => ValidateInput(input), ProcessInput);
                    }

                    await uow.CompleteAsync();
                }

                return result;
            }
            catch (Exception ex)
            {

                throw new UserFriendlyException(ex.Message);
            }

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CRM_Lead_Create, AppPermissions.Pages_Tenant_CRM_Lead_Edit)]
        public async Task<LeadInput> SaveAndReturnEntity(LeadInput input)
        {
            LeadInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CRM_Lead_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<LeadInput, LeadInput>(_leadService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CRM_Lead_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<LeadInput, LeadInput>(_leadService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<LeadListDto>> GetPaginado(GetLeadInput input)
        {
            var condicoes = new List<WhereIfCondition<Lead>>
            {
                new WhereIfCondition<Lead>()
            };
            var result = await base.GetListPaged<LeadListDto, GetLeadInput>(_leadService, input, condicoes);
            return result;
        }

        public async Task<LeadInput> GetById(IdInput input)
        {
            return await base.GetEntityById<LeadInput, IdInput>(_leadService, input);
        }

        public async Task<LeadInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<LeadInput, IdInput>(_leadService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CRM_Lead_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_leadService, input);
        }
    }
}
