﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CRM.Geral.Lead;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CRM.Interfaces
{
    public interface ILeadAppService : IApplicationService
    {
        Task<IdInput> Save(LeadInput input);

        Task<LeadInput> SaveAndReturnEntity(LeadInput input);

        Task<PagedResultDto<LeadListDto>> GetPaginado(GetLeadInput input);

        Task Delete(IdInput input);

        Task<LeadInput> GetById(IdInput input);

        Task<LeadInput> GetByExternalId(IdInput input);
    }
}
