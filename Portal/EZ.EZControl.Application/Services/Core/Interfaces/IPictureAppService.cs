﻿using Abp.Application.Services;
using EZ.EZControl.Dto.Core.Geral.Picture;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Core.Interfaces
{
    public interface IPictureAppService : IApplicationService
    {
        Task<int> InsertOrUpdatePicture(PictureInput input);
    }
}
