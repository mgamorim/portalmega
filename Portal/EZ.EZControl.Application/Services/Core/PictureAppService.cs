﻿using Abp.Domain.Repositories;
using Abp.IO;
using Abp.UI;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Dto.Core.Geral.Picture;
using EZ.EZControl.Services.Core.Geral.Interfaces;
using EZ.EZControl.Services.Core.Interfaces;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Core
{
    public class PictureAppService : EZControlAppServiceBase<Picture>, IPictureAppService
    {
        private readonly IPictureService _pictureService;
        private readonly IAppFolders _appFolders;

        public PictureAppService(IRepository<Picture, int> repository,
                                 IPictureService pictureService, IAppFolders appFolders)
            : base(repository)
        {
            _pictureService = pictureService;
            _appFolders = appFolders;
        }

        public async Task<int> InsertOrUpdatePicture(PictureInput input)
        {
            var tempProfilePicturePath = Path.Combine(_appFolders.TempFileDownloadFolder, input.FileName);

            byte[] byteArray;

            using (var fsTempProfilePicture = new FileStream(tempProfilePicturePath, FileMode.Open))
            {
                using (var bmpImage = new Bitmap(fsTempProfilePicture))
                {
                    var width = input.Width == 0 ? bmpImage.Width : input.Width;
                    var height = input.Height == 0 ? bmpImage.Height : input.Height;
                    var bmCrop = bmpImage.Clone(new Rectangle(input.X, input.Y, width, height), bmpImage.PixelFormat);

                    using (var stream = new MemoryStream())
                    {
                        bmCrop.Save(stream, bmpImage.RawFormat);
                        stream.Close();
                        byteArray = stream.ToArray();
                    }
                }
            }

            if (byteArray.LongLength > 102400) //100 KB
                throw new UserFriendlyException(L("ResizedProfilePicture_Warn_SizeLimit"));

            if (input.Id > 0)
                await _pictureService.Delete(input.Id);

            var storedFile = new Picture(byteArray);
            var result = await _pictureService.CreateEntity(storedFile);

            FileHelper.DeleteIfExists(tempProfilePicturePath);

            return result;
        }
    }
}
