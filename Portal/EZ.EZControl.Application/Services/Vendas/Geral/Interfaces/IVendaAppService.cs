﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZPag.Geral.Deposito;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Vendas.Geral.Interfaces
{
    public interface IVendaAppService : IApplicationService
    {
        /// <summary>
        /// Realiza a venda de um produto de plano de saúde a partir das informações contidas na Proposta
        /// </summary>
        /// <param name="input">IdInput com o Id da Proposta de Contratação</param>
        /// <returns>IdInput com o Id do Pedido</returns>
        Task<PedidoInput> RealizaVendaProdutoPlanoDeSaude(IdInput input);
        Task<DepositoInput> RealizaVendaProdutoPlanoDeSaudeDeposito(DepositoInput input);
        Task<DepositoInput> ConfirmaRecebimentoVendaProdutoPlanoDeSaudeDeposito(DepositoInput input);
        Task<DepositoInput> GetDadosDeposito(IdInput input);
        Dto.EZPag.Geral.DepositoTransferenciaBancariaInput GetDadosDepositoBancarios(IdInput input);
    }
}