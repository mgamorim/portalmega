﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Vendas.Geral.ItemDePedido;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Vendas.Geral.Interfaces
{
    public interface IItemDePedidoAppService : IApplicationService
    {
        Task<PagedResultDto<ItemDePedidoListDto>> GetPedidoPaginado(GetItemDePedidoInput input);
    }
}