﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Vendas.Geral.Interfaces
{
    public interface IPedidoAppService : IApplicationService
    {
        Task<PagedResultDto<PedidoListDto>> GetPedidoPaginado(GetPedidoInput input);

        Task<IdInput> Save(PedidoInput input);

        Task<PedidoInput> GetById(IdInput input);
    }
}