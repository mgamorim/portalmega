﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Helpers;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.EZPag.Geral.Deposito;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.EZLiv.MapToEntity;
using EZ.EZControl.Services.EZPag.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Services.Vendas.Geral.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using EZ.EZControl.Services.Vendas.Venda.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Vendas.Geral
{
    public class VendaAppService : EZControlAppServiceBase<Pedido>, IVendaAppService
    {
        private readonly IVendaService _vendaService;
        private readonly IPropostaDeContratacaoService _propostaService;
        private readonly IClienteService _clienteService;
        private readonly IContratoService _contratoService;
        private readonly IValorPorFaixaEtariaService _valorPorFaixaEtariaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPedidoService _pedidoService;
        private readonly IItemDePedidoService _itemDePedidoService;
        private readonly IDepositoService _depositoService;
        private readonly IDepositoTransferenciaBancariaService _depositoTransferenciaBancariaService;
        private readonly ISnapshotPropostaDeContratacaoService _snapshotPropostaDeContratacaoService;

        public VendaAppService(
            IRepository<Pedido, int> repository,
            IVendaService vendaService,
            IPropostaDeContratacaoService propostaService,
            IClienteService clienteService,
            IContratoService contratoService,
            IValorPorFaixaEtariaService valorPorFaixaEtariaService,
            IPessoaFisicaService pessoaFisicaService,
            IPedidoService pedidoService,
            IItemDePedidoService itemDePedidoService,
            IDepositoService depositoService,
            IDepositoTransferenciaBancariaService depositoTransferenciaBancariaService,
            ISnapshotPropostaDeContratacaoService snapshotPropostaDeContratacaoService
        )
            : base(repository)
        {
            _vendaService = vendaService;
            _propostaService = propostaService;
            _clienteService = clienteService;
            _contratoService = contratoService;
            _valorPorFaixaEtariaService = valorPorFaixaEtariaService;
            _pessoaFisicaService = pessoaFisicaService;
            _pedidoService = pedidoService;
            _itemDePedidoService = itemDePedidoService;
            _depositoService = depositoService;
            _depositoTransferenciaBancariaService = depositoTransferenciaBancariaService;
            _snapshotPropostaDeContratacaoService = snapshotPropostaDeContratacaoService;
        }
        private async Task ValidateProposta(PropostaDeContratacao proposta)
        {
            var validationErrors = new List<ValidationResult>();
            if (proposta.Titular == null)
            {
                validationErrors.Add(new ValidationResult(L("Venda.EmptyTitularError")));
            }
            if (proposta.Produto == null)
            {
                validationErrors.Add(new ValidationResult(L("Venda.EmptyProdutoError")));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }
        public async Task<PedidoInput> RealizaVendaProdutoPlanoDeSaude(IdInput input)
        {
            return await InternalVendaProdutoDePlanoDeSaude(input.Id, FormaDePagamentoEnum.Boleto);
        }

        public async Task<DepositoInput> RealizaVendaProdutoPlanoDeSaudeDeposito(DepositoInput input)
        {
            try
            {
                var pedido = await InternalVendaProdutoDePlanoDeSaude(input.PropostaId, input.formaDePagamento == FormaDePagamentoEnum.Deposito ? FormaDePagamentoEnum.Deposito : FormaDePagamentoEnum.DepositoEmConta);
                var idPedido = pedido.Id;
                var deposito = await _depositoService
                    .GetAll()
                    .Where(x => x.PedidoId == pedido.Id)
                    .FirstOrDefaultAsync();
                await ValidateInputDeposito(input);
                if (deposito == null) deposito = new Deposito();
                deposito = input.MapTo<Deposito>();
                deposito = await ProcessInputDeposito(deposito, input);
                deposito.PedidoId = idPedido;
                deposito.TenantId = 1;
                var depositoEntity = await _depositoService.CreateOrUpdateAndReturnSavedEntity(deposito);

                var pedidoEntity = await _pedidoService.GetById(pedido.Id);
                pedidoEntity.FormaDePagamento = input.formaDePagamento == FormaDePagamentoEnum.Deposito ? FormaDePagamentoEnum.Deposito : FormaDePagamentoEnum.DepositoEmConta;

                if (deposito.Recebido)
                {
                    pedidoEntity.Status = PedidoStatusEnum.Fechado;
                }

                await _pedidoService.UpdateEntity(pedidoEntity);

                var retorno = depositoEntity.MapTo<DepositoInput>();

                retorno.formaDePagamento = pedidoEntity.FormaDePagamento;

                return retorno;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<DepositoInput> ConfirmaRecebimentoVendaProdutoPlanoDeSaudeDeposito(DepositoInput input)
        {
            try
            {
                using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
                {
                    if (input.Id == 0)
                    {
                        throw new UserFriendlyException("O identificação (id) do transferência (depósito) não foi informado.");
                    }

                    if (input.PedidoId == 0)
                    {
                        throw new UserFriendlyException("O identificação (id) do pedido não foi informado.");
                    }

                    var deposito = await _depositoService.GetById(input.Id);
                    var pedidoEntity = await _pedidoService.GetById(input.PedidoId);

                    if (input.Recebido)
                    {
                        pedidoEntity.Status = PedidoStatusEnum.Fechado;
                        await _pedidoService.UpdateEntity(pedidoEntity);
                        deposito.Recebido = input.Recebido;
                        deposito = await _depositoService.UpdateEntity(deposito);
                        return deposito.MapTo<DepositoInput>();
                    }

                    throw new UserFriendlyException("Não foi possível confirmar transferência (depósito).");
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        private async Task ValidateInputDeposito(DepositoInput input)
        {
            var validationDeposito = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Comprovante))
            {
                validationDeposito.Add(new ValidationResult(L("Deposito.EmptyComprovanteError")));
            }

            var user = await UserManager.FindByIdAsync(AbpSession.UserId.Value);
            if (user.TipoDeUsuario == TipoDeUsuarioEnum.Corretor && !input.Recebido)
            {
                validationDeposito.Add(new ValidationResult(L("Deposito.EmptyRecebidoError")));
            }

            if (validationDeposito.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationDeposito);
            }
        }

        private async Task<Deposito> ProcessInputDeposito(Deposito entity, DepositoInput input)
        {
            if (!string.IsNullOrEmpty(input.Comprovante) && entity.DataHoraEnvio == default(DateTime))
            {
                entity.UsuarioEnvioId = AbpSession.UserId.Value;
                entity.DataHoraEnvio = DateTime.Now;
            }

            if (input.Recebido && !entity.Recebido)
            {
                entity.UsuarioDeclaranteId = AbpSession.UserId;
                entity.DataHoraDeclaracao = DateTime.Now;
            }

            return entity;
        }

        private async Task<PedidoInput> InternalVendaProdutoDePlanoDeSaude(int propostaId, FormaDePagamentoEnum formaDePagamento)
        {
            CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter);
            var proposta = await _propostaService.GetById(propostaId);
            await ValidateProposta(proposta);
            var idadeTitular = PessoaHelpers.GetIdade(proposta.Titular.PessoaFisica.DataDeNascimento.Value);
            var titularMenorDeIdade = (idadeTitular < 18);
            var dependentes = proposta.Titular.Dependentes;
            var idadesBeneficiarios = new List<int>();
            idadesBeneficiarios.Add(idadeTitular);
            foreach (var dependente in dependentes)
            {
                idadesBeneficiarios.Add(PessoaHelpers.GetIdade(dependente.PessoaFisica.DataDeNascimento.Value));
            }
            var valores = new List<decimal>();
            var contrato = await _contratoService.GetAll().Where(x => x.ProdutoDePlanoDeSaudeId == proposta.Produto.Id && x.IsActive && DateTime.Now >= x.DataInicioVigencia && DateTime.Now <= x.DataFimVigencia).FirstOrDefaultAsync();
            var valoresPorFaixa = await _valorPorFaixaEtariaService.GetAll().Where(x => x.ContratoId == contrato.Id).ToListAsync();
            foreach (var idade in idadesBeneficiarios)
            {
                var valorParaIdade = valoresPorFaixa.Where(x => idade >= x.FaixaEtaria.IdadeInicial && x.FaixaEtaria.IdadeFinal >= idade).FirstOrDefault().Valor;
                valores.Add(valorParaIdade);
            }
            //Criar cliente a partir do Titular/Responsavel
            var cliente = new Cliente();
            if (titularMenorDeIdade)
            {
                cliente = await CriarOuRecuperarCliente(proposta.Responsavel.PessoaFisica);
            }
            else
            {
                cliente = await CriarOuRecuperarCliente(proposta.Titular.PessoaFisica);
            }
            //Criar pedido
            var pedido = await CreatePedido(cliente, proposta.Produto, valores, formaDePagamento);
            //se já tiver um pedido associado, deve ser removido pois não será mais usado
            if (proposta.PedidoId > 0)
            {
                await _pedidoService.Delete(proposta.PedidoId.Value);
            }
            //Atualizar referência do pedido na proposta
            proposta.Pedido = pedido;
            proposta.PedidoId = pedido.Id;
            //atualiza a proposta com o novo pedido
            var propostaUpdated = await _propostaService.UpdateEntity(proposta);

            MapManual map = new MapManual();
            var MapProposta = await map.MapManualTask(propostaUpdated);


            if (propostaUpdated.Aceite && propostaUpdated.AceiteCorretor && propostaUpdated.PassoDaProposta >= PassoDaPropostaEnum.Pagamento)
            {
                await CreateSnapshot(MapProposta);
            }
            //Retornar o Id do Pedido
            var retorno = pedido.MapTo<PedidoInput>();

            retorno.FormaDePagamento = pedido.FormaDePagamento;

            return retorno;
        }

        private async Task<Pedido> CreatePedido(Cliente cliente, ProdutoDePlanoDeSaude produto, List<decimal> valores, FormaDePagamentoEnum formaDePagamento)
        {
            var pedido = new Pedido();
            pedido.Cliente = cliente;
            pedido.ClienteId = cliente.Id;
            pedido.FormaDePagamento = formaDePagamento;
            var itemDePedido = new ItemDePedido();
            itemDePedido.TipoDeItemDePedido = TipoDeItemDePedidoEnum.Produto;
            itemDePedido.AtribuirPedido(pedido, _itemDePedidoService);
            itemDePedido.AtribuirProduto(produto, _itemDePedidoService);
            itemDePedido.AtribuirQuantidade(1, _itemDePedidoService);
            //Atualizar o valor do pedido com o somatório dos valores por faixa etária
            itemDePedido.Valor = valores.Sum();
            itemDePedido.ValorCadastro = valores.Sum();
            pedido.AddItemDePedido(itemDePedido, _pedidoService);
            //Gravar o Pedido
            var id = await _pedidoService.CreateEntity(pedido);
            pedido = await _pedidoService.GetById(id);
            return pedido;
        }

        private async Task<Cliente> CriarOuRecuperarCliente(PessoaFisica pessoa)
        {
            try
            {
                var cliente = await _clienteService.GetAll().Where(x => x.PessoaId == pessoa.Id).FirstOrDefaultAsync();
                if (cliente == null)
                {
                    cliente = new Cliente();
                    var pf = await _pessoaFisicaService.GetById(pessoa.Id);
                    cliente.Pessoa = pf;
                    var id = await _clienteService.CreateEntity(cliente);
                    cliente = await _clienteService.GetById(id);
                }
                return cliente;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<DepositoInput> GetDadosDeposito(IdInput input)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var proposta = await _propostaService.GetById(input.Id);
                var deposito = await _depositoService.GetAll().Where(x => x.PedidoId == proposta.PedidoId).FirstOrDefaultAsync();
                var depositoInput = deposito.MapTo<DepositoInput>();
                depositoInput.formaDePagamento = deposito.Pedido.FormaDePagamento;
                depositoInput.PropostaId = proposta.Id;
                var depositoTransferenciaBancaria = _depositoTransferenciaBancariaService.GetDepositoBancarioByPessoaJuridicaId(proposta.CorretoraId ?? 0);
                depositoInput.DepositoTransferenciaBancariaInput = new DepositoTransferenciaBancariaInput();
                if (depositoTransferenciaBancaria != null)
                {
                    depositoInput.DepositoTransferenciaBancariaInput.Banco = depositoTransferenciaBancaria.Banco;
                    depositoInput.DepositoTransferenciaBancariaInput.Agencia = depositoTransferenciaBancaria.Agencia;
                    depositoInput.DepositoTransferenciaBancariaInput.Conta = depositoTransferenciaBancaria.Conta;
                    depositoInput.DepositoTransferenciaBancariaInput.Titular = depositoTransferenciaBancaria.Titular;
                    depositoInput.DepositoTransferenciaBancariaInput.Comprovante = depositoTransferenciaBancaria.Comprovante;
                }
                    return depositoInput;
            }
        }

        public DepositoTransferenciaBancariaInput GetDadosDepositoBancarios(IdInput input)
        {
            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                
                var depositoTransferenciaBancaria = _depositoTransferenciaBancariaService.GetDepositoBancarioByEmpresaId(input.Id);
                var DepositoTransferenciaBancariaInput = new DepositoTransferenciaBancariaInput();
                if (depositoTransferenciaBancaria != null)
                {
                    DepositoTransferenciaBancariaInput.Banco = depositoTransferenciaBancaria.Banco;
                    DepositoTransferenciaBancariaInput.Agencia = depositoTransferenciaBancaria.Agencia;
                    DepositoTransferenciaBancariaInput.Conta = depositoTransferenciaBancaria.Conta;
                    DepositoTransferenciaBancariaInput.Titular = depositoTransferenciaBancaria.Titular;
                    DepositoTransferenciaBancariaInput.Descricao = depositoTransferenciaBancaria.Descricao;
                    DepositoTransferenciaBancariaInput.IsActive = depositoTransferenciaBancaria.IsActive;
                    DepositoTransferenciaBancariaInput.Comprovante = depositoTransferenciaBancaria.Comprovante;
                }
                return DepositoTransferenciaBancariaInput;
            }
        }

        // TODO Mover para a propostaDeContratacaoService ou similar
        private async Task CreateSnapshot(PropostaDeContratacaoInput input)
        {
            string jsonSnapshot = JsonConvert.SerializeObject(input);
            var snapShotExistente = await _snapshotPropostaDeContratacaoService.GetAll().Where(x => x.PropostaDeContratacaoId == input.Id).FirstOrDefaultAsync();
            if (snapShotExistente != null)
            {
                await _snapshotPropostaDeContratacaoService.Delete(snapShotExistente.Id);
            }
            var snapShot = new SnapshotPropostaDeContratacao()
            {
                PropostaDeContratacaoId = input.Id,
                Json = Encoding.ASCII.GetBytes(jsonSnapshot)
            };
            await _snapshotPropostaDeContratacaoService.CreateEntity(snapShot);
        }
    }
}