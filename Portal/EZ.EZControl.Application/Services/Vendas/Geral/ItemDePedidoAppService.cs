﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Dto.Vendas.Geral.ItemDePedido;
using EZ.EZControl.Services.Vendas.Geral.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Vendas.Geral
{
    public class ItemDePedidoAppService : EZControlAppServiceBase<ItemDePedido>, IItemDePedidoAppService
    {
        private readonly IItemDePedidoService _itemDePedidoService;

        public ItemDePedidoAppService(
            IRepository<ItemDePedido, int> repository,
            IItemDePedidoService itemDePedidoService)
            : base(repository)
        {
            _itemDePedidoService = itemDePedidoService;
        }

        public async Task<PagedResultDto<ItemDePedidoListDto>> GetPedidoPaginado(GetItemDePedidoInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemDePedido>>
            {
                new WhereIfCondition<ItemDePedido>(true, x => x.Pedido.Id == input.PedidoId)
            };
            var result = await base.GetListPaged<ItemDePedidoListDto, GetItemDePedidoInput>(_itemDePedidoService, input, condicoes);

            return result;
        }
    }
}