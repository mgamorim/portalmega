﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Domain.Vendas.Pedidos;
using EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque;
using EZ.EZControl.Dto.Vendas.Geral.ItemDePedido;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using EZ.EZControl.Services.Servicos.Geral.Interfaces;
using EZ.EZControl.Services.Vendas.Geral.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Vendas.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Vendas_Pedido, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PedidoAppService : EZControlAppServiceBase<Pedido>, IPedidoAppService
    {
        private readonly IPedidoService _pedidoService;
        private readonly IItemDePedidoService _itemDePedidoService;
        private readonly IClienteService _clienteService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IProdutoService _produtoService;
        private readonly IServicoService _servicoService;
        private readonly ISaldoEstoqueService _saldoEstoqueService;
        private readonly IEmpresaService _empresaService;

        public PedidoAppService(
            IRepository<Pedido, int> repository,
            IPedidoService pedidoService,
            IPessoaFisicaService pessoaFisicaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IClienteService clienteService,
            IItemDePedidoService itemDePedidoService,
            IProdutoService produtoService,
            IServicoService servicoService,
            ISaldoEstoqueService saldoEstoqueService,
            IEmpresaService empresaService)
            : base(repository)
        {
            _pedidoService = pedidoService;
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _clienteService = clienteService;
            _itemDePedidoService = itemDePedidoService;
            _produtoService = produtoService;
            _servicoService = servicoService;
            _saldoEstoqueService = saldoEstoqueService;
            _empresaService = empresaService;
        }

        private void ValidateInput(PedidoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (input.ClienteId == 0)
            {
                validationErrors.Add(new ValidationResult(L("Pedido.ClienteObrigatorioError")));
            }

            if (!input.Items.Any())
            {
                validationErrors.Add(new ValidationResult(L("Pedido.SemItens")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private ItemDePedido ToItemDePedido(Pedido pedido, ItemDePedidoListDto item)
        {
            ItemDePedido itemDePedido = null;

            itemDePedido = new ItemDePedido();
            itemDePedido.Id = item.Id;
            itemDePedido.TipoDeItemDePedido = item.TipoDeItemDePedido;
            itemDePedido.Valor = item.Valor;
            itemDePedido.AtribuirQuantidade(item.Quantidade, _itemDePedidoService);

            if (item.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto)
            {
                Produto produto;
                try
                {
                    produto = _produtoService.GetAll().FirstOrDefault(x => x.Id == item.Produto.Id);
                    if (produto == null)
                        throw new Exception(L("Produto.ProdutoNotFoundError"));
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Produto.NotFoundError"), ex.Message);
                }
                itemDePedido.AtribuirProduto(produto, _itemDePedidoService);
                itemDePedido.AtribuirPedido(pedido, _itemDePedidoService);
                itemDePedido.ValorCadastro = produto.Valor;
            }
            else
            {
                Servico servico;
                try
                {
                    servico = _servicoService.GetAll().FirstOrDefault(x => x.Id == item.Servico.Id);
                    if (servico == null)
                        throw new Exception();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Servico.NotFoundError"), ex.Message);
                }
                itemDePedido.AtribuirServico(servico, _itemDePedidoService);
                itemDePedido.AtribuirPedido(pedido, _itemDePedidoService);
                itemDePedido.ValorCadastro = servico.Valor;
            }

            return itemDePedido;
        }

        private void AdicionarNovoItem(Pedido pedido, ItemDePedidoListDto item)
        {
            ItemDePedido itemDePedido = ToItemDePedido(pedido, item);

            pedido.AddItemDePedido(itemDePedido, _pedidoService);
        }

        #region Atualizacao de Saldo do Estoque
        private async Task ProcessInputPedido(SaldoEstoque saldoEstoque, SaldoEstoquePedidoInput input)
        {
            #region Empresa
            Empresa empresa = null;
            try
            {
                empresa = await _empresaService.GetById(Thread.CurrentPrincipal.GetEmpresaIdFromClaim().Value);
            }
            catch (Exception)
            {
                throw new UserFriendlyException(L("Estoque.EmpresaNotFoundError"));
            }
            saldoEstoque.AssociarEmpresa(empresa, _saldoEstoqueService);
            #endregion

            #region Produto
            if (input.ProdutoId > 0)
            {
                Produto produto = null;

                try
                {
                    produto = await _produtoService.GetById(input.ProdutoId);
                    saldoEstoque.AssociarProduto(produto, _saldoEstoqueService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion            

            #region Pedido
            if (input.PedidoId > 0)
            {
                Pedido pedido = null;

                try
                {
                    pedido = await _pedidoService.GetById(input.PedidoId);
                    saldoEstoque.AssociarPedido(pedido, _saldoEstoqueService);                    
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            #endregion
        }

        private async Task AtualizarSaldoOnCreate(int pedidoId)
        {
            Pedido pedido = Repository.Get(pedidoId);
            foreach(ItemDePedido item in pedido.ItensDePedidoReadOnly)
            {
                await AtualizarSaldo(item, false);
            }   
        }

        private async Task AtualizarSaldo(ItemDePedido input, bool estorno)
        {
            SaldoEstoquePedidoInput inputPedido = new SaldoEstoquePedidoInput { PedidoId = input.PedidoId, ProdutoId = input.ProdutoId.Value, Quantidade = input.Quantidade };
            SaldoEstoque saldo = inputPedido.MapTo<SaldoEstoque>();
            await ProcessInputPedido(saldo, inputPedido);
            saldo.PrepararPedido(_saldoEstoqueService);

            if (estorno)
                await _saldoEstoqueService.EstornarPedido(saldo);
            else
                await _saldoEstoqueService.AtualizarPedido(saldo);
        }
        #endregion

        private async Task ProcessInput(Pedido pedido, PedidoInput input)
        {
            if (input.ClienteId > 0)
            {
                try
                {
                    pedido.Cliente = await _clienteService.GetById(input.ClienteId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Cliente.NotFoundError"), ex.Message);
                }
            }

            // Novo Pedido
            if (pedido.Id == 0)
            {
                foreach (var item in input.Items)
                {
                    AdicionarNovoItem(pedido, item);                    
                }
            }
            // Alterando um Pedido
            else
            {
                List<ItemDePedido> itensDoUsuario = input.Items.Select(x => ToItemDePedido(pedido, x)).ToList();

                var itensRemovidos = pedido.ItensDePedidoReadOnly.Except(itensDoUsuario);
                var itensAdicionados = itensDoUsuario.Except(pedido.ItensDePedidoReadOnly);
                var itensAlterados = itensDoUsuario.Intersect(pedido.ItensDePedidoReadOnly);

                foreach (var itemRemovido in itensRemovidos)
                {   
                    ItemDePedido itemDePedido = ToItemDePedido(pedido, itemRemovido.MapTo<ItemDePedidoListDto>());

                    pedido.RemoveItemDePedido(itemRemovido, _pedidoService);
                    await _itemDePedidoService.Delete(itemRemovido.Id);
                    if(itemRemovido.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto)
                        await AtualizarSaldo(itemDePedido, true);
                }

                foreach (var itemAdicionado in itensAdicionados)
                {
                    AdicionarNovoItem(pedido, itemAdicionado.MapTo<ItemDePedidoListDto>());
                    if (itemAdicionado.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto)
                        await AtualizarSaldo(itemAdicionado, false);
                }

                foreach (var itemAlterado in itensAlterados)
                {
                    ItemDePedido item = pedido.ItensDePedidoReadOnly.FirstOrDefault(x => x.Id == itemAlterado.Id);
                    item.AtribuirQuantidade(itemAlterado.Quantidade, _itemDePedidoService);
                    if (itemAlterado.TipoDeItemDePedido == TipoDeItemDePedidoEnum.Produto)
                        await AtualizarSaldo(itemAlterado, false);
                }
            }
        }

        public async Task<PagedResultDto<PedidoListDto>> GetPedidoPaginado(GetPedidoInput input)
        {
            try
            {
                var result = await this.ApplyEmpresaFilter<Task<PagedResultDto<PedidoListDto>>>(async () =>
                {
                    var queryPedido = _pedidoService.GetAll();
                    var queryCliente = _clienteService.GetAll();
                    var queryPessoaFisica = _pessoaFisicaService.GetAll();
                    var queryPessoaJuridica = _pessoaJuridicaService.GetAll();
                    var queryItemPedido = _itemDePedidoService.GetAll();

                    var query = from pedido in queryPedido
                                join itemPedido in queryItemPedido on pedido.Id equals itemPedido.Pedido.Id
                                join cliente in queryCliente on pedido.Cliente.Id equals cliente.Id
                                join pessoaFisica in queryPessoaFisica on pedido.Cliente.Pessoa.Id equals pessoaFisica.Id into pessoaFisicaGroup
                                from pf in pessoaFisicaGroup.DefaultIfEmpty()
                                join pessoaJuridica in queryPessoaJuridica on pedido.Cliente.Pessoa.Id equals pessoaJuridica.Id into pessoaJuridicaGroup
                                from pj in pessoaJuridicaGroup.DefaultIfEmpty()
                                group itemPedido by new
                                {
                                    PedidoId = pedido.Id,
                                    PessoaFisica = pf,
                                    PessoaJuridica = pj,
                                    Cliente = pedido.Cliente,
                                    Tipo = pedido.Tipo,
                                    Status = pedido.Status                                    
                                }
                        into pedidoGroup
                                select new
                                {
                                    PedidoId = pedidoGroup.Key.PedidoId,
                                    PessoaFisica = pedidoGroup.Key.PessoaFisica,
                                    PessoaJuridica = pedidoGroup.Key.PessoaJuridica,
                                    Cliente = pedidoGroup.Key.Cliente,
                                    Tipo = pedidoGroup.Key.Tipo,
                                    Status = pedidoGroup.Key.Status,
                                    Total = pedidoGroup.Sum(x => x.Valor * x.Quantidade)
                                };

                    if (input != null)
                    {
                        if (!string.IsNullOrEmpty(input.NomeCliente))
                            query = query.Where(x =>
                                x.PessoaFisica != null
                                    ? x.PessoaFisica.Nome.Contains(input.NomeCliente)
                                    : x.PessoaJuridica.NomeFantasia.Contains(input.NomeCliente) ||
                                      x.PessoaJuridica.RazaoSocial.Contains(input.NomeCliente));
                    }

                    var count = await query.CountAsync();
                    var dados = await query
                        .OrderBy("PedidoId")
                        .PageBy(input)
                        .ToListAsync();

                    List<PedidoListDto> listDtos = new List<PedidoListDto>();

                    foreach (var item in dados)
                    {
                        PedidoListDto pedidoListDto = new PedidoListDto()
                        {
                            Id = item.PedidoId,
                            NomeCliente = item.Cliente.NomePessoa,
                            ClienteId = item.Cliente.Id,
                            Valor = item.Total,
                            Tipo = item.Tipo,
                            Status = item.Status
                        };

                        listDtos.Add(pedidoListDto);
                    }

                    return new PagedResultDto<PedidoListDto>(count, listDtos);
                });

                return result;   
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Vendas_Pedido_Create, AppPermissions.Pages_Tenant_Vendas_Pedido_Edit)]
        [UnitOfWork(IsDisabled = true)]
        public async Task<IdInput> Save(PedidoInput input)
        {
            try
            {
                IdInput result = null;

                using (var uow = this.UnitOfWorkManager.Begin())
                {
                    if (input.Id == default(int))
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Agencia_Create))
                            throw new UserFriendlyException(L("PermissionToCreateRecord"));

                        result = await base.CreateEntity<IdInput, PedidoInput>(_pedidoService, input, () => ValidateInput(input), ProcessInput);
                        await AtualizarSaldoOnCreate(result.Id);
                    }
                    else
                    {
                        if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Agencia_Edit))
                            throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                        result = await base.UpdateEntity<IdInput, PedidoInput>(_pedidoService, input, () => ValidateInput(input), ProcessInput);
                    }

                    await uow.CompleteAsync();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PedidoInput> GetById(IdInput input)
        {
            var lista = await base.GetEntityById<PedidoInput, IdInput>(_pedidoService, input);
            return lista;
        }
    }
}