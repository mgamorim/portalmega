﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Site;
using EZ.EZControl.Services.CMSFrontEnd.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMSFrontEnd
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMSFrontEnd_Menu, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SiteAppService : EZControlAppServiceBase<Site>, ISiteAppService
    {
        private readonly IRepository<Site> _siteService;

        public SiteAppService(IRepository<Site> repository, IRepository<Site> siteService)
            : base(repository)
        {
            _siteService = siteService;
        }

        public async Task<SiteListDto> GetByHost(GetByHostFrontDto input)
        {
            var result = await this.ApplyEmpresaFilter<Task<SiteListDto>>(async () =>
            {
                var site = await _siteService.GetAll().Where(x => x.Hosts.Any(h => h.Url == input.Host)).FirstOrDefaultAsync();
                var siteListDto = site.MapTo<SiteListDto>();

                return siteListDto;
            });

            return result;
        }
    }
}