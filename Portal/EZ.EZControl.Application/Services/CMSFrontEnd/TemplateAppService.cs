﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using EZ.EZControl.Services.CMS.Interfaces;
using ITemplateAppService = EZ.EZControl.Services.CMSFrontEnd.Interfaces.ITemplateAppService;
using EZ.EZControl.Dto.CMS.Geral.Template;
using System;

namespace EZ.EZControl.Services.CMSFrontEnd
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMSFrontEnd_Template, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TemplateAppService : EZControlAppServiceBase<Template>, ITemplateAppService
    {
        private readonly ITemplateService _service;

        public TemplateAppService(IRepository<Template> repository, ITemplateService service)
            : base(repository)
        {
            this._service = service;
        }

        public async Task<TemplateInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TemplateInput, IdInput>(_service, input);
        }
    }
}
