﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Menu;
using EZ.EZControl.Services.CMSFrontEnd.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMSFrontEnd
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMSFrontEnd_Menu, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class MenuAppService : EZControlAppServiceBase<Menu>, IMenuAppService
    {
        private readonly IMenuService _menuService;
        private readonly IRepository<Site> _siteService;
        public MenuAppService(IRepository<Menu> repository, IMenuService menuService, IRepository<Site> siteService)
            : base(repository)
        {
            _menuService = menuService;
            _siteService = siteService;
        }
        public async Task<GetMenuFrontDto> GetHtml(IdInput input)
        {
            var menu = await _menuService.GetById(input.Id);
            GetMenuFrontDto dto = new GetMenuFrontDto()
            {
                Html = _menuService.GetHtml(menu)
            };
            return dto;
        }
    }
}
