﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using EZ.EZControl.Dto.CMS.Geral.Template;

namespace EZ.EZControl.Services.CMSFrontEnd.Interfaces
{
    public interface ITemplateAppService : IApplicationService
    {
        Task<TemplateInput> GetById(IdInput input);
    }
}
