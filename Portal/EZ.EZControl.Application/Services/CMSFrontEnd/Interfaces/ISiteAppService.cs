﻿using Abp.Application.Services;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Site;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMSFrontEnd.Interfaces
{
    public interface ISiteAppService : IApplicationService
    {
        Task<SiteListDto> GetByHost(GetByHostFrontDto input);
    }
}
