﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Menu;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMSFrontEnd.Interfaces
{
    public interface IMenuAppService : IApplicationService
    {
        Task<GetMenuFrontDto> GetHtml(IdInput input);
    }
}
