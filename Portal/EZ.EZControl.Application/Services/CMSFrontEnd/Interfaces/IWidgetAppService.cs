﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Widget;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMSFrontEnd.Interfaces
{
    public interface IWidgetAppService : IApplicationService
    {
        Task<WidgetFrontInput> GetById(IdInput idInput);
        Task<WidgetFrontInput> Get(GetWidgetFrontInput input);
        Task<IEnumerable<WidgetFrontInput>> GetList(GetWidgetFrontInput input);
    }
}
