﻿using Abp.Application.Services;
using EZ.EZControl.Dto.CMSFrontEnd.Geral;

namespace EZ.EZControl.Services.CMSFrontEnd.Interfaces
{
    public interface ITemplatePadraoAppService : IApplicationService
    {
        CMSFrontEndResultBaseDto<TemplatePadrao> SaveContato(TemplatePadraoContatoDto input);
    }
}