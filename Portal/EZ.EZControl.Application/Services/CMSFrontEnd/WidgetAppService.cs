﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Widget;
using EZ.EZControl.Services.CMS.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using IWidgetAppService = EZ.EZControl.Services.CMSFrontEnd.Interfaces.IWidgetAppService;

namespace EZ.EZControl.Services.CMSFrontEnd
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMSFrontEnd_Widget, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class WidgetAppService : EZControlAppServiceBase<WidgetBase>, IWidgetAppService
    {
        private readonly IWidgetBaseService<WidgetBase> _baseService;

        public WidgetAppService(IRepository<WidgetBase, int> repository,
                                IWidgetBaseService<WidgetBase> baseService)
            : base(repository)
        {
            _baseService = baseService;
        }

        public async Task<WidgetFrontInput> GetById(IdInput idInput)
        {
            var widget = await _baseService.GetById(idInput.Id);
            var retorno = (widget is WidgetHTML) ? (widget as WidgetHTML).MapTo<WidgetFrontInput>() : (widget as WidgetMenu).MapTo<WidgetFrontInput>();
            return retorno;
        }

        public async Task<WidgetFrontInput> Get(GetWidgetFrontInput input)
        {
            var widget = await _baseService
                                    .GetAll()
                                    .FirstOrDefaultAsync(x =>
                                                            x.Site.Id == input.SiteId &&
                                                            x.Template.Id == input.TemplateId &&
                                                            x.Posicao == input.Posicao);
            var retorno = widget.MapTo<WidgetFrontInput>();

            return retorno;
        }

        public async Task<IEnumerable<WidgetFrontInput>> GetList(GetWidgetFrontInput input)
        {
            var widgets = await _baseService
                                    .GetAll()
                                    .Where(x =>
                                               x.Site.Id == input.SiteId &&
                                               x.Template.Id == input.TemplateId &&
                                               x.Posicao == input.Posicao)
                                    .ToListAsync();

            var inputs = new List<WidgetFrontInput>();
            foreach (var w in widgets)
            {
                inputs.Add(w.MapTo<WidgetFrontInput>());
            }

            return inputs;
        }
    }
}
