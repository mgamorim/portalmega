﻿using Abp.AutoMapper;
using EZ.EZControl.Domain.CMSFrontend.Enums;
using EZ.EZControl.Dto.CMSFrontEnd.Geral;
using EZ.EZControl.EZ.EzEntity;
using EZ.EZControl.Services.CMSFrontEnd.Interfaces;

namespace EZ.EZControl.Services.CMSFrontEnd
{
    [AutoMapFrom(typeof(TemplatePadraoContatoDto))]
    public class TemplatePadrao : EzFullAuditedEntityMustHaveTenant
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Mensagem { get; set; }
    }

    public class TemplatePadraoContatoDto
    {
        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Mensagem { get; set; }
    }

    public class TemplatePadraoAppService : EZControlAppServiceBase, ITemplatePadraoAppService
    {
        public CMSFrontEndResultBaseDto<TemplatePadrao> SaveContato(TemplatePadraoContatoDto input)
        {
            string erros = string.Empty;

            if (string.IsNullOrEmpty(input.Nome))
            {
                erros += "O nome é obrigatório.";
            }

            if (string.IsNullOrEmpty(input.Email))
            {
                erros = !string.IsNullOrEmpty(erros) ? erros += " " : erros;
                erros += "O e-mail é obrigatório.";
            }

            if (string.IsNullOrEmpty(input.Telefone))
            {
                erros = !string.IsNullOrEmpty(erros) ? erros += " " : erros;
                erros += "O telefone é obrigatório.";
            }

            if (string.IsNullOrEmpty(input.Mensagem))
            {
                erros = !string.IsNullOrEmpty(erros) ? erros += " " : erros;
                erros += "A mensagem é obrigatória.";
            }

            var result = new CMSFrontEndResultBaseDto<TemplatePadrao>();
            result.Resultado = input.MapTo<TemplatePadrao>();
            result.Status = CMSFrontEndResultStatusEnum.Sucesso;

            if (!string.IsNullOrEmpty(erros))
            {
                result.Status = CMSFrontEndResultStatusEnum.Erro;
                result.Descricao = erros;
                result.Titulo = "Erro ao processar os dados formulário.";
            }

            return result;
        }
    }
}