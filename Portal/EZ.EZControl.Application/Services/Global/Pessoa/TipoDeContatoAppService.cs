﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeContato;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
                AppPermissions.Pages_Tenant_Global_TipoDeContato,
                AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato,
                AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TipoDeContatoAppService : EZControlAppServiceBase<TipoDeContato>, ITipoDeContatoAppService
    {
        private readonly ITipoDeContatoService _tipoDeContatoService;

        public TipoDeContatoAppService(IRepository<TipoDeContato> tipoDeContatoRepository, ITipoDeContatoService tipoDeContatoService)
            : base(tipoDeContatoRepository)
        {
            _tipoDeContatoService = tipoDeContatoService;
        }

        private void ValidateInput(TipoDeContatoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.TipoDeContatoFixo == null)
            {
                validationErrors.Add(new ValidationResult(String.Format(L("TipoDeContato.RequiredTipoError"), input.TipoDeContatoFixo), new[] { "tipoDeContatoFixo".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Descricao))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("TipoDeContato.RequiredDescricaoError"), input.Descricao), new[] { "descricao".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Descricao == input.Descricao))
                validationErrors.Add(new ValidationResult(String.Format(L("TipoDeContato.DuplicateDescricaoError"), input.Descricao), new[] { "descricao".ToCamelCase() }));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeContato_Create, AppPermissions.Pages_Tenant_Global_TipoDeContato_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Edit
            )]
        public async Task<IdInput> Save(TipoDeContatoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeContato_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Create)
                    )
                    result = await base.CreateEntity<IdInput, TipoDeContatoInput>(_tipoDeContatoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeContato_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Edit))
                    result = await base.UpdateEntity<IdInput, TipoDeContatoInput>(_tipoDeContatoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeContato_Create, AppPermissions.Pages_Tenant_Global_TipoDeContato_Edit)]
        public async Task<TipoDeContatoInput> SaveAndReturnEntity(TipoDeContatoInput input)
        {
            TipoDeContatoInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeContato_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Create))
                    result = await base.CreateAndReturnEntity<TipoDeContatoInput, TipoDeContatoInput>(_tipoDeContatoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeContato_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Edit))
                    result = await base.UpdateAndReturnEntity<TipoDeContatoInput, TipoDeContatoInput>(_tipoDeContatoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        public async Task<TipoDeContatoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TipoDeContatoInput, IdInput>(_tipoDeContatoService, input);
        }

        public async Task<TipoDeContatoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TipoDeContatoInput, IdInput>(_tipoDeContatoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeContato_Delete, 
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeContato_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeContato_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_tipoDeContatoService, input);
        }

        public async Task<PagedResultDto<TipoDeContatoListDto>> GetTiposDeContatosPaginado(GetTipoDeContatoInput input)
        {
            var condicoes = new List<WhereIfCondition<TipoDeContato>>
            {
                new WhereIfCondition<TipoDeContato>(!string.IsNullOrEmpty(input.Descricao),
                    x => x.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<TipoDeContatoListDto, GetTipoDeContatoInput>(_tipoDeContatoService, input, condicoes);

            return result;
        }
    }
}