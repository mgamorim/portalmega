﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
        AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico,
        AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico,
        AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico,
        AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico,
        AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class EnderecoEletronicoAppService : EZControlAppServiceBase<EnderecoEletronico>, IEnderecoEletronicoAppService
    {
        private readonly IEnderecoEletronicoService _enderecoEletronicoService;
        private readonly IPessoaService _pessoaService;
        private readonly IContatoService _contatoService;

        public EnderecoEletronicoAppService(
            IRepository<EnderecoEletronico> enderecoEletronicoRepository,
            IPessoaService pessoaService,
            IEnderecoEletronicoService enderecoEletronicoService,
            IContatoService contatoService)
            : base(enderecoEletronicoRepository)
        {
            _enderecoEletronicoService = enderecoEletronicoService;
            _contatoService = contatoService;
            _pessoaService = pessoaService;
        }

        private void ValidateInput(EnderecoEletronicoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (input.Endereco == null)
                validationErrors.Add(new ValidationResult(String.Format(L("EnderecoEletronico.RequiredEnderecoError"), input.Endereco), new[] { "endereco".ToCamelCase() }));

            if (_enderecoEletronicoService.CheckForDuplicateInstance(input.Id, x => x.Endereco == input.Endereco))
                validationErrors.Add(new ValidationResult(String.Format(L("EnderecoEletronico.DuplicateEnderecoError"), input.Endereco), new[] { "endereco".ToCamelCase() }));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(EnderecoEletronico enderecoEletronico, EnderecoEletronicoInput input)
        {
            if (input.PessoaId > 0)
            {
                Domain.Global.Pessoa.Pessoa pessoa = null;

                try
                {
                    pessoa = await _pessoaService.GetById(input.PessoaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Pessoa.NotFoundError"));
                }

                try
                {
                    enderecoEletronico.AssociarPessoa(pessoa, _enderecoEletronicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("EnderecoEletronico.EmptyPessoaError"));
            }

            if (input.ContatoId > 0)
            {
                Contato contato = null;

                try
                {
                    contato = await _contatoService.GetById(input.ContatoId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Contato.NotFoundError"));
                }

                try
                {
                    enderecoEletronico.AssociarContato(contato, _enderecoEletronicoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Edit
            )]
        public async Task<IdInput> Save(EnderecoEletronicoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Create))
                    result = await base.CreateEntity<IdInput, EnderecoEletronicoInput>(_enderecoEletronicoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Edit))
                    result = await base.UpdateEntity<IdInput, EnderecoEletronicoInput>(_enderecoEletronicoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }

            }

            return result;
        }

        [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Create,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Edit
            )]
        public async Task<EnderecoEletronicoInput> SaveAndReturnEntity(EnderecoEletronicoInput input)
        {
            EnderecoEletronicoInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Create))
                    result = await base.CreateAndReturnEntity<EnderecoEletronicoInput, EnderecoEletronicoInput>(_enderecoEletronicoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }

            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Edit))
                    result = await base.UpdateAndReturnEntity<EnderecoEletronicoInput, EnderecoEletronicoInput>(_enderecoEletronicoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }

            }

            return result;
        }

        public async Task<EnderecoEletronicoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EnderecoEletronicoInput, IdInput>(_enderecoEletronicoService, input);
        }

        public async Task<EnderecoEletronicoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EnderecoEletronicoInput, IdInput>(_enderecoEletronicoService, input);
        }

        [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_ClienteEnderecoEletronico_Delete,
            AppPermissions.Pages_Tenant_Global_EmpresaEnderecoEletronico_Delete,
            AppPermissions.Pages_Tenant_Global_FornecedorEnderecoEletronico_Delete,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaEnderecoEletronico_Delete,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaEnderecoEletronico_Delete
            )]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_enderecoEletronicoService, input);
        }

        public async Task<PagedResultDto<EnderecoEletronicoListDto>> GetEnderecosEletronicosPaginado(GetEnderecoEletronicoInput input)
        {
            IList<WhereIfCondition<EnderecoEletronico>> condicoes = new List<WhereIfCondition<EnderecoEletronico>>();

            condicoes.Add(new WhereIfCondition<EnderecoEletronico>(true, x => x.Pessoa.Id == input.PessoaId));
            condicoes.Add(new WhereIfCondition<EnderecoEletronico>(!input.Endereco.IsNullOrEmpty(), x => x.Endereco.Contains(input.Endereco)));

            var result = await base.GetListPaged<EnderecoEletronicoListDto, GetEnderecoEletronicoInput>(_enderecoEletronicoService, input, condicoes);

            return result;
        }

        public async Task<EnderecoEletronicoInput> GetByPessoaId(IdInput input)
        {
            try
            {
                var queryEnderecoEletronico = await _enderecoEletronicoService
                  .GetAll()
                  .Where(x => x.PessoaId == input.Id && x.TipoDeEnderecoEletronico == Domain.Global.Enums.TipoDeEnderecoEletronicoEnum.EmailPrincipal)
                  .Select(x => new EnderecoEletronicoInput
                  {
                      Id = x.Id,
                      Endereco = x.Endereco,
                      TipoDeEnderecoEletronico = x.TipoDeEnderecoEletronico,
                      PessoaId = x.PessoaId
                  }).FirstOrDefaultAsync();

                return queryEnderecoEletronico;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}