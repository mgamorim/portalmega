﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento;
using EZ.EZControl.EZ.EzFieldValidation;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITipoDeDocumentoAppService : IApplicationService
    {
        Task<IdInput> Save(TipoDeDocumentoInput input);

        Task<TipoDeDocumentoInput> SaveAndReturnEntity(TipoDeDocumentoInput input);

        Task<TipoDeDocumentoInput> GetById(IdInput input);

        Task<TipoDeDocumentoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<ListResultDto<TipoDeDocumentoListDto>> GetTipoDeDocumentos(GetTipoDeDocumentoInput input);

        Task<PagedResultDto<TipoDeDocumentoListDto>> GetTipoDeDocumentosPaginado(GetTipoDeDocumentoInput input);

        Task<TipoDeDocumentoListDto> GetTipoDeDocumentoByDescricao(GetTipoDeDocumentoInput input);

        Task<ListResultDto<TipoDeDocumentoListDto>> GetByPessoaId(IdInput input);

        Task<TipoDeDocumentoListDto> GetTipoDeDocumentoByTipoFixo(GetTipoDeDocumentoInput input);

        EzFieldValidation IsCpf(string cpf);

        EzFieldValidation IsCnpj(string cnpj);

        EzFieldValidation IsPis(string pis);
    }
}