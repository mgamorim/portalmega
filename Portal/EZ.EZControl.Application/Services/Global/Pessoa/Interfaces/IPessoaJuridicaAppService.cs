﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IPessoaJuridicaAppService : IApplicationService
    {
        Task<IdInput> Save(PessoaJuridicaInput input);

        Task<PessoaJuridicaInput> SaveAndReturnEntity(PessoaJuridicaInput input);

        Task<PessoaJuridicaListDto> GetById(IdInput input);

        Task<PessoaJuridicaListDto> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<PessoaJuridicaListDto>> GetPessoasPaginado(GetPessoaInput input);
    }
}