﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IPessoaFisicaAppService : IApplicationService
    {
        Task<IdInput> Save(PessoaFisicaInput input);

        Task<PessoaFisicaInput> SaveAndReturnEntity(PessoaFisicaInput input);

        Task<PessoaFisicaListDto> GetById(IdInput input);

        Task<PessoaFisicaInput> GetId(IdInput input);

        Task<PessoaFisicaListDto> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<PessoaFisicaListDto>> GetPessoasPaginado(GetPessoaInput input);

        Task<PagedResultDto<PessoaFisicaByCPFListDto>> GetByCPFPaginado(GetPessoaFisicaByCPFInput input);

        //Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForId(GetPessoaFisicaByCPFInput input);
        Task<PessoaFisicaListDto> GetPessoaByCpf(GetPessoaFisicaByCPFInput input);
    }
}