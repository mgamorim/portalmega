﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.Documento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IDocumentoAppService : IApplicationService
    {
        Task<IdInput> Save(DocumentoInput input);

        Task<DocumentoInput> SaveAndReturnEntity(DocumentoInput input);

        Task<DocumentoInput> GetById(IdInput input);

        Task<DocumentoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<DocumentoListDto>> GetDocumentosPaginado(GetDocumentoInput input);
    }
}