﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeContato;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITipoDeContatoAppService : IApplicationService
    {
        Task<IdInput> Save(TipoDeContatoInput input);

        Task<TipoDeContatoInput> SaveAndReturnEntity(TipoDeContatoInput input);

        Task<TipoDeContatoInput> GetById(IdInput input);

        Task<TipoDeContatoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<TipoDeContatoListDto>> GetTiposDeContatosPaginado(GetTipoDeContatoInput input);
    }
}