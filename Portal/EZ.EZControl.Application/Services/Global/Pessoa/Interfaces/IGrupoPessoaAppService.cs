﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IGrupoPessoaAppService : IApplicationService
    {
        Task<IdInput> Save(GrupoPessoaInput input);

        Task<GrupoPessoaInput> SaveAndReturnEntity(GrupoPessoaInput input);

        Task<PagedResultDto<GrupoPessoaListDto>> GetGruposPessoaPaginado(GetGrupoPessoaInput input);

        Task<PagedResultDto<GrupoPessoaListDto>> GetGruposPessoaExceptForId(GetGrupoPessoaExceptForIdInput input);

        Task<GrupoPessoaInput> GetById(IdInput input);

        Task<GrupoPessoaListDto> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<List<string>> GetGrupoPessoa(GetGrupoPessoaInput input);
    }
}