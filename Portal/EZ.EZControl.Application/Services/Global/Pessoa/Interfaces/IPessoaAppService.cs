﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IPessoaAppService : IApplicationService
    {
        Task<PagedResultDto<PessoaListDto>> GetPessoas(GetPessoaInput input);
        Task<PessoaListDto> GetById(IdInput input);
        Task<PessoaListDto> GetByExternalId(IdInput input);
    }
}