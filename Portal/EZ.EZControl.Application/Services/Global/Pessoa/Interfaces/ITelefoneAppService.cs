﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITelefoneAppService : IApplicationService
    {
        //Task<IdInput> CreateTelefone(TelefoneInput input);

        Task<TelefoneInput> GetById(IdInput input);

        Task<TelefoneInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<TelefoneListDto>> GetTelefonesPaginado(GetTelefoneInput input);

        Task<IdInput> Save(TelefoneInput input);

        Task<TelefoneInput> SaveAndReturnEntity(TelefoneInput input);

        Task<TelefoneInput> GetByPessoaId(IdInput input);

    }
}