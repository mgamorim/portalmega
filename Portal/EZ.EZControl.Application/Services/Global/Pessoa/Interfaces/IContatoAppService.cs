﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.Contato;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IContatoAppService : IApplicationService
    {
        Task<IdInput> Save(ContatoInput input);

        Task<ContatoInput> SaveAndReturnEntity(ContatoInput input);

        Task<PagedResultDto<ContatoListDto>> GetContatosPaginado(GetContatoInput input);

        Task<ContatoInput> GetById(IdInput input);

        Task<ContatoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}