﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.Tratamento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface ITratamentoAppService : IApplicationService
    {
        Task<IdInput> Save(TratamentoInput input);

        Task<TratamentoInput> SaveAndReturnEntity(TratamentoInput input);

        Task<TratamentoInput> GetById(IdInput input);

        Task<TratamentoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<TratamentoListDto>> GetTratamentosPaginado(GetTratamentoInput input);
    }
}