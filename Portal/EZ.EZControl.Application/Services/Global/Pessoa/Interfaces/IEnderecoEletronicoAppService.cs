﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa.Interfaces
{
    public interface IEnderecoEletronicoAppService : IApplicationService
    {
        Task<IdInput> Save(EnderecoEletronicoInput input);

        Task<EnderecoEletronicoInput> SaveAndReturnEntity(EnderecoEletronicoInput input);

        Task<EnderecoEletronicoInput> GetById(IdInput input);

        Task<EnderecoEletronicoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<EnderecoEletronicoListDto>> GetEnderecosEletronicosPaginado(GetEnderecoEletronicoInput input);

        //Task<ListResultDto<EnderecoEletronicoListDto>> GetByPessoaId(IdInput input);

        Task<EnderecoEletronicoInput> GetByPessoaId(IdInput input);
    }
}