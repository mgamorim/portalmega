﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Documento;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
                AppPermissions.Pages_Tenant_Global_Documento,
                AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento,
                AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class DocumentoAppService : EZControlAppServiceBase<Documento>, IDocumentoAppService
    {
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IPessoaService _pessoaService;
        private readonly IDocumentoService _documentoService;
        private readonly IEstadoService _estadoService;

        public DocumentoAppService(
            IRepository<Documento, int> repository,
            ITipoDeDocumentoService tipoDeDocumentoService,
            IPessoaService pessoaRepository,
            IEstadoService estadoService,
            IDocumentoService documentoService)
            : base(repository)
        {
            _documentoService = documentoService;
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _pessoaService = pessoaRepository;
            _estadoService = estadoService;
        }

        private void ValidateInput(DocumentoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (CheckForFieldDuplicated(Repository, input.Id, input.TipoDeDocumentoId, x => x.TipoDeDocumentoId == input.TipoDeDocumentoId & x.PessoaId == input.PessoaId))
            {
                validationErrors.Add(new ValidationResult(L("Documento.DuplicateTipoDeDocumentoError"), new[] { "tipodedocumentoid".ToCamelCase() }));
            }            

            if (string.IsNullOrEmpty(input.Numero))
                validationErrors.Add(new ValidationResult(L("Documento.RequiredNumeroError"), new[] { "numero".ToCamelCase() }));

            if (CheckForFieldDuplicated(Repository, input.Id, input.Numero, x => x.Numero == input.Numero))
            {
                validationErrors.Add(new ValidationResult(L("Documento.DuplicateNumeroError"), new[] { "numero".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Documento documento, DocumentoInput input)
        {
            if (input.TipoDeDocumentoId > 0)
            {
                TipoDeDocumento tipoDeDocumento = null;

                try
                {
                    tipoDeDocumento = await _tipoDeDocumentoService.GetById(input.TipoDeDocumentoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("TipoDeDocumento.NotFoundError"));
                }

                try
                {
                    documento.AssociarTipoDocumento(tipoDeDocumento, _documentoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            if (input.EstadoId != null && input.EstadoId > 0)
            {
                Estado estado = null;

                try
                {
                    estado = await _estadoService.GetById((int)input.EstadoId);
                    documento.Estado = estado;
                    //documento.AssociarTipoDocumento(estado, _service);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Estado.NotFoundError"));
                }
            }
            else
            {
                documento.Estado = null;
            }

            if (input.PessoaId > 0)
            {
                Domain.Global.Pessoa.Pessoa pessoa = null;

                try
                {
                    pessoa = await _pessoaService.GetById(input.PessoaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Pessoa.NotFoundError"));
                }

                try
                {
                    documento.AssociarPessoa(pessoa, _documentoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Documento.EmptyPessoaError"));
            }

            try
            {
               if (documento.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cnpj ||
               documento.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf)
                {
                    //associar o documento respectivo com o documento principal.
                    var pessoa = await _pessoaService.GetById(input.PessoaId);
                    pessoa.AssociarDocumentoPrincipal(documento, _pessoaService);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

           

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Documento_Create, AppPermissions.Pages_Tenant_Global_Documento_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Edit
            )]
        public async Task<IdInput> Save(DocumentoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
               
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Documento_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Create)
                   )
                    result = await base.CreateEntity<IdInput, DocumentoInput>(_documentoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
                
            }
            else
            {
               if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Documento_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Edit)
                  )
                    result = await base.UpdateEntity<IdInput, DocumentoInput>(_documentoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Documento_Create, AppPermissions.Pages_Tenant_Global_Documento_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Edit,
                     AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Edit
            )]
        public async Task<DocumentoInput> SaveAndReturnEntity(DocumentoInput input)
        {
            DocumentoInput result = null;
            
            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Documento_Create) ||
                   PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Create) ||
                   PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Create)
                  )
                    result = await base.CreateAndReturnEntity<DocumentoInput, DocumentoInput>(_documentoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }

            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Documento_Edit) ||
                   PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Edit) ||
                   PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Edit)
                 )
                    result = await base.UpdateAndReturnEntity<DocumentoInput, DocumentoInput>(_documentoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }

                
            }

            return result;
        }

        public async Task<DocumentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<DocumentoInput, IdInput>(_documentoService, input);
        }

        public async Task<DocumentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<DocumentoInput, IdInput>(_documentoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Documento_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaDocumento_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaDocumento_Delete
            )]
        public async Task Delete(IdInput input)
        {

            var doc = _documentoService.GetAll().FirstOrDefault(x => x.Id == input.Id);
            _documentoService.CanDelete(doc);
            await base.DeleteEntity(_documentoService, input);
        }

        public async Task<PagedResultDto<DocumentoListDto>> GetDocumentosPaginado(GetDocumentoInput input)
        {
            IList<WhereIfCondition<Documento>> condicoes = new List<WhereIfCondition<Documento>>();

            condicoes.Add(new WhereIfCondition<Documento>(input.PessoaId > 0, x => x.Pessoa.Id == input.PessoaId));
            condicoes.Add(new WhereIfCondition<Documento>(!input.Numero.IsNullOrEmpty(), x => x.Numero.Contains(input.Numero)));

            var result = await base.GetListPaged<DocumentoListDto, GetDocumentoInput>(_documentoService, input, condicoes);

            return result;
        }
    }
}