﻿using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    public abstract class PessoaAppServiceBase<TPessoa> : EZControlAppServiceBase<TPessoa>
        where TPessoa : Domain.Global.Pessoa.Pessoa
    {
        private readonly IPessoaService _pessoaService;
        private readonly IGrupoPessoaService _grupoPessoaService;
        private readonly IDocumentoService _documentoService;

        protected PessoaAppServiceBase(
            IRepository<TPessoa, int> repository,
            IPessoaService pessoaService,
            IGrupoPessoaService grupoPessoaService,
            IDocumentoService documentoService)
            : base(repository)
        {
            _pessoaService = pessoaService;
            _grupoPessoaService = grupoPessoaService;
            _documentoService = documentoService;
        }

        protected virtual void ValidateInput(PessoaInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (input.TipoPessoa == null)
            {
                validationErrors.Add(new ValidationResult(L("Pessoa.RequiredTipoDePessoaError"), new[] { "tipoDePessoa".ToCamelCase() }));
            }
            else
            {
                if (!Enum.IsDefined(typeof(TipoPessoaEnum), input.TipoPessoa))
                {
                    validationErrors.Add(new ValidationResult(L("Pessoa.InvalidTipoDePessoaError"), new[] { "tipoDePessoa".ToCamelCase() }));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(Domain.Global.Pessoa.Pessoa pessoa, PessoaInput input)
        {
            if (input.DocumentoPrincipal != null && input.DocumentoPrincipal.Id > 0)
            {
                var documento = await _documentoService.GetById(input.DocumentoPrincipal.Id);
                pessoa.AssociarDocumentoPrincipal(documento, _pessoaService);
            }

            await PreencherGrupoPessoa(pessoa, input.GrupoPessoaId);
        }

        private async Task PreencherGrupoPessoa(Domain.Global.Pessoa.Pessoa pessoa, int? grupoPessoaId)
        {
            if (grupoPessoaId == 0 && pessoa.GrupoPessoa != null)
            {
                pessoa.AssociarGrupoPessoa(null, _pessoaService);
                return;
            }

            if (pessoa.GrupoPessoa != null && pessoa.GrupoPessoa.Id == grupoPessoaId)
                return;

            if (grupoPessoaId.HasValue && grupoPessoaId != 0)
            {
                try
                {
                    GrupoPessoa grupoPessoa = await _grupoPessoaService.GetById(grupoPessoaId.Value);
                    pessoa.AssociarGrupoPessoa(grupoPessoa, _pessoaService);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("GrupoPessoa.NotFoundError"));
                }
            }
        }
    }
}