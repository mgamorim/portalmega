﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Contato;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_Contato,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaContato,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class ContatoAppService : EZControlAppServiceBase<Contato>, IContatoAppService
    {
        private readonly IContatoService _contatoService;
        private readonly IPessoaService _pessoaService;
        private readonly ITipoDeContatoService _tipoDeContatoService;
        private readonly ITratamentoService _tratamentoService;

        public ContatoAppService(
            IRepository<Contato, int> contatoRepository,
            IContatoService contatoService,
            IPessoaService pessoaService,
            ITipoDeContatoService tipoDeContatoService,
            ITratamentoService tratamentoService)
            : base(contatoRepository)
        {
            _contatoService = contatoService;
            _pessoaService = pessoaService;
            _tipoDeContatoService = tipoDeContatoService;
            _tratamentoService = tratamentoService;
        }

        private void ValidateInput(ContatoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("Contato.RequiredNomeError"),
                    input.Nome), new[] { "nome".ToCamelCase() }));

            if (_contatoService.CheckForDuplicateInstance(input.Id, x => x.Nome == input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("Contato.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (input.Sexo != null)
            {
                if (!Enum.IsDefined(typeof(SexoEnum), input.Sexo))
                {
                    validationErrors.Add(new ValidationResult(L("Contato.InvalidSexoError")));
                }
            }

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(Contato contato, ContatoInput input)
        {
            // Pessoa
            // Obrigatorio
            if (input.PessoaId > 0)
            {
                Domain.Global.Pessoa.Pessoa pessoa = null;

                try
                {
                    pessoa = await _pessoaService.GetById(input.PessoaId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Pessoa.NotFoundError"));
                }

                try
                {
                    contato.AssociarPessoa(pessoa, _contatoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Contato.EmptyPessoaError"));
            }

            // Tipo de Contato
            // Obrigatório
            if (input.TipoDeContatoId > 0)
            {
                TipoDeContato tipoDeContato = null;

                try
                {
                    tipoDeContato = await _tipoDeContatoService.GetById(input.TipoDeContatoId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("TipoDeContato.NotFoundError"));
                }

                try
                {
                    contato.AssociarTipoDeContato(tipoDeContato, _contatoService);
                }
                catch (Exception ex)
                {

                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Contato.EmptyTipoDeContatoError"));
            }

            // Tratamento
            // opcional
            if (input.TratamentoId.HasValue && input.TratamentoId > 0)
            {
                Tratamento tratamento = null;

                try
                {
                    tratamento = await _tratamentoService.GetById(input.TratamentoId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Tratamento.NotFoundError"));
                }

                try
                {
                    contato.AssociarTratamento(tratamento, _contatoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                contato.AssociarTratamento(null, _contatoService);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Contato_Create, AppPermissions.Pages_Tenant_Global_Contato_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Edit
            )]
        public async Task<IdInput> Save(ContatoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Contato_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Create))
                    result = await base.CreateEntity<IdInput, ContatoInput>(_contatoService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Contato_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Edit))
                    result = await base.UpdateEntity<IdInput, ContatoInput>(_contatoService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Contato_Create, AppPermissions.Pages_Tenant_Global_Contato_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Edit
            )]
        public async Task<ContatoInput> SaveAndReturnEntity(ContatoInput input)
        {
            ContatoInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Contato_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Create))
                    result = await base.CreateAndReturnEntity<ContatoInput, ContatoInput>(_contatoService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }

            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Contato_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Edit))
                    result = await base.UpdateAndReturnEntity<ContatoInput, ContatoInput>(_contatoService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        public async Task<PagedResultDto<ContatoListDto>> GetContatosPaginado(GetContatoInput input)
        {
            var condicoes = new List<WhereIfCondition<Contato>>
            {
                new WhereIfCondition<Contato>(
                    input.PessoaId > 0 ||
                    input.TipoDeContatoId > 0||
                    input.TratamentoId > 0 ||
                    !input.Nome.IsNullOrEmpty() ||
                    !input.Cargo.IsNullOrEmpty() ||
                    !input.Setor.IsNullOrEmpty(),
                    a =>
                        a.Nome.Contains(input.Nome) ||
                        a.Cargo.Contains(input.Cargo) ||
                        a.Setor.Contains(input.Setor))
            };

            var result = await base.GetListPaged<ContatoListDto, GetContatoInput>(_contatoService, input, condicoes);
            return result;
        }

        public async Task<ContatoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ContatoInput, IdInput>(_contatoService, input);
        }

        public async Task<ContatoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ContatoInput, IdInput>(_contatoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Contato_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaContato_Delete, 
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaContato_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_contatoService, input);
        }
    }
}