﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaFisica, AppPermissions.Pages_Tenant_Global_PessoaJuridica, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PessoaAppService : EZControlAppServiceBase, IPessoaAppService
    {
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IPessoaService _pessoaService;

        public PessoaAppService(
            IPessoaService pessoaService,
            IPessoaFisicaService pessoaFisicaService,
            IPessoaJuridicaService pessoaJuridicaService)
        {
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _pessoaService = pessoaService;
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoas(GetPessoaInput input)
        {
            try
            {
                if (input == null)
                    throw new Exception("É necessário definir um critério para buscar por pessoas.");

                var queryPessoaFisica = _pessoaFisicaService
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), x => x.Nome.Contains(input.Nome))
                    .Select(x => new PessoaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.Nome,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Fisica
                    });

                var queryPessoaJuridica = _pessoaJuridicaService
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.Nome), x => x.NomeFantasia.Contains(input.Nome))
                    .Select(x => new PessoaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.NomeFantasia,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Juridica
                    });

                IQueryable<PessoaListDto> query;

                if (input.TipoPessoa != null && input.TipoPessoa == TipoPessoaEnum.Fisica)
                {
                    query = queryPessoaFisica;
                }
                else if (input.TipoPessoa != null && input.TipoPessoa == TipoPessoaEnum.Juridica)
                {
                    query = queryPessoaJuridica;
                }
                else
                {
                    query = queryPessoaJuridica.Union(queryPessoaFisica);
                }

                var count = await query.CountAsync();

                if (!string.IsNullOrEmpty(input.Sorting))
                    query = query.OrderBy(input.Sorting);

                List<PessoaListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PessoaListDto> GetById(IdInput input)
        {
            var pessoaDto = new PessoaListDto();

            var pessoa = await _pessoaService.GetById(input.Id);

            if (pessoa.IsPessoaFisica)
            {
                pessoaDto.Id = pessoa.Id;
                pessoaDto.NomePessoa = (pessoa as PessoaFisica).Nome;
                pessoaDto.TipoPessoa = TipoPessoaEnum.Fisica;
            }
            else if (pessoa.IsPessoaJuridica)
            {
                pessoaDto.Id = pessoa.Id;
                pessoaDto.NomePessoa = (pessoa as PessoaJuridica).NomeFantasia;
                pessoaDto.TipoPessoa = TipoPessoaEnum.Juridica;
            }

            return pessoaDto;
        }

        public async Task<PessoaListDto> GetByExternalId(IdInput input)
        {
            var pessoaDto = new PessoaListDto();

            var pessoa = await _pessoaService.GetByExternalId(input.Id);

            if (pessoa.IsPessoaFisica)
            {
                pessoaDto.Id = pessoa.Id;
                pessoaDto.NomePessoa = (pessoa as PessoaFisica).Nome;
                pessoaDto.TipoPessoa = TipoPessoaEnum.Fisica;
            }
            else if (pessoa.IsPessoaJuridica)
            {
                pessoaDto.Id = pessoa.Id;
                pessoaDto.NomePessoa = (pessoa as PessoaJuridica).NomeFantasia;
                pessoaDto.TipoPessoa = TipoPessoaEnum.Fisica;
            }

            return pessoaDto;
        }
    }
}