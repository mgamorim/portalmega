﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
        AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone,
        AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TelefoneAppService : EZControlAppServiceBase<Telefone>, ITelefoneAppService
    {
        private readonly ITelefoneService _telefoneService;
        private readonly IPessoaService _pessoaService;
        private readonly IContatoService _contatoService;

        public TelefoneAppService(
            IRepository<Telefone> telefoneRepository,
            IPessoaService pessoaService,
            ITelefoneService telefoneService,
            IContatoService contatoService)
            : base(telefoneRepository)
        {
            _telefoneService = telefoneService;
            _contatoService = contatoService;
            _pessoaService = pessoaService;
        }

      
        private readonly IRepository<Domain.Global.Pessoa.Pessoa> _pessoaRepository;

       

        private void ValidateInput(TelefoneInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (!Enum.IsDefined(typeof(TipoTelefoneEnum), input.Tipo))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Telefone.RequiredTipoError"), input.Tipo), new[] { "tipo".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.DDD))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Telefone.RequiredDDDError"), input.DDD), new[] { "ddd".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Numero))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Telefone.RequiredNumeroError"), input.Numero), new[] { "numero".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }



        private async Task ProcessInput(Telefone telefone, TelefoneInput input)
        {
            if (input.PessoaId > 0)
            {
                Domain.Global.Pessoa.Pessoa pessoa = null;

                try
                {
                    pessoa = await _pessoaService.GetById(input.PessoaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Pessoa.NotFoundError"));
                }

                try
                {
                    telefone.AssociarPessoa(pessoa, _telefoneService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Telefone.EmptyPessoaError"));
            }
        }

        //private async Task BeforeSaveEntity(Telefone telefone, TelefoneInput input)
        //{
        //    if (input.PessoaId > 0)
        //    {
        //        Domain.Global.Pessoa.Pessoa pessoa = null;

        //        try
        //        {
        //            pessoa = await _pessoaRepository.GetAsync(input.PessoaId);
        //            telefone.Pessoa = pessoa;
        //        }
        //        catch (Exception)
        //        {
        //            throw new UserFriendlyException(L("Pessoa.NotFoundError"));
        //        }
        //    }
        //    else
        //    {
        //        throw new UserFriendlyException(L("Telefone.EmptyPessoaError"));
        //    }
        //}

        
    [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Create,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Create,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Edit)]
        public async Task<IdInput> Save(TelefoneInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Create) )
                    result = await base.CreateEntity<IdInput, TelefoneInput>(_telefoneService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Edit))
                    result = await base.UpdateEntity<IdInput, TelefoneInput>(_telefoneService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Create,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Create,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Edit)]
        public async Task<TelefoneInput> SaveAndReturnEntity(TelefoneInput input)
        {
            TelefoneInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Create))
                    result = await base.CreateAndReturnEntity<TelefoneInput, TelefoneInput>(_telefoneService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Edit))
                    result = await base.UpdateAndReturnEntity<TelefoneInput, TelefoneInput>(_telefoneService, input, () => ValidateInput(input), ProcessInput);
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }


        [AbpAuthorize(
           AppPermissions.Pages_Tenant_Global_PessoaFisicaTelefone_Delete,
           AppPermissions.Pages_Tenant_Global_PessoaJuridicaTelefone_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_telefoneService, input);
        }

       
        public async Task<TelefoneInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TelefoneInput, IdInput>(_telefoneService, input);
        }

        public async Task<TelefoneInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TelefoneInput, IdInput>(_telefoneService, input);
        }

        public async Task<PagedResultDto<TelefoneListDto>> GetTelefonesPaginado(GetTelefoneInput input)
        {
            IList<WhereIfCondition<Telefone>> condicoes = new List<WhereIfCondition<Telefone>>();

            condicoes.Add(new WhereIfCondition<Telefone>(true, x => x.Pessoa.Id == input.PessoaId));
            condicoes.Add(new WhereIfCondition<Telefone>(!input.Numero.IsNullOrEmpty(), x => x.Numero.Contains(input.Numero)));

            var result = await base.GetListPaged<TelefoneListDto, GetTelefoneInput>(_telefoneService, input, condicoes);

            return result;
        }

        public async Task<TelefoneInput> GetByPessoaId(IdInput input)
        {
            var queryTelefone = await _telefoneService
                  .GetAll()
                  .Where(x => x.PessoaId == input.Id && x.Tipo == Domain.Global.Enums.TipoTelefoneEnum.Celular)
                  .Select(x => new TelefoneInput
                  {
                      Id = x.Id,
                      DDI = x.DDI,
                      DDD = x.DDD,
                      Numero = x.Numero,
                      PessoaId = input.Id,
                      Tipo = x.Tipo
                  }).FirstOrDefaultAsync();

                return queryTelefone;
        }
    }
}