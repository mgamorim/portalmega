﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento;
using EZ.EZControl.EZ.EzFieldValidation;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_TipoDeDocumento,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                  AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class TipoDeDocumentoAppService : EZControlAppServiceBase<TipoDeDocumento>, ITipoDeDocumentoAppService
    {
        private readonly ITipoDeDocumentoService _tipoDeDocumentoService;
        private readonly IPessoaService _pessoaService;

        public TipoDeDocumentoAppService(
            IRepository<TipoDeDocumento> repository,
            ITipoDeDocumentoService tipoDeDocumentoService,
            IPessoaService pessoaService)
            : base(repository)
        {
            _tipoDeDocumentoService = tipoDeDocumentoService;
            _pessoaService = pessoaService;
        }

        private void ValidateInput(TipoDeDocumentoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (_tipoDeDocumentoService.CheckForFieldDuplicated(input.Id, input.Descricao, x => x.Descricao == input.Descricao))
            {
                validationErrors.Add(new ValidationResult(L("TipoDeDocumento.DuplicateDescricaoError"), new[] { "descricao".ToCamelCase() }));
            }

            if (input.TipoPessoa != null)
            {
                if (!Enum.IsDefined(typeof(TipoPessoaEnum), input.TipoPessoa))
                {
                    validationErrors.Add(new ValidationResult(L("TipoDeDocumento.InvalidTipoPessoaError"), new[] { "tipoPessoa".ToCamelCase() }));
                }
            }
            else
            {
                validationErrors.Add(new ValidationResult(L("TipoDeDocumento.RequiredTipoPessoaError"), new[] { "tipoPessoa".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Create, AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Edit)]
        public async Task<IdInput> Save(TipoDeDocumentoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Create))
                    result = await base.CreateEntity<IdInput, TipoDeDocumentoInput>(_tipoDeDocumentoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Edit))
                    result = await base.UpdateEntity<IdInput, TipoDeDocumentoInput>(_tipoDeDocumentoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Create, AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Edit)]
        public async Task<TipoDeDocumentoInput> SaveAndReturnEntity(TipoDeDocumentoInput input)
        {
            TipoDeDocumentoInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Create))
                    result = await base.CreateAndReturnEntity<TipoDeDocumentoInput, TipoDeDocumentoInput>(_tipoDeDocumentoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }

            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Edit))
                    result = await base.UpdateAndReturnEntity<TipoDeDocumentoInput, TipoDeDocumentoInput>(_tipoDeDocumentoService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        public async Task<TipoDeDocumentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TipoDeDocumentoInput, IdInput>(_tipoDeDocumentoService, input);
        }

        public async Task<TipoDeDocumentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TipoDeDocumentoInput, IdInput>(_tipoDeDocumentoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeDocumento_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeDocumento_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeDocumento_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_tipoDeDocumentoService, input);
        }

        public async Task<ListResultDto<TipoDeDocumentoListDto>> GetTipoDeDocumentos(GetTipoDeDocumentoInput input)
        {
            try
            {
                bool tipoPessoaDefinido = input.TipoPessoa != null && Enum.IsDefined(typeof(TipoPessoaEnum), input.TipoPessoa);

                var query = _tipoDeDocumentoService
                    .GetAll()
                    .WhereIf(!input.Descricao.IsNullOrEmpty(), x => x.Descricao.Contains(input.Descricao))
                    .WhereIf(tipoPessoaDefinido, x => x.TipoPessoa == input.TipoPessoa);

                var list = await query.ToListAsync();

                var listDto = list.MapTo<List<TipoDeDocumentoListDto>>();
                var result = new ListResultDto<TipoDeDocumentoListDto>(listDto);

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<ListResultDto<TipoDeDocumentoListDto>> GetByPessoaId(IdInput input)
        {
            try
            {
                var pessoa = await _pessoaService.GetById(input.Id);

                var query = _tipoDeDocumentoService
                    .GetAll()
                    .Where(x => x.TipoPessoa == pessoa.TipoPessoa);

                var list = await query.ToListAsync();

                var listDto = list.MapTo<List<TipoDeDocumentoListDto>>();
                var result = new ListResultDto<TipoDeDocumentoListDto>(listDto);

                return result;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<PagedResultDto<TipoDeDocumentoListDto>> GetTipoDeDocumentosPaginado(GetTipoDeDocumentoInput input)
        {
            IList<WhereIfCondition<TipoDeDocumento>> condicoes = new List<WhereIfCondition<TipoDeDocumento>>();

            condicoes.Add(new WhereIfCondition<TipoDeDocumento>(!input.Descricao.IsNullOrEmpty(), x => x.Descricao.Contains(input.Descricao)));
            condicoes.Add(new WhereIfCondition<TipoDeDocumento>(input.TipoPessoa != null, x => x.TipoPessoa == input.TipoPessoa));

            var result = await base.GetListPaged<TipoDeDocumentoListDto, GetTipoDeDocumentoInput>(_tipoDeDocumentoService, input, condicoes);

            return result;
        }

        public async Task<TipoDeDocumentoListDto> GetTipoDeDocumentoByDescricao(GetTipoDeDocumentoInput input)
        {
            var tipoDeDocumento = await _tipoDeDocumentoService
                .GetAll()
                .Where(x => x.Descricao == input.Descricao)
                .FirstOrDefaultAsync();

            var result = tipoDeDocumento.MapTo<TipoDeDocumentoListDto>();

            return result;
        }

        public async Task<TipoDeDocumentoListDto> GetTipoDeDocumentoByTipoFixo(GetTipoDeDocumentoInput input)
        {
            var tipoDeDocumento = _tipoDeDocumentoService.GetByTipoFixo(input.TipoDeDocumento.Value);

            return tipoDeDocumento.MapTo<TipoDeDocumentoListDto>();
        }


        public EzFieldValidation IsCpf(string cpf)
        {
            bool isValid = _tipoDeDocumentoService.IsCpf(cpf);
            string name = L("Enum_TipoDeDocumentoEnum_Cpf");
            string messageError = L("TipoDeDocumento.MessagemDeErroValidacaoCpf");

            return new EzFieldValidation(isValid, name, messageError);
        }

        public EzFieldValidation IsCnpj(string cnpj)
        {
            bool isValid = _tipoDeDocumentoService.IsCnpj(cnpj);
            string name = L("Enum_TipoDeDocumentoEnum_Cnpj");
            string messageError = L("TipoDeDocumento.MessagemDeErroValidacaoCnpj");

            return new EzFieldValidation(isValid, name, messageError);
        }

        public EzFieldValidation IsPis(string pis)
        {
            bool isValid = _tipoDeDocumentoService.IsPis(pis);
            string name = L("Enum_TipoDeDocumentoEnum_Pis");
            string messageError = L("TipoDeDocumento.MessagemDeErroValidacaoPis");

            return new EzFieldValidation(isValid, name, messageError);
        }
    }
}