﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_GrupoPessoa,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]

    public class GrupoPessoaAppService : EZControlAppServiceBase<GrupoPessoa>, IGrupoPessoaAppService
    {
        private readonly IGrupoPessoaService _grupoPessoaService;

        public GrupoPessoaAppService(IRepository<GrupoPessoa> grupoPessoaRepository, IGrupoPessoaService grupoPessoaService)
            : base(grupoPessoaRepository)
        {
            _grupoPessoaService = grupoPessoaService;
        }

        private void ValidateInput(GrupoPessoaInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("GrupoPessoa.RequiredNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (CheckForFieldDuplicated(Repository, input.Id, input.Nome, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("GrupoPessoa.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(GrupoPessoa grupoPessoa, GrupoPessoaInput input)
        {
            if (input.PaiId > 0)
            {
                try
                {
                    grupoPessoa.Pai = await _grupoPessoaService.GetAll().Where(x => x.Id == input.PaiId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("GrupoPessoa.NotFoundError"), ex.Message);
                }
            }
            if (CheckForDuplicateInstance<GrupoPessoa, int>(Repository, input.Id, x => x.Slug == input.Slug))
            {
                throw new UserFriendlyException(L("GrupoPessoa.DuplicateSlugError"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Create, AppPermissions.Pages_Tenant_Global_GrupoPessoa_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Edit)]
        public async Task<IdInput> Save(GrupoPessoaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Create))
                    result = await base.CreateEntity<IdInput, GrupoPessoaInput>(_grupoPessoaService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Edit))
                    result = await base.UpdateEntity<IdInput, GrupoPessoaInput>(_grupoPessoaService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(
            AppPermissions.Pages_Tenant_Global_GrupoPessoa_Create, AppPermissions.Pages_Tenant_Global_GrupoPessoa_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Edit,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Edit)]
        public async Task<GrupoPessoaInput> SaveAndReturnEntity(GrupoPessoaInput input)
        {
            GrupoPessoaInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Create))
                    result = await base.CreateAndReturnEntity<GrupoPessoaInput, GrupoPessoaInput>(_grupoPessoaService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Edit))
                    result = await base.UpdateAndReturnEntity<GrupoPessoaInput, GrupoPessoaInput>(_grupoPessoaService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }

            }

            return result;
        }

        public async Task<PagedResultDto<GrupoPessoaListDto>> GetGruposPessoaPaginado(GetGrupoPessoaInput input)
        {
            var condicoes = new List<WhereIfCondition<GrupoPessoa>>
            {
                new WhereIfCondition<GrupoPessoa>(!input.Nome.IsNullOrEmpty(),
                    x => x.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<GrupoPessoaListDto, GetGrupoPessoaInput>(_grupoPessoaService, input, condicoes);
            return result;
        }

        public async Task<PagedResultDto<GrupoPessoaListDto>> GetGruposPessoaExceptForId(GetGrupoPessoaExceptForIdInput input)
        {
            var query = _grupoPessoaService
                .GetAll()
                .Where(x => x.Id != input.GrupoPessoaId && x.TipoDePessoa == input.TipoDePessoa)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Nome), x => x.Nome.Contains(input.Nome));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<GrupoPessoaListDto>>();

            return new PagedResultDto<GrupoPessoaListDto>(count, listDtos);
        }

        public async Task<GrupoPessoaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<GrupoPessoaInput, IdInput>(_grupoPessoaService, input);
        }

        public async Task<GrupoPessoaListDto> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<GrupoPessoaListDto, IdInput>(_grupoPessoaService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_GrupoPessoa_Delete,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaGrupoPessoa_Delete,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaGrupoPessoa_Delete)]
        public async Task Delete(IdInput input)
        {
            try
            {
                await base.DeleteEntity(_grupoPessoaService, input);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAllowAnonymous]
        public async Task<List<string>> GetGrupoPessoa(GetGrupoPessoaInput input)
        {
            var grupos = await base.Repository.GetAllListAsync(x => x.Nome.Contains(input.Nome));

            return grupos.Select(x => x.Nome).ToList();
        }
    }
}