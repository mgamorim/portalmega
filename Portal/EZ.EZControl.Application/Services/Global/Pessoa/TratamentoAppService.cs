﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Tratamento;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Tratamento, 
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaTratamento,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaTratamento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TratamentoAppService : EZControlAppServiceBase<Tratamento>, ITratamentoAppService
    {
        private readonly ITratamentoService _tratamentoService;

        public TratamentoAppService(
            IRepository<Tratamento, int> tratamentoRepository,
            ITratamentoService tratamentoService)
            : base(tratamentoRepository)
        {
            _tratamentoService = tratamentoService;
        }

        private void ValidateInput(TratamentoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
                validationErrors.Add(new ValidationResult(string.Format(L("Tratamento.RequiredDescricaoError"), input.Descricao), new[] { "descricao".ToCamelCase() }));

            if (_tratamentoService.CheckForDuplicateInstance(input.Id, x => x.Descricao == input.Descricao))
                validationErrors.Add(new ValidationResult(string.Format(L("Tratamento.DuplicateDescricaoError"), input.Descricao), new[] { "descricao".ToCamelCase() }));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Tratamento_Create, AppPermissions.Pages_Tenant_Global_Tratamento_Edit)]
        public async Task<IdInput> Save(TratamentoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Tratamento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, TratamentoInput>(_tratamentoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Tratamento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, TratamentoInput>(_tratamentoService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Tratamento_Create, AppPermissions.Pages_Tenant_Global_Tratamento_Edit)]
        public async Task<TratamentoInput> SaveAndReturnEntity(TratamentoInput input)
        {
            TratamentoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Tratamento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<TratamentoInput, TratamentoInput>(_tratamentoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Tratamento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<TratamentoInput, TratamentoInput>(_tratamentoService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<TratamentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TratamentoInput, IdInput>(_tratamentoService, input);
        }

        public async Task<TratamentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TratamentoInput, IdInput>(_tratamentoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Tratamento_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_tratamentoService, input);
        }

        public async Task<PagedResultDto<TratamentoListDto>> GetTratamentosPaginado(GetTratamentoInput input)
        {
            IList<WhereIfCondition<Tratamento>> condicoes = new List<WhereIfCondition<Tratamento>>();

            condicoes.Add(new WhereIfCondition<Tratamento>(!input.Descricao.IsNullOrEmpty(), x => x.Descricao.Contains(input.Descricao)));

            var result = await base.GetListPaged<TratamentoListDto, GetTratamentoInput>(_tratamentoService, input, condicoes);

            return result;
        }
    }
}