﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaFisica, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PessoaFisicaAppService : PessoaAppServiceBase<PessoaFisica>, IPessoaFisicaAppService
    {
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaService _pessoaService;
        private readonly ITratamentoService _tratamentoService;
        private readonly IDocumentoService _documentoService;
        private readonly IEnderecoEletronicoService _enderecoEletronicoService;

        public PessoaFisicaAppService(
            IRepository<PessoaFisica, int> repository,
            IPessoaService pessoaService,
            IPessoaFisicaService pessoaFisicaService,
            IGrupoPessoaService grupoPessoaService,
            IDocumentoService documentoService,
            IEnderecoEletronicoService enderecoEletronicoService,
            ITratamentoService tratamentoService)
            : base(repository, pessoaService, grupoPessoaService, documentoService)
        {
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaService = pessoaService;
            _tratamentoService = tratamentoService;
            _documentoService = documentoService;
            _enderecoEletronicoService = enderecoEletronicoService;
        }

        protected override void ValidateInput(PessoaInput input)
        {
            base.ValidateInput(input);

            var validationErrors = new List<ValidationResult>();

            if (input != null)
            {
                PessoaFisicaInput pessoaFisicaInput = input as PessoaFisicaInput;

                if (pessoaFisicaInput != null)
                {
                    if (pessoaFisicaInput.Sexo != null)
                    {
                        if (!Enum.IsDefined(typeof(SexoEnum), pessoaFisicaInput.Sexo))
                        {
                            validationErrors.Add(new ValidationResult(L("PessoaFisica.InvalidSexoError"), new[] { "sexo".ToCamelCase() }));
                        }
                    }

                    if (pessoaFisicaInput.EstadoCivil != null)
                    {
                        if (!Enum.IsDefined(typeof(EstadoCivilEnum), pessoaFisicaInput.EstadoCivil))
                        {
                            validationErrors.Add(new ValidationResult("PessoaFisica.InvalidEstadoCivilError", new[] { "estadoCivil".ToCamelCase() }));
                        }
                    }
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected override async Task ProcessInput(Domain.Global.Pessoa.Pessoa pessoa, PessoaInput input)
        {
            await base.ProcessInput(pessoa, input);

            if (input != null)
            {
                PessoaFisicaInput pessoaFisicaInput = input as PessoaFisicaInput;
                PessoaFisica pessoaFisica = pessoa as PessoaFisica;

                if (pessoaFisica.Documentos == null)
                    pessoaFisica.Documentos = new List<Documento>();

                if (pessoaFisica != null && pessoaFisicaInput != null)
                {
                    // Tratamento
                    if (pessoaFisicaInput.TratamentoId > 0)
                    {
                        try
                        {
                            var tratamento = await _tratamentoService.GetById(pessoaFisicaInput.TratamentoId);
                            pessoaFisica.AssociarTratamento(tratamento, _pessoaFisicaService);
                        }
                        catch (Exception)
                        {
                            throw new UserFriendlyException(L("Tratamento.NotFoundError"));
                        }
                    }
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaFisica_Create, AppPermissions.Pages_Tenant_Global_PessoaFisica_Edit)]
        public async Task<IdInput> Save(PessoaFisicaInput input)
        {
            //CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter);
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisica_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, PessoaFisicaInput>(_pessoaFisicaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisica_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, PessoaFisicaInput>(_pessoaFisicaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaFisica_Create, AppPermissions.Pages_Tenant_Global_PessoaFisica_Edit)]
        public async Task<PessoaFisicaInput> SaveAndReturnEntity(PessoaFisicaInput input)
        {
            //CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter);
            PessoaFisicaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisica_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<PessoaFisicaInput, PessoaFisicaInput>(_pessoaFisicaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisica_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<PessoaFisicaInput, PessoaFisicaInput>(_pessoaFisicaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PessoaFisicaListDto> GetById(IdInput input)
        {
            return await base.GetEntityById<PessoaFisicaListDto, IdInput>(_pessoaFisicaService, input);
        }

        public async Task<PessoaFisicaInput> GetId(IdInput input)
        {
            return await base.GetEntityById<PessoaFisicaInput, IdInput>(_pessoaFisicaService, input);
        }

        public async Task<PessoaFisicaListDto> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PessoaFisicaListDto, IdInput>(_pessoaFisicaService, input);
        }

        // TODO: Marcos 2016-07-25.  O comit será de forma manual para conseguir pegar a exceção gerada pelo banco.
        // No caso de violação de FKs, exibir uma mensagem para o usuário
        [UnitOfWork(IsDisabled = true)]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaFisica_Delete)]
        public async Task Delete(IdInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                try
                {
                    await base.DeleteEntity(_pessoaFisicaService, input);
                    trans.Complete();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.InnerException != null)
                    {
                        var sqlEx = ex.InnerException.InnerException;
                        if (ex.InnerException.InnerException is SqlException)
                        {
                            if (sqlEx.Message.Contains("FK"))
                            {
                                throw new UserFriendlyException("Não foi possível excluir esse registro, ele está sendo referenciado por outras entidades.");
                            }
                        }
                    }
                }
            }
        }

        public async Task<PagedResultDto<PessoaFisicaListDto>> GetPessoasPaginado(GetPessoaInput input)
        {
            var condicoes = new List<WhereIfCondition<PessoaFisica>>
            {
                new WhereIfCondition<PessoaFisica>(
                        !string.IsNullOrEmpty(input.Nome) ||
                        !string.IsNullOrEmpty(input.DescricaoGrupoPessoa),
                            x =>
                        x.Nome.Contains(input.Nome) ||
                        (x.GrupoPessoa != null && x.GrupoPessoa.Descricao.Contains(input.DescricaoGrupoPessoa)))
            };
            var result = await base.GetListPaged<PessoaFisicaListDto, GetPessoaInput>(_pessoaFisicaService, input, condicoes);

            return result;
        }

        public async Task<PagedResultDto<PessoaFisicaByCPFListDto>> GetByCPFPaginado(GetPessoaFisicaByCPFInput input)
        {
            //.Select(x => x.Pessoa as PessoaFisica)
            //.ToListAsync();


            var pessoas = await _documentoService
                   .GetAll()
                   .Where(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf && x.Numero.StartsWith(input.CPFPessoaFisica))
                   .Select(x => x.Pessoa as PessoaFisica)
                   .ToListAsync();
            //.Select(x => new PessoaFisicaByCPFListDto
            //{
            //    Id = x.PessoaId,
            //    Nome = x.Pessoa.NomePessoa.ToString(),
            //    //Nome = _pessoaFisicaService.GetById(x.PessoaId).Result.Nome,
            //    CPF = x.Numero,
            //    GrupoPessoaId = x.Pessoa.GrupoPessoa != null ? x.Pessoa.GrupoPessoa.Id : 0,
            //    GrupoPessoaNome = x.Pessoa.GrupoPessoa != null ? x.Pessoa.GrupoPessoa.Descricao : null,
            //}
            //        )
            //.ToListAsync();

            //var n = _pessoaFisicaService.GetById(x.PessoaId)

            var listDtos = pessoas.MapTo<List<PessoaFisicaByCPFListDto>>();


            return new PagedResultDto<PessoaFisicaByCPFListDto>(pessoas.Count, listDtos);



            //var queryPessoaFisica = _pessoaFisicaService
            //       .GetAll()
            //       .WhereIf(!string.IsNullOrEmpty(input.CPFPessoaFisica), x => x.Cpf.Contains(input.CPFPessoaFisica))
            //       .Select(x => new PessoaListDto
            //       {
            //           Id = x.Id,
            //           NomePessoa = x.Nome,
            //           GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
            //           GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
            //           {
            //               Id = x.GrupoPessoa.Id,
            //               Descricao = x.GrupoPessoa.Descricao,
            //               PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
            //           } : null,
            //           TipoPessoa = TipoPessoaEnum.Fisica
            //       });


        }


        public async Task<PessoaFisicaListDto> GetPessoaByCpf(GetPessoaFisicaByCPFInput input)
        {
            var pessoaFisica = await _pessoaFisicaService.GetPessoaByCpf(input.CPFPessoaFisica);
            return pessoaFisica.MapTo<PessoaFisicaListDto>();
        }


    }
}


