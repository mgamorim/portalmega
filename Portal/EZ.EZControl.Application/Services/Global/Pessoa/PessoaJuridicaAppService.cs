﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Pessoa
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaJuridica, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PessoaJuridicaAppService : PessoaAppServiceBase<PessoaJuridica>, IPessoaJuridicaAppService
    {
        private readonly IPessoaJuridicaService _pessoaJuridicaService;

        public PessoaJuridicaAppService(
            IRepository<PessoaJuridica, int> repository,
            IPessoaService pessoaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IGrupoPessoaService grupoPessoaService,
            IDocumentoService documentoService)
            : base(repository, pessoaService, grupoPessoaService, documentoService)
        {
            _pessoaJuridicaService = pessoaJuridicaService;
        }

        protected override void ValidateInput(PessoaInput input)
        {
            base.ValidateInput(input);
        }

        protected override async Task ProcessInput(Domain.Global.Pessoa.Pessoa pessoa, PessoaInput input)
        {
            await base.ProcessInput(pessoa, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridica_Edit)]
        public async Task<IdInput> Save(PessoaJuridicaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, PessoaJuridicaInput>(_pessoaJuridicaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, PessoaJuridicaInput>(_pessoaJuridicaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridica_Edit)]
        public async Task<PessoaJuridicaInput> SaveAndReturnEntity(PessoaJuridicaInput input)
        {
            PessoaJuridicaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<PessoaJuridicaInput, PessoaJuridicaInput>(_pessoaJuridicaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<PessoaJuridicaInput, PessoaJuridicaInput>(_pessoaJuridicaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PessoaJuridicaListDto> GetById(IdInput input)
        {
            return await base.GetEntityById<PessoaJuridicaListDto, IdInput>(_pessoaJuridicaService, input);
        }

        public async Task<PessoaJuridicaListDto> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PessoaJuridicaListDto, IdInput>(_pessoaJuridicaService, input);
        }

        // TODO: Marcos 2016-07-25.  O comit será de forma manual para conseguir pegar a exceção gerada pelo banco.
        // No caso de violação de FKs, exibir uma mensagem para o usuário
        [UnitOfWork(IsDisabled = true)]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_PessoaJuridica_Delete)]
        public async Task Delete(IdInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                try
                {
                    await base.DeleteEntity(_pessoaJuridicaService, input);
                    trans.Complete();
                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null && ex.InnerException.InnerException != null)
                    {
                        var sqlEx = ex.InnerException.InnerException;
                        if (ex.InnerException.InnerException is SqlException)
                        {
                            if (sqlEx.Message.Contains("FK"))
                            {
                                throw new UserFriendlyException("Não foi possível excluir esse registro, ele está sendo referenciado por outras entidades.");
                            }
                        }
                    }
                }
            }
        }

        public async Task<PagedResultDto<PessoaJuridicaListDto>> GetPessoasPaginado(GetPessoaInput input)
        {
            var condicoes = new List<WhereIfCondition<PessoaJuridica>>
            {
                new WhereIfCondition<PessoaJuridica>(
                        !string.IsNullOrEmpty(input.Nome) ||
                        !string.IsNullOrEmpty(input.DescricaoGrupoPessoa),
                            x =>
                        x.NomeFantasia.Contains(input.Nome) ||
                        x.RazaoSocial.Contains(input.Nome) ||
                        (x.GrupoPessoa != null && x.GrupoPessoa.Descricao.Contains(input.DescricaoGrupoPessoa)))
            };
            var result = await base.GetListPaged<PessoaJuridicaListDto, GetPessoaInput>(_pessoaJuridicaService, input, condicoes);

            return result;
        }
    }
}