﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using EZ.EZControl.Services.EZLiv.Interfaces;
using System.Data.Entity;
using Abp.Linq.Extensions;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Filters;

namespace EZ.EZControl.Services.Global.Geral
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Profissao, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ProfissaoAppService : EZControlAppServiceBase<Profissao>, IProfissaoAppService
    {
        private readonly IProfissaoService _profissaoService;
        private readonly IProdutoDePlanoDeSaudeService _produtoService;
        public ProfissaoAppService(IRepository<Profissao> profissaoRepository, 
            IProfissaoService profissaoService,
            IProdutoDePlanoDeSaudeService produtoService)
             : base(profissaoRepository)
        {
            _profissaoService = profissaoService;
            _produtoService = produtoService;
        }

        private void ValidateInput(ProfissaoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Codigo))
                validationErrors.Add(new ValidationResult(string.Format(L("Profissao.RequiredCodigoError"), input.Codigo), new[] { "codigo".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Titulo))
                validationErrors.Add(new ValidationResult(string.Format(L("Profissao.RequiredTituloError"), input.Titulo), new[] { "titulo".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Codigo == input.Codigo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Profissao.DuplicateCodigoError"), input.Codigo), new[] { "codigo".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Titulo == input.Titulo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Profissao.DuplicateTituloError"), input.Titulo), new[] { "titulo".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<PagedResultDto<ProfissaoListDto>> GetProfissoesPaginado(GetProfissaoInput input)
        {
            var condicoes = new List<WhereIfCondition<Profissao>>
            {
                new WhereIfCondition<Profissao>(
                    !(string.IsNullOrEmpty(input.Codigo)) ||
                    !(string.IsNullOrEmpty(input.Titulo)),
                    x =>
                        x.Codigo.Contains(input.Codigo) ||
                        x.Titulo.Contains(input.Titulo))
            };

            var result = await base.GetListPaged<ProfissaoListDto, GetProfissaoInput>(_profissaoService, input, condicoes);
            return result;
        }

        public async Task<ProfissaoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ProfissaoInput, IdInput>(_profissaoService, input);
        }

        public async Task<ProfissaoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ProfissaoInput, IdInput>(_profissaoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Profissao_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_profissaoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Profissao_Create, AppPermissions.Pages_Tenant_Global_Profissao_Edit)]
        public async Task<IdInput> Save(ProfissaoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Profissao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ProfissaoInput>(_profissaoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Profissao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ProfissaoInput>(_profissaoService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Profissao_Create, AppPermissions.Pages_Tenant_Global_Profissao_Edit)]
        public async Task<ProfissaoInput> SaveAndReturnEntity(ProfissaoInput input)
        {
            ProfissaoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Profissao_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ProfissaoInput, ProfissaoInput>(_profissaoService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Profissao_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ProfissaoInput, ProfissaoInput>(_profissaoService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<List<ProfissaoAutoCompleteListDto>> Get(GetProfissaoInput input)
        {
            var all = _profissaoService.GetAll();

            all = all.WhereIf(
                !string.IsNullOrEmpty(input.Titulo) || !string.IsNullOrEmpty(input.Codigo),
                    x =>
                        x.Titulo.ToLower().Contains(input.Titulo.ToLower())
                        || x.Codigo.ToLower().Contains(input.Codigo.ToLower())).AsQueryable();

            return all.ToList().MapTo<List<ProfissaoAutoCompleteListDto>>();
        }

        [AbpAllowAnonymous]
        public async Task<List<string>> GetProfissao(GetProfissaoInput input)
        {
            var profissoes = await base.Repository.GetAllListAsync(x => x.Titulo.Contains(input.Titulo));

            return profissoes.Select(x => x.Titulo).ToList();
        }

        public async Task<PagedResultDto<ProfissaoListDto>> GetWithProduto(GetProfissaoInput input)
        {
            //Busco todos os produtos que possuam chancelas que possuam profissoes
            var produtos = await _produtoService.GetAll().Where(x => x.Chancelas.Any(c => c.Profissoes.Any())).ToListAsync();
            //Seleciono somente os ids dessas profissoes
            var q = (from i in produtos
                    from c in i.Chancelas
                    from p in c.Profissoes
                    select p.Id).ToList();

            var query = await _profissaoService.GetAll().Where(x => q.Contains(x.Id)).ToListAsync();

            var count = query.Count;
            var dados = query
                .OrderBy(x => x.Codigo);

            var listDtos = dados.MapTo<List<ProfissaoListDto>>();
            return new PagedResultDto<ProfissaoListDto>(count, listDtos);
        }
    }
}