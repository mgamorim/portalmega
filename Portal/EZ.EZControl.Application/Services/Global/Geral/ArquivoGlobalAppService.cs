﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Arquivo;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Arquivo, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ArquivoGlobalAppService : EZControlAppServiceBase<ArquivoGlobal>, IArquivoGlobalAppService
    {
        private readonly IArquivoGlobalService _arquivoGlobalService;

        public ArquivoGlobalAppService(IRepository<ArquivoGlobal> arquivoRepository, IArquivoGlobalService service)
            : base(arquivoRepository)
        {
            _arquivoGlobalService = service;
        }

        private void ValidateInput(ArquivoGlobalInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(
                    new ValidationResult(String.Format("Já existe um arquivo com esse nome.", input.Nome),
                        new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Arquivo_Create, AppPermissions.Pages_Tenant_Global_Arquivo_Edit)]
        public async Task<IdInput> Save(ArquivoGlobalInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Arquivo_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ArquivoGlobalInput>(_arquivoGlobalService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Arquivo_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ArquivoGlobalInput>(_arquivoGlobalService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Arquivo_Create, AppPermissions.Pages_Tenant_Global_Arquivo_Edit)]
        public async Task<ArquivoGlobalInput> SaveAndReturnEntity(ArquivoGlobalInput input)
        {
            ArquivoGlobalInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Arquivo_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ArquivoGlobalInput, ArquivoGlobalInput>(_arquivoGlobalService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Arquivo_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ArquivoGlobalInput, ArquivoGlobalInput>(_arquivoGlobalService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Arquivo_Delete)]
        public async Task Delete(IdInput input)
        {
            await ApplyEmpresaFilter(async () =>
            {
                var path = this.Repository.GetAll()
                            .Where(x => x.Id == input.Id)
                            .Select(x => x.Path)
                            .FirstOrDefault();

                this.RemoveFile(HttpRuntime.AppDomainAppPath + path);
                await base.DeleteEntity(_arquivoGlobalService, input);
            });
        }

        public async Task<PagedResultDto<ArquivoGlobalListDto>> GetArquivosPaginado(GetArquivoGlobalInput input)
        {
            TipoDeArquivoEnum tipo;
            var enumValue = Enum.TryParse(input.Nome, true, out tipo);
            var condicoes = new List<WhereIfCondition<ArquivoGlobal>>
            {
                new WhereIfCondition<ArquivoGlobal>(!(string.IsNullOrEmpty(input.Nome)) ||
                                                        enumValue,
                                                        a =>
                                                        a.Nome.Contains(input.Nome)  ||
                                                        a.TipoDeArquivoFixo == tipo)
            };
            var result = await base.GetListPaged<ArquivoGlobalListDto, GetArquivoGlobalInput>(_arquivoGlobalService, input, condicoes);
            return result;
        }

        [AbpAllowAnonymous]
        public async Task<ArquivoGlobalInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ArquivoGlobalInput, IdInput>(_arquivoGlobalService, input);
        }

        //public async Task<ArquivoGlobalInput> GetLogoEmpresaById(IdInput input)
        //{           
        //    return await base.GetEntityById<ArquivoGlobalInput, IdInput>(_arquivoGlobalService, input);
        //}

        public async Task<ArquivoGlobalInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ArquivoGlobalInput, IdInput>(_arquivoGlobalService, input);
        }

        private async void RemoveFile(String path)
        {
            await Task.Factory.StartNew(() =>
            {
                if (!String.IsNullOrEmpty(path))
                    File.Delete(path);
            });
        }

        public async Task<ArquivoGlobalInput> GetIdByName(ArquivoGlobalInput input)
        {
            var arquivo = await _arquivoGlobalService.GetAll().Where(x => x.Nome.ToLower() == input.Nome.ToLower()).FirstOrDefaultAsync();
            return arquivo.MapTo<ArquivoGlobalInput>();
        }
    }

}
