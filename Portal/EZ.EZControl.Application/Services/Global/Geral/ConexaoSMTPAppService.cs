﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.ConexaoSMTP;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ConexaoSMTP, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ConexaoSMTPAppService : EZControlAppServiceBase<ConexaoSMTP>, IConexaoSMTPAppService
    {
        private readonly IConexaoSMTPService _conexaoSMTPService;

        public ConexaoSMTPAppService(IRepository<ConexaoSMTP, int> repository, IConexaoSMTPService conexaoSMTPService)
            : base(repository)
        {
            _conexaoSMTPService = conexaoSMTPService;
        }

        private void ValidateInput(ConexaoSMTPInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("ConexaoSMTP.EmptyNomeConexaoSMTPError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.ServidorSMTP))
                validationErrors.Add(new ValidationResult(String.Format(L("ConexaoSMTP.EmptyServidorConexaoSMTPError"), input.ServidorSMTP), new[] { "servidorSMTP".ToCamelCase() }));

            if (input.Porta <= 0)
                validationErrors.Add(new ValidationResult(String.Format(L("ConexaoSMTP.EmptyPortaConexaoSMTPError"), input.Porta), new[] { "porta".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("ConexaoSMTP.DuplicateNomeConexaoSMTPError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (input.RequerAutenticacao && (string.IsNullOrEmpty(input.UsuarioAutenticacao) || string.IsNullOrEmpty(input.SenhaAutenticacao)))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("ConexaoSMTP.RequerAutenticacaoNoUserPasswordError"), input.Nome), new[] { "usuarioAutenticacao".ToCamelCase(), "senhaAutenticacao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Create, AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Edit)]
        public async Task<IdInput> Save(ConexaoSMTPInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ConexaoSMTPInput>(_conexaoSMTPService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ConexaoSMTPInput>(_conexaoSMTPService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Create, AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Edit)]
        public async Task<ConexaoSMTPInput> SaveAndReturnEntity(ConexaoSMTPInput input)
        {
            ConexaoSMTPInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ConexaoSMTPInput, ConexaoSMTPInput>(_conexaoSMTPService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ConexaoSMTP_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ConexaoSMTPInput, ConexaoSMTPInput>(_conexaoSMTPService, input, () => ValidateInput(input));
            }

            return result;
        }

        public async Task<PagedResultDto<ConexaoSMTPListDto>> GetConexoesSMTPPaginado(GetConexaoSMTPInput input)
        {
            var condicoes = new List<WhereIfCondition<ConexaoSMTP>>
            {
                new WhereIfCondition<ConexaoSMTP>(
                        !(string.IsNullOrEmpty(input.Nome)) ||
                        !(string.IsNullOrEmpty(input.ServidorSMTP)),
                    a =>
                        a.Nome.Contains(input.Nome) ||
                        a.ServidorSMTP.Contains(input.ServidorSMTP))
            };
            var result = await base.GetListPaged<ConexaoSMTPListDto, GetConexaoSMTPInput>(_conexaoSMTPService, input, condicoes);
            return result;
        }

        public async Task<ConexaoSMTPInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ConexaoSMTPInput, IdInput>(_conexaoSMTPService, input);
        }

        public async Task<ConexaoSMTPInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ConexaoSMTPInput, IdInput>(_conexaoSMTPService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ConexaoSMTP)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_conexaoSMTPService, input);
        }
    }
}