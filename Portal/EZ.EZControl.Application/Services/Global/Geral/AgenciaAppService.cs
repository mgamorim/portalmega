﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Agencia;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Agencia, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class AgenciaAppService : EZControlAppServiceBase<Agencia>, IAgenciaAppService
    {
        private readonly IBancoService _bancoService;
        private readonly IAgenciaService _agenciaService;

        public AgenciaAppService(IRepository<Agencia> agenciaRepository,
            IBancoService bancoService, IAgenciaService agenciaService)
            : base(agenciaRepository)
        {
            _bancoService = bancoService;
            _agenciaService = agenciaService;
        }


        private void ValidateInput(AgenciaInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (input.BancoId == 0)
            {
                validationErrors.Add(new ValidationResult(L("Agencia.BancoObrigatorioError")));
            }

            if (string.IsNullOrEmpty(input.Codigo))
            {
                validationErrors.Add(new ValidationResult(L("Agencia.CodigoObrigatorioError")));
            }

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(L("Agencia.NomeObrigatorioError")));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Banco.Id == input.BancoId && x.Codigo == input.Codigo && (string.IsNullOrEmpty(input.DigitoVerificador) || x.DigitoVerificador == input.DigitoVerificador)))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Agencia.DuplicateAgenciaError"), (string.IsNullOrEmpty(input.DigitoVerificador) ? input.Codigo : input.Codigo + "-" + input.DigitoVerificador), input.Nome), new[] { "codigo".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Agencia agencia, AgenciaInput agenciaInput)
        {
            if (agenciaInput.BancoId > 0)
            {
                try
                {
                    agencia.Banco = await _bancoService.GetAll().Where(x => x.Id == agenciaInput.BancoId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Banco.NotFoundError"), ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Agencia_Create, AppPermissions.Pages_Tenant_Global_Agencia_Edit)]
        public async Task<IdInput> Save(AgenciaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Agencia_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, AgenciaInput>(_agenciaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Agencia_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, AgenciaInput>(_agenciaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Agencia_Create, AppPermissions.Pages_Tenant_Global_Agencia_Edit)]
        public async Task<AgenciaInput> SaveAndReturnEntity(AgenciaInput input)
        {
            AgenciaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Agencia_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<AgenciaInput, AgenciaInput>(_agenciaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Agencia_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<AgenciaInput, AgenciaInput>(_agenciaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Agencia_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_agenciaService, input);
        }

        public async Task<PagedResultDto<AgenciaListDto>> GetAgenciasPaginado(GetAgenciaInput input)
        {
            var condicoes = new List<WhereIfCondition<Agencia>>
            {
                new WhereIfCondition<Agencia>(
                    !string.IsNullOrEmpty(input.Codigo) ||
                    !string.IsNullOrEmpty(input.CodigoBanco) ||
                    !string.IsNullOrEmpty(input.NomeBanco) ||
                    !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Codigo.Contains(input.Codigo) ||
                        a.Banco.Codigo.Contains(input.CodigoBanco) ||
                        a.Banco.Nome.Contains(input.NomeBanco) ||
                        a.Nome.Contains(input.Nome))
            };

            var result = await base.GetListPaged<AgenciaListDto, GetAgenciaInput>(_agenciaService, input, condicoes);
            return result;
        }

        public async Task<ListResultDto<AgenciaListDto>> GetAgenciaByBanco(IdInput input)
        {
            var agencias = await Repository
                .GetAll()
                .Where(a => a.Banco.Id == input.Id)
                .OrderBy(x => x.Banco.Nome)
                .ThenBy(x => x.Nome)
                .ToListAsync();
            var result = new ListResultDto<AgenciaListDto>(agencias.MapTo<List<AgenciaListDto>>());
            return result;
        }

        public async Task<AgenciaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<AgenciaInput, IdInput>(_agenciaService, input);
        }

        public async Task<AgenciaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<AgenciaInput, IdInput>(_agenciaService, input);
        }
    }
}