﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.TemplateEmail;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TemplateEmail, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TemplateEmailAppService : EZControlAppServiceBase<TemplateEmail>, ITemplateEmailAppService
    {
        private readonly ITemplateEmailService _templateEmailService;

        public TemplateEmailAppService(IRepository<TemplateEmail> templateEmailRepository,
            ITemplateEmailService templateEmailService)
            : base(templateEmailRepository)
        {
            _templateEmailService = templateEmailService;
        }

        private void ValidateInput(TemplateEmailInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("TemplateDeEmail.EmptyNomeError"), new[] { "nome".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Remetente))
                validationErrors.Add(new ValidationResult(L("TemplateDeEmail.EmptyRemetenteError"), new[] { "remetente".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Assunto))
                validationErrors.Add(new ValidationResult(L("TemplateDeEmail.EmptyAssuntoError"), new[] { "assunto".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.CorpoMensagem))
                validationErrors.Add(new ValidationResult(L("TemplateDeEmail.EmptyCorpoDaMensagemError"), new[] { "corpoMensagem".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(L("TemplateDeEmail.DuplicateNomeError"), new[] { "nome".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TemplateEmail_Create, AppPermissions.Pages_Tenant_Global_TemplateEmail_Edit)]
        public async Task<IdInput> Save(TemplateEmailInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TemplateEmail_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, TemplateEmailInput>(_templateEmailService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TemplateEmail_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, TemplateEmailInput>(_templateEmailService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TemplateEmail_Create, AppPermissions.Pages_Tenant_Global_TemplateEmail_Edit)]
        public async Task<TemplateEmailInput> SaveAndReturnEntity(TemplateEmailInput input)
        {
            TemplateEmailInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TemplateEmail_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<TemplateEmailInput, TemplateEmailInput>(_templateEmailService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TemplateEmail_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<TemplateEmailInput, TemplateEmailInput>(_templateEmailService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TemplateEmail_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_templateEmailService, input);
        }

        public async Task<PagedResultDto<TemplateEmailListDto>> GetTemplatesEmailPaginado(GetTemplateEmailInput input)
        {
            var condicoes = new List<WhereIfCondition<TemplateEmail>>
            {
                new WhereIfCondition<TemplateEmail>(
                        !input.Nome.IsNullOrEmpty() ||
                        !input.Remetente.IsNullOrEmpty() ||
                        !input.Assunto.IsNullOrEmpty() ,
                            t =>
                        t.Nome.Contains(input.Nome) ||
                        t.Remetente.Contains(input.Remetente) ||
                        t.Assunto.Contains(input.Assunto))
            };
            var result = await base.GetListPaged<TemplateEmailListDto, GetTemplateEmailInput>(_templateEmailService, input, condicoes);
            return result;
        }

        public async Task<TemplateEmailInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TemplateEmailInput, IdInput>(_templateEmailService, input);
        }

        public async Task<TemplateEmailInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TemplateEmailInput, IdInput>(_templateEmailService, input);
        }
    }
}