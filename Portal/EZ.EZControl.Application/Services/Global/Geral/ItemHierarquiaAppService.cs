﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.ItemHierarquia;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ItemHierarquia, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ItemHierarquiaAppService : EZControlAppServiceBase<ItemHierarquia>, IItemHierarquiaAppService
    {
        private readonly IItemHierarquiaService _itemHierarquiaService;

        public ItemHierarquiaAppService(IRepository<ItemHierarquia> itemHierarquiaRepository,
            IItemHierarquiaService itemHierarquiaService)
            : base(itemHierarquiaRepository)
        {
            _itemHierarquiaService = itemHierarquiaService;
        }

        private void ValidateInput(ItemHierarquiaInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("ItemHierarquia.RequiredNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Codigo == input.Codigo))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("ItemHierarquia.DuplicateCodigoError"), input.Codigo), new[] { "codigo".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("ItemHierarquia.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ItemHierarquia itemHierarquia, ItemHierarquiaInput itemHierarquiaInput)
        {
            if (itemHierarquiaInput.ItemHierarquiaPaiId != null && itemHierarquiaInput.ItemHierarquiaPaiId > 0)
            {
                try
                {
                    itemHierarquia.ItemHierarquiaPai = await _itemHierarquiaService.GetAll().Where(x => x.Id == itemHierarquiaInput.ItemHierarquiaPaiId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("ItemHierarquia.NotFoundError"), ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Create, AppPermissions.Pages_Tenant_Global_ItemHierarquia_Edit)]
        public async Task<IdInput> Save(ItemHierarquiaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ItemHierarquiaInput>(_itemHierarquiaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ItemHierarquiaInput>(_itemHierarquiaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Create, AppPermissions.Pages_Tenant_Global_ItemHierarquia_Edit)]
        public async Task<ItemHierarquiaInput> SaveAndReturnEntity(ItemHierarquiaInput input)
        {
            ItemHierarquiaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ItemHierarquiaInput, ItemHierarquiaInput>(_itemHierarquiaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ItemHierarquiaInput, ItemHierarquiaInput>(_itemHierarquiaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_ItemHierarquia_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_itemHierarquiaService, input);
        }

        public async Task<PagedResultDto<ItemHierarquiaListDto>> GetItensHierarquiaPaginado(GetItemHierarquiaInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemHierarquia>>
            {
                new WhereIfCondition<ItemHierarquia>(
                        !input.Nome.IsNullOrEmpty() ||
                        !input.Codigo.IsNullOrEmpty() ||
                        !input.Mascara.IsNullOrEmpty(),
                            i =>
                        i.Nome.Contains(input.Nome) ||
                        i.Codigo.Contains(input.Codigo) ||
                        i.Mascara.Contains(input.Mascara))
            };
            var result = await base.GetListPaged<ItemHierarquiaListDto, GetItemHierarquiaInput>(_itemHierarquiaService, input, condicoes);
            return result;
        }

        public async Task<ItemHierarquiaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ItemHierarquiaInput, IdInput>(_itemHierarquiaService, input);
        }

        public async Task<ItemHierarquiaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ItemHierarquiaInput, IdInput>(_itemHierarquiaService, input);
        }

        public async Task<PagedResultDto<ItemHierarquiaListDto>> GetItensHierarquiaExceptForId(GetItemHierarquiaExceptForIdInput input)
        {
            var query = _itemHierarquiaService
                .GetAll()
                .Where(x => x.Id != input.ItemHierarquiaId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Nome), x => x.Nome.Contains(input.Nome));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<ItemHierarquiaListDto>>();

            return new PagedResultDto<ItemHierarquiaListDto>(count, listDtos);
        }
    }
}