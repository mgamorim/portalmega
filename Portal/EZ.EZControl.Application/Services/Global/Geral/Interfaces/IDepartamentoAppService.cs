﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Departamento;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IDepartamentoAppService : IApplicationService
    {
        Task<IdInput> Save(DepartamentoInput input);

        Task<DepartamentoInput> SaveAndReturnEntity(DepartamentoInput input);

        Task<PagedResultDto<DepartamentoListDto>> GetDepartamentosPaginado(GetDepartamentoInput input);

        Task<DepartamentoInput> GetById(IdInput input);

        Task<DepartamentoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}