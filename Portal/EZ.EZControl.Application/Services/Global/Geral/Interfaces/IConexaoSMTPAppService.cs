﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.ConexaoSMTP;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IConexaoSMTPAppService : IApplicationService
    {
        Task<IdInput> Save(ConexaoSMTPInput input);

        Task<ConexaoSMTPInput> SaveAndReturnEntity(ConexaoSMTPInput input);

        Task<PagedResultDto<ConexaoSMTPListDto>> GetConexoesSMTPPaginado(GetConexaoSMTPInput input);

        Task<ConexaoSMTPInput> GetById(IdInput input);

        Task<ConexaoSMTPInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}