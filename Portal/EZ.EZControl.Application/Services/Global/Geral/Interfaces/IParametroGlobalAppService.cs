﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Parametro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IParametroGlobalAppService : IApplicationService
    {
        Task<IdInput> Save(ParametroGlobalInput input);
        Task<ParametroGlobalListDto> Get();
    }
}