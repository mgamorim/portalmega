﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Arquivo;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IArquivoGlobalAppService : IApplicationService
    {
        Task<IdInput> Save(ArquivoGlobalInput input);
        Task<ArquivoGlobalInput> SaveAndReturnEntity(ArquivoGlobalInput input);
        Task<PagedResultDto<ArquivoGlobalListDto>> GetArquivosPaginado(GetArquivoGlobalInput input);
        Task<ArquivoGlobalInput> GetById(IdInput input);
        Task<ArquivoGlobalInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
        Task<ArquivoGlobalInput> GetIdByName(ArquivoGlobalInput input);
    }
}