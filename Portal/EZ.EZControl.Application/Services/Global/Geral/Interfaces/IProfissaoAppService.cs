﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IProfissaoAppService : IApplicationService
    {
        Task<IdInput> Save(ProfissaoInput input);
        Task<ProfissaoInput> SaveAndReturnEntity(ProfissaoInput input);
        Task<PagedResultDto<ProfissaoListDto>> GetProfissoesPaginado(GetProfissaoInput input);
        Task<ProfissaoInput> GetById(IdInput input);
        Task<ProfissaoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
        Task<List<ProfissaoAutoCompleteListDto>> Get(GetProfissaoInput input);
        Task<List<string>> GetProfissao(GetProfissaoInput input);
        Task<PagedResultDto<ProfissaoListDto>> GetWithProduto(GetProfissaoInput input);
    }
}