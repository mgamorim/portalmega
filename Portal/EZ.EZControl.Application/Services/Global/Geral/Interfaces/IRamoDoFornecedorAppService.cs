﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.RamoDoFornecedor;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IRamoDoFornecedorAppService : IApplicationService
    {
        Task<IdInput> Save(RamoDoFornecedorInput input);

        Task<RamoDoFornecedorInput> SaveAndReturnEntity(RamoDoFornecedorInput input);

        Task<PagedResultDto<RamoDoFornecedorListDto>> GetRamosDoFornecedorPaginado(GetRamoDoFornecedorInput input);

        Task<RamoDoFornecedorInput> GetById(IdInput input);

        Task<RamoDoFornecedorInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}