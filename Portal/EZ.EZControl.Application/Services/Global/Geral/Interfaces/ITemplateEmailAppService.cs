﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.TemplateEmail;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface ITemplateEmailAppService : IApplicationService
    {
        Task<IdInput> Save(TemplateEmailInput input);

        Task<TemplateEmailInput> SaveAndReturnEntity(TemplateEmailInput input);

        Task<PagedResultDto<TemplateEmailListDto>> GetTemplatesEmailPaginado(GetTemplateEmailInput input);

        Task<TemplateEmailInput> GetById(IdInput input);

        Task<TemplateEmailInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}