﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Agencia;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IAgenciaAppService : IApplicationService
    {
        Task<IdInput> Save(AgenciaInput input);

        Task<AgenciaInput> SaveAndReturnEntity(AgenciaInput input);

        Task<PagedResultDto<AgenciaListDto>> GetAgenciasPaginado(GetAgenciaInput input);

        Task<AgenciaInput> GetById(IdInput input);

        Task<AgenciaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<ListResultDto<AgenciaListDto>> GetAgenciaByBanco(IdInput input);
    }
}