﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.ItemHierarquia;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IItemHierarquiaAppService : IApplicationService
    {
        Task<IdInput> Save(ItemHierarquiaInput input);

        Task<ItemHierarquiaInput> SaveAndReturnEntity(ItemHierarquiaInput input);

        Task<PagedResultDto<ItemHierarquiaListDto>> GetItensHierarquiaPaginado(GetItemHierarquiaInput input);

        Task<PagedResultDto<ItemHierarquiaListDto>> GetItensHierarquiaExceptForId(GetItemHierarquiaExceptForIdInput input);

        Task<ItemHierarquiaInput> GetById(IdInput input);

        Task<ItemHierarquiaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}