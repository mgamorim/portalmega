﻿using Abp.Application.Services;
using EZ.EZControl.Dto.Global.Geral.Email;
using System;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IEmailAppService : IApplicationService
    {
        Task SendEmail(EmailDto email);
        Task SendEmailInput(EmailInput email);
        Task SendEmailSmtp(String destinatario, Int32 smtpId, Int32 templateDeEmailId);
    }
}
