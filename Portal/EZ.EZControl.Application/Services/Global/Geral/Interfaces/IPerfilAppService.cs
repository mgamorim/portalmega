﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Perfil;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IPerfilAppService : IApplicationService
    {
        Task<IdInput> Save(PerfilInput input);
        Task<PerfilInput> Get();
    }
}
