﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Banco;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IBancoAppService : IApplicationService
    {
        Task<IdInput> Save(BancoInput input);
        Task<BancoInput> SaveAndReturnEntity(BancoInput input);
        Task<PagedResultDto<BancoListDto>> GetBancosPaginado(GetBancoInput input);
        Task<BancoInput> GetById(IdInput input);
        Task<BancoInput> GetByExternalId(IdInput input);
        Task Delete(IdInput input);
    }
}