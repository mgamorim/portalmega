﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Geral.Feriado;
using System;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral.Interfaces
{
    public interface IFeriadoAppService : IApplicationService
    {
        Task<IdInput> Save(FeriadoInput input);

        Task<FeriadoInput> SaveAndReturnEntity(FeriadoInput input);

        Task<PagedResultDto<FeriadoListDto>> GetFeriadosPaginado(GetFeriadoInput input);

        Task<FeriadoInput> GetById(IdInput input);

        Task<FeriadoInput> GetByExternalId(IdInput input);

        Task<ListResultDto<FeriadoListDto>> GetByPais(IdInput input);

        Task<ListResultDto<FeriadoListDto>> GetByEstado(IdInput input);

        Task<ListResultDto<FeriadoListDto>> GetByCidade(IdInput input);

        Task<bool> IsFeriado(DateTime data);

        Task Delete(IdInput input);
    }
}