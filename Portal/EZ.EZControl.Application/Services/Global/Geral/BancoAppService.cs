﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Banco;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Banco, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class BancoAppService : EZControlAppServiceBase<Banco>, IBancoAppService
    {
        private readonly IBancoService bancoService;

        public BancoAppService(IRepository<Banco> bancoRepository, IBancoService bancoService)
            : base(bancoRepository)
        {
            this.bancoService = bancoService;
        }

        private void ValidateInput(BancoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Banco.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Codigo == input.Codigo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Banco.DuplicateCodigoError"), input.Codigo), new[] { "codigo".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Banco.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Banco_Create, AppPermissions.Pages_Tenant_Global_Banco_Edit)]
        public async Task<IdInput> Save(BancoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Banco_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, BancoInput>(bancoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Banco_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, BancoInput>(bancoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Banco_Create, AppPermissions.Pages_Tenant_Global_Banco_Edit)]
        public async Task<BancoInput> SaveAndReturnEntity(BancoInput input)
        {
            BancoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Banco_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<BancoInput, BancoInput>(bancoService, input, () => ValidateInput(input), null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Banco_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<BancoInput, BancoInput>(bancoService, input, () => ValidateInput(input), null);
            }

            return result;
        }

        public async Task<PagedResultDto<BancoListDto>> GetBancosPaginado(GetBancoInput input)
        {
            var condicoes = new List<WhereIfCondition<Banco>>
            {
                new WhereIfCondition<Banco>(
                        !string.IsNullOrEmpty(input.Codigo) ||
                        !string.IsNullOrEmpty(input.Nome),
                    a =>
                        a.Codigo.Contains(input.Codigo) ||
                        a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<BancoListDto, GetBancoInput>(bancoService, input, condicoes);

            return result;
        }

        public async Task<BancoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<BancoInput, IdInput>(bancoService, input);
        }

        public async Task<BancoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<BancoInput, IdInput>(bancoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Banco_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(bancoService, input);
        }
    }
}