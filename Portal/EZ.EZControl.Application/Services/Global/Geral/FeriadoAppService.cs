﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Feriado;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Feriado, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class FeriadoAppService : EZControlAppServiceBase<Feriado>, IFeriadoAppService
    {
        private readonly IPaisService _paisService;
        private readonly IEstadoService _estadoService;
        private readonly ICidadeService _cidadeService;
        private readonly IFeriadoService _feriadoService;

        public FeriadoAppService(IRepository<Feriado> feriadoRepository,
            IPaisService paisService,
            IEstadoService estadoService,
            ICidadeService cidadeService,
            IFeriadoService feriadoService)
            : base(feriadoRepository)
        {
            _paisService = paisService;
            _estadoService = estadoService;
            _cidadeService = cidadeService;
            _feriadoService = feriadoService;
        }

        private void ValidateInput(FeriadoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("Feriado.RequiredNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (input.Data == null || input.Data == DateTime.MinValue || input.Data == DateTime.MaxValue)
                validationErrors.Add(new ValidationResult(String.Format(L("Feriado.RequiredDataError"), input.Data), new[] { "data".ToCamelCase() }));

            if (input.Abrangencia == null)
                validationErrors.Add(new ValidationResult(String.Format(L("Feriado.RequiredAbrangenciaError"), input.Abrangencia), new[] { "abrangencia".ToCamelCase() }));

            if (input.Tipo == null)
                validationErrors.Add(new ValidationResult(String.Format(L("Feriado.RequiredTipoError"), input.Tipo), new[] { "tipo".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Feriado.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (input.Abrangencia == AbrangenciaDoFeriadoEnum.Municipal)
            {
                if (CheckForDuplicateInstance(Repository, input.Id, x => x.Data == input.Data && x.Pais.Id == input.PaisId && x.Estado.Id == input.EstadoId && x.Cidade.Id == input.CidadeId))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Feriado.DuplicateFeriadoNacional"), input.Data.ToString("dd/MM/yyyy")), new[] { "data".ToCamelCase(), "pais".ToCamelCase(), "estado".ToCamelCase(), "cidade".ToCamelCase() }));
                }
            }
            if (input.Abrangencia == AbrangenciaDoFeriadoEnum.Estadual)
            {
                if (CheckForDuplicateInstance(Repository, input.Id, x => x.Data == input.Data && x.Pais.Id == input.PaisId && x.Estado.Id == input.EstadoId))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Feriado.DuplicateFeriadoEstadual"), input.Data.ToString("dd/MM/yyyy")), new[] { "data".ToCamelCase(), "pais".ToCamelCase(), "estado".ToCamelCase() }));
                }
            }
            if (input.Abrangencia == AbrangenciaDoFeriadoEnum.Nacional)
            {
                if (CheckForDuplicateInstance(Repository, input.Id, x => x.Data == input.Data && x.Pais.Id == input.PaisId))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Feriado.DuplicateFeriadoMunicipal"), input.Data.ToString("dd/MM/yyyy")), new[] { "data".ToCamelCase(), "pais".ToCamelCase() }));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Feriado feriado, FeriadoInput input)
        {
            if (input.PaisId > 0)
                feriado.Pais = await _paisService.GetAll().Where(x => x.Id == input.PaisId).FirstOrDefaultAsync();

            if (input.EstadoId > 0)
                feriado.Estado = await _estadoService.GetAll().Where(x => x.Id == input.EstadoId).FirstOrDefaultAsync();

            if (input.CidadeId > 0)
                feriado.Cidade = await _cidadeService.GetAll().Where(x => x.Id == input.CidadeId).FirstOrDefaultAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Feriado_Create, AppPermissions.Pages_Tenant_Global_Feriado_Edit)]
        public async Task<IdInput> Save(FeriadoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Feriado_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, FeriadoInput>(_feriadoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Feriado_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, FeriadoInput>(_feriadoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Feriado_Create, AppPermissions.Pages_Tenant_Global_Feriado_Edit)]
        public async Task<FeriadoInput> SaveAndReturnEntity(FeriadoInput input)
        {
            FeriadoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Feriado_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<FeriadoInput, FeriadoInput>(_feriadoService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Feriado_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<FeriadoInput, FeriadoInput>(_feriadoService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<PagedResultDto<FeriadoListDto>> GetFeriadosPaginado(GetFeriadoInput input)
        {
            var condicoes = new List<WhereIfCondition<Feriado>>
            {

                new WhereIfCondition<Feriado>(
                    !(string.IsNullOrEmpty(input.Nome)) ||
                    input.Data > DateTime.MinValue && input.Data < DateTime.MaxValue,
                    a =>
                        a.Nome.Contains(input.Nome) ||
                        a.Data == input.Data)
            };

            var result = await base.GetListPaged<FeriadoListDto, GetFeriadoInput>(_feriadoService, input, condicoes);
            return result;
        }

        public async Task<ListResultDto<FeriadoListDto>> GetByCidade(IdInput input)
        {
            var feriados = await Repository
                .GetAll()
                .Where(f =>
                    f.Cidade.Id == input.Id
                ).
                OrderBy(x => x.Data)
                .ToListAsync();
            return new ListResultDto<FeriadoListDto>(feriados.MapTo<List<FeriadoListDto>>());
        }

        public async Task<ListResultDto<FeriadoListDto>> GetByEstado(IdInput input)
        {
            var feriados = await Repository
                .GetAll()
                .Where(f =>
                    f.Estado.Id == input.Id
                ).
                OrderBy(x => x.Data)
                .ToListAsync();
            return new ListResultDto<FeriadoListDto>(feriados.MapTo<List<FeriadoListDto>>());
        }

        public async Task<FeriadoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<FeriadoInput, IdInput>(_feriadoService, input);
        }

        public async Task<FeriadoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<FeriadoInput, IdInput>(_feriadoService, input);
        }

        public async Task<ListResultDto<FeriadoListDto>> GetByPais(IdInput input)
        {
            var feriados = await Repository
                .GetAll()
                .Where(f =>
                    f.Pais.Id == input.Id
                ).
                OrderBy(x => x.Data)
                .ToListAsync();
            return new ListResultDto<FeriadoListDto>(feriados.MapTo<List<FeriadoListDto>>());
        }

        public async Task<bool> IsFeriado(DateTime data)
        {
            var feriado = await Repository
                .GetAll()
                .Where(x => x.Data == data)
                .FirstOrDefaultAsync();
            return (feriado != null);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Feriado_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_feriadoService, input);
        }
    }
}