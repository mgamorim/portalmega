﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.Runtime.Validation;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Parametro;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using EZ.EZControl.Authorization;
using System.Data.Entity;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Parametros, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ParametroGlobalAppService : EZControlAppServiceBase<ParametroGlobal>, IParametroGlobalAppService
    {
        private readonly IParametroGlobalService _parametroGlobalService;
        private readonly IConexaoSMTPService _conexaoSMTPService;

        public ParametroGlobalAppService(IRepository<ParametroGlobal> parametroRepository,
            IParametroGlobalService parametroGlobalService, 
            IConexaoSMTPService conexaoSMTPService)
            : base(parametroRepository)
        {
            this._parametroGlobalService = parametroGlobalService;
            this._conexaoSMTPService = conexaoSMTPService;
        }

        private void ValidateInput(ParametroGlobalInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ParametroGlobal parametro, ParametroGlobalInput input)
        {
            if (input.ConexaoSMTPId > 0)
            {
                ConexaoSMTP conexao = await _conexaoSMTPService.GetAll().Where(x => x.Id == input.ConexaoSMTPId).FirstOrDefaultAsync();
                parametro.ConexaoSMTP = conexao;
            }
            else
            {
                throw new AbpValidationException(L("Parametro.EmptyConexaoSMTPError"));
            }
        }

        public async Task<IdInput> Save(ParametroGlobalInput input)
        {
            var parametro = await Get();
            if (parametro != null)
                input.Id = parametro.Id;

            IdInput savedEntity;

            if (input.Id == default(int))
                savedEntity = await base.CreateEntity<IdInput, ParametroGlobalInput>(_parametroGlobalService, input, () => ValidateInput(input), ProcessInput);
            else
                savedEntity = await base.UpdateEntity<IdInput, ParametroGlobalInput>(_parametroGlobalService, input, () => ValidateInput(input), ProcessInput);

            parametro = await Get();
            //Email
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.DefaultFromAddress, parametro.ConexaoSMTPEnderecoDeEmailPadrao);
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.DefaultFromDisplayName, parametro.ConexaoSMTPNome);
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Host, parametro.ConexaoSMTPServidorSMTP);
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Port, parametro.ConexaoSMTPPorta.ToString());
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.UserName, parametro.ConexaoSMTPUsuarioAutenticacao);
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Password, parametro.ConexaoSMTPSenhaAutenticacao);
            //await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.Domain, input.Email.SmtpDomain);
            await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.EnableSsl, parametro.ConexaoSMTPRequerConexaoCriptografada.ToString(CultureInfo.InvariantCulture).ToLower(CultureInfo.InvariantCulture));
            //await SettingManager.ChangeSettingForApplicationAsync(EmailSettingNames.Smtp.UseDefaultCredentials, input.Email.SmtpUseDefaultCredentials.ToString(CultureInfo.InvariantCulture).ToLower(CultureInfo.InvariantCulture));

            return savedEntity;
        }

        public async Task<ParametroGlobalListDto> Get()
        {
            var condicoes = new List<WhereIfCondition<ParametroGlobal>>();
            condicoes.Add(new WhereIfCondition<ParametroGlobal>(true, x => x.Id > 0));
            var parametros = await base.GetList<ParametroGlobalListDto>(_parametroGlobalService, condicoes, x => x.Id);

            return parametros.Items.FirstOrDefault();
        }
    }
}