﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Departamento;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Departamento, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class DepartamentoAppService : EZControlAppServiceBase<Departamento>, IDepartamentoAppService
    {
        private readonly IDepartamentoService service;

        public DepartamentoAppService(IRepository<Departamento, int> repository, IDepartamentoService service)
            : base(repository)
        {
            this.service = service;
        }

        private void ValidateInput(DepartamentoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("Departamento.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Departamento.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<PagedResultDto<DepartamentoListDto>> GetDepartamentosPaginado(GetDepartamentoInput input)
        {
            var condicoes = new List<WhereIfCondition<Departamento>>
            {
                new WhereIfCondition<Departamento>(!input.Nome.IsNullOrEmpty(), x => x.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<DepartamentoListDto, GetDepartamentoInput>(service, input, condicoes);
            return result;
        }

        public async Task<DepartamentoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<DepartamentoInput, IdInput>(service, input);
        }

        public async Task<DepartamentoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<DepartamentoInput, IdInput>(service, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Departamento_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(service, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Departamento_Create, AppPermissions.Pages_Tenant_Global_Departamento_Edit)]
        public async Task<IdInput> Save(DepartamentoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Departamento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, DepartamentoInput>(service, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Departamento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, DepartamentoInput>(service, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Departamento_Create, AppPermissions.Pages_Tenant_Global_Departamento_Edit)]
        public async Task<DepartamentoInput> SaveAndReturnEntity(DepartamentoInput input)
        {
            DepartamentoInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Departamento_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<DepartamentoInput, DepartamentoInput>(service, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Departamento_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<DepartamentoInput, DepartamentoInput>(service, input, () => ValidateInput(input));
            }

            return result;
        }
    }
}