﻿using Abp.Authorization;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Email;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Abp.AutoMapper;
using EZ.EZControl.EZ.EzNotification;
using EZ.EZControl.EZ.EzNotification.Interfaces;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_SendEmail, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EmailAppService : EZControlAppServiceBase, IEmailAppService
    {
        private readonly IConexaoSMTPService _conexaoSmtpService;
        private readonly ITemplateEmailService _templateEmailService;
        private readonly IParametroGlobalService _parametroGlobalService;
        private readonly IEmailNotificationService _emailNotificationService;

        public EmailAppService(IConexaoSMTPService conexaoSmtpService,
                               ITemplateEmailService templateEmailService,
                               IParametroGlobalService parametroService,
                               IEmailNotificationService emailNotificationService)
        {
            _conexaoSmtpService = conexaoSmtpService;
            _templateEmailService = templateEmailService;
            _parametroGlobalService = parametroService;
            _emailNotificationService = emailNotificationService;
        }

        public async Task SendEmail(EmailDto email)
        {
            try
            {
                var templateEntity = email.TemplateDeEmail.MapTo<TemplateEmail>();

                await _emailNotificationService.Send(email.Destinatario, templateEntity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task SendEmailInput(EmailInput email)
        {
            try
            {
                ConexaoSMTP smtp = null;
                if (email.SmtpId > 0)
                {
                    smtp = await _conexaoSmtpService.GetById(email.SmtpId);
                }

                TemplateEmail templateEmail = null;
                if (email.TemplateId > 0)
                {
                    templateEmail = await _templateEmailService.GetById(email.TemplateId);
                }

                var templateEntity = templateEmail.MapTo<TemplateEmail>();

                await _emailNotificationService.Send(email.Destinatario, templateEntity, smtp);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task SendEmailSmtp(string destinatario, int smtpId, int templateDeEmailId)
        {
            try
            {
                ConexaoSMTP smtp = null;
                if (smtpId > 0)
                {
                    smtp = await _conexaoSmtpService.GetById(smtpId);
                }

                TemplateEmail templateEmail = null;
                if (templateDeEmailId > 0)
                {
                    templateEmail = await _templateEmailService.GetById(templateDeEmailId);
                }

                await _emailNotificationService.Send(destinatario, templateEmail, smtp);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}