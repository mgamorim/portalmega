﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.RamoDoFornecedor;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class RamoDoFornecedorAppService : EZControlAppServiceBase<RamoDoFornecedor>, IRamoDoFornecedorAppService
    {
        private readonly IRamoDoFornecedorService _ramoDoFornecedorService;

        public RamoDoFornecedorAppService(IRepository<RamoDoFornecedor> ramoDoFornecedorRepository,
            IRamoDoFornecedorService ramoDoFornecedorService)
            : base(ramoDoFornecedorRepository)
        {
            this._ramoDoFornecedorService = ramoDoFornecedorService;
        }

        private void ValidateInput(RamoDoFornecedorInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("RamoDoFornecedor.RequiredNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("RamoDoFornecedor.DuplicateRamoDoFornecedorError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<PagedResultDto<RamoDoFornecedorListDto>> GetRamosDoFornecedorPaginado(GetRamoDoFornecedorInput input)
        {
            var condicoes = new List<WhereIfCondition<RamoDoFornecedor>>
            {
                new WhereIfCondition<RamoDoFornecedor>(!input.Nome.IsNullOrEmpty(), r => r.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<RamoDoFornecedorListDto, GetRamoDoFornecedorInput>(_ramoDoFornecedorService, input, condicoes);
            return result;
        }

        public async Task<RamoDoFornecedorInput> GetById(IdInput input)
        {
            return await base.GetEntityById<RamoDoFornecedorInput, IdInput>(_ramoDoFornecedorService, input);
        }

        public async Task<RamoDoFornecedorInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<RamoDoFornecedorInput, IdInput>(_ramoDoFornecedorService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_ramoDoFornecedorService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Create, AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Edit)]
        public async Task<IdInput> Save(RamoDoFornecedorInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, RamoDoFornecedorInput>(_ramoDoFornecedorService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, RamoDoFornecedorInput>(_ramoDoFornecedorService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Create, AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Edit)]
        public async Task<RamoDoFornecedorInput> SaveAndReturnEntity(RamoDoFornecedorInput input)
        {
            RamoDoFornecedorInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<RamoDoFornecedorInput, RamoDoFornecedorInput>(_ramoDoFornecedorService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_RamoDoFornecedor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<RamoDoFornecedorInput, RamoDoFornecedorInput>(_ramoDoFornecedorService, input, () => ValidateInput(input));
            }

            return result;
        }
    }
}