﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Authorization.Users.Profile;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Filters;
using EZ.EZControl.Filters.Attribute;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Dto.Global.Geral.Perfil;
using EZ.EZControl.Authorization.Roles;
using System;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.HTTP;
using System.Configuration;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace EZ.EZControl.Services.Global.Geral
{
    [DisableFilter(EzControlFilters.EmpresaFilter)]
    public class PerfilAppService : EZControlAppServiceBase<PessoaFisica>, IPerfilAppService
    {
        private readonly IPerfilService perfilService;
        private readonly IDocumentoService documentoService;
        private readonly ITipoDeDocumentoService tipoDeDocumentoService;
        private readonly IEmpresaService empresaService;
        private readonly ICorretorService corretorService;
        private readonly ICorretorDadosBancarioService corretorDadosBancarioService;
        private readonly ICorretorPessoaJuridcaService corretorPessoaJuridcaService;
        private readonly IBancoService bancoService;
        private readonly ICidadeService cidadeService;
        private readonly ITipoDeLogradouroService tipoDeLogradouroService;
        private readonly IEnderecoService enderecoService;
        private readonly ITelefoneService telefoneService;
        private readonly IPessoaService pessoaService;
        private readonly IProfileAppService _profileAppService;
        private readonly UserManager _userManager;

        public PerfilAppService(IRepository<PessoaFisica> perfilRepository,
                                       IPerfilService perfilService,
                                       IDocumentoService documentoService,
                                       ITipoDeDocumentoService tipoDeDocumentoService,
                                       IEmpresaService empresaService,
                                       ICorretorService corretorService,
                                       ICorretorDadosBancarioService corretorDadosBancarioService,
                                       ICorretorPessoaJuridcaService corretorPessoaJuridcaService,
                                       IBancoService bancoService,
                                       ICidadeService cidadeService,
                                       ITipoDeLogradouroService tipoDeLogradouroService,
                                       IEnderecoService enderecoService,
                                       ITelefoneService telefoneService,
                                       IPessoaService pessoaService,
                                       IProfileAppService profileAppService,
                                       UserManager userManager)
            : base(perfilRepository)
        {
            this.perfilService = perfilService;
            this.documentoService = documentoService;
            this.empresaService = empresaService;
            this.corretorService = corretorService;
            this.corretorDadosBancarioService = corretorDadosBancarioService;
            this.corretorPessoaJuridcaService = corretorPessoaJuridcaService;
            this.bancoService = bancoService;
            this.tipoDeDocumentoService = tipoDeDocumentoService;
            this.cidadeService = cidadeService;
            this.tipoDeLogradouroService = tipoDeLogradouroService;
            this.enderecoService = enderecoService;
            this.telefoneService = telefoneService;
            this.pessoaService = pessoaService;
            this._profileAppService = profileAppService;
            this._userManager = userManager;
        }

        private void ValidateInputBankPersona(PerfilInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (input.CorretorDadosBancario != null)
                if (!(input.CorretorDadosBancario.BancoId != 0 && input.CorretorDadosBancario.TipoConta != null && !string.IsNullOrEmpty(input.CorretorDadosBancario.CodigoAgencia) && !string.IsNullOrEmpty(input.CorretorDadosBancario.ContaCorrente)))
                {
                    if (input.CorretorDadosBancario.BancoId == 0 && string.IsNullOrEmpty(input.CorretorDadosBancario.CodigoAgencia) && string.IsNullOrEmpty(input.CorretorDadosBancario.ContaCorrente))
                    {

                    }
                    else
                    {
                        if (input.CorretorDadosBancario.BancoId == 0)
                            validationErrors.Add(new ValidationResult("Selecione um banco!"));

                        if (string.IsNullOrEmpty(input.CorretorDadosBancario.CodigoAgencia))
                        {
                            validationErrors.Add(new ValidationResult("Digite o código da agencia!"));
                        }
                        else
                        {
                            // VALIDAÇÃO AGENCIA

                            if (input.CorretorDadosBancario.BancoId == 55 || input.CorretorDadosBancario.BancoId == 45 || input.CorretorDadosBancario.BancoId == 6)
                            {
                                bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^[0-9]{4}$");

                                if (ehValido == false)
                                {
                                    validationErrors.Add(new ValidationResult("Código da agencia Incorreto ! Insira no Formato (9999) "));
                                }
                            }
                            if (input.CorretorDadosBancario.BancoId == 48 || input.CorretorDadosBancario.BancoId == 1)
                            {
                                bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^\d{4}-\d{1}$");

                                if (ehValido == false)
                                {
                                    validationErrors.Add(new ValidationResult("Código da agencia Incorreto ! Insira no Formato (9999-D) "));
                                }
                            }



                            // VALIDAÇÃO CONTA CORRENTE
                            if (input.CorretorDadosBancario.BancoId == 1 || input.CorretorDadosBancario.BancoId == 45) // BB Santander
                            {
                                bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^\d{8}-\d{1}$");

                                if (ehValido == false)
                                {
                                    validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (99999999-D) "));
                                }
                            }

                            if (input.CorretorDadosBancario.BancoId == 6) // Caixa
                            {
                                // XXX99999999 - D(X: Operação)

                                bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^\d{11}-\d{1}$");

                                if (ehValido == false)
                                {
                                    validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (XXX99999999-D) (X: Operação) "));
                                }
                            }

                            if (input.CorretorDadosBancario.BancoId == 48) // bradesco
                            {
                                //9999999-D
                                bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^\d{7}-\d{1}$");

                                if (ehValido == false)
                                {
                                    validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (9999999-D) "));
                                }
                            }

                            if (input.CorretorDadosBancario.BancoId == 55) // Itaú
                            {
                                //99999-D
                                bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^\d{5}-\d{1}$");

                                if (ehValido == false)
                                {
                                    validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (99999-D) "));
                                }
                            }


                            /*

                            if (id == 55)
                            {
                                banco = "Itaú";
                            }

                            if (id == 48)
                            {
                                banco = "Bradesco";
                            }
                            if (id == 45)
                            {
                                banco = "Santander";
                            }
                            if (id == 6)
                            {
                                banco = "Caixa Econômica";
                            }
                            if (id == 1)
                            {
                                banco = "Banco do Brasil";
                            }

                            */

                        }




                        if (string.IsNullOrEmpty(input.CorretorDadosBancario.ContaCorrente))
                            validationErrors.Add(new ValidationResult("Informe o número da Conta Corrente!"));

                        if (string.IsNullOrEmpty(input.CorretorDadosBancario.NomeFavorecido))
                            validationErrors.Add(new ValidationResult("Informe o nome do favorecido!"));

                        if (string.IsNullOrEmpty(input.CorretorDadosBancario.TipoConta))
                            validationErrors.Add(new ValidationResult("Selecione qual é o tipo da conta!"));
                    }
                }
                else
                {
                    // VALIDAÇÃO AGENCIA

                    if (input.CorretorDadosBancario.BancoId == 55 || input.CorretorDadosBancario.BancoId == 45 || input.CorretorDadosBancario.BancoId == 6)
                    {
                        bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^[0-9]{4}$");

                        if (ehValido == false)
                        {
                            validationErrors.Add(new ValidationResult("Código da agencia Incorreto ! Insira no Formato (9999) "));
                        }
                    }
                    if (input.CorretorDadosBancario.BancoId == 48 || input.CorretorDadosBancario.BancoId == 1)
                    {
                        bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.CodigoAgencia, @"^\d{4}-\d{1}$");

                        if (ehValido == false)
                        {
                            validationErrors.Add(new ValidationResult("Código da agencia Incorreto ! Insira no Formato (9999-D) "));
                        }
                    }



                    // VALIDAÇÃO CONTA CORRENTE
                    if (input.CorretorDadosBancario.BancoId == 1 || input.CorretorDadosBancario.BancoId == 45)
                    {
                        bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.ContaCorrente, @"^\d{8}-\d{1}$");

                        if (ehValido == false)
                        {
                            validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (99999999-D) "));
                        }
                    }
                    
                    if (input.CorretorDadosBancario.BancoId == 6)
                    {
                        // XXX99999999 - D(X: Operação)

                        bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.ContaCorrente, @"^\d{11}-\d{1}$");

                        if (ehValido == false)
                        {
                            validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (XXX99999999-D) (X: Operação) "));
                        }
                    }

                    if (input.CorretorDadosBancario.BancoId == 48)
                    {
                        //9999999-D
                        bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.ContaCorrente, @"^\d{7}-\d{1}$");

                        if (ehValido == false)
                        {
                            validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (9999999-D) "));
                        }
                    }

                    if (input.CorretorDadosBancario.BancoId == 55)
                    {
                        //99999-D
                        bool ehValido = Regex.IsMatch(input.CorretorDadosBancario.ContaCorrente, @"^\d{5}-\d{1}$");

                        if (ehValido == false)
                        {
                            validationErrors.Add(new ValidationResult("Código da Conta Corrente Incorreto ! Insira no Formato (99999-D) "));
                        }
                    }
                }




            if (input.CorretorPessoaJuridica != null)
                if (!(!string.IsNullOrEmpty(input.CorretorPessoaJuridica.Cnpj) && !string.IsNullOrEmpty(input.CorretorPessoaJuridica.RazaoSocial) && !string.IsNullOrEmpty(input.CorretorPessoaJuridica.NomeFantasia)))
                {

                    if (string.IsNullOrEmpty(input.CorretorPessoaJuridica.Cnpj) && string.IsNullOrEmpty(input.CorretorPessoaJuridica.RazaoSocial) && string.IsNullOrEmpty(input.CorretorPessoaJuridica.NomeFantasia))
                    {
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(input.CorretorPessoaJuridica.Cnpj))
                            validationErrors.Add(new ValidationResult("Informe o número do CNPJ!"));

                        if (string.IsNullOrEmpty(input.CorretorPessoaJuridica.RazaoSocial))
                            validationErrors.Add(new ValidationResult("Informe a razão social!"));

                        if (string.IsNullOrEmpty(input.CorretorPessoaJuridica.NomeFantasia))
                            validationErrors.Add(new ValidationResult("Informe o nome fantasia!"));
                    }

                }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private void ValidateInputUpdate(PerfilInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyNomeError")));

            if (string.IsNullOrEmpty(input.Cpf))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyCpfError")));

            if (!input.DataDeNascimento.HasValue)
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyDataDeNascimentoError")));

            if (input.EmailPrincipal == null ||
                string.IsNullOrEmpty(input.EmailPrincipal.Endereco))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyEmailError")));

            if (string.IsNullOrEmpty(input.Celular))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyCelularError")));


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private void ValidateInputCreate(PerfilInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyNomeError")));

            //if (string.IsNullOrEmpty(input.Cpf))
            //    validationErrors.Add(new ValidationResult(L("Perfil.EmptyCpfError")));

            //if (!input.DataDeNascimento.HasValue)
            //    validationErrors.Add(new ValidationResult(L("Perfil.EmptyDataDeNascimentoError")));

            if (input.EmailPrincipal == null ||
                string.IsNullOrEmpty(input.EmailPrincipal.Endereco))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyEmailError")));

            if (string.IsNullOrEmpty(input.Celular))
                validationErrors.Add(new ValidationResult(L("Perfil.EmptyCelularError")));


            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(PessoaFisica perfil, PerfilInput perfilInput)
        {
            var validationErrors = new List<ValidationResult>();

            var empresa = await GetEmpresa();

            if (perfil.Id > 0 && perfilInput != null)
            {
                //Alteração de CPF
                if (!string.IsNullOrEmpty(perfilInput.Cpf))
                {
                    if (!tipoDeDocumentoService.IsCpf(perfilInput.Cpf))
                    {
                        throw new UserFriendlyException(L("TipoDeDocumento.MessagemDeErroValidacaoCpf"));
                    }

                    var cpf = perfil.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf);
                    if (cpf != null)
                        cpf.Numero = perfilInput.Cpf;
                }

                //Alteração de RG
                if (!string.IsNullOrEmpty(perfilInput.Rg))
                {
                    var rg = perfil.Documentos.FirstOrDefault(x => x.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Rg);
                    if (rg != null)
                    {
                        rg.OrgaoExpedidor = perfilInput.OrgaoExpedidor;
                        rg.Numero = perfilInput.Rg;
                    }
                    else
                    {
                        TipoDeDocumento tipo = tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Rg);
                        var documento = new Documento();
                        documento.Numero = perfilInput.Rg;
                        documento.OrgaoExpedidor = perfilInput.OrgaoExpedidor;
                        documento.AssociarTipoDocumento(tipo, documentoService);
                        documento.Empresa = empresa;
                        documento.EmpresaId = empresa.Id;
                        documento.TenantId = empresa.TenantId;
                        perfil.Documentos.Add(documento);
                    }
                }

                //Alteração de E-mail
                if (!string.IsNullOrEmpty(perfilInput.EmailPrincipal.Endereco))
                {
                    var email = perfil.EnderecosEletronicos.FirstOrDefault(x => x.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal);
                    if (email != null)
                        email.Endereco = perfilInput.EmailPrincipal.Endereco;
                }

                //Alteração de Celular
                var celularInput = perfilInput.Celular;
                if (!string.IsNullOrEmpty(celularInput))
                {
                    celularInput = celularInput.Replace("(", string.Empty).Replace(")", string.Empty).Replace("-", string.Empty).Replace(" ", string.Empty);
                    var celular = perfil.Telefones.FirstOrDefault(x => x.Tipo == TipoTelefoneEnum.Celular);
                    if (celular != null)
                    {
                        celular.DDD = celularInput.Substring(0, 2);
                        celular.Numero = celularInput.Substring(2);
                    }
                    else
                    {
                        celular = new Telefone();
                        celular.Tipo = TipoTelefoneEnum.Celular;
                        telefoneService.PreencherNumeroCelularBrComMascaraByString(celular, perfilInput.Celular);
                        celular.Empresa = empresa;
                        celular.EmpresaId = empresa.Id;
                        celular.TenantId = empresa.TenantId;
                        perfil.Telefones.Add(celular);
                    }

                }

                //Alteração de Endereço
                if (perfilInput.EnderecoPrincipal != null)
                {
                    if (perfil.EnderecoPrincipal == null)
                    {
                        perfil.EnderecoPrincipal = new Endereco();
                        perfil.EnderecoPrincipal.EmpresaId = empresa.Id;
                        perfil.EnderecoPrincipal.TenantId = empresa.TenantId;
                    }

                    perfil.EnderecoPrincipal.PessoaId = perfil.Id;
                    perfil.EnderecoPrincipal.CEP = perfilInput.EnderecoPrincipal.CEP;
                    perfil.EnderecoPrincipal.TipoDeLogradouroId = perfilInput.EnderecoPrincipal.TipoDeLogradouroId;
                    perfil.EnderecoPrincipal.Logradouro = perfilInput.EnderecoPrincipal.Logradouro;
                    perfil.EnderecoPrincipal.Numero = perfilInput.EnderecoPrincipal.Numero;
                    perfil.EnderecoPrincipal.Complemento = perfilInput.EnderecoPrincipal.Complemento;
                    perfil.EnderecoPrincipal.Bairro = perfilInput.EnderecoPrincipal.Bairro;
                    perfil.EnderecoPrincipal.CidadeId = perfilInput.EnderecoPrincipal.CidadeId;
                }

                #region Atualiza o perfil de usuário
                var userDto = await _profileAppService.GetCurrentUserProfileForEdit();
                if (perfil?.EmailPrincipal?.Endereco != string.Empty)
                {
                    userDto.EmailAddress = perfil.EmailPrincipal.Endereco;
                }

                if (perfil?.Celular != string.Empty)
                {
                    userDto.PhoneNumber = perfil.Celular;
                }

                if (perfil?.Nome != string.Empty)
                {
                    var nomes = perfil.Nome.Split(' ');
                    userDto.Name = nomes.FirstOrDefault();

                    var sobrenome = nomes.LastOrDefault();
                    if (!string.IsNullOrWhiteSpace(sobrenome))
                    {
                        userDto.Surname = sobrenome;
                    }
                }

                if (perfil?.Celular != string.Empty)
                {
                    userDto.PhoneNumber = perfil.Celular;
                }

                await _profileAppService.UpdateCurrentUserProfile(userDto);
                #endregion
            }
            else if (perfilInput != null)
            {
                perfil.Documentos = new List<Documento>();
                perfil.TenantId = empresa.TenantId;
                if (!string.IsNullOrEmpty(perfilInput.Cpf))
                {
                    if (!tipoDeDocumentoService.IsCpf(perfilInput.Cpf))
                    {
                        throw new UserFriendlyException(L("TipoDeDocumento.MessagemDeErroValidacaoCpf"));
                    }

                    TipoDeDocumento tipo = tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Cpf);
                    var documento = new Documento();
                    documento.Numero = perfilInput.Cpf;
                    documento.AssociarTipoDocumento(tipo, documentoService);
                    documento.Empresa = empresa;
                    documento.EmpresaId = empresa.Id;
                    documento.TenantId = empresa.TenantId;
                    perfil.Documentos.Add(documento);
                }

                if (!string.IsNullOrEmpty(perfilInput.Rg))
                {
                    TipoDeDocumento tipo = tipoDeDocumentoService.GetByTipoFixo(TipoDeDocumentoEnum.Rg);
                    var documento = new Documento();
                    documento.Numero = perfilInput.Rg;
                    documento.OrgaoExpedidor = perfilInput.OrgaoExpedidor;
                    documento.AssociarTipoDocumento(tipo, documentoService);
                    documento.Empresa = empresa;
                    documento.EmpresaId = empresa.Id;
                    documento.TenantId = empresa.TenantId;
                    perfil.Documentos.Add(documento);
                }

                if (!string.IsNullOrEmpty(perfilInput.Celular))
                {
                    var telefone = new Telefone();
                    telefone.Tipo = TipoTelefoneEnum.Celular;
                    telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, perfilInput.Celular);
                    telefone.Empresa = empresa;
                    telefone.EmpresaId = empresa.Id;
                    telefone.TenantId = empresa.TenantId;

                    perfil.Telefones = new List<Telefone>();
                    perfil.Telefones.Add(telefone);
                }


                if (perfilInput.EmailPrincipal != null &&
                    !string.IsNullOrEmpty(perfilInput.EmailPrincipal.Endereco))
                {
                    var email = new EnderecoEletronico();
                    email.TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal;
                    email.Endereco = perfilInput.EmailPrincipal.Endereco;
                    email.Empresa = empresa;
                    email.EmpresaId = empresa.Id;
                    email.TenantId = empresa.TenantId;

                    perfil.EnderecosEletronicos = new List<EnderecoEletronico>();
                    perfil.EnderecosEletronicos.Add(email);
                }

                perfil.TenantId = empresa.TenantId;

                // Adicionado depois do process input, por que o entity framework não consegue adicionar a pessoaId dinamicamente nesse tipo de mapeamento
                perfil.EnderecoPrincipal = null;
                perfil.EnderecoPrincipalId = null;
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<IdInput> Save(PerfilInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                List<string> roles = new List<string>();
                if (Enum.IsDefined(typeof(TipoDeUsuarioEnum), input.TipoDeUsuario)
                    && input.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
                {
                    roles.Add(StaticRoleNames.Tenants.Corretor);
                }
                else if (Enum.IsDefined(typeof(TipoDeUsuarioEnum), input.TipoDeUsuario)
                    && input.TipoDeUsuario == TipoDeUsuarioEnum.Beneficiario)
                {
                    roles.Add(StaticRoleNames.Tenants.Beneficiario);
                }
                else
                {
                    throw new UserFriendlyException("O registro com o tipo de usuário informado ainda não está disponível.");
                }

                result = await base.CreateEntity<IdInput, PerfilInput>(perfilService, input, () => ValidateInputCreate(input), ProcessInput);
                if (result != null)
                {
                    var perfil = await Repository.FirstOrDefaultAsync(x => x.Id == result.Id);

                    if (perfil != null)
                    {
                        if (input.EnderecoPrincipal != null)
                        {
                            perfil.EnderecoPrincipal = new Endereco();
                            perfil.EnderecoPrincipal = input.EnderecoPrincipal.MapTo<Endereco>();
                            perfil.EnderecoPrincipal.TipoEndereco = TipoEnderecoEnum.Residencial;
                            perfil.EnderecoPrincipal.PessoaId = perfil.Id;
                            perfil.EnderecoPrincipal.Empresa = perfil.EmailPrincipal.Empresa; // É obrigatório existir o email principal
                            perfil.EnderecoPrincipal.EmpresaId = perfil.EmailPrincipal.Empresa.Id; // É obrigatório existir o email principal
                            perfil.EnderecoPrincipal.TenantId = perfil.EmailPrincipal.Empresa.TenantId; // É obrigatório existir o email principal
                        }

                        var user = await _userManager.CreatePorPessoaFisica(perfil, input.TipoDeUsuario, roles.ToArray());

                        var empresa = await GetEmpresa();
                        await empresaService.AssociaUsuarioEmpresa(user, empresa);
                    }
                }
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Perfil))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                var usuario = new User();
                var idUsuarioLogado = AbpSession.GetUserId();
                usuario = await UserManager.GetUserByIdAsync(idUsuarioLogado);
                var slug = usuario.Empresas.First().Slug;

                ValidateInputBankPersona(input);

                if (input.CorretorDadosBancario != null && input.CorretorDadosBancario.BancoId > 0)
                {
                    dynamic objCorretorDadosBancario = null;

                    if (input.CorretorDadosBancario.Id > 0)
                        objCorretorDadosBancario = await corretorDadosBancarioService.GetById(input.CorretorDadosBancario.Id);

                    if (objCorretorDadosBancario != null)
                    {
                        objCorretorDadosBancario.IsActive = input.CorretorDadosBancario.IsActive;
                        objCorretorDadosBancario.BancoId = input.CorretorDadosBancario.BancoId;
                        objCorretorDadosBancario.CodigoAgencia = input.CorretorDadosBancario.CodigoAgencia;
                        objCorretorDadosBancario.ContaCorrente = input.CorretorDadosBancario.ContaCorrente;
                        objCorretorDadosBancario.TipoConta = input.CorretorDadosBancario.TipoConta;
                        objCorretorDadosBancario.NomeAgencia = input.CorretorDadosBancario.NomeAgencia;
                        objCorretorDadosBancario.NomeFavorecido = input.CorretorDadosBancario.NomeFavorecido;


                    }
                    else
                    {
                        objCorretorDadosBancario = new Domain.EZLiv.SubtiposPessoa.CorretorDadosBancario();
                        objCorretorDadosBancario.IsActive = true;
                        objCorretorDadosBancario.BancoId = input.CorretorDadosBancario.BancoId;
                        
                        
                        objCorretorDadosBancario.PessoaId = usuario.PessoaId ?? 0;
                        objCorretorDadosBancario.CodigoAgencia = input.CorretorDadosBancario.CodigoAgencia;
                        objCorretorDadosBancario.ContaCorrente = input.CorretorDadosBancario.ContaCorrente;
                        objCorretorDadosBancario.TipoConta = input.CorretorDadosBancario.TipoConta;
                        objCorretorDadosBancario.NomeAgencia = input.CorretorDadosBancario.NomeAgencia;
                        objCorretorDadosBancario.NomeFavorecido = input.CorretorDadosBancario.NomeFavorecido;
                        objCorretorDadosBancario.CorretorId = input.CorretorConectadoId;

                        
                    }
                    var resultCorretorDadosBancario = await corretorDadosBancarioService.SaveCorretorDadosBancario(objCorretorDadosBancario);

                }

                if (input.CorretorPessoaJuridica != null)
                {
                    dynamic objCorretorPessoaJuridca = null;
                    if (input.CorretorPessoaJuridica.Id > 0)
                        objCorretorPessoaJuridca = await corretorPessoaJuridcaService.GetById(input.CorretorPessoaJuridica.Id);

                    if (objCorretorPessoaJuridca != null)
                    {
                        objCorretorPessoaJuridca.IsActive = input.CorretorPessoaJuridica.IsActive;
                        objCorretorPessoaJuridca.Cnpj = input.CorretorPessoaJuridica.Cnpj;
                        objCorretorPessoaJuridca.NomeFantasia = input.CorretorPessoaJuridica.NomeFantasia;
                        objCorretorPessoaJuridca.RazaoSocial = input.CorretorPessoaJuridica.RazaoSocial;
                        objCorretorPessoaJuridca.Observacao = input.CorretorPessoaJuridica.Observacao;
                    }
                    else
                    {
                        objCorretorPessoaJuridca = new Domain.EZLiv.SubtiposPessoa.CorretorPessoaJuridca();
                        objCorretorPessoaJuridca.IsActive = true;
                        objCorretorPessoaJuridca.PessoaId = usuario.PessoaId ?? 0;
                        objCorretorPessoaJuridca.Cnpj = input.CorretorPessoaJuridica.Cnpj;
                        objCorretorPessoaJuridca.NomeFantasia = input.CorretorPessoaJuridica.NomeFantasia;
                        objCorretorPessoaJuridca.RazaoSocial = input.CorretorPessoaJuridica.RazaoSocial;
                        objCorretorPessoaJuridca.Observacao = input.CorretorPessoaJuridica.Observacao;
                        objCorretorPessoaJuridca.CorretorId = input.CorretorConectadoId;
                    }
                    var resultCorretorPessoaJuridca = await corretorPessoaJuridcaService.SaveCorretorPessoaJuridca(objCorretorPessoaJuridca);















                }





                // CRIAÇÃO DE SUB CONTA

                if (usuario != null &&
                    input != null &&
                    input.CorretorDadosBancario != null &&
                    input.Cpf != null &&
                    input.CorretorPessoaJuridica != null &&
                    input.Nome != null &&
                    input.TelefoneCelular != null &&
                    input.EnderecoPrincipal != null &&
                    input.EnderecoPrincipal.Logradouro != null &&
                    input.EnderecoPrincipal.CEP != null &&
                    input.EnderecoPrincipal.Cidade != null &&
                    input.EnderecoPrincipal.Cidade.Nome != null &&
                    input.CorretorDadosBancario != null &&
                    input.CorretorDadosBancario.BancoId != 0 &&
                    input.CorretorDadosBancario.CodigoAgencia != null &&
                    input.CorretorDadosBancario.ContaCorrente != null
                    )
                {

                    ServicoHTTP _servicohttp = new ServicoHTTP();
                    SubContaInput inputSubConta = new SubContaInput();
                    inputSubConta.data = new data();

                    inputSubConta.data.business_type = "Saúde " + slug;
                    inputSubConta.data.person_type = input.CorretorDadosBancario.TipoConta == "001" ? "Pessoa Física" : "Pessoa Jurídica";
                    inputSubConta.data.cnpj = inputSubConta.data.person_type == "Pessoa Jurídica" ? input.CorretorPessoaJuridica.Cnpj : null;
                    inputSubConta.data.cpf = input.Cpf;
                    inputSubConta.data.company_name = inputSubConta.data.person_type == "Pessoa Jurídica" ? input.CorretorPessoaJuridica.RazaoSocial : "";
                    inputSubConta.data.name = input.Nome;
                    inputSubConta.data.address = input.EnderecoPrincipal == null ? "" : input.EnderecoPrincipal.Logradouro;
                    inputSubConta.data.cep = input.EnderecoPrincipal == null ? "00000000" : input.EnderecoPrincipal.CEP;
                    inputSubConta.data.city = input.EnderecoPrincipal == null ? "" : input.EnderecoPrincipal.Cidade.Nome;
                    inputSubConta.data.state = input.EnderecoPrincipal == null ? "" : "N/A";
                    inputSubConta.data.telephone = input.TelefoneCelular == null ? "" : input.TelefoneCelular.DDD + input.TelefoneCelular.Numero;


                    inputSubConta.data.bank = input.CorretorDadosBancario == null ? "" : BuscarBanco(input.CorretorDadosBancario.BancoId);
                    inputSubConta.data.bank_ag = input.CorretorDadosBancario == null ? "" : input.CorretorDadosBancario.CodigoAgencia;
                    inputSubConta.data.account_type = "Corrente";
                    inputSubConta.data.bank_cc = input.CorretorDadosBancario == null ? "" : input.CorretorDadosBancario.ContaCorrente;

                    var json = JsonConvert.SerializeObject(inputSubConta);


                    _servicohttp.PostHeader(ConfigurationManager.AppSettings["UrlApiIUGUSubConta"].ToString(), json);
                }



                result = await base.UpdateEntity<IdInput, PerfilInput>(perfilService, input, () => ValidateInputUpdate(input), ProcessInput);

            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Perfil)]
        public async Task<PerfilInput> Get()
        {
            var idUsuarioLogado = AbpSession.GetUserId();
            var usuario = await UserManager.GetUserByIdAsync(idUsuarioLogado);

            if (usuario.PessoaId.HasValue)
            {
                var perfil = await Repository.FirstOrDefaultAsync(x => x.Id == usuario.PessoaId.Value);
                if (perfil != null)
                {
                    var perfilInput = perfil.MapTo<PerfilInput>();

                    if (usuario.TipoDeUsuario == TipoDeUsuarioEnum.Corretor)
                    {
                        perfilInput.IsCorretor = true;

                        var corretorPessoaJuridica = await corretorPessoaJuridcaService.GetCorretorPessoaJuridicaByPessoaId(usuario.PessoaId ?? 0);
                        var corretorDadosBancaria = await corretorDadosBancarioService.GetCorretorDadosBancarioByPessoaId(usuario.PessoaId ?? 0);
                        var bancos = await bancoService.GetAllListAsync(x => x.IsActive == true);
                        var corretor = await corretorService.GetCorretorByPessoaId(usuario.PessoaId);

                        if (corretorPessoaJuridica != null)
                        {
                            perfilInput.CorretorPessoaJuridica = new Dto.EZLiv.SubtipoPessoa.Corretor.CorretorPessoaJuridcaInput();
                            perfilInput.CorretorPessoaJuridica.IsActive = corretorPessoaJuridica.IsActive;
                            perfilInput.CorretorPessoaJuridica.Cnpj = corretorPessoaJuridica.Cnpj;
                            perfilInput.CorretorPessoaJuridica.CorretorId = corretorPessoaJuridica.CorretorId;
                            perfilInput.CorretorPessoaJuridica.Id = corretorPessoaJuridica.Id;
                            perfilInput.CorretorPessoaJuridica.NomeFantasia = corretorPessoaJuridica.NomeFantasia;
                            perfilInput.CorretorPessoaJuridica.RazaoSocial = corretorPessoaJuridica.RazaoSocial;
                            perfilInput.CorretorPessoaJuridica.Observacao = corretorPessoaJuridica.Observacao;
                        }

                        if (corretorDadosBancaria != null)
                        {
                            perfilInput.CorretorDadosBancario = new Dto.EZLiv.SubtipoPessoa.Corretor.CorretorDadosBancarioInput();
                            perfilInput.CorretorDadosBancario.IsActive = corretorDadosBancaria.IsActive;
                            perfilInput.CorretorDadosBancario.Id = corretorDadosBancaria.Id;
                            perfilInput.CorretorDadosBancario.TipoConta = corretorDadosBancaria.TipoConta;
                            perfilInput.CorretorDadosBancario.CorretorId = corretorDadosBancaria.CorretorId;
                            perfilInput.CorretorDadosBancario.BancoId = corretorDadosBancaria.BancoId;
                            perfilInput.CorretorDadosBancario.CodigoAgencia = corretorDadosBancaria.CodigoAgencia;
                            perfilInput.CorretorDadosBancario.ContaCorrente = corretorDadosBancaria.ContaCorrente;
                            perfilInput.CorretorDadosBancario.DigitoVerificador = corretorDadosBancaria.DigitoVerificador;
                            perfilInput.CorretorDadosBancario.NomeAgencia = corretorDadosBancaria.NomeAgencia;
                            perfilInput.CorretorDadosBancario.NomeFavorecido = corretorDadosBancaria.NomeFavorecido;
                        }

                        if (bancos != null)
                        {
                            perfilInput.Bancos = new List<Dto.Global.Geral.Banco.BancoInput>();

                            perfilInput.Bancos.Add(new Dto.Global.Geral.Banco.BancoInput() { Nome = "Selecione...", Codigo = "0", IsActive = true });

                            foreach (var item in bancos)
                            {
                                var obj = new Dto.Global.Geral.Banco.BancoInput();
                                obj.Codigo = item.Codigo;
                                obj.Nome = item.Nome;
                                obj.Id = item.Id;
                                obj.IsActive = item.IsActive;

                                perfilInput.Bancos.Add(obj);
                            }
                        }

                        if (corretor != null)
                        {
                            perfilInput.CorretorConectadoId = corretor.Id;
                        }







                    }
                    else
                    {
                        perfilInput.CorretorDadosBancario = null;
                        perfilInput.CorretorPessoaJuridica = null;
                    }

                    return perfilInput;
                }
            }

            return null;
        }

        private async Task<Empresa> GetEmpresa()
        {
            return await empresaService.GetEmpresaDefault();
        }


        private string BuscarBanco(int id)
        {
            string banco = "";

            if (id == 55)
            {
                banco = "Itaú";              
            }

            if (id == 48)
            {
                banco = "Bradesco";
            }
            if (id == 45)
            {
                banco = "Santander";
            }
            if (id == 6)
            {
                banco = "Caixa Econômica";
            }
            if (id == 1)
            {
                banco = "Banco do Brasil";
            }

            return banco;

        }



    }
}
