﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface IEnderecoAppService : IApplicationService
    {
        Task<IdInput> Save(EnderecoInput input);

        Task<EnderecoInput> SaveAndReturnEntity(EnderecoInput input);

        Task<EnderecoInput> GetById(IdInput input);

        Task<EnderecoInput> GetByExternalId(IdInput input);

        Task<EnderecoInput> GetEnderecoByCep(GetEnderecoByCepInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<EnderecoListDto>> GetEnderecosPaginado(GetEnderecoInput input);
    }
}