﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface IEstadoAppService : IApplicationService
    {
        Task<IdInput> Save(EstadoInput input);

        Task<EstadoInput> SaveAndReturnEntity(EstadoInput input);

        Task<PagedResultDto<EstadoListDto>> GetEstadosPaginado(GetEstadoInput input);

        Task<EstadoInput> GetById(IdInput input);

        Task<EstadoInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<EstadoListDto> GetEstadoBySigla(EstadoInput input);

        Task<PagedResultDto<EstadoListDto>> GetEstadosByPaisId(GetEstadoByPaisInput input);

        Task<EstadoListDto> GetEstadoByCidadeId(IdInput input);
    }
}