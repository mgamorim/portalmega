﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface ICidadeAppService : IApplicationService
    {
        Task<IdInput> Save(CidadeInput input);

        Task<CidadeInput> SaveAndReturnEntity(CidadeInput input);

        Task<ListResultDto<CidadeListDto>> GetCidadesList(GetCidadeInput input);

        Task<PagedResultDto<CidadeListDto>> GetCidadesPaginado(GetCidadeInput input);

        Task<CidadeInput> GetById(IdInput input);

        Task<CidadeInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<CidadeListDto>> GetCidadesByEstadoId(GetCidadeByEstadoInput input);
    }
}