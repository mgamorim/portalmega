﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface ITipoDeLogradouroAppService : IApplicationService
    {
        Task<IdInput> Save(TipoDeLogradouroInput input);

        Task<TipoDeLogradouroInput> SaveAndReturnEntity(TipoDeLogradouroInput input);

        Task<ListResultDto<TipoDeLogradouroListDto>> GetTiposDeLogradouroList(GetTipoDeLogradouroInput input);

        Task<PagedResultDto<TipoDeLogradouroListDto>> GetTiposDeLogradouroPaginado(GetTipoDeLogradouroInput input);

        Task<TipoDeLogradouroInput> GetById(IdInput input);

        Task<TipoDeLogradouroInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);
    }
}