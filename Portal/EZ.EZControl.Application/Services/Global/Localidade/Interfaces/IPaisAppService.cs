﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Localidade.Pais;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade.Interfaces
{
    public interface IPaisAppService : IApplicationService
    {
        Task<IdInput> Save(PaisInput input);

        Task<PaisInput> SaveAndReturnEntity(PaisInput input);

        Task<PagedResultDto<PaisListDto>> GetPaisesPaginado(GetPaisInput input);

        Task<PaisInput> GetById(IdInput input);

        Task<PaisInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PaisListDto> GetPaisByCodigoISO(PaisInput input);

        Task<PaisListDto> GetPaisByEstadoId(IdInput input);

        Task<PaisListDto> GetPaisByCidadeId(IdInput input);
    }
}