﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_Estado,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class EstadoAppService : EZControlAppServiceBase<Estado>, IEstadoAppService
    {
        private readonly IPaisService _paisService;
        private readonly IEstadoService _estadoService;
        private readonly ICidadeService _cidadeService;

        public EstadoAppService(IRepository<Estado> estadoRepository,
            IPaisService paisService,
            IEstadoService estadoService,
            ICidadeService cidadeService)
            : base(estadoRepository)
        {
            _paisService = paisService;
            _estadoService = estadoService;
            _cidadeService = cidadeService;
        }

        private void ValidateInput(EstadoInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(String.Format(L("Estado.RequiredNomeError"), input.Nome),
                    new[] { "nome".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Sigla))
                validationErrors.Add(new ValidationResult(String.Format(L("Estado.RequiredSiglaError"), input.Sigla),
                    new[] { "sigla".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Capital))
                validationErrors.Add(new ValidationResult(String.Format(L("Estado.RequiredCapitalError"), input.Capital),
                    new[] { "capital".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Estado.DuplicateNomeError"), input.Nome),
                    new[] { "nome".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Sigla == input.Sigla))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Estado.DuplicateSiglaError"), input.Sigla),
                    new[] { "sigla".ToCamelCase() }));
            }

            if (!string.IsNullOrEmpty(input.CodigoIBGE) &&
                CheckForDuplicateInstance(Repository, input.Id, x => x.CodigoIBGE == input.CodigoIBGE))
            {
                validationErrors.Add(
                    new ValidationResult(String.Format(L("Estado.DuplicateCodigoIBGEError"), input.CodigoIBGE),
                        new[] { "CodigoIBGE".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task BeforeSaveEntity(Estado estado, EstadoInput input)
        {
            if (input.PaisId > 0)
            {
                Pais pais = await _paisService.GetAll().Where(x => x.Id == input.PaisId).FirstOrDefaultAsync();
                estado.Pais = pais;
            }
            else
            {
                throw new AbpValidationException(L("Estado.EmptyPaisError"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Estado_Create, AppPermissions.Pages_Tenant_Global_Estado_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Edit)]
        public async Task<IdInput> Save(EstadoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Estado_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Create))
                    result = await base.CreateEntity<IdInput, EstadoInput>(_estadoService, input, () => ValidateInput(input), BeforeSaveEntity);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }

            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Estado_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Edit))
                    result = await base.UpdateEntity<IdInput, EstadoInput>(_estadoService, input, () => ValidateInput(input), BeforeSaveEntity);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }

            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Estado_Create, AppPermissions.Pages_Tenant_Global_Estado_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Edit)]
        public async Task<EstadoInput> SaveAndReturnEntity(EstadoInput input)
        {
            EstadoInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Estado_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Create))
                    result = await base.CreateAndReturnEntity<EstadoInput, EstadoInput>(_estadoService, input, () => ValidateInput(input));
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Estado_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Edit))
                    result = await base.UpdateAndReturnEntity<EstadoInput, EstadoInput>(_estadoService, input, () => ValidateInput(input));
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        public async Task<PagedResultDto<EstadoListDto>> GetEstadosPaginado(GetEstadoInput input)
        {
            var condicoes = new List<WhereIfCondition<Estado>>
            {
                 new WhereIfCondition<Estado>(
                    !string.IsNullOrEmpty(input.NomeEstado) ||
                    !string.IsNullOrEmpty(input.SiglaEstado) ||
                    !string.IsNullOrEmpty(input.Capital) ||
                    !string.IsNullOrEmpty(input.NomePais) ||
                    !string.IsNullOrEmpty(input.CodigoIBGE),
                    a =>
                        a.Nome.Contains(input.NomeEstado) ||
                        a.Sigla.Contains(input.SiglaEstado) ||
                        a.Capital.Contains(input.Capital) ||
                        (a.Pais != null && a.Pais.Nome.Contains(input.NomePais)) ||
                        a.CodigoIBGE.Contains(input.CodigoIBGE))
            };
            var result = await base.GetListPaged<EstadoListDto, GetEstadoInput>(_estadoService, input, condicoes);

            return result;
        }

        public async Task<EstadoInput> GetById(IdInput input)
        {
            var estado = await _estadoService.GetById(input.Id);
            return estado.MapTo<EstadoInput>();
            //return await base.GetEntityById<EstadoInput, IdInput>(_estadoService, input);
        }

        public async Task<EstadoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EstadoInput, IdInput>(_estadoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Estado_Delete,
                    AppPermissions.Pages_Tenant_Global_PessoaFisicaEstado_Delete,
                    AppPermissions.Pages_Tenant_Global_PessoaJuridicaEstado_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_estadoService, input);
        }

        public async Task<EstadoListDto> GetEstadoBySigla(EstadoInput input)
        {
            var result = await this.ApplyEmpresaFilter(async () =>
            {
                var estado = await Repository
                    .GetAll()
                    .Where(x => x.Sigla == input.Sigla)
                    .FirstOrDefaultAsync();

                return estado.MapTo<EstadoListDto>();
            });

            return result;
        }

        public async Task<PagedResultDto<EstadoListDto>> GetEstadosByPaisId(GetEstadoByPaisInput input)
        {
            var query = _estadoService
                .GetAll()
                .Where(x => x.Pais.Id == input.PaisId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.NomeEstado) || !string.IsNullOrWhiteSpace(input.SiglaEstado),
                    x => x.Nome.Contains(input.NomeEstado) || x.Sigla.Contains(input.SiglaEstado));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<EstadoListDto>>();

            return new PagedResultDto<EstadoListDto>(count, listDtos);
        }

        public async Task<EstadoListDto> GetEstadoByCidadeId(IdInput input)
        {
            var pais = await _cidadeService.GetAll()
                .Where(x => x.Id == input.Id)
                .Select(x => x.Estado)
                .FirstOrDefaultAsync();

            var result = pais.MapTo<EstadoListDto>();

            return result;
        }
    }
}