﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_Cidade,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class CidadeAppService : EZControlAppServiceBase<Cidade>, ICidadeAppService
    {
        private readonly IEstadoService _estadoService;
        private readonly ICidadeService _cidadeService;

        public CidadeAppService(
            IRepository<Cidade> cidadeRepository,
            IEstadoService estadoService,
            ICidadeService cidadeService)
            : base(cidadeRepository)
        {
            _estadoService = estadoService;
            _cidadeService = cidadeService;
        }

        private void ValidateInput(CidadeInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo
            var validationErrors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("Cidade.RequiredNomeError"), new[] { "nome".ToCamelCase() }));

            if (_cidadeService.CheckForDuplicateInstance(input.Id, x => x.Nome == input.Nome && x.EstadoId == input.EstadoId))
            {
                validationErrors.Add(new ValidationResult(L("Cidade.DuplicateNomeError"), new[] { "nome".ToCamelCase() }));
            }
            if (!string.IsNullOrEmpty(input.CodigoIBGE) && _cidadeService.CheckForDuplicateInstance(input.Id, x => x.CodigoIBGE == input.CodigoIBGE))
            {
                validationErrors.Add(new ValidationResult(L("Cidade.DuplicateCodigoIBGEError"), new[] { "CodigoIBGE".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Cidade cidade, CidadeInput input)
        {
            if (input.EstadoId > 0)
            {
                Estado estado = await _estadoService.GetAll().Where(x => x.Id == input.EstadoId).FirstOrDefaultAsync();
                cidade.Estado = estado;
            }
            else
            {
                throw new AbpValidationException(L("Cidade.EmptyEstadoError"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cidade_Create, AppPermissions.Pages_Tenant_Global_Cidade_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Edit
            )]
        public async Task<IdInput> Save(CidadeInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cidade_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Create))
                    result = await base.CreateEntity<IdInput, CidadeInput>(_cidadeService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cidade_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Edit))
                    result = await base.UpdateEntity<IdInput, CidadeInput>(_cidadeService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }


            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cidade_Create, AppPermissions.Pages_Tenant_Global_Cidade_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Edit)]
        public async Task<CidadeInput> SaveAndReturnEntity(CidadeInput input)
        {
            CidadeInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cidade_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Create))
                    result = await base.CreateAndReturnEntity<CidadeInput, CidadeInput>(_cidadeService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cidade_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Edit))
                    result = await base.UpdateAndReturnEntity<CidadeInput, CidadeInput>(_cidadeService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }


            }

            return result;
        }

        public async Task<ListResultDto<CidadeListDto>> GetCidadesList(GetCidadeInput input)
        {
            var condicoes = new List<WhereIfCondition<Cidade>>
            {
                new WhereIfCondition<Cidade>(
                    !string.IsNullOrEmpty(input.NomeCidade) ||
                    !string.IsNullOrEmpty(input.CodigoIBGE),
                        x =>
                    x.Nome.Contains(input.NomeCidade) ||
                    x.CodigoIBGE.Contains(input.CodigoIBGE) )
            };

            return await base.GetList<CidadeListDto>(_cidadeService, condicoes, cidade => cidade.Nome);
        }

        public async Task<PagedResultDto<CidadeListDto>> GetCidadesPaginado(GetCidadeInput input)
        {
            var condicoes = new List<WhereIfCondition<Cidade>>
            {
                new WhereIfCondition<Cidade>(
                    !string.IsNullOrEmpty(input.NomeCidade) ||
                    !string.IsNullOrEmpty(input.CodigoIBGE) ||
                    !string.IsNullOrEmpty(input.NomeEstado) ||
                    !string.IsNullOrEmpty(input.NomePais),
                        x =>
                    x.Nome.Contains(input.NomeCidade) ||
                    x.CodigoIBGE.Contains(input.CodigoIBGE) ||
                    (x.Estado != null && x.Estado.Nome.Contains(input.NomeEstado)) ||
                    (x.Estado != null && x.Estado.Pais != null && x.Estado.Pais.Nome.Contains(input.NomePais)))
            };

            var result = await base.GetListPaged<CidadeListDto, GetCidadeInput>(_cidadeService, input, condicoes);
            return result;
        }

        public async Task<CidadeInput> GetById(IdInput input)
        {
            return await base.GetEntityById<CidadeInput, IdInput>(_cidadeService, input);
        }

        public async Task<CidadeInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<CidadeInput, IdInput>(_cidadeService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cidade_Delete,
                    AppPermissions.Pages_Tenant_Global_PessoaFisicaCidade_Delete,
                    AppPermissions.Pages_Tenant_Global_PessoaJuridicaCidade_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_cidadeService, input);
        }

        public async Task<PagedResultDto<CidadeListDto>> GetCidadesByEstadoId(GetCidadeByEstadoInput input)
        {
            var query = _cidadeService
                .GetAll()
                .Where(x => x.Estado.Id == input.EstadoId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.NomeCidade), x => x.Nome.Contains(input.NomeCidade));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<CidadeListDto>>();

            return new PagedResultDto<CidadeListDto>(count, listDtos);
        }
    }
}