﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_Endereco,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class EnderecoAppService : EZControlAppServiceBase<Endereco>, IEnderecoAppService
    {
        private const string urlViaCep = "http://viacep.com.br/ws/{0}/json/";
        private readonly IEnderecoService _enderecoService;
        private readonly IPessoaService _pessoaService;
        private readonly IEstadoService _estadoService;
        private readonly ICidadeService _cidadeService;
        private readonly IContatoService _contatoService;
        private readonly ITipoDeLogradouroService _tipoDeLogradouroService;

        public EnderecoAppService(
            IRepository<Endereco, int> repository,
            IEnderecoService enderecoService,
            IPessoaService pessoaService,
            IEstadoService estadoService,
            ICidadeService cidadeService,
            IContatoService contatoService,
            ITipoDeLogradouroService tipoDeLogradouroService)
            : base(repository)
        {
            _enderecoService = enderecoService;
            _pessoaService = pessoaService;
            _estadoService = estadoService;
            _cidadeService = cidadeService;
            _contatoService = contatoService;
            _tipoDeLogradouroService = tipoDeLogradouroService;
        }

        private void ValidateInput(EnderecoInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.CEP))
                validationErrors.Add(new ValidationResult(String.Format(L("Endereco.RequiredCepError"), input.CEP), new[] { "cep".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Logradouro))
                validationErrors.Add(new ValidationResult(String.Format(L("Endereco.RequiredLogradouroError"), input.Logradouro), new[] { "logradouro".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Numero))
                validationErrors.Add(new ValidationResult(String.Format(L("Endereco.RequiredNumeroError"), input.Numero), new[] { "numero".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Logradouro == input.Logradouro && x.PessoaId == input.PessoaId && x.Numero == input.Numero && x.Complemento == input.Complemento && x.CEP == input.CEP))
                validationErrors.Add(new ValidationResult(String.Format(L("Endereco.DuplicateDescricaoError"), input.Descricao), new[] { "logradouro".ToCamelCase(), "numero".ToCamelCase(), "complemento".ToCamelCase() }));

            if (validationErrors.Any())
                throw new AbpValidationException(L("ValidationError"), validationErrors);
        }

        private async Task ProcessInput(Endereco endereco, EnderecoInput input)
        {
            // Obrigatorio
            // Pessoa
            if (input.PessoaId > 0)
            {
                Domain.Global.Pessoa.Pessoa pessoa = null;
                try
                {
                    pessoa = await _pessoaService.GetById(input.PessoaId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Pessoa.NotFoundError"));
                }
                try
                {
                    endereco.AssociarPessoa(pessoa, _enderecoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Endereco.EmptyPessoaError"));
            }

            // Contato
            if (input.ContatoId.HasValue && input.ContatoId > 0)
            {
                Contato contato = null;

                try
                {
                    contato = await _contatoService.GetById(input.ContatoId.Value);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Contato.NotFoundError"));
                }

                try
                {
                    endereco.AssociarContato(contato, _enderecoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }

            // Obrigatorio
            // Tipo de Logradouro
            if (input.TipoDeLogradouroId > 0)
            {
                TipoDeLogradouro tipoDeLogradouro = null;

                try
                {
                    tipoDeLogradouro = await _tipoDeLogradouroService.GetById(input.TipoDeLogradouroId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("TipoDeLogradouro.NotFoundError"));
                }

                try
                {
                    endereco.AssociarTipoDeLogradouro(tipoDeLogradouro, _enderecoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }

            }
            else
            {
                throw new UserFriendlyException(L("Endereco.EmptyTipoDeLogradouroError"));
            }

            // Opcional
            // Cidade
            if (input.CidadeId > 0)
            {
                Cidade cidade = null;

                try
                {
                    cidade = await _cidadeService.GetById(input.CidadeId);
                }
                catch (Exception)
                {
                    throw new UserFriendlyException(L("Cidade.NotFoundError"));
                }

                try
                {
                    endereco.AssociarCidade(cidade, _enderecoService);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
            else
            {
                endereco.AssociarCidade(null, _enderecoService);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Endereco_Create, AppPermissions.Pages_Tenant_Global_Endereco_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Edit)]
        public async Task<IdInput> Save(EnderecoInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Endereco_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Create))
                    result = await base.CreateEntity<IdInput, EnderecoInput>(_enderecoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }

            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Endereco_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Edit))
                    result = await base.UpdateEntity<IdInput, EnderecoInput>(_enderecoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Endereco_Create, AppPermissions.Pages_Tenant_Global_Endereco_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Edit)]
        public async Task<EnderecoInput> SaveAndReturnEntity(EnderecoInput input)
        {
            EnderecoInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Endereco_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Create))
                    result = await base.CreateAndReturnEntity<EnderecoInput, EnderecoInput>(_enderecoService, input, () => ValidateInput(input));
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Endereco_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Edit))
                    result = await base.UpdateAndReturnEntity<EnderecoInput, EnderecoInput>(_enderecoService, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        public async Task<EnderecoInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EnderecoInput, IdInput>(_enderecoService, input);
        }

        public async Task<EnderecoInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EnderecoInput, IdInput>(_enderecoService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Endereco_Delete,
            AppPermissions.Pages_Tenant_Global_PessoaFisicaEndereco_Delete,
            AppPermissions.Pages_Tenant_Global_PessoaJuridicaEndereco_Delete
            )]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_enderecoService, input);
        }

        public async Task<PagedResultDto<EnderecoListDto>> GetEnderecosPaginado(GetEnderecoInput input)
        {
            IList<WhereIfCondition<Endereco>> condicoes = new List<WhereIfCondition<Endereco>>();

            condicoes.Add(new WhereIfCondition<Endereco>(input.PessoaId > 0, x => x.Pessoa.Id == input.PessoaId));
            condicoes.Add(new WhereIfCondition<Endereco>(!input.Descricao.IsNullOrEmpty(), x => x.Descricao.Contains(input.Descricao)));

            var result = await base.GetListPaged<EnderecoListDto, GetEnderecoInput>(_enderecoService, input, condicoes);


            return result;
        }

        public async Task<EnderecoInput> GetEnderecoByCep(GetEnderecoByCepInput input)
        {
            if (input.Cep.Length != 8)
                throw new UserFriendlyException(string.Format(L("Endereco.CepFormatIncorrect"), input.Cep));

            using (WebClient wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                var url = string.Format(urlViaCep, input.Cep);
                var json = wc.DownloadString(url);
                var endereco = await Task.Factory.StartNew(() => JsonConvert.DeserializeObject<EnderecoInput>(json));

                if (!endereco.Erro)
                {
                    // throw new UserFriendlyException(string.Format(L("Endereco.CepNotFound"), input.Cep));

                    endereco.CEP = endereco.CEP.Replace("-", string.Empty).Trim();

                    var tipoDeLogradouroDesc = endereco.Logradouro.Split(' ').FirstOrDefault();

                    if (!string.IsNullOrEmpty(tipoDeLogradouroDesc))
                    {
                        endereco.Logradouro = endereco.Logradouro.Replace(tipoDeLogradouroDesc, string.Empty).Trim();
                        var tipoDeLogradouro =
                            await _tipoDeLogradouroService.FirstOrDefault(x => x.Descricao.ToLower() == tipoDeLogradouroDesc.ToLower());

                        if (tipoDeLogradouro != null)
                            endereco.TipoDeLogradouroId = tipoDeLogradouro.Id;
                    }

                    var estado =
                        await _estadoService.FirstOrDefault(x => x.Sigla.ToLower() == endereco.UF.ToLower());

                    if (estado != null)
                    {
                        endereco.EstadoId = estado.Id;
                        endereco.PaisId = estado.PaisId;

                        var cidade =
                            await _cidadeService.FirstOrDefault(x => x.EstadoId == estado.Id && x.Nome.ToLower() == endereco.Localidade.ToLower());

                        if (cidade != null)
                            endereco.CidadeId = cidade.Id;
                    }
                }
                else {
                    endereco.CEP =  input.Cep;

                }

                return endereco;
            }
        }
    }
}