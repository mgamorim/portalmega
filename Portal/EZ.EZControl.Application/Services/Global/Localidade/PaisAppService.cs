﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Localidade.Pais;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_Pais,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaPais,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class PaisAppService : EZControlAppServiceBase<Pais>, IPaisAppService
    {
        private readonly IPaisService _paisService;
        private readonly IEstadoService _estadoService;
        private readonly ICidadeService _cidadeService;

        public PaisAppService(IRepository<Pais> paisRepository, IPaisService paisService, IEstadoService estadoService, ICidadeService cidadeService)
            : base(paisRepository)
        {
            _paisService = paisService;
            _estadoService = estadoService;
            _cidadeService = cidadeService;
        }

        private void ValidateInput(PaisInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
                validationErrors.Add(new ValidationResult(L("Pais.RequiredNomeError"), new[] { "nome".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.Nacionalidade))
                validationErrors.Add(new ValidationResult(L("Pais.RequiredNacionalidadeError"), new[] { "nacionalidade".ToCamelCase() }));

            if (string.IsNullOrEmpty(input.CodigoISO))
                validationErrors.Add(new ValidationResult(L("Pais.RequiredCodigoISOError"), new[] { "codigoISO".ToCamelCase() }));

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(L("Pais.DuplicateNomeError"), new[] { "nome".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nacionalidade == input.Nacionalidade))
            {
                validationErrors.Add(new ValidationResult(L("Pais.DuplicateNacionalidadeError"), new[] { "nacionalidade".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.CodigoISO == input.CodigoISO))
            {
                validationErrors.Add(new ValidationResult(L("Pais.DuplicateCodigoIsoError"), new[] { "codigoIso".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Pais_Create, AppPermissions.Pages_Tenant_Global_Pais_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Create,  AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Edit)]
        public async Task<IdInput> Save(PaisInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Pais_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Create))
                    result = await base.CreateEntity<IdInput, PaisInput>(_paisService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Pais_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Edit))
                    result = await base.UpdateEntity<IdInput, PaisInput>(_paisService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Pais_Create, AppPermissions.Pages_Tenant_Global_Pais_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Edit)]
        public async Task<PaisInput> SaveAndReturnEntity(PaisInput input)
        {
            PaisInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Pais_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Create))
                    result = await base.CreateAndReturnEntity<PaisInput, PaisInput>(_paisService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Pais_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Edit))
                    result = await base.UpdateAndReturnEntity<PaisInput, PaisInput>(_paisService, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }
            return result;
        }

        public async Task<PagedResultDto<PaisListDto>> GetPaisesPaginado(GetPaisInput input)
        {
            var condicoes = new List<WhereIfCondition<Pais>>
            {
                new WhereIfCondition<Pais>(
                    !string.IsNullOrEmpty(input.NomePais) ||
                    !string.IsNullOrEmpty(input.Nacionalidade) ||
                    !string.IsNullOrEmpty(input.CodigoISO),
                        x =>
                    x.Nome.Contains(input.NomePais) ||
                    x.Nacionalidade.Contains(input.Nacionalidade) ||
                    x.CodigoISO.Contains(input.CodigoISO))
            };
            var result = await base.GetListPaged<PaisListDto, GetPaisInput>(_paisService, input, condicoes);

            return result;
        }

        public async Task<PaisInput> GetById(IdInput input)
        {
            return await base.GetEntityById<PaisInput, IdInput>(_paisService, input);
        }

        public async Task<PaisInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<PaisInput, IdInput>(_paisService, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Pais_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaPais_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaPais_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_paisService, input);
        }

        public async Task<PaisListDto> GetPaisByCodigoISO(PaisInput input)
        {
            var result = await this.ApplyEmpresaFilter(async () =>
            {
                var pais = await Repository
                    .GetAll()
                    .Where(x => x.CodigoISO == input.CodigoISO)
                    .FirstOrDefaultAsync();

                return pais.MapTo<PaisListDto>();
            });

            return result;
        }

        public async Task<PaisListDto> GetPaisByEstadoId(IdInput input)
        {
            var pais = await _estadoService.GetAll()
                .Where(x => x.Pais.Id == input.Id)
                .Select(x => x.Pais)
                .FirstOrDefaultAsync();

            var result = pais.MapTo<PaisListDto>();

            return result;
        }

        public async Task<PaisListDto> GetPaisByCidadeId(IdInput input)
        {
            var pais = await _cidadeService.GetAll()
                .Where(x => x.Id == input.Id)
                .Select(x => x.Estado.Pais)
                .FirstOrDefaultAsync();

            var result = pais.MapTo<PaisListDto>();

            return result;
        }
    }
}