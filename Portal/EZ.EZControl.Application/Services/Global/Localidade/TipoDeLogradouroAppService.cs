﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.Localidade
{
    [AbpAuthorize(
                  AppPermissions.Pages_Tenant_Global_TipoDeLogradouro,
                  AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro,
                  AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos,
                AppPermissions.Pages_Tenant_AcessoLeitura
        )]
    public class TipoDeLogradouroAppService : EZControlAppServiceBase<TipoDeLogradouro>, ITipoDeLogradouroAppService
    {
        private readonly ITipoDeLogradouroService _tipoDeLogradouroervice;

        public TipoDeLogradouroAppService(
            IRepository<TipoDeLogradouro> tipoDeLogradouroRepository,
            ITipoDeLogradouroService tipoDeLogradouroService)
            : base(tipoDeLogradouroRepository)
        {
            _tipoDeLogradouroervice = tipoDeLogradouroService;
        }

        private void ValidateInput(TipoDeLogradouroInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Descricao))
                validationErrors.Add(new ValidationResult(L("TipoDeLogradouro.RequiredDescricaoError"), new[] { "descricao".ToCamelCase() }));

            if (_tipoDeLogradouroervice.CheckForFieldDuplicated(input.Id, input.Descricao, x => x.Descricao == input.Descricao))
            {
                validationErrors.Add(new ValidationResult("Descrição existente.", new[] { "descricao".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Create, AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Edit)]
        public async Task<IdInput> Save(TipoDeLogradouroInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Create))
                    result = await base.CreateEntity<IdInput, TipoDeLogradouroInput>(_tipoDeLogradouroervice, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Edit))
                    result = await base.UpdateEntity<IdInput, TipoDeLogradouroInput>(_tipoDeLogradouroervice, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Create, AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Edit,
                     AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Edit,
                     AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Edit)
        ]
        public async Task<TipoDeLogradouroInput> SaveAndReturnEntity(TipoDeLogradouroInput input)
        {
            TipoDeLogradouroInput result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Create)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Create))
                    result = await base.CreateAndReturnEntity<TipoDeLogradouroInput, TipoDeLogradouroInput>(_tipoDeLogradouroervice, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Edit)||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Edit))
                    result = await base.UpdateAndReturnEntity<TipoDeLogradouroInput, TipoDeLogradouroInput>(_tipoDeLogradouroervice, input, () => ValidateInput(input));
                else
                { 
                throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        public async Task<ListResultDto<TipoDeLogradouroListDto>> GetTiposDeLogradouroList(GetTipoDeLogradouroInput input)
        {
            IList<WhereIfCondition<TipoDeLogradouro>> condicoes = new List<WhereIfCondition<TipoDeLogradouro>>();
            condicoes.Add(new WhereIfCondition<TipoDeLogradouro>(!input.Descricao.IsNullOrEmpty(), x => x.Descricao.Contains(input.Descricao)));
            return await base.GetList<TipoDeLogradouroListDto>(_tipoDeLogradouroervice, condicoes, x => x.Descricao);
        }

        public async Task<PagedResultDto<TipoDeLogradouroListDto>> GetTiposDeLogradouroPaginado(GetTipoDeLogradouroInput input)
        {
            var condicoes = new List<WhereIfCondition<TipoDeLogradouro>>
            {
                new WhereIfCondition<TipoDeLogradouro>(!string.IsNullOrEmpty(input.Descricao),
                    x => x.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<TipoDeLogradouroListDto, GetTipoDeLogradouroInput>(_tipoDeLogradouroervice, input, condicoes);

            return result;
        }

        public async Task<TipoDeLogradouroInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TipoDeLogradouroInput, IdInput>(_tipoDeLogradouroervice, input);
        }

        public async Task<TipoDeLogradouroInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TipoDeLogradouroInput, IdInput>(_tipoDeLogradouroervice, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_TipoDeLogradouro_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaTipoDeLogradouro_Delete,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaTipoDeLogradouro_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_tipoDeLogradouroervice, input);
        }
    }
}