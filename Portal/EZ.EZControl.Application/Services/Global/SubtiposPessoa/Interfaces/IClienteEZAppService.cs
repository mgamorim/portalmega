﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces
{
    public interface IClienteEZAppService : IApplicationService
    {
        Task<ClienteEZPessoaIdDto> Save(ClienteEZDto input);

        Task<ClienteEZDto> SaveAndReturnEntity(ClienteEZDto input);

        Task<ClienteEZDto> Get();

        Task Delete(IdInput input);
    }
}