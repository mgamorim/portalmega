﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces
{
    public interface IClienteAppService : IApplicationService
    {
        Task<ClientePessoaIdDto> Save(ClienteInput input);

        Task<ClienteInput> SaveAndReturnEntity(ClienteInput input);

        Task<ClienteInput> GetById(IdInput input);

        Task<ClienteInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<ClienteListDto>> GetClientesPaginado(GetClienteInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForCliente(GetPessoaExceptForCliente input);
    }
}