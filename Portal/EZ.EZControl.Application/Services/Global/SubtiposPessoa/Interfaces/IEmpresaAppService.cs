﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces
{
    public interface IEmpresaAppService : IApplicationService
    {
        Task<EmpresaPessoaIdDto> Save(EmpresaInput input);

        Task<EmpresaListDto> SaveAndReturnEntity(EmpresaInput input);

        Task<EmpresaInput> GetById(IdInput input);

        Task<EmpresaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<ListResultDto<EmpresaListDto>> GetEmpresas(GetEmpresaInput input);

        Task<PagedResultDto<EmpresaListDto>> GetEmpresasPaginado(GetEmpresaInput input);

        Task<ListResultDto<EmpresaNomeListDto>> GetEmpresasPorUsuarioLogado();

        Task<EmpresaNomeListDto> GetEmpresaLogada();

        Task<EmpresaNomeListDto> EscolherEmpresa(int empresaId);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForEmpresa(GetPessoaExceptForEmpresa input);
        Task<PagedResultDto<UserListDto>> GetUsuariosNaoAssociados(IdInput input);
        Task<PagedResultDto<UserListDto>> GetUsuariosAssociados(IdInput input);
        Task AssociaUsuarioEmpresa(AssociacaoUsuarioEmpresaInput input);
        Task DesassociaUsuarioEmpresa(AssociacaoUsuarioEmpresaInput input);

        Task<Empresa> GetEmpresaBySlug(GetEmpresaInput input);
    }
}