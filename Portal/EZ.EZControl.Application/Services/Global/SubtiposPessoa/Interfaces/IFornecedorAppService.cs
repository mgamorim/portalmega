﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces
{
    public interface IFornecedorAppService : IApplicationService
    {
        Task<FornecedorPessoaIdDto> Save(FornecedorInput input);

        Task<FornecedorInput> SaveAndReturnEntity(FornecedorInput input);

        Task<FornecedorInput> GetById(IdInput input);

        Task<FornecedorInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<FornecedorListDto>> GetFornecedoresPaginado(GetFornecedorInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForFornecedor(GetPessoaExceptForFornecedor input);
    }
}