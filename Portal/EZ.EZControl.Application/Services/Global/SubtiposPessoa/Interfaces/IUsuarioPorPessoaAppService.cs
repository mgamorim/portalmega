﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces
{
    public interface IUsuarioPorPessoaAppService : IApplicationService
    {
        Task<UsuarioPorPessoaIdDto> Save(UsuarioPorPessoaInput input);

        Task<UsuarioPorPessoaInput> SaveAndReturnEntity(UsuarioPorPessoaInput input);

        Task<UsuarioPorPessoaInput> GetById(IdInput input);

        Task<UsuarioPorPessoaInput> GetByExternalId(IdInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<UsuarioPorPessoaListDto>> GetUsuariosPorPessoaPaginado(GetUsuarioPorPessoaInput input);

        Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForUsuario(GetPessoaExceptForUsuario input);
    }
}