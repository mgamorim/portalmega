﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Session;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.MultiEmpresa;
using EZ.EZControl.Services.EZLiv.MapToEntity;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa
{
    [AbpAuthorize]
    public class EmpresaAppService : EZControlAppServiceBase<Empresa>, IEmpresaAppService
    {
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IPessoaJuridicaAppService _pessoaJuridicaAppService;
        private readonly IEmpresaService _service;
        private readonly IClienteEZService _clienteEzService;
        private readonly IUserAppService _userAppService;
        private readonly IMultiEmpresa _multiEmpresa;
        public EmpresaAppService(
            IRepository<Empresa, int> repository,
            IEmpresaService service,
            IPessoaService pessoaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IPessoaJuridicaAppService pessoaJuridicaAppService,
            IClienteEZService clienteEzService,
            IUserAppService userAppService,
            IMultiEmpresa multiEmpresa)
            : base(repository)
        {
            _service = service;
            _pessoaService = pessoaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _pessoaJuridicaAppService = pessoaJuridicaAppService;
            _clienteEzService = clienteEzService;
            _userAppService = userAppService;
            _multiEmpresa = multiEmpresa;
        }

        private void ValidateInput(EmpresaInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (input.ClienteEzId == 0)
            {
                validationErrors.Add(new ValidationResult(L("Empresa.ClienteEzObrigatorioError")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        private async Task ProcessInput(Empresa empresa, EmpresaInput input)
        {
            try
            {
                if (input.ClienteEzId > 0)
                {
                    var clienteEz = await _clienteEzService.GetById(input.ClienteEzId);
                    empresa.ClienteEZ = clienteEz;
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            if (CheckForDuplicateInstance<Empresa, int>(Repository, input.Id, x => x.Slug == input.Slug))
                throw new UserFriendlyException(L("Empresa.DuplicateSlugError"));
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Empresa_Create, AppPermissions.Pages_Tenant_Global_Empresa_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Edit)]
        public async Task<EmpresaPessoaIdDto> Save(EmpresaInput input)
        {
            EmpresaListDto empresa = null;
            var repo = new ADO.Repositorio.RepositorioPortal(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Empresa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Create))
                {
                    empresa = await base.CreateAndReturnEntity<EmpresaListDto, EmpresaInput>(_service, input, () => ValidateInput(input), ProcessInput);


                    if (empresa != null && empresa.Id != 0)
                        await repo.GravarPermissoesEmpresaAsync(empresa.Id);
                }
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }


            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Empresa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Edit))
                    empresa = await base.UpdateAndReturnEntity<EmpresaListDto, EmpresaInput>(_service, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            EmpresaPessoaIdDto result = new EmpresaPessoaIdDto
            {
                EmpresaId = empresa.Id,
                PessoaId = empresa.Id
            };
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Empresa_Create, AppPermissions.Pages_Tenant_Global_Empresa_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Create, AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Edit,
                      AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Create, AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Edit)]
        public async Task<EmpresaListDto> SaveAndReturnEntity(EmpresaInput input)
        {
            EmpresaListDto result = null;

            if (input.Id == default(int))
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Empresa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Create) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Create))
                    result = await base.CreateAndReturnEntity<EmpresaListDto, EmpresaInput>(_service, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));
                }
            }
            else
            {
                if (PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Empresa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Edit) ||
                    PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Edit))
                    result = await base.UpdateAndReturnEntity<EmpresaListDto, EmpresaInput>(_service, input, () => ValidateInput(input), ProcessInput);
                else
                {
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));
                }
            }

            return result;
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<EmpresaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<EmpresaInput, IdInput>(_service, input);
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<EmpresaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<EmpresaInput, IdInput>(_service, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Empresa_Delete,
                     AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa_Delete,
                    AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa_Delete
            )]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_service, input);
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<ListResultDto<EmpresaListDto>> GetEmpresas(GetEmpresaInput input)
        {
            var condicoes = new List<WhereIfCondition<Empresa>>
            {
                new WhereIfCondition<Empresa>(input != null && !(string.IsNullOrEmpty(input.NomeFantasia)),
                    empresa => empresa.RazaoSocial.Contains(input.NomeFantasia) || empresa.NomeFantasia.Contains(input.NomeFantasia)),

                new WhereIfCondition<Empresa>(input != null && !(string.IsNullOrEmpty(input.Cnpj)),
                    empresa => empresa.Cnpj.Contains(input.Cnpj)),

                       new WhereIfCondition<Empresa>(input != null && !(string.IsNullOrEmpty(input.Slug)),
                    empresa => empresa.Slug.Contains(input.Slug))
            };

            var result = await base.GetList<EmpresaListDto>(_service, condicoes, empresa => empresa.RazaoSocial);

            return result;
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<PagedResultDto<EmpresaListDto>> GetEmpresasPaginado(GetEmpresaInput input)
        {
            try
            {
                var query = Repository.GetAll();

                if (input != null)
                {
                    if (!string.IsNullOrEmpty(input.NomeFantasia))
                        query = query.Where(x =>
                        x.NomeFantasia.Contains(input.NomeFantasia) ||
                        x.RazaoSocial.Contains(input.NomeFantasia));

                    if (!string.IsNullOrEmpty(input.Cnpj))
                        query = query.Where(x => x.Cnpj.Contains(input.Cnpj));
                }

                var count = await query.CountAsync();
                var dados = await query
                    .OrderBy("Id")
                    .PageBy(input)
                    .ToListAsync();

                List<EmpresaListDto> listDtos = new List<EmpresaListDto>();

                foreach (var item in dados)
                {
                    EmpresaListDto empresatListDto = item.MapTo<EmpresaListDto>();

                    listDtos.Add(empresatListDto);
                }

                return new PagedResultDto<EmpresaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<ListResultDto<EmpresaNomeListDto>> GetEmpresasPorUsuarioLogado()
        {
            ListResultDto<EmpresaNomeListDto> result;

            try
            {
                long userId = UserManager.AbpSession.GetUserId();
                IList<Empresa> empresas = await UserManager
                    .Users
                    .Where(x => x.Id == userId)
                    .SelectMany(x => x.Empresas.Where(y => y.IsActive == true))
                    .ToListAsync();

                var lista = empresas.MapTo<List<EmpresaNomeListDto>>();
                result = new ListResultDto<EmpresaNomeListDto>(lista);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            return result;
        }

        public async Task<EmpresaNomeListDto> GetEmpresaLogada()
        {
            EmpresaNomeListDto result = new EmpresaNomeListDto();

            ClaimsPrincipal principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            ClaimsIdentity identity = principal.Identity as ClaimsIdentity;
            var empresaClaim = identity.FindFirst("EmpresaId");

            if (empresaClaim != null)
            {
                var empresa = await _service.GetById(Convert.ToInt32(empresaClaim.Value));
                if (empresa == null)
                {
                    throw new UserFriendlyException("Empresa não encontrada.");
                }

                result = empresa.MapTo<EmpresaNomeListDto>();
            }

            return result;
        }

        [AbpAllowAnonymous]
        public async Task<EmpresaNomeListDto> EscolherEmpresa(int empresaId)
        {
            if (UserManager.AbpSession.UserId != null)
            {
                Empresa empresa;

                try
                {
                    empresa = await _service.GetById(empresaId);

                    if (!empresa.Usuarios.Any(x => x.Id == UserManager.AbpSession.UserId))
                    {
                        throw new UserFriendlyException("Usuário não pertence a essa empresa");
                    }
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }

                var claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;

                claimsPrincipal.AddOrUpdateClaim("EmpresaId", empresaId.ToString());

                EmpresaNomeListDto result = empresa.MapTo<EmpresaNomeListDto>();
                return result;
            }

            throw new UserFriendlyException("Usuário não encontrado.");
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForEmpresa(GetPessoaExceptForEmpresa input)
        {
            try
            {
                if (input == null)
                    throw new NotSupportedException("É necessário definir um critério para buscar por pessoas.");

                var queryPessoaJuridica = _pessoaJuridicaService
                    .GetAll()
                    .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                    .Select(x => new PessoaJuridicaListDto
                    {
                        Id = x.Id,
                        NomePessoa = x.NomeFantasia,
                        NomeFantasia = x.NomeFantasia,
                        RazaoSocial = x.RazaoSocial,
                        GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                        GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                        {
                            Id = x.GrupoPessoa.Id,
                            Descricao = x.GrupoPessoa.Descricao,
                            PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                        } : null,
                        TipoPessoa = TipoPessoaEnum.Juridica
                    });

                IQueryable<PessoaListDto> query = queryPessoaJuridica;

                var count = await query.CountAsync();

                List<PessoaListDto> dados = await query
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                var listDtos = dados.MapTo<List<PessoaListDto>>();
                return new PagedResultDto<PessoaListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<PagedResultDto<UserListDto>> GetUsuariosNaoAssociados(IdInput input)
        {
            try
            {
                var empresaLogadaId = input.Id;
                var usuariosNaoAssociados = UserManager
                    .Users
                    .Where(u => !u.Empresas.Any(x => x.Id == empresaLogadaId));

                var map = new MapManual().MapUserListDto(usuariosNaoAssociados);
                //var result = usuariosNaoAssociados.MapTo<List<UserListDto>>();

                return new PagedResultDto<UserListDto>(usuariosNaoAssociados.Count(), map);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
        public async Task<PagedResultDto<UserListDto>> GetUsuariosAssociados(IdInput input)
        {
            var empresaLogada = await Repository.GetAsync(input.Id);
            var result = empresaLogada.Usuarios.MapTo<List<UserListDto>>();
            return new PagedResultDto<UserListDto>(empresaLogada.Usuarios.Count, result);
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa)]
        public async Task AssociaUsuarioEmpresa(AssociacaoUsuarioEmpresaInput input)
        {
            var usuario = await UserManager.FindByIdAsync(input.UsuarioId);
            var empresa = await Repository.GetAsync(input.EmpresaId);
            await _service.AssociaUsuarioEmpresa(usuario, empresa);
        }

        [AbpAuthorize(
             AppPermissions.Pages_Tenant_Global_Empresa,
             AppPermissions.Pages_Tenant_Global_PessoaFisicaEmpresa,
             AppPermissions.Pages_Tenant_Global_PessoaJuridicaEmpresa)]
        public async Task DesassociaUsuarioEmpresa(AssociacaoUsuarioEmpresaInput input)
        {
            var usuario = await UserManager.FindByIdAsync(input.UsuarioId);
            var empresa = await Repository.GetAsync(input.EmpresaId);
            await _service.DesassociaUsuarioEmpresa(usuario, empresa);
        }

        [AbpAllowAnonymous]
        public async Task<Empresa> GetEmpresaBySlug(GetEmpresaInput input)
        {
            return await Repository.FirstOrDefaultAsync(x => x.Slug.ToLower() == input.Slug.ToLower());
        }
    }
}