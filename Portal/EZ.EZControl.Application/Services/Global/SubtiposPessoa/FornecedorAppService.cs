﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Fornecedor, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class FornecedorAppService : EZControlAppServiceBase<Fornecedor>, IFornecedorAppService
    {
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IPessoaFisicaAppService _pessoaFisicaAppService;
        private readonly IPessoaJuridicaAppService _pessoaJuridicaAppService;
        private readonly IFornecedorService _service;

        public FornecedorAppService(
            IRepository<Fornecedor, int> repository,
            IFornecedorService service,
            IPessoaService pessoaService,
            IPessoaFisicaService pessoaFisicaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IPessoaFisicaAppService pessoaFisicaAppService,
            IPessoaJuridicaAppService pessoaJuridicaAppService)
            : base(repository)
        {
            _service = service;
            _pessoaService = pessoaService;
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _pessoaFisicaAppService = pessoaFisicaAppService;
            _pessoaJuridicaAppService = pessoaJuridicaAppService;
        }

        private void ValidateInput(FornecedorInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            //if (CheckForFieldDuplicated(Repository, input.Id, input.Nome, x => x.Nome == input.Nome))
            //{
            //    validationErrors.Add(new ValidationResult("Já existe um fornecedor com esse nome.", new[] { "nome".ToCamelCase() }));
            //}

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        private async Task ProcessInput(Fornecedor fornecedor, FornecedorInput input)
        {
            try
            {
                // Delegates para preencher de input de pessoa fisica e juridica
                Func<FornecedorInput, PessoaFisicaInput> preencherPessoaFisicaInput = (fornecedorInput) =>
                {
                    PessoaFisicaInput pessoaFisicaInput = new PessoaFisicaInput();
                    fornecedorInput.PessoaFisica.MapTo(pessoaFisicaInput);
                    fornecedorInput.Pessoa.MapTo(pessoaFisicaInput);
                    return pessoaFisicaInput;
                };

                Func<FornecedorInput, PessoaJuridicaInput> preencherPessoaJuridicaInput = (fornecedorInput) =>
                {
                    PessoaJuridicaInput pessoaJuridicaInput = new PessoaJuridicaInput();
                    fornecedorInput.PessoaJuridica.MapTo(pessoaJuridicaInput);
                    fornecedorInput.Pessoa.MapTo(pessoaJuridicaInput);
                    return pessoaJuridicaInput;
                };

                if (input.Pessoa.TipoPessoa == TipoPessoaEnum.Fisica)
                {
                    if (input.Pessoa.Id > 0)
                    {
                        var pf = await _pessoaFisicaService.GetById(input.Pessoa.Id);
                        fornecedor.Pessoa = pf;
                    }
                    else
                    {
                        PessoaFisicaInput pessoaFisicaInput = preencherPessoaFisicaInput(input);
                        var pessoaId = await _pessoaFisicaAppService.Save(pessoaFisicaInput);
                        PessoaFisica pessoaFisica = await _pessoaFisicaService.GetById(pessoaId.Id);
                        fornecedor.Pessoa = pessoaFisica;
                    }
                }
                else
                {
                    if (input.Pessoa != null &&
                        input.Pessoa.Id > 0)
                    {
                        var pj = await _pessoaJuridicaService.GetById(input.Pessoa.Id);
                        fornecedor.Pessoa = pj;
                    }
                    else
                    {
                        PessoaJuridicaInput pessoaJuridicaInput = preencherPessoaJuridicaInput(input);
                        var pessoaId = await _pessoaJuridicaAppService.Save(pessoaJuridicaInput);
                        PessoaJuridica pessoaJuridica = await _pessoaJuridicaService.GetById(pessoaId.Id);
                        fornecedor.Pessoa = pessoaJuridica;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Fornecedor_Create, AppPermissions.Pages_Tenant_Global_Fornecedor_Edit)]
        public async Task<FornecedorPessoaIdDto> Save(FornecedorInput input)
        {
            FornecedorInput fornecedor = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Fornecedor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                fornecedor = await base.CreateAndReturnEntity<FornecedorInput, FornecedorInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Fornecedor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                fornecedor = await base.UpdateAndReturnEntity<FornecedorInput, FornecedorInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            FornecedorPessoaIdDto result = new FornecedorPessoaIdDto();
            result.FornecedorId = fornecedor.Id;
            result.PessoaId = fornecedor.Pessoa.Id;
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Fornecedor_Create, AppPermissions.Pages_Tenant_Global_Fornecedor_Edit)]
        public async Task<FornecedorInput> SaveAndReturnEntity(FornecedorInput input)
        {
            FornecedorInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Fornecedor_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<FornecedorInput, FornecedorInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Fornecedor_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<FornecedorInput, FornecedorInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<FornecedorInput> GetById(IdInput input)
        {
            return await base.GetEntityById<FornecedorInput, IdInput>(_service, input);
        }

        public async Task<FornecedorInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<FornecedorInput, IdInput>(_service, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Fornecedor_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_service, input);
        }

        public async Task<PagedResultDto<FornecedorListDto>> GetFornecedoresPaginado(GetFornecedorInput input)
        {
            try
            {
                var queryFornecedor = _service.GetAll();
                var queryPessoa = _pessoaService.GetAll();
                var queryPessoaFisica = _pessoaFisicaService.GetAll();
                var queryPessoaJuridica = _pessoaJuridicaService.GetAll();

                var query =  from pessoa in queryPessoa
                              join fornecedor in queryFornecedor on pessoa.Id equals fornecedor.Pessoa.Id
                              join pessoaFisica in queryPessoaFisica on fornecedor.Pessoa.Id equals pessoaFisica.Id into pessoaFisicaGroup
                              from pf in pessoaFisicaGroup.DefaultIfEmpty()
                              join pessoaJuridica in queryPessoaJuridica on fornecedor.Pessoa.Id equals pessoaJuridica.Id into pessoaJuridicaGroup
                              from pj in pessoaJuridicaGroup.DefaultIfEmpty()
                              select new
                              {
                                  fornecedor,
                                  pessoa,
                                  pessoaFisica = pf,
                                  pessoaJuridica = pj
                              };

                if (input != null)
                {
                    if (!string.IsNullOrEmpty(input.Nome))
                        query = query.Where(x =>
                        x.pessoaFisica.Nome.Contains(input.Nome) ||
                        x.pessoaJuridica.NomeFantasia.Contains(input.Nome) ||
                        x.pessoaJuridica.RazaoSocial.Contains(input.Nome));

                    if (!string.IsNullOrEmpty(input.NumeroDocumento))
                        query = query.Where(x => x.pessoa.Documentos.Any(y => y.Numero == input.NumeroDocumento));

                    if (Enum.IsDefined(typeof(TipoDeDocumentoEnum), input.TipoDeDocumento))
                    {
                        query = query.Where(x => x.pessoa.Documentos.Any(y => y.TipoDeDocumento.TipoDeDocumentoFixo == input.TipoDeDocumento));
                    }
                }

                var count = await query.CountAsync();
                var dados = await query
                    .OrderBy("fornecedor.Id")
                    .PageBy(input)
                    .ToListAsync();

                List<FornecedorListDto> listDtos = new List<FornecedorListDto>();

                foreach (var item in dados)
                {
                    FornecedorListDto fornecedortListDto = item.fornecedor.MapTo<FornecedorListDto>();

                    listDtos.Add(fornecedortListDto);
                }

                return new PagedResultDto<FornecedorListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForFornecedor(GetPessoaExceptForFornecedor input)
        {
            try
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    if (input == null)
                        throw new Exception("É necessário definir um critério para buscar por pessoas.");

                    var idsFornecedores = Repository.GetAll().Select(y => y.Pessoa.Id);

                    var queryPessoaFisica = _pessoaFisicaService
                        .GetAll()
                        .Where(x => !idsFornecedores.Contains(x.Id))
                        .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.Nome.Contains(input.NomePessoa))
                        .Select(x => new PessoaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.Nome,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Fisica
                        });

                    var queryPessoaJuridica = _pessoaJuridicaService
                        .GetAll()
                        .Where(x => !idsFornecedores.Contains(x.Id))
                        .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                        .Select(x => new PessoaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.NomeFantasia,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Juridica
                        });

                    IQueryable<PessoaListDto> query;

                    if (input.TipoPessoa != null && input.TipoPessoa == TipoPessoaEnum.Fisica)
                    {
                        query = queryPessoaFisica;
                    }
                    else if (input.TipoPessoa != null && input.TipoPessoa == TipoPessoaEnum.Juridica)
                    {
                        query = queryPessoaJuridica;
                    }
                    else
                    {
                        query = queryPessoaJuridica.Union(queryPessoaFisica);
                    }

                    var count = await query.CountAsync();

                    if (!string.IsNullOrEmpty(input.Sorting))
                        query = query.OrderBy(input.Sorting);

                    List<PessoaListDto> dados = await query
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    var listDtos = dados.MapTo<List<PessoaListDto>>();
                    return new PagedResultDto<PessoaListDto>(count, listDtos);
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}