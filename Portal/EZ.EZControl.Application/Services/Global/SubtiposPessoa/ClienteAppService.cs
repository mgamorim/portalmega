﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cliente, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ClienteAppService : EZControlAppServiceBase<Cliente>, IClienteAppService
    {
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IPessoaFisicaAppService _pessoaFisicaAppService;
        private readonly IPessoaJuridicaAppService _pessoaJuridicaAppService;
        private readonly IClienteService _service;

        public ClienteAppService(
            IRepository<Cliente, int> repository,
            IClienteService service,
            IPessoaService pessoaService,
            IPessoaFisicaService pessoaFisicaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IPessoaFisicaAppService pessoaFisicaAppService,
            IPessoaJuridicaAppService pessoaJuridicaAppService)
            : base(repository)
        {
            _service = service;
            _pessoaService = pessoaService;
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _pessoaFisicaAppService = pessoaFisicaAppService;
            _pessoaJuridicaAppService = pessoaJuridicaAppService;
        }

        private void ValidateInput(ClienteInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            //if (CheckForFieldDuplicated(Repository, input.Id, input.Nome, x => x.Nome == input.Nome))
            //{
            //    validationErrors.Add(new ValidationResult("Já existe um cliente com esse nome.", new[] { "nome".ToCamelCase() }));
            //}

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        private async Task ProcessInput(Cliente cliente, ClienteInput input)
        {
            try
            {
                // Delegates para preencher de input de pessoa fisica e juridica
                Func<ClienteInput, PessoaFisicaInput> preencherPessoaFisicaInput = (clienteInput) =>
                {
                    PessoaFisicaInput pessoaFisicaInput = new PessoaFisicaInput();
                    clienteInput.PessoaFisica.MapTo(pessoaFisicaInput);
                    clienteInput.Pessoa.MapTo(pessoaFisicaInput);
                    return pessoaFisicaInput;
                };

                Func<ClienteInput, PessoaJuridicaInput> preencherPessoaJuridicaInput = (clienteInput) =>
                {
                    PessoaJuridicaInput pessoaJuridicaInput = new PessoaJuridicaInput();
                    clienteInput.PessoaJuridica.MapTo(pessoaJuridicaInput);
                    clienteInput.Pessoa.MapTo(pessoaJuridicaInput);
                    return pessoaJuridicaInput;
                };

                if (input.Pessoa.TipoPessoa == TipoPessoaEnum.Fisica)
                {
                    if (input.Pessoa.Id > 0)
                    {
                        var pf = await _pessoaFisicaService.GetById(input.Pessoa.Id);
                        cliente.Pessoa = pf;
                    }
                    else
                    {
                        PessoaFisicaInput pessoaFisicaInput = preencherPessoaFisicaInput(input);
                        var pessoaId = await _pessoaFisicaAppService.Save(pessoaFisicaInput);
                        PessoaFisica pessoaFisica = await _pessoaFisicaService.GetById(pessoaId.Id);
                        cliente.Pessoa = pessoaFisica;
                    }
                }
                else
                {
                    if (input.Pessoa != null &&
                        input.Pessoa.Id > 0)
                    {
                        var pj = await _pessoaJuridicaService.GetById(input.Pessoa.Id);
                        cliente.Pessoa = pj;
                    }
                    else
                    {
                        PessoaJuridicaInput pessoaJuridicaInput = preencherPessoaJuridicaInput(input);
                        var pessoaId = await _pessoaJuridicaAppService.Save(pessoaJuridicaInput);
                        PessoaJuridica pessoaJuridica = await _pessoaJuridicaService.GetById(pessoaId.Id);
                        cliente.Pessoa = pessoaJuridica;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cliente_Create, AppPermissions.Pages_Tenant_Global_Cliente_Edit)]
        public async Task<ClientePessoaIdDto> Save(ClienteInput input)
        {
            ClienteInput cliente = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cliente_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                cliente = await base.CreateAndReturnEntity<ClienteInput, ClienteInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cliente_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                cliente = await base.UpdateAndReturnEntity<ClienteInput, ClienteInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            ClientePessoaIdDto result = new ClientePessoaIdDto
            {
                ClienteId = cliente.Id,
                PessoaId = cliente.Pessoa.Id
            };
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cliente_Create, AppPermissions.Pages_Tenant_Global_Cliente_Edit)]
        public async Task<ClienteInput> SaveAndReturnEntity(ClienteInput input)
        {
            ClienteInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cliente_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ClienteInput, ClienteInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_Global_Cliente_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ClienteInput, ClienteInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<ClienteInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ClienteInput, IdInput>(_service, input);
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForCliente(GetPessoaExceptForCliente input)
        {
            try
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    if (input == null)
                        throw new Exception("É necessário definir um critério para buscar por pessoas.");

                    var idsClientes = Repository.GetAll().Select(y => y.Pessoa.Id);

                    var queryPessoaFisica = _pessoaFisicaService
                        .GetAll()
                        .Where(x => !idsClientes.Contains(x.Id))
                        .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.Nome.Contains(input.NomePessoa))
                        .Select(x => new PessoaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.Nome,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Fisica
                        });

                    var queryPessoaJuridica = _pessoaJuridicaService
                        .GetAll()
                        .Where(x => !idsClientes.Contains(x.Id))
                        .WhereIf(!string.IsNullOrEmpty(input.NomePessoa), x => x.NomeFantasia.Contains(input.NomePessoa))
                        .Select(x => new PessoaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.NomeFantasia,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Juridica
                        });

                    IQueryable<PessoaListDto> query;

                    if (input.TipoPessoa != null && input.TipoPessoa == TipoPessoaEnum.Fisica)
                    {
                        query = queryPessoaFisica;
                    }
                    else if (input.TipoPessoa != null && input.TipoPessoa == TipoPessoaEnum.Juridica)
                    {
                        query = queryPessoaJuridica;
                    }
                    else
                    {
                        query = queryPessoaJuridica.Union(queryPessoaFisica);
                    }

                    var count = await query.CountAsync();

                    if (!string.IsNullOrEmpty(input.Sorting))
                        query = query.OrderBy(input.Sorting);

                    List<PessoaListDto> dados = await query
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    var listDtos = dados.MapTo<List<PessoaListDto>>();
                    return new PagedResultDto<PessoaListDto>(count, listDtos);
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<ClienteInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ClienteInput, IdInput>(_service, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_Global_Cliente_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_service, input);
        }

        public async Task<PagedResultDto<ClienteListDto>> GetClientesPaginado(GetClienteInput input)
        {
            try
            {
                var queryCliente = _service.GetAll();
                var queryPessoa = _pessoaService.GetAll();
                var queryPessoaFisica = _pessoaFisicaService.GetAll();
                var queryPessoaJuridica = _pessoaJuridicaService.GetAll();

                var query = from pessoa in queryPessoa
                            join cliente in queryCliente on pessoa.Id equals cliente.Pessoa.Id
                            join pessoaFisica in queryPessoaFisica on cliente.Pessoa.Id equals pessoaFisica.Id into pessoaFisicaGroup
                            from pf in pessoaFisicaGroup.DefaultIfEmpty()
                            join pessoaJuridica in queryPessoaJuridica on cliente.Pessoa.Id equals pessoaJuridica.Id into pessoaJuridicaGroup
                            from pj in pessoaJuridicaGroup.DefaultIfEmpty()
                            select new
                            {
                                pessoa,
                                cliente,
                                pessoaFisica = pf,
                                pessoaJuridica = pj
                            };

                if (input != null)
                {
                    if (!string.IsNullOrEmpty(input.Nome))
                        query = query.Where(x =>
                        x.pessoaFisica.Nome.Contains(input.Nome) ||
                        x.pessoaJuridica.NomeFantasia.Contains(input.Nome) ||
                        x.pessoaJuridica.RazaoSocial.Contains(input.Nome));

                    if (!string.IsNullOrEmpty(input.NumeroDocumento))
                        query = query.Where(x => x.pessoa.Documentos.Any(y => y.Numero == input.NumeroDocumento));

                    if (Enum.IsDefined(typeof(TipoDeDocumentoEnum), input.TipoDeDocumento))
                    {
                        query = query.Where(x => x.pessoa.Documentos.Any(y => y.TipoDeDocumento.TipoDeDocumentoFixo == input.TipoDeDocumento));
                    }
                }

                var count = await query.CountAsync();
                var dados = await query
                    .OrderBy("cliente.Id")
                    .PageBy(input)
                    .ToListAsync();

                List<ClienteListDto> listDtos = new List<ClienteListDto>();

                foreach (var item in dados)
                {
                    ClienteListDto clientetListDto = item.cliente.MapTo<ClienteListDto>();

                    listDtos.Add(clientetListDto);
                }

                return new PagedResultDto<ClienteListDto>(count, listDtos);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}