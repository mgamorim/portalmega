﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa
{
    [AbpAuthorize(AppPermissions.Pages_Administration_UsuarioPorPessoa, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class UsuarioPorPessoaAppService : EZControlAppServiceBase<UsuarioPorPessoa>, IUsuarioPorPessoaAppService
    {
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IDocumentoService _documentoService;
        private readonly IEnderecoEletronicoService _enderecoEletronicoService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IPessoaFisicaAppService _pessoaFisicaAppService;
        private readonly IPessoaJuridicaAppService _pessoaJuridicaAppService;
        private readonly IUsuarioPorPessoaService _service;
        private readonly UserManager _userService;
        private readonly UserAppService _userAppService;
        private readonly RoleManager _roleManager;

        public UsuarioPorPessoaAppService(
            IRepository<UsuarioPorPessoa, int> repository,
            IUsuarioPorPessoaService service,
            IPessoaService pessoaService,
            IPessoaFisicaService pessoaFisicaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IPessoaFisicaAppService pessoaFisicaAppService,
            IPessoaJuridicaAppService pessoaJuridicaAppService,
            UserManager userService,
            IDocumentoService documentoService,
            IEnderecoEletronicoService enderecoEletronicoService,
            RoleManager roleManager,
            UserAppService userAppService)
            : base(repository)
        {
            _service = service;
            _pessoaService = pessoaService;
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _pessoaFisicaAppService = pessoaFisicaAppService;
            _pessoaJuridicaAppService = pessoaJuridicaAppService;
            _userService = userService;
            _documentoService = documentoService;
            _enderecoEletronicoService = enderecoEletronicoService;
            _roleManager = roleManager;
            _userAppService = userAppService;
        }

        private void ValidateInput(UsuarioPorPessoaInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (input.User == null)
            {
                validationErrors.Add(new ValidationResult(string.Format(L("UsuarioPorPessoa.EmptyUserError"), input.User), new[] { "user".ToCamelCase() }));
            }
            else
            {
                if (string.IsNullOrEmpty(input.User.Name))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("UsuarioPorPessoa.EmptyNameError"), input.User), new[] { "name".ToCamelCase() }));
                }

                if (string.IsNullOrEmpty(input.User.Surname))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("UsuarioPorPessoa.EmptySurnameError"), input.User), new[] { "surname".ToCamelCase() }));
                }

                if (string.IsNullOrEmpty(input.User.UserName))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("UsuarioPorPessoa.EmptyUserNameError"), input.User), new[] { "userName".ToCamelCase() }));
                }

                if (string.IsNullOrEmpty(input.User.EmailAddress))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("UsuarioPorPessoa.EmptyEmailError"), input.User), new[] { "emailAddress".ToCamelCase() }));
                }

                if (string.IsNullOrEmpty(input.RoleName))
                {
                    validationErrors.Add(new ValidationResult(string.Format(L("UsuarioPorPessoa.EmptyRoleError"), input.User), new[] { "roleName".ToCamelCase() }));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        private async Task ProcessInput(UsuarioPorPessoa usuarioPorPessoa, UsuarioPorPessoaInput input)
        {
            try
            {
                if (input.UserId > 0)
                {
                    try
                    {
                        usuarioPorPessoa.User = await _userService.GetUserByIdAsync(input.UserId.To<long>());
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundUserError"), ex.Message);
                    }
                }
                else
                {
                    // Delegates para preencher de input de pessoa fisica e juridica
                    Func<UsuarioPorPessoaInput, PessoaFisicaInput> preencherPessoaFisicaInput = (usuarioPorPessoaInput) =>
                    {
                        PessoaFisicaInput pessoaFisicaInput = new PessoaFisicaInput();
                        usuarioPorPessoaInput.User.PessoaFisica.MapTo(pessoaFisicaInput);
                        usuarioPorPessoaInput.User.PessoaFisica.MapTo(pessoaFisicaInput);
                        return pessoaFisicaInput;
                    };

                    var idPessoa = input.User.PessoaFisica?.Id ?? (input.User.Pessoa?.Id ?? 0);


                    if (input.User != null && idPessoa > 0)
                    {
                        var pf = await _pessoaFisicaService.GetById(idPessoa);

                        pf.Documentos =
                            _documentoService.GetAll()
                                .Where(x => x.PessoaId == idPessoa)
                                .ToList();

                        pf.EnderecosEletronicos =
                            _enderecoEletronicoService.GetAll()
                                .Where(x => x.PessoaId == idPessoa)
                                .ToList();

                        if (!pf.EnderecosEletronicos.Any())
                            throw new UserFriendlyException(L("UsuarioPorPessoa.NotFoundEmailError"));

                        var user = User.CreateUserPorPessoa(pf, input.User.TipoDeUsuario, input.User.UserName);

                        //Assign roles
                        var role = await _roleManager.FindByNameAsync(input.RoleName);
                        if (role != null)
                            user.Roles = new List<UserRole> { new UserRole(AbpSession.TenantId, input.UserId, role.Id) };

                        usuarioPorPessoa.User = user;
                    }
                    else
                    {
                        PessoaFisicaInput pessoaFisicaInput = preencherPessoaFisicaInput(input);
                        var pessoaId = await _pessoaFisicaAppService.Save(pessoaFisicaInput);
                        PessoaFisica pessoaFisica = await _pessoaFisicaService.GetById(pessoaId.Id);
                        usuarioPorPessoa.User.Pessoa = pessoaFisica;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_UsuarioPorPessoa_Create, AppPermissions.Pages_Administration_UsuarioPorPessoa_Edit)]
        public async Task<UsuarioPorPessoaIdDto> Save(UsuarioPorPessoaInput input)
        {
            UsuarioPorPessoaInput usuarioPorPessoa = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Administration_UsuarioPorPessoa_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                usuarioPorPessoa = await base.CreateAndReturnEntity<UsuarioPorPessoaInput, UsuarioPorPessoaInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Administration_UsuarioPorPessoa_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                usuarioPorPessoa = await base.UpdateAndReturnEntity<UsuarioPorPessoaInput, UsuarioPorPessoaInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            UsuarioPorPessoaIdDto result = new UsuarioPorPessoaIdDto
            {
                UsuarioPorPessoaId = usuarioPorPessoa.Id,
                UsuarioId = usuarioPorPessoa.User.Id.To<int>(),
                PessoaId = usuarioPorPessoa.User.Pessoa.Id
            };
            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_UsuarioPorPessoa_Create, AppPermissions.Pages_Administration_UsuarioPorPessoa_Edit)]
        public async Task<UsuarioPorPessoaInput> SaveAndReturnEntity(UsuarioPorPessoaInput input)
        {
            UsuarioPorPessoaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Administration_UsuarioPorPessoa_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<UsuarioPorPessoaInput, UsuarioPorPessoaInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Administration_UsuarioPorPessoa_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<UsuarioPorPessoaInput, UsuarioPorPessoaInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        public async Task<UsuarioPorPessoaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<UsuarioPorPessoaInput, IdInput>(_service, input);
        }

        public async Task<UsuarioPorPessoaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<UsuarioPorPessoaInput, IdInput>(_service, input);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_UsuarioPorPessoa_Delete)]
        public async Task Delete(IdInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                var usuarioPorPessoa = _service.GetAll().FirstOrDefault(x => x.Id == input.Id);

                if (usuarioPorPessoa != null && usuarioPorPessoa.User != null)
                    await _userService.DeleteAsync(usuarioPorPessoa.User);

                await base.DeleteEntity(_service, input);
                trans.Complete();
            }
        }

        public async Task<PagedResultDto<UsuarioPorPessoaListDto>> GetUsuariosPorPessoaPaginado(GetUsuarioPorPessoaInput input)
        {
            var condicoes = new List<WhereIfCondition<UsuarioPorPessoa>>
            {
                new WhereIfCondition<UsuarioPorPessoa>(!string.IsNullOrEmpty(input.Nome),
                    x => x.User.Pessoa.PessoaFisica.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<UsuarioPorPessoaListDto, GetUsuarioPorPessoaInput>(_service, input, condicoes);

            return result;
        }

        public virtual async Task<PagedResultDto<PessoaListDto>> GetPessoaExceptForUsuario(GetPessoaExceptForUsuario input)
        {
            try
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    if (input == null)
                        throw new Exception("É necessário definir um critério para buscar por pessoas.");

                    var queryPessoaFisica = _pessoaFisicaService
                        .GetAll()
                        .WhereIf(!string.IsNullOrEmpty(input.Nome), x => x.Nome.Contains(input.Nome))
                        .Select(x => new UsuarioPessoaFisicaListDto
                        {
                            Id = x.Id,
                            NomePessoa = x.Nome,
                            Nome = x.Nome,
                            EstadoCivil = x.EstadoCivil,
                            Sexo = x.Sexo,
                            Nacionalidade = x.Nacionalidade,
                            EmailPessoal =
                                (x.EnderecosEletronicos.Any() && x.EnderecosEletronicos.FirstOrDefault(e => e.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal) != null) ?
                                x.EnderecosEletronicos.FirstOrDefault(e => e.TipoDeEnderecoEletronico == TipoDeEnderecoEletronicoEnum.EmailPrincipal).Endereco : string.Empty,
                            Cpf =
                                (x.Documentos.Any() && x.Documentos.FirstOrDefault(d => d.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf) != null) ?
                                x.Documentos.FirstOrDefault(d => d.TipoDeDocumento.TipoDeDocumentoFixo == TipoDeDocumentoEnum.Cpf).Numero : string.Empty,
                            GrupoPessoaId = x.GrupoPessoa != null ? x.GrupoPessoa.Id : 0,
                            GrupoPessoa = x.GrupoPessoa != null ? new GrupoPessoaListDto
                            {
                                Id = x.GrupoPessoa.Id,
                                Descricao = x.GrupoPessoa.Descricao,
                                PaiId = x.GrupoPessoa.Pai != null ? x.GrupoPessoa.Pai.Id : 0
                            } : null,
                            TipoPessoa = TipoPessoaEnum.Fisica
                        });

                    IQueryable<PessoaListDto> query = queryPessoaFisica;

                    var count = await query.CountAsync();

                    List<PessoaListDto> dados = await query
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    var listDtos = dados.MapTo<List<PessoaListDto>>();
                    return new PagedResultDto<PessoaListDto>(count, listDtos);
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}