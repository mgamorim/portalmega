﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.Global.SubtiposPessoa
{
    [AbpAuthorize(AppPermissions.Pages_Administration_ClienteEZ, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ClienteEZAppService : EZControlAppServiceBase<ClienteEZ>, IClienteEZAppService
    {
        private readonly IPessoaService _pessoaService;
        private readonly IPessoaFisicaService _pessoaFisicaService;
        private readonly IPessoaJuridicaService _pessoaJuridicaService;
        private readonly IPessoaFisicaAppService _pessoaFisicaAppService;
        private readonly IPessoaJuridicaAppService _pessoaJuridicaAppService;
        private readonly IClienteEZService _service;

        public ClienteEZAppService(
            IRepository<ClienteEZ, int> repository,
            IClienteEZService service,
            IPessoaService pessoaService,
            IPessoaFisicaService pessoaFisicaService,
            IPessoaJuridicaService pessoaJuridicaService,
            IPessoaFisicaAppService pessoaFisicaAppService,
            IPessoaJuridicaAppService pessoaJuridicaAppService)
            : base(repository)
        {
            _service = service;
            _pessoaService = pessoaService;
            _pessoaFisicaService = pessoaFisicaService;
            _pessoaJuridicaService = pessoaJuridicaService;
            _pessoaFisicaAppService = pessoaFisicaAppService;
            _pessoaJuridicaAppService = pessoaJuridicaAppService;
        }

        private void ValidateInput(ClienteEZDto input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            //if (CheckForFieldDuplicated(Repository, input.Id, input.Nome, x => x.Nome == input.Nome))
            //{
            //    validationErrors.Add(new ValidationResult("Já existe um clienteEZ com esse nome.", new[] { "nome".ToCamelCase() }));
            //}

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        private async Task ProcessInput(ClienteEZ clienteEZ, ClienteEZDto input)
        {
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_ClienteEZ)]
        public async Task<ClienteEZPessoaIdDto> Save(ClienteEZDto input)
        {
            ClienteEZDto clienteEZ = null;

            if (input.Id == default(int))
            {
                clienteEZ = await base.CreateAndReturnEntity<ClienteEZDto, ClienteEZDto>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                clienteEZ = await base.UpdateAndReturnEntity<ClienteEZDto, ClienteEZDto>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            //TODO: Remover esse Dto sem sentido
            ClienteEZPessoaIdDto result = new ClienteEZPessoaIdDto();
            result.ClienteEZId = clienteEZ.Id;
            result.PessoaId = clienteEZ.Id;
            return result;
        }

        public async Task<ClienteEZDto> SaveAndReturnEntity(ClienteEZDto input)
        {
            return await base.CreateAndReturnEntity<ClienteEZDto, ClienteEZDto>(_service, input, () => ValidateInput(input), ProcessInput);
        }

        public async Task<ClienteEZDto> Get()
        {
            ClienteEZDto result = new ClienteEZDto();

            var clienteEz = await _service.GetAll().FirstOrDefaultAsync();

            if (clienteEz != null)
            {
                clienteEz.MapTo(result);
            }

            return result;
        }

        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_service, input);
        }
    }
}