﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto;
using EZ.EZControl.Dto.CMS.Geral.Template;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Template, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TemplateAppService : EZControlAppServiceBase<Template>, ITemplateAppService
    {
        private readonly ITemplateService _service;
        private readonly ISiteService _siteService;

        public TemplateAppService(IRepository<Template> repository, ITemplateService service, ISiteService siteService)
            : base(repository)
        {
            this._service = service;
            this._siteService = siteService;
        }

        private void ValidateEntiy(List<Template> templates)
        {
            var validationErrors = new List<ValidationResult>();
            foreach (Template input in templates)
            {
                if (string.IsNullOrEmpty(input.Nome))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
                }
                if (string.IsNullOrEmpty(input.Versao))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredVersaoError"), input.Versao), new[] { "versao".ToCamelCase() }));
                }
                if (string.IsNullOrEmpty(input.TiposDePaginasSuportadas))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredTipoDePagina"), input.TiposDePaginasSuportadas), new[] { "tiposDePaginasSuportadas".ToCamelCase() }));
                }
                if (string.IsNullOrEmpty(input.TipoDePaginaParaPaginaDefault))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredTipoDePagina"), input.TipoDePaginaParaPaginaDefault), new[] { "tipoDePaginaParaPaginaDefault".ToCamelCase() }));
                }
                if (string.IsNullOrEmpty(input.TipoDePaginaParaPostDefault))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredTipoDePagina"), input.TipoDePaginaParaPostDefault), new[] { "tipoDePaginaParaPostDefault".ToCamelCase() }));
                }
                if (string.IsNullOrEmpty(input.TipoDePaginaParaCategoriaDefault))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredTipoDePagina"), input.TipoDePaginaParaCategoriaDefault), new[] { "tipoDePaginaParaCategoriaDefault".ToCamelCase() }));
                }
                if (string.IsNullOrEmpty(input.TipoDePaginaParaPaginaInicialDefault))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Template.RequiredTipoDePagina"), input.TipoDePaginaParaPaginaInicialDefault), new[] { "tipoDePaginaParaPaginaInicialDefault".ToCamelCase() }));
                }
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<PagedResultDto<TemplateListDto>> GetTemplatesPaginado(GetTemplateInput input)
        {
            var condicoes = new List<WhereIfCondition<Template>>
            {
                new WhereIfCondition<Template>(
                        !input.Nome.IsNullOrEmpty() ||
                        !input.Autor.IsNullOrEmpty() ||
                        !input.Descricao.IsNullOrEmpty(),
                            i =>
                        i.Nome.Contains(input.Nome) ||
                        i.Autor.Contains(input.Autor) ||
                        i.Descricao.Contains(input.Descricao))
            };
            var result = await base.GetListPaged<TemplateListDto, GetTemplateInput>(_service, input, condicoes);
            return result;
        }

        public async Task<TemplateInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TemplateInput, IdInput>(_service, input);
        }
        [AbpAllowAnonymous]
        public async Task<TemplateInput> GetByIdAnonymous(IdInput input)
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                try
                {
                    var template = Repository.Get(input.Id);
                    return template.MapTo<TemplateInput>();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        public async Task<TemplateInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TemplateInput, IdInput>(_service, input);
        }

        public async Task Sincronizar()
        {
            try
            {
                var templates = _service.GetTemplates();
                ValidateEntiy(templates.ToList());
                await SincronizarTemplates(templates.ToList());
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        private async Task SincronizarTemplates(List<Template> templates)
        {
            try
            {
                //Templates que devem ser atualizados, caso não existam na pasta e existam no banco.
                var templatesASeremAtualizados = await _service.GetAll()
                                                            .ToListAsync();

                templatesASeremAtualizados = templatesASeremAtualizados
                    .Where(t => !(templates.Select(x => x.Nome).Contains(t.Nome)))
                    .ToList();

                if (templatesASeremAtualizados.Any())
                {
                    foreach (Template templateDoBanco in templatesASeremAtualizados)
                    {
                        templateDoBanco.IsActive = false;
                        await _service.CreateOrUpdateEntity(templateDoBanco);
                    }
                }

                foreach (var template in templates)
                {
                    var templatesCriados = await _service.GetAll()
                                                        .ToListAsync();

                    var templateCriado = templatesCriados
                                            .FirstOrDefault(x =>
                                                            x.Nome.ToLower() == template.Nome.ToLower() &&
                                                            x.Versao == template.Versao);

                    if (templateCriado == null)
                    {
                        var templateInalterado = templatesCriados
                            .FirstOrDefault(x =>
                                            x.Nome.ToLower() == template.Nome.ToLower());

                        if (templateInalterado != null)
                        {
                            templateInalterado.Autor = template.Autor;
                            templateInalterado.AutorUrl = template.AutorUrl;
                            templateInalterado.Descricao = template.Descricao;
                            templateInalterado.Nome = template.Nome;
                            templateInalterado.NomeDoArquivo = template.NomeDoArquivo;
                            templateInalterado.Posicoes = template.Posicoes;
                            templateInalterado.TipoDePaginaParaCategoriaDefault = template.TipoDePaginaParaCategoriaDefault;
                            templateInalterado.TipoDePaginaParaPaginaDefault = template.TipoDePaginaParaPaginaDefault;
                            templateInalterado.TipoDePaginaParaPaginaInicialDefault = template.TipoDePaginaParaPaginaInicialDefault;
                            templateInalterado.TipoDePaginaParaPostDefault = template.TipoDePaginaParaPostDefault;
                            templateInalterado.TiposDePaginasSuportadas = template.TiposDePaginasSuportadas;
                            templateInalterado.TiposDeWidgetSuportados = template.TiposDeWidgetSuportados;
                            templateInalterado.Versao = template.Versao;

                            await _service.CreateOrUpdateEntity(templateInalterado);
                        }
                    }
                    else
                    {
                        if (templateCriado.IsActive == false)
                        {
                            templateCriado.IsActive = true;
                            await _service.CreateOrUpdateEntity(templateCriado);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<ListResultDto<StringDto>> GetListPosicoes(IdInput input)
        {
            var template = await _service.GetById(input.Id);
            var dados = _service.GetPosicoesByTemplate(template).Select(x => new StringDto { Valor = x }).ToImmutableList();
            var lista = new ListResultDto<StringDto>(dados);

            return lista;
        }

        public async Task<ListResultDto<StringDto>> GetTiposDeWidgetSuportados(IdInput input)
        {
            var template = await _service.GetById(input.Id);
            var dados = _service.GetTiposDeWidgetSuportadosByTemplate(template).Select(x => new StringDto { Valor = x }).ToImmutableList();
            var lista = new ListResultDto<StringDto>(dados);

            return lista;
        }

        public async Task<ListResultDto<StringDto>> GetTiposDePaginasSuportadas(IdInput input)
        {
            var template = await _service.GetById(input.Id);
            var dados = _service.GetTiposDePaginasSuportadasByTemplate(template).Select(x => new StringDto { Valor = x }).ToImmutableList();
            var lista = new ListResultDto<StringDto>(dados);

            return lista;
        }

        public async Task<ListResultDto<StringDto>> GetTiposDePaginasSuportadasBySite(IdInput input)
        {
            var site = await _siteService.GetById(input.Id);
            var dados = _service.GetTiposDePaginasSuportadasByTemplate(site.TemplateDefault).Select(x => new StringDto { Valor = x }).ToImmutableList();
            var lista = new ListResultDto<StringDto>(dados);

            return lista;
        }
    }
}
