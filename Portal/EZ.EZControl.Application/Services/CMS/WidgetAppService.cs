﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Widget;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Widget, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class WidgetAppService : EZControlAppServiceBase<WidgetBase>, IWidgetAppService
    {
        public readonly IWidgetBaseService<WidgetBase> _baseService;
        public readonly IWidgetHTMLService _widgetHTMLService;
        public readonly IWidgetMenuService _widgetMenuService;
        public readonly ISiteService _siteService;
        public readonly IMenuService _menuService;
        public readonly ITemplateService _templateService;

        public WidgetAppService(IRepository<WidgetBase> repository, IWidgetBaseService<WidgetBase> service,
            IWidgetHTMLService widgetHTMLService, IWidgetMenuService widgetMenuService,
            ISiteService siteService, IMenuService menuService, ITemplateService templateService)
            : base(repository)
        {
            _baseService = service;
            _widgetHTMLService = widgetHTMLService;
            _widgetMenuService = widgetMenuService;
            _siteService = siteService;
            _menuService = menuService;
            _templateService = templateService;
        }

        private void ValidateInput(WidgetInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Titulo == input.Titulo))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Widget.DuplicateTituloError"), input.Titulo), new[] { "titulo".ToCamelCase() }));
            }
            if (string.IsNullOrEmpty(input.Titulo))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Widget.RequiredTituloError"), input.Titulo), new[] { "titulo".ToCamelCase() }));
            }
            if (input.Tipo == TipoDeWidgetEnum.CmsHtml && string.IsNullOrEmpty(input.Conteudo))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Widget.RequiredHTMLError"), input.Conteudo), new[] { "conteudo".ToCamelCase() }));
            }
            if (input.Tipo == TipoDeWidgetEnum.CmsMenu && input.MenuId <= 0)
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Widget.RequiredMenuError"), input.MenuId), new[] { "menuId".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Widget_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_baseService, input);
        }

        public async Task<WidgetInput> GetById(IdInput input)
        {
            return await base.GetEntityById<WidgetInput, IdInput>(_baseService, input);
        }

        public async Task<WidgetInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<WidgetInput, IdInput>(_baseService, input);
        }

        public async Task<PagedResultDto<WidgetListDto>> GetPaginado(GetWidgetInput input)
        {
            var condicoes = new List<WhereIfCondition<WidgetBase>>();
            condicoes.Add(new WhereIfCondition<WidgetBase>(!input.Titulo.IsNullOrEmpty(), i => i.Titulo.Contains(input.Titulo)));
            var result = await base.GetListPaged<WidgetListDto, GetWidgetInput>(_baseService, input, condicoes);
            return result;
        }

        [AbpAuthorize(new[] { AppPermissions.Pages_Tenant_CMS_Widget_Create, AppPermissions.Pages_Tenant_CMS_Widget_Edit })]
        public async Task<IdInput> Save(WidgetInput input)
        {
            var id = 0;
            ValidateInput(input);
            if (input.Tipo == TipoDeWidgetEnum.CmsHtml)
            {
                var entity = input.MapTo<WidgetHTML>();
                entity = await ProcesInputHtml(entity, input);

                //TODO: Separar este método Save em Create e Update
                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Widget_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    id = await _widgetHTMLService.CreateEntity(entity);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Widget_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    id = (await _widgetHTMLService.UpdateEntity(entity)).Id;
                }
            }
            else
            {
                var entity = input.MapTo<WidgetMenu>();
                entity = await ProcessInputMenu(entity, input);

                //TODO: Separar este método Save em Create e Update
                if (input.Id == default(int))
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Widget_Create))
                        throw new UserFriendlyException(L("PermissionToCreateRecord"));

                    id = await _widgetMenuService.CreateEntity(entity);
                }
                else
                {
                    if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Widget_Edit))
                        throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                    id = (await _widgetMenuService.UpdateEntity(entity)).Id;
                }
            }
            return new IdInput(id);
        }

        private async Task<WidgetHTML> ProcesInputHtml(WidgetHTML widget, WidgetInput widgetInput)
        {
            if (widgetInput.SiteId > 0)
            {
                try
                {
                    widget.Site = await _siteService.GetById(widgetInput.SiteId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Widget.SiteNotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Widget.SiteNullError"));
            }

            if (widgetInput.TemplateId > 0)
            {
                try
                {
                    widget.Template = await _templateService.GetById(widgetInput.TemplateId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Widget.TemplateNotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Widget.TemplateNullError"));
            }

            return widget;
        }

        private async Task<WidgetMenu> ProcessInputMenu(WidgetMenu widget, WidgetInput widgetInput)
        {
            if (widgetInput.SiteId > 0)
            {
                try
                {
                    widget.Site = await _siteService.GetById(widgetInput.SiteId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Widget.SiteNotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Widget.SiteNullError"));
            }

            if (widgetInput.TemplateId > 0)
            {
                try
                {
                    widget.Template = await _templateService.GetById(widgetInput.TemplateId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Widget.TemplateNotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Widget.TemplateNullError"));
            }

            if (widgetInput.MenuId > 0)
            {
                try
                {
                    widget.Menu = await _menuService.GetById(widgetInput.MenuId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Widget.MenuNotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Widget.MenuNullError"));
            }

            return widget;
        }
    }
}
