﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class HostAppService : EZControlAppServiceBase<Host>, IHostAppService
    {
        private readonly IHostService _hostService;

        public HostAppService(IRepository<Host> hostRepository, IHostService hostService)
            : base(hostRepository)
        {
            this._hostService = hostService;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site_Create, AppPermissions.Pages_Tenant_CMS_Site_Edit)]
        public async Task<IdInput> Save(HostInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, HostInput>(_hostService, input, null, null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, HostInput>(_hostService, input, null, null);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site_Create, AppPermissions.Pages_Tenant_CMS_Site_Edit)]
        public async Task<HostInput> SaveAndReturnEntity(HostInput input)
        {
            HostInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<HostInput, HostInput>(_hostService, input, null, null);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<HostInput, HostInput>(_hostService, input, null, null);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_hostService, input);
        }

        public async Task<PagedResultDto<HostListDto>> GetPaginado(GetHostInput input)
        {
            var condicoes = new List<WhereIfCondition<Host>>
            {
                new WhereIfCondition<Host>(!input.Url.IsNullOrEmpty(), i => i.Url.Contains(input.Url))
            };
            var result = await base.GetListPaged<HostListDto, GetHostInput>(_hostService, input, condicoes);
            return result;
        }

        public async Task<HostInput> GetById(IdInput input)
        {
            return await base.GetEntityById<HostInput, IdInput>(_hostService, input);
        }

        public async Task<HostInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<HostInput, IdInput>(_hostService, input);
        }
    }
}