﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Arquivo;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace EZ.EZControl.Services.CMS
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Arquivo, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ArquivoCMSAppService : EZControlAppServiceBase<ArquivoCMS>, IArquivoCMSAppService
    {
        private readonly IArquivoCMSService _arquivoCMSService;

        public ArquivoCMSAppService(IRepository<ArquivoCMS> arquivoRepository, IArquivoCMSService service)
            : base(arquivoRepository)
        {
            this._arquivoCMSService = service;
        }

        private void ValidateInput(ArquivoCMSInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(L("Arquivo.EmptyNomeError"), new[] { "nome".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Arquivo.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Arquivo_Create, AppPermissions.Pages_Tenant_CMS_Arquivo_Edit)]
        public async Task<IdInput> Save(ArquivoCMSInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Arquivo_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ArquivoCMSInput>(_arquivoCMSService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Arquivo_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ArquivoCMSInput>(_arquivoCMSService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Arquivo_Create, AppPermissions.Pages_Tenant_CMS_Arquivo_Edit)]
        public async Task<ArquivoCMSInput> SaveAndReturnEntity(ArquivoCMSInput input)
        {
            ArquivoCMSInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Arquivo_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ArquivoCMSInput, ArquivoCMSInput>(_arquivoCMSService, input, () => ValidateInput(input));
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Arquivo_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ArquivoCMSInput, ArquivoCMSInput>(_arquivoCMSService, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Arquivo_Delete)]
        public async Task Delete(IdInput input)
        {
            await ApplyEmpresaFilter(async () =>
            {
                var path = this.Repository.GetAll()
                            .Where(x => x.Id == input.Id)
                            .Select(x => x.Path)
                            .FirstOrDefault();

                this.RemoveFile(HttpRuntime.AppDomainAppPath + path);
                await base.DeleteEntity(_arquivoCMSService, input);
            });
        }

        public async Task<PagedResultDto<ArquivoCMSListDto>> GetArquivosPaginado(GetArquivoCMSInput input)
        {
            var condicoes = new List<WhereIfCondition<ArquivoCMS>>
            {
                new WhereIfCondition<ArquivoCMS>(!string.IsNullOrEmpty(input.Nome),a => a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<ArquivoCMSListDto, GetArquivoCMSInput>(_arquivoCMSService, input, condicoes);
            return result;
        }

        public async Task<ArquivoCMSInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ArquivoCMSInput, IdInput>(_arquivoCMSService, input);
        }

        public async Task<ArquivoCMSInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ArquivoCMSInput, IdInput>(_arquivoCMSService, input);
        }

        private async void RemoveFile(String path)
        {
            await Task.Factory.StartNew(() =>
            {
                if (!String.IsNullOrEmpty(path))
                    File.Delete(path);
            });
        }
    }
}