﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Parametro;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IParametroCMSAppService : IApplicationService
    {
        Task<IdInput> Save(ParametroCMSInput input);

        Task<ParametroCMSInput> Get();
    }
}