﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Publicacao;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IPublicacaoBaseAppService : IApplicationService
    {
        Task<PagedResultDto<PesquisaPublicacaoListDto>> Pesquisar(PesquisaPublicacaoDto input);
    }
}
