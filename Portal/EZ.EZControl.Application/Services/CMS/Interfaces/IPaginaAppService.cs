﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Pagina;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IPaginaAppService : IApplicationService
    {
        Task<IdInput> Save(PaginaInput input);

        Task<PagedResultDto<PaginaListDto>> GetPaginasPaginado(GetPaginaInput input);

        Task<PaginaInput> GetById(IdInput input);

        Task<PaginaInput> GetBySlug(GetPaginaBySlugInput input);

        Task Delete(IdInput input);

        Task<PagedResultDto<PaginaListDto>> GetPaginaExceptForId(GetPaginaExceptForInput input);

        Task<PaginaInput> GetByExternalId(IdInput input);
    }
}