﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Post;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IPostAppService : IApplicationService
    {
        Task<IdInput> Save(PostInput input);
        Task<PagedResultDto<PostListDto>> GetPostsPaginado(GetPostInput input);
        Task<PostInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task<PostInput> GetByExternalId(IdInput input);
    }
}