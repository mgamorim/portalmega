﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface ITagAppService : IApplicationService
    {
        Task<IdInput> Save(TagInput input);
        Task<TagInput> SaveAndReturnEntity(TagInput input);
        Task<PagedResultDto<TagListDto>> GetTagsPaginado(GetTagInput input);
        Task<ListResultDto<TagListDto>> GetTagsExceptForId(GetTagExceptForIdInput input);
        Task<TagInput> GetById(IdInput input);
        Task Delete(IdInput input);
        Task<TagInput> GetByExternalId(IdInput input);
    }
}
