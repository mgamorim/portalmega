﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto;
using EZ.EZControl.Dto.CMS.Geral.Template;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface ITemplateAppService : IApplicationService
    {
        Task<PagedResultDto<TemplateListDto>> GetTemplatesPaginado(GetTemplateInput input);
        Task<TemplateInput> GetById(IdInput input);
        Task<TemplateInput> GetByIdAnonymous(IdInput input);
        Task Sincronizar();
        Task<ListResultDto<StringDto>> GetListPosicoes(IdInput input);
        Task<ListResultDto<StringDto>> GetTiposDeWidgetSuportados(IdInput input);
        Task<ListResultDto<StringDto>> GetTiposDePaginasSuportadas(IdInput input);
        Task<ListResultDto<StringDto>> GetTiposDePaginasSuportadasBySite(IdInput input);
        Task<TemplateInput> GetByExternalId(IdInput input);
    }
}
