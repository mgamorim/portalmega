﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IItemDeMenuAppService : IApplicationService
    {
        Task<IdInput> Save(ItemDeMenuInput input);

        Task<ItemDeMenuInput> SaveAndReturnEntity(ItemDeMenuInput input);

        Task<PagedResultDto<ItemDeMenuListDto>> GetItensDeMenuPaginado(GetItemDeMenuInput input);

        Task<ItemDeMenuInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ListResultDto<ItemDeMenuListDto>> GetItemDeMenuExceptForId(GetItemDeMenuExceptForInput input);

        Task<ItemDeMenuInput> GetByExternalId(IdInput input);
    }
}
