﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Site;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface ISiteAppService : IApplicationService
    {
        Task<IdInput> Save(SiteInput input);

        Task<SiteInput> SaveAndReturnEntity(SiteInput input);

        Task<PagedResultDto<SiteListDto>> GetSitesPaginado(GetSiteInput input);

        Task<SiteInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<SiteInput> GetByExternalId(IdInput input);

        Task<SiteInput> GetByHost(GetSiteInput input);
    }
}