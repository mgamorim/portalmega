﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Host;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface IHostAppService : IApplicationService
    {
        Task<IdInput> Save(HostInput input);

        Task<HostInput> SaveAndReturnEntity(HostInput input);

        Task<PagedResultDto<HostListDto>> GetPaginado(GetHostInput input);

        Task<HostInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<HostInput> GetByExternalId(IdInput input);
    }
}