﻿using System.Threading.Tasks;
using Abp.Application.Services;
using EZ.EZControl.Dto.CMS.Geral.Conteudo;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IConteudoAppService : IApplicationService
    {
        Task<ConteudoInput> Get(GetConteudoByUrlInput input);
    }
}