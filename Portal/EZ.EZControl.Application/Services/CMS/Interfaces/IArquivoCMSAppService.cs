﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Arquivo;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IArquivoCMSAppService : IApplicationService
    {
        Task<IdInput> Save(ArquivoCMSInput input);

        Task<ArquivoCMSInput> SaveAndReturnEntity(ArquivoCMSInput input);

        Task<PagedResultDto<ArquivoCMSListDto>> GetArquivosPaginado(GetArquivoCMSInput input);

        Task<ArquivoCMSInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ArquivoCMSInput> GetByExternalId(IdInput input);
    }
}
