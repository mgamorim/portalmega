﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using System.Threading.Tasks;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IMenuAppService : IApplicationService
    {
        Task<IdInput> Save(MenuInput input);

        Task<MenuInput> SaveAndReturnEntity(MenuInput input);

        Task<PagedResultDto<MenuListDto>> GetMenusPaginado(GetMenuInput input);

        Task<MenuInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<ListResultDto<ItemDeMenuListDto>> GetItensDeMenu(IdInput input);

        Task<MenuInput> GetByExternalId(IdInput input);
    }
}
