﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Widget;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Interfaces
{
    public interface IWidgetAppService : IApplicationService
    {
        Task<PagedResultDto<WidgetListDto>> GetPaginado(GetWidgetInput input);
        Task<WidgetInput> GetById(IdInput input);
        Task<IdInput> Save(WidgetInput input);
        Task Delete(IdInput input);
        Task<WidgetInput> GetByExternalId(IdInput input);
    }
}
