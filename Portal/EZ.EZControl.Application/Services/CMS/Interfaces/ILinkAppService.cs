﻿using Abp.Application.Services;
using EZ.EZControl.Dto.CMS.Geral.Link;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface ILinkAppService : IApplicationService
    {
        Task<LinkInput> GetLink(LinkInput input);
    }
}