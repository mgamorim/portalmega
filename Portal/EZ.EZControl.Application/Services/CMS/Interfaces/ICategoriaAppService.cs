﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral.Interfaces
{
    public interface ICategoriaAppService : IApplicationService
    {
        Task<IdInput> Save(CategoriaInput input);

        Task<CategoriaInput> SaveAndReturnEntity(CategoriaInput input);

        Task<PagedResultDto<CategoriaListDto>> GetCategoriasPaginado(GetCategoriaInput input);

        Task<PagedResultDto<CategoriaListDto>> GetCategoriasExceptForId(GetCategoriaExceptForIdInput input);

        Task<CategoriaInput> GetById(IdInput input);

        Task Delete(IdInput input);

        Task<CategoriaInput> GetByExternalId(IdInput input);
    }
}