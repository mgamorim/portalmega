﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Link;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System.Threading.Tasks;
using System.Linq;
using System;
using Abp.UI;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using Abp.AutoMapper;
using System.Data.Entity;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize]
    public class LinkAppService : EZControlAppServiceBase<Site>, ILinkAppService
    {
        private readonly ISiteService _siteService;
        private readonly IPostService _postService;
        private readonly IPaginaService _paginaService;
        private readonly ICategoriaService _categoriaService;
        private readonly ICategoriaAppService _categoriaAppService;
        private readonly ILinkService _linkService;
        public LinkAppService(IRepository<Site> siteRepository,
            ISiteService siteService,
            IPostService postService,
            IPaginaService paginaService,
            ICategoriaService categoriaService,
            ICategoriaAppService categoriaAppService,
            ILinkService linkService)
            : base(siteRepository)
        {
            _siteService = siteService;
            _postService = postService;
            _paginaService = paginaService;
            _categoriaService = categoriaService;
            _categoriaAppService = categoriaAppService;
            _linkService = linkService;
        }

        public async Task<LinkInput> GetLink(LinkInput input)
        {
            try
            {
                var site = await _siteService.GetAll().Where(x => x.Id == input.SiteId).FirstOrDefaultAsync();
                var linkStr = "http://{0}/{1}";
                switch (input.TipoDeLink)
                {
                    case TipoDeLinkEnum.Pagina:
                        {
                            input.Link = _linkService.GetLink(input.TipoDeLink, site, linkStr, null, input.Slug);
                            break;
                        }
                    case TipoDeLinkEnum.Post:
                        {
                            Categoria primeiraCategoriaPost = null;
                            if (input.PrimeiraCategoria == null)
                            {
                                primeiraCategoriaPost = _categoriaService.GetAll().Where(x => x.Slug == "sem-categoria").FirstOrDefault();
                                if (primeiraCategoriaPost == null)
                                {
                                    var categoriaInput = new CategoriaInput() {
                                        Nome = "Sem Categoria",
                                        Slug = "sem-categoria",
                                        SiteId = input.SiteId
                                    };
                                    var novaCategoria = await _categoriaAppService.SaveAndReturnEntity(categoriaInput);
                                    primeiraCategoriaPost = novaCategoria.MapTo<Categoria>();
                                }
                            }else
                            {
                                primeiraCategoriaPost = await _categoriaService.GetById(input.PrimeiraCategoria.Id);
                            }
                            input.Link = _linkService.GetLink(input.TipoDeLink, site, linkStr, primeiraCategoriaPost, input.Slug);
                            break;
                        }
                    case TipoDeLinkEnum.Categoria:
                        {
                            Categoria categoria = null;
                            if (!string.IsNullOrEmpty(input.Titulo) && !string.IsNullOrEmpty(input.Slug))
                            {
                                categoria = new Categoria() {
                                    Nome = input.Titulo,
                                    Slug = input.Slug
                                };
                            }

                            if (input.CategoriaPaiId > 0)
                                categoria.CategoriaPai = await _categoriaService.GetById(input.CategoriaPaiId);

                            input.Link = _linkService.GetLink(input.TipoDeLink, site, linkStr, categoria, input.Slug);
                            break;
                        }
                }
                return input;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}