﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Categoria, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class CategoriaAppService : EZControlAppServiceBase<Categoria>, ICategoriaAppService
    {
        private readonly ICategoriaService _categoriaService;
        private readonly ISiteService _siteService;

        public CategoriaAppService(IRepository<Categoria> categoriaRepository, ICategoriaService service, ISiteService siteService)
            : base(categoriaRepository)
        {
            _categoriaService = service;
            _siteService = siteService;
        }

        private void ValidateInput(CategoriaInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Categoria.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Slug))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Categoria.EmptySlugError"), input.Slug), new[] { "slug".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Categoria.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Categoria Categoria, CategoriaInput CategoriaInput)
        {
            if (CategoriaInput.CategoriaPaiId > 0)
            {
                try
                {
                    Categoria.CategoriaPai = await _categoriaService.GetAll().Where(x => x.Id == CategoriaInput.CategoriaPaiId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Categoria.NotFoundError"), ex.Message);
                }
            }

            if (CategoriaInput.SiteId > 0)
            {
                try
                {
                    Categoria.Site = await _siteService.GetAll().Where(x => x.Id == CategoriaInput.SiteId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Categoria.NotFoundSiteError"), ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Categoria_Create, AppPermissions.Pages_Tenant_CMS_Categoria_Edit)]
        public async Task<IdInput> Save(CategoriaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Categoria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, CategoriaInput>(_categoriaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Categoria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, CategoriaInput>(_categoriaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Categoria_Create, AppPermissions.Pages_Tenant_CMS_Categoria_Edit)]
        public async Task<CategoriaInput> SaveAndReturnEntity(CategoriaInput input)
        {
            CategoriaInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Categoria_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<CategoriaInput, CategoriaInput>(_categoriaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Categoria_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<CategoriaInput, CategoriaInput>(_categoriaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Categoria_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_categoriaService, input);
        }

        public async Task<PagedResultDto<CategoriaListDto>> GetCategoriasPaginado(GetCategoriaInput input)
        {
            var condicoes = new List<WhereIfCondition<Categoria>>
            {
                new WhereIfCondition<Categoria>(
                        !input.Nome.IsNullOrEmpty() ||
                        !input.NomeSite.IsNullOrEmpty(),
                            i =>
                        i.Nome.Contains(input.Nome) ||
                        (i.Site != null && i.Site.Nome.Contains(input.NomeSite)))
            };
            var result = await base.GetListPaged<CategoriaListDto, GetCategoriaInput>(_categoriaService, input, condicoes);
            return result;
        }

        public async Task<CategoriaInput> GetById(IdInput input)
        {
            return await base.GetEntityById<CategoriaInput, IdInput>(_categoriaService, input);
        }

        public async Task<CategoriaInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<CategoriaInput, IdInput>(_categoriaService, input);
        }

        public async Task<PagedResultDto<CategoriaListDto>> GetCategoriasExceptForId(GetCategoriaExceptForIdInput input)
        {
            var query = _categoriaService
                .GetAll()
                .Where(x => x.Id != input.CategoriaId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Nome), x => x.Descricao.Contains(input.Nome));

            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<CategoriaListDto>>();

            return new PagedResultDto<CategoriaListDto>(count, listDtos);
        }
    }
}