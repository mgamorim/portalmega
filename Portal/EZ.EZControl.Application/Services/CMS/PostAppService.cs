﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Post;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Post, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PostAppService : EZControlAppServiceBase<Post>, IPostAppService
    {
        private readonly IPostService _postService;
        private readonly ICategoriaService _categoriaService;
        private readonly ITagService _tagService;
        private readonly ISiteService _siteService;

        public PostAppService(IRepository<Post> postRepository,
            IPostService postService,
            ICategoriaService categoriaService,
            ITagService tagService,
            ISiteService siteService)
            : base(postRepository)
        {
            _siteService = siteService;
            _postService = postService;
            _categoriaService = categoriaService;
            _tagService = tagService;
        }

        protected virtual void ValidateInput(PostInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Titulo))
            {
                validationErrors.Add(new ValidationResult(L("Post.TituloEmptyError")));
            }

            if (string.IsNullOrEmpty(input.Slug))
            {
                validationErrors.Add(new ValidationResult(L("Post.SlugEmptyError")));
            }

            if (input.SiteId <= 0)
            {
                validationErrors.Add(new ValidationResult(L("Post.SiteNullError")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        protected virtual async Task ProcessInput(Post Post, PostInput input)
        {
            if (input.SiteId > 0)
            {
                try
                {
                    Post.Site = await _siteService.GetAll().Where(x => x.Id == input.SiteId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Post.SiteNotFoundError"), ex.Message);
                }
            }

            #region Manipulando Histórico de Edição

            try
            {
                if (Post.Historico == null) Post.Historico = new List<HistoricoPublicacao>();
                Post.Historico.Add(new HistoricoPublicacao
                {
                    Data = input.DataDePublicacao,
                    Status = input.StatusDaPublicacaoEnum,
                    IsActive = true,
                    Publicacao = Post
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            #endregion Manipulando Histórico de Edição

            #region Manipular Categorias

            if (Post.Categorias != null) Post.Categorias.Clear();
            foreach (var categoria in input.Categorias)
            {
                categoria.Slug = categoria.Nome.ToLower().Slugify(Categoria.SlugMaxLength);
                var cat = categoria.Id > 0 ? await _categoriaService.GetAll().Where(x => x.Id == categoria.Id).FirstOrDefaultAsync() : new Categoria();
                categoria.MapTo(cat);
                cat.Site = await _siteService.GetAll().Where(x => x.Id == categoria.SiteId).FirstOrDefaultAsync();

                // A lista de categorias estava sendo instanciada pelo AutoMapper.
                // Desta forma, a gente estava perdendo o controle de manipular as listas do Post.
                // Portando é necessário criar aqui, ou talvez no constructor da classe instanciar essas listas.
                if (Post.Categorias == null)
                    Post.Categorias = new List<Categoria>();

                // No caso do usuario adicionar mais de uma categoria na inserção do Post, esse if só ira entrar apenas uma unica vez,
                // porque os ids das categorias sao todos 0.
                // Para funcionar deixei comentado a linha abaixo.
                //if (!Post.Categorias.Any(x => x.Id == cat.Id))

                if (cat.Id == 0 || !Post.Categorias.Any(x => x.Id == cat.Id))
                    Post.Categorias.Add(cat);
            }

            #endregion Manipular Categorias

            #region Manipular Tags

            if (Post.Tags != null) Post.Tags.Clear();
            foreach (var tag in input.Tags)
            {
                tag.Slug = tag.Nome.ToLower().Slugify(Tag.SlugMaxLength);
                var tg = tag.Id > 0 ? await _tagService.GetAll().Where(x => x.Id == tag.Id).FirstOrDefaultAsync() : new Tag();
                tag.MapTo(tg);

                // A lista de tag estava sendo instanciada pelo AutoMapper.
                // Desta forma, a gente estava perdendo o controle de manipular as listas do Post.
                // Portando é necessário criar aqui, ou talvez no constructor da classe instanciar essas listas.
                if (Post.Tags == null)
                    Post.Tags = new List<Tag>();

                // No caso do usuario adicionar mais de uma tag na inserção do Post, esse if só ira entrar apenas uma unica vez,
                // porque os ids das categorias sao todos 0.
                // Para funcionar deixei comentado a linha abaixo.
                //if (!Post.Tags.Any(x => x.Id == tg.Id))

                if (tg.Id == 0 || !Post.Tags.Any(x => x.Id == tg.Id))
                    Post.Tags.Add(tg);
            }

            #endregion Manipular Tags
        }

        [UnitOfWork(IsDisabled = true)]
        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Post_Create, AppPermissions.Pages_Tenant_CMS_Post_Edit)]
        public async Task<IdInput> Save(PostInput input)
        {
            using (var trans = this.UnitOfWorkManager.Begin())
            {
                try
                {
                    IdInput categoriaId;

                    if (input.Id == default(int))
                    {
                        categoriaId = await base.CreateEntity<IdInput, PostInput>(_postService, input, () => ValidateInput(input), ProcessInput);
                    }
                    else
                    {
                        categoriaId = await base.UpdateEntity<IdInput, PostInput>(_postService, input, () => ValidateInput(input), ProcessInput);
                    }
                    trans.Complete();
                    return categoriaId;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Post_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_postService, input);
        }

        public async Task<PagedResultDto<PostListDto>> GetPostsPaginado(GetPostInput input)
        {
            var condicoes = new List<WhereIfCondition<Post>>
            {
                 new WhereIfCondition<Post>(
                    !string.IsNullOrEmpty(input.Titulo),
                        a =>
                            a.Titulo.Contains(input.Titulo))
            };
            var result = await base.GetListPaged<PostListDto, GetPostInput>(_postService, input, condicoes);
            return result;
        }

        public async Task<PostInput> GetById(IdInput input)
        {
            var Post = await _postService.GetAll().Where(x => x.Id == input.Id).FirstOrDefaultAsync();
            var dto = Post.MapTo<PostInput>();
            var historico = Post.Historico.OrderByDescending(x => x.Data).FirstOrDefault();
            dto.DataDePublicacao = historico.Data;
            dto.StatusDaPublicacaoEnum = historico.Status;
            return dto;
        }

        public async Task<PostInput> GetByExternalId(IdInput input)
        {
            var Post = await _postService.GetAll().Where(x => x.ExternalId == input.Id).FirstOrDefaultAsync();
            var dto = Post.MapTo<PostInput>();
            var historico = Post.Historico.OrderByDescending(x => x.Data).FirstOrDefault();
            dto.DataDePublicacao = historico.Data;
            dto.StatusDaPublicacaoEnum = historico.Status;
            return dto;
        }
    }
}