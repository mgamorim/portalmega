﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Link;
using EZ.EZControl.Dto.CMS.Geral.Publicacao;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize]
    public class PublicacaoBaseAppService : EZControlAppServiceBase<PublicacaoBase>, IPublicacaoBaseAppService
    {
        private readonly IPublicacaoBaseService _publicacaoBaseService;
        private readonly ISiteService _siteService;
        private readonly ILinkService _linkService;
        private readonly IPostService _postService;
        private readonly ICategoriaService _categoriaService;
        public PublicacaoBaseAppService(
            IRepository<PublicacaoBase> publicacaoBaseRepository,
            IPublicacaoBaseService publicacaoBaseService,
            ISiteService siteService,
            ILinkService linkService,
            IPostService postService,
            ICategoriaService categoriaService)
            : base(publicacaoBaseRepository)
        {
            _publicacaoBaseService = publicacaoBaseService;
            _siteService = siteService;
            _linkService = linkService;
            _postService = postService;
        }

        //TODO: Esse método não está no lugar correto. Definir o AppService correto. Existe por padrão o usuário visitante que vai poder pesquisa a partir do site, sendo assim, esta método deve estar num local apropriado para usuários que acessarão pelo CMS.FrontEnd
        public async Task<PagedResultDto<PesquisaPublicacaoListDto>> Pesquisar(PesquisaPublicacaoDto input)
        {
            //Busco o site passado na pesquisa
            var site = await _siteService.GetById(input.SiteId);
            //Valido a possibilidade de realizar a pesquisa em Posts e Páginas, de acordo com a configuração do site e com os parâmetros da pesquisa
            bool pesquisarPosts = site.HabilitarPesquisaPost && input.IncluiPosts;
            bool pesquisarPaginas = site.HabilitarPesquisaPagina && input.IncluiPaginas;
            //Codifico o termpo de busca para transformar os termos especiais como acentuação, etc em códigos HTML para buscar no conteúdo
            var termoHtml = input.TermpoPesquisa.EZHtmlDecode();
            //Monto as condições de busca, sempre buscando o termo pesquisado no Título e no Conteúdo das Publicações
            var query = _publicacaoBaseService.GetAll()
                .Where(x => x.Titulo.ToLower().Contains(input.TermpoPesquisa.ToLower()) || x.Conteudo.ToLower().Contains(termoHtml.ToLower()));
            if (pesquisarPaginas && pesquisarPosts)
            {
                query = query.Where(x => x.TipoDePublicacao == TipoDePublicacaoEnum.Pagina || x.TipoDePublicacao == TipoDePublicacaoEnum.Post);
            }
            else if (pesquisarPaginas && !pesquisarPosts)
            {
                query = query.Where(x => x.TipoDePublicacao == TipoDePublicacaoEnum.Pagina);
            }
            else if (!pesquisarPaginas && pesquisarPosts)
            {
                query = query.Where(x => x.TipoDePublicacao == TipoDePublicacaoEnum.Post);
            }
            else
            {
                query = query.Where(x => x.TipoDePublicacao != TipoDePublicacaoEnum.Post && x.TipoDePublicacao != TipoDePublicacaoEnum.Pagina);
            }
            var count = await query.CountAsync();
            var dados = await query
                .OrderBy(x => x.Titulo)
                .PageBy(input)
                .ToListAsync();
            var listDtos = (
                from d in dados
                select new PesquisaPublicacaoListDto()
                {
                    Categoria = GetStringPrimeiraCategoria(d).Result,
                    DataDePublicacao = d.Historico.OrderByDescending(x => x.Data).FirstOrDefault().Data,
                    Publicacao = d.Titulo,
                    Resumo = MontaResumo(d.Conteudo, input.TermpoPesquisa),
                    Site = d.Site.Nome,
                    Url = MontaLink(d).Result
                }
            ).ToList();
            return new PagedResultDto<PesquisaPublicacaoListDto>(count, listDtos);
        }

        private string MontaResumo(string conteudo, string termoPesquisa)
        {
            var retorno = string.Empty;
            var paragrafos = conteudo.Split(new string[] { ". " }, StringSplitOptions.None);
            for (var i = 0; i < paragrafos.Length; i++)
            {
                if (paragrafos[i].Contains(termoPesquisa))
                {
                    if (i > 0 && i < paragrafos.Length - 1)
                    {
                        retorno = string.Format("[{0}]{1}[{2}]", "...", paragrafos[i], "...");
                    }
                    else if (i == 0 && paragrafos.Length > 1)
                    {
                        retorno = string.Format("{0}[{1}]", paragrafos[i], "...");
                    }
                    else if (i == 0)
                    {
                        retorno = string.Format("{0}", paragrafos[i]);
                    }
                    else
                    {
                        retorno = string.Format("[{0}]{1}", "...", paragrafos[i]);
                    }
                }
            }
            return retorno;
        }
        private async Task<string> GetStringPrimeiraCategoria(PublicacaoBase publicacao)
        {
            if (publicacao.TipoDePublicacao == TipoDePublicacaoEnum.Pagina) return string.Empty;
            var post = await _postService.GetById(publicacao.Id);
            if (post.Categorias.Any())
            {
                return post.Categorias.First().Nome;
            }
            else
            {
                return string.Empty;
            }
        }
        private async Task<string> MontaLink(PublicacaoBase publicacao)
        {
            Categoria categoriaPublicacao = null;
            if (publicacao.TipoDePublicacao == TipoDePublicacaoEnum.Post)
            {
                var post = await _postService.GetById(publicacao.Id);
                if (post.Categorias.Any())
                    categoriaPublicacao = post.Categorias.FirstOrDefault();
            }
            var tipoDeLink = publicacao.TipoDePublicacao == TipoDePublicacaoEnum.Pagina ? TipoDeLinkEnum.Pagina : TipoDeLinkEnum.Post;
            return _linkService.GetLink(tipoDeLink, publicacao.Site, "http://{0}/{1}", categoriaPublicacao, publicacao.Slug);
        }
    }
}