﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Tag, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class TagAppService : EZControlAppServiceBase<Tag>, ITagAppService
    {
        private readonly ITagService _service;

        public TagAppService(IRepository<Tag> TagRepository, ITagService Service)
            : base(TagRepository)
        {
            this._service = Service;
        }

        private void ValidateInput(TagInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Tag.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Tag.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Tag_Edit)]
        public async Task<IdInput> Save(TagInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                result = await base.CreateEntity<IdInput, TagInput>(_service, input, () => ValidateInput(input));
            }
            else
            {
                result = await base.UpdateEntity<IdInput, TagInput>(_service, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Tag_Edit)]
        public async Task<TagInput> SaveAndReturnEntity(TagInput input)
        {
            TagInput result = null;

            if (input.Id == default(int))
            {
                result = await base.CreateAndReturnEntity<TagInput, TagInput>(_service, input, () => ValidateInput(input));
            }
            else
            {
                result = await base.UpdateAndReturnEntity<TagInput, TagInput>(_service, input, () => ValidateInput(input));
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Tag_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_service, input);
        }

        public async Task<PagedResultDto<TagListDto>> GetTagsPaginado(GetTagInput input)
        {
            var condicoes = new List<WhereIfCondition<Tag>>
            {
                new WhereIfCondition<Tag>(!input.Nome.IsNullOrEmpty(), i => i.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<TagListDto, GetTagInput>(_service, input, condicoes);
            return result;
        }

        public async Task<TagInput> GetById(IdInput input)
        {
            return await base.GetEntityById<TagInput, IdInput>(_service, input);
        }

        public async Task<TagInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<TagInput, IdInput>(_service, input);
        }

        public async Task<ListResultDto<TagListDto>> GetTagsExceptForId(GetTagExceptForIdInput input)
        {
            var itens = await Repository
                .GetAll()
                .Where(x => x.Id != input.TagId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Nome), x => x.Descricao.Contains(input.Nome))
                .OrderBy(x => x.Nome)
                .ToListAsync();
            return new ListResultDto<TagListDto>(itens.MapTo<List<TagListDto>>());
        }

    }
}
