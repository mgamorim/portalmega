﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Runtime.Validation;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Parametro;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Parametros, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ParametroCMSAppService : EZControlAppServiceBase<ParametroCMS>, IParametroCMSAppService
    {
        private readonly IParametroCMSService parametroCMSService;

        public ParametroCMSAppService(IRepository<ParametroCMS> parametroRepository,
            IParametroCMSService parametroCMSService)
            : base(parametroRepository)
        {
            this.parametroCMSService = parametroCMSService;
        }

        private void ValidateInput(ParametroCMSInput input)
        {
            var validationErrors = new List<ValidationResult>();

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        public async Task<IdInput> Save(ParametroCMSInput input)
        {
            var parametro = await Get();
            if (parametro != null)
                input.Id = parametro.Id;

            IdInput savedEntity;

            if (input.Id == default(int))
            {
                savedEntity = await base.CreateEntity<IdInput, ParametroCMSInput>(parametroCMSService, input, () => ValidateInput(input), null);
            } else
            {
                savedEntity = await base.UpdateEntity<IdInput, ParametroCMSInput>(parametroCMSService, input, () => ValidateInput(input), null);
            }

            return savedEntity;
        }

        public async Task<ParametroCMSInput> Get()
        {
            var condicoes = new List<WhereIfCondition<ParametroCMS>>();
            condicoes.Add(new WhereIfCondition<ParametroCMS>(true, x => x.Id > 0));
            var parametros = await base.GetList<ParametroCMSInput>(parametroCMSService, condicoes, x => x.Id);

            return parametros.Items.FirstOrDefault();
        }
    }
}