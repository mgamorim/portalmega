﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_ItemDeMenu, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class ItemDeMenuAppService : EZControlAppServiceBase<ItemDeMenu>, IItemDeMenuAppService
    {
        private readonly IItemDeMenuService _service;
        private readonly IMenuService _menuService;
        private readonly ICategoriaService _categoriaService;
        private readonly IPaginaService _paginaService;
        public ItemDeMenuAppService(IRepository<ItemDeMenu> repository,
                                    IItemDeMenuService service,
                                    IMenuService menuService,
                                    ICategoriaService categoriaService,
                                    IPaginaService paginaService)
            : base(repository)
        {
            _service = service;
            _menuService = menuService;
            _categoriaService = categoriaService;
            _paginaService = paginaService;
        }

        private void ValidateInput(ItemDeMenuInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Titulo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("ItemDeMenu.EmptyTituloError"), input.Titulo), new[] { "titulo".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.DescricaoDoTitulo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("ItemDeMenu.EmptyDescricaoDoTituloError"), input.DescricaoDoTitulo), new[] { "descricaoDoTitulo".ToCamelCase() }));
            }

            if (string.IsNullOrEmpty(input.Url))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("ItemDeMenu.EmptyUrlError"), input.Url), new[] { "url".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Titulo == input.Titulo))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("ItemDeMenu.DuplicateTituloError"), input.Titulo), new[] { "titulo".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(ItemDeMenu itemDeMenu, ItemDeMenuInput itemDeMenuInput)
        {
            if (itemDeMenuInput.MenuId > 0)
            {
                try
                {
                    itemDeMenu.Menu = await _menuService.GetAll().Where(x => x.Id == itemDeMenuInput.MenuId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("ItemDeMenu.NotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("ItemDeMenu.MenuNullError"));
            }

            if (itemDeMenuInput.ItemMenuId.HasValue && itemDeMenuInput.ItemMenuId > 0)
            {
                try
                {
                    itemDeMenu.ItemMenu = await Repository.GetAsync(itemDeMenuInput.ItemMenuId.Value);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("ItemDeMenu.NotFoundError"), ex.Message);
                }
            }
            if (itemDeMenuInput.CategoriaId.HasValue && itemDeMenuInput.CategoriaId > 0)
            {
                try
                {
                    itemDeMenu.Categoria = await _categoriaService.GetAll().Where(x => x.Id == itemDeMenuInput.CategoriaId.Value).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("ItemDeMenu.NotFoundCategoriaError"), ex.Message);
                }
            }
            if (itemDeMenuInput.PaginaId.HasValue && itemDeMenuInput.PaginaId > 0)
            {
                try
                {
                    itemDeMenu.Pagina = await _paginaService.GetAll().Where(x => x.Id == itemDeMenuInput.PaginaId.Value).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("ItemDeMenu.NotFoundError"), ex.Message);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Create, AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Edit)]
        public async Task<IdInput> Save(ItemDeMenuInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, ItemDeMenuInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, ItemDeMenuInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Create, AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Edit)]
        public async Task<ItemDeMenuInput> SaveAndReturnEntity(ItemDeMenuInput input)
        {
            ItemDeMenuInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<ItemDeMenuInput, ItemDeMenuInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<ItemDeMenuInput, ItemDeMenuInput>(_service, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_ItemDeMenu_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_service, input);
        }

        public async Task<PagedResultDto<ItemDeMenuListDto>> GetItensDeMenuPaginado(GetItemDeMenuInput input)
        {
            var condicoes = new List<WhereIfCondition<ItemDeMenu>>
            {
                new WhereIfCondition<ItemDeMenu>(
                        !string.IsNullOrEmpty(input.Titulo) ||
                        !string.IsNullOrEmpty(input.DescricaoDoTitulo) ||
                        !string.IsNullOrEmpty(input.NomeMenu) ||
                        !string.IsNullOrEmpty(input.Url),
                            a =>
                        a.Titulo.Contains(input.Titulo) ||
                        a.DescricaoDoTitulo.Contains(input.DescricaoDoTitulo) ||
                        (a.Menu != null && a.Menu.Nome.Contains(input.NomeMenu)) ||
                        a.Url.Contains(input.Url))
            };
            var result = await base.GetListPaged<ItemDeMenuListDto, GetItemDeMenuInput>(_service, input, condicoes);
            return result;
        }

        public async Task<ListResultDto<ItemDeMenuListDto>> GetItemDeMenuExceptForId(GetItemDeMenuExceptForInput input)
        {
            var query = await Repository
               .GetAll()
               .Where(x => x.Id != input.Id)
               .WhereIf(!string.IsNullOrWhiteSpace(input.Titulo), x => x.Titulo.Contains(input.Titulo))
               .OrderBy(x => x.Titulo)
               .ToListAsync();

            return new ListResultDto<ItemDeMenuListDto>(query.MapTo<List<ItemDeMenuListDto>>());
        }

        public async Task<ItemDeMenuInput> GetById(IdInput input)
        {
            return await base.GetEntityById<ItemDeMenuInput, IdInput>(_service, input);
        }

        public async Task<ItemDeMenuInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<ItemDeMenuInput, IdInput>(_service, input);
        }
    }
}
