﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Dto.CMS.Geral.Conteudo;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Services.CMS
{
    public class ConteudoAppService : EZControlAppServiceBase, IConteudoAppService
    {
        private readonly IConteudoService _conteudoService;

        public ConteudoAppService(IConteudoService conteudoService)
        {
            _conteudoService = conteudoService;
        }
        
        public async Task<ConteudoInput> Get(GetConteudoByUrlInput input)
        {
            var conteudo = await _conteudoService.FirstOrDefaultAsync(input.Url, input.NumeroDaPagina, input.ItensPorPagina);

            if (conteudo == null)
                throw new UserFriendlyException(404, "Página não encontrada.");

            return conteudo.MapTo<ConteudoInput>();
        }
    }
}