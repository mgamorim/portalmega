﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Pagina;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using EZ.EZControl.Filters;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Pagina, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class PaginaAppService : EZControlAppServiceBase<Pagina>, IPaginaAppService
    {
        private readonly IPaginaService _paginaService;
        private readonly ISiteService _siteService;

        public PaginaAppService(IRepository<Pagina> paginaRepository,
            IPaginaService paginaService,
            ISiteService siteService)
            : base(paginaRepository)
        {
            _siteService = siteService;
            _paginaService = paginaService;
        }

        protected virtual void ValidateInput(PaginaInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Titulo))
            {
                validationErrors.Add(new ValidationResult(L("Pagina.TituloEmptyError")));
            }

            if (string.IsNullOrEmpty(input.Slug))
            {
                validationErrors.Add(new ValidationResult(L("Pagina.SlugEmptyError")));
            }

            if (input.SiteId <= 0)
            {
                validationErrors.Add(new ValidationResult(L("Pagina.SiteNullError")));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException("Erro de validação", validationErrors);
            }
        }

        protected virtual async Task ProcessInput(Pagina Pagina, PaginaInput input)
        {
            if (input.SiteId > 0)
            {
                try
                {
                    Pagina.Site = await _siteService.GetAll().Where(x => x.Id == input.SiteId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Pagina.SiteNotFoundError"), ex.Message);
                }
            }

            #region Manipulando Histórico de Edição

            try
            {
                if (Pagina.Historico == null) Pagina.Historico = new List<HistoricoPublicacao>();
                Pagina.Historico.Add(new HistoricoPublicacao
                {
                    Data = input.DataDePublicacao,
                    Status = input.StatusDaPublicacaoEnum,
                    IsActive = true,
                    Publicacao = Pagina
                });
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }

            #endregion Manipulando Histórico de Edição

        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Pagina_Create, AppPermissions.Pages_Tenant_CMS_Pagina_Edit)]
        public async Task<IdInput> Save(PaginaInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Pagina_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, PaginaInput>(_paginaService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Pagina_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, PaginaInput>(_paginaService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Pagina_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_paginaService, input);
        }

        public async Task<PagedResultDto<PaginaListDto>> GetPaginasPaginado(GetPaginaInput input)
        {
            var condicoes = new List<WhereIfCondition<Pagina>>
            {
                 new WhereIfCondition<Pagina>(
                    !string.IsNullOrEmpty(input.Titulo),
                        a =>
                            a.Titulo.Contains(input.Titulo))
            };
            var result = await base.GetListPaged<PaginaListDto, GetPaginaInput>(_paginaService, input, condicoes);
            return result;
        }

        public async Task<PagedResultDto<PaginaListDto>> GetPaginaExceptForId(GetPaginaExceptForInput input)
        {
            var query = _paginaService
                .GetAll()
                .Where(x => x.Id != input.SiteId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.Titulo), x => x.Titulo.Contains(input.Titulo));

            var count = await query.CountAsync();
            var dados = await query.OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var listDtos = dados.MapTo<List<PaginaListDto>>();

            return new PagedResultDto<PaginaListDto>(count, listDtos);
        }

        public async Task<PaginaInput> GetById(IdInput input)
        {
            var Pagina = await _paginaService.GetById(input.Id);
            var dto = Pagina.MapTo<PaginaInput>();
            var historico = Pagina.Historico.OrderByDescending(x => x.Data).FirstOrDefault();
            dto.DataDePublicacao = historico.Data;
            dto.StatusDaPublicacaoEnum = historico.Status;
            return dto;
        }

        [AbpAllowAnonymous]
        public async Task<PaginaInput> GetBySlug(GetPaginaBySlugInput input)
        {
            #region ProcessInput

            if (input.SiteId == 0)
            {
                throw new UserFriendlyException(L("Pagina.SiteNullError"));
            }

            if (string.IsNullOrWhiteSpace(input.Slug))
            {
                throw new UserFriendlyException(L("Pagina.SlugNullError"));
            }

            #endregion

            using (CurrentUnitOfWork.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var pagina = await _paginaService.FirstOrDefault(x => x.SiteId == input.SiteId && x.Slug == input.Slug);

                if (pagina == null)
                    throw new HttpException(404, L("Pagina.NotFound"));

                var dto = pagina.MapTo<PaginaInput>();
                var historico = pagina.Historico.OrderByDescending(x => x.Data).FirstOrDefault();
                dto.DataDePublicacao = historico.Data;
                dto.StatusDaPublicacaoEnum = historico.Status;
                return dto;
            }
        }

        public async Task<PaginaInput> GetByExternalId(IdInput input)
        {
            var Pagina = await _paginaService.GetByExternalId(input.Id);
            var dto = Pagina.MapTo<PaginaInput>();
            var historico = Pagina.Historico.OrderByDescending(x => x.Data).FirstOrDefault();
            dto.DataDePublicacao = historico.Data;
            dto.StatusDaPublicacaoEnum = historico.Status;
            return dto;
        }
    }
}