﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;

namespace EZ.EZControl.Services.CMS
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Menu, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class MenuAppService : EZControlAppServiceBase<Menu>, IMenuAppService
    {
        private readonly IMenuService _menuService;
        private readonly IRepository<Site> _siteService;
        public MenuAppService(IRepository<Menu> repository, IMenuService menuService, IRepository<Site> siteService)
            : base(repository)
        {
            _menuService = menuService;
            _siteService = siteService;
        }

        private void ValidateInput(MenuInput input)
        {
            // TODO: Criar entradas nos arquivos Resource para traduzir as mensagens de erro abaixo

            var validationErrors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Menu.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (CheckForDuplicateInstance(Repository, input.Id, x => x.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(string.Format(L("Menu.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }

            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        private async Task ProcessInput(Menu menu, MenuInput menuInput)
        {
            if (menuInput.SiteId > 0)
            {
                try
                {
                    menu.Site = await _siteService.GetAsync(menuInput.SiteId);
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Menu.NotFoundError"), ex.Message);
                }
            }
            else
            {
                throw new UserFriendlyException(L("Menu.SiteNullError"));
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Menu_Create, AppPermissions.Pages_Tenant_CMS_Menu_Edit)]
        public async Task<IdInput> Save(MenuInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Menu_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, MenuInput>(_menuService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Menu_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, MenuInput>(_menuService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Menu_Create, AppPermissions.Pages_Tenant_CMS_Menu_Edit)]
        public async Task<MenuInput> SaveAndReturnEntity(MenuInput input)
        {
            MenuInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Menu_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<MenuInput, MenuInput>(_menuService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Menu_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<MenuInput, MenuInput>(_menuService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Menu_Delete)]
        public async Task Delete(IdInput input)
        {
            await base.DeleteEntity(_menuService, input);
        }

        public async Task<PagedResultDto<MenuListDto>> GetMenusPaginado(GetMenuInput input)
        {
            var condicoes = new List<WhereIfCondition<Menu>>
            {
                new WhereIfCondition<Menu>(
                    !string.IsNullOrEmpty(input.Nome) ||
                    !string.IsNullOrEmpty(input.NomeSite) ||
                    input.SiteId > 0,
                        a =>
                    a.Nome.Contains(input.Nome) ||
                    (a.Site != null && (a.Site.Nome.Contains(input.NomeSite) || a.Site.Id == input.SiteId)) ||
                    a.Nome.Contains(input.Nome))
            };
            var result = await base.GetListPaged<MenuListDto, GetMenuInput>(_menuService, input, condicoes);
            return result;
        }

        public async Task<MenuInput> GetById(IdInput input)
        {
            return await base.GetEntityById<MenuInput, IdInput>(_menuService, input);
        }

        public async Task<MenuInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<MenuInput, IdInput>(_menuService, input);
        }

        public async Task<ListResultDto<ItemDeMenuListDto>> GetItensDeMenu(IdInput input)
        {
            var menu = await _menuService.GetById(input.Id);
            return new ListResultDto<ItemDeMenuListDto>(_menuService.GetItensDeMenu(menu).MapTo<List<ItemDeMenuListDto>>());
        }
    }
}
