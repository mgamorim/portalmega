﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Authorization;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Filters;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Services.CMS.Geral
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site, AppPermissions.Pages_Tenant_EZLiv_MeusProdutos, AppPermissions.Pages_Tenant_AcessoLeitura)]
    public class SiteAppService : EZControlAppServiceBase<Site>, ISiteAppService
    {
        private readonly ISiteService _siteService;
        private readonly IHostService _hostService;
        private readonly ITemplateService _templateService;
        public SiteAppService(IRepository<Site> siteRepository, ISiteService SiteService, IHostService HostService, ITemplateService TemplateService)
            : base(siteRepository)
        {
            _siteService = SiteService;
            _hostService = HostService;
            _templateService = TemplateService;
        }

        private void ValidateInput(SiteInput input)
        {
            var validationErrors = new List<ValidationResult>();
            if (string.IsNullOrEmpty(input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.EmptyNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            if (string.IsNullOrEmpty(input.TipoDePaginaParaPaginaDefault))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.EmptyTipoDePaginaParaPaginaDefaultError"), input.TipoDePaginaParaPaginaDefault), new[] { "tipoDePaginaParaPaginaDefault".ToCamelCase() }));
            }
            if (string.IsNullOrEmpty(input.TipoDePaginaParaCategoriaDefault))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.EmptyTipoDePaginaParaCategoriaDefaultError"), input.TipoDePaginaParaCategoriaDefault), new[] { "tipoDePaginaParaCategoriaDefault".ToCamelCase() }));
            }
            if (string.IsNullOrEmpty(input.TipoDePaginaParaPaginaInicialDefault))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.EmptyTipoDePaginaParaPaginaInicialDefaultError"), input.TipoDePaginaParaPaginaInicialDefault), new[] { "tipoDePaginaParaPaginaInicialDefault".ToCamelCase() }));
            }
            if (string.IsNullOrEmpty(input.TipoDePaginaParaPostDefault))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.EmptyTipoDePaginaParaPostDefaultError"), input.TipoDePaginaParaPostDefault), new[] { "tipoDePaginaParaPostDefault".ToCamelCase() }));
            }
            if (input.TemplateDefaultId == 0)
            {
                validationErrors.Add(new ValidationResult(L("Site.EmptyTemplateDefaultError")));
            }
            if (input.Hosts.Count == 0)
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.EmptyHostError"), input.Hosts), new[] { "hosts".ToCamelCase() }));
            }
            if (CheckForDuplicateInstance(Repository, input.Id, i => i.Nome == input.Nome))
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.DuplicateNomeError"), input.Nome), new[] { "nome".ToCamelCase() }));
            }
            foreach (var host in input.Hosts)
            {
                if (CheckForDuplicateInstance(Repository, input.Id, i => i.Hosts.Any(x => x.Url == host.Url)))
                {
                    validationErrors.Add(new ValidationResult(String.Format(L("Site.DuplicateUrlError"), host.Url), new[] { "url".ToCamelCase() }));
                }
            }
            if (input.Hosts.Count(x => x.IsPrincipal) > 1)
            {
                validationErrors.Add(new ValidationResult(String.Format(L("Site.MaisDeUmHostPrincipalError"), input.Hosts), new[] { "hosts".ToCamelCase() }));
            }
            if (validationErrors.Any())
            {
                throw new AbpValidationException(L("ValidationError"), validationErrors);
            }
        }

        protected virtual async Task ProcessInput(Site site, SiteInput input)
        {
            if (input.TemplateDefaultId > 0)
            {
                try
                {
                    site.TemplateDefault = await _templateService.GetAll().Where(x => x.Id == input.TemplateDefaultId).FirstOrDefaultAsync();
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(L("Site.NotFoundTemplateError"), ex.Message);
                }
            }
            #region Manipular Hosts

            var hostsToRemove = new List<int>();

            foreach (var host in site.Hosts)
            {
                if (!input.Hosts.Any(x => x.Url == host.Url))
                {
                    hostsToRemove.Add(host.Id);
                }
            }

            foreach (var host in hostsToRemove)
            {
                await _hostService.Delete(host);
            }

            foreach (HostInput host in input.Hosts)
            {
                var hostExiste = await _hostService.GetAll().Where(x => x.Site.Id == site.Id && x.Url == host.Url).CountAsync() > 0;

                if (!hostExiste)
                {
                    if (site.Hosts == null)
                        site.Hosts = new List<Host>();
                    var addedHost = host.MapTo<Host>();
                    addedHost.Site = site;
                    site.Hosts.Add(addedHost);
                }
                else
                {
                    var updateHost = await _hostService.GetById(host.Id);
                    host.MapTo(updateHost);
                    await _hostService.UpdateEntity(updateHost);
                }
            }

            #endregion Manipular Hosts
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site_Create, AppPermissions.Pages_Tenant_CMS_Site_Edit)]
        [UnitOfWork(IsDisabled = false)]
        public async Task<IdInput> Save(SiteInput input)
        {
            IdInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateEntity<IdInput, SiteInput>(_siteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateEntity<IdInput, SiteInput>(_siteService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site_Create, AppPermissions.Pages_Tenant_CMS_Site_Edit)]
        public async Task<SiteInput> SaveAndReturnEntity(SiteInput input)
        {
            SiteInput result = null;

            if (input.Id == default(int))
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Create))
                    throw new UserFriendlyException(L("PermissionToCreateRecord"));

                result = await base.CreateAndReturnEntity<SiteInput, SiteInput>(_siteService, input, () => ValidateInput(input), ProcessInput);
            }
            else
            {
                if (!PermissionChecker.IsGranted(AppPermissions.Pages_Tenant_CMS_Site_Edit))
                    throw new UserFriendlyException(L("PermissionToUpdateRecord"));

                result = await base.UpdateAndReturnEntity<SiteInput, SiteInput>(_siteService, input, () => ValidateInput(input), ProcessInput);
            }

            return result;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CMS_Site_Delete)]
        public async Task Delete(IdInput input)
        {
            var site = await _siteService.GetById(input.Id);
            var hostsToRemove = new List<int>();
            foreach (var host in site.Hosts)
            {

                hostsToRemove.Add(host.Id);
            }
            foreach (var host in hostsToRemove)
            {
                await _hostService.Delete(host);
            }
            await _siteService.Delete(site.Id);
        }

        public async Task<PagedResultDto<SiteListDto>> GetSitesPaginado(GetSiteInput input)
        {
            var condicoes = new List<WhereIfCondition<Site>>();
            condicoes.Add(new WhereIfCondition<Site>(!input.Nome.IsNullOrEmpty(), i => i.Nome.Contains(input.Nome)));
            var result = await base.GetListPaged<SiteListDto, GetSiteInput>(_siteService, input, condicoes);
            return result;
        }

        public async Task<SiteInput> GetById(IdInput input)
        {
            return await base.GetEntityById<SiteInput, IdInput>(_siteService, input);
        }

        public async Task<SiteInput> GetByExternalId(IdInput input)
        {
            return await base.GetEntityByExternalId<SiteInput, IdInput>(_siteService, input);
        }
        [AbpAllowAnonymous]
        public async Task<SiteInput> GetByHost(GetSiteInput input)
        {
            using (UnitOfWorkManager.Current.DisableFilter(EzControlFilters.EmpresaFilter))
            {
                var dados = await _siteService.GetAll()
                        .Where(x => x.Hosts.Any(y => y.Url == input.Host))
                        .FirstOrDefaultAsync();

                return dados.MapTo<SiteInput>(); 
            }
        }
    }
}