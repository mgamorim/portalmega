﻿using System.Threading.Tasks;
using Abp.Application.Services;
using EZ.EZControl.Sessions.Dto;

namespace EZ.EZControl.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
