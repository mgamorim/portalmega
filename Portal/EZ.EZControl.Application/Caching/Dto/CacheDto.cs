﻿using Abp.Application.Services.Dto;

namespace EZ.EZControl.Caching.Dto
{
    public class CacheDto
    {
        public string Name { get; set; }
    }
}
