﻿using Abp.Notifications;
using EZ.EZControl.Dto;

namespace EZ.EZControl.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}