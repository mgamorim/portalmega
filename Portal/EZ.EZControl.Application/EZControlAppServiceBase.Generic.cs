﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using EZ.EZControl.Common.Filters;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Services.EZLiv.MapToEntity;
using EZ.EZControl.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl
{
    public abstract class EZControlAppServiceBase<TEntity, TPrimaryKey> : EZControlAppServiceBase
        where TEntity : class, IEzEntity<TPrimaryKey>
        where TPrimaryKey : IEquatable<TPrimaryKey>
    {
        public IRepository<TEntity, TPrimaryKey> Repository { get; set; }

        protected EZControlAppServiceBase(IRepository<TEntity, TPrimaryKey> repository)
        {
            this.Repository = repository;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TIdInput">Tipo da entrada</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public virtual async Task DeleteEntity<TIdInput>(IService<TEntity, TPrimaryKey> service, TIdInput input)
            where TIdInput : IdInput<TPrimaryKey>
        {
            Func<IService<TEntity, TPrimaryKey>, TIdInput, Task> deleteEntityFunc = async (serviceFunc, inputFunc) =>
            {
                await serviceFunc.Delete(inputFunc.Id);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                await ApplyEmpresaFilter(async () => await deleteEntityFunc(service, input));
            }
            else
            {
                await deleteEntityFunc(service, input);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult">Tipo do retorno</typeparam>
        /// <param name="service"></param>
        /// <param name="predicates"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public virtual async Task<ListResultDto<TResult>> GetList<TResult>(IService<TEntity, TPrimaryKey> service, IList<WhereIfCondition<TEntity>> predicates, Expression<Func<TEntity, object>> orderBy)
            where TResult : class, IEntityDto<TPrimaryKey>, new()
        {
            Func<IService<TEntity, TPrimaryKey>, IList<WhereIfCondition<TEntity>>, Expression<Func<TEntity, object>>, Task<ListResultDto<TResult>>> getListFunc = async (serviceFunc, predicatesFunc, orderByFunc) =>
            {
                IQueryable<TEntity> query = serviceFunc.GetAll();

                foreach (var item in predicatesFunc)
                {
                    query = query.WhereIf(item.Condition, item.Expression);
                }

                var orderByExpression = orderByFunc;
                var body = orderByExpression.Body;
                // Quando o orderby utilizar tipos como inteiro, ele será convertido (boxed) para object
                if (body.NodeType == ExpressionType.Convert && ((UnaryExpression)body).Type == typeof(object))
                {
                    var operand = ((UnaryExpression)body).Operand;
                    var lambExpression = Expression.Lambda(operand, orderByExpression.Parameters);
                    if (operand.Type == typeof(short))
                        query = query.OrderBy((Expression<Func<TEntity, short>>)lambExpression);
                    else if (operand.Type == typeof(int))
                        query = query.OrderBy((Expression<Func<TEntity, int>>)lambExpression);
                    else if (operand.Type == typeof(long))
                        query = query.OrderBy((Expression<Func<TEntity, long>>)lambExpression);
                }
                else
                    query = query.OrderBy(orderByExpression);

                var list = await query
                    .ToListAsync();

                var listDto = list.MapTo<List<TResult>>();
                return new ListResultDto<TResult>(listDto);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                 {
                     return await getListFunc(service, predicates, orderBy);
                 });
            }

            return await getListFunc(service, predicates, orderBy);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult">Tipo do retorno</typeparam>
        /// <typeparam name="TPagedResult">Tipo da retorno</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <param name="predicates"></param>
        /// <returns></returns>
        public virtual async Task<PagedResultDto<TResult>> GetListPaged<TResult, TPagedResult>(IService<TEntity, TPrimaryKey> service, TPagedResult input, IList<WhereIfCondition<TEntity>> predicates, bool orderByDescId = false)
            where TResult : class, IEntityDto<TPrimaryKey>, new()
            where TPagedResult : class, IPagedResultRequest, ISortedResultRequest, new()
        {
            try
            {
                Func<IService<TEntity, TPrimaryKey>, TPagedResult, IList<WhereIfCondition<TEntity>>, Task<PagedResultDto<TResult>>> getListPagedFunc = async (serviceFunc, inputFunc, predicatesFunc) =>
                 {
                     IQueryable<TEntity> query = serviceFunc.GetAll();

                     foreach (var item in predicatesFunc)
                     {
                         query = query.WhereIf(item.Condition, item.Expression);
                     }

                     var count = await query.CountAsync();
                     var dados = orderByDescId ?
                                    await query.OrderByDescending(x => x.Id).PageBy(inputFunc).ToListAsync() :
                                    await query.OrderBy(inputFunc.Sorting).PageBy(inputFunc).ToListAsync();

                     dynamic listDtos;
                     if (typeof(TResult).Name == "PropostaDeContratacaoInput")
                        listDtos = new MapManual().ListEntity_PropostaContratacao(dados);
                     else if (typeof(TResult).Name == "PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto")
                         listDtos = new MapManual().ListEntity_PermissaoVendaPlanoSaudeCorretora(dados);
                     else
                         listDtos = dados.MapTo<List<TResult>>();

                     return new PagedResultDto<TResult>(count, listDtos);
                 };

                if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
                {
                    return await ApplyEmpresaFilter(async () =>
                    {
                        return await getListPagedFunc(service, input, predicates);
                    });
                }

                return await getListPagedFunc(service, input, predicates);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult">Tipo do retorno</typeparam>
        /// <typeparam name="TIdInput">Tipo da retorno</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public virtual async Task<TResult> GetEntityById<TResult, TIdInput>(IService<TEntity, TPrimaryKey> service, TIdInput input)
            where TResult : IEntityDto<TPrimaryKey>
            where TIdInput : IdInput<TPrimaryKey>
        {
            Func<IService<TEntity, TPrimaryKey>, TIdInput, Task<TResult>> getEntityByIdFunc = async (serviceFunc, inputFunc) =>
            {
                TEntity entity = await serviceFunc.GetById(inputFunc.Id);

                return entity.MapTo<TResult>();
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await getEntityByIdFunc(service, input);
                });
            }

            return await getEntityByIdFunc(service, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult">Tipo do retorno</typeparam>
        /// <typeparam name="TIdInput">Tipo da retorno</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public virtual async Task<TResult> GetEntityByExternalId<TResult, TIdInput>(IService<TEntity, TPrimaryKey> service, TIdInput input)
            where TResult : IEntityDto<TPrimaryKey>
            where TIdInput : IdInput<TPrimaryKey>
        {
            Func<IService<TEntity, TPrimaryKey>, TIdInput, Task<TResult>> getEntityByExternalIdFunc = async (serviceFunc, inputFunc) =>
            {
                TEntity entity = await serviceFunc.GetByExternalId(inputFunc.Id);

                return entity.MapTo<TResult>();
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await getEntityByExternalIdFunc(service, input);
                });
            }

            return await getEntityByExternalIdFunc(service, input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TIdInput">Tipo do retorno</typeparam>
        /// <typeparam name="TInput">Tipo da retorno</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <param name="validateInput"></param>
        /// <param name="processInput"></param>
        /// <returns></returns>
        public virtual async Task<TIdInput> CreateEntity<TIdInput, TInput>(IService<TEntity, TPrimaryKey> service, TInput input, Action validateInput = null, Func<TEntity, TInput, Task> processInput = null)
            where TIdInput : IdInput<TPrimaryKey>
            where TInput : EzInputDto<TPrimaryKey>, IEntityDto<TPrimaryKey>, new()
        {
            Func<IService<TEntity, TPrimaryKey>, TInput, Action, Func<TEntity, TInput, Task>, Task<TIdInput>> createEntityFunc = async (serviceFunc, inputFunc, validateInputFunc, processInputFunc) =>
            {
                TPrimaryKey defaultValue = default(TPrimaryKey);

                if (inputFunc.Id.Equals(defaultValue))
                {
                    validateInputFunc?.Invoke();

                    TEntity entity = Activator.CreateInstance<TEntity>();

                    inputFunc.MapTo(entity);

                    if (processInputFunc != null)
                    {
                        await processInputFunc(entity, inputFunc);
                    }

                    service.SetEmpresaInAllEntities(entity);

                    if (!inputFunc.ExternalId.Equals(0) && inputFunc.IsIntegration)
                        entity.DateOfEditionIntegration = DateTime.Now;

                    try
                    {
                        await serviceFunc.CreateEntity(entity);
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        var message = new StringBuilder();
                        foreach (var error in dbEx.EntityValidationErrors)
                        {
                            foreach (var verror in error.ValidationErrors)
                            {
                                message.AppendLine(String.Format("{0}: {1}", verror.PropertyName, verror.ErrorMessage));
                            }
                        }
                        throw new UserFriendlyException(message.ToString());
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException(ex.Message);
                    }

                    Type tipo = typeof(TIdInput);
                    var obj = Activator.CreateInstance(tipo, entity.Id);
                    var res = obj as TIdInput;

                    if (res == null)
                    {
                        throw new Exception("Erro de conversão de tipos");
                    }

                    return res;
                }
                else
                {
                    throw new Exception("Erro. O id da entidade passada deve ser igual a 0.");
                }
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await createEntityFunc(service, input, validateInput, processInput);
                });
            }

            return await createEntityFunc(service, input, validateInput, processInput);
        }

        public virtual async Task<TIdInput> UpdateEntity<TIdInput, TInput>(IService<TEntity, TPrimaryKey> service, TInput input, Action validateInput = null, Func<TEntity, TInput, Task> processInput = null)
            where TIdInput : IdInput<TPrimaryKey>
            where TInput : EzInputDto<TPrimaryKey>, IEntityDto<TPrimaryKey>, new()
        {
            Func<IService<TEntity, TPrimaryKey>, TInput, Action, Func<TEntity, TInput, Task>, Task<TIdInput>> updateEntityFunc = async (serviceFunc, inputFunc, validateInputFunc, processInputFunc) =>
            {
                TPrimaryKey defaultValue = default(TPrimaryKey);

                if (!inputFunc.Id.Equals(defaultValue))
                {
                    validateInputFunc?.Invoke();

                    TEntity entity = default(TEntity);

                    try
                    {
                        entity = await serviceFunc.GetById(inputFunc.Id);
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException("Registro não encontrado.", ex.Message);
                    }

                    if(serviceFunc.GetType().Name != "CorretoraService")
                    {
                        inputFunc.MapTo(entity);
                    }                    

                    if (processInputFunc != null)
                    {
                        await processInputFunc(entity, inputFunc);
                    }

                    service.SetEmpresaInAllEntities(entity);

                    if (!inputFunc.ExternalId.Equals(0) && inputFunc.IsIntegration)
                        entity.DateOfEditionIntegration = DateTime.Now;

                    await serviceFunc.CreateOrUpdateEntity(entity);

                    Type tipo = typeof(TIdInput);
                    var obj = Activator.CreateInstance(tipo, entity.Id);
                    var res = obj as TIdInput;

                    if (res == null)
                    {
                        throw new Exception("Erro de conversão de tipos");
                    }

                    return res;
                }
                else
                {
                    throw new Exception("Erro. O id da entidade passada deve ser diferente de 0.");
                }
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await updateEntityFunc(service, input, validateInput, processInput);
                });
            }

            return await updateEntityFunc(service, input, validateInput, processInput);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntityDto">Tipo do retorno</typeparam>
        /// <typeparam name="TInput">Tipo da entrada</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <param name="validateInput"></param>
        /// <param name="processInput"></param>
        /// <returns></returns>
        public virtual async Task<TEntityDto> CreateAndReturnEntity<TEntityDto, TInput>(IService<TEntity, TPrimaryKey> service, TInput input, Action validateInput = null, Func<TEntity, TInput, Task> processInput = null)
            where TEntityDto : class, IEntityDto<TPrimaryKey>
            where TInput : EzInputDto<TPrimaryKey>, IEntityDto<TPrimaryKey>, new()
        {
            Func<IService<TEntity, TPrimaryKey>, TInput, Action, Func<TEntity, TInput, Task>, Task<TEntityDto>> createAndReturnEntityUpdate = async (serviceFunc, inputFunc, validateInputFunc, processInputFunc) =>
            {
                TPrimaryKey defaultValue = default(TPrimaryKey);

                if (inputFunc.Id.Equals(defaultValue))
                {
                    validateInputFunc?.Invoke();

                    TEntity entity = Activator.CreateInstance<TEntity>();

                    inputFunc.MapTo(entity);

                    if (processInputFunc != null)
                    {
                        await processInputFunc(entity, inputFunc);
                    }

                    service.SetEmpresaInAllEntities(entity);

                    if (!inputFunc.ExternalId.Equals(0) && inputFunc.IsIntegration)
                        entity.DateOfEditionIntegration = DateTime.Now;

                    entity = await serviceFunc.CreateOrUpdateAndReturnSavedEntity(entity);

                    return entity.MapTo<TEntityDto>();
                }
                else
                {
                    throw new Exception("Erro. O id da entidade passada deve ser igual a 0.");
                }
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await createAndReturnEntityUpdate(service, input, validateInput, processInput);
                });
            }

            return await createAndReturnEntityUpdate(service, input, validateInput, processInput);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntityDto">Tipo do retorno</typeparam>
        /// <typeparam name="TInput">Tipo da entrada</typeparam>
        /// <param name="service"></param>
        /// <param name="input"></param>
        /// <param name="validateInput"></param>
        /// <param name="processInput"></param>
        /// <returns></returns>
        public virtual async Task<TEntityDto> UpdateAndReturnEntity<TEntityDto, TInput>(IService<TEntity, TPrimaryKey> service, TInput input, Action validateInput = null, Func<TEntity, TInput, Task> processInput = null)
            where TEntityDto : class, IEntityDto<TPrimaryKey>
            where TInput : EzInputDto<TPrimaryKey>, IEntityDto<TPrimaryKey>, new()
        {
            Func<IService<TEntity, TPrimaryKey>, TInput, Action, Func<TEntity, TInput, Task>, Task<TEntityDto>> updateAndReturnEntityFunc = async (serviceFunc, inputFunc, validateInputFunc, processInputFunc) =>
            {
                TPrimaryKey defaultValue = default(TPrimaryKey);

                if (!inputFunc.Id.Equals(defaultValue))
                {
                    validateInputFunc?.Invoke();

                    TEntity entity;

                    try
                    {
                        entity = await serviceFunc.GetById(inputFunc.Id);
                    }
                    catch (Exception ex)
                    {
                        throw new UserFriendlyException("Registro não encontrado.", ex.Message);
                    }

                    inputFunc.MapTo(entity);

                    if (processInputFunc != null)
                    {
                        await processInputFunc(entity, inputFunc);
                    }

                    service.SetEmpresaInAllEntities(entity);

                    if (!inputFunc.ExternalId.Equals(0) && inputFunc.IsIntegration)
                        entity.DateOfEditionIntegration = DateTime.Now;

                    var entityRes = await serviceFunc.UpdateAndReturnEntity(entity);

                    return entityRes.MapTo<TEntityDto>();
                }
                else
                {
                    throw new Exception("Erro. O id da entidade passada deve ser diferente de 0.");
                }
            };


            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return await ApplyEmpresaFilter(async () =>
                {
                    return await updateAndReturnEntityFunc(service, input, validateInput, processInput);
                });
            }

            return await updateAndReturnEntityFunc(service, input, validateInput, processInput);
        }

        public virtual void Insert<TInput>(IService<TEntity, TPrimaryKey> service, IEnumerable<TInput> entities)
        {
            Action<IService<TEntity, TPrimaryKey>, IEnumerable<TInput>> insertFunc = (serviceFunc, entitiesFunc) =>
            {
                var listEntities = entitiesFunc.MapTo<List<TEntity>>();

                foreach (var entity in listEntities)
                {
                    service.SetEmpresaInAllEntities(entity);
                }

                serviceFunc.Insert(listEntities);
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                ApplyEmpresaFilter(() => insertFunc(service, entities));
            }
            else
            {
                insertFunc(service, entities);
            }
        }
    }

    public abstract class EZControlAppServiceBase<TEntity> : EZControlAppServiceBase<TEntity, int>
        where TEntity : class, IEzEntity<int>
    {
        protected EZControlAppServiceBase(IRepository<TEntity, int> repository)
            : base(repository)
        {
        }
    }
}