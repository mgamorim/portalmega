﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Arquivo.Mapper
{
    public class ArquivoCMSMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ArquivoCMSInput, Domain.CMS.Geral.ArquivoCMS>().ReverseMap();
            CreateMap<ArquivoCMSListDto, Domain.CMS.Geral.ArquivoCMS>().ReverseMap();
            CreateMap<ArquivoCMSListDto, ArquivoCMSInput>();
        }
    }
}
