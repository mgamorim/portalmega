﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Arquivo
{
    public class GetArquivoCMSInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
