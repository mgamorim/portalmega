﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Template.Mapper
{
    public class TemplateMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TemplateInput, Domain.CMS.Geral.Template>().ReverseMap();
            CreateMap<TemplateListDto, Domain.CMS.Geral.Template>().ReverseMap();
            CreateMap<TemplateListDto, TemplateInput>();
        }
    }
}
