﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Template
{
    public class GetTemplateInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Autor { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
