﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Template
{
    public class TemplateListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Autor { get; set; }
        public string AutorUrl { get; set; }
        public string Versao { get; set; }
        public string NomeDoArquivo { get; set; }
        public string Posicoes { get; set; }
        public string TiposDeWidgetSuportados { get; set; }
        public string TipoDePaginaParaPaginaDefault { get; set; }
        public string TipoDePaginaParaPostDefault { get; set; }
        public string TipoDePaginaParaCategoriaDefault { get; set; }
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }
    }
}
