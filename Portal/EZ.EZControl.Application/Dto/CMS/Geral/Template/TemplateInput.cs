﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Template
{
    public class TemplateInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Template.NomeMaxLength)]
        public string Nome { get; set; }
        [StringLength(Domain.CMS.Geral.Template.DescricaoMaxLength)]
        public string Descricao { get; set; }
        public string Autor { get; set; }
        public string AutorUrl { get; set; }
        [Required]
        public string Versao { get; set; }
        public string NomeDoArquivo { get; set; }
        public string Posicoes { get; set; }
        public string TiposDeWidgetSuportados { get; set; }
        [Required]
        public string TiposDePaginasSuportadas { get; set; }
        [Required]
        public string TipoDePaginaParaPaginaDefault { get; set; }
        [Required]
        public string TipoDePaginaParaPostDefault { get; set; }
        [Required]
        public string TipoDePaginaParaCategoriaDefault { get; set; }
        [Required]
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }
    }
}
