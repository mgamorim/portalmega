﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Tag
{
    public class TagInput : EzInputDto, IPassivable
    {
        public Boolean IsActive { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Tag.NomeMaxLength)]
        public String Nome { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Tag.SlugMaxLength)]
        public String Slug { get; set; }
        [StringLength(Domain.CMS.Geral.Tag.DescricaoMaxLength)]
        public String Descricao { get; set; }
    }
}
