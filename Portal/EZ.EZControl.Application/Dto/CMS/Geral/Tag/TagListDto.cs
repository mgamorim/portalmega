﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.Tag
{
    public class TagListDto : EzFullAuditedInputDto, IPassivable
    {
        public Boolean IsActive { get; set; }
        public String Nome { get; set; }
        public String Descricao { get; set; }
    }
}
