﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Tag.Mapper
{
    public class TagMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TagInput, Domain.CMS.Geral.Tag>().ReverseMap();
            CreateMap<TagListDto, Domain.CMS.Geral.Tag>().ReverseMap();
            CreateMap<TagListDto, TagInput>();
        }
    }
}
