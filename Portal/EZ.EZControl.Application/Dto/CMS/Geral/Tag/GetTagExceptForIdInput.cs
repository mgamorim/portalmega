﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Tag
{
    public class GetTagExceptForIdInput : PagedAndSortedInputDto, IShouldNormalize
    {

        public int TagId { get; set; }
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
