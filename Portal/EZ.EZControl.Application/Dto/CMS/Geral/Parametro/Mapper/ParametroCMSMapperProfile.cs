﻿using EZ.EZControl.Dto.Core.Geral.Parametro.Mapper;

namespace EZ.EZControl.Dto.CMS.Geral.Parametro.Mapper
{
    public class ParametroCMSMapperProfile : ParametroMapperProfile
    {
        protected override void Configure()
        {
            CreateMap<ParametroCMSInput, Domain.CMS.Geral.ParametroCMS>().ReverseMap();
            CreateMap<ParametroCMSListDto, Domain.CMS.Geral.ParametroCMS>().ReverseMap();
            CreateMap<ParametroCMSListDto, ParametroCMSInput>();
        }
    }
}
