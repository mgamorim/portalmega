﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Categoria
{
    public class GetCategoriaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string NomeSite { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}