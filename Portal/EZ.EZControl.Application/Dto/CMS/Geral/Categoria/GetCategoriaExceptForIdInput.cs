﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Categoria
{
    public class GetCategoriaExceptForIdInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int CategoriaId { get; set; }

        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}