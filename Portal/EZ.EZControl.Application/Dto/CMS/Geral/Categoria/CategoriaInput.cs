﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Categoria
{
    public class CategoriaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Categoria.NomeMaxLength)]
        public string Nome { get; set; }
        [StringLength(Domain.CMS.Geral.Categoria.DescricaoMaxLength)]
        public string Descricao { get; set; }
        [StringLength(Domain.CMS.Geral.Categoria.SlugMaxLength)]
        public string Slug { get; set; }
        public int? CategoriaPaiId { get; set; }
        public int PostId { get; set; }
        public int SiteId { get; set; }
    }
}