﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Categoria.Mapper
{
    public class CategoriaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CategoriaInput, Domain.CMS.Geral.Categoria>().ReverseMap();
            CreateMap<CategoriaListDto, Domain.CMS.Geral.Categoria>().ReverseMap();
            CreateMap<CategoriaListDto, CategoriaInput>();
        }
    }
}
