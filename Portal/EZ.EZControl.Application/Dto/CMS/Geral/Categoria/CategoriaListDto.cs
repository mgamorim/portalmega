﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Categoria
{
    public class CategoriaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Slug { get; set; }
        public int CategoriaPaiId { get; set; }
        public int PostId { get; set; }
        public int SiteId { get; set; }
        public SiteListDto Site { get; set; }
    }
}