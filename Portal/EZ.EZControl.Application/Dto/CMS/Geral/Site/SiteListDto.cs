﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Site
{
    public class SiteListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Host { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTags { get; set; }
        public string Owner { get; set; }
        public int TemplateDefaultId { get; set; }
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public TipoDeConteudoRSSEnum TipoDeConteudoRSS { get; set; }
        public int LimitePostPorPagina { get; set; }
        public int LimitePostPorRss { get; set; }
        public bool EvitarMecanismoDeBusca { get; set; }
        public bool PermitirComentarios { get; set; }
        public bool PermitirLinks { get; set; }
        public bool AutorInformaNomeEmail { get; set; }
        public bool NotificarPorEmailNovoComentario { get; set; }
        public bool AtivarModeracaoDeComentario { get; set; }
        public string TextoCss { get; set; }
        public string TextoJavaScript { get; set; }
        public bool HabilitarPesquisaPost { get; set; }
        public bool HabilitarPesquisaPagina { get; set; }
        public string TipoDePaginaParaPaginaDefault { get; set; }
        public string TipoDePaginaParaPostDefault { get; set; }
        public string TipoDePaginaParaCategoriaDefault { get; set; }
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }
    }
}