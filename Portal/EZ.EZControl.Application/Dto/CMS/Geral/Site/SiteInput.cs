﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.EZ;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Site
{
    public class SiteInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Site.NomeMaxLength)]
        public string Nome { get; set; }
        [StringLength(Domain.CMS.Geral.Site.DescricaoMaxLength)]
        public string Descricao { get; set; }
        //[Required]
        //[StringLength(Domain.CMS.Geral.Site.HostMaxLength)]
        //public string Host { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaTags { get; set; }
        public string Owner { get; set; }
        [Required]
        public int TemplateDefaultId { get; set; }
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public TipoDeConteudoRSSEnum TipoDeConteudoRSS { get; set; }
        public int LimitePostPorPagina { get; set; }
        public int LimitePostPorRss { get; set; }
        public bool EvitarMecanismoDeBusca { get; set; }
        public bool PermitirComentarios { get; set; }
        public bool PermitirLinks { get; set; }
        public bool AutorInformaNomeEmail { get; set; }
        public bool NotificarPorEmailNovoComentario { get; set; }
        public bool AtivarModeracaoDeComentario { get; set; }
        [StringLength(Domain.CMS.Geral.Site.TextoCssMaxLength)]
        public string TextoCss { get; set; }
        [StringLength(Domain.CMS.Geral.Site.TextoJavaScriptMaxLength)]
        public string TextoJavaScript { get; set; }
        public bool HabilitarPesquisaPost { get; set; }
        public bool HabilitarPesquisaPagina { get; set; }
        public ICollection<HostInput> Hosts { get; set; }
        [Required]
        public string TipoDePaginaParaPaginaDefault { get; set; }
        [Required]
        public string TipoDePaginaParaPostDefault { get; set; }
        [Required]
        public string TipoDePaginaParaCategoriaDefault { get; set; }
        [Required]
        public string TipoDePaginaParaPaginaInicialDefault { get; set; }

        public string HostPrincipal { get; set; }
    }
}