﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Site
{
    public class GetSiteInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string Host { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}