﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Site.Mapper
{
    public class SiteMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<SiteInput, Domain.CMS.Geral.Site>()
                .ForMember(destination => destination.Hosts, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<SiteListDto, Domain.CMS.Geral.Site>().ReverseMap();
            CreateMap<SiteListDto, SiteInput>();
        }
    }
}
