﻿using System.Collections.Generic;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Pagina;
using EZ.EZControl.Dto.CMS.Geral.Post;

namespace EZ.EZControl.Dto.CMS.Geral.Conteudo
{
    public class ConteudoInput
    {
        public TipoDeConteudoEnum TipoDeConteudo { get; set; }
        public CategoriaInput Categoria { get; set; }
        public PaginaInput Pagina { get; set; }
        public PostInput Post { get; set; }
        public List<PostInput> Posts { get; set; }
        public int AnoDosPosts { get; set; }
        public int MesDosPosts { get; set; }
    }
}
