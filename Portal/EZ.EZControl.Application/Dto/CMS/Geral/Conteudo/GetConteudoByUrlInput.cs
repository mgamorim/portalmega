﻿using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Conteudo
{
    public class GetConteudoByUrlInput
    {
        [Required]
        public string Url { get; set; }

        public int ItensPorPagina { get; set; }

        public int NumeroDaPagina { get; set; }
    }
}
