﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Conteudo.MapperProfile
{
    public class ConteudoMapperProfile : Profile
    {
        public ConteudoMapperProfile()
        {
            CreateMap<Domain.CMS.Geral.Conteudo, ConteudoInput>().ReverseMap();
        }
    }
}
