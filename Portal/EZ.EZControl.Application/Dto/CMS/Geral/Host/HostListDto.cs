﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Host
{
    public class HostListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Url { get; set; }
        public bool IsPrincipal { get; set; }
        public int SiteId { get; set; }
    }
}