﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Host
{
    public class GetHostInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Url { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Url";
            }
        }
    }
}