﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Host.Mapper
{
    public class HostMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<HostInput, Domain.CMS.Geral.Host>().ReverseMap();
            CreateMap<HostListDto, Domain.CMS.Geral.Host>().ReverseMap();
            CreateMap<HostListDto, HostInput>();
        }
    }
}
