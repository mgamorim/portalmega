﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Host
{
    public class HostInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Host.UrlMaxLength)]
        public string Url { get; set; }
        public bool IsPrincipal { get; set; }
        public int SiteId { get; set; }
    }
}