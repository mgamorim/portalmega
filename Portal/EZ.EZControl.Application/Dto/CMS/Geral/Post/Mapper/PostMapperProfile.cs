﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Post.Mapper
{
    public class PostMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PostInput, Domain.CMS.Geral.Post>()
                .ForMember(destination => destination.Categorias, opt => opt.Ignore())
                .ForMember(destination => destination.Tags, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<PostListDto, Domain.CMS.Geral.Post>().ReverseMap();
            CreateMap<PostListDto, PostInput>();
        }
    }
}