﻿using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Publicacao;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.CMS.Geral.Post
{
    public class PostInput : PublicacaoInput
    {
        public List<CategoriaInput> Categorias { get; set; }
        public List<TagInput> Tags { get; set; }
    }
}