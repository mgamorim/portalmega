﻿using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Publicacao;
using EZ.EZControl.Dto.CMS.Geral.Tag;

namespace EZ.EZControl.Dto.CMS.Geral.Post
{
    public class PostListDto : PublicacaoListDto
    {
        public CategoriaListDto[] Categorias { get; set; }
        public TagListDto[] Tags { get; set; }
    }
}