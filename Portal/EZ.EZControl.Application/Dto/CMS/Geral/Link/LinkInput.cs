﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Link
{
    public class LinkInput : EzInputDto
    {
        public TipoDeLinkEnum TipoDeLink { get; set; }
        public int SiteId { get; set; }
        public int ConteudoId { get; set; }
        public int CategoriaPaiId { get; set; }
        public string Titulo { get; set; }
        public string Slug { get; set; }
        public string Link { get; set; }
        public CategoriaInput PrimeiraCategoria { get; set; }
    }
}
