﻿using Abp.Runtime.Validation;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.HistoricoPublicacao
{
    public class GetHistoricoPublicacaoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime Data { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Data";
            }
        }
    }
}