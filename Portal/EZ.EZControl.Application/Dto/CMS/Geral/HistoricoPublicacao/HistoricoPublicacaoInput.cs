﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.HistoricoPublicacao
{
    public class HistoricoPublicacaoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int PublicacaoId { get; set; }
        public DateTime Data { get; set; }
        public StatusDaPublicacaoEnum Status { get; set; }
    }
}