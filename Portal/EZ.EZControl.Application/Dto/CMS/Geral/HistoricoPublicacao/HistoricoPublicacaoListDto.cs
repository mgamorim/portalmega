﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.HistoricoPublicacao
{
    public class HistoricoPublicacaoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string PublicacaoTitulo { get; set; }
        public DateTime Data { get; set; }
        public StatusDaPublicacaoEnum Status { get; set; }
    }
}