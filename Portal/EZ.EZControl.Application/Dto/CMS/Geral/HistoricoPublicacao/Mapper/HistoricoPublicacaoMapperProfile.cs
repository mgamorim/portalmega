﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.HistoricoPublicacao.Mapper
{
    public class HistoricoPublicacaoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<HistoricoPublicacaoInput, Domain.CMS.Geral.HistoricoPublicacao>().ReverseMap();
            CreateMap<HistoricoPublicacaoListDto, Domain.CMS.Geral.HistoricoPublicacao>().ReverseMap();
            CreateMap<HistoricoPublicacaoListDto, HistoricoPublicacaoInput>();
        }
    }
}
