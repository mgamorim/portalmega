﻿using EZ.EZControl.Dto.CMS.Geral.Publicacao;

namespace EZ.EZControl.Dto.CMS.Geral.Pagina
{
    public class PaginaInput : PublicacaoInput
    {
        public string TipoDePagina { get; set; }
    }
}