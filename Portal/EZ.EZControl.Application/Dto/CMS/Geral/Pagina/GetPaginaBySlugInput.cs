﻿using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Pagina
{
    public class GetPaginaBySlugInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int SiteId { get; set; }

        public string Slug { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Slug";
            }
        }
    }
}
