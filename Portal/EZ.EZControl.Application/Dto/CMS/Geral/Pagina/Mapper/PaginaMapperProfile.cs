﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Pagina.Mapper
{
    public class PaginaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PaginaInput, Domain.CMS.Geral.Pagina>().ReverseMap();
            CreateMap<PaginaListDto, Domain.CMS.Geral.Pagina>().ReverseMap();
            CreateMap<PaginaListDto, PaginaInput>();
        }
    }
}
