﻿using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Pagina
{
    public class GetPaginaExceptForInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int SiteId { get; set; }

        public string Titulo { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
