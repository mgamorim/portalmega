﻿using AutoMapper;
using EZ.EZControl.Dto.CMS.Geral.Menu;

namespace EZ.EZControl.Dto.CMS.Geral.ItemDeMenu.Mapper
{
    public class ItemDeMenuMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ItemDeMenuInput, Domain.CMS.Geral.ItemDeMenu>().ReverseMap();
            CreateMap<ItemDeMenuListDto, Domain.CMS.Geral.ItemDeMenu>().ReverseMap();
            CreateMap<ItemDeMenuListDto, ItemDeMenuInput>();
        }
    }
}
