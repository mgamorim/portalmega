﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.ItemDeMenu
{
    public class ItemDeMenuInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.CMS.Geral.ItemDeMenu.TituloMaxLength)]
        public string Titulo { get; set; }

        [Required]
        [StringLength(Domain.CMS.Geral.ItemDeMenu.DescricaoDoTituloMaxLength)]
        public string DescricaoDoTitulo { get; set; }

        [Required]
        public int MenuId { get; set; }
        public int? ItemMenuId { get; set; }
        public int? CategoriaId { get; set; }
        public int? PaginaId { get; set; }
        public TipoDeItemDeMenuEnum TipoDeItemDeMenu { get; set; }

        [Required]
        public string Url { get; set; }
    }
}
