﻿using System.Collections.Generic;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Dto.CMS.Geral.Pagina;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.ItemDeMenu
{
    public class ItemDeMenuListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Titulo { get; set; }
        public string DescricaoDoTitulo { get; set; }
        public MenuListDto Menu { get; set; }
        public ItemDeMenuListDto ItemMenu { get; set; }
        public CategoriaListDto Categoria { get; set; }
        public PaginaListDto Pagina { get; set; }
        public TipoDeItemDeMenuEnum TipoDeItemDeMenu { get; set; }
        public int MenuId { get; set; }
        public int ItemMenuId { get; set; }
        public int CategoriaId { get; set; }
        public int PaginaId { get; set; }
        public string Url { get; set; }
        public virtual ICollection<ItemDeMenuListDto> ItensDeMenu { get; set; }
    }
}
