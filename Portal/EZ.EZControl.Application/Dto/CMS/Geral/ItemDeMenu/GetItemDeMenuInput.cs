﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.ItemDeMenu
{
    public class GetItemDeMenuInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Titulo { get; set; }
        public string DescricaoDoTitulo { get; set; }
        public string NomeMenu { get; set; }
        public string Url { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
