﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.Publicacao
{
    public class PublicacaoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        public string Titulo { get; set; }
        public int IdImagemDestaque { get; set; }
        public DateTime DataDePublicacao { get; set; }
        public string Conteudo { get; set; }
        public string Slug { get; set; }
        public int SiteId { get; set; }
        public StatusDaPublicacaoEnum StatusDaPublicacaoEnum { get; set; }
    }
}