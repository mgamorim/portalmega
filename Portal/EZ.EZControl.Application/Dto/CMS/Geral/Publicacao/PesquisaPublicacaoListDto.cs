﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.Publicacao
{
    public class PesquisaPublicacaoListDto : EzFullAuditedInputDto
    {
        public string Site { get; set; }
        public string Publicacao { get; set; }
        public string Resumo { get; set; }
        public string Categoria { get; set; }
        public DateTime DataDePublicacao { get; set; }
        public string Url { get; set; }
    }
}