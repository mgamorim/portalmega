﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Publicacao
{
    public class PublicacaoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public TipoDePublicacaoEnum TipoDePublicacao { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Post.TituloMaxLength)]
        public string Titulo { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Post.SlugMaxLength)]
        public string Slug { get; set; }
        public string Conteudo { get; set; }
        public int IdImagemDestaque { get; set; }
        public int SiteId { get; set; }
        public StatusDaPublicacaoEnum StatusDaPublicacaoEnum { get; set; }
        public DateTime DataDePublicacao { get; set; }
    }
}