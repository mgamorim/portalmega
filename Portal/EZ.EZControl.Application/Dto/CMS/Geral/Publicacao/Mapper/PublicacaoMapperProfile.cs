﻿using AutoMapper;
using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.Dto.CMS.Geral.Publicacao.Mapper
{
    public class PublicacaoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PublicacaoInput, PublicacaoBase>().ReverseMap();
            CreateMap<PublicacaoListDto, PublicacaoBase>().ReverseMap();
            CreateMap<PublicacaoListDto, PublicacaoInput>();
        }
    }
}
