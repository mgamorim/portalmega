﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Publicacao
{
    public class PesquisaPublicacaoDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public int SiteId { get; set; }
        public string TermpoPesquisa { get; set; }
        public bool IncluiPosts { get; set; }
        public bool IncluiPaginas { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "TermoPesquisa";
            }
        }
    }
}
