﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Publicacao
{
    public class GetPublicacaoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Titulo { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}