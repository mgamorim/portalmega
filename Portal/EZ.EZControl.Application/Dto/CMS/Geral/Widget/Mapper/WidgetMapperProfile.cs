﻿using AutoMapper;
using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.Dto.CMS.Geral.Widget.Mapper
{
    public class WidgetMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<WidgetInput, WidgetBase>().ReverseMap();
            CreateMap<WidgetInput, WidgetHTML>().ReverseMap();
            CreateMap<WidgetInput, WidgetMenu>().ReverseMap();
            CreateMap<WidgetListDto, WidgetBase>().ReverseMap();
            CreateMap<WidgetListDto, WidgetInput>();
        }
    }
}
