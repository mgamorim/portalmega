﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Widget
{
    public class WidgetInput : EzInputDto, IPassivable
    {
        public string Titulo { get; set; }
        public bool ExibirTitulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public int SiteId { get; set; }
        public int TemplateId { get; set; }
        public TipoDeWidgetEnum Tipo { get; set; }
        public string Posicao { get; set; }
        public bool IsActive { get; set; }
        public int MenuId { get; set; }
        public string Conteudo { get; set; }
    }
}