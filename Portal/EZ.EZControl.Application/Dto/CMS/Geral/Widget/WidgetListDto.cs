﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMS.Geral.Widget
{
    public class WidgetListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public TipoDeWidgetEnum Tipo { get; set; }
        public string Titulo { get; set; }
    }
}