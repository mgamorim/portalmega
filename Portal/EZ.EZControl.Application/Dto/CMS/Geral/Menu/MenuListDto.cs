﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.CMS.Geral.Menu
{
    public class MenuListDto : EzFullAuditedInputDto, IPassivable
    {
        public Boolean IsActive { get; set; }
        public String Nome { get; set; }
        public Int32 SiteId { get; set; }
        public SiteListDto Site { get; set; }
    }
}
