﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMS.Geral.Menu
{
    public class MenuInput : EzInputDto, IPassivable
    {
        public Boolean IsActive { get; set; }
        [Required]
        [StringLength(Domain.CMS.Geral.Menu.NomeMaxLength)]
        public String Nome { get; set; }
        [Required]
        public Int32 SiteId { get; set; }
    }
}
