﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CMS.Geral.Menu
{
    public class GetMenuInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public int SiteId { get; set; }
        public string NomeSite { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
