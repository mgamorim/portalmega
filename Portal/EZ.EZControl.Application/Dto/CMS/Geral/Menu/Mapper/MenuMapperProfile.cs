﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMS.Geral.Menu.Mapper
{
    public class MenuMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<MenuInput, Domain.CMS.Geral.Menu>().ReverseMap();
            CreateMap<MenuListDto, Domain.CMS.Geral.Menu>().ReverseMap();
            CreateMap<MenuListDto, MenuInput>();
        }
    }
}
