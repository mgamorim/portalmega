﻿using AutoMapper;

namespace EZ.EZControl.Dto.Vendas.Geral.ItemDePedido.Mapper
{
    public class ItemDePedidoMapperProfile : Profile
    {
        public ItemDePedidoMapperProfile()
        {
            CreateMap<ItemDePedidoInput, Domain.Vendas.Pedidos.ItemDePedido>().ReverseMap();
            CreateMap<ItemDePedidoListDto, Domain.Vendas.Pedidos.ItemDePedido>().ReverseMap();
            CreateMap<ItemDePedidoListDto, ItemDePedidoInput>();
        }
    }
}