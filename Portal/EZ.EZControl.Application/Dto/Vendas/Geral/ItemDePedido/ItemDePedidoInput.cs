﻿using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Vendas.Geral.ItemDePedido
{
    public class ItemDePedidoInput : EzInputDto
    {
        public decimal Valor { get; set; }
        public decimal Quantidade { get; set; }
        public TipoDeItemDePedidoEnum TipoDeItemDePedido { get; set; }
        public int PedidoId { get; set; }
        public int? ProdutoId { get; set; }
        public int? ServicoId { get; set; }
    }
}