﻿using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Servicos.Geral.Servicos;

namespace EZ.EZControl.Dto.Vendas.Geral.ItemDePedido
{
    public class ItemDePedidoListDto : EzInputDto
    {
        public decimal Valor { get; set; }
        public decimal ValorCadastro { get; set; }
        public decimal Quantidade { get; set; }
        public TipoDeItemDePedidoEnum TipoDeItemDePedido { get; set; }
        public int PedidoId { get; set; }
        public ProdutoListDto Produto { get; set; }
        public ServicoListDto Servico { get; set; }
    }
}