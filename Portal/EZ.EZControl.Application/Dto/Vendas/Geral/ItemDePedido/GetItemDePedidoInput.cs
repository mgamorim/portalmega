﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Vendas.Geral.ItemDePedido
{
    public class GetItemDePedidoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int PedidoId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}