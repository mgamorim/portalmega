﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Vendas.Geral.Pedido
{
    public class GetPedidoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomeCliente { get; set; }

        public int Id { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}