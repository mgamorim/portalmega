﻿using AutoMapper;

namespace EZ.EZControl.Dto.Vendas.Geral.Pedido.Mapper
{
    public class PedidoMapperProfile : Profile
    {
        public PedidoMapperProfile()
        {
            CreateMap<PedidoInput, Domain.Vendas.Pedidos.Pedido>()
                .ReverseMap()
                .ForMember(destination => destination.Items, opt => opt.MapFrom(source => source.ItensDePedidoReadOnly))
                .ForMember(destination => destination.ClienteId, opt => opt.MapFrom(source => source.Cliente.Id));
            CreateMap<PedidoListDto, Domain.Vendas.Pedidos.Pedido>().ReverseMap();
            CreateMap<PedidoListDto, PedidoInput>();
        }
    }
}