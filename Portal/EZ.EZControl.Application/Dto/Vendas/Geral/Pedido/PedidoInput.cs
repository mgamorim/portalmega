﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Vendas.Geral.ItemDePedido;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Vendas.Geral.Pedido
{
    public class PedidoInput : EzInputDto
    {
        [Required]
        public int ClienteId { get; set; }

        [Required]
        public PedidoStatusEnum Status { get; set; }

        [Required]
        public PedidoTipoEnum Tipo { get; set; }

        [Required]
        public IEnumerable<ItemDePedidoListDto> Items { get; set; }
        public FormaDePagamentoEnum FormaDePagamento { get; set; }
    }
}