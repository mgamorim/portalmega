﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Vendas.Geral.Pedido
{
    public class PedidoListDto : EzInputDto
    {
        public string NomeCliente { get; set; }
        public int ClienteId { get; set; }
        public decimal Valor { get; set; }
        public PedidoStatusEnum Status { get; set; }
        public PedidoTipoEnum Tipo { get; set; }
        public FormaDePagamentoEnum FormaDePagamento { get; set; }
    }
}