﻿using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto
{
    public class FileDto
    {
        [Required]
        public string FileName { get; set; }

        public string Nome { get; set; }

        public bool IsActive { get; set; }

        [Required]
        public string FileType { get; set; }

        [Required]
        public string FileToken { get; set; }

        public string NomeSistema { get; set; }

        public DateTime Created { get; set; }

        public DateTime Modified { get; set; }

        public long Size { get; set; }

        public String Path { get; set; }

        public TipoDeDocumentoEnum Tipo { get; set; }

        public bool EmExigencia { get; set; }

        public string Motivo { get; set; }

        public TipoDeArquivoEnum TipoDeArquivo
        {
            get
            {
                return (TipoDeArquivoEnum)Enum.Parse(typeof(TipoDeArquivoEnum), this.FileType.Replace(".", "").ToUpper(), false);
            }
        }

        public SistemaEnum Sistema
        {
            get
            {
                return (SistemaEnum)Enum.Parse(typeof(SistemaEnum), this.NomeSistema, false);
            }
        }

        public FileDto()
        {
            FileToken = Guid.NewGuid().ToString("N");
        }

        public FileDto(string fileName, string fileType)
            : this()
        {
            FileName = fileName;
            FileType = fileType;
        }
    }
}