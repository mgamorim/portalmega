﻿using AutoMapper;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Menu.Mapper
{
    public class MenuMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<MenuFrontInput, Domain.CMS.Geral.Menu>().ReverseMap();
        }
    }
}
