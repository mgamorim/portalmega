﻿using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Menu
{
    public class MenuFrontInput : EzInputDto
    {
        public Boolean IsActive { get; set; }
        public String Nome { get; set; }
        public virtual SiteInput Site { get; set; }
        public virtual ICollection<ItemDeMenuInput> ItensDeMenu { get; set; }
    }
}
