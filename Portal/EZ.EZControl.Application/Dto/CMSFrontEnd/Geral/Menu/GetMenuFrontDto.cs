﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Menu
{
    public class GetMenuFrontDto : EzInputDto
    {
        public string Html { get; set; }
    }
}
