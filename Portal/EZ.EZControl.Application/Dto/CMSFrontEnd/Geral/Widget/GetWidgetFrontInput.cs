﻿using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Widget
{
    public class GetWidgetFrontInput : EzInputDto
    {
        [Required]
        public int SiteId { get; set; }
        [Required]
        public int TemplateId { get; set; }
        [Required]
        public string Posicao { get; set; }
    }
}
