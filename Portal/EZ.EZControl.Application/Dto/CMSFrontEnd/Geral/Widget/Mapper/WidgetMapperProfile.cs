﻿using AutoMapper;
using EZ.EZControl.Domain.CMS.Geral;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Widget.Mapper
{
    public class WidgetMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<WidgetFrontInput, WidgetBase>().ReverseMap();
            CreateMap<WidgetFrontInput, WidgetHTML>().ReverseMap();
            CreateMap<WidgetFrontInput, WidgetMenu>().ReverseMap();
        }
    }
}
