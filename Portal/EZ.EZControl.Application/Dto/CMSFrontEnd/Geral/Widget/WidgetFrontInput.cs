﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.CMSFrontEnd.Geral.Menu;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Widget
{
    public class WidgetFrontInput : EzInputDto
    {
        public string Titulo { get; set; }
        public bool ExibirTitulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public int SiteId { get; set; }
        public int TemplateId { get; set; }
        public TipoDeWidgetEnum Tipo { get; set; }
        public string Posicao { get; set; }
        public string Conteudo { get; set; }
        public MenuFrontInput Menu { get; set; }
    }
}
