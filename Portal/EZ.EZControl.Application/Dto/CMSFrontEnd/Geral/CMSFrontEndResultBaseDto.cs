﻿using EZ.EZControl.Domain.CMSFrontend.Enums;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral
{
    public class CMSFrontEndResultBaseDto<TOutput>
        where TOutput : class
    {
        public TOutput Resultado { get; set; }

        public string Titulo { get; set; }

        public string Descricao { get; set; }

        public CMSFrontEndResultStatusEnum Status { get; set; }
    }
}