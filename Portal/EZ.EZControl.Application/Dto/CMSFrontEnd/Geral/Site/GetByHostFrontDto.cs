﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.CMSFrontEnd.Geral.Site
{
    public class GetByHostFrontDto : EzInputDto
    {
        public string Host { get; set; }
    }
}
