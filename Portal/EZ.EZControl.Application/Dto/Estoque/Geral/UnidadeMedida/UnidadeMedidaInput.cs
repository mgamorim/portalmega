﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Domain.Estoque.Enums;

namespace EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida
{
    public class UnidadeMedidaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public UnidadePadraoEnum UnidadePadrao { get; set; }
    }
}
