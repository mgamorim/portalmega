﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida.Mapper
{
    public class UnidadeMedidaMapperProfile : Profile
    {
        public UnidadeMedidaMapperProfile()
        {
            CreateMap<UnidadeMedidaInput, Domain.Estoque.Geral.UnidadeMedida>().ReverseMap();
            CreateMap<UnidadeMedidaListDto, Domain.Estoque.Geral.UnidadeMedida>().ReverseMap();
            CreateMap<UnidadeMedidaListDto, UnidadeMedidaInput>();
        }
    }
}