﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Estoque.Geral.Natureza
{
    public class NaturezaListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Descricao { get; set; }
    }
}
