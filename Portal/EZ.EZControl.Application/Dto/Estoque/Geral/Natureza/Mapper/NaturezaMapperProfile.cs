﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.Natureza.Mapper
{
    public class NaturezaMapperProfile : Profile
    {
        public NaturezaMapperProfile()
        {
            CreateMap<NaturezaInput, Domain.Estoque.Geral.Natureza>().ReverseMap();
            CreateMap<NaturezaListDto, Domain.Estoque.Geral.Natureza>().ReverseMap();
            CreateMap<NaturezaListDto, NaturezaInput>();
        }
    }
}