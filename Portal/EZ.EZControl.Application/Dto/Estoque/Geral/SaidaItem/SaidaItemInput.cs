﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Estoque.Geral.SaidaItem
{
    public class SaidaItemInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int SaidaId { get; set; }
        public int? LocalArmazenamentoId { get; set; }
        public int ProdutoId { get; set; }
        public int Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
    }
}
