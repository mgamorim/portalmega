﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Estoque.Geral.SaidaItem
{
    public class GetSaidaItemInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int SaidaId { get; set; }
        public int? LocalArmazenamentoId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
