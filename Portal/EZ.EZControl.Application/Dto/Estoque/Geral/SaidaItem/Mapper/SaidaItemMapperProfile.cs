﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.SaidaItem.Mapper
{
    public class SaidaItemMapperProfile : Profile
    {
        public SaidaItemMapperProfile()
        {                
            CreateMap<SaidaItemInput, Domain.Estoque.Geral.SaidaItem>().ReverseMap();
            CreateMap<SaidaItemListDto, Domain.Estoque.Geral.SaidaItem>().ReverseMap();
            CreateMap<SaidaItemListDto, SaidaItemInput>();
        }
    }
}