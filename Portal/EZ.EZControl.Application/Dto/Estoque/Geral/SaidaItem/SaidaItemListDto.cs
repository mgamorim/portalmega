﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.Estoque.Geral.Entrada;
using EZ.EZControl.Dto.Estoque.Geral.Saida;

namespace EZ.EZControl.Dto.Estoque.Geral.SaidaItem
{
    public class SaidaItemListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public virtual SaidaListDto Saida { get; set; }
        public int SaidaId { get; set; }
        public virtual LocalArmazenamentoListDto LocalArmazenamento { get; set; }
        public int? LocalArmazenamentoId { get; set; }
        public virtual ProdutoListDto Produto { get; set; }
        public int ProdutoId { get; set; }
        public int Quantidade { get; set; }
        public decimal ValorUnitario { get; set; }
        public decimal Total
        {
            get { return (Quantidade * ValorUnitario); }
        }
    }
}
