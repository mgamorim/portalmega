﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;

namespace EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento
{
    public class LocalArmazenamentoListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int EmpresaId { get; set; }
        public EmpresaListDto Empresa { get; set; }
        public string Descricao { get; set; }
    }
}
