﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento.Mapper
{
    public class LocalArmazenamentoMapperProfile : Profile
    {
        public LocalArmazenamentoMapperProfile()
        {                
            CreateMap<LocalArmazenamentoInput, Domain.Estoque.Geral.LocalArmazenamento>().ReverseMap();
            CreateMap<LocalArmazenamentoListDto, Domain.Estoque.Geral.LocalArmazenamento>().ReverseMap();
            CreateMap<LocalArmazenamentoListDto, LocalArmazenamentoInput>();
        }
    }
}