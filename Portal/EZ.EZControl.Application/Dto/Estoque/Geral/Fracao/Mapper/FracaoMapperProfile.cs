﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.Fracao.Mapper
{
    public class FracaoMapperProfile : Profile
    {
        public FracaoMapperProfile()
        {                
            CreateMap<FracaoInput, Domain.Estoque.Geral.Fracao>().ReverseMap();
            CreateMap<FracaoListDto, Domain.Estoque.Geral.Fracao>().ReverseMap();
            CreateMap<FracaoListDto, FracaoInput>();
        }
    }
}