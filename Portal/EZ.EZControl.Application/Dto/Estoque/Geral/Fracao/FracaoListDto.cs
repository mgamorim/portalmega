﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;

namespace EZ.EZControl.Dto.Estoque.Geral.Fracao
{
    public class FracaoListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Descricao { get; set; }   
        public int UnidadeMedidaId { get; set; }
        public UnidadeMedidaListDto UnidadeMedida { get; set; }
        public decimal Valor { get; set; }
    }
}
