﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Runtime.Validation;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque
{
    public class GetSaldoEstoqueInput : PagedAndSortedInputDto, IShouldNormalize
    {   
        public int EmpresaId { get; set; }
        public int ProdutoId { get; set; }
        public int? LocalArmazenamentoId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ProdutoId";
            }
        }
    }
}
