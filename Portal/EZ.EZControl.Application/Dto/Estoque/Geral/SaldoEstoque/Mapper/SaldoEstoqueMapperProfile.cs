﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque.Mapper
{
    public class SaldoEstoqueMapperProfile : Profile
    {
        public SaldoEstoqueMapperProfile()
        {                
            CreateMap<SaldoEstoqueListDto, Domain.Estoque.Geral.SaldoEstoque>().ReverseMap();
            CreateMap<SaldoEstoqueListDto, SaldoEstoqueEntradaInput>();
            CreateMap<SaldoEstoqueListDto, SaldoEstoqueSaidaInput>();
            CreateMap<SaldoEstoqueListDto, SaldoEstoquePedidoInput>();
            CreateMap<SaldoEstoqueEntradaInput, Domain.Estoque.Geral.SaldoEstoque>().ReverseMap();
            CreateMap<SaldoEstoqueSaidaInput, Domain.Estoque.Geral.SaldoEstoque>().ReverseMap();
            CreateMap<SaldoEstoquePedidoInput, Domain.Estoque.Geral.SaldoEstoque>().ReverseMap();
        }
    }
}