﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque
{
    public class SaldoEstoqueEntradaInput : EzInputDto, IPassivable
    {
        public SaldoEstoqueEntradaInput()
        {                                   
            IsActive = true;
        }

        public bool IsActive { get; set; }        
        public int? LocalArmazenamentoId { get; set; }
        [Required]
        public int ProdutoId { get; set; }
        [Required]
        public decimal Quantidade { get; set; }
        [Required]
        public int EntradaId { get; set; }
    }
}
