﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Dto.Estoque.Geral.Entrada;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.Estoque.Geral.Saida;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;

namespace EZ.EZControl.Dto.Estoque.Geral.SaldoEstoque
{
    public class SaldoEstoqueListDto : EzFullAuditedInputDto, IPassivable
    {                   
        public bool IsActive { get; set; }
        public int EmpresaId { get; set; }
        public virtual EmpresaListDto Empresa { get; set; }
        //public DateTime DataAtualizacao { get; set; }
        public int? LocalArmazenamentoId { get; set; }
        public virtual LocalArmazenamentoListDto LocalArmazenamento { get; set; }
        public int ProdutoId { get; set; }
        public virtual ProdutoListDto Produto { get; set; }
        public int Quantidade { get; set; }         
        public bool Consolidado { get; set; }
        public int EntradaId { get; set; }
        public virtual EntradaListDto Entrada { get; set; }
        public int SaidaId { get; set; }
        public virtual SaidaListDto Saida { get; set; }
        public int PedidoId { get; set; }
        public virtual PedidoListDto Pedido { get; set; }
        public OrigemMovSaldoEnum OrigemMovSaldo { get; set; }       
    }
}
