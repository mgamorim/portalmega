﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.Saida.Mapper
{
    public class SaidaMapperProfile : Profile
    {
        public SaidaMapperProfile()
        {                
            CreateMap<SaidaInput, Domain.Estoque.Geral.Saida>().ReverseMap();
            CreateMap<SaidaListDto, Domain.Estoque.Geral.Saida>().ReverseMap();
            CreateMap<SaidaListDto, SaidaInput>();
        }
    }
}