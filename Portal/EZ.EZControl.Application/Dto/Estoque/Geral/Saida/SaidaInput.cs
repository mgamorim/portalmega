﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Dto.Estoque.Geral.SaidaItem;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Estoque.Geral.Saida
{
    public class SaidaInput : EzInputDto, IPassivable
    {
        public SaidaInput()
        {
            IsActive = true;
            Items = new List<SaidaItemListDto>();
        }
        public bool IsActive { get; set; }        
        public int EmpresaId { get; set; }
        public int? PedidoId { get; set; }        
        public DateTime DataSaida { get; set; }
        public MotivoSaidaEnum MotivoSaida { get; set; }
        public int Quantidade { get; set; }    
        public virtual IEnumerable<SaidaItemListDto> Items { get; set; }
        public virtual IPagedResult<SaidaItemListDto> GridItens { get; set; }
    }
}
