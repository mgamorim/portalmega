﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Dto.Estoque.Geral.SaidaItem;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.Estoque.Geral.Saida
{
    public class SaidaListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int EmpresaId { get; set; }
        public virtual EmpresaListDto Empresa { get; set; }
        public int? PedidoId { get; set; }
        public virtual PedidoListDto Pedido { get; set; }
        public DateTime DataSaida { get; set; } 
        public MotivoSaidaEnum MotivoSaida { get; set; }
        public IEnumerable<SaidaItemListDto> Items { get; set; }
    }
}
