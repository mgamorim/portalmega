﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.EntradaItem.Mapper
{
    public class EntradaItemMapperProfile : Profile
    {
        public EntradaItemMapperProfile()
        {                
            CreateMap<EntradaItemInput, Domain.Estoque.Geral.EntradaItem>().ReverseMap();
            CreateMap<EntradaItemListDto, Domain.Estoque.Geral.EntradaItem>().ReverseMap();
            CreateMap<EntradaItemListDto, EntradaItemInput>();
        }
    }
}