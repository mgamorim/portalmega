﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.Dto.Estoque.Geral.Produto;

namespace EZ.EZControl.Dto.Estoque.Geral.EntradaItem
{
    public class EntradaItemListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int? LocalArmazenamentoId { get; set; }
        public LocalArmazenamentoListDto LocalArmazenamento { get; set; }
        public int ProdutoId { get; set; }
        public virtual ProdutoListDto Produto { get; set; }
        public int Quantidade { get; set; }
        public DateTime Validade { get; set; }
        public decimal ValorUnitario { get; set; }

        public decimal Total
        {
            get { return (Quantidade * ValorUnitario); }
        }
    }
}
