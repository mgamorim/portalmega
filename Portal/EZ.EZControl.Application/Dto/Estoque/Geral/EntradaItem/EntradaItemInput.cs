﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Estoque.Geral.EntradaItem
{
    public class EntradaItemInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int EmpresaId { get; set; }

        public int? LocalArmazenamentoId { get; set; }

        [Required]
        public int ProdutoId { get; set; }                     

        public int Quantidade { get; set; }

        public DateTime Validade { get; set; }

        public decimal ValorUnitario { get; set; }

        [Required]
        public int EntradaId { get; set; }

    }
}
