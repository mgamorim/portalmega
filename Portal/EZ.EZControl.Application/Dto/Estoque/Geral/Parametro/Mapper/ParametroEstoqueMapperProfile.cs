﻿using EZ.EZControl.Dto.Core.Geral.Parametro.Mapper;

namespace EZ.EZControl.Dto.Estoque.Geral.Parametro.Mapper
{
    public class ParametroEstoqueMapperProfile : ParametroMapperProfile
    {
        public ParametroEstoqueMapperProfile()
        {
            CreateMap<ParametroEstoqueInput, Domain.Estoque.Geral.ParametroEstoque>().ReverseMap();
            CreateMap<ParametroEstoqueListDto, Domain.Estoque.Geral.ParametroEstoque>().ReverseMap();
            CreateMap<ParametroEstoqueListDto, ParametroEstoqueInput>();
        } 
    }
}
