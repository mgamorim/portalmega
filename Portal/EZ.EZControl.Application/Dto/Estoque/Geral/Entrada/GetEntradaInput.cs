﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Runtime.Validation;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Estoque.Geral.Entrada
{
    public class GetEntradaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id { get; set; }                   
        public int? FornecedorId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
