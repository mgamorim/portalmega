﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Dto.Estoque.Geral.LocalArmazenamento;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.Dto.Estoque.Geral.EntradaItem;

namespace EZ.EZControl.Dto.Estoque.Geral.Entrada
{
    public class EntradaListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int EmpresaId { get; set; }
        public EmpresaListDto Empresa { get; set; }
        public DateTime DataPedido { get; set; }                          
        public DateTime DataEntrada { get; set; } 
        public int? FornecedorId { get; set; }
        public FornecedorListDto Fornecedor { get; set; }                
        public string NotaFiscal { get; set; }                            
        public decimal ValorFrete { get; set; } 
        public IEnumerable<EntradaItemListDto> Items { get; set; }
    }
}
