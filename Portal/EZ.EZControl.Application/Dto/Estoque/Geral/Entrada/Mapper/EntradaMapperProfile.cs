﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.Entrada.Mapper
{
    public class EntradaMapperProfile : Profile
    {
        public EntradaMapperProfile()
        {                
            CreateMap<EntradaInput, Domain.Estoque.Geral.Entrada>().ReverseMap();
            CreateMap<EntradaListDto, Domain.Estoque.Geral.Entrada>().ReverseMap();
            CreateMap<EntradaListDto, EntradaInput>();
        }
    }
}