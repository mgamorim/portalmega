﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.Estoque.Geral.EntradaItem;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Estoque.Geral.Entrada
{
    public class EntradaInput : EzInputDto, IPassivable
    {
        public EntradaInput()
        {
            IsActive = true;
            Items = new List<EntradaItemListDto>();
        }
        public bool IsActive { get; set; }
        [Required]        
        public int EmpresaId { get; set; }
        [Required]
        public DateTime DataEntrada { get; set; }
        public DateTime DataPedido { get; set; }
        public int? FornecedorId { get; set; }
        public string NotaFiscal { get; set; }
        public decimal ValorFrete { get; set; }
        public virtual IEnumerable<EntradaItemListDto> Items { get; set; }
        public virtual IPagedResult<EntradaItemListDto> GridItens { get; set; }
    }
}
