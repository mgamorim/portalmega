﻿using AutoMapper;

namespace EZ.EZControl.Dto.Estoque.Geral.Produto.Mapper
{
    public class ProdutoMapperProfile : Profile
    {
        public ProdutoMapperProfile()
        {
            CreateMap<ProdutoInput, Domain.Estoque.Geral.Produto>().ReverseMap();
            CreateMap<ProdutoListDto, Domain.Estoque.Geral.Produto>().ReverseMap();
            CreateMap<ProdutoListDto, ProdutoInput>();
        }
    }
}