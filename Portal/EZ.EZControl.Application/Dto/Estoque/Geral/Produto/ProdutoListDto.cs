﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Dto.Estoque.Geral.Natureza;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Estoque.Geral.Produto
{
    public class ProdutoListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public decimal Valor { get; set; }
        public bool IsValorAjustavel { get; set; }
        public TipoDeProdutoEnum TipoDeProduto { get; set; }
        public int? UnidadeMedidaId { get; set; }
        public UnidadeMedidaListDto UnidadeMedida { get; set; }
        public int? NaturezaId { get; set; }
        public NaturezaListDto Natureza { get; set; }
        public int ImagemId { get; set; }
    }
}
