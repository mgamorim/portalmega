﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZ
{
    public class EzInputDto : EzInputDto<int>
    {
    }

    public class EzInputDtoRecursive : EzInputDto
    {
        public virtual string Nome { get; set; }
        public virtual int? PaiId { get; set; }
    }

    public class EzInputDto<TPrimaryKey> : EntityDto<TPrimaryKey>
    {
        public int ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
        public bool IsIntegration { get; set; }
    }
}
