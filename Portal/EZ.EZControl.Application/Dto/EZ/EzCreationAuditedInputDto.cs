﻿using Abp.Application.Services.Dto;
using System;

namespace EZ.EZControl.Dto.EZ
{
    public class EzCreationAuditedInputDto : CreationAuditedEntityDto
    {
        public int ExternalId { get; set; }
        public DateTime? DateOfEditionIntegration { get; set; }
    }
}
