﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;

namespace EZ.EZControl.Dto.EZ
{
    public class EzFullAuditedInputDto : EzFullAuditedInputDto<int>
    {
    }

    public class EzFullAuditedInputDtoRecursive : EzFullAuditedInputDto
    {
        public virtual string Nome { get; set; }
        public virtual int? PaiId { get; set; }
    }

    public class EzFullAuditedInputDto<TPrimaryKey> : EzInputDto<TPrimaryKey>, IFullAudited
    {
        public long? DeleterUserId { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public long? LastModifierUserId { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
    }
}
