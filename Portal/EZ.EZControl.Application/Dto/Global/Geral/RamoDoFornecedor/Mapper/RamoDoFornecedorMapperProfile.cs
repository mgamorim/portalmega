﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.RamoDoFornecedor.Mapper
{
    public class RamoDoFornecedorMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<RamoDoFornecedorInput, Domain.Global.Geral.RamoDoFornecedor>().ReverseMap();
            CreateMap<RamoDoFornecedorListDto, Domain.Global.Geral.RamoDoFornecedor>().ReverseMap();
            CreateMap<RamoDoFornecedorListDto, RamoDoFornecedorInput>();
        }
    }
}