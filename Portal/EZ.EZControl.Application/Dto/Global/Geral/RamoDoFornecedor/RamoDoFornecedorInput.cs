﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.RamoDoFornecedor
{
    public class RamoDoFornecedorInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Geral.RamoDoFornecedor.NomeMaxLength)]
        public string Nome { get; set; }
    }
}