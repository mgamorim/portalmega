﻿using System;
using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.Parametro.Mapper
{
    public class ParametroGlobalMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ParametroGlobalInput, Domain.Global.Geral.ParametroGlobal>().ReverseMap();
            CreateMap<ParametroGlobalListDto, Domain.Global.Geral.ParametroGlobal>().ReverseMap();
            CreateMap<ParametroGlobalListDto, ParametroGlobalInput>();
        }
    }
}
