﻿using EZ.EZControl.Dto.Core.Geral.Parametro;

namespace EZ.EZControl.Dto.Global.Geral.Parametro
{
    public class ParametroGlobalListDto : ParametroListDto
    {
        public int ConexaoSMTPId { get; set; }
        public string ConexaoSMTPNome { get; set; }
        public string ConexaoSMTPEnderecoDeEmailPadrao { get; set; }
        public string ConexaoSMTPServidorSMTP { get; set; }
        public int ConexaoSMTPPorta { get; set; }
        public string ConexaoSMTPUsuarioAutenticacao { get; set; }
        public string ConexaoSMTPSenhaAutenticacao { get; set; }
        public bool ConexaoSMTPRequerAutenticacao { get; set; }
        public bool ConexaoSMTPRequerConexaoCriptografada { get; set; }
    }
}