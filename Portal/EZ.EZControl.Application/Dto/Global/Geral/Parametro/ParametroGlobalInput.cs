﻿using EZ.EZControl.Dto.Core.Geral.Parametro;

namespace EZ.EZControl.Dto.Global.Geral.Parametro
{
    public class ParametroGlobalInput : ParametroInput
    {
        public int ConexaoSMTPId { get; set; }
    }
}