﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.Arquivo.Mapper
{
    public class ArquivoGlobalMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ArquivoGlobalInput, Domain.Global.Geral.ArquivoGlobal>().ReverseMap();
            CreateMap<ArquivoGlobalListDto, Domain.Global.Geral.ArquivoGlobal>().ReverseMap();
            CreateMap<ArquivoGlobalListDto, ArquivoGlobalInput>();
        }
    }
}
