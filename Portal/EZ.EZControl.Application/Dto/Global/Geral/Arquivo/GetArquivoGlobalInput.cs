﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.Arquivo
{
    public class GetArquivoGlobalInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}