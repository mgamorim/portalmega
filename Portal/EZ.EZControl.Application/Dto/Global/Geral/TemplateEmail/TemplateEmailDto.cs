﻿namespace EZ.EZControl.Dto.Global.Geral.TemplateEmail
{
    public class TemplateEmailDto
    {
        public string Remetente { get; set; }
        public string Assunto { get; set; }
        public string CorpoMensagem { get; set; }
    }
}
