﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.TemplateEmail
{
    public class TemplateEmailListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Geral.TemplateEmail.NomeMaxLength)]
        public string Nome { get; set; }

        [StringLength(Domain.Global.Geral.TemplateEmail.RemetenteMaxLength)]
        public string Remetente { get; set; }

        [StringLength(Domain.Global.Geral.TemplateEmail.AssuntoMaxLength)]
        public string Assunto { get; set; }

        public string CorpoMensagem { get; set; }

        [Required]
        public TipoDeTemplateDeMensagemEnum TipoDeTemplateDeMensagem { get; set; }
    }
}