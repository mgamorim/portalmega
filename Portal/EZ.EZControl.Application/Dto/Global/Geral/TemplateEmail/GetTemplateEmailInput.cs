﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.TemplateEmail
{
    public class GetTemplateEmailInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public string Remetente { get; set; }

        public string Assunto { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}