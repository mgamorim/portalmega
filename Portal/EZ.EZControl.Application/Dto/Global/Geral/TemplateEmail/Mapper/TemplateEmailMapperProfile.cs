﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.TemplateEmail.Mapper
{
    public class TemplateEmailMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TemplateEmailInput, Domain.Global.Geral.TemplateEmail>().ReverseMap();
            CreateMap<TemplateEmailListDto, Domain.Global.Geral.TemplateEmail>().ReverseMap();
            CreateMap<TemplateEmailListDto, TemplateEmailInput>();
        }
    }
}
