﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.Profissao
{
    public class GetProfissaoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Codigo { get; set; }
        public string Titulo { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Codigo";
            }
        }
    }
}