﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Profissao
{
    public class ProfissaoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Geral.Profissao.CodigoMaxLength)]
        public string Codigo { get; set; }

        [StringLength(Domain.Global.Geral.Profissao.TituloMaxLength)]
        public string Titulo { get; set; }
    }
}