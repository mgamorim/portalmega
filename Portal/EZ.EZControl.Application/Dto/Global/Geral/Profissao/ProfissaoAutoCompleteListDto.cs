﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Profissao
{
    public class ProfissaoAutoCompleteListDto : EzFullAuditedInputDto
    {
        public string Value { get; set; }
        public string Display { get; set; }
    }
}