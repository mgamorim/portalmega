﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.Global.Geral.Profissao.Mapper
{
    public class ProfissaoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ProfissaoInput, Domain.Global.Geral.Profissao>().ReverseMap();
            CreateMap<ProfissaoListDto, Domain.Global.Geral.Profissao>().ReverseMap();
            CreateMap<Domain.Global.Geral.Profissao, ProfissaoAutoCompleteListDto>()
                .ForMember("Display", x => x.MapFrom("Titulo"))
                .ForMember("Value", x => x.MapFrom("Id"));
            CreateMap<ProfissaoListDto, ProfissaoInput>();
        }
    }
}
