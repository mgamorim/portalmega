﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Profissao
{
    public class ProfissaoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Codigo { get; set; }
        public string Titulo { get; set; }
    }
}