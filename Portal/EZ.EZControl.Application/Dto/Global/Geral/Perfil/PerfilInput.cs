﻿using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.Global.Geral.Banco;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.Global.Geral.Perfil
{
    public class PerfilInput : PessoaFisicaInput
    {
        public PerfilInput() => this.IsCorretor = false;

        public TipoDeUsuarioEnum TipoDeUsuario { get; set; }

        public CorretorDadosBancarioInput CorretorDadosBancario { get; set; }
        public CorretorPessoaJuridcaInput CorretorPessoaJuridica { get; set; }

        public ICollection<BancoInput> Bancos { get; set; }

        public int CorretorConectadoId { get; set; }

        public bool IsCorretor { get; set; } 
    }
       
}