﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.Perfil
{
    public class GetPerfilInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
