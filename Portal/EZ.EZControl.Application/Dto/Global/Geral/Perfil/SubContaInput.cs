﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.Global.Geral.Perfil
{
    public  class SubContaInput
    {
        public data data { get; set; }
    }



    public class data
    {
        public string business_type { get; set; }
        public string person_type { get; set; }
        public string cnpj { get; set; }
        public string cpf { get; set; }
        public string company_name { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string cep { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string telephone { get; set; }
        public string bank { get; set; }
        public string bank_ag { get; set; }
        public string account_type { get; set; }
        public string bank_cc { get; set; }
    }


}
