﻿using AutoMapper;
using EZ.EZControl.Domain.Global.Pessoa;

namespace EZ.EZControl.Dto.Global.Geral.Perfil.Mapper
{
    public class PerfilMapperProfile : Profile
    {
        public PerfilMapperProfile()
        {
            CreateMap<PerfilInput, PessoaFisica>()
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<PerfilListDto, PessoaFisica>().ReverseMap();
            CreateMap<PerfilListDto, PerfilInput>();
        }
    }
}
