﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.ConexaoSMTP.Mapper
{
    public class ConexaoSMTPMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ConexaoSMTPInput, Domain.Global.Geral.ConexaoSMTP>().ReverseMap();
            CreateMap<ConexaoSMTPListDto, Domain.Global.Geral.ConexaoSMTP>().ReverseMap();
            CreateMap<ConexaoSMTPListDto, ConexaoSMTPInput>();
        }
    }
}
