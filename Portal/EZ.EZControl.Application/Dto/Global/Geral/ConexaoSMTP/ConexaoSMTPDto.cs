﻿using System;

namespace EZ.EZControl.Dto.Global.Geral.ConexaoSMTP
{
    public class ConexaoSMTPDto
    {
        public String ServidorSMTP { get; set; }
        public Int32 Porta { get; set; }
        public String UsuarioAutenticacao { get; set; }
        public String SenhaAutenticacao { get; set; }
        public Boolean RequerAutenticacao { get; set; }
        public Boolean RequerConexaoCriptografada { get; set; }
    }
}
