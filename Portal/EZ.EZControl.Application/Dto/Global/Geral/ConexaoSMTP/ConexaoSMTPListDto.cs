﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.ConexaoSMTP
{
    public class ConexaoSMTPListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Geral.ConexaoSMTP.NomeMaxLength)]
        public string Nome { get; set; }

        [StringLength(Domain.Global.Geral.ConexaoSMTP.EnderecoDeEmailPadraoMaxLength)]
        public string EnderecoDeEmailPadrao { get; set; }

        [StringLength(Domain.Global.Geral.ConexaoSMTP.ServidorSMTPMaxLength)]
        public string ServidorSMTP { get; set; }

        public int Porta { get; set; }

        [StringLength(Domain.Global.Geral.ConexaoSMTP.UsuarioAutenticacaoMaxLength)]
        public string UsuarioAutenticacao { get; set; }

        [StringLength(Domain.Global.Geral.ConexaoSMTP.SenhaAutenticacaoMaxLength)]
        public string SenhaAutenticacao { get; set; }

        public bool RequerAutenticacao { get; set; }
        public bool RequerConexaoCriptografada { get; set; }
    }
}