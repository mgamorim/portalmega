﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.ConexaoSMTP
{
    public class GetConexaoSMTPInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string ServidorSMTP { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}