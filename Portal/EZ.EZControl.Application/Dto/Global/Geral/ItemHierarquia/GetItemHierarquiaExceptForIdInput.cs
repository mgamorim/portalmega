﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.ItemHierarquia
{
    public class GetItemHierarquiaExceptForIdInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int ItemHierarquiaId { get; set; }

        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}