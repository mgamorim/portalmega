﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.ItemHierarquia
{
    public class ItemHierarquiaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string Mascara { get; set; }
        public int ItemHierarquiaPaiId { get; set; }
    }
}