﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.ItemHierarquia
{
    public class ItemHierarquiaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [Required]
        [StringLength(Domain.Global.Geral.ItemHierarquia.NomeMaxLength)]
        public string Nome { get; set; }
        [Required]
        public string Codigo { get; set; }
        [Required]
        [StringLength(Domain.Global.Geral.ItemHierarquia.MascaraMaxLength)]
        public string Mascara { get; set; }
        public int? ItemHierarquiaPaiId { get; set; }
    }
}