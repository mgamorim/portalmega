﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.ItemHierarquia
{
    public class GetItemHierarquiaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public string Codigo { get; set; }

        public string Mascara { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}