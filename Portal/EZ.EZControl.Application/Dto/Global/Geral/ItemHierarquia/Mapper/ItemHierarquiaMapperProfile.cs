﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.Global.Geral.ItemHierarquia.Mapper
{
    public class ItemHierarquiaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ItemHierarquiaInput, Domain.Global.Geral.ItemHierarquia>().ReverseMap();
            CreateMap<ItemHierarquiaListDto, Domain.Global.Geral.ItemHierarquia>().ReverseMap();
            CreateMap<ItemHierarquiaListDto, ItemHierarquiaInput>();
        }
    }
}
