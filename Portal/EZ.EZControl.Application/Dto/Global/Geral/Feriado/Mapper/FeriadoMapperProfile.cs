﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.Global.Geral.Feriado.Mapper
{
    public class FeriadoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<FeriadoInput, Domain.Global.Geral.Feriado>().ReverseMap();
            CreateMap<FeriadoListDto, Domain.Global.Geral.Feriado>().ReverseMap();
            CreateMap<FeriadoListDto, FeriadoInput>();
        }
    }
}
