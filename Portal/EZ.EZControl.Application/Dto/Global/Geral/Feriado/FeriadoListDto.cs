﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.Global.Geral.Feriado
{
    public class FeriadoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public DateTime Data { get; set; }
        public AbrangenciaDoFeriadoEnum Abrangencia { get; set; }
        public TipoDeFeriadoEnum Tipo { get; set; }
        public int PaisId { get; set; }
        public int EstadoId { get; set; }
        public int CidadeId { get; set; }
    }
}