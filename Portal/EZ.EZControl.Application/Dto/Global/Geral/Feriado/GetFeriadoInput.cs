﻿using Abp.Runtime.Validation;
using System;

namespace EZ.EZControl.Dto.Global.Geral.Feriado
{
    public class GetFeriadoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public DateTime Data { get; set; }
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}