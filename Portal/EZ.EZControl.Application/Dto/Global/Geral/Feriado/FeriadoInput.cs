﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Feriado
{
    public class FeriadoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [Required]
        [MaxLength(EZControl.Domain.Global.Geral.Feriado.NomeMaxLength)]
        public string Nome { get; set; }
        [Required]
        public DateTime Data { get; set; }
        [Required]
        public AbrangenciaDoFeriadoEnum Abrangencia { get; set; }
        [Required]
        public TipoDeFeriadoEnum Tipo { get; set; }
        public int? PaisId { get; set; }
        public int? EstadoId { get; set; }
        public int? CidadeId { get; set; }
    }
}