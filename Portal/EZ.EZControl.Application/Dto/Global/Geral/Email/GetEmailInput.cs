﻿using Abp.Runtime.Validation;
using System;

namespace EZ.EZControl.Dto.Global.Geral.Email
{
    public class GetEmailInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public String Descricao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}
