﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Email
{
    public class EmailListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.Global.Geral.TemplateEmail.RemetenteMaxLength)]
        public string Destinatario { get; set; }
        [Required]
        public int SmtpId { get; set; }
        public int TemplateId { get; set; }
        public int ParametroId { get; set; }
    }
}
