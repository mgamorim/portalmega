﻿using AutoMapper;
using EZ.EZControl.EZ.EzNotification;

namespace EZ.EZControl.Dto.Global.Geral.Email.Mapper
{
    public class EmailMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<EmailInput, EmailNotification>().ReverseMap();
            CreateMap<EmailListDto, EmailNotification>().ReverseMap();
            CreateMap<EmailListDto, EmailInput>();
        }
    }
}
