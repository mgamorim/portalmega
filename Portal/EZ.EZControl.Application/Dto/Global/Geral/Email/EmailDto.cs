﻿using EZ.EZControl.Dto.Global.Geral.ConexaoSMTP;
using EZ.EZControl.Dto.Global.Geral.TemplateEmail;

namespace EZ.EZControl.Dto.Global.Geral.Email
{
    public class EmailDto
    {
        public string Destinatario { get; set; }
        public ConexaoSMTPDto SmtpDto { get; set; }
        public TemplateEmailDto TemplateDeEmail { get; set; }
    }
}
