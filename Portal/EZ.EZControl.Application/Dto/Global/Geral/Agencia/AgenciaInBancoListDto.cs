﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Agencia
{
    [AutoMapFrom(typeof(Domain.Global.Geral.Agencia))]
    public class AgenciaInBancoListDto : EzCreationAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int BancoId { get; set; }
        public int Codigo { get; set; }
        public string DigitoVerificador { get; set; }
        public string Nome { get; set; }
    }
}