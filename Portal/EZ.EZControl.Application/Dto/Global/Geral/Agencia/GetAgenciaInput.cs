﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.Agencia
{
    public class GetAgenciaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string CodigoBanco { get; set; }
        public string NomeBanco { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }


        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}