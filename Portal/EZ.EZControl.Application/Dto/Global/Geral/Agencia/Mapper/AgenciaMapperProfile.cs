﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.Agencia.Mapper
{
    public class AgenciaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<AgenciaInput, Domain.Global.Geral.Agencia>().ReverseMap();
            CreateMap<AgenciaListDto, Domain.Global.Geral.Agencia>().ReverseMap();
            CreateMap<AgenciaListDto, AgenciaInput>();
        }
    }
}
