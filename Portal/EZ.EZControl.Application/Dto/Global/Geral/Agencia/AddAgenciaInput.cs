﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Agencia
{
    [AutoMapTo(typeof(Domain.Global.Geral.Agencia))]
    public class AddAgenciaInput
    {
        [Required]
        public bool IsActive { get; set; }

        public int BancoId { get; set; }
        public int Codigo { get; set; }
        public string DigitoVerificador { get; set; }
        public string Nome { get; set; }
    }
}