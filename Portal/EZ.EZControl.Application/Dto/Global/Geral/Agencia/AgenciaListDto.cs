﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Agencia
{
    public class AgenciaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string DigitoVerificador { get; set; }
        public int BancoId { get; set; }
    }
}