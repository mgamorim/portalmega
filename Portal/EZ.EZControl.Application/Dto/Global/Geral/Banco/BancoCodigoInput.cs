﻿using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Banco
{
    public class BancoCodigoInput
    {
        [Required]
        [MaxLength(Domain.Global.Geral.Banco.CodigoMaxLength)]
        public string Codigo { get; set; }
    }
}