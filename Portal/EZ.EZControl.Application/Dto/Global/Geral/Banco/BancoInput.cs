﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Banco
{
    public class BancoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [Required]
        [MaxLength(Domain.Global.Geral.Banco.NomeMaxLength)]
        public string Nome { get; set; }
        [Required]
        [MaxLength(Domain.Global.Geral.Banco.CodigoMaxLength)]
        public string Codigo { get; set; }
    }
}