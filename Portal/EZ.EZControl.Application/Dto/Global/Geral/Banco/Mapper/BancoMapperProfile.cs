﻿using System;
using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.Banco.Mapper
{
    public class BancoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<BancoInput, Domain.Global.Geral.Banco>().ReverseMap();
            CreateMap<BancoListDto, Domain.Global.Geral.Banco>().ReverseMap();
            CreateMap<BancoListDto, BancoInput>();
        }
    }
}
