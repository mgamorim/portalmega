﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.Banco
{
    public class GetBancoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}