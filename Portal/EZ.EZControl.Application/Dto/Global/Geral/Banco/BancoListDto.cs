﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Banco
{
    public class BancoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
    }
}