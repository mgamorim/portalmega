﻿using System;
using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.Relatorio.Mapper
{
    public class RelatorioMapperProfile : Profile
    {
        public RelatorioMapperProfile()
        {
            CreateMap<RelatorioInput, Domain.Global.Geral.Relatorio>().ReverseMap();
            CreateMap<RelatorioListDto, Domain.Global.Geral.Relatorio>().ReverseMap();
            CreateMap<RelatorioListDto, RelatorioInput>();
        }
    }
}
