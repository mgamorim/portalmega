﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Relatorio
{
    public class RelatorioListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public SistemaEnum Sistema { get; set; }
        public int TipoDeRelatorio { get; set; }
        public string Conteudo { get; set; }
    }
}