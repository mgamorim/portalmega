﻿using Abp.Runtime.Validation;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Relatorio
{ 
    public class GetRelatorioExceptForIdInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int RelatorioId { get; set; }

        public string Nome { get; set; }

        public int TipoDeRelatorio { get; set; }

        public virtual ICollection<RelatorioInput> Relatorios { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
