﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Relatorio
{
    public class RelatorioInput : EzFullAuditedInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public SistemaEnum Sistema { get; set; }

        [Required]
        public int TipoDeRelatorio { get; set; }

        public string Conteudo { get; set; }
    }
}