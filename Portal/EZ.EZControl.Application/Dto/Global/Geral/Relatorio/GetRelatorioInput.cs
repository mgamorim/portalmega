﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Core.Enums;

namespace EZ.EZControl.Dto.Global.Geral.Relatorio
{
    public class GetRelatorioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public SistemaEnum Sistema { get; set; }
        public int TipoDeRelatorio { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}