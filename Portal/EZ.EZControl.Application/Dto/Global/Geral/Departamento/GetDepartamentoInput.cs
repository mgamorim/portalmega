﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Geral.Departamento
{
    public class GetDepartamentoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}