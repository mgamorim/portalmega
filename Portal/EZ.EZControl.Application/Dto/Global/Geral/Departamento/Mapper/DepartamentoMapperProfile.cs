﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Geral.Departamento.Mapper
{
    public class DepartamentoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DepartamentoInput, Domain.Global.Geral.Departamento>().ReverseMap();
            CreateMap<DepartamentoListDto, Domain.Global.Geral.Departamento>().ReverseMap();
            CreateMap<DepartamentoListDto, DepartamentoInput>();
        }
    }
}
