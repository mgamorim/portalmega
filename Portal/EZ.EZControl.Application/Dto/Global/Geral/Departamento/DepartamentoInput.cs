﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Geral.Departamento
{
    public class DepartamentoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Geral.Departamento.NomeMaxLength)]
        public string Nome { get; set; }

        [StringLength(Domain.Global.Geral.Departamento.DescricaoMaxLength)]
        public string Descricao { get; set; }
    }
}