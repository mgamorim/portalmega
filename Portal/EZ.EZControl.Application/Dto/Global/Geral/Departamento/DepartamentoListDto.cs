﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Geral.Departamento
{
    public class DepartamentoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}