﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class EmpresaNomeListDto : EzInputDto
    {
        public string NomeFantasia { get; set; }
    }
}   