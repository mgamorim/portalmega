﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class GetEmpresaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomeFantasia { get; set; }

        public string Cnpj { get; set; }
        public string Slug { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}