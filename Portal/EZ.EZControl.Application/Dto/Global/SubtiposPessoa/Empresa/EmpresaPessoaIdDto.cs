﻿namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class EmpresaPessoaIdDto
    {
        public int EmpresaId { get; set; }

        public int PessoaId { get; set; }
    }
}