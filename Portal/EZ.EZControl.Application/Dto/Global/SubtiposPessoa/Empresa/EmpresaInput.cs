﻿using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class EmpresaInput : EzInputDto
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public string RazaoSocial { get; set; }

        [Required]
        public string NomeFantasia { get; set; }

        public string Slug { get; set; }

        [Required]
        public string Cnpj { get; set; }

        public virtual int? PessoaJuridicaId { get; set; }

        [Required]
        public int ClienteEzId { get; set; }
    }
}