﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class GetPessoaExceptForEmpresa : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomePessoa { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}