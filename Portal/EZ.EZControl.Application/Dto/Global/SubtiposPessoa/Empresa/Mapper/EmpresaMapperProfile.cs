﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa.Mapper
{
    public class EmpresaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Domain.Global.SubtiposPessoa.Empresa, EmpresaNomeListDto>().ReverseMap();

            CreateMap<EmpresaInput, Domain.Global.SubtiposPessoa.Empresa>().ReverseMap();

            CreateMap<EmpresaListDto, Domain.Global.SubtiposPessoa.Empresa>().ReverseMap();
            CreateMap<EmpresaListDto, EmpresaInput>();
        }
    }
}