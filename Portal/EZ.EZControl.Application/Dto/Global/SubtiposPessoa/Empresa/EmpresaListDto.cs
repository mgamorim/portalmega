﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class EmpresaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int ClienteEzId { get; set; }

        public string RazaoSocial { get; set; }

        public string NomeFantasia { get; set; }

        public string Slug { get; set; }

        public string Cnpj { get; set; }
    }
}