﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa
{
    public class AssociacaoUsuarioEmpresaInput : EzInputDto
    {
        public int EmpresaId { get; set; }
        public int UsuarioId { get; set; }
    }
}
