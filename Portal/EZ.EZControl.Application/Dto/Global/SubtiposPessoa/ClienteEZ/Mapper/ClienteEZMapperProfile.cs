﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ.Mapper
{
    public class ClienteEZMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ClienteEZDto, Domain.Global.SubtiposPessoa.ClienteEZ>().ReverseMap();
        }
    }
}