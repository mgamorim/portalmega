﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ
{
    public class ClienteEZDto : EzInputDto
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public string Nome { get; set; }

        public string RazaoSocial { get; set; }

        public string DocumentoPrincipal { get; set; }
    }
}