﻿namespace EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ
{
    public class ClienteEZPessoaIdDto
    {
        public int ClienteEZId { get; set; }

        public int PessoaId { get; set; }
    }
}