﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa
{
    public class GetUsuarioPorPessoaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "User.Name";
            }
        }
    }
}