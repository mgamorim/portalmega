﻿namespace EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa
{
    public class UsuarioPorPessoaIdDto
    {
        public int UsuarioPorPessoaId { get; set; }

        public int UsuarioId { get; set; }

        public int PessoaId { get; set; }
    }
}