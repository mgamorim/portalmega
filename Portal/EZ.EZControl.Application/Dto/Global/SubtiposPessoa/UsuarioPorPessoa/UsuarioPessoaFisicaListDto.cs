﻿using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa
{
    public class UsuarioPessoaFisicaListDto : PessoaFisicaListDto
    {
        public string EmailPessoal { get; set; }

        public string Cpf { get; set; }

        public string Rg { get; set; }
    }
}