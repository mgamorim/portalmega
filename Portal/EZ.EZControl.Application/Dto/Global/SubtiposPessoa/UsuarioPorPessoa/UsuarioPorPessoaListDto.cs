﻿using Abp.Domain.Entities;
using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa
{
    public class UsuarioPorPessoaListDto : EzFullAuditedInputDto, IPassivable
    {
        [Required]
        public UserListDto User { get; set; }

        [Required]
        public string RoleName { get; set; }

        public bool SendActivationEmail { get; set; }

        public bool SetRandomPassword { get; set; }

        public bool IsActive { get; set; }
    }
}