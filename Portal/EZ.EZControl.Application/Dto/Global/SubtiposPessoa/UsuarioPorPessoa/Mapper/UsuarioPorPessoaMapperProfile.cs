﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa.Mapper
{
    public class UsuarioPorPessoaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<UsuarioPorPessoaInput, Domain.Global.SubtiposPessoa.UsuarioPorPessoa>().ReverseMap();
            CreateMap<UsuarioPorPessoaListDto, Domain.Global.SubtiposPessoa.UsuarioPorPessoa>().ReverseMap();
            CreateMap<UsuarioPorPessoaListDto, UsuarioPorPessoaInput>();
        }
    }
}