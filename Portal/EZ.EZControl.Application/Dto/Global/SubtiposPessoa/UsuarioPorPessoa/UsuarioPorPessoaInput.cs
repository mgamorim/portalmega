﻿using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa
{
    public class UsuarioPorPessoaInput : EzInputDto
    {
        [Required]
        public UserEditDto User { get; set; }

        public int UserId { get; set; }

        [Required]
        public string RoleName { get; set; }

        public bool SendActivationEmail { get; set; }

        public bool SetRandomPassword { get; set; }
    }
}