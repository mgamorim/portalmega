﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor
{
    public class FornecedorInput : EzInputDto
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public PessoaInput Pessoa { get; set; }

        public PessoaFisicaInputAux PessoaFisica { get; set; }

        public PessoaJuridicaInputAux PessoaJuridica { get; set; }
    }
}