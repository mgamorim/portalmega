﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor
{
    public class FornecedorListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public PessoaListDto Pessoa { get; set; }

        public PessoaFisicaListDto PessoaFisica { get; set; }

        public PessoaJuridicaListDto PessoaJuridica { get; set; }
    }
}