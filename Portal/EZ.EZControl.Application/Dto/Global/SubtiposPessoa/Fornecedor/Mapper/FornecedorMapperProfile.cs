﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor.Mapper
{
    public class FornecedorMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<FornecedorInput, Domain.Global.SubtiposPessoa.Fornecedor>()
                .ForMember(destination => destination.RamosDoFornecedor, opt => opt.Ignore())
                .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
                .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<FornecedorListDto, Domain.Global.SubtiposPessoa.Fornecedor>().ReverseMap();
            CreateMap<FornecedorListDto, FornecedorInput>();
        }
    }
}