﻿namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor
{
    public class FornecedorPessoaIdDto
    {
        public int FornecedorId { get; set; }

        public int PessoaId { get; set; }
    }
}