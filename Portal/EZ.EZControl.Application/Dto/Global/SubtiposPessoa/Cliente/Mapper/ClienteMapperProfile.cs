﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente.Mapper
{
    public class ClienteMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ClienteInput, Domain.Global.SubtiposPessoa.Cliente>()
                .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
                .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<ClienteListDto, Domain.Global.SubtiposPessoa.Cliente>().ReverseMap();
            CreateMap<ClienteListDto, ClienteInput>();
        }
    }
}