﻿namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente
{
    public class ClientePessoaIdDto
    {
        public int ClienteId { get; set; }

        public int PessoaId { get; set; }
    }
}