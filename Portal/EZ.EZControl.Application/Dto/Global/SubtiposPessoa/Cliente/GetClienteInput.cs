﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente
{
    public class GetClienteInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public string NumeroDocumento { get; set; }

        public TipoDeDocumentoEnum TipoDeDocumento { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}