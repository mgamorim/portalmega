﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeContato.Mapper
{
    public class TipoDeContatoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TipoDeContatoInput, Domain.Global.Pessoa.TipoDeContato>().ReverseMap();
            CreateMap<TipoDeContatoListDto, Domain.Global.Pessoa.TipoDeContato>().ReverseMap();
            CreateMap<TipoDeContatoListDto, TipoDeContatoInput>();
        }
    }
}