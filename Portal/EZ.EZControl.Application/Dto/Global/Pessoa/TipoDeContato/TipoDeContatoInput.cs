﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeContato
{
    public class TipoDeContatoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [Required]
        public TipoDeContatoEnum? TipoDeContatoFixo { get; set; }

        [MaxLength(Domain.Global.Pessoa.TipoDeContato.MaxMascaraLength)]
        public string Mascara { get; set; }

        [StringLength(Domain.Global.Pessoa.TipoDeContato.MaxDescricaoLength)]
        public string Descricao { get; set; }
    }
}