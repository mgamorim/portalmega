﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeContato
{
    public class GetTipoDeContatoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}