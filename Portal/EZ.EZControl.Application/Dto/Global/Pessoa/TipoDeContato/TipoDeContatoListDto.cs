﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeContato
{
    public class TipoDeContatoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        public string Mascara { get; set; }

        public TipoDeContatoEnum TipoDeContatoFixo { get; set; }
    }
}