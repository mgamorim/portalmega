﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Dto.Global.Pessoa.Documento;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaListDto : EzFullAuditedInputDto
    {
        public string NomePessoa { get; set; }

        public DocumentoListDto DocumentoPrincipal { get; set; }

        public int DocumentoPrincipalId { get; set; }

        public GrupoPessoaListDto GrupoPessoa { get; set; }

        public int GrupoPessoaId { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public string Observacao { get; set; }
        public EnderecoInput EnderecoPrincipal { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
    }
}