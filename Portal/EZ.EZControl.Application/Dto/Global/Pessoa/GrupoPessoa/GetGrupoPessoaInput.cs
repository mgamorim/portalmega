﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa
{
    public class GetGrupoPessoaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}