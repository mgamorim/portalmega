﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa.Mapper
{
    public class GrupoPessoaMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<GrupoPessoaInput, Domain.Global.Pessoa.GrupoPessoa>().ReverseMap();
            CreateMap<GrupoPessoaListDto, Domain.Global.Pessoa.GrupoPessoa>().ReverseMap();
            CreateMap<GrupoPessoaListDto, GrupoPessoaInput>();
        }
    }
}