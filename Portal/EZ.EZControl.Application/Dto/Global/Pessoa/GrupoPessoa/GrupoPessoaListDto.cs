﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa
{
    public class GrupoPessoaListDto : EzFullAuditedInputDtoRecursive, IPassivable
    {
        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        [MaxLength(Domain.Global.Pessoa.GrupoPessoa.MaxNomeLength)]
        public override string Nome { get; set; }

        public TipoPessoaEnum TipoDePessoa { get; set; }
        public string Slug { get; set; }
    }
}