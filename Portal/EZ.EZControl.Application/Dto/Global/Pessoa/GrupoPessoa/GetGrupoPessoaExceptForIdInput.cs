﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa
{
    public class GetGrupoPessoaExceptForIdInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int GrupoPessoaId { get; set; }

        public string Nome { get; set; }

        public TipoPessoaEnum TipoDePessoa { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}