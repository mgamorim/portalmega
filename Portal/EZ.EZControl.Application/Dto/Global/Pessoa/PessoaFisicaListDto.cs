﻿using EZ.EZControl.Domain.Global.Enums;
using System;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaFisicaListDto : PessoaListDto
    {
        public string Nome { get; set; }

        public string Nacionalidade { get; set; }

        public int TratamentoId { get; set; }

        public SexoEnum? Sexo { get; set; }

        public bool PossuiFilhos { get; set; }

        public EstadoCivilEnum? EstadoCivil { get; set; }

        public DateTime? DataDeNascimento { get; set; }

    }
}