﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaFisicaInputAux
    {
        [MaxLength(PessoaFisica.MaxNomeLength)]
        public string Nome { get; set; }

        public int TratamentoId { get; set; }

        public SexoEnum? Sexo { get; set; }

        public bool PossuiFilhos { get; set; }

        public EstadoCivilEnum? EstadoCivil { get; set; }

        public DateTime? DataDeNascimento { get; set; }
    }
}