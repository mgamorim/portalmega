﻿namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaJuridicaListDto : PessoaListDto
    {
        public string RazaoSocial { get; set; }

        public string NomeFantasia { get; set; }
    }
}