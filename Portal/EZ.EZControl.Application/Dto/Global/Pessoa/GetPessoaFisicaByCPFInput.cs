﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class GetPessoaFisicaByCPFInput: PagedAndSortedInputDto, IShouldNormalize
    {
       public string CPFPessoaFisica { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "CPFPessoaFisica";
            }
        }
    }
}
