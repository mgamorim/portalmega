﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaJuridicaInputAux
    {
        [MaxLength(PessoaJuridica.MaxRazaoSocialLength)]
        public string RazaoSocial { get; set; }

        [MaxLength(PessoaJuridica.MaxNomeFantasiaLength)]
        public string NomeFantasia { get; set; }
    }
}