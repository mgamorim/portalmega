﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento;
using System;

namespace EZ.EZControl.Dto.Global.Pessoa.Documento
{
    public class DocumentoListDto : EzFullAuditedInputDto
    {
        public int PessoaId { get; set; }

        public PessoaListDto Pessoa { get; set; }

        public int TipoDeDocumentoId { get; set; }

        public TipoDeDocumentoListDto TipoDeDocumento { get; set; }

        public string Numero { get; set; }

        public string OrgaoExpedidor { get; set; }

        public DateTime? DataExpedicao { get; set; }

        public int? EstadoId { get; set; }

        public EstadoListDto Estado { get; set; }

        public DateTime? DataDeValidade { get; set; }

        public string Serie { get; set; }
    }
}