﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Documento
{
    public class GetDocumentoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int PessoaId { get; set; }

        [MaxLength(Domain.Global.Pessoa.Documento.MaxNumeroLength)]
        public string Numero { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Numero";
            }
        }
    }
}