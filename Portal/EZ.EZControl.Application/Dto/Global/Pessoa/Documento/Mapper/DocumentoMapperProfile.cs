﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.Documento.Mapper
{
    public class DocumentoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DocumentoInput, Domain.Global.Pessoa.Documento>().ReverseMap().PreserveReferences();
            CreateMap<DocumentoListDto, Domain.Global.Pessoa.Documento>().ReverseMap().PreserveReferences();
            CreateMap<DocumentoListDto, DocumentoInput>();
        }
    }
}