﻿using EZ.EZControl.Dto.EZ;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Documento
{
    public class DocumentoInput : EzInputDto
    {
        [Required]
        public int PessoaId { get; set; }

        [Required]
        public int TipoDeDocumentoId { get; set; }

        [MaxLength(Domain.Global.Pessoa.Documento.MaxNumeroLength)]
        public string Numero { get; set; }

        [MaxLength(Domain.Global.Pessoa.Documento.MaxOrgaoExpedidorLength)]
        public string OrgaoExpedidor { get; set; }

        public DateTime? DataExpedicao { get; set; }

        public int? EstadoId { get; set; }

        public DateTime? DataDeValidade { get; set; }

        [MaxLength(Domain.Global.Pessoa.Documento.MaxSerieLength)]
        public string Serie { get; set; }

    }
}