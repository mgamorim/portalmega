﻿using EZ.EZControl.Domain.Global.Pessoa;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaJuridicaInput : PessoaInput
    {
        [MaxLength(PessoaJuridica.MaxRazaoSocialLength)]
        public string RazaoSocial { get; set; }

        [MaxLength(PessoaJuridica.MaxNomeFantasiaLength)]
        public string NomeFantasia { get; set; }
    }
}