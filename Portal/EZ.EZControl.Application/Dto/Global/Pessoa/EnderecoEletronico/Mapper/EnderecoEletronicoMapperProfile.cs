﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico.Mapper
{
    public class EnderecoEletronicoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<EnderecoEletronicoInput, Domain.Global.Pessoa.EnderecoEletronico>().ReverseMap().PreserveReferences(); ;
            CreateMap<EnderecoEletronicoListDto, Domain.Global.Pessoa.EnderecoEletronico>().ReverseMap().PreserveReferences(); ;
            CreateMap<EnderecoEletronicoListDto, EnderecoEletronicoInput>();
        }
    }
}