﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico
{
    public class GetEnderecoEletronicoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int PessoaId { get; set; }

        [MaxLength(Domain.Global.Pessoa.EnderecoEletronico.MaxEnderecoLength)]
        public string Endereco { get; set; }

        public TipoDeEnderecoEletronicoEnum? TipoDeEnderecoEletronico { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Endereco";
            }
        }
    }
}