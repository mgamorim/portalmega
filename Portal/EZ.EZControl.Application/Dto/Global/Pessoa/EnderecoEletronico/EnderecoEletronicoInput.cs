﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico
{
    public class EnderecoEletronicoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [Required]
        public int PessoaId { get; set; }

        [Required]
        [StringLength(Domain.Global.Pessoa.EnderecoEletronico.MaxEnderecoLength)]
        public string Endereco { get; set; }

        [Required]
        public TipoDeEnderecoEletronicoEnum TipoDeEnderecoEletronico { get; set; }

        public int? ContatoId { get; set; }
    }
}