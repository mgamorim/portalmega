﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico
{
    public class EnderecoEletronicoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int PessoaId { get; set; }

        public PessoaListDto Pessoa { get; set; }

        public string Endereco { get; set; }

        public TipoDeEnderecoEletronicoEnum TipoDeEnderecoEletronico { get; set; }
    }
}