﻿using Abp.Domain.Entities;
//using EZ.EZControl.CMS.Frontend.Infra.Dtos;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaFisicaByCPFListDto : EzFullAuditedInputDto//PessoaListDto //EzFullAuditedInputDto
    {

        //public int Id { get; set; }

        public string Nome { get; set; }

        public string CPF { get; set; }

        public int CpfId { get; set; }

        public int GrupoPessoaId { get; set; }

        public string GrupoPessoaNome { get; set; }
    }
}
