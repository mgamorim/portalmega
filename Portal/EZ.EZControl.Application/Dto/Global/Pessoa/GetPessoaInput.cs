﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class GetPessoaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public string DescricaoGrupoPessoa { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}