﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.Telefone.Mapper
{
    public class TelefoneMapperProfile : Profile
    {
        public  TelefoneMapperProfile()
        {
            CreateMap<TelefoneInput, Domain.Global.Pessoa.Telefone>().ReverseMap().PreserveReferences(); ;
            CreateMap<TelefoneListDto, Domain.Global.Pessoa.Telefone>().ReverseMap().PreserveReferences(); ;
            CreateMap<TelefoneListDto, TelefoneInput>();
        }
    }
}