﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Telefone
{
    public class TelefoneInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int PessoaId { get; set; }

        public TipoTelefoneEnum Tipo { get; set; }
        
        [StringLength(Domain.Global.Pessoa.Telefone.MaxNumeroLength)]
        public string Numero { get; set; }

        [StringLength(Domain.Global.Pessoa.Telefone.MaxDDILength)]
        public string DDI { get; set; }

        [StringLength(Domain.Global.Pessoa.Telefone.MaxDDDLength)]
        public string DDD { get; set; }

        [StringLength(Domain.Global.Pessoa.Telefone.MaxRamalLength)]
        public string Ramal { get; set; }
    }
}