﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Pessoa.Telefone
{
    public class TelefoneListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int PessoaId { get; set; }

        public TipoTelefoneEnum Tipo { get; set; }

        public string DDI { get; set; }

        public string DDD { get; set; }

        public string Numero { get; set; }

        public string Ramal { get; set; }
    }
}