﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Telefone
{
    public class GetTelefoneInput:PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int PessoaId { get; set; }

        [MaxLength(Domain.Global.Pessoa.Telefone.MaxDDILength)]
        public string DDI { get; set; }

        [MaxLength(Domain.Global.Pessoa.Telefone.MaxDDDLength)]
        public string DDD { get; set; }

        [MaxLength(Domain.Global.Pessoa.Telefone.MaxNumeroLength)]
        public string Numero { get; set; }

        [MaxLength(Domain.Global.Pessoa.Telefone.MaxRamalLength)]
        public string Ramal { get; set; }

        public TipoTelefoneEnum? Tipo { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Numero";
            }
        }

    }
}