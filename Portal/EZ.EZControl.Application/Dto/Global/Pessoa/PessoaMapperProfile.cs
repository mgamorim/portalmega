﻿using AutoMapper;
using EZ.EZControl.Domain.Global.Pessoa;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaMapperProfile : Profile
    {
        protected override void Configure()
        {
            // Pessoa
            CreateMap<PessoaInput, PessoaFisicaInput>();
            CreateMap<PessoaInput, PessoaJuridicaInput>();
            CreateMap<PessoaInput, Domain.Global.Pessoa.Pessoa>().ReverseMap();
            CreateMap<Domain.Global.Pessoa.Pessoa, PessoaListDto>()
                .ForMember(destination => destination.GrupoPessoaId, opt => opt.MapFrom(x => x.GrupoPessoa.Id))
                .ForMember(destination => destination.GrupoPessoaId, opt => opt.MapFrom(x => x.GrupoPessoa.Id))
                .ForMember(destination => destination.GrupoPessoa, opt => opt.MapFrom(x => x.GrupoPessoa))
                .ForMember(destination => destination.DocumentoPrincipalId, opt => opt.MapFrom(x => x.DocumentoPrincipal.Id));
            CreateMap<PessoaListDto, PessoaInput>();

            // Pessoa Fisica
            CreateMap<PessoaFisicaInput, PessoaFisica>().ReverseMap()
                .ForMember(destination => destination.RgId, opt => opt.MapFrom(x => x.RgId))
                .ForMember(destination => destination.CpfId, opt => opt.MapFrom(x => x.CpfId))
                .ForMember(destination => destination.Cpf, opt => opt.MapFrom(x => x.Cpf))
                .ForMember(destination => destination.Rg, opt => opt.MapFrom(x => x.Rg))
                .ForMember(destination => destination.OrgaoExpedidor, opt => opt.MapFrom(x => x.OrgaoExpedidor));
            CreateMap<PessoaFisicaInput, PessoaFisicaInput>().ReverseMap();
            CreateMap<PessoaFisicaInput, PessoaFisicaInputAux>().ReverseMap();
            CreateMap<PessoaFisicaByCPFListDto, PessoaFisica>().ReverseMap(); ;
            CreateMap<PessoaFisicaInputAux, PessoaFisica>().ReverseMap();
            CreateMap<PessoaFisicaListDto, PessoaFisica>().ReverseMap();
            CreateMap<PessoaFisicaListDto, PessoaFisicaInput>();

            // Pessoa Juridica
            CreateMap<PessoaJuridicaInput, PessoaJuridica>().ReverseMap();
            CreateMap<PessoaJuridicaInput, PessoaJuridicaInput>().ReverseMap();
            CreateMap<PessoaJuridicaInput, PessoaJuridicaInputAux>().ReverseMap();
            CreateMap<PessoaJuridicaInputAux, PessoaJuridica>().ReverseMap();
            CreateMap<PessoaJuridica, PessoaJuridicaListDto>().ReverseMap();
            CreateMap<PessoaJuridicaListDto, PessoaJuridicaInput>();
        }
    }
}