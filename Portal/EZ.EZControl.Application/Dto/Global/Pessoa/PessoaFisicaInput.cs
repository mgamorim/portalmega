﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using System;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaFisicaInput : PessoaInput
    {
        [MaxLength(PessoaFisica.MaxNomeLength)]
        public string Nome { get; set; }

        public string Nacionalidade { get; set; }

        public int TratamentoId { get; set; }

        public SexoEnum? Sexo { get; set; }

        public bool PossuiFilhos { get; set; }

        public EstadoCivilEnum? EstadoCivil { get; set; }

        public DateTime? DataDeNascimento { get; set; }
        public string Cpf { get; set; }
        public int? CpfId { get; set; }
        public string OrgaoExpedidor { get; set; }
        public string Rg { get; set; }
        public int? RgId { get; set; }
        public string Celular { get; set; }
        public string DocumentoPrincipalPessoa { get; set; }

        public string identidade { get; set; }
        public string CPFmae { get; set; }
        public string identidademae { get; set; }



    }
}