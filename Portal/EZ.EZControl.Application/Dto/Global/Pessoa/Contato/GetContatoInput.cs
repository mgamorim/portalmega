﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Contato
{
    public class GetContatoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int PessoaId { get; set; }

        public int TipoDeContatoId { get; set; }

        public int TratamentoId { get; set; }

        public string Nome { get; set; }
        public string Setor { get; set; }
        public string Cargo { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}