﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeContato;
using EZ.EZControl.Dto.Global.Pessoa.Tratamento;
using System;

namespace EZ.EZControl.Dto.Global.Pessoa.Contato
{
    public class ContatoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int PessoaId { get; set; }

        public PessoaListDto Pessoa { get; set; }

        public int TipoDeContatoId { get; set; }

        public TipoDeContatoListDto TipoDeContato { get; set; }

        public int TratamentoId { get; set; }

        public TratamentoListDto Tratamento { get; set; }

        public int EnderecoPrincipalId { get; set; }

        public EnderecoListDto EnderecoPrincipal { get; set; }

        public string Nome { get; set; }

        public DateTime? DataNascimento { get; set; }

        public SexoEnum? Sexo { get; set; }

        public string Cargo { get; set; }

        public string Setor { get; set; }

        public string Idioma { get; set; }

        public string Observacao { get; set; }

        //TODO: Marcos. Retirar essas listas?
        //public ICollection<TelefoneListDto> Telefones { get; set; }

        //public ICollection<EnderecoEletronicoListDto> EnderecosEletronicos { get; set; }

        //public ICollection<EnderecoListDto> Enderecos { get; set; }
    }
}