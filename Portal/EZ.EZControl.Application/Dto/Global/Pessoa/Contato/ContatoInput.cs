﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Contato
{
    public class ContatoInput : EzInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.Global.Pessoa.Contato.MaxNomeLength)]
        public string Nome { get; set; }

        [StringLength(Domain.Global.Pessoa.Contato.MaxCargoLength)]
        public string Cargo { get; set; }

        [StringLength(Domain.Global.Pessoa.Contato.MaxSetorLength)]
        public string Setor { get; set; }

        [StringLength(Domain.Global.Pessoa.Contato.MaxIdiomaLength)]
        public string Idioma { get; set; }

        [Required]
        public int PessoaId { get; set; }

        public SexoEnum? Sexo { get; set; }

        [Required]
        public int TipoDeContatoId { get; set; }

        public int? TratamentoId { get; set; }

        public int? EnderecoPrincipalId { get; set; }
    }
}