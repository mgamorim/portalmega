﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.Contato.Mapper
{
    public class ContatoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ContatoInput, Domain.Global.Pessoa.Contato>().ReverseMap().PreserveReferences(); ;
            CreateMap<ContatoListDto, Domain.Global.Pessoa.Contato>().ReverseMap().PreserveReferences(); ;
            CreateMap<ContatoListDto, ContatoInput>();
        }
    }
}