﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.Tratamento.Mapper
{
    public class TratamentoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TratamentoInput, Domain.Global.Pessoa.Tratamento>().ReverseMap();
            CreateMap<TratamentoListDto, Domain.Global.Pessoa.Tratamento>().ReverseMap();
            CreateMap<TratamentoListDto, TratamentoInput>();
        }
    }
}