﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Pessoa.Tratamento
{
    public class GetTratamentoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}