﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Pessoa.Tratamento
{
    public class TratamentoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public string Descricao { get; set; }
    }
}