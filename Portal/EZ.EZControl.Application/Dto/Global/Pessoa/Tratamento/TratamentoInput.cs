﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.Tratamento
{
    public class TratamentoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Pessoa.Tratamento.MaxDescricaoLength)]
        public string Descricao { get; set; }
    }
}