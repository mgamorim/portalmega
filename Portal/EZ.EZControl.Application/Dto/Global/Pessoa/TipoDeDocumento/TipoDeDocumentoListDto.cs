﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento
{
    public class TipoDeDocumentoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public TipoPessoaEnum TipoPessoa { get; set; }

        public TipoDeDocumentoEnum TipoDeDocumentoFixo { get; set; }

        public string Descricao { get; set; }

        public string Mascara { get; set; }

        public bool Principal { get; set; }

        public bool ObrigaAnexo { get; set; }

        public int PrazoDeAtualizacaoDoAnexo { get; set; }

        public int Prioridade { get; set; }

        public bool ObrigaOrgaoExpedidor { get; set; }

        public bool ObrigaDataExpedicao { get; set; }

        public bool ObrigaUF { get; set; }

        public bool ObrigaDataDeValidade { get; set; }

        public bool SerieObrigatoria { get; set; }

        public bool ListaEmOutrosDocumentos { get; set; }

        public bool ExibirNoCadastroSimplificadoDePessoa { get; set; }

        public bool PermiteLancamentoDuplicado { get; set; }
    }
}