﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento
{
    public class GetTipoDeDocumentoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public TipoDeDocumentoEnum? TipoDeDocumento { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}