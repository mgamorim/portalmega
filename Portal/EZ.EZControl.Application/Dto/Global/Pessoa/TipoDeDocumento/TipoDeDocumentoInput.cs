﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento
{
    public class TipoDeDocumentoInput : EzInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public TipoPessoaEnum? TipoPessoa { get; set; }

        [Required]
        public TipoDeDocumentoEnum? TipoDeDocumentoFixo { get; set; }

        [Required]
        [MaxLength(Domain.Global.Pessoa.TipoDeDocumento.MaxDescricaoLength)]
        public string Descricao { get; set; }

        [MaxLength(Domain.Global.Pessoa.TipoDeDocumento.MaxMascaraLength)]
        public string Mascara { get; set; }

        [Required]
        public bool Principal { get; set; }

        [Required]
        public bool ObrigaAnexo { get; set; }

        public int PrazoDeAtualizacaoDoAnexo { get; set; }

        [Required]
        public int Prioridade { get; set; }

        [Required]
        public bool ObrigaOrgaoExpedidor { get; set; }

        [Required]
        public bool ObrigaDataExpedicao { get; set; }

        [Required]
        public bool ObrigaUF { get; set; }

        [Required]
        public bool ObrigaDataDeValidade { get; set; }

        [Required]
        public bool SerieObrigatoria { get; set; }

        [Required]
        public bool ListaEmOutrosDocumentos { get; set; }

        [Required]
        public bool ExibirNoCadastroSimplificadoDePessoa { get; set; }

        [Required]
        public bool PermiteLancamentoDuplicado { get; set; }
    }
}