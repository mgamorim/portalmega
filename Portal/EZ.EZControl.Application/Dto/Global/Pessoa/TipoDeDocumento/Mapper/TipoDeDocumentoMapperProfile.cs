﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento.Mapper
{
    public class TipoDeDocumentoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TipoDeDocumentoInput, Domain.Global.Pessoa.TipoDeDocumento>().ReverseMap();
            CreateMap<TipoDeDocumentoListDto, Domain.Global.Pessoa.TipoDeDocumento>().ReverseMap();
            CreateMap<TipoDeDocumentoListDto, TipoDeDocumentoInput>();
        }
    }
}