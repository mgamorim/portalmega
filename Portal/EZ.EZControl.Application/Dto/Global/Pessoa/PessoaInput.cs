﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Dto.Global.Pessoa.Documento;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Pessoa
{
    public class PessoaInput : EzInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        public DocumentoInput DocumentoPrincipal { get; set; }

        public int? GrupoPessoaId { get; set; }

        //[Required]
        public TipoPessoaEnum? TipoPessoa { get; set; }

        public string Observacao { get; set; }

        public virtual EnderecoEletronicoInput EmailPrincipal { get; set; }
        public virtual TelefoneInput TelefoneComercial { get; set; }
        public virtual int? DocumentoPrincipalId { get; set; }
        public virtual TelefoneInput TelefoneCelular { get; set; }
        public virtual TelefoneInput TelefoneResidencial { get; set; }
        public virtual TelefoneInput TelefoneRecados { get; set; }
        public virtual EnderecoInput EnderecoPrincipal { get; set; }
        public string NomePessoa { get; set; }
        public int TelefoneCelularId { get; set; }
        public string Email { get; set; }
        public int EmailId { get; set; }

    }
}