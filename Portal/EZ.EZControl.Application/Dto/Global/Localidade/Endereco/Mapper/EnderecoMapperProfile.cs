﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Localidade.Endereco.Mapper
{
    public class EnderecoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<EnderecoInput, Domain.Global.Localidade.Endereco>()
                .ForMember(destination => destination.Telefones, opt => opt.Ignore())
                .ReverseMap().PreserveReferences();
            CreateMap<EnderecoListDto, Domain.Global.Localidade.Endereco>().ReverseMap().PreserveReferences();
            CreateMap<EnderecoListDto, EnderecoInput>();
            CreateMap<Domain.Global.Localidade.Endereco, EnderecoListDto>();
        }
    }
}
