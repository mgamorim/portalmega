﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Endereco
{
    public class GetEnderecoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int PessoaId { get; set; }

        [MaxLength(Domain.Global.Localidade.Endereco.MaxDescricaoLength)]
        public string Descricao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}