﻿namespace EZ.EZControl.Dto.Global.Localidade.Endereco
{
    public class GetEnderecoByCepInput
    {
        public string Cep { get; set; }
    }
}