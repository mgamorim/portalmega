﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Endereco
{
    public class EnderecoInput : EzInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        public TipoEnderecoEnum TipoEndereco { get; set; }

        public string Descricao { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string CEP { get; set; }

        public string Referencia { get; set; }

        public bool EnderecoParaEntrega { get; set; }

        public int PaisId { get; set; }

        public int EstadoId { get; set; }
        public EstadoInput Estado { get; set; }

        public int CidadeId { get; set; }
        public CidadeInput Cidade { get; set; }

        [Required]
        public int PessoaId { get; set; }

        public int? ContatoId { get; set; }

        [Required]
        public int TipoDeLogradouroId { get; set; }
        public TipoDeLogradouroInput TipoDeLogradouro { get; set; }

        public bool Erro { get; set; }

        public string Localidade { get; set; }

        public string UF { get; set; }
        public string LogradouroComTipo
        {
            get
            {
                if (TipoDeLogradouro != null)
                {
                    return string.Format("{0} {1}", TipoDeLogradouro.Descricao, Logradouro);
                }else
                {
                    return string.Format("{0}", Logradouro);
                }
            }
        }
    }
}