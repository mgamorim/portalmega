﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Contato;

namespace EZ.EZControl.Dto.Global.Localidade.Endereco
{
    public class EnderecoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public TipoEnderecoEnum TipoEndereco { get; set; }

        public string Descricao { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string CEP { get; set; }

        public string Referencia { get; set; }

        public string EnderecoParaEntrega { get; set; }

        public int CidadeId { get; set; }

        public CidadeListDto Cidade { get; set; }

        public int PessoaId { get; set; }

        public PessoaListDto Pessoa { get; set; }

        public int? ContatoId { get; set; }

        public ContatoListDto Contato { get; set; }

        public int TipoDeLogradouroId { get; set; }

        public TipoDeLogradouroListDto TipoDeLogradouro { get; set; }

        public string EnderecoCompleto { get; set; }

        // TODO: Marcos. Retirar essa lista?
        //public ICollection<TelefoneListDto> Telefones { get; set; }
    }
}