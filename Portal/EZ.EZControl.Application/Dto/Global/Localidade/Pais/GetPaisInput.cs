﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Localidade.Pais
{
    public class GetPaisInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomePais { get; set; }
        public string Nacionalidade { get; set; }
        public string CodigoISO { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}