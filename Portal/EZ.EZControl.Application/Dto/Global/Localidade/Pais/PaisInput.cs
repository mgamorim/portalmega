﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Pais
{
    public class PaisInput : EzInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.Global.Localidade.Pais.MaxNomeLength)]
        public string Nome { get; set; }
        [Required]
        [StringLength(Domain.Global.Localidade.Pais.MaxNacionlidadeLength)]
        public string Nacionalidade { get; set; }

        [StringLength(Domain.Global.Localidade.Pais.MaxCodigoReceitaFederaLength)]
        public string CodigoReceitaFederal { get; set; }

        [StringLength(Domain.Global.Localidade.Pais.MaxCodigoInternacionaLength)]
        public string CodigoInternacional { get; set; }
        [Required]
        [StringLength(Domain.Global.Localidade.Pais.MaxCodigoISOLength)]
        public string CodigoISO { get; set; }

        [StringLength(Domain.Global.Localidade.Pais.MaxMascaraDoCodigoPostaLength)]
        public string MascaraDoCodigoPostal { get; set; }

        // TODO: Não pode ter essa lista senão causa um loop infinito.
        //public ICollection<EstadoInput> Estados { get; set; }

        [StringLength((Domain.Global.Localidade.Pais.MaxSigla2Caracteres))]
        public string Sigla2Caracteres { get; set; }

        [StringLength((Domain.Global.Localidade.Pais.MaxSigla3Caracteres))]
        public string Sigla3Caracteres { get; set; }
    }
}