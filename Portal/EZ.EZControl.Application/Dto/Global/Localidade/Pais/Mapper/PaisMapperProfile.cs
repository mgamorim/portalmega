﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Localidade.Pais.Mapper
{
    public class PaisMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PaisInput, Domain.Global.Localidade.Pais>()
                .ForMember(destination => destination.Estados, opt => opt.Ignore())
                .ForMember(destination => destination.Feriados, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<PaisListDto, Domain.Global.Localidade.Pais>().ReverseMap();
            CreateMap<PaisListDto, PaisInput>();
        }
    }
}
