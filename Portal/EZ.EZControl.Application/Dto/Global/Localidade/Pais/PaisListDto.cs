﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Localidade.Pais
{
    public class PaisListDto : EzFullAuditedInputDto, IPassivable
    {
        public string Nome { get; set; }

        public bool IsActive { get; set; }

        public string Nacionalidade { get; set; }

        public string CodigoReceitaFederal { get; set; }

        public string CodigoInternacional { get; set; }

        public string CodigoISO { get; set; }

        public string MascaraDoCodigoPostal { get; set; }

        // TODO: Marcos. Retirar essa lista?
        //public ICollection<EstadoListDto> Estados { get; set; }

        public string Sigla2Caracteres { get; set; }

        public string Sigla3Caracteres { get; set; }
    }
}