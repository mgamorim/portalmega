﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade
{
    [AutoMapFrom(typeof(Domain.Global.Localidade.Cidade))]
    public class CidadeInEstadoListDto : EzCreationAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public string Nome { get; set; }

        public string CodigoIBGE { get; set; }

        public int EstadoId { get; set; }
    }
}