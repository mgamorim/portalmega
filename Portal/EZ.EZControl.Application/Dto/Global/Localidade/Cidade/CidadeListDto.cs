﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Estado;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade
{
    public class CidadeListDto : EzFullAuditedInputDto, IPassivable
    {
        public string Nome { get; set; }

        public bool IsActive { get; set; }

        public string CodigoIBGE { get; set; }

        public int EstadoId { get; set; }

        public EstadoListDto Estado { get; set; }
    }
}