﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade
{
    public class GetCidadeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomeCidade { get; set; }
        public string CodigoIBGE { get; set; }
        public string NomeEstado { get; set; }
        public string NomePais { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}