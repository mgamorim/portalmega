﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade
{
    public class GetCidadeByEstadoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int EstadoId { get; set; }

        public string NomeCidade { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}