﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade
{
    [AutoMapTo(typeof(Domain.Global.Localidade.Cidade))]
    public class AddCidadeInput
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.Global.Localidade.Cidade.MaxNomeLength)]
        public string Nome { get; set; }

        [StringLength(Domain.Global.Localidade.Cidade.MaxCodigoIBGELength)]
        public string CodigoIBGE { get; set; }

        public int EstadoId { get; set; }
    }
}