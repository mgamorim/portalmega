﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade
{
    public class CidadeInput : EzInputDto, IPassivable
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.Global.Localidade.Cidade.MaxNomeLength)]
        public string Nome { get; set; }

        [StringLength(Domain.Global.Localidade.Cidade.MaxCodigoIBGELength)]
        public string CodigoIBGE { get; set; }

        public int PaisId { get; set; }

        public int EstadoId { get; set; }
        public EstadoInput Estado { get; set; }
    }
}