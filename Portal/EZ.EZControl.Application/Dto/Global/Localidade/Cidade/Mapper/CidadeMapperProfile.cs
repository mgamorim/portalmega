﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Localidade.Cidade.Mapper
{
    public class CidadeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CidadeInput, Domain.Global.Localidade.Cidade>();
            CreateMap<Domain.Global.Localidade.Cidade, CidadeInput>()
                .ForMember(destination => destination.PaisId, opt => opt.MapFrom(x => x.Estado.Pais.Id));
            CreateMap<CidadeListDto, Domain.Global.Localidade.Cidade>().ReverseMap();
            CreateMap<CidadeListDto, CidadeInput>();
        }
    }
}