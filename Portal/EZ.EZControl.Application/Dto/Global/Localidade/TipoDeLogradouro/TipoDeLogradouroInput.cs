﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro
{
    public class TipoDeLogradouroInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [StringLength(Domain.Global.Localidade.TipoDeLogradouro.MaxDescricaoLength)]
        public string Descricao { get; set; }
    }
}