﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro.Mapper
{
    public class TipoDeLogradouroMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<TipoDeLogradouroInput, Domain.Global.Localidade.TipoDeLogradouro>().ReverseMap();
            CreateMap<TipoDeLogradouroListDto, Domain.Global.Localidade.TipoDeLogradouro>().ReverseMap();
            CreateMap<TipoDeLogradouroListDto, TipoDeLogradouroInput>();
        }
    }
}
