﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro
{
    public class GetTipoDeLogradouroInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}