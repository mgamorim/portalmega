﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro
{
    public class TipoDeLogradouroListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public string Descricao { get; set; }
    }
}