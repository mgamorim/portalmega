﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Localidade.Pais;
using System;

namespace EZ.EZControl.Dto.Global.Localidade.Estado
{
    public class EstadoListDto : EzFullAuditedInputDto, IPassivable
    {
        public string Nome { get; set; }

        public bool IsActive { get; set; }

        public string Sigla { get; set; }

        public string Capital { get; set; }

        public string CodigoIBGE { get; set; }

        public PaisListDto Pais { get; set; }

        public Int32 PaisId { get; set; }
    }
}