﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Localidade.Estado
{
    public class GetEstadoByPaisInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int PaisId { get; set; }
        public string NomeEstado { get; set; }
        public string SiglaEstado { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}