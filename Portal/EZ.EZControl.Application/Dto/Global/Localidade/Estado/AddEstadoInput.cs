﻿using Abp.AutoMapper;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Global.Localidade.Estado
{
    [AutoMapTo(typeof(Domain.Global.Localidade.Estado))]
    public class AddEstadoInput
    {
        [Required]
        public bool IsActive { get; set; }

        [Required]
        [StringLength(Domain.Global.Localidade.Estado.MaxNomeLength)]
        public string Nome { get; set; }

        [Required]
        [StringLength(Domain.Global.Localidade.Estado.MaxSiglaLength)]
        public string Sigla { get; set; }

        [StringLength(Domain.Global.Localidade.Estado.MaxCapitalLength)]
        public string Capital { get; set; }

        [StringLength(Domain.Global.Localidade.Estado.MaxCodigoIBGELength)]
        public string CodigoIBGE { get; set; }

        [Required]
        public int PaisId { get; set; }
    }
}