﻿using Abp.AutoMapper;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Global.Localidade.Estado
{
    [AutoMapFrom(typeof(Domain.Global.Localidade.Estado))]
    public class EstadoInPaisListDto : EzCreationAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Capital { get; set; }
        public string CodigoIBGE { get; set; }
    }
}