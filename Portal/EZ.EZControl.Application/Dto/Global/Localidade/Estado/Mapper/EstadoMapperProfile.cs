﻿using AutoMapper;

namespace EZ.EZControl.Dto.Global.Localidade.Estado.Mapper
{
    public class EstadoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<EstadoInput, Domain.Global.Localidade.Estado>().ReverseMap();
            CreateMap<EstadoListDto, Domain.Global.Localidade.Estado>().ReverseMap();
            CreateMap<EstadoListDto, EstadoInput>();
        }
    }
}
