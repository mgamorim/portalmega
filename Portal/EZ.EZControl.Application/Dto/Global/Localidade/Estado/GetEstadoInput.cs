﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Global.Localidade.Estado
{
    public class GetEstadoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomeEstado { get; set; }
        public string SiglaEstado { get; set; }
        public string Capital { get; set; }
        public string CodigoIBGE { get; set; }
        public string NomePais { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}