﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class DocumentoBaseListDto : EzInputDto
    {
        public DocumentoBaseListDto()
        {
            Competencia = DateTime.Now;
        }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public decimal Valor { get; set; }
        public DateTime Data { get; set; }
        public DateTime Competencia { get; set; }
        public string Descricao { get; set; }
        public string Observacoes { get; set; }
        public string TipoDeDocumentoFinanceiroNome { get; set; }
        public int TipoDeDocumentoFinanceiroId { get; set; }
        public bool Estornado { get; set; }
        public DateTime? DataEstorno { get; set; }
        public string MotivoEstorno { get; set; }
    }
}
