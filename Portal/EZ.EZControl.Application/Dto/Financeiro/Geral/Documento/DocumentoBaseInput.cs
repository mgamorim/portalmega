﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class DocumentoBaseInput : EzInputDto
    {
        public DocumentoBaseInput()
        {
            Data = DateTime.Now;
        }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public decimal Valor { get; set; }
        public DateTime Data { get; set; }
        public DateTime Competencia { get; set; }
        public DateTime Vencimento { get; set; }
        public string Descricao { get; set; }
        public string Observacoes { get; set; }
        public int TipoDeDocumentoFinanceiroId { get; set; }
        public bool Estornado { get; set; }
        public DateTime? DataEstorno { get; set; }
        public string MotivoEstorno { get; set; }
    }
}