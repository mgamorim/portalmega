﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class GetDocumentoBaseInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Numero";
            }
        }
        public string Numero { get; set; }
    }
}
