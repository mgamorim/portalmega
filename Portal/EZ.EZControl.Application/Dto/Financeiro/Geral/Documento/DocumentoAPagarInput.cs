﻿namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class DocumentoAPagarInput : DocumentoBaseInput
    {
        public int FornecedorId { get; set; }
    }
}