﻿using AutoMapper;

namespace EZ.EZControl.Dto.Financeiro.Geral.Documento.Mapper
{
    public class DocumentoMapperProfile : Profile
    {
        public DocumentoMapperProfile()
        {
            CreateMap<DocumentoBaseInput, Domain.Financeiro.Geral.DocumentoBase>().ReverseMap();
            CreateMap<DocumentoBaseListDto, Domain.Financeiro.Geral.DocumentoBase>().ReverseMap();
            CreateMap<DocumentoBaseListDto, DocumentoBaseInput>();

            CreateMap<DocumentoAPagarInput, Domain.Financeiro.Geral.DocumentoAPagar>().ReverseMap();
            CreateMap<DocumentoAPagarListDto, Domain.Financeiro.Geral.DocumentoAPagar>().ReverseMap();
            CreateMap<DocumentoAPagarListDto, DocumentoAPagarInput>();

            CreateMap<DocumentoAReceberInput, Domain.Financeiro.Geral.DocumentoAReceber>().ReverseMap();
            CreateMap<DocumentoAReceberListDto, Domain.Financeiro.Geral.DocumentoAReceber>().ReverseMap();
            CreateMap<DocumentoAReceberListDto, DocumentoAReceberInput>();
        }
    }
}