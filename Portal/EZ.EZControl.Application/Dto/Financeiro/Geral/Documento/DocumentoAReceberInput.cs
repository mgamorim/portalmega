﻿namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class DocumentoAReceberInput : DocumentoBaseInput
    {
        public int ClienteId { get; set; }
    }
}