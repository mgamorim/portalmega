﻿namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class DocumentoAReceberListDto : DocumentoBaseListDto
    {
        public string ClienteNomePessoa { get; set; }
    }
}
