﻿namespace EZ.EZControl.Dto.Financeiro.Geral.Documento
{
    public class DocumentoAPagarListDto : DocumentoBaseListDto
    {
        public string FornecedorNomePessoa { get; set; }
    }
}
