﻿using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro
{
    public class TipoDeDocumentoFinanceiroListDto : EzInputDto
    {
        public string Nome { get; set; }
        public TipoDeDocumentoFinanceiroEnum Tipo { get; set; }
        public bool IsActive { get; set; }
    }
}
