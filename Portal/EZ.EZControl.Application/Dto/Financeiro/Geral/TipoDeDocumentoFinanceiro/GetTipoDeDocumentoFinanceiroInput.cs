﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Financeiro.Enums;

namespace EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro
{
    public class GetTipoDeDocumentoFinanceiroInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
        public string Nome { get; set; }
        public TipoDeDocumentoFinanceiroEnum Tipo { get; set; }
    }
}
