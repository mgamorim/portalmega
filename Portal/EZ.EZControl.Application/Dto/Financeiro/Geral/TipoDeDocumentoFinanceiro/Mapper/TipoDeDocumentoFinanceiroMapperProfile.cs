﻿using AutoMapper;
using EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro;

namespace EZ.EZControl.Dto.Financeiro.Geral.Documento.Mapper
{
    public class TipoDeDocumentoFinanceiroMapperProfile : Profile
    {
        public TipoDeDocumentoFinanceiroMapperProfile()
        {
            CreateMap<TipoDeDocumentoFinanceiroInput, Domain.Financeiro.Geral.TipoDeDocumentoFinanceiro>().ReverseMap();
            CreateMap<TipoDeDocumentoFinanceiroListDto, Domain.Financeiro.Geral.TipoDeDocumentoFinanceiro>().ReverseMap();
            CreateMap<TipoDeDocumentoFinanceiroListDto, TipoDeDocumentoFinanceiroInput>();
        }
    }
}