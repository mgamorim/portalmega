﻿using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Servicos.Geral.Servicos
{
    public class ServicoInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public decimal Valor { get; set; }

        [Required]
        public bool IsValorAjustavel { get; set; }
    }
}
