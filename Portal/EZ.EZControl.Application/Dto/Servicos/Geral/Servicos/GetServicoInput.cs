﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Servicos.Geral.Servicos
{
    public class GetServicoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
