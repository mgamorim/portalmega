﻿using AutoMapper;
using EZ.EZControl.Domain.Servicos.Geral;

namespace EZ.EZControl.Dto.Servicos.Geral.Servicos.Mapper
{
    public class ServicoMapperProfile : Profile
    {
        public ServicoMapperProfile()
        {
            CreateMap<ServicoInput, Servico>().ReverseMap();
            CreateMap<ServicoListDto, Servico>().ReverseMap();
            CreateMap<ServicoListDto, ServicoInput>();
        }
    }
}