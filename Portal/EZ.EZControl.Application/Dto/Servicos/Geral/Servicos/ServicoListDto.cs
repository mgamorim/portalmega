﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Servicos.Geral.Servicos
{
    public class ServicoListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public bool IsValorAjustavel { get; set; }
    }
}
