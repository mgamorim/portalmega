﻿using AutoMapper;

namespace EZ.EZControl.Dto.Core.Geral.Parametro.Mapper
{
    public class ParametroMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ParametroInput, Domain.Core.Geral.ParametroBase>().ReverseMap();
            CreateMap<ParametroListDto, Domain.Core.Geral.ParametroBase>().ReverseMap();
            CreateMap<ParametroListDto, ParametroInput>();
        }
    }
}
