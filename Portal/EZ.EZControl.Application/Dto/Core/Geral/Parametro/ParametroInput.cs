﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Core.Geral.Parametro
{
    public class ParametroInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int TamanhoMaximoMb { get; set; }
        public int AlturaMaximaPx { get; set; }
        public int LarguraMaximaPx { get; set; }
        public string ExtensoesDocumento { get; set; }
        public string ExtensoesImagem { get; set; }
    }
}