﻿using AutoMapper;
using EZ.EZControl.Domain.Core.Geral;

namespace EZ.EZControl.Dto.Core.Geral.Arquivo.Mapper
{
    public class ArquivoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ArquivoInput, ArquivoBase>().ReverseMap();
            CreateMap<ArquivoListDto, ArquivoBase>().ReverseMap();
            CreateMap<ArquivoListDto, ArquivoInput>();
        }
    }
}
