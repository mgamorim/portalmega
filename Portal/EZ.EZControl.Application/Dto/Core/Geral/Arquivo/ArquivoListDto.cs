﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Core.Geral.Arquivo
{
    public class ArquivoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public byte[] Conteudo { get; set; }
        public string Path { get; set; }
        public TipoDeArquivoEnum TipoDeArquivoFixo { get; set; }
        public string Token { get; set; }
        public bool IsImagem { get; set; }
    }
}