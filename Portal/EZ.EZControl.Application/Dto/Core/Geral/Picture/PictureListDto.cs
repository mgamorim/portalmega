﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Core.Geral.Picture
{
    public class PictureListDto : EzFullAuditedInputDto, IPassivable
    {
        [Required]
        public string FileName { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public bool IsActive { get; set; }
    }
}
