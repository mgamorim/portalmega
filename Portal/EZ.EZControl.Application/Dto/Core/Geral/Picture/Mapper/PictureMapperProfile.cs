﻿using AutoMapper;

namespace EZ.EZControl.Dto.Core.Geral.Picture.Mapper
{
    public class PictureMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PictureInput, Domain.Core.Geral.Picture>().ReverseMap();
            CreateMap<PictureListDto, Domain.Core.Geral.Picture>().ReverseMap();
            CreateMap<PictureListDto, PictureInput>();
        }
    }
}
