﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.Agenda.Geral.Bloqueio
{
    public class BloqueioListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public DateTime Data { get; set; }
        public DateTime Horario { get; set; }
        public int ConfiguracaoDeBloqueioId { get; set; }
    }
}
