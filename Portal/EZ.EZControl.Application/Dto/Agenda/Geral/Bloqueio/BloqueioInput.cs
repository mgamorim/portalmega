﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.Agenda.Geral.Bloqueio
{
    public class BloqueioInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public DateTime Data { get; set; }
        public TimeSpan Horario { get; set; }
        public int ConfiguracaoDeBloqueioId { get; set; }
    }
}