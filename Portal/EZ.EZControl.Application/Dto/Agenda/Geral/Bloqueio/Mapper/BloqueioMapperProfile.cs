﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.Bloqueio.Mapper
{
    public class BloqueioMapperProfile : Profile
    {
        public BloqueioMapperProfile()
        {
            CreateMap<BloqueioInput, Domain.Agenda.Geral.Bloqueio>().ReverseMap();
            CreateMap<BloqueioListDto, Domain.Agenda.Geral.Bloqueio>().ReverseMap();
            CreateMap<BloqueioListDto, BloqueioInput>();
        }
    }
}
