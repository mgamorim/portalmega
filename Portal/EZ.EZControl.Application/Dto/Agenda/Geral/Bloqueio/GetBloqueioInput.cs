﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Agenda.Geral.Bloqueio
{
    public class GetBloqueioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Data { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Data";
            }
        }
    }
}
