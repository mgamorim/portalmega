﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.Disponibilidade.Mapper
{
    public class DisponibilidadeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DisponibilidadeInput, Domain.Agenda.Geral.Disponibilidade>().ReverseMap();
            CreateMap<DisponibilidadeListDto, Domain.Agenda.Geral.Disponibilidade>().ReverseMap();
            CreateMap<DisponibilidadeListDto, DisponibilidadeInput>();
        }
    }
}
