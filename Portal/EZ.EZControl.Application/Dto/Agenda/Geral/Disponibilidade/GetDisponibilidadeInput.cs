﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Agenda.Geral.Disponibilidade
{
    public class GetDisponibilidadeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Data { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Data";
            }
        }
    }
}
