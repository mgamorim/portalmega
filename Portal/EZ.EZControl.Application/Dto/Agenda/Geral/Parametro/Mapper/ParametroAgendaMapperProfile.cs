﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Dto.Core.Geral.Parametro.Mapper;

namespace EZ.EZControl.Dto.Agenda.Geral.Parametro.Mapper
{
    public class ParametroAgendaMapperProfile : ParametroMapperProfile
    {
        protected override void Configure()
        {
            CreateMap<ParametroAgendaListDto, ParametroAgenda>().ReverseMap();
            CreateMap<ParametroAgendaInput, ParametroAgenda>().ReverseMap();
            CreateMap<ParametroAgendaListDto, ParametroAgendaInput>();
        }
    }
}
