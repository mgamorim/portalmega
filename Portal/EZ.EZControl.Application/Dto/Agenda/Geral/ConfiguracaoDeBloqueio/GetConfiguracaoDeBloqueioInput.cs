﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio
{
    public class GetConfiguracaoDeBloqueioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Titulo { get; set; }
        public string MotivoDoBloqueio { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
