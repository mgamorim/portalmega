﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda.Geral.Bloqueio;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio
{
    public class ConfiguracaoDeBloqueioInput : EzInputDto, IPassivable
    {
        public ConfiguracaoDeBloqueioInput()
        {
            IsActive = true;
            Bloqueios = new List<BloqueioInput>();
        }
        public bool IsActive { get; set; }
        public string Titulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public TipoDeEventoEnum TipoDeEvento { get; set; }
        public MotivoDoBloqueioEnum MotivoDoBloqueio { get; set; }
        public DateTime InicioBloqueio { get; set; }
        public DateTime FimBloqueio { get; set; }
        public string HorarioInicio { get; set; }
        public string HorarioFim { get; set; }
        public ICollection<BloqueioInput> Bloqueios { get; set; }
    }
}