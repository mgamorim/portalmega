﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio.Mapper
{
    public class ConfiguracaoDeBloqueioMapperProfile : Profile
    {
        public ConfiguracaoDeBloqueioMapperProfile()
        {
            CreateMap<ConfiguracaoDeBloqueioInput, Domain.Agenda.Geral.ConfiguracaoDeBloqueio>()
                .ForMember(destination => destination.Bloqueios, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<ConfiguracaoDeBloqueioListDto, Domain.Agenda.Geral.ConfiguracaoDeBloqueio>()
                .ReverseMap();
            CreateMap<ConfiguracaoDeBloqueioListDto, ConfiguracaoDeBloqueioInput>();
        }
    }
}
