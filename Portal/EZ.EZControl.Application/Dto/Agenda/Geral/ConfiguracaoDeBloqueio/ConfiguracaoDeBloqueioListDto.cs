﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio
{
    public class ConfiguracaoDeBloqueioListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Titulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public TipoDeEventoEnum TipoDeEvento { get; set; }
        public MotivoDoBloqueioEnum MotivoDoBloqueio { get; set; }
        public TipoDeDisponibilidadeEnum TipoDeDisponibilidade { get; set; }
        public DateTime InicioBloqueio { get; set; }
        public DateTime FimBloqueio { get; set; }
        public TimeSpan HorarioInicio { get; set; }
        public TimeSpan HorarioFim { get; set; }
    }
}
