﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.Evento.Mapper
{
    public class EventoReuniaoMapperProfile : Profile
    {
        public EventoReuniaoMapperProfile()
        {
            CreateMap<EventoReuniaoInput, Domain.Agenda.Geral.EventoReuniao>().ReverseMap();
        }
    }
}