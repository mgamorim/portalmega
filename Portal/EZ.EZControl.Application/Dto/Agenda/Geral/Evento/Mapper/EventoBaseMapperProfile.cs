﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.Evento.Mapper
{
    public class EventoBaseMapperProfile : Profile
    {
        public EventoBaseMapperProfile()
        {
            CreateMap<EventoInput, Domain.Agenda.Geral.EventoBase>()
                .ForMember(destination => destination.Historico, opt => opt.Ignore())
                .ForMember(destination => destination.Participantes, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<EventoListDto, Domain.Agenda.Geral.EventoBase>().ReverseMap();
            CreateMap<EventoInput, EventoListDto>();
        }
    }
}