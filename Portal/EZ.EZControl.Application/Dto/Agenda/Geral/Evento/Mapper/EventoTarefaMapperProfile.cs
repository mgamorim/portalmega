﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.Evento.Mapper
{
    public class EventoTarefaMapperProfile : Profile
    {
        public EventoTarefaMapperProfile()
        {
            CreateMap<EventoTarefaInput, Domain.Agenda.Geral.EventoTarefa>().ReverseMap();
        }
    }
}