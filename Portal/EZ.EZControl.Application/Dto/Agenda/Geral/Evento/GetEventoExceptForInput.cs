﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.Agenda.Geral.Evento
{
    public class GetEventoExceptForInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int Id { get; set; }
        public string Titulo { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
