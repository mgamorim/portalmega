﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;
using System;

namespace EZ.EZControl.Dto.Agenda.Geral.Evento
{
    public class EventoListDto : EzFullAuditedInputDto
    {
        public string Descricao { get; set; }

        public DateTime Termino { get; set; }

        public string TerminoEndZone { get; set; }

        public bool DiaInteiro { get; set; }

        public DateTime Inicio { get; set; }

        public string InicioTimeZone { get; set; }

        public int EventoPaiId { get; set; }

        public int OwnerId { get; set; }

        public int DisponibilidadeId { get; set; }

        public string RecurrenceRule { get; set; }

        public string RecurrenceException { get; set; }

        public string Titulo { get; set; }

        public TipoDeEventoEnum TipoDeEvento { get; set; }

        public SistemaEnum Sistema { get; set; }

        public StatusDoEventoEnum StatusDoEvento { get; set; }

        public PessoaListDto[] Participantes { get; set; }
    }
}
