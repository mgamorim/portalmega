﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;

namespace EZ.EZControl.Dto.Agenda.Geral.Evento
{
    public class GetEventoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Titulo { get; set; }
        public TipoDeEventoEnum Tipo { get; set; }
        public SistemaEnum Sistema { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
