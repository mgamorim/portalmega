﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda.Geral.Disponibilidade;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade
{
    public class ConfiguracaoDeDisponibilidadeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Titulo { get; set; }
        public SistemaEnum Sistema { get; set; }
        public TipoDeEventoEnum TipoDeEvento { get; set; }
        public TipoDeDisponibilidadeEnum TipoDeDisponibilidade { get; set; }
        public string DiasDaSemana { get; set; }
        public int NumeroDeDias { get; set; }
        public DateTime? DataEspecifica { get; set; }
        public TimeSpan HorarioInicio { get; set; }
        public TimeSpan HorarioFim { get; set; }
        public DateTime? DataInicioValidade { get; set; }
        public DateTime? DataFimValidade { get; set; }
        public ICollection<DisponibilidadeInput> Disponibilidades { get; set; }
        public IEnumerable<DayOfWeek> Semana
        {
            get
            {
                return string.IsNullOrEmpty(DiasDaSemana) ? null : DiasDaSemana.Split(',').Select(dia => (DayOfWeek)Enum.Parse(typeof(DayOfWeek), dia, false)).ToList();
            }
        }
    }
}