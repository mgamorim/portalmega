﻿using AutoMapper;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade.Mapper
{
    public class ConfiguracaoDeDisponibilidadeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ConfiguracaoDeDisponibilidadeInput, Domain.Agenda.Geral.ConfiguracaoDeDisponibilidade>()
               .ForMember(destination => destination.Disponibilidades, opt => opt.Ignore())
               .ReverseMap();
            CreateMap<ConfiguracaoDeDisponibilidadeListDto, Domain.Agenda.Geral.ConfiguracaoDeDisponibilidade>().ReverseMap();
            CreateMap<ConfiguracaoDeDisponibilidadeListDto, ConfiguracaoDeDisponibilidadeInput>();
        }
    }
}
