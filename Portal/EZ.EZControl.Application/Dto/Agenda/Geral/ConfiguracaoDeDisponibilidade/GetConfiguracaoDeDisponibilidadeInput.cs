﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade
{
    public class GetConfiguracaoDeDisponibilidadeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Titulo { get; set; }
        public string TipoDeDisponibilidade { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
