﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Agenda.Regra
{
    public class GetTiposDeEventosBySistemaDto
    {
        public SistemaEnum Sistema { get; set; }
    }
}
