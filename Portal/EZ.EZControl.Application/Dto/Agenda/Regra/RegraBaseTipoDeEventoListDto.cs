﻿using System.Collections.Generic;
using EZ.EZControl.Domain.Agenda.Enums;

namespace EZ.EZControl.Dto.Agenda.Regra
{
    public class RegraBaseTipoDeEventoListDto
    {
        public List<TipoDeEventoEnum> TiposDeEventos { get; set; }
    }
}
