﻿using AutoMapper;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Dto.Agenda.Regra.Mapper
{
    public class RegraReuniaoMapperProfile : Profile
    {
        public RegraReuniaoMapperProfile()
        {
            CreateMap<RegraReuniaoInput, RegraReuniao>().ReverseMap();
        }
    }
}
