﻿using AutoMapper;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Dto.Agenda.Regra.Mapper
{
    public class RegraBaseMapperProfile : Profile
    {
        public RegraBaseMapperProfile()
        {
            CreateMap<RegraBaseInput, RegraBase>().ReverseMap();
            CreateMap<RegraBaseListDto, RegraBase>().ReverseMap();
            CreateMap<RegraBaseListDto, RegraBaseInput>();
        }
    }
}
