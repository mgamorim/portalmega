﻿using AutoMapper;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Dto.Agenda.Regra.Mapper
{
    public class RegraTarefaMapperProfile : Profile
    {
        public RegraTarefaMapperProfile()
        {
            CreateMap<RegraTarefaInput, RegraTarefa>().ReverseMap();
        }
    }
}
