﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Agenda.Regra
{
    public class GetRegraBaseInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Sistema { get; set; }
        public string TipoDeEvento { get; set; }
        
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Sistema";
            }
        }
    }
}
