﻿using System.Collections.Generic;
using EZ.EZControl.Domain.Core.Enums;

namespace EZ.EZControl.Dto.Agenda.Regra
{
    public class RegraBaseSistemaListDto
    {
        public List<SistemaEnum> Sistemas { get; set; }
    }
}
