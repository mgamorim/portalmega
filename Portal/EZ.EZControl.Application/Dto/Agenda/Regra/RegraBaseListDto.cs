﻿using System;
using System.ComponentModel.DataAnnotations;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.Agenda.Regra
{
    public class RegraBaseListDto : EzFullAuditedInputDto
    {
        [Required]
        public virtual SistemaEnum Sistema { get; set; }
        [Required]
        public virtual TipoDeEventoEnum TipoDeEvento { get; set; }
        [Required]
        public int DiasExibidosDesdeHojeAteAgendamento { get; set; }
        [Required]
        public int VagasDisponibilizadasPorDia { get; set; }
        public int VagasReservadasPorDia { get; set; }
        [Required]
        public int UnidadeDeTempo { get; set; }
        [Required]
        public int VagasPorUnidadeDeTempo { get; set; }
        public int DiasAntesDoAgendamentoProibidoAlterar { get; set; }
        public int DiasParaNovoAgendamentoPorParticipante { get; set; }
        public bool OverBook { get; set; }
        public bool IsActive { get; set; }
    }
}
