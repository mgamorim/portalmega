﻿
namespace EZ.EZControl.Dto
{
    public class FileActionResult
    {
        public bool Successful { get; set; }
        public string Message { get; set; }
    }
}