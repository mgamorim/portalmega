﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class ParametroPagSeguroInput : EzInputDto
    {
        public int PessoaJuridicaId { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string AppId { get; set; }
        public string AppKey { get; set; }
    }
}
