﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class HolderInput : EzInputDto
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public DateTime Nascimento { get; set; }
    }
}
