﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using System;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class DepositoTransferenciaBancariaInput : EzInputDto
    {
        public DepositoTransferenciaBancariaInput()
        {
            this.IsActive = true;
        }
        //public int Id { get; set; }
        public int EmpresaId { get; set; }
        public int PessoaJuridicaId { get; set; }
        public string Comprovante { get; set; }
        public string Banco { get; set; }
        public string Agencia { get; set; }
        public string Conta { get; set; }
        public string Titular { get; set; }
        public string Descricao { get; set; }
        public bool IsActive { get; set; }
    }
}
