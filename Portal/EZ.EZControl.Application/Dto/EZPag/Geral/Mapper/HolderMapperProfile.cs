﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;

namespace EZ.EZControl.Dto.EZPag.Geral.Mapper
{
    public class HolderMapperProfile : Profile
    {
        public HolderMapperProfile()
        {
            CreateMap<HolderInput, EZPagHolder>().ReverseMap();
        }
    }
}
