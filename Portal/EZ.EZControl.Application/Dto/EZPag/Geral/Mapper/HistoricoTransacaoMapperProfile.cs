﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;

namespace EZ.EZControl.Dto.EZPag.Geral.Mapper
{
    public class HistoricoTransacaoMapperProfile : Profile
    {
        public HistoricoTransacaoMapperProfile() {
            CreateMap<HistoricoTransacaoInput, HistoricoTransacao>().ReverseMap();
            CreateMap<HistoricoTransacaoListDto, HistoricoTransacao>().ReverseMap();
            CreateMap<HistoricoTransacaoListDto, HistoricoTransacaoInput>();
        }
    }
}
