﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral.Mapper
{
    public class ParametroPagSeguroMapperProfile : Profile
    {
        public ParametroPagSeguroMapperProfile() {
            CreateMap<ParametroPagSeguroInput, ParametroPagSeguro>().ReverseMap();
        }
    }
}
