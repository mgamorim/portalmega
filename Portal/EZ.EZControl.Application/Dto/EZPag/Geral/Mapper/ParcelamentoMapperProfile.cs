﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;

namespace EZ.EZControl.Dto.EZPag.Geral.Mapper
{
    public class ParcelamentoMapperProfile : Profile
    {
        public ParcelamentoMapperProfile()
        {
            CreateMap<ParcelamentoInput, EZPagParcelamento>().ReverseMap();
        }
    }
}
