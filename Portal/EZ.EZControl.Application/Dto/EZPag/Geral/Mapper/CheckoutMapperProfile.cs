﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;

namespace EZ.EZControl.Dto.EZPag.Geral.Mapper
{
    public class CheckoutMapperProfile : Profile
    {
        public CheckoutMapperProfile()
        {
            CreateMap<CheckoutInput, EZPagCheckout>().ReverseMap();
        }
    }
}
