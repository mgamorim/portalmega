﻿using EZ.EZControl.Dto.Core.Geral.Parametro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral.Parametro
{
    public class ParametroEzpagInput : ParametroInput
    {
        public bool IsTest { get; set; }
    }
}
