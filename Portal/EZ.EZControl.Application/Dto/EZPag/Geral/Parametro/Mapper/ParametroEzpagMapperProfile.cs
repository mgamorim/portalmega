﻿using AutoMapper;
using EZ.EZControl.Dto.Core.Geral.Parametro.Mapper;

namespace EZ.EZControl.Dto.EZPag.Geral.Parametro.Mapper
{
    public class ParametroEzpagMapperProfile : ParametroMapperProfile
    {
        protected override void Configure()
        {
            CreateMap<ParametroEzpagInput, Domain.EZPag.Geral.ParametroEzpag>().ReverseMap();
            CreateMap<ParametroEzpagListDto, Domain.EZPag.Geral.ParametroEzpag>().ReverseMap();
            CreateMap<ParametroEzpagListDto, ParametroEzpagInput>();
        }
    }
}
