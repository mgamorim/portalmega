﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.EZPag.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral.FormaDePagamento
{
    public class GetFormaDePagamentoInput: PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }
        public TipoCheckoutPagSeguroEnum TipoDePagamento { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                //Sorting = "Id";
                Sorting = "Descricao";
            }
        }
    }
}
