﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class FormaDePagamentoInput: EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        [MaxLength(Domain.EZPag.Geral.FormaDePagamento.DescricaoMaxLength)]
        public string Descricao { get; set; }
        public TipoCheckoutPagSeguroEnum TipoDePagamento { get; set; }
    }
}
