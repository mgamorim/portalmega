﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;

namespace EZ.EZControl.Dto.EZPag.Geral.Mapper
{
    public class FormaDePagamentoMapperProfile : Profile
    {
        public FormaDePagamentoMapperProfile()
        {
            CreateMap<FormaDePagamentoInput, Domain.EZPag.Geral.FormaDePagamento >().ReverseMap();
            CreateMap<FormaDePagamentoListDto, Domain.EZPag.Geral.FormaDePagamento>().ReverseMap();
            CreateMap<FormaDePagamentoListDto, FormaDePagamentoInput>();
        }
    }
}
