﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class FormaDePagamentoListDto : EzInputDto, IPassivable
    {        
        public bool IsActive { get; set; }        
        public string Descricao { get; set; }
        public TipoCheckoutPagSeguroEnum TipoDePagamento { get; set; }
    }
}
