﻿using Abp.Application.Services.Dto;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class RetornoPagSeguroInput : EntityDto
    {
        public string notificationType { get; set; }
        public string notificationCode { get; set; }
    }
}
