﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class ParcelamentoInput : EzInputDto
    {
        public int NumeroDeParcelas { get; set; }
        public decimal ValorDaParcela { get; set; }
        public bool SemJuros { get; set; }
        public decimal ValorTotal { get; set; }
    }
}
