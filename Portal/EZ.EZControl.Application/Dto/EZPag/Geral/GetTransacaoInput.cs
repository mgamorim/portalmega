﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.EZPag.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class GetTransacaoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int PedidoId { get; set; }
        public string TransactionId { get; set; }
        public string Referencia { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPagamentoEnum StatusDoPagamento { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "TransactionId";
            }
        }
    }
}
