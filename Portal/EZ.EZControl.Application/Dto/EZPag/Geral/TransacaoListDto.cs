﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class TransacaoListDto : EzInputDto
    {
        public int PedidoId { get; set; }
        public virtual PedidoInput Pedido { get; set; }
        public string TransactionId { get; set; }
        public string Referencia { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPagamentoEnum StatusDoPagamento { get; set; }
    }
}
