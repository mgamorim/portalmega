﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class CheckoutInput : EzInputDto
    {
        public TipoCheckoutPagSeguroEnum Tipo { get; set; }
        public PedidoInput Pedido { get; set; }
        public string SenderHash { get; set; }
        public HolderInput Holder { get; set; }
        public string TokenCartao { get; set; }
        public ParcelamentoInput OpcaoDeParcelamento { get; set; }
        public string NomeDoBanco { get; set; }
    }
}
