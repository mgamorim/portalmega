﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class HistoricoTransacaoInput : EzInputDto
    {
        public int TransacaoId { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPagamentoEnum Status { get; set; }
    }
}
