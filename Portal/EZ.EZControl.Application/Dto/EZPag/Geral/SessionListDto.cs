﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class SessionListDto : EzInputDto
    {
        public SessionListDto()
        {

        }
        public SessionListDto(string id)
        {
            Id = id;
        }
        public new string Id { get; set; }
    }
}
