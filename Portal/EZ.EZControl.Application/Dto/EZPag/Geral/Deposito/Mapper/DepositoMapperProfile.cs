﻿using AutoMapper;
using EZ.EZControl.Domain.EZPag.Geral;

namespace EZ.EZControl.Dto.EZPag.Geral.Deposito.Mapper
{
    public class DepositoMapperProfile : Profile
    {
        public DepositoMapperProfile()
        {
            CreateMap<DepositoInput, Domain.EZPag.Geral.Deposito>().ReverseMap();
        }
    }
}
