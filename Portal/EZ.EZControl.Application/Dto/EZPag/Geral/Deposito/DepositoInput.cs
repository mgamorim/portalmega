﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZPag.Geral.Deposito
{
    public class DepositoInput: EzInputDto
    {
        public int PropostaId { get; set; }
        public int PedidoId { get; set; }
        public string Comprovante { get; set; }
        public long UsuarioEnvioId { get; set; }
        public long? UsuarioDeclaranteId { get; set; }
        public bool Recebido { get; set; }
        public DepositoTransferenciaBancariaInput DepositoTransferenciaBancariaInput { get; set; }
        public FormaDePagamentoEnum formaDePagamento { get; set; }
        public DateTime? DataHoraDeclaracao { get; set; }
        public DateTime DataHoraEnvio { get; set; }
    }
}
