﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;

namespace EZ.EZControl.Dto.EZPag.Geral
{
    public class TransacaoInput : EzInputDto
    {
        public int PedidoId { get; set; }
        public virtual PedidoInput Pedido { get; set; }
        public string TransactionId { get; set; }
        public string Referencia { get; set; }
        public DateTime Data { get; set; }
        public StatusDoPagamentoEnum StatusDoPagamento { get; set; }
        public DepositoTransferenciaBancariaInput DepositoTransferenciaBancaria { get; set; }
        public string LinkParaPagamento { get; set; }
    }
}
