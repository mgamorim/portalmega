﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.CRM.Geral.Lead
{
    public class LeadInput : EzInputDto, IPassivable
    {
        public LeadInput()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public virtual PessoaFisicaInput PessoaFisica { get; set; }
        public int PessoaFisicaId { get; set; }
    }
}
