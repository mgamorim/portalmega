﻿using AutoMapper;

namespace EZ.EZControl.Dto.CRM.Geral.Lead.Mapper
{
    public class LeadMapperProfile : Profile
    {
        public LeadMapperProfile()
        {
            CreateMap<LeadInput, Domain.CRM.Geral.Lead>().ReverseMap();
            CreateMap<LeadListDto, Domain.CRM.Geral.Lead>().ReverseMap();
            CreateMap<LeadListDto, LeadInput>();
        }
    }
}
