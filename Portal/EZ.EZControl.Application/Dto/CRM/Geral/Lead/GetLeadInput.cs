﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.CRM.Geral.Lead
{
    public class GetLeadInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Id { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
