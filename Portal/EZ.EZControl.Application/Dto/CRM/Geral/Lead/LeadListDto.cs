﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.CRM.Geral.Lead
{
    public class LeadListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public virtual PessoaFisicaListDto PessoaFisica { get; set; }
        public int PessoaFisicaId { get; set; }
    }
}
