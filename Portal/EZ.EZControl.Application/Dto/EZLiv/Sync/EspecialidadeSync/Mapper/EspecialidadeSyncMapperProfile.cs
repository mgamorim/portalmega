﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync.Mapper
{
    public class EspecialidadeSyncMapperProfile : Profile
    {
        public EspecialidadeSyncMapperProfile()
        {
            CreateMap<EspecialidadeSyncInput, Domain.EZLiv.Sync.EspecialidadeSync>().ReverseMap();
            CreateMap<EspecialidadeSyncListDto, Domain.EZLiv.Sync.EspecialidadeSync>().ReverseMap();
            CreateMap<EspecialidadeSyncListDto, EspecialidadeSyncInput>();
        }
    }
}
