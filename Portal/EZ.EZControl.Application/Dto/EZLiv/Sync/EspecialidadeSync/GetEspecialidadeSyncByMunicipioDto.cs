﻿namespace EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync
{
    public class GetEspecialidadeSyncByMunicipioDto
    {
        public string Estado { get; set; }
        public string NomeEstado { get; set; }
        public string Municipio { get; set; }
    }
}
