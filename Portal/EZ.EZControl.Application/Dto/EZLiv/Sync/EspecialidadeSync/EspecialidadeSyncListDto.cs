﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync
{
    public class EspecialidadeSyncListDto : EzFullAuditedInputDto
    {
        public bool IsActive { get; set; }

        public string Especialidade { get; set; }

        public string Nome { get; set; }

        public string Logradouro { get; set; }

        public string Numero { get; set; }

        public string Complemento { get; set; }

        public string Bairro { get; set; }

        public string Municipio { get; set; }

        public string Estado { get; set; }

        public string NomeEstado { get; set; }

        public string Telefone1 { get; set; }

        public string Ramal1 { get; set; }

        public string Telefone2 { get; set; }

        public string Ramal2 { get; set; }

        public string MunicipioPai { get; set; }

        public string Email { get; set; }

        public string HomePage { get; set; }

        public virtual List<ProdutoDePlanoDeSaudeListDto> ProdutosDePlanoDeSaudeListDto { get; set; }

        public int[] ProdutosDePlanoDeSaudeIds { get; set; }
    }
}
