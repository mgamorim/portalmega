﻿namespace EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync
{
    public class GetEspecialidadeSyncByNomeDto
    {
        public string Nome { get; set; }
    }
}
