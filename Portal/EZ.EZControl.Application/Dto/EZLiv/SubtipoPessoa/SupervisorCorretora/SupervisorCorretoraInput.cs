﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorCorretora
{
    public class SupervisorCorretoraInput : EzInputDto
    {
        public bool IsActive { get; set; }
        public PessoaInput Pessoa { get; set; }
        //public virtual CorretoraInput Corretora { get; set; }
        public int CorretoraId { get; set; }
    }
}