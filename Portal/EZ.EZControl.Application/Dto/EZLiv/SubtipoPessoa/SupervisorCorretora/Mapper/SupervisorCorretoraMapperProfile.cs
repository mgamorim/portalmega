﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorCorretora.Mapper
{
    public class SupervisorCorretoraMapperProfile : Profile
    {
        public SupervisorCorretoraMapperProfile()
        {
            CreateMap<SupervisorCorretoraInput, Domain.EZLiv.SubtiposPessoa.SupervisorCorretora>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<SupervisorCorretoraListDto, Domain.EZLiv.SubtiposPessoa.SupervisorCorretora>().ReverseMap();
            CreateMap<SupervisorCorretoraListDto, SupervisorCorretoraInput>();
        }
    }
}
