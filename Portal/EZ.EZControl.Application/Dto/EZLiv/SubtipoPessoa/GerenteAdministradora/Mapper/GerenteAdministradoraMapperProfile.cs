﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteAdministradora.Mapper
{
    public class GerenteAdministradoraMapperProfile : Profile
    {
        public GerenteAdministradoraMapperProfile()
        {
            CreateMap<GerenteAdministradoraInput, Domain.EZLiv.SubtiposPessoa.GerenteAdministradora>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<GerenteAdministradoraListDto, Domain.EZLiv.SubtiposPessoa.GerenteAdministradora>().ReverseMap();
            CreateMap<GerenteAdministradoraListDto, GerenteAdministradoraInput>();
        }
    }
}
