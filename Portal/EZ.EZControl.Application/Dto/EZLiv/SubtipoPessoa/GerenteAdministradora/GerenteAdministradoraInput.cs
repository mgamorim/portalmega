﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteAdministradora
{
    public class GerenteAdministradoraInput : EzInputDto
    {
        public bool IsActive { get; set; }
        public PessoaInput Pessoa { get; set; }
        public int PessoaId { get; set; }
        //public AdministradoraInput Administradora { get; set; }
        public int AdministradoraId { get; set; }
    }
}
