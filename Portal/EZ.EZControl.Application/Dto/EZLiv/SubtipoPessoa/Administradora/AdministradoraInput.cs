﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteAdministradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.HomologadorAdministradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorAdministradora;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora
{
    public class AdministradoraInput : EzInputDto, IPassivable
    {
        public AdministradoraInput()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int PessoaJuridicaId { get; set; }
        public ParametroPagSeguroInput ParametroPagSeguro { get; set; }
        public int ParametroPagSeguroId { get; set; }
        public int ImagemId { get; set; }
        public virtual ICollection<CorretoraInput> Corretoras { get; set; }
        public virtual ICollection<SupervisorAdministradoraInput> Supervisores { get; set; }
        public virtual ICollection<GerenteAdministradoraInput> Gerentes { get; set; }
        public virtual ICollection<HomologadorAdministradoraInput> Homologadores { get; set; }
    }
}
