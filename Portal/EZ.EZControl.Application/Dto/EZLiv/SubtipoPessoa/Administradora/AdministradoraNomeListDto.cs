﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora
{
    public class AdministradoraNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}