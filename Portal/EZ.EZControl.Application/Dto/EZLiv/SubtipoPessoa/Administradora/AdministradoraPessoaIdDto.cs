﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora
{
    public class AdministradoraPessoaIdDto
    {
        public int AdministradoraId { get; set; }
        public int PessoaId { get; set; }
    }
}