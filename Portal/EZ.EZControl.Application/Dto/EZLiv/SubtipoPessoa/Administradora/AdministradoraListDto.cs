﻿using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora
{
    public class AdministradoraListDto : PessoaJuridicaListDto
    {
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int ImagemId { get; set; }
        public virtual ICollection<CorretoraInput> Corretoras { get; set; }
    }
}
