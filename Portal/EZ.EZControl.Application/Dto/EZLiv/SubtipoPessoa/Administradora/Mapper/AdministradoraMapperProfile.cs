﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora.Mapper
{
    public class AdministradoraMapperProfile : Profile
    {
        public AdministradoraMapperProfile()
        {
            CreateMap<AdministradoraInput, Domain.EZLiv.SubtiposPessoa.Administradora>()
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<AdministradoraListDto, Domain.EZLiv.SubtiposPessoa.Administradora>().ReverseMap();
            CreateMap<AdministradoraListDto, AdministradoraInput>();
        }
    }
}
