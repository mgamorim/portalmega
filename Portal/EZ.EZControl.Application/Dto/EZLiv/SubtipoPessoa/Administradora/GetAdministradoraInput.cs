﻿using Abp.Runtime.Validation;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora
{
    public class GetAdministradoraInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public PessoaJuridicaInput PessoaJuridica { get; set; }

        public string NomeFantasia { get; set; }
        public string RazaoSocial { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "PessoaJuridica.NomeFantasia";
            }
        }
    }
}
