﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel
{
    public class ResponsavelListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public virtual TipoDeResponsavelEnum TipoDeResponsavel { get; set; }

        public PessoaFisicaListDto PessoaFisica { get; set; }
    }
}
