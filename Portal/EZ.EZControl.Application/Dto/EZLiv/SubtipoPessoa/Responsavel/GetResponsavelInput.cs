﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel
{
    public class GetResponsavelInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
