﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel
{
    public class ResponsavelPessoaIdDto
    {
        public int ResponsavelId { get; set; }
        public int PessoaId { get; set; }
    }
}