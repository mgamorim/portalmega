﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel
{
    public class ResponsavelInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public PessoaInput Pessoa { get; set; }

        public PessoaFisicaInput PessoaFisica { get; set; }

        public virtual TipoDeResponsavelEnum TipoDeResponsavel { get; set; }
    }
}
