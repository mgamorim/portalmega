﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel.Mapper
{
    public class ResponsavelMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Domain.EZLiv.SubtiposPessoa.Responsavel, ResponsavelNomeListDto>()
                .ForMember(destination => destination.Nome, opt => opt.MapFrom(source => source.PessoaFisica.Nome))
                .ReverseMap();

            CreateMap<ResponsavelInput, Domain.EZLiv.SubtiposPessoa.Responsavel>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<ResponsavelListDto, Domain.EZLiv.SubtiposPessoa.Responsavel>().ReverseMap();
            CreateMap<ResponsavelListDto, ResponsavelInput>();
        }
    }
}
