﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel
{
    public class ResponsavelNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}