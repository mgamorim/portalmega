﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora
{
    public class GetGerenteCorretoraInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public int CorretoraId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
