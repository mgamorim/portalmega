﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora
{
    public class GerenteCorretoraInput : EzInputDto
    {
        public bool IsActive { get; set; }
        public PessoaInput Pessoa { get; set; }
        public int PessoaId { get; set; }
        //public CorretoraInput Corretora { get; set; }
        public int CorretoraId { get; set; }
    }
}
