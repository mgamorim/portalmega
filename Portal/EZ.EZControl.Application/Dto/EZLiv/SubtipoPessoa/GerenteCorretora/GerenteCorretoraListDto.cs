﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora
{
    public class GerenteCorretoraListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public PessoaListDto Pessoa { get; set; }
    }
}
