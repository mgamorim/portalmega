﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora.Mapper
{
    public class GerenteCorretoraMapperProfile : Profile
    {
        public GerenteCorretoraMapperProfile()
        {
            CreateMap<GerenteCorretoraInput, Domain.EZLiv.SubtiposPessoa.GerenteCorretora>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<GerenteCorretoraListDto, Domain.EZLiv.SubtiposPessoa.GerenteCorretora>().ReverseMap();
            CreateMap<GerenteCorretoraListDto, GerenteCorretoraInput>();
        }
    }
}
