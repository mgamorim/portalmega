﻿using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente
{
    public class DependenteListDto : BeneficiarioListDto
    {
        public string GrauDeParentesco { get; set; }
    }
}
