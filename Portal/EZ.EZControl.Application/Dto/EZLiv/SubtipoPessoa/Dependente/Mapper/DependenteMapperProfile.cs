﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente.Mapper
{
    public class DependenteMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DependenteInput, Domain.EZLiv.SubtiposPessoa.Dependente>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<DependenteListDto, Domain.EZLiv.SubtiposPessoa.Dependente>().ReverseMap();
            CreateMap<DependenteListDto, DependenteInput>();
        }
    }
}
