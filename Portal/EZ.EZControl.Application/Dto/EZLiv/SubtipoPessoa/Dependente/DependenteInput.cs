﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente
{
    public class DependenteInput : BeneficiarioInput
    {
        public virtual GrauDeParentescoEnum GrauDeParentesco { get; set; }
    }
}
