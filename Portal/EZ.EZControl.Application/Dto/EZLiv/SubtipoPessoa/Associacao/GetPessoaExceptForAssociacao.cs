﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao
{
    public class GetPessoaExceptForAssociacao : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomePessoa { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
