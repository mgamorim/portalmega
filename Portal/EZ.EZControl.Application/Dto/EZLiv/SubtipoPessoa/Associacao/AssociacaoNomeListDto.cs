﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao
{
    public class AssociacaoNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}