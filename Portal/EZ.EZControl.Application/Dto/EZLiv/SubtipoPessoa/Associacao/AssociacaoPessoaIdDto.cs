﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao
{
    public class AssociacaoPessoaIdDto
    {
        public int AssociacaoId { get; set; }
        public int PessoaId { get; set; }
    }
}