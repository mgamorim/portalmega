﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao
{
    public class AssociacaoInput : EzInputDto, IPassivable
    {
        public AssociacaoInput()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int PessoaJuridicaId { get; set; }
        public ParametroPagSeguroInput ParametroPagSeguro { get; set; }
        public int ParametroPagSeguroId { get; set; }
        public int ImagemId { get; set; }
        public string Instrucoes { get; set; }
    }
}
