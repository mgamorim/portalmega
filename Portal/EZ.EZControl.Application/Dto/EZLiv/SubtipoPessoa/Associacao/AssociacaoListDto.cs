﻿using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao
{
    public class AssociacaoListDto : PessoaJuridicaListDto
    {
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int ImagemId { get; set; }
        public string Instrucoes { get; set; }
    }
}
