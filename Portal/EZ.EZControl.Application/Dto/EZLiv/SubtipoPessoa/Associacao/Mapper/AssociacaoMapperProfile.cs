﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao.Mapper
{
    public class AssociacaoMapperProfile : Profile
    {
        public AssociacaoMapperProfile()
        {
            CreateMap<AssociacaoInput, Domain.EZLiv.SubtiposPessoa.Associacao>()
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<AssociacaoListDto, Domain.EZLiv.SubtiposPessoa.Associacao>().ReverseMap();
            CreateMap<AssociacaoListDto, AssociacaoInput>();
        }
    }
}
