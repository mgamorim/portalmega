﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora.Mapper
{
    public class OperadoraMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Domain.EZLiv.SubtiposPessoa.Operadora, OperadoraNomeListDto>()
                .ForMember(destination => destination.Nome, opt => opt.MapFrom(source => source.PessoaJuridica.NomeFantasia))
                .ReverseMap();

            CreateMap<OperadoraInput, Domain.EZLiv.SubtiposPessoa.Operadora>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<OperadoraListDto, Domain.EZLiv.SubtiposPessoa.Operadora>().ReverseMap();
            CreateMap<OperadoraListDto, OperadoraInput>();
        }
    }
}
