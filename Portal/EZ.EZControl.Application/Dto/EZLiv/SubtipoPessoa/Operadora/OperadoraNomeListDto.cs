﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora
{
    public class OperadoraNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}