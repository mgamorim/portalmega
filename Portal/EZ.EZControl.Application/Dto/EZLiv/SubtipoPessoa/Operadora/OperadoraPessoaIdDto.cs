﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora
{
    public class OperadoraPessoaIdDto
    {
        public int OperadoraId { get; set; }
        public int PessoaId { get; set; }
    }
}