﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora
{
    public class OperadoraInput : EzInputDto, IPassivable
    {
        public OperadoraInput()
        {
            this.IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }

        public string CodigoAns { get; set; }

        public PessoaInput Pessoa { get; set; }
        public int? PessoaId { get; set; }

        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int? PessoaJuridicaId { get; set; }

        public int ImagemId { get; set; }
    }
}
