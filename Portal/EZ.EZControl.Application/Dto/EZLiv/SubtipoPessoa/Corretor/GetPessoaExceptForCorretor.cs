﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class GetPessoaExceptForCorretor : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomePessoa { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
