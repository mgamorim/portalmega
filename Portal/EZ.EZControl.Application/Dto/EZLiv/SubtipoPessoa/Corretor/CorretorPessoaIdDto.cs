﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class CorretorPessoaIdDto
    {
        public int CorretorId { get; set; }
        public int PessoaId { get; set; }
    }
}