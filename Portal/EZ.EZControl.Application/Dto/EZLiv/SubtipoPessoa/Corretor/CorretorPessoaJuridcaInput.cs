﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class CorretorPessoaJuridcaInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public virtual CorretorInput Corretor { get; set; }
        public int CorretorId { get; set; }
        public int PessoaId { get; set; }
        public string Cnpj { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Observacao { get; set; }
    }
}
