﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class CorretorInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public PessoaInput Pessoa { get; set; }

        public virtual ICollection<CorretoraInput> Corretoras { get; set; }

        //public PessoaFisicaInput Pessoa {get;set;}

    }
}
