﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor.Mapper
{
    public class CorretorMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CorretorInput, Domain.EZLiv.SubtiposPessoa.Corretor>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<CorretorListDto, Domain.EZLiv.SubtiposPessoa.Corretor>().ReverseMap();
            CreateMap<CorretorListDto, CorretorInput>();
        }
    }
}
