﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Geral.Banco;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class CorretorDadosBancarioInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public virtual CorretorInput Corretor { get; set; }
        public int CorretorId { get; set; }
        public virtual ICollection<BancoInput> Bancos { get; set; }
        public int BancoId { get; set; }
        public string TipoConta { get; set; }
        public string CodigoAgencia { get; set; }
        public string NomeAgencia { get; set; }
        public string DigitoVerificador { get; set; }
        public string ContaCorrente { get; set; }
        public string NomeFavorecido { get; set; }
    }
}
