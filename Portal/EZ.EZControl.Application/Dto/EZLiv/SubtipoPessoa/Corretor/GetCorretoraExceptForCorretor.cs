﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class GetCorretoraExceptForCorretor : PagedAndSortedInputDto, IShouldNormalize
    {
        public int CorretorId { get; set; }

        public string NomePessoa { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public virtual ICollection<CorretoraInput> Corretoras { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
