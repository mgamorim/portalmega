﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor
{
    public class CorretorNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}