﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora
{
    public class CorretoraNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}