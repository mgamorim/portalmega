﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorCorretora;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora
{
    public class CorretoraInput : EzInputDto, IPassivable
    {
        public CorretoraInput()
        {
            IsActive = true;
        }

        public IdInput IdInput { get; set; }
        public string Nome { get; set; }
        public bool IsActive { get; set; }
        public int ImagemId { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int PessoaJuridicaId { get; set; }
        public ParametroPagSeguroInput ParametroPagSeguro { get; set; }
        public int ParametroPagSeguroId { get; set; }
        public DepositoTransferenciaBancariaInput DepositoTransferenciaBancaria { get; set; }
        public int DepositoTransferenciaBancariaId { get; set; }
        public CorretorInput Corretor { get; set; }
        public int CorretorId { get; set; }
        public virtual ICollection<CorretorInput> Corretores { get; set; }
        public virtual ICollection<SupervisorCorretoraInput> Supervisores { get; set; }
        public virtual ICollection<GerenteCorretoraInput> Gerentes { get; set; }
    }
}
