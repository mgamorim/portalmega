﻿using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora
{
    public class CorretoraListDto : PessoaJuridicaListDto
    {
        public string Nome { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public CorretorListDto Corretor { get; set; }
        public int CorretorId { get; set; }
        public int ImagemId { get; set; }

        //Somente para permissões
        public ProdutoDePlanoDeSaudeListDto ProdutoDePlanoDeSaude { get; set; }
    }
}
