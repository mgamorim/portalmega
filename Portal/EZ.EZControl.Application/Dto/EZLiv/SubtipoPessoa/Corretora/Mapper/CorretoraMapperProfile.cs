﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora.Mapper
{
    public class CorretoraMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CorretoraInput, Domain.EZLiv.SubtiposPessoa.Corretora>()
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<CorretoraListDto, Domain.EZLiv.SubtiposPessoa.Corretora>().ReverseMap();
            CreateMap<CorretoraListDto, CorretoraInput>();
        }
    }
}
