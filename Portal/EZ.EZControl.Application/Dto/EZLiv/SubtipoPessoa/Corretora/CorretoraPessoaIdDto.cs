﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora
{
    public class CorretoraPessoaIdDto
    {
        public int CorretorId { get; set; }
        public int PessoaId { get; set; }
    }
}