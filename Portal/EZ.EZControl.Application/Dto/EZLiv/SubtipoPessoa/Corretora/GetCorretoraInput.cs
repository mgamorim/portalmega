﻿using Abp.Runtime.Validation;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora
{
    public class GetCorretoraInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public string Nome { get; set; }
        public string NomeFantasia { get; set; }
        public string RazaoSocial { get; set; }
        public int[] Ids { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "PessoaJuridica.NomeFantasia";
            }
        }
    }
}
