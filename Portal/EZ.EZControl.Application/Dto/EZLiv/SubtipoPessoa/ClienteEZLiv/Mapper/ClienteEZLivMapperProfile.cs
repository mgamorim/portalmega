﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ClienteEZLiv.Mapper
{
    public class ClienteEZLivMapperProfile : Profile
    {
        public ClienteEZLivMapperProfile()
        {
            CreateMap<ClienteEZLivInput, Domain.EZLiv.SubtiposPessoa.ClienteEZLiv>().ReverseMap();
            CreateMap<ClienteEZLivListDto, Domain.EZLiv.SubtiposPessoa.ClienteEZLiv>().ReverseMap();
            CreateMap<ClienteEZLivListDto, ClienteEZLivInput>();
        }
    }
}
