﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ClienteEZLiv
{
    public class GetClienteEZLivInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomeCliente { get; set; }
        public int ClienteId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
