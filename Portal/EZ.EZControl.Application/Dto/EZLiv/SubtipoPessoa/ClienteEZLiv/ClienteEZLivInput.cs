﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ClienteEZLiv
{
    public class ClienteEZLivInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int ClienteId { get; set; }

        public string Observacao { get; set; }

        public ClienteInput Cliente { get; set; }
    }
}
