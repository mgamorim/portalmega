﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.HomologadorAdministradora
{
    public class GetHomologadorAdministradoraInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
