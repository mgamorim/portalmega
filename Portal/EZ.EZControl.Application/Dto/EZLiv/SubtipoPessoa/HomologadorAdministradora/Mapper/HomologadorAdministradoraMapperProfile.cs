﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.HomologadorAdministradora.Mapper
{
    public class HomologadorAdministradoraMapperProfile : Profile
    {
        public HomologadorAdministradoraMapperProfile()
        {
            CreateMap<HomologadorAdministradoraInput, Domain.EZLiv.SubtiposPessoa.HomologadorAdministradora>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<HomologadorAdministradoraListDto, Domain.EZLiv.SubtiposPessoa.HomologadorAdministradora>().ReverseMap();
            CreateMap<HomologadorAdministradoraListDto, HomologadorAdministradoraInput>();
        }
    }
}
