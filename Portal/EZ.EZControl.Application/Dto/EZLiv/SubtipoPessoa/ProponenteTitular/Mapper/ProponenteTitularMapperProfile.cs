﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular.Mapper
{
    public class ProponenteProponenteTitularMapperProfile : Profile
    {
        public ProponenteProponenteTitularMapperProfile()
        {
            CreateMap<ProponenteTitularInput, Domain.EZLiv.SubtiposPessoa.ProponenteTitular>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<ProponenteTitularListDto, Domain.EZLiv.SubtiposPessoa.ProponenteTitular>().ReverseMap();
            CreateMap<ProponenteTitularListDto, ProponenteTitularInput>();
        }
    }
}
