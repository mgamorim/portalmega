﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular
{
    public class GetProponenteTitularInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
