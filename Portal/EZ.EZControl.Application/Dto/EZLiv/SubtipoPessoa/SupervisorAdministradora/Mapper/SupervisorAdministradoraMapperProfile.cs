﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorAdministradora.Mapper
{
    public class SupervisorAdministradoraMapperProfile : Profile
    {
        public SupervisorAdministradoraMapperProfile()
        {
            CreateMap<SupervisorAdministradoraInput, Domain.EZLiv.SubtiposPessoa.SupervisorAdministradora>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<SupervisorAdministradoraListDto, Domain.EZLiv.SubtiposPessoa.SupervisorAdministradora>().ReverseMap();
            CreateMap<SupervisorAdministradoraListDto, SupervisorAdministradoraInput>();
        }
    }
}
