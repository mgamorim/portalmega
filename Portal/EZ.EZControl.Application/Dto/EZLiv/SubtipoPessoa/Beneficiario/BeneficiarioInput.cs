﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.RespostaDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public PessoaInput Pessoa { get; set; }

        public PessoaFisicaInput PessoaFisica { get; set; }

        public int EstadoId { get; set; }

        public int CorretoraId { get; set; }

        public int ProfissaoId { get; set; }
        public string ProfissaoTitulo { get; set; }

        public virtual TipoDeBeneficiarioEnum TipoDeBeneficiario { get; set; }

        public string NomeDaMae { get; set; }

        public string NumeroDoCartaoNacionalDeSaude { get; set; }

        public string DeclaracaoDeNascidoVivo { get; set; }
        public List<RespostaDeDeclaracaoDeSaudeInput> RespostasDeclaracaoDeSaude { get; set; }

    }
}
