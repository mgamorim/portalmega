﻿namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioPessoaIdDto
    {
        public int BeneficiariolId { get; set; }
        public int PessoaId { get; set; }
    }
}