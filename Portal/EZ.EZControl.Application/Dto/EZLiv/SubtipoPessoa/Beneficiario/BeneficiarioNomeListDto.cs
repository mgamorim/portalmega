﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}