﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada
{
    public class ItemDeRedeCredenciadaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public bool IsRedeDiferenciada { get; set; }
        public int RedeCredenciadaId { get; set; }
        public virtual RedeCredenciadaInput RedeCredenciada { get; set; }
        public int UnidadeDeSaudeId { get; set; }
        public virtual UnidadeDeSaudeBaseInput UnidadeDeSaude { get; set; }
        public virtual bool IsHospital { get; set; }
        public virtual bool IsLaboratorio { get; set; }
    }
}
