﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase
{
    public class GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id;

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "PessoaJuridica.NomeFantasia";
            }
        }
    }
}
