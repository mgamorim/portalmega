﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada.Mapper
{
    public class ItemDeRedeCredenciadaMapperProfile : Profile
    {
        public ItemDeRedeCredenciadaMapperProfile()
        {
            CreateMap<ItemDeRedeCredenciadaInput, Domain.EZLiv.Geral.ItemDeRedeCredenciada>().ReverseMap();
            CreateMap<ItemDeRedeCredenciadaListDto, Domain.EZLiv.Geral.ItemDeRedeCredenciada>().ReverseMap();
            CreateMap<ItemDeRedeCredenciadaListDto, ItemDeRedeCredenciadaInput>();
        }
    }
}
