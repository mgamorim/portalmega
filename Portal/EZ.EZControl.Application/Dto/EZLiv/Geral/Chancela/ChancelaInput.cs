﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using System.Collections.Generic;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao;

namespace EZ.EZControl.Dto.EZLiv.Geral.Chancela
{
    public class ChancelaInput : EzInputDto, IPassivable
    {
        public ChancelaInput()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public decimal TaxaDeAdesao { get; set; }
        public decimal TaxaMensal { get; set; }
        public List<ProfissaoInput> Profissoes { get; set; }
        public int AssociacaoId { get; set; }
        public AssociacaoInput Associacao { get; set; }
    }
}
