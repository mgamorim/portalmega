﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.Chancela
{
    public class ChancelaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public decimal TaxaDeAdesao { get; set; }
        public decimal TaxaMensal { get; set; }
        public List<ProfissaoInput> Profissoes { get; set; }
    }
}
