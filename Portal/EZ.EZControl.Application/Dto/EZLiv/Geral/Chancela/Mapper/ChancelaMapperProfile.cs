﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.Chancela.Mapper
{
    public class ChancelaMapperProfile : Profile
    {
        public ChancelaMapperProfile()
        {
            CreateMap<ChancelaInput, Domain.EZLiv.Geral.Chancela>().ReverseMap();
            CreateMap<ChancelaListDto, Domain.EZLiv.Geral.Chancela>().ReverseMap();
            CreateMap<ChancelaListDto, ChancelaInput>();
        }
    }
}
