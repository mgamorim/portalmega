﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.Laboratorio.Mapper
{
    public class LaboratorioMapperProfile : Profile
    {
        public LaboratorioMapperProfile()
        {
            CreateMap<LaboratorioInput, Domain.EZLiv.Geral.Laboratorio>().ReverseMap();
            CreateMap<LaboratorioListDto, Domain.EZLiv.Geral.Laboratorio>().ReverseMap();
            CreateMap<LaboratorioListDto, LaboratorioInput>();
        }
    }
}
