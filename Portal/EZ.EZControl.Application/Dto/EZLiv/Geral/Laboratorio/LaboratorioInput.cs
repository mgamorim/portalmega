﻿using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZLiv.Geral.Laboratorio
{
    public class LaboratorioInput : UnidadeDeSaudeBaseInput
    {
        public LaboratorioInput()
        {
            IsActive = true;
            TipoDeUnidade = Domain.EZLiv.Enums.TipoDeUnidadeEnum.Laboratorio;
        }
    }
}
