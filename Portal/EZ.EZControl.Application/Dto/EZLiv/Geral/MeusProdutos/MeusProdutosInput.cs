﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.MeusProdutos
{
    public class MeusProdutosInput : EzInputDto
    {
        public string Nome { get; set; }
        public string Administradora { get; set; }
        public bool Reembolso { get; set; }
        public string Abrangencia { get; set; }
        public decimal Valor { get; set; }
    }
}
