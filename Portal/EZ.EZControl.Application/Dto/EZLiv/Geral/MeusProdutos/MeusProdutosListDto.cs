﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.MeusProdutos
{
    public class MeusProdutosListDto : EzFullAuditedInputDto
    {
        public string Nome { get; set; }
        public string Administradora { get; set; }
        public bool Reembolso { get; set; }
        public string Abrangencia { get; set; }
        public string StatusProposta { get; set; }
        public decimal Valor { get; set; }
        public int PropostaId { get; set; }
    }
}
