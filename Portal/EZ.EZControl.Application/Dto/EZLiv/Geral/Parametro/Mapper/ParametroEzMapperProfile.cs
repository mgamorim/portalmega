﻿using EZ.EZControl.Dto.Core.Geral.Parametro.Mapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.Parametro.Mapper
{
    public class ParametroEzMapperProfile : ParametroMapperProfile
    {
        protected override void Configure()
        {
            CreateMap<ParametroEZLivInput, Domain.EZLiv.Geral.ParametroEZLiv>().ReverseMap();
            CreateMap<ParametroEZLivListDto, Domain.EZLiv.Geral.ParametroEZLiv>().ReverseMap();
            CreateMap<ParametroEZLivListDto, ParametroEZLivInput>();
        }
    }
}
