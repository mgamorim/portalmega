﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase
{
    public class GetUnidadeDeSaudeBaseInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int[] Ids { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
