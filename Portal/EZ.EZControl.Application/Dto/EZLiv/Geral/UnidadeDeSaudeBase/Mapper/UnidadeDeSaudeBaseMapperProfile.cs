﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase.Mapper
{
    public class UnidadeDeSaudeBaseMapperProfile : Profile
    {
        public UnidadeDeSaudeBaseMapperProfile()
        {
            CreateMap<UnidadeDeSaudeBaseInput, Domain.EZLiv.Geral.UnidadeDeSaudeBase>().ReverseMap();
            CreateMap<UnidadeDeSaudeBaseListDto, Domain.EZLiv.Geral.UnidadeDeSaudeBase>().ReverseMap();
            CreateMap<UnidadeDeSaudeBaseListDto, UnidadeDeSaudeBaseInput>();
        }
    }
}
