﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase
{
    public class UnidadeDeSaudeBaseInput : EzInputDto, IPassivable
    {
        public UnidadeDeSaudeBaseInput()
        {
        }
        public string Nome { get; set; }
        public bool IsActive { get; set; }
        public string Email { get; set; }
        public string Site { get; set; }
        public int PessoaJuridicaId { get; set; }
        public virtual PessoaJuridicaInput PessoaJuridica { get; set; }
        public int EnderecoId { get; set; }
        public virtual EnderecoInput Endereco { get; set; }
        public int? Telefone1Id { get; set; }
        public virtual TelefoneInput Telefone1 { get; set; }
        public int? Telefone2Id { get; set; }
        public virtual TelefoneInput Telefone2 { get; set; }
        public virtual TipoDeUnidadeEnum TipoDeUnidade { get; set; }
        public virtual ICollection<EspecialidadeInput> Especialidades { get; set; }
    }
}
