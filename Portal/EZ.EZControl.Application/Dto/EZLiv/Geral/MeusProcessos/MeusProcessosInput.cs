﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.MeusProcessos
{
    public class MeusProcessosInput : EzInputDto
    {
        public string Nome { get; set; }
        public string Administradora { get; set; }
        public bool Reembolso { get; set; }
        public string Abrangencia { get; set; }
        public decimal Valor { get; set; }
    }
}
