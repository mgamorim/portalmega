﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude
{
    public class QuestionarioDeDeclaracaoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual OperadoraInput Operadora { get; set; }
        public int OperadoraId { get; set; }
        public virtual ICollection<ItemDeDeclaracaoDeSaudeInput> ItensDeDeclaracaoDeSaude { get; set; }
    }
}
