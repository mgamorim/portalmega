﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude.Mapper
{
    public class QuestionarioDeDeclaracaoDeSaudeMapperProfile : Profile
    {
        public QuestionarioDeDeclaracaoDeSaudeMapperProfile()
        {
            CreateMap<QuestionarioDeDeclaracaoDeSaudeInput, Domain.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<QuestionarioDeDeclaracaoDeSaudeListDto, Domain.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<QuestionarioDeDeclaracaoDeSaudeListDto, QuestionarioDeDeclaracaoDeSaudeInput>();
        }
    }
}
