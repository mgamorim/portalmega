﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria.Mapper
{
    public class ValorPorFaixaEtariaMapperProfile : Profile
    {
        public ValorPorFaixaEtariaMapperProfile()
        {
            CreateMap<ValorPorFaixaEtariaInput, Domain.EZLiv.Geral.ValorPorFaixaEtaria>().ReverseMap();
            CreateMap<ValorPorFaixaEtariaListDto, Domain.EZLiv.Geral.ValorPorFaixaEtaria>().ReverseMap();
            CreateMap<ValorPorFaixaEtariaListDto, ValorPorFaixaEtariaInput>();
        }
    }
}
