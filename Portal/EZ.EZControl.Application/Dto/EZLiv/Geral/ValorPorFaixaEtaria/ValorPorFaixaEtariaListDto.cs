﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria;

namespace EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria
{
    public class ValorPorFaixaEtariaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public ContratoListDto Contrato { get; set; }
        public int ContratoId { get; set; }

        public FaixaEtariaListDto FaixaEtaria { get; set; }
        public int FaixaEtariaId { get; set; }

        public decimal Valor { get; set; }
    }
}
