﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria
{
    public class GetValorPorFaixaEtariaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public decimal Valor { get; set; }

        public int ContratoId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Valor";
            }
        }
    }
}
