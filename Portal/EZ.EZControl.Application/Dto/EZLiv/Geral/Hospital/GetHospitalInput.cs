﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.Hospital
{
    public class GetHospitalInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Site { get; set; }
        public string Email { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
