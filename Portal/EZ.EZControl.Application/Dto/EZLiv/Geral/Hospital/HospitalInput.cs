﻿using EZ.EZControl.Dto.EZLiv.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZLiv.Geral.Hospital
{
    public class HospitalInput : UnidadeDeSaudeBaseInput
    {
        public HospitalInput()
        {
            IsActive = true;
            TipoDeUnidade = Domain.EZLiv.Enums.TipoDeUnidadeEnum.Hospital;
        }
    }
}
