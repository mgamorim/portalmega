﻿using Abp.Runtime.Validation;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Getal.Hospital
{
    public class GetEspecialidadeExceptForHospital : PagedAndSortedInputDto, IShouldNormalize
    {
        public int LaboratorioId { get; set; }

        public string Nome { get; set; }

        public string Codigo { get; set; }

        public virtual ICollection<EspecialidadeInput> Especialidades { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
