﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.Hospital.Mapper
{
    public class HospitalMapperProfile : Profile
    {
        public HospitalMapperProfile()
        {
            CreateMap<HospitalInput, Domain.EZLiv.Geral.Hospital>().ReverseMap();
            CreateMap<HospitalListDto, Domain.EZLiv.Geral.Hospital>().ReverseMap();
            CreateMap<HospitalListDto, HospitalInput>();
        }
    }
}
