﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario.Mapper
{
    public class DeclaracaoDoBeneficiarioMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DeclaracaoDoBeneficiarioInput, Domain.EZLiv.Geral.DeclaracaoDoBeneficiario>().ReverseMap();
            CreateMap<DeclaracaoDoBeneficiarioListDto, Domain.EZLiv.Geral.DeclaracaoDoBeneficiario>().ReverseMap();
            CreateMap<DeclaracaoDoBeneficiarioListDto, DeclaracaoDoBeneficiarioInput>();
        }
    }
}
