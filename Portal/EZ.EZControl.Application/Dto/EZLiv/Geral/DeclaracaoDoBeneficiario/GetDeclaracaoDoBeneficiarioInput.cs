﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario
{
    public class GetDeclaracaoDoBeneficiarioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
