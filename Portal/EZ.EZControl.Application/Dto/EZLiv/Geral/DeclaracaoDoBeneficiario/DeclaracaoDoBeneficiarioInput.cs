﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario;

namespace EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario
{
    public class DeclaracaoDoBeneficiarioInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual ItemDeDeclaracaoDoBeneficiarioInput[] ItensDeDeclaracaoDoBeneficiario { get; set; }
        public int[] ItensDeDeclaracaoDoBeneficiarioIds { get; set; }
    }
}
