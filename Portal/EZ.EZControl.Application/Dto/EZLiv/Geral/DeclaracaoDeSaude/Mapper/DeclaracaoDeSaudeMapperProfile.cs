﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDeSaude.Mapper
{
    public class DeclaracaoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DeclaracaoDeSaudeInput, Domain.EZLiv.Geral.DeclaracaoDeSaude>().ReverseMap();
            CreateMap<DeclaracaoDeSaudeListDto, Domain.EZLiv.Geral.DeclaracaoDeSaude>().ReverseMap();
            CreateMap<DeclaracaoDeSaudeListDto, DeclaracaoDeSaudeInput>();
        }
    }
}
