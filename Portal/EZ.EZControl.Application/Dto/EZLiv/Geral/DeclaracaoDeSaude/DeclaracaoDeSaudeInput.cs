﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDeSaude
{
    public class DeclaracaoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual ProponenteTitularInput Titular { get; set; }
        public int TitularId { get; set; }
        public virtual List<DependenteInput> Dependentes { get; set; }
        public int[] DependentesIds { get; set; }
        public virtual List<ItemDeDeclaracaoDeSaudeInput> ItensDeDeclaracaoDeSaude { get; set; }
        public int[] ItensDeDeclaracaoDeSaudeIds { get; set; }
    }
}
