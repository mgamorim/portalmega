﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;

namespace EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDeSaude
{
    public class DeclaracaoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual ProponenteTitularListDto TitularListDto { get; set; }
        public int TitularId { get; set; }
        public virtual DependenteListDto[] Dependentes { get; set; }
        public int[] DependentesIds { get; set; }
        public virtual ItemDeDeclaracaoDeSaudeListDto[] ItensDeDeclaracaoDeSaude { get; set; }
        public int[] ItensDeDeclaracaoDeSaudeIds { get; set; }
    }
}
