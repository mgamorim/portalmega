﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria
{
    public class FaixaEtariaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        public int IdadeInicial { get; set; }

        public int? IdadeFinal { get; set; }
    }
}
