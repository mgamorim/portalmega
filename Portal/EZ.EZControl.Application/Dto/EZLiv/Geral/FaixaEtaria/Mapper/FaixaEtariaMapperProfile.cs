﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria.Mapper
{
    public class FaixaEtariaMapperProfile : Profile
    {
        public FaixaEtariaMapperProfile()
        {
            CreateMap<FaixaEtariaInput, Domain.EZLiv.Geral.FaixaEtaria>().ReverseMap();
            CreateMap<FaixaEtariaListDto, Domain.EZLiv.Geral.FaixaEtaria>().ReverseMap();
            CreateMap<FaixaEtariaListDto, FaixaEtariaInput>();
        }
    }
}
