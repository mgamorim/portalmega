﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria
{
    public class GetFaixaEtariaExceptForIdsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }

        public int[] Ids { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}
