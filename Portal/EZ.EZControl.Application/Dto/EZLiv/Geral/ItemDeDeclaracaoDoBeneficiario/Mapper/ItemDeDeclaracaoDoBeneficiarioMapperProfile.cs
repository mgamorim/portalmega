﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario.Mapper
{
    public class ItemDeDeclaracaoDoBeneficiarioMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ItemDeDeclaracaoDoBeneficiarioInput, Domain.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario>().ReverseMap();
            CreateMap<ItemDeDeclaracaoDoBeneficiarioListDto, Domain.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario>().ReverseMap();
            CreateMap<ItemDeDeclaracaoDoBeneficiarioListDto, ItemDeDeclaracaoDoBeneficiarioInput>();
        }
    }
}
