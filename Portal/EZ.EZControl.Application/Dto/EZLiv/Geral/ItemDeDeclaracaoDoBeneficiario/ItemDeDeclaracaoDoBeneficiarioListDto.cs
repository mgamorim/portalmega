﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario
{
    public class ItemDeDeclaracaoDoBeneficiarioListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Pergunta { get; set; }
        public string Ajuda { get; set; }
        public int Ordenacao { get; set; }
    }
}
