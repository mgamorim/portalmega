﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.Contrato.Mapper
{
    public class ContratoMapperProfile : Profile
    {
        public ContratoMapperProfile()
        {
            CreateMap<ContratoInput, Domain.EZLiv.Geral.Contrato>().ReverseMap();
            CreateMap<ContratoListDto, Domain.EZLiv.Geral.Contrato>().ReverseMap();
            CreateMap<ContratoListDto, ContratoInput>();
        }
    }
}
