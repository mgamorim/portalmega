﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.Contrato
{
    public class GetContratoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Conteudo { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "DataCriacao";
            }
        }
    }
}
