﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ValorPorFaixaEtaria;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.Contrato
{
    public class ContratoInput : EzInputDto, IPassivable
    {
        public ContratoInput()
        {
            this.DataCriacao = DateTime.Now;
        }

        public bool IsActive { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public List<ValorPorFaixaEtariaInput> Valores { get; set; }
    }
}
