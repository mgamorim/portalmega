﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.FaixaEtaria;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;

namespace EZ.EZControl.Dto.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria
{
    public class IndiceDeReajustePorFaixaEtariaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public decimal IndiceDeReajuste { get; set; }
        public virtual FaixaEtariaInput FaixaEtaria { get; set; }
        public int FaixaEtariaId { get; set; }
        public virtual OperadoraInput Operadora { get; set; }
        public int OperadoraId { get; set; }
    }
}
