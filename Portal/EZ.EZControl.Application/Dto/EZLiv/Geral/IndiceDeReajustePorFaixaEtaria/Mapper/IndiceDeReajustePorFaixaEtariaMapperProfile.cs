﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria.Mapper
{
    public class IndiceDeReajustePorFaixaEtariaMapperProfile : Profile
    {
        public IndiceDeReajustePorFaixaEtariaMapperProfile()
        {
            CreateMap<IndiceDeReajustePorFaixaEtariaInput, Domain.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria>().ReverseMap();
            CreateMap<IndiceDeReajustePorFaixaEtariaListDto, Domain.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria>().ReverseMap();
            CreateMap<IndiceDeReajustePorFaixaEtariaListDto, IndiceDeReajustePorFaixaEtariaInput>();
        }
    }
}
