﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria
{
    public class GetIndiceDeReajustePorFaixaEtariaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public decimal IndiceDeReajuste { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "IndiceDeReajuste";
            }
        }
    }
}
