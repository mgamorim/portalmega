﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoDataDeVigenciaInput : EzInputDto
    {
        public DateTime InicioDaVigencia { get; set; }
    }
}
