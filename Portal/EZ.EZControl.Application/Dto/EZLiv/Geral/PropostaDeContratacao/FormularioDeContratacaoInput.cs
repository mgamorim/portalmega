﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDoBeneficiario;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    [Obsolete]
    public class FormularioDeContratacaoInput : EzInputDto
    {
        public string PathLogoAdministradora { get; set; }
        public string PathLogoOperadora { get; set; }
        public PropostaDeContratacaoInput Proposta { get; set; }
        public CorretoraFormularioInput Corretora { get; set; }
        public CorretorFormularioInput Corretor { get; set; }
        public AdministradoraFormularioInput Administradora { get; set; }
        public OperadoraFormularioInput Operadora { get; set; }
        public IEnumerable<DeclaracaoDeSaudeFormularioInput> DeclaracaoDeSaude { get; set; }
        public IEnumerable<PesoAlturaFormularioInput> PesoAltura { get; set; }
        public ContratoInput Contrato { get; set; }
        public IEnumerable<DetalhesDeclaracaoDeSaudeInput> DetalhesDeclaracaoDeSaude { get; set; }
        public IEnumerable<ValorPorFaixaEtariaFormularioInput> ValoresPorFaixaEtaria { get; set; }
        public IEnumerable<PercentualPorFaixaEtariaInput> PercentuaisPorFaixaEtaria { get; set; }
        public ItemDeDeclaracaoDoBeneficiarioInput ItemDeDeclaracaoDoBeneficiarioMarcado { get; set; }
        public decimal ValorDoPlano { get; set; }
        public ChancelaInput Chancela { get; set; }
        public decimal? ValorMensal
        {
            get
            {
                var valorMensalChancela = Chancela != null ? Chancela.TaxaMensal : 0;
                //var valorAdesaoChancela = Chancela != null ? Chancela.TaxaDeAdesao : 0;
                if (ValoresPorFaixaEtaria == null)
                {
                    return null;
                }
                return (ValoresPorFaixaEtaria.Sum(x => x.Valor) + valorMensalChancela);
            }
        }
        public decimal? TaxaDeAngariacao
        {
            get
            {
                if (ValoresPorFaixaEtaria == null)
                {
                    return null;
                }
                var valoresPorFaixaEtaria = ValoresPorFaixaEtaria.Sum(x => x.Valor);
                var taxaDeAdesao = Chancela != null ? Chancela.TaxaDeAdesao : 0;
                return valoresPorFaixaEtaria + taxaDeAdesao;
            }
        }
        public string EstadoNome { get; set; }
        public string CarenciasHTML { get; set; }
        public string IndicesReajusteHTML { get; set; }
    }



    public class CorretoraFormularioInput
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
    }
    public class CorretorFormularioInput
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string CPF { get; set; }
    }
    public class AdministradoraFormularioInput
    {
        public string Nome { get; set; }
        public string CNPJ { get; set; }
        public string LogoPath { get; set; }
    }
    public class OperadoraFormularioInput
    {
        public string Nome { get; set; }
        public string LogoPath { get; set; }
    }
    public class DeclaracaoDeSaudeFormularioInput
    {
        public string Pergunta { get; set; }
        public string MarcadaTitular { get; set; }
        public string MarcadaDep1 { get; set; }
        public string MarcadaDep2 { get; set; }
        public string MarcadaDep3 { get; set; }
        public string MarcadaDep4 { get; set; }
    }
    public class PesoAlturaFormularioInput
    {
        public string Pergunta { get; set; }
        public string PesoTitular { get; set; }
        public string AlturaTitular { get; set; }
        public string PesoDep1 { get; set; }
        public string AlturaDep1 { get; set; }
        public string PesoDep2 { get; set; }
        public string AlturaDep2 { get; set; }
        public string PesoDep3 { get; set; }
        public string AlturaDep3 { get; set; }
        public string PesoDep4 { get; set; }
        public string AlturaDep4 { get; set; }
    }
    public class DetalhesDeclaracaoDeSaudeInput
    {
        public string Pergunta { get; set; }
        public string Nome { get; set; }
        public string Ano { get; set; }
        public string Detalhes { get; set; }
    }
    public class ValorPorFaixaEtariaFormularioInput
    {
        public string Beneficiario { get; set; }
        public int Idade { get; set; }
        public decimal Valor { get; set; }
    }

    public class PercentualPorFaixaEtariaInput
    {
        public string FaixaEtaria { get; set; }
        public decimal Percentual { get; set; }
    }
}
