﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao.Mapper
{
    public class PropostaDeContratacaoMapperProfile : Profile
    {
        public PropostaDeContratacaoMapperProfile()
        {
            CreateMap<PropostaDeContratacaoInput, Domain.EZLiv.Geral.PropostaDeContratacao>().ReverseMap();
            CreateMap<PropostaDeContratacaoListDto, Domain.EZLiv.Geral.PropostaDeContratacao>().ReverseMap();
            CreateMap<PropostaDeContratacaoListDto, PropostaDeContratacaoInput>();
        }
    }
}
