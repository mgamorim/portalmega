﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class FichaDoRelatorioV1Input
    {
        public bool TitularEResponsavelLegal { get; set; }

        public TitularV1Input Titular { get; set; }
        public ProfissaoV1Input Profissao { get; set; }
        public ProdutoDePlanoDeSaudeV1Input Produto { get; set; }
        public ChancelaV1Input Chancela { get; set; }
        public EstadoV1Input Estado { get; set; }
        public CorretoraV1Input Corretora { get; set; }
        public CorretorV1Input Corretor { get; set; }
        public DataAceite DataAceite { get; set; }
        public string numero { get; set; }
    }
}
