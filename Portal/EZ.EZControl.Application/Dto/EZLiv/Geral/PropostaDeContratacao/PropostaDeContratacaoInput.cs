﻿using EZ.EZControl.Authorization.PermissoesPorEmpresa.Dto;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.GerenteCorretora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.SupervisorCorretora;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoInput : EzFullAuditedInputDto
    {
        public string Numero { get; set; }
        public ProponenteTitularInput Titular { get; set; }
        public int TitularId { get; set; }
        public ProdutoDePlanoDeSaudeInput Produto { get; set; }
        public int ProdutoId { get; set; }
        public ResponsavelInput Responsavel { get; set; }
        public int ResponsavelId { get; set; }
        public bool Aceite { get; set; }
        public bool AceiteDaDeclaracaoDeSaude { get; set; }
        public TipoDeHomologacaoEnum TipoDeHomologacao { get; set; }
        public DateTime? DataHoraDaHomologacao { get; set; }
        public string ObservacaoHomologacao { get; set; }
        public DateTime? DataHoraDoAceite { get; set; }
        public ContratoInput ContratoVigenteNoAceite { get; set; }
        public int? ContratoVigenteNoAceiteId { get; set; }
        public ContratoInput UltimoContratoAceito { get; set; }
        public int? UltimoContratoAceitoId { get; set; }
        public CorretorInput Corretor { get; set; }
        public int CorretorId { get; set; }
        public CorretoraInput Corretora { get; set; }
        public int? CorretoraId { get; set; }
        public PedidoInput Pedido { get; set; }
        public int? PedidoId { get; set; }
        public FormaDeContratacaoEnum FormaDeContratacao { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public StatusDaPropostaEnum StatusDaProposta { get; set; }
        public virtual List<ArquivoDocumentoInput> Documentos { get; set; }
        public VigenciaInput Vigencia { get; set; }
        public int? VigenciaId { get; set; }
        public ChancelaInput Chancela { get; set; }
        public int? ChancelaId { get; set; }
        public DateTime? InicioDeVigencia { get; set; }
        public TipoDePropostaEnum TipoDeProposta { get; set; }
        public UserDto UsuarioTitular { get; set; }
        public int? UsuarioTitularId { get; set; }
        public UserDto UsuarioResponsavel { get; set; }
        public int? UsuarioResponsavelId { get; set; }
        public ICollection<RelatorioInput> Aditivos { get; set; }
        public int EstadoId { get; set; }
        public EstadoInput Estado { get; set; }
        public int ProfissaoId { get; set; }
        public ProfissaoInput Profissao { get; set; }
        public bool TitularResponsavelLegal { get; set; }
        public int? ItemDeDeclaracaoDoBeneficiarioId { get; set; }
        public bool AceiteCorretor { get; set; }
        public DateTime? DataAceiteCorretor { get; set; }
        public int? GerenteId { get; set; }
        public int? SupervisorId { get; set; }
    }
}
