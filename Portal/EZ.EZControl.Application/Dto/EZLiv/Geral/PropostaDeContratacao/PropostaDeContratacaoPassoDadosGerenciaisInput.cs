﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoDadosGerenciaisInput : EzInputDto
    {
        public int SupervisorId { get; set; }
        public int GerenteId { get; set; }
        public int ChancelaId { get; set; }
        public ICollection<RelatorioInput> Aditivos { get; set; }
    }
}
