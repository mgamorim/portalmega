﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoPagamentoInput : EzInputDto
    {
        public FormaDePagamentoEnum FormaDePagamento { get; set; }
        public bool RecebidoDeposito { get; set; }
    }
}
