﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoSelecionarPlanoInput : EzInputDto
    {
        public PassoDaPropostaEnum PassoDaProposta { get; set; }

        [Required]
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public int CorretoraId { get; set; }

    }
}
