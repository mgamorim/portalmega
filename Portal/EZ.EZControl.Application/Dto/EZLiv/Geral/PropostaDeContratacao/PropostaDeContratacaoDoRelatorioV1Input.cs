﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoDoRelatorioV1Input
    {
        public string Numero { get; set; }
        public decimal ValorDaProposta { get; set; }
        public decimal TaxaDeAngariacao { get; set; }
        public string InicioDeVigencia { get; set; }
        public bool TitularEResponsavelLegal { get; set; }
        public string FormaDeContratacao { get; set; }
        public string TipoDeProposta { get; set; }

        public TitularV1Input Titular { get; set; }
        public ProfissaoV1Input Profissao { get; set; }
        public ProdutoDePlanoDeSaudeV1Input Produto { get; set; }
        public ChancelaV1Input Chancela { get; set; }
        public EstadoV1Input Estado { get; set; }
        public CorretoraV1Input Corretora { get; set; }
        public CorretorV1Input Corretor { get; set; }
        public DataAceite DataAceite { get; set; }
        public string DataAceiteBeneficiario { get; set; }
        public string ProtocoloAceiteBeneficiario { get; set; }
        public string DataAceiteCorretor { get; set; }
        public string ProtocoloAceiteCorretor { get; set; }
        public string DataAceiteAdm { get; set; }
        public string ProtocoloAceiteAdm { get; set; }

        public IEnumerable<ItemDeDeclaracaoDoBeneficiarioV1Input> TermosEAceiteDaDeclaracaoDeSaude { get; set; }
        public IEnumerable<ValorPorBeneficiarioPorIdadeV1Input> ValoresPorPorBeneficiarioPorIdade { get; set; }
        public IEnumerable<PerguntaDaDeclaracaoDeSaudeV1Input> PerguntasDaDeclaracaoDeSaude { get; set; }
        public IEnumerable<PerguntaDaDeclaracaoDeSaudePositivaV1Input> PerguntasDaDeclaracaoDeSaudePositivas { get; set; }
        public IEnumerable<PesoAlturaV1Input> PerguntasDePesoEAltura { get; set; }
    }

    public class TitularV1Input
    {
        public string Nome { get; set; }
        public int Idade { get; set; }
        public string EstadoCivil { get; set; }
        public string DataDeNascimento { get; set; }
        public string Sexo { get; set; }
        public string Nacionalidade { get; set; }
        public string NomeDaMae { get; set; }
        public string NumeroDoCartaoNacionalDeSaude { get; set; }
        public string DeclaracaoDeNascidoVivo { get; set; }
        public string Identidade { get; set; }
        public string OrgaoExpedidorDaIdentidade { get; set; }
        public string Cpf { get; set; }
        public string TelefoneCelular { get; set; }
        public string EmailPrincipal { get; set; }
        public string AssinaturaCurta { get; set; }
        public string AssinaturaCompleta { get; set; }

        public string Matricula { get; set; }
        public string DescontoFolha { get; set; }
        public string Boleto { get; set; }

        public string DebitoContaSim { get; set; }
        public string DebitoContaNao { get; set; }

        public string FolhaFichaSim { get; set; }
        public string FolhaFichaNao { get; set; }

        public EnderecoV1Input Endereco { get; set; }
        public List<DependenteV1Input> Dependentes { get; set; }
        public ResponsavelV1Input Responsavel { get; set; }
    }
    public class DependenteV1Input
    {
        public string Nome { get; set; }
        public string NomeDaMae { get; set; }
        public string Cpf { get; set; }
        public string DataDeNascimento { get; set; }
        public string Nacionalidade { get; set; }
        public string Sexo { get; set; }
        public string EmailPrincipal { get; set; }
        public string EstadoCivil { get; set; }
        public string NumeroDoCartaoNacionalDeSaude { get; set; }
        public string DeclaracaoDeNascidoVivo { get; set; }
        public string GrauDeParentesco { get; set; }

        public string Identidade { get; set; }
        public string IdentidadedaMae { get; set; }
        public string CpfDaMae { get; set; }

    }
    public class ResponsavelV1Input
    {
        public string Nome { get; set; }
        public string TipoDeResponsavel { get; set; }
        public string TelefoneCelular { get; set; }
        public string Cpf { get; set; }
        public string DataDeNascimento { get; set; }
        public string Nacionalidade { get; set; }
        public string Sexo { get; set; }
        public string EmailPrincipal { get; set; }
        public string EstadoCivil { get; set; }
        public string AssinaturaCurta { get; set; }
        public string AssinaturaCompleta { get; set; }
    }
    public class EnderecoV1Input
    {

        public string Logradouro { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string CEP { get; set; }
        public string Referencia { get; set; }
        public string Estado { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string LogradouroComNumeroEComplemento { get; set; }
    }
    public class OperadoraV1Input
    {
        public string Nome { get; set; }
    }
    public class AdministradoraV1Input
    {
        public string Nome { get; set; }
        public string CNPJ { get; set; }
        public string AssinaturaCompleta { get; set; }
        public string AssinaturaCurta { get; set; }

    }
    public class ProdutoDePlanoDeSaudeV1Input
    {
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public string NumeroDeRegistro { get; set; }
        public bool Reembolso { get; set; }
        public string DescricaoDoReembolso { get; set; }
        public string Abrangencia { get; set; }
        public string SegmentacaoAssistencial { get; set; }
        public string Acomodacao { get; set; }
        public string LinkRedeCredenciada { get; set; }
        public string CartaDeOrientacao { get; set; }
        public int RelatorioProspostaDeContratacaoId { get; set; }
        public int RelatorioFichaDeEntidadeId { get; set; }

        public AdministradoraV1Input Administradora { get; set; }
        public OperadoraV1Input Operadora { get; set; }
    }
    public class ProfissaoV1Input
    {
        public string Nome { get; set; }
    }
    public class CorretoraV1Input
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }

        public string Email { get; set; }

        public GerenteCorretoraV1Input Gerente { get; set; }
        public SupervisorCorretoraV1Input Supervisor { get; set; }
    }

    public class GerenteCorretoraV1Input
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string email { get; set; }
        public string celular { get; set; }
    }

    public class SupervisorCorretoraV1Input
    {
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string email { get; set; }
        public string celular { get; set; }
    }

    public class CorretorV1Input
    {
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
        public string AssinaturaCurta { get; set; }
        public string AssinaturaCompleta { get; set; }
    }
    public class ItemDeDeclaracaoDoBeneficiarioV1Input
    {
        public bool EoTermoEscolhido { get; set; }
        public string Termo { get; set; }
    }
    public class EstadoV1Input
    {
        public string Nome { get; set; }
    }
    public class ChancelaV1Input
    {
        public string Nome { get; set; }
        public decimal TaxaDeAdesao { get; set; }
        public decimal TaxaMensal { get; set; }
        public EntidadeV1Input Entidade { get; set; }
    }
    public class EntidadeV1Input
    {
        public string Nome { get; set; }
    }


    public class PesoAlturaV1Input
    {
        public string Pergunta { get; set; }
        public string RespostaTitular { get; set; }
        public string RespostaDep1 { get; set; }
        public string RespostaDep2 { get; set; }
        public string RespostaDep3 { get; set; }
        public string RespostaDep4 { get; set; }
    }
    public class PerguntaDaDeclaracaoDeSaudeV1Input
    {
        public string Pergunta { get; set; }
        public string PerguntaEnumerada { get; set; }
        public string Numero { get; set; }
        public string RespostaTitular { get; set; }
        public string Ano { get; set; }
        public string Detalhes { get; set; }
        public string RespostaDep1 { get; set; }
        public string RespostaDep2 { get; set; }
        public string RespostaDep3 { get; set; }
        public string RespostaDep4 { get; set; }
    }
    public class PerguntaDaDeclaracaoDeSaudePositivaV1Input
    {
        public string NomeDoBeneficiario { get; set; }
        public string NomeDoBeneficiarioComTipoDeBeneficiario { get; set; }
        public string Pergunta { get; set; }
        public string PerguntaEnumerada { get; set; }
        public string Numero { get; set; }
        public string Resposta { get; set; }
        public string Ano { get; set; }
        public string Detalhes { get; set; }
    }
    public class ValorPorBeneficiarioPorIdadeV1Input
    {
        public string Beneficiario { get; set; }
        public int Idade { get; set; }
        public decimal Valor { get; set; }
    }

    public class DataAceite
    {
        public string Dia { get; set; }
        public string Mes { get; set; }
        public string Ano { get; set; }
    }

}
