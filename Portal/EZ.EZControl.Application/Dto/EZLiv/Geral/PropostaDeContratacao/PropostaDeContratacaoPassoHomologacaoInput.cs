﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoHomologacaoInput : EzInputDto
    {
        public TipoDeHomologacaoEnum TipoDeHomologacao { get; set; }
        public string ObservacaoHomologacao { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public DateTime? DataHoraDaHomologacao { get; set; }

    }
}
