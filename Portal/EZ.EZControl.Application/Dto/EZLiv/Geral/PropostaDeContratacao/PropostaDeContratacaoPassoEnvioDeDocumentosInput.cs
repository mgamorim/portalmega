﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoEnvioDeDocumentosInput : EzInputDto
    {
        public int PropostaContratacaoId { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
    }
}
