﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class GetPropostaDeContratacaoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NumeroDaProposta { get; set; }
        public string NomeDoBeneficiario { get; set; }
        public string CPFDoBeneficiario { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
