﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoListDto : EzFullAuditedInputDto
    {
        public string Numero { get; set; }
        public ProponenteTitularListDto Titular { get; set; }
        public int TitularId { get; set; }
        //public ProdutoDePlanoDeSaudeListDto Produto { get; set; }
        public int ProdutoId { get; set; }
        //public ResponsavelListDto Responsavel { get; set; }
        public int ResponsavelId { get; set; }
        public bool Aceite { get; set; }
        public TipoDeHomologacaoEnum TipoDeHomologacao { get; set; }
        public DateTime? DataHoraDaHomologacao { get; set; }
        public DateTime DataHoraDoAceite { get; set; }
        //public ContratoListDto ContratoVigenteNoAceite { get; set; }
        public int ContratoVigenteNoAceiteId { get; set; }
        //public ContratoListDto UltimoContratoAceito { get; set; }
        public int UltimoContratoAceitoId { get; set; }
        public FormaDeContratacaoEnum FormaDeContratacao { get; set; }
        //public CorretorListDto Corretor { get; set; }
        public int CorretorId { get; set; }
        //public CorretoraListDto Corretora { get; set; }
        public int? CorretoraId { get; set; }
        //public PedidoListDto Pedido { get; set; }
        public int? PedidoId { get; set; }
        //public VigenciaListDto Vigencia { get; set; }
        public int? VigenciaId { get; set; }
        public ICollection<RelatorioInput> Aditivos { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public StatusDaPropostaEnum StatusDaProposta { get; set; }
        public DateTime? InicioDeVigencia { get; set; }
        public TipoDePropostaEnum TipoDeProposta { get; set; }
        public bool TitularResponsavelLegal { get; set; }
        public int? ItemDeDeclaracaoDoBeneficiarioId { get; set; }
    }
}
