﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using System;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    [Obsolete]
    public class FormularioDeAssociacaoInput : EzInputDto
    {
        public string PathLogoAssociacao { get; set; }
        public PropostaDeContratacaoInput Proposta { get; set; }
        public ChancelaInput Chancela { get; set; }
        public string EstadoNome { get; set; }
        public string Instrucoes { get; set; }

        public string numero { get; set; }

        public DataAceite DataAceite { get; set; }
    }
}
