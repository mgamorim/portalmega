﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoAceiteDoContratoInput : EzInputDto
    {
        public int PropostaContratacaoId { get; set; }
        public int ContratoId { get; set; }
        public bool Aceite { get; set; }
        public bool EnviaCopiaAdministradora { get; set; }
        public bool EnviaCopiaCorretora { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public bool AceiteCorretor { get; set; }
        public DateTime? DataAceiteCorretor { get; set; }
    }
}
