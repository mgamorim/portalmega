﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;

namespace EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude
{
    public class RespostaDoItemDeDeclaracaoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Observacao { get; set; }
        public virtual ItemDeDeclaracaoDeSaudeListDto ItemDeDeclaracaoDeSaudeListDto { get; set; }
        public int ItemDeDeclaracaoDeSaudeId { get; set; }
        public RespostaDoItemDeDeclaracaoDeSaudeEnum RespostaDoItemDeDeclaracaoDeSaudeFixo { get; set; }
        public virtual PropostaDeContratacaoListDto PropostaDeContratacao { get; set; }
        public int PropostaDeContratacaoId { get; set; }
        public int AnoDoEvento { get; set; }

    }
}
