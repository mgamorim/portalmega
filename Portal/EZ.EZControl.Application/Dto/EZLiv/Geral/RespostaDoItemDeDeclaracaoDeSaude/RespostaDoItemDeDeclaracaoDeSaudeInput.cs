﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude
{
    public class RespostaDoItemDeDeclaracaoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Observacao { get; set; }
        public virtual ItemDeDeclaracaoDeSaudeInput ItemDeDeclaracaoDeSaudeInput { get; set; }
        public int ItemDeDeclaracaoDeSaudeId { get; set; }
        public RespostaDoItemDeDeclaracaoDeSaudeEnum RespostaDoItemDeDeclaracaoDeSaudeFixo { get; set; }
        public virtual BeneficiarioInput BeneficiarioInput { get; set; }
        public int BeneficiarioId { get; set; }
        public virtual PropostaDeContratacaoInput PropostaDeContratacao { get; set; }
        public int PropostaDeContratacaoId { get; set; }
        public int AnoDoEvento { get; set; }
    }
}
