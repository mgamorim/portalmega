﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude
{
    public class GetRespostaDoItemDeDeclaracaoDeSaudeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Pergunta { get; set; }
        public string Observacao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Pergunta";
            }
        }
    }
}
