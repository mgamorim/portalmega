﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude.Mapper
{
    public class RespostaDoItemDeDeclaracaoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<RespostaDoItemDeDeclaracaoDeSaudeInput, Domain.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<RespostaDoItemDeDeclaracaoDeSaudeListDto, Domain.EZLiv.Geral.RespostaDoItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<RespostaDoItemDeDeclaracaoDeSaudeListDto, RespostaDoItemDeDeclaracaoDeSaudeInput>();
        }
    }
}
