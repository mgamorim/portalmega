﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude
{
    public class ItemDeDeclaracaoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Pergunta { get; set; }
        public virtual BeneficiarioListDto BeneficiarioListDto { get; set; }
        public int BeneficiarioId { get; set; }
        public TipoDeItemDeDeclaracaoEnum TipoDeItemDeDeclaracao { get; set; }
        public int Ordenacao { get; set; }
    }
}
