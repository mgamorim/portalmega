﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude.Mapper
{
    public class ItemDeDeclaracaoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ItemDeDeclaracaoDeSaudeInput, Domain.EZLiv.Geral.ItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<ItemDeDeclaracaoDeSaudeListDto, Domain.EZLiv.Geral.ItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<ItemDeDeclaracaoDeSaudeListDto, ItemDeDeclaracaoDeSaudeInput>();
        }
    }
}
