﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude
{
    public class GetItemDeDeclaracaoDeSaudeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Pergunta { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Pergunta";
            }
        }
    }
}
