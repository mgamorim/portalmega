﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude
{
    public class ItemDeDeclaracaoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Pergunta { get; set; }

        public TipoDeItemDeDeclaracaoEnum TipoDeItemDeDeclaracao { get; set; }
        public int Ordenacao { get; set; }
    }

}
