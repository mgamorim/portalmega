﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.RespostaDeDeclaracaoDeSaude
{
    public class RespostaDeDeclaracaoDeSaudeInput : EzInputDto
    {
        public string Pergunta { get; set; }
        public int PerguntaId { get; set; }
        public TipoDeItemDeDeclaracaoEnum TipoDeItemDeDeclaracao { get; set; }
        public string Marcada { get; set; }
        public string Observacao { get; set; }
        public int AnoDoEvento { get; set; }
        public int PropostaDeContratacaoId { get; set; }
        public int Ordenacao { get; set; }
    }
}
