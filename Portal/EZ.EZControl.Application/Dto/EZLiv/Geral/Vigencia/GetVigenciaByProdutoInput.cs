﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.Vigencia
{
    public class GetVigenciaByProdutoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NumeroDeBoletos { get; set; }
        public int ProdutoId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NumeroDeBoletos";
            }
        }
    }
}
