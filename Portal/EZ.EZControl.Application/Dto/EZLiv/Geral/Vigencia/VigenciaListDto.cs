﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.Vigencia
{
    public class VigenciaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int DataInicialDaProposta { get; set; }
        public int DataFinalDaProposta { get; set; }
        public int DataDeFechamento { get; set; }
        public int DataDeVigencia { get; set; }
        public string NumeroDeBoletos { get; set; }
    }
}
