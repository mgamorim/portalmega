﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.Vigencia
{
    public class VigenciaInput : EzInputDto, IPassivable
    {
        public VigenciaInput()
        {
            this.IsActive = true;
        }
        public bool IsActive { get; set; }
        public int DataInicialDaProposta { get; set; }
        public int DataFinalDaProposta { get; set; }
        public int DataDeFechamento { get; set; }
        public int DataDeVigencia { get; set; }
        public string NumeroDeBoletos { get; set; }
    }
}
