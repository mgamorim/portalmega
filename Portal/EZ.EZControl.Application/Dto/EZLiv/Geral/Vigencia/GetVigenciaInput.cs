﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZLiv.Geral.Vigencia
{
    public class GetVigenciaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NumeroDeBoletos { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NumeroDeBoletos";
            }
        }
    }
}
