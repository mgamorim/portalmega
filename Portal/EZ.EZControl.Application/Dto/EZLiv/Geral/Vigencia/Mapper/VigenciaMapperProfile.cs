﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.Vigencia.Mapper
{
    public class VigenciaMapperProfile : Profile
    {
        public VigenciaMapperProfile()
        {
            CreateMap<VigenciaInput, Domain.EZLiv.Geral.Vigencia>().ReverseMap();
            CreateMap<VigenciaListDto, Domain.EZLiv.Geral.Vigencia>().ReverseMap();
            CreateMap<VigenciaListDto, VigenciaInput>();
        }
    }
}
