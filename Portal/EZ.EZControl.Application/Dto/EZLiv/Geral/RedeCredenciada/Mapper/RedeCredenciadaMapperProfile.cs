﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada.Mapper
{
    public class RedeCredenciadaMapperProfile : Profile
    {
        public RedeCredenciadaMapperProfile()
        {
            CreateMap<RedeCredenciadaInput, Domain.EZLiv.Geral.RedeCredenciada>().ReverseMap();
            CreateMap<RedeCredenciadaListDto, Domain.EZLiv.Geral.RedeCredenciada>().ReverseMap();
            CreateMap<RedeCredenciadaListDto, RedeCredenciadaInput>();
        }
    }
}
