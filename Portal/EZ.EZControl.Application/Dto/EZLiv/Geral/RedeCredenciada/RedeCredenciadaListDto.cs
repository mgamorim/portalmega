﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeRedeCredenciada;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada
{
    public class RedeCredenciadaListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public virtual IEnumerable<ItemDeRedeCredenciadaListDto> Items { get; set; }
        public virtual IEnumerable<ItemDeRedeCredenciadaListDto> HospitaisDiferenciados
        {
            get
            {
                return Items != null ? Items.Where(x => x.IsRedeDiferenciada && x.IsHospital) : null;
            }
        }
        public virtual IEnumerable<ItemDeRedeCredenciadaListDto> LaboratoriosDiferenciados
        {
            get
            {
                return Items != null ? Items.Where(x => x.IsRedeDiferenciada && x.IsLaboratorio) : null;
            }
        }
    }
}
