﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretor.Mapper
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretorMapperProfile : Profile
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretorMapperProfile()
        {
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretorInput, Domain.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretor>().ReverseMap();
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, Domain.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretor>().ReverseMap();
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto, PermissaoDeVendaDePlanoDeSaudeParaCorretorInput>();
        }

    }
}
