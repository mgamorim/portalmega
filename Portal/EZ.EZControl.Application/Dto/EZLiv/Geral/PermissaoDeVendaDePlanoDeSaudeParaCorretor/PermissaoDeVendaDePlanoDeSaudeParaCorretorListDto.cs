﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretor
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto : EzFullAuditedInputDto, IPassivable
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretorListDto()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Comissao { get; set; }
        public virtual CorretoraListDto Corretora { get; set; }
        public int CorretoraId { get; set; }
        public List<CorretorInput> Corretores { get; set; }
        public List<ProdutoDePlanoDeSaudeListDto> Produtos { get; set; }
        public virtual EmpresaListDto Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}
