﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento.Mapper
{
    public class ArquivoDocumentoMapperProfile : Profile
    {
        public ArquivoDocumentoMapperProfile()
        {
            CreateMap<ArquivoDocumentoInput, Domain.EZLiv.Geral.ArquivoDocumento>().ReverseMap();
            CreateMap<ArquivoDocumentoListDto, Domain.EZLiv.Geral.ArquivoDocumento>().ReverseMap();
            CreateMap<ArquivoDocumentoListDto, ArquivoDocumentoInput>();
        }
    }
}
