﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Core.Geral.Arquivo;

namespace EZ.EZControl.Dto.EZLiv.Geral.ArquivoDocumento
{
    public class ArquivoDocumentoInput : ArquivoInput
    {
        public TipoDeDocumentoEnum Tipo { get; set; }
        public bool EmExigencia { get; set; }
        public string Motivo { get; set; }
        public int PropostaDeContratacaoId { get; set; }
    }
}
