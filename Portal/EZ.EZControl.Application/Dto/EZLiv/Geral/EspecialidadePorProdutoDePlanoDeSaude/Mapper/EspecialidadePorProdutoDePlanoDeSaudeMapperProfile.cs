﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude.Mapper
{
    public class EspecialidadePorProdutoDePlanoDeSaudeMapperProfile : Profile
    {
        public EspecialidadePorProdutoDePlanoDeSaudeMapperProfile()
        {
            CreateMap<EspecialidadePorProdutoDePlanoDeSaudeInput, Domain.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<EspecialidadePorProdutoDePlanoDeSaudeListDto, Domain.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<EspecialidadePorProdutoDePlanoDeSaudeListDto, EspecialidadePorProdutoDePlanoDeSaudeInput>();
        }
    }
}
