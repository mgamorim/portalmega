﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude.Mapper
{
    public class PlanoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PlanoDeSaudeInput, Domain.EZLiv.Geral.PlanoDeSaude>().ReverseMap();
            CreateMap<PlanoDeSaudeListDto, Domain.EZLiv.Geral.PlanoDeSaude>().ReverseMap();
            CreateMap<PlanoDeSaudeListDto, PlanoDeSaudeInput>();
        }
    }
}
