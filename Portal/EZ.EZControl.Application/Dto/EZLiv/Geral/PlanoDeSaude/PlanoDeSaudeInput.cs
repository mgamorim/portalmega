﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude
{
    public class PlanoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string NumeroDeRegistro { get; set; }
        public bool Reembolso { get; set; }
        public string DescricaoDoReembolso { get; set; }
        public AbrangenciaDoPlanoEnum Abrangencia { get; set; }
        public SegmentacaoAssistencialDoPlanoEnum SegmentacaoAssistencial { get; set; }
        public PlanoDeSaudeAnsEnum? PlanoDeSaudeAns { get; set; }
        public AcomodacaoEnum? Acomodacao { get; set; }
    }
}
