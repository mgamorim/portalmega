﻿using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora
{
    public class GetPermissoesPorCorretoraInput
    {
        public bool TemCorretoras { get; set; }
        public virtual CorretoraListDto Corretora { get; set; }
        public decimal Valor { get; set; }
    }
}
