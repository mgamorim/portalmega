﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto : EzFullAuditedInputDto, IPassivable
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto()
        {
            IsActive = true;
        }
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual AdministradoraListDto Administradora { get; set; }
        public int AdministradoraId { get; set; }
        public List<CorretoraInput> Corretoras { get; set; }
        public List<ProdutoDePlanoDeSaudeListDto> Produtos { get; set; }
        public virtual EmpresaListDto Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}
