﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraByIdInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual AdministradoraInput Administradora { get; set; }
        public int AdministradoraId { get; set; }
        public virtual List<IdInput> CorretorasIds { get; set; }
        public virtual List<IdInput> ProdutosIds { get; set; }
        public virtual EmpresaInput Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}
