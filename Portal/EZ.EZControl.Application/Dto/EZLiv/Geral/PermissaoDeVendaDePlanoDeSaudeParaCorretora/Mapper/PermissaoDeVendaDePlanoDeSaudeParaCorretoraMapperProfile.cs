﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora.Mapper
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraMapperProfile : Profile
    {
        public PermissaoDeVendaDePlanoDeSaudeParaCorretoraMapperProfile()
        {
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput, Domain.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora>().ReverseMap();
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, Domain.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora>().ReverseMap();
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretoraListDto, PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput>();
            CreateMap<PermissaoDeVendaDePlanoDeSaudeParaCorretoraByIdInput, Domain.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora>();
        }

    }
}
