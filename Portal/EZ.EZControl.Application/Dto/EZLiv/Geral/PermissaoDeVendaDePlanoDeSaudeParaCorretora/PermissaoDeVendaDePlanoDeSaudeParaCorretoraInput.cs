﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.PermissaoDeVendaDePlanoDeSaudeParaCorretora
{
    public class PermissaoDeVendaDePlanoDeSaudeParaCorretoraInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public virtual AdministradoraInput Administradora { get; set; }
        public int AdministradoraId { get; set; }
        public virtual List<CorretoraInput> Corretoras { get; set; }
        public virtual List<ProdutoDePlanoDeSaudeListDto> Produtos { get; set; }
        public virtual EmpresaInput Empresa { get; set; }
        public int EmpresaId { get; set; }
    }
}
