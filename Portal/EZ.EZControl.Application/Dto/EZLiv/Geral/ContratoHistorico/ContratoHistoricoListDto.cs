﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using System;

namespace EZ.EZControl.Dto.EZLiv.Geral.ContratoHistorico
{
    public class ContratoHistoricoListDto : EzFullAuditedInputDto
    {
        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public ProdutoDePlanoDeSaudeListDto ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public ContratoListDto Contrato { get; set; }
        public int ContratoId { get; set; }
        public long UserId { get; set; }
        public DateTime DataEvento { get; set; }
    }
}
