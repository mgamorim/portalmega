﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ContratoHistorico.Mapper
{
    public class ContratoHistoricoMapperProfile : Profile
    {
        public ContratoHistoricoMapperProfile()
        {
            CreateMap<ContratoHistoricoInput, Domain.EZLiv.Geral.ContratoHistorico>().ReverseMap();
            CreateMap<ContratoHistoricoListDto, Domain.EZLiv.Geral.ContratoHistorico>().ReverseMap();
            CreateMap<ContratoHistoricoListDto, ContratoHistoricoInput>();
        }
    }
}
