﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZLiv.Geral.ContratoHistorico
{
    public class ContratoHistoricoInput : EzInputDto
    {
        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public int ContratoId { get; set; }
        public long UserId { get; set; }
        public DateTime DataEvento { get; set; }
    }
}
