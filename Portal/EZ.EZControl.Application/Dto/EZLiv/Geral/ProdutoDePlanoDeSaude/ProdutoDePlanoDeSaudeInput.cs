﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario;
using EZ.EZControl.Dto.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class ProdutoDePlanoDeSaudeInput : ProdutoInput
    {
        public IdInput IdInput { get; set; }
        public PlanoDeSaudeInput PlanoDeSaude { get; set; }
        public int PlanoDeSaudeId { get; set; }

        public OperadoraInput Operadora { get; set; }
        public int OperadoraId { get; set; }

        public AdministradoraInput Administradora { get; set; }
        public int? AdministradoraId { get; set; }

        public bool CarenciaEspecial { get; set; }
        public string DescricaoDaCarenciaEspecial { get; set; }

        public bool Acompanhante { get; set; }
        public string DescricaoDoAcompanhante { get; set; }

        public bool CoberturaExtra { get; set; }
        public string DescricaoDaCoberturaExtra { get; set; }

        public int[] EspecialidadesIds { get; set; }

        public virtual IEnumerable<EspecialidadePorProdutoDePlanoDeSaudeListDto> Especialidades { get; set; }

        public ContratoInput Contrato { get; set; }

        [Url]
        public string LinkRedeCredenciada { get; set; }

        public RedeCredenciadaInput RedeCredenciada { get; set; }
        public int? RedeCredenciadaId { get; set; }

        public RelatorioInput RelatorioFichaDeEntidade { get; set; }
        public int? RelatorioFichaDeEntidadeId { get; set; }

        public RelatorioInput RelatorioProspostaDeContratacao { get; set; }
        public int? RelatorioProspostaDeContratacaoId { get; set; }

        public virtual ICollection<CidadeInput> Cidades { get; set; }

        public virtual ICollection<EstadoInput> Estados { get; set; }
        public FormaDeContratacaoEnum FormaDeContratacao { get; set; }

        public virtual ICollection<RelatorioInput> Aditivos { get; set; }

        public QuestionarioDeDeclaracaoDeSaudeInput QuestionarioDeDeclaracaoDeSaude { get; set; }
        public int QuestionarioDeDeclaracaoDeSaudeId { get; set; }
        public DeclaracaoDoBeneficiarioInput DeclaracaoDoBeneficiario { get; set; }
        public int? DeclaracaoDoBeneficiarioId { get; set; }
        public virtual ICollection<VigenciaInput> Vigencias { get; set; }
        public virtual ICollection<ChancelaInput> Chancelas { get; set; }
        public int? NumeroDeDiasParaEncerrarAsPropostasRelacionadas { get; set; }
        public string CartaDeOrientacao { get; set; }
    }
}
