﻿using Abp.Runtime.Validation;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class GetEstadoExceptForProduto : PagedAndSortedInputDto, IShouldNormalize
    {
        public int ProdutoDePlanoDeSaudeId { get; set; }

        public string Nome { get; set; }

        public virtual ICollection<EstadoInput> Estados { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
