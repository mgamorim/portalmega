﻿using EZ.EZControl.Dto.EZ;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class GetValoresByProdutoInput : EzInputDto
    {
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public int ProponenteId { get; set; }
        public int ContratoId { get; set; }
        public int profissaoId { get; set; }
        public int estadoId { get; set; }
    }

    public class GetValoresAndProdutoByCorretorInput : EzInputDto
    {
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public int ProponenteId { get; set; }
        public int ContratoId { get; set; }
        public int CorretorId { get; set; }
        public int profissaoId { get; set; }
        public int estadoId { get; set; }
    }

    public class ValoresByProdutoListDto
    {
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public TitularDetailInput Titular { get; set; }
        public ICollection<DependenteDetailInput> Dependentes { get; set; }
    }

    public class TitularDetailInput
    {
        public string Nome { get; set; }
        public decimal Valor { get; set; }
    }

    public class DependenteDetailInput
    {
        public string Nome { get; set; }
        public decimal Valor { get; set; }
    }
}
