﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude.Mapper
{
    public class ProdutoDePlanoDeSaudeMapperProfile : Profile
    {
        public ProdutoDePlanoDeSaudeMapperProfile()
        {
            CreateMap<ProdutoDePlanoDeSaudeInput, Domain.EZLiv.Geral.ProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<ProdutoDePlanoDeSaudeListDto, Domain.EZLiv.Geral.ProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<ProdutoDePlanoDeSaudeListDto, ProdutoDePlanoDeSaudeListDto>();
        }
    }
}
