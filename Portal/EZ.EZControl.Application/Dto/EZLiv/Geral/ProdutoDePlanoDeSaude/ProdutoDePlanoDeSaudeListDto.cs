﻿using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.EZLiv.Geral.Contrato;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDoBeneficiario;
using EZ.EZControl.Dto.EZLiv.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.PlanoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.QuestionarioDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Geral.Relatorio;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class ProdutoDePlanoDeSaudeListDto : ProdutoListDto
    {
        public virtual PlanoDeSaudeListDto PlanoDeSaude { get; set; }
        public int PlanoDeSaudeId { get; set; }
        public string AdministradoraNomePessoa { get; set; }
        public virtual OperadoraListDto Operadora { get; set; }
        public int? OperadoraId { get; set; }
        public string OperadoraNome { get; set; }
        public bool CarenciaEspecial { get; set; }
        public string DescricaoDaCarenciaEspecial { get; set; }
        public bool Acompanhante { get; set; }
        public string DescricaoDoAcompanhante { get; set; }
        public bool CoberturaExtra { get; set; }
        public string DescricaoDaCoberturaExtra { get; set; }
        public ContratoListDto Contrato { get; set; }
        public IEnumerable<EspecialidadePorProdutoDePlanoDeSaudeListDto> Especialidades { get; set; }
        public string LinkRedeCredenciada { get; set; }
        public RedeCredenciadaListDto RedeCredenciada { get; set; }
        public int? RedeCredenciadaId { get; set; }
        public int? RelatorioFichaDeEntidadeId { get; set; }
        public int? RelatorioProspostaDeContratacaoId { get; set; }
        public QuestionarioDeDeclaracaoDeSaudeListDto QuestionarioDeDeclaracaoDeSaude { get; set; }
        public int QuestionarioDeDeclaracaoDeSaudeId { get; set; }
        public ICollection<RelatorioInput> Aditivos { get; set; }
        public virtual ICollection<CidadeListDto> Cidades { get; set; }
        public virtual ICollection<EstadoListDto> Estados { get; set; }
        public virtual FormaDeContratacaoEnum FormaDeContratacao { get; set; }
        public virtual ICollection<VigenciaListDto> Vigencias { get; set; }
        public int? NumeroDeDiasParaEncerrarAsPropostasRelacionadas { get; set; }
        public DeclaracaoDoBeneficiarioListDto DeclaracaoDoBeneficiario { get; set; }
        public int? DeclaracaoDoBeneficiarioId { get; set; }
        public string RedeMedica
        {
            get; set;
        }
        public string Laboratorios
        {
            get; set;
        }
    }
}
