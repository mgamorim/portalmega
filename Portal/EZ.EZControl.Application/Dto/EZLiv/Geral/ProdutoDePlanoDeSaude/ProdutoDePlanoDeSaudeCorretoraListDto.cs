﻿using EZ.EZControl.Dto.Estoque.Geral.Produto;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class ProdutoDePlanoDeSaudeCorretoraListDto : ProdutoListDto
    {
        public string CorretoraNome { get; set; }
    }
}
