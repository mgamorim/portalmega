﻿using EZ.EZControl.Dto.EZ;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class DetalhesProdutoDePlanoDeSaudeInput : EzInputDto
    {
        public string Nome { get; set; }
        public string Administradora { get; set; }
        public string Operadora { get; set; }
        public string OperadoraLogo { get; set; }
        public string Acomodacao { get; set; }
        public string Abrangencia { get; set; }
        public bool Reembolso { get; set; }
        public decimal Valor { get; set; }
        public StatusDaPropostaInput StatusProposta { get; set; }
        public string Descricao { get; set; }
        public string RedeCredenciada { get; set; }
    }

    public class StatusDaPropostaInput
    {
        public int Id { get; set; }
        public int PassoDaProposta { get; set; }
        public int StatusDaProposta { get; set; }
        public List<DocumentoDaPropostaInput> Documentos { get; set; }
    }
    public class DocumentoDaPropostaInput
    {
        public string Nome { get; set; }
        public string Url { get; set; }
    }
}
