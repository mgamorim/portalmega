﻿namespace EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude
{
    public class GetDetalhesProdutoInput
    {
        public int ProdutoId { get; set; }
        public int PropostaId { get; set; }
    }
}
