﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio;
using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta
{
    public class ConfiguracaoDeBloqueioConsultaListDto : ConfiguracaoDeBloqueioListDto
    {
        public int MedicoId { get; set; }
        public int EspecialidadeId { get; set; }
        public string MedicoNomePessoa { get; set; }
        public string EspecialidadeNome { get; set; }
    }
}
