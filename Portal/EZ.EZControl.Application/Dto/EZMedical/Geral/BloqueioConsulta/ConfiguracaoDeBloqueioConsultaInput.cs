﻿using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta
{
    public class ConfiguracaoDeBloqueioConsultaInput : ConfiguracaoDeBloqueioInput
    {
        public int MedicoId { get; set; }
        public int EspecialidadeId { get; set; }
    }
}