﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta.Mapper
{
    public class ConfiguracaoDeBloqueioConsultaMapperProfile : Profile
    {
        public ConfiguracaoDeBloqueioConsultaMapperProfile()
        {
            CreateMap<ConfiguracaoDeBloqueioConsultaInput, Domain.Agenda.Geral.ConfiguracaoDeBloqueio>()
                .ForMember(destination => destination.Bloqueios, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<ConfiguracaoDeBloqueioConsultaListDto, Domain.Agenda.Geral.ConfiguracaoDeBloqueio>()
                .ReverseMap();
            CreateMap<ConfiguracaoDeBloqueioConsultaListDto, ConfiguracaoDeBloqueioConsultaInput>();
        }
    }
}
