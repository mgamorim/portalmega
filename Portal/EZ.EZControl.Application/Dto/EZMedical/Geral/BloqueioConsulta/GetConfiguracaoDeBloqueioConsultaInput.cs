﻿using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta
{
    public class GetConfiguracaoDeBloqueioConsultaInput : GetConfiguracaoDeBloqueioInput
    {
        public string Medico { get; set; }
        public string Especialidade { get; set; }
    }
}
