﻿using AutoMapper;
using EZ.EZControl.Domain.Agenda.Geral;

namespace EZ.EZControl.Dto.EZMedical.Geral.Regra.Mapper
{
    public class RegraConsultaMapperProfile : Profile
    {
        public RegraConsultaMapperProfile()
        {
            CreateMap<RegraConsultaInput, RegraConsulta>().ReverseMap();
            CreateMap<RegraConsultaListDto, RegraConsulta>().ReverseMap();
            CreateMap<RegraConsultaListDto, RegraConsultaInput>();
        }
    }
}
