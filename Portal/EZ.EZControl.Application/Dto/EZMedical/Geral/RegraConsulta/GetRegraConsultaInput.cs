﻿using EZ.EZControl.Dto.Agenda.Regra;

namespace EZ.EZControl.Dto.EZMedical.Geral.Regra
{
    public class GetRegraConsultaInput : GetRegraBaseInput
    {
        public string Medico { get; set; }
        public string Especialidade { get; set; }
    }
}
