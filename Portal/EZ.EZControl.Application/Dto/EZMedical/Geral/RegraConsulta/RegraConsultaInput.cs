﻿using EZ.EZControl.Dto.Agenda.Regra;

namespace EZ.EZControl.Dto.EZMedical.Geral.Regra
{
    public class RegraConsultaInput : RegraBaseInput
    {
        public int MedicoId { get; set;}
        public int EspecialidadeId { get; set; }
    }
}
