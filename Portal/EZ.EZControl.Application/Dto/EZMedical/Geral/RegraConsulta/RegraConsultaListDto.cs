﻿using EZ.EZControl.Dto.Agenda.Regra;

namespace EZ.EZControl.Dto.EZMedical.Geral.Regra
{
    public class RegraConsultaListDto : RegraBaseListDto
    {
        public int MedicoId { get; set;}
        public int EspecialidadeId { get; set; }
        public string MedicoNomePessoa { get; set; }
        public string EspecialidadeNome { get; set; }
    }
}
