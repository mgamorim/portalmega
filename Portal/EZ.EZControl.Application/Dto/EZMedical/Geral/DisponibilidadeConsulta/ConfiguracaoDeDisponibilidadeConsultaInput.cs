﻿using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta
{
    public class ConfiguracaoDeDisponibilidadeConsultaInput : ConfiguracaoDeDisponibilidadeInput
    {
        public int MedicoId { get; set; }
        public int EspecialidadeId { get; set; }
    }
}