﻿using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta
{
    public class GetConfiguracaoDeDisponibilidadeConsultaInput : GetConfiguracaoDeDisponibilidadeInput
    {
        public string Medico { get; set; }
        public string Especialidade { get; set; }
    }
}
