﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta.Mapper
{
    public class ConfiguracaoDeDisponibilidadeConsultaMapperProfile : Profile
    {
        public ConfiguracaoDeDisponibilidadeConsultaMapperProfile()
        {
            CreateMap<ConfiguracaoDeDisponibilidadeConsultaInput, Domain.Agenda.Geral.ConfiguracaoDeDisponibilidade>()
               .ForMember(destination => destination.Disponibilidades, opt => opt.Ignore())
               .ReverseMap();
            CreateMap<ConfiguracaoDeDisponibilidadeConsultaListDto, Domain.Agenda.Geral.ConfiguracaoDeDisponibilidade>().ReverseMap();
            CreateMap<ConfiguracaoDeDisponibilidadeConsultaListDto, ConfiguracaoDeDisponibilidadeConsultaInput>();
        }
    }
}
