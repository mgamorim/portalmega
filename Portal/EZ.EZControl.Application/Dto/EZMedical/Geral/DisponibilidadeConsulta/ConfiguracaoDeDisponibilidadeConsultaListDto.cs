﻿using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeDisponibilidade;

namespace EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta
{
    public class ConfiguracaoDeDisponibilidadeConsultaListDto : ConfiguracaoDeDisponibilidadeListDto
    {
        public string MedicoNomePessoa { get; set; }
        public int MedicoId { get; set; }
        public string EspecialidadeNome { get; set; }
        public int EspecialidadeId { get; set; }
    }
}
