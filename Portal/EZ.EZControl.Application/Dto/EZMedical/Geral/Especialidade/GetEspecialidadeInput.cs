﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZMedical.Geral.Especialidade
{
    public class GetEspecialidadeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public int[] Ids { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}