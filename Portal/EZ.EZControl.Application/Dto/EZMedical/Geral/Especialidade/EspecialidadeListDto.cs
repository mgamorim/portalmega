﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZMedical.Geral.Especialidade
{
    public class EspecialidadeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
    }
}