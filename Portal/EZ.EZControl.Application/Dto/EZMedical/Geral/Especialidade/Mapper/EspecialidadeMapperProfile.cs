﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZMedical.Geral.Especialidade.Mapper
{
    public class EspecialidadeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<EspecialidadeInput, Domain.EZMedical.Geral.Especialidade>().ReverseMap();
            CreateMap<EspecialidadeListDto, Domain.EZMedical.Geral.Especialidade>().ReverseMap();
            CreateMap<EspecialidadeListDto, EspecialidadeInput>();
        }
    }
}