﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZMedical.Geral.Evento.Mapper
{
    public class EventoConsultaMapperProfile : Profile
    {
        public EventoConsultaMapperProfile()
        {
            CreateMap<EventoConsultaInput, Domain.EZMedical.Geral.EventoConsulta>()
                .ForMember(destination => destination.Historico, opt => opt.Ignore())
                .ForMember(destination => destination.Participantes, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<EventoConsultaListDto, Domain.EZMedical.Geral.EventoConsulta>().ReverseMap();
            CreateMap<EventoConsultaListDto, EventoConsultaInput>();
        }
    }
}