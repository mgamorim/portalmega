﻿using Abp.Runtime.Validation;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.EZMedical.Geral.Evento
{
    public class GetEventoConsultaExceptForInput : PagedAndSortedInputDto, IShouldNormalize
    {
        [Required]
        public int Id { get; set; }
        public string Titulo { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Titulo";
            }
        }
    }
}
