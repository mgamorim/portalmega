﻿using EZ.EZControl.Dto.Agenda.Geral.Evento;
using System;

namespace EZ.EZControl.Dto.EZMedical.Geral.Evento
{
    public class GetEventoConsultaInput : GetEventoInput
    {
        public DateTime Inicio { get; set; }
        public DateTime Termino { get; set; }
    }
}
