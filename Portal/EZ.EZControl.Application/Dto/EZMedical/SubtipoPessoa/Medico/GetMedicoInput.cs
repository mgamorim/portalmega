﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico
{
    public class GetMedicoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string NumeroDocumento { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
