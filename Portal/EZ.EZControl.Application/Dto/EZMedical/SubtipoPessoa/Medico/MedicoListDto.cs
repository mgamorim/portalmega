﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico
{
    public class MedicoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int PictureId { get; set; }

        public PessoaFisicaListDto PessoaFisica { get; set; }

        public EspecialidadeListDto[] Especialidades { get; set; }
    }
}
