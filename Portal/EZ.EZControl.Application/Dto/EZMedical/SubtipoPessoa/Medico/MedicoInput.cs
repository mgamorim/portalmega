﻿using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico
{
    public class MedicoInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public int PictureId { get; set; }

        public PessoaInput Pessoa { get; set; }

        public PessoaFisicaInput PessoaFisica { get; set; }

        public int[] IdsEspecialidades { get; set; }

        public EspecialidadeListDto[] Especialidades { get; set; }
    }
}
