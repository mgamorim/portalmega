﻿namespace EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico
{
    public class MedicoPessoaIdDto
    {
        public int MedicoId { get; set; }
        public int PessoaId { get; set; }
        public int PictureId { get; set; }
    }
}