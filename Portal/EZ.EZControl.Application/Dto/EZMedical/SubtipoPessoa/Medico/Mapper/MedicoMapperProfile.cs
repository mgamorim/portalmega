﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico.Mapper
{
    public class MedicoMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Domain.EZMedical.SubtiposPessoa.Medico, MedicoNomeListDto>()
                .ForMember(destination => destination.Nome, opt => opt.MapFrom(source => source.PessoaFisica.Nome))
                .ReverseMap();

            CreateMap<MedicoInput, Domain.EZMedical.SubtiposPessoa.Medico>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<MedicoListDto, Domain.EZMedical.SubtiposPessoa.Medico>().ReverseMap();
            CreateMap<MedicoListDto, MedicoInput>();
        }
    }
}
