﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico
{
    public class MedicoNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}