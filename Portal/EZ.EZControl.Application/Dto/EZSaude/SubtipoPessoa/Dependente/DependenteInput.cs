﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente
{
    public class DependenteInput : BeneficiarioInput
    {
        public virtual GrauDeParentescoEnum GrauDeParentesco { get; set; }
    }
}
