﻿using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente
{
    public class DependenteListDto : BeneficiarioListDto
    {
        public string GrauDeParentesco { get; set; }
    }
}
