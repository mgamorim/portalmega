﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente.Mapper
{
    public class DependenteMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DependenteInput, Domain.EZSaude.SubtiposPessoa.Dependente>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<DependenteListDto, Domain.EZSaude.SubtiposPessoa.Dependente>().ReverseMap();
            CreateMap<DependenteListDto, DependenteInput>();
        }
    }
}
