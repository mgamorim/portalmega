﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente
{
    public class GetDependenteInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
