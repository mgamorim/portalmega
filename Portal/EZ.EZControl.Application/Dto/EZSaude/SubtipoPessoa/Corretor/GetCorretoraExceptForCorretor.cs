﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor
{
    public class GetCorretoraExceptForCorretor : PagedAndSortedInputDto, IShouldNormalize
    {
        public int CorretorId { get; set; }

        public string NomePessoa { get; set; }

        public TipoPessoaEnum? TipoPessoa { get; set; }

        public virtual ICollection<CorretoraInput> Corretoras { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
