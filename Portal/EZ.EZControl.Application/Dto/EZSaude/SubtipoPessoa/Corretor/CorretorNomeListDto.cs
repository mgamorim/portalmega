﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor
{
    public class CorretorNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}