﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor.Mapper
{
    public class CorretorMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CorretorInput, Domain.EZSaude.SubtiposPessoa.Corretor>()
               .ForMember(destination => destination.PessoaFisica, opt => opt.Ignore())
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<CorretorListDto, Domain.EZSaude.SubtiposPessoa.Corretor>().ReverseMap();
            CreateMap<CorretorListDto, CorretorInput>();
        }
    }
}
