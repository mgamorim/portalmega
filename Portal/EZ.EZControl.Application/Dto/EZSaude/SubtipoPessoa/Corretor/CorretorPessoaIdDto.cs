﻿namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor
{
    public class CorretorPessoaIdDto
    {
        public int CorretorId { get; set; }
        public int PessoaId { get; set; }
    }
}