﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor
{
    public class GetCorretorInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
