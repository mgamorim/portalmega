﻿using AutoMapper;
using EZ.EZControl.Domain.EZSaude.SubtiposPessoa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario.Mapper
{
    public class BeneficiarioMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<BeneficiarioBase, BeneficiarioNomeListDto>()
                .ForMember(destination => destination.Nome, opt => opt.MapFrom(source => source.PessoaFisica.Nome))
                .ReverseMap();

            CreateMap<BeneficiarioInput, BeneficiarioBase>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<BeneficiarioListDto, BeneficiarioBase>().ReverseMap();
            CreateMap<BeneficiarioListDto, BeneficiarioInput>();
        }
    }
}
