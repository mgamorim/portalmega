﻿using EZ.EZControl.Dto.EZ;
using System;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioUserPessoaInput : EzInputDto
    {
        public BeneficiarioUserPessoaInput()
        {
            this.SendEmail = true;
        }

        public bool SendEmail { get; set; }

        public string NomeCompleto { get; set; }

        public int TitularId { get; set; }

        public string Email { get; set; }

        public int EmailId { get; set; }

        public string Cpf { get; set; }

        public int CpfId { get; set; }

        public DateTime DataDeNascimento { get; set; }

        public string Celular { get; set; }

        public int CelulaId { get; set; }

        public string EstadoUf { get; set; }
        public string Profissao { get; set; }

        public int ProfissaoId { get; set; }

        public string GrupoPessoaSlug { get; set; }

        public string GrupoPessoa { get; set; }

        public string Corretora { get; set; }

        public string CorretoraSlug { get; set; }

        public int PessoaId { get; set; }
    }
}
