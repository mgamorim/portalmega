﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}