﻿using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioUserInput : EzInputDto
    {
        public BeneficiarioUserInput()
        {
            this.SendEmail = true;
        }

        public string StaticRoleName { get; set; }
        public TipoDeUsuarioEnum TipoDeUsuario { get; set; }
        public bool SendEmail { get; set; }
    }
}
