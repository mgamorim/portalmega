﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public PessoaFisicaListDto PessoaFisica { get; set; }

        public EstadoListDto Estado { get; set; }

        public CorretoraListDto Corretora { get; set; }

        public ProfissaoListDto Profissao { get; set; }

        public virtual TipoDeBeneficiarioEnum TipoDeBeneficiario { get; set; }

        public string NomeDaMae { get; set; }

        public string NumeroDoCartaoNacionalDeSaude { get; set; }

        public string DeclaracaoDeNascidoVivo { get; set; }
    }
}
