﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.RespostaDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.Global.Pessoa;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioInput : EzInputDto
    {
        public bool IsActive { get; set; }

        public PessoaInput Pessoa { get; set; }

        public PessoaFisicaInput PessoaFisica { get; set; }

        public int EstadoId { get; set; }

        public int CorretoraId { get; set; }

        public int ProfissaoId { get; set; }

        public virtual TipoDeBeneficiarioEnum TipoDeBeneficiario { get; set; }

        public string NomeDaMae { get; set; }

        public string NumeroDoCartaoNacionalDeSaude { get; set; }

        public string DeclaracaoDeNascidoVivo { get; set; }
        public List<RespostaDeDeclaracaoDeSaudeInput> RespostasDeclaracaoDeSaude { get; set; }
    }
}
