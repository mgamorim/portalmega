﻿namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class BeneficiarioPessoaIdDto
    {
        public int BeneficiariolId { get; set; }
        public int PessoaId { get; set; }
    }
}