﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario
{
    public class GetBeneficiarioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
