﻿using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Operadora
{
    public class OperadoraListDto : PessoaJuridicaListDto
    {
        public bool IsActive { get; set; }

        public string Nome { get; set; }

        public string CodigoAns { get; set; }

        public int ImagemId { get; set; }
    }
}
