﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Operadora
{
    public class OperadoraNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}