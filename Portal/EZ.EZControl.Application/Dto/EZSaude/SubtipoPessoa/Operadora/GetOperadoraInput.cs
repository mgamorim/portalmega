﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Operadora
{
    public class GetOperadoraInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string CodigoAns { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
