﻿namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Operadora
{
    public class OperadoraPessoaIdDto
    {
        public int OperadoraId { get; set; }
        public int PessoaId { get; set; }
    }
}