﻿using EZ.EZControl.Common.Helpers;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Responsavel;
using System;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular
{
    public class ProponenteTitularListDto : BeneficiarioListDto
    {
        public virtual ResponsavelListDto Responsavel { get; set; }

        public virtual DependenteListDto[] Dependentes { get; set; }

        public virtual StatusTipoDeBeneficiarioTitularEnum StatusBeneficiario { get; set; }

        public string NomeResponsavelGrid
        {
            get
            {
                if (Idade < 18)
                {
                    if (Responsavel == null || Responsavel.PessoaFisica == null)
                        return string.Empty;

                    return Responsavel.PessoaFisica.Nome;
                }

                return string.Empty;
            }
        }

        public int Idade
        {
            get
            {
                if (this.PessoaFisica != null && this.PessoaFisica.DataDeNascimento.HasValue)
                {
                    return PessoaHelpers.GetIdade(PessoaFisica.DataDeNascimento.Value);
                }

                return 0;
            }
        }
    }
}
