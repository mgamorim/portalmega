﻿using EZ.EZControl.Common.Helpers;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Responsavel;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular
{
    public class ProponenteTitularInput : BeneficiarioInput
    {
        public int ResponsavelId { get; set; }

        public int[] DependenteIds { get; set; }

        public List<DependenteInput> Dependentes { get; set; }

        public ResponsavelInput Responsavel { get; set; }

        public string Celular { get; set; }

        public int CelularId { get; set; }

        public virtual StatusTipoDeBeneficiarioTitularEnum StatusBeneficiario { get; set; }

        public string NomeResponsavelGrid {
            get
            {
                if(Idade < 18)
                {
                    if (Responsavel == null || Responsavel.PessoaFisica == null)
                        return string.Empty;

                    return Responsavel.PessoaFisica.Nome;
                }

                return string.Empty;
            }
        }

        public int Idade {
            get {
                if(this.PessoaFisica != null && this.PessoaFisica.DataDeNascimento.HasValue)
                {
                    return PessoaHelpers.GetIdade(PessoaFisica.DataDeNascimento.Value);
                }

                return 0;
            }
        }
    }
}