﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular.Mapper
{
    public class ProponenteProponenteTitularMapperProfile : Profile
    {
        public ProponenteProponenteTitularMapperProfile()
        {
            CreateMap<ProponenteTitularInput, Domain.EZSaude.SubtiposPessoa.ProponenteTitular>()
                .ForMember(destination => destination.Pessoa, opt => opt.Ignore())
                .ForMember(destination => destination.NomePessoa, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<ProponenteTitularListDto, Domain.EZSaude.SubtiposPessoa.ProponenteTitular>().ReverseMap();
            CreateMap<ProponenteTitularListDto, ProponenteTitularInput>();
        }
    }
}
