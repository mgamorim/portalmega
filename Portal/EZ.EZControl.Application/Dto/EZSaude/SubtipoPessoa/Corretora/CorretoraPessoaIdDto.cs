﻿namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora
{
    public class CorretoraPessoaIdDto
    {
        public int CorretorId { get; set; }
        public int PessoaId { get; set; }
    }
}