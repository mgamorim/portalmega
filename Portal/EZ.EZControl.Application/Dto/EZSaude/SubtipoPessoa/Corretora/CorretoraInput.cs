﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora
{
    public class CorretoraInput : EzInputDto, IPassivable
    {
        public CorretoraInput()
        {
            IsActive = true;
        }
        public string Nome { get; set; }
        public bool IsActive { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int PessoaJuridicaId { get; set; }
        public ParametroPagSeguroInput ParametroPagSeguro { get; set; }
        public int ParametroPagSeguroId { get; set; }
        public CorretorInput Corretor { get; set; }
        public int CorretorId { get; set; }
        public string Slug { get; set; }
    }
}
