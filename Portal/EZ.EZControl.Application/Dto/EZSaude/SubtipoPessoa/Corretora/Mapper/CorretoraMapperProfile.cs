﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora.Mapper
{
    public class CorretoraMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<CorretoraInput, Domain.EZSaude.SubtiposPessoa.Corretora>()
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<CorretoraListDto, Domain.EZSaude.SubtiposPessoa.Corretora>().ReverseMap();
            CreateMap<CorretoraListDto, CorretoraInput>();
        }
    }
}
