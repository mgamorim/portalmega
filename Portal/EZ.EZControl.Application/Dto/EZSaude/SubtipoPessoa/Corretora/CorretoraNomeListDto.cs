﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora
{
    public class CorretoraNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}