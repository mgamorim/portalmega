﻿using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora
{
    public class CorretoraListDto : PessoaJuridicaListDto
    {
        public string Nome { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public CorretorListDto Corretor { get; set; }
        public int CorretorId { get; set; }
        public string Slug { get; set; }
    }
}
