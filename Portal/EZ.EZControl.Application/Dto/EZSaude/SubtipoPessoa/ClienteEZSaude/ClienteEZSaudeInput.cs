﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ClienteEZSaude
{
    public class ClienteEZSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public int ClienteId { get; set; }

        public string Observacao { get; set; }

        public ClienteInput Cliente { get; set; }
    }
}
