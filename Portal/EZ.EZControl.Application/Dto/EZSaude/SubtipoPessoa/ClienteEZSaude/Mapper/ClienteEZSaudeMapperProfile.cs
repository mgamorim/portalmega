﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ClienteEZSaude.Mapper
{
    public class ClienteEZSaudeMapperProfile : Profile
    {
        public ClienteEZSaudeMapperProfile()
        {
            CreateMap<ClienteEZSaudeInput, Domain.EZSaude.SubtiposPessoa.ClienteEZSaude>().ReverseMap();
            CreateMap<ClienteEZSaudeListDto, Domain.EZSaude.SubtiposPessoa.ClienteEZSaude>().ReverseMap();
            CreateMap<ClienteEZSaudeListDto, ClienteEZSaudeInput>();
        }
    }
}
