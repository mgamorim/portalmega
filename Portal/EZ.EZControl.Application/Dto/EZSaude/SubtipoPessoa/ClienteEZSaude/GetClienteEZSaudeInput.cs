﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Runtime.Validation;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ClienteEZSaude
{
    public class GetClienteEZSaudeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NomeCliente { get; set; }
        public int ClienteId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
