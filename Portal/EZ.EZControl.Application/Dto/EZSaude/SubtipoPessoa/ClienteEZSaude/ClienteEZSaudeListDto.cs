﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ClienteEZSaude
{
    public class ClienteEZSaudeListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public ClienteListDto Cliente { get; set; }
        public int ClienteId { get; set; }
    }
}
