﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora
{
    public class AdministradoraInput : EzInputDto, IPassivable
    {
        public AdministradoraInput()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int PessoaJuridicaId { get; set; }
        public ParametroPagSeguroInput ParametroPagSeguro { get; set; }
        public int ParametroPagSeguroId { get; set; }
        public int ImagemId { get; set; }

    }
}
