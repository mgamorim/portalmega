﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora.Mapper
{
    public class AdministradoraMapperProfile : Profile
    {
        public AdministradoraMapperProfile()
        {
            CreateMap<AdministradoraInput, Domain.EZSaude.SubtiposPessoa.Administradora>()
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<AdministradoraListDto, Domain.EZSaude.SubtiposPessoa.Administradora>().ReverseMap();
            CreateMap<AdministradoraListDto, AdministradoraInput>();
        }
    }
}
