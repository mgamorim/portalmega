﻿using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora
{
    public class AdministradoraListDto : PessoaJuridicaListDto
    {
        public PessoaJuridicaInput PessoaJuridica { get; set; }
        public int ImagemId { get; set; }
    }
}
