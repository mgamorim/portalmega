﻿namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora
{
    public class AdministradoraPessoaIdDto
    {
        public int AdministradoraId { get; set; }
        public int PessoaId { get; set; }
    }
}