﻿using EZ.EZControl.Dto.Global.Pessoa;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Associacao
{
    public class AssociacaoListDto : PessoaJuridicaListDto
    {
        public PessoaJuridicaInput PessoaJuridica { get; set; }
    }
}
