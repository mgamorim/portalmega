﻿namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Associacao
{
    public class AssociacaoPessoaIdDto
    {
        public int AssociacaoId { get; set; }
        public int PessoaId { get; set; }
    }
}