﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Associacao.Mapper
{
    public class AssociacaoMapperProfile : Profile
    {
        public AssociacaoMapperProfile()
        {
            CreateMap<AssociacaoInput, Domain.EZSaude.SubtiposPessoa.Associacao>()
               .ForMember(destination => destination.PessoaJuridica, opt => opt.Ignore())
               .ReverseMap();

            CreateMap<AssociacaoListDto, Domain.EZSaude.SubtiposPessoa.Associacao>().ReverseMap();
            CreateMap<AssociacaoListDto, AssociacaoInput>();
        }
    }
}
