﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Associacao
{
    public class AssociacaoNomeListDto : EzInputDto
    {
        public string Nome { get; set; }
    }
}