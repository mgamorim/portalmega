﻿using EZ.EZControl.Dto.EZSaude.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZSaude.Geral.Hospital
{
    public class HospitalInput : UnidadeDeSaudeBaseInput
    {
        public HospitalInput()
        {
            IsActive = true;
            TipoDeUnidade = Domain.EZSaude.Enums.TipoDeUnidadeEnum.Hospital;       
        }
    }
}
