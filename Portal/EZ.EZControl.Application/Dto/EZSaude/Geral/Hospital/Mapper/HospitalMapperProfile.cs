﻿using AutoMapper;
using EZ.EZControl.Dto.EZSaude.Geral.Hospital;

namespace EZ.EZControl.Dto.EZSaude.Geral.Hospital.Mapper
{
    public class HospitalMapperProfile : Profile
    {
        public HospitalMapperProfile()
        {
            CreateMap<HospitalInput, Domain.EZSaude.Geral.Hospital>().ReverseMap();
            CreateMap<HospitalListDto, Domain.EZSaude.Geral.Hospital>().ReverseMap();
            CreateMap<HospitalListDto, HospitalInput>();
        }
    }
}
