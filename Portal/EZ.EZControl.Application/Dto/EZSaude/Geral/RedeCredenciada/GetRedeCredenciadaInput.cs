﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.RedeCredenciada
{
    public class GetRedeCredenciadaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
