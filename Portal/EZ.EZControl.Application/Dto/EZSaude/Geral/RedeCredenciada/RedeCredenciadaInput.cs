﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.Hospital;
using EZ.EZControl.Dto.EZSaude.Geral.ItemDeRedeCredenciada;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Dto.EZSaude.Geral.RedeCredenciada
{
    public class RedeCredenciadaInput : EzInputDto, IPassivable
    {
        public RedeCredenciadaInput()
        {
            IsActive = true;            
            Items = new List<ItemDeRedeCredenciadaListDto>();
        }

        public bool IsActive { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }        
        public virtual IEnumerable<ItemDeRedeCredenciadaListDto> Items { get; set; }        
        public virtual IEnumerable<ItemDeRedeCredenciadaListDto> HospitaisDiferenciados
        {
            get
            {
                return Items != null ? Items.Where(x => x.IsRedeDiferenciada && x.IsHospital) : null;
            }
        }
        public virtual IEnumerable<ItemDeRedeCredenciadaListDto> LaboratoriosDiferenciados
        {
            get
            {
                return Items != null ? Items.Where(x => x.IsRedeDiferenciada && x.IsLaboratorio) : null;
            }
        }
    }
}
