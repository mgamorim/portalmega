﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.RedeCredenciada.Mapper
{
    public class RedeCredenciadaMapperProfile : Profile
    {
        public RedeCredenciadaMapperProfile()
        {
            CreateMap<RedeCredenciadaInput, Domain.EZSaude.Geral.RedeCredenciada>().ReverseMap();
            CreateMap<RedeCredenciadaListDto, Domain.EZSaude.Geral.RedeCredenciada>().ReverseMap();
            CreateMap<RedeCredenciadaListDto, RedeCredenciadaInput>();
        }
    }
}
