﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.RespostaDoItemDeDeclaracaoDeSaude.Mapper
{
    public class RespostaDoItemDeDeclaracaoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<RespostaDoItemDeDeclaracaoDeSaudeInput, Domain.EZSaude.Geral.RespostaDoItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<RespostaDoItemDeDeclaracaoDeSaudeListDto, Domain.EZSaude.Geral.RespostaDoItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<RespostaDoItemDeDeclaracaoDeSaudeListDto, RespostaDoItemDeDeclaracaoDeSaudeInput>();
        }
    }
}
