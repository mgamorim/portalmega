﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZSaude.Geral.RespostaDoItemDeDeclaracaoDeSaude
{
    public class RespostaDoItemDeDeclaracaoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Observacao { get; set; }
        public virtual ItemDeDeclaracaoDeSaudeInput ItemDeDeclaracaoDeSaudeInput { get; set; }
        public int ItemDeDeclaracaoDeSaudeId { get; set; }
        public RespostaDoItemDeDeclaracaoDeSaudeEnum RespostaDoItemDeDeclaracaoDeSaudeFixo { get; set; }
        public virtual BeneficiarioInput BeneficiarioInput { get; set; }
        public int BeneficiarioId { get; set; }
    }
}
