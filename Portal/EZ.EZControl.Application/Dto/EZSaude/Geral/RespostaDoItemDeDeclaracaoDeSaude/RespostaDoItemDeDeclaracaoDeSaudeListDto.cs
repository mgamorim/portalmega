﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.ItemDeDeclaracaoDeSaude;

namespace EZ.EZControl.Dto.EZSaude.Geral.RespostaDoItemDeDeclaracaoDeSaude
{
    public class RespostaDoItemDeDeclaracaoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Observacao { get; set; }
        public virtual ItemDeDeclaracaoDeSaudeListDto ItemDeDeclaracaoDeSaudeListDto { get; set; }
        public int ItemDeDeclaracaoDeSaudeId { get; set; }
        public RespostaDoItemDeDeclaracaoDeSaudeEnum RespostaDoItemDeDeclaracaoDeSaudeFixo { get; set; }
    }
}
