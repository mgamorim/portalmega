﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.DeclaracaoDeSaude.Mapper
{
    public class DeclaracaoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<DeclaracaoDeSaudeInput, Domain.EZSaude.Geral.DeclaracaoDeSaude>().ReverseMap();
            CreateMap<DeclaracaoDeSaudeListDto, Domain.EZSaude.Geral.DeclaracaoDeSaude>().ReverseMap();
            CreateMap<DeclaracaoDeSaudeListDto, DeclaracaoDeSaudeInput>();
        }
    }
}
