﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.FaixaEtaria
{
    public class GetFaixaEtariaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Descricao { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Descricao";
            }
        }
    }
}
