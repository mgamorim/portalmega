﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.FaixaEtaria.Mapper
{
    public class FaixaEtariaMapperProfile : Profile
    {
        public FaixaEtariaMapperProfile()
        {
            CreateMap<FaixaEtariaInput, Domain.EZSaude.Geral.FaixaEtaria>().ReverseMap();
            CreateMap<FaixaEtariaListDto, Domain.EZSaude.Geral.FaixaEtaria>().ReverseMap();
            CreateMap<FaixaEtariaListDto, FaixaEtariaInput>();
        }
    }
}
