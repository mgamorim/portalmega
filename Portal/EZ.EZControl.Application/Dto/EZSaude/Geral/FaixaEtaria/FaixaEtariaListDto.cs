﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.Geral.FaixaEtaria
{
    public class FaixaEtariaListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public string Descricao { get; set; }

        public int IdadeInicial { get; set; }

        public int? IdadeFinal { get; set; }
    }
}
