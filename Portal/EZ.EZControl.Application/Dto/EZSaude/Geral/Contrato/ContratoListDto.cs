﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.Geral.ValorPorFaixaEtaria;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.Contrato
{
    public class ContratoListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public ProdutoDePlanoDeSaudeListDto ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public List<ValorPorFaixaEtariaListDto> Valores { get; set; }
    }
}
