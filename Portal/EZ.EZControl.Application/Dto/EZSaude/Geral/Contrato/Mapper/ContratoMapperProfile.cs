﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.Contrato.Mapper
{
    public class ContratoMapperProfile : Profile
    {
        public ContratoMapperProfile()
        {
            CreateMap<ContratoInput, Domain.EZSaude.Geral.Contrato>().ReverseMap();
            CreateMap<ContratoListDto, Domain.EZSaude.Geral.Contrato>().ReverseMap();
            CreateMap<ContratoListDto, ContratoInput>();
        }
    }
}
