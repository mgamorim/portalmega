﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.ItemDeRedeCredenciada.Mapper
{
    public class ItemDeRedeCredenciadaMapperProfile : Profile
    {
        public ItemDeRedeCredenciadaMapperProfile()
        {
            CreateMap<ItemDeRedeCredenciadaInput, Domain.EZSaude.Geral.ItemDeRedeCredenciada>().ReverseMap();
            CreateMap<ItemDeRedeCredenciadaListDto, Domain.EZSaude.Geral.ItemDeRedeCredenciada>().ReverseMap();
            CreateMap<ItemDeRedeCredenciadaListDto, ItemDeRedeCredenciadaInput>();
        }
    }
}
