﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZSaude.Geral.UnidadeDeSaudeBase
{
    public class GetUnidadeDeSaudeExceptForItemRedeCredenciadaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id;

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "PessoaJuridica.NomeFantasia";
            }
        }
    }
}
