﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZSaude.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZSaude.Geral.ItemDeRedeCredenciada
{
    public class ItemDeRedeCredenciadaListDto : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public bool IsRedeDiferenciada { get; set; }
        public int RedeCredenciadaId { get; set; }
        public virtual RedeCredenciadaListDto RedeCredenciada { get; set; }
        public int UnidadeDeSaudeId { get; set; }
        public virtual UnidadeDeSaudeBaseInput UnidadeDeSaude { get; set; }
        public virtual bool IsHospital { get; set; }
        public virtual bool IsLaboratorio { get; set; }
    }
}
