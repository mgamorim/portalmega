﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.ItemDeRedeCredenciada
{
    public class GetItemDeRedeCredenciadaInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id { get; set; }
        public int RedeCredenciadaId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
