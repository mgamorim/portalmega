﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.ItemDeDeclaracaoDeSaude.Mapper
{
    public class ItemDeDeclaracaoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ItemDeDeclaracaoDeSaudeInput, Domain.EZSaude.Geral.ItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<ItemDeDeclaracaoDeSaudeListDto, Domain.EZSaude.Geral.ItemDeDeclaracaoDeSaude>().ReverseMap();
            CreateMap<ItemDeDeclaracaoDeSaudeListDto, ItemDeDeclaracaoDeSaudeInput>();
        }
    }
}
