﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZSaude.Geral.ItemDeDeclaracaoDeSaude
{
    public class ItemDeDeclaracaoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Pergunta { get; set; }
    }
}
