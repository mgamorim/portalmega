﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;

namespace EZ.EZControl.Dto.EZSaude.Geral.ItemDeDeclaracaoDeSaude
{
    public class ItemDeDeclaracaoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string Pergunta { get; set; }
        public virtual BeneficiarioListDto BeneficiarioListDto { get; set; }
        public int BeneficiarioId { get; set; }
    }
}
