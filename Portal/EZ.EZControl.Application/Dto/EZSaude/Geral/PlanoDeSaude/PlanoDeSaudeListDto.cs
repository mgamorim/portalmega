﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude
{
    public class PlanoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }
        public string NumeroDeRegistro { get; set; }
        public bool Reembolso { get; set; }
        public string DescricaoDoReembolso { get; set; }
        public AbrangenciaDoPlanoEnum Abrangencia { get; set; }
        public SegmentacaoAssistencialDoPlanoEnum SegmentacaoAssistencial { get; set; }
        public PlanoDeSaudeAnsEnum? PlanoDeSaudeAns { get; set; }
        public AcomodacaoEnum? Acomodacao { get; set; }
    }
}
