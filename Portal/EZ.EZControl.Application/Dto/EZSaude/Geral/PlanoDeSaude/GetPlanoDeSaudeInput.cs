﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude
{
    public class GetPlanoDeSaudeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string NumeroDeRegistro { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NumeroDeRegistro";
            }
        }
    }
}
