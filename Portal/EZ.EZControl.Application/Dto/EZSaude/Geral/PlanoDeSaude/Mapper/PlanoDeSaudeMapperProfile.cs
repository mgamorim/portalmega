﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude.Mapper
{
    public class PlanoDeSaudeMapperProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<PlanoDeSaudeInput, Domain.EZSaude.Geral.PlanoDeSaude>().ReverseMap();
            CreateMap<PlanoDeSaudeListDto, Domain.EZSaude.Geral.PlanoDeSaude>().ReverseMap();
            CreateMap<PlanoDeSaudeListDto, PlanoDeSaudeInput>();
        }
    }
}
