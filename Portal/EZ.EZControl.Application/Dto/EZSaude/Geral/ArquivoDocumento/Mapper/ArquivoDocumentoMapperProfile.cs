﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.ArquivoDocumento.Mapper
{
    public class ArquivoDocumentoMapperProfile : Profile
    {
        public ArquivoDocumentoMapperProfile()
        {
            CreateMap<ArquivoDocumentoInput, Domain.EZSaude.Geral.ArquivoDocumento>().ReverseMap();
            CreateMap<ArquivoDocumentoListDto, Domain.EZSaude.Geral.ArquivoDocumento>().ReverseMap();
            CreateMap<ArquivoDocumentoListDto, ArquivoDocumentoInput>();
        }
    }
}
