﻿using EZ.EZControl.Dto.Core.Geral.Arquivo;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Dto.EZSaude.Geral.ArquivoDocumento
{
    public class ArquivoDocumentoInput : ArquivoInput
    {
        public TipoDeDocumentoEnum Tipo { get; set; }
        public bool EmExigencia { get; set; }
        public string Motivo { get; set; }
        public int PropostaDeContratacaoId { get; set; }
    }
}
