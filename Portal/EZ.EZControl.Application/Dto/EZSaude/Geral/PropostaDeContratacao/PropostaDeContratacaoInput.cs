﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.ArquivoDocumento;
using EZ.EZControl.Dto.EZSaude.Geral.Contrato;
using EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Responsavel;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoInput : EzInputDto
    {
        public string Numero { get; set; }
        public ProponenteTitularInput Titular { get; set; }
        public int TitularId { get; set; }
        public ProdutoDePlanoDeSaudeInput Produto { get; set; }
        public int ProdutoId { get; set; }
        public ResponsavelInput Responsavel { get; set; }
        public int ResponsavelId { get; set; }
        public bool Aceite { get; set; }
        public bool Homologada { get; set; }
        public string ObservacaoHomologacao { get; set; }
        public DateTime DataHoraDoAceite { get; set; }
        public ContratoInput ContratoVigenteNoAceite { get; set; }
        public int ContratoVigenteNoAceiteId { get; set; }
        public ContratoInput UltimoContratoAceito { get; set; }
        public int UltimoContratoAceitoId { get; set; }
        public CorretorInput Corretor { get; set; }
        public int CorretorId { get; set; }
        public PedidoInput Pedido { get; set; }
        public int? PedidoId { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public StatusDaPropostaEnum StatusDaProposta { get; set; }
        public virtual List<ArquivoDocumentoInput> Documentos { get; set; }
    }
}
