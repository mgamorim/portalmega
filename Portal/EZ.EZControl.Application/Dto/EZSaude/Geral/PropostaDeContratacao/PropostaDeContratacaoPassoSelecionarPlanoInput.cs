﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using System.ComponentModel.DataAnnotations;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoSelecionarPlanoInput : EzInputDto
    {
        public PassoDaPropostaEnum PassoDaProposta { get; set; }

        [Required]
        public int ProdutoDePlanoDeSaudeId { get; set; }
    }
}
