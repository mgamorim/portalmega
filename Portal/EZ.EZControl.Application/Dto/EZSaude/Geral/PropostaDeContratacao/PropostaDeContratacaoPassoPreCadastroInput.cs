﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoPreCadastroInput : EzInputDto
    {
        public PassoDaPropostaEnum PassoDaProposta { get; set; }

        public string NomeCompleto { get; set; }

        public int TitularId { get; set; }

        public string Email { get; set; }

        public int EmailId { get; set; }

        public string Cpf { get; set; }

        public int CpfId { get; set; }

        public DateTime DataDeNascimento { get; set; }

        public string Celular { get; set; }

        public int CelulaId { get; set; }

        public int EstadoId { get; set; }

        public int ProfissaoId { get; set; }

        public int GrupoPessoaId { get; set; }

        public List<DependenteInput> Dependentes { get; set; }

        public int PessoaId { get; set; }
    }
}
