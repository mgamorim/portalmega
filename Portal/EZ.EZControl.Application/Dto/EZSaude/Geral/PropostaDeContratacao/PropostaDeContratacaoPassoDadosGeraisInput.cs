﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoDadosGeraisInput : EzInputDto
    {
        public ProponenteTitularInput ProponenteTitular { get; set; }
        public List<DependenteInput> Dependentes { get; set; }
        // Proponente Titular
        public string NomeProponenteTitular { get; set; }
        public int CpfIdProponenteTitular { get; set; }
        public string CpfProponenteTitular { get; set; }
        public DateTime DataDeNascimentoProponenteTitular { get; set; }
        public int RgIdProponenteTitular { get; set; }
        public string RgProponenteTitular { get; set; }
        public string OrgaoExpedidorProponenteTitular { get; set; }
        public string NacionalidadeProponenteTitular { get; set; }
        public int EmailIdProponenteTitular { get; set; }
        public string EmailProponenteTitular { get; set; }
        public string NomeDaMaeProponenteTitular { get; set; }
        public string NumeroDoCartaoNacionalDeSaudeProponenteTitular { get; set; }
        public string DeclaracaoDeNascidoVivoProponenteTitular { get; set; }
        public SexoEnum? SexoProponenteTitular { get; set; }
        public EstadoCivilEnum? EstadoCivilProponenteTitular { get; set; }
        //Reponsaveis Legais
        public TipoDeResponsavelEnum? TipoResponsavel { get; set; }
        public string NomeResponsavel { get; set; }
        public int CpfIdReponsavel { get; set; }
        public string CpfReponsavel { get; set; }
        public DateTime DataDeNascimentoReponsavel { get; set; }
        public string NacionalidadeResponsavel { get; set; }
        public SexoEnum? SexoResponsavel { get; set; }
        public EstadoCivilEnum? EstadoCivilResponsavel { get; set; }
        public EnderecoInput Endereco { get; set; }
    }
}
