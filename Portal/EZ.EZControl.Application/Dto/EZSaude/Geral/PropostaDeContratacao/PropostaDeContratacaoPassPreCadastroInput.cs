﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoPreCadastroInput : EzInputDto
    {
        public PassoDaPropostaEnum PassoDaProposta { get; set; }

        [Required]
        public string NomeCompleto { get; set; }

        public int TitularId { get; set; }

        [Required]
        public string Email { get; set; }

        public int EmailId { get; set; }

        [Required]
        public string Cpf { get; set; }

        public int CpfId { get; set; }

        [Required]
        public DateTime DataDeNascimento { get; set; }

        [Required]
        public string Celular { get; set; }

        public int CelularId { get; set; }

        [Required]
        public int EstadoId { get; set; }

        [Required]
        public int ProfissaoId { get; set; }
    }
}
