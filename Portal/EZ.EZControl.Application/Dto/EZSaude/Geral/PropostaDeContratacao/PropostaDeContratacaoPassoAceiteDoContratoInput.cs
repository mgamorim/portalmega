﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoAceiteDoContratoInput : EzInputDto
    {
        public int PropostaContratacaoId { get; set; }
        public int ContratoId { get; set; }
        public bool Aceite { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
    }
}
