﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoEnvioDeDocumentosInput : EzInputDto
    {
        public int PropostaContratacaoId { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
    }
}
