﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.Contrato;
using EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Responsavel;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using System;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoListDto : EzFullAuditedInputDto
    {
        public string Numero { get; set; }
        public ProponenteTitularListDto Titular { get; set; }
        public int TitularId { get; set; }
        public ProdutoDePlanoDeSaudeListDto Produto { get; set; }
        public int ProdutoId { get; set; }
        public ResponsavelListDto Responsavel { get; set; }
        public int ResponsavelId { get; set; }
        public bool Aceite { get; set; }
        public bool Homologada { get; set; }
        public DateTime DataHoraDoAceite { get; set; }
        public ContratoListDto ContratoVigenteNoAceite { get; set; }
        public int ContratoVigenteNoAceiteId { get; set; }
        public ContratoListDto UltimoContratoAceito { get; set; }
        public int UltimoContratoAceitoId { get; set; }
        public CorretorListDto Corretor { get; set; }
        public int CorretorId { get; set; }
        public PedidoListDto Pedido { get; set; }
        public int? PedidoId { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }
        public StatusDaPropostaEnum StatusDaProposta { get; set; }
    }
}
