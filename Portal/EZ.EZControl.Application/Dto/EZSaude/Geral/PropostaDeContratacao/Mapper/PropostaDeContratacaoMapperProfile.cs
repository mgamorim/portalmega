﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao.Mapper
{
    public class PropostaDeContratacaoMapperProfile : Profile
    {
        public PropostaDeContratacaoMapperProfile()
        {
            CreateMap<PropostaDeContratacaoInput, Domain.EZSaude.Geral.PropostaDeContratacao>().ReverseMap();
            CreateMap<PropostaDeContratacaoListDto, Domain.EZSaude.Geral.PropostaDeContratacao>().ReverseMap();
            CreateMap<PropostaDeContratacaoListDto, PropostaDeContratacaoInput>();
        }
    }
}
