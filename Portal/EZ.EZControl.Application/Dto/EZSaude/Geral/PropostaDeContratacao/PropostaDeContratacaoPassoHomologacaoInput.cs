﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.Geral.PropostaDeContratacao
{
    public class PropostaDeContratacaoPassoHomologacaoInput : EzInputDto
    {
        public bool Homologada { get; set; }
        public string ObservacaoHomologacao { get; set; }
        public PassoDaPropostaEnum PassoDaProposta { get; set; }

    }
}
