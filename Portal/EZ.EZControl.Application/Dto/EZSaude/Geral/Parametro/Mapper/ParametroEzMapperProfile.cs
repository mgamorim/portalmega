﻿using AutoMapper;
using EZ.EZControl.Dto.Core.Geral.Parametro.Mapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.Parametro.Mapper
{
    public class ParametroEzMapperProfile : ParametroMapperProfile
    {
        protected override void Configure()
        {
            CreateMap<ParametroEzsaudeInput, Domain.EZSaude.Geral.ParametroEzsaude>().ReverseMap();
            CreateMap<ParametroEzsaudeListDto, Domain.EZSaude.Geral.ParametroEzsaude>().ReverseMap();
            CreateMap<ParametroEzsaudeListDto, ParametroEzsaudeInput>();
        }
    }
}
