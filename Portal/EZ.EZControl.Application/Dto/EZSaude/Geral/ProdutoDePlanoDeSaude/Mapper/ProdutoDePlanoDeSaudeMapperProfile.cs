﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude.Mapper
{
    public class ProdutoDePlanoDeSaudeMapperProfile : Profile
    {
        public ProdutoDePlanoDeSaudeMapperProfile()
        {
            CreateMap<ProdutoDePlanoDeSaudeInput, Domain.EZSaude.Geral.ProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<ProdutoDePlanoDeSaudeListDto, Domain.EZSaude.Geral.ProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<ProdutoDePlanoDeSaudeListDto, ProdutoDePlanoDeSaudeListDto>();
        }
    }
}
