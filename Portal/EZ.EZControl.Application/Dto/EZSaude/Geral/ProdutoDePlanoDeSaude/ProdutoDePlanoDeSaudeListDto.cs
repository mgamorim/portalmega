﻿using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.EZSaude.Geral.Contrato;
using EZ.EZControl.Dto.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude
{
    public class ProdutoDePlanoDeSaudeListDto : ProdutoListDto
    {
        public virtual PlanoDeSaudeListDto PlanoDeSaude { get; set; }
        public int PlanoDeSaudeId { get; set; }

        public virtual AdministradoraListDto Administradora { get; set; }
        public int? AdministradoraId { get; set; }

        public virtual OperadoraListDto Operadora { get; set; }
        public int? OperadoraId { get; set; }

        public bool CarenciaEspecial { get; set; }
        public string DescricaoDaCarenciaEspecial { get; set; }

        public bool Acompanhante { get; set; }
        public string DescricaoDoAcompanhante { get; set; }

        public bool CoberturaExtra { get; set; }
        public string DescricaoDaCoberturaExtra { get; set; }

        public ContratoListDto Contrato { get; set; }

        public IEnumerable<EspecialidadePorProdutoDePlanoDeSaudeListDto> Especialidades { get; set; }

        public RedeCredenciadaListDto RedeCredenciada { get; set; }
        public int? RedeCredenciadaId { get; set; }

        public virtual ICollection<CidadeListDto> Cidades { get; set; }

        public virtual ICollection<EstadoListDto> Estados { get; set; }
    }
}
