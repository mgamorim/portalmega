﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude
{
    public class GetProdutoDePlanoDeSaudeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
