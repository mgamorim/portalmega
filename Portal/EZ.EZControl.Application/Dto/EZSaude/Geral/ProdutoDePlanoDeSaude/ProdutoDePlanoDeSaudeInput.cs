﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.EZSaude.Geral.Contrato;
using EZ.EZControl.Dto.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude;
using EZ.EZControl.Dto.EZSaude.Geral.RedeCredenciada;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude
{
    public class ProdutoDePlanoDeSaudeInput : ProdutoInput
    {
        public PlanoDeSaudeInput PlanoDeSaude { get; set; }
        public int PlanoDeSaudeId { get; set; }

        public AdministradoraInput Administradora { get; set; }
        public int? AdministradoraId { get; set; }

        public OperadoraInput Operadora { get; set; }
        public int OperadoraId { get; set; }

        public bool CarenciaEspecial { get; set; }
        public string DescricaoDaCarenciaEspecial { get; set; }

        public bool Acompanhante { get; set; }
        public string DescricaoDoAcompanhante { get; set; }

        public bool CoberturaExtra { get; set; }
        public string DescricaoDaCoberturaExtra { get; set; }
        
        public int[] EspecialidadesIds { get; set; }

        public virtual IEnumerable<EspecialidadePorProdutoDePlanoDeSaudeListDto> Especialidades { get; set; }

        public ContratoInput Contrato { get; set; }

        public RedeCredenciadaInput RedeCredenciada { get; set; }
        public int? RedeCredenciadaId { get; set; }

        public virtual ICollection<CidadeInput> Cidades { get; set; }

        public virtual ICollection<EstadoInput> Estados { get; set; }
    }
}
