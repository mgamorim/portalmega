﻿using Abp.Runtime.Validation;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using System.Collections.Generic;

namespace EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude
{
    public class GetCidadeExceptForProduto : PagedAndSortedInputDto, IShouldNormalize
    {
        public int ProdutoDePlanoDeSaudeId { get; set; }

        public string Nome { get; set; }
        
        public virtual ICollection<CidadeInput> Cidades { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
