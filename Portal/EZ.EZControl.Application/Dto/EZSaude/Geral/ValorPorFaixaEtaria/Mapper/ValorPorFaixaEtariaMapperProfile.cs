﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.ValorPorFaixaEtaria.Mapper
{
    public class ValorPorFaixaEtariaMapperProfile : Profile
    {
        public ValorPorFaixaEtariaMapperProfile()
        {
            CreateMap<ValorPorFaixaEtariaInput, Domain.EZSaude.Geral.ValorPorFaixaEtaria>().ReverseMap();
            CreateMap<ValorPorFaixaEtariaListDto, Domain.EZSaude.Geral.ValorPorFaixaEtaria>().ReverseMap();
            CreateMap<ValorPorFaixaEtariaListDto, ValorPorFaixaEtariaInput>();
        }
    }
}
