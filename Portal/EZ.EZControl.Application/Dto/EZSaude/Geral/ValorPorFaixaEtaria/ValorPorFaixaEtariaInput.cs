﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.FaixaEtaria;

namespace EZ.EZControl.Dto.EZSaude.Geral.ValorPorFaixaEtaria
{
    public class ValorPorFaixaEtariaInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int ContratoId { get; set; }

        public FaixaEtariaInput FaixaEtaria { get; set; }
        public int FaixaEtariaId { get; set; }

        public decimal Valor { get; set; }
    }
}
