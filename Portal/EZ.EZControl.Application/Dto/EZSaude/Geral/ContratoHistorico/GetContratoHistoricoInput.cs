﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.ContratoHistorico
{
    public class GetContratoHistoricoInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int ContratoId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "DataEvento";
            }
        }
    }
}
