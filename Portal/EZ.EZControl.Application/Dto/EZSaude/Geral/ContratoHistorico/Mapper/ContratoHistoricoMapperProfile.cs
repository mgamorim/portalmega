﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.ContratoHistorico.Mapper
{
    public class ContratoHistoricoMapperProfile : Profile
    {
        public ContratoHistoricoMapperProfile()
        {
            CreateMap<ContratoHistoricoInput, Domain.EZSaude.Geral.ContratoHistorico>().ReverseMap();
            CreateMap<ContratoHistoricoListDto, Domain.EZSaude.Geral.ContratoHistorico>().ReverseMap();
            CreateMap<ContratoHistoricoListDto, ContratoHistoricoInput>();
        }
    }
}
