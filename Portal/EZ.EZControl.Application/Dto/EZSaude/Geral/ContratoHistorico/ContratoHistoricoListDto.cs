﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZSaude.Geral.Contrato;
using EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude;
using System;

namespace EZ.EZControl.Dto.EZSaude.Geral.ContratoHistorico
{
    public class ContratoHistoricoListDto : EzFullAuditedInputDto
    {
        public DateTime DataCriacao { get; set; }
        public DateTime DataInicioVigencia { get; set; }
        public DateTime DataFimVigencia { get; set; }
        public string Conteudo { get; set; }
        public ProdutoDePlanoDeSaudeListDto ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }
        public ContratoListDto Contrato { get; set; }
        public int ContratoId { get; set; }
        public long UserId { get; set; }
        public DateTime DataEvento { get; set; }
    }
}
