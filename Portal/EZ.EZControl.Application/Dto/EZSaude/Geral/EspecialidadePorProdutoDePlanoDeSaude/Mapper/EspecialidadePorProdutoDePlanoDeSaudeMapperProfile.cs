﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude.Mapper
{
    public class EspecialidadePorProdutoDePlanoDeSaudeMapperProfile : Profile
    {
        public EspecialidadePorProdutoDePlanoDeSaudeMapperProfile()
        {
            CreateMap<EspecialidadePorProdutoDePlanoDeSaudeInput, Domain.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<EspecialidadePorProdutoDePlanoDeSaudeListDto, Domain.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude>().ReverseMap();
            CreateMap<EspecialidadePorProdutoDePlanoDeSaudeListDto, EspecialidadePorProdutoDePlanoDeSaudeInput>();
        }
    }
}
