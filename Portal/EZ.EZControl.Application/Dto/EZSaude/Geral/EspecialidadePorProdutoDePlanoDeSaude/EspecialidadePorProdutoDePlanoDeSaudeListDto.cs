﻿using Abp.Domain.Entities;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude;

namespace EZ.EZControl.Dto.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude
{
    public class EspecialidadePorProdutoDePlanoDeSaudeListDto : EzFullAuditedInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public ProdutoDePlanoDeSaudeListDto ProdutoDePlanoDeSaude { get; set; }
        public int ProdutoDePlanoDeSaudeId { get; set; }

        public EspecialidadeListDto Especialidade { get; set; }
        public int EspecialidadeId { get; set; }
    }
}
