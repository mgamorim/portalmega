﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude
{
    public class GetEspecialidadePorProdutoDePlanoDeSaudeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public decimal nome { get; set; }

        public int ProdutoDePlanoDeSaudeId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "id";
            }
        }
    }
}
