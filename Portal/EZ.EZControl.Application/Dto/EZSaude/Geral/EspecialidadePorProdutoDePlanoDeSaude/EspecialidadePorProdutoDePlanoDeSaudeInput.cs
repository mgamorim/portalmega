﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZ;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;

namespace EZ.EZControl.Dto.EZSaude.Geral.EspecialidadePorProdutoDePlanoDeSaude
{
    public class EspecialidadePorProdutoDePlanoDeSaudeInput : EzInputDto, IPassivable
    {
        public bool IsActive { get; set; }

        public int ProdutoDePlanoDeSaudeId { get; set; }

        public EspecialidadeInput Especialidade { get; set; }
        public int EspecialidadeId { get; set; }
    }
}
