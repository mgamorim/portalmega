﻿using Abp.Domain.Entities;
using EZ.EZControl.Dto.EZSaude.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZSaude.Geral.Laboratorio
{
    public class LaboratorioListDto : UnidadeDeSaudeBaseListDto
    {
    }
}
