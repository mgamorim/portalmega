﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.Laboratorio.Mapper
{
    public class LaboratorioMapperProfile : Profile
    {
        public LaboratorioMapperProfile()
        {
            CreateMap<LaboratorioInput, Domain.EZSaude.Geral.Laboratorio>().ReverseMap();
            CreateMap<LaboratorioListDto, Domain.EZSaude.Geral.Laboratorio>().ReverseMap();
            CreateMap<LaboratorioListDto, LaboratorioInput>();
        }
    }
}
