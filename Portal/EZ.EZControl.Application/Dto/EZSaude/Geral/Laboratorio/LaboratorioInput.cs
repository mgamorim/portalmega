﻿using EZ.EZControl.Dto.EZSaude.Geral.UnidadeDeSaudeBase;

namespace EZ.EZControl.Dto.EZSaude.Geral.Laboratorio
{
    public class LaboratorioInput : UnidadeDeSaudeBaseInput
    {
        public LaboratorioInput()
        {
            IsActive = true;
            TipoDeUnidade = Domain.EZSaude.Enums.TipoDeUnidadeEnum.Laboratorio;       
        }
    }
}
