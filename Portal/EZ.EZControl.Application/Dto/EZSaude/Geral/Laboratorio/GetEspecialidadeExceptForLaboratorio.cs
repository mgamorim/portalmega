﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Dto.EZSaude.Getal.Laboratorio
{
    public class GetEspecialidadeExceptForLaboratorio : PagedAndSortedInputDto, IShouldNormalize
    {
        public int LaboratorioId { get; set; }

        public string Nome { get; set; }

        public string Codigo { get; set; }

        public virtual ICollection<EspecialidadeInput> Especialidades { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "NomePessoa";
            }
        }
    }
}
