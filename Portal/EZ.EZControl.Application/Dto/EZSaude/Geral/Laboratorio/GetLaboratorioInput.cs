﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Geral.Laboratorio
{
    public class GetLaboratorioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int Id { get; set; }

        public string Nome { get; set; }
        public string Site { get; set; }
        public string Email { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
