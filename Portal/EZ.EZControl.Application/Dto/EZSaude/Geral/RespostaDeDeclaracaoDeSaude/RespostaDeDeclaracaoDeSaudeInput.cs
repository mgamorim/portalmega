﻿using EZ.EZControl.Dto.EZ;

namespace EZ.EZControl.Dto.EZSaude.Geral.RespostaDeDeclaracaoDeSaude
{
    public class RespostaDeDeclaracaoDeSaudeInput : EzInputDto
    {
        public string Pergunta { get; set; }
        public int PerguntaId { get; set; }
        public string Marcada { get; set; }
        public string Observacao { get; set; }
    }
}
