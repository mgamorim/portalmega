﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Geral.UnidadeDeSaudeBase.Mapper
{
    public class UnidadeDeSaudeBaseMapperProfile : Profile
    {
        public UnidadeDeSaudeBaseMapperProfile()
        {
            CreateMap<UnidadeDeSaudeBaseInput, Domain.EZSaude.Geral.UnidadeDeSaudeBase>().ReverseMap();
            CreateMap<UnidadeDeSaudeBaseListDto, Domain.EZSaude.Geral.UnidadeDeSaudeBase>().ReverseMap();
            CreateMap<UnidadeDeSaudeBaseListDto, UnidadeDeSaudeBaseInput>();
        }
    }
}
