﻿using AutoMapper;

namespace EZ.EZControl.Dto.EZSaude.Sync.EspecialidadeSync.Mapper
{
    public class EspecialidadeSyncMapperProfile : Profile
    {
        public EspecialidadeSyncMapperProfile()
        {
            CreateMap<EspecialidadeSyncInput, Domain.EZSaude.Sync.EspecialidadeSync>().ReverseMap();
            CreateMap<EspecialidadeSyncListDto, Domain.EZSaude.Sync.EspecialidadeSync>().ReverseMap();
            CreateMap<EspecialidadeSyncListDto, EspecialidadeSyncInput>();
        }
    }
}
