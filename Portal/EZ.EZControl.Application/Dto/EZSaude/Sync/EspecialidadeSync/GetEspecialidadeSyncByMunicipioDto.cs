﻿namespace EZ.EZControl.Dto.EZSaude.Sync.EspecialidadeSync
{
    public class GetEspecialidadeSyncByMunicipioDto
    {
        public string Estado { get; set; }
        public string NomeEstado { get; set; }
        public string Municipio { get; set; }
    }
}
