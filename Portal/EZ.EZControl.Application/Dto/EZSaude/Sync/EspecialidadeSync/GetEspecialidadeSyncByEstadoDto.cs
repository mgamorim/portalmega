﻿namespace EZ.EZControl.Dto.EZSaude.Sync.EspecialidadeSync
{
    public class GetEspecialidadeSyncByEstadoDto
    {
        public string Estado { get; set; }
        public string NomeEstado { get; set; }
    }
}
