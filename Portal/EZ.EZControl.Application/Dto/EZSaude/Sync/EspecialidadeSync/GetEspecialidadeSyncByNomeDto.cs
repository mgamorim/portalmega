﻿namespace EZ.EZControl.Dto.EZSaude.Sync.EspecialidadeSync
{
    public class GetEspecialidadeSyncByNomeDto
    {
        public string Nome { get; set; }
    }
}
