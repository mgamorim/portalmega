﻿using Abp.Runtime.Validation;

namespace EZ.EZControl.Dto.EZSaude.Sync.EspecialidadeSync
{
    public class GetEspecialidadeSyncInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Nome { get; set; }
        public string Estado { get; set; }
        public string Municipio { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Nome";
            }
        }
    }
}
