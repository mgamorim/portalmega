﻿using Abp.Application.Services;
using Abp.Collections.Extensions;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Runtime.Session;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.EZ.EzEntity.Interfaces;
using EZ.EZControl.Filters;
using EZ.EZControl.MultiTenancy;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace EZ.EZControl
{
    /// <summary>
    /// All application services in this application is derived from this class.
    /// We can add common application service methods here.
    /// </summary>
    public abstract class EZControlAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected EZControlAppServiceBase()
        {
            LocalizationSourceName = EZControlConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual User GetCurrentUser()
        {
            var user = UserManager.FindById(AbpSession.GetUserId());
            if (user == null)
            {
                throw new ApplicationException("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
            }
        }

        protected virtual Tenant GetCurrentTenant()
        {
            using (CurrentUnitOfWork.SetTenantId(null))
            {
                return TenantManager.GetById(AbpSession.GetTenantId());
            }
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        protected virtual bool CheckForFieldDuplicated<TEntity, TPrimaryKey>(IRepository<TEntity, TPrimaryKey> repository, TPrimaryKey id, object valor, Func<TEntity, bool> condicao)
            where TEntity : class, IEntity<TPrimaryKey>
            where TPrimaryKey : IEquatable<TPrimaryKey>
        {
            Func<IRepository<TEntity, TPrimaryKey>, TPrimaryKey, object, Func<TEntity, bool>, bool> checkForFieldDuplicatedFunc = (repositoryFunc, idFunc, valorFunc, condicaoFunc) =>
            {
                return repositoryFunc.GetAll()
                    .WhereIf(valorFunc != null, condicaoFunc)
                    .Any(x => !x.Id.Equals(idFunc));
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return ApplyEmpresaFilter(() =>
                {
                    return checkForFieldDuplicatedFunc(repository, id, valor, condicao);
                });
            }

            return checkForFieldDuplicatedFunc(repository, id, valor, condicao);
        }
        protected virtual bool CheckForDuplicateInstance<TEntity, TPrimaryKey>(IRepository<TEntity, TPrimaryKey> repository, TPrimaryKey id, Expression<Func<TEntity, bool>> condicao)
            where TEntity : class, IEntity<TPrimaryKey>
            where TPrimaryKey : IEquatable<TPrimaryKey>
        {
            Func<IRepository<TEntity, TPrimaryKey>, TPrimaryKey, Expression<Func<TEntity, bool>>, bool> checkForDuplicateInstanceFunc = (repositoryFunc, idFunc, condicaoFunc) =>
            {
                return repositoryFunc.GetAll()
                    .Where(condicaoFunc)
                    .Any(x => !x.Id.Equals(idFunc));
            };

            if (typeof(IHasEmpresa).IsAssignableFrom(typeof(TEntity)))
            {
                return ApplyEmpresaFilter(() =>
                {
                    return checkForDuplicateInstanceFunc(repository, id, condicao);
                });
            }

            return checkForDuplicateInstanceFunc(repository, id, condicao);
        }

        protected TResult ApplyEmpresaFilter<TResult>(Func<TResult> func)
        {
            if (func == null)
            {
                throw new NotSupportedException("Os parâmetros do método ExecActionIfHasEmpresa não podem estar nulos.");
            }

            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

            if (empresaId <= 0) {
                throw new NotSupportedException("Não foi possível recuperar a empresaId da Claim Principal.");
            }

            using (CurrentUnitOfWork.EnableFilter(EzControlFilters.EmpresaFilter))
            {
                using (CurrentUnitOfWork.SetFilterParameter(EzControlFilters.EmpresaFilter, EzControlFilters.Parameters.EmpresaId, empresaId))
                {
                    return func();
                }
            }
        }

        protected void ApplyEmpresaFilter(Action action)
        {
            if (action == null)
            {
                throw new NotSupportedException("Os parâmetros do método ExecActionIfHasEmpresa não podem estar nulos.");
            }

            var empresaId = Thread.CurrentPrincipal.GetEmpresaIdFromClaim();

            if (empresaId <= 0)
            {
                throw new NotSupportedException("Não foi possível recuperar a empresaId da Claim Principal.");
            }

            using (CurrentUnitOfWork.EnableFilter(EzControlFilters.EmpresaFilter))
            {
                using (CurrentUnitOfWork.SetFilterParameter(EzControlFilters.EmpresaFilter, EzControlFilters.Parameters.EmpresaId, empresaId))
                {
                    action();
                }
            }
        }
    }
}