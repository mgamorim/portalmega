﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace EZ.EZControl.Localization.Dto
{
    public class CreateOrUpdateLanguageInput
    {
        [Required]
        public ApplicationLanguageEditDto Language { get; set; }
    }
}