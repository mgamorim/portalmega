using System.Collections.Generic;
using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Dto;

namespace EZ.EZControl.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}