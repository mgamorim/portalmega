﻿using EZ.EZControl.Domain.Core.Enums;

namespace EZ.EZControl.Authorization.Users.Dto
{
    public class GetTipoDeUsuarioInput
    {
        public TipoDeUsuarioEnum TipoDeUsuario { get; set; }
        public int UserId { get; set; }
        public string StrRoles { get; set; }
    }
}