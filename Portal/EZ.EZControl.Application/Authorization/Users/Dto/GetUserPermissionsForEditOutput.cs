﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using EZ.EZControl.Authorization.Permissions.Dto;

namespace EZ.EZControl.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}