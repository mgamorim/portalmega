﻿using Abp.Application.Services.Dto;

namespace EZ.EZControl.Authorization.Roles.Dto
{
    public class GetRolesInput 
    {
        public string Permission { get; set; }
    }
}
