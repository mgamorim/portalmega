﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Authorization.Permissions.Dto;

namespace EZ.EZControl.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
