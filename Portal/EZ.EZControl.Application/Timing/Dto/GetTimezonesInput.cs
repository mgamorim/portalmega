﻿using Abp.Configuration;

namespace EZ.EZControl.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope;
    }
}
