﻿using EZ.EZControl.Dto;

namespace EZ.EZControl.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}