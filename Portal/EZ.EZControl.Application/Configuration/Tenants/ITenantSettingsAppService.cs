﻿using System.Threading.Tasks;
using Abp.Application.Services;
using EZ.EZControl.Configuration.Tenants.Dto;

namespace EZ.EZControl.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);
    }
}
