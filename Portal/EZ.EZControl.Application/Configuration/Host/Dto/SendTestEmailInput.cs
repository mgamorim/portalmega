﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using EZ.EZControl.Authorization.Users;

namespace EZ.EZControl.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}