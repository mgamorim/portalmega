﻿using System.Threading.Tasks;
using Abp.Application.Services;
using EZ.EZControl.Configuration.Host.Dto;

namespace EZ.EZControl.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
