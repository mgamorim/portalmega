﻿using System.Collections.Generic;

namespace EZ.EZControl.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatesWebLogLines { get; set; }
    }
}
