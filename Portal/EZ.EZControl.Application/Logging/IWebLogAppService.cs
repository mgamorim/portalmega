﻿using Abp.Application.Services;
using EZ.EZControl.Dto;
using EZ.EZControl.Logging.Dto;

namespace EZ.EZControl.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
