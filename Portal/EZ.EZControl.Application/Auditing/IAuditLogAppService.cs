using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using EZ.EZControl.Auditing.Dto;
using EZ.EZControl.Dto;

namespace EZ.EZControl.Auditing
{
    public interface IAuditLogAppService : IApplicationService
    {
        Task<PagedResultDto<AuditLogListDto>> GetAuditLogs(GetAuditLogsInput input);

        Task<FileDto> GetAuditLogsToExcel(GetAuditLogsInput input);
    }
}