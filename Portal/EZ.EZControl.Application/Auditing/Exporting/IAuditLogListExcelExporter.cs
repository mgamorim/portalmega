﻿using System.Collections.Generic;
using EZ.EZControl.Auditing.Dto;
using EZ.EZControl.Dto;

namespace EZ.EZControl.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
