﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using EZ.EZControl.Tests.Application.CMS.Integration.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Integration
{
    /// <summary>
    /// Classe de Teste criada para Testar as propriedades de integração
    /// E o update na propriedade DateOfEditionIntegration das entidades
    /// Exemplo foi feito na classe de Tag
    /// </summary>
    public class IntegrationCMSAppService_Tests : IntegrationCMSAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateTagAndTestAsync(string descricao, string nome, string slug,
            int idExterno = 0, bool isIntegration = false)
        {
            await base.TagAppService.Save(new TagInput
            {
                IsActive = true,
                Nome = nome,
                Slug = slug,
                Descricao = descricao,
                ExternalId = idExterno,
                IsIntegration = isIntegration
            });
        }

        private async Task<TagInput> CreateAndReturnTagAndTestAsync(string descricao, string nome, string slug,
            int idExterno = 0, bool isIntegration = false)
        {
            return await base.TagAppService.SaveAndReturnEntity(new TagInput
            {
                IsActive = true,
                Nome = nome,
                Slug = slug,
                Descricao = descricao,
                ExternalId = idExterno,
                IsIntegration = isIntegration
            });
        }

        [Fact]
        public async Task Should_Create_And_Return_Tag_With_Integration()
        {
            #region Arrange

            base.CreateTestTags();

            #endregion

            #region Action

            var tag = await CreateAndReturnTagAndTestAsync("Email", "eMail", "e-mail", 15, true);

            #endregion

            #region Assert

            tag.ShouldNotBeNull();

            #endregion
        }

        [Fact]
        public async Task Should_Create_And_Return_Tag_Without_Integration()
        {
            #region Arrange

            base.CreateTestTags();

            #endregion

            #region Action

            var dto = await CreateAndReturnTagAndTestAsync("Contato", "iContato", "fale-conosco");

            #endregion

            #region Assert

            dto.ShouldNotBeNull();

            #endregion
        }

        [Fact]
        public async Task Should_Update_Tag_With_Integration()
        {
            #region Arrange

            await CreateTagAndTestAsync("Home", "iHome", "pagina-inicial", 2485, true);

            #endregion

            #region Action

            var entity = await TagAppService.GetByExternalId(new IdInput(2485));
            entity.Nome = "Erro";
            entity.Slug = "tag-de-erro";
            entity.Descricao = "iErro";

            var dto = await base.TagAppService.SaveAndReturnEntity(entity);

            #endregion

            #region Assert

            dto.Nome.ShouldNotBe("Home");
            dto.Slug.ShouldNotBe("pagina-inicial");
            dto.Descricao.ShouldNotBe("iHome");

            #endregion
        }

        [Fact]
        public async Task Should_Create_And_Get_Tag_ByExternalId()
        {
            #region Arrange

            await CreateTagAndTestAsync("Home", "iHome", "pagina-inicial", 2485, true);

            #endregion

            #region Action

            var dto = await TagAppService.GetByExternalId(new IdInput(2485));

            #endregion

            #region Assert

            dto.ExternalId.ShouldNotBe(0);
            dto.DateOfEditionIntegration.ShouldNotBeNull();
            #endregion
        }
    }
}
