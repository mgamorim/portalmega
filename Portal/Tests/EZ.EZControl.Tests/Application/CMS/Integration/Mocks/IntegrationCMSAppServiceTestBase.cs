﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Integration.Mocks
{
    public class IntegrationCMSAppServiceTestBase : AppTestBase
    {
        protected readonly ITagAppService TagAppService;

        public IntegrationCMSAppServiceTestBase()
        {
            this.TagAppService = Resolve<ITagAppService>();
        }

        protected void CreateTestTags()
        {
            UsingDbContext(context =>
            {
                context.Tags.Add(CreateTagEntity("Marketing", "iMarketing", "_marketing", 1025));
                context.Tags.Add(CreateTagEntity("Blog", "iBlog", "_blog"));
            });
        }

        private Tag CreateTagEntity(string descricao, string nome, string slug, int idExterno = 0)
        {
            return new Tag
            {
                IsActive = true,
                Nome = nome,
                Slug = slug,
                Descricao = descricao,
                ExternalId = idExterno,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
