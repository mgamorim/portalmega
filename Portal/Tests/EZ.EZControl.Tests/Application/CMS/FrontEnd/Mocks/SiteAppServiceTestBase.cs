﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.CMSFrontEnd.Geral.Mocks
{
    public class SiteAppServiceTestBase : AppTestBase
    {
        protected readonly ISiteAppService _siteAppService;
        protected readonly Services.CMSFrontEnd.Interfaces.ISiteAppService _siteFrontEndAppService;

        public SiteAppServiceTestBase()
        {
            _siteAppService = Resolve<ISiteAppService>();
            _siteFrontEndAppService = Resolve<Services.CMSFrontEnd.Interfaces.ISiteAppService>();
        }
        protected void CreateTestTemplates()
        {
            UsingDbContext(context =>
            {
                context.Templates.Add(CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"));
            });
        }
        public Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                    string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                    string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                    string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
        private Site CreateSiteEntity(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner)
        {
            var site = new Site()
            {
                Nome = nome,
                Descricao = descricao,
                MetaKeywords = metaKeywords,
                MetaTags = metaTags,
                Owner = owner,
                EmpresaId = EmpresaIdDefaultTest
            };
            site.Hosts.Add(new Host() {
                IsActive = true,
                IsPrincipal = true,
                Url = host,
                EmpresaId = EmpresaIdDefaultTest
            });
            return site;
        }
    }
}