﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using EZ.EZControl.Domain.CMS.Enums;

namespace EZ.EZControl.Tests.Application.CMSFrontEnd.Geral.Mocks
{
    public class MenuAppServiceTestBase : AppTestBase
    {
        protected readonly IMenuAppService _menuAppService;
        protected readonly ISiteAppService _siteAppService;
        protected readonly IItemDeMenuAppService _itemDeMenuAppService;

        public MenuAppServiceTestBase()
        {
            _menuAppService = Resolve<IMenuAppService>();
            _siteAppService = Resolve<ISiteAppService>();
            _itemDeMenuAppService = Resolve<IItemDeMenuAppService>();
        }

        protected void CreateMenuTest()
        {
            UsingDbContext(context =>
            {
                context.Menus.Add(CreateMenuEntity());
            });
        }

        protected void CreateEstruturaMenuTest()
        {
            UsingDbContext(context =>
            {
                context.Menus.Add(CreateMenuEntity());
                var menu = context.Menus.Find(1);

                context.ItensDeMenu.Add(CreateItemDeMenuEntity(menu));
                var item = context.ItensDeMenu.Find(1);

                context.ItensDeMenu.Add(CreateSubItemDeMenuEntity(item));
            });
        }

        private ItemDeMenu CreateItemDeMenuEntity(Menu menu)
        {
            return new ItemDeMenu()
            {
                Menu = menu,
                DescricaoDoTitulo = "Google",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Titulo = "Google",
                Url = "http://www.google.com",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ItemDeMenu CreateSubItemDeMenuEntity(ItemDeMenu itemDeMenu)
        {
            return new ItemDeMenu()
            {
                ItemMenu = itemDeMenu,
                DescricaoDoTitulo = "Amazon",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Titulo = "Amazon",
                Url = "http://www.amazon.com",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Menu CreateMenuEntity()
        {
            return new Menu
            {
                IsActive = true,
                Nome = "Inicial",
                Site = CreateSiteEntity("Hinode", "", "http://www.hinode.com.br", "", "", "")
            };
        }

        private Site CreateSiteEntity(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner)
        {
            return new Site()
            {
                Nome = nome,
                Descricao = descricao,
                //Host = host,
                MetaKeywords = metaKeywords,
                MetaTags = metaTags,
                Owner = owner,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}