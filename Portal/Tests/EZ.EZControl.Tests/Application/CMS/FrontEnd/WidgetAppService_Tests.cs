﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Dto.CMS.Geral.Site;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using WidgetAppServiceTestBase = EZ.EZControl.Tests.Application.CMSFrontEnd.Geral.Mocks.WidgetAppServiceTestBase;

namespace EZ.EZControl.Tests.Application.CMSFrontEnd.Geral
{
    public class WidgetAppService_Tests : WidgetAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateSiteAndTestAsync()
        {
            await SiteAppService.Save(
                new SiteInput
                {
                    Nome = "EZControl",
                    Descricao = string.Empty,
                    Hosts = new List<HostInput>
                            {
                                new HostInput
                                {
                                    IsActive = true,
                                    IsPrincipal = true,
                                    Url = "ezcontrol.com"
                                }
                            },
                    MetaKeywords = string.Empty,
                    MetaTags = string.Empty,
                    Owner = string.Empty,
                    TemplateDefaultId = 1,
                    TipoDePaginaParaPaginaDefault = "Login",
                    TipoDePaginaParaCategoriaDefault = "QuemSomos",
                    TipoDePaginaParaPaginaInicialDefault = "Contato",
                    TipoDePaginaParaPostDefault = "Home"
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Id == 1);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateMenuAndTestAsync()
        {
            await base.MenuAppService.Save(
                new MenuInput()
                {
                    IsActive = true,
                    SiteId = 1,
                    Nome = "Lateral"
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Menus.FirstOrDefaultAsync(i => i.Site.Id == 1);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }

        private async Task CreateWidgetHTMLAndTestAsync()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync();
            await WidgetAppService.Save(new Dto.CMS.Geral.Widget.WidgetInput
            {
                Conteudo = "<p>Teste</p>",
                ExibirTitulo = true,
                IsActive = true,
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                SiteId = 1,
                TemplateId = 1,
                Tipo = TipoDeWidgetEnum.CmsHtml,
                Titulo = "Teste HTML"
            });
        }

        private async Task CreateWidgetMenuAndTestAsync()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync();
            await CreateMenuAndTestAsync();
            await WidgetAppService.Save(new Dto.CMS.Geral.Widget.WidgetInput()
            {
                ExibirTitulo = true,
                IsActive = true,
                MenuId = 1,
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                SiteId = 1,
                TemplateId = 1,
                Tipo = TipoDeWidgetEnum.CmsMenu,
                Titulo = "Teste Menu"
            });
        }

        [Fact]
        public async Task Should_Get_Widget_Menu()
        {
            #region Arrange
            await CreateWidgetMenuAndTestAsync();
            #endregion

            #region Action
            var wigetInput = await base.WidgetFrontEndAppService.GetById(new IdInput(1));
            #endregion

            #region Assert
            wigetInput.Menu.ShouldNotBeNull();
            #endregion
        }

        [Fact]
        public async Task Should_Get_Widget_Html()
        {
            #region Arrange
            await CreateWidgetHTMLAndTestAsync();
            #endregion

            #region Action
            var wigetInput = await base.WidgetFrontEndAppService.GetById(new IdInput(1));
            #endregion

            #region Assert
            wigetInput.Conteudo.ShouldNotBe(string.Empty);
            #endregion
        }

        [Fact]
        public async Task Should_Get_List_Html()
        {
            #region Arrange
            await CreateWidgetMenuAndTestAsync();
            #endregion

            #region Action
            var widgets = await base.WidgetFrontEndAppService.GetList(new Dto.CMSFrontEnd.Geral.Widget.GetWidgetFrontInput { SiteId = 1, TemplateId = 1, Posicao = "sidebar" });
            #endregion

            #region Assert
            widgets.ShouldNotBe(null);
            widgets.Count().ShouldBeGreaterThan(0);
            foreach (var widget in widgets)
            {
                widget.Menu.ShouldNotBeNull();
            }
            #endregion
        }
    }
}