﻿using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Tests.Application.CMSFrontEnd.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMSFrontEnd.Geral
{
    public class SiteAppService_Tests : SiteAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }
        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }
        private async Task CreateSiteAndTestAsync(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner,
                                                  string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            await _siteAppService.Save(
                new SiteInput
                {
                    Nome = nome,
                    Descricao = descricao,
                    Hosts = new List<HostInput>
                            {
                                new HostInput
                                {
                                    IsActive = true,
                                    IsPrincipal = true,
                                    Url = host
                                }
                            },
                    MetaKeywords = metaKeywords,
                    MetaTags = metaTags,
                    Owner = owner,
                    TemplateDefaultId = 1,
                    TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                    TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                    TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                    TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault

                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Nome == nome);
                createdSite.ShouldNotBe(null);
            });
        }
        [Fact]
        public async Task Should_Get_Site_By_Host()
        {
            #region Arrange
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("EzControl", "", "ezcontrol.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            #endregion

            #region Action
            var siteDto = await _siteFrontEndAppService.GetByHost(new Dto.CMSFrontEnd.Geral.Site.GetByHostFrontDto() { Host = "ezcontrol.com.br" });
            #endregion


            #region Assert
            siteDto.ShouldNotBe(null);
            #endregion
        }
        [Fact]
        public async Task Should_Not_Get_Site_By_Host()
        {
            #region Arrange
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("EzControl", "", "ezcontrol.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            #endregion
            #region Action
            var siteDto = await _siteFrontEndAppService.GetByHost(new Dto.CMSFrontEnd.Geral.Site.GetByHostFrontDto() { Host = "ezcontrolfrontend.com.br" });
            #endregion
            #region Assert
            siteDto.ShouldBeNull();
            #endregion
        }
    }
}