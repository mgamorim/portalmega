﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMSFrontEnd.Geral
{
    public class MenuAppService_Tests : MenuAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }
        private async Task CreateSiteAndTestAsync(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner,
                                                  string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            await _siteAppService.Save(
                new SiteInput()
                {
                    Nome = nome,
                    Descricao = descricao,
                    Hosts = new List<HostInput>
                            {
                                new HostInput
                                {
                                    IsActive = true,
                                    IsPrincipal = true,
                                    Url = "ezcontrol.com"
                                }
                            },
                    MetaKeywords = metaKeywords,
                    MetaTags = metaTags,
                    Owner = owner,
                    TemplateDefaultId = 1,
                    TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                    TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                    TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                    TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Nome == nome);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }

        private async Task CreateMenuAndTestAsync()
        {
            await base._menuAppService.Save(
            new MenuInput()
            {
                IsActive = true,
                SiteId = 1,
                Nome = "Lateral"
            });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Menus.FirstOrDefaultAsync(i => i.Site.Id == 1);
                createdSite.ShouldNotBe(null);
            });
        }
        private async Task CreateItensDeMenuAndTestAsync()
        {
            await _itemDeMenuAppService.Save(
            new ItemDeMenuInput()
            {
                IsActive = true,
                MenuId = 1,
                DescricaoDoTitulo = "Google",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Titulo = "Google",
                Url = "http://www.google.com"
            });
            await _itemDeMenuAppService.Save(
            new ItemDeMenuInput()
            {
                IsActive = true,
                MenuId = 1,
                ItemMenuId = 1,
                DescricaoDoTitulo = "Amazon",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Titulo = "Amazon",
                Url = "http://www.amazon.com"
            });
            await UsingDbContext(async context =>
            {
                var createdItemDeMenu = await context.ItensDeMenu.FirstOrDefaultAsync(i => i.Menu.Id == 1);
                createdItemDeMenu.ShouldNotBe(null);
            });
        }
        [Fact]
        public async Task Should_Return_MenuHTML()
        {
            #region Arrange

            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("Hinode", "", "www.hinode.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            await CreateMenuAndTestAsync();
            await CreateItensDeMenuAndTestAsync();
            #endregion

            await RunAsyncInTransaction(async (uow) =>
             {
                 #region Action
                 var menu = await _menuService.GetById(1);
                 var htmlMenu = _menuService.GetHtml(menu);
                 #endregion

                 #region Assert
                 htmlMenu.ShouldNotBe(string.Empty);
                 #endregion
             });
        }
    }
}