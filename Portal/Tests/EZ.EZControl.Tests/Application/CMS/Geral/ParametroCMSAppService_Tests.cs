﻿using EZ.EZControl.Dto.CMS.Geral.Parametro;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class ParametroCMSAppService_Tests : ParametroCMSAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Parametro()
        {
            await CreateParametrosAsync();
        }

        private async Task CreateParametrosAsync()
        {
            base.CreateTestParametros();

            await ParametroCMSAppService.Save(new ParametroCMSInput
            {
                AlturaMaximaPx = 100,
                ExtensoesDocumento = "doc;pdf",
                ExtensoesImagem = "png;jpeg",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1
            });

            await UsingDbContext(async context =>
            {
                var createdParametro = await context.ParametrosCMS.FirstOrDefaultAsync();
                createdParametro.ShouldNotBe(null);
            });
        }
    }
}