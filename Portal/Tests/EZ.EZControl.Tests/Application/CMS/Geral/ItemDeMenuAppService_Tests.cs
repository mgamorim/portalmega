﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class ItemDeMenuAppService_Tests : ItemDeMenuAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateSiteAndTestAsync(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner,
                                                  string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            await _siteAppService.Save(
                new SiteInput
                {
                    Nome = nome,
                    Descricao = descricao,
                    Hosts = new List<HostInput>
                            {
                                new HostInput
                                {
                                    IsActive = true,
                                    IsPrincipal = true,
                                    Url = "ezcontrol.com"
                                }
                            },
                    MetaKeywords = metaKeywords,
                    MetaTags = metaTags,
                    Owner = owner,
                    TemplateDefaultId = 1,
                    TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                    TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                    TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                    TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Nome == nome);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateMenuAndTestAsync()
        {
            await base._menuAppService.Save(
            new MenuInput
            {
                IsActive = true,
                SiteId = 1,
                Nome = "Lateral"
            });
            await UsingDbContext(async context =>
            {
                var createdMenu = await context.Menus.FirstOrDefaultAsync(i => i.Site.Id == 1);
                createdMenu.ShouldNotBe(null);
            });
        }

        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }

        private async Task CreateItemDeMenuAndTestAsync()
        {
            await base._itemDeMenuAppService.Save(
            new ItemDeMenuInput
            {
                IsActive = true,
                Titulo = "Item de Teste",
                DescricaoDoTitulo = "Esse é o Item de Teste",
                Url = "www.boulevard.com.br/itemdeteste",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                MenuId = 1
            });
            await UsingDbContext(async context =>
            {
                var createdItemDeMenu = await context.ItensDeMenu.FirstOrDefaultAsync(i => i.Menu.Id == 1);
                createdItemDeMenu.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Item_De_Menu()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("Hinode", "", "http://www.hinode.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            await CreateMenuAndTestAsync();
            await CreateItemDeMenuAndTestAsync();
        }

        [Fact]
        public async Task Should_Not_Create_Item_De_Menu_With_Duplicate_Titulo()
        {
            base.CreateItemDeMenuTest();
            await _itemDeMenuAppService.Save(
                new ItemDeMenuInput
                {
                    IsActive = true,
                    Titulo = "Submenu",
                    DescricaoDoTitulo = "Esse é o Item de Teste",
                    Url = "www.boulevard.com.br/itemdeteste",
                    TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                    MenuId = 1
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Delete_Item_De_Menu()
        {
            base.CreateItemDeMenuTest();
            await _itemDeMenuAppService.Delete(new IdInput(1));
            await _itemDeMenuAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}