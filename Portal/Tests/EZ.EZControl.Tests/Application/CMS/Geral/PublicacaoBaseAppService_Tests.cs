﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Pagina;
using EZ.EZControl.Dto.CMS.Geral.Post;
using EZ.EZControl.Dto.CMS.Geral.Publicacao;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class PublicacaoBaseAppService_Tests : PublicacaoBaseAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum tipo, string titulo, string conteudo,
            Site site, string slug)
        {
            if (tipo == TipoDePublicacaoEnum.Post)
            {
                await PostAppService.Save(
                    new PostInput()
                    {
                        Conteudo = conteudo,
                        Titulo = titulo,
                        SiteId = site.Id,
                        TipoDePublicacao = tipo,
                        Slug = slug,
                        StatusDaPublicacaoEnum = StatusDaPublicacaoEnum.Publicada,
                        Categorias = new List<CategoriaInput>
                        {
                            new CategoriaInput
                            {
                                IsActive = true,
                                Descricao = "Categoria de Teste",
                                Nome = "Teste",
                                Slug = "categoria_de_teste",
                                SiteId = site.Id
                            }
                        },
                        Tags = new List<TagInput>
                        {
                            new TagInput
                            {
                                IsActive = true,
                                Nome = "TagTeste",
                                Descricao = "Tag de Teste",
                                Slug = "tag_de_teste"
                            }
                        },
                        IsActive = true
                    });
                await UsingDbContext(async context =>
                {
                    var createdPost = await context.Posts.FirstOrDefaultAsync(i => i.Titulo == titulo);
                    createdPost.ShouldNotBe(null);
                });
            }
            else
            {
                await PaginaAppService.Save(
                    new PaginaInput()
                    {
                        Conteudo = conteudo,
                        Titulo = titulo,
                        SiteId = site.Id,
                        TipoDePublicacao = tipo,
                        Slug = slug,
                        StatusDaPublicacaoEnum = StatusDaPublicacaoEnum.Publicada
                    });
                await UsingDbContext(async context =>
                {
                    var createdPagina = await context.Paginas.FirstOrDefaultAsync(i => i.Titulo == titulo);
                    createdPagina.ShouldNotBe(null);
                });
            }
        }

        [Fact]
        public async Task Should_Create_Publicacao_Pagina()
        {
            CreateTestsSite();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await
                CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity,
                    "pagina_de_teste");
        }

        [Fact]
        public async Task Should_Create_Publicacao_Post()
        {
            CreateTestsSite();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await
                CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity,
                    "post_de_teste");
        }

        [Fact]
        public async Task Should_Delete_Publicacao()
        {
            base.CreateTestsPosts();
            var publicacao = await PostAppService.GetById(new IdInput(1));
            await PostAppService.Delete(new IdInput(publicacao.Id));
            await PostAppService.GetById(new IdInput(1)).ShouldThrowAsync<Exception>();
        }

        [Fact]
        public async Task Should_Search_Pagina_E_Post()
        {
            CreateTestsSiteHabilitaPesquisaPaginaEPost();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await
                CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity,
                    "pagina_de_teste");
            await
                CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity,
                    "post_de_teste");
            var resultadoPesquisa = await PublicacaoBaseAppService.Pesquisar(new PesquisaPublicacaoDto()
            {
                IncluiPaginas = true,
                IncluiPosts = true,
                MaxResultCount = 10,
                SiteId = 1,
                SkipCount = 0,
                Sorting = "TermoPesquisa",
                TermpoPesquisa = "Teste"
            });
            resultadoPesquisa.Items.Count.ShouldBe(2);
        }

        [Fact]
        public async Task Should_Search_Pagina()
        {
            CreateTestsSiteHabilitaPesquisaPaginaEPost();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity, "pagina_de_teste");
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity, "post_de_teste");
            var resultadoPesquisa = await PublicacaoBaseAppService.Pesquisar(new PesquisaPublicacaoDto()
            {
                IncluiPaginas = true,
                IncluiPosts = false,
                MaxResultCount = 10,
                SiteId = 1,
                SkipCount = 0,
                Sorting = "TermoPesquisa",
                TermpoPesquisa = "Teste"
            });
            resultadoPesquisa.Items.Count.ShouldBe(1);
        }

        [Fact]
        public async Task Should_Search_Post()
        {
            CreateTestsSiteHabilitaPesquisaPaginaEPost();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity, "pagina_de_teste");
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity, "post_de_teste");
            var resultadoPesquisa = await PublicacaoBaseAppService.Pesquisar(new PesquisaPublicacaoDto()
            {
                IncluiPaginas = false,
                IncluiPosts = true,
                MaxResultCount = 10,
                SiteId = 1,
                SkipCount = 0,
                Sorting = "TermoPesquisa",
                TermpoPesquisa = "Teste"
            });
            resultadoPesquisa.Items.Count.ShouldBe(1);
        }

        [Fact]
        public async Task Should_Search_Nothing()
        {
            CreateTestsSiteHabilitaPesquisaPaginaEPost();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity, "pagina_de_teste");
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity, "post_de_teste");
            var resultadoPesquisa = await PublicacaoBaseAppService.Pesquisar(new PesquisaPublicacaoDto()
            {
                IncluiPaginas = false,
                IncluiPosts = false,
                MaxResultCount = 10,
                SiteId = 1,
                SkipCount = 0,
                Sorting = "TermoPesquisa",
                TermpoPesquisa = "Teste"
            });
            resultadoPesquisa.Items.Count.ShouldBe(0);
        }

        [Fact]
        public async Task Should_Search_Nothing_Site_Disabled()
        {
            CreateTestsSiteDesabilitaPesquisas();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity, "pagina_de_teste");
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity, "post_de_teste");
            var resultadoPesquisa = await PublicacaoBaseAppService.Pesquisar(new PesquisaPublicacaoDto()
            {
                IncluiPaginas = true,
                IncluiPosts = true,
                MaxResultCount = 10,
                SiteId = 1,
                SkipCount = 0,
                Sorting = "TermoPesquisa",
                TermpoPesquisa = "Teste"
            });
            resultadoPesquisa.Items.Count.ShouldBe(0);
        }
    }
}