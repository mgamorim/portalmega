﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class TemplateAppService_Tests : TemplateAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Get_Posicoes()
        {
            #region Arrange

            base.CreateTestTemplates();

            #endregion

            #region Action

            var posicoes = await base.TemplateAppService.GetListPosicoes(new IdInput(1));

            #endregion

            #region Assert

            posicoes.Items.Count.ShouldNotBe(0);

            #endregion
        }

        [Fact]
        public async Task Should_Get_TiposDeWidgetSuportados()
        {
            #region Arrange

            base.CreateTestTemplates();

            #endregion

            #region Action

            var tiposDeWidget = await base.TemplateAppService.GetTiposDeWidgetSuportados(new IdInput(1));

            #endregion

            #region Assert

            tiposDeWidget.Items.Count.ShouldNotBe(0);

            #endregion
        }

        [Fact]
        public async Task Should_Get_TiposDePaginasSuportadas()
        {
            #region Arrange
            base.CreateTestTemplates();
            #endregion

            #region Action
            var tiposDePaginas = await base.TemplateAppService.GetTiposDePaginasSuportadas(new IdInput(1));
            #endregion

            #region Assert
            tiposDePaginas.Items.Count.ShouldNotBe(0);
            #endregion
        }

        [Fact]
        public async Task Should_Get_TipoDePaginaParaPaginaDefault()
        {
            #region Arrange
            base.CreateTestTemplates();
            #endregion

            #region Action
            var template = await base.TemplateAppService.GetById(new IdInput(1));
            #endregion

            #region Assert
            template.TipoDePaginaParaPaginaDefault.ShouldNotBeNullOrEmpty();
            #endregion
        }

        [Fact]
        public async Task Should_Get_TipoDePaginaParaPostDefault()
        {
            #region Arrange
            base.CreateTestTemplates();
            #endregion

            #region Action
            var template = await base.TemplateAppService.GetById(new IdInput(1));
            #endregion

            #region Assert
            template.TipoDePaginaParaPostDefault.ShouldNotBeNullOrEmpty();
            #endregion
        }

        [Fact]
        public async Task Should_Get_TipoDePaginaParaCategoriaDefault()
        {
            #region Arrange
            base.CreateTestTemplates();
            #endregion

            #region Action
            var template = await base.TemplateAppService.GetById(new IdInput(1));
            #endregion

            #region Assert
            template.TipoDePaginaParaCategoriaDefault.ShouldNotBeNullOrEmpty();
            #endregion
        }

        [Fact]
        public async Task Should_Get_TipoDePaginaParaPaginaInicialDefault()
        {
            #region Arrange
            base.CreateTestTemplates();
            #endregion

            #region Action
            var template = await base.TemplateAppService.GetById(new IdInput(1));
            #endregion

            #region Assert
            template.TipoDePaginaParaPaginaInicialDefault.ShouldNotBeNullOrEmpty();
            #endregion
        }

    }
}
