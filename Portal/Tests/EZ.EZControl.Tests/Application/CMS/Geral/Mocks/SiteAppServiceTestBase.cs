﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class SiteAppServiceTestBase : AppTestBase
    {
        protected readonly ISiteAppService SiteAppService;
        private readonly Template TemplateDefault;
        private readonly Template TemplateTeste;

        public SiteAppServiceTestBase()
        {
            SiteAppService = Resolve<ISiteAppService>();
            TemplateDefault = CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                    "sidebar, top-menu, rodape",
                                                    "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                    "Home, Contato, QuemSomos, Login",
                                                    "Login",
                                                    "Home",
                                                    "QuemSomos",
                                                    "Contato");

            TemplateTeste = CreateTemplate("EzControl Teste", "Json EzControl Teste", "EzSoft", "EzSoft", "2.0", "EZ",
                                                    "sidebar, top-menu, rodape",
                                                    "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                    "Home, Contato, QuemSomos, Login",
                                                    "Login",
                                                    "Home",
                                                    "QuemSomos",
                                                    "Contato");
        }

        protected void CreateTestSites()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", "www.hinode.com.br", "", "", "", TemplateDefault, "Login", "QuemSomos", "Contato", "Home"));
                context.Sites.Add(CreateSiteEntity("Boulevard", "", "www.boulevard.com.br", "", "", "", TemplateTeste, "Login", "QuemSomos", "Contato", "Home"));
            });
        }

        protected void CreateTestTemplates()
        {
            UsingDbContext(context =>
            {
                context.Templates.Add(CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"));
            });
        }

        private Site CreateSiteEntity(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner, Template template,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            var site = new Site
            {
                Nome = nome,
                Descricao = descricao,
                MetaKeywords = metaKeywords,
                MetaTags = metaTags,
                Owner = owner,
                TemplateDefault = template,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };
            site.Hosts.Add(new Host
            {
                IsPrincipal = true,
                Url = host,
                EmpresaId = EmpresaIdDefaultTest
            });
            return site;
        }

        private Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                      string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                      string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}