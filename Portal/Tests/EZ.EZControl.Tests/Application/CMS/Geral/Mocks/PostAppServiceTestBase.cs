﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using System.Collections.Generic;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class PostAppServiceTestBase : AppTestBase
    {
        protected readonly IPostAppService PostAppService;
        protected readonly ISiteAppService SiteAppService;
        private readonly Template TemplateDefault;
        private readonly Template TemplateTeste;

        public PostAppServiceTestBase()
        {
            PostAppService = Resolve<IPostAppService>();
            SiteAppService = Resolve<ISiteAppService>();

            TemplateDefault = CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                    "sidebar, top-menu, rodape",
                                                    "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                    "Home, Contato, QuemSomos, Login",
                                                    "Login",
                                                    "Home",
                                                    "QuemSomos",
                                                    "Contato");

            TemplateTeste = CreateTemplate("EzControl Teste", "Json EzControl Teste", "EzSoft", "EzSoft", "2.0", "EZ",
                                                    "sidebar, top-menu, rodape",
                                                    "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                    "Home, Contato, QuemSomos, Login",
                                                    "Login",
                                                    "Home",
                                                    "QuemSomos",
                                                    "Contato");
        }

        protected void CreateTestsPosts()
        {
            UsingDbContext(context =>
            {
                context.Posts.Add(CreatePostEntity(TipoDePublicacaoEnum.Pagina, "Página de Teste", CreateSiteEntity("Hinode", "", TemplateDefault, "Login", "QuemSomos", "Contato", "Home"), "pagina_de_teste"));
                context.Posts.Add(CreatePostEntity(TipoDePublicacaoEnum.Post, "Post de Teste", CreateSiteEntity("LeBel", "", TemplateTeste, "Login", "QuemSomos", "Contato", "Home"), "post_de_teste"));
                context.Publicacoes.Add(CreatePostEntity(TipoDePublicacaoEnum.Post, "Post de Teste Publicação", CreateSiteEntity("LeBel Publicação", "", TemplateTeste, "Login", "QuemSomos", "Contato", "Home"), "post_de_teste"));
            });
        }

        protected void CreateTestsSite()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", TemplateDefault, "Login", "QuemSomos", "Contato", "Home"));
                context.Sites.Add(CreateSiteEntity("LeBel", "", TemplateTeste, "Login", "QuemSomos",
                    "Contato", "Home"));
            });
        }

        private Post CreatePostEntity(TipoDePublicacaoEnum tipo, string titulo, Site site, string slug)
        {
            var post = new Post
            {
                Site = site,
                TipoDePublicacao = tipo,
                Titulo = titulo,
                Slug = slug,
                Historico = new List<HistoricoPublicacao> { new HistoricoPublicacao()
                    {
                        EmpresaId = EmpresaIdDefaultTest
                    }
                },
                EmpresaId = EmpresaIdDefaultTest
            };
            return post;
        }

        private Site CreateSiteEntity(string nome, string descricao, Template template,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            return new Site
            {
                Nome = nome,
                Descricao = descricao,
                TemplateDefault = template,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                     string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                     string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                     string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}