﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class TemplateAppServiceTestBase : AppTestBase
    {
        protected readonly ITemplateAppService TemplateAppService;

        public TemplateAppServiceTestBase()
        {
            this.TemplateAppService = Resolve<ITemplateAppService>();
        }

        protected void CreateTestTemplates()
        {
            UsingDbContext(context =>
            {
                context.Templates.Add(CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"));
            });
        }

        public Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                       string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                       string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                       string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}
