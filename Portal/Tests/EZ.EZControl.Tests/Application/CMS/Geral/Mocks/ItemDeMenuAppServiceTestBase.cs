﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class ItemDeMenuAppServiceTestBase : AppTestBase
    {
        protected readonly IItemDeMenuAppService _itemDeMenuAppService;
        protected readonly IMenuAppService _menuAppService;
        protected readonly ISiteAppService _siteAppService;

        public ItemDeMenuAppServiceTestBase()
        {
            this._menuAppService = Resolve<IMenuAppService>();
            this._siteAppService = Resolve<ISiteAppService>();
            this._itemDeMenuAppService = Resolve<IItemDeMenuAppService>();
        }

        protected void CreateItemDeMenuTest()
        {
            UsingDbContext(context =>
            {
                context.ItensDeMenu.Add(CreateItemDeMenuEntity());
            });
        }

        protected void CreateTestTemplates()
        {
            UsingDbContext(context =>
            {
                context.Templates.Add(CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"));
            });
        }

        private ItemDeMenu CreateItemDeMenuEntity()
        {
            return new ItemDeMenu
            {
                IsActive = true,
                Titulo = "Submenu",
                DescricaoDoTitulo = "Esse Submenu é o de Contato",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Url = "www.hinode.com.br/submenu",
                Menu = CreateMenuEntity(),
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Menu CreateMenuEntity()
        {
            return new Menu
            {
                IsActive = true,
                Nome = "Inicial",
                Site = CreateSiteEntity("Hinode", "", "", "", "",
                       CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"),
                       "Login", "QuemSomos", "Contato", "Home"),
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Site CreateSiteEntity(string nome, string descricao, string metaKeywords, string metaTags, string owner, Template template,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            return new Site
            {
                Nome = nome,
                Descricao = descricao,
                TemplateDefault = template,
                MetaKeywords = metaKeywords,
                MetaTags = metaTags,
                Owner = owner,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                     string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                     string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                     string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}