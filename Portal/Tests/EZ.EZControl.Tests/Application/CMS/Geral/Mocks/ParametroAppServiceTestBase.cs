﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public abstract class ParametroCMSAppServiceTestBase : AppTestBase
    {
        protected readonly IParametroCMSAppService ParametroCMSAppService;

        protected ParametroCMSAppServiceTestBase()
        {
            ParametroCMSAppService = Resolve<IParametroCMSAppService>();
        }

        protected void CreateTestParametros()
        {
            UsingDbContext(context =>
            {
                context.ParametrosCMS.Add(CreateParametroEntity());
            });
        }

        private ParametroCMS CreateParametroEntity()
        {
            return new ParametroCMS
            {
                AlturaMaximaPx = 100,
                ExtensoesImagem = "jpg;png;gif",
                ExtensoesDocumento = "doc;docx;pdf;xlsx",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}