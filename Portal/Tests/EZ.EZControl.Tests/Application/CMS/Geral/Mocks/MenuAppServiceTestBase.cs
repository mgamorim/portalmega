﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class MenuAppServiceTestBase : AppTestBase
    {
        protected readonly Services.CMS.Interfaces.IMenuAppService _menuAppService;
        protected readonly ISiteAppService _siteAppService;
        protected readonly IItemDeMenuAppService _itemDeMenuAppService;
        protected readonly Services.CMSFrontEnd.Interfaces.IMenuService _menuService;
        public MenuAppServiceTestBase()
        {
            _menuAppService = Resolve<IMenuAppService>();
            _siteAppService = Resolve<ISiteAppService>();
            _itemDeMenuAppService = Resolve<IItemDeMenuAppService>();
            _menuService = Resolve<Services.CMSFrontEnd.Interfaces.IMenuService>();
        }

        protected void CreateMenuTest()
        {
            UsingDbContext(context =>
            {
                context.Menus.Add(CreateMenuEntity());
            });
        }

        protected void CreateTestTemplates()
        {
            UsingDbContext(context =>
            {
                context.Templates.Add(CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"));
            });
        }
        protected void CreateEstruturaMenuTest()
        {
            UsingDbContext(context =>
            {
                context.Menus.Add(CreateMenuEntity());
                var menu = context.Menus.Find(1);

                context.ItensDeMenu.Add(CreateItemDeMenuEntity(menu));
                var item = context.ItensDeMenu.Find(1);

                context.ItensDeMenu.Add(CreateSubItemDeMenuEntity(item));
            });
        }

        private ItemDeMenu CreateItemDeMenuEntity(Menu menu)
        {
            return new ItemDeMenu
            {
                Menu = menu,
                DescricaoDoTitulo = "Google",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Titulo = "Google",
                Url = "http://www.google.com",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ItemDeMenu CreateSubItemDeMenuEntity(ItemDeMenu itemDeMenu)
        {
            return new ItemDeMenu
            {
                ItemMenu = itemDeMenu,
                DescricaoDoTitulo = "Amazon",
                TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                Titulo = "Amazon",
                Url = "http://www.amazon.com",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        public Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                    string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                    string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                    string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }

        private Menu CreateMenuEntity()
        {
            return new Menu
            {
                IsActive = true,
                Nome = "Inicial",
                Site = CreateSiteEntity("Hinode", "", "http://www.hinode.com.br", "", "", "",
                       CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"),
                       "Login", "QuemSomos", "Contato", "Home"),
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Site CreateSiteEntity(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner, Template template,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            var site = new Site
            {
                Nome = nome,
                Descricao = descricao,
                TemplateDefault = template,
                MetaKeywords = metaKeywords,
                MetaTags = metaTags,
                Owner = owner,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };
            site.Hosts.Add(new Host
            {
                IsPrincipal = true,
                Url = host,
                EmpresaId = EmpresaIdDefaultTest
            });
            return site;
        }
    }
}