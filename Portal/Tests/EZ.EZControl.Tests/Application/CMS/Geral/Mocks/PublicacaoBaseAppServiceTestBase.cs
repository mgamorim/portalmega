﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;
using System.Collections.Generic;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class PublicacaoBaseAppServiceTestBase : AppTestBase
    {
        protected readonly IPostAppService PostAppService;
        protected readonly IPaginaAppService PaginaAppService;
        protected readonly ISiteAppService SiteAppService;
        protected readonly IPublicacaoBaseAppService PublicacaoBaseAppService;
        private readonly Template TemplateDefault;
        private readonly Template TemplateTeste;

        public PublicacaoBaseAppServiceTestBase()
        {
            PostAppService = Resolve<IPostAppService>();
            PaginaAppService = Resolve<IPaginaAppService>();
            SiteAppService = Resolve<ISiteAppService>();
            PublicacaoBaseAppService = Resolve<IPublicacaoBaseAppService>();

            TemplateDefault = CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                    "sidebar, top-menu, rodape",
                                                    "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                    "Home, Contato, QuemSomos, Login",
                                                    "Login",
                                                    "Home",
                                                    "QuemSomos",
                                                    "Contato");

            TemplateTeste = CreateTemplate("EzControl Teste", "Json EzControl Teste", "EzSoft", "EzSoft", "2.0", "EZ",
                                                    "sidebar, top-menu, rodape",
                                                    "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                    "Home, Contato, QuemSomos, Login",
                                                    "Login",
                                                    "Home",
                                                    "QuemSomos",
                                                    "Contato");
        }

        protected void CreateTestsPublicacoes()
        {
            UsingDbContext(context =>
            {
                context.Paginas.Add(CreatePaginaEntity(TipoDePublicacaoEnum.Pagina, "Página de Teste", CreateSiteEntity("Hinode", "", "http://www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home"), "pagina_de_teste"));
                context.Posts.Add(CreatePostEntity(TipoDePublicacaoEnum.Post, "Post de Teste", CreateSiteEntity("LeBel", "", "http://www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home"), "post_de_teste"));
            });
        }

        protected void CreateTestsPosts()
        {
            UsingDbContext(context =>
            {
                context.Posts.Add(CreatePostEntity(TipoDePublicacaoEnum.Post, "Post de Teste 1", CreateSiteEntity("Hinode", "", "http://www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home"), "post_de_teste_1"));
                context.Posts.Add(CreatePostEntity(TipoDePublicacaoEnum.Post, "Post de Teste 2", CreateSiteEntity("LeBel", "", "http://www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home"), "post_de_teste_2"));
            });
        }

        protected void CreateTestsSite()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", "www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home"));
                context.Sites.Add(CreateSiteEntity("LeBel", "", "www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home"));
            });
        }

        protected void CreateTestsSiteHabilitaPesquisaPaginaEPost()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", "www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home", true, true));
                context.Sites.Add(CreateSiteEntity("LeBel", "", "www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home", true, true));
            });
        }

        protected void CreateTestsSiteHabilitaPesquisaPagina()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", "www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home", false, true));
                context.Sites.Add(CreateSiteEntity("LeBel", "", "www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home", false, true));
            });
        }

        protected void CreateTestsSiteHabilitaPesquisaPost()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", "www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home", true, false));
                context.Sites.Add(CreateSiteEntity("LeBel", "", "www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home", true, false));
            });
        }

        protected void CreateTestsSiteDesabilitaPesquisas()
        {
            UsingDbContext(context =>
            {
                context.Sites.Add(CreateSiteEntity("Hinode", "", "www.hinode.com.br", TemplateDefault, "Login", "QuemSomos", "Contato", "Home", false, false));
                context.Sites.Add(CreateSiteEntity("LeBel", "", "www.lebel.com.br", TemplateTeste, "Login", "QuemSomos", "Contato", "Home", false, false));
            });
        }

        private Post CreatePostEntity(TipoDePublicacaoEnum tipo, string titulo, Site site, string slug)
        {
            var post = new Post
            {
                Site = site,
                TipoDePublicacao = tipo,
                Titulo = titulo,
                Slug = slug,
                Historico = new List<HistoricoPublicacao> { new HistoricoPublicacao() { EmpresaId = EmpresaIdDefaultTest } },
                EmpresaId = EmpresaIdDefaultTest
            };
            return post;
        }

        private Pagina CreatePaginaEntity(TipoDePublicacaoEnum tipo, string titulo, Site site, string slug)
        {
            var pagina = new Pagina
            {
                Site = site,
                TipoDePublicacao = tipo,
                Titulo = titulo,
                Slug = slug,
                Historico = new List<HistoricoPublicacao> { new HistoricoPublicacao() { EmpresaId = EmpresaIdDefaultTest } },
                EmpresaId = EmpresaIdDefaultTest
            };
            return pagina;
        }

        private Site CreateSiteEntity(string nome, string descricao, string host, Template template,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault,
                                      bool habilitarPesquisaPost = true, bool habilitarPesquisaPagina = true)
        {

            var site = new Site
            {
                Nome = nome,
                Descricao = descricao,
                TemplateDefault = template,
                HabilitarPesquisaPost = habilitarPesquisaPost,
                HabilitarPesquisaPagina = habilitarPesquisaPagina,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };
            site.Hosts.Add(new Host
            {
                IsPrincipal = true,
                Url = host,
                EmpresaId = EmpresaIdDefaultTest
            });
            return site;
        }

        private Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                      string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                      string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}