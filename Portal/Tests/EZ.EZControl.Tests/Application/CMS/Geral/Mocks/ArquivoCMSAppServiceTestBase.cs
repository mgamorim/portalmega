﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Services.CMS.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class ArquivoCMSAppServiceTestBase : AppTestBase
    {
        protected readonly IArquivoCMSAppService ArquivoAppService;

        public ArquivoCMSAppServiceTestBase()
        {
            this.ArquivoAppService = Resolve<IArquivoCMSAppService>();
        }

        protected void CreateTestArquivos()
        {
            UsingDbContext(context =>
            {
                context.ArquivosCMS.Add(CreateArquivo("Recibo", TipoDeArquivoEnum.PDF, new Byte[0]));
                context.ArquivosCMS.Add(CreateArquivo("Despesa", TipoDeArquivoEnum.JPG, new Byte[1]));
                context.ArquivosCMS.Add(CreateArquivo("Planilha Contábil", TipoDeArquivoEnum.XLS, new Byte[2]));
            });
        }

        private ArquivoCMS CreateArquivo(String nome, TipoDeArquivoEnum tipoDeArquivo, Byte[] conteudo)
        {
            return new ArquivoCMS
            {
                IsActive = true,
                Nome = nome,
                TipoDeArquivoFixo = tipoDeArquivo,
                Conteudo = conteudo,
                Path = @"C:\Uploads\CMS",
                Token = Guid.NewGuid().ToString("N")
                //EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}