﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using System;
using System.Collections.Generic;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class TagAppServiceTestBase : AppTestBase
    {
        protected readonly ITagAppService TagAppService;

        public TagAppServiceTestBase()
        {
            this.TagAppService = Resolve<ITagAppService>();
        }

        protected void CreateTestTags()
        {
            UsingDbContext(context =>
            {
                context.Tags.Add(CreateTagEntity("Marketing", "iMarketing", "_marketing"));
                context.Tags.Add(CreateTagEntity("Blog", "iBlog", "_blog"));
            });
        }

        private PublicacaoBase CreatePublicacaoEntity(TipoDePublicacaoEnum tipo, string titulo, Site site)
        {
            var publicacao = new PublicacaoBase
            {
                Site = site,
                TipoDePublicacao = tipo,
                Titulo = titulo,
                Historico = new List<HistoricoPublicacao> {new HistoricoPublicacao()
                    {
                        EmpresaId = EmpresaIdDefaultTest
                    }
                },
                EmpresaId = EmpresaIdDefaultTest
            };
            return publicacao;
        }

        private Tag CreateTagEntity(String descricao, String nome, String slug)
        {
            return new Tag
            {
                IsActive = true,
                Nome = nome,
                Slug = slug,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}