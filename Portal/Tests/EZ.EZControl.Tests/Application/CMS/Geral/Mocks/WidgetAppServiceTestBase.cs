﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.CMS.Geral.Interfaces;
using EZ.EZControl.Services.CMS.Interfaces;

namespace EZ.EZControl.Tests.Application.CMS.Geral.Mocks
{
    public class WidgetAppServiceTestBase : AppTestBase
    {
        protected readonly IWidgetAppService WidgetAppService;
        protected readonly ISiteAppService SiteAppService;
        protected readonly IMenuAppService MenuAppService;
        protected readonly ITemplateAppService TemplateAppService;
        public WidgetAppServiceTestBase()
        {
            WidgetAppService = Resolve<IWidgetAppService>();
            SiteAppService = Resolve<ISiteAppService>();
            MenuAppService = Resolve<IMenuAppService>();
            TemplateAppService = Resolve<ITemplateAppService>();
        }

        protected void CreateTestWidgets()
        {
            UsingDbContext(context =>
            {
                context.WidgetsHTML.Add(CreateWidgetHTMLEntity(CreateSiteEntity(), CreateTemplateEntity()));
                context.WidgetsMenu.Add(CreateWidgetMenuEntity(CreateSiteEntity(), CreateTemplateEntity()));
            });
        }

        private Site CreateSiteEntity()
        {
            return new Site()
            {
                Nome = "EZControl",
                //Host = "www.ezcontrol.com.br",
                Descricao = string.Empty,
                Owner = "EZSoft",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Template CreateTemplateEntity()
        {
            return new Template
            {
                IsActive = true,
                Nome = "EzControl",
                Descricao = "Json EzControl",
                Autor = "EzSoft",
                AutorUrl = "EzSoft",
                Versao = "1.0",
                NomeDoArquivo = "EZ",
                Posicoes = "sidebar, top-menu, rodape",
                EmpresaId = EmpresaIdDefaultTest
            };

        }

        private Menu CreateMenuEntity(Site site)
        {
            return new Menu
            {
                IsActive = true,
                Nome = "Principal",
                Site = site,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private WidgetHTML CreateWidgetHTMLEntity(Site site, Template template)
        {
            return new WidgetHTML()
            {
                Conteudo = "<p>Teste</p>",
                ExibirTitulo = true,
                IsActive = true,
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                Site = site,
                Template = template,
                Tipo = TipoDeWidgetEnum.CmsHtml,
                Titulo = "Teste HTML",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private WidgetMenu CreateWidgetMenuEntity(Site site, Template template)
        {
            return new WidgetMenu()
            {
                ExibirTitulo = true,
                IsActive = true,
                Menu = CreateMenuEntity(site),
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                Site = site,
                Template = template,
                Tipo = TipoDeWidgetEnum.CmsMenu,
                Titulo = "Teste Menu",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        protected void CreateTestTemplates()
        {
            UsingDbContext(context =>
            {
                context.Templates.Add(CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato"));
            });
        }

        public Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                       string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                       string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                       string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}