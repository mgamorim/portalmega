﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Dto.CMS.Geral.Widget;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class WidgetAppService_Tests : WidgetAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateSiteAndTestAsync()
        {
            await SiteAppService.Save(
                new SiteInput
                {
                    Nome = "EZControl",
                    TemplateDefaultId = 1,
                    Descricao = string.Empty,
                    TipoDePaginaParaPaginaDefault = "Login",
                    TipoDePaginaParaCategoriaDefault = "QuemSomos",
                    TipoDePaginaParaPaginaInicialDefault = "Contato",
                    TipoDePaginaParaPostDefault = "Home",
                    Hosts = new List<HostInput>
                    {
                        new HostInput
                        {
                            IsActive = true,
                            IsPrincipal = true,
                            Url = "ezcontrol.com"
                        }
                    },
                    MetaKeywords = string.Empty,
                    MetaTags = string.Empty,
                    Owner = string.Empty
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Id == 1);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateMenuAndTestAsync()
        {
            await base.MenuAppService.Save(
                new MenuInput
                {
                    IsActive = true,
                    SiteId = 1,
                    Nome = "Lateral"
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Menus.FirstOrDefaultAsync(i => i.Site.Id == 1);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }

        private async Task CreateWidgetHTMLAndTestAsync()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync();
            await WidgetAppService.Save(new WidgetInput
            {
                Conteudo = "<p>Teste</p>",
                ExibirTitulo = true,
                IsActive = true,
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                SiteId = 1,
                TemplateId = 1,
                Tipo = TipoDeWidgetEnum.CmsHtml,
                Titulo = "Teste HTML"
            });
        }

        private async Task CreateWidgetMenuAndTestAsync()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync();
            await CreateMenuAndTestAsync();
            await WidgetAppService.Save(new WidgetInput()
            {
                ExibirTitulo = true,
                IsActive = true,
                MenuId = 1,
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                SiteId = 1,
                TemplateId = 1,
                Tipo = TipoDeWidgetEnum.CmsMenu,
                Titulo = "Teste Menu"
            });
        }

        [Fact]
        public async Task Should_Create_Widget_HTML()
        {
            await CreateWidgetHTMLAndTestAsync();
            var createdWidget = await WidgetAppService.GetById(new IdInput(1));
            createdWidget.ShouldNotBe(null);
        }

        [Fact]
        public async Task Should_Create_Widget_Menu()
        {
            await CreateWidgetMenuAndTestAsync();
            var createdWidget = await WidgetAppService.GetById(new IdInput(1));
            createdWidget.ShouldNotBe(null);
        }

        [Fact]
        public async Task Should_Not_Create_Widtget_With_Duplicate_Titulo()
        {
            await CreateWidgetHTMLAndTestAsync();
            await WidgetAppService.Save(new WidgetInput()
            {
                Conteudo = "<p>Teste</p>",
                ExibirTitulo = true,
                IsActive = true,
                Posicao = "sidebar",
                Sistema = SistemaEnum.CMS,
                SiteId = 1,
                TemplateId = 1,
                Tipo = TipoDeWidgetEnum.CmsHtml,
                Titulo = "Teste HTML"
            }).ShouldThrowAsync<AbpValidationException>();
        }
    }
}