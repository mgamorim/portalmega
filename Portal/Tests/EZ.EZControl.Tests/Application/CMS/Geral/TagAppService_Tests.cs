﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class TagAppService_Tests : TagAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateTagAndTestAsync(string nome, string descricao, string slug)
        {
            await base.TagAppService.Save(
                new TagInput()
                {
                    IsActive = true,
                    Nome = nome,
                    Descricao = descricao,
                    Slug = slug
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Tags.FirstOrDefaultAsync(i => i.Nome == nome);
                createdSite.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Tag()
        {
            await CreateTagAndTestAsync("Hinode", "HND", "_hinode");
            await CreateTagAndTestAsync("Boulevard", "BLVD", "_boulevard");
        }

        [Fact]
        public async Task Should_Not_Create_Tag_With_Duplicate_Nome()
        {
            base.CreateTestTags();
            await TagAppService.Save(
                new TagInput
                {
                    IsActive = true,
                    Nome = "iMarketing",
                    Descricao = "Marketing",
                    Slug = "_marketing"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Delete_Tag()
        {
            base.CreateTestTags();
            await TagAppService.Delete(new IdInput(1));
            await TagAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}