﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class CategoriaAppService_Tests : CategoriaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateCategoriaAndTestAsync(string nome, string descricao, Categoria categoriaPai,
            PublicacaoBase publicacao, int siteId, string slug, int externalId = 0)
        {
            await CategoriaAppService.Save(
                new CategoriaInput()
                {
                    ExternalId = externalId,
                    IsActive = true,
                    Nome = nome,
                    Descricao = descricao,
                    CategoriaPaiId = categoriaPai != null ? categoriaPai.Id : (int?)null,
                    PostId = publicacao.Id,
                    SiteId = siteId,
                    Slug = slug
                });
            await UsingDbContext(async context =>
            {
                var createdCategoria = await context.Categorias.FirstOrDefaultAsync(i => i.Nome == nome);
                createdCategoria.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Categoria()
        {
            CreateTestsPost();
            var publicacaoDto = await PostAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = await SiteAppService.GetById(new IdInput { Id = 1 });
            var publicacaoEntity = publicacaoDto.MapTo<PublicacaoBase>();
            await CreateCategoriaAndTestAsync("Blog", "", null, publicacaoEntity, siteEntity.Id, "contato-completo");
            await CreateCategoriaAndTestAsync("Marketing", "", null, publicacaoEntity, siteEntity.Id, "not-found");
        }

        [Fact]
        public async Task Should_Not_Create_Categoria_With_Duplicate_Nome()
        {
            CreateTestCategorias();
            await CategoriaAppService.Save(
                new CategoriaInput()
                {
                    Nome = "Marketing",
                    Descricao = "",
                    PostId = 1
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Delete_Categoria()
        {
            CreateTestCategorias();
            await CategoriaAppService.Delete(new IdInput(1));
            await CategoriaAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

        [Fact]
        public async Task Should_Create_Categoria_Whith_CategoriaPai()
        {
            CreateTestsPost();
            var publicacaoDto = await PostAppService.GetById(new IdInput { Id = 1 });
            var publicacaoEntity = publicacaoDto.MapTo<PublicacaoBase>();
            var siteEntity = await SiteAppService.GetById(new IdInput { Id = 1 });
            await CreateCategoriaAndTestAsync("Marketing", "", null, publicacaoEntity, siteEntity.Id, "contato");
            var CategoriaPai = await CategoriaAppService.GetById(new IdInput(1));
            await CreateCategoriaAndTestAsync("Marketing Digital", "", new Categoria() { Id = CategoriaPai.Id }, publicacaoEntity, siteEntity.Id, "home");
        }
    }
}