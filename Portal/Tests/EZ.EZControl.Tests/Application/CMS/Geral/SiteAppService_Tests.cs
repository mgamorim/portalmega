﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class SiteAppService_Tests : SiteAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateSiteAndTestAsync(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner,
                                                  string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            await SiteAppService.Save(
                new SiteInput
                {
                    Nome = nome,
                    Descricao = descricao,
                    Hosts = new List<HostInput>
                    {
                        new HostInput
                        {
                            IsActive = true,
                            IsPrincipal = true,
                            Url = host
                        }
                    },
                    MetaKeywords = metaKeywords,
                    MetaTags = metaTags,
                    Owner = owner,
                    TemplateDefaultId = 1,
                    TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                    TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                    TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                    TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Nome == nome);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Site()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("Hinode", "", "http://www.hinode.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            await CreateSiteAndTestAsync("Boulevard", "", "http://www.boulevard.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
        }

        [Fact]
        public async Task Should_Not_Create_Site_With_Duplicate_Nome()
        {
            CreateTestSites();
            await SiteAppService.Save(
                new SiteInput
                {
                    Nome = "Hinode",
                    Descricao = "",
                    Hosts = new List<HostInput>
                            {
                                new HostInput
                                {
                                    IsActive = true,
                                    IsPrincipal = true,
                                    Url = "ezcontrol.com"
                                }
                            },
                    TemplateDefaultId = 1,
                    TipoDePaginaParaPaginaDefault = "Login",
                    TipoDePaginaParaCategoriaDefault = "QuemSomos",
                    TipoDePaginaParaPaginaInicialDefault = "Contato",
                    TipoDePaginaParaPostDefault = "Home"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Not_Create_Site_With_Duplicate_Url()
        {
            CreateTestSites();

            var siteInput = new SiteInput
            {
                Nome = "Hinode Site",
                Descricao = "",
                TemplateDefaultId = 1,
                TipoDePaginaParaPaginaDefault = "Login",
                TipoDePaginaParaCategoriaDefault = "QuemSomos",
                TipoDePaginaParaPaginaInicialDefault = "Contato",
                TipoDePaginaParaPostDefault = "Home",
                Hosts = new List<HostInput>
                {
                    new HostInput
                    {
                        IsPrincipal = true,
                        Url = "www.hinode.com.br"
                    }
                }
            };

            await SiteAppService.Save(siteInput).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Delete_Site()
        {
            CreateTestSites();
            await SiteAppService.Delete(new IdInput(1));
            await SiteAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}