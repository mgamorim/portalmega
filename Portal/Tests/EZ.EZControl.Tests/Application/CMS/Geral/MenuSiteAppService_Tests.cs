﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Dto.CMS.Geral.Host;
using EZ.EZControl.Dto.CMS.Geral.ItemDeMenu;
using EZ.EZControl.Dto.CMS.Geral.Menu;
using EZ.EZControl.Dto.CMS.Geral.Site;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class MenuAppService_Tests : MenuAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateSiteAndTestAsync(string nome, string descricao, string host, string metaKeywords, string metaTags, string owner,
                                                  string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            await _siteAppService.Save(
           new SiteInput
           {
               Nome = nome,
               Descricao = descricao,
               MetaKeywords = metaKeywords,
               MetaTags = metaTags,
               Owner = owner,
               TemplateDefaultId = 1,
               TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
               TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
               TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
               TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
               Hosts = new List<HostInput>
               {
                       new HostInput
                       {
                           IsActive = true,
                           IsPrincipal = true,
                           Url = host
                       }
               }
           });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Sites.FirstOrDefaultAsync(i => i.Nome == nome);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateTemplateAndTestAsync()
        {
            CreateTestTemplates();
            await UsingDbContext(async context =>
            {
                var createdTemplate = await context.Templates.FirstOrDefaultAsync(t => t.Id == 1);
                createdTemplate.ShouldNotBe(null);
            });
        }

        private async Task CreateMenuAndTestAsync()
        {
            await base._menuAppService.Save(
                new MenuInput()
                {
                    IsActive = true,
                    SiteId = 1,
                    Nome = "Lateral"
                });
            await UsingDbContext(async context =>
            {
                var createdSite = await context.Menus.FirstOrDefaultAsync(i => i.Site.Id == 1);
                createdSite.ShouldNotBe(null);
            });
        }

        private async Task CreateItensDeMenuAndTestAsync()
        {
            await _itemDeMenuAppService.Save(
                new ItemDeMenuInput()
                {
                    IsActive = true,
                    MenuId = 1,
                    DescricaoDoTitulo = "Google",
                    TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                    Titulo = "Google",
                    Url = "http://www.google.com"
                });
            await _itemDeMenuAppService.Save(
                new ItemDeMenuInput()
                {
                    IsActive = true,
                    MenuId = 1,
                    ItemMenuId = 1,
                    DescricaoDoTitulo = "Amazon",
                    TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                    Titulo = "Amazon",
                    Url = "http://www.amazon.com"
                });
            await UsingDbContext(async context =>
            {
                var createdItemDeMenu = await context.ItensDeMenu.FirstOrDefaultAsync(i => i.Menu.Id == 1);
                createdItemDeMenu.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Menu()
        {
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("Hinode", "", "http://www.hinode.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            await CreateSiteAndTestAsync("Boulevard", "", "http://www.boulevard.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            await CreateMenuAndTestAsync();
        }

        [Fact]
        public async Task Should_Not_Create_Menu_With_Duplicate_Nome()
        {
            #region Arrange
            CreateMenuTest();
            #endregion

            #region Action e Assert
            await _menuAppService.Save(
                new MenuInput
                {
                    IsActive = true,
                    Nome = "Inicial",
                    SiteId = 1
                }).ShouldThrowAsync<AbpValidationException>();
            #endregion
        }

        [Fact]
        public async Task Should_Delete_Menu()
        {
            #region Arrange
            CreateMenuTest();
            #endregion

            #region Action
            await _menuAppService.Delete(new IdInput(1));
            #endregion

            #region Assert
            await _menuAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            #endregion
        }
        [Fact]
        public async Task Should_Return_ItensDeMenu()
        {
            #region Arrange
            await CreateTemplateAndTestAsync();
            await CreateSiteAndTestAsync("Hinode", "", "www.hinode.com.br", "", "", "", "Login", "QuemSomos", "Contato", "Home");
            await CreateMenuAndTestAsync();
            await CreateItensDeMenuAndTestAsync();
            #endregion

            #region Action
            var itensDeMenu = await _menuAppService.GetItensDeMenu(new IdInput(1));
            #endregion

            #region Assert
            itensDeMenu.Items.Count.ShouldNotBe(0);
            #endregion
        }
    }
}