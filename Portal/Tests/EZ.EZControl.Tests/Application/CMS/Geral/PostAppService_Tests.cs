﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.UI;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Dto.CMS.Geral.Categoria;
using EZ.EZControl.Dto.CMS.Geral.Post;
using EZ.EZControl.Dto.CMS.Geral.Tag;
using EZ.EZControl.Tests.Application.CMS.Geral.Mocks;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.CMS.Geral
{
    public class PostAppService_Tests : PostAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum tipo, string titulo, string conteudo, Site site, string slug)
        {
            await PostAppService.Save(
            new PostInput()
            {
                Conteudo = conteudo,
                Titulo = titulo,
                SiteId = site.Id,
                TipoDePublicacao = tipo,
                Slug = slug,
                StatusDaPublicacaoEnum = StatusDaPublicacaoEnum.Publicada,
                Categorias = new List<CategoriaInput>
                {
                       new CategoriaInput
                       {
                           IsActive = true,
                           Descricao = "Categoria de Teste",
                           Nome = "Teste",
                           Slug = "categoria_de_teste",
                           SiteId = site.Id
                       }
                },
                Tags = new List<TagInput>
                {
                        new TagInput
                        {
                            IsActive = true,
                            Nome = "TagTeste",
                            Descricao = "Tag de Teste",
                            Slug = "tag_de_teste"
                        }
                },
                IsActive = true
            });
            await UsingDbContext(async context =>
            {
                var createdPublicacao = await context.Posts.FirstOrDefaultAsync(i => i.Titulo == titulo);
                createdPublicacao.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Publicacao_Pagina()
        {
            CreateTestsSite();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Pagina, "Página de Teste", "Teste", siteEntity, "pagina_de_teste");
        }

        [Fact]
        public async Task Should_Create_Publicacao_Post()
        {
            CreateTestsSite();
            var siteDto = await SiteAppService.GetById(new IdInput { Id = 1 });
            var siteEntity = siteDto.MapTo<Site>();
            await CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "Teste", siteEntity, "post_de_teste");
        }

        [Fact]
        public async Task Should_Delete_Publicacao()
        {
            base.CreateTestsPosts();
            //var publicacao = await PostAppService.GetById(new IdInput(1));
            await base.PostAppService.Delete(new IdInput(1));
            await base.PostAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();

            //await PostAppService.Delete(new IdInput(publicacao.Id));
            //await PostAppService.GetById(new IdInput(1)).ShouldThrowAsync<Exception>();
        }

        [Fact]
        public async Task Should_Clear_Tags()
        {
            CreateTestsSite();
            var site = await SiteAppService.GetById(new IdInput(1));
            await this.CreatePublicacaoAndTestAsync(TipoDePublicacaoEnum.Post, "Post de Teste", "<strong>Teste</strong>", site.MapTo<Site>(), "post-de-teste");
            var publicacao = await PostAppService.GetById(new IdInput(1));
            publicacao.Tags.Clear();
            await PostAppService.Save(publicacao);
        }
    }
}