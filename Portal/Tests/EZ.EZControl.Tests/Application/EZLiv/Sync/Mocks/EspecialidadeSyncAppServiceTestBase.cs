﻿using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Sync.Mocks
{
    public class EspecialidadeSyncAppServiceTestBase : AppTestBase
    {
        public readonly IEspecialidadeSyncAppService EspecialidadeSyncAppService;
        protected readonly IProdutoDePlanoDeSaudeAppService ProdutoDePlanoDeSaudeAppService;
        protected readonly IPlanoDeSaudeAppService PlanoDeSaudeAppService;

        public EspecialidadeSyncAppServiceTestBase()
        {
            this.EspecialidadeSyncAppService = Resolve<IEspecialidadeSyncAppService>();
            this.ProdutoDePlanoDeSaudeAppService = Resolve<IProdutoDePlanoDeSaudeAppService>();
            this.PlanoDeSaudeAppService = Resolve<IPlanoDeSaudeAppService>();
        }

        protected void CreateTestPlanosDeSaude()
        {
            UsingDbContext(context =>
            {
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.260/16-1", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Rubi207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.388/16-7", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.387/16-9", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante217, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "3x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e internação"));
            });
        }

        protected void CreateTestProdutosDePlanoDeSaude()
        {
            UsingDbContext(context =>
            {
                var plano = CreatePlanoDeSaudeEntity("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual,
                    AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207,
                    SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há");

                context.ProdutosDePlanoDeSaude.Add(CreateProdutoDePlanoDeSaudeEntity(plano, "Especial 100", 250, "O melhor custo"));
            });
        }


        private PlanoDeSaude CreatePlanoDeSaudeEntity(string numeroDoRegistro, AbrangenciaDoPlanoEnum abrangencia, AcomodacaoEnum acomodacao,
                                                      PlanoDeSaudeAnsEnum planoAns, SegmentacaoAssistencialDoPlanoEnum segmentacao, string descricaoDoReembolso, bool reembolso = false)
        {
            return new PlanoDeSaude
            {
                IsActive = true,
                NumeroDeRegistro = numeroDoRegistro,
                Reembolso = reembolso,
                DescricaoDoReembolso = descricaoDoReembolso,
                Abrangencia = abrangencia,
                Acomodacao = acomodacao,
                PlanoDeSaudeAns = planoAns,
                SegmentacaoAssistencial = segmentacao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ProdutoDePlanoDeSaude CreateProdutoDePlanoDeSaudeEntity(PlanoDeSaude plano, string nome, decimal valor, string descricao)
        {
            return new ProdutoDePlanoDeSaude
            {
                IsActive = true,
                PlanoDeSaude = plano,
                Nome = nome,
                Valor = valor,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
