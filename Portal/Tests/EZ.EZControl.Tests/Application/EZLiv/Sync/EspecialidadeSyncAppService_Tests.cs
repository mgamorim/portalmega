﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Dto.EZLiv.Sync.EspecialidadeSync;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZLiv.Sync.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Sync
{
    public class EspecialidadeSyncAppService_Tests : EspecialidadeSyncAppServiceTestBase
    {
        public EspecialidadeSyncAppService_Tests()
        {
            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        private EspecialidadeSyncInput CreateEspecialidadeSync(string nome, int idProduto, string especialidade, string logradouro, string numero,
                                                               string complemento, string bairro, string municipio, string estado,
                                                               string nomeEstado, string telefone1, string ramal1, string telefone2,
                                                               string ramal2, string municipioPai, string email, string homePage)
        {
            return new EspecialidadeSyncInput
            {
                IsActive = true,
                Nome = nome,
                Especialidade = especialidade,
                Logradouro = logradouro,
                Numero = numero,
                Complemento = complemento,
                Bairro = bairro,
                Municipio = municipio,
                Estado = estado,
                NomeEstado = nomeEstado,
                Telefone1 = telefone1,
                Ramal1 = ramal1,
                Telefone2 = telefone2,
                Ramal2 = ramal2,
                MunicipioPai = municipioPai,
                Email = email,
                HomePage = homePage,
                ProdutosDePlanoDeSaudeIds = new[] { idProduto }
            };
        }

        private async void CreateEspecialidadesSync()
        {
            base.CreateTestPlanosDeSaude();
            base.CreateTestProdutosDePlanoDeSaude();

            var especialidadeSyncInput = this.CreateEspecialidadeSync("Obstetria", 1, "Médico Obstetra", "Rua", "1000",
                                                                      string.Empty, "Barra da Tijuca", "Rio de Janeiro", "Rio de Janeiro", "Rio de Janeiro", "(21)3040-1612",
                                                                      "501", "(21)9465-7321", string.Empty, "Rio de Janeiro", "teste@teste.com.br", "www.teste.com.br");

            var especialidade2SyncInput = this.CreateEspecialidadeSync("Nutrição Clínica", 1, "Nutricionista", "Rua", "584",
                                                                      string.Empty, "Capim", "Governador Valadares", "Minas Gerais", "Minas Gerais", "(31)3040-1612",
                                                                      "501", "(31)9465-7321", string.Empty, "Governador Valadares", "teste@teste.com.br", "www.teste.com.br");

            var especialidade3SyncInput = this.CreateEspecialidadeSync("Nutrição Esportiva", 1, "Nutricionista", "Avenida", "22",
                                                                      string.Empty, "Interlagos", "Governador Valadares", "Minas Gerais", "Minas Gerais", "(31)3040-1612",
                                                                      "8", "(31)9465-7321", string.Empty, "Governador Valadares", "teste@teste.com.br", "www.teste.com.br");

            await base.EspecialidadeSyncAppService.Save(especialidadeSyncInput);
            await base.EspecialidadeSyncAppService.Save(especialidade2SyncInput);
            await base.EspecialidadeSyncAppService.Save(especialidade3SyncInput);
        }

        [Fact]
        public async Task Should_Create_EspecialidadeSync()
        {
            #region Arrange
            base.CreateTestPlanosDeSaude();
            base.CreateTestProdutosDePlanoDeSaude();
            #endregion

            #region Action
            var especialidadeSyncInput = this.CreateEspecialidadeSync("Psicólogo", 1, "Médico Psicólogo", "Rua", "1000",
                                                                     "Casa 3", "Barra da Tijuca", "Rio de Janeiro", "Rio de Janeiro", "Rio de Janeiro", "(21)3040-1612",
                                                                     "501", "(21)9465-7321", string.Empty, "Rio de Janeiro", "teste@teste.com.br", "www.teste.com.br");
            #endregion

            #region Assert
            await base.EspecialidadeSyncAppService.Save(especialidadeSyncInput);
            #endregion

        }

        [Fact]
        public async Task Delete_EspecialidadeSync()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                base.CreateTestPlanosDeSaude();
                base.CreateTestProdutosDePlanoDeSaude();
                var especialidadeSyncInput = this.CreateEspecialidadeSync("Psicólogo", 1, "Médico Psicólogo", "Rua", "1000",
                                                                          "Casa 3", "Barra da Tijuca", "Rio de Janeiro", "Rio de Janeiro", "Rio de Janeiro", "(21)3040-1612",
                                                                          "501", "(21)9465-7321", string.Empty, "Rio de Janeiro", "teste@teste.com.br", "www.teste.com.br");
                await base.EspecialidadeSyncAppService.Save(especialidadeSyncInput);


                var especialidades = await base.EspecialidadeSyncAppService.GetPaginado(new GetEspecialidadeSyncInput
                {
                    MaxResultCount = 1000,
                    SkipCount = 0,
                    Nome = "Psicólogo"
                });

                var especialidadeId = especialidades.Items[0].Id;

                await base.EspecialidadeSyncAppService.Delete(new IdInput(especialidadeId));
                await uow.Current.SaveChangesAsync();
                await base.EspecialidadeSyncAppService.GetById(new IdInput(especialidadeId)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        [Fact]
        public async Task Should_Get_EspecialidadeSync_By_Estado()
        {
            #region Arrange
            this.CreateEspecialidadesSync();
            #endregion

            #region Action
            var especialidadesPorEstado = await base.EspecialidadeSyncAppService.GetEspecialidadeSyncByEstado(new GetEspecialidadeSyncByEstadoDto { Estado = "Rio de Janeiro" });
            #endregion

            #region Assert
            especialidadesPorEstado.Items.Count.ShouldNotBe(0);
            #endregion
        }

        [Fact]
        public async Task Should_Get_EspecialidadeSync_By_Estado_Distinct()
        {
            #region Arrange
            this.CreateEspecialidadesSync();
            #endregion

            #region Action
            var especialidadesPorEstado = await base.EspecialidadeSyncAppService.GetEspecialidadeSyncByEstadoDistinct(new GetEspecialidadeSyncByEstadoDto { Estado = "Minas Gerais" });
            #endregion

            #region Assert
            especialidadesPorEstado.Items.Count.ShouldNotBe(0);
            #endregion
        }

        [Fact]
        public async Task Should_Get_EspecialidadeSync_By_Municipio()
        {
            #region Arrange
            this.CreateEspecialidadesSync();
            #endregion

            #region Action
            var especialidadesPorMunicipio =
                await base.EspecialidadeSyncAppService
                            .GetEspecialidadeSyncByMunicipio(
                                new GetEspecialidadeSyncByMunicipioDto
                                {
                                    Municipio = "Rio de Janeiro",
                                    Estado = "Rio de Janeiro"
                                });
            #endregion

            #region Assert
            especialidadesPorMunicipio.Items.Count.ShouldNotBe(0);
            #endregion
        }

        [Fact]
        public async Task Should_Get_EspecialidadeSync_By_Municipio_Distinct()
        {
            #region Arrange
            this.CreateEspecialidadesSync();
            #endregion

            #region Action
            var especialidadesPorMunicipio =
                await base.EspecialidadeSyncAppService
                            .GetEspecialidadeSyncByMunicipio(
                                new GetEspecialidadeSyncByMunicipioDto
                                {
                                    Estado = "Minas Gerais",
                                    Municipio = "Governador Valadares"
                                });
            #endregion

            #region Assert
            especialidadesPorMunicipio.Items.Count.ShouldNotBe(0);
            #endregion
        }

        [Fact]
        public async Task Should_Get_EspecialidadeSync_By_Nome_Distinct()
        {
            #region Arrange
            this.CreateEspecialidadesSync();
            #endregion

            #region Action
            var especialidadesPorNome =
                await base.EspecialidadeSyncAppService
                            .GetEspecialidadeSyncByNomeDistinct(new GetEspecialidadeSyncByNomeDto { Nome = "Nutrição Esportiva" });
            #endregion

            #region Assert
            especialidadesPorNome.Items.Count.ShouldNotBe(0);
            #endregion
        }
    }
}
