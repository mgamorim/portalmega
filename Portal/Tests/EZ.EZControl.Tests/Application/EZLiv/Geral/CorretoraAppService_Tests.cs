﻿using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Configuration.Host;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class CorretoraAppService_Tests : CorretoraAppServiceTestBase
    {
        private readonly IRepository<Documento> _documentoRepository;
        private readonly IRepository<TipoDeDocumento> _tipoDeDocumentoRepository;
        private readonly IRepository<PessoaFisica> _pessoaRepository;
        private readonly IHostSettingsAppService _hostSettingsAppService;
        private readonly ISettingManager _settingManager;

        public CorretoraAppService_Tests()
        {
            _documentoRepository = Resolve<IRepository<Documento>>();
            _tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
            _pessoaRepository = Resolve<IRepository<PessoaFisica>>();
            _hostSettingsAppService = Resolve<IHostSettingsAppService>();
            _settingManager = Resolve<ISettingManager>();

            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        private async Task<PessoaJuridicaInput> SavePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            PessoaJuridicaInput pessoaJuridicaInput = new PessoaJuridicaInput
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial,
                TipoPessoa = TipoPessoaEnum.Juridica,
                IsActive = true
            };

            var result = await base.PessoaJuridicaAppService.SaveAndReturnEntity(pessoaJuridicaInput);

            return result;
        }

        private async Task<GrupoPessoaInput> SaveGrupoPessoa(string descricao)
        {
            GrupoPessoaInput grupoPessoaInput = new GrupoPessoaInput
            {
                Descricao = descricao,
                Nome = descricao,
                TipoDePessoa = TipoPessoaEnum.Fisica,
                IsActive = true
            };

            var result = await base.GrupoPessoaAppService.SaveAndReturnEntity(grupoPessoaInput);

            return result;
        }

        private Documento CreateDocumento(PessoaFisica pessoa, TipoDeDocumento tipoDeDocumento, string numero = null,
            DateTime? dataDeExpedicao = null, string orgao = null)
        {
            var doc = new Documento(pessoa, tipoDeDocumento, numero, orgao, dataDeExpedicao)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
            return doc;
        }


        private TipoDeDocumento CreateTipoDeDocumento(string descricao, string mascara,
           TipoDeDocumentoEnum tipoDeDocumentoFixo)
        {
            return new TipoDeDocumento
            {
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                Descricao = descricao,
                TipoDeDocumentoFixo = tipoDeDocumentoFixo,
                Mascara = mascara
            };
        }

        private async Task<PessoaFisicaInput> SavePessoaFisica(string nome, string nacionalidade, DateTime dataDeNascimento, SexoEnum sexo, EstadoCivilEnum estadoCivil)
        {
            var gp = await SaveGrupoPessoa("Corretor");

            PessoaFisicaInput pessoaFisicaInput = new PessoaFisicaInput
            {
                Nome = nome,
                IsActive = true,
                Nacionalidade = nacionalidade,
                DataDeNascimento = dataDeNascimento,
                Sexo = sexo,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = estadoCivil,
                GrupoPessoaId = gp.Id
            };

            var result = await base.PessoaFisicaAppService.SaveAndReturnEntity(pessoaFisicaInput);
            var pessoaTitular = _pessoaRepository.Get(result.Id);
            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);

            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoCpf);

            var documentoCpf = this.CreateDocumento(pessoaTitular, tipoDeDocumentoCpf, "08164610747");

            _documentoRepository.InsertOrUpdate(documentoCpf);



            return await base.PessoaFisicaAppService.GetId(new IdInput(result.Id));
        }

        private async Task<CorretorInput> SaveCorretor(string nome, string nacionalidade, DateTime dataDeNascimento, SexoEnum sexo, EstadoCivilEnum estadoCivil, PessoaFisicaInput pf)
        {
            return await base.CorretorAppService.SaveAndReturnEntity(new CorretorInput
            {
                IsActive = true,
                Pessoa = pf
            });
        }

        private async Task<IdInput> CreateCorretoraAndTestAsync(string razaoSocial, string nomeFantasia)
        {
            var pj = await SavePessoaJuridica(nomeFantasia, razaoSocial);
            var pf = await SavePessoaFisica("Wescley Arruda", "Brasileiro", DateTime.Now.AddYears(-20), SexoEnum.Masculino, EstadoCivilEnum.Casado);
            var corretor = await SaveCorretor("Wescley Arruda", "Brasileiro", DateTime.Now.AddYears(-20), SexoEnum.Masculino, EstadoCivilEnum.Casado, pf);

            return await CorretoraAppService.Save(new CorretoraInput
            {
                IsActive = true,
                Nome = nomeFantasia,
                PessoaJuridica = pj,
                PessoaJuridicaId = pj.Id,
                Corretor = corretor,
                CorretorId = corretor.Id,
                ParametroPagSeguro = new ParametroPagSeguroInput
                {
                    Token = Guid.NewGuid().ToString(),
                    Email = "teste@gmail.com",
                    PessoaJuridicaId = pj.Id
                }
            });
        }

        [Fact]
        public async Task Should_Create_Corretora()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await CreateCorretoraAndTestAsync("Bem Benefícios", "Bem Benefícios LTDA");
            });
        }

        [Fact]
        public async Task Delete_Corretora()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var corretora = await CreateCorretoraAndTestAsync("Mosaíco Corretor", "M Corretor Eirelli");
                await base.CorretoraAppService.Delete(corretora);
                await uow.Current.SaveChangesAsync();
                await base.CorretoraAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}