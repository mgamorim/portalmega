﻿using Abp.Application.Services.Dto;
using Castle.Core.Internal;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZLiv.Geral.PropostaDeContratacao;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class PropostaDeContratacaoAppService_Tests : PropostaDeContratacaoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }
        public PropostaDeContratacaoAppService_Tests()
        {
            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }
        [Fact]
        public async Task Should_Save_Proposta_Passo_1()
        {
            await CreateCorretorTest();
            CreateTitularTest();

            var titularId = 0;
            UsingDbContext(c =>
            {
                titularId = c.Proponentes.FirstOrDefault().Id;
            });

            var proposta = new PropostaDeContratacaoInput()
            {
                TitularId = titularId
            };
            var createdProposta = await PropostaDeContratacaoAppService.Save(proposta);
            createdProposta.ShouldNotBe(null);
            createdProposta.Id.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Save_Proposta_Passo_2()
        {
            await CreateCorretorTest();
            CreateTitularTest();
            CreateProdutoTest();

            var titularId = 0;
            UsingDbContext(c =>
            {
                titularId = c.Proponentes.FirstOrDefault().Id;
            });

            var produtoId = 0;
            UsingDbContext(c =>
            {
                produtoId = c.ProdutosDePlanoDeSaude.FirstOrDefault().Id;
            });

            var proposta = new PropostaDeContratacaoInput()
            {
                TitularId = titularId,
                ProdutoId = produtoId
            };
            var createdProposta = await PropostaDeContratacaoAppService.Save(proposta);
            createdProposta.ShouldNotBe(null);
            createdProposta.Id.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Save_Proposta_Passo_3()
        {
            await CreateCorretorTest();
            CreateTitularTest();
            CreateProdutoTest();
            CreateResponsavelTest();

            var titularId = 0;
            UsingDbContext(c =>
            {
                titularId = c.Proponentes.FirstOrDefault().Id;
            });

            var produtoId = 0;
            UsingDbContext(c =>
            {
                produtoId = c.ProdutosDePlanoDeSaude.FirstOrDefault().Id;
            });

            var responsavelId = 0;
            UsingDbContext(c =>
            {
                responsavelId = c.Responsaveis.FirstOrDefault().Id;
            });

            var proposta = new PropostaDeContratacaoInput()
            {
                TitularId = titularId,
                ProdutoId = produtoId,
                ResponsavelId = responsavelId
            };
            var createdProposta = await PropostaDeContratacaoAppService.Save(proposta);
            createdProposta.ShouldNotBe(null);
            createdProposta.Id.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Save_Proposta_Passo_4()
        {
            await CreateCorretorTest();
            CreateTitularTest();
            CreateProdutoTest();
            CreateResponsavelTest();
            CreateContratoTest();

            var titularId = 0;
            UsingDbContext(c =>
            {
                titularId = c.Proponentes.FirstOrDefault().Id;
            });

            var produtoId = 0;
            UsingDbContext(c =>
            {
                produtoId = c.ProdutosDePlanoDeSaude.FirstOrDefault().Id;
            });

            var responsavelId = 0;
            UsingDbContext(c =>
            {
                responsavelId = c.Responsaveis.FirstOrDefault().Id;
            });

            var contratoId = 0;
            UsingDbContext(c =>
            {
                contratoId = c.Contratos.FirstOrDefault().Id;
            });

            var proposta = new PropostaDeContratacaoInput()
            {
                TitularId = titularId,
                ProdutoId = produtoId,
                ResponsavelId = responsavelId,
                ContratoVigenteNoAceiteId = contratoId,
                Aceite = true,
                DataHoraDoAceite = DateTime.Now,
                UltimoContratoAceitoId = contratoId
            };
            var createdProposta = await PropostaDeContratacaoAppService.Save(proposta);
            createdProposta.ShouldNotBe(null);
            createdProposta.Id.ShouldBeGreaterThan(0);
        }


        [Fact]
        public async Task Should_Save_Proposta_Passo_Homologacao()
        {
            await CreateCorretorTest();
            CreateTitularTest();
            CreateProdutoTest();
            CreateResponsavelTest();
            CreateContratoTest();

            var titularId = 0;
            UsingDbContext(c =>
            {
                titularId = c.Proponentes.FirstOrDefault().Id;
            });

            var produtoId = 0;
            UsingDbContext(c =>
            {
                produtoId = c.ProdutosDePlanoDeSaude.FirstOrDefault().Id;
            });

            var responsavelId = 0;
            UsingDbContext(c =>
            {
                responsavelId = c.Responsaveis.FirstOrDefault().Id;
            });

            var contratoId = 0;
            UsingDbContext(c =>
            {
                contratoId = c.Contratos.FirstOrDefault().Id;
            });

            var proposta = new PropostaDeContratacaoInput
            {
                TitularId = titularId,
                ProdutoId = produtoId,
                ResponsavelId = responsavelId,
                ContratoVigenteNoAceiteId = contratoId,
                Aceite = true,
                DataHoraDoAceite = DateTime.Now,
                UltimoContratoAceitoId = contratoId,
                TipoDeHomologacao = TipoDeHomologacaoEnum.Homologado
            };
            var createdProposta = await PropostaDeContratacaoAppService.Save(proposta);
            createdProposta.ShouldNotBe(null);
            createdProposta.Id.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Filter_Propostas_By_Corretor_Logado()
        {
            await CreateCorretorTest();
            CreateTitularTest();

            var titularId = 0;
            UsingDbContext(c =>
            {
                titularId = c.Proponentes.FirstOrDefault().Id;
            });

            var proposta = new PropostaDeContratacaoInput()
            {
                TitularId = titularId
            };
            await PropostaDeContratacaoAppService.Save(proposta);

            var propostas = await PropostaDeContratacaoAppService.GetPaginado(new GetPropostaDeContratacaoInput());

            var corretores = await CorretorAppService.GetCorretoresPaginado(new GetCorretorInput());

            var corretorId = corretores.Items.FirstOrDefault().Id;

            propostas.Items.ForEach(i =>
            {
                i.CorretorId.ShouldBe(corretorId);
            });
        }

        [Fact]
        public async Task DeveCriarPassoPreCadastro()
        {
            CreateTipoDeDocumento();
            CreateProfissao();
            CreateEstado();
            await CreateCorretorTest();

            var profissaoId = 0;
            UsingDbContext(c =>
            {
                profissaoId = c.Profissoes.FirstOrDefault().Id;
            });

            var estadoId = 0;
            UsingDbContext(c =>
            {
                estadoId = c.Estados.FirstOrDefault().Id;
            });

            //Dados Iniciais - Arrange (Input)
            PropostaDeContratacaoPassoPreCadastroInput input = new PropostaDeContratacaoPassoPreCadastroInput();
            input.NomeCompleto = "Diego Bonomo";
            input.DataDeNascimento = Convert.ToDateTime("29/08/1984");
            input.Cpf = "70558576001";
            input.Email = "email@email.com";
            input.Celular = "963985544";
            input.ProfissaoId = profissaoId;
            input.EstadoId = estadoId;

            //Action
            var retorno = await PropostaDeContratacaoAppService.SavePassoPreCadastro(input);

            //Assert
            PropostaDeContratacaoAppService.GetById(new IdInput(retorno.Id)).ShouldNotBeNull();
        }
    }
}