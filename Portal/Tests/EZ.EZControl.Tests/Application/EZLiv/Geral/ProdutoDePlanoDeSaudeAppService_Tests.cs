﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Dto.EZLiv.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class ProdutoDePlanoDeSaudeAppService_Tests : ProdutoDePlanoDeSaudeAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            // PreInitialize();
            base.PreInitialize();
        }

        private async Task CreateProdutoDePlanoDeSaudeAndTestAsync(string nome, decimal valor, string descricao, int idNatureza, int idUnidade, int idOperadora, int idEspecialidade, int idAssociacao = 0)
        {
            await ProdutoDePlanoDeSaudeAppService.Save(new ProdutoDePlanoDeSaudeInput
            {
                IsActive = true,
                PlanoDeSaudeId = 1,
                NaturezaId = idNatureza,
                UnidadeMedidaId = idUnidade,
                OperadoraId = idOperadora,
                Nome = nome,
                Valor = valor,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = descricao,
                EspecialidadesIds = new[] { 1 },
                FormaDeContratacao = FormaDeContratacaoEnum.Adesao
                //AssociacaoId = idAssociacao
            });
        }


        [Fact]
        public async Task Should_Create_Produto_De_Plano_De_Saude()
        {
            CreateNatureza();
            CreateUnidade();
            CreatePlanoDeSaude();
            CreateOperadora();
            CreateEspecialidade();
            CreateAssociacao();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1, 1);
        }

        [Fact]
        public async Task Should_Not_Create_Produto_De_Plano_De_Saude_With_Duplicate_Nome()
        {
            CreateNatureza();
            CreateUnidade();
            CreatePlanoDeSaude();
            CreateOperadora();
            CreateEspecialidade();
            CreateAssociacao();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1, 1);

            await ProdutoDePlanoDeSaudeAppService.Save(new ProdutoDePlanoDeSaudeInput
            {
                IsActive = true,
                PlanoDeSaudeId = 2,
                Nome = "Especial 100",
                Valor = 250,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = "Teste"
                //AssociacaoId = 1
            })
            .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Produto_De_Plano_De_Saude()
        {
            CreateNatureza();
            CreateUnidade();
            CreatePlanoDeSaude();
            CreateOperadora();
            CreateEspecialidade();
            CreateAssociacao();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1, 1);
            await ProdutoDePlanoDeSaudeAppService.Delete(new IdInput(1));
            await ProdutoDePlanoDeSaudeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

        [Fact]
        public async Task Should_Create_Produto_De_Plano_De_Saude_Adesao()
        {
            CreateNatureza();
            CreateUnidade();
            CreatePlanoDeSaude();
            CreateOperadora();
            CreateEspecialidade();
            CreateAssociacao();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1, 1);
        }
        [Fact]
        public async Task Should_Create_Produto_De_Plano_De_Saude_Adesao_Com_Chancelas()
        {
            CreateNatureza();
            CreateUnidade();
            CreatePlanoDeSaude();
            CreateOperadora();
            CreateEspecialidade();
            CreateAssociacao();
            CreateChancela();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1, 1);
            var produtoDePlanoDeSaude = await ProdutoDePlanoDeSaudeAppService.GetById(new IdInput(1));
            produtoDePlanoDeSaude.Chancelas.Count.ShouldBeGreaterThan(0);
        }
    }
}
