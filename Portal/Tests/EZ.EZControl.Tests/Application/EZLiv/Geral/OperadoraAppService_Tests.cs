﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Operadora;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class OperadoraAppService_Tests : OperadoraAppServiceTestBase
    {
        public OperadoraAppService_Tests()
        {
            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateOperadoraAndTestAsync(string nome, string codigoAns, int produtoId, string razaoSocial, string nomeFantasia)
        {
            var pessoaJuridica = await base.PessoaJuridicaAppService.SaveAndReturnEntity(new PessoaJuridicaInput
            {
                RazaoSocial = razaoSocial,
                NomeFantasia = nomeFantasia,
                TipoPessoa = TipoPessoaEnum.Juridica
            });

            await base.OperadoraAppService.Save(new OperadoraInput
            {
                Nome = nome,
                CodigoAns = codigoAns,
                PessoaJuridica = pessoaJuridica,
            });
        }

        [Fact]
        public async Task Should_Create_Operadora()
        {
            base.CreateTestPlanosDeSaude();
            base.CreateNatureza();
            base.CreateUnidade();
            await CreateOperadoraAndTestAsync("Amil", "326305", 1, "Amil SA", "Amil SA");
        }

        [Fact]
        public async Task Delete_Operadora()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                base.CreateTestPlanosDeSaude();
                base.CreateNatureza();
                base.CreateUnidade();
                await CreateOperadoraAndTestAsync("Golden Cross", "403911", 1, "Golden Cross Ltda", "Golden Cross Ltda");

                var operadoras = await base.OperadoraAppService.GetOperadorasPaginado(new GetOperadoraInput()
                {
                    MaxResultCount = 1000,
                    SkipCount = 0,
                    CodigoAns = "403911",
                    Nome = "Golden Cross"
                });

                var operadoraId = operadoras.Items[0].Id;

                await base.OperadoraAppService.Delete(new IdInput(operadoraId));
                await uow.Current.SaveChangesAsync();
                await base.OperadoraAppService.GetById(new IdInput(operadoraId)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}