﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Dto.EZLiv.Geral.IndiceDeReajustePorFaixaEtaria;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class IndiceDeReajustePorFaixaEtariaAppService_Tests : IndiceDeReajustePorFaixaEtariaAppServiceTestBase
    {
        public IndiceDeReajustePorFaixaEtariaAppService_Tests()
        {
        }

        private IndiceDeReajustePorFaixaEtariaInput CreateIndiceDeReajustePorFaixaEtaria(int idFaixaEtaria, int idOperadora, decimal indiceDeReajuste)
        {
            return new IndiceDeReajustePorFaixaEtariaInput
            {
                IsActive = true,
                FaixaEtariaId = idFaixaEtaria,
                OperadoraId = idOperadora,
                IndiceDeReajuste = indiceDeReajuste,
            };
        }

        [Fact]
        public async Task Should_Create_IndiceDeReajustePorFaixaEtaria()
        {
            base.CreateFaixaEtaria();
            base.CreateOperadora();

            #region Assert
            await base.IndiceDeReajustePorFaixaEtariaAppService.Save(CreateIndiceDeReajustePorFaixaEtaria(1, 1, 75));
            await base.IndiceDeReajustePorFaixaEtariaAppService.Save(CreateIndiceDeReajustePorFaixaEtaria(2, 1, 32));
            await base.IndiceDeReajustePorFaixaEtariaAppService.Save(CreateIndiceDeReajustePorFaixaEtaria(2, 1, 4));
            #endregion
        }

        [Fact]
        public async Task Should_Update_IndiceDeReajustePorFaixaEtaria()
        {
            #region Arrange
            base.CreateFaixaEtaria();
            base.CreateOperadora();
            await base.IndiceDeReajustePorFaixaEtariaAppService.Save(CreateIndiceDeReajustePorFaixaEtaria(1, 1, 62));
            #endregion

            #region Action
            var indices = await base.IndiceDeReajustePorFaixaEtariaAppService.GetPaginado(new GetIndiceDeReajustePorFaixaEtariaInput
            {
                MaxResultCount = 1000,
                SkipCount = 0,
                IndiceDeReajuste = 62
            });

            var indiceId = indices.Items[0].Id;
            var indice = await base.IndiceDeReajustePorFaixaEtariaAppService.GetById(new IdInput(indiceId));

            indice.IndiceDeReajuste = 35;
            #endregion

            #region Assert
            await base.IndiceDeReajustePorFaixaEtariaAppService.Save(indice);
            #endregion
        }

        [Fact]
        public async Task Delete_IndiceDeReajustePorFaixaEtaria()
        {

            #region Arrange
            base.CreateFaixaEtaria();
            base.CreateOperadora();

            await base.IndiceDeReajustePorFaixaEtariaAppService.Save(CreateIndiceDeReajustePorFaixaEtaria(2, 1, 41));
            #endregion

            #region Action

            var indices = await base.IndiceDeReajustePorFaixaEtariaAppService.GetPaginado(new GetIndiceDeReajustePorFaixaEtariaInput
            {
                MaxResultCount = 1000,
                SkipCount = 0,
                IndiceDeReajuste = 41
            });

            var indiceId = indices.Items[0].Id;

            #endregion

            #region Assert
            await base.IndiceDeReajustePorFaixaEtariaAppService.Delete(new IdInput(indiceId));
            await base.IndiceDeReajustePorFaixaEtariaAppService.GetById(new IdInput(indiceId)).ShouldThrowAsync<UserFriendlyException>();
            #endregion
        }
    }
}
