﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Associacao;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class AssociacaoAppService_Tests : AssociacaoAppServiceTestBase
    {
        public AssociacaoAppService_Tests()
        {
            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        private async Task<PessoaJuridicaInput> SavePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            PessoaJuridicaInput pessoaJuridicaInput = new PessoaJuridicaInput
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial,
                TipoPessoa = TipoPessoaEnum.Juridica,
                NomePessoa= nomeFantasia,
                IsActive = true
            };

            var result = await base.PessoaJuridicaAppService.SaveAndReturnEntity(pessoaJuridicaInput);

            return result;
        }

        private async Task<IdInput> CreateAssociacaoAndTestAsync(string razaoSocial, string nomeFantasia)
        {
            var pj = await SavePessoaJuridica(nomeFantasia, razaoSocial);

                return await AssociacaoAppService.Save(new AssociacaoInput
                {
                    IsActive = true,
                    PessoaJuridica = pj,
                    PessoaJuridicaId = pj.Id
                    //ParametroPagSeguro = new ParametroPagSeguroInput
                    //{
                    //    Token = Guid.NewGuid().ToString(),
                    //    Email = "teste@gmail.com",
                    //    PessoaJuridicaId = pj.Id
                    //}
                });
            
        }

        [Fact]
        public async Task Should_Create_Associacao()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await CreateAssociacaoAndTestAsync("AUSSESP", "Associação dos Usuários de Planos de Saúde do Estado de São Paulo");
            });
        }

        [Fact]
        public async Task Delete_Associacao()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var corretora = await CreateAssociacaoAndTestAsync("AUSSESP", "Associação dos Usuários de Planos de Saúde do Estado de São Paulo");
                await base.AssociacaoAppService.Delete(corretora);
                await uow.Current.SaveChangesAsync();
                await base.AssociacaoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}