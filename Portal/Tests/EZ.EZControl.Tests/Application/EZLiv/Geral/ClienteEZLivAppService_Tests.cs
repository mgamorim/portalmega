﻿using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ClienteEZLiv;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class ClienteEZLivAppService_Tests : ClienteEZLivAppServiceTestBase
    {
        public ClienteEZLivAppService_Tests()
        {

        }

        [Fact]
        public async Task ShouldCreateClienteEZLiv_PessoaFisica()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                #region Arrange                                             
                var cliente = await GetClientePessoaFisica();
                var ClienteEZLivInput = new ClienteEZLivInput
                {
                    ClienteId = cliente.Id
                };
                #endregion

                #region Action
                await uow.Current.SaveChangesAsync();
                var clienteEZLiv = await ClienteEZLivAppService.Save(ClienteEZLivInput);
                #endregion
            });
        }

        [Fact]
        public async Task ShouldCreateClienteEZLiv_PessoaJuridica()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                #region Arrange
                var cliente = await GetClientePessoaJuridica();

                var ClienteEZLivInput = new ClienteEZLivInput
                {
                    ClienteId = cliente.Id
                };
                #endregion

                #region Action
                await uow.Current.SaveChangesAsync();
                var clienteEZLiv = await ClienteEZLivAppService.Save(ClienteEZLivInput);
                #endregion
            });
        }
    }
}