﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.EZLiv.Geral.Chancela;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class ChancelaAppService_Tests : ChancelaAppServiceTestBase
    {
        private readonly IRepository<Profissao> _profissaoRepository;
        public ChancelaAppService_Tests()
        {
            _profissaoRepository = Resolve<IRepository<Profissao>>();
        }
        private List<ProfissaoInput> CreateProfissoesAndTestAsync()
        {
            var profissao = new ProfissaoInput()
            {
                Codigo = "001",
                Titulo = "Analista de Sistemas",
                IsActive = true
            };
            var profissoes = new List<ProfissaoInput>();
            profissoes.Add(SaveProfissao(profissao));
            return profissoes;
        }
        private ProfissaoInput SaveProfissao(ProfissaoInput input)
        {
            var id = _profissaoRepository.InsertAndGetId(input.MapTo<Profissao>());
            var profissao = _profissaoRepository.Get(id);
            return profissao.MapTo<ProfissaoInput>();
        }
        private async Task<ChancelaInput> CreateChancelaAndTestAsync(string nome, decimal taxaDeAdesao, decimal taxaMensal, List<ProfissaoInput> profissoes)
        {
            var chancela = new ChancelaInput()
            {
                IsActive = true,
                Nome = nome,
                TaxaDeAdesao = taxaDeAdesao,
                TaxaMensal = taxaMensal,
                Profissoes = profissoes
            };
            return await SaveChancela(chancela);
        }
        private async Task<ChancelaInput> SaveChancela(ChancelaInput input)
        {
            return await ChancelaAppService.SaveAndReturnEntity(input);
        }
        [Fact]
        public async Task Should_Create_Chancela()
        {
            var profissoes = CreateProfissoesAndTestAsync();
            await CreateChancelaAndTestAsync("ASSUESP", 100, 5, profissoes);
            var chancela = await ChancelaAppService.GetById(new IdInput(1));
            chancela.ShouldNotBe(null);
        }
    }
}