﻿using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class PlanoDeSaudeAppServiceTestBase : AppTestBase
    {
        protected readonly IPlanoDeSaudeAppService PlanoDeSaudeAppService;

        public PlanoDeSaudeAppServiceTestBase()
        {
            this.PlanoDeSaudeAppService = Resolve<IPlanoDeSaudeAppService>();
        }
    }
}
