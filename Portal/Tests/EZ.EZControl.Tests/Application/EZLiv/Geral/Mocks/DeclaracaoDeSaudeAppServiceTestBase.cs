﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class DeclaracaoDeSaudeAppServiceTestBase : AppTestBase
    {
        public readonly IProponenteTitularAppService ProponenteTitularAppService;
        public readonly IDependenteAppService DependenteAppService;
        public readonly IResponsavelAppService ResponsavelAppService;
        public readonly IRepository<ProponenteTitular> ProponenteTitularRepository;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IDocumentoAppService DocumentoAppService;
        public readonly ITipoDeDocumentoAppService TipoDeDocumentoAppService;
        public readonly IDeclaracaoDeSaudeAppService DeclaracaoDeSaudeAppService;
        public readonly IItemDeDeclaracaoDeSaudeAppService ItemDeDeclaracaoDeSaudeAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly IProfissaoAppService ProfissaoAppService;

        public DeclaracaoDeSaudeAppServiceTestBase()
        {
            ProponenteTitularAppService = Resolve<IProponenteTitularAppService>();
            DependenteAppService = Resolve<IDependenteAppService>();
            ProponenteTitularRepository = Resolve<IRepository<ProponenteTitular>>();
            ResponsavelAppService = Resolve<IResponsavelAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            DocumentoAppService = Resolve<IDocumentoAppService>();
            TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoAppService>();
            DeclaracaoDeSaudeAppService = Resolve<IDeclaracaoDeSaudeAppService>();
            ItemDeDeclaracaoDeSaudeAppService = Resolve<IItemDeDeclaracaoDeSaudeAppService>();
            PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            ProfissaoAppService = Resolve<IProfissaoAppService>();
        }
    }
}