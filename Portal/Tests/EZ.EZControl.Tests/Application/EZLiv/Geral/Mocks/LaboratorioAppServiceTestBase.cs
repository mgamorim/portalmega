﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Data.Entity.Migrations;
using System.Linq;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class LaboratorioAppServiceTestBase : AppTestBase
    {
        public readonly ILaboratorioAppService laboratorioAppService;
        public readonly IPessoaJuridicaAppService pessoaJuridicaAppService;
        protected readonly IContatoAppService ContatoAppService;
        protected readonly IContatoService ContatoService;
        protected readonly ITipoDeLogradouroAppService TipoDeLogradouroAppService;
        protected readonly IEnderecoAppService EnderecoAppService;

        public LaboratorioAppServiceTestBase()
        {
            laboratorioAppService = Resolve<ILaboratorioAppService>();
            pessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            EnderecoAppService = Resolve<IEnderecoAppService>();
            TipoDeLogradouroAppService = Resolve<ITipoDeLogradouroAppService>();
            ContatoAppService = Resolve<IContatoAppService>();
            ContatoService = Resolve<IContatoService>();
        }


        protected void CreatePessoa(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                context.PessoasJuridicas.Add(pessoa as PessoaJuridica);
                context.SaveChanges();
            });
        }

        protected void CreateTipoDeContato(TipoDeContato tipoDeContato)
        {
            UsingDbContext(context =>
            {
                context.TipoDeContatos.AddOrUpdate(tipoDeContato);
                context.SaveChanges();
            });
        }

        protected void CreateContato(Contato contato)
        {
            UsingDbContext(context =>
            {
                context.Contatos.AddOrUpdate(contato);
                context.SaveChanges();
            });
        }

        protected void CreateTipoDeLogradouro(TipoDeLogradouro tipoDeLogradouro)
        {
            UsingDbContext(context =>
            {
                context.TiposDeLogradouros.AddOrUpdate(tipoDeLogradouro);
                context.SaveChanges();
            });
        }

        protected void CreatePais(Pais pais)
        {

            UsingDbContext(context =>
            {
                context.Paises.Add(pais);
                context.SaveChanges();
            });
        }

        protected void CreateEstado(Estado estado)
        {
            UsingDbContext(context =>
            {
                context.Estados.Add(estado);
                context.SaveChanges();
            });
        }

        protected void CreateCidade(Cidade cidade)
        {
            UsingDbContext(context =>
            {
                context.Cidades.Add(cidade);
                context.SaveChanges();
            });
        }

        protected void CreateCidade(Pais pais, Estado estado, Cidade cidade)
        {
            UsingDbContext(context =>
            {
                var paisCreated = context.Paises.Add(pais);
                estado.Pais = paisCreated;

                var estadoCreated = context.Estados.Add(estado);
                cidade.Estado = estadoCreated;

                context.Cidades.Add(cidade);
                context.SaveChanges();
            });
        }
        public void CreateEndereco(Endereco endereco)
        {
            UsingDbContext(context =>
            {
                context.Enderecos.Add(endereco);
                context.SaveChanges();
            });
        }

        private PessoaJuridica CreatePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            return new PessoaJuridica
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial
            };
        }

        private TipoDeContato CreateTipoDeContato(string descricao)
        {
            return new TipoDeContato
            {
                IsActive = true,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Contato CreateContato(PessoaJuridica pessoaJuridica, TipoDeContato tipoDeContato, string nome, string cargo, string setor)
        {
            var tratamento = new Tratamento
            {
                Descricao = "Sr.",
                EmpresaId = EmpresaIdDefaultTest
            };
            return new Contato(pessoaJuridica, nome, tipoDeContato, tratamento, SexoEnum.NaoInformado, null, cargo,
                setor, null, null, ContatoService)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        public Pais CreatePais(string nome, string nacionalidade, string codigoIso)
        {
            return new Pais
            {
                IsActive = true,
                Nome = nome,
                Nacionalidade = nacionalidade,
                CodigoISO = codigoIso,
                TenantId = 1
            };
        }

        public Estado CreateEstado(Pais pais, string nome, string capital, string sigla)
        {
            return new Estado
            {
                IsActive = true,
                Nome = nome,
                Capital = capital,
                Sigla = sigla,
                Pais = pais,
                TenantId = 1
            };
        }

        public Cidade CreateCidade(Estado estado, string nome, string codigoIBGE)
        {
            return new Cidade
            {
                IsActive = true,
                Nome = nome,
                CodigoIBGE = codigoIBGE,
                Estado = estado,
                TenantId = 1
            };
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(string descricao)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao
            };
        }
        public int GetTelefoneId(int pessoaId)
        {
            int telefoneId = 0;

            UsingDbContext(context =>
            {
                var pessoa = context.Pessoas.Find(pessoaId);

                telefoneId = pessoa.Telefones.FirstOrDefault().Id;
            });

            return telefoneId;
        }
    }
}
