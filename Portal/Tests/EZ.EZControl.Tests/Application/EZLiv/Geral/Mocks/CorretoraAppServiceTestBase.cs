﻿using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class CorretoraAppServiceTestBase : AppTestBase
    {
        public readonly ICorretoraAppService CorretoraAppService;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly IEmpresaAppService EmpresaAppService;
        public readonly IClienteEZAppService ClienteEzAppService;
        public readonly IGrupoPessoaAppService GrupoPessoaAppService;
        public readonly ICorretorAppService CorretorAppService;
        public readonly IDocumentoAppService DocumentoAppService;
        public readonly ITipoDeDocumentoAppService TipoDeDocumentoAppService;

        public CorretoraAppServiceTestBase()
        {
            CorretoraAppService = Resolve<ICorretoraAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            EmpresaAppService = Resolve<IEmpresaAppService>();
            ClienteEzAppService = Resolve<IClienteEZAppService>();
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
            CorretorAppService = Resolve<ICorretorAppService>();
            DocumentoAppService = Resolve<IDocumentoAppService>();
            TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoAppService>();
        }
    }
}
