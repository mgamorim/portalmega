﻿using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class AssociacaoAppServiceTestBase : AppTestBase
    {
        public readonly IAssociacaoAppService AssociacaoAppService;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly IEmpresaAppService EmpresaAppService;
        public readonly IClienteEZAppService ClienteEzAppService;
        public readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public AssociacaoAppServiceTestBase()
        {
            AssociacaoAppService = Resolve<IAssociacaoAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            EmpresaAppService = Resolve<IEmpresaAppService>();
            ClienteEzAppService = Resolve<IClienteEZAppService>();
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }
    }
}
