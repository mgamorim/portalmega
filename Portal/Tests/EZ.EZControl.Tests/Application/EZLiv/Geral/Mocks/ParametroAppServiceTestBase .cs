﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class ParametroAppServiceTestBase : AppTestBase
    {
        protected readonly IParametroEZLivAppService ParametroEZLivAppService;
        public ParametroAppServiceTestBase()
        {
            ParametroEZLivAppService = Resolve<IParametroEZLivAppService>();
        }

        protected void CreateTestParametros()
        {
            UsingDbContext(context =>
            {
                context.ParametroEZLiv.Add(CreateParametroEntity());
            });
        }

        private ParametroEZLiv CreateParametroEntity()
        {
            return new ParametroEZLiv
            {
                AlturaMaximaPx = 100,
                ExtensoesImagem = "jpg;png;gif",
                ExtensoesDocumento = "doc;docx;pdf;xlsx",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

    }
}
