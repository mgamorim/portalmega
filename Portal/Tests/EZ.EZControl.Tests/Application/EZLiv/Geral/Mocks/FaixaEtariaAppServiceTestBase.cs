﻿using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class FaixaEtariaAppServiceTestBase : AppTestBase
    {
        public readonly IFaixaEtariaAppService FaixaEtariaAppService;

        public FaixaEtariaAppServiceTestBase()
        {
            FaixaEtariaAppService = Resolve<IFaixaEtariaAppService>();
        }
    }
}
