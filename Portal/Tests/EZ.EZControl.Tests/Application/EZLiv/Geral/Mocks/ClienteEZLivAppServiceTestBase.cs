﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class ClienteEZLivAppServiceTestBase : AppTestBase
    {
        public readonly IClienteEZLivAppService ClienteEZLivAppService;
        private readonly IClienteAppService ClienteAppService;
        private readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public ClienteEZLivAppServiceTestBase()
        {
            ClienteEZLivAppService = Resolve<IClienteEZLivAppService>();
            ClienteAppService = Resolve<IClienteAppService>();
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }

        private async Task<GrupoPessoaInput> CreateGrupoPessoa()
        {
            var grupo = new GrupoPessoaInput
            {
                IsActive = true,
                Descricao = "Grupo Cliente",
                Nome = "Grupo Cliente"
            };

            return await GrupoPessoaAppService.SaveAndReturnEntity(grupo);
        }

        private ClienteInput CreateClienteInput(
            string nome,
            string nomeFantasia,
            string razaoSocial,
            TipoPessoaEnum tipoPessoa,
            string observacao)
        {
            var input = new ClienteInput();
            GrupoPessoaInput grupo = CreateGrupoPessoa().Result;

            input.Pessoa = new PessoaInput();
            input.IsActive = true;
            input.Pessoa.GrupoPessoaId = grupo.Id;
            input.Pessoa.TipoPessoa = tipoPessoa;
            input.Pessoa.Observacao = observacao;

            if (tipoPessoa == TipoPessoaEnum.Fisica)
            {
                input.PessoaFisica = new PessoaFisicaInputAux();
                input.PessoaFisica.Nome = nome;
            }
            else
            {
                input.PessoaJuridica = new PessoaJuridicaInputAux();
                input.PessoaJuridica.NomeFantasia = nomeFantasia;
                input.PessoaJuridica.RazaoSocial = razaoSocial;
            }

            return input;
        }

        public async Task<ClienteInput> GetClientePessoaFisica()
        {
            var input = CreateClienteInput("Maria José", string.Empty, string.Empty, TipoPessoaEnum.Fisica, "Teste Observação");
            return await ClienteAppService.SaveAndReturnEntity(input);
        }

        public async Task<ClienteInput> GetClientePessoaJuridica()
        {
            var input = CreateClienteInput(string.Empty, "EZSoft", "EZSoft", TipoPessoaEnum.Juridica, "Teste Observação");
            return await ClienteAppService.SaveAndReturnEntity(input);
        }
    }
}
