﻿using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class OperadoraAppServiceTestBase : AppTestBase
    {
        protected readonly IProdutoDePlanoDeSaudeAppService ProdutoDePlanoDeSaudeAppService;
        protected readonly IPlanoDeSaudeAppService PlanoDeSaudeAppService;
        protected readonly IOperadoraAppService OperadoraAppService;
        public readonly IEmpresaAppService EmpresaAppService;
        public readonly IClienteEZAppService ClienteEzAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public OperadoraAppServiceTestBase()
        {
            this.ProdutoDePlanoDeSaudeAppService = Resolve<IProdutoDePlanoDeSaudeAppService>();
            this.PlanoDeSaudeAppService = Resolve<IPlanoDeSaudeAppService>();
            this.OperadoraAppService = Resolve<IOperadoraAppService>();
            this.EmpresaAppService = Resolve<IEmpresaAppService>();
            this.ClienteEzAppService = Resolve<IClienteEZAppService>();
            this.GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
            this.PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
        }

        protected void CreateTestPlanosDeSaude()
        {
            UsingDbContext(context =>
            {
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.260/16-1", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Rubi207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.388/16-7", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.387/16-9", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante217, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "3x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e internação"));
            });
        }

        protected void CreateTestProdutosDePlanoDeSaude()
        {
            UsingDbContext(context =>
            {
                var plano = CreatePlanoDeSaudeEntity("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual,
                    AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207,
                    SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há");

                context.ProdutosDePlanoDeSaude.Add(CreateProdutoDePlanoDeSaudeEntity(plano, "Especial 100", 250, "O melhor custo"));
            });
        }

        protected void CreateNatureza()
        {
            UsingDbContext(context =>
            {
                context.Naturezas.Add(CreateNaturezaEntity("Natureza"));
            });
        }

        protected void CreateUnidade()
        {
            UsingDbContext(context =>
            {
                context.UnidadeMedidas.Add(CreateUnidadeDeMedidaEntity("Metro", UnidadePadraoEnum.Metro, 250));
                context.UnidadeMedidas.Add(CreateUnidadeDeMedidaEntity("Quilo", UnidadePadraoEnum.Quilograma, 100));
            });
        }

        private Natureza CreateNaturezaEntity(string descricao)
        {
            return new Natureza
            {
                Descricao = descricao,
                IsActive = true
            };
        }

        private UnidadeMedida CreateUnidadeDeMedidaEntity(string descricao, UnidadePadraoEnum unidadePadrao, decimal valor)
        {
            return new UnidadeMedida
            {
                Descricao = descricao,
                IsActive = true,
                UnidadePadrao = unidadePadrao,
                Valor = valor
            };
        }


        private PlanoDeSaude CreatePlanoDeSaudeEntity(string numeroDoRegistro, AbrangenciaDoPlanoEnum abrangencia, AcomodacaoEnum acomodacao,
                                                      PlanoDeSaudeAnsEnum planoAns, SegmentacaoAssistencialDoPlanoEnum segmentacao, string descricaoDoReembolso, bool reembolso = false)
        {
            return new PlanoDeSaude
            {
                IsActive = true,
                NumeroDeRegistro = numeroDoRegistro,
                Reembolso = reembolso,
                DescricaoDoReembolso = descricaoDoReembolso,
                Abrangencia = abrangencia,
                Acomodacao = acomodacao,
                PlanoDeSaudeAns = planoAns,
                SegmentacaoAssistencial = segmentacao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ProdutoDePlanoDeSaude CreateProdutoDePlanoDeSaudeEntity(PlanoDeSaude plano, string nome, decimal valor, string descricao)
        {
            return new ProdutoDePlanoDeSaude
            {
                IsActive = true,
                PlanoDeSaude = plano,
                Nome = nome,
                Valor = valor,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
