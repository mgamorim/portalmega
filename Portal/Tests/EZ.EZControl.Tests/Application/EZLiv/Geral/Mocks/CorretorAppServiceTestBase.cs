﻿using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class CorretorAppServiceTestBase : AppTestBase
    {
        public readonly ICorretorAppService CorretorAppService;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly ICorretoraAppService CorretoraAppService;
        public readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public CorretorAppServiceTestBase()
        {
            CorretorAppService = Resolve<ICorretorAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            CorretoraAppService = Resolve<ICorretoraAppService>();
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }
    }
}
