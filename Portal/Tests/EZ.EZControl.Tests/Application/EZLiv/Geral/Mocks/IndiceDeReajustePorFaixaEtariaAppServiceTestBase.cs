﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class IndiceDeReajustePorFaixaEtariaAppServiceTestBase : AppTestBase
    {
        public readonly IIndiceDeReajustePorFaixaEtariaAppService IndiceDeReajustePorFaixaEtariaAppService;
        public readonly IFaixaEtariaAppService FaixaEtariaAppService;
        public readonly IOperadoraAppService OperadoraAppService;

        public IndiceDeReajustePorFaixaEtariaAppServiceTestBase()
        {
            this.IndiceDeReajustePorFaixaEtariaAppService = Resolve<IIndiceDeReajustePorFaixaEtariaAppService>();
            this.OperadoraAppService = Resolve<IOperadoraAppService>();
            this.FaixaEtariaAppService = Resolve<IFaixaEtariaAppService>();
        }

        protected void CreateOperadora()
        {
            UsingDbContext(context =>
            {
                context.Operadoras.Add(CreateOperadora("Amil", "33698-AB", "Amil SA", "Amil Operadora"));
            });
        }

        protected void CreateFaixaEtaria()
        {
            UsingDbContext(context =>
            {
                context.FaixasEtarias.Add(CreateFaixaEtariaEntity("De 0 a 18", 0, 18));
                context.FaixasEtarias.Add(CreateFaixaEtariaEntity("De 19 a 25", 19, 25));
            });
        }

        private Operadora CreateOperadora(string nome, string codigoAns, string razaoSocial, string nomeFantasia)
        {
            return new Operadora
            {
                IsActive = true,
                Nome = nome,
                CodigoAns = codigoAns,
                PessoaJuridica = new PessoaJuridica
                {
                    RazaoSocial = razaoSocial,
                    NomeFantasia = nomeFantasia
                }
            };
        }

        private FaixaEtaria CreateFaixaEtariaEntity(string descricao, int idadeInicial, int idadeFinal)
        {
            return new FaixaEtaria
            {
                Descricao = descricao,
                IsActive = true,
                IdadeInicial = idadeInicial,
                IdadeFinal = idadeFinal,
                TenantId = AbpSession.TenantId.Value,
            };
        }
    }
}
