﻿using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class VigenciaAppServiceTestBase : AppTestBase
    {
        public readonly IVigenciaAppService vigenciaAppService;

        public VigenciaAppServiceTestBase()
        {
            vigenciaAppService = Resolve<IVigenciaAppService>();
        }
    }
}
