﻿using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class ChancelaAppServiceTestBase : AppTestBase
    {
        public readonly IChancelaAppService ChancelaAppService;

        public ChancelaAppServiceTestBase()
        {
            ChancelaAppService = Resolve<IChancelaAppService>();
        }
    }
}
