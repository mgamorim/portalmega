﻿using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class ValorPorFaixaEtariaAppServiceTestBase : AppTestBase
    {
        public readonly IValorPorFaixaEtariaAppService ValorPorFaixaEtariaAppService;
        protected readonly IProdutoDePlanoDeSaudeAppService ProdutoDePlanoDeSaudeAppService;
        protected readonly IPlanoDeSaudeAppService PlanoDeSaudeAppService;
        protected readonly IUnidadeMedidaAppService UnidadeMedidaAppService;
        protected readonly INaturezaAppService NaturezaAppService;
        protected readonly IContratoAppService ContratoAppService;
        protected readonly IFaixaEtariaAppService FaixaEtariaAppService;

        public ValorPorFaixaEtariaAppServiceTestBase()
        {
            ValorPorFaixaEtariaAppService = Resolve<IValorPorFaixaEtariaAppService>();
            this.ProdutoDePlanoDeSaudeAppService = Resolve<IProdutoDePlanoDeSaudeAppService>();
            this.PlanoDeSaudeAppService = Resolve<IPlanoDeSaudeAppService>();
            this.UnidadeMedidaAppService = Resolve<IUnidadeMedidaAppService>();
            this.NaturezaAppService = Resolve<INaturezaAppService>();
            this.ContratoAppService = Resolve<IContratoAppService>();
            this.FaixaEtariaAppService = Resolve<IFaixaEtariaAppService>();
        }

        protected void CreatePlanoDeSaude()
        {
            UsingDbContext(context =>
            {
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.260/16-1", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Rubi207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.388/16-7", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante207, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação", true));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.387/16-9", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante217, SegmentacaoAssistencialDoPlanoEnum.HospitalarAmbulatorialObstetricia, "3x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e internação", true));
            });
        }

        protected void CreateOperadora()
        {
            UsingDbContext(context =>
            {
                context.Operadoras.Add(CreateOperadora("Amil", "33698-AB", "Amil SA", "Amil Operadora"));
            });
        }

        protected void CreateNatureza()
        {
            UsingDbContext(context =>
            {
                context.Naturezas.Add(CreateNaturezaEntity("Natureza"));
            });
        }

        protected void CreateUnidade()
        {
            UsingDbContext(context =>
            {
                context.UnidadeMedidas.Add(CreateUnidadeDeMedidaEntity("Metro", UnidadePadraoEnum.Metro, 250));
                context.UnidadeMedidas.Add(CreateUnidadeDeMedidaEntity("Quilo", UnidadePadraoEnum.Quilograma, 100));
            });
        }

        private PlanoDeSaude CreatePlanoDeSaudeEntity(string numeroDoRegistro, AbrangenciaDoPlanoEnum abrangencia, AcomodacaoEnum acomodacao,
                                                      PlanoDeSaudeAnsEnum planoAns, SegmentacaoAssistencialDoPlanoEnum segmentacao, string descricaoDoReembolso, bool reembolso = false)
        {
            return new PlanoDeSaude
            {
                IsActive = true,
                NumeroDeRegistro = numeroDoRegistro,
                DescricaoDoReembolso = descricaoDoReembolso,
                Reembolso = reembolso,
                Abrangencia = abrangencia,
                Acomodacao = acomodacao,
                PlanoDeSaudeAns = planoAns,
                SegmentacaoAssistencial = segmentacao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Operadora CreateOperadora(string nome, string codigoAns, string razaoSocial, string nomeFantasia)
        {
            return new Operadora
            {
                IsActive = true,
                Nome = nome,
                CodigoAns = codigoAns,
                PessoaJuridica = new PessoaJuridica
                {
                    RazaoSocial = razaoSocial,
                    NomeFantasia = nomeFantasia
                }
            };
        }

        private Natureza CreateNaturezaEntity(string descricao)
        {
            return new Natureza
            {
                Descricao = descricao,
                IsActive = true
            };
        }

        private UnidadeMedida CreateUnidadeDeMedidaEntity(string descricao, UnidadePadraoEnum unidadePadrao, decimal valor)
        {
            return new UnidadeMedida
            {
                Descricao = descricao,
                IsActive = true,
                UnidadePadrao = unidadePadrao,
                Valor = valor
            };
        }
    }
}
