﻿using EZ.EZControl.Services.EZLiv.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class ContratoAppServiceTestBase : AppTestBase
    {
        public readonly IContratoAppService ContratoAppService;

        public ContratoAppServiceTestBase()
        {
            ContratoAppService = Resolve<IContratoAppService>();
        }
    }
}
