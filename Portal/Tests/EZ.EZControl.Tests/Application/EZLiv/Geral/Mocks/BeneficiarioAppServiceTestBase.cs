﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Services.EZLiv.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks
{
    public class BeneficiarioAppServiceTestBase : AppTestBase
    {
        public readonly IBeneficiarioBaseAppService BeneficiarioBaseAppService;
        public readonly IProponenteTitularAppService ProponenteTitularAppService;
        public readonly IDependenteAppService DependenteAppService;
        public readonly IResponsavelAppService ResponsavelAppService;
        public readonly IRepository<ProponenteTitular> ProponenteTitularRepository;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IDocumentoAppService DocumentoAppService;
        public readonly ITipoDeDocumentoAppService TipoDeDocumentoAppService;
        public readonly ICorretorAppService CorretorAppService;

        public BeneficiarioAppServiceTestBase()
        {
            BeneficiarioBaseAppService = Resolve<IBeneficiarioBaseAppService>();
            ProponenteTitularAppService = Resolve<IProponenteTitularAppService>();
            DependenteAppService = Resolve<IDependenteAppService>();
            ProponenteTitularRepository = Resolve<IRepository<ProponenteTitular>>();
            ResponsavelAppService = Resolve<IResponsavelAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            DocumentoAppService = Resolve<IDocumentoAppService>();
            TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoAppService>();
            CorretorAppService = Resolve<ICorretorAppService>();
        }
    }
}
