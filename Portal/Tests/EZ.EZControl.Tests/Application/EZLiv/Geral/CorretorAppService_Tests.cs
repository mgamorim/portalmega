﻿using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.UI;
using EZ.EZControl.Configuration.Host;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Corretora;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class CorretorAppService_Tests : CorretorAppServiceTestBase
    {
        private readonly IRepository<Documento> _documentoRepository;
        private readonly IRepository<TipoDeDocumento> _tipoDeDocumentoRepository;
        private readonly IRepository<PessoaFisica> _pessoaRepository;
        private readonly IRepository<PessoaJuridica> _pessoaJuridicaRepository;
        private readonly IHostSettingsAppService _hostSettingsAppService;
        private readonly ISettingManager _settingManager;

        public CorretorAppService_Tests()
        {
            _documentoRepository = Resolve<IRepository<Documento>>();
            _tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
            _pessoaRepository = Resolve<IRepository<PessoaFisica>>();
            _pessoaJuridicaRepository = Resolve<IRepository<PessoaJuridica>>();
            _hostSettingsAppService = Resolve<IHostSettingsAppService>();
            _settingManager = Resolve<ISettingManager>();
        }

        private Documento CreateDocumento(PessoaFisica pessoa, TipoDeDocumento tipoDeDocumento, string numero = null,
           DateTime? dataDeExpedicao = null, string orgao = null)
        {
            var doc = new Documento(pessoa, tipoDeDocumento, numero, orgao, dataDeExpedicao)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
            return doc;
        }

        private TipoDeDocumento CreateTipoDeDocumento(string descricao, string mascara,
           TipoDeDocumentoEnum tipoDeDocumentoFixo)
        {
            return new TipoDeDocumento
            {
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                Descricao = descricao,
                TipoDeDocumentoFixo = tipoDeDocumentoFixo,
                Mascara = mascara
            };
        }

        private async Task<PessoaJuridicaInput> SavePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            PessoaJuridicaInput pessoaJuridicaInput = new PessoaJuridicaInput
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial,
                TipoPessoa = TipoPessoaEnum.Juridica,
                IsActive = true
            };

            var result = await base.PessoaJuridicaAppService.SaveAndReturnEntity(pessoaJuridicaInput);

            return result;
        }

        private async Task<PessoaFisicaInput> SavePessoaFisica(string nome, string nacionalidade, DateTime dataDeNascimento, SexoEnum sexo, EstadoCivilEnum estadoCivil, GrupoPessoaInput grupo)
        {

            PessoaFisicaInput pessoaFisicaInput = new PessoaFisicaInput
            {
                Nome = nome,
                IsActive = true,
                Nacionalidade = nacionalidade,
                DataDeNascimento = dataDeNascimento,
                Sexo = sexo,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = estadoCivil,
                GrupoPessoaId = grupo.Id
            };

            var result = await base.PessoaFisicaAppService.SaveAndReturnEntity(pessoaFisicaInput);
            return result;
        }

        private async Task<CorretorInput> SaveCorretor(PessoaFisicaInput pf)
        {
            return await base.CorretorAppService.SaveAndReturnEntity(new CorretorInput
            {
                IsActive = true,
                Pessoa = pf
            });
        }

        private async Task<IdInput> SaveCorretora()
        {

            var pj = await SavePessoaJuridica("Bem Benefícios", "Bem Benefícios LTDA");
            var gp = await SaveGrupoPessoa("Corretoras", "slugCorretoras");
            var pf = await SavePessoaFisica("Wescley Arruda", "Brasileiro", DateTime.Now.AddYears(-20), SexoEnum.Masculino, EstadoCivilEnum.Casado, gp);

            //Adicionar Documento para Pessoa Fisica do tipo CPF
            var pessoaTitular = _pessoaRepository.Get(pf.Id);
            //Documentos
            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);
            var documentoCpf = this.CreateDocumento(pessoaTitular, tipoDeDocumentoCpf, "967.098.583-83");
            _documentoRepository.InsertOrUpdate(documentoCpf);

            var corretor = await SaveCorretor(pf);

            CorretoraInput corretoraInput = new CorretoraInput
            {
                IsActive = true,
                Nome = "Corretora Teste",
                PessoaJuridica = pj,
                PessoaJuridicaId = pj.Id,
                Corretor = corretor,
                CorretorId = corretor.Id,
                //Slug = "SlugCorretoradeTeste",
                ParametroPagSeguro = new ParametroPagSeguroInput
                {
                    Token = Guid.NewGuid().ToString(),
                    Email = "teste@gmail.com",
                    PessoaJuridicaId = pj.Id,
                    AppId = "1234@##$",
                    AppKey = "12@#$$34567890"
                }
            };
            var result = await base.CorretoraAppService.Save(corretoraInput);
            return result;
        }

        private async Task<GrupoPessoaInput> SaveGrupoPessoa(string descricao, string slug)
        {
            GrupoPessoaInput grupoPessoaInput = new GrupoPessoaInput
            {
                Descricao = descricao,
                Nome = descricao,
                Slug = slug,
                TipoDePessoa = TipoPessoaEnum.Fisica,
                IsActive = true
            };

            var result = await base.GrupoPessoaAppService.SaveAndReturnEntity(grupoPessoaInput);

            return result;
        }

        private async Task<CorretorInput> CreateCorretorPfAndTestAsync(string nome, string nacionalidade, DateTime dataDeNascimento, SexoEnum sexo, EstadoCivilEnum estadoCivil, string cpf)
        {
            //Tipo De Documento
            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);
            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoCpf);

            var gp = await SaveGrupoPessoa("PF", "slugPF");

            //Pessoa Fisica para Corretor
            var pf = await SavePessoaFisica(nome, nacionalidade, dataDeNascimento, sexo, estadoCivil, gp);
            //Adicionar Documento para Pessoa Fisica do tipo CPF
            var pessoaTitular = _pessoaRepository.Get(pf.Id);

            //Documentos
            var documentoCpf = this.CreateDocumento(pessoaTitular, tipoDeDocumentoCpf, cpf);
            _documentoRepository.InsertOrUpdate(documentoCpf);

            return await SaveCorretor(pf);


        }

        private async Task CreateCorretorPjAndTestAsync(string razaoSocial, string nomeFantasia)
        {
            // var corretoraId = await SaveCorretora();

            // var corretora = await base.CorretoraAppService.GetById(corretoraId);
            //  List<CorretoraInput> corretoras = new List<CorretoraInput>();

            //  corretoras.Add(corretora);

            var pj = await SavePessoaJuridica(razaoSocial, nomeFantasia);
            var pessoa = _pessoaJuridicaRepository.Get(pj.Id);

            await base.CorretorAppService.Save(new CorretorInput
            {
                IsActive = true,
                Pessoa = pj
                //Corretoras = corretoras,
                //Pessoa = new PessoaJuridicaInput
                //{
                //    IsActive = true,
                //    TipoPessoa = TipoPessoaEnum.Juridica,
                //    RazaoSocial = razaoSocial,
                //    NomeFantasia = nomeFantasia
                //}
            });

        }

        [Fact]
        public async Task Should_Create_Corretor_PF()
        {
            await CreateCorretorPfAndTestAsync("Jupira Cardoso", "Angolana", DateTime.Now.AddYears(-50), SexoEnum.Feminino, EstadoCivilEnum.Solteiro, "147.289.456-18");
            await CreateCorretorPfAndTestAsync("Ana Maria Rosário", "Chilena", DateTime.Now.AddYears(-35), SexoEnum.Feminino, EstadoCivilEnum.Casado, "981.383.724-19");
            await CreateCorretorPfAndTestAsync("Rogério Almeida Souza", "Brasileiro", DateTime.Now.AddYears(-20), SexoEnum.Masculino, EstadoCivilEnum.Divorciado, "717.884.171-00");
        }

        [Fact]
        public async Task Should_Create_Corretor_PJ()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await CreateCorretorPjAndTestAsync("Sérgio Faro Corretor", "SF Corretor SA");
                await CreateCorretorPjAndTestAsync("RJMID Planos de Saúde", "RJMID Planos de Saúde Eirelli");
            });
        }

        [Fact]
        public async Task Delete_Corretor_PF()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var corretor = await CreateCorretorPfAndTestAsync("Regina Mendonça", "Uruguaia", DateTime.Now.AddYears(-36), SexoEnum.Feminino, EstadoCivilEnum.Casado, "428.581.827-28");
                await base.CorretorAppService.Delete(new IdInput(corretor.Id));
                await uow.Current.SaveChangesAsync();
                await base.CorretorAppService.GetById(new IdInput(corretor.Id)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        [Fact]
        public async Task Delete_Corretor_PJ()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await CreateCorretorPjAndTestAsync("Mosaíco Corretor", "M Corretor Eirelli");
                await base.CorretorAppService.Delete(new IdInput(1));
                await uow.Current.SaveChangesAsync();
                await base.CorretorAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}
