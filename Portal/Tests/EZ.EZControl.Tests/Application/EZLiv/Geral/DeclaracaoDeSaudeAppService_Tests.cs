﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using EZ.EZControl.Domain.EZLiv.Enums;
using EZ.EZControl.Domain.EZLiv.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZLiv.Geral.DeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.Geral.ItemDeDeclaracaoDeSaude;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.EZLiv.SubtipoPessoa.Responsavel;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class DeclaracaoDeSaudeAppService_Tests : DeclaracaoDeSaudeAppServiceTestBase
    {
        private readonly IRepository<Documento> _documentoRepository;
        private readonly IRepository<TipoDeDocumento> _tipoDeDocumentoRepository;
        private readonly IRepository<Pessoa> _pessoaRepository;
        private readonly IRepository<EnderecoEletronico> _enderecoEletronicoRepository;
        private readonly IRepository<Telefone> _telefoneRepository;
        private readonly IRepository<Endereco> _enderecoRepository;
        private readonly IRepository<Cidade> _cidadeRepository;
        private readonly IRepository<TipoDeLogradouro> _tipoDeLogradouroRepository;
        private readonly IRepository<Corretora> _corretoraRepository;
        private readonly IRepository<ProponenteTitular> _proponenteTitularRepository;
        private readonly IRepository<PessoaFisica> _pessoaFisicaRepository;

        public DeclaracaoDeSaudeAppService_Tests()
        {
            _documentoRepository = Resolve<IRepository<Documento>>();
            _tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
            _pessoaRepository = Resolve<IRepository<Pessoa>>();
            _enderecoEletronicoRepository = Resolve<IRepository<EnderecoEletronico>>();
            _telefoneRepository = Resolve<IRepository<Telefone>>();
            _enderecoRepository = Resolve<IRepository<Endereco>>();
            _cidadeRepository = Resolve<IRepository<Cidade>>();
            _tipoDeLogradouroRepository = Resolve<IRepository<TipoDeLogradouro>>();
            _corretoraRepository = Resolve<IRepository<Corretora>>();
            _proponenteTitularRepository = Resolve<IRepository<ProponenteTitular>>();
            _pessoaFisicaRepository = Resolve<IRepository<PessoaFisica>>();

            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private TipoDeDocumento CreateTipoDeDocumento(string descricao, string mascara,
            TipoDeDocumentoEnum tipoDeDocumentoFixo)
        {
            return new TipoDeDocumento
            {
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                Descricao = descricao,
                TipoDeDocumentoFixo = tipoDeDocumentoFixo,
                Mascara = mascara
            };
        }

        private Telefone CreateTelefone(Pessoa pessoa, TipoTelefoneEnum tipoDeTelefone, string ddi, string ddd,
            string numero, string ramal = null)
        {
            var tel = new Telefone(pessoa, tipoDeTelefone, ddd, numero, ddi, ramal);
            tel.EmpresaId = EmpresaIdDefaultTest;
            return tel;
        }

        private Documento CreateDocumento(Pessoa pessoa, TipoDeDocumento tipoDeDocumento, string numero = null,
            DateTime? dataDeExpedicao = null, string orgao = null)
        {
            var doc = new Documento(pessoa, tipoDeDocumento, numero, orgao, dataDeExpedicao);
            doc.EmpresaId = EmpresaIdDefaultTest;
            return doc;
        }

        private EnderecoEletronico CreateEnderecoEletronico(Pessoa pessoa,
            TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
        {
            var end = new EnderecoEletronico(pessoa, tipoDeEnderecoEletronico, endereco);
            end.EmpresaId = EmpresaIdDefaultTest;
            return end;
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(string descricao)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao
            };
        }

        private Cidade CreateCidade()
        {
            var cidade = _cidadeRepository.GetAll().FirstOrDefault(x => x.Nome == "Rio de Janeiro");

            if (cidade == null)
            {
                cidade = new Cidade()
                {
                    CodigoIBGE = "33301",
                    Estado = CreateEstado(),
                    Nome = "Rio de Janeiro"
                };
            };

            return cidade;
        }

        private Estado CreateEstado()
        {
            return new Estado
            {
                Nome = "Rio de Janeiro",
                Capital = "Rio de Janeiro",
                Sigla = "RJ",
                CodigoIBGE = "33",
                Pais = CreatePais()
            };
        }

        private Pais CreatePais()
        {
            return new Pais
            {
                CodigoISO = "BRA",
                Nacionalidade = "Brasileira",
                Nome = "Brasil"
            };
        }

        private Corretora CreateCorretora(string razaoSocial, string nomeFantasia)
        {
            var pessoaJuridica = base.PessoaJuridicaAppService.SaveAndReturnEntity(new PessoaJuridicaInput
            {
                RazaoSocial = razaoSocial,
                NomeFantasia = nomeFantasia,
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Juridica
            }).Result;

            return new Corretora()
            {
                PessoaJuridicaId = pessoaJuridica.Id
            };
        }

        private async Task<ProfissaoInput> CreateProfissaoInput(string codigo, string titulo)
        {
            var profissoes = await ProfissaoAppService.GetProfissoesPaginado(new GetProfissaoInput()
            {
                Codigo = codigo,
                Titulo = titulo,
                MaxResultCount = 10,
                SkipCount = 0
            });

            if (profissoes.TotalCount == 0)
            {
                return await ProfissaoAppService.SaveAndReturnEntity(new ProfissaoInput()
                {
                    Codigo = codigo,
                    Titulo = titulo
                });
            }
            else
            {
                return profissoes.Items[0].MapTo<ProfissaoInput>();
            }
        }

        private Endereco CreateEndereco(Pessoa pessoa, TipoEnderecoEnum tipoDeEndereco,
            TipoDeLogradouro tipoDeLogradouro, Cidade cidade,
            string descricao, string logradouro, string numero, string complemento, string bairro, string cep,
            string referencia)
        {
            return new Endereco(pessoa, descricao, tipoDeEndereco, tipoDeLogradouro, logradouro, numero, complemento,
                bairro, cep, cidade, referencia)
            {
                EnderecoParaEntrega = true,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ResponsavelInput CreateAndReturnResponsavel(string nomeDaMaeTitular)
        {
            var pfResponsavel = base.PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nomeDaMaeTitular,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-50),
                Sexo = SexoEnum.Feminino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado
            }).Result;

            return new ResponsavelInput
            {
                PessoaFisica = pfResponsavel,
                TipoDeResponsavel = TipoDeResponsavelEnum.PaiMae,
                IsActive = true
            };
        }

        private DependenteInput CreateAndReturnDependente(string nomeDoDependente, GrauDeParentescoEnum grauDeParentesco,
            string nomeDaMaeDependente, int corretoraId, int profissaoId, int estadoId)
        {
            var pfDependente = base.PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nomeDoDependente,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-20),
                Sexo = SexoEnum.Masculino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Solteiro,
            }).Result;

            return new DependenteInput
            {
                PessoaFisica = pfDependente,
                GrauDeParentesco = grauDeParentesco,
                NomeDaMae = nomeDaMaeDependente,
                TipoDeBeneficiario = TipoDeBeneficiarioEnum.Dependente,
                CorretoraId = corretoraId,
                ProfissaoId = profissaoId,
                EstadoId = estadoId
            };
        }

        private ProponenteTitularInput CreateAndReturnTitular(IActiveUnitOfWork uow, string nomeDoTitular, string nomeDaMaeTitular,
            ResponsavelInput responsavel = null, List<DependenteInput> dependentes = null)
        {
            var pfTitular = base.PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nomeDoTitular,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-40),
                Sexo = SexoEnum.Masculino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado
            }).Result;

            var pessoaTitular = _pessoaRepository.Get(pfTitular.Id);

            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);
            var tipoDeDocumentoRg = this.CreateTipoDeDocumento("RG", "99.9999.999-9", TipoDeDocumentoEnum.Rg);

            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoCpf);
            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoRg);

            var documentoCpf = this.CreateDocumento(pessoaTitular, tipoDeDocumentoCpf, "67515814675");
            var documentoRg = this.CreateDocumento(pessoaTitular, tipoDeDocumentoRg, "283068668", DateTime.Now,
                "DETRAN-RJ");

            _documentoRepository.InsertOrUpdate(documentoCpf);
            _documentoRepository.InsertOrUpdate(documentoRg);

            var enderecoEletronico = this.CreateEnderecoEletronico(pessoaTitular, TipoDeEnderecoEletronicoEnum.EmailPrincipal,
                "ezcontrol@ezsoft.com.br");
            _enderecoEletronicoRepository.InsertOrUpdate(enderecoEletronico);

            var telefone = this.CreateTelefone(pessoaTitular, TipoTelefoneEnum.Celular, "55", "21", "964264040");
            _telefoneRepository.InsertOrUpdate(telefone);

            var cidade = this.CreateCidade();
            _cidadeRepository.InsertOrUpdate(cidade);

            var tipoDeLogradouro = CreateTipoDeLogradouro("Avenida");
            _tipoDeLogradouroRepository.InsertOrUpdate(tipoDeLogradouro);

            var endereco = this.CreateEndereco(pessoaTitular, TipoEnderecoEnum.Residencial, tipoDeLogradouro, cidade,
                "Residencial", "Américas", "1000", "Sala 3", "Barra da Tijuca", "21370888",
                "Próximo ao supermercado");

            _enderecoRepository.InsertOrUpdate(endereco);

            var pessoaFisica = _pessoaFisicaRepository.Get(pessoaTitular.Id);

            var corretora = this.CreateCorretora("Corretora 1", "Corretora 1");

            _corretoraRepository.InsertOrUpdate(corretora);
            uow.SaveChanges();

            var profissaoInput = CreateProfissaoInput("001", " Analista de Sistemas").Result;

            var proponenteTitular = new ProponenteTitularInput
            {
                PessoaFisica = pfTitular,
                IsActive = true,
                NomeDaMae = nomeDaMaeTitular,
                TipoDeBeneficiario = TipoDeBeneficiarioEnum.Titular,
                CorretoraId = corretora.Id,
                ProfissaoId = profissaoInput.Id,
                EstadoId = cidade.EstadoId
            };

            if (responsavel != null)
            {
                var pessoaResponsavel = _pessoaRepository.Get(responsavel.PessoaFisica.Id);
                var documentoResponsavelCpf = this.CreateDocumento(pessoaResponsavel, tipoDeDocumentoCpf, "67515814678");
                _documentoRepository.InsertOrUpdate(documentoResponsavelCpf);
                proponenteTitular.Responsavel = responsavel;
            }

            if (dependentes != null)
            {
                foreach (var dependente in dependentes)
                {
                    var pessoaDependente = _pessoaRepository.Get(dependente.PessoaFisica.Id);
                    var documentoDependenteCpf = this.CreateDocumento(pessoaDependente, tipoDeDocumentoCpf,
                        "012345678910");
                    _documentoRepository.InsertOrUpdate(documentoDependenteCpf);
                }

                proponenteTitular.Dependentes = dependentes;
            }

            return proponenteTitular;
        }

        private ItemDeDeclaracaoDeSaudeInput CreateItemDeDeclaracao(ProponenteTitularInput proponenteTitular, string pergunta)
        {
            return new ItemDeDeclaracaoDeSaudeInput
            {
                Pergunta = pergunta,
                IsActive = true
            };
        }

        [Fact]
        public async Task Should_Create_Declaracao_De_Saude_With_Titular()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = CreateAndReturnTitular(uow.Current, "Roberto Duran Ramalho", "Marlene Duran");

                var declaracaoDeSaude = new DeclaracaoDeSaudeInput
                {
                    Nome = "Declaração de Teste",
                    Titular = proponenteTitular,
                    IsActive = true,
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaudeInput>
                    {
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve bronquite?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem diabetes? Qual tipo?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve varizes?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Encontra-se internado ou em tratamento?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve doença congênita (desde que nasceu)? Alguma síndrome (Down, Marfan ou outra)?")
                    }
                };

                await uow.Current.SaveChangesAsync();
                await base.DeclaracaoDeSaudeAppService.Save(declaracaoDeSaude);
            });
        }

        [Fact]
        public async Task Should_Create_Declaracao_De_Saude_With_Titular_And_Dependentes()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var profissaoInput = CreateProfissaoInput("001", " Analista de Sistemas").Result;

                var cidade = CreateCidade();
                _cidadeRepository.InsertOrUpdate(cidade);

                var corretora = CreateCorretora("Corretora 2", "Corretora 2");
                _corretoraRepository.InsertOrUpdate(corretora);

                uow.Current.SaveChanges();

                var dependentes = new List<DependenteInput>
                {
                        CreateAndReturnDependente("Jorge Henrique Fagundes", GrauDeParentescoEnum.Filho, "Priscila Albuquerque", corretora.Id, profissaoInput.Id, cidade.EstadoId),
                        CreateAndReturnDependente("Almir Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes" , corretora.Id, profissaoInput.Id, cidade.EstadoId),
                        CreateAndReturnDependente("Pietro Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes" , corretora.Id, profissaoInput.Id, cidade.EstadoId)
                };

                var proponenteTitular = CreateAndReturnTitular(uow.Current, "Antonio Fagundes", "Leila Fagundes", null, dependentes);

                var declaracaoDeSaude = new DeclaracaoDeSaudeInput
                {
                    Nome = "Declaração de Teste",
                    IsActive = true,
                    Titular = proponenteTitular,
                    Dependentes = new List<DependenteInput>(),
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaudeInput>()
                };

                await uow.Current.SaveChangesAsync();
                var declaracao = await base.DeclaracaoDeSaudeAppService.SaveAndReturnEntity(declaracaoDeSaude);

                declaracao.Dependentes = dependentes;

                declaracao.ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaudeInput>
                {
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve bronquite?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve doença congênita (desde que nasceu)? Alguma síndrome (Down, Marfan ou outra)?")
                };

                await base.DeclaracaoDeSaudeAppService.Save(declaracao);
            });
        }

        [Fact]
        public async Task Delete_Declaracao_De_Saude_With_Titular()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = CreateAndReturnTitular(uow.Current, "Luiz Carvalho Mendonça", "Sibelli Mendonça");
                var declaracaoDeSaude = new DeclaracaoDeSaudeInput
                {
                    Nome = "Declaração de Teste",
                    Titular = proponenteTitular,
                    IsActive = true,
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaudeInput>
                    {
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve bronquite?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem diabetes? Qual tipo?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve varizes?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Encontra-se internado ou em tratamento?"),
                        this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve doença congênita (desde que nasceu)? Alguma síndrome (Down, Marfan ou outra)?")
                    }
                };

                await base.DeclaracaoDeSaudeAppService.Save(declaracaoDeSaude);
                await base.DeclaracaoDeSaudeAppService.Delete(new IdInput(1));
                await uow.Current.SaveChangesAsync();
                await base.DeclaracaoDeSaudeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        [Fact]
        public async Task Delete_Declaracao_De_Saude_With_Titular_And_Dependentes()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var profissaoInput = CreateProfissaoInput("002", " Diretor").Result;

                var cidade = CreateCidade();
                _cidadeRepository.InsertOrUpdate(cidade);

                var corretora = CreateCorretora("Corretora 2", "Corretora 2");
                _corretoraRepository.InsertOrUpdate(corretora);

                uow.Current.SaveChanges();
                var dependentes = new List<DependenteInput>
                {
                    CreateAndReturnDependente("Jorge Henrique Fagundes", GrauDeParentescoEnum.Filho, "Priscila Albuquerque", corretora.Id, profissaoInput.Id, cidade.EstadoId),
                    CreateAndReturnDependente("Almir Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes", corretora.Id, profissaoInput.Id, cidade.EstadoId),
                    CreateAndReturnDependente("Pietro Fagundes", GrauDeParentescoEnum.Neto, "Joana Fagundes", corretora.Id, profissaoInput.Id, cidade.EstadoId)
                };

                var proponenteTitular = CreateAndReturnTitular(uow.Current, "Antonio Fagundes", "Leila Fagundes", null, dependentes);

                var declaracaoDeSaude = new DeclaracaoDeSaudeInput
                {
                    Nome = "Declaração de Teste",
                    IsActive = true,
                    Titular = proponenteTitular,
                    Dependentes = new List<DependenteInput>(),
                    ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaudeInput>()
                };

                await uow.Current.SaveChangesAsync();
                var declaracao = await base.DeclaracaoDeSaudeAppService.SaveAndReturnEntity(declaracaoDeSaude);

                declaracao.Dependentes = dependentes;

                declaracao.ItensDeDeclaracaoDeSaude = new List<ItemDeDeclaracaoDeSaudeInput>
                {
                    this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve bronquite?"),
                    this.CreateItemDeDeclaracao(proponenteTitular, "Tem ou teve doença congênita (desde que nasceu)? Alguma síndrome (Down, Marfan ou outra)?")
                };

                await base.DeclaracaoDeSaudeAppService.Save(declaracao);
                await base.DeclaracaoDeSaudeAppService.Delete(new IdInput(1));
            });
        }
    }
}