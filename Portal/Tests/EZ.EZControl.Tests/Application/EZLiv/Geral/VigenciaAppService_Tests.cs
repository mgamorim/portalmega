﻿using EZ.EZControl.Dto.EZLiv.Geral.Vigencia;
using EZ.EZControl.Tests.Application.EZLiv.Geral.Mocks;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZLiv.Geral
{
    public class VigenciaAppService_Tests : VigenciaAppServiceTestBase
    {
        public VigenciaAppService_Tests()
        {
        }

        private VigenciaInput CreateVigencia()
        {
            return new VigenciaInput
            {
                IsActive = true,
                NumeroDeBoletos = "20",
                DataDeVigencia = 1,
                DataInicialDaProposta = 20,
                DataFinalDaProposta = 18,
                DataDeFechamento = 6
            };
        }

        [Fact]
        public async Task Should_Create_Vigencia()
        {
            #region Assert
            await base.vigenciaAppService.Save(CreateVigencia());
            #endregion

            var teste = await base.vigenciaAppService.GetPaginado(new GetVigenciaInput());
        }

        //[Fact]
        //public async Task Should_Update_Vigencia()
        //{
        //    #region Arrange
        //    await base.VigenciaAppService.Save(CreateVigencia("19 A 23", 19, 23));
        //    #endregion

        //    #region Action
        //    var faixasEtarias = await base.VigenciaAppService.GetPaginado(new GetVigenciaInput
        //    {
        //        MaxResultCount = 1000,
        //        SkipCount = 0,
        //        Descricao = "19 A 23"
        //    });

        //    var vigenciaId = faixasEtarias.Items[0].Id;
        //    var vigencia = await base.VigenciaAppService.GetById(new IdInput(vigenciaId));

        //    vigencia.Descricao = "39 A 43";
        //    vigencia.IdadeInicial = 39;
        //    vigencia.IdadeFinal = 43;
        //    #endregion

        //    #region Assert
        //    await base.VigenciaAppService.Save(vigencia);
        //    #endregion
        //}

        //[Fact]
        //public async Task Delete_Vigencia()
        //{
        //    #region Arrange
        //    await base.VigenciaAppService.Save(CreateVigencia("44 A 48", 44, 48));
        //    #endregion

        //    #region Action

        //    var faixasEtarias = await base.VigenciaAppService.GetPaginado(new GetVigenciaInput()
        //    {
        //        MaxResultCount = 1000,
        //        SkipCount = 0,
        //        Descricao = "44 A 48"
        //    });

        //    var vigenciaId = faixasEtarias.Items[0].Id;

        //    #endregion

        //    #region Assert
        //    await base.VigenciaAppService.Delete(new IdInput(vigenciaId));
        //    await base.VigenciaAppService.GetById(new IdInput(vigenciaId)).ShouldThrowAsync<UserFriendlyException>();
        //    #endregion
        //}
    }
}
