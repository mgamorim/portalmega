﻿using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Services.Financeiro.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Financeiro.Geral.Mocks
{
    public class TipoDeDocumentoFinanceiroAppServiceTestBase : AppTestBase
    {
        protected readonly ITipoDeDocumentoFinanceiroAppService TipoDeDocumentoAppService;
        public TipoDeDocumentoFinanceiroAppServiceTestBase()
        {
            this.TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoFinanceiroAppService>();
        }

        protected void CreateTestTiposDeDocumento()
        {
            UsingDbContext(context =>
            {
                context.TiposDeDocumentoFinanceiro.Add(CreateTipoDeDocumento("Nota Fiscal Eletrônica", TipoDeDocumentoFinanceiroEnum.APagar));
                context.TiposDeDocumentoFinanceiro.Add(CreateTipoDeDocumento("Boleto", TipoDeDocumentoFinanceiroEnum.AReceber));
            });
        }

        private TipoDeDocumentoFinanceiro CreateTipoDeDocumento(string nome, TipoDeDocumentoFinanceiroEnum tipo)
        {
            return new TipoDeDocumentoFinanceiro
            {
                Nome = nome,
                Tipo = tipo,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}