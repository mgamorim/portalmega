﻿using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Financeiro.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Financeiro.Geral.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Financeiro.Geral.Mocks
{
    public class DocumentoFinanceiroAppServiceTestBase : AppTestBase
    {
        protected readonly IDocumentoAPagarAppService DocumentoAPagarAppService;
        protected readonly IDocumentoAReceberAppService DocumentoAReceberAppService;
        protected readonly IFornecedorAppService FornecedorAppService;
        protected readonly IClienteAppService ClienteAppService;
        protected readonly ITipoDeDocumentoFinanceiroAppService TipoDeDocumentoAppService;
        public DocumentoFinanceiroAppServiceTestBase()
        {
            DocumentoAPagarAppService = Resolve<IDocumentoAPagarAppService>();
            DocumentoAReceberAppService = Resolve<IDocumentoAReceberAppService>();
            FornecedorAppService = Resolve<IFornecedorAppService>();
            ClienteAppService = Resolve<IClienteAppService>();
            TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoFinanceiroAppService>();
        }

        protected void CreateTestDocumentos()
        {
            UsingDbContext(context =>
            {
                context.DocumentosAPagar.Add(CreateDocumentoAPagar("Documento a Pagar", "1000", DateTime.Now, 1000));
                context.DocumentosAReceber.Add(CreateDocumentoAReceber("Documento a Receber", "2000", DateTime.Now, 2000));
            });
        }

        private DocumentoAPagar CreateDocumentoAPagar(string Descricao, string Numero, DateTime Data, decimal Valor)
        {
            return new DocumentoAPagar
            {
                Descricao = Descricao,
                Data = Data,
                Numero = Numero,
                Valor = Valor,
                Fornecedor = new Fornecedor() {
                    PessoaFisica = new PessoaFisica() {
                        NomePessoa = "Pessoa de Teste"
                    },
                    EmpresaId = EmpresaIdDefaultTest
                },
                TipoDeDocumentoFinanceiro = new TipoDeDocumentoFinanceiro() {
                    Nome = "Nota Fiscal Eletrônica",
                    Tipo = TipoDeDocumentoFinanceiroEnum.APagar,
                    EmpresaId = EmpresaIdDefaultTest
                },
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private DocumentoAReceber CreateDocumentoAReceber(string Descricao, string Numero, DateTime Data, decimal Valor)
        {
            return new DocumentoAReceber
            {
                Descricao = Descricao,
                Data = Data,
                Numero = Numero,
                Valor = Valor,
                Cliente = new Cliente()
                {
                    PessoaFisica = new PessoaFisica()
                    {
                        NomePessoa = "Pessoa de Teste"
                    },
                    EmpresaId = EmpresaIdDefaultTest
                },
                TipoDeDocumentoFinanceiro = new TipoDeDocumentoFinanceiro()
                {
                    Nome = "Boleto",
                    Tipo = TipoDeDocumentoFinanceiroEnum.AReceber,
                    EmpresaId = EmpresaIdDefaultTest
                },
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}