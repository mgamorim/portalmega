﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Financeiro.Geral.Documento;
using EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using EZ.EZControl.Tests.Application.Financeiro.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Financeiro.Geral
{
    public class DocumentoFinanceiroAppService_Tests : DocumentoFinanceiroAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateDocumentoAPagarAndTestAsync()
        {
            await DocumentoAPagarAppService.Save(new DocumentoAPagarInput
            {
                Descricao = "Documento a Pagar",
                Data = DateTime.Now,
                Numero = "1000",
                Valor = 1000,
                FornecedorId = 1,
                TipoDeDocumentoFinanceiroId = 1
            });
        }

        private async Task CreateDocumentoAReceberAndTestAsync()
        {
            await DocumentoAReceberAppService.Save(new DocumentoAReceberInput
            {
                Descricao = "Documento a Receber",
                Data = DateTime.Now,
                Numero = "2000",
                Valor = 2000,
                ClienteId = 1,
                TipoDeDocumentoFinanceiroId = 1
            });
        }

        private async Task CreateFornecedor()
        {
            await FornecedorAppService.Save(new FornecedorInput()
            {
                PessoaFisica = new PessoaFisicaInputAux()
                {
                    Nome = "Pessoa de Teste",
                    Sexo = SexoEnum.NaoInformado,
                    DataDeNascimento = new DateTime(1990, 1, 1),
                    EstadoCivil = EstadoCivilEnum.NaoInformado,
                    PossuiFilhos = false
                },
                Pessoa = new PessoaInput()
                {
                    TipoPessoa = TipoPessoaEnum.Fisica,
                }
            });
        }

        private async Task CreateCliente()
        {
            await ClienteAppService.Save(new ClienteInput()
            {
                PessoaFisica = new PessoaFisicaInputAux()
                {
                    Nome = "Pessoa de Teste",
                    Sexo = SexoEnum.NaoInformado,
                    DataDeNascimento = new DateTime(1990, 1, 1),
                    EstadoCivil = EstadoCivilEnum.NaoInformado,
                    PossuiFilhos = false
                },
                Pessoa = new PessoaInput()
                {
                    TipoPessoa = TipoPessoaEnum.Fisica
                }
            });
        }

        private async Task CreateTipoDeDocumentoAPagar()
        {
            await TipoDeDocumentoAppService.Save(new TipoDeDocumentoFinanceiroInput()
            {
                Nome = "Nota Fiscal Eletrônica",
                Tipo = TipoDeDocumentoFinanceiroEnum.APagar
            });
        }

        private async Task CreateTipoDeDocumentoAReceber()
        {
            await TipoDeDocumentoAppService.Save(new TipoDeDocumentoFinanceiroInput()
            {
                Nome = "Boleto",
                Tipo = TipoDeDocumentoFinanceiroEnum.AReceber
            });
        }

        [Fact]
        public async Task Should_Create_TipoDeDocumento_APagar()
        {
            await CreateFornecedor();
            await CreateTipoDeDocumentoAPagar();
            await CreateDocumentoAPagarAndTestAsync();

            await UsingDbContext(async context =>
            {
                var createdDoc = await context.DocumentosAPagar.FirstOrDefaultAsync();
                createdDoc.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_TipoDeDocumento_AReceber()
        {
            await CreateCliente();
            await CreateTipoDeDocumentoAReceber();
            await CreateDocumentoAReceberAndTestAsync();

            await UsingDbContext(async context =>
            {
                var createdDoc = await context.DocumentosAReceber.FirstOrDefaultAsync();
                createdDoc.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Not_Create_Documento_APagar_With_Duplicate_Numero_E_Complemento()
        {
            CreateTestDocumentos();
            await CreateDocumentoAPagarAndTestAsync().ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Not_Create_Documento_AReceber_With_Duplicate_Numero_E_Complemento()
        {
            CreateTestDocumentos();
            await CreateDocumentoAReceberAndTestAsync().ShouldThrowAsync<AbpValidationException>();
        }
    }
}