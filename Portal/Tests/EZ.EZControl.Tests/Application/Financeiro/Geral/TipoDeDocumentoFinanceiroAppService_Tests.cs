﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Financeiro.Enums;
using EZ.EZControl.Dto.Financeiro.Geral.TipoDeDocumentoFinanceiro;
using EZ.EZControl.Tests.Application.Financeiro.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Financeiro.Geral
{
    public class TipoDeDocumentoFinanceiroAppService_Tests : TipoDeDocumentoFinanceiroAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }
        private async Task CreateTipoDeDocumentoAndTestAsync(string nome, TipoDeDocumentoFinanceiroEnum tipo)
        {

            await base.TipoDeDocumentoAppService.Save(new TipoDeDocumentoFinanceiroInput
            {
                Nome = nome,
                Tipo = tipo
            });
        }
        [Fact]
        public async Task Should_Create_TipoDeDocumento_APagar()
        {
            await CreateTipoDeDocumentoAndTestAsync("Nota Fiscal Eletrônica", TipoDeDocumentoFinanceiroEnum.APagar);

            await UsingDbContext(async context =>
            {
                var createdTipo = await context.TiposDeDocumentoFinanceiro.FirstOrDefaultAsync();
                createdTipo.ShouldNotBe(null);
            });
        }
        [Fact]
        public async Task Should_Create_TipoDeDocumento_AReceber()
        {
            await CreateTipoDeDocumentoAndTestAsync("Boleto", TipoDeDocumentoFinanceiroEnum.AReceber);

            await UsingDbContext(async context =>
            {
                var createdTipo = await context.TiposDeDocumentoFinanceiro.FirstOrDefaultAsync();
                createdTipo.ShouldNotBe(null);
            });
        }
        [Fact]
        public async Task Should_Not_Create_TipoDeDocumento_APagar_With_Duplicate_Nome()
        {
            base.CreateTestTiposDeDocumento();
            await CreateTipoDeDocumentoAndTestAsync("Nota Fiscal Eletrônica", TipoDeDocumentoFinanceiroEnum.APagar)
                .ShouldThrowAsync<UserFriendlyException>();
        }
        [Fact]
        public async Task Should_Not_Create_TipoDeDocumento_AReceber_With_Duplicate_Nome()
        {
            base.CreateTestTiposDeDocumento();
            await CreateTipoDeDocumentoAndTestAsync("Boleto", TipoDeDocumentoFinanceiroEnum.AReceber)
                .ShouldThrowAsync<UserFriendlyException>();
        }
        [Fact]
        public async Task Should_Delete_TipoDeDocumento()
        {
            base.CreateTestTiposDeDocumento();
            await TipoDeDocumentoAppService.Delete(new IdInput(1));
            await TipoDeDocumentoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}