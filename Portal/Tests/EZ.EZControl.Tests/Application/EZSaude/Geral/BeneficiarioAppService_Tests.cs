﻿using Abp.Application.Services.Dto;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using Abp.UI;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Configuration.Host;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Domain.EZSaude.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Beneficiario;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretor;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Dependente;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.ProponenteTitular;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Responsavel;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Services.EZSaude.Interfaces;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class BeneficiarioAppService_Tests : BeneficiarioAppServiceTestBase
    {
        private readonly IRepository<Documento> _documentoRepository;
        private readonly IRepository<TipoDeDocumento> _tipoDeDocumentoRepository;
        private readonly IRepository<Pessoa> _pessoaRepository;
        private readonly IRepository<EnderecoEletronico> _enderecoEletronicoRepository;
        private readonly IRepository<Telefone> _telefoneRepository;
        private readonly IRepository<Endereco> _enderecoRepository;
        private readonly IRepository<Profissao> _profissaoRepository;
        private readonly IRepository<Estado> _estadoRepository;
        private readonly IRepository<Cidade> _cidadeRepository;
        private readonly IRepository<TipoDeLogradouro> _tipoDeLogradouroRepository;
        private readonly IRepository<Corretora> _corretoraRepository;
        private readonly IRepository<GrupoPessoa> _grupoPessoaRepository;
        private readonly IRepository<ProponenteTitular> _proponenteTitularRepository;
        private readonly IBeneficiarioBaseService _beneficiarioService;
        private readonly IHostSettingsAppService _hostSettingsAppService;
        private readonly ISettingManager _settingManager;

        public BeneficiarioAppService_Tests()
        {
            _documentoRepository = Resolve<IRepository<Documento>>();
            _tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
            _pessoaRepository = Resolve<IRepository<Pessoa>>();
            _enderecoEletronicoRepository = Resolve<IRepository<EnderecoEletronico>>();
            _telefoneRepository = Resolve<IRepository<Telefone>>();
            _enderecoRepository = Resolve<IRepository<Endereco>>();
            _profissaoRepository = Resolve<IRepository<Profissao>>();
            _estadoRepository = Resolve<IRepository<Estado>>();
            _cidadeRepository = Resolve<IRepository<Cidade>>();
            _tipoDeLogradouroRepository = Resolve<IRepository<TipoDeLogradouro>>();
            _corretoraRepository = Resolve<IRepository<Corretora>>();
            _corretoraRepository = Resolve<IRepository<Corretora>>();
            _grupoPessoaRepository = Resolve<IRepository<GrupoPessoa>>();
            _proponenteTitularRepository = Resolve<IRepository<ProponenteTitular>>();
            _hostSettingsAppService = Resolve<IHostSettingsAppService>();
            _settingManager = Resolve<ISettingManager>();

            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedDevelopment(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private TipoDeDocumento CreateTipoDeDocumento(string descricao, string mascara,
            TipoDeDocumentoEnum tipoDeDocumentoFixo)
        {
            return new TipoDeDocumento
            {
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                Descricao = descricao,
                TipoDeDocumentoFixo = tipoDeDocumentoFixo,
                Mascara = mascara,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Telefone CreateTelefone(Pessoa pessoa, TipoTelefoneEnum tipoDeTelefone, string ddi, string ddd,
            string numero, string ramal = null)
        {
            var telefone = new Telefone(pessoa, tipoDeTelefone, ddd, numero, ddi, ramal)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
            return telefone;

        }

        private Documento CreateDocumento(Pessoa pessoa, TipoDeDocumento tipoDeDocumento, string numero = null,
            DateTime? dataDeExpedicao = null, string orgao = null)
        {
            var doc = new Documento(pessoa, tipoDeDocumento, numero, orgao, dataDeExpedicao)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
            return doc;
        }

        private EnderecoEletronico CreateEnderecoEletronico(Pessoa pessoa,
            TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
        {
            return new EnderecoEletronico(pessoa, tipoDeEnderecoEletronico, endereco) { EmpresaId = EmpresaIdDefaultTest };
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(string descricao)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private GrupoPessoa CreateGrupoPessoa(string nome, string descricao)
        {
            return new GrupoPessoa
            {
                IsActive = true,
                Descricao = descricao,
                Nome = nome,
                Slug = nome,
                TipoDePessoa = TipoPessoaEnum.Fisica
            };
        }

        private Corretora CreateCorretora()
        {
             var corretor = SaveCorretor("Wescley Arruda", "Brasileiro", DateTime.Now.AddYears(-20), SexoEnum.Masculino, EstadoCivilEnum.Casado);

            return new Corretora
            {
                IsActive = true,
                Slug = "Corretora ABC",
                Nome = "Corretora ABC",
                PessoaJuridica = new PessoaJuridica
                {
                    RazaoSocial = "Corretora ABC",
                    NomeFantasia = "Corretora ABC"
                },
                EmpresaId = EmpresaIdDefaultTest,
                TenantId = AbpSession.TenantId.Value,
                CorretorId=corretor.Id
            };
        }

        private async Task<CorretorInput> SaveCorretor(string nome, string nacionalidade, DateTime dataDeNascimento, SexoEnum sexo, EstadoCivilEnum estadoCivil)
        {
            var pf = await base.PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nome,
                IsActive = true,
                Nacionalidade = nacionalidade,
                DataDeNascimento = dataDeNascimento,
                Sexo = sexo,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = estadoCivil,
            });

            var grupoPessoa = CreateGrupoPessoa("Corretor", "Corretor");

            var grupoPessoaId = _grupoPessoaRepository.InsertOrUpdateAndGetId(grupoPessoa);

            var pessoaTitular = _pessoaRepository.Get(pf.Id);

            //var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);
            
            //_tipoDeDocumentoRepository.InsertOrUpdateAndGetId(tipoDeDocumentoCpf);

            pf.GrupoPessoaId = grupoPessoaId;
            pf.Cpf = "12775314716";


            return await base.CorretorAppService.SaveAndReturnEntity(new CorretorInput
            {
                IsActive = true,
                Pessoa = pf,
                
                
                //Pessoa = new PessoaFisicaInput
                //{
                //    Nome = nome,
                //    IsActive = true,
                //    Nacionalidade = nacionalidade,
                //    DataDeNascimento = dataDeNascimento,
                //    Sexo = sexo,
                //    TipoPessoa = TipoPessoaEnum.Fisica,
                //    EstadoCivil = estadoCivil
                //}
            });
        }

        private Profissao CreateProfissao()
        {
            return new Profissao
            {
                Codigo = "001",
                Titulo = "Analista de Sistemas"
            };
        }

        private Cidade CreateCidade()
        {
            return new Cidade
            {
                CodigoIBGE = "33301",
                Estado = CreateEstado(),
                Nome = "Rio de Janeiro",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Estado CreateEstado()
        {
            return new Estado
            {
                Nome = "Rio de Janeiro",
                Capital = "Rio de Janeiro",
                Sigla = "RJ",
                CodigoIBGE = "33",
                Pais = CreatePais(),
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Pais CreatePais()
        {
            return new Pais
            {
                CodigoISO = "BRA",
                Nacionalidade = "Brasileira",
                Nome = "Brasil",
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Endereco CreateEndereco(Pessoa pessoa, TipoEnderecoEnum tipoDeEndereco,
            TipoDeLogradouro tipoDeLogradouro, Cidade cidade,
            string descricao, string logradouro, string numero, string complemento, string bairro, string cep,
            string referencia)
        {
            return new Endereco(pessoa, descricao, tipoDeEndereco, tipoDeLogradouro, logradouro, numero, complemento,
                bairro, cep, cidade, referencia)
            {
                EnderecoParaEntrega = true,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private async Task<ResponsavelInput> CreateAndReturnResponsavel(string nomeDaMaeTitular)
        {
            var pfResponsavel = new PessoaFisicaInput
            {
                Nome = nomeDaMaeTitular,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-50),
                Sexo = SexoEnum.Feminino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado,
                Cpf = "12775314716"

            };

            return new ResponsavelInput
            {
                PessoaFisica = pfResponsavel,
                TipoDeResponsavel = TipoDeResponsavelEnum.PaiMae,
                IsActive = true
            };
        }

        private async Task<DependenteInput> CreateAndReturnDependente(string nomeDoDependente, GrauDeParentescoEnum grauDeParentesco,
            string nomeDaMaeDependente)
        {

            var pfDependente = new PessoaFisicaInput
            {
                Nome = nomeDoDependente,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-50),
                Sexo = SexoEnum.Feminino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado,
                Cpf = "12775314716"

            };

            return new DependenteInput
            {
                PessoaFisica = pfDependente,
                GrauDeParentesco = grauDeParentesco,
                NomeDaMae = nomeDaMaeDependente,
                TipoDeBeneficiario = TipoDeBeneficiarioEnum.Dependente
            };
        }

        private async Task<ProponenteTitularInput> CreateAndReturnTitular(string nomeDoTitular, string nomeDaMaeTitular,
            ResponsavelInput responsavel = null, List<DependenteInput> dependentes = null)
        {
            var pfTitular = await base.PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nomeDoTitular,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = (responsavel != null) ? DateTime.Now.AddYears(-5) : DateTime.Now.AddYears(-40),
                Sexo = SexoEnum.Masculino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado
            });

            var grupoPessoa = CreateGrupoPessoa("Médico", "Médico");

            var grupoPessoaId = _grupoPessoaRepository.InsertOrUpdateAndGetId(grupoPessoa);

            var pessoaTitular = _pessoaRepository.Get(pfTitular.Id);

            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);
            var tipoDeDocumentoRg = this.CreateTipoDeDocumento("RG", "99.9999.999-9", TipoDeDocumentoEnum.Rg);

            _tipoDeDocumentoRepository.InsertOrUpdateAndGetId(tipoDeDocumentoCpf);
            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoRg);

            //var documentoCpf = this.CreateDocumento(pessoaTitular, tipoDeDocumentoCpf, "67515814675");
            //var documentoRg = this.CreateDocumento(pessoaTitular, tipoDeDocumentoRg, "283068668", DateTime.Now,
            //    "DETRAN-RJ");

            //_documentoRepository.InsertOrUpdate(documentoCpf);
            //_documentoRepository.InsertOrUpdate(documentoRg);

            //var enderecoEletronico = this.CreateEnderecoEletronico(pessoaTitular, TipoDeEnderecoEletronicoEnum.EmailPrincipal, "angelo@ezsoft.com.br");
            //_enderecoEletronicoRepository.InsertOrUpdate(enderecoEletronico);

            //var telefone = this.CreateTelefone(pessoaTitular, TipoTelefoneEnum.Celular, "55", "21", "964264040");
            //_telefoneRepository.InsertOrUpdate(telefone);

            var cidade = this.CreateCidade();
            _cidadeRepository.InsertOrUpdate(cidade);

            //var tipoDeLogradouro = CreateTipoDeLogradouro("Avenida");
            //_tipoDeLogradouroRepository.InsertOrUpdate(tipoDeLogradouro);

            //var endereco = this.CreateEndereco(pessoaTitular, TipoEnderecoEnum.Residencial, tipoDeLogradouro, cidade,
            //    "Residencial", "Américas", "1000", "Sala 3", "Barra da Tijuca", "21370888",
            //    "Próximo ao supermercado");

            //_enderecoRepository.InsertOrUpdate(endereco);

            var profissaoId = _profissaoRepository.InsertOrUpdateAndGetId(CreateProfissao());
            var corretoraId = _corretoraRepository.InsertOrUpdateAndGetId(CreateCorretora());

            // pfTitular = await base.PessoaFisicaAppService.GetId(new IdInput(pfTitular.Id));
            pfTitular.GrupoPessoaId = grupoPessoaId;

            var proponenteTitular = new ProponenteTitularInput
            {
                PessoaFisica = pfTitular,
                IsActive = true,
                NomeDaMae = nomeDaMaeTitular,
                EstadoId = cidade.EstadoId,
                ProfissaoId = profissaoId,
                CorretoraId = corretoraId,
                TipoDeBeneficiario = TipoDeBeneficiarioEnum.Titular,
                StatusBeneficiario = StatusTipoDeBeneficiarioTitularEnum.PreCadastradoPeloCorretor
            };
            proponenteTitular.PessoaFisica.Cpf = "12775314716";
            proponenteTitular.PessoaFisica.EmailPrincipal = new EnderecoEletronicoInput { Endereco = "angelo@ezsoft.com.br" };
            proponenteTitular.Celular = "(21) 97014-2357";
            proponenteTitular.PessoaFisica.Rg = "210588554";
            proponenteTitular.PessoaFisica.OrgaoExpedidor = "Detran";

            if (responsavel != null)
            {
                proponenteTitular.Responsavel = responsavel;
                //var pessoaResponsavel = _pessoaRepository.Get(responsavel.PessoaFisica.Id);
                //var documentoResponsavelCpf = this.CreateDocumento(pessoaResponsavel, tipoDeDocumentoCpf, "67515814678");
                //_documentoRepository.InsertOrUpdate(documentoResponsavelCpf);
                //proponenteTitular.Responsavel = responsavel;
            }

            if (dependentes != null)
            {
                foreach (var dependente in proponenteTitular.Dependentes)
                {
                    dependente.EstadoId = cidade.EstadoId;
                    //dependente.ProfissaoId = profissaoId;
                    dependente.CorretoraId = corretoraId;
                }

                //foreach (var dependente in dependentes)
                //{
                //    dependente.EstadoId = cidade.EstadoId;
                //    dependente.ProfissaoId = profissaoId;
                //    dependente.CorretoraId = corretoraId;

                //    var pessoaDependente = _pessoaRepository.Get(dependente.PessoaFisica.Id);
                //    var documentoDependenteCpf = this.CreateDocumento(pessoaDependente, tipoDeDocumentoCpf,
                //        "012345678910");
                //    _documentoRepository.InsertOrUpdate(documentoDependenteCpf);
                //}

                //proponenteTitular.Dependentes = dependentes;
            }

            return proponenteTitular;

        }

        [Fact]
        public async Task Should_Create_Titular()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = await CreateAndReturnTitular("Michel Duarte", "Natália Duarte");
                await uow.Current.SaveChangesAsync();
                var proponenteTitularRetorno = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);
            });
        }

        [Fact]
        public async Task Should_Create_Titular_With_Responsavel()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var responsavel = await CreateAndReturnResponsavel("Jupira Andrade");
                var proponenteTitular = await CreateAndReturnTitular("Natanael Andrade", "Jupira Andrade", responsavel);
                await uow.Current.SaveChangesAsync();
                await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);
            });

        }

        [Fact]
        public async Task Should_Create_Titular_With_Dependentes()
        {
            await RunAsyncInTransaction(async (uow) =>
            {

                var proponenteTitular = await CreateAndReturnTitular("Michel Duarte", "Natália Duarte");
                await uow.Current.SaveChangesAsync();
                var proponenteTitularRetorno = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);

                var proponenteGetInput = await base.ProponenteTitularAppService.GetById(new IdInput(proponenteTitularRetorno.Id));


                var dependentes = new List<DependenteInput>
                {
                    await CreateAndReturnDependente("Jorge Henrique Fagundes", GrauDeParentescoEnum.Filho, "Priscila Albuquerque"),
                    await CreateAndReturnDependente("Almir Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes"),
                    await CreateAndReturnDependente("Pietro Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes")
                };


                proponenteGetInput.Dependentes = new List<DependenteInput>();

                foreach (var dependente in dependentes)
                {
                    dependente.EstadoId = proponenteGetInput.EstadoId;

                    proponenteGetInput.Dependentes.Add(dependente);
                }

                proponenteGetInput.Celular = "(21) 97014-2357";

                await base.ProponenteTitularAppService.Save(proponenteGetInput);
            });
        }

        [Fact]
        public async Task Should_Create_Titular_With_Responsavel_And_Dependentes()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var responsavel = await CreateAndReturnResponsavel("Jupira Andrade");
                var proponenteTitular = await CreateAndReturnTitular("Natanael Andrade", "Jupira Andrade", responsavel);
                await uow.Current.SaveChangesAsync();
                var proponenteTitularRetorno = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);

                var proponenteGetInput = await base.ProponenteTitularAppService.GetById(new IdInput(proponenteTitularRetorno.Id));

                var dependentes = new List<DependenteInput>
                {
                    await CreateAndReturnDependente("Jorge Henrique Fagundes", GrauDeParentescoEnum.Filho, "Priscila Albuquerque"),
                    await CreateAndReturnDependente("Almir Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes"),
                    await CreateAndReturnDependente("Pietro Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes")
                };


                proponenteGetInput.Dependentes = new List<DependenteInput>();

                foreach (var dependente in dependentes)
                {
                    dependente.EstadoId = proponenteGetInput.EstadoId;

                    proponenteGetInput.Dependentes.Add(dependente);
                }

                proponenteGetInput.Celular = "(21) 97014-2357";

                await base.ProponenteTitularAppService.Save(proponenteGetInput);
            });
        }

        [Fact]
        public async Task Delete_Titular()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = await CreateAndReturnTitular("Luiz Carvalho Mendonça", "Sibelli Mendonça");
                var titular = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);
                await base.ProponenteTitularAppService.Delete(new IdInput(titular.Id));
                await uow.Current.SaveChangesAsync();
                await base.ProponenteTitularAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        [Fact]
        public async Task Delete_Responsavel()
        {

            await RunAsyncInTransaction(async (uow) =>
            {
                var responsavel = await CreateAndReturnResponsavel("Noir Ramalho");
                var proponenteTitular = await CreateAndReturnTitular("Jaime Ramalho", "Noir Ramalho", responsavel);
                await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);
                var titular = await base.ProponenteTitularAppService.GetById(new IdInput(1));
                await base.ResponsavelAppService.Delete(new IdInput(titular.ResponsavelId));
                await uow.Current.SaveChangesAsync();
                await base.ResponsavelAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        [Fact]
        public async Task Delete_Dependente()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = await CreateAndReturnTitular("Michel Duarte", "Natália Duarte");
                await uow.Current.SaveChangesAsync();
                var proponenteTitularRetorno = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);

                var proponenteGetInput = await base.ProponenteTitularAppService.GetById(new IdInput(proponenteTitularRetorno.Id));

                var dependentes = new List<DependenteInput>
                {
                    await CreateAndReturnDependente("Jorge Henrique Fagundes", GrauDeParentescoEnum.Filho, "Priscila Albuquerque"),
                    await CreateAndReturnDependente("Almir Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes"),
                    await CreateAndReturnDependente("Pietro Fagundes", GrauDeParentescoEnum.Neto, "Mariana Fagundes")
                };


                proponenteGetInput.Dependentes = new List<DependenteInput>();

                foreach (var dependente in dependentes)
                {
                    dependente.EstadoId = proponenteGetInput.EstadoId;

                    proponenteGetInput.Dependentes.Add(dependente);
                }

                proponenteGetInput.Celular = "(21) 97014-2357";

                await base.ProponenteTitularAppService.Save(proponenteGetInput);

                int totalDependentes = proponenteGetInput.Dependentes.Count;

                proponenteGetInput.Dependentes.Remove(proponenteGetInput.Dependentes.FirstOrDefault());

                await base.ProponenteTitularAppService.Save(proponenteGetInput);

                var proponenteRemoveDependente = await base.ProponenteTitularAppService.GetById(new IdInput(proponenteTitularRetorno.Id));
                //await base.DependenteAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        [Fact]
        public async Task Should_Create_Beneficiario_IsValid()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = await CreateAndReturnTitular("Roman Aguiar", "Erika Aguiar");
                await uow.Current.SaveChangesAsync();
                var proponenteTitularInput = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);
                base.BeneficiarioBaseAppService.IsValidBeneficiario(new IdInput(proponenteTitularInput.Id)).ShouldNotThrow();
            });
        }

        [Fact]
        public async Task Should_Create_User_Corretor()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var proponenteTitular = await CreateAndReturnTitular("José Justino", "Maria Justino");
                await uow.Current.SaveChangesAsync();
                var beneficiarioId = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);

                await base.BeneficiarioBaseAppService.CreateUserBeneficiario(new BeneficiarioUserInput
                {
                    Id = beneficiarioId.Id,
                    TipoDeUsuario = TipoDeUsuarioEnum.Corretor,
                    StaticRoleName = StaticRoleNames.Tenants.Corretor,
                    SendEmail = false
                });
            });
        }

        [Fact]
        public async Task Should_Create_User_Beneficiario()
        {
            try
            {
                await RunAsyncInTransaction(async (uow) =>
                {
                    SetSmtpEzSoft();
                    var proponenteTitular = await CreateAndReturnTitular("Marcos de Castro Tavares", "Micheli de Castro Tavares");
                    await uow.Current.SaveChangesAsync();
                    var beneficiarioId = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);

                    await base.BeneficiarioBaseAppService.CreateUserBeneficiario(new BeneficiarioUserInput
                    {
                        Id = beneficiarioId.Id,
                        TipoDeUsuario = TipoDeUsuarioEnum.Beneficiario,
                        StaticRoleName = StaticRoleNames.Tenants.Beneficiario,
                        SendEmail = true
                    });
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        [Fact]
        public async Task Should_Create_User_Pessoa_Beneficiario()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                SetSmtpEzSoft();
                var proponenteTitular = await CreateAndReturnTitular("Renato Magalhães", "Noemi Magalhães Souza");
                await uow.Current.SaveChangesAsync();
                var beneficiarioId = await base.ProponenteTitularAppService.SaveAndReturnEntity(proponenteTitular);

                try
                { 
                await base.BeneficiarioBaseAppService.CreateUserPessoaBeneficiarioFront(new BeneficiarioUserPessoaInput
                {
                    SendEmail = true,
                    DataDeNascimento = proponenteTitular.PessoaFisica.DataDeNascimento.Value,
                    Email = proponenteTitular.PessoaFisica.EmailPrincipal.Endereco,
                    Cpf = proponenteTitular.PessoaFisica.Cpf,
                    NomeCompleto = proponenteTitular.PessoaFisica.Nome,
                    GrupoPessoaSlug = "Médico",
                    Celular = proponenteTitular.Celular,
                    CorretoraSlug = "Corretora ABC",
                    Profissao = "Analista de Sistemas",
                    ProfissaoId = proponenteTitular.ProfissaoId,
                    EstadoUf = "RJ"
                });
                }
                catch(Exception e)
                {
                    throw e;
                }
            });
        }

        private void SetSmtpEzSoft()
        {
            _settingManager.ChangeSettingForApplication(EmailSettingNames.DefaultFromAddress, "contato@ezsoft.com.br");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.DefaultFromDisplayName, "EZControl - EZSoft");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.Host, "mail.smtp2go.com");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.UserName, "smtp_admin_ezsoft");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.Password, "N2M4enNlZXozNTRk");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.Domain, "");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.EnableSsl, "false");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.Port, "587");
            _settingManager.ChangeSettingForApplication(EmailSettingNames.Smtp.UseDefaultCredentials, "false");
        }
    }
}
