﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZSaude.Geral.Parametro;
using EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class ParametroAppService_Tests : ParametroAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Parametro()
        {
            await CreateParametrosAsync();
        }

        private async Task CreateParametrosAsync()
        {
            base.CreateTestParametros();

            await ParametroEzsaudeAppService.Save(new ParametroEzsaudeInput
            {
                AlturaMaximaPx = 100,
                ExtensoesDocumento = "doc;pdf",
                ExtensoesImagem = "png;jpeg",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1
            });

            await UsingDbContext(async context =>
            {
                var createdParametro = await context.ParametroEzsaude.FirstOrDefaultAsync();
                createdParametro.ShouldNotBe(null);
            });
        }

    }
}
