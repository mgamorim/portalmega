﻿using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Domain.EZSaude.Geral;
using EZ.EZControl.Domain.EZSaude.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.EZSaude.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class ProdutoDePlanoDeSaudeAppServiceTestBase : AppTestBase
    {
        protected readonly IProdutoDePlanoDeSaudeAppService ProdutoDePlanoDeSaudeAppService;
        protected readonly IPlanoDeSaudeAppService PlanoDeSaudeAppService;
        protected readonly IUnidadeMedidaAppService UnidadeMedidaAppService;
        protected readonly INaturezaAppService NaturezaAppService;

        public ProdutoDePlanoDeSaudeAppServiceTestBase()
        {
            this.ProdutoDePlanoDeSaudeAppService = Resolve<IProdutoDePlanoDeSaudeAppService>();
            this.PlanoDeSaudeAppService = Resolve<IPlanoDeSaudeAppService>();
            this.UnidadeMedidaAppService = Resolve<IUnidadeMedidaAppService>();
            this.NaturezaAppService = Resolve<INaturezaAppService>();
        }

        protected void CreatePlanoDeSaude()
        {
            UsingDbContext(context =>
            {
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.260/16-1", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Rubi207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "Não há"));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.388/16-7", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação", true));
                context.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity("ANS 475.387/16-9", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante217, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "3x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e internação", true));
            });
        }

        protected void CreateOperadora()
        {
            UsingDbContext(context =>
            {
                context.Operadoras.Add(CreateOperadora("Amil", "33698-AB", "Amil SA", "Amil Operadora"));
            });
        }

        protected void CreateNatureza()
        {
            UsingDbContext(context =>
            {
                context.Naturezas.Add(CreateNaturezaEntity("Natureza"));
            });
        }

        protected void CreateUnidade()
        {
            UsingDbContext(context =>
            {
                context.UnidadeMedidas.Add(CreateUnidadeDeMedidaEntity("Metro", UnidadePadraoEnum.Metro, 250));
                context.UnidadeMedidas.Add(CreateUnidadeDeMedidaEntity("Quilo", UnidadePadraoEnum.Quilograma, 100));
            });
        }

        protected void CreateEspecialidade()
        {
            UsingDbContext(context =>
            {
                context.Especialidades.Add(CreateEspecialidadeEntity("1", "Cardiologia"));
            });
        }

        private PlanoDeSaude CreatePlanoDeSaudeEntity(string numeroDoRegistro, AbrangenciaDoPlanoEnum abrangencia, AcomodacaoEnum acomodacao,
                                                      PlanoDeSaudeAnsEnum planoAns, SegmentacaoAssistencialDoPlanoEnum segmentacao, string descricaoDoReembolso, bool reembolso = false)
        {
            return new PlanoDeSaude
            {
                IsActive = true,
                NumeroDeRegistro = numeroDoRegistro,
                DescricaoDoReembolso = descricaoDoReembolso,
                Reembolso = reembolso,
                Abrangencia = abrangencia,
                Acomodacao = acomodacao,
                PlanoDeSaudeAns = planoAns,
                SegmentacaoAssistencial = segmentacao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Operadora CreateOperadora(string nome, string codigoAns, string razaoSocial, string nomeFantasia)
        {
            return new Operadora
            {
                IsActive = true,
                Nome = nome,
                CodigoAns = codigoAns,
                PessoaJuridica = new PessoaJuridica
                {
                    RazaoSocial = razaoSocial,
                    NomeFantasia = nomeFantasia
                },
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Natureza CreateNaturezaEntity(string descricao)
        {
            return new Natureza
            {
                Descricao = descricao,
                IsActive = true,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private UnidadeMedida CreateUnidadeDeMedidaEntity(string descricao, UnidadePadraoEnum unidadePadrao, decimal valor)
        {
            return new UnidadeMedida
            {
                Descricao = descricao,
                IsActive = true,
                UnidadePadrao = unidadePadrao,
                Valor = valor,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Especialidade CreateEspecialidadeEntity(string codigo, string nome)
        {
            return new Especialidade
            {
                Codigo = codigo,
                Nome = nome,
                IsActive = true,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
