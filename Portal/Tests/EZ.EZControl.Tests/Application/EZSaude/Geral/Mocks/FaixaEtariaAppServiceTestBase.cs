﻿using EZ.EZControl.Services.EZSaude.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class FaixaEtariaAppServiceTestBase : AppTestBase
    {
        public readonly IFaixaEtariaAppService FaixaEtariaAppService;

        public FaixaEtariaAppServiceTestBase()
        {
            FaixaEtariaAppService = Resolve<IFaixaEtariaAppService>();
        }
    }
}
