﻿using EZ.EZControl.Services.EZSaude.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class AdministradoraAppServiceTestBase : AppTestBase
    {
        public readonly IAdministradoraAppService AdministradoraAppService;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly IEmpresaAppService EmpresaAppService;
        public readonly IClienteEZAppService ClienteEzAppService;
        public readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public AdministradoraAppServiceTestBase()
        {
            AdministradoraAppService = Resolve<IAdministradoraAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            EmpresaAppService = Resolve<IEmpresaAppService>();
            ClienteEzAppService = Resolve<IClienteEZAppService>();
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }
    }
}
