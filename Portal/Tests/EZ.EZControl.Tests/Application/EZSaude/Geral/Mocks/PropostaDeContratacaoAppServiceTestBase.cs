﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Domain.EZSaude.Geral;
using EZ.EZControl.Domain.EZSaude.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Estoque.Geral.Interfaces;
using EZ.EZControl.Services.EZSaude.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class PropostaDeContratacaoAppServiceTestBase : AppTestBase
    {
        public readonly IPropostaDeContratacaoAppService PropostaDeContratacaoAppService;
        public readonly IProponenteTitularAppService TitularAppService;
        public readonly IResponsavelAppService ResponsavelAppService;
        public readonly IContratoAppService ContratoAppService;
        public readonly IProdutoDePlanoDeSaudeAppService ProdutoDePlanoDeSaudeAppService;
        private readonly IPessoaService PessoaService;
        private readonly IProdutoService ProdutoService;
        private readonly UserManager UserManager;
        public readonly ICorretorAppService CorretorAppService;
        public PropostaDeContratacaoAppServiceTestBase()
        {
            PropostaDeContratacaoAppService = Resolve<IPropostaDeContratacaoAppService>();
            TitularAppService = Resolve<IProponenteTitularAppService>();
            ResponsavelAppService = Resolve<IResponsavelAppService>();
            ContratoAppService = Resolve<IContratoAppService>();
            ProdutoDePlanoDeSaudeAppService = Resolve<IProdutoDePlanoDeSaudeAppService>();
            PessoaService = Resolve<IPessoaService>();
            ProdutoService = Resolve<IProdutoService>();
            UserManager = Resolve<UserManager>();
            CorretorAppService = Resolve<ICorretorAppService>();
        }
        public async Task CreateCorretorTest()
        {
            await UsingDbContext(async c =>
            {
                c.Corretores.Add(await CreateCorretorEntity());
            });

            var corretor = new Corretor();

            await UsingDbContext(async c =>
            {
                corretor = await c.Corretores.FirstOrDefaultAsync();
            });

            await AssociaCorretorUsuarioCorrente(corretor);
        }
        private async Task<Corretor> CreateCorretorEntity()
        {
            var pessoaFisicaId = 0;
            CreatePessoaFisica();
            UsingDbContext(c =>
            {
                pessoaFisicaId = c.PessoasFisicas.FirstOrDefault().Id;
            });
            var corretor = new Corretor()
            {
                EmpresaId = EmpresaIdDefaultTest,
                IsActive = true,
                PessoaId = pessoaFisicaId,
                TenantId = 1
            };
            return corretor;
        }

        private async Task AssociaCorretorUsuarioCorrente(Corretor corretor)
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var user = await UserManager.GetUserByIdAsync(AbpSession.UserId.Value);
                var pessoa = await PessoaService.GetById(corretor.PessoaId);
                user.Pessoa = pessoa;
                user.TipoDeUsuario = TipoDeUsuarioEnum.Corretor;
                await UserManager.UpdateAsync(user);
            }, true);
        }

        #region Titular
        public void CreateTitularTest()
        {
            UsingDbContext(c =>
            {
                c.Proponentes.Add(CreateTitularEntity());
            });
        }

        private void CreatePessoaFisica()
        {
            UsingDbContext(c =>
            {
                c.PessoasFisicas.Add(CreatePessoaFisicaEntity());
            });
        }

        public void CreateEstado()
        {
            UsingDbContext(c =>
            {
                c.Estados.Add(CreateEstadoEntity());
            });
        }

        public void CreateProfissao()
        {
            UsingDbContext(c =>
            {
                c.Profissoes.Add(CreateProfissaoEntity());
            });
        }

        private void CreateCorretora()
        {
            UsingDbContext(c =>
            {
                c.Corretoras.Add(CreateCorretoraEntity());
            });
        }

        private void CreatePais()
        {
            UsingDbContext(c =>
            {
                c.Paises.Add(CreatePaisEntity());
            });
        }

        private void CreatePessoaJuridica()
        {
            UsingDbContext(c =>
            {
                c.PessoasJuridicas.Add(CreatePessoaJuridicaEntity());
            });
        }

        private PessoaJuridica CreatePessoaJuridicaEntity()
        {
            var pj = new PessoaJuridica()
            {
                NomeFantasia = "EzSoft",
                NomePessoa = "EzSoft",
                RazaoSocial = "EzSoft",
                TenantId = 1
            };
            var grupoPessoa = new GrupoPessoa()
            {
                Nome = "Empresas",
                TenantId = 1,
                IsActive = true,
                TipoDePessoa = TipoPessoaEnum.Juridica
            };
            pj.AssociarGrupoPessoa(grupoPessoa, PessoaService);
            return pj;
        }

        private Pais CreatePaisEntity()
        {
            var pais = new Pais()
            {
                CodigoInternacional = "55",
                CodigoISO = "55",
                CodigoReceitaFederal = "55",
                EmpresaId = EmpresaIdDefaultTest,
                MascaraDoCodigoPostal = "99999-999",
                Nacionalidade = "Brasileira",
                Nome = "Brasil",
                IsActive = true,
                Sigla2Caracteres = "BR",
                Sigla3Caracteres = "BRA",
                TenantId = 1
            };
            return pais;
        }

        private Corretora CreateCorretoraEntity()
        {
            var corretora = new Corretora()
            {
                PessoaJuridicaId = 1,
                EmpresaId = EmpresaIdDefaultTest,
                TenantId = 1,
                IsActive = true
            };
            return corretora;
        }

        private Operadora CreateOperadoraEntity()
        {
            var operadora = new Operadora()
            {
                PessoaJuridicaId = 1,
                EmpresaId = EmpresaIdDefaultTest,
                TenantId = 1,
                CodigoAns = "ANS215445",
                IsActive = true,
                Nome = "Bem"
            };
            return operadora;
        }

        private Profissao CreateProfissaoEntity()
        {
            var profissao = new Profissao()
            {
                Codigo = "001",
                IsActive = true,
                TenantId = 1,
                Titulo = "Analista de Sistemas"
            };
            return profissao;
        }

        private Estado CreateEstadoEntity()
        {
            CreatePais();
            var estado = new Estado()
            {
                Capital = "Rio de Janeiro",
                CodigoIBGE = "21",
                EmpresaId = EmpresaIdDefaultTest,
                PaisId = 1,
                Sigla = "RJ",
                Nome = "Rio de Janeiro",
                TenantId = 1
            };
            return estado;
        }

        private PessoaFisica CreatePessoaFisicaEntity()
        {
            var pf = new PessoaFisica()
            {
                Nome = "Pessoa Fisica Teste",
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-40),
                Sexo = SexoEnum.Masculino,
                EstadoCivil = EstadoCivilEnum.Casado
            };
            var grupoPessoa = new GrupoPessoa()
            {
                Nome = "Titular",
                TenantId = 1,
                IsActive = true,
                TipoDePessoa = TipoPessoaEnum.Fisica
            };
            pf.AssociarGrupoPessoa(grupoPessoa, PessoaService);
            return pf;
        }

        private ProponenteTitular CreateTitularEntity()
        {
            CreateEstado();
            CreateProfissao();
            CreatePessoaFisica();
            CreateCorretora();

            var pessoaFisicaId = 0;
            UsingDbContext(c =>
            {
                pessoaFisicaId = c.PessoasFisicas.FirstOrDefault().Id;
            });

            var corretoraId = 0;
            UsingDbContext(c =>
            {
                corretoraId = c.Corretoras.FirstOrDefault().Id;
            });

            var estadoId = 0;
            UsingDbContext(c =>
            {
                estadoId = c.Estados.FirstOrDefault().Id;
            });

            var profissaoId = 0;
            UsingDbContext(c =>
            {
                profissaoId = c.Profissoes.FirstOrDefault().Id;
            });

            var titular = new ProponenteTitular()
            {
                PessoaFisicaId = pessoaFisicaId,
                CorretoraId = corretoraId,
                EmpresaId = EmpresaIdDefaultTest,
                EstadoId = estadoId,
                ProfissaoId = profissaoId,
                TenantId = 1,
                TipoDeBeneficiario = TipoDeBeneficiarioEnum.Titular,
                NomeDaMae = "Mãe do Beneficiário",
                IsActive = true,
                NumeroDoCartaoNacionalDeSaude = "0101010101",
                DeclaracaoDeNascidoVivo = "0101010101"
            };
            return titular;
        }

        public void CreateTipoDeDocumento()
        {
            var tipoDeDocumento = new TipoDeDocumento()
            {
                TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf,
                Descricao = "CPF",
                TipoPessoa = TipoPessoaEnum.Fisica,
                IsActive = true,
                EmpresaId = EmpresaIdDefaultTest,
                TenantId = 1
            };

            UsingDbContext(c =>
            {
                c.TipoDeDocumentos.Add(tipoDeDocumento);
            });
        }
        #endregion

        #region Produto
        public void CreateProdutoTest()
        {
            UsingDbContext(c =>
            {
                c.ProdutosDePlanoDeSaude.Add(CreateProdutoEntity());
            });
        }

        public void CreateOperadora()
        {
            UsingDbContext(c =>
            {
                c.Operadoras.Add(CreateOperadoraEntity());
            });
        }

        private void CreatePlanoDeSaude()
        {
            UsingDbContext(c =>
            {
                c.PlanosDeSaude.Add(CreatePlanoDeSaudeEntity());
            });
        }

        private void CreateNatureza()
        {
            UsingDbContext(c =>
            {
                c.Naturezas.Add(CreateNaturezaEntity());
            });
        }

        private void CreateUnidadeDeMedida()
        {
            UsingDbContext(c =>
            {
                c.UnidadeMedidas.Add(CreateUnidadeMedidaEntity());
            });
        }

        private UnidadeMedida CreateUnidadeMedidaEntity()
        {
            var unidadeMedida = new UnidadeMedida()
            {
                Descricao = "Unidade",
                EmpresaId = EmpresaIdDefaultTest,
                TenantId = 1,
                UnidadePadrao = UnidadePadraoEnum.Quilograma,
                Valor = 1
            };
            return unidadeMedida;
        }

        private Natureza CreateNaturezaEntity()
        {
            var natureza = new Natureza()
            {
                EmpresaId = EmpresaIdDefaultTest,
                Descricao = "Natureza",
                TenantId = 1
            };
            return natureza;
        }

        private PlanoDeSaude CreatePlanoDeSaudeEntity()
        {
            var plano = new PlanoDeSaude()
            {
                Abrangencia = AbrangenciaDoPlanoEnum.Nacional,
                Acomodacao = AcomodacaoEnum.Apartamento,
                EmpresaId = EmpresaIdDefaultTest,
                NumeroDeRegistro = "0101010101",
                PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Rubi207,
                IsActive = true,
                Reembolso = false,
                SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospComObstetricia,
                TenantId = 1
            };
            return plano;
        }

        private ProdutoDePlanoDeSaude CreateProdutoEntity()
        {
            CreatePlanoDeSaude();
            CreateUnidadeDeMedida();
            CreateNatureza();
            CreateOperadora();

            var planoDeSaudeId = 0;
            UsingDbContext(c =>
            {
                planoDeSaudeId = c.PlanosDeSaude.FirstOrDefault().Id;
            });

            var operadoraId = 0;
            UsingDbContext(c =>
            {
                operadoraId = c.Operadoras.FirstOrDefault().Id;
            });

            var natureza = new Natureza();
            UsingDbContext(c =>
            {
                natureza = c.Naturezas.FirstOrDefault();
            });

            var unidadeDeMedida = new UnidadeMedida();
            UsingDbContext(c =>
            {
                unidadeDeMedida = c.UnidadeMedidas.FirstOrDefault();
            });

            var produto = new ProdutoDePlanoDeSaude()
            {
                EmpresaId = EmpresaIdDefaultTest,
                IsActive = true,
                IsValorAjustavel = true,
                Nome = "Plano 100",
                PlanoDeSaudeId = planoDeSaudeId,
                OperadoraId = operadoraId,
                TenantId = 1,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Valor = 250.00m,
                Descricao = "Melhor plano do Brasil"
            };
            produto.AssociarNatureza(natureza, ProdutoService);
            produto.AssociarUnidadeMedida(unidadeDeMedida, ProdutoService);
            return produto;
        }
        #endregion

        #region Responsavel
        public void CreateResponsavelTest()
        {
            UsingDbContext(c =>
            {
                c.Responsaveis.Add(CreateResponsavelEntity());
            });
        }
        private Responsavel CreateResponsavelEntity()
        {
            var pessoaFisicaId = 0;
            UsingDbContext(c =>
            {
                pessoaFisicaId = c.PessoasFisicas.FirstOrDefault().Id;
            });

            var responsavel = new Responsavel()
            {
                EmpresaId = EmpresaIdDefaultTest,
                IsActive = true,
                PessoaFisicaId = pessoaFisicaId,
                TenantId = 1,
                TipoDeResponsavel = TipoDeResponsavelEnum.PaiMae
            };

            return responsavel;
        }
        #endregion

        #region Contrato
        public void CreateContratoTest()
        {
            UsingDbContext(c =>
            {
                c.Contratos.Add(CreateContratoEntity());
            });
        }
        private Contrato CreateContratoEntity()
        {
            var produtoDePlanoDeSaudeId = 0;
            UsingDbContext(c =>
            {
                produtoDePlanoDeSaudeId = c.ProdutosDePlanoDeSaude.FirstOrDefault().Id;
            });
            var contrato = new Contrato
            {
                EmpresaId = EmpresaIdDefaultTest,
                IsActive = true,
                DataCriacao = DateTime.Now,
                DataInicioVigencia = DateTime.Now.AddDays(10),
                DataFimVigencia = DateTime.Now.AddYears(1),
                Conteudo = "Contrato de Teste",
                ProdutoDePlanoDeSaudeId = produtoDePlanoDeSaudeId,
                TenantId = 1
            };

            return contrato;
        }
        #endregion
    }
}
