﻿using EZ.EZControl.Services.EZSaude.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class PlanoDeSaudeAppServiceTestBase : AppTestBase
    {
        protected readonly IPlanoDeSaudeAppService PlanoDeSaudeAppService;

        public PlanoDeSaudeAppServiceTestBase()
        {
            this.PlanoDeSaudeAppService = Resolve<IPlanoDeSaudeAppService>();
        }
    }
}
