﻿using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Domain.EZSaude.Geral;
using EZ.EZControl.Services.EZSaude.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class ParametroAppServiceTestBase : AppTestBase
    {
        protected readonly IParametroEzsaudeAppService ParametroEzsaudeAppService;
        public ParametroAppServiceTestBase()
        {
            ParametroEzsaudeAppService = Resolve<IParametroEzsaudeAppService>();
        }

        protected void CreateTestParametros()
        {
            UsingDbContext(context =>
            {
                context.ParametroEzsaude.Add(CreateParametroEntity());
            });
        }

        private ParametroEzsaude CreateParametroEntity()
        {
            return new ParametroEzsaude
            {
                AlturaMaximaPx = 100,
                ExtensoesImagem = "jpg;png;gif",
                ExtensoesDocumento = "doc;docx;pdf;xlsx",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

    }
}
