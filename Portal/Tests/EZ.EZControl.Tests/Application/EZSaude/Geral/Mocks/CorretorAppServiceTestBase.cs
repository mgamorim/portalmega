﻿using EZ.EZControl.Domain.EZSaude.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Corretora;
using EZ.EZControl.Services.EZSaude.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class CorretorAppServiceTestBase : AppTestBase
    {
        public readonly ICorretorAppService CorretorAppService;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly IPessoaJuridicaAppService PessoaJuridicaAppService;
        public readonly ICorretoraAppService CorretoraAppService;
        public readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public CorretorAppServiceTestBase()
        {
            CorretorAppService = Resolve<ICorretorAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            PessoaJuridicaAppService = Resolve<IPessoaJuridicaAppService>();
            CorretoraAppService = Resolve<ICorretoraAppService>();
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }
    }
}
