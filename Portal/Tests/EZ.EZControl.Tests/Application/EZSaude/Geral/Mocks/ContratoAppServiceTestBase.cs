﻿using EZ.EZControl.Services.EZSaude.Interfaces;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks
{
    public class ContratoAppServiceTestBase : AppTestBase
    {
        public readonly IContratoAppService ContratoAppService;

        public ContratoAppServiceTestBase()
        {
            ContratoAppService = Resolve<IContratoAppService>();
        }
    }
}
