﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Dto.EZSaude.Geral.PlanoDeSaude;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class PlanoDeSaudeAppService_Tests : PlanoDeSaudeAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreatePlanoDeSaudeAndTestAsync(string numeroDoRegistro, AbrangenciaDoPlanoEnum abrangencia, AcomodacaoEnum acomodacao,
                                                          PlanoDeSaudeAnsEnum planoAns, SegmentacaoAssistencialDoPlanoEnum segmentacao, string descricaoDoReembolso = null, bool reembolso = false)
        {
            await base.PlanoDeSaudeAppService.Save(new PlanoDeSaudeInput
            {
                IsActive = true,
                NumeroDeRegistro = numeroDoRegistro,
                DescricaoDoReembolso = descricaoDoReembolso,
                Reembolso = reembolso,
                Abrangencia = abrangencia,
                Acomodacao = acomodacao,
                PlanoDeSaudeAns = planoAns,
                SegmentacaoAssistencial = segmentacao
            });

            await UsingDbContext(async context =>
            {
                var createdPlano = await context.PlanosDeSaude.FirstOrDefaultAsync(x => x.NumeroDeRegistro == numeroDoRegistro);
                createdPlano.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Planos_De_Saude()
        {
            await CreatePlanoDeSaudeAndTestAsync("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "Não há");
            await CreatePlanoDeSaudeAndTestAsync("ANS 475.260/16-1", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Rubi207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "Não há");
            await CreatePlanoDeSaudeAndTestAsync("ANS 475.388/16-7", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação", true);
            await CreatePlanoDeSaudeAndTestAsync("ANS 475.387/16-9", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante217, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "3x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e internação", true);
        }

        [Fact]
        public async Task Should_Not_Create_Plano_De_Saude_With_Duplicate_Tipo()
        {
            await CreatePlanoDeSaudeAndTestAsync("ANS 475.261/16-9", AbrangenciaDoPlanoEnum.Estadual, AcomodacaoEnum.Enfermaria, PlanoDeSaudeAnsEnum.Safira207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "Não há");
            await base.PlanoDeSaudeAppService.Save(new PlanoDeSaudeInput
            {
                IsActive = true,
                PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Safira207,
                SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospComObstetricia,
                Abrangencia = AbrangenciaDoPlanoEnum.GrupoDeEstados,
                Acomodacao = AcomodacaoEnum.Enfermaria,
                NumeroDeRegistro = "ANS 475.267/16-8",
                Reembolso = false
            })
            .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Plano_De_Saude()
        {
            await CreatePlanoDeSaudeAndTestAsync("ANS 475.388/16-7", AbrangenciaDoPlanoEnum.Nacional, AcomodacaoEnum.Apartamento, PlanoDeSaudeAnsEnum.Diamante207, SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial, "1x Tabela CBHPM para consultas, exames, terapias e atendimento ambulatorial e 2x para internação");
            await base.PlanoDeSaudeAppService.Delete(new IdInput(1));
            await base.PlanoDeSaudeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}
