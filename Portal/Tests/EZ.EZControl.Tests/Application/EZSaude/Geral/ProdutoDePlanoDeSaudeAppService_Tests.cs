﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Dto.EZSaude.Geral.ProdutoDePlanoDeSaude;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class ProdutoDePlanoDeSaudeAppService_Tests : ProdutoDePlanoDeSaudeAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateProdutoDePlanoDeSaudeAndTestAsync(string nome, decimal valor, string descricao, int idNatureza, int idUnidade, int idOperadora, int idEspecialidade)
        {
            await base.ProdutoDePlanoDeSaudeAppService.Save(new ProdutoDePlanoDeSaudeInput
            {
                IsActive = true,
                PlanoDeSaudeId = 1,
                NaturezaId = idNatureza,
                UnidadeMedidaId = idUnidade,
                OperadoraId = idOperadora,
                Nome = nome,
                Valor = valor,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = descricao,
                EspecialidadesIds = new [] { 1 }
            });
        }


        [Fact]
        public async Task Should_Create_Produto_De_Plano_De_Saude()
        {
            base.CreateNatureza();
            base.CreateUnidade();
            base.CreatePlanoDeSaude();
            base.CreateOperadora();
            base.CreateEspecialidade();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1);
        }

        [Fact]
        public async Task Should_Not_Create_Produto_De_Plano_De_Saude_With_Duplicate_Nome()
        {
            base.CreateNatureza();
            base.CreateUnidade();
            base.CreatePlanoDeSaude();
            base.CreateOperadora();
            base.CreateEspecialidade();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1);

            await base.ProdutoDePlanoDeSaudeAppService.Save(new ProdutoDePlanoDeSaudeInput
            {
                IsActive = true,
                PlanoDeSaudeId = 2,
                Nome = "Especial 100",
                Valor = 250,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = "Teste"
            })
            .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Produto_De_Plano_De_Saude()
        {
            base.CreateNatureza();
            base.CreateUnidade();
            base.CreatePlanoDeSaude();
            base.CreateOperadora();
            base.CreateEspecialidade();
            await CreateProdutoDePlanoDeSaudeAndTestAsync("Especial 100", 250, "O maior custo beneficio.", 1, 1, 1, 1);
            await base.ProdutoDePlanoDeSaudeAppService.Delete(new IdInput(1));
            await base.ProdutoDePlanoDeSaudeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}
