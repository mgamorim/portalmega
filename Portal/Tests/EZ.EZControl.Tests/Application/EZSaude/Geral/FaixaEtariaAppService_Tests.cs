﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Dto.EZSaude.Geral.FaixaEtaria;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class FaixaEtariaAppService_Tests : FaixaEtariaAppServiceTestBase
    {
        public FaixaEtariaAppService_Tests()
        {
        }

        private FaixaEtariaInput CreateFaixaEtaria(string descricao, int idadeInicial, int? idadeFinal = null)
        {
            return new FaixaEtariaInput
            {
                IsActive = true,
                Descricao = descricao,
                IdadeInicial = idadeInicial,
                IdadeFinal = idadeFinal
            };
        }

        [Fact]
        public async Task Should_Create_FaixaEtaria()
        {
            #region Assert
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("0 A 18", 0, 18));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("19 A 23", 19, 23));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("24 A 28", 24, 28));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("29 A 33", 29, 33));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("34 A 38", 34, 38));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("39 A 43", 39, 43));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("44 A 48", 44, 48));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("49 A 53", 49, 53));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("54 A 58", 54, 58));
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("Acima de 59", 59));
            #endregion
        }

        [Fact]
        public async Task Should_Update_FaixaEtaria()
        {
            #region Arrange
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("19 A 23", 19, 23));
            #endregion

            #region Action
            var faixasEtarias = await base.FaixaEtariaAppService.GetPaginado(new GetFaixaEtariaInput
            {
                MaxResultCount = 1000,
                SkipCount = 0,
                Descricao = "19 A 23"
            });

            var faixaEtariaId = faixasEtarias.Items[0].Id;
            var faixaEtaria = await base.FaixaEtariaAppService.GetById(new IdInput(faixaEtariaId));

            faixaEtaria.Descricao = "39 A 43";
            faixaEtaria.IdadeInicial = 39;
            faixaEtaria.IdadeFinal = 43;
            #endregion

            #region Assert
            await base.FaixaEtariaAppService.Save(faixaEtaria);
            #endregion
        }

        [Fact]
        public async Task Delete_FaixaEtaria()
        {
            #region Arrange
            await base.FaixaEtariaAppService.Save(CreateFaixaEtaria("44 A 48", 44, 48));
            #endregion

            #region Action

            var faixasEtarias = await base.FaixaEtariaAppService.GetPaginado(new GetFaixaEtariaInput()
            {
                MaxResultCount = 1000,
                SkipCount = 0,
                Descricao = "44 A 48"
            });

            var faixaEtariaId = faixasEtarias.Items[0].Id;

            #endregion

            #region Assert
            await base.FaixaEtariaAppService.Delete(new IdInput(faixaEtariaId));
            await base.FaixaEtariaAppService.GetById(new IdInput(faixaEtariaId)).ShouldThrowAsync<UserFriendlyException>();
            #endregion
        }
    }
}
