﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZSaude.Geral.Laboratorio;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class LaboratorioAppService_Tests : LaboratorioAppServiceTestBase
    {
        public LaboratorioAppService_Tests()
        {
            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedDevelopment(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        private Contato CreateContato(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeContato tipoDeContato)
        {
            var tratamento = new Tratamento
            {
                Descricao = "Sr.",
                EmpresaId = EmpresaIdDefaultTest
            };
            Contato contato = new Contato(pessoa, tipoDeContato, tratamento, ContatoService)
            {
                Nome = "Ana Rita",
                EmpresaId = EmpresaIdDefaultTest
            };
            contato.AssociarTipoDeContato(tipoDeContato, ContatoService);
            base.CreateContato(contato);

            return contato;
        }

        private TipoDeLogradouro CreateTipoDeLogradouro()
        {
            TipoDeLogradouro tipoDeLogradouro = new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = "Avenida",
                EmpresaId = EmpresaIdDefaultTest
            };
            base.CreateTipoDeLogradouro(tipoDeLogradouro);

            return tipoDeLogradouro;
        }

        private PessoaJuridica CreatePessoaJuridica()
        {
            PessoaJuridica pj = new PessoaJuridica();
            pj.RazaoSocial = "Amil";
            pj.NomeFantasia = "Amil";


            Telefone telefone = new Telefone
            {
                Tipo = TipoTelefoneEnum.Celular,
                DDI = "55",
                DDD = "21",
                Numero = "970047456",
                Ramal = "21",
                EmpresaId = EmpresaIdDefaultTest
            };


            if (pj.Telefones == null) pj.Telefones = new List<Telefone>();

            pj.Telefones.Add(telefone);

            base.CreatePessoa(pj);

            return pj;
        }

        private Endereco CreateEnderecoInput(string descricao)
        {
            var pessoa = this.CreatePessoaJuridica();
            var tipoDeContato = this.CreateTipoDeContato();
            var contato = this.CreateContato(pessoa, tipoDeContato);
            var tipoDeLogradouro = this.CreateTipoDeLogradouro();
            var cidade = this.CreateCidade();

            Endereco endereco = new Endereco
            {
                PessoaId = pessoa.Id,
                ContatoId = contato.Id,
                CidadeId = cidade.Id,
                TipoDeLogradouroId = tipoDeLogradouro.Id,
                TipoEndereco = TipoEnderecoEnum.Residencial,
                Descricao = descricao,
                Logradouro = "Av. Vicente de Carvalho",
                Numero = "6524",
                Referencia = "Próximo ao Metrô de Vicente de Carvalho",
                Bairro = "Vicente de Carvalho",
                Complemento = "Casa 2",
                EnderecoParaEntrega = false,
                CEP = "21633000",
                EmpresaId = EmpresaIdDefaultTest
            };

            CreateEndereco(endereco);

            return endereco;
        }

        private TipoDeContato CreateTipoDeContato()
        {
            TipoDeContato tipoDeContato = new TipoDeContato
            {
                Descricao = "Pessoal",
                EmpresaId = EmpresaIdDefaultTest
            };
            base.CreateTipoDeContato(tipoDeContato);

            return tipoDeContato;
        }

        private Pais CreatePais()
        {
            var pais = base.CreatePais("Argentina", "Argentino", "AR");
            base.CreatePais(pais);

            return pais;
        }

        private Estado CreateEstado(Pais pais)
        {
            var estado = base.CreateEstado(pais, "Buenos Aires", "Buenos Aires", "BA");

            base.CreateEstado(estado);

            return estado;
        }

        private Cidade CreateCidade()
        {
            var pais = CreatePais("Argentina", "Argentino", "AR");
            var estado = CreateEstado(pais, "Buenos Aires", "La Plata", "RJ");
            var cidade = CreateCidade(estado, "La Plata", "3304904");

            base.CreateCidade(pais, estado, cidade);

            return cidade;
        }

        private async Task<IdInput> CreateLaboratorioAndTestAsync()
        {
            var endereco = CreateEnderecoInput("Endereço 1");

            int telefoneId = base.GetTelefoneId(endereco.PessoaId);

            var l = new LaboratorioInput();
            l.Nome = "Laboratório 1";
            l.IsActive = true;
            l.PessoaJuridicaId = endereco.PessoaId;
            l.EnderecoId = endereco.Id;
            l.Telefone1Id = telefoneId;

            return await laboratorioAppService.Save(l);
        }

        [Fact]
        public async Task Should_Create_Laboratorio()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await CreateLaboratorioAndTestAsync();
            });
        }

        [Fact]
        public async Task Delete_Laboratorio()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var laboratorio = await CreateLaboratorioAndTestAsync();
                await base.laboratorioAppService.Delete(laboratorio);
                await uow.Current.SaveChangesAsync();
                await base.laboratorioAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}