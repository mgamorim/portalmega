﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.EZSaude.SubtipoPessoa.Administradora;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Tests.Application.EZSaude.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZSaude.Geral
{
    public class AdministradoraAppService_Tests : AdministradoraAppServiceTestBase
    {
        public AdministradoraAppService_Tests()
        {
            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedDevelopment(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        private async Task<PessoaJuridicaInput> SavePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            PessoaJuridicaInput pessoaJuridicaInput = new PessoaJuridicaInput
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial,
                TipoPessoa = TipoPessoaEnum.Juridica,
                IsActive = true
            };

            var result = await base.PessoaJuridicaAppService.SaveAndReturnEntity(pessoaJuridicaInput);

            return result;
        }

        private async Task<GrupoPessoaInput> CreateGrupoPessoa(string nome, string descricao)
        {
            var grupo = new GrupoPessoaInput
            {
                IsActive = true,
                Descricao = descricao,
                Nome = nome
            };

            return await base.GrupoPessoaAppService.SaveAndReturnEntity(grupo);
        }

        private async Task<IdInput> CreateAdministradoraAndTestAsync(string razaoSocial, string nomeFantasia)
        {
            var pj = await SavePessoaJuridica(nomeFantasia, razaoSocial);
            return await AdministradoraAppService.Save(new AdministradoraInput
            {
                IsActive = true,
                PessoaJuridica = pj,
                PessoaJuridicaId = pj.Id
            });
        }

       
        [Fact]
        public async Task Should_Create_Administradora()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await CreateAdministradoraAndTestAsync("Bem Benefícios", "Bem Benefícios LTDA");
            });
        }

        [Fact]
        public async Task Delete_Administradora()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var administradora = await CreateAdministradoraAndTestAsync("Mosaíco Administradora", "M Administradora Eirelli");
                await base.AdministradoraAppService.Delete(administradora);
                await uow.Current.SaveChangesAsync();
                await base.AdministradoraAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}