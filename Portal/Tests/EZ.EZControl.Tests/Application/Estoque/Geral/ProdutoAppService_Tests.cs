﻿using System.Data.Entity;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Tests.Application.Estoque.Geral.Mocks;
using Shouldly;
using Xunit;
using EZ.EZControl.Domain.Estoque.Geral;

namespace EZ.EZControl.Tests.Application.Estoque.Geral
{
    public class ProdutoAppService_Tests : ProdutoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Produto()
        {
            var unidadeMedida = await CreateUnidadeDeMedidaAsync();
            var natureza = await CreateNaturezaAsync();
            await CreateProdutoAndTestAsync("EZCRM", "Produto EZCRM", 550.69M, false, unidadeMedida, natureza);
            await CreateProdutoAndTestAsync("EZCMS", "Produto EZCMS", 1500.64M, true, unidadeMedida, natureza);
        }

        [Fact]
        public async Task Should_Not_Create_Produto_With_Duplicate_Nome()
        {
            CreateTestEstoque();
            await ProdutoAppService.Save(
                new ProdutoInput()
                {
                    Valor = 550.69M,
                    Nome = "EZCRM",
                    Descricao = "Produto EZCRM",
                    IsValorAjustavel = false
                }).ShouldThrowAsync<AbpValidationException>();
        }

        private async Task CreateProdutoAndTestAsync(string nome, string descricao, decimal valor, bool isValorAjustavel, UnidadeMedida unidadeDeMedida, Natureza natureza)
        {
            await ProdutoAppService.Save(
                new ProdutoInput()
                {
                    Nome = nome,
                    Valor = valor,
                    Descricao = descricao,
                    IsValorAjustavel = isValorAjustavel,
                    UnidadeMedidaId = unidadeDeMedida.Id,
                    NaturezaId = natureza.Id
                });
            await UsingDbContext(async context =>
            {
                var createdProduto = await context.Produtos.FirstOrDefaultAsync(b => b.Nome == nome);
                createdProduto.ShouldNotBe(null);
            });
        }

        private async Task<UnidadeMedida> CreateUnidadeDeMedidaAsync()
        {
            UnidadeMedida unidadeMedida = null;

            await UsingDbContextAsync(async context =>
            {
                unidadeMedida = new UnidadeMedida()
                {
                    Descricao = "Metro",
                    IsActive = true
                };

                context.UnidadeMedidas.Add(unidadeMedida);
                await context.SaveChangesAsync();
            });

            return unidadeMedida;
        }

        private async Task<Natureza> CreateNaturezaAsync()
        {
            Natureza natureza = null;

            await UsingDbContextAsync(async context =>
            {
                natureza = new Natureza()
                {
                    Descricao = "Natureza",
                    IsActive = true
                };

                context.Naturezas.Add(natureza);
                await context.SaveChangesAsync();
            });

            return natureza;
        }

        [Fact]
        public async Task Delete_Produto()
        {
            base.CreateTestEstoque();
            await base.ProdutoAppService.Delete(new IdInput(1));
            await base.ProdutoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

    }
}