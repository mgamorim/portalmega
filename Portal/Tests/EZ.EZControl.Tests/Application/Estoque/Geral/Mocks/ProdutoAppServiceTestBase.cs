﻿using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Services.Estoque.Interfaces;

namespace EZ.EZControl.Tests.Application.Estoque.Geral.Mocks
{
    public abstract class ProdutoAppServiceTestBase : AppTestBase
    {
        protected readonly IProdutoAppService ProdutoAppService;

        public ProdutoAppServiceTestBase()
        {
            ProdutoAppService = Resolve<IProdutoAppService>();
        }

        protected void CreateTestEstoque()
        {
            UsingDbContext(context =>
            {
                context.Produtos.Add(CreateProdutoEntity("EZCRM", "Produto EZCRM", 550.69M, false));
                context.Produtos.Add(CreateProdutoEntity("EZCMS", "Produto EZCMS", 1500.64M, true));
            });
        }

        private Produto CreateProdutoEntity(string nome, string descricao, decimal valor, bool isValorAjustavel)
        {
            return new Produto()
            {
                Nome = nome,
                Valor = valor,
                Descricao = descricao,
                IsValorAjustavel = isValorAjustavel,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}