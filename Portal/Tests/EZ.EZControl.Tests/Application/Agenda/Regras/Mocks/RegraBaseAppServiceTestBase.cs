﻿using System;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;

namespace EZ.EZControl.Tests.Application.Agenda.Regras.Mocks
{
    public abstract class RegraBaseAppServiceTestBase : AppTestBase
    {
        protected readonly IRegraBaseAppService RegraBaseAppService;

        protected RegraBaseAppServiceTestBase()
        {
            RegraBaseAppService = Resolve<IRegraBaseAppService>();
        }

        protected void CreateTestRegraBase()
        {
            UsingDbContext(context =>
            {
                context.RegraBases.Add(CreateRegraBaseEntity());
            });
        }

        private RegraBase CreateRegraBaseEntity()
        {
            return new RegraBase()
            {
                IsActive = true,
                Sistema = SistemaEnum.Global,
                DiasAntesDoAgendamentoProibidoAlterar = 10,
                DiasExibidosDesdeHojeAteAgendamento = 90,
                DiasParaNovoAgendamentoPorParticipante = 15,
                OverBook = false,
                UnidadeDeTempo = 30,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                VagasDisponibilizadasPorDia = 14,
                VagasPorUnidadeDeTempo = 3,
                VagasReservadasPorDia = 12,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}