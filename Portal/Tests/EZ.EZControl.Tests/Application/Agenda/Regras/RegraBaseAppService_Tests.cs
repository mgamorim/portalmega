﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda.Regra;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using EZ.EZControl.Tests.Application.Agenda.Regras.Mocks;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Application.Agenda.Regras
{
    public class RegraBaseAppService_Tests : RegraBaseAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_RegraBase()
        {
            #region Arrange
            await RegraBaseAppService.Save(new RegraBaseInput
            {
                IsActive = true,
                Sistema = SistemaEnum.Global,
                DiasAntesDoAgendamentoProibidoAlterar = 10,
                DiasExibidosDesdeHojeAteAgendamento = 90,
                DiasParaNovoAgendamentoPorParticipante = 15,
                OverBook = false,
                UnidadeDeTempo = 30,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                VagasDisponibilizadasPorDia = 14,
                VagasPorUnidadeDeTempo = 3,
                VagasReservadasPorDia = 12
            });
            #endregion


            await UsingDbContext(async context =>
            {
                #region Action
                var created = await context.RegraBases.FirstOrDefaultAsync();
                #endregion


                #region Action
                created.ShouldNotBe(null);
                created.Sistema.ShouldBe(SistemaEnum.Global);
                created.TipoDeEvento.ShouldBe(TipoDeEventoEnum.Tarefa);
                created.UnidadeDeTempo.ShouldBe(30);
                #endregion
            });

        }

        [Fact]
        public async Task Should_Update_RegraBase()
        {
            #region Arrange
            base.CreateTestRegraBase();
            var regraBase = await base.RegraBaseAppService.GetById(new IdInput(1));
            #endregion

            #region Action
            regraBase.UnidadeDeTempo = 12;
            await base.RegraBaseAppService.Save(regraBase);
            #endregion

            #region Assert

            base.RegraBaseAppService.GetById(new IdInput(1)).Result.UnidadeDeTempo.ShouldBe(12);

            #endregion
        }

        [Fact]
        public async Task Should_Delete_RegraBase()
        {
            #region Arrange
            base.CreateTestRegraBase();
            #endregion

            #region Action
            await base.RegraBaseAppService.Delete(new IdInput(1));
            #endregion

            #region Assert
            await base.RegraBaseAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            #endregion
        }
    }
}