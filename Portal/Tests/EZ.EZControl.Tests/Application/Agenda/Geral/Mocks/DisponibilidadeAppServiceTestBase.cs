﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Agenda.Geral.Mocks
{
    public class DisponibilidadeAppServiceTestBase : AppTestBase
    {
        protected readonly IDisponibilidadeAppService DisponibilidadeAppService;
        public DisponibilidadeAppServiceTestBase()
        {
            this.DisponibilidadeAppService = Resolve<IDisponibilidadeAppService>();
        }

        protected void CreateTestDisponibilidades()
        {
            UsingDbContext(context =>
            {
                context.Disponibilidades.Add(CreateDisponibilidade(CreateConfiguracaoDeDisponibilidade(), DateTime.Now, DateTime.Now.AddHours(5).TimeOfDay));
                context.Disponibilidades.Add(CreateDisponibilidade(CreateConfiguracaoDeDisponibilidade(), DateTime.Now, DateTime.Now.AddHours(15).TimeOfDay));
            });
        }

        private Disponibilidade CreateDisponibilidade(ConfiguracaoDeDisponibilidade configuracaoDeDisponibilidade, DateTime data, TimeSpan horario)
        {
            return new Disponibilidade
            {
                IsActive = true,
                Data = data,
                Horario = horario,
                ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidade,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ConfiguracaoDeDisponibilidade CreateConfiguracaoDeDisponibilidade()
        {
            return new ConfiguracaoDeDisponibilidade()
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}