﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using System;

namespace EZ.EZControl.Tests.Application.Agenda.Geral.Mocks
{
    public class ConfiguracaoDeBloqueioAppServiceTestBase : AppTestBase
    {
        protected readonly IConfiguracaoDeBloqueioAppService ConfiguracaoDeBloqueioAppService;
        protected readonly IRegraTarefaAppService TarefaAppService;
        public ConfiguracaoDeBloqueioAppServiceTestBase()
        {
            this.ConfiguracaoDeBloqueioAppService = Resolve<IConfiguracaoDeBloqueioAppService>();
            this.TarefaAppService = Resolve<IRegraTarefaAppService>();
        }

        protected void CreateTestConfiguracaoDeBloqueios()
        {
            UsingDbContext(context =>
            {
                context.ConfiguracoesDeBloqueio.Add(CreateConfiguracaoDeBloqueio());
            });
        }

        private ConfiguracaoDeBloqueio CreateConfiguracaoDeBloqueio()
        {
            return new ConfiguracaoDeBloqueio
            {
                IsActive = true,
                FimBloqueio = new DateTime(2016, 6, 30),
                HorarioFim = new TimeSpan(18, 0, 0),
                HorarioInicio = new TimeSpan(9, 0, 0),
                InicioBloqueio = new DateTime(2016, 1, 1),
                MotivoDoBloqueio = MotivoDoBloqueioEnum.Ferias,
                Sistema = SistemaEnum.Agenda,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                Titulo = "Bloqueio de Férias",
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}