﻿using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Services.Agenda.Interfaces;

namespace EZ.EZControl.Tests.Application.Agenda.Geral.Mocks
{
    public abstract class ParametroAgendaAppServiceTestBase : AppTestBase
    {
        protected readonly IParametroAgendaAppService ParametroAgendaAppService;

        protected ParametroAgendaAppServiceTestBase()
        {
            ParametroAgendaAppService = Resolve<IParametroAgendaAppService>();
        }

        protected void CreateTestParametros()
        {
            UsingDbContext(context =>
            {
                context.ParametrosAgenda.Add(CreateParametroEntity());
            });
        }

        private ParametroAgenda CreateParametroEntity()
        {
            return new ParametroAgenda
            {
                AlturaMaximaPx = 100,
                ExtensoesImagem = "jpg;png;gif",
                ExtensoesDocumento = "doc;docx;pdf;xlsx",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}