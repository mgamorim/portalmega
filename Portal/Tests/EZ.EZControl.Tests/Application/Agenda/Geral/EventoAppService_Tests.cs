﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda.Geral.Evento;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Tests.Application.Agenda.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Agenda.Geral
{
    public class EventoAppService_Tests : EventoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateEventoAndTestAsync(string titulo, string descricao, DateTime inicio, DateTime termino,
            SistemaEnum sistema, TipoDeEventoEnum tipoDeEvento, StatusDoEventoEnum status)
        {

            await base.EventoAppService.Save(new EventoInput
            {
                DiaInteiro = true,
                Titulo = titulo,
                Descricao = descricao,
                Inicio = inicio,
                Termino = termino,
                OwnerId = 1,
                DisponibilidadeId = 1,
                Sistema = sistema,
                TipoDeEvento = tipoDeEvento,
                StatusDoEvento = status,
                Participantes = new PessoaListDto[]
                {
                    new PessoaJuridicaListDto
                    {
                        NomeFantasia = "Fulano Software",
                        RazaoSocial = "Fulano Software SA"
                    }
                }
            });

            await UsingDbContext(async context =>
            {
                var createdEvento = await context.EventosBase.FirstOrDefaultAsync();
                createdEvento.ShouldNotBe(null);
            });
        }

        private async Task CreateDisponibilidadeAndTestAsync()
        {
            base.CreateDisponibilidade();
            await UsingDbContext(async context =>
            {
                var createdDispo = await context.Disponibilidades.FirstOrDefaultAsync(t => t.Id == 1);
                createdDispo.ShouldNotBe(null);
            });
        }

        private async Task CreateOwnerAndTestAsync()
        {
            base.CreatePessoaJuridica("Teste", "Teste SA");
            await UsingDbContext(async context =>
            {
                var createdOwner = await context.PessoasJuridicas.FirstOrDefaultAsync(t => t.Id == 1);
                createdOwner.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Evento()
        {
            await CreateOwnerAndTestAsync();
            await CreateDisponibilidadeAndTestAsync();
            await CreateEventoAndTestAsync("Teste de Evento", "Evento de Teste", new DateTime(2016, 10, 10, 09, 30, 00), new DateTime(2016, 10, 15, 18, 30, 00), SistemaEnum.CMS, TipoDeEventoEnum.Tarefa, StatusDoEventoEnum.Transferido);
        }

        [Fact]
        public async Task Delete_Evento()
        {
            base.CreateTestEventos();
            await base.EventoAppService.Delete(new IdInput(1));
            await base.EventoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}