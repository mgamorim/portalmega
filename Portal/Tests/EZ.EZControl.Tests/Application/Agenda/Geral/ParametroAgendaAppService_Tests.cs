﻿using EZ.EZControl.Dto.Agenda.Geral.Parametro;
using EZ.EZControl.Tests.Application.Agenda.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Agenda.Geral
{
    public class ParametroAgendaAppService_Tests : ParametroAgendaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Parametro()
        {
            await CreateParametrosAsync();
        }

        private async Task CreateParametrosAsync()
        {
            base.CreateTestParametros();

            await ParametroAgendaAppService.Save(new ParametroAgendaInput
            {
                AlturaMaximaPx = 100,
                ExtensoesDocumento = "doc;pdf",
                ExtensoesImagem = "png;jpeg",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1
            });

            await UsingDbContext(async context =>
            {
                var createdParametro = await context.ParametrosAgenda.FirstOrDefaultAsync();
                createdParametro.ShouldNotBe(null);
            });
        }
    }
}