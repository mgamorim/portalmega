﻿using EZ.EZControl.Tests.Application.Agenda.Geral.Mocks;

namespace EZ.EZControl.Tests.Application.Agenda.Geral
{
    public class DisponibilidadeAppService_Tests : DisponibilidadeAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        //private async Task CreateDisponibilidadeAndTestAsync(ConfiguracaoDeDisponibilidadeInput configuracaoDeDisponibilidade, DateTime data, DateTime horario)
        //{
        //    await base.DisponibilidadeAppService.Save(new DisponibilidadeInput
        //    {
        //        IsActive = true,
        //        Data = data,
        //        Horario = horario.TimeOfDay,
        //        ConfiguracaoDeDisponibilidadeId = configuracaoDeDisponibilidade.Id
        //    });
        //    await UsingDbContext(async context =>
        //    {
        //        var createdDisponibilidade = await context.Disponibilidades.FirstOrDefaultAsync(x => x.Data == data);
        //        createdDisponibilidade.ShouldNotBe(null);
        //    });
        //}

        //[Fact]
        //public async Task Should_Create_Disponibilidade()
        //{
        //    var configuracao = new ConfiguracaoDeDisponibilidadeInput();
        //    await CreateDisponibilidadeAndTestAsync(configuracao, DateTime.Now, DateTime.Now.AddHours(10));
        //}

        //[Fact]
        //public async Task Delete_Disponibilidade()
        //{
        //    base.CreateTestDisponibilidades();
        //    await base.DisponibilidadeAppService.Delete(new IdInput(1));
        //    await base.DisponibilidadeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        //}
    }
}