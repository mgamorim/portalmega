﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Dto.Agenda.Geral.ConfiguracaoDeBloqueio;
using EZ.EZControl.Dto.Agenda.Regra;
using EZ.EZControl.Tests.Application.Agenda.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Agenda.Geral
{
    public class ConfiguracaoDeBloqueioAppService_Tests : ConfiguracaoDeBloqueioAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateConfiguracaoDeBloqueioAndTestAsync()
        {
            await base.ConfiguracaoDeBloqueioAppService.Save(new ConfiguracaoDeBloqueioInput
            {
                IsActive = true,
                FimBloqueio = new DateTime(2016, 6, 30),
                HorarioFim = "08:00",
                HorarioInicio = "17:00",
                InicioBloqueio = new DateTime(2016, 1, 1),
                MotivoDoBloqueio = MotivoDoBloqueioEnum.Ferias,
                Sistema = SistemaEnum.Agenda,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                Titulo = "Bloqueio de Férias"
            });
            await UsingDbContext(async context =>
            {
                var createdConfiguracaoDeBloqueio = await context.ConfiguracoesDeBloqueio.FirstOrDefaultAsync();
                createdConfiguracaoDeBloqueio.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_ConfiguracaoDeBloqueio()
        {
            try
            {
                await this.RunAsyncInTransaction(async (uow) =>
                {
                    await CreateRegra();
                    await CreateConfiguracaoDeBloqueioAndTestAsync();
                });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private async Task CreateRegra()
        {
            await base.TarefaAppService.Save(new RegraTarefaInput()
            {
                DiasAntesDoAgendamentoProibidoAlterar = 2,
                DiasExibidosDesdeHojeAteAgendamento = 60,
                DiasParaNovoAgendamentoPorParticipante = 15,
                IsActive = true,
                OverBook = true,
                Sistema = SistemaEnum.Agenda,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                UnidadeDeTempo = 30,
                VagasDisponibilizadasPorDia = 30,
                VagasPorUnidadeDeTempo = 1,
                VagasReservadasPorDia = 5
            });
        }

        [Fact]
        public async Task Delete_ConfiguracaoDeBloqueio()
        {
            base.CreateTestConfiguracaoDeBloqueios();
            await base.ConfiguracaoDeBloqueioAppService.Delete(new IdInput(1));
            await base.ConfiguracaoDeBloqueioAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}