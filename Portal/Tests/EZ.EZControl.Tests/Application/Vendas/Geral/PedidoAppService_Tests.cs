﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Vendas.Enums;
using EZ.EZControl.Dto.Estoque.Geral.Natureza;
using EZ.EZControl.Dto.Estoque.Geral.Produto;
using EZ.EZControl.Dto.Estoque.Geral.UnidadeMedida;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using EZ.EZControl.Dto.Vendas.Geral.ItemDePedido;
using EZ.EZControl.Dto.Vendas.Geral.Pedido;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Tests.Application.Vendas.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Vendas.Geral
{
    public class PedidoAppService_Tests : PedidoAppServiceTestBase
    {
        private readonly IGrupoPessoaAppService grupoPessoaAppService;
        private readonly IUnidadeMedidaAppService unidadeMedidaAppService;
        private readonly INaturezaAppService naturezaAppService;

        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        public PedidoAppService_Tests()
        {
            grupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
            unidadeMedidaAppService = Resolve<IUnidadeMedidaAppService>();
            naturezaAppService = Resolve<INaturezaAppService>();
        }

        #region Cliente

        private ClienteInput CreateClienteInput(
            int grupoPessoaId,
            string nome,
            string nomeFantasia,
            string razaoSocial,
            TipoPessoaEnum tipoPessoa,
            string observacao)
        {
            var input = new ClienteInput();

            input.Pessoa = new PessoaInput();
            input.Pessoa.GrupoPessoaId = grupoPessoaId;
            input.Pessoa.TipoPessoa = tipoPessoa;
            input.Pessoa.Observacao = observacao;

            if (tipoPessoa == TipoPessoaEnum.Fisica)
            {
                input.PessoaFisica = new PessoaFisicaInputAux();
                input.PessoaFisica.Nome = nome;
            }
            else
            {
                input.PessoaJuridica = new PessoaJuridicaInputAux();
                input.PessoaJuridica.NomeFantasia = nomeFantasia;
                input.PessoaJuridica.RazaoSocial = razaoSocial;
            }

            return input;
        }

        private async Task<GrupoPessoaInput> CreateGrupoPessoa()
        {
            var grupo = new GrupoPessoaInput
            {
                IsActive = true,
                Descricao = "Grupo Administrativo",
                Nome = "Grupo Administrativo"
            };

            return await grupoPessoaAppService.SaveAndReturnEntity(grupo);
        }

        public async Task<ClientePessoaIdDto> SaveCliente(
            int grupoPessoaId,
            string nome,
            string nomeFantasia,
            string razaoSocial,
            TipoPessoaEnum tipoPessoa,
            string observacao)
        {
            var input = CreateClienteInput(grupoPessoaId, nome, nomeFantasia, razaoSocial, tipoPessoa, observacao);
            var cliente = await ClienteAppService.Save(input);
            return cliente;
        }

        #endregion Cliente

        #region Produto

        public async Task<IdInput> CreateProdutoAndTestAsync(string nome, string descricao, decimal valor,
            bool isValorAjustavel, int unidadeMedidaId, int naturezaId)
        {
            var id = await ProdutoAppService.Save(
                new ProdutoInput()
                {
                    Nome = nome,
                    Valor = valor,
                    Descricao = descricao,
                    IsValorAjustavel = isValorAjustavel,
                    IsActive = true,
                    UnidadeMedidaId = unidadeMedidaId,
                    NaturezaId = naturezaId
                });

            return id;
        }

        public async Task<PagedResultDto<ProdutoListDto>> GetProdutosPaginado(GetProdutoInput input)
        {
            return await ProdutoAppService.GetProdutoPaginado(input);
        }

        #endregion Produto

        private async Task<PedidoInput> CreatePedidoInput()
        {
            var grupoPessoa = await this.CreateGrupoPessoa();
            var clienteId = await SaveCliente(grupoPessoa.Id, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");

            var unidadeMedida = await unidadeMedidaAppService.SaveAndReturnEntity(new UnidadeMedidaInput()
            {
                IsActive = true,
                Descricao = "Metro",
                Valor = 1,
                UnidadePadrao = UnidadePadraoEnum.Metro
            });

            var natureza = await naturezaAppService.SaveAndReturnEntity(new NaturezaInput()
            {
                IsActive = true,
                Descricao = "Natureza"
            });

            await CreateProdutoAndTestAsync("EZCRM", "Produto EZCRM", 550.69M, false, unidadeMedida.Id, natureza.Id);
            await CreateProdutoAndTestAsync("EZCMS", "Produto EZCMS", 1500.64M, true, unidadeMedida.Id, natureza.Id);

            var produtoList = await GetProdutosPaginado(new GetProdutoInput());
            var produto = produtoList.Items.FirstOrDefault(x => x.Nome == "EZCRM");

            return new PedidoInput()
            {
                ClienteId = clienteId.ClienteId,
                Items = new List<ItemDePedidoListDto>()
                {
                    new ItemDePedidoListDto()
                    {
                        Produto = produto,
                        Quantidade = 1,
                        TipoDeItemDePedido = TipoDeItemDePedidoEnum.Produto,
                        Valor = produtoList.Items.FirstOrDefault(x => x.Id == 1).Valor,
                        ValorCadastro = produtoList.Items.FirstOrDefault(x => x.Id == 1).Valor
                    }
                }
            };
        }

        private async Task CreatePedidoAndTestAsync(PedidoInput pedidoInput)
        {
            var id = await this.PedidoAppService.Save(pedidoInput);

            await UsingDbContextAsync(async context =>
            {
                var pedidoCreated = await context.Pedidos.FirstOrDefaultAsync(x => x.Id == id.Id);
                pedidoCreated.ShouldNotBeNull();
            });
        }

        [Fact]
        public async Task Should_Create_Pedido()
        {
            var pedidoInput = await CreatePedidoInput();
            await this.CreatePedidoAndTestAsync(pedidoInput);

            UsingDbContext((context) =>
            {
                context.Pedidos.Count().ShouldBe(1);
            });
        }

        // TODO: Este teste só funciona se rodar na base de dados local.
        // Parece que existe alguma limitação do Effort para executar uma query com groupby.

        //[Fact]
        //public async Task Should_Get_Pedido_Paginado()
        //{

        //    var pedidoInput = await CreatePedidoInput();
        //    await this.CreatePedidoAndTestAsync(pedidoInput);

        //    await RunAsyncInTransaction(async (uow) =>
        //    {
        //        var getPedidoInput = new GetPedidoInput();
        //        getPedidoInput.NomeCliente = string.Empty;
        //        PagedResultDto<PedidoListDto> dados = await PedidoAppService.GetPedidoPaginado(getPedidoInput);
        //        dados.TotalCount.ShouldBe(1);
        //    }, true);
        //}
    }
}