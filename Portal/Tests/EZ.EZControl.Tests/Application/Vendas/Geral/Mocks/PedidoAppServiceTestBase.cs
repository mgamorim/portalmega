﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Estoque.Geral;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Estoque.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Vendas.Geral.Interfaces;
using EZ.EZControl.Services.Vendas.Pedidos.Interfaces;

namespace EZ.EZControl.Tests.Application.Vendas.Geral.Mocks
{
    public abstract class PedidoAppServiceTestBase : AppTestBase
    {
        protected readonly IPedidoAppService PedidoAppService;
        protected readonly IPedidoService PedidoService;
        protected readonly IItemDePedidoService ItemDePedidoService;
        public readonly IClienteAppService ClienteAppService;
        public readonly IRepository<Cliente> ClienteRepository;
        protected readonly IProdutoAppService ProdutoAppService;

        protected PedidoAppServiceTestBase()
        {
            PedidoService = Resolve<IPedidoService>();
            PedidoAppService = Resolve<IPedidoAppService>();
            ItemDePedidoService = Resolve<IItemDePedidoService>();
            ClienteAppService = Resolve<IClienteAppService>();
            ClienteRepository = Resolve<IRepository<Cliente>>();
            ProdutoAppService = Resolve<IProdutoAppService>();
        }

        protected void CreateTestEstoque()
        {
            UsingDbContext(context =>
            {
                context.Produtos.Add(CreateProdutoEntity("EZCRM", "Produto EZCRM", 550.69M, false));
                context.Produtos.Add(CreateProdutoEntity("EZCMS", "Produto EZCMS", 1500.64M, true));
            });
        }

        private Produto CreateProdutoEntity(string nome, string descricao, decimal valor, bool isValorAjustavel)
        {
            return new Produto()
            {
                Nome = nome,
                Valor = valor,
                Descricao = descricao,
                IsValorAjustavel = isValorAjustavel
            };
        }
    }
}