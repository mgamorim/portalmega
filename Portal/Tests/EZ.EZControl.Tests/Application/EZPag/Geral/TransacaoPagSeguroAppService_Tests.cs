﻿using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZPag.Geral
{
    public class TransacaoPagSeguroAppService_Tests : AppTestBase
    {
        private readonly ITransacaoPagSeguroAppService transacaoAppService;
        public TransacaoPagSeguroAppService_Tests()
        {
            transacaoAppService = Resolve<ITransacaoPagSeguroAppService>();
        }
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }
        [Fact]       
        public void Should_Create_Session()
        {
            var session = transacaoAppService.CriarSessao(new Abp.Application.Services.Dto.IdInput(1));
            session.Id.ShouldNotBeNull();
        }
        [Fact]
        public void Should_Checkout_Via_Boleto()
        {
            //TODO: Criar input de exemplo de checkout via boleto
            //transacaoAppService.CheckoutViaBoleto().ShouldNotThrow();
        }
        [Fact]
        public void Should_Checkout_Via_Cartao_De_Credito()
        {
            //TODO: Criar input de exemplo de checkout via cartão de crédito
            //transacaoAppService.CheckoutViaCartaoDeCredito().ShouldNotThrow();
        }
        [Fact]
        public void Should_Checkout_Via_Debito_Online()
        {
            //TODO: Criar input de exemplo de checkout via débito online
            //transacaoAppService.CheckoutViaDebitoOnline().ShouldNotThrow();
        }
    }
}
