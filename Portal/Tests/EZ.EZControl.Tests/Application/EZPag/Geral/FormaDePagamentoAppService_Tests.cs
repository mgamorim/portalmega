﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Dto.EZPag.Geral.FormaDePagamento;
using EZ.EZControl.Tests.Application.EZPag.Geral.Mocks;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZPag.Geral
{
    public class FormaDePagamentoAppService_Tests: FormaDePagamentoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateFormaPagamentoAndTestAsync(string descricao, TipoCheckoutPagSeguroEnum tipo)
        {
            await base.FormaDePagamentoAppService.Save(new FormaDePagamentoInput
            {
                IsActive = true,
                Descricao = descricao,
                TipoDePagamento=tipo
            });
        }

        [Fact]
        public async Task Should_Create_FormaDePagamento()
        {
            base.CreateTestFormaDePagamento();
            await CreateFormaPagamentoAndTestAsync("DÉBITO",TipoCheckoutPagSeguroEnum.ONLINE_DEBIT);
        }
        
        [Fact]
        public async Task Delete_FormaDePagamento()
        {
            base.CreateTestFormaDePagamento();
            await base.FormaDePagamentoAppService.Delete(new IdInput(1));
            await base.FormaDePagamentoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}
