﻿using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Tests.Application.EZPag.Geral.Mocks
{
    public class ParametroAppServiceTestBase: AppTestBase
    {
        protected readonly IParametroEzpagAppService ParametroEzpagAppService;
        public ParametroAppServiceTestBase()
        {
            ParametroEzpagAppService = Resolve<IParametroEzpagAppService>();
        }

        protected void CreateTestParametros()
        {
            UsingDbContext(context =>
            {
                context.ParametroEzpag.Add(CreateParametroEntity());
            });
        }

        private ParametroEzpag CreateParametroEntity()
        {
            return new ParametroEzpag
            {
                AlturaMaximaPx = 100,
                ExtensoesImagem = "jpg;png;gif",
                ExtensoesDocumento = "doc;docx;pdf;xlsx",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1,
                IsTest =true,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
