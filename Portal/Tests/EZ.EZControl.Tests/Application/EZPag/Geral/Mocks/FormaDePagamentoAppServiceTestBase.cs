﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZPag.Geral;
using EZ.EZControl.Migrations;
using EZ.EZControl.Services.EZPag.Geral.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EZPag.Geral.Mocks
{
    public class FormaDePagamentoAppServiceTestBase: AppTestBase
    {
        protected readonly IFormaDePagamentoAppService FormaDePagamentoAppService;

        public FormaDePagamentoAppServiceTestBase()
        {
            this.FormaDePagamentoAppService = Resolve<IFormaDePagamentoAppService>();
        }


        protected void CreateTestFormaDePagamento()
        {
            UsingDbContext(context =>
            {
                context.FormasDePagamentos.Add(CreateFormaDePagamentoEntity("BOLETO", TipoCheckoutPagSeguroEnum.BOLETO));
                context.FormasDePagamentos.Add(CreateFormaDePagamentoEntity("CARTÃO", TipoCheckoutPagSeguroEnum.CREDIT_CARD));
            });
        }

        private EZControl.Domain.EZPag.Geral.FormaDePagamento CreateFormaDePagamentoEntity(string descricao, TipoCheckoutPagSeguroEnum tipo)
        {
            return new EZControl.Domain.EZPag.Geral.FormaDePagamento
            {
                IsActive =true,
                Descricao = descricao,
                TipoDePagamento = tipo,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
