﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Domain.EZPag.Geral;
using EZ.EZControl.Services.EZPag.Interfaces;

namespace EZ.EZControl.Tests.Application.EZPag.Geral.Mocks
{
    public class TransacaoAppServiceTestBase : AppTestBase
    {
        protected readonly ITransacaoService TransacaoService;
        public TransacaoAppServiceTestBase()
        {
            TransacaoService = Resolve<ITransacaoService>();
        }

        public Transacao NovaInstanciaDeTransacao()
        {
            return new Transacao(); ;
        }
    }
}
