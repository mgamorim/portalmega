﻿using EZ.EZControl.Domain.EZPag.Enums;
using EZ.EZControl.Tests.Application.EZPag.Geral.Mocks;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZPag.Geral
{
    public class TransacaoAppService_Tests : TransacaoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public void TraduzirStatusPagSeguro()
        {
            var status = StatusDoPagamentoEnum.Iniciado;

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.Initiated);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.Iniciado);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.WaitingPayment);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.AguardandoPagamento);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.InAnalysis);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.EmAnalise);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.Paid);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.Pago);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.Available);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.Disponivel);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.InDispute);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.EmDisputa);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.Refunded);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.Reembolsado);

            status = TransacaoService.TraduzirStatusPagSeguro(Uol.PagSeguro.Enums.TransactionStatus.Cancelled);
            status.ShouldBeOneOf(StatusDoPagamentoEnum.Cancelado);
        }
    }
}
