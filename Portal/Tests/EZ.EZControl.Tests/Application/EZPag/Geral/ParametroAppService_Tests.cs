﻿using EZ.EZControl.Dto.EZPag.Geral.Parametro;
using EZ.EZControl.Tests.Application.EZPag.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZPag.Geral
{
    public class ParametroAppService_Tests: ParametroAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Parametro()
        {
            await CreateParametrosAsync();
        }

        private async Task CreateParametrosAsync()
        {
            base.CreateTestParametros();

            await ParametroEzpagAppService.Save(new ParametroEzpagInput
            {
                AlturaMaximaPx = 100,
                ExtensoesDocumento = "doc;pdf",
                ExtensoesImagem = "png;jpeg",
                LarguraMaximaPx = 260,
                IsTest = true,
                TamanhoMaximoMb = 1
            });

            await UsingDbContext(async context =>
            {
                var createdParametro = await context.ParametroEzpag.FirstOrDefaultAsync();
                createdParametro.ShouldNotBe(null);
            });
        }
    }
}
