﻿using EZ.EZControl.Domain.Servicos.Geral;
using EZ.EZControl.Services.Servicos.Interfaces;

namespace EZ.EZControl.Tests.Application.Servicos.Geral.Mocks
{
    public abstract class ServicoAppServiceTestBase : AppTestBase
    {
        protected readonly IServicoAppService ServicoAppService;

        public ServicoAppServiceTestBase()
        {
            ServicoAppService = Resolve<IServicoAppService>();
        }

        protected void CreateTestServicos()
        {
            UsingDbContext(context =>
            {
                context.Servicos.Add(CreateServicoEntity("Cricação de sites", 5500.69M));
                context.Servicos.Add(CreateServicoEntity("Cricação de Loja Virtual", 15100.64M));
            });
        }

        private Servico CreateServicoEntity(string nome, decimal valor)
        {
            return new Servico()
            {
                Nome = nome,
                Valor = valor,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}