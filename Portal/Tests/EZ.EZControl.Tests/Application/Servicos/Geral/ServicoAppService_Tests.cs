﻿using System.Data.Entity;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Servicos.Geral.Servicos;
using EZ.EZControl.Tests.Application.Servicos.Geral.Mocks;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Application.Servicos.Geral
{
    public class ServicoAppService_Tests : ServicoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Servico()
        {
            await CreateServicoAndTestAsync("Cricação de sites", 5500.69M);
            await CreateServicoAndTestAsync("Cricação de Loja Virtual", 15100.64M);
        }

        [Fact]
        public async Task Should_Not_Create_Servico_With_Duplicate_Nome()
        {
            CreateTestServicos();
            await ServicoAppService.Save(
                new ServicoInput()
                {
                    Valor = 5500.69M,
                    Nome = "Cricação de sites"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        private async Task CreateServicoAndTestAsync(string nome, decimal valor)
        {
            await ServicoAppService.Save(
                new ServicoInput()
                {
                    Nome = nome,
                    Valor = valor
                });
            await UsingDbContext(async context =>
            {
                var createdServico = await context.Servicos.FirstOrDefaultAsync(b => b.Nome == nome);
                createdServico.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Delete_Servico()
        {
            base.CreateTestServicos();
            await base.ServicoAppService.Delete(new IdInput(1));
            await base.ServicoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

    }
}