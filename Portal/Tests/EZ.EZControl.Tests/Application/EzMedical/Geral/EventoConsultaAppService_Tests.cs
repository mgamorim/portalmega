﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZMedical.Geral.Evento;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral
{
    public class EventoConsultaAppService_Tests : EventoConsultaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateEventoAndTestAsync(string titulo, string descricao, DateTime inicio, DateTime termino,
            SistemaEnum sistema, TipoDeEventoEnum tipoDeEvento, StatusDoEventoEnum status, Disponibilidade disponibilidade, PessoaJuridica owner)
        {
            await base.EventoConsultaAppService.Save(new EventoConsultaInput
            {
                DiaInteiro = true,
                Titulo = titulo,
                Descricao = descricao,
                Inicio = inicio,
                Termino = termino,
                OwnerId = owner.Id,
                DisponibilidadeId = disponibilidade.Id,
                Sistema = sistema,
                TipoDeEvento = tipoDeEvento,
                StatusDoEvento = status,
                Participantes = new PessoaListDto[]
                {
                    new PessoaJuridicaListDto
                    {
                        NomeFantasia = "Fulano Software",
                        RazaoSocial = "Fulano Software SA"
                    }
                }
            });

            await UsingDbContext(async context =>
            {
                var createdEvento = await context.EventosConsulta.FirstOrDefaultAsync();
                createdEvento.ShouldNotBe(null);
            });
        }

        private async Task<Disponibilidade> CreateDisponibilidadeAndTestAsync()
        {
            var disponibilidade = base.CreateDisponibilidade();
            await UsingDbContext(async context =>
            {
                var createdDispo = await context.Disponibilidades.FirstOrDefaultAsync(t => t.Id == disponibilidade.Id);
                createdDispo.ShouldNotBe(null);
            });

            return disponibilidade;
        }

        private async Task<PessoaJuridica> CreateOwnerAndTestAsync()
        {
            var pessoaJuridica = base.CreatePessoaJuridica("Teste", "Teste SA");
            await UsingDbContext(async context =>
            {
                var createdOwner = await context.PessoasJuridicas.FirstOrDefaultAsync(t => t.Id == pessoaJuridica.Id);
                createdOwner.ShouldNotBe(null);
            });
            return pessoaJuridica;
        }

        [Fact]
        public async Task Should_Create_Evento()
        {
            var owner = await CreateOwnerAndTestAsync();
            var disponibilidade = await CreateDisponibilidadeAndTestAsync();
            await CreateEventoAndTestAsync("Teste de Evento", "Evento de Teste", new DateTime(2016, 10, 10, 09, 30, 00), new DateTime(2016, 10, 15, 18, 30, 00), SistemaEnum.CMS, TipoDeEventoEnum.Tarefa, StatusDoEventoEnum.Agendado, disponibilidade, owner);
        }
    }
}