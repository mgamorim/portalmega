﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Dto.EZMedical.Geral.Regra;
using EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZMedical.Regras
{
    public class RegraConsultaAppService_Tests : RegraConsultaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_RegraConsulta()
        {
            #region Arrange

            int medicoId = 0;
            Especialidade especialidade = null;

            await UsingDbContextAsync(async context =>
            {
                especialidade = await CreateEspecialidade(context);
                medicoId = await CreateMedico(context, especialidade);
            });

            await RegraConsultaAppService.Save(new RegraConsultaInput
            {
                IsActive = true,
                Sistema = SistemaEnum.Global,
                DiasAntesDoAgendamentoProibidoAlterar = 10,
                DiasExibidosDesdeHojeAteAgendamento = 90,
                DiasParaNovoAgendamentoPorParticipante = 15,
                OverBook = false,
                UnidadeDeTempo = 30,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                VagasDisponibilizadasPorDia = 14,
                VagasPorUnidadeDeTempo = 3,
                VagasReservadasPorDia = 12,
                MedicoId = medicoId,
                EspecialidadeId = especialidade.Id
            });

            #endregion Arrange

            await UsingDbContext(async context =>
            {
                #region Action

                var created = await context.RegraConsultas.FirstOrDefaultAsync();

                #endregion Action

                #region Action

                created.ShouldNotBe(null);

                #endregion Action
            });
        }

        [Fact]
        public async Task Should_Update_RegraConsulta()
        {
            #region Arrange

            var regra = await CreateTestRegraConsulta();
            var regraBase = await base.RegraConsultaAppService.GetById(new IdInput(regra.Id));

            #endregion Arrange

            #region Action

            regraBase.UnidadeDeTempo = 12;
            await base.RegraConsultaAppService.Save(regraBase);

            #endregion Action

            #region Assert

            var regraInput = await base.RegraConsultaAppService.GetById(new IdInput(regra.Id));
            regraInput.UnidadeDeTempo.ShouldBe(12);

            #endregion Assert
        }

        [Fact]
        public async Task Should_Delete_RegraConsulta()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                #region Arrange

                var regra = await CreateTestRegraConsulta();

                #endregion Arrange

                #region Action

                await base.RegraConsultaAppService.Delete(new IdInput(regra.Id));

                await uow.Current.SaveChangesAsync();

                #endregion Action

                #region Assert

                await base.RegraConsultaAppService.GetById(new IdInput(regra.Id)).ShouldThrowAsync<UserFriendlyException>();

                #endregion Assert
            });
        }
    }
}