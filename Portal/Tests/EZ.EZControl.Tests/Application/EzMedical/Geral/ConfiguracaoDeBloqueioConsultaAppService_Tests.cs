﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeBloqueioConsulta;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral
{
    public class ConfiguracaoDeBloqueioConsultaAppService_Tests : ConfiguracaoDeBloqueioConsultaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        protected async Task<Medico> CreateMedico(EZControlDbContext context, Especialidade especialidade)
        {
            var grupoPessoa = await CreateGrupoPessoa(context, "Médicos");
            var picture = await CreatePicture(context);

            var pf = new PessoaFisica("Doutor Pimpolho", grupoPessoa);

            Medico medico = await context.Medicos.Where(x => x.PessoaFisica.Nome == "Doutor Pimpolho").FirstOrDefaultAsync();

            if (medico == null)
            {
                medico = new Medico
                {
                    IsActive = true,
                    Especialidades = new[] { especialidade },
                    Picture = picture,
                    PessoaFisica = pf
                };

                context.Medicos.Add(medico);
                await context.SaveChangesAsync();
            }

            return medico;
        }

        protected async Task<Especialidade> CreateEspecialidade(EZControlDbContext context)
        {
            Especialidade especialidade = null;

            especialidade = context.Especialidades.Where(x => x.Nome == "Urologia").FirstOrDefault();
            if (especialidade == null)
            {
                especialidade = new Especialidade()
                {
                    Codigo = "001",
                    Nome = "Urologia",
                    EmpresaId = EmpresaIdDefaultTest
                };

                context.Especialidades.Add(especialidade);
                await context.SaveChangesAsync();
            }

            return especialidade;
        }

        protected async Task<GrupoPessoa> CreateGrupoPessoa(EZControlDbContext context, string nome)
        {
            var grupoPessoa = await context.GrupoPessoas.Where(x => x.Nome == nome).FirstOrDefaultAsync();

            if (grupoPessoa == null)
            {
                grupoPessoa = new GrupoPessoa();
                grupoPessoa.IsActive = true;
                grupoPessoa.Nome = nome;
                grupoPessoa.Descricao = nome;

                context.GrupoPessoas.Add(grupoPessoa);
                await context.SaveChangesAsync();
            }

            return grupoPessoa;
        }

        protected async Task<Picture> CreatePicture(EZControlDbContext context)
        {
            Picture picture = null;

            picture = context.Pictures.FirstOrDefault();
            if (picture == null)
            {
                picture = new Picture();
                picture.EmpresaId = EmpresaIdDefaultTest;
                picture.Bytes = new byte[0];
                context.Pictures.Add(picture);
                await context.SaveChangesAsync();
            }

            return picture;
        }

        private async Task CreateConfiguracaoDeBloqueioAndTestAsync()
        {
            await ConfiguracaoDeBloqueioConsultaAppService.Save(new ConfiguracaoDeBloqueioConsultaInput
            {
                IsActive = true,
                FimBloqueio = new DateTime(2016, 6, 30),
                HorarioFim = "08:00",
                HorarioInicio = "17:00",
                InicioBloqueio = new DateTime(2016, 1, 1),
                MotivoDoBloqueio = MotivoDoBloqueioEnum.Ferias,
                Sistema = SistemaEnum.Agenda,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                Titulo = "Bloqueio de Férias",
                EspecialidadeId = 1,
                MedicoId = 1
            });
            await UsingDbContext(async context =>
            {
                var createdConfiguracaoDeBloqueio = await context.ConfiguracoesDeBloqueio.FirstOrDefaultAsync();
                createdConfiguracaoDeBloqueio.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_ConfiguracaoDeBloqueio()
        {
            try
            {
                await UsingDbContextAsync(async context =>
                {
                    await CreateRegra(context);
                    await CreateConfiguracaoDeBloqueioAndTestAsync();
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async Task<RegraConsulta> CreateRegra(EZControlDbContext context)
        {
            var especialidade = await CreateEspecialidade(context);
            var medico = await CreateMedico(context, especialidade);

            RegraConsulta regra = null;

            regra = new RegraConsulta()
            {
                DiasAntesDoAgendamentoProibidoAlterar = 2,
                DiasExibidosDesdeHojeAteAgendamento = 60,
                DiasParaNovoAgendamentoPorParticipante = 15,
                IsActive = true,
                OverBook = true,
                Sistema = SistemaEnum.Agenda,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                UnidadeDeTempo = 30,
                VagasDisponibilizadasPorDia = 30,
                VagasPorUnidadeDeTempo = 1,
                VagasReservadasPorDia = 5,
                Medico = medico,
                Especialidade = especialidade,
                EmpresaId = EmpresaIdDefaultTest
            };

            context.RegraConsultas.Add(regra);
            await context.SaveChangesAsync();
            return regra;
        }

        [Fact]
        public async Task Delete_ConfiguracaoDeBloqueio()
        {
            var configuracao = await base.CreateTestConfiguracaoDeBloqueioConsultas();
            await base.ConfiguracaoDeBloqueioConsultaAppService.Delete(new IdInput(configuracao.Id));
            await base.ConfiguracaoDeBloqueioConsultaAppService.GetById(new IdInput(configuracao.Id)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}