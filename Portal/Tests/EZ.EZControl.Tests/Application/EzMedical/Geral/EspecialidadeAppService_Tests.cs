﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.EZMedical.Geral.Especialidade;
using EZ.EZControl.Tests.Application.EzMedical.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EzMedical.Geral
{
    public class EspecialidadeAppService_Tests : EspecialidadeAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateEspecialidadeAndTestAsync(string codigo, string nome)
        {
            await base.EspecialidadeAppService.Save(new EspecialidadeInput
            {
                IsActive = true,
                Codigo = codigo,
                Nome = nome
            });
        }

        [Fact]
        public async Task Should_Create_Especialidade()
        {
            base.CreateTestEspecialidades();
            await CreateEspecialidadeAndTestAsync("225135", "Dermatologista");
            await CreateEspecialidadeAndTestAsync("225139", "Epidemiologista");
            await CreateEspecialidadeAndTestAsync("225155", "Endocrinologista");
        }

        [Fact]
        public async Task Should_Not_Create_Especialidade_With_Duplicate_Codigo()
        {
            base.CreateTestEspecialidades();
            await base.EspecialidadeAppService.Save(
                new EspecialidadeInput
                {
                    IsActive = true,
                    Codigo = "2251",
                    Nome = "Clínico Geral"
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Especialidade()
        {
            base.CreateTestEspecialidades();
            await base.EspecialidadeAppService.Delete(new IdInput(1));
            await base.EspecialidadeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}
