﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZMedical.Geral.ConfiguracaoDeDisponibilidadeConsulta;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral
{
    public class ConfiguracaoDeDisponibilidadeConsultaAppService_Tests : ConfiguracaoDeDisponibilidadeConsultaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        protected async Task<Picture> CreatePicture(EZControlDbContext context)
        {
            Picture picture = null;

            picture = context.Pictures.FirstOrDefault();
            if (picture == null)
            {
                picture = new Picture();
                picture.EmpresaId = EmpresaIdDefaultTest;
                picture.Bytes = new byte[0];
                context.Pictures.Add(picture);
                await context.SaveChangesAsync();
            }

            return picture;
        }

        protected async Task<GrupoPessoa> CreateGrupoPessoa(EZControlDbContext context, string nome)
        {
            var grupoPessoa = await context.GrupoPessoas.Where(x => x.Nome == nome).FirstOrDefaultAsync();

            if (grupoPessoa == null)
            {
                grupoPessoa = new GrupoPessoa();
                grupoPessoa.IsActive = true;
                grupoPessoa.Nome = nome;
                grupoPessoa.Descricao = nome;

                context.GrupoPessoas.Add(grupoPessoa);
                await context.SaveChangesAsync();
            }

            return grupoPessoa;
        }

        protected async Task<Medico> CreateMedico(EZControlDbContext context, Especialidade especialidade)
        {
            var grupoPessoa = await CreateGrupoPessoa(context, "Médicos");
            var picture = await CreatePicture(context);

            var pf = new PessoaFisica("Doutor Pimpolho", grupoPessoa);

            Medico medico = await context.Medicos.Where(x => x.PessoaFisica.Nome == "Doutor Pimpolho").FirstOrDefaultAsync();

            if (medico == null)
            {
                medico = new Medico
                {
                    IsActive = true,
                    Especialidades = new[] { especialidade },
                    Picture = picture,
                    PessoaFisica = pf
                };

                context.Medicos.Add(medico);
                await context.SaveChangesAsync();
            }

            return medico;
        }

        protected async Task<Especialidade> CreateEspecialidade(EZControlDbContext context)
        {
            Especialidade especialidade = null;

            especialidade = context.Especialidades.Where(x => x.Nome == "Urologia").FirstOrDefault();
            if (especialidade == null)
            {
                especialidade = new Especialidade()
                {
                    Codigo = "001",
                    Nome = "Urologia",
                    EmpresaId = EmpresaIdDefaultTest
                };

                context.Especialidades.Add(especialidade);
                await context.SaveChangesAsync();
            }

            return especialidade;
        }

        private async Task CreateConfiguracaoDeDisponibilidadeAndTestAsync(EZControlDbContext context)
        {
            var especialidade = await CreateEspecialidade(context);
            var medico = await CreateMedico(context, especialidade);

            await DisponibilidadeConsultaAppService.Save(new ConfiguracaoDeDisponibilidadeConsultaInput
            {
                IsActive = true,
                DataFimValidade = new System.DateTime(2016, 1, 31),
                DataInicioValidade = new System.DateTime(2016, 1, 1),
                HorarioFim = new System.TimeSpan(10, 0, 0),
                HorarioInicio = new System.TimeSpan(9, 30, 0),
                NumeroDeDias = 30,
                Sistema = SistemaEnum.EZMedical,
                TipoDeDisponibilidade = TipoDeDisponibilidadeEnum.DiasCorridos,
                TipoDeEvento = TipoDeEventoEnum.Consulta,
                Titulo = "Consultas de Janeiro",
                EspecialidadeId = especialidade.Id,
                MedicoId = medico.Id
            });

            var createdDisponibilidade = await context.ConfiguracoesDeDisponibilidadeConsulta.FirstOrDefaultAsync();
            createdDisponibilidade.ShouldNotBe(null);
        }

        [Fact]
        public async Task Should_Create_ConfiguracaoDeDisponibilidade()
        {
            await UsingDbContextAsync(async context =>
            {
                await CreateRegraConsultaEntity(context);
                await CreateConfiguracaoDeDisponibilidadeAndTestAsync(context);
            });
        }

        private async Task<RegraConsulta> CreateRegraConsultaEntity(EZControlDbContext context)
        {
            var especialidade = await CreateEspecialidade(context);
            var medico = await CreateMedico(context, especialidade);

            RegraConsulta regra = null;

            regra = new RegraConsulta()
            {
                IsActive = true,
                Sistema = SistemaEnum.EZMedical,
                DiasAntesDoAgendamentoProibidoAlterar = 10,
                DiasExibidosDesdeHojeAteAgendamento = 90,
                DiasParaNovoAgendamentoPorParticipante = 15,
                OverBook = false,
                UnidadeDeTempo = 30,
                TipoDeEvento = TipoDeEventoEnum.Consulta,
                VagasDisponibilizadasPorDia = 14,
                VagasPorUnidadeDeTempo = 3,
                VagasReservadasPorDia = 12,
                Medico = medico,
                Especialidade = especialidade,
                EmpresaId = EmpresaIdDefaultTest
            };

            context.RegraConsultas.Add(regra);
            await context.SaveChangesAsync();
            return regra;
        }

        [Fact]
        public async Task Delete_ConfiguracaoDeBloqueio()
        {
            var configuracao = await base.CreateTestDisponibilidades();
            await DisponibilidadeConsultaAppService.Delete(new IdInput(configuracao.Id));
            await DisponibilidadeConsultaAppService.GetById(new IdInput(configuracao.Id)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}