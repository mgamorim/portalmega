﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces.RegraConsulta;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks
{
    public class ConfiguracaoDeBloqueioConsultaAppServiceTestBase : AppTestBase
    {
        protected readonly IConfiguracaoDeBloqueioConsultaAppService ConfiguracaoDeBloqueioConsultaAppService;
        protected readonly IRegraConsultaAppService RegraConsultaAppService;
        protected readonly IPessoaService PessoaService;

        public ConfiguracaoDeBloqueioConsultaAppServiceTestBase()
        {
            ConfiguracaoDeBloqueioConsultaAppService = Resolve<IConfiguracaoDeBloqueioConsultaAppService>();
            RegraConsultaAppService = Resolve<IRegraConsultaAppService>();
            PessoaService = Resolve<IPessoaService>();
        }

        protected async Task<ConfiguracaoDeBloqueioConsulta> CreateTestConfiguracaoDeBloqueioConsultas()
        {
            ConfiguracaoDeBloqueioConsulta configuracao = null;

            await UsingDbContextAsync(async context =>
            {
                configuracao = await CreateConfiguracaoDeBloqueioConsulta(context);
                context.ConfiguracoesDeBloqueio.Add(configuracao);
                context.SaveChanges();
            });

            return configuracao;
        }

        protected async Task<Picture> CreatePicture(EZControlDbContext context)
        {
            Picture picture = null;

            picture = context.Pictures.FirstOrDefault();
            if (picture == null)
            {
                picture = new Picture();
                picture.EmpresaId = EmpresaIdDefaultTest;
                picture.Bytes = new byte[0];
                context.Pictures.Add(picture);
                await context.SaveChangesAsync();
            }

            return picture;
        }

        protected async Task<GrupoPessoa> CreateGrupoPessoa(EZControlDbContext context, string nome)
        {
            var grupoPessoa = await context.GrupoPessoas.Where(x => x.Nome == nome).FirstOrDefaultAsync();

            if (grupoPessoa == null)
            {
                grupoPessoa = new GrupoPessoa();
                grupoPessoa.IsActive = true;
                grupoPessoa.Nome = nome;
                grupoPessoa.Descricao = nome;

                context.GrupoPessoas.Add(grupoPessoa);
                await context.SaveChangesAsync();
            }

            return grupoPessoa;
        }

        protected async Task<Medico> CreateMedico(EZControlDbContext context, Especialidade especialidade)
        {
            var grupoPessoa = await CreateGrupoPessoa(context, "Médicos");
            var picture = await CreatePicture(context);

            var pf = new PessoaFisica("Doutor Pimpolho", grupoPessoa);

            Medico medico = await context.Medicos.Where(x => x.PessoaFisica.Nome == "Doutor Pimpolho").FirstOrDefaultAsync();

            if (medico == null)
            {
                medico = new Medico
                {
                    IsActive = true,
                    Especialidades = new[] { especialidade },
                    Picture = picture,
                    PessoaFisica = pf
                };

                context.Medicos.Add(medico);
                await context.SaveChangesAsync();
            }

            return medico;
        }

        protected async Task<Especialidade> CreateEspecialidade(EZControlDbContext context)
        {
            Especialidade especialidade = null;

            especialidade = context.Especialidades.Where(x => x.Nome == "Urologia").FirstOrDefault();
            if (especialidade == null)
            {
                especialidade = new Especialidade()
                {
                    Codigo = "001",
                    Nome = "Urologia",
                    EmpresaId = EmpresaIdDefaultTest
                };

                context.Especialidades.Add(especialidade);
                await context.SaveChangesAsync();
            }

            return especialidade;
        }

        private async Task<ConfiguracaoDeBloqueioConsulta> CreateConfiguracaoDeBloqueioConsulta(EZControlDbContext context)
        {
            var especialidade = await CreateEspecialidade(context);
            var medico = await CreateMedico(context, especialidade);

            var configuracao = new ConfiguracaoDeBloqueioConsulta
            {
                IsActive = true,
                FimBloqueio = new DateTime(2016, 6, 30),
                HorarioFim = new TimeSpan(18, 0, 0),
                HorarioInicio = new TimeSpan(9, 0, 0),
                InicioBloqueio = new DateTime(2016, 1, 1),
                MotivoDoBloqueio = MotivoDoBloqueioEnum.Ferias,
                Sistema = SistemaEnum.Agenda,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                Titulo = "Bloqueio de Férias",
                Medico = medico,
                Especialidade = especialidade,
                EmpresaId = EmpresaIdDefaultTest
            };

            context.ConfiguracoesDeBloqueioConsulta.Add(configuracao);
            await context.SaveChangesAsync();
            return configuracao;
        }
    }
}