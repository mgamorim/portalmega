﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Services.EZMedical.Medico.Interfaces;

namespace EZ.EZControl.Tests.Application.EzMedical.Geral.Mocks
{
    public class MedicoAppServiceTestBase : AppTestBase
    {
        public readonly IMedicoAppService MedicoAppService;
        public readonly IRepository<Medico> MedicoRepository;

        public MedicoAppServiceTestBase()
        {
            MedicoAppService = Resolve<IMedicoAppService>();
            MedicoRepository = Resolve<IRepository<Medico>>();
        }
    }
}
