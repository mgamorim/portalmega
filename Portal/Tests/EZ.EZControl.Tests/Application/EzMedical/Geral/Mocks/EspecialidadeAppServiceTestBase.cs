﻿using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Services.EZMedical.Especialidade.Interfaces;

namespace EZ.EZControl.Tests.Application.EzMedical.Geral.Mocks
{
    public class EspecialidadeAppServiceTestBase : AppTestBase
    {
        protected readonly IEspecialidadeAppService EspecialidadeAppService;

        public EspecialidadeAppServiceTestBase()
        {
            this.EspecialidadeAppService = Resolve<IEspecialidadeAppService>();
        }

        protected void CreateTestEspecialidades()
        {
            UsingDbContext(context =>
            {
                context.Especialidades.Add(CreateEspecialidade("2251", "Médicos Clínicos"));
                context.Especialidades.Add(CreateEspecialidade("225133", "Médico Psicanalista"));
            });
        }

        private Especialidade CreateEspecialidade(string codigo, string nome)
        {
            return new Especialidade
            {
                IsActive = true,
                Codigo = codigo,
                Nome = nome,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
