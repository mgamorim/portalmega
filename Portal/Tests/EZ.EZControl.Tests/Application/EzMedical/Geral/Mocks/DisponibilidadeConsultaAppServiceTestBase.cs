﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Services.EZMedical.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces.RegraConsulta;
using EZ.EZControl.Services.EZMedical.Medico;
using EZ.EZControl.Services.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks
{
    public class ConfiguracaoDeDisponibilidadeConsultaAppServiceTestBase : AppTestBase
    {
        protected readonly IConfiguracaoDeDisponibilidadeConsultaAppService DisponibilidadeConsultaAppService;
        protected readonly IPessoaService PessoaService;
        protected readonly IRegraConsultaAppService RegraConsultaAppService;
        protected readonly GrupoPessoaAppService GrupoPessoaAppService;
        protected readonly MedicoAppService MedicoAppService;

        public ConfiguracaoDeDisponibilidadeConsultaAppServiceTestBase()
        {
            DisponibilidadeConsultaAppService = Resolve<IConfiguracaoDeDisponibilidadeConsultaAppService>();
            PessoaService = Resolve<IPessoaService>();
            RegraConsultaAppService = Resolve<IRegraConsultaAppService>();
            GrupoPessoaAppService = Resolve<GrupoPessoaAppService>();
            MedicoAppService = Resolve<MedicoAppService>();
        }

        protected async Task<ConfiguracaoDeDisponibilidadeConsulta> CreateTestDisponibilidades()
        {
            ConfiguracaoDeDisponibilidadeConsulta configuracao = null;

            await UsingDbContextAsync(async context =>
            {
                configuracao = await CreateConfiguracaoDeDisponibilidade(context);
                var disponibilidade = CreateDisponibilidade(configuracao, DateTime.Now, DateTime.Now.AddHours(5).TimeOfDay);
                context.Disponibilidades.Add(disponibilidade);
            });

            return configuracao;
        }

        protected async Task<Picture> CreatePicture(EZControlDbContext context)
        {
            Picture picture = null;

            picture = context.Pictures.FirstOrDefault();
            if (picture == null)
            {
                picture = new Picture();
                picture.EmpresaId = EmpresaIdDefaultTest;
                picture.Bytes = new byte[0];
                context.Pictures.Add(picture);
                await context.SaveChangesAsync();
            }

            return picture;
        }

        protected async Task<GrupoPessoa> CreateGrupoPessoa(EZControlDbContext context, string nome)
        {
            var grupoPessoa = await context.GrupoPessoas.Where(x => x.Nome == nome).FirstOrDefaultAsync();

            if (grupoPessoa == null)
            {
                grupoPessoa = new GrupoPessoa();
                grupoPessoa.IsActive = true;
                grupoPessoa.Nome = nome;
                grupoPessoa.Descricao = nome;

                context.GrupoPessoas.Add(grupoPessoa);
                await context.SaveChangesAsync();
            }

            return grupoPessoa;
        }

        protected async Task<Medico> CreateMedico(EZControlDbContext context, Especialidade especialidade)
        {
            var grupoPessoa = await CreateGrupoPessoa(context, "Médicos");
            var picture = await CreatePicture(context);

            var pf = new PessoaFisica("Doutor Pimpolho", grupoPessoa);

            Medico medico = await context.Medicos.Where(x => x.PessoaFisica.Nome == "Doutor Pimpolho").FirstOrDefaultAsync();

            if (medico == null)
            {
                medico = new Medico
                {
                    IsActive = true,
                    Especialidades = new[] { especialidade },
                    Picture = picture,
                    PessoaFisica = pf
                };

                context.Medicos.Add(medico);
                await context.SaveChangesAsync();
            }

            return medico;
        }

        protected async Task<Especialidade> CreateEspecialidade(EZControlDbContext context)
        {
            Especialidade especialidade = null;

            especialidade = context.Especialidades.Where(x => x.Nome == "Urologia").FirstOrDefault();
            if (especialidade == null)
            {
                especialidade = new Especialidade()
                {
                    Codigo = "001",
                    Nome = "Urologia",
                    EmpresaId = EmpresaIdDefaultTest
                };

                context.Especialidades.Add(especialidade);
                await context.SaveChangesAsync();
            }

            return especialidade;
        }

        private Disponibilidade CreateDisponibilidade(ConfiguracaoDeDisponibilidadeConsulta configuracaoDeDisponibilidade, DateTime data, TimeSpan horario)
        {
            return new Disponibilidade
            {
                IsActive = true,
                Data = data,
                Horario = horario,
                ConfiguracaoDeDisponibilidade = configuracaoDeDisponibilidade,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private async Task<ConfiguracaoDeDisponibilidadeConsulta> CreateConfiguracaoDeDisponibilidade(EZControlDbContext context)
        {
            var especialidade = await CreateEspecialidade(context);
            var medico = await CreateMedico(context, especialidade);

            var configuracao = new ConfiguracaoDeDisponibilidadeConsulta() {
                Titulo = "Consultas",
                DataInicioValidade = new DateTime(2016, 1, 1),
                DataFimValidade = new DateTime(2016, 1, 31),
                Especialidade = especialidade,
                HorarioFim = new TimeSpan(10, 0, 0),
                HorarioInicio = new TimeSpan(10, 30, 0),
                Medico = medico,
                Sistema = SistemaEnum.EZMedical,
                TipoDeDisponibilidade = TipoDeDisponibilidadeEnum.DiasCorridos,
                NumeroDeDias = 30,
                TipoDeEvento = TipoDeEventoEnum.Consulta,
                EmpresaId = EmpresaIdDefaultTest
            };

            context.ConfiguracoesDeDisponibilidadeConsulta.Add(configuracao);
            await context.SaveChangesAsync();
            return configuracao;
        }
    }
}