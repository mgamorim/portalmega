﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Agenda.Interfaces;
using EZ.EZControl.Services.EZMedical.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks
{
    public class EventoConsultaAppServiceTestBase : AppTestBase
    {
        protected readonly IEventoConsultaAppService EventoConsultaAppService;
        protected readonly IConfiguracaoDeDisponibilidadeAppService ConfiguracaoDeDisponibilidadeAppService;
        public EventoConsultaAppServiceTestBase()
        {
            this.EventoConsultaAppService = Resolve<IEventoConsultaAppService>();
            this.ConfiguracaoDeDisponibilidadeAppService = Resolve<IConfiguracaoDeDisponibilidadeAppService>();
        }

        protected void CreateTestEventos()
        {
            UsingDbContext(context =>
            {
                context.EventosConsulta.Add(CreateEventoConsulta("Consulta Anual", "Consulta Anual", new DateTime(2016, 10, 10, 09, 30, 00), new DateTime(2016, 10, 15, 18, 30, 00), CreatePessoaJuridica("ArkaGroot", "ArkaGroot Soluções"), SistemaEnum.Global, TipoDeEventoEnum.Reuniao, StatusDoEventoEnum.Agendado));
            });
        }

        private EventoConsulta CreateEventoConsulta(string titulo, string descricao, DateTime inicio, DateTime termino, Pessoa owner, SistemaEnum sistema, TipoDeEventoEnum tipoDeEventoConsulta, StatusDoEventoEnum status)
        {
            CreateConfiguracaoDeDisponibilidade();
            return new EventoConsulta
            {
                DiaInteiro = true,
                Titulo = titulo,
                Descricao = descricao,
                Inicio = inicio,
                Termino = termino,
                Owner = owner,
                Sistema = sistema,
                TipoDeEvento = tipoDeEventoConsulta,
                Participantes = CreateParticipantes(),
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ICollection<Pessoa> CreateParticipantes()
        {
            return new List<Pessoa>
            {
                CreatePessoaJuridica("EZSoft", "EZSoft Ltda"),
                CreatePessoaJuridica("Empresa Exemplo", "Empresa Exemplo Ltda")
            };
        }

        protected void CreatePessoa(Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                if (pessoa is PessoaFisica)
                    context.PessoasFisicas.Add((PessoaFisica)pessoa);
                else if (pessoa is PessoaJuridica)
                    context.PessoasJuridicas.Add((PessoaJuridica)pessoa);

                context.SaveChanges();
            });
        }

        protected PessoaJuridica CreatePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            var pessoaJuridica = new PessoaJuridica
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial
            };

            CreatePessoa(pessoaJuridica);

            return pessoaJuridica;
        }

        private void CreateRegra()
        {
            UsingDbContext(context =>
            {
                context.RegraBases.Add(new RegraBase
                {
                    Sistema = SistemaEnum.Global,
                    TipoDeEvento = TipoDeEventoEnum.Tarefa,
                    UnidadeDeTempo = 30,
                    DiasExibidosDesdeHojeAteAgendamento = 1,
                    VagasDisponibilizadasPorDia = 1,
                    VagasPorUnidadeDeTempo = 1,
                    EmpresaId = EmpresaIdDefaultTest
                });

                context.SaveChanges();
            });
        }

        protected void CreateConfiguracaoDeDisponibilidade()
        {
            CreateRegra();

            UsingDbContext(context =>
            {
                context.ConfiguracoesDeDisponibilidades.Add(new ConfiguracaoDeDisponibilidade
                {
                    IsActive = true,
                    Titulo = "Configuração de Teste",
                    Sistema = SistemaEnum.Global,
                    TipoDeEvento = TipoDeEventoEnum.Tarefa,
                    TipoDeDisponibilidade = TipoDeDisponibilidadeEnum.DiasCorridos,
                    NumeroDeDias = 5,
                    HorarioInicio = new TimeSpan(9, 30, 00),
                    HorarioFim = new TimeSpan(18, 30, 00),
                    DataInicioValidade = new DateTime(2016, 10, 10, 09, 30, 00),
                    DataFimValidade = new DateTime(2016, 10, 15, 18, 30, 00),
                    EmpresaId = EmpresaIdDefaultTest
                });

                context.SaveChanges();
            });
        }

        protected Disponibilidade CreateDisponibilidade()
        {
            CreateConfiguracaoDeDisponibilidade();
            Disponibilidade disponilibidade = null;

            UsingDbContext(context =>
            {
                disponilibidade = new Disponibilidade()
                {
                    IsActive = true,
                    ConfiguracaoDeDisponibilidade = context.ConfiguracoesDeDisponibilidades.FirstOrDefault(t => t.Id == 1),
                    Data = new DateTime(2016, 10, 10, 09, 30, 00),
                    Horario = new TimeSpan(9, 30, 00),
                    EmpresaId = EmpresaIdDefaultTest
                };

                context.Disponibilidades.Add(disponilibidade);

                context.SaveChanges();
            });

            return disponilibidade;
        }
    }
}