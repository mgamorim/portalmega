﻿using EZ.EZControl.Domain.Agenda.Enums;
using EZ.EZControl.Domain.Agenda.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Core.Geral;
using EZ.EZControl.Domain.EZMedical.Geral;
using EZ.EZControl.Domain.EZMedical.SubtiposPessoa;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Services.EZMedical.Interfaces.RegraConsulta;
using EZ.EZControl.Services.EZMedical.Medico.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EZMedical.Geral.Mocks
{
    public abstract class RegraConsultaAppServiceTestBase : AppTestBase
    {
        protected readonly IRegraConsultaAppService RegraConsultaAppService;
        protected readonly IMedicoAppService MedicoAppService;
        protected readonly IGrupoPessoaAppService GrupoPessoaAppServce;

        protected RegraConsultaAppServiceTestBase()
        {
            RegraConsultaAppService = Resolve<IRegraConsultaAppService>();
            MedicoAppService = Resolve<IMedicoAppService>();
            GrupoPessoaAppServce = Resolve<IGrupoPessoaAppService>();
        }

        protected async Task<RegraConsulta> CreateTestRegraConsulta()
        {
            RegraConsulta regra = null;

            await UsingDbContext(async context =>
            {
                regra = await CreateRegraConsultaEntity(context);
                context.RegraConsultas.Add(regra);
                await context.SaveChangesAsync();
            });

            return regra;
        }

        protected async Task<GrupoPessoa> CreateGrupoPessoa(EZControlDbContext context, string nome)
        {
            var grupoPessoa = await context.GrupoPessoas.Where(x => x.Nome == nome).FirstOrDefaultAsync();

            if (grupoPessoa == null)
            {
                grupoPessoa = new GrupoPessoa();
                grupoPessoa.IsActive = true;
                grupoPessoa.Nome = nome;
                grupoPessoa.Descricao = nome;

                context.GrupoPessoas.Add(grupoPessoa);
                await context.SaveChangesAsync();
            }

            return grupoPessoa;
        }

        protected async Task<Picture> CreatePicture(EZControlDbContext context)
        {
            Picture picture = null;

            picture = context.Pictures.FirstOrDefault();
            if (picture == null)
            {
                picture = new Picture();
                picture.EmpresaId = EmpresaIdDefaultTest;
                picture.Bytes = new byte[0];
                context.Pictures.Add(picture);
                await context.SaveChangesAsync();
            }

            return picture;
        }

        protected async Task<int> CreateMedico(EZControlDbContext context, Especialidade especialidade)
        {
            var grupoPessoa = await CreateGrupoPessoa(context, "Médicos");
            var picture = await CreatePicture(context);

            var pf = new PessoaFisica("Doutor Pimpolho", grupoPessoa);

            Medico medico = await context.Medicos.Where(x => x.PessoaFisica.Nome == "Doutor Pimpolho").FirstOrDefaultAsync();

            if (medico == null)
            {
                medico = new Medico
                {
                    IsActive = true,
                    Especialidades = new[] { especialidade },
                    Picture = picture,
                    PessoaFisica = pf
                };

                context.Medicos.Add(medico);
                await context.SaveChangesAsync();
            }

            return medico.Id;
        }

        protected async Task<Especialidade> CreateEspecialidade(EZControlDbContext context)
        {
            Especialidade especialidade = null;

            especialidade = context.Especialidades.Where(x => x.Nome == "Urologia").FirstOrDefault();
            if (especialidade == null)
            {
                especialidade = new Especialidade()
                {
                    Codigo = "001",
                    Nome = "Urologia",
                    EmpresaId = EmpresaIdDefaultTest
                };

                context.Especialidades.Add(especialidade);
                await context.SaveChangesAsync();
            }

            return especialidade;
        }

        private async Task<RegraConsulta> CreateRegraConsultaEntity(EZControlDbContext context)
        {
            var especialidade = await CreateEspecialidade(context);
            var medicoId = await CreateMedico(context, especialidade);

            Medico medico = null;

            RegraConsulta regra = null;

            medico = context.Medicos.Where(x => x.Id == medicoId).FirstOrDefault();
            regra = new RegraConsulta()
            {
                IsActive = true,
                Sistema = SistemaEnum.Global,
                DiasAntesDoAgendamentoProibidoAlterar = 10,
                DiasExibidosDesdeHojeAteAgendamento = 90,
                DiasParaNovoAgendamentoPorParticipante = 15,
                OverBook = false,
                UnidadeDeTempo = 30,
                TipoDeEvento = TipoDeEventoEnum.Tarefa,
                VagasDisponibilizadasPorDia = 14,
                VagasPorUnidadeDeTempo = 3,
                VagasReservadasPorDia = 12,
                Medico = medico,
                Especialidade = especialidade,
                EmpresaId = EmpresaIdDefaultTest
            };

            return regra;
        }
    }
}