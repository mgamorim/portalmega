﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Core.Geral.Picture;
using EZ.EZControl.Dto.EZMedical.SubtipoPessoa.Medico;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.Core.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Tests.Application.EzMedical.Geral.Mocks;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.EzMedical.Geral
{
    public class MedicoAppService_Tests : MedicoAppServiceTestBase
    {
        private readonly IRepository<EZControl.Domain.Global.Pessoa.Pessoa> pessoaRepository;
        private readonly IGrupoPessoaAppService grupoPessoaAppService;
        private readonly IPictureAppService pictureAppService;
        private readonly IAppFolders appFolders;

        public MedicoAppService_Tests()
        {
            this.grupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
            this.pessoaRepository = Resolve<IRepository<EZControl.Domain.Global.Pessoa.Pessoa>>();
            this.pictureAppService = Resolve<IPictureAppService>();
            this.appFolders = Resolve<IAppFolders>();
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async void CreateGrupoPessoa()
        {
            var grupo = new GrupoPessoaInput
            {
                IsActive = true,
                Descricao = "Médicos"
            };

            await grupoPessoaAppService.SaveAndReturnEntity(grupo);
        }

        private async void CreatePicture()
        {
            var teste = appFolders.TempFileDownloadFolder;
            var picture = new PictureInput()
            {
                IsActive = true,
                FileName = "teste.jpg",
                X = 10,
                Y = 10,
                Width = 15,
                Height = 20
            };

            await pictureAppService.InsertOrUpdatePicture(picture);
        }

        private async Task<MedicoPessoaIdDto> CreateMedico(string nome)
        {
            var pf = new PessoaFisicaInput
            {
                Nome = nome,
                GrupoPessoaId = 1,
                TipoPessoa = TipoPessoaEnum.Fisica,
                IsActive = true
            };

            MedicoInput medico = new MedicoInput
            {
                Pessoa = pf,
                PessoaFisica = pf,
                PictureId = 1

            };

            return await MedicoAppService.Save(medico);
        }

        //[Fact]
        //public async Task ShouldCreateMedico()
        //{
        //    await RunAsyncInTransaction(async (uow) =>
        //    {
        //        this.CreatePicture();
        //        this.CreateGrupoPessoa();
        //        await CreateMedico("Roberto Cajado");
        //    });
        //}
    }
}
