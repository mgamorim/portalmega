﻿using Abp.Application.Services.Dto;
using EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro;
using EZ.EZControl.Tests.Application.Global.Integration.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Integration
{
    /// <summary>
    /// Classe de Teste criada para Testar as propriedades de integração
    /// E o update na propriedade DateOfEditionIntegration das entidades
    /// Exemplo foi feito na classe de Tipo de Logradouro
    /// </summary>
    public class IntegrationGlobalAppService_Tests : IntegrationGlobalAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task<TipoDeLogradouroInput> CreateAndReturnTipoDeLogradouroAndTestAsync(string descricao, int idExterno = 0)
        {
            return await base.TipoDeLogradouroAppService.SaveAndReturnEntity(new TipoDeLogradouroInput
            {
                IsActive = true,
                Descricao = descricao,
                ExternalId = idExterno
            });
        }

        [Fact]
        public async Task Should_Create_And_Return_TipoDeLogradouro_With_Integration()
        {
            //Arrange
            base.CreateTestTiposDeLogradouros();

            //Action
            var dto = await CreateAndReturnTipoDeLogradouroAndTestAsync("Rua", 104);

            //Assert
            dto.ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Create_And_Return_TipoDeLogradouro_Without_Integration()
        {
            //Arrange
            base.CreateTestTiposDeLogradouros();

            //Action
            var dto = await CreateAndReturnTipoDeLogradouroAndTestAsync("Vila");

            //Assert
            dto.ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Update_TipoDeLogradouro_With_Integration()
        {
            //Arrange
            await CreateAndReturnTipoDeLogradouroAndTestAsync("Rua", 104);

            //Action
            var entity = await TipoDeLogradouroAppService.GetByExternalId(new IdInput(104));
            entity.Descricao = "Alameda";
            var dto = await base.TipoDeLogradouroAppService.SaveAndReturnEntity(entity);

            //Assert
            dto.Descricao.ShouldNotBe("Rua");
        }

        [Fact]
        public async Task Should_Create_And_Get_TipoDeLogradouro_ByExternalId()
        {
            //Arrange
            await CreateAndReturnTipoDeLogradouroAndTestAsync("Avendia", 18);

            //Action
            var dto = await TipoDeLogradouroAppService.GetByExternalId(new IdInput(18));

            //Assert
            dto.ShouldNotBeNull();
        }
    }
}
