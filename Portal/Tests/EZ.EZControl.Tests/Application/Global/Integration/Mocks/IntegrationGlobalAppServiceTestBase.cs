﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Integration.Mocks
{
    public class IntegrationGlobalAppServiceTestBase : AppTestBase
    {
        protected readonly ITipoDeLogradouroAppService TipoDeLogradouroAppService;
        protected readonly ITipoDeLogradouroService TipoDeLogradouroService;

        public IntegrationGlobalAppServiceTestBase()
        {
            this.TipoDeLogradouroAppService = Resolve<ITipoDeLogradouroAppService>();
        }

        protected void CreateTestTiposDeLogradouros()
        {
            UsingDbContext(context =>
            {
                context.TiposDeLogradouros.Add(CreateTipoDeLogradouro("Avenida", 11));
                context.TiposDeLogradouros.Add(CreateTipoDeLogradouro("Loteamento"));
            });
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(string descricao, int idExterno = 0)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao,
                ExternalId = idExterno
            };
        }
    }
}
