﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Localidade.Cidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Global.Localidade.Pais;
using EZ.EZControl.Tests.Application.Global.Localidade.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Localidade
{
    public class CidadeAppService_Tests : CidadeAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateCidadeAndTestAsync(Pais pais, Estado estado, String nome, String codigoIBGE)
        {
            var _pais = await base.PaisAppService.GetPaisByCodigoISO(
                new PaisInput
                {
                    IsActive = true,
                    Nome = pais.Nome,
                    Nacionalidade = pais.Nacionalidade,
                    CodigoISO = pais.CodigoISO
                });

            var _estado = await base.EstadoAppService.GetEstadoBySigla(
                new EstadoInput
                {
                    IsActive = true,
                    Nome = estado.Nome,
                    Capital = estado.Capital,
                    Sigla = estado.Sigla,
                    PaisId = _pais.Id
                });

            await base.CidadeAppService.Save(
            new CidadeInput
            {
                IsActive = true,
                Nome = nome,
                CodigoIBGE = codigoIBGE,
                EstadoId = _estado.Id
            });
        }

        [Fact]
        public async Task Should_Create_Cidade()
        {
            base.CreateTestCidades();
            var _pais = new Pais
            {
                Nome = "Brasil",
                CodigoISO = "BR",
                Nacionalidade = "Brasileiro"
            };

            var _estado = new Estado
            {
                IsActive = true,
                Nome = "Rio de Janeiro",
                Capital = "Rio de Janeiro",
                Sigla = "RJ"
            };

            await CreateCidadeAndTestAsync(_pais, _estado, "Itaperuna", "3302205");
            await CreateCidadeAndTestAsync(_pais, _estado, "Arraial do Cabo", "3300258");
        }

        [Fact]
        public async Task Should_Not_Create_Cidade_With_Duplicate_Sigla()
        {
            base.CreateTestCidades();
            var _estado = await base.EstadoAppService.GetEstadoBySigla(new EstadoInput
            {
                IsActive = true,
                Nome = "Minas Gerais",
                Capital = "Belo Horizonte",
                Sigla = "MG",
                CodigoIBGE = "31"
            });

            await base.CidadeAppService.Save(new CidadeInput
            {
                IsActive = true,
                Nome = "Muriaé",
                EstadoId = _estado.Id,
                CodigoIBGE = "3143906"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Cidade()
        {
            base.CreateTestCidades();
            await base.CidadeAppService.Delete(new IdInput(1));
            await base.CidadeAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}