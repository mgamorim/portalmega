﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Localidade.Estado;
using EZ.EZControl.Dto.Global.Localidade.Pais;
using EZ.EZControl.Tests.Application.Global.Localidade.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Localidade
{
    public class EstadoAppService_Tests : EstadoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateEstadoAndTestAsync(Pais pais, String nome, String capital, String sigla, String codigoIBGE)
        {
            var _pais = await base.PaisAppService.GetPaisByCodigoISO(
                new PaisInput
                {
                    Nome = pais.Nome,
                    Nacionalidade = pais.Nacionalidade,
                    CodigoISO = pais.CodigoISO
                });

            await base.EstadoAppService.Save(
                new EstadoInput
                {
                    IsActive = true,
                    Nome = nome,
                    Capital = capital,
                    Sigla = sigla,
                    CodigoIBGE = codigoIBGE,
                    PaisId = _pais.Id
                });
        }

        [Fact]
        public async Task Should_Create_Estado()
        {
            base.CreateTestEstados();

            await CreateEstadoAndTestAsync(new Pais
            {
                Nome = "Brasil",
                CodigoISO = "BR",
                Nacionalidade = "Brasileiro"
            }, "Bahia", "Salvador", "BA", "29");

            await CreateEstadoAndTestAsync(new Pais
            {
                Nome = "Brasil",
                CodigoISO = "BR",
                Nacionalidade = "Brasileiro"
            }, "Espiríto Santo", "Vitória", "ES", "32");
        }

        [Fact]
        public async Task Should_Not_Create_Estado_With_Duplicate_Sigla()
        {
            base.CreateTestEstados();
            var _pais = await base.PaisAppService.GetPaisByCodigoISO(new PaisInput
            {
                Nome = "Brasil",
                Nacionalidade = "Brasileiro",
                CodigoISO = "BR"
            });

            await base.EstadoAppService.Save(new EstadoInput
            {
                IsActive = true,
                Capital = "Rio de Janeiro",
                Nome = "Rio de Janeiro",
                PaisId = _pais.Id,
                Sigla = "RJ",
                CodigoIBGE = "33"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Estado()
        {
            base.CreateTestEstados();
            await base.EstadoAppService.Delete(new IdInput(1));
            await base.EstadoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}