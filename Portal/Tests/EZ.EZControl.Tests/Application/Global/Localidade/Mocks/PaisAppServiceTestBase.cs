﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Localidade.Mocks
{
    public class PaisAppServiceTestBase : AppTestBase
    {
        protected readonly IPaisAppService PaisAppService;

        public PaisAppServiceTestBase()
        {
            this.PaisAppService = Resolve<IPaisAppService>();
        }

        protected void CreateTestPaises()
        {
            UsingDbContext(context =>
            {
                context.Paises.Add(CreatePais("Brasil", "Brasileiro", "BR"));
                context.Paises.Add(CreatePais("Argentina", "Argentino", "AR"));
            });
        }

        private Pais CreatePais(String nome, String nacionalidade, String codigoIso)
        {
            return new Pais
            {
                IsActive = true,
                Nome = nome,
                Nacionalidade = nacionalidade,
                CodigoISO = codigoIso
            };
        }
    }
}