﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Localidade.Mocks
{
    public class TipoDeLogradouroAppServiceTestBase : AppTestBase
    {
        protected readonly ITipoDeLogradouroAppService TipoDeLogradouroAppService;

        public TipoDeLogradouroAppServiceTestBase()
        {
            this.TipoDeLogradouroAppService = Resolve<ITipoDeLogradouroAppService>();
        }

        protected void CreateTestTiposDeLogradouros()
        {
            UsingDbContext(context =>
            {
                context.TiposDeLogradouros.Add(CreateTipoDeLogradouro("Vila"));
                context.TiposDeLogradouros.Add(CreateTipoDeLogradouro("Avenida"));
                context.TiposDeLogradouros.Add(CreateTipoDeLogradouro("Rua"));
                context.TiposDeLogradouros.Add(CreateTipoDeLogradouro("Loteamento"));
            });
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(String descricao)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao
            };
        }
    }
}