﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Data.Entity.Migrations;

namespace EZ.EZControl.Tests.Application.Global.Localidade.Mocks
{
    public class EnderecoAppServiceTestBase : AppTestBase
    {
        protected readonly IContatoAppService ContatoAppService;
        protected readonly IContatoService ContatoService;
        protected readonly ITipoDeLogradouroAppService TipoDeLogradouroAppService;
        protected readonly IEnderecoAppService EnderecoAppService;

        public EnderecoAppServiceTestBase()
        {
            this.EnderecoAppService = Resolve<IEnderecoAppService>();
            this.TipoDeLogradouroAppService = Resolve<ITipoDeLogradouroAppService>();
            this.ContatoAppService = Resolve<IContatoAppService>();
            this.ContatoService = Resolve<IContatoService>();
        }

        protected void CreateTestEndereco()
        {
            UsingDbContext(context =>
            {
                var _tipoDeContato = CreateTipoDeContato("Comercial");
                var _pessoa = CreatePessoaJuridica("AGF7 Tecnologia", "AGF");
                var _tipoDeLogradouro = CreateTipoDeLogradouro("Avenida");
                var _contato = CreateContato(_pessoa, _tipoDeContato, "João Fernandez", "Analista", "Desenvolvimento");
                var _pais = CreatePais("Brasil", "Brasileiro", "BR");
                var _estado = CreateEstado(_pais, "Rio de Janeiro", "Rio de Janeiro", "RJ");
                var _cidade = CreateCidade(_estado, "Rio de Janeiro", "3304904");

                context.Enderecos.Add(CreateEndereco(_pessoa, _tipoDeLogradouro, _contato, _cidade,
                    TipoEnderecoEnum.Comercial,
                    "Endereço para Entrega", "Av. Embaixador Abelardo Bueno", "1023", "Barra da Tijuca",
                    "Polo de Cine e Vídeo", "Prédio Comercial", "22775040", true));
            });
        }

        protected void CreateTestEnderecoDuplicate(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                var _tipoDeContato = CreateTipoDeContato("Comercial");
                var _pessoa = pessoa;
                var _tipoDeLogradouro = CreateTipoDeLogradouro("Avenida");
                var _contato = CreateContato(_pessoa.PessoaFisica, _tipoDeContato, "João", "Analista", "Desenvolvimento");
                var _pais = CreatePais("Brasil", "Brasileira", "BR");
                var _estado = CreateEstado(_pais, "Rio de Janeiro", "Rio de Janeiro", "RJ");
                var _cidade = CreateCidade(_estado, "Rio de Janeiro", "3304904");

                context.Enderecos.Add(CreateEndereco(_pessoa.PessoaFisica, _tipoDeLogradouro, _contato, _cidade,
                    TipoEnderecoEnum.Comercial,
                    "Endereço Residencial", "Aladim", "88", "Vila Valqueire",
                    "Academia Lopes", "bl 3 apt 607", "21330100", false));
            });
        }

        protected void CreatePessoa(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                if (pessoa is PessoaFisica)
                    context.PessoasFisicas.Add(pessoa as PessoaFisica);
                else if (pessoa is PessoaJuridica)
                    context.PessoasJuridicas.Add(pessoa as PessoaJuridica);

                context.SaveChanges();
            });
        }

        protected void CreateTipoDeContato(TipoDeContato tipoDeContato)
        {
            UsingDbContext(context =>
            {
                context.TipoDeContatos.AddOrUpdate(tipoDeContato);
                context.SaveChanges();
            });
        }

        protected void CreateContato(Contato contato)
        {
            UsingDbContext(context =>
            {
                context.Contatos.AddOrUpdate(contato);
                context.SaveChanges();
            });
        }

        protected void CreateTipoDeLogradouro(TipoDeLogradouro tipoDeLogradouro)
        {
            UsingDbContext(context =>
            {
                context.TiposDeLogradouros.AddOrUpdate(tipoDeLogradouro);
                context.SaveChanges();
            });
        }

        protected void CreatePais(Pais pais)
        {

            UsingDbContext(context =>
            {
                context.Paises.Add(pais);
                context.SaveChanges();
            });
        }

        protected void CreateEstado(Estado estado)
        {
            UsingDbContext(context =>
            {
                context.Estados.Add(estado);
                context.SaveChanges();
            });
        }

        protected void CreateCidade(Cidade cidade)
        {
            UsingDbContext(context =>
            {
                context.Cidades.Add(cidade);
                context.SaveChanges();
            });
        }

        protected void CreateCidade(Pais pais, Estado estado, Cidade cidade)
        {
            UsingDbContext(context =>
            {
                var paisCreated = context.Paises.Add(pais);
                estado.Pais = paisCreated;

                var estadoCreated = context.Estados.Add(estado);
                cidade.Estado = estadoCreated;

                context.Cidades.Add(cidade);
                context.SaveChanges();
            });
        }

        private Endereco CreateEndereco(PessoaFisica pessoaFisica, TipoDeLogradouro tipoDeLogradouro, Contato contato, Cidade cidade, TipoEnderecoEnum tipoDeEndereco,
                                       string descricao, string logradouro, string numero, string bairro, string referencia, string complemento, string cep, Boolean enderecoParaEntrega)
        {
            var endereco =
                new Endereco(pessoaFisica, descricao, tipoDeEndereco, tipoDeLogradouro, logradouro, numero,
                    complemento, bairro, cep, cidade, referencia)
                {
                    Contato = contato,
                    EmpresaId = EmpresaIdDefaultTest
                };
            return endereco;
        }

        private Endereco CreateEndereco(PessoaJuridica pessoaJuridica, TipoDeLogradouro tipoDeLogradouro, Contato contato, Cidade cidade, TipoEnderecoEnum tipoDeEndereco,
                                        string descricao, string logradouro, string numero, string bairro, string referencia, string complemento, string cep, Boolean enderecoParaEntrega)
        {
            var endereco =
                new Endereco(pessoaJuridica, descricao, tipoDeEndereco, tipoDeLogradouro, logradouro, numero,
                    complemento, bairro, cep, cidade, referencia)
                {
                    Contato = contato,
                    EmpresaId = EmpresaIdDefaultTest
                };
            return endereco;
        }

        private PessoaJuridica CreatePessoaJuridica(string nomeFantasia, string razaoSocial)
        {
            return new PessoaJuridica
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial
            };
        }

        private TipoDeContato CreateTipoDeContato(string descricao)
        {
            return new TipoDeContato
            {
                IsActive = true,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Contato CreateContato(PessoaJuridica pessoaJuridica, TipoDeContato tipoDeContato, string nome, string cargo, string setor)
        {
            var tratamento = new Tratamento
            {
                Descricao = "Sr.",
                EmpresaId = EmpresaIdDefaultTest
            };
            return new Contato(pessoaJuridica, nome, tipoDeContato, tratamento, SexoEnum.NaoInformado, null, cargo,
                setor, null, null, ContatoService)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Contato CreateContato(PessoaFisica pessoaFisica, TipoDeContato tipoDeContato, string nome, string cargo, string setor)
        {
            var tratamento = new Tratamento
            {
                Descricao = "Sr.",
                EmpresaId = EmpresaIdDefaultTest
            };
            return new Contato(pessoaFisica, nome, tipoDeContato, tratamento, SexoEnum.Masculino, null, cargo,
                setor, null, null, ContatoService)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        public Pais CreatePais(string nome, string nacionalidade, string codigoIso)
        {
            return new Pais
            {
                IsActive = true,
                Nome = nome,
                Nacionalidade = nacionalidade,
                CodigoISO = codigoIso,
                TenantId = 1
            };
        }

        public Estado CreateEstado(Pais pais, string nome, string capital, string sigla)
        {
            return new Estado
            {
                IsActive = true,
                Nome = nome,
                Capital = capital,
                Sigla = sigla,
                Pais = pais,
                TenantId = 1
            };
        }

        public Cidade CreateCidade(Estado estado, string nome, string codigoIBGE)
        {
            return new Cidade
            {
                IsActive = true,
                Nome = nome,
                CodigoIBGE = codigoIBGE,
                Estado = estado,
                TenantId = 1
            };
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(string descricao)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao
            };
        }
    }
}