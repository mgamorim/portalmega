﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Localidade.Mocks
{
    public class CidadeAppServiceTestBase : AppTestBase
    {
        public readonly IPaisAppService PaisAppService;
        public readonly IEstadoAppService EstadoAppService;
        public readonly ICidadeAppService CidadeAppService;

        public CidadeAppServiceTestBase()
        {
            this.PaisAppService = Resolve<IPaisAppService>();
            this.EstadoAppService = Resolve<IEstadoAppService>();
            this.CidadeAppService = Resolve<ICidadeAppService>();
        }

        protected void CreateTestCidades()
        {
            UsingDbContext(context =>
            {
                var pais = context.Paises.Add(CreatePais("Brasil", "Brasileiro", "BR"));
                var estadoRJ = context.Estados.Add(CreateEstado(pais, "Rio de Janeiro", "Rio de Janeiro", "RJ", "33"));
                var estadoMG = context.Estados.Add(CreateEstado(pais, "Minas Gerais", "Belo Horizonte", "MG", "31"));
                context.Cidades.Add(CreateCidade(estadoRJ, "Rio de Janeiro", "3304904"));
                context.Cidades.Add(CreateCidade(estadoRJ, "Duque de Caxias", "3301702"));
                context.Cidades.Add(CreateCidade(estadoRJ, "Maricá", "3302700"));
                context.Cidades.Add(CreateCidade(estadoMG, "Muriaé", "3143906"));
            });
        }

        private Cidade CreateCidade(Estado estado, string nome, string codigoIBGE)
        {
            return new Cidade
            {
                IsActive = true,
                Nome = nome,
                CodigoIBGE = codigoIBGE,
                Estado = estado
            };
        }

        private Estado CreateEstado(Pais pais, string nome, string capital, string sigla, string codigoIBGE)
        {
            return new Estado
            {
                IsActive = true,
                Nome = nome,
                Capital = capital,
                Sigla = sigla,
                Pais = pais,
                CodigoIBGE = codigoIBGE
            };
        }

        private Pais CreatePais(String nome, String nacionalidade, String codigoIso)
        {
            return new Pais
            {
                IsActive = true,
                Nome = nome,
                Nacionalidade = nacionalidade,
                CodigoISO = codigoIso
            };
        }
    }
}