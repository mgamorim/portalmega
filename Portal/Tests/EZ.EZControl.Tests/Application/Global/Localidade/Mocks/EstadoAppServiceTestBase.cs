﻿using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Localidade.Mocks
{
    public class EstadoAppServiceTestBase : AppTestBase
    {
        protected readonly IPaisAppService PaisAppService;
        protected readonly IEstadoAppService EstadoAppService;

        public EstadoAppServiceTestBase()
        {
            this.PaisAppService = Resolve<IPaisAppService>();
            this.EstadoAppService = Resolve<IEstadoAppService>();
        }

        protected void CreateTestEstados()
        {
            UsingDbContext(context =>
            {
                var pais = context.Paises.Add(CreatePais("Brasil", "Brasileiro", "BR"));
                context.Estados.Add(CreateEstado(pais, "Rio de Janeiro", "Rio de Janeiro", "RJ"));
                context.Estados.Add(CreateEstado(pais, "São Paulo", "São Paulo", "SP"));
                context.Estados.Add(CreateEstado(pais, "Minas Gerais", "Belo Horizonte", "MG"));
                context.Estados.Add(CreateEstado(pais, "Paraíba", "João Pessoa", "PB"));
                context.SaveChanges();
            });
        }

        private Estado CreateEstado(Pais pais, String nome, String capital, String sigla)
        {
            return new Estado
            {
                IsActive = true,
                Nome = nome,
                Capital = capital,
                Sigla = sigla,
                Pais = pais
            };
        }

        private Pais CreatePais(String nome, String nacionalidade, String codigoIso)
        {
            return new Pais
            {
                IsActive = true,
                Nome = nome,
                Nacionalidade = nacionalidade,
                CodigoISO = codigoIso
            };
        }
    }
}