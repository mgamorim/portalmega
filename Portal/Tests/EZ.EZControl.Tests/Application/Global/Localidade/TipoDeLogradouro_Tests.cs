﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Localidade.TipoDeLogradouro;
using EZ.EZControl.Tests.Application.Global.Localidade.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Localidade
{
    public class TipoDeLogradouro_Tests : TipoDeLogradouroAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateTipoDeLogradouroAndTestAsync(String descricao)
        {
            await base.TipoDeLogradouroAppService.Save(new TipoDeLogradouroInput
            {
                IsActive = true,
                Descricao = descricao
            });
        }

        [Fact]
        public async Task Should_Create_TipoDeLogradouro()
        {
            base.CreateTestTiposDeLogradouros();
            await CreateTipoDeLogradouroAndTestAsync("Via");
            await CreateTipoDeLogradouroAndTestAsync("Aeroporto");
            await CreateTipoDeLogradouroAndTestAsync("Viela");
        }

        [Fact]
        public async Task Should_Not_Create_TipoDeLogradouro_With_Duplicate_Descricao()
        {
            base.CreateTestTiposDeLogradouros();
            await base.TipoDeLogradouroAppService.Save(
                new TipoDeLogradouroInput
                {
                    Descricao = "Avenida"
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_TipoDeLogradouro()
        {
            base.CreateTestTiposDeLogradouros();
            await base.TipoDeLogradouroAppService.Delete(new IdInput(1));
            await base.TipoDeLogradouroAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}