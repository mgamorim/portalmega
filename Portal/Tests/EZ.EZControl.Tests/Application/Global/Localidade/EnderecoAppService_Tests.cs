﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Localidade.Endereco;
using EZ.EZControl.Tests.Application.Global.Localidade.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Localidade
{
    public class EnderecoAppService_Tests : EnderecoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateEnderecoAndTestAsync(string descricao)
        {
            await base.EnderecoAppService.Save(CreateEnderecoInput(descricao));
        }

        [Fact]
        public async Task Should_Create_Endereco()
        {
            await CreateEnderecoAndTestAsync("Endereço de Correspondência");
        }

        [Fact]
        public async Task Should_Get_Endereco_By_Cep()
        {
            var endereco = await base.EnderecoAppService.GetEnderecoByCep(new GetEnderecoByCepInput
            {
                Cep = "21760030"
            });

            endereco.ShouldNotBeNull();
        }

        [Fact]
        public async Task Should_Not_Create_Endereco_With_Duplicate_Logradouro_Numero_Complemento_Cep()
        {
            var _pessoa = CreatePessoaFisica();
            CreateTestEnderecoDuplicate(_pessoa);

            //var pessoa = this.CreatePessoaFisica();
            var tipoDeContato = this.CreateTipoDeContato();
            //var contato = this.CreateContato(_pessoa, tipoDeContato);
            var tipoDeLogradouro = this.CreateTipoDeLogradouro();

            await EnderecoAppService.Save(new EnderecoInput
            {
                IsActive = true,
                PessoaId = _pessoa.Id,
                //ContatoId = contato.Id,
                TipoDeLogradouroId = tipoDeLogradouro.Id,
                TipoEndereco = TipoEnderecoEnum.Residencial,
                Descricao = "Endereço Residencial",
                Logradouro = "Aladim",
                Numero = "88",
                Referencia = "Academia Lopes",
                Bairro = "Vila Valqueire",
                Complemento = "bl 3 apt 607",
                EnderecoParaEntrega = false,
                CEP = "21330100"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Endereco()
        {
            base.CreateTestEndereco();
            await base.EnderecoAppService.Delete(new IdInput(1));
            await base.EnderecoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

        private PessoaFisica CreatePessoaFisica()
        {
            PessoaFisica pessoaFisica = new PessoaFisica { Nome = "Marcos" };
            base.CreatePessoa(pessoaFisica);

            return pessoaFisica;
        }

        private TipoDeContato CreateTipoDeContato()
        {
            TipoDeContato tipoDeContato = new TipoDeContato
            {
                Descricao = "Pessoal",
                EmpresaId = EmpresaIdDefaultTest
            };
            base.CreateTipoDeContato(tipoDeContato);

            return tipoDeContato;
        }

        private Pais CreatePais()
        {
            var pais = base.CreatePais("Argentina", "Argentino", "AR");
            base.CreatePais(pais);

            return pais;
        }

        private Estado CreateEstado(Pais pais)
        {
            var estado = base.CreateEstado(pais, "Buenos Aires", "Buenos Aires", "BA");

            base.CreateEstado(estado);

            return estado;
        }

        private Cidade CreateCidade()
        {
            var pais = CreatePais("Argentina", "Argentino", "AR");
            var estado = CreateEstado(pais, "Buenos Aires", "La Plata", "RJ");
            var cidade = CreateCidade(estado, "La Plata", "3304904");

            base.CreateCidade(pais, estado, cidade);

            return cidade;
        }

        private TipoDeLogradouro CreateTipoDeLogradouro()
        {
            TipoDeLogradouro tipoDeLogradouro = new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = "Avenida"
            };
            base.CreateTipoDeLogradouro(tipoDeLogradouro);

            return tipoDeLogradouro;
        }


        private Contato CreateContato(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeContato tipoDeContato)
        {
            var tratamento = new Tratamento
            {
                Descricao = "Sr.",
                EmpresaId = EmpresaIdDefaultTest
            };
            Contato contato = new Contato(pessoa, tipoDeContato, tratamento, ContatoService)
            {
                Nome = "Ana Rita",
                EmpresaId = EmpresaIdDefaultTest
            };
            contato.AssociarTipoDeContato(tipoDeContato, ContatoService);
            base.CreateContato(contato);

            return contato;
        }

        private EnderecoInput CreateEnderecoInput(string descricao)
        {
            var pessoa = this.CreatePessoaFisica();
            var tipoDeContato = this.CreateTipoDeContato();
            var contato = this.CreateContato(pessoa, tipoDeContato);
            var tipoDeLogradouro = this.CreateTipoDeLogradouro();
            var cidade = this.CreateCidade();

            return new EnderecoInput
            {
                IsActive = true,
                PessoaId = pessoa.Id,
                ContatoId = contato.Id,
                CidadeId = cidade.Id,
                TipoDeLogradouroId = tipoDeLogradouro.Id,
                TipoEndereco = TipoEnderecoEnum.Residencial,
                Descricao = descricao,
                Logradouro = "Av. Vicente de Carvalho",
                Numero = "6524",
                Referencia = "Próximo ao Metrô de Vicente de Carvalho",
                Bairro = "Vicente de Carvalho",
                Complemento = "Casa 2",
                EnderecoParaEntrega = false,
                CEP = "21633000"
            };
        }
    }
}