﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Localidade.Pais;
using EZ.EZControl.Tests.Application.Global.Localidade.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Localidade
{
    public class PaisAppService_Tests : PaisAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreatePaisAndTestAsync(string nome, string codigoIso, string nacionalidade)
        {
            await base.PaisAppService.Save(new PaisInput
            {
                IsActive = true,
                Nome = nome,
                CodigoISO = codigoIso,
                Nacionalidade = nacionalidade
            });
        }

        [Fact]
        public async Task Should_Create_Pais()
        {
            base.CreateTestPaises();
            await CreatePaisAndTestAsync("Itália", "IT", "Italiano");
            await CreatePaisAndTestAsync("França", "FR", "Francês");
        }

        [Fact]
        public async Task Should_Not_Create_Pais_With_Duplicate_CodigoISO()
        {
            base.CreateTestPaises();
            await base.PaisAppService.Save(
                new PaisInput
                {
                    Nome = "Brasil",
                    CodigoISO = "BR",
                    Nacionalidade = "Brasileiro"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Agencia()
        {
            base.CreateTestPaises();
            await base.PaisAppService.Delete(new IdInput(1));
            await base.PaisAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}