﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.EnderecoEletronico;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class EnderecoEletronicoAppService_Tests : EnderecoEletronicoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateEnderecoEletronicoAndTestAsync(EZControl.Domain.Global.Pessoa.Pessoa pessoa, int contatoId, string endereco, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico)
        {
            await base.EnderecoEletronicoAppService.Save(new EnderecoEletronicoInput
            {
                IsActive = true,
                Endereco = endereco,
                TipoDeEnderecoEletronico = tipoDeEnderecoEletronico,
                PessoaId = pessoa.Id,
                ContatoId = contatoId
            });
        }

        [Fact]
        public async Task Should_Create_EnderecoEletronico()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };

            this.CreatePessoa(pessoaFisica);
            this.CreateContato(pessoaFisica);

            base.CreateTestEnderecoEletronico(pessoaFisica);
            await CreateEnderecoEletronicoAndTestAsync(pessoaFisica, 1, "abcdefg@outlook.com", TipoDeEnderecoEletronicoEnum.Twiter);
            await CreateEnderecoEletronicoAndTestAsync(pessoaFisica, 2, "abc@msn.com", TipoDeEnderecoEletronicoEnum.MSN);
        }

        [Fact]
        public async Task Should_Not_Create_EnderecoEletronico_With_Duplicate_Endereco()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };

            this.CreatePessoa(pessoaFisica);
            this.CreateContato(pessoaFisica);

            base.CreateTestEnderecoEletronico(pessoaFisica);
            await base.EnderecoEletronicoAppService.Save(
                new EnderecoEletronicoInput
                {
                    Endereco = "teste@gmail.com",
                    TipoDeEnderecoEletronico = TipoDeEnderecoEletronicoEnum.EmailPrincipal
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_EnderecoEletronico()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };

            this.CreatePessoa(pessoaFisica);
            this.CreateContato(pessoaFisica);

            base.CreateTestEnderecoEletronico(pessoaFisica);
            await base.EnderecoEletronicoAppService.Delete(new IdInput(1));
            await base.EnderecoEletronicoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}