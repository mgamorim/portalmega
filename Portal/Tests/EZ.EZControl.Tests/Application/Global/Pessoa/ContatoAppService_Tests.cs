﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Contato;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class ContatoAppService_Tests : ContatoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateContatoAndTestAsync(string nome)
        {
            await base.ContatoAppService.Save(this.CreateContatoInput(nome));
        }

        [Fact]
        public async Task Should_Create_Contato()
        {
            var pessoaJuridica = CreatePessoaJuridica("AGF7 Tecnologia", "AGF");
            this.CreateTratamento();
            base.CreateTestContato(pessoaJuridica);
            await CreateContatoAndTestAsync("Aloísio Freitas");
            await CreateContatoAndTestAsync("Reginaldo Farias de Lima");
        }

        [Fact]
        public async Task Should_Not_Create_Contato_With_Duplicate_Nome()
        {
            var pessoaJuridica = CreatePessoaJuridica("João Fernandez", "JF");

            base.CreateTestContato(pessoaJuridica);
            await base.ContatoAppService.Save(this.CreateContatoInput("João Fernandez")).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Contato()
        {
            var pessoaJuridica = CreatePessoaJuridica("João Fernandez", "JF");

            base.CreateTestContato(pessoaJuridica);
            await base.ContatoAppService.Delete(new IdInput(1));
            await base.ContatoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

        private TipoDeContato CreateTipoDeContato()
        {
            var tipoDeContato = new TipoDeContato
            {
                IsActive = true,
                Descricao = "Comercial",
                EmpresaId = EmpresaIdDefaultTest
            };

            base.CreateTipoDeContato(tipoDeContato);

            return tipoDeContato;
        }

        private ContatoInput CreateContatoInput(string nome)
        {
            var pessoaJuridica = CreatePessoaJuridica("João Fernandez", "JF");
            var tipoDeContato = CreateTipoDeContato();

            return new ContatoInput
            {
                IsActive = true,
                Nome = nome,
                PessoaId = pessoaJuridica.Id,
                TipoDeContatoId = tipoDeContato.Id,
                TratamentoId = 1
            };
        }
    }
}