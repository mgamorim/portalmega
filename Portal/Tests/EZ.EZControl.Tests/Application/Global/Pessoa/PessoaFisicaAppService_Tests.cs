﻿using Abp.Configuration;
using Abp.Domain.Repositories;
using EZ.EZControl.Configuration.Host;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class PessoaFisicaAppService_Tests : PessoaFisicaAppServiceTestBase
    {
        private readonly IRepository<Documento> _documentoRepository;
        private readonly IRepository<TipoDeDocumento> _tipoDeDocumentoRepository;
        private readonly IRepository<PessoaFisica> _pessoaRepository;
        private readonly IRepository<EnderecoEletronico> _enderecoEletronicoRepository;
        private readonly IRepository<Telefone> _telefoneRepository;
        private readonly IRepository<Endereco> _enderecoRepository;
        private readonly IRepository<Profissao> _profissaoRepository;
        private readonly IRepository<Estado> _estadoRepository;
        private readonly IHostSettingsAppService _hostSettingsAppService;
        private readonly ISettingManager _settingManager;

        public PessoaFisicaAppService_Tests()
        {
            _documentoRepository = Resolve<IRepository<Documento>>();
            _tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
            _pessoaRepository = Resolve<IRepository<PessoaFisica>>();
            _enderecoEletronicoRepository = Resolve<IRepository<EnderecoEletronico>>();
            _telefoneRepository = Resolve<IRepository<Telefone>>();
            _enderecoRepository = Resolve<IRepository<Endereco>>();
            _profissaoRepository = Resolve<IRepository<Profissao>>();
            _estadoRepository = Resolve<IRepository<Estado>>();
            _hostSettingsAppService = Resolve<IHostSettingsAppService>();
            _settingManager = Resolve<ISettingManager>();
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private TipoDeDocumento CreateTipoDeDocumento(string descricao, string mascara,
            TipoDeDocumentoEnum tipoDeDocumentoFixo)
        {
            return new TipoDeDocumento
            {
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                Descricao = descricao,
                TipoDeDocumentoFixo = tipoDeDocumentoFixo,
                Mascara = mascara
            };
        }

        private Telefone CreateTelefone(PessoaFisica pessoa, TipoTelefoneEnum tipoDeTelefone, string ddi, string ddd,
            string numero, string ramal = null)
        {
            var telefone = new Telefone(pessoa, tipoDeTelefone, ddd, numero, ddi, ramal)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
            return telefone;

        }

        private Documento CreateDocumento(PessoaFisica pessoa, TipoDeDocumento tipoDeDocumento, string numero = null,
            DateTime? dataDeExpedicao = null, string orgao = null)
        {
            var doc = new Documento(pessoa, tipoDeDocumento, numero, orgao, dataDeExpedicao)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
            return doc;
        }

        private EnderecoEletronico CreateEnderecoEletronico(PessoaFisica pessoa,
            TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
        {
            return new EnderecoEletronico(pessoa, tipoDeEnderecoEletronico, endereco) { EmpresaId = EmpresaIdDefaultTest };
        }

        private TipoDeLogradouro CreateTipoDeLogradouro(string descricao)
        {
            return new TipoDeLogradouro
            {
                IsActive = true,
                Descricao = descricao
            };
        }

        private GrupoPessoa CreateGrupoPessoa(string nome, string descricao)
        {
            return new GrupoPessoa
            {
                IsActive = true,
                Descricao = descricao,
                Nome = nome
            };
        }




        private async Task CreatePessoaFisicaAndTestAsync(string nome)
        {
            var pfTitular = await base.PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nome,
                IsActive = true,
                Nacionalidade = "Brasileira",
                DataDeNascimento = DateTime.Now.AddYears(-40),
                Sexo = SexoEnum.Masculino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado,

            });

            var pessoaTitular = _pessoaRepository.Get(pfTitular.Id);

            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);

            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoCpf);


            var documentoCpf = this.CreateDocumento(pessoaTitular, tipoDeDocumentoCpf, "08164610747");

            _documentoRepository.InsertOrUpdate(documentoCpf);

            var enderecoEletronico = this.CreateEnderecoEletronico(pessoaTitular, TipoDeEnderecoEletronicoEnum.EmailPrincipal, "wescley@ezsoft.com.br");
            _enderecoEletronicoRepository.InsertOrUpdate(enderecoEletronico);

            var telefone = this.CreateTelefone(pessoaTitular, TipoTelefoneEnum.Celular, "55", "21", "993405432");
            _telefoneRepository.InsertOrUpdate(telefone);

            //return pessoaTitular;
        }


        [Fact]
        public async Task Should_Get_CpfPaginado()
        {

            await RunAsyncInTransaction(async (uow) =>
            {
                PessoaFisica pessoaFisica = new PessoaFisica
                {
                    Nome = "Wescley"
                };

                this.CreatePessoaFisica(pessoaFisica);
                await CreatePessoaFisicaAndTestAsync("Wescley Arruda");
                var cpfPaginado = new GetPessoaFisicaByCPFInput();
                cpfPaginado.CPFPessoaFisica = "081";
                //await uow.Current.SaveChangesAsync();
                await base.PessoaFisicaAppService.GetByCPFPaginado(cpfPaginado);
            });
        }
    }
}
