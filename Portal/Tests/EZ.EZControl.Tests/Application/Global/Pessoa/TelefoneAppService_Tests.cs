﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Telefone;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class TelefoneAppService_Tests : TelefoneAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateTelefoneAndTestAsync(EZControl.Domain.Global.Pessoa.Pessoa pessoa, String DDI, String DDD, String numero, TipoTelefoneEnum tipo, String ramal = null)
        {
            await base.TelefoneAppService.Save(new TelefoneInput
            {
                Tipo = tipo,
                DDI = DDI,
                DDD = DDD,
                Numero = numero,
                Ramal = ramal,
                PessoaId = pessoa.Id
            });
        }

        private PessoaFisica CreatePessoaFisica()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };
            this.CreatePessoa(pessoaFisica);
            return pessoaFisica;
        }

        [Fact]
        public async Task Should_Create_Telefone()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Wescley"
            };

            this.CreatePessoa(pessoaFisica);

            base.CreateTestTelefone(pessoaFisica);
            await CreateTelefoneAndTestAsync(pessoaFisica, "55", "21", "21239100", TipoTelefoneEnum.Residencial);
            await CreateTelefoneAndTestAsync(pessoaFisica, "55", "21", "993405432", TipoTelefoneEnum.Celular);
            await CreateTelefoneAndTestAsync(pessoaFisica, "55", "21", "3303737", TipoTelefoneEnum.Comercial, "1234");


        }

        [Fact]
        public async Task Delete_Telefone()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Wescley"
            };

            this.CreatePessoa(pessoaFisica);
            base.CreateTestTelefone(pessoaFisica);

            await base.TelefoneAppService.Delete(new IdInput(1));
            await base.TelefoneAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

        [Fact]
        public async Task Should_Not_Create_Telefone_With_Duplicate_Numero()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Wescley"
            };

            this.CreatePessoa(pessoaFisica);

            base.CreateTestTelefone(pessoaFisica);
            await base.TelefoneAppService.Save(
                new TelefoneInput
                {
                    Numero = "30303737",
                    Tipo = TipoTelefoneEnum.Comercial
                })
                .ShouldThrowAsync<AbpValidationException>();
        }
    }
}