﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.Documento;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class DocumentoAppService_Tests : DocumentoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateDocumentoAndTestAsync(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeDocumento tipoDeDocumento, String numero, String orgaoExpedidor = null, DateTime? dataDeExpedicao = null,
                                                       Estado estado = null, DateTime? dataDeValidade = null, String serie = null)
        {
            var _tipoDeDocumento = await base.TipoDeDocumentoAppService.GetTipoDeDocumentoByDescricao(new GetTipoDeDocumentoInput { Descricao = tipoDeDocumento.Descricao });

            var _documento = new DocumentoInput
            {
                PessoaId = pessoa.Id,
                Numero = numero,
                TipoDeDocumentoId = _tipoDeDocumento.Id,
                OrgaoExpedidor = orgaoExpedidor,
                DataExpedicao = dataDeExpedicao,
                DataDeValidade = dataDeValidade,
                Serie = serie
            };

            if (estado != null)
            {
                _documento.EstadoId = estado.Id;
            }

            await DocumentoAppService.Save(_documento);
        }

        [Fact]
        public async Task Should_Create_Documento()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };

            this.CreatePessoa(pessoaFisica);

            base.CreateTestDocumento(pessoaFisica);

            var tipoDeDocumentoCPF = new TipoDeDocumento
            {
                TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf,
                Descricao = "CPF",
                IsActive = true,
                ExibirNoCadastroSimplificadoDePessoa = true,
                Mascara = "999.999.999-99",
                ObrigaAnexo = false,
                ObrigaDataDeValidade = true,
                ObrigaDataExpedicao = true,
                ListaEmOutrosDocumentos = true,
                ObrigaOrgaoExpedidor = true,
                PermiteLancamentoDuplicado = false,
                PrazoDeAtualizacaoDoAnexo = 30,
                ObrigaUF = true,
                Prioridade = 1,
                Principal = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                SerieObrigatoria = true
            };

            await CreateDocumentoAndTestAsync(pessoaFisica, tipoDeDocumentoCPF, "131.624.209-96");
            await CreateDocumentoAndTestAsync(pessoaFisica, tipoDeDocumentoCPF, "096.004.658-78");
        }

        [Fact]
        public async Task Should_Not_Create_Documento_With_Duplicate_Tipo()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Wescley"
            };

            this.CreatePessoa(pessoaFisica);

            base.CreateTestDocumento(pessoaFisica);
            var _tipoDeDocumento = await base.TipoDeDocumentoAppService.GetTipoDeDocumentoByDescricao(new GetTipoDeDocumentoInput { Descricao = "CPF" });

            await DocumentoAppService.Save(new DocumentoInput
            {
                TipoDeDocumentoId = _tipoDeDocumento.Id,
                Numero = "081.646.107-47"
            })
            .ShouldThrowAsync<AbpValidationException>();
        }


        [Fact]
        public async Task Should_Not_Create_Documento_With_Duplicate_Numero()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };

            this.CreatePessoa(pessoaFisica);

            base.CreateTestDocumento(pessoaFisica);
            var _tipoDeDocumento = await base.TipoDeDocumentoAppService.GetTipoDeDocumentoByDescricao(new GetTipoDeDocumentoInput { Descricao = "CPF" });

            await DocumentoAppService.Save(new DocumentoInput
            {
                TipoDeDocumentoId = _tipoDeDocumento.Id,
                Numero = "131.627.852-98"
            })
            .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Documento()
        {
            PessoaFisica pessoaFisica = new PessoaFisica
            {
                Nome = "Marcos"
            };

            this.CreatePessoa(pessoaFisica);

            base.CreateTestDocumento(pessoaFisica);
            await DocumentoAppService.Delete(new IdInput(3));
            await DocumentoAppService.GetById(new IdInput(3)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}