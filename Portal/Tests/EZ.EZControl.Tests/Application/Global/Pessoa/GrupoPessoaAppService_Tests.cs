﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Runtime.Validation;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using EZ.EZControl.Domain.Global.Enums;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class GrupoPessoaAppService_Tests : GrupoPessoaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_GrupoPessoa()
        {
            await CreateGrupoPessoaAndTestAsync("Grupo OAB", TipoPessoaEnum.Juridica);
            await CreateGrupoPessoaAndTestAsync("Grupo Itaú", TipoPessoaEnum.Juridica);
            await CreateGrupoPessoaAndTestAsync("Grupo Santander", TipoPessoaEnum.Juridica);
        }

        [Fact]
        public async Task Should_Create_GrupoPessoa_With_GrupoSuperior()
        {
            await CreateGrupoPessoaWithGrupoSuperiorAndTestAsync("Grupo OAB", TipoPessoaEnum.Juridica, "Grupo Advogados", TipoPessoaEnum.Juridica);
            await CreateGrupoPessoaWithGrupoSuperiorAndTestAsync("Grupo Ambev", TipoPessoaEnum.Juridica, "Grupo Bebidas", TipoPessoaEnum.Juridica);
        }

        [Fact]
        public async Task Should_Not_Create_GrupoPessoa_With_Duplicate_Nome()
        {
            base.CreateTestGrupoPessoa();
            await base.GrupoPessoaAppService
                    .Save(new GrupoPessoaInput { Nome = "Grupo OAB" })
                    .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Not_Create_GrupoPessoa_With_GrupoSuperior_Duplicate_Nome()
        {

            base.CreateTestGrupoPessoaWithGrupoSuperior();
            await base.GrupoPessoaAppService
                    .Save(new GrupoPessoaInput { Nome = "Grupo OAB", TipoDePessoa = TipoPessoaEnum.Juridica })
                    .ShouldThrowAsync<AbpValidationException>();

        }

        [Fact]
        public async Task Should_CreateOrUpdate_GrupoPessoa()
        {
            #region Criar Grupo Pessoa

            this.CreateTestGrupoPessoa();

            #endregion Criar Grupo Pessoa

            #region Alterar Grupo Pessoa

            var gruposPessoa = await base.GrupoPessoaAppService.GetGruposPessoaPaginado(new GetGrupoPessoaInput { Nome = "Grupo OAB" });
            var grupoPessoa = gruposPessoa.Items[0];
            grupoPessoa.Nome = "Grupo Riachuelo";

            var input = grupoPessoa.MapTo<GrupoPessoaInput>();

            await base.GrupoPessoaAppService.Save(input);

            await UsingDbContext(async context =>
            {
                var updateGrupoPessoa = await context.GrupoPessoas.FirstOrDefaultAsync(x => x.Nome == "Grupo Riachuelo");
                updateGrupoPessoa.ShouldNotBe(null);
            });

            #endregion Alterar Grupo Pessoa
        }

        [Fact]
        public async Task Should_CreateOrUpdate_GrupoPessoaComGrupoSuperior()
        {
            #region Criar Grupo Pessoa com Superior

            base.CreateTestGrupoPessoaWithGrupoSuperior();

            #endregion Criar Grupo Pessoa com Superior

            #region Alterar Grupo Pessoa com Superior

            var gruposPessoa = await base.GrupoPessoaAppService.GetGruposPessoaPaginado(new GetGrupoPessoaInput { Nome = "Grupo OAB" });
            var grupoPessoa = gruposPessoa.Items[0];
            grupoPessoa.Nome = "Grupo Riachuelo";

            var gruposPessoaComSuperior = await base.GrupoPessoaAppService.GetGruposPessoaPaginado(new GetGrupoPessoaInput { Nome = "Grupo Advogados" });
            var grupoPessoaComSuperior = gruposPessoaComSuperior.Items[0];
            grupoPessoaComSuperior.Nome = "Grupo C&A";

            var input = grupoPessoa.MapTo<GrupoPessoaInput>();
            var input2 = grupoPessoaComSuperior.MapTo<GrupoPessoaInput>();

            await base.GrupoPessoaAppService.Save(input);
            await base.GrupoPessoaAppService.Save(input2);

            await UsingDbContext(async context =>
            {
                var updateGrupoPessoa = await context.GrupoPessoas.FirstOrDefaultAsync(x => x.Nome == "Grupo Riachuelo");
                updateGrupoPessoa.ShouldNotBe(null);
            });

            await UsingDbContext(async context =>
            {
                var contemGrupoPessoaComSuperior = await context.GrupoPessoas.AnyAsync(x => x.Pai.Nome == "Grupo Riachuelo");
                contemGrupoPessoaComSuperior.ShouldNotBe(true);
            });

            #endregion Alterar Grupo Pessoa com Superior
        }

        [Fact]
        public async Task Should_Delete_GrupoPessoa()
        {
            #region Criar Grupo Pessoa

            base.CreateTestGrupoPessoa();

            #endregion Criar Grupo Pessoa

            GetGrupoPessoaInput input = new GetGrupoPessoaInput { Nome = "Grupo OAB" };

            var grupos = await base.GrupoPessoaAppService.GetGruposPessoaPaginado(input);
            var grupo = grupos.Items[0];
            grupo.ShouldNotBe(null);

            await base.GrupoPessoaAppService.Delete(new IdInput(grupo.Id));
            grupos = await base.GrupoPessoaAppService.GetGruposPessoaPaginado(input);

            if (grupos.TotalCount > 0)
            {
                grupo = grupos.Items[0];
                if (grupo != null)
                    grupo.IsDeleted.ShouldBe(true);
            }
        }

        private async Task CreateGrupoPessoaAndTestAsync(String nome, TipoPessoaEnum tipoPessoa)
        {
            await base.GrupoPessoaAppService.Save(new GrupoPessoaInput() { Nome = nome, TipoDePessoa = tipoPessoa});
            await UsingDbContext(async context =>
            {
                var createdGrupoPessoa = await context.GrupoPessoas.FirstOrDefaultAsync(g => g.Nome == nome);
                createdGrupoPessoa.ShouldNotBe(null);
            });
        }

        private async Task CreateGrupoPessoaWithGrupoSuperiorAndTestAsync(String nome, TipoPessoaEnum tipoPessoa, String nomeSuperior, TipoPessoaEnum tipoPessoaSuperior)
        {
            GrupoPessoaInput input = new GrupoPessoaInput { IsActive = true, Nome = nome, TipoDePessoa = tipoPessoa};
            var grupoPessoa = await base.GrupoPessoaAppService.Save(input);
            await UsingDbContext(async context =>
            {
                var createdGrupoPessoa = await context.GrupoPessoas.FirstOrDefaultAsync(x => x.Id == grupoPessoa.Id);
                createdGrupoPessoa.ShouldNotBe(null);
            });

            GrupoPessoaInput input2 = new GrupoPessoaInput { IsActive = true, Nome = nomeSuperior, PaiId = grupoPessoa.Id, TipoDePessoa = tipoPessoaSuperior };
            var grupoPessoaSuperior = await base.GrupoPessoaAppService.Save(input2);
            await UsingDbContext(async context =>
            {
                var createdGrupoPessoaSuperior = await context.GrupoPessoas.FirstOrDefaultAsync(x => x.Id == grupoPessoaSuperior.Id);
                createdGrupoPessoaSuperior.ShouldNotBe(null);
            });
        }
    }
}