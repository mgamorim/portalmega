﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeDocumento;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class TipoDeDocumentoAppService_Tests : TipoDeDocumentoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateTipoDeDocumentoAndTestAsync(TipoDeDocumentoEnum tipo, String descricao, Boolean exibeNoCadastroSilmples, Boolean listaEmOutrosDocs, String mascara,
                                                             Boolean obrigaAnexo, Boolean obrigaDataDeValidade, Boolean obrigaDataDeExpedicao, Boolean obrigaOrgaoExpeditor, Boolean obrigaUf,
                                                             Boolean permiteLancamentoDuplicado, Int32 prazoDeAtualizacaoDoAnexo, Boolean principal, Int32 prioridade, Boolean serieObrigatoria, TipoPessoaEnum tipoPessoa)
        {
            await base.TipoDeDocumentoAppService.Save(new TipoDeDocumentoInput
            {
                IsActive = true,
                TipoDeDocumentoFixo = tipo,
                Descricao = descricao,
                ExibirNoCadastroSimplificadoDePessoa = exibeNoCadastroSilmples,
                ListaEmOutrosDocumentos = listaEmOutrosDocs,
                Mascara = mascara,
                ObrigaAnexo = obrigaAnexo,
                ObrigaDataDeValidade = obrigaDataDeValidade,
                ObrigaDataExpedicao = obrigaDataDeExpedicao,
                ObrigaOrgaoExpedidor = obrigaOrgaoExpeditor,
                ObrigaUF = obrigaUf,
                PermiteLancamentoDuplicado = permiteLancamentoDuplicado,
                PrazoDeAtualizacaoDoAnexo = prazoDeAtualizacaoDoAnexo,
                Principal = principal,
                Prioridade = prioridade,
                SerieObrigatoria = serieObrigatoria,
                TipoPessoa = tipoPessoa
            });
        }

        [Fact]
        public async Task Should_Create_TipoDeDocumento()
        {
            base.CreateTestTipoDeDocumento();
            await CreateTipoDeDocumentoAndTestAsync(TipoDeDocumentoEnum.Rg, "RG", true, true, "99.999.999-9", false, true, true, true, true, false, 60, true, 3, true, TipoPessoaEnum.Fisica);
        }

        [Fact]
        public async Task Should_Not_Create_TipoDeDocumento_With_Duplicate_Descricao()
        {
            base.CreateTestTipoDeDocumento();
            await base.TipoDeDocumentoAppService.Save(
                new TipoDeDocumentoInput
                {
                    IsActive = true,
                    TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf,
                    Descricao = "CPF",
                    ExibirNoCadastroSimplificadoDePessoa = true,
                    ListaEmOutrosDocumentos = true,
                    Mascara = "999.999.999-99",
                    ObrigaAnexo = false,
                    ObrigaDataDeValidade = true,
                    ObrigaDataExpedicao = true,
                    ObrigaOrgaoExpedidor = true,
                    ObrigaUF = true,
                    PermiteLancamentoDuplicado = false,
                    PrazoDeAtualizacaoDoAnexo = 30,
                    Principal = true,
                    Prioridade = 1,
                    SerieObrigatoria = true,
                    TipoPessoa = TipoPessoaEnum.Fisica
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_TipoDeDocumento()
        {
            base.CreateTestTipoDeDocumento();
            await base.TipoDeDocumentoAppService.Delete(new IdInput(1));
            await base.TipoDeDocumentoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}