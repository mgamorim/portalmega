﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Pessoa.TipoDeContato;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class TipoDeContatoAppService_Tests : TipoDeContatoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateTipoDeContatoAndTestAsync(String descricao)
        {
            await base.TipoDeContatoAppServices.Save(new TipoDeContatoInput
            {
                IsActive = true,
                Descricao = descricao,
                TipoDeContatoFixo = TipoDeContatoEnum.Telefone,
                Mascara = "99999-9999"
            });
        }

        [Fact]
        public async Task Should_Create_TipoDeContato()
        {
            base.CreateTestTipoDeContato();
            await CreateTipoDeContatoAndTestAsync("Administrativo");
            await CreateTipoDeContatoAndTestAsync("Pessoal");
        }

        [Fact]
        public async Task Should_Not_Create_Tratamento_With_Duplicate_Descricao()
        {
            base.CreateTestTipoDeContato();
            await base.TipoDeContatoAppServices.Save(
                new TipoDeContatoInput
                {
                    IsActive = true,
                    Descricao = "Principal",
                    TipoDeContatoFixo = TipoDeContatoEnum.Telefone,
                    Mascara = "99999-9999"
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_TipoDeLogradouro()
        {
            base.CreateTestTipoDeContato();
            await base.TipoDeContatoAppServices.Delete(new IdInput(1));
            await base.TipoDeContatoAppServices.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}