﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using EZ.EZControl.Domain.Global.Enums;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public abstract class GrupoPessoaAppServiceTestBase : AppTestBase
    {
        protected readonly IGrupoPessoaAppService GrupoPessoaAppService;

        public GrupoPessoaAppServiceTestBase()
        {
            GrupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }

        protected void CreateTestGrupoPessoa()
        {
            UsingDbContext(context =>
            {
                context.GrupoPessoas.Add(CreateGrupoPessoaEntity("Grupo OAB", TipoPessoaEnum.Juridica));
            });
        }

        protected void CreateTestGrupoPessoaWithGrupoSuperior()
        {
            UsingDbContext(context =>
            {
                context.GrupoPessoas.Add(CreateGrupoPessoaWithGrupoSuperiorEntity("Grupo Advogados", TipoPessoaEnum.Juridica, "Grupo OAB", TipoPessoaEnum.Juridica));
            });
        }

        private GrupoPessoa CreateGrupoPessoaEntity(String nome, TipoPessoaEnum tipoPessoa)
        {
            return new GrupoPessoa
            {
                Nome = nome,
                TipoDePessoa = tipoPessoa
            };
        }

        private GrupoPessoa CreateGrupoPessoaWithGrupoSuperiorEntity(String nome, TipoPessoaEnum tipoPessoa,  String nomeSuperior, TipoPessoaEnum tipoPessoaSuperior)
        {
            return new GrupoPessoa
            {
                Nome = nome,
                TipoDePessoa = tipoPessoa,
                Pai = new GrupoPessoa
                {
                    Nome = nomeSuperior,
                    TipoDePessoa = tipoPessoaSuperior
                }
            };
        }
    }
}