﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class TipoDeContatoAppServiceTestBase : AppTestBase
    {
        protected readonly ITipoDeContatoAppService TipoDeContatoAppServices;

        public TipoDeContatoAppServiceTestBase()
        {
            this.TipoDeContatoAppServices = Resolve<ITipoDeContatoAppService>();
        }

        protected void CreateTestTipoDeContato()
        {
            UsingDbContext(context =>
            {
                context.TipoDeContatos.Add(CreateTipoDeContato("Principal"));
                context.TipoDeContatos.Add(CreateTipoDeContato("Comercial"));
            });
        }

        private TipoDeContato CreateTipoDeContato(String descricao)
        {
            return new TipoDeContato
            {
                IsActive = true,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}