﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class DocumentoAppServiceTestBase : AppTestBase
    {
        protected readonly IDocumentoAppService DocumentoAppService;
        protected readonly ITipoDeDocumentoAppService TipoDeDocumentoAppService;

        public DocumentoAppServiceTestBase()
        {
            this.DocumentoAppService = Resolve<IDocumentoAppService>();
            this.TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoAppService>();
        }

        protected void CreateTestDocumento(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {

                var cpf = CreateTipoDeDocumento(TipoDeDocumentoEnum.Cpf, "CPF", true, true, "999.999.999-99", false,
                    true, true, true, true, false, 30, true, 1, true, TipoPessoaEnum.Fisica);

                var cnpj = CreateTipoDeDocumento(TipoDeDocumentoEnum.Cnpj, "CNPJ", true, true, "99.999.999/9999-99",
                    false, true, true, true, true, false, 90, true, 2, true, TipoPessoaEnum.Juridica);

                var rg = CreateTipoDeDocumento(TipoDeDocumentoEnum.Rg, "RG", true, true, "99.999.999-9", false,
                    true, true, true, true, false, 60, true, 3, true, TipoPessoaEnum.Fisica);

                context.Documentos.Add(CreateDocumento(pessoa, cpf, "131.627.852-98", "Detran"));
                context.Documentos.Add(CreateDocumento(pessoa, cnpj, "241.223.171/0001-71"));
                context.Documentos.Add(CreateDocumento(pessoa, rg, "09.961.877-9"));
            });
        }

        protected void CreatePessoa(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                if (pessoa is PessoaFisica)
                    context.PessoasFisicas.Add(pessoa as PessoaFisica);
                else if (pessoa is PessoaJuridica)
                    context.PessoasJuridicas.Add(pessoa as PessoaJuridica);

                context.SaveChanges();
            });
        }

        private Documento CreateDocumento(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeDocumento tipoDeDocumento, String numero, String orgaoExpedidor = null, DateTime? dataDeExpedicao = null,
                                          Estado estado = null, DateTime? dataDeValidade = null, String serie = null)
        {
            return new Documento(pessoa, tipoDeDocumento, numero, orgaoExpedidor, dataDeExpedicao, estado, dataDeValidade, serie) { EmpresaId = EmpresaIdDefaultTest };
        }

        private TipoDeDocumento CreateTipoDeDocumento(TipoDeDocumentoEnum tipo, String descricao, Boolean exibeNoCadastroSilmples, Boolean listaEmOutrosDocs, String mascara,
                                                     Boolean obrigaAnexo, Boolean obrigaDataDeValidade, Boolean obrigaDataDeExpedicao, Boolean obrigaOrgaoExpeditor, Boolean obrigaUf,
                                                     Boolean permiteLancamentoDuplicado, Int32 prazoDeAtualizacaoDoAnexo, Boolean principal, Int32 prioridade, Boolean serieObrigatoria, TipoPessoaEnum tipoPessoa)
        {
            return new TipoDeDocumento(tipo)
            {
                IsActive = true,
                Descricao = descricao,
                ExibirNoCadastroSimplificadoDePessoa = exibeNoCadastroSilmples,
                ListaEmOutrosDocumentos = listaEmOutrosDocs,
                Mascara = mascara,
                ObrigaAnexo = obrigaAnexo,
                ObrigaDataDeValidade = obrigaDataDeValidade,
                ObrigaDataExpedicao = obrigaDataDeExpedicao,
                ObrigaOrgaoExpedidor = obrigaOrgaoExpeditor,
                ObrigaUF = obrigaUf,
                PermiteLancamentoDuplicado = permiteLancamentoDuplicado,
                PrazoDeAtualizacaoDoAnexo = prazoDeAtualizacaoDoAnexo,
                Principal = principal,
                Prioridade = prioridade,
                SerieObrigatoria = serieObrigatoria,
                TipoPessoa = tipoPessoa
            };
        }
    }
}