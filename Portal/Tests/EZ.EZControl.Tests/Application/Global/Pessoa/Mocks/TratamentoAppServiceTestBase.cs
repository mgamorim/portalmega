﻿using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class TratamentoAppServiceTestBase : AppTestBase
    {
        protected readonly ITratamentoAppService TratamentoAppService;

        public TratamentoAppServiceTestBase()
        {
            this.TratamentoAppService = Resolve<ITratamentoAppService>();
        }

        protected void CreateTestTratamento()
        {
            UsingDbContext(context =>
            {
                context.Tratamentos.Add(CreateTratamento("Sr."));
                context.Tratamentos.Add(CreateTratamento("Sra."));
                context.Tratamentos.Add(CreateTratamento("Dr."));
                context.Tratamentos.Add(CreateTratamento("Dra."));
            });
        }

        private Tratamento CreateTratamento(String descricao)
        {
            return new Tratamento
            {
                IsActive = true,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}