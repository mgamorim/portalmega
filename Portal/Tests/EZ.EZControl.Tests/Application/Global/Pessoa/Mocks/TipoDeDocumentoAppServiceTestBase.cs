﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class TipoDeDocumentoAppServiceTestBase : AppTestBase
    {
        protected readonly ITipoDeDocumentoAppService TipoDeDocumentoAppService;

        public TipoDeDocumentoAppServiceTestBase()
        {
            this.TipoDeDocumentoAppService = Resolve<ITipoDeDocumentoAppService>();
        }

        protected void CreateTestTipoDeDocumento()
        {
            UsingDbContext(context =>
            {
                context.TipoDeDocumentos.Add(CreateTipoDeDocumento(TipoDeDocumentoEnum.Cpf, "CPF", true, true, "999.999.999-99", false, true, true, true, true, false, 30, true, 1, true, TipoPessoaEnum.Fisica));
                context.TipoDeDocumentos.Add(CreateTipoDeDocumento(TipoDeDocumentoEnum.Cnpj, "CNPJ", true, true, "99.999.999/9999-99", false, true, true, true, true, false, 90, true, 2, true, TipoPessoaEnum.Juridica));
            });
        }

        private TipoDeDocumento CreateTipoDeDocumento(TipoDeDocumentoEnum tipo, String descricao, Boolean exibeNoCadastroSilmples, Boolean listaEmOutrosDocs, String mascara,
                                                      Boolean obrigaAnexo, Boolean obrigaDataDeValidade, Boolean obrigaDataDeExpedicao, Boolean obrigaOrgaoExpeditor, Boolean obrigaUf,
                                                      Boolean permiteLancamentoDuplicado, Int32 prazoDeAtualizacaoDoAnexo, Boolean principal, Int32 prioridade, Boolean serieObrigatoria, TipoPessoaEnum tipoPessoa)
        {
            return new TipoDeDocumento(tipo)
            {
                IsActive = true,
                Descricao = descricao,
                ExibirNoCadastroSimplificadoDePessoa = exibeNoCadastroSilmples,
                ListaEmOutrosDocumentos = listaEmOutrosDocs,
                Mascara = mascara,
                ObrigaAnexo = obrigaAnexo,
                ObrigaDataDeValidade = obrigaDataDeValidade,
                ObrigaDataExpedicao = obrigaDataDeExpedicao,
                ObrigaOrgaoExpedidor = obrigaOrgaoExpeditor,
                ObrigaUF = obrigaUf,
                PermiteLancamentoDuplicado = permiteLancamentoDuplicado,
                PrazoDeAtualizacaoDoAnexo = prazoDeAtualizacaoDoAnexo,
                Principal = principal,
                Prioridade = prioridade,
                SerieObrigatoria = serieObrigatoria,
                TipoPessoa = tipoPessoa
            };
        }
    }
}