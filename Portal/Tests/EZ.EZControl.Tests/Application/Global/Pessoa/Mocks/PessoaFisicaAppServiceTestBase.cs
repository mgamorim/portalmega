﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Services.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class PessoaFisicaAppServiceTestBase:AppTestBase
    {
        protected readonly IPessoaFisicaAppService PessoaFisicaAppService;


        public PessoaFisicaAppServiceTestBase()
        {
            this.PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
        }

        protected void CreatePessoaFisica(EZControl.Domain.Global.Pessoa.PessoaFisica pessoa)
        {
            UsingDbContext(context =>
            {
                context.PessoasFisicas.Add(pessoa);
                context.SaveChanges();
            });
        }



        private void CreatePessoaFisica(string nome,string nacionalidade,SexoEnum sexo,EstadoCivilEnum estadoCivil,string cpf, int grupoPessoa )
        {
            var pessoaf = new PessoaFisica();
            pessoaf.Nome = nome;
            pessoaf.Nacionalidade = nacionalidade;
            pessoaf.Sexo = sexo;
            pessoaf.EstadoCivil = estadoCivil;
            pessoaf.GrupoPessoa.Id = grupoPessoa;
            var doc = new Documento();
            doc.Numero = cpf;
            doc.TipoDeDocumento.TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf;
            pessoaf.Documentos.Add(doc);
                   
            UsingDbContext(context =>
            {
                context.PessoasFisicas.Add(pessoaf);
                context.SaveChanges();
            });
        }

       
    }
}
