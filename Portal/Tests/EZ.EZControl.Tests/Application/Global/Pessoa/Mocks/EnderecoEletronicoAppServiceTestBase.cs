﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class EnderecoEletronicoAppServiceTestBase : AppTestBase
    {
        protected readonly IEnderecoEletronicoAppService EnderecoEletronicoAppService;
        protected readonly IContatoService ContatoService;

        public EnderecoEletronicoAppServiceTestBase()
        {
            this.EnderecoEletronicoAppService = Resolve<IEnderecoEletronicoAppService>();
            this.ContatoService = Resolve<IContatoService>();
        }

        protected void CreatePessoa(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                if (pessoa is PessoaFisica)
                    context.PessoasFisicas.Add(pessoa as PessoaFisica);
                else if (pessoa is PessoaJuridica)
                    context.PessoasJuridicas.Add(pessoa as PessoaJuridica);

                context.SaveChanges();
            });
        }

        protected void CreateContato(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                context.Contatos.Add(CreateContato(pessoa, "João Fernandez", "Analista", "Desenvolvimento"));
                context.Contatos.Add(CreateContato(pessoa, "Mario Castro", "Analista", "Desenvolvimento"));
            });
        }

        protected void CreateTratamento()
        {
            UsingDbContext(context =>
            {
                context.Tratamentos.Add(CreateTratamento("Sr."));
            });
        }

        protected void CreateTestEnderecoEletronico(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                context.EnderecosEletronicos.Add(CreateEnderecoEletronico(pessoa, "teste@gmail.com", TipoDeEnderecoEletronicoEnum.EmailPrincipal));
                context.EnderecosEletronicos.Add(CreateEnderecoEletronico(pessoa, "teste_nfe@ezcontrol.com", TipoDeEnderecoEletronicoEnum.Email_NFe));
            });
        }

        private EnderecoEletronico CreateEnderecoEletronico(EZControl.Domain.Global.Pessoa.Pessoa pessoa, String endereco, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico)
        {
            return new EnderecoEletronico(pessoa, tipoDeEnderecoEletronico, endereco)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Contato CreateContato(EZControl.Domain.Global.Pessoa.Pessoa pessoa, string nome, string cargo, string setor)
        {
            var tipoDeContato = new TipoDeContato
            {
                IsActive = true,
                Descricao = "Comercial",
                EmpresaId = EmpresaIdDefaultTest
            };

            var tratamento = CreateTratamento("Sr.");

            return new Contato(pessoa, nome, tipoDeContato, tratamento, SexoEnum.NaoInformado, null, cargo, setor, null,
                null, ContatoService)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Tratamento CreateTratamento(string descricao)
        {
            return new Tratamento
            {
                Descricao = descricao,
                IsActive = true,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}