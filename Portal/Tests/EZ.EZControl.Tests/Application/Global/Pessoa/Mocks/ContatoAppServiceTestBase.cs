﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class ContatoAppServiceTestBase : AppTestBase
    {
        protected readonly IContatoAppService ContatoAppService;
        protected readonly IContatoService ContatoService;
        protected readonly ITipoDeContatoAppService TipoDeContatoAppService;

        public ContatoAppServiceTestBase()
        {
            this.ContatoAppService = Resolve<IContatoAppService>();
            this.TipoDeContatoAppService = Resolve<ITipoDeContatoAppService>();
            this.ContatoService = Resolve<IContatoService>();
        }

        protected void CreateTestContato(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                var _tipoDeContato = CreateTipoDeContato("Comercial");

                context.Contatos.Add(CreateContato(pessoa, _tipoDeContato, "João Fernandez", "Analista", "Desenvolvimento"));
                context.Contatos.Add(CreateContato(pessoa, _tipoDeContato, "Mario Castro", "Analista", "Desenvolvimento"));
            });
        }

        private Contato CreateContato(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeContato tipoDeContato, String nome, String cargo, String setor)
        {
            var tratamento = new Tratamento { Descricao = "Sr.",
                EmpresaId = EmpresaIdDefaultTest
            };
            return new Contato(pessoa, nome, tipoDeContato, tratamento, SexoEnum.NaoInformado, null, cargo, setor, null,
                null, ContatoService)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        protected void CreatePessoa(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                if (pessoa is PessoaFisica)
                    context.PessoasFisicas.Add(pessoa as PessoaFisica);
                else if (pessoa is PessoaJuridica)
                    context.PessoasJuridicas.Add(pessoa as PessoaJuridica);

                context.SaveChanges();
            });
        }

        protected void CreateTipoDeContato(TipoDeContato tipoDeContato)
        {
            UsingDbContext(context =>
            {
                context.TipoDeContatos.Add(tipoDeContato);
                context.SaveChanges();
            });
        }

        protected void CreateTratamento()
        {
            UsingDbContext(context =>
            {
                context.Tratamentos.Add(new Tratamento
                {
                    Descricao = "Sr.",
                    IsActive = true,
                    EmpresaId = EmpresaIdDefaultTest
                });
                context.SaveChanges();
            });
        }

        protected PessoaJuridica CreatePessoaJuridica(String nomeFantasia, String razaoSocial)
        {
            var pessoaJuridica = new PessoaJuridica
            {
                NomeFantasia = nomeFantasia,
                RazaoSocial = razaoSocial
            };

            CreatePessoa(pessoaJuridica);

            return pessoaJuridica;
        }

        private TipoDeContato CreateTipoDeContato(String descricao)
        {
            return new TipoDeContato
            {
                IsActive = true,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}