﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Pessoa.Mocks
{
    public class TelefoneAppServiceTestBase : AppTestBase
    {
        protected readonly ITelefoneAppService TelefoneAppService;

        public TelefoneAppServiceTestBase()
        {
            this.TelefoneAppService = Resolve<ITelefoneAppService>();
        }

        protected void CreateTestTelefone(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                context.Telefones.Add(CreateTelefone(pessoa, "55", "21", "964264146", TipoTelefoneEnum.Celular));
                context.Telefones.Add(CreateTelefone(pessoa, "55", "21", "21239100", TipoTelefoneEnum.Comercial, "6238"));
            });
        }

       
        protected void CreatePessoa(EZControl.Domain.Global.Pessoa.Pessoa pessoa)
        {
            UsingDbContext(context =>
            {
                if (pessoa is PessoaFisica)
                    context.PessoasFisicas.Add(pessoa as PessoaFisica);
                else if (pessoa is PessoaJuridica)
                    context.PessoasJuridicas.Add(pessoa as PessoaJuridica);

                context.SaveChanges();
            });
        }

        private Telefone CreateTelefone(EZControl.Domain.Global.Pessoa.Pessoa pessoa, String DDI, String DDD, String numero, TipoTelefoneEnum tipo, String ramal = null)
        {
            return new Telefone(pessoa, tipo, DDD, numero, DDI, ramal)
            {
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}