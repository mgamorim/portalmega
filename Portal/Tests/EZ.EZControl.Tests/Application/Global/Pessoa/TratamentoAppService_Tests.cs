﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Pessoa.Tratamento;
using EZ.EZControl.Tests.Application.Global.Pessoa.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Pessoa
{
    public class TratamentoAppService_Tests : TratamentoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateTratamentoAndTestAsync(String descricao)
        {
            await base.TratamentoAppService.Save(new TratamentoInput
            {
                IsActive = true,
                Descricao = descricao
            });
        }

        [Fact]
        public async Task Should_Create_Tratamento()
        {
            base.CreateTestTratamento();
            await CreateTratamentoAndTestAsync("Doutor");
            await CreateTratamentoAndTestAsync("Senhor");
            await CreateTratamentoAndTestAsync("Comandante");
        }

        [Fact]
        public async Task Should_Not_Create_Tratamento_With_Duplicate_Descricao()
        {
            base.CreateTestTratamento();
            await base.TratamentoAppService.Save(
                new TratamentoInput
                {
                    Descricao = "Sr."
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Tratamento()
        {
            base.CreateTestTratamento();
            await base.TratamentoAppService.Delete(new IdInput(1));
            await base.TratamentoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}