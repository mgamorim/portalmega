﻿using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks
{
    public class UsuarioPorPessoaAppServiceTestBase : AppTestBase
    {
        public readonly IPessoaService PessoaService;
        public readonly IPessoaJuridicaService PessoaJuridicaService;
        public readonly IPessoaFisicaService PessoaFisicaService;
        public readonly IUsuarioPorPessoaService UsuarioPorPessoaService;
        public readonly IUsuarioPorPessoaAppService UsuarioPorPessoaAppService;
        public readonly IPessoaFisicaAppService PessoaFisicaAppService;
        public readonly UserManager UserService;

        public UsuarioPorPessoaAppServiceTestBase()
        {
            PessoaService = Resolve<IPessoaService>();
            PessoaJuridicaService = Resolve<IPessoaJuridicaService>();
            PessoaFisicaService = Resolve<IPessoaFisicaService>();
            UsuarioPorPessoaService = Resolve<IUsuarioPorPessoaService>();
            UsuarioPorPessoaAppService = Resolve<IUsuarioPorPessoaAppService>();
            PessoaFisicaAppService = Resolve<IPessoaFisicaAppService>();
            UserService = Resolve<UserManager>();
        }
    }
}