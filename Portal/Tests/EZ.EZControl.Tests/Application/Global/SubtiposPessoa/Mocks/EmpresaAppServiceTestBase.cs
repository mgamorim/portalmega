﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks
{
    public class EmpresaAppServiceTestBase : AppTestBase
    {
        public readonly IEmpresaAppService EmpresaAppService;
        public readonly IRepository<Empresa> EmpresaRepository;

        public EmpresaAppServiceTestBase()
        {
            EmpresaAppService = Resolve<IEmpresaAppService>();
            EmpresaRepository = Resolve<IRepository<Empresa>>();
        }
    }
}