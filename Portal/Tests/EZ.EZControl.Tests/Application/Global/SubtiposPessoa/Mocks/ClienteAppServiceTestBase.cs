﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks
{
    public class ClienteAppServiceTestBase : AppTestBase
    {
        public readonly IClienteAppService ClienteAppService;
        public readonly IRepository<Cliente> ClienteRepository;

        public ClienteAppServiceTestBase()
        {
            ClienteAppService = Resolve<IClienteAppService>();
            ClienteRepository = Resolve<IRepository<Cliente>>();
        }
    }
}