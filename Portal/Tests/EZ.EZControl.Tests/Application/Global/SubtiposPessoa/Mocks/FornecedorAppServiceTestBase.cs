﻿using Abp.Domain.Repositories;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks
{
    public class FornecedorAppServiceTestBase : AppTestBase
    {
        public readonly IFornecedorAppService FornecedorAppService;
        public readonly IRepository<Fornecedor> FornecedorRepository;

        public FornecedorAppServiceTestBase()
        {
            FornecedorAppService = Resolve<IFornecedorAppService>();
            FornecedorRepository = Resolve<IRepository<Fornecedor>>();
        }
    }
}