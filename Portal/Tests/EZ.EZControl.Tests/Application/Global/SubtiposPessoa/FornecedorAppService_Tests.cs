﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Fornecedor;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa
{
    //public class FornecedorAppService_Tests : FornecedorAppServiceTestBase
    //{
    //    private readonly IRepository<EZControl.Domain.Global.Pessoa.Pessoa> pessoaRepository;
    //    private readonly IRepository<Documento> documentoRepository;
    //    private readonly IRepository<TipoDeDocumento> tipoDeDocumentoRepository;

    //    public FornecedorAppService_Tests()
    //    {
    //        pessoaRepository = Resolve<IRepository<EZControl.Domain.Global.Pessoa.Pessoa>>();
    //        documentoRepository = Resolve<IRepository<Documento>>();
    //        tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
    //    }

    //    protected override void PreInitialize()
    //    {
    //        // Deixar comentado para não registrar uma conexão fake
    //        //base.PreInitialize();
    //    }

    //    private FornecedorInput CreateFornecedorInput(
    //        int pessoaId,
    //        int grupoPessoaId,
    //        string nome,
    //        string nomeFantasia,
    //        string razaoSocial,
    //        TipoPessoaEnum tipoPessoa,
    //        string observacao)
    //    {
    //        var input = new FornecedorInput();

    //        input.Pessoa = new PessoaInput();
    //        input.Pessoa.Id = pessoaId;
    //        input.Pessoa.GrupoPessoaId = grupoPessoaId;
    //        input.Pessoa.TipoPessoa = tipoPessoa;
    //        input.Pessoa.Observacao = observacao;

    //        if (tipoPessoa == TipoPessoaEnum.Fisica)
    //        {
    //            input.PessoaFisica = new PessoaFisicaInputAux();
    //            input.PessoaFisica.Nome = nome;
    //        }
    //        else
    //        {
    //            input.PessoaJuridica = new PessoaJuridicaInputAux();
    //            input.PessoaJuridica.NomeFantasia = nomeFantasia;
    //            input.PessoaJuridica.RazaoSocial = razaoSocial;
    //        }

    //        return input;
    //    }

    //    private async Task<FornecedorPessoaIdDto> SaveFornecedor(
    //        int pessoaId,
    //        int grupoPessoaId,
    //        string nome,
    //        string nomeFantasia,
    //        string razaoSocial,
    //        TipoPessoaEnum tipoPessoa,
    //        string observacao)
    //    {
    //        var input = CreateFornecedorInput(pessoaId, grupoPessoaId, nome, nomeFantasia, razaoSocial, tipoPessoa, observacao);
    //        var fornecedor = await FornecedorAppService.Save(input);
    //        return fornecedor;
    //    }

    //    [Fact]
    //    public async Task ShouldCreateFornecedor()
    //    {
    //        await RunAsyncInTransaction(async (uow) =>
    //         {
    //             await SaveFornecedor(0, 0, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");
    //         });
    //    }

    //    [Fact]
    //    public async Task ShouldCreateFornecedorAndGetFornecedoresPaginado()
    //    {
    //        await RunAsyncInTransaction(async (uow) =>
    //        {
    //            var dto1 = await SaveFornecedor(0, 0, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");
    //            var dto2 = await SaveFornecedor(0, 0, "Marcos Lommez", string.Empty, string.Empty, TipoPessoaEnum.Fisica, "Alguma observacao");
    //            var fornecedor1 = pessoaRepository.Get(dto1.PessoaId);
    //            var fornecedor2 = pessoaRepository.Get(dto2.PessoaId);

    //            // Criar tipo de documento CNPJ
    //            var tipoDocumentoCnpj = new TipoDeDocumento()
    //            {
    //                IsActive = true,
    //                Descricao = "Cnpj",
    //                TipoPessoa = TipoPessoaEnum.Juridica,
    //                TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cnpj
    //            };

    //            tipoDeDocumentoRepository.InsertOrUpdate(tipoDocumentoCnpj);

    //            // Criar tipo de documento CPF
    //            var tipoDocumentoCpf = new TipoDeDocumento()
    //            {
    //                IsActive = true,
    //                Descricao = "Cpf",
    //                TipoPessoa = TipoPessoaEnum.Fisica,
    //                TipoDeDocumentoFixo = TipoDeDocumentoEnum.Cpf
    //            };

    //            tipoDeDocumentoRepository.InsertOrUpdate(tipoDocumentoCpf);

    //            // Criar documento
    //            Documento documento1 = new Documento(fornecedor1, tipoDocumentoCnpj, "12345678901234");
    //            Documento documento2 = new Documento(fornecedor2, tipoDocumentoCpf, "12345678901");
    //            documentoRepository.InsertOrUpdate(documento1);
    //            documentoRepository.InsertOrUpdate(documento2);

    //            await uow.Current.SaveChangesAsync();

    //            GetFornecedorInput fornecedorInput = new GetFornecedorInput();

    //            fornecedorInput.SkipCount = 0;
    //            fornecedorInput.MaxResultCount = 10;
    //            fornecedorInput.Sorting = "Id";
    //            fornecedorInput.Nome = "Light";
    //            fornecedorInput.TipoDeDocumento = TipoDeDocumentoEnum.Cnpj;
    //            fornecedorInput.NumeroDocumento = "12345678901234";

    //            var result = await FornecedorAppService.GetFornecedoresPaginado(fornecedorInput);

    //            result.Items.Count.ShouldBe(1);
    //        });
    //    }

    //    [Fact]
    //    public async Task GetFornecedoresPaginado()
    //    {
    //        GetFornecedorInput fornecedorInput = new GetFornecedorInput();

    //        var result = await this.GetFornecedoresPaginado(fornecedorInput);

    //        result.Items.Count.ShouldBeGreaterThan(0);
    //    }

    //    [Fact]
    //    public async Task Delete_Fornecedor()
    //    {
    //        await RunAsyncInTransaction(async (uow) =>
    //        {
    //            var dto = await SaveFornecedor(0, 0, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");
    //            await FornecedorAppService.Delete(new IdInput(dto.FornecedorId));
    //            await uow.Current.SaveChangesAsync();
    //            await FornecedorAppService.GetById(new IdInput(dto.FornecedorId)).ShouldThrowAsync<UserFriendlyException>();
    //        });
    //    }
        
    //    public async Task<PagedResultOutput<FornecedorListDto>> GetFornecedoresPaginado(GetFornecedorInput input)
    //    {
    //        PagedResultOutput<FornecedorListDto> result = null;



    //        try
    //        {
    //            using (UsingTenantId(1))
    //            {
    //                using (var context = LocalIocManager.Resolve<EZControlDbContext>())
    //                {



    //            //await UsingDbContext(async context => 
    //            //{
    //            //    await this.RunAsyncInTransaction(async (uow) =>
    //            //    {
    //                    var queryFornecedor = context.Fornecedores;
    //                    var queryPessoa = context.Pessoas;
    //                    var queryPessoaFisica = context.PessoasFisicas;
    //                    var queryPessoaJuridica = context.PessoasJuridicas;

    //                    var query = from fornecedor in queryFornecedor
    //                                join pessoa in queryPessoa on fornecedor.Pessoa.Id equals pessoa.Id
    //                                join pessoaFisica in queryPessoaFisica on pessoa.Id equals pessoaFisica.Id into pessoaFisicaGroup
    //                                from pf in pessoaFisicaGroup.DefaultIfEmpty()
    //                                join pessoaJuridica in queryPessoaJuridica on pessoa.Id equals pessoaJuridica.Id into pessoaJuridicaGroup
    //                                from pj in pessoaJuridicaGroup.DefaultIfEmpty()
    //                                select new
    //                                {
    //                                    fornecedor = fornecedor,
    //                                    pessoa = pessoa,
    //                                    pessoaFisica = pf,
    //                                    pessoaJuridica = pj
    //                                };

    //                    if (input != null)
    //                    {
    //                        if (!string.IsNullOrEmpty(input.Nome))
    //                            query = query.Where(x =>
    //                            x.pessoaFisica.Nome.Contains(input.Nome) ||
    //                            x.pessoaJuridica.NomeFantasia.Contains(input.Nome) ||
    //                            x.pessoaJuridica.RazaoSocial.Contains(input.Nome));

    //                        if (!string.IsNullOrEmpty(input.NumeroDocumento))
    //                            query = query.Where(x => x.pessoa.Documentos.Any(y => y.Numero == input.NumeroDocumento));

    //                        if (Enum.IsDefined(typeof(TipoDeDocumentoEnum), input.TipoDeDocumento))
    //                        {
    //                            query = query.Where(x => x.pessoa.Documentos.Any(y => y.TipoDeDocumento.TipoDeDocumentoFixo == input.TipoDeDocumento));
    //                        }
    //                    }

    //                    var count = await query.CountAsync();
    //                    var dados = await query
    //                        .OrderBy("fornecedor.Id")
    //                        .PageBy(input)
    //                        .ToListAsync();

    //                    List<FornecedorListDto> listDtos = new List<FornecedorListDto>();

    //                    foreach (var item in dados)
    //                    {
    //                        FornecedorListDto fornecedortListDto = item.fornecedor.MapTo<FornecedorListDto>();

    //                        listDtos.Add(fornecedortListDto);
    //                    }

    //                    result = new PagedResultOutput<FornecedorListDto>(count, listDtos);
    //                };                    
    //            };


    //            return result;
    //        }
    //        catch (Exception ex)
    //        {
    //            throw new UserFriendlyException(ex.Message);
    //        }
    //    }
    //}
}