﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Cliente;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa
{
    public class ClienteAppService_Tests : ClienteAppServiceTestBase
    {
        private readonly IGrupoPessoaAppService grupoPessoaAppService;

        public ClienteAppService_Tests()
        {
            grupoPessoaAppService = Resolve<IGrupoPessoaAppService>();
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private ClienteInput CreateClienteInput(
            int grupoPessoaId,
            string nome,
            string nomeFantasia,
            string razaoSocial,
            TipoPessoaEnum tipoPessoa,
            string observacao)
        {
            var input = new ClienteInput();

            input.Pessoa = new PessoaInput();
            input.Pessoa.GrupoPessoaId = grupoPessoaId;
            input.Pessoa.TipoPessoa = tipoPessoa;
            input.Pessoa.Observacao = observacao;

            if (tipoPessoa == TipoPessoaEnum.Fisica)
            {
                input.PessoaFisica = new PessoaFisicaInputAux();
                input.PessoaFisica.Nome = nome;
            }
            else
            {
                input.PessoaJuridica = new PessoaJuridicaInputAux();
                input.PessoaJuridica.NomeFantasia = nomeFantasia;
                input.PessoaJuridica.RazaoSocial = razaoSocial;
            }

            return input;
        }

        public async Task<ClientePessoaIdDto> SaveCliente(
            int grupoPessoaId,
            string nome,
            string nomeFantasia,
            string razaoSocial,
            TipoPessoaEnum tipoPessoa,
            string observacao)
        {
            var input = CreateClienteInput(grupoPessoaId, nome, nomeFantasia, razaoSocial, tipoPessoa, observacao);
            var cliente = await ClienteAppService.Save(input);
            return cliente;
        }

        [Fact]
        public async Task ShouldCreateCliente()
        {
            ClientePessoaIdDto clienteId = null;

            await RunAsyncInTransaction(async (uow) =>
            {
                var grupoPessoa = await this.CreateGrupoPessoa();
                clienteId = await SaveCliente(grupoPessoa.Id, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");
            });

            clienteId.ClienteId.ShouldBe(1);
        }

        [Fact]
        public async Task ShouldCreateClienteAndGetClientesPaginado()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var grupoPessoa = await this.CreateGrupoPessoa();
                await SaveCliente(grupoPessoa.Id, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");

                GetClienteInput clienteInput = new GetClienteInput
                {
                    SkipCount = 0,
                    MaxResultCount = 10,
                    Sorting = "Id",
                    Nome = "Light"
                };

                var result = await ClienteAppService.GetClientesPaginado(clienteInput);

                result.Items.Count.ShouldBe(1);
            });
        }

        [Fact]
        public async Task Delete_Cliente()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var grupoPessoa = await this.CreateGrupoPessoa();
                var dto = await SaveCliente(grupoPessoa.Id, "", "Light", "Light Ltda", TipoPessoaEnum.Juridica, "Alguma observacao");
                await ClienteAppService.Delete(new IdInput(dto.ClienteId));
                await uow.Current.SaveChangesAsync();
                await ClienteAppService.GetById(new IdInput(dto.ClienteId)).ShouldThrowAsync<UserFriendlyException>();
            });
        }

        private async Task<GrupoPessoaInput> CreateGrupoPessoa()
        {
            var grupo = new GrupoPessoaInput
            {
                IsActive = true,
                Descricao = "Grupo Administrativo",
                Nome = "Grupo Administrativo"
            };

            return await grupoPessoaAppService.SaveAndReturnEntity(grupo);
        }
    }
}