﻿using Abp.Application.Services.Dto;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa.GrupoPessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.ClienteEZ;
using EZ.EZControl.Dto.Global.SubtiposPessoa.Empresa;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.Migrations.Seed.Tenants;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using EZ.EZControl.Services.Global.SubtiposPessoa.Interfaces;
using EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa
{
    public class EmpresaAppService_Tests : EmpresaAppServiceTestBase
    {
        private readonly IGrupoPessoaAppService grupoPessoaAppService;
        private readonly IClienteEZAppService clienteEzAppService;

        public EmpresaAppService_Tests()
        {
            clienteEzAppService = Resolve<IClienteEZAppService>();
            grupoPessoaAppService = Resolve<IGrupoPessoaAppService>();

            UsingDbContext(context =>
            {
                var ezSeedDevelopment = new EzSeedEZLiv(context, 1);
                var tenantEmpresaBuilder = new TenantEmpresaBuilder(context, 1);
                tenantEmpresaBuilder.CreateEmpresaExemplo();
                ezSeedDevelopment.UpdateAdminUserPermissions();
            });
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private EmpresaInput CreateEmpresaInput(
            string nome,
            string razaoSocial)
        {
            var input = new EmpresaInput
            {
                NomeFantasia = nome,
                RazaoSocial = razaoSocial
            };

            return input;
        }

        private async Task<GrupoPessoaInput> CreateGrupoPessoa()
        {
            var grupo = new GrupoPessoaInput
            {
                IsActive = true,
                Descricao = "Grupo Administrativo",
                Nome = "Grupo Administrativo"
            };

            return await grupoPessoaAppService.SaveAndReturnEntity(grupo);
        }

        private async Task<EmpresaPessoaIdDto> SaveEmpresa(
            string nomeFantasia,
            string razaoSocial)
        {
            var input = CreateEmpresaInput(nomeFantasia, razaoSocial);
            var empresa = await EmpresaAppService.Save(input);
            return empresa;
        }

        [Fact]
        public async Task ShouldCreateEmpresa()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await SaveEmpresa("Light", "Light Ltda");
            });
        }

        [Fact]
        public async Task ShouldCreateEmpresaAndGetEmpresasPaginado()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                await SaveEmpresa("Light", "Light Ltda");

                GetEmpresaInput empresaInput = new GetEmpresaInput
                {
                    SkipCount = 0,
                    MaxResultCount = 10,
                    Sorting = "Id",
                    NomeFantasia = "Light"
                };

                var result = await EmpresaAppService.GetEmpresasPaginado(empresaInput);

                result.Items.Count.ShouldBe(1);
            });
        }

        [Fact]
        public async Task Delete_Empresa()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var dto = await SaveEmpresa("Light", "Light Ltda");
                await EmpresaAppService.Delete(new IdInput(dto.EmpresaId));
                await uow.Current.SaveChangesAsync();
                await EmpresaAppService.GetById(new IdInput(dto.EmpresaId)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}