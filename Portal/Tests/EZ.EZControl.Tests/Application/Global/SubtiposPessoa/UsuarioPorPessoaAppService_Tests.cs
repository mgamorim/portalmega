﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Net.Mail;
using Abp.UI;
using EZ.EZControl.Authorization.Users.Dto;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Dto.Global.Pessoa;
using EZ.EZControl.Dto.Global.SubtiposPessoa.UsuarioPorPessoa;
using EZ.EZControl.Tests.Application.Global.SubtiposPessoa.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.SubtiposPessoa
{
    public class UsuarioPorPessoaAppService_Tests : UsuarioPorPessoaAppServiceTestBase
    {
        private readonly IEmailSender _emailSender;
        private readonly IRepository<Documento> _documentoRepository;
        private readonly IRepository<TipoDeDocumento> _tipoDeDocumentoRepository;
        private readonly IRepository<EZControl.Domain.Global.Pessoa.Pessoa> _pessoaRepository;
        private readonly IRepository<EnderecoEletronico> _enderecoEletronicoRepository;

        public UsuarioPorPessoaAppService_Tests()
        {
            _emailSender = Resolve<IEmailSender>();
            _documentoRepository = Resolve<IRepository<Documento>>();
            _tipoDeDocumentoRepository = Resolve<IRepository<TipoDeDocumento>>();
            _pessoaRepository = Resolve<IRepository<EZControl.Domain.Global.Pessoa.Pessoa>>();
            _enderecoEletronicoRepository = Resolve<IRepository<EnderecoEletronico>>();
        }

        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private TipoDeDocumento CreateTipoDeDocumento(string descricao, string mascara, TipoDeDocumentoEnum tipoDeDocumentoFixo)
        {
            return new TipoDeDocumento
            {
                IsActive = true,
                TipoPessoa = TipoPessoaEnum.Fisica,
                Descricao = descricao,
                TipoDeDocumentoFixo = tipoDeDocumentoFixo,
                Mascara = mascara
            };
        }

        private EnderecoEletronico CreateEnderecoEletronico(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeEnderecoEletronicoEnum tipoDeEnderecoEletronico, string endereco)
        {
            return new EnderecoEletronico(pessoa, tipoDeEnderecoEletronico, endereco) { EmpresaId = EmpresaIdDefaultTest };
        }

        private Documento CreateDocumento(EZControl.Domain.Global.Pessoa.Pessoa pessoa, TipoDeDocumento tipoDeDocumento, string numero = null, DateTime? dataDeExpedicao = null, string orgao = null)
        {
            return new Documento(pessoa, tipoDeDocumento, numero, orgao, dataDeExpedicao) { EmpresaId = EmpresaIdDefaultTest };
        }

        private async Task<UsuarioPorPessoaInput> CreateUsuarioPorPessoa(string name, string surname, string email, string password, string nomeCompleto, string roleName, TipoDeUsuarioEnum tipoDeUsuario)
        {
            var pf = await PessoaFisicaAppService.SaveAndReturnEntity(new PessoaFisicaInput
            {
                Nome = nomeCompleto,
                IsActive = true,
                Nacionalidade = "Brasileiro",
                DataDeNascimento = DateTime.Now.AddYears(-30),
                Sexo = SexoEnum.Masculino,
                TipoPessoa = TipoPessoaEnum.Fisica,
                EstadoCivil = EstadoCivilEnum.Casado
            });

            var pessoa = _pessoaRepository.Get(pf.Id);

            var tipoDeDocumentoCpf = this.CreateTipoDeDocumento("CPF", "999.9999.999-99", TipoDeDocumentoEnum.Cpf);
            var tipoDeDocumentoRg = this.CreateTipoDeDocumento("RG", "99.9999.999-9", TipoDeDocumentoEnum.Rg);

            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoCpf);
            _tipoDeDocumentoRepository.InsertOrUpdate(tipoDeDocumentoRg);

            var documentoCpf = this.CreateDocumento(pessoa, tipoDeDocumentoCpf, "67515814675");
            var documentoRg = this.CreateDocumento(pessoa, tipoDeDocumentoRg, "283068668", DateTime.Now, "DETRAN-RJ");

            _documentoRepository.InsertOrUpdate(documentoCpf);
            _documentoRepository.InsertOrUpdate(documentoRg);

            var enderecoEletronico = this.CreateEnderecoEletronico(pessoa, TipoDeEnderecoEletronicoEnum.EmailPrincipal, "rmarcondes@gmail.com");
            _enderecoEletronicoRepository.InsertOrUpdate(enderecoEletronico);

            return new UsuarioPorPessoaInput
            {
                RoleName = roleName,
                User = new UserEditDto
                {
                    Name = name,
                    Surname = surname,
                    EmailAddress = email,
                    Password = password,
                    TipoDeUsuario = tipoDeUsuario,
                    UserName = email,
                    PessoaFisica = pf
                }
            };
        }

        [Fact]
        public async Task Should_Create_UsuarioPorPessoa()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var usuarioPorPessoa = await CreateUsuarioPorPessoa("Rogério", "Marcondes", "rmarcondes@gmail.com", Guid.NewGuid().ToString("N").Truncate(16), "Rogério da Silva Marcondes", "Corretor", TipoDeUsuarioEnum.Corretor);
                await uow.Current.SaveChangesAsync();
                await base.UsuarioPorPessoaAppService.Save(usuarioPorPessoa);
            });
        }

        [Fact]
        public async Task Delete_UsuarioPorPessoa()
        {
            await RunAsyncInTransaction(async (uow) =>
            {
                var usuarioPorPessoa = await CreateUsuarioPorPessoa("João", "Júnior", "sjunior@gmail.com", Guid.NewGuid().ToString("N").Truncate(16), "João Silva Júnior", "Corretor", TipoDeUsuarioEnum.Corretor);
                await uow.Current.SaveChangesAsync();
                var usuarioId = await base.UsuarioPorPessoaAppService.Save(usuarioPorPessoa);
                await base.UsuarioPorPessoaAppService.Delete(new IdInput(usuarioId.UsuarioPorPessoaId));
                await uow.Current.SaveChangesAsync();
                await base.UsuarioPorPessoaAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
            });
        }
    }
}