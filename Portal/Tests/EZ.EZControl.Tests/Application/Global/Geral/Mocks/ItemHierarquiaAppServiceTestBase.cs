﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class ItemHierarquiaAppServiceTestBase : AppTestBase
    {
        protected readonly IItemHierarquiaAppService ItemHierarquiaAppService;

        public ItemHierarquiaAppServiceTestBase()
        {
            ItemHierarquiaAppService = Resolve<IItemHierarquiaAppService>();
        }

        protected void CreateTestItensHierarquia()
        {
            UsingDbContext(context =>
            {
                context.ItensHierarquia.Add(CreateItemHierarquiaEntity("001", "9.9", "Gerência de Atendimento", null));
                context.ItensHierarquia.Add(CreateItemHierarquiaEntity("002", "9.9", "Gerência de Desenvolvimento", null));
            });
        }

        private ItemHierarquia CreateItemHierarquiaEntity(string codigo, string mascara, string nome, ItemHierarquia itemHierarquiaPai)
        {
            return new ItemHierarquia()
            {
                Codigo = codigo,
                Nome = nome,
                Mascara = mascara,
                ItemHierarquiaPai = itemHierarquiaPai,
                TenantId = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}