﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class EmailAppServiceTestBase : AppTestBase
    {
        public readonly IEmailAppService emailAppService;
        public readonly IConexaoSMTPAppService smtpAppService;
        public readonly ITemplateEmailAppService templateEmailAppService;
        public readonly IParametroGlobalAppService parametroAppService;

        public EmailAppServiceTestBase()
        {
            this.emailAppService = Resolve<IEmailAppService>();
            this.smtpAppService = Resolve<IConexaoSMTPAppService>();
            this.templateEmailAppService = Resolve<ITemplateEmailAppService>();
            this.parametroAppService = Resolve<IParametroGlobalAppService>();
        }

        protected void CreateTestSMTP()
        {
            UsingDbContext(context =>
            {
                context.ConexoesSMTP.Add(CreateSMTPEntity("smtppadrao@ezsoft.com.br", "SMTP EZSoft", 587, "smtppadrao@ezsoft.com.br", "Senha123", "smtp.uni5.net"));
            });
        }

        protected void CreateTestTemplateEmail()
        {
            UsingDbContext(context =>
            {
                context.TemplatesEmail.Add(CreateTemplateEntity("E-mail de Teste", "E-mail de Teste", "Estamos enviando esse e-mail para Testes, não responda!", "smtppadrao@ezsoft.com.br"));
            });
        }

        protected void CreateTestParametro()
        {
            UsingDbContext(context =>
            {
                context.ParametrosGlobal.Add(CreateParametroEntity());
            });
        }

        private ConexaoSMTP CreateSMTPEntity(String enderecoPadrao, String nome, Int32 porta, String usuario, String senha, String servidor)
        {
            return new ConexaoSMTP
            {
                IsActive = true,
                EnderecoDeEmailPadrao = enderecoPadrao,
                Nome = nome,
                Porta = porta,
                RequerAutenticacao = true,
                RequerConexaoCriptografada = true,
                UsuarioAutenticacao = usuario,
                SenhaAutenticacao = senha,
                ServidorSMTP = servidor,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private TemplateEmail CreateTemplateEntity(String assunto, String nome, String corpoMensagem, String remetente)
        {
            return new TemplateEmail
            {
                IsActive = true,
                Assunto = assunto,
                Nome = nome,
                CorpoMensagem = corpoMensagem,
                Remetente = remetente,
                TipoDeTemplateDeMensagem = TipoDeTemplateDeMensagemEnum.BoasVindas,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private ParametroGlobal CreateParametroEntity()
        {
            return new ParametroGlobal
            {
                IsActive = true,
                ConexaoSMTP =
                    CreateSMTPEntity("smtppadrao@ezsoft.com.br", "SMTP AGF", 587, "smtppadrao@ezsoft.com.br", "Senha123",
                        "smtp.uni5.net"),
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}