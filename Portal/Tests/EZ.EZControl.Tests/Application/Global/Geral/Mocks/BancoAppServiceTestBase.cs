﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public abstract class BancoAppServiceTestBase : AppTestBase
    {
        protected readonly IBancoAppService BancoAppService;

        public BancoAppServiceTestBase()
        {
            BancoAppService = Resolve<IBancoAppService>();
        }

        protected void CreateTestBancos()
        {
            UsingDbContext(context =>
            {
                context.Bancos.Add(CreateBancoEntity("001", "Banco do Brasil"));
                context.Bancos.Add(CreateBancoEntity("033", "Banco Santander"));
                context.Bancos.Add(CreateBancoEntity("237", "Banco Bradesco"));
                context.Bancos.Add(CreateBancoEntity("341", "Banco Itaú"));
            });
        }

        private Banco CreateBancoEntity(string codigo, string nome)
        {
            return new Banco()
            {
                Codigo = codigo,
                Nome = nome,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}