﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class ArquivoGlobalAppServiceTestBase : AppTestBase
    {
        protected readonly IArquivoGlobalAppService ArquivoAppService;

        public ArquivoGlobalAppServiceTestBase()
        {
            this.ArquivoAppService = Resolve<IArquivoGlobalAppService>();
        }

        protected void CreateTestArquivos()
        {
            UsingDbContext(context =>
            {
                context.ArquivosGlobal.Add(CreateArquivo("Recibo", TipoDeArquivoEnum.PDF, new Byte[0]));
                context.ArquivosGlobal.Add(CreateArquivo("Despesa", TipoDeArquivoEnum.JPG, new Byte[1]));
                context.ArquivosGlobal.Add(CreateArquivo("Planilha Contábil", TipoDeArquivoEnum.XLS, new Byte[2]));
            });
        }

        private ArquivoGlobal CreateArquivo(String nome, TipoDeArquivoEnum tipoDeArquivo, Byte[] conteudo)
        {
            return new ArquivoGlobal
            {
                IsActive = true,
                Nome = nome,
                TipoDeArquivoFixo = tipoDeArquivo,
                Conteudo = conteudo,
                Path = @"C:\Uploads\Global",
                Token = Guid.NewGuid().ToString("N")
                //EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}