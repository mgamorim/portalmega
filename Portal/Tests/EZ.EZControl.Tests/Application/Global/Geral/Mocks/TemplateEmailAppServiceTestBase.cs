﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class TemplateEmailAppServiceTestBase : AppTestBase
    {
        public readonly ITemplateEmailAppService TemplateEmailAppService;

        public TemplateEmailAppServiceTestBase()
        {
            this.TemplateEmailAppService = Resolve<ITemplateEmailAppService>();
        }

        protected void CreateTestTemplatesEmail()
        {
            UsingDbContext(context =>
            {
                context.TemplatesEmail.Add(CreateTemplateEmail("Casas Bahia", "Promoções Casas Bahia", "Casas Bahia", "no-reply@casasbahia.com"));
                context.TemplatesEmail.Add(CreateTemplateEmail("Ricardo Eletro", "Saldão Ricardo", "Ricardo Eletro", "no-reply@ricardoeletro.com.br"));
            });
        }

        private TemplateEmail CreateTemplateEmail(String assunto, String corpoMensagem, String nome, String remetente)
        {
            return new TemplateEmail
            {
                Assunto = assunto,
                CorpoMensagem = corpoMensagem,
                Nome = nome,
                Remetente = remetente,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}