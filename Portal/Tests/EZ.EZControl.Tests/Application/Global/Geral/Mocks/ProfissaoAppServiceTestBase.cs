﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class ProfissaoAppServiceTestBase : AppTestBase
    {
        protected readonly IProfissaoAppService ProfissaoAppService;

        public ProfissaoAppServiceTestBase()
        {
            ProfissaoAppService = Resolve<IProfissaoAppService>();
        }

        protected void CreateTestProfissoes()
        {
            UsingDbContext(context =>
            {
                context.Profissoes.Add(CreateEntityProfissao("001", "Analista de Sistemas"));
                context.Profissoes.Add(CreateEntityProfissao("002", "Analista de Testes"));
            });
        }

        private Profissao CreateEntityProfissao(string codigo, string titulo)
        {
            return new Profissao()
            {
                Codigo = codigo,
                Titulo = titulo
            };
        }
    }
}