﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class RamoDoFornecedorAppServiceTestBase : AppTestBase
    {
        protected readonly IRamoDoFornecedorAppService RamoDoFornecedorAppService;

        public RamoDoFornecedorAppServiceTestBase()
        {
            this.RamoDoFornecedorAppService = Resolve<IRamoDoFornecedorAppService>();
        }

        protected void CreateTestRamosDosFornecedores()
        {
            UsingDbContext(context =>
            {
                context.RamosDoFornecedor.Add(CreateRamoDoFornecedor("Alimentos e Bebidas"));
                context.RamosDoFornecedor.Add(CreateRamoDoFornecedor("Prestação de Serviços"));
                context.RamosDoFornecedor.Add(CreateRamoDoFornecedor("Imóveis"));
            });
        }

        private RamoDoFornecedor CreateRamoDoFornecedor(String nome)
        {
            return new RamoDoFornecedor
            {
                IsActive = true,
                Nome = nome,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}