﻿using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Services.Global.Localidade.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class FeriadoAppServiceTestBase : AppTestBase
    {
        protected readonly IFeriadoAppService FeriadoAppService;
        protected readonly IPaisAppService PaisAppService;
        protected readonly IEstadoAppService EstadoAppService;
        protected readonly ICidadeAppService CidadeAppService;

        public FeriadoAppServiceTestBase()
        {
            FeriadoAppService = Resolve<IFeriadoAppService>();
            PaisAppService = Resolve<IPaisAppService>();
            EstadoAppService = Resolve<IEstadoAppService>();
            CidadeAppService = Resolve<ICidadeAppService>();
        }

        protected void CreateTestFeriados()
        {
            UsingDbContext(context =>
            {
                var pais = context.Paises.Add(CreatePaisEntity("Brasil", "Brasileira", "pt-BR"));
                context.Feriados.Add(
                    CreateFeriadoEntity(
                        AbrangenciaDoFeriadoEnum.Nacional,
                        new DateTime(2016, 9, 7),
                        "Independência do Brasil",
                        TipoDeFeriadoEnum.Ordinario,
                        null,
                        null,
                        pais
                    )
                );
                context.Feriados.Add(
                    CreateFeriadoEntity(
                        AbrangenciaDoFeriadoEnum.Nacional,
                        new DateTime(2016, 4, 21),
                        "Descobrimento do Brasil",
                        TipoDeFeriadoEnum.Ordinario,
                        null,
                        null,
                        pais
                    )
                );
                var estado = context.Estados.Add(CreateEstadoEntity("Rio de Janeiro", "Rio de Janeiro", "RJ", "RJ", pais));
                var cidade = context.Cidades.Add(CreateCidadeEntity("Rio de Janeiro", "RJ", estado));
                context.Feriados.Add(
                    CreateFeriadoEntity(
                        AbrangenciaDoFeriadoEnum.Municipal,
                        new DateTime(2016, 11, 21),
                        "Consciência Negra",
                        TipoDeFeriadoEnum.Ordinario,
                        cidade,
                        estado,
                        pais
                    )
                );
            });
        }

        private Cidade CreateCidadeEntity(string nome, string codigoIBGE, Estado estado)
        {
            return new Cidade()
            {
                CodigoIBGE = codigoIBGE,
                Estado = estado,
                Nome = nome
            };
        }

        private Estado CreateEstadoEntity(string capital, string nome, string sigla, string codigoIBGE, Pais pais)
        {
            return new Estado()
            {
                Capital = capital,
                Nome = nome,
                Sigla = sigla,
                CodigoIBGE = codigoIBGE,
                Pais = pais
            };
        }

        protected void CreateTestPais()
        {
            UsingDbContext(context =>
            {
                context.Paises.Add(CreatePaisEntity("Brasil", "Brasileira", "pt-BR"));
            });
        }

        private Feriado CreateFeriadoEntity(AbrangenciaDoFeriadoEnum abrangencia, DateTime data, string nome, TipoDeFeriadoEnum tipo, Cidade cidade, Estado estado, Pais pais)
        {
            return new Feriado()
            {
                Abrangencia = abrangencia,
                Data = data,
                Nome = nome,
                Pais = pais,
                Tipo = tipo,
                Cidade = cidade,
                Estado = estado,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Pais CreatePaisEntity(string nome, string nacionalidade, string codigoISO)
        {
            return new Pais()
            {
                CodigoISO = codigoISO,
                Nacionalidade = nacionalidade,
                Nome = nome
            };
        }
    }
}