﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class AgenciaAppServiceTestBase : AppTestBase
    {
        protected readonly IAgenciaAppService AgenciaAppService;
        protected readonly IBancoAppService BancoAppService;

        public AgenciaAppServiceTestBase()
        {
            AgenciaAppService = Resolve<IAgenciaAppService>();
            BancoAppService = Resolve<IBancoAppService>();
        }

        protected void CreateTestAgencias()
        {
            UsingDbContext(context =>
            {
                context.Agencias.Add(
                    CreateAgenciaEntity(
                        CreateBancoEntity("033", "Banco Santander S/A"),
                        "Rio Branco", "3003", string.Empty));
                context.Agencias.Add(
                    CreateAgenciaEntity(
                        CreateBancoEntity("341", "Banco Itaú S/A"),
                        "Via Brasil", "6526", string.Empty));
            });
        }

        protected void CreateTestBancos()
        {
            UsingDbContext(context =>
            {
                context.Bancos.Add(CreateBancoEntity("033", "Banco Santander S/A"));
                context.Bancos.Add(CreateBancoEntity("341", "Banco Itaú S/A"));
            });
        }

        private Agencia CreateAgenciaEntity(Banco banco, string nome, string codigo, string digitoVerificador)
        {
            return new Agencia()
            {
                Banco = banco,
                Nome = nome,
                Codigo = codigo,
                DigitoVerificador = digitoVerificador,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Banco CreateBancoEntity(string codigo, string nome)
        {
            return new Banco()
            {
                Codigo = codigo,
                Nome = nome,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}