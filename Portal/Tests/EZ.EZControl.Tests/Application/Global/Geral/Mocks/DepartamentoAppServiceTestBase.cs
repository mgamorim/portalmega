﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using System;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class DepartamentoAppServiceTestBase : AppTestBase
    {
        protected readonly IDepartamentoAppService DepartamentoAppService;

        public DepartamentoAppServiceTestBase()
        {
            this.DepartamentoAppService = Resolve<IDepartamentoAppService>();
        }

        protected void CreateTestDepartamentos()
        {
            UsingDbContext(context =>
            {
                context.Departamentos.Add(CreateDepartamento("RH", "Departamento de Recursos Humanos"));
                context.Departamentos.Add(CreateDepartamento("Desenvolvimento", "Departamento de Desenvolvimento"));
                context.Departamentos.Add(CreateDepartamento("Suporte", "Departamento de Suporte ao Cliente"));
            });
        }

        private Departamento CreateDepartamento(String nome, String descricao)
        {
            return new Departamento
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}