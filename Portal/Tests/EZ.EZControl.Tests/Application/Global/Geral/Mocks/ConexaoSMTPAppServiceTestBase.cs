﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public class ConexaoSMTPAppServiceTestBase : AppTestBase
    {
        protected readonly IConexaoSMTPAppService ConexaoSMTPAppService;

        public ConexaoSMTPAppServiceTestBase()
        {
            ConexaoSMTPAppService = Resolve<IConexaoSMTPAppService>();
        }

        protected void CreateTestConexoesSMTP()
        {
            UsingDbContext(context =>
            {
                context.ConexoesSMTP.Add(CreateConexaoSMTPEntity("GMail", 587, "gmailteste", "smtp.gmail.com", true, true, "romariz@gmail.com"));
                context.ConexoesSMTP.Add(CreateConexaoSMTPEntity("Outlook", 587, "outlookteste", "smtp.outlook.com", true, true, "fabioromariz@hotmail.com"));
            });
        }

        private ConexaoSMTP CreateConexaoSMTPEntity(string nome, int porta, string senhaAutenticacao, string servidorSMTP, bool requerAutenticacao, bool requerConexaoCriptografada, string usuarioAutenticacao)
        {
            return new ConexaoSMTP()
            {
                Nome = nome,
                Porta = porta,
                SenhaAutenticacao = senhaAutenticacao,
                ServidorSMTP = servidorSMTP,
                RequerAutenticacao = requerAutenticacao,
                RequerConexaoCriptografada = requerConexaoCriptografada,
                UsuarioAutenticacao = usuarioAutenticacao,
                IsActive = true,
                EnderecoDeEmailPadrao = usuarioAutenticacao,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}