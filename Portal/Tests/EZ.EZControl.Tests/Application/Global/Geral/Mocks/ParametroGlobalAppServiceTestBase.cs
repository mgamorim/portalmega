﻿using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Services.Global.Geral.Interfaces;

namespace EZ.EZControl.Tests.Application.Global.Geral.Mocks
{
    public abstract class ParametroGlobalAppServiceTestBase : AppTestBase
    {
        protected readonly IParametroGlobalAppService ParametroGlobalAppService;

        protected ParametroGlobalAppServiceTestBase()
        {
            ParametroGlobalAppService = Resolve<IParametroGlobalAppService>();
        }

        protected void CreateTestParametros()
        {
            UsingDbContext(context =>
            {
                context.ParametrosGlobal.Add(CreateParametroEntity());
            });
        }

        private ParametroGlobal CreateParametroEntity()
        {
            return new ParametroGlobal
            {
                ConexaoSMTP = new ConexaoSMTP
                {
                    IsActive = true,
                    Nome = "Conexão SMTP",
                    EnderecoDeEmailPadrao = "smtppadrao@ezsoft.com.br",
                    ServidorSMTP = "smtp.uni5.net",
                    Porta = 587,
                    RequerAutenticacao = true,
                    UsuarioAutenticacao = "smtppadrao@ezsoft.com.br",
                    SenhaAutenticacao = "Senha123",
                    RequerConexaoCriptografada = true,
                    EmpresaId = EmpresaIdDefaultTest
                },
                AlturaMaximaPx = 100,
                ExtensoesImagem = "jpg;png;gif",
                ExtensoesDocumento = "doc;docx;pdf;xlsx",
                LarguraMaximaPx = 260,
                TamanhoMaximoMb = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}