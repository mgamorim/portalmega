﻿using EZ.EZControl.Dto.Global.Geral.Parametro;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class ParametroGlobalAppService_Tests : ParametroGlobalAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Parametro()
        {
            await CreateParametrosAsync();
        }

        private async Task CreateParametrosAsync()
        {
            base.CreateTestParametros();

            await ParametroGlobalAppService.Save(new ParametroGlobalInput { ConexaoSMTPId = 1 });

            await UsingDbContext(async context =>
            {
                var createdParametro = await context.ParametrosGlobal.FirstOrDefaultAsync();
                createdParametro.ShouldNotBe(null);
            });
        }
    }
}