﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Agencia;
using EZ.EZControl.Dto.Global.Geral.Banco;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class AgenciaAppService_Tests : AgenciaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateAgenciaAndTestAsync(Banco banco, string nome, string codigo, string digitoVerificador)
        {
            var _bancos = await BancoAppService.GetBancosPaginado(new GetBancoInput() { Codigo = banco.Codigo });
            var _banco = _bancos.Items[0];
            await AgenciaAppService.Save(
                new AgenciaInput()
                {
                    BancoId = _banco.Id,
                    Nome = nome,
                    Codigo = codigo,
                    DigitoVerificador = digitoVerificador
                }
            );
        }

        [Fact]
        public async Task Should_Create_Agencia()
        {
            CreateTestBancos();
            await CreateAgenciaAndTestAsync(new Banco() { Codigo = "033" }, "Rio Branco", "3003", string.Empty);
            await CreateAgenciaAndTestAsync(new Banco() { Codigo = "341" }, "Via Brasil", "6526", string.Empty);
            await CreateAgenciaAndTestAsync(new Banco() { Codigo = "341" }, "Downtown", "23791", string.Empty);
        }

        [Fact]
        public async Task Should_Not_Create_Agencia_With_Duplicate_Codigo()
        {
            CreateTestAgencias();
            var bancos = await BancoAppService.GetBancosPaginado(new GetBancoInput() { Codigo = "033" });
            var banco = bancos.Items[0];
            await AgenciaAppService.Save(new AgenciaInput()
            {
                BancoId = banco.Id,
                Codigo = "3003",
                Nome = "Rio Preto",
                DigitoVerificador = string.Empty
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task GetAgenciaByBanco()
        {
            CreateTestAgencias();
            var bancos = await BancoAppService.GetBancosPaginado(new GetBancoInput() { Codigo = "033" });
            var banco = bancos.Items[0];
            var agencia = await AgenciaAppService.GetAgenciaByBanco(new IdInput(banco.Id));
            agencia.ShouldNotBe(null);
        }

        [Fact]
        public async Task Delete_Agencia()
        {
            CreateTestAgencias();
            await AgenciaAppService.Delete(new IdInput(1));
            await AgenciaAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}