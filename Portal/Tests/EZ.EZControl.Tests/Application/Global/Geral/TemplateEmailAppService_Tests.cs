﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Geral.TemplateEmail;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class TemplateEmailAppService_Tests : TemplateEmailAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateTemplateEmailAndTestAsync(String assunto, String corpoMensagem, String nome, String remetente)
        {
            await base.TemplateEmailAppService
                    .Save(new TemplateEmailInput
                    {
                        IsActive = true,
                        Nome = nome,
                        Assunto = assunto,
                        CorpoMensagem = corpoMensagem,
                        Remetente = remetente,
                    });
        }

        [Fact]
        public async Task Should_Create_TemplateEmail()
        {
            await CreateTemplateEmailAndTestAsync(
            "E-mail Marketing GMail",
            "E-mail Marketing",
            "GMail Google",
            "no-replay@gmail.com");

            await CreateTemplateEmailAndTestAsync(
            "E-mail Marketing Outlook",
            "E-mail Marketing",
            "Outlook Microsoft",
            "no-replay@outlook.com");
        }

        [Fact]
        public async Task Should_Not_Create_TemplateEmail_With_Duplicate_Nome()
        {
            base.CreateTestTemplatesEmail();
            await base.TemplateEmailAppService.Save(
                new TemplateEmailInput
                {
                    Nome = "Casas Bahia",
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Template()
        {
            base.CreateTestTemplatesEmail();
            await base.TemplateEmailAppService.Delete(new IdInput(1));
            await base.TemplateEmailAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}