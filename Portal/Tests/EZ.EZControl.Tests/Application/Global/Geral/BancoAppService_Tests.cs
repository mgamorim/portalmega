﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.Banco;
using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class BancoAppService_Tests : BancoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        [Fact]
        public async Task Should_Create_Banco()
        {
            await CreateBancoAndTestAsync("001", "Banco do Brasil");
            await CreateBancoAndTestAsync("033", "Banco Santander");
            await CreateBancoAndTestAsync("237", "Banco Bradesco");
            await CreateBancoAndTestAsync("341", "Banco Itaú");
        }

        [Fact]
        public async Task Should_Not_Create_Banco_With_Duplicate_Codigo()
        {
            CreateTestBancos();
            await BancoAppService.Save(
                new BancoInput()
                {
                    Codigo = "001",
                    Nome = "Banco Alterado"
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Not_Create_Banco_With_Duplicate_Nome()
        {
            CreateTestBancos();
            await BancoAppService.Save(
                new BancoInput()
                {
                    Codigo = "002",
                    Nome = "Banco do Brasil"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        private async Task CreateBancoAndTestAsync(string codigo, string nome)
        {
            await BancoAppService.Save(
                new BancoInput()
                {
                    Codigo = codigo,
                    Nome = nome
                });
            await UsingDbContext(async context =>
            {
                var createdBanco = await context.Bancos.FirstOrDefaultAsync(b => b.Codigo == codigo);
                createdBanco.ShouldNotBe(null);
            });
        }

        [Fact]
        public void Should_BulkInsert()
        {
            var bancos = new List<Banco>()
            {
                new Banco() {Codigo = "100", IsActive = true, Nome = "Banco1"},
                new Banco() {Codigo = "200", IsActive = true, Nome = "Banco2"},
                new Banco() {Codigo = "300", IsActive = true, Nome = "Banco3"}
            };

            IBancoService bancoService = this.Resolve<IBancoService>();

            this.RunInTransaction((uow) =>
            {
                bancoService.Insert(bancos);
                uow.Current.SaveChanges();
                bancoService.GetAll().ToList().Count.ShouldBe(3);
            }, true);
        }
    }
}