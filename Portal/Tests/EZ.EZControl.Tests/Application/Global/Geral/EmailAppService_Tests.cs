﻿using EZ.EZControl.Services.Global.Geral.Interfaces;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class EmailAppService_Tests : EmailAppServiceTestBase
    {
        private readonly ITemplateEmailAppService templateEmailService;

        public EmailAppService_Tests()
        {
            this.templateEmailService = Resolve<ITemplateEmailAppService>();
        }

        //Classe de Teste comentada, pois a configuração SMTP só está funcionando local.

        //[Fact]
        //public async Task SendEmail()
        //{
        //    var email = new EmailDto
        //    {
        //        Destinatario = "afaustino10@gmail.com",
        //        smtpDto = new ConexaoSMTPDto
        //        {
        //            ServidorSMTP = "smtp.uni5.net",
        //            Porta = 587,
        //            RequerAutenticacao = true,
        //            UsuarioAutenticacao = "smtppadrao@ezsoft.com.br",
        //            SenhaAutenticacao = "Senha123",
        //            RequerConexaoCriptografada = true
        //        },
        //        templateDeEmail = new TemplateEmailDto
        //        {
        //            Assunto = "Teste",
        //            Remetente = "smtppadrao@ezsoft.com.br",
        //            CorpoMensagem = "E-mail de Teste!",
        //        }
        //    };

        //    await this.emailAppService.SendEmail(email);
        //}

        //[Fact]
        //public async Task SendEmailSmtp()
        //{
        //    base.CreateTestSMTP();
        //    base.CreateTestTemplateEmail();

        //    var destinatario = "afaustino10@gmail.com";
        //    var smtp = await base.smtpAppService.GetById(new IdInput(1));
        //    var template = await base.templateEmailAppService.GetById(new IdInput(1));

        //    await this.emailAppService.SendEmailSmtp(destinatario, smtp.Id, template.Id);
        //}

        //[Fact]
        //public async Task SendEmailByParameter()
        //{
        //    base.CreateTestParametro();
        //    base.CreateTestTemplateEmail();

        //    var destinatario = "afaustino10@gmail.com";
        //    var parametro = await base.parametroAppService.Get();
        //    var template = await base.templateEmailAppService.GetById(new IdInput(1));

        //    await this.emailAppService.SendEmailParameter(destinatario, parametro.Id, template.Id);
        //}
    }
}