﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Geral.Departamento;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class DepartamentoAppService_Tests : DepartamentoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateDepartamentoAndTestAsync(String nome, String descricao)
        {
            await base.DepartamentoAppService.Save(new DepartamentoInput()
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao
            });
            await UsingDbContext(async context =>
            {
                var createdDepartamento = await context.Departamentos.FirstOrDefaultAsync(x => x.Nome == nome);
                createdDepartamento.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Departamento()
        {
            await CreateDepartamentoAndTestAsync("TI", "Departamento de Infraestrutura e TI");
        }

        [Fact]
        public async Task Should_Not_Create_Departamento_With_Duplicate_Nome()
        {
            base.CreateTestDepartamentos();
            await base.DepartamentoAppService.Save(new DepartamentoInput()
            {
                Nome = "RH"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Departamento()
        {
            base.CreateTestDepartamentos();
            await base.DepartamentoAppService.Delete(new IdInput(1));
            await base.DepartamentoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}