﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Geral.Profissao;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class ProfissaoAppService_Tests : ProfissaoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateProfissaoAndTestAssync(string codigo, string titulo)
        {
            await ProfissaoAppService.Save(new ProfissaoInput()
            {
                Codigo = codigo,
                Titulo = titulo
            });
            await UsingDbContext(async context =>
            {
                var createdProfissao = await context.Profissoes.FirstOrDefaultAsync(x => x.Codigo == codigo);
                createdProfissao.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Profissao()
        {
            await CreateProfissaoAndTestAssync("001", "Analista de Sistemas");
        }

        [Fact]
        public async Task Should_Not_Create_Profissao_With_Duplicate_Codigo()
        {
            CreateTestProfissoes();
            await ProfissaoAppService.Save(new ProfissaoInput()
            {
                Codigo = "001",
                Titulo = "Torneiro Mecânico"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Not_Create_Profissao_With_Duplicate_Titulo()
        {
            CreateTestProfissoes();
            await ProfissaoAppService.Save(new ProfissaoInput()
            {
                Codigo = "999",
                Titulo = "Analista de Sistemas"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_Profissao()
        {
            CreateTestProfissoes();
            await ProfissaoAppService.Delete(new IdInput(1));
            await ProfissaoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}