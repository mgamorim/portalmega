﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Geral.RamoDoFornecedor;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class RamoDoFornecedorAppService_Tests : RamoDoFornecedorAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateRamoDoFornecedorAndTestAsync(String nome)
        {
            await base.RamoDoFornecedorAppService.Save(new RamoDoFornecedorInput()
            {
                IsActive = true,
                Nome = nome
            });
            await UsingDbContext(async context =>
            {
                var createdRamoDoFornecedor = await context.RamosDoFornecedor.FirstOrDefaultAsync(x => x.Nome == nome);
                createdRamoDoFornecedor.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_RamoDoFornecedor()
        {
            await CreateRamoDoFornecedorAndTestAsync("TI");
        }

        [Fact]
        public async Task Should_Not_Create_RamoDoFornecedor_With_Duplicate_Nome()
        {
            base.CreateTestRamosDosFornecedores();
            await base.RamoDoFornecedorAppService.Save(new RamoDoFornecedorInput()
            {
                Nome = "Imóveis"
            }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_RamoDoFornecedor()
        {
            base.CreateTestRamosDosFornecedores();
            await base.RamoDoFornecedorAppService.Delete(new IdInput(1));
            await base.RamoDoFornecedorAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}