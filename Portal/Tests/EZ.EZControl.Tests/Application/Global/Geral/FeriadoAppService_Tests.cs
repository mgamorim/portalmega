﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Domain.Global.Localidade;
using EZ.EZControl.Dto.Global.Geral.Feriado;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class FeriadoAppService_Tests : FeriadoAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateFeriadoAndTestAsync(AbrangenciaDoFeriadoEnum abrangencia, DateTime data, string nome, TipoDeFeriadoEnum tipo, Pais pais)
        {
            var _pais = await PaisAppService.GetById(new IdInput(1));
            await FeriadoAppService.Save(
                new FeriadoInput()
                {
                    Abrangencia = abrangencia,
                    Data = data,
                    Nome = nome,
                    PaisId = _pais.Id,
                    Tipo = tipo
                });
        }

        [Fact]
        public async Task Should_Create_Feriado()
        {
            CreateTestPais();
            await CreateFeriadoAndTestAsync(AbrangenciaDoFeriadoEnum.Nacional, new DateTime(2016, 9, 7), "Independência do Brasil", TipoDeFeriadoEnum.Ordinario, new Pais() { Id = 1 });
            await CreateFeriadoAndTestAsync(AbrangenciaDoFeriadoEnum.Nacional, new DateTime(2016, 4, 21), "Descobrimento do Brasil", TipoDeFeriadoEnum.Ordinario, new Pais() { Id = 1 });
        }

        [Fact]
        public async Task Should_Not_Create_Duplicate_Feriado()
        {
            CreateTestFeriados();
            var pais = await PaisAppService.GetById(new IdInput(1));
            var estado = await EstadoAppService.GetById(new IdInput(1));
            var cidade = await CidadeAppService.GetById(new IdInput(1));
            await FeriadoAppService.Save(
                new FeriadoInput()
                {
                    Abrangencia = AbrangenciaDoFeriadoEnum.Municipal,
                    Data = new DateTime(2016, 11, 21),
                    Nome = "Zumbi",
                    EstadoId = estado.Id,
                    CidadeId = cidade.Id,
                    PaisId = pais.Id,
                    Tipo = TipoDeFeriadoEnum.Ordinario
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task DeleteFeriado()
        {
            CreateTestFeriados();
            await FeriadoAppService.Delete(new IdInput(1));
            await FeriadoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}