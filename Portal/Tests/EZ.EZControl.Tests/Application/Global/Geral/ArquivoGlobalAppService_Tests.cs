﻿using Abp.Runtime.Validation;
using EZ.EZControl.Domain.Global.Enums;
using EZ.EZControl.Dto.Global.Geral.Arquivo;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class ArquivoGlobalAppService_Tests : ArquivoGlobalAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateArquivoAndTestAsync(String nome, TipoDeArquivoEnum tipoDeArquivo, Byte[] conteudo)
        {
            await base.ArquivoAppService.Save(new ArquivoGlobalInput
            {
                IsActive = true,
                Nome = nome,
                TipoDeArquivoFixo = tipoDeArquivo,
                Conteudo = conteudo,
                Path = @"C:\Uploads\Global",
                Token = Guid.NewGuid().ToString("N")
            });
        }

        [Fact]
        public async Task Should_Create_Arquivo()
        {
            base.CreateTestArquivos();
            await CreateArquivoAndTestAsync("Nota Fiscal", TipoDeArquivoEnum.PDF, new Byte[1]);
            await CreateArquivoAndTestAsync("Planilha Hospitalar", TipoDeArquivoEnum.XLS, new Byte[2]);
        }

        [Fact]
        public async Task Should_Not_Create_Arquivo_With_Duplicate_Nome()
        {
            base.CreateTestArquivos();
            await base.ArquivoAppService.Save(
                new ArquivoGlobalInput
                {
                    IsActive = true,
                    Nome = "Recibo",
                    TipoDeArquivoFixo = TipoDeArquivoEnum.PDF,
                    Conteudo = new Byte[10],
                    Path = @"C:\Uploads\Global",
                    Token = Guid.NewGuid().ToString("N"),
                })
                .ShouldThrowAsync<AbpValidationException>();
        }

        //[Fact]
        //public async Task Delete_Arquivo()
        //{
        //    base.CreateTestArquivos();
        //    await base.ArquivoAppService.Delete(new IdInput(1));
        //    await base.ArquivoAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        //}
    }
}