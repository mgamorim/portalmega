﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Dto.Global.Geral.ConexaoSMTP;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class ConexaoSMTPAppService_Tests : ConexaoSMTPAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            // Deixar comentado para não registrar uma conexão fake
            base.PreInitialize();
        }

        private async Task CreateConexaoSMTPAndTestAsync(string nome, int porta, string servidorSMTP, bool requerAutenticacao, bool requerConexaoCriptografada, string senhaAutenticacao, string usuarioAutenticacao)
        {
            await ConexaoSMTPAppService.Save(
                new ConexaoSMTPInput()
                {
                    Nome = nome,
                    Porta = porta,
                    RequerAutenticacao = requerAutenticacao,
                    RequerConexaoCriptografada = requerConexaoCriptografada,
                    SenhaAutenticacao = senhaAutenticacao,
                    ServidorSMTP = servidorSMTP,
                    UsuarioAutenticacao = usuarioAutenticacao,
                    IsActive = true,
                    EnderecoDeEmailPadrao = usuarioAutenticacao
                });
        }

        [Fact]
        public async Task Should_Create_ConexaoSMTP()
        {
            await CreateConexaoSMTPAndTestAsync("Gmail", 587, "smtp.gmail.com", true, true, "gmailteste", "romariz@gmail.com");
            await CreateConexaoSMTPAndTestAsync("Outlook", 587, "smtp.outlook.com", true, true, "outlookteste", "fabioromariz@hotmail.com");
        }

        [Fact]
        public async Task Should_Not_Create_ConexaoSMTP_With_Duplicate_Nome()
        {
            CreateTestConexoesSMTP();
            await ConexaoSMTPAppService.Save(
                new ConexaoSMTPInput()
                {
                    Nome = "GMail",
                    Porta = 544,
                    RequerAutenticacao = true,
                    RequerConexaoCriptografada = false,
                    SenhaAutenticacao = "senhaAlterada",
                    ServidorSMTP = "smtp.gmail.com",
                    UsuarioAutenticacao = "romariz@gmail.com",
                    IsActive = true,
                    EnderecoDeEmailPadrao = "romariz@gmail.com"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Delete_ConexaoSMTP()
        {
            CreateTestConexoesSMTP();
            await ConexaoSMTPAppService.Delete(new IdInput(1));
            await ConexaoSMTPAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }
    }
}