﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using Abp.UI;
using EZ.EZControl.Domain.Global.Geral;
using EZ.EZControl.Dto.Global.Geral.ItemHierarquia;
using EZ.EZControl.Tests.Application.Global.Geral.Mocks;
using Shouldly;
using System.Data.Entity;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Application.Global.Geral
{
    public class ItemHierarquiaAppService_Tests : ItemHierarquiaAppServiceTestBase
    {
        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        private async Task CreateItemHierarquiaAndTestAsync(string codigo, string mascara, string nome, ItemHierarquia itemHierarquiaPai)
        {
            await ItemHierarquiaAppService.Save(
                new ItemHierarquiaInput()
                {
                    Codigo = codigo,
                    Mascara = mascara,
                    Nome = nome,
                    ItemHierarquiaPaiId = itemHierarquiaPai != null ? itemHierarquiaPai.Id : (int?)null
                });
            await UsingDbContext(async context =>
            {
                var createdItemHierarquia = await context.ItensHierarquia.FirstOrDefaultAsync(i => i.Codigo == codigo);
                createdItemHierarquia.ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_ItemHierarquia()
        {
            await CreateItemHierarquiaAndTestAsync("001", "9", "Gerência de Atendimento", null);
            await CreateItemHierarquiaAndTestAsync("002", "9", "Gerência de Desenvolvimento", null);
        }

        [Fact]
        public async Task Should_Not_Create_ItemHierarquia_With_Duplicate_Codigo()
        {
            CreateTestItensHierarquia();
            await ItemHierarquiaAppService.Save(
                new ItemHierarquiaInput()
                {
                    Codigo = "001",
                    Mascara = "999",
                    Nome = "Gerência Alterada"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Not_Create_ItemHierarquia_With_Duplicate_Nome()
        {
            CreateTestItensHierarquia();
            await ItemHierarquiaAppService.Save(
                new ItemHierarquiaInput()
                {
                    Codigo = "008",
                    Mascara = "999",
                    Nome = "Gerência de Desenvolvimento"
                }).ShouldThrowAsync<AbpValidationException>();
        }

        [Fact]
        public async Task Should_Delete_ItemHierarquia()
        {
            CreateTestItensHierarquia();
            await ItemHierarquiaAppService.Delete(new IdInput(1));
            await ItemHierarquiaAppService.GetById(new IdInput(1)).ShouldThrowAsync<UserFriendlyException>();
        }

        [Fact]
        public async Task Should_Create_ItemHierarquia_Whith_ItemHierarquiaPai()
        {
            await CreateItemHierarquiaAndTestAsync("001", "99", "Gerência de Desenvolvimento", null);
            var itemHierarquiaPai = await ItemHierarquiaAppService.GetById(new IdInput(1));
            await CreateItemHierarquiaAndTestAsync("002", "99.99", "Arquitetura de Software", new ItemHierarquia() { Id = itemHierarquiaPai.Id });
        }
    }
}