﻿using Abp.Authorization;
using Abp.Runtime.Caching;
using EZ.EZControl.Authorization.Permissions;
using EZ.EZControl.Authorization.Roles;
using EZ.EZControl.Authorization.Users;
using EZ.EZControl.Common.Extensions;
using EZ.EZControl.Domain.Global.Permissao;
using EZ.EZControl.Domain.Global.SubtiposPessoa;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using EZ.EZControl.MultiTenancy;
using EZ.EZControl.Services.Global.SubtiposPessoas.Interfaces;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace EZ.EZControl.Tests.Authorization.PermissaoUsuarioPorEmpresa
{
    public class PermissaoUsuarioPorEmpresa_Tests : AppTestBase
    {
        private readonly IPermissionChecker PermissionChecker;
        private readonly IEmpresaService EmpresaService;
        private readonly UserManager UserManager;
        private readonly RoleManager RoleManager;
        private readonly IPermissionManager PermissionManager;
        private readonly ICacheManager CacheManager;

        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        public PermissaoUsuarioPorEmpresa_Tests()
        {
            PermissionChecker = Resolve<IPermissionChecker>();
            EmpresaService = Resolve<IEmpresaService>();
            UserManager = Resolve<UserManager>();
            PermissionManager = Resolve<IPermissionManager>();
            CacheManager = Resolve<ICacheManager>();
            RoleManager = Resolve<RoleManager>();
        }

        private void CreateEmpresaClaim(Empresa empresa)
        {
            ClaimsPrincipal claimsPrincipal = Thread.CurrentPrincipal as ClaimsPrincipal;
            claimsPrincipal.AddOrUpdateClaim("EmpresaId", empresa.Id.ToString());
        }

        private void EZSeed()
        {
            UsingDbContext(context =>
            {
                var ezSeed = new EzSeed(context, 1);
                ezSeed.Create();
            });
        }

        private void PrepareTest()
        {
            EZSeed();

            var empresa = EmpresaService.GetAll().FirstOrDefault();

            CreateEmpresaClaim(empresa);
        }

        private async Task<User> LogonUser()
        {
            var ezsoft = await GetUserByUserNameAsync("ezsoft");

            LoginAsTenant(Tenant.DefaultTenantName, ezsoft.UserName);

            return ezsoft;
        }

        [Fact]
        public async Task ShouldBeGranted_Empresa1()
        {
            int permissionsGranted = 0;

            var permissions = new[]
            {
                "Pages.Administration.Languages", "Pages.Administration.Languages.Create",
                "Pages.Administration.Languages.Delete", "Pages.Administration.Languages.Edit",
                "Pages.Administration.Languages.ChangeTexts"
            };

            await this.RunAsyncInTransaction(async (uow) =>
            {
                PrepareTest();

                foreach (var permission in permissions)
                {
                    if (await PermissionChecker.IsGrantedAsync(permission))
                    {
                        permissionsGranted++;
                    }
                }
            });

            permissionsGranted.ShouldBe(5);
        }

        [Fact]
        public async Task ShouldNotBeGranted_Empresa2()
        {
            int permissionsProhibited = 0;

            var permissions = new[]
            {
                "Pages.Administration.Languages", "Pages.Administration.Languages.Create",
                "Pages.Administration.Languages.Delete", "Pages.Administration.Languages.Edit",
                "Pages.Administration.Languages.ChangeTexts"
            };

            await this.RunAsyncInTransaction(async (uow) =>
            {
                PrepareTest();

                var empresa = EmpresaService.GetAll().FirstOrDefault(x => x.NomeFantasia == "Safe Care");

                if (empresa == null)
                {
                    throw new Exception("O seed não gerou duas empresas cadastradas para realizar o teste");
                }

                CreateEmpresaClaim(empresa);

                foreach (var permission in permissions)
                {
                    if (!await PermissionChecker.IsGrantedAsync(permission))
                    {
                        permissionsProhibited++;
                    }
                }
            });

            permissionsProhibited.ShouldBe(5);
        }

        [Fact]
        public async Task ShouldAddPermissionsByUserAndBeGranted_Empresa2()
        {
            int permissionsGranted = 0;

            var permissions = new List<string>()
            {
                "Pages.Administration.Languages", "Pages.Administration.Languages.Create",
                "Pages.Administration.Languages.Delete", "Pages.Administration.Languages.Edit",
                "Pages.Administration.Languages.ChangeTexts"
            };

            // Adicionar novas permissões para o usuario na empresa 2
            await this.RunAsyncInTransaction(async (uow) =>
            {
                PrepareTest();

                var empresa = EmpresaService.GetAll().FirstOrDefault(x => x.NomeFantasia == "EZSoft");

                if (empresa == null)
                {
                    throw new Exception("O seed não gerou duas empresas cadastradas para realizar o teste");
                }

                CreateEmpresaClaim(empresa);

                var user = await GetCurrentUserAsync();
                var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

                List<string> newPermissions = new List<string>();
                newPermissions.AddRange(permissions);
                newPermissions.AddRange(grantedPermissions.Select(x => x.Name));

                var permissionsToUpdated = PermissionManager.GetPermissionsFromNamesByValidating(newPermissions);
                await UserManager.SetGrantedPermissionsAsync(user, permissionsToUpdated);

                var cacheKey = user.Id + "@" + GetCurrentTenant().Id + "@" + empresa.Id;
                CacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).Remove(cacheKey);

                foreach (var permission in permissions)
                {
                    if (await PermissionChecker.IsGrantedAsync(permission))
                    {
                        permissionsGranted++;
                    }
                }

                permissionsGranted.ShouldBe(5);
            });
        }

        [Fact]
        public async Task ShouldAddAndResetPermissionsByUser_Empresa2()
        {
            int permissionsGranted = 0;

            var permissions = new List<string>()
            {
                "Pages.Administration.Languages", "Pages.Administration.Languages.Create",
                "Pages.Administration.Languages.Delete", "Pages.Administration.Languages.Edit",
                "Pages.Administration.Languages.ChangeTexts"
            };

            // Adicionar novas permissões para o usuario na empresa 2
            await this.RunAsyncInTransaction(async (uow) =>
            {
                PrepareTest();

                var empresa = EmpresaService.GetAll().FirstOrDefault(x => x.NomeFantasia == "Safe Care");

                if (empresa == null)
                {
                    throw new Exception("O seed não gerou duas empresas cadastradas para realizar o teste");
                }

                CreateEmpresaClaim(empresa);

                var user = await GetCurrentUserAsync();
                var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

                List<string> newPermissions = new List<string>();
                newPermissions.AddRange(permissions);
                newPermissions.AddRange(grantedPermissions.Select(x => x.Name));

                var permissionsToUpdated = PermissionManager.GetPermissionsFromNamesByValidating(newPermissions);
                await UserManager.SetGrantedPermissionsAsync(user, permissionsToUpdated);

                var cacheKey = user.Id + "@" + GetCurrentTenant().Id + "@" + empresa.Id;
                CacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).Remove(cacheKey);

                foreach (var permission in permissions)
                {
                    if (await PermissionChecker.IsGrantedAsync(permission))
                    {
                        permissionsGranted++;
                    }
                }

                permissionsGranted.ShouldBe(5);

                CacheManager.GetCache(PermissaoEmpresaPorUsuario.CacheStoreName).Remove(cacheKey);

                await UserManager.ResetAllPermissionsAsync(user);

                await uow.Current.SaveChangesAsync();

                var permissionsProhibited = 0;

                foreach (var permission in permissions)
                {
                    if (!await PermissionChecker.IsGrantedAsync(permission))
                    {
                        permissionsProhibited++;
                    }
                }

                permissionsProhibited.ShouldBe(5);
            });
        }

        [Fact]
        public async Task ShouldAddPermissionsByRoleAndBeGranted_Empresa2()
        {
            int permissionsGranted = 0;

            var permissions = new List<string>()
            {
                "Pages.Administration.Languages", "Pages.Administration.Languages.Create",
                "Pages.Administration.Languages.Delete", "Pages.Administration.Languages.Edit",
                "Pages.Administration.Languages.ChangeTexts"
            };

            // Adicionar novas permissões para o usuario na empresa 2
            await this.RunAsyncInTransaction(async (uow) =>
            {
                PrepareTest();

                var empresa = EmpresaService.GetAll().FirstOrDefault(x => x.NomeFantasia == "EZSoft");

                if (empresa == null)
                {
                    throw new Exception("O seed não gerou duas empresas cadastradas para realizar o teste");
                }

                CreateEmpresaClaim(empresa);

                var user = await GetCurrentUserAsync();
                var role = await RoleManager.GetRoleByNameAsync("Admin");
                var grantedPermissions = await RoleManager.GetGrantedPermissionsAsync(role);

                List<string> newPermissions = new List<string>();
                newPermissions.AddRange(permissions);
                newPermissions.AddRange(grantedPermissions.Select(x => x.Name));

                var permissionsToUpdated = PermissionManager.GetPermissionsFromNamesByValidating(newPermissions);
                await RoleManager.SetGrantedPermissionsAsync(role, permissionsToUpdated);

                var cacheKey = user.Id + "@" + GetCurrentTenant().Id + "@" + empresa.Id;
                CacheManager.GetCache(PermissaoEmpresaPorRole.CacheStoreName).Remove(cacheKey);

                foreach (var permission in permissions)
                {
                    if (await PermissionChecker.IsGrantedAsync(permission))
                    {
                        permissionsGranted++;
                    }
                }

                permissionsGranted.ShouldBe(5);
            });
        }
    }
}