﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;

namespace EZ.EZControl.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(EZControlDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}
