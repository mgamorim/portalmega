﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using ML.MarcosLima.DataBuilder.Cms.Site;

namespace EZ.EZControl.Tests.TestDatas.Cms.Template
{
    public class TestTemplateDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestTemplateDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TemplateDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
