﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using ML.MarcosLima.DataBuilder.Cms.Post;

namespace EZ.EZControl.Tests.TestDatas.Cms.Post
{
    public class TestPostDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestPostDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new PostDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
