﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Tests.TestDatas.Cms.Categoria;
using EZ.EZControl.Tests.TestDatas.Cms.Pagina;
using EZ.EZControl.Tests.TestDatas.Cms.Post;
using EZ.EZControl.Tests.TestDatas.Cms.Site;
using EZ.EZControl.Tests.TestDatas.Cms.Template;

namespace EZ.EZControl.Tests.TestDatas.Cms
{
    public class TestCmsDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestCmsDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TestTemplateDataBuilder(_context, _tenantId, _empresaId).Create();
            new TestSiteDataBuilder(_context, _tenantId, _empresaId).Create();
            new TestCategoriaDataBuilder(_context, _tenantId, _empresaId).Create();
            new TestPaginaDataBuilder(_context, _tenantId, _empresaId).Create();
            new TestPostDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
