﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using ML.MarcosLima.DataBuilder.Cms.Site;

namespace EZ.EZControl.Tests.TestDatas.Cms.Site
{
    public class TestSiteDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestSiteDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new SiteDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
