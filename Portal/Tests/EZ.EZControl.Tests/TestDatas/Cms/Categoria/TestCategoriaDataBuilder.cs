﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using ML.EZControl.DataBuilder.Cms.Categoria;

namespace EZ.EZControl.Tests.TestDatas.Cms.Categoria
{
    public class TestCategoriaDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestCategoriaDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new CategoriaDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
