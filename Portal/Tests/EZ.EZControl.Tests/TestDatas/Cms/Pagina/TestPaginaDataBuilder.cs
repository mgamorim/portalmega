﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using ML.MarcosLima.DataBuilder.Cms.Pagina;

namespace EZ.EZControl.Tests.TestDatas.Cms.Pagina
{
    public class TestPaginaDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestPaginaDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new PaginaDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
