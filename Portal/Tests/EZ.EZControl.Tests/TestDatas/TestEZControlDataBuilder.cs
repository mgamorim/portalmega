﻿using EntityFramework.DynamicFilters;
using EZ.EZControl.EntityFramework;
using EZ.EZControl.Tests.TestDatas.Cms;

namespace EZ.EZControl.Tests.TestDatas
{
    public class TestEZControlDataBuilder
    {
        private readonly EZControlDbContext _context;
        private readonly int _tenantId;
        private readonly int _empresaId;

        public TestEZControlDataBuilder(EZControlDbContext context, int tenantId, int empresaId)
        {
            _context = context;
            _tenantId = tenantId;
            _empresaId = empresaId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TestCmsDataBuilder(_context, _tenantId, _empresaId).Create();

            _context.SaveChanges();
        }
    }
}
