﻿using System;
using EZ.EZControl.Migrations.Seed.EZSeeds;
using Xunit;
using System.Data.Entity.Validation;

namespace EZ.EZControl.Tests.Seeds
{
    public class EZSeed_Tests : AppTestBase
    {
        private EzSeed _ezSeed;

        protected override void PreInitialize()
        {
            base.PreInitialize();
        }

        [Fact]
        public void SeedEmpresas_Test()
        {
            try
            {
                UsingDbContext(context =>
                {
                    _ezSeed = new EzSeed(context, 1);
                    _ezSeed.Create();
                });
            }
            catch (DbEntityValidationException dbEx)
            {
                throw dbEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}