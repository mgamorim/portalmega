﻿using System.Data.SqlClient;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.General
{
    public class ConnectionString_Tests
    {
        [Fact]
        public void SqlConnectionStringBuilder_Test()
        {
            var csb = new SqlConnectionStringBuilder("Server=localhost; Database=EZControl; Trusted_Connection=True;");
            csb["Database"].ShouldBe("EZControl");
        }
    }
}
