﻿using EZ.EZControl.Domain.EZLiv.Geral;
using EZ.EZControl.Services.EZLiv.Interfaces;
using Shouldly;
using System;
using Xunit;

namespace EZ.EZControl.Tests.Domain.EZLiv.Geral
{
    public class ContratoDomainService_Tests : ContratoDomainServiceTestBase
    {
        private readonly IContratoService _contratoService;

        public ContratoDomainService_Tests()
        {
            _contratoService = Resolve<IContratoService>();
        }

        [Fact]
        public void Should_Without_Items_GetTipoDeEventosBySistema()
        {
            #region Arrange
            CreateProdutoDePlanoDeSaudeEntity();

            Contrato contrato = new Contrato()
            {
                DataCriacao = DateTime.Now,
                DataInicioVigencia = DateTime.Now.AddMonths(1),
                DataFimVigencia = DateTime.Now.AddMonths(12),
                ProdutoDePlanoDeSaudeId = 1,
                EmpresaId = EmpresaIdDefaultTest
            };
            #endregion

            #region Action
            var result = _contratoService.CreateOrUpdateAndReturnSavedEntity(contrato);
            #endregion

            var contratoNew = new Contrato();
            UsingDbContext(context =>
            {
                contratoNew = context.Contratos.Find(1);
            });

            contratoNew.DataFimVigencia = contratoNew.DataFimVigencia.AddYears(1);
            result = _contratoService.CreateOrUpdateAndReturnSavedEntity(contratoNew);

            #region Assert
            result.ShouldNotBeNull();
            #endregion
        }
    }
}
