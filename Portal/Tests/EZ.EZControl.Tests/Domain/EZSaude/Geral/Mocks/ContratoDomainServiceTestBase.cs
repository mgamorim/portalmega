﻿using EZ.EZControl.Domain.Estoque.Enums;
using EZ.EZControl.Domain.EZSaude.Enums;
using EZ.EZControl.Domain.EZSaude.Geral;
using EZ.EZControl.Services.EZSaude.Interfaces;

namespace EZ.EZControl.Tests.Domain.EZSaude.Geral
{
    public class ContratoDomainServiceTestBase : AppTestBase
    {
        private readonly IContratoService _contratoService;
        private readonly IProdutoDePlanoDeSaudeService _produtoDePlanoDeSaudeService;

        public ContratoDomainServiceTestBase()
        {
            _contratoService = Resolve<IContratoService>();
            _produtoDePlanoDeSaudeService = Resolve<IProdutoDePlanoDeSaudeService>();
        }

        private void CreatePlanoDeSaudeEntity()
        {
            PlanoDeSaude plano = new PlanoDeSaude
            {
                IsActive = true,
                NumeroDeRegistro = "ANS 475.261/16-9",
                Reembolso = false,
                DescricaoDoReembolso = "Não há",
                Abrangencia = AbrangenciaDoPlanoEnum.Estadual,
                Acomodacao = AcomodacaoEnum.Enfermaria,
                PlanoDeSaudeAns = PlanoDeSaudeAnsEnum.Safira207,
                SegmentacaoAssistencial = SegmentacaoAssistencialDoPlanoEnum.HospComObstetriciaAmbulatorial,
                EmpresaId = EmpresaIdDefaultTest
            };

            UsingDbContext(context =>
            {
                context.PlanosDeSaude.Add(plano);
            });
        }

        protected void CreateProdutoDePlanoDeSaudeEntity()
        {
            CreatePlanoDeSaudeEntity();

            ProdutoDePlanoDeSaude produto = new ProdutoDePlanoDeSaude()
            {
                IsActive = true,
                PlanoDeSaudeId = 1,
                Nome = "Especial 100",
                Valor = 250.00m,
                TipoDeProduto = TipoDeProdutoEnum.Virtual,
                Descricao = "O maior custo beneficio.",
                EmpresaId = EmpresaIdDefaultTest
            };

            UsingDbContext(context =>
            {
                context.ProdutosDePlanoDeSaude.Add(produto);
            });
        }
    }
}
