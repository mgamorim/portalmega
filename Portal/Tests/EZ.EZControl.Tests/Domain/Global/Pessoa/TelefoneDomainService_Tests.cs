﻿using Shouldly;
using System;
using Abp.Dependency;
using Abp.UI;
using EZ.EZControl.Domain.Global.Pessoa;
using EZ.EZControl.Services.Global.Pessoa.Interfaces;
using Xunit;

namespace EZ.EZControl.Tests.Domain.Agenda.Regras
{
    public class TelefoneDomainService_Tests : ITransientDependency
    {
        private readonly ITelefoneService _telefoneService;

        public TelefoneDomainService_Tests()
        {
            _telefoneService = IocManager.Instance.Resolve<ITelefoneService>();
        }

        [Fact]
        public void Deve_Ter_Sucesso_Preencher_Numero_CelularBr_By_String()
        {
            #region Arrange
            Telefone telefone = new Telefone();
            var numero = "(21) 99708-8787";
            #endregion

            #region Action
            _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, numero);
            #endregion

            #region Assert
            telefone.DDD.ShouldBe("21");
            telefone.Numero.ShouldBe("997088787");
            #endregion
        }


        [Fact]
        public void Nao_Deve_Ter_Sucesso_FormatoIncorreto_Preencher_Numero_CelularBr_By_String()
        {
            #region Arrange
            Telefone telefone = new Telefone();
            var numero = "(21) 997088787"; // Formato incorreto - deve ser (99) 99999-9999
            #endregion

            #region Action and Assert

            Should.Throw<UserFriendlyException>(() =>
            {
                _telefoneService.PreencherNumeroCelularBrComMascaraByString(telefone, numero);
            });
            #endregion
        }
    }
}
