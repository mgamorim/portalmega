﻿using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.Agenda.Interfaces.Regra;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Domain.Agenda.Regras
{
    public class RegraBaseDomainService_Tests : AppTestBase
    {
        private readonly IRegraBaseService _regraBaseService;

        public RegraBaseDomainService_Tests()
        {
            _regraBaseService = Resolve<IRegraBaseService>();
        }

        [Fact]
        public void Should_Without_Items_GetTipoDeEventosBySistema()
        {
            #region Arrange
            SistemaEnum sistemaAgenda = SistemaEnum.Agenda;
            SistemaEnum sistemaCms = SistemaEnum.CMS;
            #endregion

            #region Action
            var tiposDeEventosBySistemaAgenda = _regraBaseService.GetTiposDeEventosBySistema(sistemaAgenda);
            var tiposDeEventosBySistemaCms = _regraBaseService.GetTiposDeEventosBySistema(sistemaCms);
            #endregion

            #region Assert
            tiposDeEventosBySistemaAgenda.Count.ShouldBe(2);
            tiposDeEventosBySistemaCms.Count.ShouldBe(0);
            #endregion
        }

        [Fact]
        public void Should_With_Items_GetTipoDeEventosBySistemaGlobal()
        {
            #region Arrange
            SistemaEnum sistema = SistemaEnum.Global;
            #endregion

            #region Action
            var tiposDeEventosBySistema = _regraBaseService.GetTiposDeEventosBySistema(sistema);
            #endregion

            #region Assert
            tiposDeEventosBySistema.Count.ShouldBe(2);
            #endregion
        }

        [Fact]
        public void Should_With_Items_GetTipoDeEventosBySistemaEzMedical()
        {
            #region Arrange
            SistemaEnum sistema = SistemaEnum.EZMedical;
            #endregion

            #region Action
            var tiposDeEventosBySistema = _regraBaseService.GetTiposDeEventosBySistema(sistema);
            #endregion

            #region Assert
            tiposDeEventosBySistema.Count.ShouldBe(1);
            #endregion
        }

    }
}
