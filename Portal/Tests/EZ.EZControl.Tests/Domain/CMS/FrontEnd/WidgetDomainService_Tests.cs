﻿using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Domain.Core.Enums;
using EZ.EZControl.Services.CMS.Interfaces;
using Shouldly;
using System.Collections.Generic;
using Xunit;

namespace EZ.EZControl.Tests.Domain.CMS.FrontEnd
{
    public class WidgetDomainService_Tests : AppTestBase
    {
        private readonly IWidgetHTMLService _widgetHtmlService;
        private readonly IWidgetMenuService _widgetMenuService;
        private readonly IMenuService _menuService;
        private readonly string _widgetHtml;
        private readonly string _widgetMenu;

        public WidgetDomainService_Tests()
        {
            this._widgetHtmlService = Resolve<IWidgetHTMLService>();
            this._widgetMenuService = Resolve<IWidgetMenuService>();
            this._menuService = Resolve<IMenuService>();

            this._widgetHtml =
                "<div class=\"ezwidget ezwidget-id-1 ezwidget-tipo-CmsHtml\"><div class=\"ezwidget-titulo\"><h3>Teste HTML</h3></div><div class=\"ezwidget-conteudo\"><p>Teste</p></div></div>";

            this._widgetMenu =
                "<div class=\"ezwidget ezwidget-id-1 ezwidget-tipo-CmsHtml\"><div class=\"ezwidget-titulo\"><h3>Teste Menu</h3></div><div class=\"ezwidget-conteudo\"><div class='ezmenu ezmenu-id-1'>\r\n<ul>\r\n<li class='ezitemdemenu ezitemdemenu-id-1 ezitemdemenu-tipo-link'>\r\n<a href='www.hinode.com.br/submenu'>Submenu</a>\r\n</li>\r\n</ul>\r\n</div>\r\n</div></div>";
        }

        [Fact]
        public void Should_Create_Html_Widget_Html()
        {
            #region Arrange
            var template = CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                 "sidebar, top-menu, rodape",
                                                 "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                 "Home, Contato, QuemSomos, Login",
                                                 "Login",
                                                 "Home",
                                                 "QuemSomos",
                                                 "Contato");
            var site = CreateSite("EZControl", string.Empty, string.Empty, string.Empty, string.Empty, template, "Login", "QuemSomos", "Contato", "Home");
            var widget = _widgetHtmlService.CreateAndReturnEntity(CreateWidgetHtml("<p>Teste</p>", "sidebar", "Teste HTML", true, site, template));
            #endregion

            #region Action
            var widgetHtml = _widgetHtmlService.CreateWidgetHtml(widget.Result);
            #endregion

            #region Assert
            widgetHtml.ShouldNotBe(string.Empty);
            widgetHtml.ShouldBe(_widgetHtml);
            #endregion
        }

        [Fact]
        public void Should_Create_Html_Widget_Menu()
        {
            #region Arrange
            var template = CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "2.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato");
            var site = CreateSite("EZControl", string.Empty, string.Empty, string.Empty, string.Empty, template, "Login", "QuemSomos", "Contato", "Home");
            var menu = CreateMenu("Lateral", site);
            var widget = _widgetMenuService.CreateAndReturnEntity(CreateWidgetMenu("sidebar", "Teste Menu", true, site, template, menu));
            #endregion

            #region Action
            var widgetMenu = _widgetMenuService.CreateWidgetHtml(widget.Result);
            #endregion

            #region Assert
            widgetMenu.ShouldNotBe(string.Empty);
            widgetMenu.ShouldBe(_widgetMenu);
            #endregion
        }

        private Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                      string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                      string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }

        private Site CreateSite(string nome, string descricao, string metaKeywords, string metaTags, string owner, Template template,
                                string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault, string tipoDePaginaParaPostDefault)
        {
            return new Site
            {
                Nome = nome,
                Descricao = descricao,
                MetaKeywords = metaKeywords,
                MetaTags = metaTags,
                Owner = owner,
                TemplateDefault = template,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        private Menu CreateMenu(string nome, Site site)
        {
            return new Menu
            {
                Site = site,
                IsActive = true,
                Nome = nome,
                ItensDeMenu = new List<ItemDeMenu>
                {
                    new ItemDeMenu
                    {
                        IsActive = true,
                        Titulo = "Submenu",
                        DescricaoDoTitulo = "Esse Submenu é o de Contato",
                        TipoDeItemDeMenu = TipoDeItemDeMenuEnum.Link,
                        Url = "www.hinode.com.br/submenu",
                        EmpresaId = EmpresaIdDefaultTest
                    }
                },
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        public WidgetHTML CreateWidgetHtml(string conteudo, string posicao, string titulo, bool exibirTitulo, Site site, Template template)
        {
            return new WidgetHTML
            {
                Conteudo = conteudo,
                IsActive = true,
                ExibirTitulo = exibirTitulo,
                Posicao = posicao,
                Sistema = SistemaEnum.CMS,
                Site = site,
                Template = template,
                Tipo = TipoDeWidgetEnum.CmsHtml,
                Titulo = titulo,
                EmpresaId = EmpresaIdDefaultTest
            };
        }

        public WidgetMenu CreateWidgetMenu(string posicao, string titulo, bool exibirTitulo, Site site, Template template, Menu menu)
        {
            return new WidgetMenu
            {
                IsActive = true,
                ExibirTitulo = exibirTitulo,
                Posicao = posicao,
                Sistema = SistemaEnum.CMS,
                Site = site,
                Template = template,
                Tipo = TipoDeWidgetEnum.CmsHtml,
                Titulo = titulo,
                Menu = menu,
                EmpresaId = EmpresaIdDefaultTest
            };
        }
    }
}
