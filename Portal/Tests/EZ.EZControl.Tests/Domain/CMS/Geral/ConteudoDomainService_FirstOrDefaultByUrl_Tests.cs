﻿using System.Threading.Tasks;
using EZ.EZControl.Domain.CMS.Enums;
using EZ.EZControl.Services.CMS.Interfaces;
using EZ.EZControl.Tests.TestDatas.Cms.Categoria;
using EZ.EZControl.Tests.TestDatas.Cms.Pagina;
using EZ.EZControl.Tests.TestDatas.Cms.Post;
using EZ.EZControl.Tests.TestDatas.Cms.Site;
using EZ.EZControl.Tests.TestDatas.Cms.Template;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Domain.CMS.Geral
{
    public class ConteudoDomainService_FirstOrDefaultByUrl_Tests : AppTestBase
    {
        private readonly IConteudoService _conteudoService;

        public ConteudoDomainService_FirstOrDefaultByUrl_Tests()
        {
            _conteudoService = LocalIocManager.Resolve<IConteudoService>();

            UsingDbContext(context =>
            {
                new TestTemplateDataBuilder(context, AbpSession.TenantId.Value, EmpresaIdDefaultTest).Create();
                new TestSiteDataBuilder(context, AbpSession.TenantId.Value, EmpresaIdDefaultTest).Create();
                new TestPaginaDataBuilder(context, AbpSession.TenantId.Value, EmpresaIdDefaultTest).Create();
                new TestCategoriaDataBuilder(context, AbpSession.TenantId.Value, EmpresaIdDefaultTest).Create();
                new TestPostDataBuilder(context, AbpSession.TenantId.Value, EmpresaIdDefaultTest).Create();
            });
        }

        #region Testes com 1 segmento

        [Fact]
        public async Task Deve_Recuperar_Pagina_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/pagina1";

            //Action
            var conteudoPagina = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoPagina.Pagina.ShouldNotBe(null);
            conteudoPagina.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.Pagina);
        }

        [Fact]
        public async Task Nao_Deve_Recuperar_Pagina_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/pagina-impossivel-de-recuperar-5245646252";

            //Action
            var conteudoPagina = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoPagina.ShouldBe(null);
        }

        [Fact]
        public async Task Deve_Recuperar_Categoria_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.Categoria.ShouldNotBe(null);
            conteudoCategoria.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComUltimosPosts);
            conteudoCategoria.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Deve_Recuperar_PostsPorAno_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/2017";

            //Action
            var conteudoAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.PostsPorAno);
            conteudoAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Deve_Recuperar_PostsPorAno_Pagina2_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/2017";

            //Action
            var conteudoAno = await _conteudoService.FirstOrDefaultAsync(url, 2);

            //Assert
            conteudoAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.PostsPorAno);
            conteudoAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        #endregion

        #region Testes com 2 segmentos

        [Fact]
        public async Task Deve_Recuperar_SubcategoriaCom2Segmentos_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.Categoria.ShouldNotBe(null);
            conteudoCategoria.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComUltimosPosts);
            conteudoCategoria.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Nao_Deve_Recuperar_SubcategoriaCom2Segmentos_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria-impossivel-de-recuperar-5245646252";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.ShouldBe(null);
        }

        [Fact]
        public async Task Deve_Recuperar_CategoriaComPostsPorAno_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/2017";

            //Action
            var conteudoCategoriaPorAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoriaPorAno.Categoria.ShouldNotBe(null);
            conteudoCategoriaPorAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComPostsPorAno);
            conteudoCategoriaPorAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Deve_Recuperar_PostsPorMes_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/2017/03";

            //Action
            var conteudoAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.PostsPorMes);
            conteudoAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        #endregion

        #region Testes com 3 segmentos

        [Fact]
        public async Task Deve_Recuperar_SubcategoriaCom3Segmentos_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/categoria123";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.Categoria.ShouldNotBe(null);
            conteudoCategoria.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComUltimosPosts);
            conteudoCategoria.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Nao_Deve_Recuperar_SubcategoriaCom3Segmentos_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/categoria-impossivel-de-recuperar-5245646252";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.ShouldBe(null);
        }

        [Fact]
        public async Task Deve_Recuperar_CategoriaComPostsPorMes_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/2017/03";

            //Action
            var conteudoAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComPostsPorMes);
            conteudoAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Deve_Recuperar_SubCategoriaCom2SegmentosComPostsPorAno_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/2017";

            //Action
            var conteudoCategoriaPorAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoriaPorAno.Categoria.ShouldNotBe(null);
            conteudoCategoriaPorAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComPostsPorAno);
            conteudoCategoriaPorAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        #endregion

        #region Testes 4 segmentos ou mais

        [Fact]
        public async Task Deve_Recuperar_SubcategoriaCom4Segmentos_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/categoria123/categoria1234";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.Categoria.ShouldNotBe(null);
            conteudoCategoria.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComUltimosPosts);
            conteudoCategoria.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Nao_Deve_Recuperar_SubcategoriaCom4Segmentos_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/categorias123/categoria-impossivel-de-recuperar-5245646252";

            //Action
            var conteudoCategoria = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoria.ShouldBe(null);
        }

        [Fact]
        public async Task Deve_Recuperar_SubCategoriaCom3SegmentosComPostsPorAno_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/categoria123/2017";

            //Action
            var conteudoCategoriaPorAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoCategoriaPorAno.Categoria.ShouldNotBe(null);
            conteudoCategoriaPorAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComPostsPorAno);
            conteudoCategoriaPorAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Deve_Recuperar_SubCategoriaCom2SegmentosComPostsPorMes_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/categoria12/2017/03";

            //Action
            var conteudoAno = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoAno.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.CategoriaComPostsPorMes);
            conteudoAno.Posts.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Deve_Recuperar_Post_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/2017/03/post-1-1";

            //Action
            var conteudoPost = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoPost.TipoDeConteudo.ShouldBe(TipoDeConteudoEnum.Post);
            conteudoPost.Post.ShouldNotBe(null);
        }

        [Fact]
        public async Task Nao_Deve_Recuperar_Post_ByUrlAsync()
        {
            //Arrange
            var url = "http://sitedeteste.com/categoria1/2017/03/categoria-impossivel-de-recuperar-5245646252";

            //Action
            var conteudoPost = await _conteudoService.FirstOrDefaultAsync(url);

            //Assert
            conteudoPost.ShouldBe(null);
        }


        #endregion
    }
}
