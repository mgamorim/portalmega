﻿using EZ.EZControl.Domain.CMS.Geral;
using EZ.EZControl.Services.CMS.Interfaces;
using Shouldly;
using System.Linq;
using Xunit;

namespace EZ.EZControl.Tests.Domain.CMS.Geral
{
    public class TemplateDomainService_Tests : AppTestBase
    {
        private readonly ITemplateService _templateService;
        private readonly Template _template;

        public TemplateDomainService_Tests()
        {
            this._templateService = Resolve<ITemplateService>();
            this._template = CreateTemplate("EzControl", "Json EzControl", "EzSoft", "EzSoft", "1.0", "EZ",
                                                     "sidebar, top-menu, rodape",
                                                     "CmsMenu, CMSHtml, CMSOutra, QualquerCoisa",
                                                     "Home, Contato, QuemSomos, Login",
                                                     "Login",
                                                     "Home",
                                                     "QuemSomos",
                                                     "Contato");
        }

        [Fact]
        public void Should_Get_List_Posicoes()
        {
            var posicoes = _templateService.GetPosicoesByTemplate(_template);
            posicoes.ToList().Count.ShouldNotBe(0);
        }

        [Fact]
        public void Should_Get_List_TiposDeWidget()
        {
            var tiposDeWidget = _templateService.GetTiposDeWidgetSuportadosByTemplate(_template);
            tiposDeWidget.ToList().Count.ShouldNotBe(0);
        }

        [Fact]
        public void Should_Get_List_TiposDePaginas()
        {
            var tiposDePaginas = _templateService.GetTiposDePaginasSuportadasByTemplate(_template);
            tiposDePaginas.ToList().Count.ShouldNotBe(0);
        }


        public Template CreateTemplate(string nome, string descricao, string autor, string autorUrl, string versao,
                                      string nomeDoArquivo, string posicoes, string tiposDeWidget, string tiposDePaginas,
                                      string tipoDePaginaParaPaginaDefault, string tipoDePaginaParaPostDefault,
                                      string tipoDePaginaParaCategoriaDefault, string tipoDePaginaParaPaginaInicialDefault)
        {
            return new Template
            {
                IsActive = true,
                Nome = nome,
                Descricao = descricao,
                Autor = autor,
                AutorUrl = autorUrl,
                Versao = versao,
                NomeDoArquivo = nomeDoArquivo,
                Posicoes = posicoes,
                TiposDeWidgetSuportados = tiposDeWidget,
                TiposDePaginasSuportadas = tiposDePaginas,
                TipoDePaginaParaCategoriaDefault = tipoDePaginaParaCategoriaDefault,
                TipoDePaginaParaPaginaDefault = tipoDePaginaParaPaginaDefault,
                TipoDePaginaParaPaginaInicialDefault = tipoDePaginaParaPaginaInicialDefault,
                TipoDePaginaParaPostDefault = tipoDePaginaParaPostDefault,
                EmpresaId = EmpresaIdDefaultTest
            };

        }
    }
}
