﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using EZ.EZControl.EZ.EzNotification;
using EZ.EZControl.EZ.EzNotification.Enums;
using EZ.EZControl.EZ.EzNotification.Interfaces;
using Shouldly;
using Xunit;

namespace EZ.EZControl.Tests.Infra.EzNotification
{
    public class EzSmsNotificationTests : AppTestBase
    {
        private readonly ISmsNotificationService _smsNotificationService;

        public EzSmsNotificationTests()
        {
            _smsNotificationService = Resolve<ISmsNotificationService>(); ;
        }

        //[Fact]
        //public async Task Deve_Enviar_Um_Sms_Para_Um_Telefone()
        //{
        //    var sms = new SmsNotification
        //    {
        //        TipoSms = TipoSmsEnum.Curto,
        //        Numero = "21983305805",
        //        Mensagem = "Chegouuuuuuuu pelo EZLiv",
        //        Referencia = "Ref1",
        //        DataEHora = DateTime.Now,
        //        Chave = "Z6H5FAUYT9WCC4SDFDJ2X7ZW"
        //    };

        //    Should.NotThrow(async () =>
        //    {
        //        await _smsNotificationService.Send(sms);
        //    });
        //}

        //[Fact]
        //public async Task Deve_Enviar_Um_Sms_Para_Varios_Telefone()
        //{
        //    var sms = new SmsNotification
        //    {
        //        TipoSms = TipoSmsEnum.Curto,
        //        Numero = "21996554233",
        //        Mensagem = "Chegouuuuuuuu pelo EZLiv",
        //        Referencia = "Ref1",
        //        DataEHora = DateTime.Now,
        //        Chave = "Z6H5FAUYT9WCC4SDFDJ2X7ZW"
        //    };

        //    var telefones = new List<string>()
        //    {
        //        "21997080944",
        //        "21982569434"
        //    };

        //    Should.NotThrow(async () =>
        //    {
        //        await _smsNotificationService.SendAll(sms, telefones);
        //    });
        //}
    }
}
