﻿using EZ.EZControl.EZ.EzNotification.Interfaces;
using EZ.EZControl.Infra.EzNotification.Services;

namespace EZ.EZControl.Tests.Infra.EzNotification
{
    public class EzEmailNotificationTests : AppTestBase
    {
        private readonly IEmailNotificationService _emailNotificationService;

        public EzEmailNotificationTests()
        {
            _emailNotificationService = Resolve<EmailNotificationService>();
        }

        //[Fact]
        //public async Task Deve_Enviar_Um_Email()
        //{
        //    var email = TemplateDeEnvioDeEmailDeSistema.GetHtmlNotificacaoUsuarioCriadoPeloBeneficiario("marcosli", "VaiSaber", "http://www.ezliv.com.br/resetdesenha");

        //    var destinatario = "marcos@ezliv.com.br";

        //    var templateDeEmail = new TemplateEmail()
        //    {
        //        Assunto = "Teste Deve_Enviar_Um_Email - EZLiv",
        //        Remetente = "contato@ezsoft.com.br",
        //        CorpoMensagem = email,
        //        Nome = "Projeto de Teste"
        //    };

        //    var conexaoSmtp = new ConexaoSMTP()
        //    {
        //        ServidorSMTP = "mail.smtp2go.com",
        //        EnderecoDeEmailPadrao = "contato@ezsoft.com.br",
        //        Porta = 587,
        //        Nome = "Projeto Teste - EZSoft",
        //        UsuarioAutenticacao = "ezsoftdev",
        //        SenhaAutenticacao = "aHVndXVnY2pqZnd1",
        //        RequerAutenticacao = true,
        //        RequerConexaoCriptografada = false
        //    };

        //    Should.NotThrow(async () =>
        //    {
        //        await _emailNotificationService.SendEmail(destinatario, templateDeEmail, conexaoSmtp);
        //    });
        //}
    }
}
